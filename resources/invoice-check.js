$(document).ready(function(){ 

  let url = new URLSearchParams(window.location.search);


  if(url.get('module') === 'Invoice' && url.get('view') === 'Edit'){

    
   let searchBtn = document.getElementById('Invoice_editView_fieldName_salesorder_id_select');

    searchBtn.onclick = checkPop();

    function checkPop(){    
      $(window).on('shown.bs.modal', function() { 
        $('#popupModal').modal('show');       
        // ordersPop();       
    });
    }

    // function ordersPop(){
    //   let tbody = document.querySelector('.listview-table tbody');
    //   let list = tbody.querySelectorAll('.listViewEntries');
    //   for(let i = 0, len = list.length; i < len; i++){
    //     list[i].onclick = (e)=>{
    //      let orderId = e.currentTarget.getAttribute('data-id');          
    //      sendQuery(orderId);
    //     }
    //   }
    // }


    function sendQuery(orderId){       
      $.ajax({ 
        type: "POST", 
        url: "invoices/index.php",   
        data: {orderId:orderId}, 
        dataType: "json",
        success: function(result){       
          insertValues(result);                                             
          }            
      }); 
    }

    function repeat(func, times) {
      func();
      times && --times && repeat(func, times);
  }

    function insertValues(order){     

      if(order.productid.length > 1){   
        repeat(function () {   $("#addProduct").trigger("click");   },order.productid.length-1);
        for(let i = 0, len = order.productid.length; i < len; i++){ 
          order.proid = order.productid[i].productid;        
          order.weight = order.productid[i].cargo_wgt;
          order.length = order.productid[i].cargo_length;
          order.width = order.productid[i].cargo_width;
          order.height = order.productid[i].cargo_height;  
          order.margin = order.productid[i].margin;  
          document.getElementById(`productName${i+1}`).value = `${order.weight} ${order.length}x${order.width}x${order.height}`;     
          document.getElementById(`productName${i+1}`).setAttribute('disabled','disabled'); 
          document.getElementById(`hdnProductId${i+1}`).value = order.proid;
          document.querySelector(`[name="margin${i+1}"]`).value = order.margin;
        }        
      }else{
        order.productid.forEach(orderId => {
          order.proid = orderId.productid; 
          order.weight = orderId.cargo_wgt;
          order.length = orderId.cargo_length;
          order.width = orderId.cargo_width;
          order.height = orderId.cargo_height;
          order.margin = orderId.margin;
          document.getElementById(`productName1`).value = `${order.weight} ${order.length}x${order.width}x${order.height}`;
          document.getElementById(`productName1`).setAttribute('disabled','disabled');
          document.getElementById(`hdnProductId1`).value = order.proid;
          document.querySelector('[name="margin1"]').value = order.margin;
      });
      }    

     let orderID = document.getElementById('salesorder_id');
     let salesorderid = document.querySelector('[name="salesorder_id"]');
     let accountname = document.getElementById('account_id_display');
     let accoundid = document.querySelector('[name="account_id"]');
    //  let billcity = document.getElementById('Invoice_editView_fieldName_bill_city');
    //  let billstreet = document.getElementById('Invoice_editView_fieldName_bill_street');
    //  let billcode = document.getElementById('Invoice_editView_fieldName_bill_code');

    //  let shipcity = document.getElementById('Invoice_editView_fieldName_ship_city');
    //  let shipstreet = document.getElementById('Invoice_editView_fieldName_ship_street');
    //  let shipcode = document.getElementById('Invoice_editView_fieldName_ship_code');
  
     let grandTotal = document.getElementById('grandTotal');


     let createUser = _USERMETA.id;

     let createUserId = document.createElement('input');
     createUserId.setAttribute('name','create_user_id');
     createUserId.setAttribute('value','');
     createUserId.setAttribute('type','hidden');
     createUserId.value = createUser;

     salesorderid.parentElement.append(createUserId);

     orderID.value = order.order_no;
     salesorderid.value = order.salesorderid;
     orderID.setAttribute('disabled','disabled');
     accountname.value = order.name;
     accoundid.value = order.accid;

    //  billcity.value = order.billcity;
    //  billstreet.value = order.billstreet;
    //  billcode.value = order.billcode.replace('LT-','');
    //  shipcity.value = order.shipcity;
    //  shipstreet.value = order.shipstreet;
    //  shipcode.value = order.shipcode.replace('LT-','');
     grandTotal.innerText = parseFloat(order.price).toFixed(2);

    }        


}

});