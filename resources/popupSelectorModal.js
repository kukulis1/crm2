let globalE;

let clientUrl = new URLSearchParams(window.location.search);

$(document).ready(function () {
  if (clientUrl.get('module') == 'Accounts' && (clientUrl.get('view') == 'Detail' || clientUrl.get('view') == 'Edit')) {
    if (document.querySelector('[data-block="Kainyno zonos"]') != null) {
      if (document.querySelector('[data-block="Kainyno zonos"] h4').innerHTML == 'Kainyno zonos') {
        locationSelector();
        includeHTML();
      }
    }
  }
});


async function locationSelector() {
  if (document.querySelector('[data-block="Kainyno zonos"]') != null) {
    if (document.querySelector('[data-block="Kainyno zonos"] h4').innerHTML = 'Kainyno zonos') {
      const tablas = document.querySelector('[data-block="Kainyno zonos"]');

      const table2 = document.querySelectorAll('[data-block="Kainyno zonos"] td select:not(.except)');


      const popUp = document.createElement('div');
      popUp.setAttribute('id', 'popup');
      popUp.setAttribute('w3-include-html', '/price-algorithm/popup.html');
      tablas.append(popUp);

      const divas = document.createElement("div");
      divas.setAttribute('id', 'wait');
      divas.setAttribute('style', 'display:none;position:absolute;top:55%;left:43%;padding:2px;');

      const img = document.createElement('img');
      img.setAttribute('src', '/resources/lg.ajax-spinner-gif.gif');
      img.setAttribute("width", '256');
      img.setAttribute("height", '256');
      divas.append(img);
      tablas.append(divas);

      let zones;

      if (clientUrl.get('view') == 'Edit') {
        zones = $('[data-block="Kainyno zonos"] td [data-fieldtype="string"]');
      } else {
        zones = $('[data-block="Kainyno zonos"] td .fa-pencil');
      }

      zones.on('click', function (e) {
        let column = $(e.target).data('column');
        let columnType = $(e.target).data('typeColumn');
        let accountid = (clientUrl.get('view') == 'Edit' ? $('[name="record"]').val() : $('[name="record_id"]').val());

        setTimeout(function () {
          let input;
          let whoZone = function () {
            let ats;
            $.ajax({
              'async': false,
              'type': "POST",
              'global': false,
              'url': "modules/Newpricebooks/ajax/getType.php",
              'data': { accountid: accountid, column: columnType },
              'success': function (data) {
                ats = data;
              }
            });
            return ats;
          }();

          document.querySelector(`[name="${column}"]`).setAttribute('readonly', 'readonly');
          input = document.querySelector(`[name="${column}"]`).value;

          let start = true;
          let list = input.split(":");
          list = list.slice(1, list.length - 1);
          let inputs = 0;
          if (input != '') {
            appendLocations(whoZone, list);
            inputs = list.length;
          }

          $.ajax({
            type: "POST",
            url: "price-algorithm/popup.php",
            data: { whoZone: whoZone, inputs: inputs, start: start, col: column },
            dataType: "json",
            beforeSend: function () {
              $('#wait').show();
            },
            success: function (result) {
              if (result == null) {
                $('#wait').hide();
                alert('Pirma pasirinkite kainos tipą');
                return;
              } else {
                $('.modal-table').html(result.res);
                $("#selectCitiesModal").modal('show');
                addField(whoZone);
                save(e);
                $('#wait').hide();
              }
            }
          });
        }, 100);
      });
      $("#popup").load('/price-algorithm/popup.php');
    }
  }
}

function addField(whoZone) {
  $('[data-add-field="modal"]').unbind().click(function (e) {
    $.ajax({
      type: "POST",
      url: "price-algorithm/popup.php",
      data: { whoZone: whoZone, inputs: 1 },
      dataType: "json",
      success: function (result) {
        if (result != '') {
          $('.modal-table').append(result.res);
        }
      }
    });
    e.preventDefault();
  });
}



function save(e) {
  $('[data-save="modal"]').unbind().click(function () {
    $("#wait").css("display", "block");
    let selected = [];
    const rows = document.querySelectorAll(".modal-table tr");

    for (let g = 0; g < rows.length; g++) {
      if ($(rows[g].querySelector('.type')).hasClass('selectas')) {
        let nodeList = rows[g].querySelectorAll('select:not(.except)');
        let group = '';
        $(nodeList).each(function () {
          group += $(this).val() + ' ';
        });

        group = group.slice(0, -1);
        selected.push(group);
      } else if ($(rows[g].querySelector('.type')).hasClass('inputas')) {
        selected.push(rows[g].querySelector('input[type="number"]').value);
      }
    }

    let values = selected.filter(function (zone) {
      return zone != "Pasirinkite";
    });

    const selectedType = $(".modal-table .except option:selected");
    let valuesType = Array.from(selectedType).map(el => el.innerHTML);
    valuesType = valuesType.filter(function (type) {
      return type != "Pasirinkite";
    });


    let list = values.join(",");
    let parts = list.split(',');
    let locationList = ':';

    for (var i = 0; i < parts.length; i++) {
      if (valuesType[i] != 'Atstumas') {
        locationList += `${parts[i]}`;
        locationList += ':';
      } else {
        locationList += `${parts[i]}`;
        locationList += ':';
      }
    }

    insertValue(e, locationList);
    $("#selectCitiesModal").modal('hide');
    $("div.input-save-wrap .inlineAjaxSave").click();
    saveToDB2(e);
  });
  $('[data-dismiss="modal"]').click(function () { $("div.input-save-wrap .inlineAjaxCancel").click(); });
}

function saveToDB2(e) {
  const selectedType = $(".modal-table .except option:selected");
  const inputs = $(".modal-table .post");
  const inputAdr = $(".modal-table .adr");
  const inputAdrCode = $(".modal-table .adr_code");
  let line = '';
  let adr = '';
  let adrCode = '';

  let valuesType = Array.from(selectedType).map(el => el.innerHTML);
  valuesType = valuesType.filter(function (type) {
    return type != "Pasirinkite";
  });

  let listType = valuesType.join(",");

  for (let i = 0; inputs.length > i; i++) {
    inp = inputs[i].value;
    numb = inputs[i].dataset.num;
    if (i % 2 == 0) {
      line += (i > 1 ? '/' : '') + numb + "=" + inp;
    } else {
      line += ";" + inp;
    }
  }

  for (let i = 0; inputAdr.length > i; i++) {
    inp = inputAdr[i].value;
    numb = inputAdr[i].dataset.num;
    if (i % 2 == 0) {
      adr += (i > 1 ? '/' : '') + numb + "=" + inp;
    } else {
      adr += ";" + inp;
    }
  }

  for (let i = 0; inputAdrCode.length > i; i++) {
    inp = inputAdrCode[i].value;
    numb = inputAdrCode[i].dataset.num;
    if (i % 2 == 0) {
      adrCode += (i > 1 ? '/' : '') + numb + "=" + inp;
    } else {
      adrCode += ";" + inp;
    }
  }




  let column = $('[data-col]').data('col');
  let columnType = $(e.target).data('typeColumn');
  let clientUrl = new URLSearchParams(window.location.search);
  let accountid = clientUrl.get('record');
  let list = '';

  $.ajax({
    type: "POST",
    url: "pricebookAjax/saveZones.php",
    data: { column: column, list: list, listType: listType, columnType: columnType, accountid: accountid, line: line, adr: adr, adrCode: adrCode },
    dataType: "JSON",
    success: function (result) {
      // console.log(result);
    }
  });
}


function insertValue(e, locationList) {
  let column = $(e.target).data('column');
  $(`[name="${column}"]`).val(locationList);
  setTimeout(function () {
    $("#wait").css("display", "none");
  }, 1000);

}

function appendLocations(whoZones, list) {

  const except = new Array('Kazlų Rūda', 'Kazlų Rūdos r.', 'Kudirkos Naumiestis', 'Naujoji Akmenė', 'Baltoji Vokė', 'Vilniaus apskr.', 'Kauno apskr.', 'Alytaus apskr.', 'Utenos apskr.', 'Panevėžio apskr.', 'Marijampolės apskr.',
    'Tauragės apskr.', 'Šiaulių apskr.', 'Telšių apskr.', 'Klaipėdos apskr.');
  const latvia = '(LV)';

  setTimeout(function () {
    let whoZone = whoZones.split(",");
    for (let i = 0; i < list.length; i++) {
      if (whoZone.length < list.length) whoZone.push(whoZone[0]);
      if (whoZone[i] == 'Atstumas') {
        $(`#number_inp${i}`).val(list[i]);
      } else {
        let check = list[i].split(' ').slice(0, 2).join(' ');
        let checkr = list[i].split(' ').slice(0, 3).join(' ');


        if (whoZone[i] == "Geozona - Miestas" || whoZone[i] == "Rajonas - Miestas" || whoZone[i] == "Rajonas - Rajonas") {
          if (except.includes(checkr)) {
            listFirst = list[i].split(' ').slice(0, 3).join(' ');
          } else {
            listFirst = list[i].split(' ').slice(0, 2).join(' ');
          }
        } else {
          if (except.includes(check)) {
            listFirst = list[i].split(' ').slice(0, 2).join(' ');
          } else if (checkr.includes(latvia)) {
            let t = checkr.split(" ");
            t = t.indexOf(latvia) + 1;
            if (t == 2) {
              listFirst = list[i].split(' ').slice(0, 2).join(' ');
            } else {
              listFirst = list[i].split(' ').shift();
            }
          } else {
            listFirst = list[i].split(' ').shift();
          }
        }

        let check2 = listSecond = list[i].split(' ').slice(-2).join(' ');
        let checkr2 = listSecond = list[i].split(' ').slice(-3).join(' ');

        if (whoZone[i] == "Apskritis - Rajonas" || whoZone[i] == "LTU - Rajonas" || whoZone[i] == "Miestas - Geozona" || whoZone[i] == "Miestas - Rajonas" || whoZone[i] == "Rajonas - Rajonas") {
          if (except.includes(checkr2)) {
            listSecond = list[i].split(' ').slice(-3).join(' ');
          } else {
            listSecond = list[i].split(' ').slice(-2).join(' ');
          }
        } else {
          if (except.includes(check2)) {
            listSecond = list[i].split(' ').slice(-2).join(' ');
          } else if (checkr.includes(latvia)) {
            let t = checkr.split(" ");
            t = t.indexOf(latvia) + 1;
            if (t == 2) {
              listSecond = list[i].split(' ').slice(-1).join(' ');
            } else {
              listSecond = list[i].split(' ').slice(1).join(' ');
            }
          } else {
            listSecond = list[i].split(' ').pop();
          }
        }

        let fieldFirst = document.querySelectorAll(".modal-table tr")[i].querySelectorAll('.selectas')[0];
        let fieldSecond = document.querySelectorAll(".modal-table tr")[i].querySelectorAll('.selectas')[1];


        const option = document.createElement('option');
        option.setAttribute('selected', 'selected');
        option.innerHTML = listFirst;
        fieldFirst.append(option);


        const option2 = document.createElement('option');
        option2.setAttribute('selected', 'selected');
        option2.innerHTML = listSecond;
        fieldSecond.append(option2);

      }

    }
    checkBothSide();
    postCodeInputs();
  }, 2000);


}

if (clientUrl.get('module') == 'Accounts') {

  function changeFieldType(event) {
    let inputs = 1;
    let col = $('.modal-body').data('zoneColumn');
    let parent = event.target.parentElement.parentElement;
    let allTd = parent.querySelectorAll('td');
    let whoZone = event.target.value;
    let theParent = $(event.target).parent().parent();
    let ids = $('.modal-table tr').length;

    $.ajax({
      type: "POST",
      url: "/price-algorithm/popup.php",
      data: { whoZone: whoZone, inputs: inputs, col: col, ids: ids, change: 1 },
      dataType: "JSON",
      success: function (result) {
        if (result != '') {
          for (let n = 0; n < allTd.length; n++) {
            allTd[n].remove();
          }
          theParent.append(result.res);
        }
      }
    });

  }

}


function deleteRow(e) {
  if (confirm("Ar tikrai norite ištrinti eilutę?") == true) {
    e.target.parentElement.parentElement.parentElement.remove();

    const dataAppend = document.querySelectorAll('[data-append]').length;
    for (let i = 0; dataAppend > i; i++) {
      document.querySelectorAll('[data-append]')[i].children[0].children[0].setAttribute('onclick', `bothSidesCheckBox(event,${i + 1});`);

      if (document.querySelectorAll('[data-append]')[i].children[1].children[1] != undefined) {
        document.querySelectorAll('[data-append]')[i].children[1].children[1].dataset.num = i + 1;
        document.querySelectorAll('[data-append]')[i].children[2].children[1].dataset.num = i + 1;
      }

    }

  }
}




if (clientUrl.get('module') == 'Accounts' && (clientUrl.get('view') == 'Detail' || clientUrl.get('view') == 'Edit')) {

  function postCodeInputs() {
    let clientUrl = new URLSearchParams(window.location.search);
    let accountid = clientUrl.get('record');
    let column = "cf_" + $('[data-col]').data('col');

    var responce = new Promise(function (resolve, reject) {
      let type = 'post';
      $.ajax({
        type: "POST",
        url: "price-algorithm/directions.php",
        data: { accountid: accountid, column: column, type: type },
        success: function (result) {
          if (result != '') {
            let splitSlash = result.split("/");
            let rowArr = new Array();
            let codes = new Array();
            let code = new Array();
            let num = new Array();

            for (let i = 0; splitSlash.length > i; i++) {
              rowArr.push(splitSlash[i].split("="));
            }

            for (let e = 0; rowArr.length > e; e++) {
              let nr = rowArr[e][0] - 1;
              document.querySelectorAll('[data-col]')[nr].setAttribute('checked', 'checked');
              codes.push(rowArr[e][1].split(";"));
              rowlen = rowArr.length;
              if (rowlen == 1) rowlen = rowlen + 1;
              for (let f = 0; rowlen > f; f++) {
                if (codes[e][f] != undefined) {
                  code.push(codes[e][f]);
                  num.push(rowArr[e]);
                }
              }
            }
            createInputs(num, code);
          }
        },
        error: function (err) {
          reject(err);
        }
      });
    });

    var responce2 = new Promise(function (resolve, reject) {
      let type = 'adr';
      $.ajax({
        type: "POST",
        url: "/price-algorithm/directions.php",
        dataType: "JSON",
        data: { accountid: accountid, column: column, type: type },
        success: function (result) {
          if (result[0] != '' && result[0] != null) {
            let splitSlash = result[1].split("/");
            let rowArr = new Array();
            let codes = new Array();
            let code = new Array();
            let num = new Array();

            let splitSlash2 = result[0].split("/");
            let rowArr2 = new Array();
            let codes2 = new Array();
            let code2 = new Array();
            let num2 = new Array();

            for (let i = 0; splitSlash.length > i; i++) {
              rowArr.push(splitSlash[i].split("="));
              rowArr2.push(splitSlash2[i].split("="));
            }

            for (let e = 0; rowArr.length > e; e++) {
              let nr = rowArr[e][0] - 1;
              document.querySelectorAll('[data-column]')[nr].setAttribute('checked', 'checked');
              codes.push(rowArr[e][1].split(";"));
              codes2.push(rowArr2[e][1].split(";"));
              rowlen = rowArr.length;
              if (rowlen == 1) rowlen = rowlen + 1;
              for (let f = 0; rowlen > f; f++) {
                if (codes[e][f] != undefined) {
                  code.push(codes[e][f]);
                  num.push(rowArr[e]);
                  code2.push(codes2[e][f]);
                  num2.push(rowArr2[e]);
                }
              }
            }
            createInputs2(num, code, code2);
          }
        },
        error: function (err) {
          reject(err);
        }
      });
    });

  }

  function createInputs(row, codes) {

    for (let id = 0; codes.length > id; id++) {

      ids = row[id][0] - 1;

      if (id % 2 == 0) {
        let input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('class', 'inputElement post');
        input.setAttribute('data-num', `${row[id][0]}`);
        input.setAttribute('style', 'margin-top: 10px;');
        input.setAttribute('id', `post_code${id}`);
        input.setAttribute('onkeyup', `loadCheckPostCode(event);`);
        input.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
        input.value = codes[id];
        let td = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[0];
        td.append(input);
      } else {
        let input2 = document.createElement('input');
        input2.setAttribute('type', 'text');
        input2.setAttribute('class', 'inputElement post ');
        input2.setAttribute('data-num', `${row[id][0]}`);
        input2.setAttribute('style', 'margin-top: 10px;');
        input2.setAttribute('id', `post_code${id}`);
        input2.setAttribute('onkeyup', `loadCheckPostCode(event);`);
        input2.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
        input2.value = codes[id];
        let td2 = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[1];
        td2.append(input2);
      }
    }

  }

  function createInputs2(row, adr, code) {

    for (let id = 0; code.length > id; id++) {

      ids = row[id][0] - 1;

      if (id % 2 == 0) {
        let input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('class', `inputElement adr`);
        input.setAttribute('autocomplete', `off`);
        input.setAttribute('data-num', `${row[id][0]}`);
        input.setAttribute('style', 'margin-top: 10px;');
        input.setAttribute('id', `address${id}`);
        input.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');
        input.value = adr[id];

        let postInput = document.createElement('input');
        postInput.setAttribute('class', 'adr_code');
        postInput.setAttribute('type', 'hidden');
        postInput.setAttribute('data-num', `${row[id][0]}`);
        postInput.setAttribute('id', `address_post${id}`);
        postInput.value = code[id];

        let td = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[0];
        td.append(input);
        td.append(postInput);
        getSuggestionsAddress(id, 'from');
      } else {
        let input2 = document.createElement('input');
        input2.setAttribute('type', 'text');
        input2.setAttribute('class', `inputElement adr`);
        input2.setAttribute('autocomplete', `off`);
        input2.setAttribute('data-num', `${row[id][0]}`);
        input2.setAttribute('style', 'margin-top: 10px;');
        input2.setAttribute('id', `address${id}`);
        input2.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');
        input2.value = adr[id];

        let postInput2 = document.createElement('input');
        postInput2.setAttribute('class', 'adr_code');
        postInput2.setAttribute('type', 'hidden');
        postInput2.setAttribute('data-num', `${row[id][0]}`);
        postInput2.setAttribute('id', `address_post${id}`);
        postInput2.value = code[id];

        let td2 = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[1];
        td2.append(input2);
        td2.append(postInput2);
        getSuggestionsAddress(id, 'to');
        let td3 = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[2];
        let span = document.createElement('span');
        span.setAttribute('class', 'fa fa-plus-circle  cursorPointer add_new_addresses');
        span.setAttribute('title', 'Pridėti adresą');
        span.setAttribute('style', 'font-size: 20px;margin-top: 25px;');
        span.setAttribute('data-toggle', 'modal');
        span.setAttribute('data-target', '#addNewAddress');
        td3.append(span);
      }
    }

  }


  function getSuggestionsAddress(id, which) {
    let clientUrl = new URLSearchParams(window.location.search);
    let accountid = clientUrl.get('record');
    $.ajax({
      type: "POST",
      url: "modules/Newpricebooks/ajax/getAddressHistory.php",
      data: { accountid: accountid, which: which },
      dataType: 'JSON',
      success: function (result) {
        autoCompleteAddress2(document.getElementById(`address${id}`), result, which, id);
      }
    });
  }



  function includeHTML() {
    var elmnt, file, xhttp;
    elmnt = document.querySelector("[w3-include-html]");
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
          setTimeout(() => {
            if (this.status == 200) { elmnt.innerHTML = this.responseText; }
          }, 500);
        }
      }
      elmnt.removeAttribute("w3-include-html");

      xhttp.open("GET", file, true);
      xhttp.send();

      return;
    }
  }




  function checkBothSide() {
    let clientUrl = new URLSearchParams(window.location.search);
    let accountid = clientUrl.get('record');
    let column = $('[data-col]').data('col');
    let type = 'check';

    var responce = new Promise(function (resolve, reject) {
      $.ajax({
        type: "POST",
        url: "price-algorithm/bothSide.php",
        data: { accountid: accountid, column: column, type: type },
        success: function (result) {
          if (result == 1) {
            document.querySelector('.modal-body #bothSides').checked = true;
          } else {
            document.querySelector('.modal-body #bothSides').checked = false;
          }
        },
        error: function (err) {
          reject(err);
        }
      });
    });
  }



  function updateBothSide() {
    let clientUrl = new URLSearchParams(window.location.search);
    let accountid = clientUrl.get('record');
    let column = $('[data-col]').data('col');
    let type = 'update';
    let update = '';

    if ($('#bothSides').is(':checked')) {
      update = 1;
    } else {
      update = 0;
    }

    var responce = new Promise(function (resolve, reject) {
      $.ajax({
        type: "POST",
        url: "price-algorithm/bothSide.php",
        data: { accountid: accountid, column: column, type: type, update: update },
        success: function (result) {
        },
        error: function (err) {
          reject(err);
        }
      });
    });
  }


  function loadCheckPostCode(event) {

    let city = event.target.parentElement.getElementsByTagName('select')[0].value;
    let post = event.target.value;

    var responce = new Promise(function (resolve, reject) {
      $.ajax({
        type: "POST",
        url: "/price-algorithm/checkCities.php",
        data: { city: city, post: post },
        success: function (result) {
          if (!result) {
            $(event.target).addClass('alert alert-danger');
          } else {
            $(event.target).removeClass('alert alert-danger');
          }
        },
        error: function (err) {
          reject(err);
        }
      });
    });
  }


  function directPostCodesCheckBox(event, id) {

    let id2 = parseInt(id) + 1;
    let tds = event.target.parentElement.parentElement.getElementsByTagName('td');
    let ids = event.target.parentElement.parentElement.getElementsByTagName('td')[0].getElementsByTagName('select')[0].dataset.nr
    let ids2 = event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByTagName('select')[0].dataset.nr

    let input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('class', 'inputElement post');
    input.setAttribute('style', 'margin-top: 10px;');
    input.setAttribute('id', `post_code${ids}`);
    input.setAttribute('data-num', `${id + 1}`);
    input.setAttribute('onkeyup', `loadCheckPostCode(event);`);
    input.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
    let td = tds[2];

    let input2 = document.createElement('input');
    input2.setAttribute('type', 'text');
    input2.setAttribute('class', 'inputElement post');
    input2.setAttribute('style', 'margin-top: 10px;');
    input2.setAttribute('id', `post_code${ids2}`);
    input2.setAttribute('data-num', `${id + 1}`);
    input2.setAttribute('onkeyup', `loadCheckPostCode(event);`);
    input2.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
    let td2 = tds[1];

    if ($(event.target).is(':checked')) {
      td.append(input);
      td2.append(input2);
    } else {
      event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByClassName('post')[0].remove();
      event.target.parentElement.parentElement.getElementsByTagName('td')[2].getElementsByClassName('post')[0].remove();
    }

  }


  function directAddressCheckBox(event, id) {

    let tds = event.target.parentElement.parentElement.getElementsByTagName('td');
    let ids = event.target.parentElement.parentElement.getElementsByTagName('td')[0].getElementsByTagName('select')[0].dataset.nr
    let ids2 = event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByTagName('select')[0].dataset.nr

    let input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('class', 'inputElement adr');
    input.setAttribute('autocomplete', `off`);
    input.setAttribute('style', 'margin-top: 10px;');
    input.setAttribute('id', `address${ids}`);
    input.setAttribute('data-num', `${id + 1}`);
    input.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');

    let postInput = document.createElement('input');
    postInput.setAttribute('class', 'adr_code');
    postInput.setAttribute('type', 'hidden');
    postInput.setAttribute('data-num', `${id + 1}`);
    postInput.setAttribute('id', `address_post${ids}`);
    let td = tds[2];

    let input2 = document.createElement('input');
    input2.setAttribute('type', 'text');
    input2.setAttribute('class', 'inputElement adr');
    input2.setAttribute('autocomplete', `off`);
    input2.setAttribute('style', 'margin-top: 10px;');
    input2.setAttribute('id', `address${ids2}`);
    input2.setAttribute('data-num', `${id + 1}`);
    input2.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');

    let postInput2 = document.createElement('input');
    postInput2.setAttribute('class', 'adr_code');
    postInput2.setAttribute('type', 'hidden');
    postInput2.setAttribute('data-num', `${id + 1}`);
    postInput2.setAttribute('id', `address_post${ids2}`);

    let td2 = tds[1];


    if ($(event.target).is(':checked')) {
      td.append(input);
      td.append(postInput);
      getSuggestionsAddress(ids, 'from');

      td2.append(input2);
      td2.append(postInput2);
      getSuggestionsAddress(ids2, 'to');
      let td3 = tds[3];
      let span = document.createElement('span');
      span.setAttribute('class', 'fa fa-plus-circle  cursorPointer add_new_addresses');
      span.setAttribute('data-from', ids);
      span.setAttribute('data-to', ids2);
      span.setAttribute('title', 'Pridėti adresą');
      span.setAttribute('style', 'font-size: 20px;margin-top: 25px;');
      span.setAttribute('data-toggle', 'modal');
      span.setAttribute('data-target', '#addNewAddress');
      td3.append(span);
      setTimeout(() => {
        document.querySelector('.add_new_addresses').addEventListener('click', setInputIdsInModal2);
      }, 100);
    } else {
      event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByClassName('adr')[0].remove();
      event.target.parentElement.parentElement.getElementsByTagName('td')[2].getElementsByClassName('adr')[0].remove();
      event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByClassName('adr_code')[0].remove();
      event.target.parentElement.parentElement.getElementsByTagName('td')[2].getElementsByClassName('adr_code')[0].remove();
      event.target.parentElement.parentElement.getElementsByTagName('td')[3].getElementsByClassName('add_new_addresses')[0].remove();
    }

  }

}

function setInputIdsInModal2(e) {
  let from = e.target.dataset.from;
  let to = e.target.dataset.to;
  setTimeout(() => {
    $('#fromInput').val(to);
    $('#toInput').val(from);
    document.getElementById('submit_new_address').addEventListener('click', saveNewAddress2);
  }, 200);
}


function saveNewAddress2() {

  let fromInputs = document.querySelectorAll('#new_address input');
  let toInputs = document.querySelectorAll('#new_address2 input');
  let fromArray = new Array();
  let toArray = new Array();
  for (let i = 0, len = fromInputs.length; i < len; i++) {
    let name = fromInputs[i].name;
    if (fromInputs[i].value == '') {
      document.getElementById(`req-${name}`).classList.remove('hide');
    } else {
      document.getElementById(`req-${name}`).classList.add('hide');
      fromArray.push(fromInputs[i].value);
    }
  }

  for (let i = 0, len = toInputs.length; i < len; i++) {
    let name = toInputs[i].name;
    if (toInputs[i].value == '') {
      document.getElementById(`req-${name}`).classList.remove('hide');
    } else {
      document.getElementById(`req-${name}`).classList.add('hide');
      toArray.push(toInputs[i].value);
    }
  }

  if (fromArray.length == 4 && toArray.length == 4) {
    saveAddress2(fromArray, 'from');
    saveAddress2(toArray, 'to');
  }

}

function saveAddress2(info, type) {

  let clientUrl = new URLSearchParams(window.location.search);
  let accountid = clientUrl.get('record');
  let company = info[0];
  let city = info[1];
  let address = info[2];
  let code = info[3];

  let from = $('#fromInput').val();
  let to = $('#toInput').val();

  let line = company + " " + address + " " + city + " " + code;

  $.ajax({
    type: "POST",
    url: "modules/Newpricebooks/ajax/saveAddress.php",
    data: { accountid: accountid, type: type, company: company, city: city, address: address, code: code },
    beforeSend: function () {
      $('#wait3').show();
      $('#zones_table').css('opacity', '0.2');
    },
    success: function (result) {
      setTimeout(() => {
        $('#wait3').hide();
        $('#zones_table').css('opacity', '1');
        $('.dismiss').click();
        if (type == 'from') {
          $('#address' + from).val(line);
          $('#address_post' + from).val(code);
          document.getElementById('address' + from).classList.add('selected');
        } else {
          $('#address' + to).val(line);
          $('#address_post' + to).val(code);
          document.getElementById('address' + to).classList.add('selected');
        }
      }, 1000);
    }
  });

}


function autoCompleteAddress2(inp, arr, which, number) {
  /*the autocomplete function takes two arguments,
 the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  let company;
  let postCode;

  let except = document.getElementsByTagName('input');


  if (which == 'from') company = arr.load_company; else if (which == 'to') company = arr.unload_company;
  if (which == 'from') postCode = arr.bill_code; else if (which == 'to') postCode = arr.ship_code;

  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function (e) {

    if (which == 'from') {
      $(`#address${number}`).removeClass('selected');
    } else if (which == 'to') {
      $(`#address${number}`).removeClass('selected');
    }


    var a, b, i, val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items-employee");

    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < company.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (((company[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
        // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        /*create a DIV element for each matching element:*/
        b = document.createElement("DIV");
        b.setAttribute('class', 'listas');
        /*make the matching letters bold:*/
        b.innerHTML = company[i].substr(0, val.length);
        b.innerHTML += company[i].substr(val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + company[i] + "'>";

        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener("click", function (e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.getElementsByTagName("input")[0].value;
          /*close the list of autocompleted values,
          (or any other open lists of autocompleted values:*/
          closeAllLists();
        });

        code = document.createElement("input");
        code.setAttribute('class', 'code');
        code.setAttribute('type', 'hidden');
        code.setAttribute('value', `${postCode[i]}`);

        a.appendChild(b);
        b.appendChild(code);

        let listas = document.querySelectorAll('.listas');
        let codeClass = document.querySelectorAll('.code');


        for (let i = 0; i < listas.length; i++) {
          listas[i].onclick = () => {
            // inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
            inp.classList.add('selected');
            if (which == 'from') {
              $(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
            } else {
              $(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
            }

          }
        }

      }
      inp.onchange = () => {
        if (!inp.classList.contains('selected')) {
          inp.value = ''; // jei nieko nepasirinkama is saraso istustina laukeli
        }
      }
    }
  });


  inp.addEventListener("click", function (e) {


    var a, b, i, val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    // if (!val) { return false;}
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items-employee");

    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < company.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/

      // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
      /*create a DIV element for each matching element:*/
      b = document.createElement("DIV");
      b.setAttribute('class', 'listas');
      /*make the matching letters bold:*/
      b.innerHTML = company[i];
      //  b.innerHTML += company[i];
      /*insert a input field that will hold the current array item's value:*/
      b.innerHTML += "<input type='hidden' value='" + company[i] + "'>";

      /*execute a function when someone clicks on the item value (DIV element):*/
      b.addEventListener("click", function (e) {
        /*insert the value for the autocomplete text field:*/
        inp.value = this.getElementsByTagName("input")[0].value;
        /*close the list of autocompleted values,
        (or any other open lists of autocompleted values:*/
        closeAllLists();
      });

      code = document.createElement("input");
      code.setAttribute('class', 'code');
      code.setAttribute('type', 'hidden');
      code.setAttribute('value', `${postCode[i]}`);

      a.appendChild(b);
      b.appendChild(code);

      let listas = document.querySelectorAll('.listas');
      let codeClass = document.querySelectorAll('.code');


      for (let i = 0; i < listas.length; i++) {
        listas[i].onclick = () => {
          // inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
          inp.classList.add('selected');
          if (which == 'from') {
            $(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
          } else {
            $(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
          }

        }
      }


      inp.onchange = () => {
        if (!inp.classList.contains('selected')) {
          inp.value = ''; // jei nieko nepasirinkama is saraso istustina laukeli
        }
      }
    }
  });



  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function (e) {

    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
      /*If the arrow DOWN key is pressed,
      increase the currentFocus variable:*/
      currentFocus++;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 38) { //up
      /*If the arrow UP key is pressed,
      decrease the currentFocus variable:*/
      currentFocus--;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });



  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items-employee");

    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);

  }, false);

  for (var i = 0; i < except.length; i++) {
    except[i].addEventListener("click", function (ev) {
      ev.stopPropagation();
    }, false);
  }
}

$("[data-link-key=LBL_RECORD_DETAILS]").click(function () {
  setTimeout(function () {
    if (document.querySelector('[data-block="Kainyno zonos"]') != null) {
      if (document.querySelector('[data-block="Kainyno zonos"] h4').innerText = 'Kainyno zonos') {
        locationSelector();
        includeHTML();
      }
    }
  }, 1200);
});


