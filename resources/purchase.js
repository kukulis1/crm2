let address = new URLSearchParams(window.location.search);


function filterPurchaseInvoices() {
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let parameters = '';
  let viewname = document.querySelector('[name="cvid"]').value;

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      parameters += `${form_key}=${form_value}:`;
    }
  }

  parameters = parameters.slice(0, -1);
  const exportBtn = document.querySelector('#export_purchase');
  const exportDetailBtn = document.querySelector('#export_detail_purchase');
  if (exportBtn != null && exportDetailBtn != null) {
    exportBtn.setAttribute('onclick', `window.location.href = 'vtlib/Vtiger/exel/purchase_order_invoices.php?viewname=${viewname}&search_parameters=${parameters}'`);
    exportDetailBtn.setAttribute('onclick', `window.location.href = 'vtlib/Vtiger/exel/purchase_order_detail_invoices.php?viewname=${viewname}&search_parameters=${parameters}'`);
  }
}

function callTemplatesPopUp(vendorid){
  let html = '';   
  $('#popup_body').html('');
  $.ajax({
    type: "POST",
    url: "modules/Vendors/ajax/getTemplates.php",
    data: {vendorid:vendorid},
    dataType: "JSON",
    success: function (response) {
      if(response.status){
        if(confirm('Ar norite pasirinkti šabloną?')){
          response.templates.forEach((item)=>{
            html += `<div class="card card-hover" data-vendorid="${item.vendorid}" data-record="${item.record}">
                      <div class="card-body">
                        <h5>${item.template_name}</h5>`;
                        // html += `<ul id="menu">`;
                        // html += `<li><b>Paslauga</b></li>`;
                        // html += `<li><b>Kaštų centras</b></li>`;
                        // html += `<li><b>Suma</b></li>`;
                        // html += `<li><b>Regionas</b></li>`;
                        // html += `</ul>`;
                          item.loads.forEach((row)=>{                   
                            html += `<ul id="menu">`;
                            html += `<li>${row.productName}</li>`;
                            html += `<li>${row.costname}</li>`;
                            html += `<li>${row.listprice} €</li>`;
                            html += `<li>${row.region_name}</li>`;
                            html += ` </ul>`;
                          });                                                 
                          html += ` </div>
                    </div>`;
          });
          $('#popup_body').append(html);
          $('#purchaseTemplates').modal('show');
          initCardClickEvent();
        }
      }else{
        // console.log('Fail');
      }
    }
  });
}

function initCardClickEvent(){
  $('.card-hover').on('click', function(){
    let record = $(this).data('record');
    let vendorid = $(this).data('vendorid');
    $.ajax({
      type: "POST",
      url: "modules/Vendors/ajax/getTemplate.php",
      data: {record:record,vendorid:vendorid},
      dataType: "JSON",
      success: function (res) {  
        if($('#lineItemTab .lineItemRow').length > 1){
          for(let e = 2; e <= $('#lineItemTab .lineItemRow').length; e++){
            $(`#row${e}`).remove();
          }
        }
        res.template.forEach((item)=>{
          document.querySelector('[data-fieldname="cf_1343"]').setAttribute('data-selected-value', `${item.invoice_type}`);
          document.querySelector('#select2-chosen-2').innerText = `${item.invoice_type}`;
          $('#PurchaseOrder_editView_fieldName_cf_1345').val(item.invoice_date);
          $('#PurchaseOrder_editView_fieldName_description').text(item.description);
          whenChangeInvoiceDateChangeDueDate();
          let e = 1;
          item.loads.forEach((row)=>{           
            if(e > 1)  $('#addService').click();   
            e++; 
          });
          setTimeout(() => {
            let i = 1;
            item.loads.forEach((row)=>{             
                let total = parseFloat(row.qty) * parseFloat(row.listprice);
                $(`#productName${i}`).val(row.productName);
                $(`#comment${i}`).val(row.comment);
                $(`#costcenter${i}`).val(row.costcenter);
                $(`#employee${i}`).val(row.employee);
                $(`#object${i}`).val(row.object);
                $(`#qty${i}`).val(row.qty);
                $(`#fix-price${i}`).val(parseFloat(row.listprice).toFixed(2));
                $(`#productTotal${i}`).html(total.toFixed(2));
                $(`#region${i}`).val(row.region);                      
              i++;
            });
          }, 200);
        }); 
        setTimeout(() => {
          setPricesOnload2();
        },200);
        $('#purchaseTemplates').modal('hide');
      }
    });
  });
}

$('#PurchaseOrder_editView_fieldName_cf_1355').on('input', function(){
  let thisInstance = this;
  let number = $(thisInstance).val();
  let vendorid = $('[name="vendor_id"]').val();
  let alert = '<p id="no_exist" style="color:red;">Saskaita su tokiu numeriu jau yra</p>';
  
  if(vendorid != ''){
    $.ajax({
      type: "POST",
      url: "modules/PurchaseOrder/ajax/checkInvoiceNumber.php",
      data: {vendorid:vendorid,number:number},
      success: function (response) {       
        if(response == 'true'){
          $(thisInstance).addClass('alert alert-danger');       
          $(thisInstance).parent().append(alert); 
        }else{
          $(thisInstance).removeClass('alert alert-danger');
          $(thisInstance).parent().find('#no_exist').remove();    
        }
      }
    });
  }
});


function initInvoiceNumberChecker(vendorid){
    let number = $('#PurchaseOrder_editView_fieldName_cf_1355').val();
    let alert = '<p id="no_exist" style="color:red;">Saskaita su tokiu numeriu jau yra</p>';
   
    $.ajax({
      type: "POST",
      url: "modules/PurchaseOrder/ajax/checkInvoiceNumber.php",
      data: {vendorid:vendorid,number:number},
      success: function (response) {       
        if(response == 'true'){
          $('#PurchaseOrder_editView_fieldName_cf_1355').addClass('alert alert-danger');       
          $('#PurchaseOrder_editView_fieldName_cf_1355').parent().append(alert); 
        }else{
          $('#PurchaseOrder_editView_fieldName_cf_1355').removeClass('alert alert-danger');
          $('#PurchaseOrder_editView_fieldName_cf_1355').parent().find('#no_exist').remove();    
        }
      }
    });
  
}



if (address.get('module') == 'PurchaseOrder' && address.get('view') == 'Edit') {
  $('#vendor_id_display').on('change', function(){
    setTimeout(() => {
      let vendorid = $(this).parent().find('[name="vendor_id"]').val();
      callTemplatesPopUp(vendorid);
      initInvoiceNumberChecker(vendorid);
    }, 500);
  });
}


if (address.get('module') == 'PurchaseOrder' && address.get('view') == 'List') {
  $(document).ready(function () {
    if (document.querySelector('#export_detail_purchase') != null) {
      filterPurchaseInvoices();
    }
  });


  $(document).ajaxComplete(function () {
    if (document.querySelector('#export_detail_purchase') != null) {
      filterPurchaseInvoices();
    }
  });
}

if (address.get('module') == 'PurchaseOrder' && address.get('view') == 'Detail') {

  $('#PurchaseOrder_detailView_fieldLabel_cf_1612').remove();
  $('#PurchaseOrder_detailView_fieldValue_cf_1612').remove();

  $('#PurchaseOrder_detailView_fieldLabel_cf_1616').remove();
  $('#PurchaseOrder_detailView_fieldValue_cf_1616').remove();

  $(document).ajaxComplete(function () {
    $('#PurchaseOrder_detailView_fieldLabel_cf_1612').remove();
    $('#PurchaseOrder_detailView_fieldValue_cf_1612').remove();

    $('#PurchaseOrder_detailView_fieldLabel_cf_1616').remove();
    $('#PurchaseOrder_detailView_fieldValue_cf_1616').remove();
  });
}


if (address.get('module') == 'PurchaseOrder' && address.get('view') != 'Detail' && address.get('view') != 'List') {


  $(document).ready(function () {

    let purchaseType = $('[data-fieldname="cf_1343"]').val();
    if (purchaseType != 'Užsakymas') {
      $('#PurchaseOrder_editView_fieldName_cf_1345').attr('required', 'true');
      $('#PurchaseOrder_editView_fieldName_duedate').attr('required', 'true');
      $('#PurchaseOrder_editView_fieldName_cf_1355').attr('required', 'true');
    }

    $('[data-fieldname="cf_1343"]').on('change', function () {
      if (this.value == 'Užsakymas') {
        $('#PurchaseOrder_editView_fieldName_cf_1345').removeAttr('required');
        $('#PurchaseOrder_editView_fieldName_duedate').removeAttr('required');
        $('#PurchaseOrder_editView_fieldName_cf_1355').removeAttr('required');
      } else {
        $('#PurchaseOrder_editView_fieldName_cf_1345').attr('required', 'true');
        $('#PurchaseOrder_editView_fieldName_duedate').attr('required', 'true');
        $('#PurchaseOrder_editView_fieldName_cf_1355').attr('required', 'true');
      }
    });

    setTimeout(() => {
      $('#lineItemTab').sortable('destroy');
      localStorage.removeItem('netTotal');
      localStorage.removeItem('chargesTotal');

      let total = parseFloat($('#netTotal').text());
      localStorage.setItem("netTotal", total); 

      let total_vat = parseFloat($('#chargesTotal').val());  
      localStorage.setItem("chargesTotal", total_vat); 

    }, 1000);

    if (document.querySelector(`#fix-price1`).value == '') {
      document.querySelector(`#fix-price1`).value = 0;
    }

    loadPriceByQty();
    changePriceByQty();
    if (address.get('record')) {
      loadDiscount();
    } else {
      setPricesOnload2();
    }

    $(function () {
      $("#purchase_date1").datepicker({
        dateFormat: "yy-mm-dd",
        autoclose: true
      });
    });

    $('.add_new_entity').click(function (e) {
      let thestring = e.target.id;
      let id = thestring.replace(/^\D+/g, '');
      addNewItemService(id);
    });
    if (document.getElementById('addService')) {
      document.getElementById('addService').onclick = () => {
        setTimeout(() => {
          let rows = document.querySelectorAll('#lineItemTab .lineItemRow').length;
          document.querySelector(`#fix-price${rows}`).value = 0;
          document.getElementById(`qty${rows}`).dataset.num = rows;
          loadDatePicker(rows);
          changePriceByQty();
          document.querySelector('#totalProductCount').value = document.querySelectorAll('.lineItemRow').length;
        }, 1)
        loadPurchaseAjaxLists();
        loadNewPurchages();
      }

    }
    // document.querySelector('#vendor_id_display').onchange = () => {
    //   setTimeout(() => {
    //     let user = document.querySelector('[name="vendor_id"]').value;
    //     getDeferralDate(user);
    //     getVendorBankIBAN(user);
    //   }, 1);
    // }


  });


  function trigerInput(event){
    setTimeout(() => {
      let user = $(event).parent().find('.sourceField').val();
      getDeferralDate(user);
      getVendorBankIBAN(user);
    }, 1);
  }


  function loadNewPurchages() {
    setTimeout(() => {
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow');
      for (i = 1; i <= rows.length; i++) {
        document.querySelector(`#fix-price${i}`).onkeyup = (e) => {
          changePrice(e);
        }
        document.querySelector(`#fix-price${i}`).onchange = () => {
          setPricesOnload2();
        }
      }
    }, 100);

  }

  function changePriceByQty() {
    setTimeout(() => {
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow');
      for (i = 1; i <= rows.length; i++) {
        document.querySelector(`#qty${i}`).onkeyup = (e) => {
          let num = e.target.dataset.num;
          let count = e.target.value;
          let price = document.getElementById(`fix-price${num}`).value;
          // let discountType = document.getElementById(`discount_type${num}`).value;
          // let discountPercent = document.getElementById(`discount_percentage${num}`).value;
          // let discountTotalPrice = document.querySelector(`#row${num}`).getElementsByTagName('td')[5].getElementsByTagName('span')[1].innerText;
          // let discountPrice = parseFloat(discountTotalPrice.slice(1, -1));
          // let discountTotal = document.getElementById(`discountTotal${num}`);
          // let totalAfterDiscount = document.getElementById(`totalAfterDiscount${num}`);
          // let discount;

          if (count != '' || count > 0) {
            let newPrice = price * count;
            // let newPrice2;

            // if (address.get('record') != null) {
            //   if (discountType == 'percentage') {
            //     discount = newPrice * discountPercent / 100;
            //     newPrice2 = newPrice - (newPrice * discountPercent / 100);
            //   } else if (discountType == 'amount') {
            //     discount = newPrice - (newPrice - discountPrice);
            //     newPrice2 = newPrice - discountPrice;
            //   } else {
            //     discount = 0;
            //     newPrice2 = 0;
            //   }
            // } else {
            //   if (discountType == 'percentage') {
            //     discount = newPrice * discountPercent / 100;
            //     newPrice2 = newPrice - (newPrice * discountPercent / 100);
            //   } else if (discountType == 'amount') {
            //     discount = newPrice - (newPrice - discountPrice);
            //     newPrice2 = newPrice - discountPrice;
            //   } else {
            //     discount = 0;
            //     newPrice2 = 0;
            //   }
            // }


            // discountTotal.innerHTML = discount;
            // totalAfterDiscount.innerHTML = newPrice2;
            // document.getElementById(`netPrice${num}`).innerHTML = parseFloat(newPrice).toFixed(2);   
            document.getElementById(`productTotal${num}`).innerHTML = parseFloat(newPrice).toFixed(2);
            setPricesOnload2();
          }

        }
      }
    }, 100);
  }

  function loadPriceByQty() {
    let rows = document.querySelectorAll('#lineItemTab .lineItemRow');
    for (i = 1; i <= rows.length; i++) {
      let count = document.getElementById(`qty${i}`).value;
      let price = document.getElementById(`fix-price${i}`).value;
      if (count != '' || count > 0) {
        let newPrice = price * count;
        document.getElementById(`productTotal${i}`).innerHTML = parseFloat(newPrice).toFixed(2);
        document.getElementById(`netPrice${i}`).innerHTML = parseFloat(newPrice).toFixed(2);
      }
    }
  }



  function setPricesOnload2() {
    if($('#adjustment').val() <= 0 && $('#adjustment_pvm').val() <= 0){
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow');
      let grand = document.querySelector('#grandTotal');
      let chargesTotalDisplay = document.querySelector('#chargesTotalDisplay');
      let chargesTotal = document.querySelector('#chargesTotal');
      let netTotal = document.querySelector('#netTotal');

      let pricesArr = new Array();
      let netTotalArr = new Array();
      let pvmArr = new Array();
      for (let p = 1, len = rows.length; p <= len; p++) {

        let productTotal = document.querySelectorAll(`#productTotal${p}`)[0].innerText;
        let productTotal_2 = document.querySelectorAll(`#productTotal${p}`)[1].innerText;
        let netPrice = document.querySelector(`#netPrice${p}`).innerText;
        let fixprice = document.querySelector(`#fix-price${p}`).value;
        let vatprice = document.querySelector(`#vatprice${p}`);
        let vatpriceInp = document.querySelector(`[name='vatpriceInp${p}']`);
        let count = document.getElementById(`qty${p}`).value;

        fixprice = fixprice * count;
        let vat = addVAT(p);
        vatprice.innerText = vat;
        vatpriceInp.value = vat;
        netPrice = parseFloat(productTotal) + parseFloat(vat);

        if (address.get('record') != null) {
          document.querySelector(`#netPrice${p}`).innerText = round(parseFloat(netPrice), 2);
          document.getElementById(`productTotal${p}`).innerHTML = round(parseFloat(productTotal), 2);
          productTotal2 = netPrice;
        } else {
          document.querySelector(`#netPrice${p}`).innerText = round(parseFloat(netPrice), 2);
          document.getElementById(`productTotal${p}`).innerHTML = round(parseFloat(productTotal), 2);
          productTotal2 = netPrice;
        }

        pricesArr.push(productTotal2);
        pvmArr.push(vat);
        netTotalArr.push(productTotal);

      }


      pricesArr = pricesArr.map((x) => {
        return parseFloat(x);
      });

      pvmArr = pvmArr.map((x) => {
        return parseFloat(x);
      });

      netTotalArr = netTotalArr.map((x) => {
        return parseFloat(x);
      });


      setTimeout(() => {
        chargesTotalDisplay.innerText = round(pvmArr.reduce((a, b) => a + b, 0), 2);
        chargesTotal.value = round(pvmArr.reduce((a, b) => a + b, 0), 2);
        grand.innerText = round(pricesArr.reduce((a, b) => a + b, 0), 2);
        netTotal.innerText = round(netTotalArr.reduce((a, b) => a + b, 0), 2);
      }, 1000);
    }
  }

  if (address.get('module') == 'PurchaseOrder') {

    function loadDiscount(type) {
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow');
      for (let id = 1, len = rows.length; id <= len; id++) {
        discount_type = $(`#discount_type${id}`).val();
        let productTotal = document.querySelectorAll(`#productTotal${id}`)[0];
        let productTotal2 = document.querySelectorAll(`#productTotal${id}`)[1];
        let newPrice;

        if (discount_type == 'percentage') {
          let inputValue = document.querySelector(`#discount_percentage${id}`);
          let percent = inputValue.value;
          let discount = productTotal2.innerHTML / 100 * percent;
          newPrice = productTotal2.innerHTML - discount;
        } else if (discount_type == 'amount') {
          let inputValue = document.querySelector(`#discount_amount${id}`);
          let amount = inputValue.value;
          newPrice = productTotal2.innerHTML - amount;
        } else {
          newPrice = productTotal2.innerHTML;
        }

        productTotal.innerHTML = newPrice;
      }

      setTimeout(() => {
        setPricesOnload2();
      }, 100);

    }

    function getDiscount(e) {

      setTimeout(() => {
        let element = e.target.parentElement.parentElement.parentElement.parentElement.id
        let inputValue = e.target.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input[type=text]");

        let father = e.target.parentElement.parentElement.parentElement.parentElement.querySelector(".discountUI").id;
        let id = father.replace(/\D/g, '');
        discount_type = $(`#discount_type${id}`).val();

        let productTotal = document.querySelectorAll(`#productTotal${id}`)[0];
        let productTotal2 = document.querySelectorAll(`#productTotal${id}`)[1];
        let newPrice;

        if (discount_type == 'percentage') {
          let percent = inputValue[0].value;
          let discount = productTotal2.innerHTML / 100 * percent;
          newPrice = productTotal2.innerHTML - discount;
        } else if (discount_type == 'amount') {
          let amount = inputValue[1].value;
          newPrice = productTotal2.innerHTML - amount;
        } else {
          newPrice = productTotal2.innerHTML;
        }

        productTotal.innerHTML = newPrice;

        $(`#${element}`).popover('destroy');
      }, 100);
      setTimeout(() => {
        setPricesOnload2()
      }, 200);
    }

  }

  $('.region_id').on('change', function () {
    setTimeout(() => {
      setPricesOnload2();
    }, 600);

  });

  function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
  }



  function addVAT(id) {
    let vatPercent;
    let data = $(`#region${id} :selected`).data('info');

    if (data.charges !== undefined) {
      data = document.querySelectorAll(`#region${id} option`)[0].getAttribute('data-info');
      let parseJson = JSON.parse(data);
      vatPercent = parseJson.charges[0].percent;
    } else {
      vatPercent = data.value;
    }

    let productTotal = document.querySelector(`#productTotal${id}`).innerText;
    let taxPrice = parseFloat(productTotal) / 100 * parseFloat(vatPercent);
    return round(taxPrice, 2);

  }


  function changePrice(e) {
    let priceField = e.target.parentElement.parentElement.parentElement.getElementsByClassName('productTotal')[0];
    let priceField2 = e.target.parentElement.parentElement.parentElement.getElementsByClassName('productTotal')[1];
    let qty = e.target.parentElement.parentElement.parentElement.getElementsByClassName('qty')[0];
    let newPrice = e.target.value;
    priceField.innerHTML = (parseFloat(newPrice) * parseFloat(qty.value)).toFixed(2);
    priceField2.innerHTML = (parseFloat(newPrice) * parseFloat(qty.value)).toFixed(2);
  }



  // suggestions

  function autocomplete(inp, arr, place) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false; }
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      if (place == 'history') {
        a.setAttribute("class", "autocomplete-items");
      } else {
        a.setAttribute("class", "autocomplete-items-employee");
      }
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (((arr[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
          // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          b.setAttribute('class', 'listas');
          /*make the matching letters bold:*/
          b.innerHTML = arr[i].substr(0, val.length);
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function (e) {
            /*insert the value for the autocomplete text field:*/
            inp.value = this.getElementsByTagName("input")[0].value;
            /*close the list of autocompleted values,
            (or any other open lists of autocompleted values:*/
            closeAllLists();
          });
          a.appendChild(b);
          let listas = document.querySelectorAll('.listas');
          for (let i = 0; i < listas.length; i++) {
            listas[i].onclick = () => {
              // inp.readOnly = true;
              inp.classList.add('selected');
            }
          }

        }
        inp.onchange = (e) => {
          if (!inp.classList.contains('selected')) {
            if (inp.placeholder != 'Darbuotojas' && inp.placeholder != 'Objektas') {
              inp.value = '';
            }
            $(e.target.parentElement).find('.add_new_entity').addClass('alert-danger');
          } else {
            $(e.target.parentElement).find('.add_new_entity').removeClass('alert-danger');
          }
        }
      }
    });




    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      if (place == 'history') {
        var x = document.getElementsByClassName("autocomplete-items");
      } else {
        var x = document.getElementsByClassName("autocomplete-items-employee");
      }
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
      closeAllLists(e.target);

    });
  }


  function getVendorBankIBAN(id) {
    $.ajax({
      type: "POST",
      url: "purchase/userIban.php",
      data: { id: id },
      success: function (data) {
        $('#PurchaseOrder_editView_fieldName_cf_1610').val(data);
      }
    });
  }


  function getDeferralDate(user) {
    $.ajax({
      type: "POST",
      url: "purchase/deferral_date.php",
      data: { user: user },
      dataType: 'JSON',
      success: function (date) {
        whenChangePurchaseDateChangeDueDate(date);
      }
    });
  }


  function loadPurchaseAjaxLists() {
    $.ajax({
      type: "POST",
      url: "purchase/items.php",
      data: { id: 1 },
      dataType: 'JSON',
      success: function (history) {
        rows = document.querySelectorAll('#lineItemTab .lineItemRow');
        for (i = 1, len = rows.length; i <= len; i++) {
          autocomplete(document.getElementById(`productName${i}`), history, 'history');
        }
      }
    });

    $.ajax({
      type: "POST",
      url: "purchase/users_list.php",
      data: { id: 1 },
      dataType: 'JSON',
      success: function (user_list) {
        rows = document.querySelectorAll('#lineItemTab .lineItemRow');
        for (i = 1, len = rows.length; i <= len; i++) {
          autocomplete(document.getElementById(`employee${i}`), user_list, 'users');
        }
      }
    });

    $.ajax({
      type: "POST",
      url: "purchase/object.php",
      data: { id: 1 },
      dataType: 'JSON',
      success: function (object) {
        rows = document.querySelectorAll('#lineItemTab .lineItemRow');
        for (i = 1, len = rows.length; i <= len; i++) {
          autocomplete(document.getElementById(`object${i}`), object, 'object');
        }
      }
    });
  }


  function whenChangePurchaseDateChangeDueDate(payment_deferral) {
    let date = new Date();
    var day = String(date.getDate()).padStart(2, '0');
    var month = String(date.getMonth() + 1).padStart(2, '0');
    var years = date.getFullYear();

    let today = `${years}-${month}-${day}`;

    let invoiceDate = document.getElementById('PurchaseOrder_editView_fieldName_cf_1345');

    invoiceDate.value = today;

    let paymentDay = String(date.getDate()).padStart(2, '0');
    if (payment_deferral != '') {
      paymentDay = parseFloat(paymentDay) + parseFloat(payment_deferral);
    } else {
      paymentDay = paymentDay;
    }

    // let paymentDeferral = `${years}-${month}-${paymentDay}`;
    let payUntil = document.getElementById('PurchaseOrder_editView_fieldName_duedate');
    // payUntil.value = paymentDeferral;


    const duedate = document.createElement('input');
    duedate.setAttribute('id', 'duedate');
    duedate.setAttribute('type', 'hidden');
    payUntil.parentElement.append(duedate);
    document.getElementById('duedate').value = payment_deferral;
    setTimeout(() => {
      whenChangeInvoiceDateChangeDueDate();
      loadDateDetails();
    }, 100);
  }

  function whenChangeInvoiceDateChangeDueDate() {
    let duedate = document.querySelector('#duedate');
    let invoiceDate = document.querySelector('#PurchaseOrder_editView_fieldName_cf_1345');
    let invoiceDateShows = document.querySelector('#PurchaseOrder_editView_fieldName_duedate');

    let newDate = new Date(invoiceDate.value);
    let day = String(newDate.getDate() + Number(duedate.value)).padStart(2, '0');
    let month = String(newDate.getMonth() + 1).padStart(2, '0');
    let years = newDate.getFullYear();

    let dayOfMonth = daysInMonth(month, years);
    if (dayOfMonth < day) {
      let muchDays = day - dayOfMonth;
      if (muchDays < dayOfMonth) month = Number(month) + 1; day = String(muchDays).padStart(2, '0');
    }
    let newUntilDay = `${years}-${month}-${day}`;
    invoiceDateShows.value = newUntilDay;
  }

  function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  function loadDateDetails() {
    let invoiceDate = document.querySelector('#PurchaseOrder_editView_fieldName_cf_1345');
    invoiceDate.onchange = () => {
      whenChangeInvoiceDateChangeDueDate();
    }
  }

  function loadDatePicker(i) {
    $(function () {
      $(`#purchase_date${i}`).datepicker({
        dateFormat: "yy-mm-dd",
        autoclose: true
      });
    });
  }

  function addNewItemService(id) {

    document.getElementById("submit_new_entity").onclick = function () {
      let url = "purchase/addNewItemsServices.php";
      let title = $("[name='title']").val();
      let code = $("[name='code']").val();
      let type = $("[name='type']").val();

      if (title == '') {
        document.getElementById('req-title').classList.remove('hide');
      } else {
        document.getElementById('req-title').classList.add('hide');
      }

      if (code == '') {
        document.getElementById('req-code').classList.remove('hide');
      } else {
        document.getElementById('req-code').classList.add('hide');
      }


      if (title != '' && code != '') {

        $.ajax({
          type: "POST",
          url: url,
          data: { title: title, code: code, type: type },
          dataType: 'json',
          success: function (data) {
            if (data == 'Success') {
              $(`#productName${id}`).val(title);
              $(`#productName${id}`).attr('readonly', 'readonly');
              $("[name='title']").val('');
              $("[name='code']").val('');
              $('.dismiss').click();
            } else {
              document.getElementById('title-exists').classList.remove('hide');
              setTimeout(function () {
                document.getElementById('title-exists').classList.add('hide');
              }, 2000);
            }
          }
        });
      }


    }
  }


  loadNewPurchages()
  loadPurchaseAjaxLists();
}


if (address.get('module') == 'PurchaseOrder' && address.get('Email')) {
  $(document).ready(function () {
    getEmailInfo();
  });
}else if(address.get('module') == 'PurchaseOrder' && address.get('region') != null){
  let region =  address.get('region');
  let netTotal =  address.get('netTotal');
  $('#region1').val(region);  
  $('#fix-price1').val(netTotal);
  setPricesOnload2();
} 


if (address.get('module') == 'PurchaseOrder' && address.get('purchaseorderid')) {
  $(document).ready(function () {
    $('#fileBox').show();
    $('.search_in_mail').show();
    let searchContent = $('.search_content');    
    $('#search_document').on('keyup', function(){
      let searchKey = $(this).val();
        if(searchKey.length >= 3){
          $.ajax({
            type: "POST",
            url: "modules/Invoiceemails/ajax/search.php",
            data: {searchText:searchKey},
            dataType: "JSON",
            beforeSend: function(){
              searchContent.hide();
              $('.empty-results').hide();
            },
            success: function (response) {
              let html = '';
              if(response.length > 0){                         
                  response.forEach(element => {                  
                    html += '<ul style="list-style:none;">';
                    html += `<li>${element.title}</li>`;
                    html += `<li><ul>`; 
                    let name = element.original_file_name.split('|##|');
                    let url = element.filename.split('|##|'); 
                    let path = (element.new_path != null ? element.new_path.slice(0,-1) : '');
                    for(i =0; i < name.length;i++) {
                      html += `<li data-mailid="${element.email_id}" data-url="${url[i]}" data-filepath="${path}" data-filename="${name[i].split('.').shift()}" data-original_file_name="${name[i]}">${name[i]} <i class="fa fa-plus" onclick="addDocument(event)" style="cursor: pointer;color: green;"></i></li>`;
                    }
                    html += `</ul></li>`;
                    html += '</ul>';
                  });
                  searchContent.show();
                  searchContent.html(html);
              }else{
                $('.empty-results').show();
              }
            }
          });
        }else{
          searchContent.hide();
        }
    })
  });
}


function addDocument(e){
  let count = $('#fileinfo div').length+1;
  $('#fileBox').show();
  let userid = _USERMETA.id;
  let fileinfo = document.getElementById('fileinfo');
  $('[name="filecount"]').val(count);
  $('[name="mailid"]').val($(e.target).parent().data('mailid'));
  $('[name="userid"]').val(userid);

  let  fileurl = document.createElement('div');
  fileurl.setAttribute('id', `fileurl${count}`);

  let filepath = document.createElement('input');
  filepath.setAttribute('type', 'hidden');
  filepath.setAttribute('class', 'filepath');
  filepath.setAttribute('name', `filepath${count}`);
  filepath.setAttribute('value', $(e.target).parent().data('filepath'));

  let filename = document.createElement('input'); 
  filename.setAttribute('type', 'hidden');
  filename.setAttribute('class', 'filename');
  filename.setAttribute('name', `filename${count}`);
  filename.setAttribute('value', $(e.target).parent().data('filename'));

  let filefullname = document.createElement('input');
  filefullname.setAttribute('type', 'hidden');
  filefullname.setAttribute('class', 'filefullname');
  filefullname.setAttribute('name', `filefullname${count}`);
  filefullname.setAttribute('value', $(e.target).parent().data('original_file_name'));

  fileurl.innerHTML = `<a target="_blank" href="${$(e.target).parent().data('url')}">${$(e.target).parent().data('original_file_name')}</a> <i class="fa fa-minus" onclick="removeDocument(event)" style="cursor: pointer;color: red;"></i>`;
  fileurl.append(filepath);
  fileurl.append(filename);
  fileurl.append(filefullname);
  fileinfo.append(fileurl);

  if($(e.target).parent().parent().find('li').length == 1){
    $(e.target).parent().parent().parent().parent().remove();
  }else{
    $(e.target).parent().remove();
  }

  if( $('.search_content ul').length < 1){
    $('.search_content').hide();
  }
}

function removeDocument(e){
  $(e.target).parent().remove();
  let count = $('#fileinfo div').length;
  $('[name="filecount"]').val(count);

  if(count > 0){
    for (i = 0; i < count; i++) {
      $('#fileinfo div').eq(i).attr('id', `fileurl${i+1}`);
      $('#fileinfo div').eq(i).find('.filepath').attr('name', `filepath${i+1}`);
      $('#fileinfo div').eq(i).find('.filename').attr('name', `filename${i+1}`);
      $('#fileinfo div').eq(i).find('.filefullname').attr('name', `filefullname${i+1}`);      
    }
  }  
}


function getEmailInfo() {
  let id = address.get('Email');
  let edited = address.get('edited');
  let userid = _USERMETA.id;
  let fileinfo = document.getElementById('fileinfo');
  $.ajax({
    type: "POST",
    url: "purchase/emailInfo.php",
    data: { id: id, edited: edited},
    dataType: 'JSON',
    success: function (data) {
      if (data.mail.mailid) {
        $('#fileBox').show();
        $('[name="mailid"]').val(data.mail.mailid);
        $('[name="userid"]').val(userid);
        $('[name="filecount"]').val(data.file.filename.length);
        $('#PurchaseOrder_editView_fieldName_description').val(data.mail.body);

        let fileurl = [];
        let filepath = [];
        let filename = [];
        let filefullname = [];

        for (let i = 0; i < data.file.filename.length; i++) {
          fileurl[i] = document.createElement('div');
          fileurl[i].setAttribute('id', `fileurl${i + 1}`);

          filepath[i] = document.createElement('input');
          filepath[i].setAttribute('type', 'hidden');
          filepath[i].setAttribute('name', `filepath${i + 1}`);
          filepath[i].setAttribute('value', data.file.filepath[i]);

          filename[i] = document.createElement('input');
          filename[i].setAttribute('type', 'hidden');
          filename[i].setAttribute('name', `filename${i + 1}`);
          filename[i].setAttribute('value', data.file.filename[i]);

          filefullname[i] = document.createElement('input');
          filefullname[i].setAttribute('type', 'hidden');
          filefullname[i].setAttribute('name', `filefullname${i + 1}`);
          filefullname[i].setAttribute('value', data.file.fullfilename[i]);

          fileurl[i].innerHTML = `<a target="_blank" href="${data.file.url[i]}">${data.file.original_file_name[i]}</a>`;
          fileurl[i].append(filepath[i]);
          fileurl[i].append(filename[i]);
          fileurl[i].append(filefullname[i]);
          fileinfo.append(fileurl[i]);

        }


      }
    }
  });
}


if (address.get('module') == 'PurchaseOrder' && address.get('view') == 'Edit' && address.get('returnmodule') == 'Claims') {
  let claimid = address.get('returnrecord');

  let date = new Date();
  var day = String(date.getDate()).padStart(2, '0');
  var month = String(date.getMonth() + 1).padStart(2, '0');
  var years = date.getFullYear();
  let today = `${years}-${month}-${day}`;

  $.ajax({
    type: "POST",
    url: "purchase/fillClaimClient.php",
    data: { claimid: claimid },
    dataType: 'JSON',
    success: function (data) {
      let vendorname = document.getElementById('vendor_id_display');
      let vendorid = document.querySelector('[name="vendor_id"]');
      let vendorIdDisplay = document.getElementById('vendor_id_display');
      vendorname.value = data.vendorname;
      vendorid.value = data.vendorid;
      vendorIdDisplay.setAttribute('disabled', 'disabled');
      document.querySelector('.clearReferenceSelection').classList.remove('hide');
      getDeferralDate(data.vendorid);

      $('#fix-price1').val(data.amount);
      $('#productTotal1').html(data.amount);
      $('#productName1').val('Nuolaida pagal pretenziją');
      $('#hdnProductId1').val(36641);
      $('#lineItemType1').val('Products');
      $('#purchase_date1').val(today);
      $("#region1").val(1);
      setPricesOnload2();
    }
  });
}

function fixPrice(evt,which){
  let amount = 0;

  if($(evt.target).val() != ''){
   amount = parseFloat($(evt.target).val());
  }

  if(which == 'total'){
    let type = $('input[name=adjustmentType]:checked').val();   
    let netTotal =  parseFloat(localStorage.getItem("netTotal"));   

    if (type == '+') {
			netTotal += parseFloat(amount);
		} else if (type == '-') {
			netTotal -= parseFloat(amount);
		}

    $('#netTotal').text(netTotal.toFixed(2));

  }else if(which == 'vat'){

    let type = $('input[name=adjustmentPvmType]:checked').val(); 
    let new_vat = parseFloat(localStorage.getItem("chargesTotal"));   

      if (type == '+') {
        new_vat += parseFloat(amount);
      } else if (type == '-') {
        new_vat -= parseFloat(amount);
      }

      $('#chargesTotal').val(new_vat.toFixed(2));
      $('#chargesTotalDisplay').text(new_vat.toFixed(2)); 
  }

  let vat = parseFloat($('#chargesTotal').val());
  let netTotal = parseFloat($('#netTotal').text());
  let grandTotal = netTotal+vat;
  $('#grandTotal').text(grandTotal.toFixed(2));
}


function setCorrections(event,which){
  if(which == 'total'){
    $(event.target).parent().parent().parent().parent().find('#adjustment').trigger('paste');
  }else if(which == 'vat'){
    $(event.target).parent().parent().parent().parent().find('#adjustment_pvm').trigger('paste');
  }
}