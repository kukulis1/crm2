let SalesOrderLink = new URLSearchParams(window.location.search);

if (SalesOrderLink.get('module') == 'SalesOrder' && SalesOrderLink.get('view') == 'Edit') {

  $(document).ready(function () {
    getGlobalTaxableMetersValue();
  });

  document.getElementById("account_id_display").addEventListener("keydown", sendSuggestions);
  document.getElementById('account_id_display').addEventListener('mousedown', sendSuggestions);

}


function sendSuggestions(event) {

  document.querySelector('#ui-id-1').addEventListener('mousedown', sendSuggestions);

  if (event instanceof MouseEvent) {
    setTimeout(() => {
      account_id = document.querySelector('.sourceField').value;
      getSuggestions(account_id);
      getTaxableMetersValue(account_id);
      cargoWithOutDimensions(account_id);
      if (account_id != '') {
        removeInfo();
        checkAccountDebt(account_id);
        checkAccountLastShipmentCode(account_id);
      } else {
        $('#info').remove();
      }
    }, 200);
  } else if (event instanceof KeyboardEvent && event.keyCode === 13) {
    setTimeout(() => {
      account_id = document.querySelector('.sourceField').value;
      getSuggestions(account_id);
      getTaxableMetersValue(account_id);
      cargoWithOutDimensions(account_id);
      if (account_id != '') {
        removeInfo();
        checkAccountDebt(account_id);
        checkAccountLastShipmentCode(account_id);
      } else {
        $('#info').remove();
      }
    }, 200);
  }
}

function checkAccountLastShipmentCode(accountid) {
  let last_shipment_code = $('#last_shipment_code');
  let assign_to_last_order = $('#assign_to_last_order');
  let last_shipment_code_text = $('#last_shipment_code_text');

  $.ajax({
    type: "POST",
    url: "salesorders/checkLastShipmentCode.php",
    data: { accountid: accountid },
    dataType: "JSON",
    success: function (response) {
      if (response.success == true) {     
        last_shipment_code.html(response.shipment_code);
        assign_to_last_order.removeClass('hide');
        last_shipment_code_text.removeClass('hide');
      } else {
        last_shipment_code.html('');
        assign_to_last_order.addClass('hide');
        last_shipment_code_text.addClass('hide');       
      }

    }
  });
}

function checkAccountDebt(accountid) {
  let info = document.querySelector('.referencefield-wrapper ').parentElement;
  let p = document.createElement('p');
  p.setAttribute('id', 'info');
  p.setAttribute('style', 'font-weight: 700;');

  $.ajax({
    type: "POST",
    url: "salesorders/checkClientDebt.php",
    data: { accountid: accountid },
    dataType: 'JSON',
    success: function (res) {
      p.innerHTML = `Skola: <span ${(res.debt > 0 ? 'style="color: red;"' : '')}>${res.debt}</span> / Mediana: <span ${(res.median > 0 ? 'style="color: red;"' : '')}>${res.median}</span>`;
      info.append(p);
      if (res.black_list == 1) {
        if (!confirm('Klientas juodajame sąraše ar tikrai norite kurti užsakymą?')) {
          $('.clearReferenceSelection').click();
        }
      }
    }
  });
}


function removeInfo() {
  $('.clearReferenceSelection').on('click', () => {
    $('#info').remove();
  });
}

function getSuggestions(account_id) {
  $.ajax({
    type: "POST",
    url: "salesorders/suggestions.php",
    data: { account_id: account_id },
    dataType: 'JSON',
    success: function (history) {
      autocompleteSa(document.getElementById(`SalesOrder_editView_fieldName_load_company`), history.load_company, 'bill');
      autocompleteSa(document.getElementById(`SalesOrder_editView_fieldName_unload_company`), history.unload_company, 'ship');
    }
  });
}


function getGlobalTaxableMetersValue() {
  $.ajax({
    type: "POST",
    url: "salesorders/globalTaxableMetersValue.php",
    data: { 1: 1 },
    success: function (res) {
      $('#globalMetersValue').val(res);
    }
  });
}

function getTaxableMetersValue(account_id) {
  $.ajax({
    type: "POST",
    url: "salesorders/taxableMetersValue.php",
    data: { account_id: account_id },
    success: function (res) {
      $('#accMetersValue').val(res);
    }
  });
}

function cargoWithOutDimensions(account_id) {
  let block = document.querySelectorAll('.fieldBlockContainer .lineItemRow .productName');

  $.ajax({
    type: "POST",
    url: "salesorders/cargoWithOutDimensions.php",
    data: { account_id: account_id },
    success: function (res) {
      if (res == 0 && res != '') {
        $('.disabled_create_btn').prop("disabled", false);
        for (let i = 0, len = block.length; i < len; i++) {
          block[i].removeAttribute('required');
          block[i].removeAttribute('data-rule-required');
          block[i].removeAttribute('aria-required');
        }
      } else {
        $('.disabled_create_btn').prop("disabled", true);
        for (let i = 0, len = block.length; i < len; i++) {
          block[i].setAttribute('required', true);
          block[i].setAttribute('data-rule-required', true);
          block[i].setAttribute('aria-required', true);
        }
      }
    }
  });
}



function autocompleteSa(inp, arr, which) {
  /*the autocomplete function takes two arguments,
 the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  let company;
  let postCode;
  let cities;
  let streets;
  let contacts;
  let countries

  if (which == 'bill') company = arr.load_company; else if (which == 'ship') company = arr.unload_company;
  if (which == 'bill') postCode = arr.bill_code; else if (which == 'ship') postCode = arr.ship_code;
  if (which == 'bill') cities = arr.bill_city; else if (which == 'ship') cities = arr.ship_city;
  if (which == 'bill') streets = arr.bill_street; else if (which == 'ship') streets = arr.ship_street;
  if (which == 'bill') contacts = arr.load_contact; else if (which == 'ship') contacts = arr.unload_contact;
  if (which == 'bill') countries = arr.bill_country; else if (which == 'ship') countries = arr.ship_country;

  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function (e) {

    if (which == 'bill') {
      $('#SalesOrder_editView_fieldName_load_company').removeClass('selected');
    } else if (which == 'ship') {
      $('#SalesOrder_editView_fieldName_unload_company').removeClass('selected');
    }

    var a, b, i, val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");

    a.setAttribute("class", "autocomplete-items-employee");

    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < company.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (((company[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
        // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        /*create a DIV element for each matching element:*/
        b = document.createElement("DIV");
        b.setAttribute('class', 'listas');
        /*make the matching letters bold:*/
        b.innerHTML = company[i].substr(0, val.length);
        b.innerHTML += company[i].substr(val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + company[i] + "'>";

        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener("click", function (e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.getElementsByTagName("input")[0].value;
          /*close the list of autocompleted values,
          (or any other open lists of autocompleted values:*/
          closeAllLists();
        });

        code = document.createElement("input");
        code.setAttribute('class', 'code');
        code.setAttribute('type', 'hidden');
        code.setAttribute('value', `${postCode[i]}`);


        city = document.createElement("input");
        city.setAttribute('class', 'city');
        city.setAttribute('type', 'hidden');
        city.setAttribute('value', `${cities[i]}`);



        street = document.createElement("input");
        street.setAttribute('class', 'street');
        street.setAttribute('type', 'hidden');
        street.setAttribute('value', `${streets[i]}`);

        contact = document.createElement("input");
        contact.setAttribute('class', 'contact');
        contact.setAttribute('type', 'hidden');
        contact.setAttribute('value', `${contacts[i]}`);


        country = document.createElement("input");
        country.setAttribute('class', 'country');
        country.setAttribute('type', 'hidden');
        country.setAttribute('type', 'hidden');
        country.setAttribute('value', `${countries[i]}`);


        a.appendChild(b);
        b.appendChild(code);
        b.appendChild(city);
        b.appendChild(street);
        b.appendChild(contact);
        b.appendChild(country);


        let listas = document.querySelectorAll('.listas');
        let codeClass = document.querySelectorAll('.code');
        let cityClass = document.querySelectorAll('.city');
        let streetClass = document.querySelectorAll('.street');
        let contactClass = document.querySelectorAll('.contact');
        let countryClass = document.querySelectorAll('.country');

        for (let i = 0; i < listas.length; i++) {
          listas[i].onclick = () => {
            // inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
            inp.classList.add('selected');
            if (which == 'bill') {
              $('#SalesOrder_editView_fieldName_load_contact').val((contactClass[i].value != 'null' ? contactClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_bill_street').val((streetClass[i].value != 'null' ? streetClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_bill_city').val((cityClass[i].value != 'null' ? cityClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_bill_code').val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_bill_country').val((countryClass[i].value != 'null' ? countryClass[i].value : ''));
            } else {
              $('#SalesOrder_editView_fieldName_unload_contact').val((contactClass[i].value != 'null' ? contactClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_ship_street').val((streetClass[i].value != 'null' ? streetClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_ship_city').val((cityClass[i].value != 'null' ? cityClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_ship_code').val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
              $('#SalesOrder_editView_fieldName_ship_country').val((countryClass[i].value != 'null' ? countryClass[i].value : ''));
            }

          }
        }

      }
      inp.onchange = () => {
        if (!inp.classList.contains('selected')) {
          // inp.value = ''; jei nieko nepasirinkama is saraso istustina laukeli
        }
      }
    }
  });




  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
      /*If the arrow DOWN key is pressed,
      increase the currentFocus variable:*/
      currentFocus++;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 38) { //up
      /*If the arrow UP key is pressed,
      decrease the currentFocus variable:*/
      currentFocus--;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items-employee");

    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);

  });
}