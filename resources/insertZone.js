$(document).ready(function(){ 
  if($('.module-breadcrumb-Detail .module-title').html()){
  if($('.module-breadcrumb-Detail .module-title').html().trim() === "Kainoraščiai" && $("span.bookname").html().trim() !== 'BAZINISLT'){ 

  function insertZones(){
        const originUrl = window.location.origin;
        const record = document.querySelector("[name=record_id]").value;
        const url = `${originUrl}/price-algorithm/api.php?record=${record}`;     

          fetch(url)   
          .then((resp) => resp.json())
          .then(function(data) {   
              getConsigness(data[0]);                
            })
          .catch(function(err) {
              console.log(err);
          });   
      
  }

function getConsigness(zona){
        const all = document.querySelector("tbody").getElementsByTagName("td");
        for(let i = 0, len = all.length; i <= len-1; i++){
          let res = document.querySelector("tbody").getElementsByTagName("td")[i].innerText;

    if(zona.city_to_city != null){
      for(let a = 0, len = zona.city_to_city.length; a < len; a++){
          if(res == zona.city_to_city[a]){
            const city_to_city = zona.city_to_city.filter(function(zone) {
              return zone == res;        
            });         
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = city_to_city+" "+"M-M"; 
          }
      } 
    }

    if(zona.city_to_district != null){
      for(let b = 0, len = zona.city_to_district.length; b < len; b++){
           if(res == zona.city_to_district[b]){
            const city_to_district = zona.city_to_district.filter(function(zone2) {
              return zone2 == res;            
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = city_to_district+" "+"M-R";  
          }
        }
      }

      if(zona.city_to_geozone != null){
        for(let c = 0, len = zona.city_to_geozone.length; c < len; c++){
           if(res == zona.city_to_geozone[c]){
            const city_to_geozone = zona.city_to_geozone.filter(function(zone3) {
              return zone3 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = city_to_geozone+" "+"M-G"; 
          }
        }
      }

      if(zona.district_to_city != null){
        for(let d = 0, len = zona.district_to_city.length; d < len; d++){
           if(res == zona.district_to_city[d]){
            const district_to_city = zona.district_to_city.filter(function(zone4) {
              return zone4 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = district_to_city+" "+"R-M"; 
          }
        }
      }

      if(zona.geozone_to_city != null){
        for(let e = 0, len = zona.geozone_to_city.length; e < len; e++){
           if(res == zona.geozone_to_city[e]){
            const geozone_to_city = zona.geozone_to_city.filter(function(zone6) {
              return zone6 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = geozone_to_city+" "+"G-M"; 
          }
        }
      }

      if(zona.district_to_district != null){
        for(let f = 0, len = zona.district_to_district.length; f < len; f++){
           if(res == zona.district_to_district[f]){
            const district_to_district = zona.district_to_district.filter(function(zone5) {
              return zone5 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = district_to_district+" "+"R-R"; 

          }
        }
      }

      if(zona.ltu_to_city != null){
        for(let g = 0, len = zona.ltu_to_city.length; g < len; g++){
           if(res == zona.ltu_to_city[g]){
            const ltu_to_city = zona.ltu_to_city.filter(function(zone7) {
              return zone7 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = ltu_to_city+" "+"LTU-M";  

          }
        }
      }

      if(zona.city_to_ltu != null){
        for(let g = 0, len = zona.city_to_ltu.length; g < len; g++){
           if(res == zona.city_to_ltu[g]){
            const city_to_ltu = zona.city_to_ltu.filter(function(zone10) {
              return zone10 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = city_to_ltu+" "+"M-LTU";  

          }
        }
      }

      if(zona.distance != null){
        for(let j = 0, len = zona.distance.length; j < len; j++){
           if(res == zona.distance[j]){
            const distance = zona.distance.filter(function(zone8) {
              return zone8 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = distance+" "+"Atstumas";  

          }
        }
      }

      if(zona.ltu_to_ltu != null){
          for(let h = 0, len = zona.ltu_to_ltu.length; h < len; h++){
           if(res == zona.ltu_to_ltu[h]){
            const ltu_to_ltu = zona.ltu_to_ltu.filter(function(zone9) {
              return zone9 == res;
            });
            document.querySelector("tbody").getElementsByTagName("td")[i].innerHTML = ltu_to_ltu+" "+"LTU-LTU"; 
          }
        }      
      }
        }             
      }

// Insert BAZINISLT prices

function insertBazinisltPrice(){
  const originUrl = window.location.origin;
  let record_id = $('[name="record_id"').val();  
  const url = `${originUrl}/price-algorithm/bazinislt-kaina.php?record=${record_id}`;        
    fetch(url)   
    .then((resp) => resp.json())
    .then(function(data) { 
      if(data != ''){         
          setBazinis(data);
      }                     
      })
    .catch(function(err) {
        console.log(err);
    });      
      
}


function setBazinis(data){
  const all = document.querySelector("tbody").getElementsByTagName("tr");
  for(let i = 0, len = all.length; i <= len-1; i++){

    const input = document.querySelector("tbody").getElementsByTagName("tr")[i].getElementsByTagName("td");
    const minWeight = input[2];
    const maxWeight = input[3];
    const minVolume = input[4];
    const maxVolume = input[5];
    const setBazinisPrice = input[12];
    
      for(let a = 0, len = input.length; a < len; a++){
          if( parseFloat(minWeight.innerText).toFixed(1) >= parseFloat(data[a].min_weight_kg).toFixed(1) && parseFloat(maxWeight.innerText).toFixed(1) <= parseFloat(data[a].max_weight_kg).toFixed(1) && parseFloat(minVolume.innerText).toFixed(1) >= parseFloat(data[a].min_volume_m3).toFixed(1) && parseFloat(maxVolume.innerText).toFixed(1) <= parseFloat(data[a].max_volume_m3).toFixed(1) ){          
            setBazinisPrice.innerText = `€${parseInt(data[a].listprice)}`;   
          }
      } 
    
} 
}

  $(document).ajaxComplete(function(){
    if($('[data-modulename=Products]').html()){ 
      insertZones(); 
      insertBazinisltPrice();         
    }
  });




}
}
});