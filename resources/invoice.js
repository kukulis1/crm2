let url = new URLSearchParams(window.location.search);

if (url.get('module') == 'Invoice' || url.get('module') == 'Unpaidinvoices') {

  if (url.get('module') == 'Unpaidinvoices' && url.get('view') == 'List') {
    // Client Reminder to pay invoice
    remindPayInvoice();
    $(document).ajaxComplete(function () {
      remindPayInvoice();
    });
  }


  if(url.get('module') == 'Invoice' && url.get('view') == 'Edit'){
    $(`.fix-price`).live("keyup", function (e) {
      $('.saveButton').attr('disabled',true);
      // $("#wait_untill_calculate").show();
    });

    $('#region_id').on('change',function(){
      $('.saveButton').attr('disabled',true);
      // $("#wait_untill_calculate").show();
    });
  }


  if (url.get('module') == 'Invoice' && url.get('view') == 'Edit' && url.get('view') == 'account_id'){
    $(document).ready(function () {
      setTimeout(() => {
        let accountid = $('[name="account_id"]').val();
        let group = $('[name="account_id"]').parent().parent();
        $.ajax({
          type: "POST",
          url: "modules/Invoice/ajax/checkCustomerDiscount.php",
          data: {accountid:accountid},
          dataType: "JSON",
          success: function (response) {            
            if(response.data != null){
              if(response.data.num > 0){
                $('#Invoice_editView_fieldName_description').val(`Pagal pretenzij${(parseInt(response.data.num) > 1 ? 'as':'ą')} ${response.data.claim_no} klientas turi ${response.data.remaining_amount}€ nuolaida`);
                $('#Invoice_editView_fieldName_description').css('color','red');
                $('#Invoice_editView_fieldName_description').css('font-size','20px');             
                let input = `<input type="hidden" name="customerdiscountid" value="${response.data.customerdiscountid}">`;
                let input2 = `<input type="hidden" id="remaining_amount" value="${response.data.remaining_amount}">`;
                group.append(input);
                group.append(input2);
              }
            }
          }
        });
      }, 1000);
    });
  }



  function remindPayInvoice() {
    $(function () {
      $('.reminder_popover').popover({
        container: 'body',
        html: true,
        sanitize: false,
      })
    });



    $("#table-content").off().on('click', '.reminder_popover', function (e) {
      $('.reminder_popover').not(this).popover('hide');
      setTimeout(() => {
        if (document.querySelector('.invoiceid') != undefined) {
          getEmail(e);
          document.querySelector('.invoiceid').value = e.target.parentElement.parentElement.parentElement.dataset.id;
        }
        if (document.querySelector('.icon_id') != undefined) {
          document.querySelector('.icon_id').value = e.target.id;
        }
      }, 500);

    });
  }

  function getEmail(e) {
    let accoundid = e.target.parentElement.parentElement.parentElement.querySelector('[data-accountid]').dataset.accountid;
    let input = document.querySelector('[type="email"]');
    $.ajax({
      type: "POST",
      url: "invoices/getClientEmail.php",
      data: { accoundid: accoundid },
      success: function (result) {
        if (result != 'empty') {
          input.value = result;
        }
      }
    });

  }


  function clientReminderToPay(e) {
    let input = e.target.parentElement.querySelector('[type="email"]');
    let type = e.target.parentElement.querySelector('.type').value;
    let invoiceid = e.target.parentElement.querySelector('.invoiceid').value;
    let icon = e.target.parentElement.querySelector('.icon_id').value;

    var formData = new FormData();
    let ins = document.getElementById('files').files.length;

    for (var x = 0; x < ins; x++) {
      formData.append('file[]', $('#files').prop('files')[x]);
    }

    formData.append('email', input.value);
    formData.append('type', type);
    formData.append('invoiceid', invoiceid);

    if (input.value != '') {
      if (input.classList.contains('alert') && input.classList.contains('alert-danger')) {
        input.classList.remove('alert');
        input.classList.remove('alert-danger');
      }
      $.ajax({
        type: "POST",
        url: "invoices/send_reminder_mail.php",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
        beforeSend: function () {
          $(e.target.parentElement.parentElement).popover('hide');
          $('#table-content').css('opacity', '0.2');
          $('#wait3').show();
        },
        success: function (result) {
          if (result.status == 'success') {
            $('#' + icon).css('color', '#fd7e14');
            $('#wait3').hide();
            $('#table-content').css('opacity', '1');
          } else {
            $('#wait3').hide();
            $('#table-content').css('opacity', '1');
            alert(`Įvyko klaida, laiškas neišsiųstas. Prašome, bandykite dar kartą. ${result.response}`);
          }
        }
      });
    } else {
      input.classList.add('alert');
      input.classList.add('alert-danger');
    }

  }


  if (url.get('module') == 'Invoice' && url.get('view') == 'List') {
    $(document).ready(function () {
      if (document.querySelector('#export_invoice') != null) {
        filterSalesOrdersInvoices();
      }
    });


    $(document).ajaxComplete(function () {
      if (document.querySelector('#export_invoice') != null) {
        filterSalesOrdersInvoices();
      }
    });
  }


  function filterSalesOrdersInvoices() {
    let form_length = $('.searchRow input:not([type="hidden"])').length;
    let parameters = '';
    let viewname = document.querySelector('[name="cvid"]').value;

    for (let i = 0; i < form_length; i++) {
      let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
      let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
      if (form_value != '') {
        parameters += `${form_key}=${form_value}:`;
      }
    }

    parameters = parameters.slice(0, -1);
    const exportBtn = document.querySelector('#export_invoice');
    const exportDetailBtn = document.querySelector('#export_detail_invoice');
    exportBtn.setAttribute('onclick', `window.location.href = 'vtlib/Vtiger/exel/sales_order_invoices.php?viewname=${viewname}&search_parameters=${parameters}'`);
    exportDetailBtn.setAttribute('onclick', `window.location.href = 'vtlib/Vtiger/exel/sales_order_detail_invoices.php?viewname=${viewname}&search_parameters=${parameters}'`);
  }



  if (url.get('module') == 'Invoice' && url.get('view') == 'List') {
    fieldsCheker();


    $(document).ajaxComplete(function () {
      fieldsCheker();
    });

  }


  var checkInvoiceType = function () {
    var tmp = null;
    let recordId = url.get('record');
    $.ajax({
      'async': false,
      'type': "POST",
      'global': false,
      'dataType': 'JSON',
      'url': "invoices/checkInvoiceType.php",
      'data': { recordId: recordId },
      'success': function (data) {
        if (parseInt(data) === 121391) {
          tmp = true;
        } else if (data == 'Fail') {
          tmp = false;
        }
      }
    });
    return tmp;
  }();

  if (url.get('module') == 'Invoice' && url.get('newRecord') == 'true' && url.get('view') != 'Detail') {
    loadInvoiceDetails();
  }


  if (url.get('module') == 'Invoice' && url.get('newRecord') == 'true' && url.get('record') == null && url.get('view') != 'Detail') {
    jQuery('[name="account_id"]').attr('class', 'sourceField');
    setToDayDate();
    loadItemsForInvoice();
    loadInvoiceDetails();
    loadPurchaseAjaxLists();
    setNewRecordSettings();

    if (document.querySelector(`#fix-price1`).value == '') {
      document.querySelector(`#fix-price1`).value = 0;
    }

    loadPriceByQty();
    changePriceByQty();
    document.getElementById(`lineItemType1`).value = 'Item';
    document.getElementById(`hdnProductId1`).value = 121391;

    $('.add_new_entity').click(function (e) {
      let thestring = e.target.id;
      let id = thestring.replace(/^\D+/g, '');
      addNewItemService(id);
    });
  } else if (url.get('module') == 'Invoice' && url.get('record') && url.get('view') != 'Detail') {
    loadFixPrice();
    if (checkInvoiceType) {
      loadItemsForInvoice();
      setNewRecordSettings();
      loadPriceByQty();
      changePriceByQty();
      loadDiscount();
      $('.add_new_entity').click(function (e) {
        let thestring = e.target.id;
        let id = thestring.replace(/^\D+/g, '');
        addNewItemService(id);
      });
    }
  }


  if (url.get('module') == 'Invoice' && url.get('view') == 'Edit' && url.get('invoices')) {

    count = $(".fix-price");

    for (let i = 1; i < count.length; i++) {
      document.getElementById(`fix-price${i}`).value = -document.getElementById(`fix-price${i}`).value;
      document.getElementById(`productTotal${i}`).innerText = -document.getElementById(`productTotal${i}`).innerText;
      document.getElementById(`netPrice${i}`).innerText = -document.getElementById(`netPrice${i}`).innerText;
    }

    setPricesOnload();
    setToDayDate();
    document.querySelectorAll('[data-fieldname="cf_1277"] option')[2].setAttribute('selected', 'selected');
    document.querySelector('[data-fieldname="cf_1277"]').setAttribute('data-selected-value', 'Kreditinė');

  }


  function fieldsCheker() {
    $('.listViewEntriesCheckBox').click(function () {
      if ($(this).is(':checked')) {
        addInvoiceList();
      } else {
        addInvoiceList();
      }
    });
  }


  function addInvoiceList() {
    let usersArr = new Array();
    let body = $('#listview-table tbody tr');
    let invoiceid = new Array();
    for (let i = 0; i < body.length; i++) {
      let check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
      if (check) {
        usersArr.push(document.querySelectorAll('#listview-table tbody tr')[i].children[5].querySelectorAll('span a')[0].href.replace(/[^0-9]/g, ''));
        invoiceid.push($('#listview-table tbody tr').eq(i).data('id'));
      }
    }

    if (usersArr.length > 0) {
      checkUserId = usersArr.every((val, i, arr) => val === arr[0]);
    } else {
      checkUserId = false;
    }

    if (checkUserId) {
      createBtn(invoiceid);
    } else {
      createBtn(false);
    }
  }

  function createBtn(invoiceid) {

    if (typeof invoiceid === 'boolean') {
      if (document.getElementById('invoice_for_invoice') != undefined) {
        document.getElementById('invoice_for_invoice').remove();
      }
    } else {
      if (document.getElementById('invoice_for_invoice') != undefined) {
        document.getElementById('invoice_for_invoice').remove();
      }
      let menu = document.querySelector('#appnav ul');
      let li = document.createElement('li');
      let firstLi = document.querySelectorAll('#appnav .navbar-nav li')[0];
      li.setAttribute('id', 'invoice_for_invoice');
      li.setAttribute('type', 'button');
      li.setAttribute('class', 'btn addButton btn-warning');
      li.setAttribute('style', 'padding: 6px 12px; margin: 4px 2px;');
      li.setAttribute('onclick', `window.location.href="index.php?module=Invoice&view=Edit&invoices=${invoiceid}&create"`);
      li.innerHTML = `Išrašyti kreditinę saskaitą`;
      if (document.getElementById('invoice_for_invoice') == undefined) {
        menu.insertBefore(li, firstLi);
      }
    }
  }


  if (url.get('module') == 'Invoice' && url.get('view') == 'Edit' && !url.get('record') && url.get('newRecord' != 'true' && !checkInvoiceType)) {

    jQuery('[name="account_id"]').attr('class', 'sourceField');
    setTimeout(() => {
      loadInvoiceDetails();
      addService();
      loadPurchaseAjaxLists();
      setToDayDate();
      loadCheckInputFields();
    }, 1000);
  }

  if ((url.get('module') == 'Invoice' && url.get('view') != 'Detail') && (url.get("orderid") != null || url.get("account_id") != null || url.get("record") != null)) {

    $('.listViewEntriesMainCheckBox').click(function () {
      if ($(this).is(':checked')) {
        $('input:checkbox').attr('checked', true);
        disableUncheckFields(true);
      } else {
        $('input:checkbox').attr('checked', false);
        disableUncheckFields(false);
      }
    });

    function removePriceWhenUncheck(i) {
      if (typeof i === 'boolean') {
        $('.netPrice').addClass('except');
      } else {
        $(i.target).parent().parent().find('.netPrice').addClass('except');
      }
      $('.saveButton').attr('disabled',true);
      // $("#wait_untill_calculate").show();
      setPricesOnload();
    }

    function addPriceWhencheck(i) {
      if (typeof i === 'boolean') {
        $('.netPrice').removeClass('except');
      } else {
        $(i.target).parent().parent().find('.netPrice').removeClass('except');
      }
      $('.saveButton').attr('disabled',true);
      // $("#wait_untill_calculate").show();
      setTimeout(function () {
        setPricesOnload();
      }, 1);
    }



    let radios = $('#prices_fix_inputs input[type="radio"]');
    function checkRadio() {
      return radios.filter(':checked').filter(function () {
        return (this.value == '+');
      }).length > 0;
    }

    function inRange() {
      if (checkRadio()) {
        adjustment = $("#adjustment").val();
        total = $("#grandTotal").html();
        grandTotal = (parseFloat(total) + parseFloat(adjustment));
        $("#grandTotal").html(parseFloat(grandTotal).toFixed(2));
      } else {
        adjustment = $("#adjustment").val();
        total = $("#grandTotal").html();
        grandTotal = (parseFloat(total) - parseFloat(adjustment));
        $("#grandTotal").html(parseFloat(grandTotal).toFixed(2));

      }
    }

    $('#adjustment').change(inRange);
    radios.change(inRange);

    $('#received').change(() => {
      receive = $("#received").val();
      total = $("#grandTotal").html();
      grandTotal = (parseFloat(total) - parseFloat(receive));
      $("#balance").val(parseFloat(grandTotal).toFixed(2));
    });


    function saveAddress(value, order, location, event) {
      $.ajax({
        type: "POST",
        url: "invoices/updateAddress.php",
        data: { value: value, order: order, location: location },
        success: function (result) {
          if (result == 'success') {
            event.classList.add('hide');
            event.parentElement.querySelectorAll('input')[1].classList.add('hide');
          }
        }
      });
    }

  }

  

  function disableUncheckFields(d) {
    if (typeof d === 'boolean') {
      if (d) {
        $(".lineItemRow :input").not('[name="list"]').not('.order_id_check').not('.order_checked').prop("disabled", false);
        addPriceWhencheck(true);

        let orderId = document.querySelectorAll('.order_id:not(.except)');
        let salesorderid = document.querySelector('[name="salesorder_id"]').value;
        if (salesorderid.length > 1) salesorderid = salesorderid.split(","); else salesorderid = new Array();
        for (let o = 1; o < orderId.length; o++) {
          if (orderId[o].value != 0) {
            salesorderid.push(orderId[o].value);
          }
        }
        document.querySelector('[name="salesorder_id"]').value = salesorderid;
      } else {
        $(".lineItemRow :input").not('[name="list"]').not('.order_id_check').not('.order_checked').prop("disabled", true);

        removePriceWhenUncheck(true);
        document.querySelector('[name="salesorder_id"]').value = '';
      }
    } else {

      let checkbox = d.target;
      if (!checkbox.checked) {

        d.target.parentElement.parentElement.classList.add('cancel');

        $(d.target).parent().parent().find('input').not('[name="list"]').not('.order_id_check').not('.order_checked').prop("disabled", true);
        $(d.target).parent().parent().find('.order_checked').val(0);
        $(d.target).parent().parent().find('textarea').not('[name="list"]').prop("disabled", true);
        $(d.target).parent().parent().find('select').not('[name="list"]').prop("disabled", true);
        $(d.target).parent().parent().find('button').not('[name="list"]').prop("disabled", true);

        removePriceWhenUncheck(d);
        let orderId = $(d.target).parent().parent().find('.order_id').val();
        let salesorderid = document.querySelector('[name="salesorder_id"]').value;
        if (salesorderid.length > 1) salesorderid = salesorderid.split(","); else salesorderid = new Array();
        let newValues = salesorderid.filter((list) => {
          return list != orderId;
        });
        document.querySelector('[name="salesorder_id"]').value = newValues;

      } else {
        d.target.parentElement.parentElement.classList.remove('cancel');
        $(d.target).parent().parent().find('input').not('[name="list"]').not('.order_id_check').not('.order_checked').prop("disabled", false);
        $(d.target).parent().parent().find('.order_checked').val(1);
        $(d.target).parent().parent().find('textarea').not('[name="list"]').prop("disabled", false);
        $(d.target).parent().parent().find('select').not('[name="list"]').prop("disabled", false);
        $(d.target).parent().parent().find('button').not('[name="list"]').prop("disabled", false);
        addPriceWhencheck(d);
        let orderId = $(d.target).parent().parent().find('.order_id').val();
        let salesorderid = document.querySelector('[name="salesorder_id"]').value;
        if (orderId != 0) {
          if (salesorderid.length > 1) salesorderid = salesorderid.split(","); else salesorderid = new Array();
          salesorderid.push(orderId);
        }
        document.querySelector('[name="salesorder_id"]').value = salesorderid;

      }
    }
  }





  $(document).ready(function () {
    if ((url.get('module') == 'Invoice' && url.get('create') != null && url.get('view') != 'Detail') && (url.get("orderid") != null || url.get("account_id") != null || url.get("record") != null || url.get("invoices") != null)) {

      document.querySelector('#sales_order_title').style.display = 'none';
      document.querySelector('#sales_order_input').style.display = 'none';

      let orderid = url.get("orderid");
      let userid = url.get("account_id");
      let from = url.get("from");
      let to = url.get("to");
      let filterBy = url.get("filterBy");
      let orderDate = url.get("orderDate");
      let orderNumber = url.get("orderNumber");
      let customer = url.get("customer");
      let pricebook = url.get("pricebook");
      let loadDate = url.get("loadDate");
      let loadAddress = url.get("loadAddress");
      let unloadDate = url.get("unloadDate");
      let unloadAddress = url.get("unloadAddress");
      let status = url.get("status");
      let manager = url.get("manager");

      let record = url.get("record");
      let invoices = url.get("invoices");

      if (orderid != null) orderid = orderid.replace(/'/g, '');
      if (userid != null) userid = userid.replace(/'/g, '');
      if (record != null) record = record.replace(/'/g, '');


      if (orderDate != null) orderDate = orderDate.replace(/'/g, '');
      if (orderNumber != null) orderNumber = orderNumber.replace(/'/g, '');
      if (customer != null) customer = customer.replace(/'/g, '');
      if (pricebook != null) pricebook = pricebook.replace(/'/g, '');
      if (loadDate != null) loadDate = loadDate.replace(/'/g, '');
      if (loadAddress != null) loadAddress = loadAddress.replace(/'/g, '');
      if (unloadDate != null) unloadDate = unloadDate.replace(/'/g, '');
      if (unloadAddress != null) unloadAddress = unloadAddress.replace(/'/g, '');
      if (status != null) status = status.replace(/'/g, '');
      if (manager != null) manager = manager.replace(/'/g, '');


      if (url.get('order_ids') != null && url.get('pre') == 1) {
     
        let hideInput = document.createElement('input');
        hideInput.setAttribute('type', 'hidden');
        hideInput.setAttribute('name', 'prelimiter');
        hideInput.setAttribute('id', 'prelimiter');
        hideInput.setAttribute('value', 1);
        let invoiceType = document.querySelector('[data-fieldname="cf_1277"]');
        invoiceType.append(hideInput);
        let type = $('[name="cf_1277"] option:selected').val();

        if(type == 'Debetinė' && $('[name="cf_1277"] option').length > 2){
          $('[data-fieldname="cf_1277"] option').eq(3).attr('selected', 'selected');
          $('[data-fieldname="cf_1277"]').attr('data-selected-value', 'Išankstinė');
          $('#select2-chosen-2').text('Išankstinė'); 
        }else{
          $('[data-fieldname="cf_1277"] option').eq(1).attr('selected', 'selected');
          $('[data-fieldname="cf_1277"]').attr('data-selected-value', type);
          $('#select2-chosen-2').text(type);        
        } 
        

        sendQueryPre(url.get("order_ids"));
      } else if (url.get('orderid') != null) {
        sendQuery(orderid);
      } else if (url.get('account_id') != null) {        
        sendQueryUser(filterBy, from, to, userid, orderDate, orderNumber, customer, pricebook, loadDate, loadAddress, unloadDate, unloadAddress, status, manager);
      } else if (url.get('record') != null) {
        sendQueryRecord(record);
      } else if (url.get('invoices') != null) {
        sendQueryInvoices(invoices);
      }

      function sendQueryPre(orderId) {
        $.ajax({
          type: "POST",
          url: "invoices/invoice_filler_pre.php",
          data: { orderId: orderId },
          dataType: 'JSON',
          success: function (result) {
            insertValues(result);
          }
        });
      }

      function sendQuery(orderId) {
        $.ajax({
          type: "POST",
          url: "invoices/invoice_filler.php",
          data: { orderId: orderId },
          dataType: 'JSON',
          success: function (result) {
            insertValues(result);
          }
        });
      }

      function sendQueryUser(filterBy, from, to, userid, orderDate, orderNumber, customer, pricebook, loadDate, loadAddress, unloadDate, unloadAddress, status, manager) {
        $.ajax({
          type: "GET",
          url: `invoices/invoice_filler_by_user.php?userid=${userid}${(orderDate) ? '&orderDate=' + orderDate : ''}${(orderNumber) ? '&orderNumber=' + orderNumber : ''}${(customer) ? '&customer=' + customer : ''}${(pricebook) ? '&pricebook=' + pricebook : ''}${(loadDate) ? '&loadDate=' + loadDate : ''}${(loadAddress) ? '&loadAddress=' + loadAddress : ''}${(unloadDate) ? '&unloadDate=' + unloadDate : ''}${(unloadAddress) ? '&unloadAddress=' + unloadAddress : ''}${(status) ? '&status=' + status : ''}${(manager) ? '&manager=' + manager : ''}${(filterBy) ? '&filterBy=' + filterBy : ''}${(from) ? '&from=' + from : ''}${(to) ? '&to=' + to : ''}`,
          dataType: 'JSON',
          success: function (result) {       
            insertValues(result);
          }
        });
      }

      function sendQueryRecord(orderId) {
        $.ajax({
          type: "POST",
          url: "invoices/invoice_filler_edit.php",
          data: { orderId: orderId },
          dataType: 'JSON',
          success: function (result) {
            insertValues(result);
          }
        });
      }

      function sendQueryInvoices(invoices) {
        $.ajax({
          type: "POST",
          url: "invoices/invoice_for_invoice.php",
          data: { invoices: invoices },
          dataType: 'JSON',
          success: function (result) {
            insertKreditValue(result);

          }
        });
      }

      function repeat(func, times) {
        func();
        times && --times && repeat(func, times);
      }

      function insertKreditValue(client) {
        let accountname = document.getElementById('account_id_display');
        let accoundid = document.querySelector('[name="account_id"]');
        let accoundIdDisplay = document.getElementById('account_id_display');
        // salesorderid.value = client.salesorderid;
        accountname.value = client.accountname;
        accoundid.value = client.accountid;
        accoundIdDisplay.setAttribute('disabled', 'disabled');

        let date = new Date();
        var day = String(date.getDate()).padStart(2, '0');
        var month = String(date.getMonth() + 1).padStart(2, '0');
        var years = date.getFullYear();

        let today = `${years}-${month}-${day}`;

        let invoiceDate = document.getElementById('Invoice_editView_fieldName_invoicedate');

        let paymentDay = String(date.getDate()).padStart(2, '0');
        if (client.payment_deferral != '') {
          paymentDay = parseFloat(paymentDay) + parseFloat(client.payment_deferral);
        } else {
          paymentDay = paymentDay;
        }

        document.getElementById('duedate').value = client.payment_deferral;
        setTimeout(() => {
          whenChangeInvoiceDateChangeDueDate();
        }, 100);

      }


      // Fill fields
      function insertValues(order) {
        if (order.salesorderid == null) {
          alert('Sąskaita jau išrašyta arba toks užsakymas neegzistuoja');
          window.history.back();
        } else {       

          let date = new Date();
          var day = String(date.getDate()).padStart(2, '0');
          var month = String(date.getMonth() + 1).padStart(2, '0');
          var years = date.getFullYear();

          let today = `${years}-${month}-${day}`;

          let invoiceDate = document.getElementById('Invoice_editView_fieldName_invoicedate');

          let paymentDay = String(date.getDate()).padStart(2, '0');
          if (order.payment_deferral != '') {
            paymentDay = parseFloat(paymentDay) + parseFloat(order.payment_deferral);
          } else {
            paymentDay = paymentDay;
          }
          
          document.getElementById('duedate').value = order.payment_deferral;
          setTimeout(() => {
            whenChangeInvoiceDateChangeDueDate();
          }, 100);  


          invoiceDate.value = today;

          setPricesOnload();
          $(`.fix-price`).live("input", function (e) {
            fixInput(e);
          });



          checkboxes = document.getElementsByName('list');
          for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = 'checked';
          }

          if (!url.get('orderid')) {
            var so_popup = document.querySelectorAll('.so_popup');
            for (let i = 1; i < so_popup.length; i++) {
              var so_btn = document.getElementById(`so_popup${i}`);
              so_btn.dataset.target = so_btn.dataset.target + i;
              so_btn.dataset.num = i;
            }
          }  

        }
      }


      function addMainFuelService(surcharge) {
        $(document).ready(() => {
          setTimeout(() => {
            $("#addService").trigger("click");
            addMainFuelServiceValues(surcharge);
          }, 200);

        });
      }

      async function addMainFuelServiceValues(surcharge) {
        let promise = new Promise((res, rej) => {
          setTimeout(() => res(), 1000)
        });
        let result = await promise;
        let service_id = $(".service_select").length - 1;
        $(`#service_select${service_id}`).val(8);
        $(`#fix-price${service_id}`).val(surcharge);
        $(`#productTotal${service_id}`).html(surcharge);
        $(`#netPrice${service_id}`).html(surcharge);
        setPricesOnload();
      }

    } else if (url.get('module') == 'Invoice' && url.get('view') == 'Edit' && url.get("record") != null) {
      jQuery('[name="account_id"]').attr('class', 'sourceField');
      document.querySelector('#sales_order_title').style.display = 'none';
      document.querySelector('#sales_order_input').style.display = 'none';
      setPricesOnload();
    } else if ((url.get('module') == 'Invoice' && url.get('view') == 'Edit') && (url.get('newRecord') == 'true' || url.get('record'))) {

      $(document).ready(function () {
        document.getElementById("account_id_display").addEventListener("keydown", waitChanges);
        document.getElementById('account_id_display').addEventListener('mousedown', waitChanges);
      });


      function waitChanges(event) {
        document.querySelector('#ui-id-1').addEventListener('mousedown', waitChanges);
        if (event instanceof MouseEvent) {
          setTimeout(() => {
            setDefferral();
          }, 200);
        } else if (event instanceof KeyboardEvent && event.keyCode === 13) {
          setTimeout(() => {
            setDefferral();
          }, 200);
        }
      }



      function setDefferral() {
        let accountid = $('[name="account_id"]').val();
        if (accountid != '') {
          $.ajax({
            type: "POST",
            url: "invoices/payment_deferral.php",
            data: { accountid: accountid },
            success: function (deferral) {
              loadPaymentDeferral(deferral);
            }
          });
        }
      }



      $('.relatedPopup #Invoice_editView_fieldName_account_id_select').on('click', function () {
        $(document).ajaxComplete(function () {
          $('.listViewEntries').on('click', function () {
            setTimeout(() => {
              let accountid = $('[name="account_id"]').val();
              $.ajax({
                type: "POST",
                url: "invoices/payment_deferral.php",
                data: { accountid: accountid },
                success: function (deferral) {
                  loadPaymentDeferral(deferral);
                }
              });
            }, 500);
          });
        });
      });
    }

    function loadPaymentDeferral(deferral) {    
      const d = new Date();  
      d.setDate(d.getDate() + Number(deferral));    
      let newUntilDay = d.toISOString().substring(0, 10);
      document.getElementById('Invoice_editView_fieldName_duedate').value = newUntilDay;
    }


    if ((url.get('module') == 'Invoice' && url.get('view') == 'Edit') && url.get('newRecord') != 'true' && !checkInvoiceType && (url.get("orderid") != null || url.get("account_id") != null || url.get("record") != null)) {

      setTimeout(() => {
        loadInvoiceDetails();
        addService();
        loadPurchaseAjaxLists();

      }, 1000);
    }

  });


  function changePrice(e) {
    let priceField = $(e.target).parent().parent().parent().find('.productTotal'); 
    let netPrice = $(e.target).parent().parent().parent().find('.netPrice'); 
    let netPriceInput = $(e.target).parent().parent().parent().find('.netPriceValue'); 
    let newPrice = $(e.target).val();
    if (newPrice == '') newPrice = 0;

    priceField.html(parseFloat(newPrice).toFixed(2));
    netPrice.html(parseFloat(newPrice).toFixed(2));

    if($(e.target).parent().parent().parent().find('.netPriceValue').val() != '') {
      netPriceInput.val(parseFloat(newPrice).toFixed(2));
    }
    if (url.get('newRecord') == 'true') {
      loadPriceByQty();
    }
  }

  function loadDiscount() {
    let rows = document.querySelectorAll('#lineItemTab tr:not(.hide) .netPrice');
    for (let p = 1, len = rows.length; p <= len; p++) {
      let netPrice = document.getElementById(`netPrice${p}`);
      let productTotal = document.getElementById(`productTotal${p}`);
      let discountTotal = document.getElementById(`discountTotal${p}`);
      let discountPrice = parseFloat(productTotal.innerHTML) - parseFloat(discountTotal.innerHTML);
      $(`#totalAfterDiscount${p}`).html(discountPrice);
      netPrice.innerHTML = discountPrice;
    }
  }

  function loadInvoiceDetails() {
    let invoiceDate = document.querySelector('#Invoice_editView_fieldName_invoicedate');
    invoiceDate.onchange = () => {
      whenChangeInvoiceDateChangeDueDate();
    }

    // let taxRegion = document.querySelector('#region_id');
    // taxRegion.onchange = () => {
    //   let chooseOption = document.querySelector('.modal-content .confirm-box-ok');
    //   chooseOption.onclick = () => {
    //     setTimeout(() => {
    //       setPricesOnload();
    //     }, 600);
    //   }

    // }

    $('#region_id').on('click', function(){
      $('.bootbox-confirm').find('.confirm-box-ok').on('click', function(){        
        setTimeout(() => {
          setPricesOnload();
        }, 600);
      });  

    });



    $(`.fix-price`).live("input", function (e) {
       fixInput(e);
    });
  }

  function loadFixPrice() {   
    $(`.fix-price`).live("input", function (e) {
       fixInput(e);
    });  
  }

  var fix_input_timer;
  function fixInput(e){  
    if(fix_input_timer){
        clearTimeout(fix_input_timer);
        fix_input_timer = null;
    } 
    fix_input_timer = setTimeout(function(){
      changePrice(e);
      loadDiscountToRow(e);
      countTotal();
    }, 1000);
  }


  var input_timer;
  function countTotal(){  
    if(input_timer){
        clearTimeout(input_timer);
        input_timer = null;
    } 
    input_timer = setTimeout(function(){
      setTotalPrice();
    }, 1000);
  }

  function loadDiscountToRow(e) {
    let discount_type = $(e.target).parent().parent().find('.discountUI').find('.discount_type').val();
    let fixPrice = $(e.target).parent().parent().parent().find('.fix-price');
    let newPrice;

    if (discount_type == 'percentage') {
      let percent = $(e.target).parent().parent().find('.discountUI').find('.discount_percentage').val();      
      let discount = parseFloat(fixPrice.val()) / 100 * percent;
      newPrice = parseFloat(fixPrice.val()) - parseFloat(discount);      
    } else if (discount_type == 'amount') {      
      let amount = $(e.target).parent().parent().find('.discountUI').find('.discount_amount').val();
      newPrice = parseFloat(fixPrice.val()) - parseFloat(amount);      
    }  

    if (discount_type == 'percentage' || discount_type == 'amount') {
      let discountPrice = parseFloat(fixPrice.val()) - parseFloat(newPrice);
      $(e.target).parent().parent().parent().find('.discountTotal').html(discountPrice)
      $(e.target).parent().parent().parent().find('.totalAfterDiscount').html(newPrice);
  
      $(e.target).parent().parent().parent().find('.netPrice').html(newPrice);    
      $(e.target).parent().parent().parent().find('.netPriceValue').val(newPrice);    
    }
  }


  function getDiscount(e) {
    setTimeout(() => {
      let element = e.target.parentElement.parentElement.parentElement.parentElement.id
      let inputValue = e.target.parentElement.parentElement.parentElement.parentElement.querySelectorAll("input[type=text]");

      let father = e.target.parentElement.parentElement.parentElement.parentElement.querySelector(".discountUI").id;
      let id = father.replace(/\D/g, '');
      discount_type = $(`#discount_type${id}`).val();

      let netPrice = document.querySelectorAll(`#netPrice${id}`)[0];
      let productTotal = document.querySelectorAll(`#productTotal${id}`)[0];
      let newPrice;

      if (discount_type == 'percentage') {
        let percent = inputValue[0].value;
        let discount = productTotal.innerHTML / 100 * percent;
        newPrice = productTotal.innerHTML - discount;
      } else if (discount_type == 'amount') {
        let amount = inputValue[1].value;
        newPrice = productTotal.innerHTML - amount;
      } else {
        newPrice = productTotal.innerHTML;
      }

      let discountPrice = parseFloat(productTotal.innerHTML) - parseFloat(newPrice);
      $(`#discountTotal${id}`).html(discountPrice);
      $(`#totalAfterDiscount${id}`).html(newPrice);

      netPrice.innerHTML = newPrice;

      $(`#${element}`).popover('destroy');
    }, 100);
    setTimeout(() => {
      setPricesOnload();
    }, 200);
  }



  // }

  function addService() {
    document.getElementById('addService').onclick = () => {
      setTimeout(() => {
        let count = document.querySelectorAll('.lineItemRow').length;
        document.getElementById(`productName${count}`).value = 'Services';
        document.getElementById(`order_number${count}`).innerText = 'Paslauga';
        document.getElementById(`hdnProductId${count}`).value = 36641;
        document.getElementById(`order_id${count}`).value = 0;
        document.getElementById(`bill_address${count}`).style = 'display:none;';
        document.getElementById(`ship_address${count}`).style = 'display:none;';
        document.getElementById(`cargo_wgt${count}`).style = 'display:none;';
        document.getElementById(`cargo_volume${count}`).style = 'display:none;';
        // document.getElementById(`costcenter${count}`).style = 'display:none;';
        // document.getElementById(`object${count}`).style = 'display:none;';
        // document.getElementById(`employee${count}`).style = 'display:none;';
        document.getElementById(`measure${count}`).style = 'display:none;';
        document.getElementById(`service_select${count}`).style = 'width: 100%; display:block;';
        jQuery(`#row${count} .addServiceAfterCargo`).hide();
        let order_num = document.getElementById(`order_number${count}`);
        let hideInput = document.createElement('input');
        hideInput.setAttribute('type', 'hidden');
        hideInput.setAttribute('class', `info_popup`);
        order_num.append(hideInput);
        getService(count);
        loadInvoiceDetails();
        loadCheckInputFields();
      }, 50);
    }
  }

  function checkServiceType(event){
    const service_id = $(event.target).parent().parent().parent().find('.service_select').val();
    if(service_id == 33){
      const remaining_amount = $('#remaining_amount').val();
      const price = Math.abs($(event.target).val());
      if(remaining_amount < price){
        alert('Įrašyta nuolaida didesnė nei kliento kortelėje');
      }
    }
    
  }

  function loadPurchaseAjaxLists() {

    $.ajax({
      type: "POST",
      url: "purchase/users_list.php",
      data: { id: 1 },
      dataType: 'JSON',
      success: function (user_list) {
        rows = document.querySelectorAll('#lineItemTab .lineItemRow');
        for (i = 1, len = rows.length; i <= len; i++) {
          autocomplete(document.getElementById(`employee${i}`), user_list, 'users');
        }
      }
    });

    $.ajax({
      type: "POST",
      url: "invoices/object.php",
      data: { id: 1 },
      dataType: 'JSON',
      success: function (object) {
        rows = document.querySelectorAll('#lineItemTab .lineItemRow');
        for (i = 1, len = rows.length; i <= len; i++) {
          autocomplete(document.getElementById(`object${i}`), object, 'object');
        }
      }
    });
  }

  // suggestions

  function autocomplete(inp, arr, place) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/

    if (inp != null) {
      inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();

        if (val.length >= 2) {

          if (!val) { return false; }
          currentFocus = -1;
          /*create a DIV element that will contain the items (values):*/
          a = document.createElement("DIV");
          a.setAttribute("id", this.id + "autocomplete-list");
          if (place == 'history') {
            a.setAttribute("class", "autocomplete-items");
          } else {
            a.setAttribute("class", "autocomplete-items-employee");
          }
          /*append the DIV element as a child of the autocomplete container:*/
          this.parentNode.appendChild(a);
          /*for each item in the array...*/
          for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (((arr[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
              // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
              /*create a DIV element for each matching element:*/
              b = document.createElement("DIV");
              b.setAttribute('class', 'listas');
              /*make the matching letters bold:*/
              b.innerHTML = arr[i].substr(0, val.length);
              b.innerHTML += arr[i].substr(val.length);
              /*insert a input field that will hold the current array item's value:*/
              b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
              /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function (e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
              });
              a.appendChild(b);
              let listas = document.querySelectorAll('.listas');
              for (let i = 0; i < listas.length; i++) {
                listas[i].onclick = () => {
                  inp.readOnly = true;
                  inp.classList.add('selected');
                }
              }

            }
            inp.onchange = (e) => {
              if (!inp.classList.contains('selected')) {
                inp.value = '';
                $(e.target.parentElement).find('.add_new_entity').addClass('alert-danger');
              } else {
                $(e.target.parentElement).find('.add_new_entity').removeClass('alert-danger');
              }
            }
          }
        }
      });
    }


    if (inp != null) {
      /*execute a function presses a key on the keyboard:*/
      inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
      });
    }
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      if (place == 'history') {
        var x = document.getElementsByClassName("autocomplete-items");
      } else {
        var x = document.getElementsByClassName("autocomplete-items-employee");
      }
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
      closeAllLists(e.target);

    });
  }

  function getService(i) {
    let select = document.getElementById(`service_select${i}`);
    let option;
    $.ajax({
			type: "POST",
			url: "modules/Invoice/ajax/getServices.php",
			data: {1:1},
			dataType: "JSON",
			success: function (response) {
				response.forEach(service => {
					option = `<option value="${service.id}">${service.name}</option>`;
					select.innerHTML += option;
				});
			}
		});
  }

  function whenChangeInvoiceDateChangeDueDate() {
    let duedate = Number(document.querySelector('#duedate').value);
    let invoiceDate = document.querySelector('#Invoice_editView_fieldName_invoicedate');
    let invoiceDateShows = document.querySelector('#Invoice_editView_fieldName_duedate');

    Date.prototype.addDays = function(days) {
      var date = new Date(this.valueOf());
      date.setDate(date.getDate() + days);
      return date;
    }

    if(duedate <= 0){
      duedate = 0;
    }

   
    let date = new Date(invoiceDate.value);
    let newDate = date.addDays(duedate);

 
    // let newDate = new Date(invoiceDate.value);
    // let day = String(newDate.getDate() + Number(duedate.value)).padStart(2, '0');
    // let month = String(newDate.getMonth() + 1).padStart(2, '0');
    // let years = newDate.getFullYear();



    // let dayOfMonth = daysInMonth(month, years);
    // if (dayOfMonth < day) {
    //   let muchDays = Number(day) - Number(dayOfMonth);
    //   if (muchDays <= dayOfMonth) {
    //     month = Number(month) + 1;
    //     month = String(month).padStart(2, '0');
    //     day = String(muchDays).padStart(2, '0');
    //   }
    // }


    // if (Number(month) > 12) {
    //   month = '01';
    //   years = years + 1;
    // }
    // let newUntilDay = `${years}-${month}-${day}`;
    invoiceDateShows.value = newDate.toISOString().substring(0, 10);
  }

  function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }



  function setTotalPrice() { 
    let netPricesArr = $('#lineItemTab tr:not(.hide) .netPrice:not(.except)').map(function(){return Number($(this).text());}).get();  
    let totalPrice = netPricesArr.reduce((a, b) => a + b, 0);
    $('#netTotal').html(round(totalPrice, 2));
    let vat = addIvoiceVAT();
    vat = parseFloat(vat); 
    netPricesArr.push(vat);  
    
    setTimeout(() => {
      let totalPriceWithVat = netPricesArr.reduce((a, b) => a + b, 0);

      if (vat != 0) {
        $('#grandTotal').html(round(totalPriceWithVat, 2));
      } else {
        $('#chargesTotalDisplay').html('0.00');
        $('#grandTotal').html(round(totalPriceWithVat, 2));
      }      
   
      $('.saveButton').attr('disabled',false);  
        // $("#wait_untill_calculate").hide();    

    }, 1000);
  }


  function setPricesOnload() {   
    let netPricesArr = $('#lineItemTab tr:not(.hide) .netPrice:not(.except)').map(function(){return Number($(this).text());}).get();
    let totalPrice = netPricesArr.reduce((a, b) => a + b, 0);

    $('#netTotal').html(round(totalPrice, 2));
    let vat = addIvoiceVAT();
    vat = parseFloat(vat);
    netPricesArr.push(vat);  

    setTimeout(() => {
      let newTotalWithVat = netPricesArr.reduce((a, b) => a + b, 0);         
      if (vat != 0) {
        $('#grandTotal').html(round(newTotalWithVat,2));
      } else {
        $('#chargesTotalDisplay').html('0.00');
        $('#grandTotal').html(round(newTotalWithVat,2));
      }

      $('.saveButton').attr('disabled',false);
      // $("#wait_untill_calculate").hide();  

    }, 1000);
  }

  function round(value, decimals) {    
    let newNumber = 0;
    if(value < 0){
      newNumber = Number(Math.round((value*-1) + 'e' + decimals) * -1 + 'e-' + decimals);
    }else{
      newNumber  = Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }
    return newNumber;
  }

  function addIvoiceVAT() {

    let data;
    let id = document.querySelector(`#region_id`).value;
    data = document.querySelectorAll(`#region_id option`)[id].getAttribute('data-info');
    let parseJson = JSON.parse(data);
    let vatPercent;

    if (id == 0) {
      // vatPercent = parseJson.charges[0].percent;
      vatPercent = 21;
    } else {
      vatPercent = parseJson.value;
    }

    let netTotal = document.querySelector('#netTotal').innerText;
    let chargesTotalDisplay = document.querySelector('#chargesTotalDisplay');
    let chargesTotal = document.querySelector('#chargesTotal');

    let taxPrice = parseFloat(netTotal) / 100 * parseFloat(vatPercent);

    chargesTotalDisplay.innerText = round(taxPrice, 2);
    chargesTotal.value = round(taxPrice, 2);
    return round(taxPrice, 2);

  }


  function setToDayDate() {
    let date = new Date();
    var day = String(date.getDate()).padStart(2, '0');
    var month = String(date.getMonth() + 1).padStart(2, '0');
    var years = date.getFullYear();
    let today = `${years}-${month}-${day}`;
    let invoiceDate = document.getElementById('Invoice_editView_fieldName_invoicedate');
    let paymentDay = String(date.getDate()).padStart(2, '0');
    paymentDay = paymentDay;
    invoiceDate.value = today;
  }

  function loadCheckInputFields() {
    $('[name="list"]').live("click", function (e) {
      disableUncheckFields(e.target.dataset.id);
    });
  }

  function loadItemsForInvoice() {
    $.ajax({
      type: "POST",
      url: "purchase/items.php",
      data: { id: 1 },
      dataType: 'JSON',
      success: function (history) {
        rows = document.querySelectorAll('#lineItemTab .lineItemRow');
        for (i = 1, len = rows.length; i <= len; i++) {
          autocomplete(document.getElementById(`productName${i}`), history, 'history');
        }
      }
    });
  }

  function addNewItemService(id) {

    document.getElementById("submit_new_entity").onclick = function () {
      let url = "purchase/addNewItemsServices.php";
      let title = $("[name='title']").val();
      let code = $("[name='code']").val();
      let type = $("[name='type']").val();

      if (title == '') {
        document.getElementById('req-title').classList.remove('hide');
      } else {
        document.getElementById('req-title').classList.add('hide');
      }

      if (code == '') {
        document.getElementById('req-code').classList.remove('hide');
      } else {
        document.getElementById('req-code').classList.add('hide');
      }


      if (title != '' && code != '') {

        $.ajax({
          type: "POST",
          url: url,
          data: { title: title, code: code, type: type },
          dataType: 'json',
          success: function (data) {
            if (data == 'Success') {
              $(`#productName${id}`).val(title);
              $(`#productName${id}`).attr('readonly', 'readonly');
              $("[name='title']").val('');
              $("[name='code']").val('');
              $('.dismiss').click();
            } else {
              document.getElementById('title-exists').classList.remove('hide');
              setTimeout(function () {
                document.getElementById('title-exists').classList.add('hide');
              }, 2000);
            }
          }
        });
      }


    }
  }

  function setNewRecordSettings() {
    document.getElementById('addService').onclick = () => {
      loadItemsForInvoice();
      setTimeout(function () {
        loadInvoiceDetails();
        loadPurchaseAjaxLists();
        let count = document.querySelectorAll('.lineItemRow').length;
        document.getElementById(`lineItemType${count}`).value = 'Item';
        document.getElementById(`hdnProductId${count}`).value = 121391;
        document.querySelector(`#fix-price${count}`).value = 0;
        document.getElementById(`qty${count}`).dataset.num = count;
        changePriceByQty();
      }, 10);

    }
  }

  function changePriceByQty() {

    setTimeout(() => {
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow');
      for (i = 1; i <= rows.length; i++) {
        document.getElementById(`qty${i}`).onkeyup = (e) => {

          let num = e.target.dataset.num;
          let count = e.target.value;
          let price = document.getElementById(`fix-price${num}`).value;
          let discountType = document.getElementById(`discount_type${num}`).value;
          let discountPercent = document.getElementById(`discount_percentage${num}`).value;
          let discountTotalPrice = document.querySelector(`#row${num}`).getElementsByTagName('td')[4].getElementsByTagName('span')[2].innerText;
          let discountPrice = parseFloat(discountTotalPrice.slice(1, -1));
          let discountTotal = document.getElementById(`discountTotal${num}`);
          let totalAfterDiscount = document.getElementById(`totalAfterDiscount${num}`);
          let discount;

          if (count != '' || count > 0) {
            let newPrice = price * count;
            let newPrice2;

            if (address.get('record') != null) {
              if (discountType == 'percentage') {
                discount = newPrice * discountPercent / 100;
                newPrice2 = newPrice - (newPrice * discountPercent / 100);
              } else if (discountType == 'amount') {
                discount = newPrice - (newPrice - discountPrice);
                newPrice2 = newPrice - discountPrice;
              } else {
                discount = 0;
                newPrice2 = newPrice;
              }
            } else {
              if (discountType == 'percentage') {
                discount = newPrice * discountPercent / 100;
                newPrice2 = newPrice - (newPrice * discountPercent / 100);
              } else if (discountType == 'amount') {
                discount = newPrice - (newPrice - discountPrice);
                newPrice2 = newPrice - discountPrice;
              } else {
                discount = 0;
                newPrice2 = newPrice;
              }
            }


            discountTotal.innerHTML = discount;
            totalAfterDiscount.innerHTML = newPrice2;
            document.getElementById(`productTotal${num}`).innerHTML = round(parseFloat(newPrice), 2);
            document.getElementById(`netPrice${num}`).innerHTML = round(parseFloat(newPrice2), 2);
            setPricesOnload();
          }

        }
      }
    }, 100);
  }


  function loadPriceByQty() {
    let rows = document.querySelectorAll('#lineItemTab .lineItemRow');
    for (i = 1; i <= rows.length; i++) {
      let count = document.getElementById(`qty${i}`).value;
      let price = document.getElementById(`fix-price${i}`).value;
      if (count != '' || count > 0) {
        let newPrice = price * count;
        document.getElementById(`productTotal${i}`).innerHTML = parseFloat(newPrice).toFixed(2);
        document.getElementById(`netPrice${i}`).innerHTML = parseFloat(newPrice).toFixed(2);
        document.getElementById(`totalAfterDiscount${i}`).innerHTML = parseFloat(newPrice).toFixed(2);
      }
    }
  }


  // $('.relationRealDelete').on('click', function (e) {
  //   e.stopImmediatePropagation();
  //   let data = e;
  //   let url = '/invoices/deleteInvoiceDebt.php';
  //   var message = app.vtranslate('LBL_DELETE_CONFIRMATION');
  //   app.helper.showConfirmationBox({ 'message': message }).then(
  //     function () {
  //       var element = jQuery(data.currentTarget);
  //       var row = element.closest('tr');
  //       var relatedRecordid = row.data('id');

  //       $.ajax({
  //         type: "POST",
  //         url: url,
  //         data: { relatedRecordid: relatedRecordid },
  //         dataType: 'json',
  //         success: function (res) {
  //           if (res = 'Success') {
  //             row.remove();
  //           } else {
  //             alert('Ištrinti nepavyko');
  //           }
  //         }
  //       });

  //     }

  //   );
  // });




  if (url.get('module') == 'Unpaidinvoices') {

    $(document).ready(function () {
      initCommentPopUp();
    });

    // total records show number
    $('.totalNumberOfRecords2').on('click', function () {
      $('.showTotalCountIcon2').addClass('hide');

      setTimeout(function () {
        $('#count_show').removeClass('hide');
      }, 1000);
    });

    $('[data-columnname="(account_id ; (Accounts) assigned_user_id)"]').html('<i class="fa fa-sort customsort"></i> Vadybininkas');
    $(document).ready(function () {

      $(function () {
        $('.pay_day').datepicker({
          dateFormat: "yy-mm-dd",
          autoclose: true
        });
      });

      DebtsFieldsCheker();

      // $(function () {
      //   $('.comments_popover').popover({
      //     container: 'body',
      //     html: true
      //   });
      //   console.log('load');
      // });

      $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
          // hide any open popovers when the anywhere else in the body is clicked
          if (!$(this).is('.popover') && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
          }
        });
      });

    });

    $(document).ajaxComplete(function () {
      $(function () {
        $('.pay_day').datepicker({
          dateFormat: "yy-mm-dd",
          autoclose: true
        });
      });
      DebtsFieldsCheker();
      initCommentPopUp();
    });
  }

  function initCommentPopUp() {
    $('.comments_popover').popover({
      container: 'body',
      html: true
    });
    $('.comments_popover').on('click', function (e) {
      $('.comments_popover').not(this).popover('hide');
      let invoiceId = e.target.parentElement.dataset.invoiceId;
      saveComment(invoiceId, e);
    });
  }

  function saveComment(invoiceid, event) {
    setTimeout(() => {
      let newestComments = document.getElementById('newestComments');
      let html = '<ul style="padding: 0 10px;">';
      $.ajax({
        type: "POST",
        url: '/invoices/getUnpaidInvoiceComment.php',
        data: { invoiceid: invoiceid },
        dataType: 'JSON',
        success: function (res) {
          if (res != null) {
            res.forEach(row => {
              html += `<li style="list-style: none; padding: 10px 0 10px 0;"><p style="color: blue;">${row.employee} <small style="color: #777;">${row.createdtime}</small></p>${row.commentcontent} </li>`;
            });
            html += '</ul>';
            if (newestComments) {
              newestComments.innerHTML = html;
            }
          }
        }
      });
    }, 500);
    setTimeout(() => {
      let counter = 0;
      $(document).off("click", ".saveComment").on("click", ".saveComment", function (e) {
        $(this).off('click');
        let comment = e.target.parentElement.getElementsByTagName('textarea')[0].value;
        let cloud = event.target;
        let userid = _USERMETA.id;
        $('.comments_popover').popover('hide');
        if (comment != '' && counter == 0) {
          cloud.setAttribute('style', 'color: red;');
          setTimeout(() => {
            $.ajax({
              type: "POST",
              url: '/invoices/saveUnpaidInvoiceComment.php',
              data: { invoiceid: invoiceid, comment: comment, userid: userid },
              success: function (res) {
              }
            });
          }, 1000);
        }
        counter++;
      });

    }, 500);
  }

  function DebtsFieldsCheker() {
    $('.listViewEntriesCheckBox').click(function () {
      if ($(this).is(':checked')) {
        insertCheckedId();
      } else {
        insertCheckedId();
      }
    });
  }

  function insertCheckedId() {
    let body = $('#listview-table tbody tr');
    let invoiceid = new Array();

    for (let i = 0; i < body.length; i++) {
      let check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
      if (check) {
        invoiceid.push($('#listview-table tbody tr').eq(i).data('id'));
      }
    }

    if (invoiceid.length > 0) {
      $('#pay_debt_btn').removeClass('btn-secondary');
      $('#pay_debt_btn').addClass('btn-success');
      $('#pay_debt_btn').attr('disabled', false);
      $('#selectedInvoices').val(invoiceid);
    } else {
      $('#pay_debt_btn').removeClass('btn-success');
      $('#pay_debt_btn').addClass('btn-secondary');
      $('#pay_debt_btn').attr('disabled', true);
      $('#selectedInvoices').val('');
    }

  }

  function addPaidDebtList() {
    let invoiceid = $('#selectedInvoices').val();
    getSelectedOrdersInfo(invoiceid);
  }

  function getSelectedOrdersInfo(invoiceid) {
    let date = $('#pay_day').val();
    let payMethod = $('#pay_method').val();
    let modalBody = document.querySelector('.modal-table');
    let modalSummary = document.querySelector('.modal-summary');
    let tableElements = '';
    let totalPrice;
    let numbers = new Array();

    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();
    today = `${yyyy}-${mm}-${dd}`;


    if (date == '') date = today;

    $.ajax({
      type: "POST",
      url: '/invoices/getSelectedOrdersInfo.php',
      data: { invoiceid: invoiceid },
      dataType: 'json',
      success: function (res) {
        if (res != null) {
          res.forEach(item => {
            tableElements += `
            <tr class="listViewContentHeader">
              <td class="listViewEntryValue">${item.subject}</td>               
              <td class="listViewEntryValue">${(item.accountname ? item.accountname : '--')}</td>              
              <td class="listViewEntryValue">${item.total}</td>              
            </tr>`;
            numbers.push(item.total);
            totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
          });
        }

        modalBody.innerHTML = tableElements;
        modalSummary.innerHTML = `
          <tr class="listViewContentHeader">
            <td class="listViewEntryValue">${date}</td>          
            <td class="listViewEntryValue">${payMethod}</td>          
            <td class="listViewEntryValue">${totalPrice.toFixed(2)}</td>
          </tr>`;
      }
    });

  }




  function paySelectedDebts() {
    let invoiceid = $('#selectedInvoices').val();
    let payMethod = $('#pay_method').val();
    let date = $('#pay_day').val();
    invoices = invoiceid.split(',');
    const user_id = _USERMETA.id;

    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();
    today = `${yyyy}-${mm}-${dd}`;


    if (date == '') date = today;

    $.ajax({
      type: "POST",
      url: '/invoices/paySelectedDebts.php',
      data: { invoiceid: invoiceid, payMethod: payMethod, date: date,user_id:user_id },
      beforeSend: function () {
        $('#wait2').show();
        $('#table_begin').css('opacity', '0.2');
        $('#table_begin2').css('opacity', '0.2');
      },
      success: function (res) {
        if (res == 'true') {
          setTimeout(() => {
            $('#wait2').hide();
            $('#table_begin').css('opacity', '1');
            $('#table_begin2').css('opacity', '1');
            $('[data-dismiss="modal"]').click();

            $('#pay_debt_btn').removeClass('btn-success');
            $('#pay_debt_btn').addClass('btn-secondary');
            $('#pay_debt_btn').attr('disabled', true);
            $('#selectedInvoices').val('');

            invoices.forEach(inv => {
              $(`[data-id="${inv}"]`).remove();
            });
          }, 2000);
        } else {
          alert('Įvyko klaida, praneškite puslapio administratoriui.');
        }

      }

    });
  }



  function searchByDebtOrPayed() {
    let form_length = $('.searchRow input:not([type="hidden"])').length;
    let form_array = new Array();
    let html = '';
    let fake_search = document.getElementById('fake_search');
    $('#messageBar').removeClass('hide');
    $('#wait3').show();

    for (let i = 0; i < form_length; i++) {
      let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
      let form_value = $('.searchRow input:not([type="hidden"])')[i].value;

      if (form_value != '') {
        form_array.push({ [form_key]: form_value });
      }
    }

    let assigned_users = $('.select2.listSearchContributor:not([multiple])');

    for (let i = 0; i < assigned_users.length; i++) {
      let form_key = '';
      if (i == 0) form_key = 'owner_name';
      if (i == 1) form_key = 'debt_managers_name';
      if (i == 2) form_key = 'managers_name';
      if (i == 3) form_key = 'client_manager_name';

      let form_value = assigned_users[i].innerText;
      if (form_value != '') {
        form_array.push({ [form_key]: form_value });
      }
    }

    $.ajax({
      type: "POST",
      url: '/invoices/filter_unpaid_invoices.php',
      data: { form_array: form_array },
      dataType: "JSON",
      success: function (response) {
        $('#real_search').addClass('hide');
        response.forEach(res => {
          html += `
        <tr class="listViewEntries" data-id="${res.invoiceid}"
        data-recordurl="index.php?module=Invoice&view=Detail&record=${res.invoiceid}&app=SALES&independent=true" id="Unpaidinvoices_listView_row_1">
        <td class="listViewRecordActions">
          <div class="table-actions">
          <span class="input"><input type="checkbox" value="${res.invoiceid}" class="listViewEntriesCheckBox" /></span>
          <span>
            <a class="action comments_popover" data-invoice-id="${res.invoiceid}"
                title="Rašyti komentarą" 
                rel="popover" 
                data-toggle="popover"               
                data-content='<textarea cols="50" rows="5"></textarea>
                <button class="btn btn-success btn-sm btn-block saveComment">Įrašyti</button>
                <div id="newestComments"></div>'>
                <i class="fa fa-comment" title="Rašyti komentarą" ${(res.commentcontent ? 'style="color: red;"' : '')}></i>
            </a>
          </span>

          <span><a class="quickView fa fa-eye icon action" data-app="SALES" title="Quick View"></a></span>          
          <span><a class="markStar fa icon action fa-star-o" title=" Click to follow"></a></span>
          <span class="more dropdown action"><span href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v icon"></i></span>
          <ul class="dropdown-menu">
            <li>
              <a data-id="${res.invoiceid}"
                href="index.php?module=Invoice&view=Detail&record=${res.invoiceid}&mode=showDetailViewByMode&requestMode=full&tab_label=Sąskaita Išsami informacija&app=SALES">Išsami
                informacija</a>
            </li>
            <li>
              <a data-id="${res.invoiceid}" href="javascript:void(0);"
                data-url="index.php?module=Invoice&view=Edit&record=${res.invoiceid}&app=SALES" name="editlink">Redaguoti</a>
            </li>
            <li>
              <a data-id="${res.invoiceid}" href="javascript:void(0);" class="deleteRecordButton">Pašalinti</a>
            </li>
          </ul>
        </span>          
          </div>
        </td>
      
        <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${res.debt}</span>
          </span>
        </td>
        <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${(res.payed != null ? res.payed : '')}</span>
          </span>
        </td>

        <td data-id="${res.invoiceid}" class="listViewEntryValue js-reference-display-value reminder_popover" data-toggle="popover" title="Priminti apie mokėjimą" data-content="`;
          if (res.remind_time) {
            html += ` <label style='font-size: 12px;'>Apie mokėjimą buvo priminta:</label> <span>  ${res.remind_count}  ${(res.remind_count == 1 ? 'kartą' : 'kartus')}
          </span><br><label style='font-size: 12px;'>Paskutinis priminimas:</label> <span> ${res.remind_time}</span>`;
          }
          html += `  <div class='form-group'>											
          <input type='email' class='form-control client_email' aria-describedby='emailHelp' placeholder='Įveskite el. pašto adresą'>
          <input type='hidden' class='invoiceid'>
          <input type='hidden' class='icon_id'>
          <small id='emailHelp' class='form-text text-muted'>Kliento kuriam norite priminti apie mokėjimą el. pašto adresas</small>
          <select id='emailType' class='form-control type' style='margin-top: 10px;'>
            <option value ='remind'>Priminimas</option>
            <option value='debt'>Skola</option>
          </select>
            <small id='emailType' class='form-text text-muted'>Priminimo tipas</small>
        </div>
         <button type='submit' class='btn btn-primary' onclick='clientReminderToPay(event);'>Siųsti</button>
        ">
        <span class="fieldValue js-reference-display-value">
          <span class="js-reference-display-value picklist-color picklist-453-Created" ${(res.late_payment > 0 ? 'style="color: red;"' : '')}>${(res.late_payment != null ? res.late_payment : '')}
          <i id="icon_${res.invoiceid}" title="${(!res.remind_time ? 'Priminti apie mokėjimą' : 'Priminta ' + res.remind_time)}" class="fa fa-bell js-reference-display-value" style="font-size: 15px; margin-left: 10px; color:#${(res.remind_time ? 'fd7e14' : '777')};"></i>
          </span>
        </span>
      </td>    
        <td class="listViewEntryValue" data-accountid="${res.accountid}" data-name="account_id" title="${res.account_id}" data-rawvalue="${res.accountid}"
        data-field-type="reference">
        <span class="fieldValue">
          <span class="value">
            <a class="js-reference-display-value" href="?module=Accounts&view=Detail&record=${res.accountid}" title="Klientai">${(res.account_id != null ? res.account_id : '')}</a>
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>

            
      <td class="listViewEntryValue" data-name="invoice_no" title="${res.invoice_no}" data-rawvalue="${res.invoice_no}"
      data-field-type="string">
      <span class="fieldValue">
        <span class="value">
          <a href="index.php?module=Invoice&view=Detail&record=${res.invoiceid}&app=SALES">${(res.invoice_no != null ? res.invoice_no : '')}</a>
        </span>
      </span>
    </td>


    <td class="listViewEntryValue" data-name="duedate" title="${res.invoicedate}" data-rawvalue="${res.invoicedate}"
    data-field-type="date">
    <span class="fieldValue">
      <span class="value">
        ${(res.invoicedate != null ? res.invoicedate : '')}
      </span>
    </span>
    <span class="hide edit"> </span>
  </td>
            
        <td class="listViewEntryValue" data-name="hdnGrandTotal" title="" data-rawvalue="" data-field-type="currency">
          <span class="fieldValue">
            <span class="value">
              ${(res.hdnGrandTotal != null ? res.hdnGrandTotal : '')}
            </span>
          </span>
        </td>
      
        <td class="listViewEntryValue" data-name="duedate" title="${res.duedate}" data-rawvalue="${res.duedate}"
          data-field-type="date">
          <span class="fieldValue">
            <span class="value">
              ${(res.duedate != null ? res.duedate : '')}
            </span>
          </span>
          <span class="hide edit"> </span>
        </td>
      
    

        <td class="listViewEntryValue" data-name="assigned_user_id2" title="${res.owner_name}" data-rawvalue="${res.smownerid}" data-field-type="owner">
        <span class="fieldValue">
          <span class="value">
           ${(res.owner_name != null ? res.owner_name : '')}
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>

      <td class="listViewEntryValue" data-name="assigned_user_id3" title="${res.debt_managers_name}" data-rawvalue="${res.cf_1657}" data-field-type="owner">
      <span class="fieldValue">
        <span class="value">
         ${(res.debt_managers_name != null ? res.debt_managers_name : '')}
        </span>
      </span>
      <span class="hide edit"> </span>
      </td>

      <td class="listViewEntryValue" data-name="assigned_user_id3" title="${res.managers_name}" data-rawvalue="${res.cf_1659}" data-field-type="owner">
      <span class="fieldValue">
        <span class="value">
         ${(res.managers_name != null ? res.managers_name : '')}
        </span>
      </span>
      <span class="hide edit"> </span>
    </td>

    <td class="listViewEntryValue" data-name="assigned_user_id3" title="${res.client_manager_name}" data-rawvalue="${res.cf_1655}" data-field-type="owner">
    <span class="fieldValue">
      <span class="value">
       ${(res.client_manager_name != null ? res.client_manager_name : '')}
      </span>
    </span>
    <span class="hide edit"> </span>
  </td>

      </tr>
        `;
        });

        fake_search.innerHTML = html;

        setTimeout(() => {
          fake_search.classList.remove('hide');
          $('#messageBar').addClass('hide');
          $('#wait3').hide();
          DebtsFieldsCheker();
        }, 1500);
      }
    });

  }

}