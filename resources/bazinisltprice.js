let siteUrl = new URLSearchParams(window.location.search);

$(document).ready(function(){


  if(siteUrl.get('module') == 'PriceBooks' && siteUrl.get('relatedModule') == 'Products'){


      function insertBazinisltPrice(){
        const originUrl = window.location.origin;
        let record = siteUrl.get('record');
        const url = `${originUrl}/price-algorithm/bazinislt-kaina.php?record=${record}`;    
        console.log(url);
          fetch(url)   
          .then((resp) => resp.json())
          .then(function(data) {   
                setBazinis(data);                              
            })
          .catch(function(err) {
              console.log(err);
          });   
      
      }


function setBazinis(data){
  const all = document.querySelector("tbody").getElementsByTagName("tr");
  for(let i = 0, len = all.length; i <= len-1; i++){
    // let res = document.querySelector("tbody").getElementsByTagName("td")[i].innerText;
    //  const td  =  document.querySelector("tbody");
    //  td.getElementsByTagName("td")[i].setAttribute("id", `laukelis-${i}`);
    
    const input = document.querySelector("tbody").getElementsByTagName("tr")[i].getElementsByTagName("td");
    const minWeight = input[2];
    const maxWeight = input[3];
    const minVolume = input[4];
    const maxVolume = input[5];
    const setBazinisPrice = input[12];

      for(let a = 0, len = data.length; a < len; a++){

          if(minWeight.innerHTML >= data[a].min_weight_kg && maxWeight.innerHTML <= data[a].max_weight_kg && minVolume.innerHTML >= data[a].min_volume_m3 && maxVolume.innerHTML <= data[a].max_volume_m3){          
            setBazinisPrice.innerText = `€${parseInt(data[a].listprice)}`;
            }           
          }
      } 
    
} 
}

$(document).ajaxComplete(function(){
  if(siteUrl.get('module') == 'PriceBooks' && siteUrl.get('relatedModule') == 'Products'){ 
   insertBazinisltPrice();         
  }
});


});
