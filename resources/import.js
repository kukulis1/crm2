$(document).ready(function () {
  $('#criticalOrders .load_company').on('keyup', function () {
    let id = $(this).data('id');
    let name = $(this).val();
    if (name.length >= 3) {
      getSuggestionsPricebook(id, name);
    }
  });
});



function getSuggestionsPricebook(id, name) {
  $.ajax({
    type: "POST",
    url: "/import/accountsSuggestions.php",
    data: { name: name },
    dataType: 'JSON',
    success: function (result) {
      autocompleteImportAccount(document.querySelector(`#criticalOrders [name="load_company${id}"`), result, id);
    }
  });
}

function autocompleteImportAccount(inp, arr, id) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  let accountname = arr.accountname;
  let accountid = arr.accountid;
  let customer_id = arr.customer_id;

  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function (e) {
    // $('#pricebook_name').removeClass('selected2');
    if (inp.value == '') {
      // $(`[name="load_company${id}"`).remove();
      $(`[name="accountid${id}"]`).val('');
      $(`[name="customer_id${id}"]`).val('');
    }
    var a, b, i, val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();

    if (!val) { return false; }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");

    a.setAttribute("class", "autocomplete-items-employee");

    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/

    if (accountname != null) {
      for (i = 0; i < accountname.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (((accountname[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
          // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          b.setAttribute('class', 'listas');
          /*make the matching letters bold:*/
          b.innerHTML = accountname[i].substr(0, val.length);
          b.innerHTML += accountname[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + accountname[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function (e) {
            /*insert the value for the autocomplete text field:*/
            inp.value = this.getElementsByTagName("input")[0].value;
            /*close the list of autocompleted values,
            (or any other open lists of autocompleted values:*/
            let errors = true;
            setTimeout(() => {
              $('#criticalOrders .accountid').each(function () {
                if (!$(this).val()) {
                  errors = false;
                }
              });
              if (errors) {
                $('#criticalOrders input[type="submit"]').prop('disabled', false);
              }
            }, 200);
            closeAllLists();
          });

          accid = document.createElement("input");
          accid.setAttribute('class', 'accid');
          accid.setAttribute('type', 'hidden');
          accid.setAttribute('value', `${accountid[i]}`);

          customerid = document.createElement("input");
          customerid.setAttribute('class', 'customerid');
          customerid.setAttribute('type', 'hidden');
          customerid.setAttribute('value', `${customer_id[i]}`);

          a.appendChild(b);
          b.appendChild(accid);
          b.appendChild(customerid);
          let accidClass = document.querySelectorAll('.accid');
          let customeridClass = document.querySelectorAll('.customerid');
          let listas = document.querySelectorAll('.listas');

          for (let i = 0; i < listas.length; i++) {
            listas[i].onclick = () => {
              // inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
              inp.classList.add('selected2');
              if (accountid !== undefined) {
                $(`[name="accountid${id}"]`).val(accidClass[i].value);
              }
              if (customer_id !== undefined) {
                $(`[name="customer_id${id}"]`).val(customeridClass[i].value);
              }
            }
          }

        }
        inp.onchange = () => {
          if (!inp.classList.contains('selected2')) {
            // inp.value = ''; jei nieko nepasirinkama is saraso istustina laukeli
          }
        }
      }
    }
  });




  /*execute a function presses a key on the keyboard:*/
  // inp.addEventListener("keydown", function (e) {
  //   var x = document.getElementById(this.id + "autocomplete-list");
  //   if (x) x = x.getElementsByTagName("div");
  //   if (e.keyCode == 40) {
  //     /*If the arrow DOWN key is pressed,
  //     increase the currentFocus variable:*/
  //     currentFocus++;
  //     /*and and make the current item more visible:*/
  //     addActive(x);
  //   } else if (e.keyCode == 38) { //up
  //     /*If the arrow UP key is pressed,
  //     decrease the currentFocus variable:*/
  //     currentFocus--;
  //     /*and and make the current item more visible:*/
  //     addActive(x);
  //   } else if (e.keyCode == 13) {
  //     /*If the ENTER key is pressed, prevent the form from being submitted,*/
  //     e.preventDefault();
  //     if (currentFocus > -1) {
  //       /*and simulate a click on the "active" item:*/
  //       if (x) x[currentFocus].click();
  //     }
  //   }
  // });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items-employee");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);

  });
}