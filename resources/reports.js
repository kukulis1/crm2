let report = new URLSearchParams(window.location.search);

if ((report.get('module') == 'Home' || report.get('module') == null) && document.querySelector('.nav-tabs') != null) {

  $(document).ready(function () {

    let modal = document.createElement('div');

    modal.innerHTML = `
    <style>
      .active-string {
        cursor:pointer;
      }

      .active-string:hover {
        color: #15c;
      }

      .modal-overflow{
        overflow-x: hidden;
        overflow-y: auto;
      }
    </style>
    <!-- main report -->
<div class="modal-overflow modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 1220px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reportModalLabel">Statistika</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="wait2" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
      <table>
        <tr>
          <td><button type="button" class="btn btn-primary" onclick="getDayReport('main');">Diena</button></td>
          <td><button type="button" class="btn btn-primary" onclick="getWeekReport('main');">Savaitė</button></td>
          <td><button type="button" class="btn btn-primary" onclick="getMonthReport('main');">Mėnuo</button></td>
        </tr>
      </table>

      <table class="table">
        <thead>
          <tr>
            <td>
              <input type="text" id="from" class="listSearchContributor inputElement date_input" placeholder="Nuo" style="width: 150px; margin-right:10px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
            </td>            
            <td>
              <input type="text" id="to" class="listSearchContributor inputElement date_input" placeholder="Iki" style="width: 150px; margin-right:10px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
            </td>
            <td>
              <button type="button" class="btn btn-default" style="height: 60px;" onclick="getReport('main');">Bendra apyvarta</button>                      
            </td>
            <td>
              <button type="button" class="btn btn-default" style="height: 60px;" onclick="getReport('client');">Klientai</button> 
            </td>
            <td>
              <button type="button" class="btn btn-default" style="height: 60px;" onclick="getReport('service');">Pardavimai</button>              
            </td> 
            <td>
            <button type="button" class="btn btn-default" style="height: 60px;" onclick="getReport('purchase');">Pirkimai</button>              
          </td>        
            <td>
              <button type="button" class="btn btn-default" style="height: 60px;" onclick="getReport('debt');">Pardavimo skolos</button>
            </td>
            <td>
            <button type="button" class="btn btn-default" style="height: 60px;" onclick="getReport('purchase-debt');">Pirkimų skolos</button>
          </td>
          </tr>   
        </thead>
        <tbody class="modal-table">
          </tbody>
        </table>
        <button type="button" id="export-button" class="btn btn-success hide" style="margin-bottom: 20px;">Export</button>
        <table class="table" id="table_begin">
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--Orders without docs -->
<div class="modal fade" id="withoutDocsModal" tabindex="-1" role="dialog" aria-labelledby="withoutDocsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 1100px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="withoutDocsModalLabel">Užsakymai be dokumentų statistika</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="wait3" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>

      <table>
        <tr>
          <td><button type="button" class="btn btn-primary" onclick="getDayReport('withOutDocs');">Diena</button></td>
          <td><button type="button" class="btn btn-primary" onclick="getWeekReport('withOutDocs');">Savaitė</button></td>
          <td><button type="button" class="btn btn-primary" onclick="getMonthReport('withOutDocs');">Mėnuo</button></td>
        </tr>
      </table>

      <table class="table">
        <thead>
          <tr>
            <td>
              <input type="text" id="from2" class="listSearchContributor inputElement date_input" placeholder="Nuo" style="width: 150px; margin-right:10px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
            </td>            
            <td>
              <input type="text" id="to2" class="listSearchContributor inputElement date_input" placeholder="Iki" style="width: 150px; margin-right:10px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
            </td>
            <td>
              <button type="button" class="btn btn-default" style="width: 200px;" onclick="getWithOutDocsReport('main');">Pateikti rezultatus</button>
            </td>
            <td>
              <button type="button" class="btn btn-default" style="width: 200px;" onclick="getWithOutDocsReport('base');">Bazinis kaina</button>
            </td>
            <td>
              <button type="button" class="btn btn-default" style="width: 200px;" onclick="getWithOutDocsReport('zero');">Nulinė kaina</button>
            </td>
          </tr>   
        </thead>
        <tbody class="modal-table">
          </tbody>
        </table>
        <table class="table" id="table_begin2">
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--Orders with docs -->
<div class="modal fade" id="withDocsModal" tabindex="-1" role="dialog" aria-labelledby="withDocsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 1100px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="withDocsModalLabel">Sąskaitų išrašymas statistika</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="wait4" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>

      <table>
        <tr>
          <td><button type="button" class="btn btn-primary" onclick="getDayReport('withDocs');">Diena</button></td>
          <td><button type="button" class="btn btn-primary" onclick="getWeekReport('withDocs');">Savaitė</button></td>
          <td><button type="button" class="btn btn-primary" onclick="getMonthReport('withDocs');">Mėnuo</button></td>
        </tr>
      </table>

      <table class="table">
        <thead>
          <tr>
            <td>
              <input type="text" id="from3" class="listSearchContributor inputElement date_input" placeholder="Nuo" style="width: 150px; margin-right:10px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
            </td>            
            <td>
              <input type="text" id="to3" class="listSearchContributor inputElement date_input" placeholder="Iki" style="width: 150px; margin-right:10px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
            </td>
            <td>
              <button type="button" class="btn btn-default" style="width: 200px;" onclick="getWithDocsReport('main');">Pateikti rezultatus</button>
            </td>
            <td>
            <button type="button" class="btn btn-default" style="width: 200px;" onclick="getWithDocsReport('base');">Bazinis kaina</button>
          </td>
          <td>
          <button type="button" class="btn btn-default" style="width: 200px;" onclick="getWithDocsReport('zero');">Nulinė kaina</button>
        </td>
          </tr>   
        </thead>
        <tbody class="modal-table">
          </tbody>
        </table>
        <table class="table" id="table_begin3">
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Detail lines -->
<div class="modal fade" id="detailLines" tabindex="-1" role="dialog" aria-labelledby="detailLinesTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 1100px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detailModalLongTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="wait5" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
      <table class="table">
        <thead>
          <tr>
            <th>Saskaitos numeris</th>
            <th>Saskaitos data</th>
            <th>Klientas</th>
            <th>Kaštų centras</th>
            <th>Paslauga</th>
            <th>Suma be PVM</th>
            <th>Suma su PVM</td>
            </tr>
          </thead>
          <tbody id="detailContent"></tbody>
        </table>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="restartPopUp('detailContent');">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

<!-- Orders detalization -->
<div class="modal fade" id="ordersDetalization" tabindex="-1" role="dialog" aria-labelledby="ordersDetalizationTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 1100px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detailModalLongTitle">Užsakymų detalizacija</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="wait6" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
      <table class="table">
        <thead>
          <tr>
            <th>Užsakymo numeris</th>
            <th>Užsakymo data</th>
            <th>Klientas</th>       
            <th>Suma</th>         
            </tr>
          </thead>
          <tbody id="OrdersContent"></tbody>
        </table>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="restartPopUp('OrdersContent');">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

`;


    let dashBoardContainer = document.querySelector('.dashBoardContainer');
    let tabs = document.querySelector('.nav-tabs');

    let li = document.createElement('li');
    let button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.setAttribute('class', 'btn btn-primary');
    button.setAttribute('data-toggle', 'modal');
    button.setAttribute('data-target', '#reportModal');
    button.innerText = 'Statistika';
    li.append(button);

    let li2 = document.createElement('li');
    let button2 = document.createElement('button');
    button2.setAttribute('type', 'button');
    button2.setAttribute('class', 'btn btn-primary');
    button2.setAttribute('data-toggle', 'modal');
    button2.setAttribute('data-target', '#withoutDocsModal');
    button2.innerText = 'Užsakymai be dok. stats';
    li2.append(button2);

    let li3 = document.createElement('li');
    let button3 = document.createElement('button');
    button3.setAttribute('type', 'button');
    button3.setAttribute('class', 'btn btn-primary');
    button3.setAttribute('data-toggle', 'modal');
    button3.setAttribute('data-target', '#withDocsModal');
    button3.innerText = 'Sąskaitų išrašymas stats';
    li3.append(button3);

    tabs.append(li);
    tabs.append(li2);
    tabs.append(li3);

    dashBoardContainer.append(modal);

    $(function () {
      $('.date_input').datepicker({
        dateFormat: "yy-mm-dd",
        autoclose: true,
        todayHighlight: true
      });
    });
  });
}

function initSearch(type) {
  $('.search_input').each(function () {
    $(this).keydown(function (e) {
      var key = e.which;
      if (key == 13 || key == 9) {
        getReport(type);
      }
    });
    $(this).change(function () {
      getReport(type);
    });
  });
}


function getMonthReport(which) {
  let date = new Date();
  let from = new Date(date.getFullYear(), date.getMonth(), 1);
  let to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  from = from.getFullYear() + "-" + String(from.getMonth() + 1).padStart(2, '0') + "-" + String(from.getDate()).padStart(2, '0');
  to = to.getFullYear() + "-" + String(to.getMonth() + 1).padStart(2, '0') + "-" + String(to.getDate()).padStart(2, '0');
  if (which == 'main') {
    document.getElementById('from').value = from;
    document.getElementById('to').value = to;
  } else if (which == 'withOutDocs') {
    document.getElementById('from2').value = from;
    document.getElementById('to2').value = to;
  }
  else if (which == 'withDocs') {
    document.getElementById('from3').value = from;
    document.getElementById('to3').value = to;
  }
}


function getDayReport(which) {
  let date = new Date();
  today = date.getFullYear() + "-" + String(date.getMonth() + 1).padStart(2, '0') + "-" + String(date.getDate()).padStart(2, '0');
  if (which == 'main') {
    document.getElementById('from').value = today;
    document.getElementById('to').value = today;
  } else if (which == 'withOutDocs') {
    document.getElementById('from2').value = today;
    document.getElementById('to2').value = today;
  } else if (which == 'withDocs') {
    document.getElementById('from3').value = today;
    document.getElementById('to3').value = today;
  }
}

function getWeekReport(which) {
  let startOfWeek = moment().startOf('week').toDate();
  let endOfWeek = moment().endOf('week').toDate();
  firstWeekDay = startOfWeek.getFullYear() + "-" + String(startOfWeek.getMonth() + 1).padStart(2, '0') + "-" + String(startOfWeek.getDate() + 1).padStart(2, '0');
  lastWeekDay = endOfWeek.getFullYear() + "-" + String(endOfWeek.getMonth() + 1).padStart(2, '0') + "-" + String(endOfWeek.getDate() + 1).padStart(2, '0');
  if (which == 'main') {
    document.getElementById('from').value = firstWeekDay;
    document.getElementById('to').value = lastWeekDay;
  } else if (which == 'withOutDocs') {
    document.getElementById('from2').value = firstWeekDay;
    document.getElementById('to2').value = lastWeekDay;
  } else if (which == 'withDocs') {
    document.getElementById('from3').value = firstWeekDay;
    document.getElementById('to3').value = lastWeekDay;
  }
}

function getReport(type) {
  let from = document.getElementById('from').value;
  let to = document.getElementById('to').value;
  if (from != '' && to != '') {
    if (type == 'purchase') {
      getPurchaseReportResponce(type, from, to);
    } else {
      getReportResponce(type, from, to);
    }
  }
}

function getPurchaseReportResponce(type, from, to) {
  let invoice_type, accountname, invoice_no, debt, payed, late_payment, duedate, proc, withoutVat, withVat, invoicedate, costcenter, employee, object;

  $('.search_input').each(function () {
    if ($(this).val() != '') {
      if ($(this).attr("id") == 'invoice_type') {
        invoice_type = $(this).val();
      }
      if ($(this).attr("id") == 'accountname') {
        accountname = $(this).val();
      }
      if ($(this).attr("id") == 'invoicedate') {
        invoicedate = $(this).val();
      }
      if ($(this).attr("id") == 'invoice_no') {
        invoice_no = $(this).val();
      }
      if ($(this).attr("id") == 'debt') {
        debt = $(this).val();
      }
      if ($(this).attr("id") == 'payed') {
        payed = $(this).val();
      }
      if ($(this).attr("id") == 'late_payment') {
        late_payment = $(this).val();
      }
      if ($(this).attr("id") == 'duedate') {
        duedate = $(this).val();
      }
      if ($(this).attr("id") == 'proc') {
        proc = $(this).val();
      }
      if ($(this).attr("id") == 'withoutVat') {
        withoutVat = $(this).val();
      }
      if ($(this).attr("id") == 'withVat') {
        withVat = $(this).val();
      }
      if ($(this).attr("id") == 'costcenter') {
        costcenter = $(this).val();
      }
      if ($(this).attr("id") == 'employee') {
        employee = $(this).val();
      }
      if ($(this).attr("id") == 'object') {
        object = $(this).val();
      }
    }
  });

  let table_begin = document.getElementById('table_begin');
  let tableElements = '';
  let totalPrice;
  let numbers = new Array();
  let debts = new Array();
  $('#wait2').show();

  $.ajax({
    type: "POST",
    url: '/reports/reportPurchase.php',
    data: { invoice_type: invoice_type, from: from, to: to, accountname: accountname, invoice_no: invoice_no, debt: debt, payed: payed, late_payment: late_payment, duedate: duedate, proc: proc, withoutVat: withoutVat, withVat: withVat, invoicedate: invoicedate, costcenter: costcenter, employee: employee, object: object },
    dataType: "JSON",
    success: function (res) {   

      if (res != null) {
        tableElements += `<tr>                              
                            <th>Paslauga</th>
                            <th>Tipas</th>                 
                            <th>Kaštų centras</th>
                            <th>Darbuotojas</th>
                            <th>Objektas</th>                           
                            <th>Pvm. %</th>                           
                            <th>Be Pvm.</th>
                            <th>Su Pvm.</th>
                          </tr>`;

        tableElements += `<tr>                             
                            <td><input type="text" id="accountname" class="listSearchContributor inputElement search_input" value="${(accountname) ? accountname : ''}"></td>
                             <td><select class="listSearchContributor inputElement search_input" id="invoice_type">
                                                            <option value="">---</option>
                                                            <option value="Debetinė" ${invoice_type == 'Debetinė' ? 'selected' : ''}>Debetinė</option>
                                                            <option value="Kreditinė" ${invoice_type == 'Kreditinė' ? 'selected' : ''}>Kreditinė</option>
                                                            <option value="Išankstinė" ${invoice_type == 'Išankstinė' ? 'selected' : ''}>Išankstinė</option>
                          </select></td>
                          <td><select class="listSearchContributor inputElement search_input" id="costcenter">
                                  <option value="">---</option>                                                             
                                  <option value="103914" ${costcenter == 103914 ? 'selected' : ''}>TRANSPORTAS</option> 
                                  <option value="103915" ${costcenter == 103915 ? 'selected' : ''}>EKSPEDIJAVIMAS</option> 
                                  <option value="103916" ${costcenter == 103916 ? 'selected' : ''}>SERVISAS</option> 
                                  <option value="103917" ${costcenter == 103917 ? 'selected' : ''}>PERKRAUSTYMAS</option> 
                                  <option value="103918" ${costcenter == 103918 ? 'selected' : ''}>VILNIAUS SANDĖLIS</option>
                                  <option value="103919" ${costcenter == 103919 ? 'selected' : ''}>KLAIPĖDOS SANDĖLIS</option> 
                                  <option value="103920" ${costcenter == 103920 ? 'selected' : ''}>KAUNO SANDĖLIS</option> 
                                  <option value="103921" ${costcenter == 103921 ? 'selected' : ''}>ADMINISTRACIJA</option>  
                                </select>                                                         
                          </td>                             
                            <td><input type="text" id="employee" class="listSearchContributor inputElement search_input" value="${(employee) ? employee : ''}"></td>
                            <td><input type="text" id="object" class="listSearchContributor inputElement search_input" value="${(object) ? object : ''}"></td>                             
                            <td><input type="text" id="proc" class="listSearchContributor inputElement search_input" value="${(proc) ? proc : ''}"></td>
                            <td><input type="text" id="withoutVat" class="listSearchContributor inputElement search_input" value="${(withoutVat) ? withoutVat : ''}"></td>
                            <td><input type="text" id="withVat" class="listSearchContributor inputElement search_input" value="${(withVat) ? withVat : ''}"></td/>
                          </tr>`;
        tableElements += `<tr>                           
                            <td></td>                                       
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="font-weight: 600;">Viso su PVM:</td>
                            <td style="font-weight: 600;" id="totalWithVat"></td>
                            </tr>`;

        let index = 1;
        res.forEach(item => {
          tableElements += `
            <tr class="listViewContentHeader">             
              <td data-toggle="modal" data-target="#detailLines" data-productid="${item.productid}" data-invoiceid="${item.invoiceids}" data-costcenterids="${item.costcenterids}" class="listViewEntryValue active-string">${(item.accountname) ? item.accountname : ''}</td> 
              <td class="listViewEntryValue"> ${(item.invoice_type ? item.invoice_type : '---')}</td>
              <td class="listViewEntryValue" ${(item.costcenter_tks_cost ? '' : 'title="Nepasirinkta"')}> ${(item.costcenter_tks_cost ? item.costcenter_tks_cost : '---')}</td>
              <td class="listViewEntryValue"> ${(item.employee ? item.employee : '---')}</td>
              <td class="listViewEntryValue"> ${(item.object ? item.object : '---')}</td>          
              <td class="listViewEntryValue" title='${(item.article) ? item.article : ''}'>${item.tax}%</td>                    
              <td class="listViewEntryValue">${(item.withoutVat) ? parseFloat(item.withoutVat).toFixed(2) : ''}</td>              
              <td class="listViewEntryValue">${(item.withVat) ? parseFloat(item.withVat).toFixed(2) : ''}</td>              
            </tr>`;
          numbers.push(item.withVat2);
          debts.push(item.debt2);
          index++;
        });
        if (typeof numbers !== 'undefined' && numbers.length > 0) {
          totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
          totalDebt = debts.map(Number).reduce((a, b) => { return a + b; });
        }

        tableElements += `
            <tr class="listViewContentHeader">              
              <td class="listViewEntryValue"></td>   
              <td class="listViewEntryValue"></td> 
              <td class="listViewEntryValue"></td>          
              <td class="listViewEntryValue"></td>        
              <td class="listViewEntryValue"></td>        
              <td class="listViewEntryValue"></td>        
              <td class="listViewEntryValue" style="font-weight: 600;">Viso su PVM:</td>              
              <td class="listViewEntryValue" style="font-weight: 600;">${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>              
            </tr>`;
      }

      setTimeout(() => {

        table_begin.innerHTML = tableElements;
        $('#wait2').hide();

        $('.date_input2').datepicker({
          dateFormat: "yy-mm-dd",
          autoclose: true,
          todayHighlight: true
        });

        initSearch(type);
        document.getElementById('totalWithVat').innerText = (totalPrice) ? totalPrice.toFixed(2) : '';
        initClickEvent2();
      }, 1000);

    }
  });



}


async function getReportResponce(type, from, to) {

  let invoice_type, accountname, invoice_no, debt, payed, late_payment, duedate, proc, withoutVat, withVat, invoicedate, costcenter, employee, object;

  $('.search_input').each(function () {
    if ($(this).val() != '') {
      if ($(this).attr("id") == 'invoice_type') {
        invoice_type = $(this).val();
      }
      if ($(this).attr("id") == 'accountname') {
        accountname = $(this).val();
      }
      if ($(this).attr("id") == 'invoicedate') {
        invoicedate = $(this).val();
      }
      if ($(this).attr("id") == 'invoice_no') {
        invoice_no = $(this).val();
      }
      if ($(this).attr("id") == 'debt') {
        debt = $(this).val();
      }
      if ($(this).attr("id") == 'payed') {
        payed = $(this).val();
      }
      if ($(this).attr("id") == 'late_payment') {
        late_payment = $(this).val();
      }
      if ($(this).attr("id") == 'duedate') {
        duedate = $(this).val();
      }
      if ($(this).attr("id") == 'proc') {
        proc = $(this).val();
      }
      if ($(this).attr("id") == 'withoutVat') {
        withoutVat = $(this).val();
      }
      if ($(this).attr("id") == 'withVat') {
        withVat = $(this).val();
      }
      if ($(this).attr("id") == 'costcenter') {
        costcenter = $(this).val();
      }
      if ($(this).attr("id") == 'employee') {
        employee = $(this).val();
      }
      if ($(this).attr("id") == 'object') {
        object = $(this).val();
      }
    }
  });

  let exporBtn = document.querySelector('#export-button');

  exporBtn.setAttribute('onclick', `window.location.href = 'vtlib/Vtiger/exel/reportServices.php?from=${(from ? from : '')}&to=${to ? to : ''}&type=${(accountname ? accountname : '')}&costcenter=${(costcenter ? costcenter : '')}&employee=${(employee ? employee : '')}&object=${(object ? object : '')}&proc=${(proc ? proc : '')}&withoutVat=${(withoutVat ? withoutVat : '')}&withVat=${(withVat ? withVat : '')}&invoice_type=${(invoice_type ? invoice_type : '')}'`);

  let table_begin = document.getElementById('table_begin');
  let tableElements = '';
  let totalPrice;
  let totalDebt;
  let numbers = new Array();
  let debts = new Array();
  $('#wait2').show();
  var responce = new Promise(function (resolve, reject) {
    $.ajax({
      type: "POST",
      url: '/reports/report.php',
      data: { invoice_type: invoice_type, type: type, from: from, to: to, accountname: accountname, invoice_no: invoice_no, debt: debt, payed: payed, late_payment: late_payment, duedate: duedate, proc: proc, withoutVat: withoutVat, withVat: withVat, invoicedate: invoicedate, costcenter: costcenter, employee: employee, object: object },
      dataType: 'JSON',
      success: function (res) {
     
        if (res != null) {
          if (type == 'service') {
            $('#export-button').removeClass('hide');
          } else {
            $('#export-button').addClass('hide');
          }  

          const type2 = type;

          if(type == 'purchase-debt') type = 'debt';

          tableElements += `<tr>                              
                              ${(type != 'main') ? `<th>${(type == 'client' || type == 'debt') ? 'Klientas' : 'Paslauga'}</th>` : '<th>Sąskaitos data</th>'}
                              ${(type == 'service') ? '<th>Tipas</th>' : ''}
                              ${(type == 'debt') ? '<th>Sąskaitos data</th>' : ''}  
                              ${(type == 'debt') ? '<th>Sąskaitos Nr.</th>' : ''}  
                              ${(type == 'debt') ? '<th>Skola</th>' : ''}   
                              ${(type == 'debt') ? '<th>Apmokėjimas</th>' : ''}   
                              ${(type == 'debt') ? '<th>Pradelsta</th>' : ''}   
                              ${(type == 'debt') ? '<th>Mokėjimo diena</th>' : ''}   
                              ${(type == 'service') ? '<th>Kaštų centras</th>' : ''}
                              ${(type == 'service') ? '<th>Darbuotojas</th>' : ''}
                              ${(type == 'service') ? '<th>Objektas</th>' : ''}
                              ${(type != 'main' && type2 != 'purchase-debt') ? '<th>Pvm. %</th>' : ''}
                              ${(type == 'main') ? '<th>Sąskaitų per dieną</th>' : ''}
                              <th>Be Pvm.</th>
                              <th>Su Pvm.</th>
                            </tr>`;

          tableElements += `<tr>                             
                              <td><input type="text" id="accountname" class="listSearchContributor inputElement search_input ${(type == 'main') ? 'date_input2' : ''}" ${(type == 'main') ? 'data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off"' : ''} value="${(accountname) ? accountname : ''}"></td>
                              ${(type == 'service') ? ` <td><select class="listSearchContributor inputElement search_input" id="invoice_type">
                                                              <option value="">---</option>
                                                              <option value="Debetinė" ${invoice_type == 'Debetinė' ? 'selected' : ''}>Debetinė</option>
                                                              <option value="Kreditinė" ${invoice_type == 'Kreditinė' ? 'selected' : ''}>Kreditinė</option>
                                                              <option value="Išankstinė" ${invoice_type == 'Išankstinė' ? 'selected' : ''}>Išankstinė</option>
                            </select></td>` : ''}
                              ${(type == 'service') ? `<td><select class="listSearchContributor inputElement search_input" id="costcenter">
                                                              <option value="">---</option>                                                             
                                                              <option value="103914" ${costcenter == 103914 ? 'selected' : ''}>TRANSPORTAS</option> 
                                                              <option value="103915" ${costcenter == 103915 ? 'selected' : ''}>EKSPEDIJAVIMAS</option> 
                                                              <option value="103916" ${costcenter == 103916 ? 'selected' : ''}>SERVISAS</option> 
                                                              <option value="103917" ${costcenter == 103917 ? 'selected' : ''}>PERKRAUSTYMAS</option> 
                                                              <option value="103918" ${costcenter == 103918 ? 'selected' : ''}>VILNIAUS SANDĖLIS</option>
                                                              <option value="103919" ${costcenter == 103919 ? 'selected' : ''}>KLAIPĖDOS SANDĖLIS</option> 
                                                              <option value="103920" ${costcenter == 103920 ? 'selected' : ''}>KAUNO SANDĖLIS</option> 
                                                              <option value="103921" ${costcenter == 103921 ? 'selected' : ''}>ADMINISTRACIJA</option>  
                                                           </select>                                                         
                                                          </td>` : ''}                              
                              ${(type == 'service') ? ` <td><input type="text" id="employee" class="listSearchContributor inputElement search_input" value="${(employee) ? employee : ''}"></td>` : ''}
                              ${(type == 'service') ? ` <td><input type="text" id="object" class="listSearchContributor inputElement search_input" value="${(object) ? object : ''}"></td>` : ''}
                              ${(type == 'debt') ? ` <td><input type="text" id="invoicedate" class="listSearchContributor inputElement search_input" value="${(invoicedate) ? invoicedate : ''}"></td>` : ''}
                              <td>${(type == 'debt') ? `<input type="text" id="invoice_no" class="listSearchContributor inputElement search_input" value="${(invoice_no) ? invoice_no : ''}">` :
              `<input type="text" id="proc" class="listSearchContributor inputElement search_input" value="${(proc) ? proc : ''}">`}</td>

                              <td>${(type == 'debt') ? `<input type="text" id="debt" class="listSearchContributor inputElement search_input" value="${(debt) ? debt : ''}">` :
              `<input type="text" id="withoutVat" class="listSearchContributor inputElement search_input" value="${(withoutVat) ? withoutVat : ''}">`}</td>
                       
              
          <td>${(type == 'debt') ? `<select id="payed" class="listSearchContributor inputElement search_input">
                                        <option value='visi' ${(payed == 'visi') ? 'selected' : ''}>Visi</option>
                                        <option value='paid' ${(payed == 'paid') ? 'selected' : ''}>Apmokėta</option>
                                        <option value='unpaid' ${(payed == 'unpaid') ? 'selected' : ''}>Neapmokėta</option>
                                    </select>` 
          : `<input type="text" id="withVat" class="listSearchContributor inputElement search_input" value="${(withVat) ? withVat : ''}">`}</td>



                              ${(type == 'debt') ? `<td><select id="late_payment" class="inputElement search_input" >
                                  <option value="" ${(late_payment == null) ? "selected" : ''}>---</option>
                                  <option value="1" ${(late_payment == 1) ? "selected" : ''}>Taip</option>
                                  <option value="0" ${(late_payment == 0) ? "selected" : ''}>Ne</option>
                                  </select>
                              </td>` : ''}
                              ${(type == 'debt') ? ` <td><input type="text" id="duedate" class="listSearchContributor inputElement search_input" value="${(duedate) ? duedate : ''}"></td>` : ''}
                              ${(type == 'debt' && type2 != 'purchase-debt') ? ` <td><input type="text" id="proc" class="listSearchContributor inputElement search_input" value="${(proc) ? proc : ''}"></td>` : ''}
                              ${(type == 'debt') ? `<td><input type="text" id="withoutVat" class="listSearchContributor inputElement search_input" value="${(withoutVat) ? withoutVat : ''}"></td>` : ''}
                              ${(type == 'debt') ? `<td><input type="text" id="withVat" class="listSearchContributor inputElement search_input" value="${(withVat) ? withVat : ''}"></td>` : ''}
                            </tr>`;
          tableElements += `<tr>
                              ${(type == 'debt') ? `<td></td>` : ''}
                              ${(type == 'service') ? '<td></td>' : ''}
                              ${(type == 'debt') ? `<td></td>` : ''}
                              ${(type == 'debt') ? `<td style="font-weight: 600;">Viso skola</td>` : ''}
                              ${(type == 'debt') ? `<td style="font-weight: 600;" id="totalDebt"></td>` : ''}
                              ${(type == 'debt') ? `<td></td>` : ''}
                              ${(type == 'debt' && type2 != 'purchase-debt') ? `<td></td>` : ''}
                              <td></td>
                              <td></td>
                              ${(type == 'service') ? '<td></td>' : ''}
                              ${(type == 'service') ? '<td></td>' : ''}
                              ${(type == 'service') ? '<td></td>' : ''}
                              <td style="font-weight: 600;">Viso su PVM:</td>
                              <td style="font-weight: 600;" id="totalWithVat"></td>
                              </tr>`;
                              
          let index = 1;
          res.forEach(item => {
            let totalWithVat = parseFloat(item.withVat);
            let totalWithoutVat = parseFloat(item.withoutVat);
            let totalDebt = parseFloat(item.debt); 
            let totalPaid = parseFloat(item.payed);     
            tableElements += `
              <tr class="listViewContentHeader">             
              ${(type != 'main') ? `<td ${(type == 'service') ? 'data-toggle="modal" data-target="#detailLines"' :''} 
              ${(type == 'service' ? `data-invoiceid="${item.invoiceids}" data-services="${item.services}" data-costcenterids="${item.costcenterids}"` : '')} class="listViewEntryValue${(type == 'service' ? ' active-string' : '')}">${(item.accountname) ? item.accountname : ''}</td>` : `<td class="listViewEntryValue">${item.invoicedate ? item.invoicedate : '---'}</td>`} 

              ${(type == 'service') ? `<td class="listViewEntryValue"> ${(item.invoice_type ? item.invoice_type : '---')}</td>` : ''}
              ${(type == 'service') ? `<td class="listViewEntryValue" ${(item.costcenter_tks_cost ? '' : 'title="Nepasirinkta"')}> ${(item.costcenter_tks_cost ? item.costcenter_tks_cost : '---')}</td>` : ''}
              ${(type == 'service') ? `<td class="listViewEntryValue"> ${(item.employee ? item.employee : '---')}</td>` : ''}
              ${(type == 'service') ? `<td class="listViewEntryValue"> ${(item.object ? item.object : '---')}</td>` : ''}
              ${(type == 'debt') ? `<td class="listViewEntryValue">${(item.invoicedate) ? item.invoicedate : ''}</td>` : ''}   
              ${(type == 'debt') ? `<td class="listViewEntryValue">${(item.invoice_no) ? `<a target="_blank" href="/index.php?module=${item.setype}&view=Detail&record=${item.invoiceid}">${item.invoice_no}</a>` : ''}</td>` : ''}   
              ${(type == 'debt') ? `<td class="listViewEntryValue">${totalDebt.toFixed(2)}</td>` : ''}   
              ${(type == 'debt') ? `<td class="listViewEntryValue">${(totalPaid) ? totalPaid.toFixed(2) : 0}</td>` : ''}   
              ${(type == 'debt') ? `<td class="listViewEntryValue">${(item.late_payment) ? item.late_payment : ''}</td>` : ''}   
              ${(type == 'debt') ? `<td class="listViewEntryValue">${(item.duedate) ? item.duedate : ''}</td>` : ''}   
              ${(type != 'main' && type2 != 'purchase-debt') ? `<td class="listViewEntryValue" title='${(item.article) ? item.article : ''}'>${item.tax}%</td>` : ''}         
              ${(type == 'main') ? `<td class="listViewEntryValue">${item.total_invoice}</td>` : ''}                  
                <td class="listViewEntryValue">${(totalWithoutVat) ? totalWithoutVat.toFixed(2) : ''}</td>              
                <td class="listViewEntryValue">${(totalWithVat) ? totalWithVat.toFixed(2) : ''}</td>              
              </tr>`;
            numbers.push(totalWithVat);
            debts.push(totalDebt);
            index++;
          });
          if (typeof numbers !== 'undefined' && numbers.length > 0) {
            totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
            totalDebt = debts.map(Number).reduce((a, b) => { return a + b; });
          }

          tableElements += `
              <tr class="listViewContentHeader">
                ${(type == 'debt') ? `<td class="listViewEntryValue"></td>` : ''} 
                ${(type == 'service') ? `<td class="listViewEntryValue"></td>` : ''}  
                <td class="listViewEntryValue" ${(type == 'debt') ? 'style="font-weight: 600;"' : ''}> ${(type == 'debt' ) ? 'Viso skola:' : ''}</td>    
                ${(type == 'debt') ? `<td class="listViewEntryValue" style="font-weight: 600;">${(totalDebt) ? totalDebt.toFixed(2) : ''}</td>` : ''}      
                ${(type == 'debt') ? `<td class="listViewEntryValue"></td>` : ''}   
                ${(type == 'debt') ? `<td class="listViewEntryValue"></td>` : ''}                    
                ${(type == 'debt') ? `<td class="listViewEntryValue"></td>` : ''}  
                ${(type == 'debt' || type == 'service') ? `<td class="listViewEntryValue"></td>` : ''}  
                ${(type == 'service') ? `<td class="listViewEntryValue"></td>` : ''}  
                ${(type == 'service') ? `<td class="listViewEntryValue"></td>` : ''}  
           
                ${ (type2 != 'purchase-debt') ? '<td class="listViewEntryValue"></td>' : ''}          
                <td class="listViewEntryValue" style="font-weight: 600;">Viso su PVM:</td>              
                <td class="listViewEntryValue" style="font-weight: 600;">${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>              
              </tr>`;


              setTimeout(() => {

                table_begin.innerHTML = tableElements;
                $('#wait2').hide();
      
                if (type == 'debt') {
                  document.getElementById('totalDebt').innerText = (totalDebt) ? totalDebt.toFixed(2) : '';
                } else if (type == 'main') {
                  $('.date_input2').datepicker({
                    dateFormat: "yy-mm-dd",
                    autoclose: true,
                    todayHighlight: true
                  });
                }
                initSearch(type2);
                document.getElementById('totalWithVat').innerText = (totalPrice) ? totalPrice.toFixed(2) : '';
                if (type2 == 'service') {
                  initClickEvent();
                }
              }, 1000);
        }


      }
    });
  });
}

function initClickEvent() {
  $('#table_begin .listViewEntryValue:first-child').on('click', function () {
    let invoiceid = $(this).data('invoiceid');
    let service = $(this).data('services');
    let costcenter = $(this).data('costcenterids');
    let title = $(this).html();
    let detailContent = document.querySelector('#detailContent');
    let tableElements = '';
    let totalPrice;
    let numbers = new Array();
    $('#detailModalLongTitle').html(title);
    $('#wait5').show();
    $.ajax({
      type: "POST",
      url: '/reports/detailLines.php',
      data: { invoiceid: invoiceid, service: service, costcenter: costcenter, },
      dataType: 'JSON',
      success: function (res) {
        if (res != null) {
          res.forEach(item => {
            tableElements += `
              <tr class="listViewContentHeader">          
                <td class="listViewEntryValue">${(item.invoice_no) ? `<a target="_blank" href="/index.php?module=Invoice&view=Detail&record=${item.invoiceid}">${item.invoice_no}</a>` : ''}</td>              
                <td class="listViewEntryValue">${(item.invoicedate) ? item.invoicedate : ''}</td>              
                <td class="listViewEntryValue">${(item.accountname) ? item.accountname : ''}</td>         
                <td class="listViewEntryValue">${(item.costcenter_tks_cost) ? item.costcenter_tks_cost : ''}</td>         
                <td class="listViewEntryValue">${(item.service_name) ? item.service_name : ''}</td>         
                <td class="listViewEntryValue">${(item.listprice) ? item.listprice : ''}</td>         
                <td class="listViewEntryValue">${(item.listprice_vat) ? item.listprice_vat : ''}</td>         
              </tr>`;
            numbers.push(item.withVat2);
          });

          if (typeof numbers !== 'undefined' && numbers.length > 0) {
            totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
          }

          tableElements += `
            <tr class="listViewContentHeader">
              <td class="listViewEntryValue"></td>        
              <td class="listViewEntryValue"></td>        
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue" style="font-weight: 600;">Viso: ${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>              
            </tr>`;

          setTimeout(() => {
            $('#wait5').hide();
            detailContent.innerHTML = tableElements;
          }, 1000);

        }
      }
    });

  });
}


function initClickEvent2() {
  $('#table_begin .listViewEntryValue:first-child').on('click', function () {
    let invoiceid = $(this).data('invoiceid');
    let costcenter = $(this).data('costcenterids');
    let productid = $(this).data('productid');
    let title = $(this).html();
    let detailContent = document.querySelector('#detailContent');
    let tableElements = '';
    let totalPrice;
    let numbers = new Array();
    $('#detailModalLongTitle').html(title);
    $('#wait5').show();
    $.ajax({
      type: "POST",
      url: '/reports/detailLinesPurchase.php',
      data: { invoiceid: invoiceid, costcenter: costcenter, productid: productid },
      dataType: 'JSON',
      success: function (res) {
        console.log(res);
        if (res != null) {
          res.forEach(item => {
            tableElements += `
              <tr class="listViewContentHeader">          
                <td class="listViewEntryValue">${(item.purchaseorder_no) ? `<a target="_blank" href="/index.php?module=PurchaseOrder&view=Detail&record=${item.purchaseorderid}">${item.purchaseorder_no}</a>` : ''}</td>              
                <td class="listViewEntryValue">${(item.invoicedate) ? item.invoicedate : ''}</td>              
                <td class="listViewEntryValue">${(item.vendorname) ? item.vendorname : ''}</td>         
                <td class="listViewEntryValue">${(item.costcenter_tks_cost) ? item.costcenter_tks_cost : ''}</td>         
                <td class="listViewEntryValue">${(item.service_name) ? item.service_name : ''}</td>         
                <td class="listViewEntryValue">${(item.listprice) ? item.listprice : ''}</td>         
                <td class="listViewEntryValue">${(item.listprice_vat) ? item.listprice_vat : ''}</td>         
              </tr>`;
            numbers.push(item.withVat2);
          });

          if (typeof numbers !== 'undefined' && numbers.length > 0) {
            totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
          }

          tableElements += `
            <tr class="listViewContentHeader">
              <td class="listViewEntryValue"></td>        
              <td class="listViewEntryValue"></td>        
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue"></td>              
              <td class="listViewEntryValue" style="font-weight: 600;">Viso: ${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>              
            </tr>`;

          setTimeout(() => {
            $('#wait5').hide();
            detailContent.innerHTML = tableElements;
          }, 1000);

        }
      }
    });

  });
}

function show_orders(thisInstance) {    
    let salesorders = $(thisInstance).data('salesorders');
    let detailContent = document.querySelector('#OrdersContent');
    let tableElements = '';
    $('#wait6').show();
    $.ajax({
      type: "POST",
      url: '/reports/detailOrders.php',
      data: { salesorders: salesorders },
      dataType: 'JSON',
      success: function (res) {
        console.log(res);
        if (res != null) {
          res.forEach(item => {
            tableElements += `
              <tr class="listViewContentHeader">          
                <td class="listViewEntryValue">${(item.salesorderid) ? `<a target="_blank" href="/index.php?module=SalesOrder&view=Detail&record=${item.salesorderid}">${(item.shipment_code) ? item.shipment_code : 'Užsakymas'}</a>` : '---'}</td>              
                <td class="listViewEntryValue">${(item.createdtime) ? item.createdtime : '---'}</td>              
                <td class="listViewEntryValue">${(item.accountname) ? item.accountname : '---'}</td>                    
                <td class="listViewEntryValue">${(item.total) ? item.total : ''}</td>         
              </tr>`;          
          });


          setTimeout(() => {
            $('#wait6').hide();
            detailContent.innerHTML = tableElements;
          }, 1000);

        }
      }
    });


}


function restartPopUp(content) {
  document.querySelector(`#${content}`).innerHTML = '';
}


function getWithOutDocsReport(type) {
  let from = document.getElementById('from2').value;
  let to = document.getElementById('to2').value;
  if (from != '' && to != '') {
    getWithOutDocsReportResponce(type, from, to);
  }
}


async function getWithOutDocsReportResponce(type, from, to) {

  let table_begin = document.getElementById('table_begin2');
  let tableElements = '';
  let totalPrice;
  let totalOrders;
  let numbers = new Array();
  let orders = new Array();
  $('#wait3').show();

  var responce2 = new Promise(function (resolve, reject) {
    $.ajax({
      type: "POST",
      url: '/reports/withOutDocsStats.php',
      data: { type: type, from: from, to: to },
      dataType: 'JSON',
      success: function (res) {
        console.log(res);
        if (res != null) {
          tableElements += `<tr>      
        <th>Užsakymo data</th>
        <th>Užsakymų skaičius</th>
        <th>Suma</th>
        </tr>`;

          res.forEach(item => {
            tableElements += `
            <tr class="listViewContentHeader">          
              <td class="listViewEntryValue">${(item.order_date) ? item.order_date : ''}</td>              
              <td class="listViewEntryValue active-string" onclick="show_orders(this);" data-toggle="modal" data-target="#ordersDetalization" data-salesorders=${item.salesorders}>${(item.count) ? item.count : ''}</td>              
              <td class="listViewEntryValue">${(item.total) ? item.total : ''}</td>         
            </tr>`;
            numbers.push(item.total);
            orders.push(item.count);
          });



          if (typeof numbers !== 'undefined' && numbers.length > 0) {
            totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
          }

          if (typeof orders !== 'undefined' && orders.length > 0) {
            totalOrders = orders.map(Number).reduce((a, b) => { return a + b; });
          }

          tableElements += `
          <tr class="listViewContentHeader">
            <td class="listViewEntryValue"></td>        
            <td class="listViewEntryValue" style="font-weight: 600;">Viso: ${(totalOrders) ? totalOrders : ''}</td>              
            <td class="listViewEntryValue" style="font-weight: 600;">Viso: ${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>              
          </tr>`;


          setTimeout(() => {
            table_begin.innerHTML = tableElements;
            $('#wait3').hide();
          }, 1000);

        }
      }
    });

  });

}

function getWithDocsReport(type) {
  let from = document.getElementById('from3').value;
  let to = document.getElementById('to3').value;
  if (from != '' && to != '') {
    getWithDocsReportResponce(type, from, to);
  }
}


async function getWithDocsReportResponce(type, from, to) {
  let table_begin = document.getElementById('table_begin3');
  let tableElements = '';
  let totalPrice;
  let totalOrders;
  let numbers = new Array();
  let orders = new Array();
  $('#wait4').show();

  var responce2 = new Promise(function (resolve, reject) {
    $.ajax({
      type: "POST",
      url: '/reports/withDocsStats.php',
      data: { type: type, from: from, to: to },
      dataType: 'JSON',
      success: function (res) {
        if (res != null) {
          tableElements += `<tr>      
        <th>Užsakymo data</th>
        <th>Užsakymų skaičius</th>
        <th>Suma</th>
        </tr>`;

          res.forEach(item => {
            tableElements += `
            <tr class="listViewContentHeader">          
              <td class="listViewEntryValue">${(item.order_date) ? item.order_date : ''}</td>              
              <td class="listViewEntryValue">${(item.count) ? item.count : ''}</td>              
              <td class="listViewEntryValue">${(item.total) ? item.total : ''}</td>              
            </tr>`;
            numbers.push(item.total);
            orders.push(item.count);
          });


          if (typeof numbers !== 'undefined' && numbers.length > 0) {
            totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
          }

          if (typeof orders !== 'undefined' && orders.length > 0) {
            totalOrders = orders.map(Number).reduce((a, b) => { return a + b; });
          }


          tableElements += `
          <tr class="listViewContentHeader">
            <td class="listViewEntryValue"></td>        
            <td class="listViewEntryValue" style="font-weight: 600;">Viso: ${(totalOrders) ? totalOrders : ''}</td>              
            <td class="listViewEntryValue" style="font-weight: 600;">Viso: ${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>              
          </tr>`;


          setTimeout(() => {

            table_begin.innerHTML = tableElements;
            $('#wait4').hide();

          }, 1000);

        }
      }
    });

  });

}
