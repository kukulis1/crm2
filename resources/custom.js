window.onload = function() {

  const tables =  document.querySelector('[data-block="Kainyno zonos"]');
  tables.setAttribute("id", "tablas");

  const maintable = document.getElementById("tablas").children[2];

  const thead = document.createElement("thead");
  maintable.append(thead);
  tbody = maintable.children[0];
  maintable.insertBefore(thead, tbody);

  
      let firstTr = tbody.children[0];
      let secondTr = tbody.children[1];
      let thirdTr = tbody.children[2];
      let fourtTr = tbody.children[3];
      let fifthtTr = tbody.children[4];
      let sixTr = tbody.children[5];
      let sevenTr = tbody.children[6];
      let eightTr = tbody.children[7];
      let nineTr = tbody.children[8];
      let tenTr = tbody.children[9];
      let elevenTr = tbody.children[10];
      let twelveTr = tbody.children[11];

    

      maintable.setAttribute("class", "container");


  firstTr.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 
  fifthtTr.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 
  nineTr.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 

  fifthtTr.remove();
  nineTr.remove();

  firstTr.append(fifthtTr.children[1]);
  firstTr.append(nineTr.children[1]);

  thead.append(firstTr);

  thead.children[0].children[0].remove();
  thead.children[0].children[1].remove();
  thead.children[0].children[1].remove();


  secondTr.children[0].remove();
  secondTr.children[1].remove();

  thirdTr.children[0].remove();
  thirdTr.children[1].remove();

  fourtTr.children[0].remove();
  fourtTr.children[1].remove();

  sixTr.children[0].remove();
  sixTr.children[1].remove();

  sevenTr.children[0].remove();
  sevenTr.children[1].remove();

  eightTr.children[0].remove();
  eightTr.children[1].remove();

  tenTr.children[0].remove();
  tenTr.children[1].remove();

  elevenTr.children[0].remove();
  elevenTr.children[1].remove();

  twelveTr.children[0].remove();
  twelveTr.children[1].remove();


  secondTr.remove();
  const maintrToAppend = document.createElement("tr");
  let maintd = document.createElement("td");
  maintd.append(secondTr.children[0].children[1]);
  maintd.append(secondTr.children[1].children[1]);

  maintd.append(thirdTr.children[0].children[1]);
  maintd.append(thirdTr.children[1].children[1]);

  maintd.append(fourtTr.children[0].children[1]);
  maintd.append(fourtTr.children[1].children[1]);  

  thirdTr.remove();
  let maintd2 = document.createElement("td");
  maintd2.append(sixTr.children[0].children[1]);
  maintd2.append(sixTr.children[1].children[1]);

  maintd2.append(sevenTr.children[0].children[1]);
  maintd2.append(sevenTr.children[1].children[1]);

  maintd2.append(eightTr.children[0].children[1]);
  maintd2.append(eightTr.children[1].children[1]);

  nineTr.remove();
  let maintd3 = document.createElement("td");
  maintd3.append(tenTr.children[0].children[1]);
  maintd3.append(tenTr.children[1].children[1]);

  maintd3.append(elevenTr.children[0].children[1]);
  maintd3.append(elevenTr.children[1].children[1]);

  maintd3.append(twelveTr.children[0].children[1]);
  maintd3.append(twelveTr.children[1].children[1]);

  maintrToAppend.append(maintd);
  maintrToAppend.append(maintd2);
  maintrToAppend.append(maintd3);
  tbody.append(maintrToAppend);
  tbody.insertBefore(maintrToAppend, tbody.firstChild);




  const fromCity = tbody.children[0].children[0].children[0];
  const toCity = tbody.children[0].children[0].children[1];

  const fromDistrict = tbody.children[0].children[0].children[2];
  const toDistrict = tbody.children[0].children[0].children[3];

  const fromGeozone = tbody.children[0].children[0].children[4];
  const toGeozone = tbody.children[0].children[0].children[5];


  const fromCity2 = tbody.children[0].children[1].children[0];
  const toCity2 = tbody.children[0].children[1].children[1];

  const fromDistrict2 = tbody.children[0].children[1].children[2];
  const toDistrict2 = tbody.children[0].children[1].children[3];

  const fromGeozone2 = tbody.children[0].children[1].children[4];
  const toGeozone2 = tbody.children[0].children[1].children[5];


  
  const fromCity3 = tbody.children[0].children[2].children[0];
  const toCity3 = tbody.children[0].children[2].children[1];

  const fromDistrict3 = tbody.children[0].children[2].children[2];
  const toDistrict3 = tbody.children[0].children[2].children[3];

  const fromGeozone3 = tbody.children[0].children[2].children[4];
  const toGeozone3 = tbody.children[0].children[2].children[5];



  fromCity.remove();
  toCity.remove();
  fromDistrict.remove();
  toDistrict.remove();
  fromGeozone.remove();
  toGeozone.remove();
  
  fromCity2.remove();
  toCity2.remove();
  fromDistrict2.remove();
  toDistrict2.remove();
  fromGeozone2.remove();
  toGeozone2.remove();

  fromCity3.remove();
  toCity3.remove();
  fromDistrict3.remove();
  toDistrict3.remove();
  fromGeozone3.remove();
  toGeozone3.remove();



  // pirmas

  const fromCityUse = document.createElement("div");
  fromCityUse.setAttribute("id", "show");
  fromCityUse.appendChild(fromCity);

  const toCityUse = document.createElement("div");
  toCityUse.setAttribute("id", "show");
  toCityUse.appendChild(toCity);


  const fromDistrictUse = document.createElement("div");
  fromDistrictUse.setAttribute("id", "show");
  fromDistrictUse.append(fromDistrict);

  const toDistrictUse = document.createElement("div");
  toDistrictUse.setAttribute("id", "show");
  toDistrictUse.append(toDistrict);


  const fromGeoZoneUse = document.createElement("div");
  fromGeoZoneUse.setAttribute("id", "show");
  fromGeoZoneUse.append(fromGeozone);

  const toGeoZoneUse = document.createElement("div");
  toGeoZoneUse.setAttribute("id", "show");
  toGeoZoneUse.append(toGeozone);

  const maintr = document.createElement("tr");
  const preparedTd = document.createElement("td");
  const preparedTd2 = document.createElement("td");
  const preparedTd3 = document.createElement("td");
  maintr.setAttribute("id", "maintr");
  maintr.append(preparedTd);
  maintr.append(preparedTd2);
  maintr.append(preparedTd3);
  tbody.append(maintr);
  tbody.insertBefore(maintr, tbody.firstChild);


  const textIs = document.innerText = 'Iš ';
  const textI = document.innerText = 'Į ';
  const textFrom = document.createElement("div");
  textFrom.setAttribute("id","show-text");
  textFrom.append(textIs);

  const textTo = document.createElement("div");
  textTo.setAttribute("id","show-text");
  textTo.append(textI);



  // Antras

  const fromCityUse2 = document.createElement("div");
  fromCityUse2.setAttribute("align", "center");
  fromCityUse2.setAttribute("id", "show");
  fromCityUse2.appendChild(fromCity2);

  const toCityUse2 = document.createElement("div");
  toCityUse2.setAttribute("id", "show");
  toCityUse2.appendChild(toCity2);


  const fromDistrictUse2 = document.createElement("div");
  fromDistrictUse2.setAttribute("id", "show");
  fromDistrictUse2.append(fromDistrict2);

  const toDistrictUse2 = document.createElement("div");
  toDistrictUse2.setAttribute("id", "show");
  toDistrictUse2.append(toDistrict2);


  const fromGeoZoneUse2 = document.createElement("div");
  fromGeoZoneUse2.setAttribute("id", "show");
  fromGeoZoneUse2.append(fromGeozone2);

  const toGeoZoneUse2 = document.createElement("div");
  toGeoZoneUse2.setAttribute("id", "show");
  toGeoZoneUse2.append(toGeozone2);



  const textIs2 = document.innerText = 'Iš ';
  const textI2 = document.innerText = 'Į ';

  const textFrom2 = document.createElement("div");
  textFrom2.setAttribute("id","show-text");
  textFrom2.append(textIs2);

  const textTo2 = document.createElement("div");
  textTo2.setAttribute("id","show-text");
  textTo2.append(textI2);


  // Trecias

  const fromCityUse3 = document.createElement("div");
  fromCityUse3.setAttribute("id", "show");
  fromCityUse3.appendChild(fromCity3);

  const toCityUse3 = document.createElement("div");
  toCityUse3.setAttribute("id", "show");
  toCityUse3.appendChild(toCity3);


  const fromDistrictUse3 = document.createElement("div");
  fromDistrictUse3.setAttribute("id", "show");
  fromDistrictUse3.append(fromDistrict3);

  const toDistrictUse3 = document.createElement("div");
  toDistrictUse3.setAttribute("id", "show");
  toDistrictUse3.append(toDistrict3);


  const fromGeoZoneUse3 = document.createElement("div");
  fromGeoZoneUse3.setAttribute("id", "show");
  fromGeoZoneUse3.append(fromGeozone3);

  const toGeoZoneUse3 = document.createElement("div");
  toGeoZoneUse3.setAttribute("id", "show");
  toGeoZoneUse3.append(toGeozone3);



  const textIs3 = document.innerText = 'Iš ';
  const textI3 = document.innerText = 'Į ';
  const textFrom3 = document.createElement("div");
  textFrom3.setAttribute("id","show-text");
  textFrom3.append(textIs3);

  const textTo3 = document.createElement("div");
  textTo3.setAttribute("id","show-text");
  textTo3.append(textI3);


  thead.getElementsByTagName("select")[0].onchange = function() {
      let index = this.selectedIndex;
      let inputText = this.children[index].innerHTML.trim();
      let getShow = tbody.children[1]; 

      if(inputText == "Miestas - Miestas"){
        if(getShow != null) {
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }  
        maintr.firstChild.append(textFrom);
        maintr.firstChild.append(fromCityUse);
        maintr.firstChild.append(textTo);
        maintr.firstChild.append(toCityUse);     
      
      
      }else if(inputText == "Miestas - Rajonas"){
        if(getShow != null){          
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }
        maintr.firstChild.append(textFrom);
        maintr.firstChild.append(fromCityUse);
        maintr.firstChild.append(textTo);     
        maintr.firstChild.append(toDistrictUse);  
        
      }else if(inputText == "Rajonas - Miestas"){
        if(getShow != null) {       
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }
        maintr.firstChild.append(textFrom);
        maintr.firstChild.append(fromDistrictUse);
        maintr.firstChild.append(textTo);  
        maintr.firstChild.append(toCityUse);     
    
      }else if(inputText == "Rajonas - Rajonas"){
        if(getShow != null) {
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }
        maintr.firstChild.append(textFrom);
        maintr.firstChild.append(fromDistrictUse);  
        maintr.firstChild.append(textTo);
        maintr.firstChild.append(toDistrictUse);
        
      }else if(inputText == "Geozona - Geozona"){
        if(getShow != null) {
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }
        maintr.firstChild.append(textFrom);
        maintr.firstChild.append(fromGeoZoneUse);  
        maintr.firstChild.append(textTo);
        maintr.firstChild.append(toGeoZoneUse);  
        
      }else if(inputText == "Miestas - Geozona"){
        if(getShow != null) {
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }
        maintr.firstChild.append(textFrom);
        maintr.firstChild.append(fromCityUse);  
        maintr.firstChild.append(textTo);
        maintr.firstChild.append(toGeoZoneUse);  

      }else if(inputText == "Geozona - Miestas"){
        if(getShow != null) {
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }
        maintr.firstChild.append(textFrom);
        maintr.firstChild.append(fromGeoZoneUse);  
        maintr.firstChild.append(textTo);
        maintr.firstChild.append(toCityUse);  

      }else if(inputText == "LTU - Miestas"){
        if(getShow != null) {
          fromCityUse.remove();
          toCityUse.remove();
          fromDistrictUse.remove();
          toDistrictUse.remove();
          fromGeoZoneUse.remove();
          toGeoZoneUse.remove();
          textTo.remove();
          textFrom.remove();
        }
        maintr.firstChild.append(textTo);
        maintr.firstChild.append(toCityUse);

      }else if(inputText == "LTU - LTU"){
        fromCityUse.remove();
        toCityUse.remove();
        fromDistrictUse.remove();
        toDistrictUse.remove();
        fromGeoZoneUse.remove();
        toGeoZoneUse.remove();
        textTo.remove();
        textFrom.remove();
        
      }else{
        fromCityUse.remove();
        toCityUse.remove();
        fromDistrictUse.remove();
        toDistrictUse.remove();
        fromGeoZoneUse.remove();
        toGeoZoneUse.remove();
        textTo.remove();
        textFrom.remove();
      }  


      // console.log(tbody);  


      let listass = document.getElementsByClassName("select2-choices")[0]; 
      listass.onclick = function() {
      
      let listItemRemoveClass = document.querySelectorAll(".select2-drop-multi ul")[1];
      
      listItemRemoveClass.onclick = setTimeout(removeClass, 1);

      function removeClass(){       
        let pickList = document.querySelectorAll(".select2-drop-multi ul li")[1];  
        pickList.classList.remove("select2-selected");
      } 
     
    }
      
    }
    
    
    thead.getElementsByTagName("select")[1].onchange = function() {
      let index = this.selectedIndex;
      let inputText2 = this.children[index].innerHTML.trim();
      let getShow2 = tbody.children[1]; 

      if(inputText2 == "Miestas - Miestas"){
        if(getShow2 != null) {
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }  
        maintr.children[1].append(textFrom2);
        maintr.children[1].append(fromCityUse2);
        maintr.children[1].append(textTo2);
        maintr.children[1].append(toCityUse2);     
      
      
      }else if(inputText2 == "Miestas - Rajonas"){
        if(getShow2 != null){          
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }
         maintr.children[1].append(textFrom2);
         maintr.children[1].append(fromCityUse2);
         maintr.children[1].append(textTo2);     
         maintr.children[1].append(toDistrictUse2);  
        
      }else if(inputText2 == "Rajonas - Miestas"){
        if(getShow2 != null) {       
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }
         maintr.children[1].append(textFrom2);
         maintr.children[1].append(fromDistrictUse2);
         maintr.children[1].append(textTo2);  
         maintr.children[1].append(toCityUse2);     
    
      }else if(inputText2 == "Rajonas - Rajonas"){
        if(getShow2 != null) {
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }
         maintr.children[1].append(textFrom2);
         maintr.children[1].append(fromDistrictUse2);  
         maintr.children[1].append(textTo2);
         maintr.children[1].append(toDistrictUse2);
        
      }else if(inputText2 == "Geozona - Geozona"){
        if(getShow2 != null) {
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }
         maintr.children[1].append(textFrom2);
         maintr.children[1].append(fromGeoZoneUse2);  
         maintr.children[1].append(textTo2);
         maintr.children[1].append(toGeoZoneUse2);  
        
      }else if(inputText2 == "Miestas - Geozona"){
        if(getShow2 != null) {
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }
         maintr.children[1].append(textFrom2);
         maintr.children[1].append(fromCityUse2);  
         maintr.children[1].append(textTo2);
         maintr.children[1].append(toGeoZoneUse2);  

      }else if(inputText2 == "Geozona - Miestas"){
        if(getShow2 != null) {
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }
         maintr.children[1].append(textFrom2);
         maintr.children[1].append(fromGeoZoneUse2);  
         maintr.children[1].append(textTo2);
         maintr.children[1].append(toCityUse2);  

      }else if(inputText2 == "LTU - Miestas"){
        if(getShow2 != null) {
          fromCityUse2.remove();
          toCityUse2.remove();
          fromDistrictUse2.remove();
          toDistrictUse2.remove();
          fromGeoZoneUse2.remove();
          toGeoZoneUse2.remove();
          textTo2.remove();
          textFrom2.remove();
        }
         maintr.children[1].append(textTo2);
         maintr.children[1].append(toCityUse2);

      }else if(inputText2 == "LTU - LTU"){
        fromCityUse2.remove();
        toCityUse2.remove();
        fromDistrictUse2.remove();
        toDistrictUse2.remove();
        fromGeoZoneUse2.remove();
        toGeoZoneUse2.remove();
        textTo2.remove();
        textFrom2.remove();
        
      }else{
        fromCityUse2.remove();
        toCityUse2.remove();
        fromDistrictUse2.remove();
        toDistrictUse2.remove();
        fromGeoZoneUse2.remove();
        toGeoZoneUse2.remove();
        textTo2.remove();
        textFrom2.remove();
      }  
    }
    thead.getElementsByTagName("select")[2].onchange = function() {
      let index = this.selectedIndex;
      let inputText3 = this.children[index].innerHTML.trim();
      let getShow3 = tbody.children[1]; 

      if(inputText3 == "Miestas - Miestas"){
        if(getShow3 != null) {
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }  
        maintr.lastChild.append(textFrom3);
        maintr.lastChild.append(fromCityUse3);
        maintr.lastChild.append(textTo3);
        maintr.lastChild.append(toCityUse3);     
      
      
      }else if(inputText3 == "Miestas - Rajonas"){
        if(getShow3 != null){          
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }
        maintr.lastChild.append(textFrom3);
        maintr.lastChild.append(fromCityUse3);
        maintr.lastChild.append(textTo3);     
        maintr.lastChild.append(toDistrictUse3);  
        
      }else if(inputText3 == "Rajonas - Miestas"){
        if(getShow3 != null) {       
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }
        maintr.lastChild.append(textFrom3);
        maintr.lastChild.append(fromDistrictUse3);
        maintr.lastChild.append(textTo3);  
        maintr.lastChild.append(toCityUse3);     
    
      }else if(inputText3 == "Rajonas - Rajonas"){
        if(getShow3 != null) {
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }
        maintr.lastChild.append(textFrom3);
        maintr.lastChild.append(fromDistrictUse3);  
        maintr.lastChild.append(textTo3);
        maintr.lastChild.append(toDistrictUse3);
        
      }else if(inputText3 == "Geozona - Geozona"){
        if(getShow3 != null) {
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }
        maintr.lastChild.append(textFrom3);
        maintr.lastChild.append(fromGeoZoneUse3);  
        maintr.lastChild.append(textTo3);
        maintr.lastChild.append(toGeoZoneUse3);  
        
      }else if(inputText3 == "Miestas - Geozona"){
        if(getShow3 != null) {
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }
        maintr.lastChild.append(textFrom3);
        maintr.lastChild.append(fromCityUse3);  
        maintr.lastChild.append(textTo3);
        maintr.lastChild.append(toGeoZoneUse3);  

      }else if(inputText3 == "Geozona - Miestas"){
        if(getShow3 != null) {
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }
        maintr.lastChild.append(textFrom3);
        maintr.lastChild.append(fromGeoZoneUse3);  
        maintr.lastChild.append(textTo3);
        maintr.lastChild.append(toCityUse3);  

      }else if(inputText3 == "LTU - Miestas"){
        if(getShow3 != null) {
          fromCityUse3.remove();
          toCityUse3.remove();
          fromDistrictUse3.remove();
          toDistrictUse3.remove();
          fromGeoZoneUse3.remove();
          toGeoZoneUse3.remove();
          textTo3.remove();
          textFrom3.remove();
        }
        maintr.lastChild.append(textTo3);
        maintr.lastChild.append(toCityUse3);

      }else if(inputText3 == "LTU - LTU"){
        fromCityUse3.remove();
        toCityUse3.remove();
        fromDistrictUse3.remove();
        toDistrictUse3.remove();
        fromGeoZoneUse3.remove();
        toGeoZoneUse3.remove();
        textTo3.remove();
        textFrom3.remove();
        
      }else{
        fromCityUse3.remove();
        toCityUse3.remove();
        fromDistrictUse3.remove();
        toDistrictUse3.remove();
        fromGeoZoneUse3.remove();
        toGeoZoneUse3.remove();
        textTo3.remove();
        textFrom3.remove();
      }  
    }

//************************************************************************************************************************ */
    // ANTRA EILE
//************************************************************************************************************************ */

secondTable = document.createElement("table");
tables.append(secondTable);
secondTable.setAttribute("class", "container");

const maintable2 = document.getElementsByClassName("container")[1];

let secondLineFirst = tbody.children[9];
let secondLineSecond = tbody.children[10];
let secondLineThird = tbody.children[11];
let secondLineFourt = tbody.children[12];
let secondLineFifth = tbody.children[13];
let secondLineSix = tbody.children[14];
let secondLineSeven = tbody.children[15];
let secondLineEight = tbody.children[16];
let secondLineNine = tbody.children[17];
let secondLineTen = tbody.children[18];
let secondLineEleven = tbody.children[19];
let secondLineTwelve = tbody.children[20];

secondLineFirst.remove();
secondLineSecond.remove();
secondLineThird.remove();
secondLineFourt.remove();

secondLineFifth.remove();
secondLineSix.remove();
secondLineSeven.remove();
secondLineEight.remove();

secondLineNine.remove();
secondLineTen.remove();
secondLineEleven.remove();
secondLineTwelve.remove();

secondLineFirst.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 
secondLineFifth.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 
secondLineNine.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 


// Create new thead and tbody tags
const theadSecond = document.createElement("thead");
maintable2.append(theadSecond);
const tbodySecond = document.createElement("tbody");
maintable2.append(tbodySecond);


// // Create tr and td tags
const maintrToAppendSecond = document.createElement("tr");
let maintdSecondFirst = document.createElement("td");
maintdSecondFirst.append(secondLineFirst.children[1]);

let maintdSecondFifth = document.createElement("td");
maintdSecondFifth.append(secondLineFifth.children[1]);

let maintdSecondNine = document.createElement("td");
maintdSecondNine.append(secondLineNine.children[1]);


maintrToAppendSecond.append(maintdSecondFirst);
maintrToAppendSecond.append(maintdSecondFifth);
maintrToAppendSecond.append(maintdSecondNine);
theadSecond.append(maintrToAppendSecond);


// Pirmas

const SecondLinefromCity = secondLineSecond.children[1];
const SecondLinetoCity =  secondLineSecond.children[3];

const SecondLinefromDistrict = secondLineThird.children[1];
const SecondLinetoDistrict = secondLineThird.children[3];

const SecondLinefromGeozone = secondLineFourt.children[1];
const SecondLinetoGeozone = secondLineFourt.children[3];


const SecondLinefromCityUse = document.createElement("div");
SecondLinefromCityUse.setAttribute("id", "show");
SecondLinefromCityUse.appendChild(SecondLinefromCity);

const SecondLinetoCityUse = document.createElement("div");
SecondLinetoCityUse.setAttribute("id", "show");
SecondLinetoCityUse.appendChild(SecondLinetoCity);


const SecondLinefromDistrictUse = document.createElement("div");
SecondLinefromDistrictUse.setAttribute("id", "show");
SecondLinefromDistrictUse.append(SecondLinefromDistrict);

const SecondLinetoDistrictUse = document.createElement("div");
SecondLinetoDistrictUse.setAttribute("id", "show");
SecondLinetoDistrictUse.append(SecondLinetoDistrict);


const SecondLinefromGeoZoneUse = document.createElement("div");
SecondLinefromGeoZoneUse.setAttribute("id", "show");
SecondLinefromGeoZoneUse.append(SecondLinefromGeozone);

const SecondLinetoGeoZoneUse = document.createElement("div");
SecondLinetoGeoZoneUse.setAttribute("id", "show");
SecondLinetoGeoZoneUse.append(SecondLinetoGeozone);


const SecondLinemaintr = document.createElement("tr");
const SecondLinepreparedTd = document.createElement("td");
const SecondLinepreparedTd2 = document.createElement("td");
const SecondLinepreparedTd3 = document.createElement("td");
SecondLinemaintr.setAttribute("id", "maintr");
SecondLinemaintr.append(SecondLinepreparedTd);
SecondLinemaintr.append(SecondLinepreparedTd2);
SecondLinemaintr.append(SecondLinepreparedTd3);
tbodySecond.append(SecondLinemaintr);
tbodySecond.insertBefore(SecondLinemaintr, tbodySecond.firstChild);


const SecondLinetextIs = document.innerText = 'Iš ';
const SecondLinetextI = document.innerText = 'Į ';
const SecondLinetextFrom = document.createElement("div");
SecondLinetextFrom.setAttribute("id","show-text");
SecondLinetextFrom.append(SecondLinetextIs);

const SecondLinetextTo = document.createElement("div");
SecondLinetextTo.setAttribute("id","show-text");
SecondLinetextTo.append(SecondLinetextI);


// Antras


const SecondLinefromCity2 = secondLineSix.children[1];
const SecondLinetoCity2 =  secondLineSix.children[3];

const SecondLinefromDistrict2 = secondLineSeven.children[1];
const SecondLinetoDistrict2 = secondLineSeven.children[3];

const SecondLinefromGeozone2 = secondLineEight.children[1];
const SecondLinetoGeozone2 = secondLineEight.children[3];

const SecondLinefromCityUse2 = document.createElement("div");
SecondLinefromCityUse2.setAttribute("id", "show");
SecondLinefromCityUse2.appendChild(SecondLinefromCity2);

const SecondLinetoCityUse2 = document.createElement("div");
SecondLinetoCityUse2.setAttribute("id", "show");
SecondLinetoCityUse2.appendChild(SecondLinetoCity2);

const SecondLinefromDistrictUse2 = document.createElement("div");
SecondLinefromDistrictUse2.setAttribute("id", "show");
SecondLinefromDistrictUse2.append(SecondLinefromDistrict2);

const SecondLinetoDistrictUse2 = document.createElement("div");
SecondLinetoDistrictUse2.setAttribute("id", "show");
SecondLinetoDistrictUse2.append(SecondLinetoDistrict2);


const SecondLinefromGeoZoneUse2 = document.createElement("div");
SecondLinefromGeoZoneUse2.setAttribute("id", "show");
SecondLinefromGeoZoneUse2.append(SecondLinefromGeozone2);

const SecondLinetoGeoZoneUse2 = document.createElement("div");
SecondLinetoGeoZoneUse2.setAttribute("id", "show");
SecondLinetoGeoZoneUse2.append(SecondLinetoGeozone2);

const SecondLinetextIs2 = document.innerText = 'Iš ';
const SecondLinetextI2 = document.innerText = 'Į ';
const SecondLinetextFrom2 = document.createElement("div");
SecondLinetextFrom2.setAttribute("id","show-text");
SecondLinetextFrom2.append(SecondLinetextIs2);

const SecondLinetextTo2 = document.createElement("div");
SecondLinetextTo2.setAttribute("id","show-text");
SecondLinetextTo2.append(SecondLinetextI2);



// Trecias

const SecondLinefromCity3 = secondLineTen.children[1];
const SecondLinetoCity3 =  secondLineTen.children[3];

const SecondLinefromDistrict3 = secondLineEleven.children[1];
const SecondLinetoDistrict3 = secondLineEleven.children[3];

const SecondLinefromGeozone3 = secondLineTwelve.children[1];
const SecondLinetoGeozone3 = secondLineTwelve.children[3];

const SecondLinefromCityUse3 = document.createElement("div");
SecondLinefromCityUse3.setAttribute("id", "show");
SecondLinefromCityUse3.appendChild(SecondLinefromCity3);

const SecondLinetoCityUse3 = document.createElement("div");
SecondLinetoCityUse3.setAttribute("id", "show");
SecondLinetoCityUse3.appendChild(SecondLinetoCity3);

const SecondLinefromDistrictUse3 = document.createElement("div");
SecondLinefromDistrictUse3.setAttribute("id", "show");
SecondLinefromDistrictUse3.append(SecondLinefromDistrict3);

const SecondLinetoDistrictUse3 = document.createElement("div");
SecondLinetoDistrictUse3.setAttribute("id", "show");
SecondLinetoDistrictUse3.append(SecondLinetoDistrict3);


const SecondLinefromGeoZoneUse3 = document.createElement("div");
SecondLinefromGeoZoneUse3.setAttribute("id", "show");
SecondLinefromGeoZoneUse3.append(SecondLinefromGeozone3);

const SecondLinetoGeoZoneUse3 = document.createElement("div");
SecondLinetoGeoZoneUse3.setAttribute("id", "show");
SecondLinetoGeoZoneUse3.append(SecondLinetoGeozone3);

const SecondLinetextIs3 = document.innerText = 'Iš ';
const SecondLinetextI3 = document.innerText = 'Į ';
const SecondLinetextFrom3 = document.createElement("div");
SecondLinetextFrom3.setAttribute("id","show-text");
SecondLinetextFrom3.append(SecondLinetextIs3);

const SecondLinetextTo3 = document.createElement("div");
SecondLinetextTo3.setAttribute("id","show-text");
SecondLinetextTo3.append(SecondLinetextI3);



theadSecond.getElementsByTagName("select")[0].onchange = function() {
  let index = this.selectedIndex;
  let SecondLineinputText = this.children[index].innerHTML.trim();
  let getShow = maintable2.children[0].children[0].children[0]; 

  if(SecondLineinputText == "Miestas - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }  
    SecondLinemaintr.firstChild.append(SecondLinetextFrom);
    SecondLinemaintr.firstChild.append(SecondLinefromCityUse);
    SecondLinemaintr.firstChild.append(SecondLinetextTo);
    SecondLinemaintr.firstChild.append(SecondLinetoCityUse);     
  
  
  }else if(SecondLineinputText == "Miestas - Rajonas"){
    if(getShow != null){          
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }
    SecondLinemaintr.firstChild.append(SecondLinetextFrom);
    SecondLinemaintr.firstChild.append(SecondLinefromCityUse);
    SecondLinemaintr.firstChild.append(SecondLinetextTo);     
    SecondLinemaintr.firstChild.append(SecondLinetoDistrictUse);  
    
  }else if(SecondLineinputText == "Rajonas - Miestas"){
    if(getShow != null) {       
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }
    SecondLinemaintr.firstChild.append(SecondLinetextFrom);
    SecondLinemaintr.firstChild.append(SecondLinefromDistrictUse);
    SecondLinemaintr.firstChild.append(SecondLinetextTo);  
    SecondLinemaintr.firstChild.append(SecondLinetoCityUse);     

  }else if(SecondLineinputText == "Rajonas - Rajonas"){
    if(getShow != null) {
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }
    SecondLinemaintr.firstChild.append(SecondLinetextFrom);
    SecondLinemaintr.firstChild.append(SecondLinefromDistrictUse);  
    SecondLinemaintr.firstChild.append(SecondLinetextTo);
    SecondLinemaintr.firstChild.append(SecondLinetoDistrictUse);
    
  }else if(SecondLineinputText == "Geozona - Geozona"){
    if(getShow != null) {
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }
    SecondLinemaintr.firstChild.append(SecondLinetextFrom);
    SecondLinemaintr.firstChild.append(SecondLinefromGeoZoneUse);  
    SecondLinemaintr.firstChild.append(SecondLinetextTo);
    SecondLinemaintr.firstChild.append(SecondLinetoGeoZoneUse);  
    
  }else if(SecondLineinputText == "Miestas - Geozona"){
    if(getShow != null) {
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }
    SecondLinemaintr.firstChild.append(SecondLinetextFrom);
    SecondLinemaintr.firstChild.append(SecondLinefromCityUse);  
    SecondLinemaintr.firstChild.append(SecondLinetextTo);
    SecondLinemaintr.firstChild.append(SecondLinetoGeoZoneUse);  

  }else if(SecondLineinputText == "Geozona - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }
    SecondLinemaintr.firstChild.append(SecondLinetextFrom);
    SecondLinemaintr.firstChild.append(SecondLinefromGeoZoneUse);  
    SecondLinemaintr.firstChild.append(SecondLinetextTo);
    SecondLinemaintr.firstChild.append(SecondLinetoCityUse);  

  }else if(SecondLineinputText == "LTU - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse.remove();
      SecondLinetoCityUse.remove();
      SecondLinefromDistrictUse.remove();
      SecondLinetoDistrictUse.remove();
      SecondLinefromGeoZoneUse.remove();
      SecondLinetoGeoZoneUse.remove();
      SecondLinetextTo.remove();
      SecondLinetextFrom.remove();
    }
    SecondLinemaintr.firstChild.append(SecondLinetextTo);
    SecondLinemaintr.firstChild.append(SecondLinetoCityUse);

  }else if(SecondLineinputText == "LTU - LTU"){
    SecondLinefromCityUse.remove();
    SecondLinetoCityUse.remove();
    SecondLinefromDistrictUse.remove();
    SecondLinetoDistrictUse.remove();
    SecondLinefromGeoZoneUse.remove();
    SecondLinetoGeoZoneUse.remove();
    SecondLinetextTo.remove();
    SecondLinetextFrom.remove();
    
  }else{
    SecondLinefromCityUse.remove();
    SecondLinetoCityUse.remove();
    SecondLinefromDistrictUse.remove();
    SecondLinetoDistrictUse.remove();
    SecondLinefromGeoZoneUse.remove();
    SecondLinetoGeoZoneUse.remove();
    SecondLinetextTo.remove();
    SecondLinetextFrom.remove();
  }  
  // console.log(SecondLineinputText);  
  
}

// Antras

theadSecond.getElementsByTagName("select")[1].onchange = function() {
  let index = this.selectedIndex;
  let SecondLineinputText2 = this.children[index].innerHTML.trim();
  let getShow = maintable2.children[0].children[0].children[1]; 

  if(SecondLineinputText2 == "Miestas - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }  
    SecondLinemaintr.children[1].append(SecondLinetextFrom2);
    SecondLinemaintr.children[1].append(SecondLinefromCityUse2);
    SecondLinemaintr.children[1].append(SecondLinetextTo2);
    SecondLinemaintr.children[1].append(SecondLinetoCityUse2);     
  
  
  }else if(SecondLineinputText2 == "Miestas - Rajonas"){
    if(getShow != null){          
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }
    SecondLinemaintr.children[1].append(SecondLinetextFrom2);
    SecondLinemaintr.children[1].append(SecondLinefromCityUse2);
    SecondLinemaintr.children[1].append(SecondLinetextTo2);     
    SecondLinemaintr.children[1].append(SecondLinetoDistrictUse2);  
    
  }else if(SecondLineinputText2 == "Rajonas - Miestas"){
    if(getShow != null) {       
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }
    SecondLinemaintr.children[1].append(SecondLinetextFrom2);
    SecondLinemaintr.children[1].append(SecondLinefromDistrictUse2);
    SecondLinemaintr.children[1].append(SecondLinetextTo2);  
    SecondLinemaintr.children[1].append(SecondLinetoCityUse2);     

  }else if(SecondLineinputText2 == "Rajonas - Rajonas"){
    if(getShow != null) {
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }
    SecondLinemaintr.children[1].append(SecondLinetextFrom2);
    SecondLinemaintr.children[1].append(SecondLinefromDistrictUse2);  
    SecondLinemaintr.children[1].append(SecondLinetextTo2);
    SecondLinemaintr.children[1].append(SecondLinetoDistrictUse2);
    
  }else if(SecondLineinputText2 == "Geozona - Geozona"){
    if(getShow != null) {
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }
    SecondLinemaintr.children[1].append(SecondLinetextFrom2);
    SecondLinemaintr.children[1].append(SecondLinefromGeoZoneUse2);  
    SecondLinemaintr.children[1].append(SecondLinetextTo2);
    SecondLinemaintr.children[1].append(SecondLinetoGeoZoneUse2);  
    
  }else if(SecondLineinputText2 == "Miestas - Geozona"){
    if(getShow != null) {
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }
    SecondLinemaintr.children[1].append(SecondLinetextFrom2);
    SecondLinemaintr.children[1].append(SecondLinefromCityUse2);  
    SecondLinemaintr.children[1].append(SecondLinetextTo2);
    SecondLinemaintr.children[1].append(SecondLinetoGeoZoneUse2);  

  }else if(SecondLineinputText2 == "Geozona - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }
    SecondLinemaintr.children[1].append(SecondLinetextFrom2);
    SecondLinemaintr.children[1].append(SecondLinefromGeoZoneUse2);  
    SecondLinemaintr.children[1].append(SecondLinetextTo2);
    SecondLinemaintr.children[1].append(SecondLinetoCityUse2);  

  }else if(SecondLineinputText2 == "LTU - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse2.remove();
      SecondLinetoCityUse2.remove();
      SecondLinefromDistrictUse2.remove();
      SecondLinetoDistrictUse2.remove();
      SecondLinefromGeoZoneUse2.remove();
      SecondLinetoGeoZoneUse2.remove();
      SecondLinetextTo2.remove();
      SecondLinetextFrom2.remove();
    }
    SecondLinemaintr.children[1].append(SecondLinetextTo2);
    SecondLinemaintr.children[1].append(SecondLinetoCityUse2);

  }else if(SecondLineinputText2 == "LTU - LTU"){
    SecondLinefromCityUse2.remove();
    SecondLinetoCityUse2.remove();
    SecondLinefromDistrictUse2.remove();
    SecondLinetoDistrictUse2.remove();
    SecondLinefromGeoZoneUse2.remove();
    SecondLinetoGeoZoneUse2.remove();
    SecondLinetextTo2.remove();
    SecondLinetextFrom2.remove();
    
  }else{
    SecondLinefromCityUse2.remove();
    SecondLinetoCityUse2.remove();
    SecondLinefromDistrictUse2.remove();
    SecondLinetoDistrictUse2.remove();
    SecondLinefromGeoZoneUse2.remove();
    SecondLinetoGeoZoneUse2.remove();
    SecondLinetextTo2.remove();
    SecondLinetextFrom2.remove();
  }  
  // console.log(SecondLineinputText2);  
  
}

// Trecias

theadSecond.getElementsByTagName("select")[2].onchange = function() {
  let index = this.selectedIndex;
  let SecondLineinputText3 = this.children[index].innerHTML.trim();
  let getShow = maintable2.children[0].children[0].children[2]; 

  if(SecondLineinputText3 == "Miestas - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }  
    SecondLinemaintr.lastChild.append(SecondLinetextFrom3);
    SecondLinemaintr.lastChild.append(SecondLinefromCityUse3);
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);
    SecondLinemaintr.lastChild.append(SecondLinetoCityUse3);     
  
  
  }else if(SecondLineinputText3 == "Miestas - Rajonas"){
    if(getShow != null){          
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }
    SecondLinemaintr.lastChild.append(SecondLinetextFrom3);
    SecondLinemaintr.lastChild.append(SecondLinefromCityUse3);
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);     
    SecondLinemaintr.lastChild.append(SecondLinetoDistrictUse3);  
    
  }else if(SecondLineinputText3 == "Rajonas - Miestas"){
    if(getShow != null) {       
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }
    SecondLinemaintr.lastChild.append(SecondLinetextFrom3);
    SecondLinemaintr.lastChild.append(SecondLinefromDistrictUse3);
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);  
    SecondLinemaintr.lastChild.append(SecondLinetoCityUse3);     

  }else if(SecondLineinputText3 == "Rajonas - Rajonas"){
    if(getShow != null) {
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }
    SecondLinemaintr.lastChild.append(SecondLinetextFrom3);
    SecondLinemaintr.lastChild.append(SecondLinefromDistrictUse3);  
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);
    SecondLinemaintr.lastChild.append(SecondLinetoDistrictUse3);
    
  }else if(SecondLineinputText3 == "Geozona - Geozona"){
    if(getShow != null) {
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }
    SecondLinemaintr.lastChild.append(SecondLinetextFrom3);
    SecondLinemaintr.lastChild.append(SecondLinefromGeoZoneUse3);  
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);
    SecondLinemaintr.lastChild.append(SecondLinetoGeoZoneUse3);  
    
  }else if(SecondLineinputText3 == "Miestas - Geozona"){
    if(getShow != null) {
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }
    SecondLinemaintr.lastChild.append(SecondLinetextFrom3);
    SecondLinemaintr.lastChild.append(SecondLinefromCityUse3);  
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);
    SecondLinemaintr.lastChild.append(SecondLinetoGeoZoneUse3);  

  }else if(SecondLineinputText3 == "Geozona - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }
    SecondLinemaintr.lastChild.append(SecondLinetextFrom3);
    SecondLinemaintr.lastChild.append(SecondLinefromGeoZoneUse3);  
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);
    SecondLinemaintr.lastChild.append(SecondLinetoCityUse3);  

  }else if(SecondLineinputText3 == "LTU - Miestas"){
    if(getShow != null) {
      SecondLinefromCityUse3.remove();
      SecondLinetoCityUse3.remove();
      SecondLinefromDistrictUse3.remove();
      SecondLinetoDistrictUse3.remove();
      SecondLinefromGeoZoneUse3.remove();
      SecondLinetoGeoZoneUse3.remove();
      SecondLinetextTo3.remove();
      SecondLinetextFrom3.remove();
    }
    SecondLinemaintr.lastChild.append(SecondLinetextTo3);
    SecondLinemaintr.lastChild.append(SecondLinetoCityUse3);

  }else if(SecondLineinputText3 == "LTU - LTU"){
    SecondLinefromCityUse3.remove();
    SecondLinetoCityUse3.remove();
    SecondLinefromDistrictUse3.remove();
    SecondLinetoDistrictUse3.remove();
    SecondLinefromGeoZoneUse3.remove();
    SecondLinetoGeoZoneUse3.remove();
    SecondLinetextTo3.remove();
    SecondLinetextFrom3.remove();
    
  }else{
    SecondLinefromCityUse3.remove();
    SecondLinetoCityUse3.remove();
    SecondLinefromDistrictUse3.remove();
    SecondLinetoDistrictUse3.remove();
    SecondLinefromGeoZoneUse3.remove();
    SecondLinetoGeoZoneUse3.remove();
    SecondLinetextTo3.remove();
    SecondLinetextFrom3.remove();
  } 
  
  // console.log(SecondLineinputText3);
}

/*********************************************************************************************************** */
// TRECIA EILE
/********************************************************************************************************** */

thirdTable = document.createElement("table");
tables.append(thirdTable);
thirdTable.setAttribute("class", "container");

const maintable3 = document.getElementsByClassName("container")[2];

let thirdLineFirst = tbody.children[9];
let thirdLineSecond = tbody.children[10];
let thirdLineThird = tbody.children[11];
let thirdLineFourt = tbody.children[12];
let thirdLineFifth = tbody.children[13];
let thirdLineSix = tbody.children[14];
let thirdLineSeven = tbody.children[15];
let thirdLineEight = tbody.children[16];
let thirdLineNine = tbody.children[17];
let thirdLineTen = tbody.children[18];
let thirdLineEleven = tbody.children[19];
let thirdLineTwelve = tbody.children[20];

thirdLineFirst.remove();
thirdLineSecond.remove();
thirdLineThird.remove();
thirdLineFourt.remove();

thirdLineFifth.remove();
thirdLineSix.remove();
thirdLineSeven.remove();
thirdLineEight.remove();

thirdLineNine.remove();
thirdLineTen.remove();
thirdLineEleven.remove();
thirdLineTwelve.remove();


thirdLineFirst.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 
thirdLineFifth.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 
thirdLineNine.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 


// Create new thead and tbody tags
const theadThird = document.createElement("thead");
maintable3.append(theadThird);
const tbodyThird = document.createElement("tbody");
maintable3.append(tbodyThird);


// // Create tr and td tags
const maintrToAppendThird = document.createElement("tr");
let maintdThirdFirst = document.createElement("td");
maintdThirdFirst.append(thirdLineFirst.children[1]);

let maintdThirdFifth = document.createElement("td");
maintdThirdFifth.append(thirdLineFifth.children[1]);

let maintdThirdNine = document.createElement("td");
maintdThirdNine.append(thirdLineNine.children[1]);


maintrToAppendThird.append(maintdThirdFirst);
maintrToAppendThird.append(maintdThirdFifth);
maintrToAppendThird.append(maintdThirdNine);
theadThird.append(maintrToAppendThird);

// Pirmas

const ThirdLinefromCity = thirdLineSecond.children[1];
const ThirdLinetoCity = thirdLineSecond.children[3];

const ThirdLinefromDistrict = thirdLineThird.children[1];
const ThirdLinetoDistrict = thirdLineThird.children[3];

const ThirdLinefromGeozone = thirdLineFourt.children[1];
const ThirdLinetoGeozone = thirdLineFourt.children[3];


const ThirdLinefromCityUse = document.createElement("div");
ThirdLinefromCityUse.setAttribute("id", "show");
ThirdLinefromCityUse.appendChild(ThirdLinefromCity);

const ThirdLinetoCityUse = document.createElement("div");
ThirdLinetoCityUse.setAttribute("id", "show");
ThirdLinetoCityUse.appendChild(ThirdLinetoCity);


const ThirdLinefromDistrictUse = document.createElement("div");
ThirdLinefromDistrictUse.setAttribute("id", "show");
ThirdLinefromDistrictUse.append(ThirdLinefromDistrict);

const ThirdLinetoDistrictUse = document.createElement("div");
ThirdLinetoDistrictUse.setAttribute("id", "show");
ThirdLinetoDistrictUse.append(ThirdLinetoDistrict);


const ThirdLinefromGeoZoneUse = document.createElement("div");
ThirdLinefromGeoZoneUse.setAttribute("id", "show");
ThirdLinefromGeoZoneUse.append(ThirdLinefromGeozone);

const ThirdLinetoGeoZoneUse = document.createElement("div");
ThirdLinetoGeoZoneUse.setAttribute("id", "show");
ThirdLinetoGeoZoneUse.append(ThirdLinetoGeozone);


const ThirdLinemaintr = document.createElement("tr");
const ThirdLinepreparedTd = document.createElement("td");
const ThirdLinepreparedTd2 = document.createElement("td");
const ThirdLinepreparedTd3 = document.createElement("td");
ThirdLinemaintr.setAttribute("id", "maintr");
ThirdLinemaintr.append(ThirdLinepreparedTd);
ThirdLinemaintr.append(ThirdLinepreparedTd2);
ThirdLinemaintr.append(ThirdLinepreparedTd3);
tbodyThird.append(ThirdLinemaintr);
tbodyThird.insertBefore(ThirdLinemaintr, tbodyThird.firstChild);


const ThirdLinetextIs = document.innerText = 'Iš ';
const ThirdLinetextI = document.innerText = 'Į ';
const ThirdLinetextFrom = document.createElement("div");
ThirdLinetextFrom.setAttribute("id","show-text");
ThirdLinetextFrom.append(ThirdLinetextIs);

const ThirdLinetextTo = document.createElement("div");
ThirdLinetextTo.setAttribute("id","show-text");
ThirdLinetextTo.append(ThirdLinetextI);



// Antras


const ThirdLinefromCity2 = thirdLineSix.children[1];
const ThirdLinetoCity2 =  thirdLineSix.children[3];

const ThirdLinefromDistrict2 = thirdLineSeven.children[1];
const ThirdLinetoDistrict2 = thirdLineSeven.children[3];

const ThirdLinefromGeozone2 = thirdLineEight.children[1];
const ThirdLinetoGeozone2 = thirdLineEight.children[3];

const ThirdLinefromCityUse2 = document.createElement("div");
ThirdLinefromCityUse2.setAttribute("id", "show");
ThirdLinefromCityUse2.appendChild(ThirdLinefromCity2);

const ThirdLinetoCityUse2 = document.createElement("div");
ThirdLinetoCityUse2.setAttribute("id", "show");
ThirdLinetoCityUse2.appendChild(ThirdLinetoCity2);

const ThirdLinefromDistrictUse2 = document.createElement("div");
ThirdLinefromDistrictUse2.setAttribute("id", "show");
ThirdLinefromDistrictUse2.append(ThirdLinefromDistrict2);

const ThirdLinetoDistrictUse2 = document.createElement("div");
ThirdLinetoDistrictUse2.setAttribute("id", "show");
ThirdLinetoDistrictUse2.append(ThirdLinetoDistrict2);


const ThirdLinefromGeoZoneUse2 = document.createElement("div");
ThirdLinefromGeoZoneUse2.setAttribute("id", "show");
ThirdLinefromGeoZoneUse2.append(ThirdLinefromGeozone2);

const ThirdLinetoGeoZoneUse2 = document.createElement("div");
ThirdLinetoGeoZoneUse2.setAttribute("id", "show");
ThirdLinetoGeoZoneUse2.append(ThirdLinetoGeozone2);

const ThirdLinetextIs2 = document.innerText = 'Iš ';
const ThirdLinetextI2 = document.innerText = 'Į ';
const ThirdLinetextFrom2 = document.createElement("div");
ThirdLinetextFrom2.setAttribute("id","show-text");
ThirdLinetextFrom2.append(ThirdLinetextIs2);

const ThirdLinetextTo2 = document.createElement("div");
ThirdLinetextTo2.setAttribute("id","show-text");
ThirdLinetextTo2.append(ThirdLinetextI2);



// Trecias

const ThirdLinefromCity3 = thirdLineTen.children[1];
const ThirdLinetoCity3 =  thirdLineTen.children[3];

const ThirdLinefromDistrict3 = thirdLineEleven.children[1];
const ThirdLinetoDistrict3 = thirdLineEleven.children[3];

const ThirdLinefromGeozone3 = thirdLineTwelve.children[1];
const ThirdLinetoGeozone3 = thirdLineTwelve.children[3];

const ThirdLinefromCityUse3 = document.createElement("div");
ThirdLinefromCityUse3.setAttribute("id", "show");
ThirdLinefromCityUse3.appendChild(ThirdLinefromCity3);

const ThirdLinetoCityUse3 = document.createElement("div");
ThirdLinetoCityUse3.setAttribute("id", "show");
ThirdLinetoCityUse3.appendChild(ThirdLinetoCity3);

const ThirdLinefromDistrictUse3 = document.createElement("div");
ThirdLinefromDistrictUse3.setAttribute("id", "show");
ThirdLinefromDistrictUse3.append(ThirdLinefromDistrict3);

const ThirdLinetoDistrictUse3 = document.createElement("div");
ThirdLinetoDistrictUse3.setAttribute("id", "show");
ThirdLinetoDistrictUse3.append(ThirdLinetoDistrict3);


const ThirdLinefromGeoZoneUse3 = document.createElement("div");
ThirdLinefromGeoZoneUse3.setAttribute("id", "show");
ThirdLinefromGeoZoneUse3.append(ThirdLinefromGeozone3);

const ThirdLinetoGeoZoneUse3 = document.createElement("div");
ThirdLinetoGeoZoneUse3.setAttribute("id", "show");
ThirdLinetoGeoZoneUse3.append(ThirdLinetoGeozone3);

const ThirdLinetextIs3 = document.innerText = 'Iš ';
const ThirdLinetextI3 = document.innerText = 'Į ';
const ThirdLinetextFrom3 = document.createElement("div");
ThirdLinetextFrom3.setAttribute("id","show-text");
ThirdLinetextFrom3.append(ThirdLinetextIs3);

const ThirdLinetextTo3 = document.createElement("div");
ThirdLinetextTo3.setAttribute("id","show-text");
ThirdLinetextTo3.append(ThirdLinetextI3);



theadThird.getElementsByTagName("select")[0].onchange = function() {
  let index = this.selectedIndex;
  let ThirdLineinputText = this.children[index].innerHTML.trim();
  let getShow = maintable3.children[0].children[0].children[0]; 

  if(ThirdLineinputText == "Miestas - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }  
    ThirdLinemaintr.firstChild.append(ThirdLinetextFrom);
    ThirdLinemaintr.firstChild.append(ThirdLinefromCityUse);
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);
    ThirdLinemaintr.firstChild.append(ThirdLinetoCityUse);     
  
  
  }else if(ThirdLineinputText == "Miestas - Rajonas"){
    if(getShow != null){          
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }
    ThirdLinemaintr.firstChild.append(ThirdLinetextFrom);
    ThirdLinemaintr.firstChild.append(ThirdLinefromCityUse);
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);     
    ThirdLinemaintr.firstChild.append(ThirdLinetoDistrictUse);  
    
  }else if(ThirdLineinputText == "Rajonas - Miestas"){
    if(getShow != null) {       
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }
    ThirdLinemaintr.firstChild.append(ThirdLinetextFrom);
    ThirdLinemaintr.firstChild.append(ThirdLinefromDistrictUse);
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);  
    ThirdLinemaintr.firstChild.append(ThirdLinetoCityUse);     

  }else if(ThirdLineinputText == "Rajonas - Rajonas"){
    if(getShow != null) {
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }
    ThirdLinemaintr.firstChild.append(ThirdLinetextFrom);
    ThirdLinemaintr.firstChild.append(ThirdLinefromDistrictUse);  
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);
    ThirdLinemaintr.firstChild.append(ThirdLinetoDistrictUse);
    
  }else if(ThirdLineinputText == "Geozona - Geozona"){
    if(getShow != null) {
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }
    ThirdLinemaintr.firstChild.append(ThirdLinetextFrom);
    ThirdLinemaintr.firstChild.append(ThirdLinefromGeoZoneUse);  
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);
    ThirdLinemaintr.firstChild.append(ThirdLinetoGeoZoneUse);  
    
  }else if(ThirdLineinputText == "Miestas - Geozona"){
    if(getShow != null) {
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }
    ThirdLinemaintr.firstChild.append(ThirdLinetextFrom);
    ThirdLinemaintr.firstChild.append(ThirdLinefromCityUse);  
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);
    ThirdLinemaintr.firstChild.append(ThirdLinetoGeoZoneUse);  

  }else if(ThirdLineinputText == "Geozona - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }
    ThirdLinemaintr.firstChild.append(ThirdLinetextFrom);
    ThirdLinemaintr.firstChild.append(ThirdLinefromGeoZoneUse);  
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);
    ThirdLinemaintr.firstChild.append(ThirdLinetoCityUse);  

  }else if(ThirdLineinputText == "LTU - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse.remove();
      ThirdLinetoCityUse.remove();
      ThirdLinefromDistrictUse.remove();
      ThirdLinetoDistrictUse.remove();
      ThirdLinefromGeoZoneUse.remove();
      ThirdLinetoGeoZoneUse.remove();
      ThirdLinetextTo.remove();
      ThirdLinetextFrom.remove();
    }
    ThirdLinemaintr.firstChild.append(ThirdLinetextTo);
    ThirdLinemaintr.firstChild.append(ThirdLinetoCityUse);

  }else if(ThirdLineinputText == "LTU - LTU"){
    ThirdLinefromCityUse.remove();
    ThirdLinetoCityUse.remove();
    ThirdLinefromDistrictUse.remove();
    ThirdLinetoDistrictUse.remove();
    ThirdLinefromGeoZoneUse.remove();
    ThirdLinetoGeoZoneUse.remove();
    ThirdLinetextTo.remove();
    ThirdLinetextFrom.remove();
    
  }else{
    ThirdLinefromCityUse.remove();
    ThirdLinetoCityUse.remove();
    ThirdLinefromDistrictUse.remove();
    ThirdLinetoDistrictUse.remove();
    ThirdLinefromGeoZoneUse.remove();
    ThirdLinetoGeoZoneUse.remove();
    ThirdLinetextTo.remove();
    ThirdLinetextFrom.remove();
  }  
  // console.log(ThirdLineinputText);  
}


// Antras

theadThird.getElementsByTagName("select")[1].onchange = function() {
  let index = this.selectedIndex;
  let ThirdLineinputText2 = this.children[index].innerHTML.trim();
  let getShow = maintable3.children[0].children[0].children[1]; 

  if(ThirdLineinputText2 == "Miestas - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }  
    ThirdLinemaintr.children[1].append(ThirdLinetextFrom2);
    ThirdLinemaintr.children[1].append(ThirdLinefromCityUse2);
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);
    ThirdLinemaintr.children[1].append(ThirdLinetoCityUse2);     
  
  
  }else if(ThirdLineinputText2 == "Miestas - Rajonas"){
    if(getShow != null){          
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }
    ThirdLinemaintr.children[1].append(ThirdLinetextFrom2);
    ThirdLinemaintr.children[1].append(ThirdLinefromCityUse2);
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);     
    ThirdLinemaintr.children[1].append(ThirdLinetoDistrictUse2);  
    
  }else if(ThirdLineinputText2 == "Rajonas - Miestas"){
    if(getShow != null) {       
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }
    ThirdLinemaintr.children[1].append(ThirdLinetextFrom2);
    ThirdLinemaintr.children[1].append(ThirdLinefromDistrictUse2);
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);  
    ThirdLinemaintr.children[1].append(ThirdLinetoCityUse2);     

  }else if(ThirdLineinputText2 == "Rajonas - Rajonas"){
    if(getShow != null) {
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }
    ThirdLinemaintr.children[1].append(ThirdLinetextFrom2);
    ThirdLinemaintr.children[1].append(ThirdLinefromDistrictUse2);  
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);
    ThirdLinemaintr.children[1].append(ThirdLinetoDistrictUse2);
    
  }else if(ThirdLineinputText2 == "Geozona - Geozona"){
    if(getShow != null) {
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }
    ThirdLinemaintr.children[1].append(ThirdLinetextFrom2);
    ThirdLinemaintr.children[1].append(ThirdLinefromGeoZoneUse2);  
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);
    ThirdLinemaintr.children[1].append(ThirdLinetoGeoZoneUse2);  
    
  }else if(ThirdLineinputText2 == "Miestas - Geozona"){
    if(getShow != null) {
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }
    ThirdLinemaintr.children[1].append(ThirdLinetextFrom2);
    ThirdLinemaintr.children[1].append(ThirdLinefromCityUse2);  
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);
    ThirdLinemaintr.children[1].append(ThirdLinetoGeoZoneUse2);  

  }else if(ThirdLineinputText2 == "Geozona - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }
    ThirdLinemaintr.children[1].append(ThirdLinetextFrom2);
    ThirdLinemaintr.children[1].append(ThirdLinefromGeoZoneUse2);  
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);
    ThirdLinemaintr.children[1].append(ThirdLinetoCityUse2);  

  }else if(ThirdLineinputText2 == "LTU - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse2.remove();
      ThirdLinetoCityUse2.remove();
      ThirdLinefromDistrictUse2.remove();
      ThirdLinetoDistrictUse2.remove();
      ThirdLinefromGeoZoneUse2.remove();
      ThirdLinetoGeoZoneUse2.remove();
      ThirdLinetextTo2.remove();
      ThirdLinetextFrom2.remove();
    }
    ThirdLinemaintr.children[1].append(ThirdLinetextTo2);
    ThirdLinemaintr.children[1].append(ThirdLinetoCityUse2);

  }else if(ThirdLineinputText2 == "LTU - LTU"){
    ThirdLinefromCityUse2.remove();
    ThirdLinetoCityUse2.remove();
    ThirdLinefromDistrictUse2.remove();
    ThirdLinetoDistrictUse2.remove();
    ThirdLinefromGeoZoneUse2.remove();
    ThirdLinetoGeoZoneUse2.remove();
    ThirdLinetextTo2.remove();
    ThirdLinetextFrom2.remove();
    
  }else{
    ThirdLinefromCityUse2.remove();
    ThirdLinetoCityUse2.remove();
    ThirdLinefromDistrictUse2.remove();
    ThirdLinetoDistrictUse2.remove();
    ThirdLinefromGeoZoneUse2.remove();
    ThirdLinetoGeoZoneUse2.remove();
    ThirdLinetextTo2.remove();
    ThirdLinetextFrom2.remove();
  }  
  // console.log(ThirdLineinputText2);  
  
}

// Trecias

theadThird.getElementsByTagName("select")[2].onchange = function() {
  let index = this.selectedIndex;
  let ThirdLineinputText3 = this.children[index].innerHTML.trim();
  let getShow = maintable3.children[0].children[0].children[2]; 

  if(ThirdLineinputText3 == "Miestas - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }  
    ThirdLinemaintr.lastChild.append(ThirdLinetextFrom3);
    ThirdLinemaintr.lastChild.append(ThirdLinefromCityUse3);
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);
    ThirdLinemaintr.lastChild.append(ThirdLinetoCityUse3);     
  
  
  }else if(ThirdLineinputText3 == "Miestas - Rajonas"){
    if(getShow != null){          
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }
    ThirdLinemaintr.lastChild.append(ThirdLinetextFrom3);
    ThirdLinemaintr.lastChild.append(ThirdLinefromCityUse3);
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);     
    ThirdLinemaintr.lastChild.append(ThirdLinetoDistrictUse3);  
    
  }else if(ThirdLineinputText3 == "Rajonas - Miestas"){
    if(getShow != null) {       
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }
    ThirdLinemaintr.lastChild.append(ThirdLinetextFrom3);
    ThirdLinemaintr.lastChild.append(ThirdLinefromDistrictUse3);
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);  
    ThirdLinemaintr.lastChild.append(ThirdLinetoCityUse3);     

  }else if(ThirdLineinputText3 == "Rajonas - Rajonas"){
    if(getShow != null) {
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }
    ThirdLinemaintr.lastChild.append(ThirdLinetextFrom3);
    ThirdLinemaintr.lastChild.append(ThirdLinefromDistrictUse3);  
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);
    ThirdLinemaintr.lastChild.append(ThirdLinetoDistrictUse3);
    
  }else if(ThirdLineinputText3 == "Geozona - Geozona"){
    if(getShow != null) {
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }
    ThirdLinemaintr.lastChild.append(ThirdLinetextFrom3);
    ThirdLinemaintr.lastChild.append(ThirdLinefromGeoZoneUse3);  
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);
    ThirdLinemaintr.lastChild.append(ThirdLinetoGeoZoneUse3);  
    
  }else if(ThirdLineinputText3 == "Miestas - Geozona"){
    if(getShow != null) {
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }
    ThirdLinemaintr.lastChild.append(ThirdLinetextFrom3);
    ThirdLinemaintr.lastChild.append(ThirdLinefromCityUse3);  
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);
    ThirdLinemaintr.lastChild.append(ThirdLinetoGeoZoneUse3);  

  }else if(ThirdLineinputText3 == "Geozona - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }
    ThirdLinemaintr.lastChild.append(ThirdLinetextFrom3);
    ThirdLinemaintr.lastChild.append(ThirdLinefromGeoZoneUse3);  
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);
    ThirdLinemaintr.lastChild.append(ThirdLinetoCityUse3);  

  }else if(ThirdLineinputText3 == "LTU - Miestas"){
    if(getShow != null) {
      ThirdLinefromCityUse3.remove();
      ThirdLinetoCityUse3.remove();
      ThirdLinefromDistrictUse3.remove();
      ThirdLinetoDistrictUse3.remove();
      ThirdLinefromGeoZoneUse3.remove();
      ThirdLinetoGeoZoneUse3.remove();
      ThirdLinetextTo3.remove();
      ThirdLinetextFrom3.remove();
    }
    ThirdLinemaintr.lastChild.append(ThirdLinetextTo3);
    ThirdLinemaintr.lastChild.append(ThirdLinetoCityUse3);

  }else if(ThirdLineinputText3 == "LTU - LTU"){
    ThirdLinefromCityUse3.remove();
    ThirdLinetoCityUse3.remove();
    ThirdLinefromDistrictUse3.remove();
    ThirdLinetoDistrictUse3.remove();
    ThirdLinefromGeoZoneUse3.remove();
    ThirdLinetoGeoZoneUse3.remove();
    ThirdLinetextTo3.remove();
    ThirdLinetextFrom3.remove();
    
  }else{
    ThirdLinefromCityUse3.remove();
    ThirdLinetoCityUse3.remove();
    ThirdLinefromDistrictUse3.remove();
    ThirdLinetoDistrictUse3.remove();
    ThirdLinefromGeoZoneUse3.remove();
    ThirdLinetoGeoZoneUse3.remove();
    ThirdLinetextTo3.remove();
    ThirdLinetextFrom3.remove();
  } 

}

/****************************************************************************************************** */
  // KETVIRTA EILE
/****************************************************************************************************** */


fourtTable = document.createElement("table");
tables.append(fourtTable);
fourtTable.setAttribute("class", "container text-center");



const maintable4 = document.getElementsByClassName("container")[3];

let fourtLineFirst = tbody.children[9];
let fourtLineSecond = tbody.children[10];
let fourtLineThird = tbody.children[11];
let fourtLineFourt = tbody.children[12];

fourtLineFirst.remove();
fourtLineSecond.remove();
fourtLineThird.remove();
fourtLineFourt.remove();

fourtLineFirst.getElementsByTagName("div")[0].setAttribute("id", "selectas"); 


const theadFourt = document.createElement("thead");
maintable4.append(theadFourt);
const tbodyFourt = document.createElement("tbody");
maintable4.append(tbodyFourt);


// // Create tr and td tags
const maintrToAppendFourt = document.createElement("tr");
let maintdFourtFirst = document.createElement("td");
maintdFourtFirst.append(fourtLineFirst.children[1]);

maintrToAppendFourt.append(maintdFourtFirst);
theadFourt.append(maintrToAppendFourt);


const FourtLinefromCity = fourtLineSecond.children[1];
const FourtLinetoCity =  fourtLineSecond.children[3];

const FourtLinefromDistrict = fourtLineThird.children[1];
const FourtLinetoDistrict = fourtLineThird.children[3];

const FourtLinefromGeozone = fourtLineFourt.children[1];
const FourtLinetoGeozone = fourtLineFourt.children[3];


const FourtLinefromCityUse = document.createElement("div");
FourtLinefromCityUse.setAttribute("id", "show");
FourtLinefromCityUse.appendChild(FourtLinefromCity);

const FourtLinetoCityUse = document.createElement("div");
FourtLinetoCityUse.setAttribute("id", "show");
FourtLinetoCityUse.appendChild(FourtLinetoCity);


const FourtLinefromDistrictUse = document.createElement("div");
FourtLinefromDistrictUse.setAttribute("id", "show");
FourtLinefromDistrictUse.append(FourtLinefromDistrict);

const FourtLinetoDistrictUse = document.createElement("div");
FourtLinetoDistrictUse.setAttribute("id", "show");
FourtLinetoDistrictUse.append(FourtLinetoDistrict);


const FourtLinefromGeoZoneUse = document.createElement("div");
FourtLinefromGeoZoneUse.setAttribute("id", "show");
FourtLinefromGeoZoneUse.append(FourtLinefromGeozone);

const FourtLinetoGeoZoneUse = document.createElement("div");
FourtLinetoGeoZoneUse.setAttribute("id", "show");
FourtLinetoGeoZoneUse.append(FourtLinetoGeozone);


const FourtLinemaintr = document.createElement("tr");
const FourtLinepreparedTd = document.createElement("td");

FourtLinemaintr.setAttribute("id", "maintr");
FourtLinemaintr.append(FourtLinepreparedTd);
tbodyFourt.append(FourtLinemaintr);
tbodyFourt.insertBefore(FourtLinemaintr, tbodyFourt.firstChild);


const FourtLinetextIs = document.innerText = 'Iš ';
const FourtLinetextI = document.innerText = 'Į ';
const FourtLinetextFrom = document.createElement("div");
FourtLinetextFrom.setAttribute("id","show-text");
FourtLinetextFrom.append(FourtLinetextIs);

const FourtLinetextTo = document.createElement("div");
FourtLinetextTo.setAttribute("id","show-text");
FourtLinetextTo.append(FourtLinetextI);


theadFourt.getElementsByTagName("select")[0].onchange = function() {
  let index = this.selectedIndex;
  let FourtLineinputText = this.children[index].innerHTML.trim();
  let getShow = maintable4.children[0].children[0].children[0]; 

  if(FourtLineinputText == "Miestas - Miestas"){
    if(getShow != null) {
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }  
    FourtLinemaintr.firstChild.append(FourtLinetextFrom);
    FourtLinemaintr.firstChild.append(FourtLinefromCityUse);
    FourtLinemaintr.firstChild.append(FourtLinetextTo);
    FourtLinemaintr.firstChild.append(FourtLinetoCityUse);     
  
  
  }else if(FourtLineinputText == "Miestas - Rajonas"){
    if(getShow != null){          
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }
    FourtLinemaintr.firstChild.append(FourtLinetextFrom);
    FourtLinemaintr.firstChild.append(FourtLinefromCityUse);
    FourtLinemaintr.firstChild.append(FourtLinetextTo);     
    FourtLinemaintr.firstChild.append(FourtLinetoDistrictUse);  
    
  }else if(FourtLineinputText == "Rajonas - Miestas"){
    if(getShow != null) {       
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }
    FourtLinemaintr.firstChild.append(FourtLinetextFrom);
    FourtLinemaintr.firstChild.append(FourtLinefromDistrictUse);
    FourtLinemaintr.firstChild.append(FourtLinetextTo);  
    FourtLinemaintr.firstChild.append(FourtLinetoCityUse);     

  }else if(FourtLineinputText == "Rajonas - Rajonas"){
    if(getShow != null) {
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }
    FourtLinemaintr.firstChild.append(FourtLinetextFrom);
    FourtLinemaintr.firstChild.append(FourtLinefromDistrictUse);  
    FourtLinemaintr.firstChild.append(FourtLinetextTo);
    FourtLinemaintr.firstChild.append(FourtLinetoDistrictUse);
    
  }else if(FourtLineinputText == "Geozona - Geozona"){
    if(getShow != null) {
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }
    FourtLinemaintr.firstChild.append(FourtLinetextFrom);
    FourtLinemaintr.firstChild.append(FourtLinefromGeoZoneUse);  
    FourtLinemaintr.firstChild.append(FourtLinetextTo);
    FourtLinemaintr.firstChild.append(FourtLinetoGeoZoneUse);  
    
  }else if(FourtLineinputText == "Miestas - Geozona"){
    if(getShow != null) {
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }
    FourtLinemaintr.firstChild.append(FourtLinetextFrom);
    FourtLinemaintr.firstChild.append(FourtLinefromCityUse);  
    FourtLinemaintr.firstChild.append(FourtLinetextTo);
    FourtLinemaintr.firstChild.append(FourtLinetoGeoZoneUse);  

  }else if(FourtLineinputText == "Geozona - Miestas"){
    if(getShow != null) {
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }
    FourtLinemaintr.firstChild.append(FourtLinetextFrom);
    FourtLinemaintr.firstChild.append(FourtLinefromGeoZoneUse);  
    FourtLinemaintr.firstChild.append(FourtLinetextTo);
    FourtLinemaintr.firstChild.append(FourtLinetoCityUse);  

  }else if(FourtLineinputText == "LTU - Miestas"){
    if(getShow != null) {
      FourtLinefromCityUse.remove();
      FourtLinetoCityUse.remove();
      FourtLinefromDistrictUse.remove();
      FourtLinetoDistrictUse.remove();
      FourtLinefromGeoZoneUse.remove();
      FourtLinetoGeoZoneUse.remove();
      FourtLinetextTo.remove();
      FourtLinetextFrom.remove();
    }
    FourtLinemaintr.firstChild.append(FourtLinetextTo);
    FourtLinemaintr.firstChild.append(FourtLinetoCityUse);

  }else if(FourtLineinputText == "LTU - LTU"){
    FourtLinefromCityUse.remove();
    FourtLinetoCityUse.remove();
    FourtLinefromDistrictUse.remove();
    FourtLinetoDistrictUse.remove();
    FourtLinefromGeoZoneUse.remove();
    FourtLinetoGeoZoneUse.remove();
    FourtLinetextTo.remove();
    FourtLinetextFrom.remove();
    
  }else{
    FourtLinefromCityUse.remove();
    FourtLinetoCityUse.remove();
    FourtLinefromDistrictUse.remove();
    FourtLinetoDistrictUse.remove();
    FourtLinefromGeoZoneUse.remove();
    FourtLinetoGeoZoneUse.remove();
    FourtLinetextTo.remove();
    FourtLinetextFrom.remove();
  }  
  // console.log(FourtLineinputText);  
  
}




// let listItemRemoveClass = document.getElementsByTagName("body");
// // listItemRemoveClass.classList.remove("select2-selected");

// listItemRemoveClass.click = function() {      
//  alert('suveike');      
// }

// console.log(listItemRemoveClass);





};