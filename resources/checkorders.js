let page = new URLSearchParams(window.location.search);
if (page.get('module') == 'SalesOrder' && page.get('view') == 'List') {




  $(document).ready(function () {
    createCheckEvent();
    createCheckEvent2();
  });

  // $( document ).ajaxComplete(function() {
  //  	createCheckEvent();
  //  	createCheckEvent2();
  // });

}

function createCheckEvent() {
  document.getElementById('sinchro').classList.remove('hide');
  let refreshBtn = $('#refresh-info');
  let loader = $('#spin');
  refreshBtn.click(() => {
    refreshBtn.hide();
    loader.show();
    getMetrikaOrders();
  });
}

function createCheckEvent2() {
  let refreshBtn = $('#refresh-info2');
  refreshBtn.click(() => {
    importMetrika();
  });
}



function getMetrikaOrders() {

  var today = new Date();
  var day = String(today.getDate()).padStart(2, '0');
  var month = String(today.getMonth() + 1).padStart(2, '0');
  var year = today.getFullYear();
  today = year + '-' + month + '-' + day;

  const url = 'https://uzsakymai.parnasas.lt/export/crm/orders.php';
  const user = '123';
  const password = 'raktas';
  const date = today;
  const formdata = new FormData();

  formdata.append('user', user);
  formdata.append('password', password);
  formdata.append('date_from', date);
  formdata.append('date_to', date);

  fetch(url, { method: 'POST', body: formdata })
    .then((resp) => resp.json())
    .then(function (data) {
      $('#metrika').html(data.order.length);
      getTodayOrders();
    });
}

function getTodayOrders() {
  let refreshBtn = $('#refresh-info');
  let loader = $('#spin');
  $.ajax({
    type: "POST",
    url: "checkorders/checktodayorders.php",
    data: { 1: 1 },
    success: function (result) {
      $('#crm_orders').html(result);
      loader.hide();
      refreshBtn.show();
    }
  });
}



function importMetrika() {

  if (document.querySelector('.metrika_result') != null) {
    $('#metrika_table').hide();
    let tr = document.querySelectorAll('.metrika_result');
    for (let i = 0; i < tr.length; i++) {
      tr[i].remove();
    }
  }

  if (document.querySelector('.crm_result') != null) {
    $('#crm_table').hide();
    let tr = document.querySelectorAll('.crm_result');
    for (let i = 0; i < tr.length; i++) {
      tr[i].remove();
    }
  }

  let import_metrika = true;


  $.ajax({
    type: "POST",
    url: "checkorders/importMetrika.php",
    data: { import_metrika: import_metrika },
    dataType: 'JSON',
    beforeSend: function () {
      $('#ordersMatchSpinner').show();
    },
    success: function (result) {
      console.log(result);
      if (result == 'success_m') {
        importSalesOrders();
      } else {
        alert('Įvyko klaida');
        $('[data-dismiss="modal"]').click();
      }
    }
  });
}

function importSalesOrders() {
  let import_salesorder = true;
  $.ajax({
    type: "POST",
    url: "checkorders/importSalesOrders.php",
    data: { import_salesorder: import_salesorder },
    dataType: 'JSON',
    success: function (result) {
      console.log(result);
      if (result == 'success_s') {
        showResults();
      } else {
        alert('Įvyko klaida');
        $('[data-dismiss="modal"]').click();
      }
    }

  });
}

function showResults() {
  let notMetrikaTable;
  let notCrmTable;
  let metrikaTable = document.getElementById('not_metrika_items');
  let crmTable = document.getElementById('not_crm_items');

  let show_results = true;

  $.ajax({
    type: "POST",
    url: "checkorders/showResults.php",
    data: { show_results: show_results },
    dataType: 'JSON',
    success: function (result) {
      notMetrikaTable = '';
      notCrmTable = '';
      $('#ordersMatchSpinner').hide();
      if (result.not_inserted_to_metrika != '' && result.not_inserted_to_metrika != undefined) {
        result.not_inserted_to_metrika.forEach(not_metrika_items => {
          $('#metrika_table').show();
          notMetrikaTable += `
        <tr class="listViewHeaders metrika_result">					
          <td class="listViewEntryValue">${(not_metrika_items.createdtime != null ? not_metrika_items.createdtime : '')}</td>
          <td class="listViewEntryValue" align="center">${(not_metrika_items.shipment_code != null ? not_metrika_items.shipment_code : '')}</td>
          <td class="listViewEntryValue">${(not_metrika_items.accountname != null ? not_metrika_items.accountname : '')}</td>
          <td class="listViewEntryValue">${(not_metrika_items.load_company != null ? not_metrika_items.load_company : '')}</td>
          <td class="listViewEntryValue">${(not_metrika_items.unload_company ? not_metrika_items.unload_company : '')}</td>
          <td class="listViewEntryValue">${(not_metrika_items.update_date != null ? not_metrika_items.update_date : '')}</td>
        </tr>`;

        });
        metrikaTable.innerHTML = notMetrikaTable;
      } else {
        $('#metrika_all').show();
      }

      if (result.not_inserted_to_crm != '' && result.not_inserted_to_crm != undefined) {
        $('#crm_table').show();
        result.not_inserted_to_crm.forEach(not_crm_items => {
          notCrmTable += `
          <tr class="listViewHeaders crm_result">		
            <td class="listViewEntryValue" align="center">${(not_crm_items.order_date != null ? not_crm_items.order_date : '')}</td>
            <td class="listViewEntryValue" align="center">${(not_crm_items.shipment_code) ? not_crm_items.shipment_code : ''}</td>
            <td class="listViewEntryValue">${(not_crm_items.load_company_name ? not_crm_items.load_company_name : '')}</td>
            <td class="listViewEntryValue">${(not_crm_items.unload_company_name ? not_crm_items.unload_company_name : '')}</td>
            <td class="listViewEntryValue">${(not_crm_items.update_metrika ? not_crm_items.update_metrika : '')}</td>
          </tr>`;
          crmTable.innerHTML = notCrmTable;
        });
      } else {
        $('#crm_all').show();
      }

    }
  });
}

