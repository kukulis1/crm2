let link = new URLSearchParams(window.location.search);



function filterSalesOrderExport() {
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let parameters = '';
  let viewname = document.querySelector('[name="cvid"]').value;

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      parameters += `${form_key}=${form_value}:`;
    }
  }

  parameters = parameters.slice(0, -1);
  const exportBtn = document.querySelector('#export_sales');
  const exportDetailBtn = document.querySelector('#export_detail_sales');
  exportBtn.setAttribute('onclick', `window.location.href = 'vtlib/Vtiger/exel/sales_order_export.php?viewname=${viewname}&search_parameters=${parameters}'`);
  exportDetailBtn.setAttribute('onclick', `window.location.href = 'vtlib/Vtiger/exel/sales_order_detail_export.php?viewname=${viewname}&search_parameters=${parameters}'`);
}



if (link.get('module') == 'SalesOrder' && address.get('view') == 'List') {
  $(document).ready(function () {
    if(document.querySelector('#export_detail_sales') != null){
      if(document.querySelector('#export_detail_sales') != null){
       filterSalesOrderExport();
      }
    }
  });

  $(document).ajaxComplete(function () {
    if(document.querySelector('#export_detail_sales') != null){
      if(document.querySelector('#export_detail_sales') != null){
        filterSalesOrderExport();
       }
    }
  });
}



if (link.get('module') == 'SalesOrder' && link.get('view') == 'Edit') {
  $(document).ready(function () {
    if (link.get('record') != null) {
      let check_service = document.querySelectorAll('.selectedModuleId');
      for (let i = 1, len = check_service.length; i < len; i++) {
        if (check_service[i].value == '36641') {
          loadDetails(i);
        }
      }
    }

    checkComeFrom();
  });
if(document.getElementById('addService')){
  document.getElementById('addService').onclick = () => {
    setTimeout(() => {
      let last = document.querySelectorAll('.lineItemRow:not(.lineItemCloneCopy)').length;
      let count = document.querySelectorAll('.lineItemRow:not(.lineItemCloneCopy)')[last - 1].dataset.rowNum;
      loadDetails(count);
    }, 1)
  }
}

  $('.deleteRow').click(() => {
    setPriceAfterDeleteRow();
  });

  var isInt = function(e,field) {
    if(!isNaN(e.value)){
      document.getElementById(field).value = '';
    }
  }

  var isString = function(e,field) {
    let billCountry = $('#SalesOrder_editView_fieldName_bill_country').val();
    let shipCountry = $('#SalesOrder_editView_fieldName_ship_country').val();

    if((field == 'SalesOrder_editView_fieldName_bill_code' && billCountry.toLowerCase() == 'pol') ||
     (field == 'SalesOrder_editView_fieldName_ship_code' && shipCountry.toLowerCase() == 'pol')){}
    else{      
      if(isNaN(e.value)){
        document.getElementById(field).value = '';
      }
    }
  }

}




if (link.get('module') == 'SalesOrder' && link.get('view') == 'Detail') {
  $(document).ajaxComplete(function () {
    if (document.querySelector('#SalesOrder_detailView_fieldValue_cf_1556 .action') != null) {
      document.querySelector('#SalesOrder_detailView_fieldValue_cf_1556 .action').onclick = (e) => {
        let orderId = link.get('record');
        let parent = e.target.parentElement.parentElement;
        setTimeout(() => {
          let saveBtn = parent.querySelector('.inlineAjaxSave');
          saveBtn.onclick = () => {
            let input = document.querySelector('[name="cf_1556"]').value;
            saveFuelPrice(orderId, input, edit = false);
          }
        }, 100);


      }
    }

  });
} else if (link.get('module') == 'SalesOrder' && link.get('view') == 'Edit') {
  let orderId = link.get('record');
  document.querySelector('#SalesOrder_editView_fieldName_cf_1556').onkeyup = () => {
    let input = document.querySelector('#SalesOrder_editView_fieldName_cf_1556').value;
    saveFuelPrice(orderId, input, edit = true);
  }
}

function saveFuelPrice(orderId, input, edit) {
  $.ajax({
    type: "POST",
    url: "salesorders/fuelPrice.php",
    data: { orderId: orderId, input: input, edit: edit },
    success: function (res) {
    }
  });
}


function loadDetails(i) {
  let productName = document.getElementById(`productName${i}`);

  document.getElementById(`cargo_wgt${i}`).style = "display: none;";
  document.getElementById(`cargo_length${i}`).style = "display: none;";
  document.getElementById(`cargo_width${i}`).style = "display: none;";
  document.getElementById(`cargo_height${i}`).style = "display: none;";


  productName.value = 'Services';

  document.getElementById(`measure${i}`).style = "display: none;";
  document.getElementById(`hdnProductId${i}`).value = 36641;
  document.getElementById(`service_select${i}`).style = 'width: 100%; display:block;margin-left: 70px;';
  document.getElementById(`service_div${i}`).style = 'display:block;';
  document.getElementById(`lineItemType${i}`).value = "Services";
  document.getElementById(`row${i}`).classList.add('except');
  setPrice(i);
}

function setPrice(i) {
  let grandTotal = document.getElementById('grandTotal');

  let hidePrice = new Array();

  document.getElementById(`service_price${i}`).onchange = (e) => {
    let count = document.querySelectorAll('.lineItemRow').length;
    let type_price = e.target.value;
    if (type_price == '') type_price = 0;
    document.querySelector(`[name='margin${i}']`).value = type_price;
    for (e = 0; e < count; e++) {
      hidePrice[e] = document.querySelector(`[name='margin${e + 1}']`).value;
    }
    let all = hidePrice.map(Number).reduce((a, b) => { return a + b });
    grandTotal.innerText = parseFloat(all).toFixed(2);
    $(`[name='margin${i}']`).val(parseFloat(type_price).toFixed(2));
    $(`#listPrice${i}`).val(parseFloat(type_price).toFixed(2));
  }
}

function setPriceAfterDeleteRow() {
  setTimeout(() => {
    let grandTotal = document.getElementById('grandTotal');
    let row = document.querySelectorAll('.lineItemRow');
    let rowArr = new Array();
    for (i = 0; i < row.length; i++) {
      let count = row[i].dataset.rowNum;
      rowArr.push($(`[name='margin${count}']`).val());
    }
    let rez = rowArr.map(Number).reduce((a, b) => { return a + b });
    grandTotal.innerText = parseFloat(rez).toFixed(2);
  }, 1);
}

function checkComeFrom() {
  let rows = document.querySelectorAll('#lineItemTab .lineItemRow:not(.except)');
  for (i = 1; i <= rows.length; i++) {
    let come = document.getElementById(`come_from${i}`);
    if (come.value == 1) {
      let margin = $(`[name='margin${i}']`).val();
      let newMargin = margin / rows.length;
      margin = newMargin;
      $(`[name='margin${i}']`).val(margin);
    }
  }
}