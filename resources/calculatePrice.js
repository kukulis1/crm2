$(document).ready(function () {

  let url = new URLSearchParams(window.location.search);


  if ((url.get('module') == 'SalesOrder' || url.get('module') == 'Invoice') && url.get('view') == 'Edit') {
    setTimeout(() => {
      $('#lineItemTab').sortable('destroy');
    }, 1000);

  }

  if (url.get('module') == 'SalesOrder' && url.get('view') == 'Edit') {
    const totalHidden = document.querySelector('#grandTotal').innerHTML;

    $('span.clearLineItem').attr('id', 'clearfield-1');
    $('span.clearLineItem').removeClass('clearLineItem');


    //Paleidzia kainos perskaiciavima jei irasomas faktinis turis
    if (url.get('record') != null) {
      $('#SalesOrder_editView_fieldName_cf_1365').change(function () {
        trigger();
      });
    }


    const blockPrice = document.querySelectorAll("[name='editContent']")[0];

    if (totalHidden !== '') {
      setTimeout(function () {
        $('#grandTotal').html(totalHidden);
      }, 200);
    }

    let agreed_price = $('#SalesOrder_editView_fieldName_cf_1376');

    agreed_price.keyup(function () {
      let grand = $('#grandTotal');
      let pricebook_price = $('#SalesOrder_editView_fieldName_cf_1374');
      let agreed_price_desc = document.getElementById('SalesOrder_editView_fieldName_cf_1378');


      if ($(this).val() != '') {
        $('#SalesOrder_editView_fieldName_cf_1297').val('Sutarta kaina');
        // document.querySelector('.disabled_create_btn').removeAttribute('disabled');
      } else {
        // document.querySelector('.disabled_create_btn').setAttribute('disabled', 'disabled');
        $('#SalesOrder_editView_fieldName_cf_1297').val('');
      }

      agreed = agreed_price.val();
      if (agreed == '') { agreed = pricebook_price.val(); }

      if (parseFloat(pricebook_price.val()).toFixed(2) != parseFloat(agreed).toFixed(2)) {
        agreed_price_desc.setAttribute('required', 'required');
      } else if (parseFloat(pricebook_price.val()).toFixed(2) == parseFloat(agreed).toFixed(2)) {
        agreed_price_desc.removeAttribute('required');
      }
      grand.html(parseFloat(agreed).toFixed(2));
    });


    let createUser = _USERMETA.id;
    let createUserId = document.createElement('input');
    let userName = document.querySelector('#account_id_display');
    createUserId.setAttribute('name', 'create_user_id');
    createUserId.setAttribute('value', '');
    createUserId.setAttribute('type', 'hidden');
    createUserId.value = createUser;
    userName.append(createUserId);

    const divas = document.createElement("div");
    divas.setAttribute('id', 'wait');
    divas.setAttribute('style', 'display:none;position:absolute;top: 75%;left:50%;padding:2px;z-index: 1001;');
    const img = document.createElement('img');
    img.setAttribute('src', '/resources/lg.ajax-spinner-gif.gif');
    img.setAttribute("width", '256');
    img.setAttribute("height", '256');
    divas.append(img);
    blockPrice.append(divas);

    const alert = document.createElement("div");
    alert.setAttribute("id", "alert");
    alert.setAttribute("display", "block");
    alert.setAttribute("style", "color:red;margin-left: 90px;margin-top: 10px;");


    var radios = $('input[type="radio"]');
    function checkRadio() {
      return radios.filter(':checked').filter(function () {
        return (this.value == '+');
      }).length > 0;
    }


    let total2 = '';

    function bindButton() {
      $("#adjustment").unbind('mouseover').one("mouseover", function () {
        total2 = $("#grandTotal").html();
      });
    }
    bindButton();


    let total = '';
    function totalForQanty() {
      total = $('#grandTotal').html();
    }




    function inRange() {
      if (checkRadio()) {
        adjustment = $("#adjustment").val();
        grandTotal = (parseFloat(total2) + parseFloat(adjustment));
        $("#grandTotal").html(parseFloat(grandTotal).toFixed(2));
      } else {
        adjustment = $("#adjustment").val();
        total = $("#grandTotal").html();
        grandTotal = (parseFloat(total2) - parseFloat(adjustment));
        $("#grandTotal").html(parseFloat(grandTotal).toFixed(2));

      }
    }

    if ($("#grandTotal").html() != '0.00') {
      inRange();
    }

    $('#adjustment').change(inRange);
    radios.change(inRange);

    function getPrice() {

      let accId = document.querySelector('[name="account_id"').value;
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow:not(.except)');
      let cargoKgArray = new Array();
      let cargoKgArray2 = new Array();
      let values = new Array();
      let values_m2 = new Array();
      let cargoVolume = new Array();
      let cargoVolume2 = new Array();
      let cargoSquare;
      let cargoSquare2 = new Array();
      let cargoTemp = new Array();
      let cargoTemp2 = new Array();
      let cargoKg;
      let dimensions;
      let dimensions_m2;
      let weight;
      let length;
      let width;
      let height;
      let measure;
      let measures = new Array();
      let pll = 0;
      let meters = new Array();
      let meters2;
      let globalTaxableMeter = ($('#accMetersValue').val() == 0 || $('#accMetersValue').val() == '' ? $('#globalMetersValue').val() : $('#accMetersValue').val());
      let taxableMeters = false;


      for (let e = 0, len = rows.length; e < len; e++) {
        c = rows[e].dataset.rowNum;

        weight = $('#cargo_wgt' + c).val();
        length = $('#cargo_length' + c).val();
        width = $('#cargo_width' + c).val();
        height = $('#cargo_height' + c).val();
        measure = $('#measure' + c).val();
        skipMeters = $('#skipMeters' + c).val();

        measures.push(Number(measure));
        if ((length > globalTaxableMeter || width > globalTaxableMeter) && skipMeters == 0) {
          meters2 = (length > width ? length : width);
          meters.push(meters2);
          document.querySelector(`#row${c}`).classList.add('meters');
          document.querySelector(`#meter_price${c}`).classList.remove('hide');
          document.querySelector(`#meters${c}`).value = 1;
          taxableMeters = true;
        } else {
          document.querySelector(`#row${c}`).classList.remove('meters');
          document.querySelector(`#meter_price${c}`).classList.add('hide');
          document.querySelector(`#meters${c}`).value = '';
          dimensions = `${weight} ${length}x${width}x${height}`;
          dimensions_m2 = length * width;
          values.push(dimensions);
          values_m2.push(dimensions_m2);
          values = values.join();
          values = values.split(" ").join();
          values = values.split(",");
        }
      }


      let qantity = new Array();

      for (j = 0; j <= values.length; j = j + 2) {
        if (values[j] !== undefined) {
          cargoKgArray.push(Number(values[j]));
          cargoTemp.push(values[j + 1]);
        }
      }

      rows = document.querySelectorAll('#lineItemTab .lineItemRow:not(.except):not(.meters)');

      for (let m = 0; m < rows.length; m++) {
        b = rows[m].dataset.rowNum;
        qantity[m] = parseInt(document.getElementById(`qty${b}`).value);

        cargoTemp2[m] = cargoTemp[m];
        cargoTemp2[m] = cargoTemp2[m].replace(/X/g, 'x');
        cargoTemp2[m] = cargoTemp2[m].replace(/,/g, '.');
        cargoTemp2[m] = cargoTemp2[m].split("x");
        cargoTemp2[m] = cargoTemp2[m].map(Number).reduce(function (a, b) { return a * b; });
        cargoVolume[m] = cargoTemp2[m];

        cargoVolume2[m] = parseFloat(cargoVolume[m]) * parseFloat(qantity[m]);
        cargoSquare2[m] = parseFloat(values_m2[m]) * parseFloat(qantity[m]);
        cargoKgArray2[m] = parseFloat(cargoKgArray[m]);
      }


      if (cargoKgArray2.length != 0) cargoKg = cargoKgArray2.map(Number).reduce((a, b) => a + b, 0);
      if (cargoVolume2.length != 0) cargoVolume = cargoVolume2.map(Number).reduce(function (a, b) { return a + b; });
      if (cargoSquare2.length != 0) cargoSquare = cargoSquare2.map(Number).reduce(function (a, b) { return a + b; });

      cargoVolume = parseFloat(cargoVolume).toFixed(3);

      let revisedVolume = $('#SalesOrder_editView_fieldName_cf_1365').val();

      if (url.get('record') != null) {
        cargoVolume = (revisedVolume >= cargoVolume ? revisedVolume : cargoVolume);
      }


      if (cargoKg === undefined) cargoKg = 0;
      if (cargoVolume == 'NaN') cargoVolume = 0;
      if (cargoSquare === undefined) cargoSquare = 0;



      measures = measures.map(function (item) { return item == 2 || item == 3 ? 1 : item; });

      const found = measures.includes(1);
      let allEqual = false;

      if (found) {
        allEqual = measures.every((val, i, arr) => val === arr[0]);
      }

      if (allEqual) {
        pll = qantity.map(Number).reduce((a, b) => a + b, 0);
      }

      console.log(cargoKg + " kg");
      console.log(cargoVolume + " m3");
      console.log(cargoSquare + " m2");
      console.log(meters + " m");
      console.log(pll + " pll");
      rows = undefined;


      let fromPost = $('#SalesOrder_editView_fieldName_bill_code').val();
      let toPost = $('#SalesOrder_editView_fieldName_ship_code').val();
      let distance = 0;
      // let distance = $('#SalesOrder_editView_fieldName_cf_928').val();
      let from_country = $('#SalesOrder_editView_fieldName_bill_country').val();
      let to_country = $('#SalesOrder_editView_fieldName_ship_country').val();


      const appendAlert = document.querySelector(`#row${i} .input-group2`);
      const blockNum = document.querySelectorAll(".lineItemRow ").length;
      if (accId == '') {
        appendAlert.append(alert);
        $("#alert").html("Pirma įveskite kliento pavadinimą");
        $("#wait").css("display", "none");
      } else if (fromPost == '' && toPost == '') {
        appendAlert.append(alert);
        $("#alert").html("Pirma įveskite pašto kodą");
        $("#wait").css("display", "none");
      } else {
        // if (taxableMeters) {
        //   for (let x = 0; x < meters.length; x++) {
        //     sendAjaxRequest2(accId, fromPost, toPost, meters[x], from_country, to_country, x + 1);
        //   }
        // }
        sendAjaxRequest(accId, fromPost, toPost, cargoKg, cargoVolume, distance, cargoSquare, from_country, to_country, pll);
      }
    }

    function sendAjaxRequest(accId, fromPost, toPost, cargoKg, cargoVolume, distance, cargoSquare, from_country, to_country, pll) {
      $.ajax({
        type: "POST",
        url: "price-algorithm/index.php",
        data: { accId: accId, fromPost: fromPost, toPost: toPost, cargoKg: cargoKg, cargoVolume: cargoVolume, distance: distance, cargoSquare: cargoSquare, from_country: from_country, to_country: to_country, pll: pll },
        dataType: "JSON",
        success: function (result) {
          console.log(result);
          createPricesInput(result.price, 'standart', '');
          setTimeout(() => {
            if($('#SalesOrder_editView_fieldName_cf_1376').val() == ''){
              showPriceOrErrorInFrontEnd(result);
            }else{
              $("#wait").css("display", "none");
              if(result.price > 0){
                $('#SalesOrder_editView_fieldName_cf_1374').val(result.price);
                $('#SalesOrder_editView_fieldName_cf_1297').val(result.combination);
              }
            }
          }, 500);

        }
      });
    }

    function sendAjaxRequest2(accId, fromPost, toPost, meters, from_country, to_country, count) {
      let id = document.querySelectorAll('.meters')[count - 1].dataset.rowNum;
      $.ajax({
        type: "POST",
        url: "price-algorithm/meters.php",
        data: { accId: accId, fromPost: fromPost, toPost: toPost, meters: meters, from_country: from_country, to_country: to_country },
        dataType: "JSON",
        success: function (result) {
          console.log(result);
          console.log(result.price);
          if (result.price == 0) {
            console.log(id);

            $(`#skipMeters${id}`).val('1');
            $("#wait").css("display", "none");
            getPrice();
          } else {
            $(`#skipMeters${id}`).val('0');
            createPricesInput(result.price, 'meters', count);
          }
        }
      });
    }

    function createPricesInput(price, type, count) {
      if (type == 'standart') {
        if (!document.getElementById('standart')) {
          let pricesDiv = document.querySelector('#sumPrices');
          let input = document.createElement('input');
          input.setAttribute('id', 'standart');
          input.setAttribute('value', price);
          pricesDiv.append(input);
        } else {
          $('#standart').val(price);
        }

      } else {
        if (!document.getElementById(`meter${count}`)) {
          let pricesDiv = document.querySelector('#sumPrices');
          let input = document.createElement('input');
          input.setAttribute('id', `meter${count}`);
          input.setAttribute('value', price);
          pricesDiv.append(input);
        } else {
          $(`#meter${count}`).val(price);
        }

      }


    }

    function showPriceOrErrorInFrontEnd(result) {

      const appendAlert = document.querySelector(`#row${i} .input-group2`);

      let inputs = document.querySelectorAll('#sumPrices input');
      let prices = new Array();
      for (let i = 0; i < inputs.length; i++) {
        prices.push(inputs[i].value);
      }

      let price = prices.map(Number).reduce((a, b) => a + b, 0);

      result.price = price

      if (result.price !== 0) {
        setTimeout(function () {
          $("#wait").css("display", "none");
        }, 500);

        setMarginPrice(result.price);
        let serviceMargin = document.querySelectorAll('.except');
        if (serviceMargin.length != 0) {
          let sM = checkServiceMargins();
          result.price = parseFloat(result.price) + parseFloat(sM);

        }
        // $(`#hdnProductId${i}`).val(result.productid);
        $(`#lineItemType${i}`).val('Products'); 
        insertValue(result.price, i);
        $(`#productTotal${i}`).html(parseFloat(result.price).toFixed(2));
        $(`#totalAfterDiscount${i}`).html(parseFloat(result.price).toFixed(2));
        $(`#netPrice${i}`).html(parseFloat(result.price).toFixed(2));
        $(`#listPrice${i}`).val(parseFloat(result.price).toFixed(2));

        $('#productName' + i).attr('readonly', 'readonly');
        $('#SalesOrder_editView_fieldName_cf_1297').val(result.combination);
        $('#SalesOrder_editView_fieldName_cf_1572').val(result.stevedoring);


        let grand = $("#grandTotal");
        let pricebook_price = $('#SalesOrder_editView_fieldName_cf_1374');
        let agreed_price = $('#SalesOrder_editView_fieldName_cf_1376');
        // if(grand.html() == '0.00'){   
        let defaultas = true;
        let price_true = result.price;

        if (document.querySelector('#SalesOrder_editView_fieldName_cf_924').checked) {
          price_true = parseFloat(result.price / 100 * result.fast) + price_true;
          defaultas = false;
        }

        if (document.querySelector('#SalesOrder_editView_fieldName_cf_926').checked) {
          price_true = parseFloat(price_true) + parseFloat(result.loading_work);
          defaultas = false;
        }

        if (document.querySelector('#SalesOrder_editView_fieldName_cf_930').checked) {
          price_true = parseFloat(result.price / 100 * result.termo) + price_true;
          defaultas = false;
        }

        if (defaultas) {
          grand.html(parseFloat(result.price).toFixed(2));
          if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
            // agreed_price.val(parseFloat(result.price).toFixed(2));
          }
          pricebook_price.val(parseFloat(result.price).toFixed(2));

        } else {
          grand.html(parseFloat(price_true).toFixed(2));
          if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
            // agreed_price.val(parseFloat(price_true).toFixed(2));
          }
          pricebook_price.val(parseFloat(price_true).toFixed(2));
        }
        // }else if(blockNum <= 1 ){ 
        if (fastTrig == 1) {
          if (document.querySelector('#SalesOrder_editView_fieldName_cf_924').checked) {
            if (result.fast == 0) {
              let fastPlace = document.querySelector('#SalesOrder_editView_fieldName_cf_924').parentElement;
              let fastWarning = document.createElement('div');
              fastWarning.setAttribute('style', 'color:red;');
              fastWarning.setAttribute('id', 'fast-warning');
              fastWarning.innerText = "Skubus kaina nenustatyta";
              fastPlace.append(fastWarning);
            }
            grand.html((parseFloat(grand.html() / 100 * result.fast) + parseFloat(grand.html())).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
              // agreed_price.val((parseFloat(grand.html() / 100 * result.fast) + parseFloat(grand.html())).toFixed(2));
            }
            pricebook_price.val((parseFloat(grand.html() / 100 * result.fast) + parseFloat(grand.html())).toFixed(2));
          } else {
            let fastW = document.getElementById('fast-warning');
            if (fastW != undefined) {
              fastW.remove();
            }
            grand.html((parseFloat(grand.html()) - parseFloat(result.price / 100 * result.fast)).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
            //   agreed_price.val((parseFloat(grand.html()) - parseFloat(result.price / 100 * result.fast)).toFixed(2));
            }
            pricebook_price.val((parseFloat(grand.html()) - parseFloat(result.price / 100 * result.fast)).toFixed(2));
          }
        } else if (termoTrig == 1) {
          if (document.querySelector('#SalesOrder_editView_fieldName_cf_930').checked) {
            if (result.termo == 0) {
              let termoPlace = document.querySelector('#SalesOrder_editView_fieldName_cf_930').parentElement;
              let termoWarning = document.createElement('div');
              termoWarning.setAttribute('style', 'color:red;');
              termoWarning.setAttribute('id', 'termo-warning');
              termoWarning.innerText = "Termo režimo kaina nenustatyta";
              termoPlace.append(termoWarning);
            }
            grand.html((parseFloat(grand.html() / 100 * result.termo) + parseFloat(grand.html())).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
              // agreed_price.val((parseFloat(grand.html() / 100 * result.termo) + parseFloat(grand.html())).toFixed(2));
            }
            pricebook_price.val((parseFloat(grand.html() / 100 * result.termo) + parseFloat(grand.html())).toFixed(2));
          } else {
            let termoW = document.getElementById('termo-warning');
            if (termoW != undefined) {
              termoW.remove();
            }
            grand.html((parseFloat(grand.html()) - parseFloat(result.price / 100 * result.termo)).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
              // agreed_price.val((parseFloat(grand.html()) - parseFloat(result.price / 100 * result.termo)).toFixed(2));
            }
            pricebook_price.val((parseFloat(grand.html()) - parseFloat(result.price / 100 * result.termo)).toFixed(2));
          }
        } else if (loadingWorkTrig == 1) {
          if (document.querySelector('#SalesOrder_editView_fieldName_cf_926').checked) {
            if (result.loading_work == 0) {
              let loading_workPlace = document.querySelector('#SalesOrder_editView_fieldName_cf_926').parentElement;
              let loading_workWarning = document.createElement('div');
              loading_workWarning.setAttribute('style', 'color:red;');
              loading_workWarning.setAttribute('id', 'loading-work-warning');
              loading_workWarning.innerText = "Krovos darbų kaina nenustatyta";
              loading_workPlace.append(loading_workWarning);
            }
            grand.html((parseFloat(result.loading_work) + parseFloat(grand.html())).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
              // agreed_price.val((parseFloat(result.loading_work) + parseFloat(grand.html())).toFixed(2));
            }
            pricebook_price.val((parseFloat(result.loading_work) + parseFloat(grand.html())).toFixed(2));
          } else {
            let loadingW = document.getElementById('loading-work-warning');
            if (loadingW != undefined) {
              loadingW.remove();
            }
            grand.html((parseFloat(grand.html()) - parseFloat(result.loading_work)).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
              // agreed_price.val((parseFloat(grand.html()) - parseFloat(result.loading_work)).toFixed(2));
            }
            pricebook_price.val((parseFloat(grand.html()) - parseFloat(result.loading_work)).toFixed(2));
          }
        } else {
          if (document.querySelector('#SalesOrder_editView_fieldName_cf_924').checked) {
            grand.html((parseFloat(result.price / 100 * result.fast) + parseFloat(result.price)).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
              // agreed_price.val((parseFloat(result.price / 100 * result.fast) + parseFloat(result.price)).toFixed(2));
            }
            pricebook_price.val((parseFloat(result.price / 100 * result.fast) + parseFloat(result.price)).toFixed(2));
          } else {
            grand.html(parseFloat(result.price).toFixed(2));
            if (agreed_price.val() == '' || agreed_price.val() == pricebook_price.val()) {
              // agreed_price.val(parseFloat(result.price).toFixed(2));
            }
            pricebook_price.val(parseFloat(result.price).toFixed(2));
          }
        }

        postTrig = '';
        fastTrig = '';
        termoTrig = '';
        loadingWorkTrig = '';
        $('#faster').val(result.fast);
        $('#termo').val(result.termo);
        $('#downtime').val(result.downtime);
        if (parseFloat(result.loading_work) !== 0) {
          $('#loading_work').val(parseFloat(result.loading_work).toFixed(2));
        }

        // document.querySelector('.disabled_create_btn').removeAttribute('disabled');
        $("#wait").css("display", "block");

      } else {

        // document.querySelector('.disabled_create_btn').setAttribute('disabled', 'disabled');
        loadAgreedPrice();

        $(document).ajaxComplete(function () {
          setTimeout(function () {
            $("#wait").css("display", "none");
          }, 1000);
        });
        appendAlert.append(alert);
        setMarginPrice(result.price);
        $("#grandTotal").html(parseFloat(result.price).toFixed(2));

        $('.input-group2').click(function () {
          alert.remove();
        });

        $("#alert").html(result.combination);
        $('#SalesOrder_editView_fieldName_cf_1297').val(result.combination);
        // $("#qty1").val('1');
        $("#adjustment").val('0');
      }
    }

    function loadAgreedPrice() {
      $('#SalesOrder_editView_fieldName_cf_1376').on('change', function () {
        console.log('change');
       
        if ($(this).val() != '') {
          $('#SalesOrder_editView_fieldName_cf_1297').val('Sutarta kaina');
          // document.querySelector('.disabled_create_btn').removeAttribute('disabled');
        } else {
          // document.querySelector('.disabled_create_btn').setAttribute('disabled', 'disabled');
          $('#SalesOrder_editView_fieldName_cf_1297').val('');
        }
      });
    }


    function setMarginPrice(price) {
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow:not(.except)');

      if (rows.length == 1) {
        $(`[name='margin${rows.length}']`).val(price);
      } else {

        let marArray = new Array();
        let totalMargin
        for (i = 0; i < rows.length; i++) {
          c = rows[i].dataset.rowNum;
          marArray.push($(`[name='margin${c}']`).val());
        }
        totalMargin = marArray.map(Number).reduce((a, b) => { return a + b; });
        if (price >= totalMargin) {
          price = price - totalMargin;
        }
        c = rows[rows.length - 1].dataset.rowNum;
        $(`[name='margin${c}']`).val(price);

      }
    }

    function checkServiceMargins() {
      let row = document.querySelectorAll('.except');
      let rowArr = new Array();
      for (i = 0; i < row.length; i++) {
        let count = row[i].dataset.rowNum;
        rowArr.push($(`[name='margin${count}']`).val());
      }

      let rez = rowArr.map(Number).reduce((a, b) => { return a + b });
      return rez;
    }



    function insertValue(result, pro) {
      // if(document.getElementById(`productName${pro}`).getAttribute("disabled") != 'disabled'){
      $(`#hideinput${pro}`).val(parseFloat(result).toFixed(2));
      // } 
    }

    $('#lineItemTab').click(function () {
      alert.remove();
    });

    document.getElementById('SalesOrder_editView_fieldName_bill_code').classList.add('enter_trig');
    document.getElementById('SalesOrder_editView_fieldName_ship_code').classList.add('enter_trig');
    document.getElementById('lineItemTab').classList.add('enter_trig');



    var input = document.querySelectorAll(".enter_trig");
    for (let trigId = 0; trigId < input.length; trigId++) {
      input[trigId].addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
          event.preventDefault();
          trigger();
          postTrigger();
        }
      });
    }



    $('body').on('change', '#lineItemTab', function (e) {
      let id;
      if ($(e.target).hasClass("tare")) {
        id = e.target.parentElement.parentElement.dataset.rowNum;
        let tare = e.target.value;

        if (url.get('record') == null) {
          if (tare == 1) {
            $(`#cargo_length${id}`).val(0.8);
            $(`#cargo_width${id}`).val(1.2);
          } else if (tare == 2) {
            $(`#cargo_length${id}`).val(1.2);
            $(`#cargo_width${id}`).val(1);
          } else if (tare == 3) {
            $(`#cargo_length${id}`).val(1.2);
            $(`#cargo_width${id}`).val(1.2);
          } else if (tare == 14) {
            $(`#cargo_length${id}`).val(0.8);
            $(`#cargo_width${id}`).val(0.6);
          } else {
            $(`#cargo_length${id}`).val('');
            $(`#cargo_width${id}`).val('');
          }
        }
      } else {
        id = e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.dataset.rowNum;
        if ($(`#cargo_wgt${id}`).val() && $(`#cargo_length${id}`).val() && $(`#cargo_width${id}`).val() && $(`#cargo_height${id}`).val()) {
          trigger();
          postTrigger();
        }
      }

    });

    let postTrig = '';
    function postTrigger() {
      if ($('#cargo_wgt1').val() && $('#cargo_length1').val() && $('#cargo_width1').val() && $('#cargo_height1').val()) {
        $('#SalesOrder_editView_fieldName_bill_code').change(function () {
          postTrig = 1;
          trigger();
        });

        $('#SalesOrder_editView_fieldName_ship_code').change(function () {
          postTrig = 1;
          trigger();
        });
      }
    }
    let fastTrig;
    $('#SalesOrder_editView_fieldName_cf_924').click(function () {
      if ($(this).is(':checked')) {
        if ($('#grandTotal').html() != '0.00') {
          fastTrig = 1;
          trigger();
        }
      } else {
        if ($('#grandTotal').html() != '0.00') {
          fastTrig = 1;
          trigger();
        }
      }
    });


    let termoTrig;
    $('#SalesOrder_editView_fieldName_cf_930').click(function () {
      if ($(this).is(':checked')) {
        if ($('#grandTotal').html() != '0.00') {
          termoTrig = 1;
          trigger();
        }
      } else {
        if ($('#grandTotal').html() != '0.00') {
          termoTrig = 1;
          trigger();
        }
      }
    });


    let loadingWorkTrig;
    $('#SalesOrder_editView_fieldName_cf_926').click(function () {
      if ($(this).is(':checked')) {
        if ($('#grandTotal').html() != '0.00') {
          loadingWorkTrig = 1;
          trigger();
        }
      } else {
        if ($('#grandTotal').html() != '0.00') {
          loadingWorkTrig = 1;
          trigger();
        }
      }
    });


    postTrigger();

    function trigger() {
      $("#wait").css("display", "block");
      let product = document.querySelectorAll('.lineItemRow');
      let pro;
      for (let i = 0, len = product.length; i <= len - 1; i++) {
        pro = document.querySelectorAll('.lineItemRow')[i].dataset.rowNum;
      }

      if (typeof ($(`#hideinput${pro}`).val()) === "undefined") {
        const hideinput = document.createElement('input');
        hideinput.setAttribute('id', `hideinput${pro}`);
        hideinput.setAttribute('class', `hideprice`);
        hideinput.setAttribute('type', 'hidden');
        hideinput.setAttribute('value', '');
        blockPrice.append(hideinput);

        const qanty = document.createElement('input');
        qanty.setAttribute('id', 'qanty' + pro);
        qanty.setAttribute('type', 'hidden');
        qanty.setAttribute('value', '1');
        blockPrice.append(qanty);
      }
      if (typeof ($(`#faster`).val()) === "undefined") {
        const faster = document.createElement('input');
        faster.setAttribute('id', 'faster');
        faster.setAttribute('type', 'hidden');
        faster.setAttribute('value', '');
        blockPrice.append(faster);
      }
      if (typeof ($(`#termo`).val()) === "undefined") {
        const termo = document.createElement('input');
        termo.setAttribute('id', 'termo');
        termo.setAttribute('type', 'hidden');
        termo.setAttribute('value', '');
        blockPrice.append(termo);
      }
      if (typeof ($(`#downtime`).val()) === "undefined") {
        const downtime = document.createElement('input');
        downtime.setAttribute('id', 'downtime');
        downtime.setAttribute('type', 'hidden');
        downtime.setAttribute('value', '');
        blockPrice.append(downtime);
      }
      if (typeof ($(`#loading_work`).val()) === "undefined") {
        const loading_work = document.createElement('input');
        loading_work.setAttribute('id', 'loading_work');
        loading_work.setAttribute('type', 'hidden');
        loading_work.setAttribute('value', '');
        blockPrice.append(loading_work);
      }

      getPrice();

    }


    function qantity(qanty, sum, qty, pro) {
      let grandTotal = '';
      if (qanty <= qty) {
        grandTotal = parseFloat(total) + (parseFloat(sum) * qty);
        $('#qanty' + pro).val(qty);
      } else {
        grandTotal = parseFloat(total) + (parseFloat(sum) * qty);
        $('#qanty' + pro).val(qty);
      }

      setTimeout(function () {
        $("#wait").css("display", "none");
      }, 1000);

      $("#grandTotal").html(parseFloat(grandTotal).toFixed(2));

    }

    const addProduct = $('#addProduct');
    addProduct.click(function () {
      let product = document.querySelectorAll('*[placeholder="Pvz: 30 0,7x0,4x0,3"]');
      for (let i = 1, len = product.length; i <= len; i++) {
        $("#adjustment").on("mouseover", function () {
          bindButton();
        });
        $('#qty' + i).attr("readonly", "readonly");
        // $('#productName'+i).attr('disabled', 'disabled');
        document.getElementById('totalProductCount').value = i;
      }
    });
  }
});


function isNumber(evt) {
  let charCode = (event.which) ? event.which : event.keyCode;
  evt.target.value = evt.target.value.replace(/,/g, '.');


  if (charCode != 46 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  } else {
    //if dot sign entered more than once then don't allow to enter dot sign again. 46 is the code for dot sign
    let parts = evt.srcElement.value.split('.');

    if (parts.length > 1 && (charCode == 46 || charCode == 44)) {
      return false;
    }
    return true;
  }
}
