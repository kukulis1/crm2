<?php

require_once "config.inc.php";
require 'v1/external_data/utils.php';
ini_set('display_errors', 1);
error_reporting(E_ALL);


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $invoiceid = 30710679;

  $created = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(createdtime, '%Y-%m-%d') AS created																   
												           FROM vtiger_crmentity WHERE crmid = $invoiceid"))['created'];

	// $create_invoice = 'create_invoice';
	$create_invoice = 'edit_invoice';


  if(filesize("logs/$create_invoice/$created/$invoiceid.log") > 0){
    $create_log = fopen("logs/$create_invoice/$created/$invoiceid.log", "r");
    $create_log = fread($create_log,filesize("logs/$create_invoice/$created/$invoiceid.log"));   
  }

  $splited = explode('--------------------------------', $create_log);
  $data = json_decode($splited[4], true);

  $invoice_data = array();
  $tot_no_prod = $data['totalProductCount'];


  for($i=1; $i <= $tot_no_prod; $i++){

		// Susirankam pagrindines eilutes
		$services_count = $data['services_count'.$i] ?? 0;
		$service_by_product = (($data['hdnProductId'.$i] ?? '14244') == 121391 ? 30 : 1);
		if(!empty($data['productName'.$i])){
			$invoice_data['productName'][] = $data['productName'.$i] ?? '';
			$invoice_data['comment'][] = $data['comment'.$i] ?? '';
			$invoice_data['hdnProductId'][] = (($data['INDEPENDENT'] ?? false) ? 121391: $data['hdnProductId'.$i]);
			$invoice_data['lineItemType'][] = $data['lineItemType'.$i] ?? '';
			$invoice_data['order_id'][] = $data['order_id'.$i] ? $data['order_id'.$i] : 0;
			$invoice_data['bill_address'][] = $data['bill_address'.$i] ?? '';
			$invoice_data['service_select'][] = (($data['service_select'.$i] ?? false) ?: $service_by_product);
			$invoice_data['ship_address'][] = $data['ship_address'.$i] ?? '';
			$invoice_data['note_field'][] = $data['note_field'.$i] ?? '';
			$invoice_data['costcenter'][] = $data['costcenter'.$i] ?? '';
			$invoice_data['object'][] = $data['object'.$i] ?? '';
			$invoice_data['employee'][] = $data['employee'.$i] ?? '';
			$invoice_data['cargo_measure_invoice'][] = $data['cargo_measure_invoice'.$i] ?? '';
			$invoice_data['cargo_wgt'][] = $data['cargo_wgt'.$i] ?? '';
			$invoice_data['cargo_volume'][] = $data['cargo_volume'.$i] ?? '';
			$invoice_data['fix-price'][] = $data['fix-price'.$i] ?? '';
			$invoice_data['discount_type'][] = $data['discount_type'.$i] ?? '';
			$invoice_data['discount'][] = $data['discount'.$i] ?? '';
			$invoice_data['discount_percentage'][] = $data['discount_percentage'.$i] ?? '';
			$invoice_data['discount_amount'][] = $data['discount_amount'.$i] ?? '';
			$invoice_data['netPrice'][] = $data['netPrice'.$i] ?? '';		
			$invoice_data['quantity'][] = (($data['qty'.$i] ?? false) ?: 1) ?? '';	


		
			// Susirenkam pridetas paslaugas
			if($services_count > 0){
				for($e=$services_count; $e  >= 1; $e--){
					if(!empty($data["productName$i-$e"])){
						$service_by_product2 = ($data["service_select$i-$e"] == 121391 ? 30 : 1);
						$invoice_data['productName'][] = ($data["productName$i-$e"] ? $data["productName$i-$e"] : '');
						$invoice_data['comment'][] =  $data["comment$i-$e"] ?? '';
						$invoice_data['hdnProductId'][] = $data["hdnProductId$i-$e"] ?? '';
						$invoice_data['lineItemType'][] = $data["lineItemType$i-$e"] ?? '';
						$invoice_data['order_id'][] = $data["order_id$i-$e"] ?? 0;
						$invoice_data['bill_address'][] = $data["bill_address$i-$e"] ?? '';
						$invoice_data['service_select'][] = (!empty($data["service_select$i-$e"]) ? $data["service_select$i-$e"] : $service_by_product2);

						$invoice_data['ship_address'][] = $data["ship_address$i-$e"] ?? '';
						$invoice_data['note_field'][] = $data["note_field$i-$e"] ?? '';
						$invoice_data['costcenter'][] = $data["costcenter$i-$e"] ?? '';
						$invoice_data['object'][] = $data["object$i-$e"] ?? '';
						$invoice_data['employee'][] = $data["employee$i-$e"] ?? '';
						$invoice_data['cargo_measure_invoice'][] = (!empty($data["cargo_measure_invoice$i-$e"]) ? $data["cargo_measure_invoice$i-$e"] : null);
						$invoice_data['cargo_wgt'][] = $data["cargo_wgt$i-$e"] ?? 0;
						$invoice_data['cargo_volume'][] = $data["cargo_volume$i-$e"] ?? 0;
						$invoice_data['fix-price'][] = $data["fix-price$i-$e"] ?? 0;
						$invoice_data['discount_type'][] = $data["discount_type$i-$e"] ?? '';
						$invoice_data['discount'][] = $data["discount$i-$e"] ?? '';
						$invoice_data['discount_percentage'][] = $data["discount_percentage$i-$e"] ?? '';
						$invoice_data['discount_amount'][] = $data["discount_amount$i-$e"] ?? '';
						$invoice_data['netPrice'][] = ($data["netPrice$i-$e"] ? $data["netPrice$i-$e"] : '');
						$invoice_data['quantity'][] = (!empty($data["qty$i-$e"]) ? $data["qty$i-$e"] : 1);		
					}			
				}
			}
		}
	}


	// echo '<pre>';
	// print_R($invoice_data);
	// echo '</pre>';
  
  
	// die;


  if($conn->query("DELETE FROM vtiger_inventoryproductrel WHERE id  = $invoiceid")){


    $insert_query = 'INSERT INTO vtiger_inventoryproductrel(id, productid, sequence_no, discount_percent, discount_amount,quantity, listprice,comment, cargo_measure, cargo_wgt,order_id,note,bill_ads,ship_ads,service, m3,inventory_type,costcenter,employee,object) VALUES ';


    $products_count = COUNT($invoice_data['productName']);
    $prod_seq=1;

    for($n =0; $n < $products_count; $n++){
      $prod_id = $invoice_data['hdnProductId'][$n];
      $discount_percent = ($invoice_data['discount_type'][$n] == 'percentage' ? $invoice_data['discount_percentage'][$n] : 0.00);
      $discount_sum = ($invoice_data['discount_type'][$n] == 'amount' ? $invoice_data['discount'][$n] : 0);
      $quantity = ($invoice_data['quantity'][$n] ?: 1);
      $listprice = ($invoice_data['fix-price'][$n] ?: 0);		
      $cargo_measure = $invoice_data['cargo_measure_invoice'][$n];
      $cargo_wgt = ($invoice_data['lineItemType'][$n] == 'Item' ? $invoice_data['productName'][$n] : $invoice_data['cargo_wgt'][$n]);
      $comment = $invoice_data['comment'][$n];
      $order_id = $invoice_data['order_id'][$n];
      $note = $invoice_data['note_field'][$n];		
      $bill_ads = $invoice_data['bill_address'][$n];
      $ship_ads = $invoice_data['ship_address'][$n];
      $service_select = $invoice_data['service_select'][$n];
      $m3 = $invoice_data['cargo_volume'][$n]; 
      $costcenter = $invoice_data['costcenter'][$n]; 
      $employee = $invoice_data['employee'][$n]; 
      $object = $invoice_data['object'][$n]; 

      $insert_query .= "('$invoiceid', '$prod_id', $prod_seq, '$discount_percent', '$discount_sum',$quantity, $listprice,'$comment', '$cargo_measure', '$cargo_wgt', '$order_id','$note','$bill_ads', '$ship_ads',$service_select,'$m3','','$costcenter','$employee','$object'),";

      $prod_seq++;
    }


    $insert_query = rtrim($insert_query,',');


    echo '<pre>';
    print_R($insert_query);
    echo '</pre>';

    if($conn->query($insert_query)){
      echo "INSERTED";
    } else {
      echo("Error description: " . $conn->error);
    }

  }else{
    echo("Error description: " . $conn->error);
  }