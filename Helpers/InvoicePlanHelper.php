<?php

namespace Helpers;

class InvoicePlanHelper
{
    static function getInvoicesPlan($db, $dates=[], $salesorderids=[]){
        if(empty($dates)){
            return 0;
        } 

        $count_salesorderids = count($salesorderids);

        if($count_salesorderids > 0){
            $imploded_salesorderids = implode(',', $salesorderids);
        }

        $periodicity = [
			'Kas savaitę' => 'every_week',
			'Už visą mėn.' => 'every_month',
			'Du kartus per mėn.' => 'two_times_of_month'
		];	

        $from = min($dates);
        $to = max($dates);     
        $today = date('Y-m-d');   

		$total_invoices_to_write = 0;

		$get_customers_rule = $db->query("SELECT vtiger_account.accountid, autoinvoicingrules_tks_groupby AS rule						
											FROM vtiger_autoinvoicingrules r
											JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
											JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
											JOIN vtiger_account ON vtiger_account.accountid=rel.crmid
											WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND autoinvoicingrules_tks_autoinv = 1
											GROUP BY relcrmid", \PDO::FETCH_ASSOC)->fetchAll();
		
		$customers_rule = InvoicePlanHelper::pluck($get_customers_rule, 'rule','accountid');

		$first_order = "SELECT vtiger_salesorder.accountid, COUNT(vtiger_salesorder.salesorderid) AS orders, vtiger_salesorder.salesorderid
						FROM vtiger_account 
						JOIN vtiger_salesorder ON vtiger_salesorder.accountid=vtiger_account.accountid
						LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
						LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
						INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_account.accountid
						INNER JOIN vtiger_crmentity so_entity ON so_entity.crmid=vtiger_salesorder.salesorderid
						WHERE (DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN ? AND ?) AND vtiger_salesordercf.cf_1456 = 1 
                        AND  vtiger_crmentity.setype = 'Accounts' AND so_entity.setype = 'SalesOrder' 
                        AND DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 90 DAY) >= now()                         
						";
        if($count_salesorderids > 0){
            $first_order .= " AND vtiger_salesorder.salesorderid IN ($imploded_salesorderids)";     
        }   

        $first_order  .= " AND vtiger_invoice_salesorders_list.invoiceid IS NULL  AND vtiger_crmentity.deleted = 0 AND so_entity.deleted = 0
        GROUP BY vtiger_account.accountid
        HAVING orders = 1"; 
        
        $first_order_execute = $db->prepare($first_order);
        $first_order_execute->execute([$from, $to]);

        $not_in_salesorderids = [];
        foreach ($first_order_execute ?? [] as $value) {
            $total_invoices_to_write += 1;
            $not_in_salesorderids[] = $value->salesorderid;
        }
        
        $not_in_salesorderids_imploded = 1;
        if(count($not_in_salesorderids) > 0){
            $not_in_salesorderids_imploded = implode(',', $not_in_salesorderids);
        }

		$by_conditions = "SELECT vtiger_salesorder.accountid, vtiger_salesorder.salesorderid, vtiger_accountscf.cf_1281 AS periodicity 
						 FROM vtiger_account 
						 JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_account.accountid
						 JOIN vtiger_salesorder ON vtiger_salesorder.accountid=vtiger_account.accountid
						 LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
						 LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
						 INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_account.accountid
						 INNER JOIN vtiger_crmentity so_entity ON so_entity.crmid=vtiger_salesorder.salesorderid
						 WHERE vtiger_salesordercf.cf_1456 = 1 AND vtiger_crmentity.setype = 'Accounts' AND so_entity.setype = 'SalesOrder' AND vtiger_invoice_salesorders_list.invoiceid IS NULL ";	
		
		$by_conditions .=  
		" AND 	
			CASE WHEN vtiger_accountscf.cf_1281 != ''
					THEN
						CASE 
							WHEN vtiger_accountscf.cf_1281 = 'Kas savaitę'
                                THEN 								
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 7 DAY), '%Y-%m-%d') BETWEEN '$from' AND '$to' 
                                    OR 
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 7 DAY), '%Y-%m-%d') >= '$to'
                                                 
                            WHEN  vtiger_accountscf.cf_1281 = 'Du kartus per mėn.'    
                                THEN                         
                                    (DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 15 DAY), '%Y-%m-%d') BETWEEN '$from' AND '$to' 
                                    OR 
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 15 DAY), '%Y-%m-%d') >= '$to') 

                                    OR 

                                    (DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') BETWEEN '$from' AND '$to'  
                                    OR 
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') >= '$to') 

							WHEN vtiger_accountscf.cf_1281 = 'Už visą mėn.' 
                                THEN
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') BETWEEN '$from' AND '$to'
                                    OR
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') >= '$to'                  
						END
				 ELSE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$from' AND '$to'  
                      OR 
                      DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') <= '$today' 
			END ";

        if($count_salesorderids > 0){
            $by_conditions .= " AND vtiger_salesorder.salesorderid IN ($imploded_salesorderids) ";  
        }

        if(count($not_in_salesorderids) > 0){
            $by_conditions .= " AND vtiger_salesorder.salesorderid NOT IN ($not_in_salesorderids_imploded) ";
        }

		$by_conditions .= " AND vtiger_crmentity.deleted = 0 AND so_entity.deleted = 0 GROUP BY vtiger_salesorder.salesorderid ";		
				
		$with_out_conditions_invoices = [];
		$conditions_invoices = [];
		$salesorders_by_invoicing_rules = [];

		$by_conditions_execute = $db->prepare($by_conditions);
		$by_conditions_execute->execute();

        

		foreach ($by_conditions_execute ?? [] as $value) {
			if(empty($value->periodicity)){
				$with_out_conditions_invoices[$value->accountid][] = true;	
			}else{
				if(empty($customers_rule[$value->accountid])){		
					$conditions_invoices[$value->accountid][] = true;		
				}else{
					$salesorders_by_invoicing_rules[$periodicity[$value->periodicity]][$value->accountid][] = $value->salesorderid;
				}
			}
		}

		foreach ($periodicity ?? [] as $period) {
			foreach ($salesorders_by_invoicing_rules[$period] ?? [] as $accountid => $salesorderid) {
                $total_invoices_to_write = InvoicePlanHelper::groupSalesOrders($db, $customers_rule[$accountid], $accountid, implode(',',$salesorderid));	
			}
		}

		$total_invoices_to_write += count($conditions_invoices);
		$total_invoices_to_write += count($with_out_conditions_invoices);
    
        return $total_invoices_to_write;
    }


    static function getAllTimeInvoicesPlan($db){
        $periodicity = [
			'Kas savaitę' => 'every_week',
			'Už visą mėn.' => 'every_month',
			'Du kartus per mėn.' => 'two_times_of_month'
		];	

		$total_invoices_to_write = 0;

		$get_customers_rule = $db->query("SELECT vtiger_account.accountid, autoinvoicingrules_tks_groupby AS rule						
											FROM vtiger_autoinvoicingrules r
											JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
											JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
											JOIN vtiger_account ON vtiger_account.accountid=rel.crmid
											WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND autoinvoicingrules_tks_autoinv = 1
											GROUP BY relcrmid", \PDO::FETCH_ASSOC)->fetchAll();
		
		$customers_rule = InvoicePlanHelper::pluck($get_customers_rule, 'rule','accountid');
		

		$first_order = "SELECT vtiger_salesorder.accountid, COUNT(vtiger_salesorder.salesorderid) AS orders, vtiger_salesorder.salesorderid
						FROM vtiger_account 
						JOIN vtiger_salesorder ON vtiger_salesorder.accountid=vtiger_account.accountid
						LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
						LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
						INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_account.accountid
						INNER JOIN vtiger_crmentity so_entity ON so_entity.crmid=vtiger_salesorder.salesorderid
						WHERE vtiger_salesordercf.cf_1456 = 1 AND  vtiger_crmentity.setype = 'Accounts' AND so_entity.setype = 'SalesOrder' AND DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 90 DAY) >= now() 
						AND vtiger_invoice_salesorders_list.invoiceid IS NULL AND vtiger_crmentity.deleted = 0 AND so_entity.deleted = 0
						GROUP BY vtiger_account.accountid
						HAVING orders = 1";

		$first_order_execute = $db->prepare($first_order);
		$first_order_execute->execute();

		$first_order_count = 0;
		$not_in_salesorderids = [];
		foreach ($first_order_execute as $value) {
			$total_invoices_to_write += 1;
			$first_order_count += 1;
			$not_in_salesorderids[] = $value->salesorderid;
		}				

		

		$by_conditions = "SELECT vtiger_salesorder.accountid, vtiger_salesorder.salesorderid, vtiger_accountscf.cf_1281 AS periodicity 
						 FROM vtiger_account 
						 JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_account.accountid
						 JOIN vtiger_salesorder ON vtiger_salesorder.accountid=vtiger_account.accountid
						 LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
						 LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
						 INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_account.accountid
						 INNER JOIN vtiger_crmentity so_entity ON so_entity.crmid=vtiger_salesorder.salesorderid
						 WHERE vtiger_salesordercf.cf_1456 = 1 AND vtiger_crmentity.setype = 'Accounts' AND so_entity.setype = 'SalesOrder' 
						 AND vtiger_invoice_salesorders_list.invoiceid IS NULL ";
						 if(count($not_in_salesorderids) > 0){
							$not_in_salesorderids_imploded = implode(',', $not_in_salesorderids);
							
							$by_conditions .= " AND vtiger_salesorder.salesorderid NOT IN ($not_in_salesorderids_imploded) ";
						 }
						 $by_conditions .= " AND vtiger_crmentity.deleted = 0 AND so_entity.deleted = 0 ";	
		
		$by_conditions .=  
		" AND 	
			CASE WHEN vtiger_accountscf.cf_1281 != ''
					THEN
						CASE 
							WHEN vtiger_accountscf.cf_1281 = 'Kas savaitę' 
								THEN YEAR(so_entity.createdtime) <= YEAR(NOW()) AND WEEK(so_entity.createdtime, 1) < WEEK(NOW(), 1) 
								
							WHEN  vtiger_accountscf.cf_1281 = 'Du kartus per mėn.'                                             
								THEN (MONTH(so_entity.createdtime) = MONTH(NOW()) AND 
								(DAYOFMONTH(NOW()) >= 15) AND DAYOFMONTH(so_entity.createdtime) < 15) OR 
								(DAYOFMONTH(NOW()) = date('t') AND DAYOFMONTH(so_entity.createdtime) > 15) OR 
								MONTH(NOW()) > MONTH(so_entity.createdtime)

							WHEN vtiger_accountscf.cf_1281 = 'Už visą mėn.' 
								THEN MONTH(so_entity.createdtime) < MONTH(NOW()) OR YEAR(NOW()) > YEAR(so_entity.createdtime)
								ELSE YEAR(so_entity.createdtime) <= YEAR(NOW())                                      
						END
				 ELSE true 	
			END  
		";  

		$by_conditions .= " GROUP BY vtiger_salesorder.salesorderid ";		

		
		$with_out_conditions_invoices = [];
		$conditions_invoices = [];
		$salesorders_by_invoicing_rules = [];

		$by_conditions_execute = $db->prepare($by_conditions);
		$by_conditions_execute->execute();

		foreach ($by_conditions_execute as $key => $value) {
			if(empty($value->periodicity)){
				$with_out_conditions_invoices[$value->accountid][] = true;	
			}else{
				if(empty($customers_rule[$value->accountid])){		
					$conditions_invoices[$value->accountid][] = true;		
				}else{
					$salesorders_by_invoicing_rules[$periodicity[$value->periodicity]][$value->accountid][] = $value->salesorderid;
				}
			}
		}

		foreach ($periodicity as $period) {
			foreach ($salesorders_by_invoicing_rules[$period] as $accountid => $salesorderid) {
				$rez = InvoicePlanHelper::groupSalesOrders($db, $customers_rule[$accountid], $accountid, implode(',',$salesorderid));
				$total_invoices_to_write += $rez; 				
			}
		}

		$total_invoices_to_write += count($conditions_invoices);
		$total_invoices_to_write += count($with_out_conditions_invoices);

        return $total_invoices_to_write;

    }

    static function getInvoicesPlanForDashboard($db, $dates=[]){
        if(empty($dates)){
            return 0;
        }            

        $periodicity = [
			'Kas savaitę' => 'every_week',
			'Už visą mėn.' => 'every_month',
			'Du kartus per mėn.' => 'two_times_of_month'
		];  
        
        $today = date('Y-m-d');
      
		$total_invoices_to_write = 0;
		$total_amount= 0;

		$get_customers_rule = $db->query("SELECT vtiger_account.accountid, autoinvoicingrules_tks_groupby AS rule						
											FROM vtiger_autoinvoicingrules r
											JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
											JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
											JOIN vtiger_account ON vtiger_account.accountid=rel.crmid
											WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND autoinvoicingrules_tks_autoinv = 1
											GROUP BY relcrmid");
		
		$customers_rule = $db->pluck($get_customers_rule, 'rule','accountid');

    
		$first_order = "SELECT vtiger_salesorder.accountid, COUNT(vtiger_salesorder.salesorderid) AS orders, vtiger_salesorder.salesorderid, 
        IF(cf_1376 > 0,cf_1376, REPLACE(ROUND(vtiger_salesorder.total,2),'.',',')) as totalprice 
						FROM vtiger_account 
						JOIN vtiger_salesorder ON vtiger_salesorder.accountid=vtiger_account.accountid
						LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
						LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
						INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_account.accountid
						INNER JOIN vtiger_crmentity so_entity ON so_entity.crmid=vtiger_salesorder.salesorderid
						WHERE (DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN ? AND ?) AND vtiger_salesordercf.cf_1456 = 1 
                        AND  vtiger_crmentity.setype = 'Accounts' AND so_entity.setype = 'SalesOrder' 
                        AND DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 90 DAY) >= now()                         
						AND vtiger_invoice_salesorders_list.invoiceid IS NULL AND vtiger_crmentity.deleted = 0 AND so_entity.deleted = 0
						GROUP BY vtiger_account.accountid
						HAVING orders = 1";

        
        $first_order_execute = $db->pquery($first_order, [$dates['from'], $dates['to']]);
        

        $not_in_salesorderids = [];
        foreach ($first_order_execute ?? [] as $value) {
            $total_invoices_to_write += 1;
            $total_amount += $value['totalprice'];
            $not_in_salesorderids[] = $value['salesorderid'];
        }
        
        
        $not_in_salesorderids_imploded = 1;
        if(count($not_in_salesorderids) > 0){
            $not_in_salesorderids_imploded = implode(',', $not_in_salesorderids);
        }

		$by_conditions = "SELECT vtiger_salesorder.accountid, vtiger_salesorder.salesorderid, vtiger_accountscf.cf_1281 AS periodicity, IF(cf_1376 > 0,cf_1376, REPLACE(ROUND(vtiger_salesorder.total,2),'.',',')) as totalprice
						 FROM vtiger_account 
						 JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_account.accountid
						 JOIN vtiger_salesorder ON vtiger_salesorder.accountid=vtiger_account.accountid
						 LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
						 LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
						 INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_account.accountid
						 INNER JOIN vtiger_crmentity so_entity ON so_entity.crmid=vtiger_salesorder.salesorderid
						 WHERE vtiger_salesordercf.cf_1456 = 1 AND vtiger_crmentity.setype = 'Accounts' AND so_entity.setype = 'SalesOrder' AND vtiger_invoice_salesorders_list.invoiceid IS NULL AND 
						 vtiger_crmentity.deleted = 0 AND so_entity.deleted = 0 ";	
		
		$by_conditions .=  
		" AND 	
			CASE WHEN vtiger_accountscf.cf_1281 != ''
					THEN
						CASE 
							WHEN vtiger_accountscf.cf_1281 = 'Kas savaitę'
                                THEN 								
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 7 DAY), '%Y-%m-%d') BETWEEN '{$dates['from']}' AND '{$dates['to']}'    
                                    OR      
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 7 DAY), '%Y-%m-%d') >= '{$dates['to']}'  
								
							WHEN  vtiger_accountscf.cf_1281 = 'Du kartus per mėn.'    
                                THEN                         
                                    (DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 15 DAY), '%Y-%m-%d') BETWEEN '{$dates['from']}' AND '{$dates['to']}'  
                                    OR
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 15 DAY), '%Y-%m-%d') >= '{$dates['to']}')   

                                    OR   

                                    (DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') BETWEEN '{$dates['from']}' AND '{$dates['to']}'  
                                    OR           
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') >= '{$dates['to']}')


							WHEN vtiger_accountscf.cf_1281 = 'Už visą mėn.' 
                                THEN
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') BETWEEN '{$dates['from']}' AND '{$dates['to']}'  
                                    OR             
                                    DATE_FORMAT(DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 30 DAY), '%Y-%m-%d') >= '{$dates['to']}'        

						END
				 ELSE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '{$dates['from']}' AND '{$dates['to']}'
                      OR 
                      DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') <= '$today' 
			END  
		";  

        if(count($not_in_salesorderids) > 0){
            $by_conditions .= " AND vtiger_salesorder.salesorderid NOT IN ($not_in_salesorderids_imploded) ";
        }

		$by_conditions .= " GROUP BY vtiger_salesorder.salesorderid ";		
		
		$with_out_conditions_invoices = [];
		$conditions_invoices = [];
		$salesorders_by_invoicing_rules = [];

		$by_conditions_execute = $db->pquery($by_conditions);       

		foreach ($by_conditions_execute ?? [] as $value) {
            $total_amount += $value['totalprice'];
			if(empty($value['periodicity'])){          
				$with_out_conditions_invoices[$value['accountid']][] = true;	
			}else{
				if(empty($customers_rule[$value['accountid']])){		
					$conditions_invoices[$value['accountid']][] = true;		
				}else{
					$salesorders_by_invoicing_rules[$periodicity[$value['periodicity']]][$value['accountid']][] = $value['salesorderid'];
				}
			}
		}        

		foreach ($periodicity ?? [] as $period) {
			foreach ($salesorders_by_invoicing_rules[$period] ?? [] as $accountid => $salesorderid) {	
                $total_invoices_to_write = InvoicePlanHelper::groupSalesOrders($db, $customers_rule[$accountid], $accountid, implode(',',$salesorderid)); 
			}
		}        

		$total_invoices_to_write += count($conditions_invoices);
		$total_invoices_to_write += count($with_out_conditions_invoices);
    
        return ['count' => $total_invoices_to_write, 'amount' => $total_amount];
    }

    static function pluck($array, $value, $key){
        $plucked_array = [];
        foreach ($array ?? [] as $data) {
            $plucked_array[$data[$key]] = $data[$value];
        }
        
        return $plucked_array;
    }
    
    static function groupSalesOrders($db, $table, $accountid, $salesorderids){	 
    
        $street = ($table == 'load_address' ? 'bill_street' : 'ship_street');
        $city = ($table == 'load_address' ? 'bill_city' : 'ship_city');
        
        $query = "SELECT vtiger_salesorder.salesorderid, load_company, 
        ".($table == 'employee' ? "IF(vtiger_salesorder.source = 'Metrika', IF(created_person IS NOT NULL, created_person, CONCAT(first_name,' ',last_name)),CONCAT(first_name,' ',last_name)) AS employee" : "REPLACE(REPLACE($street,',',' '),'.',' ') AS street, $city as city")."
        FROM `vtiger_salesorder`
        LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid                                                       
        JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid           
        JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
        JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
        ".($table == 'employee' ? "JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smcreatorid" : "")."	
        INNER JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid AND vtiger_crmentity.setype = 'SalesOrder'              
        WHERE vtiger_crmentity.deleted = 0 AND vtiger_account.accountid = $accountid AND vtiger_salesordercf.cf_1468 = 0 AND vtiger_salesorder.salesorderid IN ($salesorderids)
        GROUP BY vtiger_salesorder.salesorderid 
        ORDER BY vtiger_salesorder.shipment_code DESC";   
    
                    
        $execute = $db->query($query, \PDO::FETCH_ASSOC)->fetchAll();
    
        $orders_array = [];
  
        foreach ($execute ?? [] as $row){
            $orders_array[] = $row;     
        }
    
    
        $grouped_addreses = [];
        if($table == 'employee'){
            for($i =0; $i < count($orders_array); $i++){    
                if(!empty($orders_array[$i]['salesorderid'])){				
                    $grouped_addreses[$orders_array[$i]['employee']] = true; 	                   
                }
            }
        }else{
            $splited = InvoicePlanHelper::splitAddresses($db, $orders_array);
            for($i =0; $i < count($orders_array); $i++){    
                if(!empty($splited[$i]['salesorderid'])){		
                    $grouped_addreses[$splited[$i]['street']] = true;                    	
                }
            } 
       }

        return count($grouped_addreses);
    }
    
    static function clean($string) {
        $string = str_replace('UAB', '', $string); 
        $string = explode(' ',trim($string));       
        $string = preg_replace('/[^\da-z ]/i', '', $string[0]); // Removes special chars.     
        return $string; 
    }
    
    static function splitAddresses($db, $addresses){
        $lietuviskos = array('Ą','Č','Ę','Ė','Į','Š','Ų','Ū','Ž','ą','č','ę','ė','į','š','ų','ū','ž');
        $lotyniskos = array('Ą','C','E','E','I','S','U','U','Z','a','c','e','e','i','s','u','u','z');
    
        $cities = $db->query("SELECT DISTINCT cities_tks_city AS city FROM `vtiger_cities` LEFT JOIN vtiger_crmentity ON crmid=citiesid WHERE deleted = 0 GROUP BY crmid", \PDO::FETCH_ASSOC)->fetchAll();
        
        $cities_array = array();
    
        foreach($cities ?? [] as $row){
            if(!empty($row['city']))  $cities_array[] = $row['city'];
        }
    
        $cities_array[] = 'gatve';
        $cities_array[] = 'gatvė';
    
        $array = array();
        $address_array = array();
    
        $count = 0;
        foreach ($addresses ?? [] as $row) {
            $array = explode(" ",$row['street']);
    
            $load_company = trim(InvoicePlanHelper::clean($row['load_company']));    
    
            for($i = 0; $i < count($array); $i++){
                $string = trim(str_replace($lietuviskos,$lotyniskos,$array[$i]));
    
                if($string != '-'){
    
                    if(!is_numeric($string)){
                        $cut = substr($string, 0, -1);
                        if(is_numeric($cut)){          
                        $address_array[$count]['nr'] = $cut; 
                        $address_array[$count]['city'] = $row['city'];        
                        $address_array[$count]['salesorderid'] = $row['salesorderid'];      
                        $address_array[$count]['load_company'] = $load_company;            
                        }else{
                        $cut2 = substr($string, 1);
                        if(is_numeric($cut2)){            
                            $address_array[$count]['nr'] = $cut2;
                            $address_array[$count]['city'] = $row['city'];	            
                            $address_array[$count]['salesorderid'] = $row['salesorderid']; 
                            $address_array[$count]['load_company'] = $load_company; 
                
                        }
                        }
                        $is_city = in_array($string,$cities_array);
                        if(strlen($string) > 3 && !$is_city){
                        $address_array[$count]['street'] = $string;   
                        $address_array[$count]['city'] = $row['city'];
                        $address_array[$count]['salesorderid'] = $row['salesorderid'];   
                        $address_array[$count]['load_company'] = $load_company;  
                
                        }else{
                            if(is_numeric($string)){        
                                $address_array[$count]['nr'] = $string;
                            }
                        }
    
                    }elseif(is_numeric($string)){        
                        $address_array[$count]['nr'] = $string;
                        $address_array[$count]['city'] = $row['city'];           
                        $address_array[$count]['salesorderid'] = $row['salesorderid']; 
                        $address_array[$count]['load_company'] = $load_company;     
                    }
    
                }else{
                    $address_array[$count]['nr'] = '';
                    $address_array[$count]['city'] = $row['city'];
                    $address_array[$count]['street'] = 'Be-gatves';              
                    $address_array[$count]['salesorderid'] = $row['salesorderid']; 
                    $address_array[$count]['load_company'] = $load_company;    
                }
    
            }  
    
            $count++;
        }
    
        return $address_array;
    }

}