<?php

namespace Helpers;

class CargosHelper
{
    static function getAllRevisedMeasures($connection, $sales_order_ids=[]){
        if(empty($sales_order_ids)){
            return [];
        }
    
        $calculated_measures = []; 
        $imploded_sales_orders_ids = implode(',', $sales_order_ids);
        $measures_results = $connection->query(
        "   SELECT id, IFNULL(revised_quantity, 1) as revised_quantity, IFNULL(revised_measure, '-') as revised_measure, IFNULL(revised_weight, 0) as revised_weight, IFNULL(revised_length, 0) as revised_length, IFNULL(revised_width, 0) as revised_width, IFNULL(revised_height, 0) as revised_height
            FROM vtiger_inventoryproductrel
            WHERE id IN ($imploded_sales_orders_ids)
                AND productid = 14244
        ");
    
        foreach($measures_results as $measure){
            for($i = 0; $i < (int)$measure['revised_quantity']; $i++){
                $divided_weight = round((float)$measure['revised_weight']/(int)$measure['revised_quantity'],2);
                $calculated_measures[$measure['id']][] = "{$measure['revised_measure']} {$divided_weight} {$measure['revised_length']}x{$measure['revised_width']}x{$measure['revised_height']}";
            }
        }
    
        return $calculated_measures;
    }

    static function getAllRevisedMeasuresForLoads($connection, $external_load_ids, $sales_order_id){
        if(empty($external_load_ids)){
            return [];
        }

        $calculated_measures = []; 
        $imploded_external_load_ids = implode(',', $external_load_ids);
        $measures_results = $connection->query(
        "   SELECT external_load_id, IFNULL(revised_quantity, 1) as revised_quantity, IFNULL(revised_measure, '-') as revised_measure, IFNULL(revised_weight, 0) as revised_weight, IFNULL(revised_length, 0) as revised_length, IFNULL(revised_width, 0) as revised_width, IFNULL(revised_height, 0) as revised_height
            FROM vtiger_inventoryproductrel
            WHERE id = $sales_order_id
                AND external_load_id IN ($imploded_external_load_ids) 
                AND productid = 14244
        ");
        
        foreach($measures_results as $measure){
            for($i = 0; $i < (int)$measure['revised_quantity']; $i++){
                $divided_weight = round((float)$measure['revised_weight']/(int)$measure['revised_quantity'],2);
                $calculated_measures[$measure['external_load_id']][] = "{$measure['revised_measure']} {$divided_weight} {$measure['revised_length']}x{$measure['revised_width']}x{$measure['revised_height']}";
            }
        }

        return $calculated_measures;
    }
}