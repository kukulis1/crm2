<?php
include 'modules/Invoice/InvoicePDFController.php';

function GenerateInvoice($recordId, $user){   
    $moduleName = 'Invoice'; 

    $controller = new Vtiger_InvoicePDFController($moduleName);
    $controller->loadRecord($recordId);

    $sequenceNo = getModuleSequenceNumber($moduleName, $recordId);    
    $fileName = str_replace("/", "",$sequenceNo);	

    $response = $controller->Output($fileName.'.pdf', 'D');        
    return $response;
}?>