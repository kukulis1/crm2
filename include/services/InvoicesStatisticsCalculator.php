<?php


class InvoicesStatisticsCalculator
{
    /** @var DatabaseWrapper */
    private $databaseWrapper;

    /** @var float */
    private $totalIncome = 0;
    /** @var DateTime */
    private $oldestInvoiceDate;
    /** @var float */
    private $timeIntervalInMonths = 0;
    /** @var float */
    private $ordersCount = 0;
    /** @var float */
    private $averageOrdersCount = 0;
    /** @var float */
    private $averageIncomePerMonth = 0;
    /** @var float */
    private $thisMonthIncome = 0;
    /** @var float */
    private $previousMonthIncome = 0;

    /** @var int */
    private $thisMonthOrdersCount = 0;
    /** @var int */
    private $previousMonthOrdersCount = 0;

    /** @var float */
    private $incomeMonthChange = 0;

    /** @var int */
    private $ordersMonthChange = 0;


    /**
     * InvoicesStatisticsCalculator constructor.
     * @param DatabaseWrapper $databaseWrapper
     */
    public function __construct(DatabaseWrapper $databaseWrapper)
    {
        $this->databaseWrapper = $databaseWrapper;
    }

    /**
     * @param DateTime $now
     * @param int $clientId
     */
    public function calculateInvoiceStatistics(DateTime $now, $clientId)
    {
        // generally must be called in the given sequence, as these functions uses each other results
        // 1) total income
        $this->totalIncome = $this->calculateTotalIncome($clientId);
        // 2) oldest invoice date
        $this->oldestInvoiceDate = $this->findOldestInvoiceDate($clientId);
        // 3) Time interval in months
        $this->timeIntervalInMonths = $this->calculateTimeIntervalInMonths($this->oldestInvoiceDate, $now);
        // 4) average income per month
        $this->averageIncomePerMonth = $this->calculateAverageIncomePerMonth(
            $this->totalIncome,
            $this->timeIntervalInMonths
        );
        // 5) Total orders count
        $this->ordersCount = $this->findTotalOrdersCount($clientId);
        // 6) average orders per month
        $this->averageOrdersCount = $this->calculateAverageOrdersPerMonth(
            $this->ordersCount,
            $this->timeIntervalInMonths
        );

        $monthBegining = $this->calculateThisMonthBegining($now);
        $nextMonthBegining = $this->calculateNextMothBegining($monthBegining);
        $previousMonthBegining = $this->calculatePreviousMonthBegining($monthBegining);

        // 7) This month and previous month income
        $this->thisMonthIncome = $this->getIncomeForTheDateInterval($clientId, $monthBegining, $nextMonthBegining);
        $this->previousMonthIncome = $this->getIncomeForTheDateInterval(
            $clientId,
            $previousMonthBegining,
            $monthBegining
        );

        // 8) This month and previous month orders count
        $this->thisMonthOrdersCount = $this->getOrdersCountForTheDateInterval(
            $clientId,
            $monthBegining,
            $nextMonthBegining
        );
        $this->previousMonthOrdersCount = $this->getOrdersCountForTheDateInterval(
            $clientId,
            $previousMonthBegining,
            $monthBegining
        );

        // 9) Income change of previous and this month
        $this->incomeMonthChange = $this->incomeMonthChange = $this->thisMonthIncome - $this->previousMonthIncome;

        // 10) Orders count change of previous and this month
        $this->ordersMonthChange = $this->thisMonthOrdersCount - $this->previousMonthOrdersCount;
    }

    // --- calculators ---

    private function calculateTotalIncome($clientId)
    {
        $sql = "select sum(balance) balance_total from vtiger_invoice where accountid=$clientId";
        $pdo = $this->databaseWrapper->getPdo();
        $results = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        return $results[0]['balance_total'];
    }

    private function findOldestInvoiceDate($clientId)
    {
        $sql = "select min(invoicedate) oldest_date from vtiger_invoice  where accountid=$clientId";
        $pdo = $this->databaseWrapper->getPdo();
        $results = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        return new DateTime($results[0]['oldest_date']);
    }

    private function calculateTimeIntervalInMonths(DateTime $dateFrom, DateTime $dateTo)
    {
        $yearFrom = intval($dateFrom->format('y'));
        $monthFrom = intval($dateFrom->format('m'));

        $yearTo = intval($dateTo->format('y'));
        $monthTo = intval($dateTo->format('m'));

        return ($yearTo * 12 + $monthTo) - ($yearFrom * 12 + $monthFrom) + 1; // including the last month
    }

    // 4) average income per month
    private function calculateAverageIncomePerMonth($totalIncome, $intervalInMonths)
    {
        return $totalIncome / $intervalInMonths;
    }

    // 5) Total orders count
    private function findTotalOrdersCount($clientId)
    {
        $sql = "select count(io.salesorderid) orders_count from
                  vtiger_invoice i join
                  vtiger_invoice_salesorders_list io
            on i.invoiceid = io.invoiceid
            where i.accountid = $clientId;
            ";
        $pdo = $this->databaseWrapper->getPdo();
        $results = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        return $results[0]['orders_count'];
    }

    // 6) average orders per month
    private function calculateAverageOrdersPerMonth($ordersCount, $months)
    {
        return (float)$ordersCount / $months;
    }

    private function calculateThisMonthBegining(DateTime $now)
    {
        $year = intval($now->format('y'));
        $month = intval($now->format('m'));

        // a) calculate month begining date.
        $monthBegining = clone $now;
        $monthBegining->setTime(0, 0, 0);
        $monthBegining->setDate($year, $month, 1);

        return $monthBegining;
    }

    private function calculateNextMothBegining(DateTime $thisMonthBegining)
    {
        $previousMonthBegining = clone $thisMonthBegining;
        $dateInterval = new DateInterval('P1M');
        $previousMonthBegining->add($dateInterval);

        return $previousMonthBegining;
    }

    private function calculatePreviousMonthBegining(DateTime $thisMonthBegining)
    {
        $previousMonthBegining = clone $thisMonthBegining;
        $dateInterval = new DateInterval('P1M');
        $dateInterval->invert = 1;
        $previousMonthBegining->add($dateInterval);

        return $previousMonthBegining;
    }

    private function getIncomeForTheDateInterval(int $clientId, DateTime $from, DateTime $to)
    {
        $fromStr = $from->format('y-m-d');
        $toStr = $to->format('y-m-d');

        $sql = "select sum(balance) balance_total
                from vtiger_invoice where accountid = $clientId 
                 and invoicedate >= '$fromStr' 
                 and invoicedate < '$toStr' ";
        $pdo = $this->databaseWrapper->getPdo();
        $query = $pdo->query($sql);
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results[0]['balance_total'];
    }

    private function getOrdersCountForTheDateInterval(int $clientId, DateTime $from, DateTime $to)
    {
        $fromStr = $from->format('y-m-d');
        $toStr = $to->format('y-m-d');

        $sql = "select count(io.salesorderid) orders_count from
                  vtiger_invoice i join
                  vtiger_invoice_salesorders_list io
            on i.invoiceid = io.invoiceid
            where i.accountid = $clientId
                 and invoicedate >= '$fromStr' 
                 and invoicedate < '$toStr' ;
            ";

        $pdo = $this->databaseWrapper->getPdo();
        $query = $pdo->query($sql);
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        return $results[0]['orders_count'];
    }


    // --- getters ---

    public function getTotalIncome()
    {
        return $this->totalIncome;
    }

    public function getOldestInvoiceDate()
    {
        return $this->oldestInvoiceDate;
    }

    public function getTimeIntervalInMonths()
    {
        return $this->timeIntervalInMonths;
    }

    public function getAverageIncomePerMonth()
    {
        return $this->averageIncomePerMonth;
    }

    public function getThisMonthIncome()
    {
        return $this->thisMonthIncome;
    }

    public function getPreviousMonthIncome()
    {
        return $this->previousMonthIncome;
    }

    public function getThisMonthOrdersCount()
    {
        return $this->thisMonthOrdersCount;
    }

    public function getPreviousMonthOrdersCount()
    {
        return $this->previousMonthOrdersCount;
    }

    public function getIncomeMonthChange()
    {
        return $this->incomeMonthChange;
    }

    public function getOrdersCountMonthChange()
    {
        return $this->ordersMonthChange;
    }

    public function getOrdersCount()
    {
        return $this->ordersCount;
    }

    public function getAverageOrdersCount()
    {
        return $this->averageOrdersCount;
    }

}