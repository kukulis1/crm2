<?php

class DatabaseWrapper
{
    /** @var PDO */
    private $pdo;
    /** @var string */
    private $host;
    /** @var string */
    private $database;
    /** @var string */
    private $user;
    /** @var string */
    private $password;
    /** @var string */
    private $port;
    /** @var string */
    private $type;

    private function __construct($host, $port, $user, $password, $database, $type = 'mysql')
    {
        $this->host = $host;
        $this->database = $database;
        $this->user = $user;
        $this->password = $password;
        $this->port = $port;
        $this->type = $type;
    }


    public function getPdo()
    {
        if ($this->pdo == null) {
            $this->pdo = $this->connectPdo();
        }

        return $this->pdo;
    }

    private function connectPdo()
    {
        $protocol = '';

        if ($this->type == 'mysqli' || $this->type == 'mysql ') {
            $protocol = "mysql:host=%s;port=%s,dbname=%s;";
        }

        $port = $this->port;
        if (empty ($port)) {
            $port = 3306;
        }

        $port = str_replace(':', '', $port);

        $connectionString = sprintf($protocol, $this->host, $port, $this->database);
        $pdo = new PDO(
            $connectionString,
            $this->user, $this->password
        );

        // klaidos visos tegul virsta exception'ais
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $pdo->exec('use '.$this->database );

        return $pdo;
    }


    public static $_instance = null;

    public static function getInstance()
    {
        if (self::$_instance == null) {
            include __DIR__ . '/../../config.inc.php';
            $dbconfig = $dbconfig ?? [];

            self::$_instance = new DatabaseWrapper(
                $dbconfig['db_server'],
                $dbconfig['db_port'],
                $dbconfig['db_username'],
                $dbconfig['db_password'],
                $dbconfig['db_name'],
                $dbconfig['db_type']
            );
        }

        return self::$_instance;
    }
}