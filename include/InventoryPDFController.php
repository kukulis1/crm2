<?php
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *
 ********************************************************************************/


include_once 'vtlib/Vtiger/PDF/models/Model.php';
include_once 'vtlib/Vtiger/PDF/inventory/HeaderViewer.php';
include_once 'vtlib/Vtiger/PDF/inventory/FooterViewer.php';
include_once 'vtlib/Vtiger/PDF/inventory/ContentViewer.php';
include_once 'vtlib/Vtiger/PDF/inventory/ContentViewer2.php';
include_once 'vtlib/Vtiger/PDF/viewers/PagerViewer.php';
include_once 'vtlib/Vtiger/PDF/PDFGenerator.php';
include_once 'data/CRMEntity.php';


class Vtiger_InventoryPDFController {

	protected $module;
	protected $focus = null;

	function __construct($module) {
		$this->moduleName = $module;
	}

	function loadRecord($id) {
		global $current_user;
		$this->focus = $focus = CRMEntity::getInstance($this->moduleName);
		$focus->retrieve_entity_info($id,$this->moduleName);
		$focus->apply_field_security();
		$focus->id = $id;
		$this->associated_products = getAssociatedProducts($this->moduleName,$focus);
	}

	function getPDFGenerator() {
		return new Vtiger_PDF_Generator();
	}

	function getContentViewer() {
		if($this->focusColumnValue('hdnTaxType') == "individual") {
			$contentViewer = new Vtiger_PDF_InventoryContentViewer();
		} else {
			$contentViewer = new Vtiger_PDF_InventoryTaxGroupContentViewer();
		}
		$contentViewer->setContentModels($this->buildContentModels());
		$contentViewer->setSummaryModel($this->buildSummaryModel());
		$contentViewer->setLabelModel($this->buildContentLabelModel());
		$contentViewer->setWatermarkModel($this->buildWatermarkModel());
		return $contentViewer;
	}

	function getHeaderViewer() {
		$headerViewer = new Vtiger_PDF_InventoryHeaderViewer();
		$headerViewer->setModel($this->buildHeaderModel());
		return $headerViewer;
	}

	// function getFooterViewer() {
	// 	$footerViewer = new Vtiger_PDF_InventoryFooterViewer();
	// 	$footerViewer->setModel($this->buildFooterModel());
	// 	$footerViewer->setLabelModel($this->buildFooterLabelModel());
	// 	$footerViewer->setOnLastPage();
	// 	// return $footerViewer;
	// 	// ITOMA
	// }


	function getFooterViewer(){
		$footerViewer = new Vtiger_PDF_InventoryFooterViewer();
		$footerViewer->setModel($this->buildFooterModel());
		$footerViewer->setLabelModel($this->buildFooterLabelModel());
		$footerViewer->setOnLastPage();
		return $footerViewer;
	}

	function getPagerViewer() {
		$pagerViewer = new Vtiger_PDF_PagerViewer();
		$pagerViewer->setModel($this->buildPagermodel());
		return $pagerViewer;
	}

	function Output($filename, $type) {
		if(is_null($this->focus)) return;

		$pdfgenerator = $this->getPDFGenerator();

		$pdfgenerator->setPagerViewer($this->getPagerViewer());
		$pdfgenerator->setHeaderViewer($this->getHeaderViewer());
		$pdfgenerator->setFooterViewer($this->getFooterViewer());
		$pdfgenerator->setContentViewer($this->getContentViewer());

		$pdfgenerator->generate($filename, $type);
	}

	//ITOMA
	function multiexplode ($delimiters,$data) {
		$MakeReady = str_replace($delimiters, $delimiters[0], $data);
		$Return    = explode($delimiters[0], $MakeReady);
		return  $Return;
	} 

	// Helper methods

	function buildContentModels() {
		global $adb;
		$associated_products = $this->associated_products;
		$contentModels = array();
		$productLineItemIndex = 0;
		$totaltaxes = 0;
		$no_of_decimal_places = getCurrencyDecimalPlaces();	

		$RECORD_ID = $_REQUEST['record'];
		$get_invoice_lang = $adb->pquery("SELECT IF(cf_2114 = 'LT', 'lt_lt', 'en_us') AS lang, accountid 
										  FROM vtiger_invoice 
										  JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
										  WHERE vtiger_invoice.invoiceid = ?", array($RECORD_ID));
										  
		$invoice_lang = $adb->query_result($get_invoice_lang,0,'lang');
		$accountid = $adb->query_result($get_invoice_lang,0,'accountid');

		foreach($associated_products as $productLineItem) {
			++$productLineItemIndex;
			$contentModel = new Vtiger_PDF_Model();
			
			$total_tax_percent = 0.00;
			$producttotal_taxes = 0.00;
			$quantity = ''; $listPrice = ''; $discount = ''; $taxable_total = '';
			$tax_amount = ''; $producttotal = '';
			$quantity	= $productLineItem["qty{$productLineItemIndex}"];
			$listPrice	= $productLineItem["listPrice{$productLineItemIndex}"];
			$discount	= $productLineItem["discountTotal{$productLineItemIndex}"];
			$taxable_total = $quantity * $listPrice - $discount;
			$taxable_total = number_format($taxable_total, $no_of_decimal_places,'.','');
			$producttotal = $taxable_total;
			if($this->focus->column_fields["hdnTaxType"] == "individual") {
				for($tax_count=0;$tax_count<count($productLineItem['taxes']);$tax_count++) {
					$tax_percent = $productLineItem['taxes'][$tax_count]['percentage'];
					$total_tax_percent += $tax_percent;
					$tax_amount = (($taxable_total*$tax_percent)/100);
					$producttotal_taxes += $tax_amount;
				}
			}

			$producttotal_taxes = number_format($producttotal_taxes, $no_of_decimal_places,'.','');
			$producttotal = $taxable_total+$producttotal_taxes;
			$producttotal = number_format($producttotal, $no_of_decimal_places,'.','');
			$tax = $producttotal_taxes;
			$totaltaxes += $tax;
			$totaltaxes = number_format($totaltaxes, $no_of_decimal_places,'.','');			

			$cargo_kg = $productLineItem["cargo_wgt{$productLineItemIndex}"];
			$cargo_m3 = (empty($productLineItem["volume{$productLineItemIndex}"]) ? '' : number_format($productLineItem["volume{$productLineItemIndex}"],3));
			$date =  $productLineItem["createdtime{$productLineItemIndex}"];
			$createDate = new DateTime($date); 
			$createdtime = $createDate->format('Y m-d');
			$entityType = $productLineItem["entityType{$productLineItemIndex}"];		
			$template = $productLineItem["print_template"];
			

			$order = ($entityType == 'Item' ? decode_html($productLineItem["cargo_wgt{$productLineItemIndex}"]) : decode_html($productLineItem["shipment_code{$productLineItemIndex}"])."\n".decode_html($productLineItem["customer_order_code{$productLineItemIndex}"]) );
		
	
			if(!empty($productLineItem["discount_percent{$productLineItemIndex}"]))
			$discount_price = $productLineItem["listPrice{$productLineItemIndex}"] / 100 * $productLineItem["discount_percent{$productLineItemIndex}"];
			else
			$discount_price = $productLineItem["discount_amount{$productLineItemIndex}"];		
			
			$quantity = ($entityType == 'Item' ? $productLineItem["qty{$productLineItemIndex}"] : str_replace(",", "\n",$productLineItem["cargo_measure_invoice{$productLineItemIndex}"]));	

			$pll_place = $productLineItem["pll_place{$productLineItemIndex}"];

			$measure = $quantity;

			if($accountid == 9628){
				$measure = $quantity.'/'.$pll_place;
			}

			$service_type = $productLineItem["service_type{$productLineItemIndex}"];

			if(in_array($productLineItem["service_id{$productLineItemIndex}"], [13,14,34,35])){
				$service_type = 'Papildomos išlaidos' ;
			}		
	

			$replace = ['Š' => 'Ś', 'š' => 'ś'];
			$bill_ads = decode_html($productLineItem["bill_ads{$productLineItemIndex}"]);
			$ship_ads = decode_html($productLineItem["ship_ads{$productLineItemIndex}"]);
			$bill_ads = str_replace(array_keys($replace), $replace, $bill_ads); 
			$ship_ads = str_replace(array_keys($replace), $replace, $ship_ads); 

			$contentModel->set('order_type', 	$productLineItem["order_type{$productLineItemIndex}"]);
			$contentModel->set('Date', 	($productLineItem["entityType{$productLineItemIndex}"] == 'Services' ? '' : $createdtime));
			$contentModel->set('Order',  $order);
			$contentModel->set('Loading', $bill_ads);
			$contentModel->set('Un-Load',  $ship_ads);					
			$contentModel->set('Quantity', $measure);
			$contentModel->set('Kg', $cargo_kg);
			$contentModel->set('M3',$cargo_m3);
			$contentModel->set('Sum', $productLineItem["listPrice{$productLineItemIndex}"]);
			$contentModel->set('Total', $productLineItem["productTotal{$productLineItemIndex}"].	(!empty($discount_price) ? "\n Nuolaida: ".$discount_price : "" ));
			$contentModel->set('Note', decode_html($productLineItem["note{$productLineItemIndex}"]));
			$contentModel->set('Service', decode_html($service_type));
			$contentModel->set('EntityType', $entityType);
			$contentModel->set('PrintTemplate', $template);
			$contentModel->set('lang', $invoice_lang);
			$contentModel->set('client', $accountid);
			$contentModel->set('CargoComments',$productLineItem["cargo_comments"]);
			if($template == 'PVM sąskaita-faktūra'){
				$contentModel->set('Measure', $productLineItem["cargo_measure_invoice{$productLineItemIndex}"]);
			}
			
			$contentModels[] = $contentModel;
		}

		$this->totaltaxes = $totaltaxes; //will be used to add it to the net total

		return $contentModels;
	}

	function buildContentLabelModel() {
		$labelModel = new Vtiger_PDF_Model();
		$labelModel->set('Code',      getTranslatedString('Product Code',$this->moduleName));
		$labelModel->set('Name',      getTranslatedString('Product Name',$this->moduleName));
		                
    if($this->moduleName != 'PurchaseOrder'){
			$labelModel->set('Code',      'Užsakymas');
			$labelModel->set('Name',      'Pak.');
    }                
                
		$labelModel->set('Quantity',  getTranslatedString('Quantity',$this->moduleName));
		$labelModel->set('Price',     getTranslatedString('LBL_LIST_PRICE',$this->moduleName));
		$labelModel->set('Discount',  getTranslatedString('Discount',$this->moduleName));
		$labelModel->set('Tax',       getTranslatedString('Tax',$this->moduleName));
		$labelModel->set('Total',      'Kaina be PVM');
		$labelModel->set('Comment',   getTranslatedString('Comment'),$this->moduleName);
		return $labelModel;
	}



function numberToWords ($number, $lang = 'lt', $currency = '€') { 
	$number = number_format($number, 2, '.', '');
	$number_parts = explode (".", $number ); 
	$number = $number_parts[0]; 
	$cents = $number_parts[1]; 
	$number_without_minus = str_replace('-', '', $number);
	$number_length = (int)strlen($number_without_minus); 		
		   
	$text_ones = [
		'lt' => ['', ' vienas', ' du', ' trys', ' keturi', ' penki', ' šeši', ' septyni', ' aštuoni', ' devyni'],
		'en' => ['', ' one', ' two', ' three', ' four', ' fifth', ' six', ' seven', ' eight', ' nine'],
	];
	$text_tens = [
		'lt' => ['', ' dešimt', ' dvidešimt', ' trisdešimt', 'keturiasdešimt', ' penkiasdešimt', ' šešiasdešimt', ' septyniasdešimt', 'aštuoniasdešimt', ' devyniasdešimt'],
		'en' => ['', ' ten', ' twenty', ' thirty', 'forty', ' fifty', ' sixty', ' seventy', 'eighty', ' ninety'],
	];
	$text_excepts = [
		'lt' => [' vienuolika', ' dvylika', ' trylika', 'keturiolika', ' penkiolika', ' šešiolika', ' septyniolika', ' aštuoniolika', ' devyniolika'],
		'en' => [' eleven', ' twelve', ' thirteen', 'fourteen', ' fifteen', ' sixteen', ' seventeen', ' eighteen', ' nineteen'],
	];
	$text_hundreds = [
		'lt' => ['', ' šimtas ', ' šimtai '],
		'en' => ['', ' hundred ', ' hundreds '],
	];
	$text_thousands = [
		'lt' => [' tūkstančių ', ' tūkstantis ', ' tūkstančiai'],
		'en' => [' thousands ', ' a thousand ', ' thousands'],
	];
	
	
	$words = ''; 
																 
	if($number < 0) { 
		$words = 'Minus'; 
	} 

	if( $number_length < 7){ 
		for($d = 2; $d > 0; $d--){ 
									
			if(($d == 2) && ( $number_length < 4)){
				continue; 					
			} 
									
			$number_local = substr($number, -3); 

			if($d == 2){
				$number_local = substr($number, 0, $number_length - 3); 
			}

		
			$number_local_length = strlen($number_local); 
			$hundreds = substr($number_local, -3, 1); 
			$last_two = substr($number_local, -2); 
			
			if( $number_local_length > 2 ){ 
				$words .= $text_ones[$lang][(int)$hundreds]; 
				if( $hundreds > 1 ){ 
						$words .= $text_hundreds[$lang][2]; 
				} else { 
					$words .= $text_hundreds[$lang][(int)$hundreds]; 
				} 
			} 
										
			if(($last_two > 10) && ($last_two < 20)){ 
					$words .= $text_excepts[$lang][(int)substr( $last_two, -1 ) - 1]; 
					
					if($d == 2){
						$words .= $text_thousands[$lang][0]; 
					}
			
			} else { 
				
				if($number_local_length > 1){
					$words .= $text_tens[$lang] [(int)substr( $last_two, -2, 1 ) ] . ' '; 
				}

				$words .= $text_ones[$lang][(int)substr( $last_two, -1)]; 
				
				if($d == 2){ 
					if(((int)substr($last_two, -1)) > 1){ 
						$words .= $text_thousands[$lang][2]; 
					} else { 
						$words .= $text_thousands[$lang][(int)substr($last_two, -1)]; 
					} 
				} 
			} 
		} 
		$words = trim($words); 

		if($words[0] . $words[1] == 'š'){
			$words = 'Š' . substr($words, 2); 
		} else {
			$words = ucfirst($words); 
		}
						
	} 

	if($cents < 10){
		$cents .= '0';
	} 

	return $words . ' ' . $currency . ', ' . substr($cents, 0, 2) . ' ct'; 
}


	function buildSummaryModel() {
		global $adb;
		$associated_products = $this->associated_products;
		$final_details = $associated_products[1]['final_details'];
		$template = $associated_products[1]['print_template'];
		$invoice_type = $associated_products[1]['invoice_type'];
		$summaryModel = new Vtiger_PDF_Model();

		$RECORD_ID = $_REQUEST['record'];
		$result2 = $adb->pquery("SELECT IF(cf_2114 = 'LT', 'lt_lt', 'en_us') AS lang FROM vtiger_invoicecf WHERE invoiceid = ?", array($RECORD_ID));
		$invoice_lang = $adb->query_result($result2,0,'lang');

		$netTotal = $discount = $handlingCharges =  $handlingTaxes = 0;
		$adjustment = $grandTotal = 0;

		$productLineItemIndex = 0;
		$sh_tax_percent = 0;
		foreach($associated_products as $productLineItem) {
			++$productLineItemIndex;
			$netTotal += $productLineItem["netPrice{$productLineItemIndex}"];
		}
		$netTotal = number_format(($netTotal + $this->totaltaxes), getCurrencyDecimalPlaces(),'.', '');
		// ITOMA
		// $summaryModel->set(getTranslatedString("Net Total", $this->moduleName), $this->formatPrice($netTotal));

		$discount_amount = $final_details["discount_amount_final"];
		$discount_percent = $final_details["discount_percentage_final"];

		$discount = 0.0;
        $discount_final_percent = '0.00';
		if($final_details['discount_type_final'] == 'amount') {
			$discount = $discount_amount;
		} else if($final_details['discount_type_final'] == 'percentage') {
            $discount_final_percent = $discount_percent;
			$discount = (($discount_percent*$final_details["hdnSubTotal"])/100);
		}// ITOMA
		// $summaryModel->set(getTranslatedString("Discount", $this->moduleName)."($discount_final_percent%)", $this->formatPrice($discount));

		$group_total_tax_percent = '0.00';
		//To calculate the group tax amount
		if($final_details['taxtype'] == 'group') {
			$group_tax_details = $final_details['taxes'];
			for($i=0;$i<count($group_tax_details);$i++) {
				$group_total_tax_percent += $group_tax_details[$i]['percentage'];
			}// ITOMA
			// $summaryModel->set(getTranslatedString("Tax:", $this->moduleName)."($group_total_tax_percent%)", $this->formatPrice($final_details['tax_totalamount']));
		}
		//Shipping & Handling taxes
		$sh_tax_details = $final_details['sh_taxes'];

		$count_tax = 0;

		if(!empty($sh_tax_details)) $count_tax = count($sh_tax_details);
		
		// for($i=0;$i<count($sh_tax_details);$i++) { itoma
		for($i=0;$i<$count_tax;$i++) {
			$sh_tax_percent = $sh_tax_percent + $sh_tax_details[$i]['percentage'];
		}
		//obtain the Currency Symbol
		$currencySymbol = $this->buildCurrencySymbol();

		//ITOMA 

		$note =	getTranslatedString('LBL_NOTE','Invoice',$invoice_lang);
		$total =	getTranslatedString('LBL_TOTAL_WITH_VAT','Invoice',$invoice_lang);
		$total_with_out_vat =	getTranslatedString('LBL_TOTAL_WITHOUT_VAT','Invoice',$invoice_lang);
		$pay_untill =	getTranslatedString('LBL_PAY_UNTILL','Invoice',$invoice_lang);
		$invoice_write =	getTranslatedString('LBL_INVOICE_WRITE','Invoice',$invoice_lang);
		$amount_in_words =	getTranslatedString('LBL_AMOUNT_IN_WORDS','Invoice',$invoice_lang);

	if ($this->moduleName == 'Invoice') {
		if($associated_products[1]['regionid'] == 0){
			$get_vat_procent = number_format(21);
			$ps = '';
		}else{
			$get_vat_procent = number_format($associated_products[1]['vatprocent']);
			$ps = (!empty($associated_products[1]['article']) ? "$note: " : '').html_entity_decode($associated_products[1]['article'], ENT_QUOTES, 'UTF-8');
		}
	}	
		$summaryModel->set($total_with_out_vat.":", $this->formatPrice($associated_products[1]['subtotal']));
		$summaryModel->set(chunk_split($ps, 80),	$associated_products[1]['vatprice']);

		$getCt = explode(".",$associated_products[1]['total']);

		$summaryModel->set(getTranslatedString("$total:", $this->moduleName,$invoice_lang), $this->formatPrice($associated_products[1]['total'])); // TODO add currency string

		$lang = ($invoice_lang == 'lt_lt' ? 'lt' : 'en');
		
		$numberToWord = $this->numberToWords($final_details['grandTotal'], $lang, 'EUR');	

		if($template == 'Transportas'){			
			 $template_string = 'transporto';		
			 $template_string2 = 'transport';			
		}elseif($template == 'Perkraustymas'){		
			 $template_string = 'perkraustymo';			
			 $template_string2 = 'movement';			
		}else{ 
			$template_string = 'transporto';		
			$template_string2 = 'transport';
		}


		if($template == 'PVM sąskaita-faktūra'){
			$summaryModel->set("$amount_in_words:", $numberToWord);
		}else{

			if($invoice_lang == 'lt_lt'){
				$total_for =	getTranslatedString('LBL_TOTAL_FOR','Invoice',$invoice_lang);
				$for_services =	getTranslatedString('LBL_FOR_SERVICES','Invoice',$invoice_lang);
				$all_for_services = "$total_for $template_string $for_services";
			}else{
				$total_for =	getTranslatedString('LBL_TOTAL_FOR','Invoice','lt_lt');
				$for_services =	getTranslatedString('LBL_FOR_SERVICES','Invoice','lt_lt');
				$total_for2 =	getTranslatedString('LBL_TOTAL_FOR','Invoice',$invoice_lang);			
				$for_services2 =	getTranslatedString('LBL_FOR_SERVICES','Invoice',$invoice_lang);		
				$all_for_services = "$total_for $template_string $for_services/$total_for2 $template_string2 $for_services2";
			}

			

			$summaryModel->set("$all_for_services:", $numberToWord);
		}


		if($invoice_type != 'Kreditinė'){
			$summaryModel->set("$pay_untill:", $associated_products[1]['duedate']);
		}else{
			$summaryModel->set('','');
		}

		if($invoice_lang == 'lt_lt'){
			$vat =	getTranslatedString('LBL_VAT','Invoice',$invoice_lang);
			$vat_string = "$vat ($get_vat_procent%):";
		}else{
			$vat =	getTranslatedString('LBL_VAT','Invoice',$invoice_lang);
			$vat2 =	getTranslatedString('LBL_VAT','Invoice','lt_lt');
			$vat_string = "$vat2 ($get_vat_procent%)/$vat ($get_vat_procent%):";
		}		
		
		$summaryModel->set("$invoice_write:", decode_html($associated_products[1]['rolename']." ".$associated_products[1]['first_name']." ".$associated_products[1]['last_name']));
		$summaryModel->set($vat_string,'');


		if ($this->moduleName == 'Invoice') {
			$receivedVal = $this->focusColumnValue("received");
			if (!$receivedVal) {
				$this->focus->column_fields["received"] = 0;
			}
			//If Received value is exist then only Recieved, Balance details should present in PDF
			if ($this->formatPrice($this->focusColumnValue("received")) > 0) {
				$summaryModel->set(getTranslatedString("Received", $this->moduleName), $this->formatPrice($this->focusColumnValue("received")));
				$summaryModel->set(getTranslatedString("Balance", $this->moduleName), $this->formatPrice($this->focusColumnValue("balance")));
			}
		}
		return $summaryModel;
	}

	function buildHeaderModel() {
		$headerModel = new Vtiger_PDF_Model();
		$headerModel->set('title', $this->buildHeaderModelTitle());
		$modelColumns = array($this->buildHeaderModelColumnLeft(), $this->buildHeaderModelColumnCenter(), $this->buildHeaderModelColumnRight());
		$headerModel->set('columns', $modelColumns);

		return $headerModel;
	}

	function buildHeaderModelTitle() {
		return $this->moduleName;
	}

	function buildHeaderModelColumnLeft() {
		global $adb;

		// Company information
		$result = $adb->pquery("SELECT * FROM vtiger_organizationdetails", array());
		$num_rows = $adb->num_rows($result);
		if($num_rows) {
			$resultrow = $adb->fetch_array($result);

			// ITOMA
			$RECORD_ID = $_REQUEST['record'];
			$result2 = $adb->pquery("SELECT IF(cf_2114 = 'LT', 'lt_lt', 'en_us') AS lang FROM vtiger_invoicecf WHERE invoiceid = ?", array($RECORD_ID));
			$invoice_lang = $adb->query_result($result2,0,'lang');

			$service_provider =	getTranslatedString('LBL_PROVIDER','Invoice',$invoice_lang);
			$legal_entity_code_word =	getTranslatedString('LBL_LEGAL_ENTITY_CODE','Invoice',$invoice_lang);
			$vat =	getTranslatedString('LBL_VAT_CODE','Invoice',$invoice_lang);
			$bill_address =	getTranslatedString('LBL_BILL_ADDRESS','Invoice',$invoice_lang);
			$bank =	getTranslatedString('LBL_BANK','Invoice',$invoice_lang);
			$phone =	getTranslatedString('LBL_PHONE','Invoice',$invoice_lang);

			$additionalCompanyInfo = array();
			$additionalCompanyInfoTitle = array();

			$additionalCompanyInfoTitle[] = ' '.$service_provider."\n";
			$additionalCompanyInfoTitle[] = $legal_entity_code_word."\n";
			$additionalCompanyInfoTitle[] = $val."\n";
			$additionalCompanyInfoTitle[] = $bill_address."\n";
			$additionalCompanyInfoTitle[] = $phone."\n";
			// $additionalCompanyInfoTitle[] = 'A/S Nr.'."\n\n";
			$additionalCompanyInfoTitle[] = $bank ."\n";
			// $additionalCompanyInfoTitle[] = 'Banko kodas'."\n";


			$additionalCompanyInfo[] = ' '.$resultrow['organizationname']."\n";
			$additionalCompanyInfo[] = $resultrow['companycode']."\n";
			if(!empty($resultrow['vatid'])) $additionalCompanyInfo[]= $resultrow['vatid']."\n"; 

			$additionalCompanyInfo[] = $resultrow['address'] = $resultrow['address'];
			if(!empty($resultrow['city'])) $additionalCompanyInfo[]= ", ".$resultrow['city'];
			if(!empty($resultrow['state'])) $additionalCompanyInfo[]= ",".$resultrow['state'];
			if(!empty($resultrow['country'])) $additionalCompanyInfo[]= ", ".$resultrow['country'];
			if(!empty($resultrow['code'])) $additionalCompanyInfo[]= $resultrow['code'] = str_replace("LT-", "", $resultrow['code']);
			if(!empty($resultrow['phone']))	$additionalCompanyInfo[]= "\n" .$resultrow['phone'];
			if(!empty($resultrow['bankaccountnum'])) $additionalCompanyInfo[]= "\n".$resultrow['bankname']. "\n".$resultrow['bankaccountnum']." ".$resultrow['bankcode']." ". ($resultrow['bankaccountnum2']? $resultrow['bankname2']. "\n".$resultrow['bankaccountnum2']." ".$resultrow['bankcode2'] : '') ;

			// if(!empty($resultrow['bankname'])) $additionalCompanyInfo[]= "\n".$resultrow['bankname'];
			// if(!empty($resultrow['bankcode']))	$additionalCompanyInfo[]= "\n".$resultrow['bankcode'];		


			if(!empty($resultrow['fax']))	$additionalCompanyInfo[]= "\n".getTranslatedString("Fax ", $this->moduleName). $resultrow['fax'];
			if(!empty($resultrow['website']))	$additionalCompanyInfo[]= "\n".getTranslatedString("Website ", $this->moduleName). $resultrow['website'];


				

			$modelColumnLeft = array(
					'logo' => "test/logo/".$resultrow['logoname'],				
					'title' => decode_html($this->joinValues($additionalCompanyInfoTitle, ' ')),
					'content' => decode_html($this->joinValues($additionalCompanyInfo, ' '))
			);
		}
		return $modelColumnLeft;
	}

	function buildHeaderModelColumnCenter() {
		$customerName = $this->resolveReferenceLabel($this->focusColumnValue('account_id'), 'Accounts');
		$contactName = $this->resolveReferenceLabel($this->focusColumnValue('contact_id'), 'Contacts');

		$customerNameLabel = getTranslatedString('Customer Name', $this->moduleName);
		$contactNameLabel = getTranslatedString('Contact Name', $this->moduleName);
		$modelColumnCenter = array(
				$customerNameLabel => $customerName,
				$contactNameLabel  => $contactName,
		);
		return $modelColumnCenter;
	}

	function buildHeaderModelColumnRight() {
		$issueDateLabel = getTranslatedString('Issued Date', $this->moduleName);
		$validDateLabel = getTranslatedString('Valid Date', $this->moduleName);
		$billingAddressLabel = getTranslatedString('Billing Address', $this->moduleName);
		$shippingAddressLabel = getTranslatedString('Shipping Address', $this->moduleName);

		$modelColumnRight = array(
				'dates' => array(
						$issueDateLabel  => $this->formatDate(date("Y-m-d")),
						$validDateLabel  => $this->formatDate($this->focusColumnValue('validtill')),
				),
				$billingAddressLabel  => $this->buildHeaderBillingAddress(),
				$shippingAddressLabel => $this->buildHeaderShippingAddress()
		);
		return $modelColumnRight;
	}

	function buildFooterModel() {
		$footerModel = new Vtiger_PDF_Model();
		$footerModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_DATA_KEY, from_html($this->focusColumnValue('description')));
		$footerModel->set(Vtiger_PDF_InventoryFooterViewer::$TERMSANDCONDITION_DATA_KEY, from_html($this->focusColumnValue('terms_conditions')));
		return $footerModel;
	}

	function buildFooterLabelModel() {
		$labelModel = new Vtiger_PDF_Model();
		$associated_products = $this->associated_products;
		$invoice_type = $associated_products[1]['invoice_type'];
		global $adb;

		$RECORD_ID = $_REQUEST['record'];
		$result2 = $adb->pquery("SELECT IF(cf_2114 = 'LT', 'lt_lt', 'en_us') AS lang FROM vtiger_invoicecf WHERE invoiceid = ?", array($RECORD_ID));
		$invoice_lang = $adb->query_result($result2,0,'lang');

		$warning = getTranslatedString('LBL_WARNING','Invoice',$invoice_lang);
		$transfer_requirements = getTranslatedString('LBL_TRANSFER_REQUIREMENTS','Invoice',$invoice_lang); 
		$thanks_for_use_our_services = getTranslatedString('LBL_THANKS_FOR_USE_OUR_SERVICES','Invoice',$invoice_lang); 
		
		// $labelModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_LABEL_KEY, getTranslatedString('Description',$this->moduleName));
		// $labelModel->set(Vtiger_PDF_InventoryFooterViewer::$TERMSANDCONDITION_LABEL_KEY, getTranslatedString('Terms & Conditions',$this->moduleName));
		if($invoice_type != 'Kreditinė'){
			$labelModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_HEADER_KEY,$warning."!","");	
			$labelModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_FIRST_KEY,$transfer_requirements,"");
		}

		$labelModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_SECOND_KEY,$thanks_for_use_our_services."!","");
		$labelModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_THIRD_KEY,"www.parnasas.lt","");
		$labelModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_FOURT_KEY,"www.perkraustymas.lt","");
		$labelModel->set(Vtiger_PDF_InventoryFooterViewer::$DESCRIPTION_IMAGE_KEY,'test/logo/UKAS_AND_ISO_9001_ISO_4001_RGB.JPG','');
		return $labelModel;
	}

	function buildPagerModel() {
		$footerModel = new Vtiger_PDF_Model();
		$footerModel->set('format', '-%s-');
		return $footerModel;
	}

	function getWatermarkContent() {
		return '';
	}

	function buildWatermarkModel() {
		$watermarkModel = new Vtiger_PDF_Model();
		$watermarkModel->set('content', $this->getWatermarkContent());
		return $watermarkModel;
	}

	function buildHeaderBillingAddress() {
		$billPoBox	= $this->focusColumnValues(array('bill_pobox'));
		$billStreet = $this->focusColumnValues(array('bill_street'));
		$billCity	= $this->focusColumnValues(array('bill_city'));
		$billState	= $this->focusColumnValues(array('bill_state'));
		$billCountry = $this->focusColumnValues(array('bill_country'));
		$billCode	=  $this->focusColumnValues(array('bill_code'));
		$address	= $this->joinValues(array($billPoBox, $billStreet), ' ');
		$address .= "\n".$this->joinValues(array($billCity, $billState), ',')." ".$billCode;
		$address .= "\n".$billCountry;
		return $address;
	}

	function buildHeaderShippingAddress() {
		$shipPoBox	= $this->focusColumnValues(array('ship_pobox'));
		$shipStreet = $this->focusColumnValues(array('ship_street'));
		$shipCity	= $this->focusColumnValues(array('ship_city'));
		$shipState	= $this->focusColumnValues(array('ship_state'));
		$shipCountry = $this->focusColumnValues(array('ship_country'));
		$shipCode	=  $this->focusColumnValues(array('ship_code'));
		$address	= $this->joinValues(array($shipPoBox, $shipStreet), ' ');
		$address .= "\n".$this->joinValues(array($shipCity, $shipState), ',')." ".$shipCode;
		$address .= "\n".$shipCountry;
		return $address;
	}

	function buildCurrencySymbol() {
		global $adb;
		$currencyId = $this->focus->column_fields['currency_id'];
		if(!empty($currencyId)) {
			$result = $adb->pquery("SELECT currency_symbol FROM vtiger_currency_info WHERE id=?", array($currencyId));
			return decode_html($adb->query_result($result,0,'currency_symbol'));
		}
		return false;
	}

	function focusColumnValues($names, $delimeter="\n") {
		if(!is_array($names)) {
			$names = array($names);
		}
		$values = array();
		foreach($names as $name) {
			$value = $this->focusColumnValue($name, false);
			if($value !== false) {
				$values[] = $value;
			}
		}
		return $this->joinValues($values, $delimeter);
	}

	function focusColumnValue($key, $defvalue='') {
		$focus = $this->focus;
		if(isset($focus->column_fields[$key])) {
			return decode_html($focus->column_fields[$key]);
		}
		return $defvalue;
	}

	function resolveReferenceLabel($id, $module=false) {
		if(empty($id)) {
			return '';
		}
		if($module === false) {
			$module = getSalesEntityType($id);
		}
		$label = getEntityName($module, array($id));
		return decode_html($label[$id]);
	}

	function joinValues($values, $delimeter= "\n") {
		$valueString = '';
		foreach($values as $value) {
			if(empty($value)) continue;
			$valueString .= $value . $delimeter;
		}
		return rtrim($valueString, $delimeter);
	}

	function formatNumber($value) {
		return number_format($value);
	}

	function formatPrice($value, $decimal=2) {
		$currencyField = new CurrencyField($value);
		return $currencyField->getDisplayValue(null, true);
	}

	function formatDate($value) {
		return DateTimeField::convertToUserFormat($value);
	}

}
?>
