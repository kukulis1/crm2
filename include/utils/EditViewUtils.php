<?php
/*********************************************************************************
 * The contents of this file are subject to the SugarCRM Public License Version 1.1.2
 * ("License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.sugarcrm.com/SPL
 * Software distributed under the License is distributed on an  "AS IS"  basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * The Original Code is:  SugarCRM Open Source
 * The Initial Developer of the Original Code is SugarCRM, Inc.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.;
 * All Rights Reserved.
 * Contributor(s): ______________________________________.
 ********************************************************************************/
/*********************************************************************************
 * $Header: /advent/projects/wesat/vtiger_crm/sugarcrm/include/utils/EditViewUtils.php,v 1.188 2005/04/29 05:5 * 4:39 rank Exp
 * Description:  Includes generic helper functions used throughout the application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/


require_once('include/database/PearDatabase.php');
require_once('include/ComboUtil.php'); //new
require_once('include/utils/CommonUtils.php'); //new
require_once 'modules/PickList/DependentPickListUtils.php';

/** This function returns the vtiger_invoice object populated with the details from sales order object.
* Param $focus - Invoice object
* Param $so_focus - Sales order focus
* Param $soid - sales order id
* Return type is an object array
*/

function getConvertSoToInvoice($focus,$so_focus,$soid)
{
	global $log,$current_user;
	$log->debug("Entering getConvertSoToInvoice(".get_class($focus).",".get_class($so_focus).",".$soid.") method ...");
    $log->info("in getConvertSoToInvoice ".$soid);
    $xyz=array('bill_street','bill_city','bill_code','bill_pobox','bill_country','bill_state','ship_street','ship_city','ship_code','ship_pobox','ship_country','ship_state');
	for($i=0;$i<count($xyz);$i++){
		if (getFieldVisibilityPermission('SalesOrder', $current_user->id,$xyz[$i]) == '0'){
			$so_focus->column_fields[$xyz[$i]] = $so_focus->column_fields[$xyz[$i]];
		}
		else
			$so_focus->column_fields[$xyz[$i]] = '';
	}
	$focus->column_fields['salesorder_id'] = $soid;
	$focus->column_fields['subject'] = $so_focus->column_fields['subject'];
	$focus->column_fields['customerno'] = $so_focus->column_fields['customerno'];
	$focus->column_fields['duedate'] = $so_focus->column_fields['duedate'];
	$focus->column_fields['contact_id'] = $so_focus->column_fields['contact_id'];//to include contact name in Invoice
	$focus->column_fields['account_id'] = $so_focus->column_fields['account_id'];
	$focus->column_fields['exciseduty'] = $so_focus->column_fields['exciseduty'];
	$focus->column_fields['salescommission'] = $so_focus->column_fields['salescommission'];
	$focus->column_fields['vtiger_purchaseorder'] = $so_focus->column_fields['vtiger_purchaseorder'];
	$focus->column_fields['bill_street'] = $so_focus->column_fields['bill_street'];
	$focus->column_fields['ship_street'] = $so_focus->column_fields['ship_street'];
	$focus->column_fields['bill_city'] = $so_focus->column_fields['bill_city'];
	$focus->column_fields['ship_city'] = $so_focus->column_fields['ship_city'];
	$focus->column_fields['bill_state'] = $so_focus->column_fields['bill_state'];
	$focus->column_fields['ship_state'] = $so_focus->column_fields['ship_state'];
	$focus->column_fields['bill_code'] = $so_focus->column_fields['bill_code'];
	$focus->column_fields['ship_code'] = $so_focus->column_fields['ship_code'];
	$focus->column_fields['bill_country'] = $so_focus->column_fields['bill_country'];
	$focus->column_fields['ship_country'] = $so_focus->column_fields['ship_country'];
	$focus->column_fields['bill_pobox'] = $so_focus->column_fields['bill_pobox'];
	$focus->column_fields['ship_pobox'] = $so_focus->column_fields['ship_pobox'];
	$focus->column_fields['description'] = $so_focus->column_fields['description'];
	$focus->column_fields['terms_conditions'] = $so_focus->column_fields['terms_conditions'];
    $focus->column_fields['currency_id'] = $so_focus->column_fields['currency_id'];
    $focus->column_fields['conversion_rate'] = $so_focus->column_fields['conversion_rate'];
    $focus->column_fields['pre_tax_total'] = $so_focus->column_fields['pre_tax_total'];

	$log->debug("Exiting getConvertSoToInvoice method ...");
	return $focus;

}

/** This function returns the detailed list of vtiger_products associated to a given entity or a record.
* Param $module - module name
* Param $focus - module object
* Param $seid - sales entity id
* Return type is an object array
*/
function getAssociatedProducts($module, $focus, $seid = '', $refModuleName = false)
{

	global $log;
	$log->debug("Entering getAssociatedProducts(".$module.",".get_class($focus).",".$seid."='') method ...");
	global $adb;
	$output = '';
	global $theme,$current_user;

	global $_REQUEST;

	$no_of_decimal_places = getCurrencyDecimalPlaces();
	$theme_path="themes/".$theme."/";
	$image_path=$theme_path."images/";
	$product_Detail = Array(); 


	$inventoryModules = getInventoryModules();
	if (in_array($module, $inventoryModules)) {
		$taxtype = getInventoryTaxType($module, $focus->id);
	}

	$additionalProductFieldsString = $additionalServiceFieldsString = '';
	$lineItemSupportedModules = array('Accounts', 'Contacts', 'Leads', 'Potentials');

	// DG 15 Aug 2006
	// Add "ORDER BY sequence_no" to retain add order on all inventoryproductrel items
				//Logtime 2018-04-11


	if (in_array($module, $inventoryModules))
	{
		
		$query="SELECT 
					CASE WHEN vtiger_products.productid != '' THEN vtiger_products.productname ELSE vtiger_service.servicename END as productname,
					CASE WHEN vtiger_products.productid != '' THEN vtiger_products.product_no ELSE vtiger_service.service_no END as productcode,
					CASE WHEN vtiger_products.productid != '' THEN vtiger_products.unit_price ELSE vtiger_service.unit_price END as unit_price,
					CASE WHEN vtiger_products.productid != '' THEN vtiger_products.qtyinstock ELSE 'NA' END as qtyinstock,
					CASE
						WHEN vtiger_products.productid = 121391 THEN 'Item'  
						WHEN vtiger_inventoryproductrel.productid != 36641 THEN 'Products'					 
						ELSE 'Services' 
					END as entitytype,
					CASE WHEN vtiger_products.productid = 36641 THEN '' ELSE ROUND(vtiger_inventoryproductrel.m3,3) END as m3_volume,
					CASE WHEN vtiger_products.productid = 36641 THEN '' ELSE vtiger_inventoryproductrel.cargo_wgt END as cargo_kg,
					CASE WHEN vtiger_products.productid = 36641 THEN '' ELSE vtiger_salesorder.shipment_code END as shipment_code,
					CASE WHEN vtiger_products.productid = 121391 THEN vtiger_inventoryproductrel.cargo_wgt ELSE '' END as productname,
					vtiger_inventoryproductrel.listprice, vtiger_products.is_subproducts_viewable,vtiger_inventoryproductrel.description AS product_description,vtiger_inventoryproductrel.comment, vtiger_inventoryproductrel.tax2, vtiger_inventoryproductrel.tax3,
					vtiger_inventoryproductrel.*, vtiger_inventoryproductrel.cargo_length,vtiger_inventoryproductrel.cargo_width,vtiger_inventoryproductrel.cargo_height,vtiger_crmentity.deleted, vtiger_crmentity.createdtime,vtiger_inventoryproductrel.note, vtiger_inventoryproductrel.comment, vtiger_inventoryproductrel.object, vtiger_inventoryproductrel.employee,vtiger_inventoryproductrel.costcenter as costid,DATE_FORMAT(se.createdtime, '%Y-%m-%d') as sale_createdtime,vtiger_costcenter.costcenter_tks_cost as costcenter,bill_country,ship_country ";
						if($module == 'Invoice'){
							$query .=", cf_2032 as customer_order_code, sequence_no";

							$query .= ",(SELECT
											CASE WHEN app_taxable_dimensions.type = 'revised' 
												THEN  IF(revised_measure IN (1,7) OR revised_measure IN ('pll','nestd.'), SUM(pll_rel.pll), 0)
												ELSE  IF(cargo_measure IN (1,7) OR cargo_measure IN ('pll','nestd.'), ROUND(SUM((ordered_length * ordered_width) * quantity) /0.96, 2), 0)
											END 
										FROM vtiger_inventoryproductrel as pll_rel
										LEFT JOIN `app_taxable_dimensions` ON app_taxable_dimensions.salesorderid=vtiger_inventoryproductrel.order_id  
										WHERE pll_rel.id = vtiger_inventoryproductrel.order_id
										) AS pll_place";

						}else if($module == 'PurchaseOrder'){
							$query .=", sequence_no";
						}
					$query .=" FROM vtiger_inventoryproductrel				
					LEFT JOIN vtiger_salesorder ON vtiger_salesorder.salesorderid=vtiger_inventoryproductrel.order_id ";
					if($module == 'Invoice'){
						$query .=" LEFT JOIN `vtiger_salesordercf` ON `vtiger_salesordercf`.`salesorderid`=`vtiger_salesorder`.`salesorderid` ";
					}

				$query .="	LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
          			LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
					LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
					LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_inventoryproductrel.id
					LEFT JOIN vtiger_crmentity se ON se.crmid=vtiger_salesorder.salesorderid
					LEFT JOIN vtiger_products ON vtiger_products.productid=vtiger_inventoryproductrel.productid
					LEFT JOIN vtiger_service ON vtiger_service.serviceid=vtiger_inventoryproductrel.productid		
					LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=vtiger_inventoryproductrel.costcenter								
					WHERE id=? AND vtiger_inventoryproductrel.inventory_type != 'FuelSurcharge' AND vtiger_inventoryproductrel.inventory_type != 'stevedoring'
					GROUP BY CASE WHEN vtiger_inventoryproductrel.source = 'Metrika' THEN external_load_id ELSE sequence_no END
					ORDER BY sequence_no ";
			$params = array($focus->id); 
	}
	elseif(in_array($module, $lineItemSupportedModules))
	{
		$query = '(SELECT vtiger_products.productid, vtiger_products.productname, vtiger_products.product_no AS productcode, vtiger_products.purchase_cost,
					vtiger_products.unit_price, vtiger_products.qtyinstock, vtiger_crmentity.deleted, "Products" AS entitytype,
					vtiger_products.is_subproducts_viewable, vtiger_crmentity.description '.$additionalProductFieldsString.' FROM vtiger_products
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_products.productid
					INNER JOIN vtiger_seproductsrel ON vtiger_seproductsrel.productid=vtiger_products.productid
					INNER JOIN vtiger_productcf ON vtiger_products.productid = vtiger_productcf.productid
					WHERE vtiger_seproductsrel.crmid=? AND vtiger_crmentity.deleted=0 AND vtiger_products.discontinued=1)
					UNION
					(SELECT vtiger_service.serviceid AS productid, vtiger_service.servicename AS productname, vtiger_service.service_no AS productcode,
					vtiger_service.purchase_cost, vtiger_service.unit_price, "NA" as qtyinstock, vtiger_crmentity.deleted,
					"Services" AS entitytype, 1 AS is_subproducts_viewable, vtiger_crmentity.description '.$additionalServiceFieldsString.' FROM vtiger_service
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_service.serviceid
					INNER JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_service.serviceid
					INNER JOIN vtiger_servicecf ON vtiger_service.serviceid = vtiger_servicecf.serviceid
					WHERE vtiger_crmentityrel.crmid=? AND vtiger_crmentity.deleted=0 AND vtiger_service.discontinued=1)';
			$params = array($seid, $seid);
	}
	elseif ($module == 'Vendors') {
		$query = 'SELECT vtiger_products.productid, vtiger_products.productname, vtiger_products.product_no AS productcode, vtiger_products.purchase_cost,
					vtiger_products.unit_price, vtiger_products.qtyinstock, vtiger_crmentity.deleted, "Products" AS entitytype,
					vtiger_products.is_subproducts_viewable, vtiger_crmentity.description '.$additionalServiceFieldsString.' FROM vtiger_products
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_products.productid
					INNER JOIN vtiger_vendor ON vtiger_vendor.vendorid = vtiger_products.vendor_id
					INNER JOIN vtiger_productcf ON vtiger_products.productid = vtiger_productcf.productid
					WHERE vtiger_vendor.vendorid=? AND vtiger_crmentity.deleted=0 AND vtiger_products.discontinued=1';
			$params = array($seid);
	}
	elseif ($module == 'HelpDesk') {
		$query = 'SELECT vtiger_service.serviceid AS productid, vtiger_service.servicename AS productname, vtiger_service.service_no AS productcode,
					vtiger_service.purchase_cost, vtiger_service.unit_price, "NA" as qtyinstock, vtiger_crmentity.deleted,
					"Services" AS entitytype, 1 AS is_subproducts_viewable, vtiger_crmentity.description '.$additionalServiceFieldsString.' FROM vtiger_service
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_service.serviceid
					INNER JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_service.serviceid
					INNER JOIN vtiger_servicecf ON vtiger_service.serviceid = vtiger_servicecf.serviceid
					WHERE vtiger_crmentityrel.crmid=? AND vtiger_crmentity.deleted=0 AND vtiger_service.discontinued=1';
			$params = array($seid);
	}
	elseif($module == 'Products')
	{
		$query = 'SELECT vtiger_products.productid, vtiger_products.productname, vtiger_products.product_no AS productcode, vtiger_products.purchase_cost,
					vtiger_products.unit_price, vtiger_products.qtyinstock, vtiger_crmentity.deleted, "Products" AS entitytype,
					vtiger_products.is_subproducts_viewable, vtiger_crmentity.description '.$additionalProductFieldsString.' FROM vtiger_products
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_products.productid
					INNER JOIN vtiger_productcf ON vtiger_products.productid = vtiger_productcf.productid
					WHERE vtiger_crmentity.deleted=0 AND vtiger_products.productid=?';
			$params = array($seid);
	}
	elseif($module == 'Services')
	{
		$query='SELECT vtiger_service.serviceid AS productid, vtiger_service.servicename AS productname, vtiger_service.service_no AS productcode,
					vtiger_service.purchase_cost, vtiger_service.unit_price AS unit_price, "NA" AS qtyinstock, vtiger_crmentity.deleted,
					"Services" AS entitytype, 1 AS is_subproducts_viewable, vtiger_crmentity.description '.$additionalServiceFieldsString.' FROM vtiger_service
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_service.serviceid
					INNER JOIN vtiger_servicecf ON vtiger_service.serviceid = vtiger_servicecf.serviceid
					WHERE vtiger_crmentity.deleted=0 AND vtiger_service.serviceid=?';
			$params = array($seid);
	}
	
	if($module == 'Invoice'){
		$sql = $adb->pquery("SELECT `salesorderid` FROM `vtiger_invoice` WHERE `invoiceid` = ".$focus->id);
		$sql = $adb->fetch_array($sql); 
		$salesfrominvoice = $sql['salesorderid'];

		if($_REQUEST['salesorder_id']){
			$salesList = $salesfrominvoice;
		}else{		
			if(empty($salesfrominvoice)){
				$salesList =	"SELECT `salesorderid` FROM `vtiger_invoice` WHERE `invoiceid` = ".$focus->id;
			}else{
				$salesList = $salesfrominvoice;
			}
		}


		if(!empty($salesfrominvoice)){
			$invoice_query = "SELECT vtiger_salesorder.salesorderid, vtiger_salesorder.shipment_code, vtiger_invoice.s_h_amount as vatprice,vtiger_salesordercf.cf_855 AS order_type, vtiger_invoice.duedate,vtiger_invoice.subtotal,vtiger_invoice.total,vtiger_users.title as rolename,vtiger_users.first_name,vtiger_users.last_name,vtiger_taxregions.value as vatprocent,vtiger_taxregions.article,vtiger_invoice.region_id, DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') as createdtime, vtiger_inventoryproductrel.pll,app_measures.code as cargo_measure, second.id as order_id, FORMAT(second.quantity,0) as quantity, DATE_FORMAT(vtiger_invoice.invoicedate, '%Y-%m-%d') as invoice_createdtime,vtiger_invoicecf.cf_1486 as print_template,vtiger_invoicecf.cf_1277 as invoice_type,cf_1570 as cargo_comments, cf_2032 as customer_order_code
											FROM `vtiger_invoice`
											LEFT JOIN vtiger_salesorder ON vtiger_salesorder.salesorderid IN ($salesList)
											LEFT JOIN `vtiger_salesordercf` ON `vtiger_salesordercf`.`salesorderid`=`vtiger_salesorder`.`salesorderid`
											LEFT JOIN `vtiger_crmentity` AS new_entity ON `new_entity`.`crmid`=`vtiger_invoice`.`invoiceid`
											LEFT JOIN `vtiger_users` ON `vtiger_users`.`id`=`new_entity`.`smownerid`									
											LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`vtiger_invoice`.`region_id`
											LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_salesorder`.`salesorderid`
											LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.order_id=vtiger_salesorder.salesorderid AND vtiger_inventoryproductrel.id=vtiger_invoice.invoiceid
											LEFT JOIN vtiger_inventoryproductrel AS second ON second.id=vtiger_inventoryproductrel.order_id	
											LEFT JOIN app_measures ON app_measures.id=second.cargo_measure OR app_measures.code=second.cargo_measure	
											LEFT JOIN vtiger_invoicecf on vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid	
											LEFT JOIN vtiger_accountscf cf ON cf.accountid=vtiger_invoice.accountid							
											WHERE vtiger_invoice.invoiceid = ? 										
											ORDER BY vtiger_salesorder.salesorderid DESC";
		}else{
			$invoice_query = "SELECT vtiger_invoice.s_h_amount as vatprice, vtiger_invoice.duedate,vtiger_invoice.subtotal,vtiger_invoice.total,vtiger_users.title as rolename,
			vtiger_users.first_name,vtiger_users.last_name,vtiger_taxregions.value as vatprocent,vtiger_taxregions.article,vtiger_invoice.region_id, 
			DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') as createdtime, 
			FORMAT(quantity,0) as quantity, DATE_FORMAT(vtiger_invoice.invoicedate, '%Y-%m-%d') as invoice_createdtime,
			vtiger_invoicecf.cf_1486 as print_template,vtiger_invoicecf.cf_1277 as invoice_type,cf_1570 as cargo_comments,cargo_measure
													 FROM `vtiger_invoice`											
													 LEFT JOIN `vtiger_crmentity`  ON `crmid`=`vtiger_invoice`.`invoiceid`
													 LEFT JOIN `vtiger_users` ON `vtiger_users`.`id`=`vtiger_crmentity`.`smownerid`									
													 LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`vtiger_invoice`.`region_id`										
													 LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_invoice.invoiceid												
													 LEFT JOIN vtiger_invoicecf on vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid	
													 LEFT JOIN vtiger_accountscf cf ON cf.accountid=vtiger_invoice.accountid							
													 WHERE vtiger_invoice.invoiceid = ? 										
													 ORDER BY vtiger_inventoryproductrel.sequence_no DESC";

		}								
										
		$invoice_params = array($focus->id); 

		$subquery = "SELECT second.id as order_id, FORMAT(SUM(first.cargo_length * first.cargo_width * first.cargo_height),2) AS volume, first.cargo_measure as cargo_measure_invoice, first.discount_percent, first.discount_amount, app_services_type.id as service_id, app_services_type.name as service_type
		FROM vtiger_inventoryproductrel AS first				
		LEFT JOIN vtiger_inventoryproductrel AS second ON second.id=first.order_id
		LEFT JOIN vtiger_invoice on vtiger_invoice.invoiceid=first.id
		LEFT JOIN app_services_type  ON app_services_type.id=first.service
		WHERE first.id = ? 
		GROUP BY first.sequence_no  
		ORDER BY first.sequence_no";
		
		$subquery2 = "SELECT second.id
		FROM vtiger_inventoryproductrel AS first
		LEFT JOIN vtiger_inventoryproductrel AS second ON second.id=first.order_id
		LEFT JOIN vtiger_invoice on vtiger_invoice.invoiceid=first.id
		WHERE first.id = ? ";  

		$subquery4 = "SELECT * FROM app_services_type";


	}elseif($module == 'PurchaseOrder'){
		$purchase_query = "SELECT cargo_measure AS `object`, cargo_wgt AS purchase, cargo_length AS employee, purchase_date, costcenter_tks_cost AS costname,  vtiger_costcenter.costcenterid AS costid,vtiger_costcentercf.cf_1386 AS cost_code,vtiger_itemservice.itemserviceid 
		FROM vtiger_inventoryproductrel
		LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=vtiger_inventoryproductrel.cargo_width
		LEFT JOIN vtiger_costcentercf ON vtiger_costcentercf.costcenterid=vtiger_costcenter.costcenterid
		LEFT JOIN vtiger_itemservice ON vtiger_itemservice.itemservice_tks_name=vtiger_inventoryproductrel.cargo_wgt
		WHERE id = ?
		ORDER BY sequence_no ASC";				
		$purchase_params = array($focus->id);
	}



  	$result = $adb->pquery($query, $params);
	$invoice_result = $adb->pquery($invoice_query, $invoice_params);
	$measures = $adb->pquery($subquery, $invoice_params);
	$measures2 = $adb->pquery($subquery2, $invoice_params);

	if ( empty($purchase_query) ) {
		$purchase_query = 'select 1 from dual';
		$purchase_params = [];
	}

	$purchase_result = $adb->pquery($purchase_query, $purchase_params);


	$num_rows=$adb->num_rows($result);
	$num_rows2=$adb->num_rows($measures2);


	$rez =$adb->fetch_array($result);
	$service_names =$adb->pquery($subquery4);
	// $volumes =$adb->pquery($subquery5,  $invoice_params);


	for($i=1;$i<=$num_rows;$i++)
	{
		$deleted = $adb->query_result($result,$i-1,'deleted');
		$hdnProductId = $adb->query_result($result,$i-1,'productid');
		$hdnProductcode = $adb->query_result($result,$i-1,'productcode');
		$entityType = $adb->query_result($result,$i-1,'entitytype');
                
		if($module != 'PurchaseOrder') {
			$hdnProductcode = $adb->query_result($result,$i-1,'productname');
		}                 
                
		$productnameInv=$adb->query_result($result,$i-1,'productname');

		$productdescription=$adb->query_result($result,$i-1,'description');
		$comment=$adb->query_result($result,$i-1,'comment');
		$qtyinstock=$adb->query_result($result,$i-1,'qtyinstock');
		$qty=$adb->query_result($result,$i-1,'quantity');	
		$unitprice=$adb->query_result($result,$i-1,'unit_price');
		$listprice=$adb->query_result($result,$i-1,'listprice');
		$cargo_measure_for_invoice=$adb->query_result($result,$i-1,'cargo_measure');
		$entitytype=$adb->query_result($result,$i-1,'entitytype');
		$purchaseCost = $adb->query_result($result,$i-1,'purchase_cost');
		$margin = $adb->query_result($result,$i-1,'margin');
		$isSubProductsViewable = $adb->query_result($result, $i-1, 'is_subproducts_viewable');
		$cargo_wgt=$adb->query_result($result,$i-1,'cargo_kg');
		$cargo_length=$adb->query_result($result,$i-1,'cargo_length');
		$cargo_width = $adb->query_result($result,$i-1,'cargo_width');
		$cargo_height = $adb->query_result($result,$i-1,'cargo_height');
		$bill_ads = $adb->query_result($result,$i-1,'bill_ads');				
		$ship_ads = $adb->query_result($result,$i-1,'ship_ads');			
		$note = $adb->query_result($result,$i-1,'note');		
		$order_id = $adb->query_result($result,$i-1,'external_order_id');		
		$load_id = $adb->query_result($result,$i-1,'external_load_id');		
		$volume = $adb->query_result($result,$i-1,'m3_volume');
		$shipment_code = $adb->query_result($result,$i-1,'shipment_code');
		$createdtime = $adb->query_result($result,$i-1,'sale_createdtime');
		$costcenter = $adb->query_result($result,$i-1,'costcenter');
		$meters = $adb->query_result($result,$i-1,'meters');
	
	
		if($module == 'Invoice'){	
			$independentInvoice = $adb->query_result($result,$i-1,'productname');
			$independent = $adb->query_result($result,$i-1,'productid');
			$vatprice = $adb->query_result($invoice_result,$i-1,'vatprice');		
			$order_type = $adb->query_result($invoice_result,$i-1,'order_type');
			$duedate = $adb->query_result($invoice_result,$i-1,'duedate');
			$subtotal = $adb->query_result($invoice_result,$i-1,'subtotal');
			$total = $adb->query_result($invoice_result,$i-1,'total');
			$rolename = $adb->query_result($invoice_result,$i-1,'rolename');
			$first_name = $adb->query_result($invoice_result,$i-1,'first_name');
			$last_name = $adb->query_result($invoice_result,$i-1,'last_name');
			$regionid = $adb->query_result($invoice_result,$i-1,'region_id');
			$vatprocent = $adb->query_result($invoice_result,$i-1,'vatprocent');
			$article = $adb->query_result($invoice_result,$i-1,'article');
			$customer_order_code = $adb->query_result($result,$i-1,'customer_order_code');
			$sequence_no = $adb->query_result($result,$i-1,'sequence_no');
			$pll_place = $adb->query_result($result,$i-1,'pll_place');


			if(!$customer_order_code){
				$customer_order_code = '';
			}

			$bill_country = $adb->query_result($result,$i-1,'bill_country');
			$ship_country = $adb->query_result($result,$i-1,'ship_country');
			
			// $createdtime = $adb->query_result($invoice_result,$i-1,'sale_createdtime');
			$template = $adb->query_result($invoice_result,0,'print_template');
			$invoice_type = $adb->query_result($invoice_result,$i-1,'invoice_type');
			$employee = $adb->query_result($result,$i-1,'employee');
			$object = $adb->query_result($result,$i-1,'object');
			$costid = $adb->query_result($result,$i-1,'costid');

			$pll = $adb->query_result($invoice_result,$i-1,'pll');
			$cargo_comments = $adb->query_result($invoice_result,$i-1,'cargo_comments');
			$invoice_createdtime = $adb->query_result($invoice_result,$i-1,'invoice_createdtime');	
			$service_id= $adb->query_result($measures,$i-1,'service_id');	
			$service_type = $adb->query_result($measures,$i-1,'service_type');				
			$salesorderid = $adb->query_result($measures,$i-1,'order_id');
			$cargo_measure_invoice = $adb->query_result($measures,$i-1,'cargo_measure_invoice');	
			$discount_percent = $adb->query_result($measures,$i-1,'discount_percent');	
			$discount_amount = $adb->query_result($measures,$i-1,'discount_amount');	

			$cargo_measure = array();
			$quantity = array();
			for($e = 0; $e < $num_rows2; $e++){				
					$row =  $adb->query_result_all($invoice_result,$e);	
					$cargo_measure[] = array(
																$row['order_id'] => array('measure' =>  $row['cargo_measure'], 'quantity' =>  $row['quantity'] ));																										
			}
		}elseif($module == 'SalesOrder'){
			$services = $adb->pquery("SELECT margin,app_services_type.id as service_id FROM vtiger_inventoryproductrel 
																LEFT JOIN app_services_type  ON app_services_type.id=vtiger_inventoryproductrel.service 
																WHERE vtiger_inventoryproductrel.id = ? ORDER BY sequence_no", array($focus->id));
			$service_id = $adb->query_result($services,$i-1,'service_id');
			
		}elseif($module == 'PurchaseOrder'){
			$purchase = $adb->query_result($purchase_result,$i-1,'purchase');
			$employee = $adb->query_result($purchase_result,$i-1,'employee');
			$object = $adb->query_result($purchase_result,$i-1,'object');
			$purchase_date = $adb->query_result($purchase_result,$i-1,'purchase_date');
			$costname = $adb->query_result($purchase_result,$i-1,'costname');
			$costid = $adb->query_result($purchase_result,$i-1,'costid');
			$cost_code = $adb->query_result($purchase_result,$i-1,'cost_code');
			$itemserviceid = $adb->query_result($purchase_result,$i-1,'itemserviceid');
			$region = $adb->query_result($result,$i-1,'tax2');
			$tax3=$adb->query_result($result,$i-1,'tax3');
			$sequence_no = $adb->query_result($result,$i-1,'sequence_no');
		}	


		if($module != 'PurchaseOrder') {				
			if($entitytype == 'Products') {										
				$productname = ($cargo_wgt != 'z' && $cargo_length != 'z' && $cargo_width != 'z' && $cargo_height != 'z'?  $cargo_wgt." ".(float)$cargo_length . 'x' . (float)$cargo_width . 'x' . (float)$cargo_height : 'Nenurodyti matmenys');						} else {
				$productname=$adb->query_result($result,$i-1,'productname');
			}
		}

		if($cargo_wgt){
			$product_Detail[$i]['cargo_wgt'.$i] = $cargo_wgt;
		}

		if($cargo_length){
			$product_Detail[$i]['cargo_length'.$i] = $cargo_length;
		}

		if($cargo_width){
			$product_Detail[$i]['cargo_width'.$i] = $cargo_width;
		}

		if($cargo_height){
			$product_Detail[$i]['cargo_height'.$i] = $cargo_height;
		}

		if($productnameInv){
			$product_Detail[$i]['productnameInv'.$i] = $productnameInv;
		}			
		        
		if ($purchaseCost) {
			$product_Detail[$i]['purchaseCost'.$i] = number_format($purchaseCost, $no_of_decimal_places, '.', '');
		}

		if ($margin) {
			$product_Detail[$i]['margin'.$i] = number_format($margin, $no_of_decimal_places, '.', '');
		}

		if($service_id){
			$product_Detail[$i]['service_id'.$i] = $service_id;
		}

		if($order_id){
			$product_Detail[$i]['order_id'] = $order_id;
		}

		if($load_id){
			$product_Detail[$i]['load_id'.$i] = $load_id;
		}

		if($meters){
			$product_Detail[$i]['meters'.$i] = $meters;
		}
	
		if($module == 'Invoice'){

			if($cargo_measure_for_invoice){
				$product_Detail[$i]['cargo_measure_for_invoice'.$i] = $cargo_measure_for_invoice;
			}

			if($productname) {
				$product_Detail[$i]['productnameDimensions'.$i] = $productname;
			}	

			if($independentInvoice){
				$product_Detail[$i]['independentInvoice'.$i] = $independentInvoice;
			}	

			if($independent){
				$product_Detail[$i]['independent'] = $independent;
			}	

			if($salesorderid) {
				$product_Detail[$i]['salesorderid'.$i] = $salesorderid;
			}	

			if($invoice_createdtime){
				$product_Detail[$i]['invoice_createdtime'.$i] = $invoice_createdtime;
			}

			if($costid){
				$product_Detail[$i]['costid'.$i] = $costid;
			}

			if($employee){
				$product_Detail[$i]['employee'.$i] = $employee;
			}

			if($template){
				$product_Detail[$i]['print_template'] = $template;
			}

			if($invoice_type){
				$product_Detail[$i]['invoice_type'] = $invoice_type;
			}

			if($object){
				$product_Detail[$i]['object'.$i] = $object;
			}

			if($discount_percent){
				$product_Detail[$i]['discount_percent'.$i] = $discount_percent;
			}

			if($discount_amount){
				$product_Detail[$i]['discount_amount'.$i] = $discount_amount;
			}

			if ($pll) {
				$product_Detail[$i]['pll'.$i] = number_format($pll, $no_of_decimal_places, '.', '');
			}	

			if ($cargo_comments) {
				$product_Detail[$i]['cargo_comments'] = $cargo_comments;
			}

			if($cargo_wgt){
				$product_Detail[$i]['cargo_wgt'.$i] = $cargo_wgt;
			}

			if($cargo_measure_invoice){
				$product_Detail[$i]['cargo_measure_invoice'.$i] = $cargo_measure_invoice;
			}	

			if($quantity){
				$product_Detail[$i]['quantity'.$i] = $quantity;		
			}

			if ($note) {
				$product_Detail[$i]['note'.$i] = $note;
			}elseif($entityType == 'Item'){
				$product_Detail[$i]['note'.$i] = $comment;
			}

			if($volume){
				$product_Detail[$i]['volume'.$i] = $volume;
			}

			if ($subtotal) {
				$product_Detail[$i]['subtotal'] = number_format($subtotal, $no_of_decimal_places, '.', '');
			}

			if ($total) {
				$product_Detail[$i]['total'] = number_format($total, $no_of_decimal_places, '.', '');
			}

			if($shipment_code){
				$product_Detail[$i]['shipment_code'.$i] = $shipment_code;		
			}

			if($bill_country){
				$product_Detail[$i]['bill_country'.$i] = $bill_country;		
			}

			if($ship_country){
				$product_Detail[$i]['ship_country'.$i] = $ship_country;		
			}

			if($customer_order_code){
				$product_Detail[$i]['customer_order_code'.$i] = $customer_order_code;		
			}		

			if($sequence_no){
				$product_Detail[$i]['sequence_no'.$i] = $sequence_no;		
			}		

			if($bill_ads){
				$product_Detail[$i]['bill_ads'.$i] = $bill_ads;
			}

			if($ship_ads){
				$product_Detail[$i]['ship_ads'.$i] = $ship_ads;
			}

			if($vatprice){
				$product_Detail[$i]['vatprice'] = number_format($vatprice, $no_of_decimal_places, '.', '');
			}

			if($order_type){
				$product_Detail[$i]['order_type'.$i] = $order_type;
			}		
			
			if($duedate){
				$product_Detail[$i]['duedate'] = $duedate;
			}

			if($rolename){
				$product_Detail[$i]['rolename'] = $rolename;
			} 

			if($first_name){
				$product_Detail[$i]['first_name'] = $first_name;
			} 

			if($last_name){
				$product_Detail[$i]['last_name'] = $last_name;
			} 

			if($regionid){
				$product_Detail[$i]['regionid'] = $regionid;
			}

			if($vatprocent){
				$product_Detail[$i]['vatprocent'] = $vatprocent;
			}
			
			if($article){
				$product_Detail[$i]['article'] = $article;
			}
			
			if($createdtime){
				$product_Detail[$i]['createdtime'.$i] = $createdtime;
			} 

			if($service_names){
				$product_Detail[$i]['service_names'.$i] = $service_names;
			}

			if($service_type){
				$product_Detail[$i]['service_type'.$i] = $service_type;
			}
			
			if($costcenter){
				$product_Detail[$i]['cost_center'.$i] = $costcenter;
			}

			if($costcenter){
				$product_Detail[$i]['costname'.$i] = $costcenter;
			}

			if($pll_place){
				$product_Detail[$i]['pll_place'.$i] = $pll_place;
			}		
			
		}elseif($module == 'PurchaseOrder'){
			if($purchase){
				$product_Detail[$i]['purchase'.$i] = $purchase;
			}

			if($itemserviceid){
				$product_Detail[$i]['itemserviceid'.$i] = $itemserviceid;
			}

			if($region){
				$product_Detail[$i]['region'.$i] = $region;
			}

			if($employee){
				$product_Detail[$i]['employee'.$i] = $employee;
			}

			if($object){
				$product_Detail[$i]['object'.$i] = $object;
			}

			if($purchase_date){
				$product_Detail[$i]['purchase_date'.$i] = $purchase_date;
			}

			if($costname){
				$product_Detail[$i]['costname'.$i] = $costname;
			}

			if($costid){
				$product_Detail[$i]['costid'.$i] = $costid;
			}

			if($cost_code){
				$product_Detail[$i]['cost_code'.$i] = $cost_code;
			}

			if($sequence_no){
				$product_Detail[$i]['sequence_no'.$i] = $sequence_no;		
			}
		}
	


		if(($deleted) || (!isset($deleted))){
			$product_Detail[$i]['productDeleted'.$i] = true;
		}elseif(!$deleted){
			$product_Detail[$i]['productDeleted'.$i] = false;
		}


		if (!empty($entitytype)) {
			$product_Detail[$i]['entityType'.$i]=$entitytype;
		}

		if($listprice == '')
			$listprice = $unitprice;
		if($qty =='')
			$qty = 1;

		//calculate productTotal
		// itoma
			if($module == 'Invoice' && $entityType == 'Item'){
				$productTotal = $qty*$listprice;
			}elseif($module == 'Invoice'){
				$productTotal = $listprice;		
			}else{
				$productTotal = $qty*$listprice;
			}

	
	

		//Delete link in First column
		if($i != 1)
		{
			$product_Detail[$i]['delRow'.$i]="Del";
		}

		if (in_array($module, $lineItemSupportedModules) || $module === 'Vendors' || (!$focus->mode && $seid)) {
			$subProductsQuery = 'SELECT vtiger_seproductsrel.crmid AS prod_id, quantity FROM vtiger_seproductsrel
								 INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_seproductsrel.crmid
								 INNER JOIN vtiger_products ON vtiger_products.productid = vtiger_seproductsrel.crmid
								 WHERE vtiger_seproductsrel.productid=? AND vtiger_seproductsrel.setype=? AND vtiger_products.discontinued=1';

			$subParams = array($seid);
			if (in_array($module, $lineItemSupportedModules) || $module === 'Vendors') {
				$subParams = array($hdnProductId);
			}
			array_push($subParams, 'Products');
		} else {
			$subProductsQuery = 'SELECT productid AS prod_id, quantity FROM vtiger_inventorysubproductrel WHERE id=? AND sequence_no=?';
			$subParams = array($focus->id, $i);
		}
		$subProductsResult = $adb->pquery($subProductsQuery, $subParams);
		$subProductsCount = $adb->num_rows($subProductsResult);

		$subprodid_str='';
		$subprodname_str='';

		$subProductQtyList = array();
		for($j=0; $j<$subProductsCount; $j++){
			$sprod_id = $adb->query_result($subProductsResult, $j, 'prod_id');
			$sprod_name = getProductName($sprod_id);
			if(isset($sprod_name)) {
				$subQty = $adb->query_result($subProductsResult, $j, 'quantity');
				$subProductQtyList[$sprod_id] = array('name' => $sprod_name, 'qty' => $subQty);
				if(isRecordExists($sprod_id) && $_REQUEST['view'] === 'Detail') {
					$subprodname_str .= "<a href='index.php?module=Products&view=Detail&record=$sprod_id' target='_blank'> <em> - $sprod_name ($subQty)</em><br></a>";
				} else {
					$subprodname_str .= "<em> - $sprod_name ($subQty)</em><br>";
				}
				$subprodid_str .= "$sprod_id:$subQty,";
			}
		}
		$subprodid_str = rtrim($subprodid_str, ',');

		$product_Detail[$i]['hdnProductId'.$i] = $hdnProductId;
		$product_Detail[$i]['productName'.$i] = from_html($productname);
		/* Added to fix the issue Product Pop-up name display*/
		if($_REQUEST['action'] == 'CreateSOPDF' || $_REQUEST['action'] == 'CreatePDF' || $_REQUEST['action'] == 'SendPDFMail')
			$product_Detail[$i]['productName'.$i]= htmlspecialchars($product_Detail[$i]['productName'.$i]);
		$product_Detail[$i]['hdnProductcode'.$i] = $hdnProductcode;
		$product_Detail[$i]['productDescription'.$i]= from_html($productdescription);
		if($module == 'Vendors' || $module == 'Products' || $module == 'Services' || in_array($module, $lineItemSupportedModules)) {
			$product_Detail[$i]['comment'.$i]= $productdescription;
		}else {
            $product_Detail[$i]['comment'.$i]= $comment;
		}

		if($module != 'PurchaseOrder' && $focus->object_name != 'Order') {
			$product_Detail[$i]['qtyInStock'.$i]=decimalFormat($qtyinstock);
		}
		$listprice = number_format($listprice, $no_of_decimal_places,'.','');
		$product_Detail[$i]['qty'.$i]=decimalFormat($qty);
		$product_Detail[$i]['listPrice'.$i]=$listprice;
		$product_Detail[$i]['unitPrice'.$i]=number_format($unitprice, $no_of_decimal_places,'.','');
		$product_Detail[$i]['productTotal'.$i] = number_format($productTotal, $no_of_decimal_places, '.', '');
		$product_Detail[$i]['subproduct_ids'.$i]=$subprodid_str;
		if ($isSubProductsViewable) {
			$product_Detail[$i]['subprod_qty_list'.$i] = $subProductQtyList;
			$product_Detail[$i]['subprod_names'.$i]=$subprodname_str;
		}
		$discount_percent = decimalFormat($adb->query_result($result,$i-1,'discount_percent'));
		$discount_amount = $adb->query_result($result,$i-1,'discount_amount');
		$discount_amount = decimalFormat(number_format($discount_amount, $no_of_decimal_places,'.',''));
		$discountTotal = 0;
		//Based on the discount percent or amount we will show the discount details

		//To avoid NaN javascript error, here we assign 0 initially to' %of price' and 'Direct Price reduction'(for Each Product)
		$product_Detail[$i]['discount_percent'.$i] = 0;
		$product_Detail[$i]['discount_amount'.$i] = 0;

		if(!empty($discount_percent)) {
			$product_Detail[$i]['discount_type'.$i] = "percentage";
			$product_Detail[$i]['discount_percent'.$i] = $discount_percent;
			$product_Detail[$i]['checked_discount_percent'.$i] = ' checked';
			$product_Detail[$i]['style_discount_percent'.$i] = ' style="visibility:visible"';
			$product_Detail[$i]['style_discount_amount'.$i] = ' style="visibility:hidden"';
			$discountTotal = $productTotal*$discount_percent/100;
		} elseif(!empty($discount_amount)) {
			$product_Detail[$i]['discount_type'.$i] = "amount";
			$product_Detail[$i]['discount_amount'.$i] = $discount_amount;
			$product_Detail[$i]['checked_discount_amount'.$i] = ' checked';
			$product_Detail[$i]['style_discount_amount'.$i] = ' style="visibility:visible"';
			$product_Detail[$i]['style_discount_percent'.$i] = ' style="visibility:hidden"';
			$discountTotal = $discount_amount;
		}
		else
		{
			$product_Detail[$i]['checked_discount_zero'.$i] = ' checked';
		}
		$totalAfterDiscount = $productTotal-$discountTotal;
		$totalAfterDiscount = number_format($totalAfterDiscount, $no_of_decimal_places,'.','');
		$discountTotal = number_format($discountTotal, $no_of_decimal_places,'.','');
		$product_Detail[$i]['discountTotal'.$i] = $discountTotal;
		$product_Detail[$i]['totalAfterDiscount'.$i] = $totalAfterDiscount;

		$taxTotal = 0;
		$taxTotal = number_format($taxTotal, $no_of_decimal_places,'.','');
		$product_Detail[$i]['taxTotal'.$i] = $taxTotal;


		//Calculate netprice
		if($module == 'PurchaseOrder'){
			$netPrice = $totalAfterDiscount+$taxTotal+$tax3;
		}else{
			$netPrice = $totalAfterDiscount+$taxTotal;
		}
		// ITOMA
		// if($module == 'PurchaseOrder'){
		// 	$netPrice = 	$netPrice * $qty;
		// }
		//if condition is added to call this function when we create PO/SO/Quotes/Invoice from Product module
		if (in_array($module, $inventoryModules)) {
			if($taxtype == 'individual')
			{
				//Add the tax with product total and assign to netprice
				$netPrice = $netPrice+$taxTotal;
			}
		}

			$product_Detail[$i]['netPrice'.$i] = number_format($netPrice, getCurrencyDecimalPlaces(), '.', '');
		

		//First we will get all associated taxes as array
		$tax_details = getTaxDetailsForProduct($hdnProductId,'all');
		$regionsList = array();
		foreach ($tax_details as $taxInfo) {
			$regionsInfo = array('default' => $taxInfo['percentage']);
			foreach ($taxInfo['productregions'] as $list) {
				if (is_array($list['list'])) {
					foreach(array_fill_keys($list['list'], $list['value']) as $key => $value) {
						$regionsInfo[$key] = $value;
					}
				}
			}
			$regionsList[$taxInfo['taxid']] = $regionsInfo;
		}
		//Now retrieve the tax values from the current query with the name
		for($tax_count=0;$tax_count<count($tax_details);$tax_count++)
		{
			$tax_name = $tax_details[$tax_count]['taxname'];
			$tax_label = $tax_details[$tax_count]['taxlabel'];
			$tax_value = 0;

			$tax_value = $tax_details[$tax_count]['percentage'];
			if($focus->id != '' && $taxtype == 'individual') {
				$lineItemId = $adb->query_result($result, $i-1, 'lineitem_id');
				$tax_value = getInventoryProductTaxValue($focus->id, $hdnProductId, $tax_name, $lineItemId);
				$selectedRegionId = $focus->column_fields['region_id'];
				if ($selectedRegionId) {
					$regionsList[$tax_details[$tax_count]['taxid']][$selectedRegionId] = $tax_value;
				} else {
					$regionsList[$tax_details[$tax_count]['taxid']]['default'] = $tax_value;
				}
			}

			$product_Detail[$i]['taxes'][$tax_count]['taxname']		= $tax_name;
			$product_Detail[$i]['taxes'][$tax_count]['taxlabel']	= $tax_label;
			$product_Detail[$i]['taxes'][$tax_count]['percentage']	= $tax_value;
			$product_Detail[$i]['taxes'][$tax_count]['deleted']		= $tax_details[$tax_count]['deleted'];
			$product_Detail[$i]['taxes'][$tax_count]['taxid']		= $tax_details[$tax_count]['taxid'];
			$product_Detail[$i]['taxes'][$tax_count]['type']		= $tax_details[$tax_count]['type'];
			$product_Detail[$i]['taxes'][$tax_count]['method']		= $tax_details[$tax_count]['method'];
			$product_Detail[$i]['taxes'][$tax_count]['regions']		= $tax_details[$tax_count]['regions'];
			$product_Detail[$i]['taxes'][$tax_count]['compoundon']	= $tax_details[$tax_count]['compoundon'];
			$product_Detail[$i]['taxes'][$tax_count]['regionsList']	= $regionsList[$tax_details[$tax_count]['taxid']];
		}

	}

	//set the taxtype
	$product_Detail[1]['final_details']['taxtype'] = $taxtype;

	//Get the Final Discount, S&H charge, Tax for S&H and Adjustment values
	//To set the Final Discount details
	$finalDiscount = 0;
	$product_Detail[1]['final_details']['discount_type_final'] = 'zero';

	$subTotal = ($focus->column_fields['hdnSubTotal'] != '')?$focus->column_fields['hdnSubTotal']:0;
	$subTotal = number_format($subTotal, $no_of_decimal_places,'.','');

	$product_Detail[1]['final_details']['hdnSubTotal'] = $subTotal;
	$discountPercent = ($focus->column_fields['hdnDiscountPercent'] != '')?$focus->column_fields['hdnDiscountPercent']:0;
	$discountAmount = ($focus->column_fields['hdnDiscountAmount'] != '')?$focus->column_fields['hdnDiscountAmount']:0;
    if($discountPercent != '0'){
        $discountAmount = ($product_Detail[1]['final_details']['hdnSubTotal'] * $discountPercent / 100);
    }

	//To avoid NaN javascript error, here we assign 0 initially to' %of price' and 'Direct Price reduction'(For Final Discount)
	$discount_amount_final = 0;
	$discount_amount_final = number_format($discount_amount_final, $no_of_decimal_places,'.','');
    $product_Detail[1]['final_details']['discount_percentage_final'] = 0;
	$product_Detail[1]['final_details']['discount_amount_final'] = $discount_amount_final;

	$hdnDiscountPercent = (float) $focus->column_fields['hdnDiscountPercent'];
	$hdnDiscountAmount	= (float) $focus->column_fields['hdnDiscountAmount'];

	if(!empty($hdnDiscountPercent)) {
		$finalDiscount = ($subTotal*$discountPercent/100);
		$product_Detail[1]['final_details']['discount_type_final'] = 'percentage';
		$product_Detail[1]['final_details']['discount_percentage_final'] = $discountPercent;
		$product_Detail[1]['final_details']['checked_discount_percentage_final'] = ' checked';
		$product_Detail[1]['final_details']['style_discount_percentage_final'] = ' style="visibility:visible"';
		$product_Detail[1]['final_details']['style_discount_amount_final'] = ' style="visibility:hidden"';
	} elseif(!empty($hdnDiscountAmount)) {
		$finalDiscount = $focus->column_fields['hdnDiscountAmount'];
		$product_Detail[1]['final_details']['discount_type_final'] = 'amount';
		$product_Detail[1]['final_details']['discount_amount_final'] = $discountAmount;
		$product_Detail[1]['final_details']['checked_discount_amount_final'] = ' checked';
		$product_Detail[1]['final_details']['style_discount_amount_final'] = ' style="visibility:visible"';
		$product_Detail[1]['final_details']['style_discount_percentage_final'] = ' style="visibility:hidden"';
	}
	$finalDiscount = number_format($finalDiscount, $no_of_decimal_places,'.','');
	$product_Detail[1]['final_details']['discountTotal_final'] = $finalDiscount;

	//To set the Final Tax values
	//we will get all taxes. if individual then show the product related taxes only else show all taxes
	//suppose user want to change individual to group or vice versa in edit time the we have to show all taxes. so that here we will store all the taxes and based on need we will show the corresponding taxes

	//First we should get all available taxes and then retrieve the corresponding tax values
	$tax_details = getAllTaxes('available','','edit',$focus->id);
	$taxDetails = array();

	for($tax_count=0;$tax_count<count($tax_details);$tax_count++)
	{
		if ($tax_details[$tax_count]['method'] === 'Deducted') {
			continue;
		}

		$tax_name = $tax_details[$tax_count]['taxname'];
		$tax_label = $tax_details[$tax_count]['taxlabel'];

		//if taxtype is individual and want to change to group during edit time then we have to show the all available taxes and their default values
		//Also taxtype is group and want to change to individual during edit time then we have to provide the asspciated taxes and their default tax values for individual products
		if($taxtype == 'group')
			$tax_percent = $adb->query_result($result,0,$tax_name);
		else
			$tax_percent = $tax_details[$tax_count]['percentage'];//$adb->query_result($result,0,$tax_name);

		if($tax_percent == '' || $tax_percent == 'NULL')
			$tax_percent = 0;
		$taxamount = ($subTotal-$finalDiscount)*$tax_percent/100;
        list($before_dot, $after_dot) = explode('.', $taxamount);
        if($after_dot[$no_of_decimal_places] == 5) {
            $taxamount = round($taxamount, $no_of_decimal_places, PHP_ROUND_HALF_DOWN); 
        } else {
            $taxamount = number_format($taxamount, $no_of_decimal_places,'.','');
        }

		$taxId = $tax_details[$tax_count]['taxid'];
		$taxDetails[$taxId]['taxname']		= $tax_name;
		$taxDetails[$taxId]['taxlabel']		= $tax_label;
		$taxDetails[$taxId]['percentage']	= $tax_percent;
		$taxDetails[$taxId]['amount']		= $taxamount;
		$taxDetails[$taxId]['taxid']		= $taxId;
		$taxDetails[$taxId]['type']			= $tax_details[$tax_count]['type'];
		$taxDetails[$taxId]['method']		= $tax_details[$tax_count]['method'];
		$taxDetails[$taxId]['regions']		= Zend_Json::decode(html_entity_decode($tax_details[$tax_count]['regions']));
		$taxDetails[$taxId]['compoundon']	= Zend_Json::decode(html_entity_decode($tax_details[$tax_count]['compoundon']));
	}

	$compoundTaxesInfo = getCompoundTaxesInfoForInventoryRecord($focus->id, $module);
	//Calculating compound info
	$taxTotal = 0;
	foreach ($taxDetails as $taxId => $taxInfo) {
		$compoundOn = $taxInfo['compoundon'];
		if ($compoundOn) {
			$existingCompounds = $compoundTaxesInfo[$taxId];
			if (!is_array($existingCompounds)) {
				$existingCompounds = array();
			}
			$compoundOn = array_unique(array_merge($existingCompounds, $compoundOn));
			$taxDetails[$taxId]['compoundon'] = $compoundOn;

			$amount = $subTotal-$finalDiscount;
			foreach ($compoundOn as $id) {
				$amount = (float)$amount + (float)$taxDetails[$id]['amount'];
			}
			$taxAmount = ((float)$amount * (float)$taxInfo['percentage']) / 100;
			list($beforeDot, $afterDot) = explode('.', $taxAmount);

			if ($afterDot[$no_of_decimal_places] == 5) {
				$taxAmount = round($taxAmount, $no_of_decimal_places, PHP_ROUND_HALF_DOWN);
			} else {
				$taxAmount = number_format($taxAmount, $no_of_decimal_places, '.', '');
			}

			$taxDetails[$taxId]['amount'] = $taxAmount;
		}
		$taxTotal = $taxTotal + $taxDetails[$taxId]['amount'];
	}
	$product_Detail[1]['final_details']['taxes'] = $taxDetails;
	$product_Detail[1]['final_details']['tax_totalamount'] = number_format($taxTotal, $no_of_decimal_places, '.', '');

	//To set the Shipping & Handling charge
	$shCharge = ($focus->column_fields['hdnS_H_Amount'] != '')?$focus->column_fields['hdnS_H_Amount']:0;
	$shCharge = number_format($shCharge, $no_of_decimal_places,'.','');
	$product_Detail[1]['final_details']['shipping_handling_charge'] = $shCharge;

	//To set the Shipping & Handling tax values
	//calculate S&H tax
	$shtaxtotal = 0;
	//First we should get all available taxes and then retrieve the corresponding tax values
	$shtax_details = getAllTaxes('available','sh','edit',$focus->id);

	//if taxtype is group then the tax should be same for all products in vtiger_inventoryproductrel table
	for($shtax_count=0;$shtax_count<count($shtax_details);$shtax_count++)
	{
		$shtax_name = $shtax_details[$shtax_count]['taxname'];
		$shtax_label = $shtax_details[$shtax_count]['taxlabel'];
		$shtax_percent = 0;
		//if condition is added to call this function when we create PO/SO/Quotes/Invoice from Product module
		if (in_array($module, $inventoryModules)) {
			$shtax_percent = getInventorySHTaxPercent($focus->id,$shtax_name);
		}
		$shtaxamount = $shCharge*$shtax_percent/100;
		$shtaxtotal = $shtaxtotal + $shtaxamount;
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['taxname']	= $shtax_name;
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['taxlabel']	= $shtax_label;
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['percentage']	= $shtax_percent;
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['amount']		= $shtaxamount;
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['taxid']		= $shtax_details[$shtax_count]['taxid'];
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['type']		= $shtax_details[$shtax_count]['type'];
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['method']		= $shtax_details[$shtax_count]['method'];
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['regions']	= Zend_Json::decode(html_entity_decode($shtax_details[$shtax_count]['regions']));
		$product_Detail[1]['final_details']['sh_taxes'][$shtax_count]['compoundon']	= Zend_Json::decode(html_entity_decode($shtax_details[$shtax_count]['compoundon']));
	}
	$shtaxtotal = number_format($shtaxtotal, $no_of_decimal_places,'.','');
	$product_Detail[1]['final_details']['shtax_totalamount'] = $shtaxtotal;

	//To set the Adjustment value
	$adjustment = ($focus->column_fields['txtAdjustment'] != '')?$focus->column_fields['txtAdjustment']:0;
	$adjustment = number_format($adjustment, $no_of_decimal_places,'.','');
	$product_Detail[1]['final_details']['adjustment'] = $adjustment;

	// ITOMA
	if($module == 'PurchaseOrder'){
		$adjustment_vat_result = $adb->pquery("SELECT adjustment_pvm FROM vtiger_purchaseorder WHERE purchaseorderid = ?", array($focus->id));
		$adjustment_vat = $adb->query_result($adjustment_vat_result,0,'adjustment_pvm');
		$adjustment_PVM = (	$adjustment_vat != '')?	$adjustment_vat:0;
		$adjustment_PVM = number_format($adjustment_PVM, $no_of_decimal_places,'.','');
		$product_Detail[1]['final_details']['adjustment_pvm'] = $adjustment_PVM;
	}

	//To set the grand total
	$grandTotal = ($focus->column_fields['hdnGrandTotal'] != '')?$focus->column_fields['hdnGrandTotal']:0;
	$grandTotal = number_format($grandTotal, $no_of_decimal_places,'.','');
		
	$product_Detail[1]['final_details']['grandTotal'] = $grandTotal;

	$log->debug("Exiting getAssociatedProducts method ...");
	// echo " <pre>"; print_R($product_Detail);
	return $product_Detail;

}

/** This function returns the data type of the vtiger_fields, with vtiger_field label, which is used for javascript validation.
* Param $validationData - array of vtiger_fieldnames with datatype
* Return type array
*/

function split_validationdataArray($validationData)
{
	global $log;
	$log->debug("Entering split_validationdataArray(".$validationData.") method ...");
	$fieldName = '';
	$fieldLabel = '';
	$fldDataType = '';
	$rows = count($validationData);
	foreach($validationData as $fldName => $fldLabel_array)
	{
		if($fieldName == '')
		{
			$fieldName="'".$fldName."'";
		}
		else
		{
			$fieldName .= ",'".$fldName ."'";
		}
		foreach($fldLabel_array as $fldLabel => $datatype)
		{
			if($fieldLabel == '')
			{
				$fieldLabel = "'".addslashes($fldLabel)."'";
			}
			else
			{
				$fieldLabel .= ",'".addslashes($fldLabel)."'";
			}
			if($fldDataType == '')
			{
				$fldDataType = "'".$datatype ."'";
			}
			else
			{
				$fldDataType .= ",'".$datatype ."'";
			}
		}
	}
	$data['fieldname'] = $fieldName;
	$data['fieldlabel'] = $fieldLabel;
	$data['datatype'] = $fldDataType;
	$log->debug("Exiting split_validationdataArray method ...");
	return $data;
}


?>
