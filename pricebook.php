<?php

// require_once('include/database/PearDatabase.php');
// require_once('include/utils/utils.php');

// global $adb, $log;

// $pricebookid = '5351';
// $pricebook = 'Test';
// $source = 'Generated';

// $log->debug("Entering into function CreatePricebook.");

// $adb->startTransaction();
    
//     $sql = "SELECT * FROM vtiger_pricebookproductrel WHERE pricebookid = ? LIMIT 2";

//     $result = $adb->pquery($sql, array($pricebookid));

//     while($res = $adb->fetch_array($result))
//     {

//         $productid = $res["productid"];
//         $listprice = $res["listprice"];

//         $sql_new = "SELECT p.*, e.* FROM vtiger_products p
//                           JOIN vtiger_crmentity e ON e.crmid = p.productid
//                           WHERE e.deleted = 0 AND p.productid = ?";

//         $result_new = $adb->pquery($sql_new, array($productid));

//         while($res_new = $adb->fetch_array($result_new))
//         {

//             $productname = html_entity_decode($pricebook . '_' . $res_new["productname"], ENT_NOQUOTES, 'UTF-8');   
//             $productcategory = html_entity_decode($res_new["productcategory"], ENT_NOQUOTES, 'UTF-8');
//             $discontinued = $res_new["discontinued"];
//             $qtyinstock = $res_new["qtyinstock"];
//             $min_weight_kg = $res_new["min_weight_kg"];    
//             $max_weight_kg = $res_new["max_weight_kg"];
//             $min_volume_m3 = $res_new["min_volume_m3"];
//             $max_volume_m3 = $res_new["max_volume_m3"];    
//             $consignee = html_entity_decode($res_new["consignee"], ENT_NOQUOTES, 'UTF-8');  
//             $min_square_m2 = $res_new["min_square_m2"];  
//             $max_square_m2 = $res_new["max_square_m2"];
//             $pll_pcs = $res_new["pll_pcs"];    
//             $description = html_entity_decode($res_new["description"], ENT_NOQUOTES, 'UTF-8');
//             $smcreatorid = $res_new["smcreatorid"];
//             $smownerid = $res_new["smownerid"];        

//             $created_modified_date = date("Y-m-d H:i:s");

//             $sql2 = $adb->pquery("SELECT * FROM vtiger_crmentity_seq", array());
//             $id = $adb->query_result($sql2, 0, 'id');
//             $new_product_id = $id + 1;

//             $sql3 = "INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?)";
//             $result3 = $adb->pquery($sql3, array($new_product_id, $smcreatorid, $smownerid, 'Products', $description, $created_modified_date, $created_modified_date)); 

//             $sql4 = "UPDATE vtiger_crmentity_seq SET id = ?";
//             $result4 = $adb->pquery($sql4, array($new_product_id));

//             $sql6 = $adb->pquery("SELECT cur_id FROM vtiger_modentity_num WHERE semodule = 'Products' AND prefix = 'PRO'", array());
//             $cur_id = $adb->query_result($sql6, 0, 'cur_id');        

//             $product_no = 'PRO' . $cur_id;

//             //print_r($new_product_id);
//             //die();
            
//             $sql5 = "INSERT INTO vtiger_products(productid, product_no, productname, productcategory, discontinued, qtyinstock, min_weight_kg, max_weight_kg, min_volume_m3, max_volume_m3, consignee, min_square_m2, max_square_m2, pll_pcs, productsheet, source) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//             $result5 = $adb->pquery($sql5, array($new_product_id, $product_no, $productname, $productcategory, $discontinued, $qtyinstock, $min_weight_kg, $max_weight_kg, $min_volume_m3, $max_volume_m3, $consignee, $min_square_m2, $max_square_m2, $pll_pcs, $pricebook, $source));

//             $sql7 = $adb->pquery("UPDATE vtiger_modentity_num SET cur_id = ? WHERE semodule = 'Products' AND prefix = 'PRO'", array($cur_id + 1)); 
            
//             //$sql8 = "INSERT INTO vtiger_pricebookproductrel(pricebookdid, productid, listprice, old_listprice, import_date) VALUES (?, ?, ?, ?, ?)";
//             //$result6 = $adb->pquery($sql8, array($new_pricebook_id, $new_product_id, $listprice, $listprice, $created_modified_date));            
            
//         }

//     }
    
// $adb->completeTransaction();

//  $log->debug("Exiting from function CreatePricebook");