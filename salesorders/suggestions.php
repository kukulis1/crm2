<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");



$account_id = $_POST['account_id'];

$history = $conn->query("SELECT DISTINCT load_company, bill_code,bill_city,bill_street, load_contact AS load_contact,bill_country
                          FROM vtiger_salesorder
                          LEFT JOIN vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid                     
                          WHERE vtiger_salesorder.accountid = $account_id
                          GROUP BY load_company
                          ORDER BY vtiger_salesorder.salesorderid DESC");

$history2 = $conn->query("SELECT DISTINCT unload_company, ship_code, ship_city, ship_street, unload_contact AS unload_contact,ship_country
                            FROM vtiger_salesorder
                            LEFT JOIN vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid                     
                            WHERE vtiger_salesorder.accountid = $account_id
                            GROUP BY unload_company
                            ORDER BY vtiger_salesorder.salesorderid DESC");                          


foreach($history AS $all_records){
  $load_company[] =  $all_records['load_company'];
  $load_code[] =  $all_records['bill_code'];
  $bill_city[] =  $all_records['bill_city'];
  $bill_street[] =  $all_records['bill_street'];
  $load_contact[] =  $all_records['load_contact'];
  $bill_country[] =  $all_records['bill_country'];
  $bill_legal_entity_code[] =  $all_records['legal_entity_code'];
}

foreach($history2 AS $all_records2){
  $unload_company[] = $all_records2['unload_company'];
  $unload_code[] = $all_records2['ship_code'];
  $ship_city[] = $all_records2['ship_city'];
  $ship_street[] = $all_records2['ship_street'];
  $unload_contact[] = $all_records2['unload_contact'];
  $ship_country[] = $all_records2['ship_country'];
  $ship_legal_entity_code[] = $all_records2['legal_entity_code'];
}

$all_history = array(
  'load_company' => array(
    'load_company' => $load_company, 
    'bill_code' => $load_code, 
    'bill_city' => $bill_city, 
    'bill_street' => $bill_street, 
    'load_contact' => $load_contact, 
    'bill_country' => $bill_country, 
    'bill_legal_entity_code' => $bill_legal_entity_code), 

  'unload_company' => array(
    'unload_company' => $unload_company, 
    'ship_code' => $unload_code, 
    'ship_city' => $ship_city, 
    'ship_street' => $ship_street, 
    'unload_contact' => $unload_contact,
    'ship_country' => $ship_country,
    'ship_legal_entity_code' => $ship_legal_entity_code)
);



// echo "<pre>";
// print_R($all_history);
// echo "</pre>";


echo  json_encode($all_history);

}else{
  http_response_code(404);
}
