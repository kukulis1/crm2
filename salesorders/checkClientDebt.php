<?php
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $accountid = $_POST['accountid'];

  $results = $conn->query("SELECT ROUND(total,2) AS total, CASE 
                                                                        WHEN debts_tks_suma IS NULL
                                                                        THEN ROUND(total,2)
                                                                        ELSE ROUND((total - SUM(vtiger_debts.debts_tks_suma)),2)
                                                                      END AS debt,  
                                                CASE WHEN debts_tks_data  THEN
                                                CASE 
                                                  WHEN  vtiger_invoice.duedate < max(debts_tks_data) THEN CONCAT('+',DATEDIFF( max(debts_tks_data), vtiger_invoice.duedate))
                                                  WHEN  vtiger_invoice.duedate > max(debts_tks_data) THEN CONCAT('-',DATEDIFF(vtiger_invoice.duedate,  max(debts_tks_data)))
                                                  ELSE 0
                                                END  
                                              ELSE '---'	
                                              END AS late_payment,debts_tks_suma          
                                                  FROM vtiger_invoice 
                                                  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
                                                  LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 											 
                                                  JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid                                  
                                                  WHERE vtiger_crmentity.deleted = 0 AND  vtiger_invoice.accountid = $accountid
                                                  GROUP BY vtiger_invoice.invoiceid");

  $checkBlackList = mysqli_fetch_assoc($conn->query("SELECT cf_1783 FROM vtiger_accountscf WHERE accountid = $accountid"));
  $blackList = $checkBlackList['cf_1783'];

  $debtArr = array();
  $days_arr = array();	

  foreach($results AS $row){
    $debtArr[] = $row['debt'];
    if($row['late_payment'] != '---'){
     $days_arr[] = $row['late_payment'];
    }
  }

  $debt = array_sum($debtArr);

  
  sort($days_arr);
  $count = sizeof($days_arr);   
  $index = floor($count/2);  
  if (!$count) {
    $median = "---";
  } elseif ($count & 1) {    
    $median = $days_arr[$index];
  } else {                  
    $median = ($days_arr[$index-1] + $days_arr[$index]) / 2;
  }

  $responce = array('debt' => ROUND($debt,2), 'median' => $median, 'black_list' => $blackList);

  echo json_encode($responce);

}else{
  http_response_code(404);
}