<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $accountid = $_POST['accountid'];

  $query = mysqli_fetch_assoc($conn->query("SELECT shipment_code FROM crm.vtiger_salesorder WHERE accountid = $accountid AND shipment_code != '' ORDER BY salesorderid DESC LIMIT 1"));
  $shipment_code = $query['shipment_code'];

  if($shipment_code){
    echo json_encode(array('success' => true, 'shipment_code' => $shipment_code));     
  }else{
    echo json_encode(array('success' => false));
  }
  
}else{
  http_response_code(404);
}