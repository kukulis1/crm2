<?php
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $results = mysqli_fetch_assoc($conn->query("SELECT * FROM app_other_settings WHERE title = 'LBL_CARGO_NOT_STANDART'"));
  $tax_value = $results['value'];
  echo $tax_value;
}else{
  http_response_code(404);
}