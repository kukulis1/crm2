<?php
error_reporting(0);
require_once "../config.inc.php";


if($_POST){

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $invoiceid = $_POST['invoiceid'];
  $date = $_POST['date'];
  $method = $_POST['payMethod'];
  $user_id = $_POST['user_id'];
  $date_now = date("Y-m-d H:i:s");


  $info = $conn->query("SELECT vtiger_invoice.invoiceid,
                                    CASE 
                                      WHEN vtiger_debts.debts_tks_suma THEN ROUND((total - SUM(vtiger_debts.debts_tks_suma)),2)
                                      ELSE ROUND(total,2)
                                    END AS total
                                  FROM vtiger_invoice 
                                  LEFT JOIN vtiger_account on vtiger_invoice.accountid=vtiger_account.accountid 
                                  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
                                  LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
                                  LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_debts.debtsid
                                  WHERE vtiger_invoice.invoiceid IN ($invoiceid)
                                  GROUP BY vtiger_invoice.invoiceid	 
                                  ORDER BY invoiceid DESC");
    
  
  while($row = $info->fetch_assoc()){ 
    $entity_id = last_entity_record($conn);
    insert_entity($conn,'Debts', $entity_id, $user_id, NULL, '', 'CRM', $date_now);
    insert_entityrel($conn, $row['invoiceid'], 'Invoice',  $entity_id, 'Debts');
    insertDebt($conn,$entity_id,$date,$row['total'],$method);
    $conn->query("INSERT INTO vtiger_debtscf (debtsid) VALUES ('$entity_id')");
  }


  if($entity_id){
    echo 'true';
  }else{
    echo 'false';
  }

}else{
  http_response_code(404);
}
  

function insert_entity($conn,$setype, $entity_id, $user_id, $description, $label, $source, $date)
{

  $sth = $conn->query("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) 
                       VALUES ('$entity_id', '$user_id', '$user_id', '$setype', '$description', '$source', '$label', '$date', '$date')");

}

function insert_entityrel($conn, $entity_id, $module, $relcrmid, $relmodule)
{

  $sth = $conn->query("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) 
                       VALUES ('$entity_id', '$module', '$relcrmid', '$relmodule')");
}

function insertDebt($conn, $entity_id,$date,$total,$method)
{
  $sth = $conn->query("INSERT INTO vtiger_debts (debtsid, debts_tks_data, debts_tks_suma, debts_tks_tipas) 
                           VALUES ('$entity_id', '$date', '$total', '$method')");
}

function last_entity_record($conn)
{
  $sql = "SELECT id FROM `vtiger_crmentity_seq`";  
  $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
  $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
  $lock3 = 'UNLOCK TABLES';

  $conn->query($lock); 
    $get_id = mysqli_fetch_assoc($conn->query($sql)); 
    $seq = $get_id['id'];    
  $conn->query($lock3);    

  $new_seq = $seq + 1;   

  $conn->query($lock2);    
    $conn->query("UPDATE `vtiger_crmentity_seq` SET id = $new_seq");  
  $conn->query($lock3); 

  return $new_seq;
}