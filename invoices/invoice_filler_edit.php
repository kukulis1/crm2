<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

if(is_numeric($_POST['orderId'])){
  $orderId = $_POST['orderId'];
}

    $query = "SELECT vtiger_accountscf.cf_1279 as payment_deferral
              FROM vtiger_invoice                
              LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_invoice.accountid
              WHERE vtiger_invoice.invoiceid = '".$orderId."'             
              ORDER BY vtiger_invoice.invoiceid DESC";
    $order_record = mysqli_fetch_assoc($conn->query($query));  
    
      if(!empty($order_record['payment_deferral'])){
        echo json_encode($order_record['payment_deferral']);
      }else{
        echo json_encode('empty');
      }
