<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

$data = (array)$_POST['form_array'];
$data = call_user_func_array('array_merge', $data);

$keys = array();
$values = array();

foreach($data as $key => $row){
  if(!is_numeric($key))  $keys[] = $key;
  if($row != '') $values[] = $row;
}


$query_string = "SELECT cf_1659,commentcontent, create_date AS remind_time, r.count as remind_count,
                  CASE 
                    WHEN debts_tks_suma IS NULL
                    THEN FORMAT(IF(vtiger_invoice.region_id = 5,subtotal,total),2)
                    ELSE FORMAT((IF(vtiger_invoice.region_id = 5,subtotal,total) - SUM(vtiger_debts.debts_tks_suma)),2)
                  END AS debt, 
                  debts_tks_suma,accountname AS account_id,
                  CASE 
                    WHEN debts_tks_suma IS NULL
                    THEN 0
                    ELSE FORMAT(SUM(vtiger_debts.debts_tks_suma),2)
                  END AS payed, 
                  CASE 
                    WHEN  vtiger_invoice.duedate < CURDATE()
                    THEN CONCAT('+',DATEDIFF(CURDATE(), vtiger_invoice.duedate))
                    ELSE CONCAT('-',DATEDIFF(vtiger_invoice.duedate, CURDATE()))
                  END AS late_payment,
                  FORMAT(IF(vtiger_invoice.region_id = 5,subtotal,total), 2) AS total_with_vat, vtiger_invoice.invoice_no, vtiger_invoice.invoicestatus, FORMAT(vtiger_invoice.total,2) as hdnGrandTotal, vtiger_invoice.accountid, vtiger_invoice.duedate, FORMAT(vtiger_invoice.subtotal,2) as hdnSubTotal, FORMAT(vtiger_invoice.balance,2) as balance, vtiger_crmentity.smownerid, vtiger_invoice.invoiceid, vtiger_crmentity_user_field.starred,vtiger_invoice.invoicedate, CONCAT(acc_owner_name.first_name,' ',acc_owner_name.last_name) AS owner_name, 
                  CONCAT(manager_name.first_name,' ',manager_name.last_name) AS managers_name,  
                  CONCAT(debt_manager_name.first_name,' ',debt_manager_name.last_name) AS debt_managers_name,
                  CONCAT(client_manager_name.first_name,' ',client_manager_name.last_name) AS client_manager_name
                  FROM vtiger_invoice 
                  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
                  LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
                  LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid
                  LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_account.accountid
                  INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid         
                  LEFT JOIN vtiger_users as acc_owner_name ON acc_owner_name.id = vtiger_crmentity.smownerid 
                  LEFT JOIN vtiger_users as manager_name ON manager_name.id = vtiger_accountscf.cf_1659 
                  LEFT JOIN vtiger_users as debt_manager_name ON debt_manager_name.id = vtiger_accountscf.cf_1657
                  LEFT JOIN vtiger_users as client_manager_name ON client_manager_name.id = vtiger_accountscf.cf_1655
                  LEFT JOIN vtiger_groups ON vtiger_crmentity.smownerid = vtiger_groups.groupid 
                  LEFT JOIN vtiger_crmentity_user_field ON vtiger_invoice.invoiceid = vtiger_crmentity_user_field.recordid AND vtiger_crmentity_user_field.userid=1
                  LEFT JOIN vtiger_modcomments ON vtiger_modcomments.related_to=vtiger_invoice.invoiceid 
                  LEFT JOIN vtiger_debts_reminder r ON r.invoiceid=vtiger_invoice.invoiceid
                  WHERE vtiger_crmentity.deleted=0 AND vtiger_invoice.invoiceid > 0          
                  GROUP BY vtiger_invoice.invoiceid	
                  HAVING (debt > 0 OR debts_tks_suma IS NULL) AND ";



for($i=0; $i<COUNT($keys); $i++){
  if($keys[$i] == 'duedate' || $keys[$i] == 'invoicedate'){
    $period = explode(",",$values[$i]);
    $query_string .=  "$keys[$i] BETWEEN '$period[0]' AND '$period[1]' ".($i+1 < COUNT($keys) ? 'AND ' :'');
  }else{
    $query_string .=  "$keys[$i] LIKE '$values[$i]%' ".($i+1 < COUNT($keys) ? 'AND ' :'');
  }
}

$query_string .= " ORDER BY vtiger_crmentity.createdtime DESC";


$query = $conn->query($query_string);

$result = array();
while($row = $query->fetch_assoc()) {
  $result[] = $row;      
}  

echo json_encode($result);


}else{
  http_response_code(404);
}