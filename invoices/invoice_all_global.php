<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");  

    $conn->query("SET SESSION group_concat_max_len = 1000000");
    $accountid = $_GET['accountid'];
    $orders = $_GET['orders'];

    $check_rule = $conn->query("SELECT autoinvoicingrules_tks_groupby	AS rule						
                                  FROM vtiger_autoinvoicingrules r
                                  JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
                                  JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
                                  JOIN vtiger_account a ON a.accountid=rel.crmid
                                  WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND accountid = $accountid AND autoinvoicingrules_tks_autoinv = 1
                                  GROUP BY relcrmid");

    $result = mysqli_fetch_assoc($check_rule);
    $table = $result['rule'];

    $sql = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(\"'\",address,\"'\") AS addreses FROM app_rules_$table WHERE accountid =  $accountid"));
    $address = $sql['addreses'];
    $groupBy = ($table == 'load_address' ? 'bill_street' : 'ship_street'); 

    $query = "SELECT SUM(vtiger_salesorder.total) AS total, vtiger_salesorder.accountid,CURDATE() + INTERVAL cf_1279 DAY as duedate,GROUP_CONCAT(vtiger_salesorder.salesorderid) AS id
                              FROM `vtiger_salesorder` 
                              JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
                              JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                              JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid                                   
                              WHERE salesorderid IN ($orders)
                              GROUP BY IF($groupBy IN ($address) , $groupBy,'')                       
                              ORDER BY salesorderid ASC";

  $execute = $conn->query($query); 
 
  $e = 1;
  foreach($execute as $single_order){
    $entityid = last_entity_record_id($conn);
    $prev_inv_no = setModuleSeqNumber($conn);    
    $s_h_amount = ($single_order['total'] /100) * 21;
    $total_with_vat = $single_order['total'] + $s_h_amount;
    $date = date('Y-m-d');    
    $order_ids = $single_order['id'];
    $dateTime = date('Y-m-d H:i:').($e < 10 ? "0".$e : $e);

    $conn->query("INSERT INTO `vtiger_invoice` (`invoiceid`,`subject`,`salesorderid`,`invoicedate`,`duedate`,`subtotal`,`total`,`s_h_amount`,`accountid`,`invoicestatus`,`invoice_no`) VALUES ('$entityid','$prev_inv_no','$order_ids','$date','".$single_order['duedate']."','".$single_order['total']."',$total_with_vat,$s_h_amount,'".$single_order['accountid']."','Created','$prev_inv_no') ");
    $conn->query("INSERT INTO `vtiger_invoicecf` (`invoiceid`,`cf_1277`,`cf_1486`) VALUES ('$entityid','Debetinė','Transportas')");
    $conn->query("INSERT INTO `vtiger_crmentity` (`crmid`,`smcreatorid`,`smownerid`,`modifiedby`,`setype`,`createdtime`,`modifiedtime`,`source`,`label`) VALUES ('$entityid','1','1','1','Invoice', '$dateTime','$dateTime','CRM','".$prev_inv_no."') ");  
    $conn->query("INSERT INTO vtiger_invoice_auto_invoicing (invoiceid) VALUES ('$entityid')"); 
    
    $execute2 = $conn->query("SELECT vtiger_salesorder.salesorder_no, vtiger_salesorder.salesorderid ,vtiger_salesorder.total, vtiger_salesorder.shipment_code,  vtiger_inventoryproductrel.productid,vtiger_salesorder.accountid,
                  CONCAT(vtiger_salesorder.load_date_from,', ', vtiger_sobillads.load_company,', ', vtiger_sobillads.bill_street,', ', vtiger_sobillads.bill_city,', ', vtiger_sobillads.bill_code) AS bill_ads,
                  CONCAT(vtiger_salesorder.unload_date_to,', ', vtiger_soshipads.unload_company,', ', vtiger_soshipads.ship_street,', ', vtiger_soshipads.ship_city,', ', vtiger_soshipads.ship_code) AS ship_ads,
                  CASE WHEN  vtiger_accountscf.cf_1570
                  THEN vtiger_inventoryproductrel.note
                  ELSE '' 
                  END AS note,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN 'Products' ELSE 'Services' END as entitytype,
                  CASE WHEN vtiger_inventoryproductrel.productid = 36641 THEN vtiger_inventoryproductrel.service ELSE 0 END as service_id,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 
                  THEN 
                  IF(SUM(vtiger_inventoryproductrel.margin) = vtiger_salesorder.subtotal, IF(vtiger_inventoryproductrel.source = 'Metrika', FORMAT(SUM(vtiger_inventoryproductrel.margin)/count( vtiger_inventoryproductrel.id),2), 
                  SUM(vtiger_inventoryproductrel.margin)), vtiger_salesorder.subtotal)  
                        WHEN vtiger_inventoryproductrel.productid = 36641 AND inventory_type != 'FuelSurcharge' THEN vtiger_inventoryproductrel.margin 
                        WHEN vtiger_inventoryproductrel.productid = 36641 AND inventory_type = 'FuelSurcharge' THEN vtiger_inventoryproductrel.margin 
                END as margin,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN SUM(vtiger_inventoryproductrel.cargo_wgt) ELSE vtiger_inventoryproductrel.cargo_wgt END as cargo_wgt,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN GROUP_CONCAT(DISTINCT FORMAT(vtiger_inventoryproductrel.quantity,0),' ', app_measures.code)  ELSE vtiger_inventoryproductrel.cargo_wgt END as cargo,	
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_length) ELSE vtiger_inventoryproductrel.cargo_length END as cargo_length,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_width)  ELSE vtiger_inventoryproductrel.cargo_width END AS cargo_width,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_height) ELSE vtiger_inventoryproductrel.cargo_height END as cargo_height,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  FORMAT(SUM(vtiger_inventoryproductrel.quantity),0)  ELSE  FORMAT(vtiger_inventoryproductrel.quantity,0) END AS quantity,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN CASE WHEN SUM(m3) > SUM(vtiger_inventoryproductrel.cargo_length*vtiger_inventoryproductrel.cargo_width*vtiger_inventoryproductrel.cargo_height) THEN ROUND(SUM(m3),3) ELSE ROUND(SUM(vtiger_inventoryproductrel.cargo_length*vtiger_inventoryproductrel.cargo_width*vtiger_inventoryproductrel.cargo_height),3) END  ELSE 0 END AS volume,vtiger_inventoryproductrel.service
                                FROM vtiger_salesorder  
                                LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid              
                                LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
                                LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid
                                LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
                                LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid
                                LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid                    
                                LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                                LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid 
                                LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid 
                                LEFT JOIN `app_measures` ON CASE WHEN  vtiger_inventoryproductrel.cargo_measure REGEXP '^[0-9]+$' THEN app_measures.id=vtiger_inventoryproductrel.cargo_measure ELSE app_measures.code =vtiger_inventoryproductrel.cargo_measure END
                                WHERE vtiger_crmentity.deleted = 0 AND vtiger_salesorder.salesorderid IN ($order_ids)      
                                GROUP BY vtiger_salesorder.salesorderid ,vtiger_inventoryproductrel.productid
                                ORDER BY vtiger_salesorder.shipment_code ASC,sequence_no");



      $i = 1;
      foreach($execute2 as $inv){ 
        $ship_ads = ($inv['productid'] == 14244 ? clean($inv['ship_ads']) : '');
        $bill_ads = clean($inv['bill_ads']);    
        $conn->query("INSERT INTO vtiger_inventoryproductrel(id,productid,sequence_no,quantity,listprice,comment,cargo_measure,cargo_wgt,order_id,note,bill_ads,ship_ads,service,m3,costcenter,employee,object)	
        VALUES('$entityid','".$inv['productid']."','$i','".$inv['quantity']."','".$inv['margin']."','".$inv['note']."','".$inv['cargo']."','".$inv['cargo_wgt']."','".$inv['salesorderid']."','".$inv['note']."','$bill_ads','$ship_ads','".$inv['service']."','".$inv['volume']."','97380','','')");
        $conn->query("INSERT INTO vtiger_invoice_salesorders_list (invoiceid, salesorderid) VALUES ('$entityid','".$inv['salesorderid']."')"); 
        $i++;
      }

      $e++;
  }

header("Location:/index.php?module=Automaticinvoicing&view=List");



function last_entity_record_id($conn)
{
  $result = $conn->query("SELECT id FROM vtiger_crmentity_seq");
  $id = $result->fetch_assoc();
  $crmid = $id['id'] + 1; 
  echo $crmid."<br>";   
  $conn->query("UPDATE vtiger_crmentity_seq SET id = $crmid");  
  return $crmid;
}


function setModuleSeqNumber($conn){

  $today = date('Y-m-d');

  $check_cur = mysqli_fetch_assoc($conn->query("SELECT invoice_no FROM vtiger_invoice 
                                                LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid 
                                                LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                                WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$today' AND vtiger_invoicecf.cf_1277 LIKE 'Debetinė' AND vtiger_crmentity.deleted = 0 
                                                ORDER BY vtiger_invoice.invoiceid  
                                                DESC LIMIT 1"));

  $curid = $check_cur['invoice_no'];
  if(!empty($curid)){
    $curid = substr($curid, -3);
  }else{
    $req_no = '001';
  }

  $prefix = 'PT';
  $todayDate = date('y/m/d');
  if(!empty($curid)){
    $strip = strlen($curid) - strlen($curid + 1);
    if ($strip < 0) $strip = 0;
    $temp = str_repeat("0", $strip);
    $req_no.= $temp . ($curid + 1);
  }

  $prev_inv_no = $prefix.$todayDate."/".$req_no;
  echo $prev_inv_no."<br>";
  return $prev_inv_no;
}


function clean($str) {  
  $res = str_replace(array( '\'', '"', '„','“'), '', $str);      
  return $res; 
} 

