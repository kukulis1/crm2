<?php
include '../v1/external_data/ws.config.php';
global $config;
require '../v1/external_data/utils.php';
 set_time_limit(500);
 error_reporting(1);


try {

    require('../v1/external_data/mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel (id,
                                                                external_order_id,  
                                                                external_load_id, 
                                                                productid,
                                                                quantity,                                                              
                                                                comment,
                                                                description,
                                                                cargo_measure,
                                                                cargo_wgt,
                                                                cargo_length,
                                                                cargo_width,                                                     
                                                                cargo_height, 
                                                                pll, 
                                                                document,                                                                
                                                                ean,
                                                                source,
                                                                import_date,
                                                                note,
                                                                service,
                                                                ordered_weight,
                                                                ordered_length,
                                                                ordered_width,
                                                                ordered_height,
                                                                revised_measure,
                                                                revised_quantity,
                                                                revised_weight,
                                                                revised_length,
                                                                revised_width,
                                                                revised_height,
                                                                m3) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?)');
                                                                   

    $sth4 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1317 = ? , cf_1319 = ? WHERE   salesorderid = ? ');                
    

    $sth = $dbh->prepare("SELECT DISTINCT t.*, s.salesorderid, s.accountid
                            FROM app_loads_handle t
                            JOIN vtiger_salesorder s ON s.external_order_id = t.order_id 
                            JOIN vtiger_crmentity e ON e.crmid = s.salesorderid      
                            LEFT JOIN vtiger_inventoryproductrel i ON i.external_load_id=t.id AND productid = 14244                                                                    
                            WHERE e.deleted = 0  AND i.id IS NULL
                            ");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());


    while ($row = $sth->fetch()) {

        $ordered = multiexplode(array("x","X"," "), $row['ordered']);
        $revised = multiexplode(array("x","X"," "), $row['revised']);

        $ordered_m3 = $ordered[1]*$ordered[2]*$ordered[3];
        $revised_m3 = $revised[1]*$revised[2]*$revised[3];

        $weight = $row['weight'];
        $length = $row['length'];
        $width = $row['width'];
        $height = $row['height'];


        if($ordered[0] <= $revised[0] && $ordered_m3 <= $revised_m3){
            $weight = $revised[0];
            $length = $revised[1];           
            $width = $revised[2];    
            $height = $revised[3];    
        }elseif($ordered[0] <= $revised[0] && $ordered_m3 >= $revised_m3){
            $weight = $ordered[0];
            $length = $ordered[1];           
            $width = $ordered[2];    
            $height = $ordered[3];  
        }elseif($ordered[0] >= $revised[0] && $ordered_m3 >= $revised_m3){
            $weight = $ordered[0];
            $length = $ordered[1];           
            $width = $ordered[2];    
            $height = $ordered[3];  
        }elseif($ordered[0] >= $revised[0] && $ordered_m3 <= $revised_m3){    
            $weight = $revised[0];
            $length = $revised[1];           
            $width = $revised[2];    
            $height = $revised[3];    
        }


        $weight_ordered = explode(' ' ,$row['ordered']);
        $dim_ordered = explode('x', $weight_ordered[1]);

        $weight_revised = explode(' ' ,$row['revised']);
        $dim_revised = explode('x', $weight_revised[1]);

       
        if(empty($dim_ordered[0])) $dim_ordered[0] = 0;
        if(empty($dim_ordered[1])) $dim_ordered[1] = 0;
        if(empty($dim_ordered[2])) $dim_ordered[2] = 0;

        if(empty($dim_revised[0])) $dim_revised[0] = 0;
        if(empty($dim_revised[1])) $dim_revised[1] = 0;
        if(empty($dim_revised[2])) $dim_revised[2] = 0;



            $default_product = 14244;
            $source = 'Metrika';        
            $date = $row['order_date'];        
            $date = date("Y-m-d", $date);   
            $row['quantity'] = ( $row['quantity'] < 1) ? 1 :  $row['quantity'];      


                $sth2->execute(array($row['salesorderid'],
                    $row['order_id'],
                    $row['id'],
                    $default_product, 
                    $row['quantity'],                                        
                    $row['note'], 
                    $row['note'],       
                    $row['measure'],
                    $weight,
                    $length,
                    $width,                       
                    $height,
                    $row['pll'],
                    $row['document'],                                
                    $row['ean'], 
                    $source,                     
                    $date,
                    $note,
                    2,
                    $weight_ordered[0],
                    $dim_ordered[0],
                    $dim_ordered[1],
                    $dim_ordered[2],                      
                    $row['revised_measure'],  
                    $row['revised_quantity'],  
                    $weight_revised[0],
                    $dim_revised[0],
                    $dim_revised[1],
                    $dim_revised[2],
                    $row['m3']                                        
                ));

                $orderid =  $row['order_id'];
                $ordered_2[$orderid] .= $row['quantity']." ".$row['measure']." ".$row['ordered'].", ";

                $revised_quantity = $row['revised_quantity'];
                if(in_array($row['revised_measure'], [1, 7, 'pll', 'nestd.'])){
                    $revised_quantity = $row['pll'];
                }
                $revised_2[$orderid] .= $revised_quantity." ".$row['revised_measure']." ".$row['revised'].", ";

                
                   $sth4->execute(array(
                    rtrim($ordered_2[$orderid], ', '),
                    rtrim($revised_2[$orderid], ', '),                        
                    $row['salesorderid']          
                ));              

    }

    echo json_encode("Success");
    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error! ";
   echo $e->getMessage();
}

      

?>
