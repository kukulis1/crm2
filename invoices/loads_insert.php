<?php
include '../v1/external_data/ws.config.php';
global $config;
require '../v1/external_data/utils.php';
 set_time_limit(500);
 error_reporting(1);


try {

    require('../v1/external_data/mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel (id,
                                                                external_order_id,  
                                                                external_load_id, 
                                                                productid,
                                                                quantity,                                                              
                                                                comment,
                                                                description,
                                                                cargo_measure,
                                                                cargo_wgt,
                                                                cargo_length,
                                                                cargo_width,                                                     
                                                                cargo_height, 
                                                                pll, 
                                                                document,                                                                
                                                                ean,
                                                                source,
                                                                import_date,
                                                                note,
                                                                service,
                                                                ordered_weight,
                                                                ordered_length,
                                                                ordered_width,
                                                                ordered_height,
                                                                revised_measure,
                                                                revised_quantity,
                                                                revised_weight,
                                                                revised_length,
                                                                revised_width,
                                                                revised_height,
                                                                m3) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?)');
                                                            

    $sth4 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1317 = ? , cf_1319 = ? WHERE   salesorderid = ? ');                
    

    // NOTE Get all affected order_ids
    $old_loads_order_ids_query = $dbh->prepare(
    '   SELECT loads.order_id
        FROM app_loads_handle AS loads
        GROUP BY loads.order_id
    ');
    $old_loads_order_ids_query->execute();
    $old_loads_order_ids = $old_loads_order_ids_query->fetchAll(PDO::FETCH_COLUMN);
    
    // NOTE Delete all loads from CRM that belong to affected orders
    if(!empty($old_loads_order_ids)){
        $imploded_old_loads_order_ids = implode(',', $old_loads_order_ids);
        $delete_old_crm_loads_query = $dbh->prepare(
        "   DELETE FROM vtiger_inventoryproductrel
            WHERE external_order_id IN ({$imploded_old_loads_order_ids})
        ");
        $delete_old_crm_loads_query->execute();
    }

    // NOTE Get all new loads
    $get_all_new_loads_query = $dbh->prepare(
    '   SELECT loads.*, sales_orders.salesorderid, sales_orders.accountid
        FROM app_loads_handle AS loads
        JOIN vtiger_salesorder AS sales_orders 
            ON sales_orders.external_order_id = loads.order_id                                                            
    ');
    $get_all_new_loads_query->setFetchMode(PDO::FETCH_ASSOC);
    $get_all_new_loads_query->execute();


    // NOTE Insert all new loads
    $all_new_loads = $get_all_new_loads_query->fetchAll();
    foreach($all_new_loads as $new_load){
        $ordered = multiexplode(array("x", "X", " "), $new_load['ordered']);
        $revised = multiexplode(array("x", "X", " "), $new_load['revised']);

        $ordered_m3 = $ordered[1] * $ordered[2] * $ordered[3];
        $revised_m3 = $revised[1] * $revised[2] * $revised[3];

        $weight = $new_load['weight'];
        $length = $new_load['length'];
        $width = $new_load['width'];
        $height = $new_load['height'];

        if ($ordered[0] <= $revised[0] && $ordered_m3 <= $revised_m3) {
            $weight = $revised[0];
            $length = $revised[1];
            $width = $revised[2];
            $height = $revised[3];
        } elseif ($ordered[0] <= $revised[0] && $ordered_m3 >= $revised_m3) {
            $weight = $ordered[0];
            $length = $ordered[1];
            $width = $ordered[2];
            $height = $ordered[3];
        } elseif ($ordered[0] >= $revised[0] && $ordered_m3 >= $revised_m3) {
            $weight = $ordered[0];
            $length = $ordered[1];
            $width = $ordered[2];
            $height = $ordered[3];
        } elseif ($ordered[0] >= $revised[0] && $ordered_m3 <= $revised_m3) {
            $weight = $revised[0];
            $length = $revised[1];
            $width = $revised[2];
            $height = $revised[3];
        }


        $weight_ordered = explode(' ', $new_load['ordered']);
        $dim_ordered = explode('x', $weight_ordered[1]);

        $weight_revised = explode(' ', $new_load['revised']);
        $dim_revised = explode('x', $weight_revised[1]);


        if (empty($dim_ordered[0])) $dim_ordered[0] = 0;
        if (empty($dim_ordered[1])) $dim_ordered[1] = 0;
        if (empty($dim_ordered[2])) $dim_ordered[2] = 0;

        if (empty($dim_revised[0])) $dim_revised[0] = 0;
        if (empty($dim_revised[1])) $dim_revised[1] = 0;
        if (empty($dim_revised[2])) $dim_revised[2] = 0;

        $default_product = 14244;
        $source = 'Metrika';
        $date = $new_load['order_date'];
        $date = date("Y-m-d", strtotime($date));
        $new_load['quantity'] = ($new_load['quantity'] < 1) ? 1 :  $new_load['quantity'];

        $sth2->execute(array(
            $new_load['salesorderid'],
            $new_load['order_id'],
            $new_load['id'],
            $default_product,
            $new_load['quantity'],
            $new_load['note'],
            $new_load['note'],
            $new_load['measure'],
            $weight,
            $length,
            $width,
            $height,
            $new_load['pll'],
            $new_load['document'],
            $new_load['ean'],
            $source,
            $date,
            $note,
            2,
            $weight_ordered[0],
            $dim_ordered[0],
            $dim_ordered[1],
            $dim_ordered[2],
            $new_load['revised_measure'],
            $new_load['revised_quantity'],
            $weight_revised[0],
            $dim_revised[0],
            $dim_revised[1],
            $dim_revised[2],
            $new_load['m3']
        ));

        $orderid =  $new_load['order_id'];
        $ordered_2[$orderid] .= $new_load['quantity'] . " " . $new_load['measure'] . " " . $new_load['ordered'] . ", ";
        
        $revised_quantity = $new_load['revised_quantity'];
        if(in_array($new_load['revised_measure'], [1, 7, 'pll', 'nestd.'])){
            $revised_quantity = $new_load['pll'];
        }
        $revised_2[$orderid] .= $revised_quantity . " " . $new_load['revised_measure'] . " " . $new_load['revised'] . ", ";

        $sth4->execute(array(
            rtrim($ordered_2[$orderid], ', '),
            rtrim($revised_2[$orderid], ', '),
            $new_load['salesorderid']
        ));              

    }

    echo json_encode("Success");
    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error! ";
   echo $e->getMessage();
}

?>