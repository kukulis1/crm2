<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");



  $invoiceid = $_POST['invoiceid'];

  $getComment = $conn->query("SELECT m.commentcontent, CONCAT(first_name,' ',last_name) as employee, e.createdtime
                              FROM vtiger_modcomments m
                              LEFT JOIN vtiger_crmentity e ON e.crmid=m.modcommentsid
                              LEFT JOIN vtiger_users u ON u.id=e.smcreatorid
                              WHERE related_to = $invoiceid
                              GROUP BY modcommentsid
                              ORDER BY modcommentsid DESC LIMIT 10");


  $comments_arr = array();
  foreach($getComment as $comment){
    $comments_arr[] = $comment;
  }
                     

  echo json_encode($comments_arr);


}else{
  http_response_code(404);
}