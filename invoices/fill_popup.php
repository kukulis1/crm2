<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

if(is_numeric($_POST['order'])){
  $orderId = $_POST['order'];
}

// $orderId = 74232;


$query = "SELECT 
vtiger_salesorder.salesorder_no, 
vtiger_salesorder.salesorderid,
vtiger_salesorder.total, 
vtiger_salesorder.accountid, 
vtiger_salesorder.load_date_from,vtiger_salesorder.unload_date_to, 
vtiger_salesorder.shipment_code, 
vtiger_salesorder.sostatus as status, 
vtiger_salesorder.source, 
vtiger_salesorder.shipment_code,
vtiger_account.accountname, 
vtiger_accountscf.cf_1279 AS payment_deferral,
vtiger_inventoryproductrel.productid,
vtiger_sobillads.load_company,
vtiger_sobillads.bill_city,
vtiger_sobillads.bill_code,
vtiger_sobillads.bill_street,
vtiger_sobillads.bill_country,
vtiger_sobillads.load_company_code as sender_code,
vtiger_sobillads.load_contact as sender_contact,
vtiger_soshipads.unload_company, 
vtiger_soshipads.ship_city,
vtiger_soshipads.ship_code,
vtiger_soshipads.ship_street,
vtiger_soshipads.ship_country,
vtiger_soshipads.unload_company_code as receiver_code,
vtiger_soshipads.unload_contact as receiver_contact,
vtiger_salesordercf.cf_855 AS type,
vtiger_salesordercf.cf_1299 AS note,
vtiger_salesordercf.cf_926 AS stevedoring,
vtiger_salesordercf.cf_928 AS km,
vtiger_salesordercf.cf_930 as termo, 
vtiger_salesordercf.cf_932 as working_time, 
vtiger_salesordercf.cf_1297 as pricebook, 
vtiger_salesordercf.cf_1317 as ordered, 
vtiger_salesordercf.cf_1367 as ordered_m3, 
vtiger_salesordercf.cf_1319 as revised, 
vtiger_salesordercf.cf_1365 as revised_m3, 
vtiger_salesordercf.cf_1374 as pricebook_price, 
vtiger_salesordercf.cf_1376 as agreed_price, 
vtiger_salesordercf.cf_1378 as agreed_price_desc, 
vtiger_crmentity.modifiedtime,
vtiger_crmentity.createdtime
              FROM vtiger_salesorder                         
              LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
              LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid
              LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
              LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid                    
              LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
              LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid 
              LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid 
              WHERE vtiger_salesorder.salesorderid = $orderId             
              GROUP BY  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN vtiger_salesorder.salesorderid ELSE vtiger_inventoryproductrel.sequence_no END
              ORDER BY vtiger_salesorder.salesorderid DESC";

$query2 = "SELECT app_measures.code as cargo_measure,vtiger_inventoryproductrel.quantity, vtiger_inventoryproductrel.description,app_services_type.name as service_name,
CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  vtiger_inventoryproductrel.margin ELSE vtiger_inventoryproductrel.margin END as margin,
  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  vtiger_inventoryproductrel.cargo_wgt ELSE vtiger_inventoryproductrel.cargo_wgt END as cargo_wgt,      
  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  vtiger_inventoryproductrel.cargo_length ELSE vtiger_inventoryproductrel.cargo_length END as cargo_length,
  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  vtiger_inventoryproductrel.cargo_width  ELSE vtiger_inventoryproductrel.cargo_width END AS cargo_width,
  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  vtiger_inventoryproductrel.cargo_height ELSE vtiger_inventoryproductrel.cargo_height END as cargo_height,
  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  vtiger_inventoryproductrel.quantity  ELSE  vtiger_inventoryproductrel.quantity END AS quantity,
  vtiger_inventoryproductrel.service
            FROM vtiger_inventoryproductrel         
            LEFT JOIN app_measures on app_measures.id=vtiger_inventoryproductrel.cargo_measure OR app_measures.code=vtiger_inventoryproductrel.cargo_measure
            LEFT JOIN app_services_type on app_services_type.id=vtiger_inventoryproductrel.service
            WHERE vtiger_inventoryproductrel.id = $orderId          
            GROUP BY CASE WHEN vtiger_inventoryproductrel.external_load_id = '' THEN vtiger_inventoryproductrel.sequence_no ELSE vtiger_inventoryproductrel.external_load_id END 
            ORDER BY vtiger_inventoryproductrel.id DESC";


$order_record = mysqli_fetch_assoc($conn->query($query));


  $order_dimensions = $conn->query($query2);
  $dim = array();
  while($row = $order_dimensions->fetch_assoc()){
        $dim[] = $row;
  }

  $orders = array(
    'info' => $order_record,
    'dimensions' => $dim
  );

    // echo "<pre>";
    // print_R($orders);
    // die();
  echo json_encode($orders); 
