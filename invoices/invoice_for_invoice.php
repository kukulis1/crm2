<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $invoices = $_POST['invoices'];

// $invoices =  '100211,100220';

    $query = "SELECT vtiger_account.accountname,vtiger_account.accountid,vtiger_accountscf.cf_1279 as payment_deferral
        FROM vtiger_invoice
        LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid
        LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_account.accountid
        WHERE vtiger_invoice.invoiceid IN ($invoices)	
        GROUP BY vtiger_invoice.invoiceid											
        ORDER BY vtiger_invoice.invoiceid DESC";
 
        $order_record = mysqli_fetch_array($conn->query($query));




      
      $order = array(
        'accountname' => $order_record["accountname"], 
        'accountid' => $order_record["accountid"],
        'payment_deferral' => $order_record["payment_deferral"]
      );

      // echo "<pre>";
      // print_R($order);
      echo json_encode($order);

    }else{
      http_response_code(404);
    }