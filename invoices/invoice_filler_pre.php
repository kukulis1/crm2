<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

if(is_numeric($_POST['orderId'])){
  $orderId = $_POST['orderId'];
}

// $orderId =  84926;
global $storageAddreses;

    $query = "SELECT vtiger_salesorder.salesorder_no, vtiger_salesorder.salesorderid, vtiger_salesorder.total, vtiger_salesorder.accountid,vtiger_salesorder.load_date_from,vtiger_salesorder.unload_date_to, vtiger_salesorder.shipment_code,vtiger_account.accountname,vtiger_accountscf.cf_1279 as payment_deferral,  IF(vtiger_inventoryproductrel.productid = 0, 14244, IF(vtiger_inventoryproductrel.productid IS NULL, 14244,vtiger_inventoryproductrel.productid)) AS proid, IF(vtiger_inventoryproductrel.productid = 0, 14244, IF(vtiger_inventoryproductrel.productid IS NULL, 14244,vtiger_inventoryproductrel.productid)) AS productid, vtiger_sobillads.load_company,vtiger_sobillads.bill_city,vtiger_sobillads.bill_code,vtiger_sobillads.bill_street,vtiger_soshipads.unload_company, vtiger_soshipads.ship_city,vtiger_soshipads.ship_code,vtiger_soshipads.ship_street,
      CASE ";
    foreach($storageAddreses AS $key => $item):
      $explode = explode("|##|",$item);
      $query .=	"WHEN (vtiger_sobillads.bill_street LIKE '%$explode[0]%' AND vtiger_sobillads.bill_city LIKE '$explode[1]%') OR (vtiger_soshipads.ship_street LIKE '%$explode[0]%' AND  vtiger_soshipads.ship_city LIKE '$explode[1]%') THEN '$key' ";		
    
    endforeach;
   

    $query .= "	ELSE '0' 	END AS storage,
    -- CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.margin) ELSE vtiger_inventoryproductrel.margin END as margin,
    CASE      
      WHEN vtiger_inventoryproductrel.productid != 36641 
			THEN IF(cf_1376 > 0,cf_1376, IF(vtiger_inventoryproductrel.margin, IF(vtiger_inventoryproductrel.source = 'Metrika', FORMAT(SUM(vtiger_inventoryproductrel.margin)/count( vtiger_inventoryproductrel.id),2), SUM(vtiger_inventoryproductrel.margin)), vtiger_salesorder.total))        
			ELSE vtiger_inventoryproductrel.margin END as margin,
    	ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(revised_weight) ELSE SUM(ordered_weight) END,2) AS cargo_wgt,
			ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(m3) ELSE SUM((ordered_length * ordered_width * ordered_height) * quantity) END,2)  AS volume,    
    CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_length) ELSE vtiger_inventoryproductrel.cargo_length END as cargo_length,
    CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_width)  ELSE vtiger_inventoryproductrel.cargo_width END AS cargo_width,
    CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_height) ELSE vtiger_inventoryproductrel.cargo_height END as cargo_height,
    CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.quantity)  ELSE  vtiger_inventoryproductrel.quantity END AS quantity,

    
     vtiger_inventoryproductrel.note,vtiger_inventoryproductrel.service,
    CASE 
      WHEN vtiger_salesorder.carrier LIKE '%Neteisingas adresas (pasikrovimas%' THEN 'Tuščias važiavimas. '
      WHEN vtiger_salesorder.carrier LIKE '%Neteisingas adresas (pristatymas%' THEN 'Pristatymas kitu adresu. '
      WHEN vtiger_salesorder.carrier LIKE '%Negalimas privažiavimas%' THEN 'Pristatymas kitu adresu. '
      WHEN vtiger_salesorder.carrier LIKE '%Krovinis netinkamai supakuotas transportavimui%' THEN 'Krovinis netinkamai supakuotas transportavimui - Papildomas paruošimas transportavimui. '
      WHEN vtiger_salesorder.carrier LIKE '%Nebuvo vietoje gavėjo%' THEN 'Papildomas prsitatymas. '
      WHEN vtiger_salesorder.carrier LIKE '%Krovinis neatitinka užsakymo%' THEN 'Tuščias važiavimas. '
      WHEN vtiger_salesorder.carrier LIKE '%Tuščias važiavimas%' THEN 'Tuščias važiavimas. '
      ELSE ''
    END AS fail_remark,
    CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN GROUP_CONCAT(FORMAT(vtiger_inventoryproductrel.quantity,0),' ', app_measures.code)  ELSE vtiger_inventoryproductrel.cargo_wgt END as cargo
                  FROM vtiger_salesorder    
                  -- LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid     
                  LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
                  LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid
                  LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
                  LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid   
                  -- LEFT JOIN `vtiger_invoice` ON vtiger_invoice.salesorderid=vtiger_salesorder.salesorderid    
                  LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                  LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid 
                  LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
                  LEFT JOIN `app_taxable_dimensions` ON app_taxable_dimensions.salesorderid=vtiger_salesorder.salesorderid       
                  LEFT JOIN `app_measures` ON CASE WHEN  vtiger_inventoryproductrel.cargo_measure REGEXP '^[0-9]+$' THEN app_measures.id=vtiger_inventoryproductrel.cargo_measure ELSE app_measures.code =vtiger_inventoryproductrel.cargo_measure END
                  WHERE vtiger_salesorder.salesorderid = '".$orderId."'             
                  GROUP BY  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN vtiger_salesorder.salesorderid ELSE vtiger_inventoryproductrel.service END
                  ORDER BY vtiger_salesorder.shipment_code ASC,sequence_no ASC";




      $subquery_servises = "SELECT * FROM `app_services_type`";     
      $order_record = mysqli_fetch_array($conn->query($query));             
      $order_dimensions = $conn->query($query);
      $order_services = $conn->query($subquery_servises);

 
      $dim = array();
      while($row = $order_dimensions->fetch_assoc()){ 
        $dim[] = $row;    
        $cargo_list .= $row['cargo'];        
      }

      $services = array();
      while($row2 = $order_services->fetch_assoc()){ 
        $services[] = $row2;          
      } 

      $cargo_array = explode(",", $cargo_list);
      $cargo_values = array();
      foreach($cargo_array as $str){
        $str = explode(" ",$str);
        $cargo_values[$str[1]] += $str[0];
      }

      foreach ($cargo_values as $code => $sum) {     
        $cargo .= "$sum $code,";
      }


      
      $order = array(
        'salesorder_no' => $order_record["salesorder_no"],
        'salesorderid' => $order_record["salesorderid"],
        'total' => $order_record["total"],
        'payment_deferral' => $order_record["payment_deferral"],
        'accountid' => $order_record["accountid"],
        'accountname' => $order_record["accountname"],       
        'note' => $order_record["note"],       
        'dimensions' => $dim,
        'measure' => $cargo,
        'services' => $services,
        'edit' => 0,
        'one' => true
      );

      // echo "<pre>";
      // print_R($order);
      echo json_encode($order);

    
