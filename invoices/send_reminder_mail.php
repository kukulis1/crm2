<?php
require '../vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
require '../vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


require '../vtlib/Vtiger/vendor/autoload.php';

require_once "../config.inc.php";

global $mailHost, $mailUsername , $mailPassword;


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

$query = mysqli_fetch_assoc($conn->query("SELECT value FROM app_other_settings WHERE title = 'LBL_PAY_INVOICE_REMINDER'"));
$message1 = $query['value'];

$query2 = mysqli_fetch_assoc($conn->query("SELECT value FROM app_other_settings WHERE title = 'LBL_DEBTS_REMINDER'"));
$message2 = $query2['value'];

$to = $_POST['email'];
$invoiceid = $_POST['invoiceid'];
$type = $_POST['type'];
$files = $_FILES['file']['name'];
$files_tmp = $_FILES['file']['tmp_name'];

$files_arr = array();

for($i=0; $i < count($files); $i++){
  $files_arr[] = $files[$i];
  move_uploaded_file($files_tmp[$i], "../storage/".$files[$i]);
}

$query2 = mysqli_fetch_assoc($conn->query("SELECT subject, invoicedate, duedate, ROUND(total,2) AS total,accountname  FROM vtiger_invoice i LEFT JOIN vtiger_account a ON a.accountid=i.accountid WHERE invoiceid = $invoiceid"));


$invoiceno =  $query2['subject'];
$accountname =  $query2['accountname'];
$invoicedate =  $query2['invoicedate'];
$duedate =  $query2['duedate'];
$total =  $query2['total'];

$subject = "Parnasas saskaita nr. $invoiceno";  

if($type == 'remind'){
  $full_message = '<strong>PRIMINIMAS APIE ARTĖJANTĮ MOKĖJIMO TERMINĄ</strong><br>';
  $full_message .= $message1."<br><br>";
}else{
  $full_message = $message2."<br><br>";
}

$message_table = '<html><meta charset="utf-8"><body>';
$message_table .= '<table cellpadding="10" style="border-collapse: collapse; border: 1px solid black;">';

if($type == 'remind'){
  $message_table .= '<tr>';
  $message_table .= "<th style='border: 1px solid black;'>Dokumento Nr.</th>";
  $message_table .= "<th style='border: 1px solid black;'>Klientas</th>";
  $message_table .= "<th style='border: 1px solid black;'>Išrašymo data</th>";
  $message_table .= "<th style='border: 1px solid black;'>Apmokėjimo data</th>";
  $message_table .= "<th style='border: 1px solid black;'>Suma</th></tr>";
  $message_table .= "</tr>";

  $message_table .= '<tr>';
  $message_table .= "<td style='border: 1px solid black;'>$invoiceno</td>";
  $message_table .= "<td style='border: 1px solid black;'>$accountname</td>";
  $message_table .= "<td style='border: 1px solid black;'>$invoicedate</td>";
  $message_table .= "<td style='border: 1px solid black;'>$duedate</td>";
  $message_table .= "<td style='border: 1px solid black;'>$total</td>";
  $message_table .= "</tr>";

  $message_table .= '<tr>';
  $message_table .= "<td style='border: 1px solid black;'>Bendra suma</td>";
  $message_table .= "<td></td>";
  $message_table .= "<td></td>";
  $message_table .= "<td></td>";
  $message_table .= "<td style='border: 1px solid black;'>$total</td>";
  $message_table .= "</tr>";
}else{
  $message_table .= '<tr>';
  $message_table .= "<th style='border: 1px solid black;'>Išrašymo data</th>";
  $message_table .= "<th style='border: 1px solid black;'>Klientas</th>";
  $message_table .= "<th style='border: 1px solid black;'>Mokėjimo terminas</th>";
  $message_table .= "<th style='border: 1px solid black;'>Sąskaitos Nr.</th>";
  $message_table .= "<th style='border: 1px solid black;'>Suma su PVM (Eur)</th></tr>";
  $message_table .= "</tr>";

  $message_table .= '<tr>';
  $message_table .= "<td style='border: 1px solid black;'>$invoicedate</td>";
  $message_table .= "<td style='border: 1px solid black;'>$accountname</td>";
  $message_table .= "<td style='border: 1px solid black;'>$duedate</td>";
  $message_table .= "<td style='border: 1px solid black;'>$invoiceno</td>";
  $message_table .= "<td style='border: 1px solid black;'>$total</td>";
  $message_table .= "</tr>";

  $message_table .= '<tr>';
  $message_table .= "<td></td>";
  $message_table .= "<td></td>";
  $message_table .= "<td></td>";
  $message_table .= "<td style='border: 1px solid black;'>Viso skola (Eur):</td>";
  $message_table .= "<td style='border: 1px solid black;'>$total</td>";
  $message_table .= "</tr>";
}

$message_table .= '</table>';
$message_table .= "</body></html><br><br>";


$full_message .= $message_table;

if($type == 'remind'){
  $bottom_message = "Jei sąskaita (- os) Jūsų nepasiekė ar yra kitų kliūčių ją (jas) apmokėti laiku, prašome kuo skubiau<br>
  apie tai mus informuoti, siunčiant pranešimą elektroniniu paštu: $mailUsername. Kitu<br>
  atveju, tikimės, jog Jums nekils jokių kliūčių laiku įvykdyti mokėjimo prievolės.<br><br>
  
  Telefonai pasiteiravimui: +370 652 36920<br>
  El.paštas: apskaita@parnasas.lt'<br><br>
  
  Mūsų atsiskaitomosios sąskaitos rekvizitai:<br>
  AB SEB bankas, banko kodas 70440<br>
  LT12 7044 0600 0798 3370<br><br>
  <img src='https://crm.parnasas.lt/resources/signatare.jpg'>";
}else{
  $bottom_message = "Kadangi dar neesame sulaukę Jūsų tinkamo atsiskaitymo už suteiktas paslaugas, raginame padengti susidariusį įsiskolinimą dviejų kalendorinių dienų <br>laikotarpyje, o pavedimo kopiją atsiųsti el. paštu: apskaita@parnasas.lt. <br>
  Iškilus bet kokiems neaiškumams būtinai kreipkitės į mūsų apskaitos skyrių telefonu ( 8 5) 206 10 16 . <br><br>

  Didesnis dėmesys atsiskaitymams labai pagerintų mūsų bendradarbiavimą.<br><br>

  Mūsų atsiskaitomosios sąskaitos rekvizitai:<br>
  AB SEB bankas, banko kodas 70440<br>
  LT12 7044 0600 0798 3370<br><br>
  <img src='https://crm.parnasas.lt/resources/signatare.jpg'>";
}

$full_message .= $bottom_message;

$mail = new PHPMailer(true);

try {
  //Server settings
  // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
  $mail->isSMTP();                                            // Send using SMTP
  $mail->Host       = $mailHost;                    // Set the SMTP server to send through
  $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
  $mail->Username   = $mailUsername;                     // SMTP username
  $mail->Password   = $mailPassword;                   // SMTP password
  $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
  $mail->Port       = 465;      
  $mail->SMTPOptions = array(
      'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
      )
    );                              // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    $mail->setLanguage('lt', '../vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
    $mail->CharSet = 'UTF-8';
    // $mail->SMTPDebug = false;


  $send_to = explode(",", $to);  



  $mail->setFrom($mailUsername, 'Parnasas');

  foreach($send_to AS $to){
    $mail->addAddress($to, $to);
  } 


  // Attachments
  for($x=0; $x < count($files_arr); $x++){
    $mail->addAttachment('../storage/'.$files_arr[$x]);   
  }


  // Content
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = $subject;
  $mail->Body    = $full_message;
  $mail->AltBody = $full_message;
  $mail->send();

  echo json_encode(['status' => 'success']);

  $checkQuery = $conn->query("SELECT count FROM vtiger_debts_reminder WHERE invoiceid = $invoiceid");
  $check = mysqli_num_rows($checkQuery);

  if($check){
    $conn->query("UPDATE vtiger_debts_reminder SET `count` = `count`+1 WHERE invoiceid = $invoiceid"); 
  }else{
    $conn->query("INSERT INTO vtiger_debts_reminder (invoiceid,count) VALUES ($invoiceid,1)"); 
  }

  for($x=0; $x < count($files_arr); $x++){
     unlink("../storage/".$files_arr[$x]);
  }
  exit;

} catch (Exception $e) {
  $error = $e->getMessage();
  $conn->query("INSERT INTO email_send_from_crm_fails (record, email_address, error, time) VALUES ('$invoiceid', '$to', '$error', NOW())");
  echo json_encode(['status' => 'fail', 'response' => $error]);
}
