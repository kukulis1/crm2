<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

    $from = $_POST['from'];
    $to = $_POST['to'];
    $filterBy = $_POST['filterBy'];

    $customer = $_POST['customer']; 
    $invoicedate = $_POST['invoicedate'];
    $invoice_no = $_POST['invoice_no'];   
    $total = $_POST['total'];
    $duedate = $_POST['duedate'];
    $invoice_created = $_POST['invoice_created'];
    $resp_debts = $_POST['resp_debts'];
    $resp_orders = $_POST['resp_orders'];
    $createdtime = $_POST['createdtime'];



    if(!empty($from) || !empty($to) || !empty($invoicedate) || !empty($invoice_no) ||!empty($customer) ||!empty($total) ||!empty($duedate) ||!empty($invoice_created) ||!empty($resp_debts) ||!empty($resp_orders) ||!empty($createdtime)){

        $query = "SELECT vtiger_invoice.invoiceid, accountname, invoicedate,invoice_no,ROUND(total,2) AS total , duedate, CONCAT(u.first_name,' ',u.last_name) AS created_invoice, CONCAT(responsible_for_debts.first_name,' ',responsible_for_debts.last_name) AS resp_debts, CONCAT(responsible_for_orders.first_name,' ',responsible_for_orders.last_name) AS resp_orders, createdtime,vtiger_invoice.accountid
                                          FROM `vtiger_invoice`                                           
                                          JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid                                        
                                          LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_invoice.accountid
                                          LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_invoice.accountid
                                          LEFT JOIN `vtiger_users` u ON u.id=vtiger_crmentity.smownerid     
                                          LEFT JOIN `vtiger_users` responsible_for_debts ON responsible_for_debts.id=vtiger_accountscf.cf_1657   
                                          LEFT JOIN `vtiger_users` responsible_for_orders ON responsible_for_orders.id=vtiger_accountscf.cf_1659                                                 
                                          LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_invoice.invoiceid       
                                        --  JOIN `vtiger_invoice_auto_invoicing` ON vtiger_invoice_auto_invoicing.invoiceid=vtiger_invoice.invoiceid 
                                          WHERE vtiger_crmentity.setype = 'Invoice' AND vtiger_crmentity.deleted = 0 ";                                                                              
                                   
                                          if(!empty($from) && !empty($to)){
                                            
                                            if($filterBy == 'invoice_date'){
                                              $query .= " AND vtiger_invoice.invoicedate BETWEEN '$from' AND '$to' ";
                                            }elseif($filterBy == 'due_date'){
                                              $query .= " AND vtiger_invoice.duedate BETWEEN '$from' AND '$to' ";
                                            }
                                          }

                                           if(!empty($customer)){
                                            $query .= " AND  vtiger_account.accountname LIKE  '$customer%' ";
                                           }   
                                           if(!empty($invoicedate)){
                                            $query .= " AND  vtiger_invoice.invoicedate =  '$invoicedate' ";
                                           }                                         
                                           if(!empty($invoice_no)){
                                            $query .= " AND  vtiger_invoice.invoice_no LIKE '%$invoice_no%' ";
                                           } 
                                           if(!empty($total)){
                                            $query .= " AND  vtiger_invoice.total LIKE '%$total%' ";
                                           } 
                                           if(!empty($duedate)){
                                            $query .= " AND  vtiger_invoice.duedate LIKE '%$duedate%' ";
                                           } 
                                           if(!empty($invoice_created)){
                                            $query .= " AND  CONCAT(u.first_name,' ',u.last_name) LIKE '%$invoice_created%' ";
                                           }  
                                           if(!empty($resp_debts)){
                                            $query .= " AND  CONCAT(responsible_for_debts.first_name,' ',responsible_for_debts.last_name)  LIKE '%$resp_debts%' ";
                                           }                                 
                                           if(!empty($resp_orders)){
                                            $query .= " AND CONCAT(responsible_for_orders.first_name,' ',responsible_for_orders.last_name) LIKE '%$resp_orders%' ";
                                           }                                              
                                           if(!empty($createdtime)){
                                            $query .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$createdtime' ";
                                           }                                  

                                            $query .= " GROUP BY vtiger_invoice.invoiceid ";                                          
                                            $query .= " ORDER BY vtiger_crmentity.createdtime DESC LIMIT 500";  

                                          
      $order_dimensions = $conn->query($query);
      if(!$order_dimensions){
        echo json_encode('empty');
      }

      $result = array();
      while($row = $order_dimensions->fetch_assoc()) {
        $result[] = $row;      
      }   

      if(!empty($result)){
        echo json_encode($result);
      }else{
        echo json_encode('no_results');
      }
  
  }else{
    echo json_encode('empty');
  }

}else{
  http_response_code(404);
}
