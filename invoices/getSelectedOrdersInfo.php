<?php
error_reporting(0);
require_once "../config.inc.php";


if($_POST){

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $invoiceid = $_POST['invoiceid'];

  $info = $conn->query("SELECT subject, accountname, 
                          CASE 
                            WHEN vtiger_debts.debts_tks_suma THEN ROUND((total - SUM(DISTINCT vtiger_debts.debts_tks_suma)),2)
                            ELSE ROUND(total,2)
                          END AS total
                          FROM vtiger_invoice 
                          LEFT JOIN vtiger_account on vtiger_invoice.accountid=vtiger_account.accountid 
                          LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
                          LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
                          LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_debts.debtsid
                          WHERE vtiger_invoice.invoiceid IN ($invoiceid)
                          GROUP BY vtiger_invoice.invoiceid	 
                          ORDER BY invoiceid DESC");
                          

  $invoices_info = array();
  while($row = $info->fetch_assoc()){ 
    $invoices_info[] = $row;          
  }


  echo json_encode($invoices_info);

}else{
  http_response_code(404);
}
  
