<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include '../v1/external_data/ws.config.php';
global $config;
require '../v1/external_data/utils.php';


if ($_POST) {
    try {
        require('../v1/external_data/mysql_connection.php');
        
        $orders = json_decode($_REQUEST['orders']);

        if(empty($orders)){
            echo json_encode([
                'status' => 'no_orders'
            ]);
            return;
        }
        $imploded_orders = '\''.implode('\',\'', $orders).'\'';

        $select = "DISTINCT shipment.shipment_id ,departure_time_fact,
            CASE 
                WHEN location_id IS NULL THEN 1
                WHEN sender_location_id = location_id AND is_visited is false THEN 2
                WHEN sender_location_id != location_id AND is_visited is false AND consignee_location_id = location_id THEN 3
                WHEN sender_location_id != location_id AND is_visited is true AND consignee_location_id != location_id THEN 3			   
                WHEN sender_location_id = location_id AND is_visited is true AND consignee_location_id != location_id THEN 3
                WHEN sender_location_id != location_id AND is_visited is true AND consignee_location_id = location_id THEN 4
                WHEN consignee_location_id = location_id AND is_visited is true THEN 4
                ELSE 3
            END as status
        ";
        $table = 'shipment
            LEFT JOIN delivery ON delivery.shipment_id=shipment.shipment_id
            LEFT JOIN route_point_operation ON  route_point_operation.delivery_id=delivery.delivery_id
            LEFT JOIN route_point ON route_point.route_point_id=route_point_operation.route_point_id
        ';
        $where = " shipment.shipment_code IN ({$imploded_orders}) AND departure_time_fact IS NOT NULL  AND (failed = false OR failed IS NULL)    
            ORDER BY departure_time_fact
        ";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $imported_statues = json_decode($result, true);

        $update_status_query = $dbh->prepare('UPDATE vtiger_salesorder SET sostatus = ?  WHERE external_order_id = ?');

        $grouped_statuses = [];
        foreach($imported_statues['response']['result'] as $status){
            if($status['status'] > ($grouped_statuses[$status['shipment_id']] ?? 0)){
                $grouped_statuses[$status['shipment_id']] = $status['status'];
            }
        }
       
        $statuses = [
            1 => 'Sent', 
            2 => 'Approved', 
            3 => 'in progress', 
            4 => 'delivered',
        ];

        foreach($grouped_statuses as $shipment_id => $status_key){
            $update_status_query->execute([$statuses[$status_key], $shipment_id]);
        }
        
        echo json_encode([
            'status' => 'success',
        ]);
    } catch (PDOException $e) {
        echo json_encode([
            'status' => 'error',
            'message' => $e->getMessage()
        ]);
    }
}
