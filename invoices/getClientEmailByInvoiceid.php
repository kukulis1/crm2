<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

$invoiceid = $_POST['invoiceid'];
$account_email = mysqli_fetch_assoc($conn->query("SELECT email2 FROM vtiger_invoice i LEFT JOIN vtiger_account a ON a.accountid=i.accountid WHERE i.invoiceid = $invoiceid"));

$salesorders = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(salesorderid) AS salesorderids FROM vtiger_invoice_salesorders_list WHERE invoiceid = $invoiceid"));
$salesorderids = $salesorders['salesorderids'];


$downloadImages = $conn->query("SELECT filename, title, s.notesid 
                                  FROM vtiger_senotesrel s  	
                                      LEFT JOIN vtiger_seattachmentsrel r ON r.crmid=s.notesid
                                      LEFT JOIN vtiger_attachments a ON a.attachmentsid=r.attachmentsid
                                      LEFT JOIN vtiger_notes n ON n.notesid=s.notesid
                                      WHERE r.attachmentsid IS NULL AND  s.crmid IN ($salesorderids)
                                      GROUP BY s.notesid");
$deleteFiles = array();
foreach($downloadImages AS $img){
  $oldName =  basename($img['title']);
  $ext = pathinfo($img['title'], PATHINFO_EXTENSION); 
  $newName = lt_chars(str_replace(' ', '',pathinfo($oldName, PATHINFO_FILENAME)))."_{$img['notesid']}.".$ext; 
  copy($img['filename'], '../storage/invoices/'.$newName);
  $deleteFiles[] = 'storage/invoices/'.$newName;
}                                    


$files_query = $conn->query("SELECT IF(path IS NOT NULL, filename, title) AS filename, s.notesid, `path`, a.attachmentsid
                             FROM vtiger_senotesrel s  
                             LEFT JOIN vtiger_seattachmentsrel r ON r.crmid=s.notesid
                             LEFT JOIN vtiger_attachments a ON a.attachmentsid=r.attachmentsid
                             LEFT JOIN vtiger_notes n ON n.notesid=s.notesid
                             WHERE s.crmid IN ($salesorderids)
                             GROUP BY s.notesid");
$files = array();
foreach($files_query AS $file){  
  $oldName =  basename($file['filename']);
  $ext = pathinfo($file['filename'], PATHINFO_EXTENSION); 
  $newName = lt_chars(str_replace(' ', '',pathinfo($oldName, PATHINFO_FILENAME)))."_{$file['notesid']}.".$ext; 
 
  if(empty($file['path'])){
    $newName = 'storage/invoices/'.$newName;
  }else{
    $newName = $file['path'].$file['attachmentsid'].'_'.$newName;
  }

  $files[] =  [
    'url' => $newName,
    'filename' => $file['filename']
  ];
} 

echo json_encode(array('email' => $account_email['email2'], 'files' => $files, 'delete' => $deleteFiles));

function lt_chars($text) {   
  $char = array(
  "ą" => "a",
  "Ą" => "A",
  "č" => "c",
  "Č" => "C",
  "ę" => "e",
  "Ę" => "E",
  "ė" => "e",
  "Ė" => "E",   
  "į" => "i",
  "Į" => "I",
  "š" => "s",
  "Š" => "S",
  "ų" => "u",
  "Ų" => "U",
  "ū" => "u",
  "Ū" => "U",
  "ž" => "z",
  "Ž" => "Z");
  
  foreach ($char as $lt => $nlt) { 
    $text = str_replace($lt, $nlt, $text); 
  } 

  return $text; 
}