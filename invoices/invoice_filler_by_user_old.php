<?php
// error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_GET){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

if(is_numeric($_GET['userid'])){
  $userId = $_GET['userid'];
}


$from = $_GET['from'];
$to = $_GET['to'];
$filterBy = $_GET['filterBy'];
$orderDate = $_GET['orderDate'];
$orderNumber = $_GET['orderNumber'];
$customer = $_GET['customer'];
$pricebook = $_GET['pricebook'];
$loadDate = $_GET['loadDate'];
$loadAddress = $_GET['loadAddress'];
$unloadDate = $_GET['unloadDate'];
$unloadAddress = $_GET['unloadAddress'];
$status = $_GET['status'];
$manager = $_GET['manager'];

// $userId = 2191;

// $from = '2020-04-21';
// $to = '2020-05-06';
// $filterBy == 'order_date';


    $query_dates = "SELECT DISTINCT MIN(date_format(vtiger_crmentity.createdtime,'%Y-%m-%d')) AS date_from, MAX(date_format(vtiger_crmentity.createdtime,'%Y-%m-%d')) as date_to, ROUND(SUM(vtiger_salesorder.total),2) AS total
                            FROM vtiger_account  
                            LEFT JOIN vtiger_accountscf  ON vtiger_accountscf.accountid = vtiger_account.accountid   
                            LEFT JOIN `vtiger_salesorder` ON vtiger_salesorder.accountid=vtiger_account.accountid   
                            LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid    
                            LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid 
                            LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid      
                            LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_account.accountid 
                            LEFT JOIN vtiger_clientfuelrange ON vtiger_clientfuelrange.clientfuelrangeid=vtiger_crmentityrel.relcrmid
                            LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid   
                            WHERE vtiger_account.accountid = $userId 
                            AND vtiger_accountscf.cf_1568 = 'Taikoma bendra'
                            AND clientfuelrange_tks_minkurokai IS NOT NULL 
                            AND clientfuelrange_tks_maxkurokai IS NOT NULL
                            AND clientfuelrange_tks_priemoka IS NOT NULL
                            AND fuel_entity.deleted=0
                            AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_salesordercf.cf_1456 = 1 ";
                            
                         //   CASE WHEN vtiger_salesordercf.cf_1456 THEN ";
                              if(!empty($status)){
                                $query_dates .= "  vtiger_salesorder.sostatus LIKE '".$status."%' AND ";
                              }
                              //   $query_dates .= "  vtiger_invoice_salesorders_list.salesorderid IS NULL AND
                              // CASE 
                              // WHEN  vtiger_accountscf.cf_1281 = 'Kas savaitę' THEN YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW()) AND WEEK(vtiger_crmentity.createdtime, 1) < WEEK(NOW(), 1) 
                              // WHEN  vtiger_accountscf.cf_1281 = 'Du kartus per mėn.'                                             
                              //       THEN (MONTH(vtiger_crmentity.createdtime) = MONTH(NOW()) 
                              //       AND (DAYOFMONTH(NOW()) >= 15) AND DAYOFMONTH(vtiger_crmentity.createdtime) < 15) 
                              //       OR  (DAYOFMONTH(NOW()) = '".date('t')."' AND DAYOFMONTH(vtiger_crmentity.createdtime) > 15)
                              //       OR MONTH(NOW()) > MONTH(vtiger_crmentity.createdtime)
                              // WHEN vtiger_accountscf.cf_1281 = 'Už visą mėn.' THEN MONTH(vtiger_crmentity.createdtime) < MONTH(NOW())
                              // ELSE YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW())    
                              // END ";
                              
                              if(!empty($from) && !empty($to)){
                                if($filterBy == 'order_date'){
                                  $query_dates .= " AND DATE_FORMAT(vtiger_salesorder.import_date, '%Y-%m-%d') BETWEEN $from AND $to ";
                                }elseif($filterBy == 'load_date'){
                                  $query_dates .= " AND vtiger_salesorder.load_date_from BETWEEN '$from' AND '$to' ";
                                }elseif($filterBy == 'unload_date'){
                                  $query_dates .= " AND vtiger_salesorder.unload_date_to BETWEEN '$from' AND '$to' ";
                                }
                              }
                              if(!empty($orderDate)){
                                $query_dates .= " AND  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') LIKE  '$orderDate%' ";
                              }                                         
                              if(!empty($orderNumber)){
                                $query_dates .= " AND  vtiger_salesorder.shipment_code LIKE '%$orderNumber%' ";
                              }
                              if(!empty($customer)){
                                $query_dates .= " AND  vtiger_account.accountname LIKE '$customer%' ";
                              }
                              if(!empty($pricebook)){
                                $query_dates .= " AND  vtiger_salesordercf.cf_1297 LIKE '$pricebook%' ";
                              }
                              if(!empty($loadDate)){
                                $query_dates .= " AND  vtiger_salesorder.load_date_from LIKE '$loadDate%' ";
                              }
                              if(!empty($loadAddress)){
                                $query_dates .= " AND CONCAT(vtiger_sobillads.bill_street,' ', vtiger_sobillads.bill_city,' ',vtiger_sobillads.bill_code) LIKE '%$loadAddress%' ";
                              }
                              if(!empty($unloadDate)){
                                $query_dates .= " AND  vtiger_salesorder.unload_date_from LIKE '".$unloadDate."%' ";
                              }
                              if(!empty($unloadAddress)){  
                                $query_dates .= " AND CONCAT(vtiger_soshipads.ship_street,' ', vtiger_soshipads.ship_city,' ',vtiger_soshipads.ship_code) LIKE '%$unloadAddress%' ";
                              }

                              if(!empty($manager)){
                                $query_dates .= " AND  vtiger_users.user_name LIKE '$manager%' OR vtiger_users.first_name LIKE'$manager%' OR vtiger_users.last_name  LIKE '$manager%' OR CONCAT(vtiger_users.first_name,vtiger_users.last_name) LIKE '%$manager%'";
                              }
                              // $query_dates .= " ELSE '' END";


  $od = mysqli_fetch_assoc($conn->query($query_dates));

                 
    
    $query_proc = "SELECT 
                      FORMAT( (SELECT clientfuelrange_tks_priemoka 
                          FROM vtiger_clientfuelrange  
                          LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_clientfuelrange.clientfuelrangeid   
                          LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid                 
                          WHERE fuel_entity.deleted=0 AND ( fuelpricelist_tks_galiojimopra <= '".$od["date_from"]."' OR fuelpricelist_tks_galiojimopab <= '".$od["date_to"]."') 
                          AND (fuelpricelist_tks_kurokaina BETWEEN clientfuelrange_tks_minkurokai AND clientfuelrange_tks_maxkurokai) AND (vtiger_crmentityrel.crmid = $userId)),0) as surcharge           
                          FROM vtiger_fuelpricelist
                      GROUP BY surcharge
                          HAVING surcharge IS NOT NULL";


$proc = mysqli_fetch_assoc($conn->query($query_proc));
$proc = $proc["surcharge"];


$surcharge = $od['total'] / 100 * $proc;
$surcharge = round($surcharge);

    $query = "SELECT DISTINCT vtiger_account.accountname,vtiger_account.accountid,vtiger_accountscf.cf_1279 as payment_deferral, vtiger_salesorder.salesorderid, vtiger_inventoryproductrel.productid
                FROM vtiger_account 
                LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_account.accountid
                LEFT JOIN `vtiger_salesorder` ON vtiger_salesorder.accountid=vtiger_account.accountid
                LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid   
                LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid
                LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid         
                LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid    
                LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid    
                WHERE vtiger_account.accountid = '".$userId."' AND vtiger_invoice_salesorders_list.salesorderid IS NULL 
                AND  vtiger_invoice_salesorders_list.salesorderid IS NULL 
                AND vtiger_salesordercf.cf_1456 = 1  ";
               // CASE WHEN vtiger_salesordercf.cf_1456 THEN ";

                if(!empty($status)){
                  $query .= "AND  vtiger_salesorder.sostatus LIKE '".$status."%' ";
                }

                //   $query .= "  vtiger_invoice_salesorders_list.salesorderid IS NULL AND
                // CASE 
                // WHEN  vtiger_accountscf.cf_1281 = 'Kas savaitę' THEN YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW()) AND WEEK(vtiger_crmentity.createdtime, 1) < WEEK(NOW(), 1) 
                // WHEN  vtiger_accountscf.cf_1281 = 'Du kartus per mėn.'                                             
                //       THEN (MONTH(vtiger_crmentity.createdtime) = MONTH(NOW()) 
                //       AND (DAYOFMONTH(NOW()) >= 15) AND DAYOFMONTH(vtiger_crmentity.createdtime) < 15) 
                //       OR  (DAYOFMONTH(NOW()) = '".date('t')."' AND DAYOFMONTH(vtiger_crmentity.createdtime) > 15)
                //       OR MONTH(NOW()) > MONTH(vtiger_crmentity.createdtime)
                // WHEN vtiger_accountscf.cf_1281 = 'Už visą mėn.' THEN MONTH(vtiger_crmentity.createdtime) < MONTH(NOW())
                // ELSE YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW())    
                // END ";
                if(!empty($from) && !empty($to)){
                  if($filterBy == 'order_date'){
                    $query .= " AND DATE_FORMAT(vtiger_salesorder.import_date, '%Y-%m-%d') BETWEEN '".$from."' AND '".$to."' ";
                  }elseif($filterBy == 'load_date'){
                    $query .= " AND vtiger_salesorder.load_date_from BETWEEN '$from' AND '$to' ";
                  }elseif($filterBy == 'unload_date'){
                    $query .= " AND vtiger_salesorder.unload_date_to BETWEEN '$from' AND '$to' ";
                  }
                }
                if(!empty($orderDate)){
                  $query .= " AND  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') LIKE  '$orderDate%' ";
                }                                         
                if(!empty($orderNumber)){
                  $query .= " AND  vtiger_salesorder.shipment_code LIKE '%$orderNumber%' ";
                }
                if(!empty($customer)){
                  $query .= " AND  vtiger_account.accountname LIKE '$customer%' ";
                }
                if(!empty($pricebook)){
                  $query .= " AND  vtiger_salesordercf.cf_1297 LIKE '$pricebook%' ";
                }
                if(!empty($loadDate)){
                  $query .= " AND  vtiger_salesorder.load_date_from LIKE '$loadDate%' ";
                }
                if(!empty($loadAddress)){
                  $query .= " AND CONCAT(vtiger_sobillads.bill_street,' ', vtiger_sobillads.bill_city,' ',vtiger_sobillads.bill_code) LIKE '%$loadAddress%' ";
                }
                if(!empty($unloadDate)){
                  $query .= " AND  vtiger_salesorder.unload_date_from LIKE '".$unloadDate."%' ";
                }
                if(!empty($unloadAddress)){  
                  $query .= " AND CONCAT(vtiger_soshipads.ship_street,' ', vtiger_soshipads.ship_city,' ',vtiger_soshipads.ship_code) LIKE '%$unloadAddress%' ";
                }

                if(!empty($manager)){
                  $query .= " AND  vtiger_users.user_name LIKE '$manager%' OR vtiger_users.first_name LIKE'$manager%' OR vtiger_users.last_name  LIKE '$manager%' OR CONCAT(vtiger_users.first_name,vtiger_users.last_name) LIKE '%$manager%'";
                 }
                //  $query .= " ELSE '' END";
                  $query .= " ORDER BY vtiger_salesorder.salesorderid DESC LIMIT 500";
      
      $order_record = mysqli_fetch_array($conn->query($query));

      $orders = $conn->query($query);

      $records = array();
      while($row = $orders->fetch_assoc()) {
        $records[] = $row;
      }

      

      $order = array(
        'salesorderid' => $order_record["salesorderid"],
        'payment_deferral' => $order_record["payment_deferral"],
        'accountid' => $order_record["accountid"],
        'accountname' => $order_record["accountname"],  
        'productid' => $order_record["productid"],  
        'records' => $records,
        'surcharge' => $surcharge,
        'one' => false
      );


      // echo "<pre>";
      // print_R($order);
      echo json_encode($order);


    
      
    
