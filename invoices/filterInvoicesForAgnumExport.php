<?php

require_once "../config.inc.php";
// error_reporting(1);

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $data = (array)$_POST['form_array'];
  $data = call_user_func_array('array_merge', $data);

  $keys = array();
  $values = array();

  foreach($data as $key => $row){
    if(!is_numeric($key))  $keys[] = $key;
    if($row != '') $values[] = $row;
  }


  $showExported = $_POST['showExported'];

  $first = date("Y-m-d", strtotime("first day of this month"));
  $last = date("Y-m-d", strtotime("last day of this month"));	

  $query_string = "SELECT vtiger_invoice.accountid,vtiger_account.accountname as account_id, vtiger_invoice.invoicedate, vtiger_invoice.invoice_no, ROUND(vtiger_invoice.total,2) AS hdnGrandTotal, vtiger_invoice.duedate,  vtiger_crmentity.createdtime, vtiger_invoice.invoiceid, CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS created_user, CONCAT(debts.first_name,' ',debts.last_name) AS debts_owner, CONCAT(orders.first_name,' ',orders.last_name) AS orders_owner,
  IF(vtiger_invoice_exported_agnum.invoiceid IS NOT NULL, 1,0) AS exported
                    FROM vtiger_invoice  
                    INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid 
                    LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid 
                    LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid = vtiger_invoice.accountid 
                    LEFT JOIN vtiger_users ON vtiger_crmentity.smownerid = vtiger_users.id 
                    LEFT JOIN vtiger_users debts ON vtiger_accountscf.cf_1657 = debts.id 
                    LEFT JOIN vtiger_users orders ON vtiger_accountscf.cf_1659 = orders.id 
                    LEFT JOIN vtiger_invoice_exported_agnum ON vtiger_invoice_exported_agnum.invoiceid=vtiger_invoice.invoiceid 
                    WHERE vtiger_crmentity.deleted=0 AND vtiger_invoice.invoiceid > 0 ";

                  if($showExported == 'false'){
                     $query_string .= " AND vtiger_invoice_exported_agnum.invoiceid IS NULL ";
                  }
                  $query_string .= " GROUP BY vtiger_invoice.invoiceid ";	
                      

     
  if(!empty($data)){
    $query_string .= " HAVING ";

    for($i=0; $i<COUNT($keys); $i++){
      if($keys[$i] == 'invoicedate' || $keys[$i] == 'duedate' || $keys[$i] == 'createdtime'){
        $period = explode(",",$values[$i]);
        $query_string .= " DATE_FORMAT($keys[$i], '%Y-%m-%d') BETWEEN '$period[0]' AND '$period[1]' ".($i+1 < COUNT($keys) ? 'AND ' :'');
      }else{
        $search_text = trim($values[$i]);
        $query_string .=  "$keys[$i] LIKE '$search_text%' ".($i+1 < COUNT($keys) ? 'AND ' :'');
      }
    }
  }

  $query_string .= " ORDER BY vtiger_invoice.invoiceid DESC";

  $query = $conn->query($query_string);


  $result = array();
  while($row = $query->fetch_assoc()) {
    $result[] = $row;      
  }  

  echo json_encode($result);


}else{
  http_response_code(404);
}