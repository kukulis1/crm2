<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
    
    'Invoice'   =>  'Invoices',
	//DetailView Actions
	'SINGLE_Invoice' => 'Invoice',
	'LBL_EXPORT_TO_PDF' => 'Print',
    'LBL_SEND_MAIL_PDF' => 'Send Email with PDF',

	//Basic strings
	'LBL_ADD_RECORD' => 'Add Invoice',
	'LBL_RECORDS_LIST' => 'Invoice List',

	// Blocks
	'LBL_INVOICE_INFORMATION' => 'Invoice Details',

	//Field labels
	'Sales Order' => 'Sales Order',
	'Customer No' => 'Customer No',
	'Invoice Date' => 'Invoice Date',
	'Purchase Order' => 'Purchase Order',
	'Sales Commission' => 'Sales Commission',
	'Invoice No' => 'Invoice No',
	'LBL_RECEIVED' => 'Received',
	'LBL_BALANCE' => 'Balance',
	//Added for existing Picklist Entries

	'Sent'=>'Sent',
	'Credit Invoice'=>'Credit Invoice',
	'Paid'=>'Paid',
	'AutoCreated'=>'AutoCreated',
	'Cancel' => 'Cancel',
	
	//Translation for product not found
	'LBL_THIS' => 'This',
	'LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM' => 'is deleted from the system.please remove or replace this item',
	'LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM' => 'This line item is deleted from the system,please remove this line items',
	'LBL_NOT_A_BUNDLE' => 'Not a Bundle',
	'LBL_SUB_PRODUCTS'	=> 'Sub Products',
	'LBL_ACTION'	=> 'Action',


	// itoma saskaitos vertimas
	'LBL_INVOICE_DATE' => 'Išrašymo data/Invoice date',
	'LBL_INVOICE_TITLE' => 'PVM SĄSKAITA FAKTŪRA/INVOICE',
	'LBL_PRE_INVOICE_TITLE' => 'IŠANKSTINĖ PVM SĄSKAITA FAKTŪRA/PRE-INVOICE',
	'LBL_CREDIT_INVOICE_TITLE' => 'KREDITINĖ PVM SĄSKAITA FAKTŪRA/CREDIT INVOICE',
	'LBL_PAGE' => 'Page',
	'LBL_NO' => 'No.',
	'LBL_CUSTOMER_NAME' => 'Užsakovas/Customer',
	'LBL_VAT_CODE' => 'PVM kodas/VAT code',
	'LBL_BILL_ADDRESS' => 'Adresas/Address',
	'LBL_LEGAL_ENTITY_CODE' => 'Jur. asm. kodas/Company code',
	'LBL_PROVIDER' => 'Paslaugų tiekėjas/Service provider',
	'LBL_BANK' => 'Bankas/Bank',
	'LBL_PHONE' => 'Telefonas/Phone',

	'LBL_ITEM_SERVICE' => 'Prekė/Paslauga/Item/Service',
	'LBL_ORDER' => 'Užsakymas/Order',
	'LBL_DATE' => 'Data/Date /',
	'LBL_LOADING' => 'Pakrovimas/Loading',
	'LBL_UNLOAD' => 'Iškrovimas/Un-loading',
	'LBL_TOTAL' => 'Kaina be PVM/Sum',
	'LBL_MEASURE' => 'Measure',
	'LBL_MEASURE2' => 'Pak.',
	'LBL_QTY' => 'Pak./Quantity',
	'LBL_WEIGHT' => 'Kg',
	'LBL_VOLUME' => 'm3',
	'LBL_CONTINUED_ON_NEXT_PAGE' => 'Continued on next page',
	'LBL_NOTE' => 'Note',
	'LBL_TOTAL_WITH_VAT' => 'Viso su PVM/Grand total',
	'LBL_TOTAL_WITHOUT_VAT' => 'Viso be PVM/Sum total',
	'LBL_VAT' => 'VAT',
	'LBL_PAY_UNTILL' => 'Apmokėti iki/ Invoice to be paid until',
	'LBL_INVOICE_WRITE' => 'Sąskaita išrašė/Invoice issued by',
	'LBL_AMOUNT_IN_WORDS' => 'Amount in words',
	'LBL_TOTAL_FOR' => 'Total for',
	'LBL_FOR_SERVICES' => 'services',


	'LBL_WARNING' => 'Attention',
	'LBL_TRANSFER_REQUIREMENTS' => 'In the transfer order, please indicate the number of the invoice for which you are paying',
	'LBL_THANKS_FOR_USE_OUR_SERVICES' => 'Thank you for using our services',


);

$jsLanguageStrings = array(
	'JS_PLEASE_REMOVE_LINE_ITEM_THAT_IS_DELETED' => 'Please remove line item that is deleted',
);
