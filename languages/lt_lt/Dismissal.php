<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Dismissal' => 'Atleidimas',
	'SINGLE_Dismissal' => 'atleidima',
	'ModuleName ID' => 'Atleidimo ID',
	
	'LBL_ADD_RECORD' => 'Pridėti atleidima',
	'LBL_RECORDS_LIST' => 'Atleistu sąrašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',
	'Dismissal No' => 'Atleidimo numeris',
	'Dismissal date' => 'Atleidimo data',
	'Article' => 'Straipsnis',

	

);
	
?>
