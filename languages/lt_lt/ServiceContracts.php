<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */
$languageStrings = array(
	// Basic Strings
	'ServiceContracts'=>'Paslaugų kontraktai',
	'Service Contracts'=>'Paslaugų kontraktai',
	'SINGLE_ServiceContracts'=>'Paslaugos kontraktas',
	'LBL_ADD_RECORD'=>'Pridėti paslaugų kontraktą',
	'LBL_RECORDS_LIST'=>'Paslaugų kontraktų sąrašas',
	// Blocks
	'LBL_SERVICE_CONTRACT_INFORMATION'=>'Paslaugos kontrakto išsami informacija',
	
	//Field Labels
	'Contract No'=>'Kontrakto numeris',
	'Start Date'=>'Pradžios data',
	'End Date'=>'Pabaigos data',
	'Tracking Unit'=>'Sekimo vienetas',
	'Total Units'=>'Iš viso vienetų',
	'Used Units'=>'Panaudota vienetų',
	'Progress'=>'Progresas',
	'Planned Duration'=>'Planuojama trukmė (dienomis)',
	'Actual Duration'=>'Tiksli trukmė (dienomis)',
);
