<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'SINGLE_Project'=>'Projektas',
	'LBL_ADD_RECORD'=>'Pridėti projektą',
	'LBL_RECORDS_LIST'=>'Projektų sąrašas',

	// Blocks
	'LBL_PROJECT_INFORMATION'=>'Projekto išsami informacija',

	//Field Labels
	'Project Name'=>'Projekto pavadinimas',
	'Start Date'=>'Pradžios data',
	'Target End Date'=>'Tikslo pabaigos data',
	'Actual End Date'=>'Tiksli pabaigos data',
	'Project No'=>'Projekto numeris',
	'Target Budget'=>'Siekiamas biudžetas',
	'Project Url'=>'Projekto URL',
	'Progress'=>'Progresas',

	//Summary Information
	'LBL_TASKS_OPEN'=>'Atviros užduotys',
	'LBL_TASKS_DUE' => 'Užduoties terminas',
	'LBL_TASKS_COMPLETED'=>'Įvykdytos užduotys',
	'LBL_PEOPLE'=>'Žmonės',

	//Related List
	'LBL_CHARTS'=>'Diagramos',
	'LBL_TASKS_LIST'=>'Užduočių sąrašas',
	'LBL_MILESTONES'=>'Svarbiausi įvykiai',
	'LBL_TASKS'=>'Užduotys',
	'LBL_STATUS_IS'=>'Būsena yra',
	'LBL_STATUS'=>'Būsena',
	'LBL_TICKET_PRIORITY'=>'Prioritetas',
	'LBL_MORE'=>'Daugiau...',
	
	//Summary View Widgets
	'LBL_DOWNLOAD_FILE'=>'Parsisiųsti failą',
);
