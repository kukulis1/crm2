<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Accounts'=>'Klientai',
	'SINGLE_Accounts'=>'organizaciją',
	'LBL_ADD_RECORD'=>'Pridėti klientą',
	'LBL_RECORDS_LIST'=>'Klientų sąrašas',

	// Blocks
	'LBL_ACCOUNT_INFORMATION'=>'Technine informacija',

	// Mass Action
	'LBL_SHOW_ACCOUNT_HIERARCHY'=>'Rodyti klientų hierarchiją',

	//Field Labels
	'industry'=>'Užklausos gavimo būdas',
	'Account Name'=>'Kliento pavadinimas',
	'Account No'=>'Kliento nr.',
	'Website'=>'Svetainė',
	'Ticker Symbol'=>'Kliento tipas',
	'Member Of'=>'Narystė',
	'Employees'=>'Darbuotojai',
	'Ownership'=>'Savininkystė',
	'SIC Code'=>'SIC kodas',
	'Other Email'=>'El.paštas sąskaitoms (El. paštus atskirti kableliu)',
	'Other Phone'=>'Kitas telefonas',
	'Phone'=>'Telefonas',
	'Email'=>'Pradinis el. pašto adresas',
	'Black list'=>'Juodasis sąrašas',

	
	//Added for existing picklist entries
	
	'Analyst'=>'Analitikas',
	'Competitor' => 'Varžovas',
	'Customer'=>'Klientas',
	'Integrator'=>'Integratorius',
	'Investor'=>'Investuotojas',
	'Press'=>'Spauda',
	'Prospect'=>'Prognozė',
	'Reseller'=>'Perpardavėjas',
	'LBL_START_DATE'=>'Pradžios data:',
	'LBL_END_DATE'=>'Pabaigos data',
	
	//Duplication error message
	'LBL_DUPLICATES_EXIST'=>'Toks kliento pavadinimas jau egzistuoja',
	'LBL_COPY_SHIPPING_ADDRESS'=>'Kopijuoti pristatymo adresą',
	'LBL_COPY_BILLING_ADDRESS'=>'Kopijuoti sąskaitų siuntimo adresą',
    
	'Type'=>'Subjektas',
	'Legal Entity Code'=>'Įmonės kodas',    
	'Legal VAT Code'=>'PVM mokėtojo kodas',  
	'Contact'=>'Kontaktinis asmuo', 
	'Pricebook'=>'Kainynas',  
	'Debt'=>'Skola',
	'Title'=>'Pareigos',
	'Bill Address'=>'Adresas', 
	'Municipality'=>'Miestas',  
	'Post Code'=>'Pašto kodas',
	'Assigned To'=>'Klientas priskirtas',
	'Is Converted From Lead' => 'Sukurtas iš galimų klientų',


	// itoma prideta
	'Standing sales orders' => 'Nuolatiniai pardavimo užsakymai',
	'Enabled' => 'Įjungta',
	'LBL_PRICEBOOK_PRICE' => 'Kainyno kaina',
	'LBL_AGREED_PRICE' => 'Sutarta kaina',
);

$jsLanguageStrings = array(
	'LBL_HAS_PRICEBOOK' => 'Negalima ištrinti kliento, nes jis turi priskirtą kainyną',
	'LBL_RELATED_RECORD_DELETE_CONFIRMATION'=>'Ar tikrai norite pašalinti?',
	'LBL_DELETE_CONFIRMATION'=>'Jeigi pašalinsite ši klientą, taip pat bus pašalintos ir susijusios galimybės ir pasiūlymai. Ar tikrai norite tęsti?',
	'LBL_MASS_DELETE_CONFIRMATION'=>'Jeigi pašalinsite ši klientą, taip pat bus pašalintos ir susijusios galimybės ir pasiūlymai. Ar tikrai norite tęsti?',
	'JS_DUPLICTAE_CREATION_CONFIRMATION'=>'Toks kliento pavadinimas jau egzistuoja. Ar norite sukurti dar vieną tokį pat įrašą?'
);
