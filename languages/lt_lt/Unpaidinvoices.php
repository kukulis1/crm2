<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Unpaidinvoices' => 'Neapmokėtos saskaitos',
	'UnpaidInvoices' => 'Neapmokėtos saskaitos',

	'SINGLE_Unpaidinvoices' => 'Neapmokėta saskaita',
	'ModuleName ID' => 'UnpaidInvoices ID',
	
	'LBL_ADD_RECORD' => 'Pridėti neapmokėta saskaita',
	'LBL_RECORDS_LIST' => 'Neapmokėtų saskaitų sarašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',
	'LBL_COMMENT' => 'Rašyti komentarą'
	

);
	
?>
