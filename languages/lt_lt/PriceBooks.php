<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'PriceBooks'=>'Kainoraščiai old',
	'SINGLE_PriceBooks'=>'Kainoraštis',
	'LBL_ADD_RECORD'=>'Pridėti kainoraštį',
	'LBL_RECORDS_LIST'=>'Kainoraščių sąrašas',

	// Blocks
	'LBL_PRICEBOOK_INFORMATION'=>'Kainoraščio informacija',
	'LBL_EDIT_LIST_PRICE' => 'Redaguoti kainų sąrašą',

	//Field Labels
	'Price Book Name'=>'Kainoraščio pavadinimas',
	'PriceBook No'=>'Kainoraščio nr.',
);
