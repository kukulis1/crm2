<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Purchasevise' => 'Pirkimų vizavimas',
	'PurchaseVise' => 'Pirkimų vizavimas',
	'SINGLE_Purchasevise' => 'vizavimą',
	'ModuleName ID' => 'PurchaseOrderVise ID',
	'Vise' => 'Vizuoti',
	'Purchasevise No' => 'Saskaitos numeris', 
	
	'LBL_ADD_RECORD' => 'Add PurchaseOrderVise',
	'LBL_RECORDS_LIST' => 'PurchaseOrderVise List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',
	'Assigned To'=>'Kas vizavo',
	'LBL_QUICK_CREATE'=>'Pridėti',
	'LBL_SAVE'=>'Vizuoti',
	'Created Time'=>'Kada vizuota',
	'Sukūrė' => 'Kas sukūrė',
	

);
	
?>
