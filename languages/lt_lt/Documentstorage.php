<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Documentstorage' => 'Dokumentų saugykla',
	'DocumentStorage' => 'Dokumentų saugykla',
	'SINGLE_Documentstorage' => 'dokumentą',
	'ModuleName ID' => 'DocumentStorage ID',
	'Document no' => 'Dokumento numeris',
	
	'LBL_ADD_RECORD' => 'Pridėti dokumentą',
	'LBL_RECORDS_LIST' => 'Dokumentų sąrasas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Title' => 'Pavadinimas',
	'Folder' => 'Aplankas',
	'SubFolder' => 'Sub-aplankas',
	'Size' => 'Dydis',
	'Extension' => 'Plėtinys',
	'File type' => 'Failo tipas',
	'SIMPLE_DOCUMENT' => 'Paprastas dokumentas',

	'LBL_FOLDERS' => 'Aplankai',
	'LBL_CREATE_FOLDER' => 'Sukurti aplanką',
	'LBL_CREATING_NEW' => 'Kuriamas naujas įrašas',
	'LBL_PUBLIC' => 'Viešas',
	'LBL_PRIVATE' => 'Privatus',
	'LBL_SEARCH_FOR_LIST' => 'Ieškoti aplanko',
	'LBL_NOT_FOUND' => 'Nieko nerasta',
	'LBL_EMPTY' => 'Failų nėra',
	'LBL_EMPTY_SUBFOLDERS' => 'Sub-aplankų nėra',
	'LBL_EDITING' => 'Redagavimas'
	

	

);
	
?>
