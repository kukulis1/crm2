<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'LBL_VTIGER_MIGRATION'=>'Sveiki, čia Vtiger 6 miracijos puslapis',
	'LBL_MIGRATION_COMPLETED'=>'Perkėlimas baigtas',
	'LBL_MIGRATION_WIZARD'=>'Perkėlimo vedlys',
	'LBL_PRIDE_BEING_ASSOCIATED'=>'Didžiuojamės bendradarbiaudami su Jumis.',
	'LBL_TALK_TO_US_AT_FORUMS'=>'Turite klausimų? Užsukite į Vtiger svetainės <a href="http://forums.vtiger.com" target="_blank"> Diskusijas </a><br>',
	'LBL_DISCUSS_WITH_US_AT_BLOGS'=>'Norėdami sužinoti daugiau apie Vtiger, sekite informaciją <a href="http://blogs.vtiger.com" target="_blank">Vtiger tinklaraštyje.</a>',
	'LBL_CRM_DOCUMENTATION'=>'Vtiger6 Help - <a href="http://wiki.vtiger.com/vtiger6" target="_blank">Documentacija</a> , <a href="http://www.youtube.com/vtiger" target="_blank">Vaizdinė medžiaga</a> <BR>',
	'LBL_THANKS'=>'Dėkojame, kad naudojatės klientų ryšių valdymo programa <b>Vtiger</b>',
	'LBL_WE_AIM_TO_BE_BEST'=>'Siekiame būti tiesiog geriausi!',
	'LBL_SPACE_FOR_YOU'=>'Prisijunk, čia yra vieta ir tau!',
	'LBL_MIGRATION_COMPLETED_SUCCESSFULLY' => 'Vtiger6 migracija pavyko sėkmingai',
	'LBL_RELEASE_NOTES'=>'Norėdami sužinoti šios programos versijos naujienas, skaitykite<a href="#" target="_blank">išleidimo pastabas</a> <br>',
	'LBL_WAIT'=>'Prašome palaukti',
	'LBL_INPROGRESS'=>'Vyksta perkėlimas',
	'LBL_DATABASE_CHANGE_LOG'=>'Perkėlimas: duomenų bazės žurnalas',
);
