<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Campaigns'=>'Kampanijos ',
	'SINGLE_Campaigns'=>'Kampanija',
	'LBL_ADD_RECORD'=>'Pridėti kampaniją',
	'LBL_RECORDS_LIST'=>'Kampanijų sąrašas',

	// Blocks
	'LBL_CAMPAIGN_INFORMATION'=>'Kampanijos informacija',
	'LBL_EXPECTATIONS_AND_ACTUALS'=>'Laukiama pabaigos data',
	
	//Field Labels
	'Campaign Name'=>'Kampanijos pavadinimas',
	'Campaign No'=>'Kampanijos nr.', 
	'Campaign Type'=>'Kampanijos tipas', 
	'Product'=>'Prekė',
	'Campaign Status'=>'Kampanijos būsena',
	'Num Sent'=>'Išsiųstų skaičius',
	'Sponsor'=>'Rėmėjas',
	'Target Audience'=>'Tikslinė publika',
	'TargetSize'=>'Tikslo dydis',
	'Expected Response'=>'Laukiamas atsakymas',
	'Expected Revenue'=>'Laukiamos pajamos',
	'Budget Cost'=>'Biudžeto išlaidos',
	'Actual Cost'=>'Tiksli kaina',
	'Expected Response Count'=>'Laukiamų atsakymų skaičius',
	'Expected Sales Count'=>'Laukiamų pardavimų skaičius',
	'Expected ROI'=>'Laukiamas ROI',
	'Actual Response Count'=>'Tikslus atsakymų skaičius',
	'Actual Sales Count'=>'Tikslus pardavimų skaičius',
	'Actual ROI'=>'Tikslus ROI rodiklis',
	
	//Added for existing Picklist Entries

	'Webinar'=>'Saityno seminaras',
	'Referral Program'=>'Kreipimosi programa',
	'Advertisement'=>'Reklama',
	'Banner Ads'=>'Reklamjuostė',
	'Direct Mail'=>'Tiesioginis laiškas',
	'Telemarketing'=>'Telemaketingas',
	'Others'=>'Kiti ',

	'Planning'=>'Planuojama',						      	    
	'Inactive'=>'Neaktyvus',
	'Complete'=>'Užbaigti',
	'Cancelled'=>'Atsisakyta',							      

	'Excellent'=>'Geros kokybės',
	'Good'=>'Geras',
	'Average'=>'Vidurkis',
	'Poor' => 'Silpnas',
	
	// status fields 
	'--None--'=>'N',
	'Contacted - Successful'=>'Susisiekta - sėkmingai',
	'Contacted - Unsuccessful'=>'Susisiekta - nesėkmingai',
	'Contacted - Never Contact Again'=>'Susisiekta - daugiau nebesusisiekti',
);
