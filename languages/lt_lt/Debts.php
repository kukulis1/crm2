<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Debts' => 'Skolos',
	'SINGLE_Debts' => 'mokėjimą',
	'ModuleName ID' => 'Id',
	'Debts No' => 'Nr.',
	'LBL_EDITING' => 'Mokėjimo redagavimas',
	
	'LBL_ADD_RECORD' => 'Pridėti mokėjimą',
	'LBL_RECORDS_LIST' => 'Skolų sąrašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Kita informacija',
	'LBL_MODULEBLOCK_INFORMATION' => 'Informacija',
	'ModuleFieldLabel' => 'ModuleFieldLabel Text',
);
	
?>
