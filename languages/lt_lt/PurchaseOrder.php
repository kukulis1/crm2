<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	//DetailView Actions
	'SINGLE_PurchaseOrder'=>'Pirkimo užsakymai',
	'LBL_EXPORT_TO_PDF'=>'Eksportuoti kaip PDF',
    'LBL_SEND_MAIL_PDF'=>'Siųsti laišką su PDF',

	//Basic strings
	'LBL_ADD_RECORD'=>'Pridėti pirkimo užsakymą',
	'LBL_RECORDS_LIST'=>'Pirkimo užsakymų sąrašas',
	'LBL_COPY_SHIPPING_ADDRESS'=>'Kopijuoti pristatymo adresą',
	'LBL_COPY_BILLING_ADDRESS'=>'Kopijuoti sąskaitų siuntimo adresą',
	'LBL_CHARGES' => 'PVM mokestis',
	'Paid' => 'Sumokėta',
	'Assigned To'=>'Išrašė',

	// Blocks
	'LBL_PO_INFORMATION'=>'Pirkimo užsakymo informacija',

	//Field Labels
	'PurchaseOrder No'=>'Pirkimo užsakymo nr.',
	'Requisition No'=>'Paraiškos numeris',
	'Tracking Number'=>'Sekimo numeris',
	'Sales Commission'=>'Pardavimų komisiniai',
    'LBL_PAID'=>'Apmokėta',
    'LBL_BALANCE'=>'Likutis',
    'Balance'=>'Likutis',

	//Added for existing Picklist Entries

	'Received Shipment'=>'Gauta siunta',
	
	//Translation for product not found
	'LBL_THIS'=>'Šis',
	'LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM'=>'yra pašalintas iš sistemos. Pašalinkite šį elementą ar pakeiskite kitu',
	'LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM' => 'Šis elementas buvo pašalintas iš sistemos. Prašome pašalinti jo nuorodą.',

);

$jsLanguageStrings = array(
	'JS_PLEASE_REMOVE_LINE_ITEM_THAT_IS_DELETED'=>'Prašome pašalinti nuorodą į nebeegzistuojantį elementą',
);
