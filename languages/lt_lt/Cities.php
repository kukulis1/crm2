<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Cities' => 'Miestai',
	'SINGLE_Cities' => 'Miestai',
	'ModuleName ID' => 'Miesto ID',
	
	'LBL_ADD_RECORD' => 'Pridėti miestą',
	'LBL_RECORDS_LIST' => 'Miestu sarašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Kita informacija',
	'LBL_MODULEBLOCK_INFORMATION' => 'Modulio bloko informacija',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
