<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Clientsemails' => 'Klientų žinutės',
	'ClientsEmails' => 'Klientų žinutės',
	'SINGLE_Clientsemails' => 'Klientų žinutės',
	'ModuleName ID' => 'Klientų žinutės ID',
	
	'LBL_ADD_RECORD' => 'Add Klientų žinutės',
	'LBL_RECORDS_LIST' => 'Klientų žinutės List',

	'Sender name' => 'Siuntėjas',
	'Email' => 'El. pašto adresas',
	'Review' => 'Peržiūrėjo',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
