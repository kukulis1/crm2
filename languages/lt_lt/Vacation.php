<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Vacation' => 'Atostogos',
	'SINGLE_Vacation' => 'atostogas',
	'ModuleName ID' => 'ID',
	'Vacation No' => 'Numeris',
	
	'LBL_ADD_RECORD' => 'Pridėti atostogas',
	'LBL_RECORDS_LIST' => 'Vacation List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Vacation type' => 'Atostogų tipas',
	'Order no' => 'Įsakymo nr.',
	'Date of application' => 'Prašymo data',
	'From' => 'Nuo',
	'To' => 'Iki',
	'Note' => 'Pastabos',
	'Annual' => 'Kasmetinės',
	'Annual 1 w.d.' => 'Kasmetinės 1 d.d.',
	'Unpaid vacation' => 'Neapmokamos',
	'Unpaid vacation 1 w.d.' => 'Neapmokamos 1 d.d.',
	'Additional day off' => 'Papildoma poilsio diena',
	'Parental leave' => 'Tėvystės atostogos',
	'Assigned To'=>'Pasirašęs asmuo',

	

);
	
?>
