<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Assets'=>'Turtas',
	'SINGLE_Assets'=>'Turtas',
	'LBL_ADD_RECORD'=>'Pridėti turtą',
	'LBL_RECORDS_LIST'=>'Turto sąrašas',

	// Blocks
	'LBL_ASSET_INFORMATION'=>'Turto išsami informacija',

	//Field Labels
    'Asset No'=>'Turto nr',
	'Serial Number'=>'Priekabos numeris',
	'Date Sold'=>'Pardavimo data',
	'Date in Service'=>'Data',
	'Tag Number'=>'Žymės numeris',
	'Invoice Name'=>'Važtaraščio pavadinimas',
	'Shipping Method'=>'Pristatymo būdas',
	'Shipping Tracking Number'=>'Pristatymo sekimo numeris',
	'Asset Name'=>'Automobilio numeris',
	'Customer Name'=>'Kliento pavadinimas',
	'Notes'=>'Pastabos',

	/*picklist values*/
	'In Service' => 'Palaikoma',
	'Out-of-service'=>'Nebedirba',
);
