<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Products'=>'Prekės',
	'SINGLE_Products'=>'Prekė',
	'LBL_ADD_RECORD'=>'Pridėti prekę',
	'LBL_RECORDS_LIST'=>'Prekių sąrašas',

	// Blocks
	'LBL_PRODUCT_INFORMATION'=>'Prekės informacija',
	'LBL_IMAGE_INFORMATION'=>'Prekės paveikslo informacija:',
	'LBL_STOCK_INFORMATION'=>'Sandelio informacija',
	
	'LBL_MORE_CURRENCIES'=>'daugiau valiutų', 
	'LBL_PRICES'=>'Prekių kainos',
	'LBL_PRICE'=>'Kaina',
	'LBL_RESET_PRICE'=>'Atstatyti kainą',
	'LBL_RESET'=>'Atsisakyti',
	'LBL_ADD_TO_PRICEBOOKS'=>'Įtrauktį į kainoraščius',

	//Field Labels
	'Product No'=>'Prekės nr.',
	'Part Number'=>'Dalies nr.',
	'Product Active'=>'Prekė aktyvi',
	'Manufacturer'=>'Gamintojas',
	'Product Category'=>'Prekės kategorija',
	'Website'=>'Svetainė',
	'Mfr PartNo'=>'Mfr PartNo',
	'Vendor PartNo'=>'Tiekėjo dalies nr.',
	'Usage Unit'=>'Naudojimosi vienetas',
	'Handler'=>'Tvarkytojas',
	'Reorder Level'=>'Pertvarkyti lygius',
	'Tax Class'=>'Mokesčių klasė',
	'Reorder Level'=>'Pertvarkyti lygius',
	'Vendor PartNo'=>'Tiekėjo dalies nr.',
	'Serial No'=>'Serijos nr.',
	'Qty In Stock'=>'Kiekis sandėlyje',
	'Product Sheet'=>'Prekės lapas',
	'Qty In Demand'=>'Užsakytų prekių kiekis',
	'Shortage'=>'Trūkumų sąrašas',
	'Product Image'=>'Prekės paveikslas',
	'Unit Price'=>'Vieneto kaina',
	'Commission Rate'=>'Komisinių kursas',
	'Qty/Unit'=>'Vnt.',
	
	//Added for existing picklist entries

	'--None--'=>'N',

	'Hardware'=>'Aparatinė įranga',
	'Software'=>'Programinė įranga',
	'CRM Applications'=>'Sistemos programos',

	'300-Sales-Software'=>'300-Pardavimai-Programinė įranga',
	'301-Sales-Hardware'=>'301-Pardavimai-Aparatinė įranga',
	'302-Rental-Income'=>'302-Nuoma-Pajamos',
	'303-Interest-Income'=>'303-Palūkanos-Pajamos',
	'304-Sales-Software-Support'=>'304-Pardavimai-Programinė įranga-Palaikymas',
	'305-Sales Other'=>'305-Pardavimai-Kita',
	'306-Internet Sales'=>'306-Pardavimai internetu',
	'307-Service-Hardware Labor'=>'307-Paslaugos-Aparatinės įrangos darbai',
	'308-Sales-Books'=>'308-Pardavimai-Knygos',

	'Box'=>'Pašto dėžutė',
	'Carton'=>'Kartonas',
	'Caton'=>'Kartonas',
	'Dozen'=>'Tuzinas',
	'Each'=>'Kiekvienas',
	'Hours'=>'Valandos',
	'Impressions'=>'Įspūdžiai',
	'Lb'=>'Žymė',
	'M'=>'M',
	'Pack'=>'Paketas',
	'Pages'=>'Puslapiai',
	'Pieces'=>'Dalys',
	'Reams'=>'Krūva',
	'Sheet'=>'Lapas',
	'Spiral Binder'=>'Spiralinis rišiklis',
	'Sq Ft' => 'Sq Ft',
	
	'LBL_ADD_TO_PRICEBOOKS'=>'Įtrauktį į kainoraščius',
	'LBL_CONVERSION_RATE'=>'Konvertavimo kursas',
	'LBL_NOT_A_BUNDLE'=>'Ne ryšulys',
);
