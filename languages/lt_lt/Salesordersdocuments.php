<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Salesordersdocuments' => 'Užsakymai be dokumentų',
	'SalesOrdersDocuments' => 'Užsakymai be dokumentų',
	'SINGLE_Salesordersdocuments' => 'SalesOrdersDocuments',
	'ModuleName ID' => 'SalesOrdersDocuments ID',
	
	'LBL_ADD_RECORD' => 'Add SalesOrdersDocuments',
	'LBL_RECORDS_LIST' => 'SalesOrdersDocuments List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
