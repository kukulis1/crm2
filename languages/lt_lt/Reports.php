<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'Reports'=>'Ataskaitos',
	'SINGLE_Reports'=>'Ataskaita',

	// Basic Strings
	'LBL_FOLDER_NOT_EMPTY'=>'Katalogas nėra tuščias',
	'LBL_MOVE_REPORT'=>'Perkelti ataskaitas',
	'LBL_CUSTOMIZE'=>'Individualizuoti',
	'LBL_REPORT_EXPORT_EXCEL'=>'Eksportuoti XLS formatu',
	'LBL_REPORT_PRINT'=>'Spausdinti',
	'LBL_STEP_1'=>'1 žingsnis',
	'LBL_STEP_2'=>'2 žingsnis',
	'LBL_STEP_3'=>'3 žingsnis',
	'LBL_REPORT_DETAILS'=>'Ataskaitos išsami informacija',
	'LBL_SELECT_COLUMNS'=>'Pasirinkite stulpelius',
	'LBL_FILTERS'=>'Filtrai',
	'LBL_FOLDERS'=>'Katalogai',
	'LBL_ADD_NEW_FOLDER'=>'Pridėti katalogą',
	'LBL_FOLDER_NAME'=>'Katalogo pavadinimas',
	'LBL_FOLDER_DESCRIPTION'=>'Katalogo aprašymas',
	'LBL_WRITE_YOUR_DESCRIPTION_HERE'=>'Įveskite aprašymą',
	'LBL_DUPLICATES_EXIST'=>'Toks ataskaitos pavadinimas jau egzistuoja',
	'LBL_FOLDERS_LIST'=>'Katalogų sąrašas',
	'LBL_DENIED_REPORTS'=>'Atmestos ataskaitos',
	'LBL_NO_OF_RECORDS'=>'Įrašų skaičius',
	//ListView Actions
	'LBL_ADD_RECORD'=>'Pridėti ataskaitą',
	'LBL_ADD_FOLDER'=>'Pridėti katalogą',
	'LBL_REPORT_DELETE_DENIED'=>'Neturite teisių pašalinti įrašą',

	//Folder Actions
	'LBL_FOLDER_NOT_EMPTY'=>'Katalogas nėra tuščias',
	'LBL_FOLDER_CAN_NOT_BE_DELETED'=>'Šio katalogo pašalinti negalima',

	//Mass Actions
	'LBL_REPORTS_LIST'=>'Ataskaitų sąrašas',

	//Step1 Strings
	'LBL_REPORT_NAME'=>'Ataskaitos pavadinimas',
	'LBL_REPORT_FOLDER'=>'Ataskaitų katalogas',
	'LBL_DESCRIPTION'=>'2 žingsnis: Pridėkite aprašymą',
	'PRIMARY_MODULE'=>'Pradinis modulis',
	'LBL_SELECT_RELATED_MODULES'=>'Pažymėkite susijusius modulius',
	'LBL_MAX'=>'Daugiausiai',
	'LBL_NEXT'=>'Toliau',
	'LBL_REPORTS'=>'Ataskaitų sąrašas',
	'LBL_SELECT_RELATED_MODULES'=>'Pažymėkite susijusius modulius',

	//Step2 Strings
	'LBL_GROUP_BY'=>'Grupuoti pagal',
	'LBL_SORT_ORDER'=>'Rūšiavimo tvarka',
	'LBL_ASCENDING'=>'Didėjimo tvarka',
	'LBL_DESCENDING'=>'Mažėjimo tvarka',
	'LBL_CALCULATIONS'=>'Skaičiavimai',
	'LBL_COLUMNS'=>'Stulpeliai',
	'LBL_SUM_VALUE'=>'Suma',
	'LBL_AVERAGE'=>'Vidurkis',
	'LBL_LOWEST_VALUE'=>'Žemiausia kaina',
	'LBL_HIGHEST_VALUE'=>'Aukščiausia reikšmė',

	//Step3 Strings
	'LBL_GENERATE_REPORT'=>'Sugeneruoti ataskaitą',

	//DetailView
	'LBL_SUM'=>'Suma',
	'LBL_AVG'=>'Vid ',
	'LBL_MAX'=>'Daugiausiai',
	'LBL_MIN' => 'Min',
	'LBL_FIELD_NAMES'=>'Lauko pavadinimas',
	'LBL_REPORT_CSV'=>'Eksportuoti CSV formatu',
	'LBL_VIEW_DETAILS'=>'Peržiūrėti išsamią infromaciją',
	'LBL_GENERATE_NOW'=>'Sugeneruoti dabar',

	//List View Headers
	'Report Name'=>'Ataskaitos pavadinimas',

	//Default Folders Names, Report Names and Description
	'Account and Contact Reports'=>'Organizacijų ir adresatų ataskaitos',
	'Lead Reports'=>'Galimų klientų ataskaita',
	'Potential Reports'=>'Galimybių ataskaitos',
	'Activity Reports'=>'Veiklos ataskaitos',
	'HelpDesk Reports'=>'Kortelių ataskaitos',
	'Product Reports'=>'Prekių ataskaitos',
	'Quote Reports'=>'Pasiūlymo ataskaitos',
	'PurchaseOrder Reports'=>'Pirkimo užsakymo ataskaitos',
	'SalesOrder Reports'=>'Pardavimo užsakymo ataskaitos', //Added for SO
	'Invoice Reports'=>'Važtaraščio ataskaita',
	'Campaign Reports'=>'Kampanijos ataskaitos', //Added for Campaigns
	'Contacts by Accounts'=>'Adresatai pagal organizacijas',
	'Contacts without Accounts'=>'Adresatai be organizacijų',
	'Contacts by Potentials'=>'Adresastai pagal galimybes',
	'Contacts related to Accounts'=>'Adresatai susiję su organizacijomis',
	'Contacts not related to Accounts'=>'Adresatai nesusiję su organizacijomis',
	'Contacts related to Potentials'=>'Adresatai susiję su galimybėmis',
	'Lead by Source'=>'Galimi klientai pagal šaltinį',
	'Lead Status Report'=>'Galimo kliento būsenos ataskaita',
	'Potential Pipeline'=>'Galimybių grandinė',
	'Closed Potentials'=>'Užbaigtos galimybės',
	'Potential that have Won'=>'Laimėtos galimybės',
	'Tickets by Products'=>'Kortelės pagal prekes',
	'Tickets by Priority'=>'Kortelės pagal pirmumą',
	'Open Tickets'=>'Atviros kortelės',
	'Tickets related to Products'=>'Kortelės susijusios su prekėmis',
	'Tickets that are Open'=>'Atviros kortelės',
	'Product Details'=>'Prekės išsami informacija',
	'Products by Contacts'=>'Prekės pagal adresatus',
	'Product Detailed Report'=>'Prekės išsami ataskaita',
	'Products related to Contacts'=>'Prekės susijusios su adresatais',
	'Open Quotes'=>'Atviri pasiūlymai',
	'Quotes Detailed Report'=>'Pasiūlymo išsami informacija',
	'Quotes that are Open'=>'Atviri pasiūlymai',
	'PurchaseOrder by Contacts'=>'Pirkimo užsakymai pagal adresatus',
	'PurchaseOrder Detailed Report'=>'Pirkimo užsakymo išsami ataskaita',
	'PurchaseOrder related to Contacts'=>'Pirkimo užsakymai susiję su adresatais',
	'Invoice Detailed Report'=>'Važtaraščio išsami ataskaita',
	'Last Month Activities'=>'Praeito mėnesio veiklos',
	'This Month Activities'=>'Šio mėnesio veiklos',
	'Campaign Expectations and Actuals'=>'Kampanijos tikėtinas ir tikslus rezultatas ', //Added for Campaigns
	'SalesOrder Detailed Report'=>'Pardavimo užsakymo išsami informacija', //Added for SO

	'Email Reports'=>'Laiškų ataskaitos',
	'Contacts Email Report'=>'Adresatų laiškų ataskaita',
	'Accounts Email Report'=>'Organizacijos laiškų ataskaita',
	'Leads Email Report'=>'Galimų klientų laukų atvaizdavimas',
	'Vendors Email Report'=>'Tiekėjų laiškų ataskaita',

	'Emails sent to Contacts'=>'Laiškai išsiųsti adresatams',
	'Emails sent to Organizations'=>'Laiškai išsiųsti organizacijoms',
	'Emails sent to Leads'=>'Laiškai išsiųsti galimiems klientams',
	'Emails sent to Vendors'=>'Laiškai išsiųsti tiekėjams',

	'LBL_PRINT_REPORT'=>'Atpausdinti ataskaitą',
	'LBL_RECORDS'=>'Įrašai',
	'LBL_LIMIT_EXCEEDED'=>'Tik 1000 įrašų rodoma. Eksportuokite duomenis CSV arba EXCEL formatu, norėdami pamatyti visus.',
	'LBL_TOP'=>'Aukščiausi',
	'LBL_ALL_REPORTS'=>'Visos ataskaitos',
	'LBL_CALCULATION_CONVERSION_MESSAGE'=>'Skaičiavimai atlikti pagal jūsų klientų ryšių valdymo programos numatytąją valiutą.',
);
$jsLanguageStrings = array(
	'JS_DUPLICATE_RECORD'=>'Dubliuoti ataskaitą',
	'JS_CALCULATION_LINE_ITEM_FIELDS_SELECTION_LIMITATION'=>'Apribojimas: elemento laukai (kaina, nuolaida ur kiekis) gali būti naudojami tik kai jokie kiti skaičiavimo laukai nepasirinkti.',
);
