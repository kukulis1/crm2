<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Clientfuelrange' => 'Kuro priemokos rėžiai',
	'SINGLE_Clientfuelrange' => 'kuro priemoka',
	'ClientFuelRange' => 'Kuro priemokos rėžiai',
	'ModuleName ID' => 'ClientFuelRange ID',
	
	'LBL_ADD_RECORD' => 'Pridėti kuro priemokos rėžį',
	'LBL_RECORDS_LIST' => 'Kuro priemokos rėžių sarašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Kita informacija',
	'LBL_MODULEBLOCK_INFORMATION' => 'Module bloko informacija',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
