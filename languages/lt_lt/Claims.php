<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Claims' => 'Pretenzijos',
	'SINGLE_Claims' => 'Pretenzija',
	'ModuleName ID' => 'ID',

	'Claims No' => 'Pretenzijos numeris',
	
	'LBL_ADD_RECORD' => 'Pridėti pretenzija',
	'LBL_RECORDS_LIST' => 'Pretenzijų sąrašas',

	'Main' => 'Pagrindinis',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Claim' => 'Pretenzija',
	'claim_type' => 'Pretenzijos tipas',
	'order_number' => 'Užsakymo numeris',
	'client' => 'Siuntėjo vardas',
	'order_date' => 'Užsakymo data',
	'order_type' => 'Užsakymo tipas',
	'load_date' => 'Pasikrovimo data',
	'load_place' => 'Pasikrovimo vieta',
	'unload_date' => 'Pristatymo data',
	'unload_place' => 'Pristatymo, vieta',
	'cargo' => 'Krovinys',
	'trip' => 'Reisas',
	'driver' => 'Vairuotojas',
	'stevedore' => 'Krovėjas',
	'claim_amount' => 'Pretenzijos suma',
	'claim_review' => 'Pretenziją peržiūrėjo',
	

);
	
?>
