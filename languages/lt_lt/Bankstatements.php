<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Bankstatements' => 'Banko išrašai',
	'BankStatements' => 'Banko išrašai',
	'SINGLE_Bankstatements' => 'Banko išrašai',
	'ModuleName ID' => 'BankStatements ID',

	
	'Bank' => 'Bankas',
	'Iban' => 'Sąskaita',
	'PayerReceiver' => 'Mokėtojas/Gavėjas',
	'PayerReceiverIban' => 'Mokėtojo/Gavėjo sąskaita',
	'PayerReceiverBankName' => 'Mokėtojo/Gavėjo Bankas',
	'Date' => 'Data',
	'Amount' => 'Suma',
	'Price' => 'Suma',
	'Purpose' => 'Paskirtis',
	'TransactionNo' => 'Pavedimo Nr.',
	'LBL_LEGEND' => 'Legenda',
	
	'LBL_ADD_RECORD' => 'Add BankStatements',
	'LBL_RECORDS_LIST' => 'BankStatements List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
