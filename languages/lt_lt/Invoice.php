<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	//DetailView Actions
	'SINGLE_Invoice'=>'Sąskaita',
	'LBL_EXPORT_TO_PDF'=>'Spausdinti',
  'LBL_SEND_MAIL_PDF'=>'Siųsti sąskaitą klientui',
  'LBL_CONVERT_INVOICE_TO_DEBIT'=>'Generuoti debetinę sąskaitą',

	//Basic strings
	// 'LBL_ADD_RECORD'=>'Pridėti važtaraštį',
	'LBL_ADD_RECORD'=>'Išrašyti naują saskaitą',
	'LBL_RECORDS_LIST'=>'Sąskaitų sąrašas',

	// Blocks
	'LBL_INVOICE_INFORMATION'=>'Sąskaitos informacija',

	// itoma
	'LBL_CHARGES' => 'PVM mokestis',

	//Field labels
	'Sales Order'=>'Pardavimo užsakymas',
	'Customer No'=>'Kliento Nr.',
	'Invoice Date'=>'Sąskaitos data',
	'Purchase Order'=>'Pirkimo užsakymas',
	'Sales Commission'=>'Pardavimų komisiniai',
	'Invoice No'=>'Sąskaitos nr.',
	'LBL_RECEIVED'=>'Apmokėta',
	'LBL_BALANCE'=>'Skola',
	//Added for existing Picklist Entries

	'Sent'=>'Išsiųsta',
	'Credit Invoice'=>'Kredito sąskaita',
	'Paid'=>'Apmokėta',
	'AutoCreated'=>'Automatiškai sukurta',
	'Cancel'=>'Atsisakyti',
	'LBL_MEASURE' => 'Matas',
	
	'LBL_BILLING_ADDRESS_FROM'=>'Kopijuoti pakrovimo adresą iš',
	'LBL_SHIPPING_ADDRESS_FROM'=>'Kopijuoti iškrovimo adresą iš',
	'Billing Address'=>'Pakrovimo adresas',
	'Billing City'=>'Pakrovimo miestas',
	'Billing Code'=>'Pakrovimo pašto kodas',
	'Billing Country'=>'Pakrovimo šalis',
	'Billing Po Box'=>'Pakrovimo pašto dėžutė',
	'Billing State'=>'Pakrovimo savivaldybė', 
	'Load Company'=>'Pakrovimo įmonė',     
	'Shipping Address'=>'Iškrovimo adresas',
	'Shipping City'=>'Iškrovimo miestas',
	'Shipping State'=>'Iškrovimo savivaldybė',
	'Shipping Code'=>'Iškrovimo pašto kodas',
	'Shipping Country'=>'Iškrovimo šalis',
	'Shipping Po Box'=>'Iškrovimo pašto dėžutė',
	'Unload Company'=>'Iškrovimo įmonė',
    
	'Load Date From'=>'Pakrovimo data nuo',
	'Load Time From'=>'Pakrovimo laikas nuo',
	'Load Date To'=>'Pakrovimo data iki',
	'Load Time To'=>'Pakrovimo laikas iki',
	'Unload Date From'=>'Iškrovimo data nuo',
	'Unload Time From'=>'Iškrovimo laikas nuo',
	'Unload Date To'=>'Iškrovimo data iki',
	'Unload Time To'=>'Iškrovimo laikas iki',
	'late payment' => 'Pradelsta sumokėti',
	'untilduedate' => 'Liko iki mokėjimo',	
	'Assigned To'=> 'Sąskaitą išrašė', 

	// itoma saskaitos vertimas
	'LBL_INVOICE_DATE' => 'Išrašymo data',
	'LBL_INVOICE_TITLE' => 'PVM SĄSKAITA FAKTŪRA',
	'LBL_PRE_INVOICE_TITLE' => 'IŠANKSTINĖ PVM SĄSKAITA FAKTŪRA',
	'LBL_CREDIT_INVOICE_TITLE' => 'KREDITINĖ PVM SĄSKAITA FAKTŪRA',
	'LBL_PAGE' => 'Psl.',
	'LBL_NO' => 'Nr.',
	'LBL_CUSTOMER_NAME' => 'Užsakovas',
	'LBL_VAT_CODE' => 'PVM kodas',
	'LBL_BILL_ADDRESS' => 'Adresas',
	'LBL_LEGAL_ENTITY_CODE' => 'Jur. asm. kodas',
	'LBL_CUSTOMER_NAME' => 'Užsakovas',
	'LBL_PROVIDER' => 'Paslaugų tiekėjas',
	'LBL_BANK' => 'Bankas',
	'LBL_PHONE' => 'Telefonas',

	'LBL_ITEM_SERVICE' => 'Prekė/Paslauga',
	'LBL_ORDER' => 'Užsakymas',
	'LBL_DATE' => 'Data /',
	'LBL_LOADING' => 'Pakrovimas',
	'LBL_UNLOAD' => 'Iškrovimas',
	'LBL_TOTAL' => 'Kaina be PVM',
	'LBL_TOTAL_SUM' => 'Suma be PVM',
	'LBL_MEASURE' => 'Matas',
	'LBL_MEASURE2' => 'Pak.',
	'LBL_QTY' => 'Kiekis',
	'LBL_WEIGHT' => 'Kg',
	'LBL_VOLUME' => 'm3',
	'LBL_CONTINUED_ON_NEXT_PAGE' => 'Tęsinys kitame lape',
	'LBL_NOTE' => 'Pastaba',
	'LBL_TOTAL_WITH_VAT' => 'Viso su PVM',
	'LBL_TOTAL_WITHOUT_VAT' => 'Viso be PVM',
	'LBL_VAT' => 'PVM',
	'LBL_PAY_UNTILL' => 'Apmokėti iki',
	'LBL_INVOICE_WRITE' => 'Sąskaitą išrašė',
	'LBL_AMOUNT_IN_WORDS' => 'Suma žodžiais',
	'LBL_TOTAL_FOR' => 'Viso už',
	'LBL_FOR_SERVICES' => 'paslaugas',


	'LBL_WARNING' => 'Atkreipkite dėmesį',
	'LBL_TRANSFER_REQUIREMENTS' => 'Pavedime prašome nurodyti PVM sąskaitos-faktūros, už kurią mokate, numerį',
	'LBL_THANKS_FOR_USE_OUR_SERVICES' => 'Dėkojame, kad naudojatės mūsų paslaugomis',
	
	//Translation for product not found
	'LBL_THIS'=>'Šis',
	'LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM'=>'yra pašalintas iš sistemos. Pašalinkite šį elementą ar pakeiskite kitu',
	'LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM' => 'Šis elementas buvo pašalintas iš sistemos. Prašome pašalinti jo nuorodą.',
);

$jsLanguageStrings = array(
	'JS_PLEASE_REMOVE_LINE_ITEM_THAT_IS_DELETED'=>'Prašome pašalinti nuorodą į nebeegzistuojantį elementą',
);
