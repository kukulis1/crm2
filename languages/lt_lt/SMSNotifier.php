<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
    'SMSNotifier'=>'Informavimas SMS žinute',
	'LBL_SEND_SMS_TO_SELECTED_NUMBERS'=>'Siųsti žinutes visiems pažymėtiems',
	'LBL_STEP_1'=>'1 žingsnis',
	'LBL_STEP_2'=>'2 žingsnis',
	'LBL_SELECT_THE_PHONE_NUMBER_FIELDS_TO_SEND'=>'Norėdami siųsti, pasirinkite telefono numerio lauką',
	'LBL_TYPE_THE_MESSAGE'=>'Rašykite žinutę',
	'LBL_WRITE_YOUR_MESSAGE_HERE'=>'rašykite savo žinutę čia',
	'LBL_ADD_MORE_FIELDS'=>'Pridėti daugiau laukų',
	'LBL_SERVER_CONFIG'=>'Serverio konfigūravimas',

	//DetailView Actions
	'LBL_CHECK_STATUS'=>'Patikrinti būseną',
	'message'=>'Pranešimas',

	//Blocks
	'LBL_SMSNOTIFIER_INFORMATION' => 'SMS žinutės infromacija',
	'SINGLE_SMSNotifier'=>'Informavimas SMS žinute',
);
