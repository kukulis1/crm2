<?php

$languageStrings = array(
        'LBL_PERSONAL_DETAIL' => 'Asmeninės detalės',
        'SINGLE_Employee' => 'darbuotoją',
        'LBL_MODULE_NAME' => 'Darbuotojai',
        'LBL_ADD_RECORD' => 'Pridėti darbuotoją',
        'LBL_INSTALL' => 'Install',
        'LBL_CHANGE_COMPANY_INFORMATION' => 'Pakeisti įmonės informacija',
        'LBL_VALIDATE' => 'Aktyvi',
        'LBL_ORDER_NOW' => 'Užsakyti',
        'LBL_LICENSE' => 'Licenzijos nustatymai',
        'LBL_URL' => 'Jūsų vtiger crm adresas',
        'LBL_LICENSE_KEY' => 'Licenzijos numeris',
        'LBL_COMPANY_LICENSE_INFO' => 'Jūsų įmonės informacija',
        'LBL_REACTIVATE' => 'Re-aktyvuoti licenziją',
        'LBL_DEACTIVATE' => 'De-actyvuoti licenziją',
        "LBL_DEACTIVATE_QUESTION"=>"Ar tikrai norite deaktyvuoti licenziją?",
        'Employee' => 'Darbuotojai',
        'Address' => 'Gyvenamasis adresas',
        'Active Employee' => 'Aktyvus darbuotojai',
        'Inactive Employee' => 'Neaktyvus darbuotojai',
        'LBL_CREATE_USER' => 'Sukurti crm vartotoją',
        // Personal detail

        'Salary Department' => 'Atlyginimas',
        'Date of Birth' => 'Gimimo data',
        'Position' => 'Pareigos',
        'Start Work Date' => 'Darbo pradžios data',
        'Official Working' => 'Įdarbinimo data',
        'Employee Number' => 'Tabelio nr.',
        'Desired Salary' => 'Norimas atlyginimas',
        'Official Salary' => 'Oficialus atlyginimas',
        'Gender' => 'Lytis',
        'User ID' => 'Darbuotojo id',
        'Marital Status' => 'Šeimyninė padėtis',
        'Married' => 'Vedęs/Ištekėjusi',
        'Single' => 'Vienišas',
        'Probationary Period' => 'Bandomasis laikotarpis',
        'Private Email' => 'Asmeninis el. pašto adresas',
        'Created' => 'Sukurtas',

  
);