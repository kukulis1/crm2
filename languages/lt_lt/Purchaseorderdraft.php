<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Purchaseorderdraft' => 'Pirkimo saskaitų juodraštis',
	'PurchaseOrderDraft' => 'Pirkimo saskaitų juodraštis',
	'SINGLE_Purchaseorderdraft' => 'PurchaseOrderDraft',
	'ModuleName ID' => 'PurchaseOrderDraft ID',
	
	'LBL_ADD_RECORD' => 'Add PurchaseOrderDraft',
	'LBL_RECORDS_LIST' => 'PurchaseOrderDraft List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Vendor' => 'Tiekėjo pavadinimas',
	'Amount' => 'Suma',
	'Route date' => 'Reiso data',
	'registration_number' => 'Mašinos nr.',
	'Route_code' => 'Reisas',
	'Driver_name' => 'Vairuotojas',
	'Note' => 'Pastabos'

	

);
	
?>
