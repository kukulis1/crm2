<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Leads'=>'Galimi klientai',
	'SINGLE_Leads'=>'Galimas klientas',
	'LBL_RECORDS_LIST'=>'Galimų klientų sąrašas',
	'LBL_ADD_RECORD'=>'Pridėti galimą klientą',

	// Blocks
	'LBL_LEAD_INFORMATION'=>'Galimų klientų informacija',

	//Field Labels
	'LBL_SENDER_NAME' => 'Siuntėjas',
	'Lead No'=>'Galimo kliento nr.',
	'Company'=>'Įmonė',
	'Designation'=>'Pavadinimas',
	'Website'=>'Svetainė',
	'Industry'=>'Užklausos gavimo būdas',
	'Lead Status'=>'Būsena',
	'No Of Employees'=>'Darbuotojų skaičius',
	'Phone'=>'Telefonas',
	'Secondary Email'=>'Atsarginis el. paštas',
	'Email'=>'El. pašto adresas',
	'LBL_ADDRESS_INFORMATION'=>'Krovinio pakrovimo/iškrovimo adresai',
	//Added for Existing Picklist Entries

	'--None--' => '---',
	'Mr.'=>'Ponas',
	'Ms.'=>'Panelė',
	'Mrs.'=>'Ponia',
	'Dr.'=>'Dr.',
	'Prof.'=>'Prof.',

	//Lead Status Picklist values
	'Attempted to Contact'=>'Bandyta susisiekti',
	'Cold'=>'Neįpareigojantis',
	'Contact in Future'=>'Būsimas adresatas',
	'Contacted'=>'Susisiekta',
	'Hot'=>'Karštas',
	'Junk Lead'=>'Neaktualus galimas klientas',
	'Lost Lead'=>'Prarastas galimas klientas',
	'Not Contacted'=>'Nesusisiekta',
	'Pre Qualified'=>'Prieš patikslinimą',
	'Qualified'=>'Patikslinta',
	'Warm' => 'Šiltas',
	'State'=>'Rajonas',
	'load_company' => 'Siuntėjas',
	'unload_company' => 'Gavėjas',

	// Mass Action
	'LBL_CONVERT_LEAD'=>'Konvertuoti į klientą',

	//Convert Lead
	'LBL_TRANSFER_RELATED_RECORD'=>'Perkelti susijusius įrašus:',
	'LBL_CONVERT_LEAD_ERROR'=>'Norėdami pakeisti galimą klientą turite įjungti organizaciją arba adresatą.',
	'LBL_CONVERT_LEAD_ERROR_TITLE'=>'Moduliai išjungti',
	'CANNOT_CONVERT'=>'Negalima konvertuoti',
	'LBL_FOLLOWING_ARE_POSSIBLE_REASONS'=>'Į galimas priežastis įeina:',
	'LBL_LEADS_FIELD_MAPPING_INCOMPLETE'=>'Jokie privalomi laukai neatvaizduojami',
	'LBL_MANDATORY_FIELDS_ARE_EMPTY'=>'Kai kurie privalomieji laukai neužpildyti',
	'LBL_LEADS_FIELD_MAPPING'=>'Galimų klientų laukų atvaizdavimas nebaigtas ( Nuostatos>Modulių tvarkyklė>Galimi klientai>Galimų klientų laukų atvaizdavimas)',

	//Leads Custom Field Mapping
	'LBL_CUSTOM_FIELD_MAPPING'=>'Redaguoti laukų atvaizdavimą',
	'LBL_WEBFORMS'=>'Tinklaraščio formų nustatymai',
	'LBL_PRICEBOOK_PRICE' => 'Kainyno kaina',
	'LBL_AGREED_PRICE' => 'Sutarta kaina',
);
$jsLanguageStrings = array(
	'JS_SELECT_CONTACTS'=>'Norėdami tęsti pasirinkite adresatus',
	'JS_SELECT_ORGANIZATION'=>'Norėdami tęsti pažymėkite organizaciją',
	'JS_SELECT_ORGANIZATION_OR_CONTACT_TO_CONVERT_LEAD'=>'Konvertavimui reikia pažymėti adresatą ar Organizaciją'
);
