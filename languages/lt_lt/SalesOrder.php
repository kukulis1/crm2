<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	//DetailView Actions
	'SINGLE_SalesOrder'=>'Pardavimo užsakymas',
	'LBL_EXPORT_TO_PDF'=>'Eksportuoti kaip PDF',
    'LBL_SEND_MAIL_PDF'=>'Siųsti laišką su PDF',

	//Basic strings
	'LBL_ADD_RECORD'=>'Pridėti pardavimo užsakymą',
	'LBL_RECORDS_LIST'=>'Pardavimo užsakymų sąrašas',
	'LBL_PAY_GRAND_TOTAL' => 'Mokama kaina',

	// Blocks
	'LBL_SO_INFORMATION'=>'Pardavimo užsakymo informacija',

	//Field labels
	'SalesOrder No'=>'Pardavimo užsakymo Nr.',
	'Shipment Code'=>'Siuntos Nr.',
	'Quote Name'=>'Pasiūlymo pavadinimas',
	'Customer No'=>'Kliento Nr.',
	'Requisition No'=>'Paraiškos nr.',
	'Tracking Number'=>'Sekimo numeris',
	'Sales Commission'=>'Pardavimų komisiniai',
	'Purchase Order'=>'Pirkimo užsakymas',
	'Vendor Terms'=>'Tiekėjo terminai',
	'Pending'=>'Laukiama',
	'Enable Recurring'=>'Įjungti pasikartojamumą',
	'Frequency'=>'Dažnis',
	'Start Period'=>'Pradžios periodas',
	'End Period'=>'Pabaigos periodas',
	'Payment Duration'=>'Atsiskaitymo trukmė',
	'Invoice Status' => 'Važtaraščio būsena',
	'Organization' => 'Užsakovas',
	//Added for existing Picklist Entries

	'Sub Total'=>'Tarpinė suma',
	'AutoCreated'=>'Automatiškai sukurta',
	'Sent'=>'Išsiųsta',
	'Credit Invoice'=>'Kredito sąskaita',
	'Paid'=>'Apmokėta',
	
	//Translation for product not found
	'LBL_THIS'=>'Šis',
	'LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM'=>'yra pašalintas iš sistemos. Pašalinkite šį elementą ar pakeiskite kitu',
	'LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM'=>'Šis elementas buvo pašalintas iš sistemos. Prašome jį pšalinti iš sąrašų.',
    
	'LBL_BILLING_ADDRESS_FROM'=>'Kopijuoti pakrovimo adresą iš',
	'LBL_SHIPPING_ADDRESS_FROM'=>'Kopijuoti iškrovimo adresą iš',
	'Billing Address'=>'Pakrovimo adresas',
	'Billing City'=>'Pakrovimo miestas',
	'Billing Code'=>'Pakr. p/k',
	'Billing Country'=>'Pakrovimo šalis (LTU, LVA, EST, POL)',
	'Billing Po Box'=>'Pakrovimo pašto dėžutė',
	'Billing State'=>'Pakrovimo savivaldybė', 
	'Load Company'=>'Siuntėjas', 
	'Load Contact'=>'Siuntėjo kontaktas', 
	'Shipping Address'=>'Iškrovimo adresas',
	'Shipping City'=>'Iškrovimo miestas',
	'Shipping State'=>'Iškrovimo savivaldybė',
	'Shipping Code'=>'Iškr. p/k',
	'Shipping Country'=>'Iškrovimo šalis (LTU, LVA, EST, POL)',
	'Shipping Po Box'=>'Iškrovimo pašto dėžutė',
	'Unload Company'=>'Gavėjas',
	'Unload Contact'=>'Gavėjo kontaktas', 
    
	'Load Date From'=>'Pakrovimo data ir laikas nuo iki',
	'Load Time From'=>'Pakrovimo laikas nuo',
	'Load Date To'=>'Pakrovimo data iki',
	'Load Time To'=>'Pakrovimo laikas iki',
	'Unload Date From'=>'Iškrovimo data ir laikas nuo iki',
	'Unload Time From'=>'Iškrovimo laikas nuo',
	'Unload Date To'=>'Iškrovimo data iki',
	'Unload Time To'=>'Iškrovimo laikas iki',
    
	'Not Sent'=>'Neišsiųstas', 
	'Sent'=>'Naujas',    
	'Not Delivered'=>'Nepriduotas',  
	'Approved'=>'Priimtas',
	'calculated'=>'Persvertas',
	'delivered'=>'Atliktas',
	'in progress'=>'Vykdomas',
	'not started'=>'Nepradėtas',
	'Created'=>'Naujas',
	'iVAZ No'=>'Metrika iVAZ Nr.',  
	'Direction'=>'Metrika kryptis',  
	'Metrika Order ID'=>'Metrikos operacijos Nr.',  
	'Barcode'=>'Metrika barkodas',  
	'Metrika Status'=>'Metrika įterpimo būsena',  
	'Errors'=>'Metrika klaidos',    
	'Load Company Code'=>'Siuntėjo kodas',  
	'Unload Company Code'=>'Gavėjo kodas', 
	'Not Delivery Reason'=>'Nepridavimo priežastis',  
	'Feedback'=>'Įvertinimas', 
	'Folder Name'=>'Failo tipas',
	'LBL_FOLDER_NAME'=>'Failo tipas',


    
);

$jsLanguageStrings = array(
	'JS_PLEASE_REMOVE_LINE_ITEM_THAT_IS_DELETED'=>'Prašome pašalinti ',
);
