<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Routepoints' => 'Reisu taškai',
	'RoutePoints' => 'Reisu taškai',
	'SINGLE_Routepoints' => 'RoutePoints',
	'ModuleName ID' => 'RoutePoints ID',
	
	'LBL_ADD_RECORD' => 'Add RoutePoints',
	'LBL_RECORDS_LIST' => 'RoutePoints List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'customer_name' => 'Klientas',
	'point_seq_no' => 'Eil. Nr.',
	'remarks' => 'Pastabos',
	'kg_load' => 'Kg load',
	'm3_load' => 'M3 load',
	'pll_load' => 'Pll load',
	'kg_unload' => 'Kg unload',
	'm3_unload' => 'M3 unload',
	'pll_unload' => 'Pll unload',
	'price_agreed' => 'Kaina',
	'type' => 'Taško tipas',
	'location' => 'Reiso pradžios vieta',
	'start_time' => 'Pradžios data ir laikas',
	'end_time' => 'Pabaigos data ir laikas',

	

);
	
?>
