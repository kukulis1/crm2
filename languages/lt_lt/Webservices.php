<?php
/*********************************************************************************
 ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
  * ("License"); You may not use this file except in compliance with the License
  * The Original Code is:  vtiger CRM Open Source
  * The Initial Developer of the Original Code is vtiger.
  * Portions created by vtiger are Copyright (C) vtiger.
  * All Rights Reserved.
 *
  ********************************************************************************/

$languageStrings = array(
	'LBL_INVALID_OLD_PASSWORD' => 'Neteisingai įrašytas buvęs slaptažodis',
	'LBL_NEW_PASSWORD_MISMATCH'=>"New Password and confirm password don't match",
	'LBL_DATABASE_QUERY_ERROR'=>'Duomenų bazės klaida vykdant užklausą',
	'LBL_CHANGE_PASSWORD_FAILURE'=>'Nepavyko pakeisti slaptažodžio',
);
