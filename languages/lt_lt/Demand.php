<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Demand' => 'Poreikis',
	'SINGLE_Demand' => 'poreiki',
	'ModuleName ID' => 'poreikio ID',
	
	'LBL_ADD_RECORD' => 'Pridėti poreikį',
	'LBL_RECORDS_LIST' => 'Poreikiu sarašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',
	'Start date' => 'Turi pradėti dirbti',	
	'howmuch' => 'Kiek darbuotojų reikia',
	'position' => 'Pozicija',
	'city' => 'Miestas',
	'salary' => 'Atlyginimas',
	'find untill' => 'Nuo kada turi pradėti dirbti',
	'characteristics' => 'Charakteristika',
	'advertisement' => 'Skelbimo tekstas',
	'other' => 'Kita info',
	'Start data' => 'Darbo pradžia',
	'Search sources' => 'Paieškos šaltiniai, kaina',
	'Personal block' => 'Paieška',
	'Setting Position' => 'pareigos',

	

);
	
?>
