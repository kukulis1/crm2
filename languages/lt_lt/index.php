<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include 'ws.config.php';
if (!$config) {
    die('config file was not located');
}

// get action from URL
$action = '';
$version = 'v1';

if (!empty($_SERVER['REQUEST_URI'])) {
    $version = $_GET['version'];
    $action = $_GET['action'];
}

// get request body
$request = file_get_contents('php://input');

// decode request
$data = json_decode($request, true);

$fileExists = file_exists($action . '.php');


if (!$fileExists) {
// default message
    $message = array(
        'status' => 404,
        'response' => array('d' => array (
            'type' => 'WS_NOT_FOUND',
            'message' => 'Webservice not found'
        ))
    );
    header('HTTP/1.0 ' . $message['status']);
    header('Content-type: application/json');
    die(json_encode($message['response']));
}

// webservice
include $action . '.php';

// logic
switch ($action) {
    case 'login':
        $message = login($data);
        break;
    case 'logout':
        $message = logout($data);
        break; 
    case 'get_routes':
        $message = getRoutes($data);
        break;  
    case 'get_sites':
        $message = getSites($data);
        break;  
    case 'get_orders':
        $message = getOrders($data);
        break; 
    case 'get_loads':
        $message = getLoads($data);
        break;      
    case 'get_auto':
        $message = getAuto($data);
        break;      
    case 'get_comments':
        $message = getComments($data);
        break;     
    case 'sync_routes':       
        $message = syncRoutes($data);
        break;    
    case 'sync_sites':       
        $message = syncSites($data);
        break;    
    case 'sync_orders':       
        $message = syncOrders($data);
        break;   
    case 'sync_loads':       
        $message = syncLoads($data);
        break;    
    case 'sync_feedback':       
        $message = syncFeedback($data);
        break;   
    case 'sync_checks':       
        $message = syncChecks($data);
        break;      
    case 'sync_comments':
        $message = syncComments($data);
        break;     
    case 'sync_task_picture':
        $message = syncTaskPicture($data);
        break;     
    case 'sync_gps':
        $message = syncGPS($data);
        break;      
}

// response
if (substr($message['status'], 0, 1) == 2) {
    header('Content-type: application/json');
    die(json_encode($message['response']));
} else {
    header('HTTP/1.0 ' . $message['status']);
    header('Content-type: application/json');
    die(json_encode($message['response']));
}

?>