<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'SINGLE_Documents'=>'dokumenta',
	'Documents'=>'Dokumentai',
	'LBL_ADD_RECORD'=>'Pridėti dokumentą',
	'LBL_RECORDS_LIST'=>'Dokumentų sąrašas',

	// Blocks
	'LBL_NOTE_INFORMATION'=>'Pagrindinė informacija',
	'LBL_FILE_INFORMATION'=>'Failo informacija',
	'LBL_DESCRIPTION'=>'2 žingsnis: Pridėkite aprašymą',

	//Field Labels
	'Title'=>'Pavadinimas',
	'File Name'=>'Failo pavadinimas',
	'Note'=>'Pastaba',
	'File Type'=>'Failo tipas',
	'File Size'=>'Dydis',
	'Download Type'=>'Parsisiuntimo tipas',
	'Version'=>'Versija',
	'Active'=>'Aktyvus',
	'Download Count'=>'Parsisiuntimų skaičius',
	'Folder Name'=>'Katalogo pavadinimas',
	'Document No'=>'Dokumento nr.',
	'Last Modified By'=>'Paskutiniį kartą keitė',
	'LBL_FILE_UPLOAD' => 'Įkelti failą',

	//Folder
	'LBL_FOLDER_HAS_DOCUMENTS'=>'Iškelkite dokumentus iš katalogo, norėdami pašalinti',

	//DetailView Actions
	'LBL_NEW_DOCUMENT' => 'Naujas dokumentas',
	'LBL_DOWNLOAD_FILE'=>'Parsisiųsti failą',
	'LBL_VIEW_FILE' => 'Peržiūrėti failą',
	'LBL_CHECK_FILE_INTEGRITY'=>'Patikrinti failų integraciją',
	'LBL_EMAIL_FILE_AS_ATTACHMENT'=>'Siųsti failą kaip priedą',

	//EditView
	'LBL_INTERNAL'=>'Vidinis',
	'LBL_EXTERNAL'=>'Išorinis',
	'LBL_MAX_UPLOAD_SIZE'=>'Didžiausia galima išsiunčiamo failo apimtis',

	//ListView Actions
	'LBL_MOVE'=>'Perkelti',
	'LBL_ADD_FOLDER'=>'Pridėti katalogą',
	'LBL_FOLDERS_LIST'=>'Katalogų sąrašas',
	'LBL_FOLDERS'=>'Katalogai',
	'LBL_DOCUMENTS_MOVED_SUCCESSFULLY'=>'Dokumentai perkelti sėkmingai',
	'LBL_DENIED_DOCUMENTS'=>'Atmesti dokumentai',
	'MB'=>'MB',

	'LBL_ADD_NEW_FOLDER'=>'Pridėti katalogą',
	'LBL_FOLDER_NAME'=>'Katalogo pavadinimas',
	'LBL_FOLDER_DESCRIPTION'=>'Katalogo aprašymas',

	//Check file integrity messages
	'LBL_FILE_AVAILABLE'=>'Failą galima parsisiųsti',
	'LBL_FILE_NOT_AVAILABLE'=>'Šio dokumento parsisiųsti negalima',


	'LBL_UPLOAD' => 'Įkelti',
	'LBL_SELECT_FILE_FROM_COMPUTER' => 'Pasirinkite failą iš kompiuterio',
	'LBL_FILE_UPLOAD' => 'Įkelti failą iš kompiuterio',
	'LBL_LINK_EXTERNAL_DOCUMENT' => 'Dokumento nuoroda',
	'LBL_TO_SERVICE' => 'į %s',
	'LBL_FROM_SERVICE' => ' %s',
	'LBL_CREATE_NEW' => 'Sukurti nauja %s',
	'LBL_TO_SERVER' => 'crm\'a',
	'LBL_FILE_URL' => 'Failo nuoroda iš interneto',
	'SalesOrder Docs' => 'Pardavimo užsakymu failai',
);

$jsLanguageStrings = array(
	'JS_EXCEEDS_MAX_UPLOAD_SIZE' => 'Viršyta maksimali atsiuntimo apimtis',
	'JS_NEW_FOLDER'=>'Naujas katalogas',
	'JS_MOVE_DOCUMENTS'=>'Perkelti dokumentus',
	//Move documents confirmation message
	'JS_ARE_YOU_SURE_YOU_WANT_TO_MOVE_DOCUMENTS_TO'=>'Ar tikrai norite perkelti šį(iuos) failą(us) į',
	'JS_FOLDER'=>'katalogas',
	'JS_OPERATION_DENIED'=>'Operacija atmesta',
	'JS_FOLDER_IS_NOT_EMPTY'=>'Katalogas nėra tuščias',
	'JS_SPECIAL_CHARACTERS'=>'Ypatingi simboliai kaip',
	'JS_NOT_ALLOWED'=>'negalimi',
);
