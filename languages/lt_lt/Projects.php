<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Projects' => 'Projektai',
	'SINGLE_Projects' => 'projekta',
	'ModuleName ID' => 'Projects ID',

	'Projects No' => 'Nr.',
	
	'LBL_ADD_RECORD' => 'Pridėti projektą',
	'LBL_RECORDS_LIST' => 'Projektų sąrašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Order type' => 'Užsakymo tipas',
	'Working days' => 'Darbo dienų',
	'Working hours' => 'Valandų',
	'Amount of people' => 'Žmonių kiekis',
	'Transport' => 'Transportas',
	'Transport price' => 'Transporto kaina',
	'Hired transport' => 'Samdomas transportas',
	'Hired transport price' => 'Samdomo transporto kaina',
	'Insurance' => 'Draudimas',
	'Insurance price' => 'Draudimo kaina',
	'Hired people' => 'Samdomi žmonės',
	'Hired people price' => 'Samdomų žmonių kaina',
	'Fuel km.' => 'Kuras km.',
	'Fuel km. price' => 'Kuro kaina',

	'LBL_PRICEBOOK_PRICE' => 'Kainoraščio kaina', 
	'LBL_AGREED_PRICE' => 'Sutarta kaina',
	

);
	
?>
