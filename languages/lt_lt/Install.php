<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

$languageStrings = array(
	'LBL_INSTALLATION_WIZARD'=>'Diegimo vedlys',
	'LBL_WELCOME'=>'Sveiki',
	'LBL_WELCOME_TO_VTIGER6_SETUP_WIZARD'=>'Sveiki, čia Vtiger 6 diegimo vedlys',
	'LBL_VTIGER6_SETUP_WIZARD_DESCRIPTION'=>'Šis diegimo vedlys padės Jums įvykdyti visą programos Vtiger diegimo procesą',
	'LBL_INSTALL_BUTTON'=>'Diegti',
	'LBL_DISAGREE'=>'Nesutikti',
	'LBL_I_AGREE'=>'Sutinku',
	'LBL_INSTALL_PREREQUISITES'=>'Diegimo prielaidos',
	'LBL_RECHECK'=>'Patikrinti dar kartą',
	'LBL_PHP_CONFIGURATION'=>'PHP konfigūracija',
	'LBL_REQUIRED_VALUE'=>'Reikalinga reikšmė',
	'LBL_PRESENT_VALUE'=>'Esama reikšmė',
	'LBL_TRUE'=>'True',
	'LBL_PHP_RECOMMENDED_SETTINGS'=>'Rekomenduojamos PHP nuostatos',
	'LBL_READ_WRITE_ACCESS'=>'Skaitymo/rašymo prieiga',
	'LBL_SYSTEM_CONFIGURATION'=>'Sistemos konfigūracija',
	'LBL_DATABASE_INFORMATION'=>'Duomenų bazės informacija',
	'LBL_DATABASE_TYPE'=>'Duomenų bazės tipas',
	'LBL_HOST_NAME'=>'Savininko pavadinimas',
	'LBL_USERNAME'=>'Vartotojo vardas',
	'LBL_PASSWORD'=>'Slaptažodis',
	'LBL_DB_NAME'=>'Duomenų bazės pavadinimas:',
	'LBL_CREATE_NEW_DB'=>'Sukurti naują duomenų bazę',
	'LBL_ROOT_USERNAME'=>'Pagrindinio vartotojo vardas',
	'LBL_ROOT_PASSWORD'=>'Pagrindinio vartotojo slaptažodis',
	'LBL_SYSTEM_INFORMATION'=>'Sistemos informacija',
	'LBL_CURRENCIES'=>'Valiuta',
	'LBL_ADMIN_INFORMATION'=>'Administratoriaus informacija',
	'LBL_RETYPE_PASSWORD'=>'Pakartokite slaptažodį',
	'LBL_DATE_FORMAT'=>'Datos formatas',
	'LBL_TIME_ZONE'=>'Laiko juosta',
	'LBL_PLEASE_WAIT'=>'Prašome palaukti',
	'LBL_INSTALLATION_IN_PROGRESS'=>'Vyksta diegimas',
	'LBL_EMAIL'=>'El. paštas',
	'LBL_ADMIN_USER_INFORMATION'=>'Administratoriaus informacija',
	'LBL_CURRENCY'=>'Valiuta',
	'LBL_URL'=>'URL',
	'LBL_CONFIRM_CONFIGURATION_SETTINGS'=>'Patvirtinti nuostatas',
	'LBL_NEXT'=>'Toliau',
	'LBL_PHP_VERSION'=>'PHP versija',
	'LBL_IMAP_SUPPORT'=>'IMAP palaikymas',
	'LBL_ZLIB_SUPPORT' => 'Zlib palaikymas',
	'LBL_GD_LIBRARY'=>'DK bibliotekos palaikymas',
	'ERR_DATABASE_CONNECTION_FAILED'=>'Nepavyko pasiekti duomenų bazės serverio',
	'ERR_INVALID_MYSQL_PARAMETERS'=>'Nurodyti netinkami mySQL ryšio parametrai',
	'MSG_LIST_REASONS'=>'Taip galėjo nutikti dėl toliau išvardytų priežasčių',
	'MSG_DB_PARAMETERS_INVALID'=>'specified database user, password, hostname, database type, or port is invalid',
	'MSG_DB_USER_NOT_AUTHORIZED'=>'nurodytas duomenų bazės vartotojas neturi teisės prisijungti prie duomenų bazės serverio iš ost',
	'LBL_MORE_INFORMATION'=>'Daugiau informacijos',
	'ERR_INVALID_MYSQL_VERSION'=>'Ši MySQL versija nepalaikoma, atnaujinkite į versiją MySQL 5.1.x ar aukštesnę ',
	'ERR_UNABLE_CREATE_DATABASE'=>'Nepavyko sukurti duomenų bazės',
	'MSG_DB_ROOT_USER_NOT_AUTHORIZED'=>'Pranešimas: Nurodyto duomenų bazės pagrindinis vartotojas neturi teisių sukurti duomenų bazės arba duomenų bazės pavadinimas turi specialiųjų simbolių. Pabandykite pakeisti duomenų bazės nuostatas.',
	'ERR_DB_NOT_FOUND'=>'Nepavyko rasti šios duomenų bazės. Pabandykite pakeisti duomenų bazės nuostatas',
	'LBL_PASSWORD_MISMATCH'=>'Pakartotinai įveskite slaptažodžius, nes slaptažodis ir pakartotinai įvestas slaptažodis nesutampa.. ',
	'LBL_ONE_LAST_THING'=>'Paskutinis dalykas…'
);
