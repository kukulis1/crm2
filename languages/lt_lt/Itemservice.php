<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Itemservice' => 'Prekės/Paslaugos',
	'ItemService' => 'Prekės/Paslaugos',
	'SINGLE_Itemservice' => 'Prekės/Paslaugos',
	'ModuleName ID' => 'Prekės/Paslaugos ID',

	'Item' => 'Prekė',
	'Service' => 'Paslauga',

	'Itemservice No' => 'Nr.',
	'Name' => 'Pavadinimas',
	'Accounting code' => 'Buhalterinis kodas',
	
	'LBL_ADD_RECORD' => 'Pridėti Prekę/Paslaugą',
	'LBL_RECORDS_LIST' => 'Sąrašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Kita Informacija',
	'LBL_MODULEBLOCK_INFORMATION' => 'Modulio  Informacija',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
