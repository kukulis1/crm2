<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Customerdiscount' => 'Kliento nuolaidos',
	'CustomerDiscount' => 'Kliento nuolaidos',
	'SINGLE_Customerdiscount' => 'Kliento nuolaidos',
	'ModuleName ID' => 'CustomerDiscount ID',
	
	'LBL_ADD_RECORD' => 'Add CustomerDiscount',
	'LBL_RECORDS_LIST' => 'CustomerDiscount List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Claim_no' => 'Pretenzijos numeris',
	'Amount' => 'Suma',
	'Used' => 'Panaudota',

	

);
	
?>
