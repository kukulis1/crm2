<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Vendors'=>'Tiekėjai',
	'SINGLE_Vendors'=>'Tiekėjas',
	'LBL_ADD_RECORD'=>'Pridėti tiekėją',
	'LBL_RECORDS_LIST' => 'Tiekėjų sąrašas',

	// Blocks
	'LBL_VENDOR_INFORMATION'=>'Tiekėjo informacija:',
	'LBL_VENDOR_ADDRESS_INFORMATION'=>'Adreso informacija:',
	
	//Field Labels
	'Vendor Name'=>'Tiekėjo pavadinimas',
	'Vendor No'=>'Tiekėjo nr.',
	'Website'=>'Svetainė',
	'GL Account'=>'DK paskyra',
	'Phone'=>'Telefonas',
	'Email'=>'Pradinis el. pašto adresas',
	
	//Added for existing Picklist entries

	'300-Sales-Software'=>'300-Pardavimai-Programinė įranga',
	'301-Sales-Hardware'=>'301-Pardavimai-Aparatinė įranga',
	'302-Rental-Income'=>'302-Nuoma-Pajamos',
	'303-Interest-Income'=>'303-Palūkanos-Pajamos',
	'304-Sales-Software-Support'=>'304-Pardavimai-Programinė įranga-Palaikymas',
	'305-Sales Other'=>'305-Pardavimai-Kita',
	'306-Internet Sales'=>'306-Pardavimai internetu',
	'307-Service-Hardware Labor'=>'307-Paslaugos-Aparatinės įrangos darbai',
	'308-Sales-Books'=>'308-Pardavimai-Knygos',
	'templates' => 'Sąskaitų šablonai įjungti', 


	'Enabled' => 'Įjungta',
	'LBL_PRICEBOOK_PRICE' => 'Kainyno kaina',
	'LBL_AGREED_PRICE' => 'Sutarta kaina',
);

$jsLanguageStrings = array(
	'LBL_RELATED_RECORD_DELETE_CONFIRMATION'=>'Ar tikrai norite pašalinti?',
	'LBL_DELETE_CONFIRMATION'=>'Jeigi pašalinsite šį tiekėją, taip pat bus pašalinti ir susiję pirkimų užsakymai. Ar tikrai norite tęsti?',
	'LBL_MASS_DELETE_CONFIRMATION'=>'Jeigi pašalinsite šį tiekėją, taip pat bus pašalinti ir susiję pirkimų užsakymai. Ar tikrai norite tęsti?',
);
