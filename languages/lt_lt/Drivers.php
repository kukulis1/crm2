<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Drivers' => 'Vairuotojai',
	'SINGLE_Drivers' => 'Vairuotojai',
	'ModuleName ID' => 'Drivers ID',
	
	'LBL_ADD_RECORD' => 'Add Drivers',
	'LBL_RECORDS_LIST' => 'Drivers List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Route date' => 'Reiso data',
	'Driver name' => 'Vairuotojas',

	

);
	
?>
