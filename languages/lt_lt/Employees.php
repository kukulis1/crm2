<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Employees' => 'Darbuotojai2',
	'SINGLE_Employees' => 'Darbuotojai2',
	'ModuleName ID' => 'ID',
	'Employees No' => 'Numeris',
	
	'LBL_ADD_RECORD' => 'Pridėti darbuotoją',
	'LBL_RECORDS_LIST' => 'Darbuotojų sąrašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	'Name' => 'Vardas',
	'Last name' => 'Pavardė',
	'email' => 'El. paštas',
	'Email' => 'El. paštas',
	'Birth day' => 'Gimimo diena',
	'Position' => 'Pareigos',
	'Person code' => 'Asmens kodas',
	'Company' => 'Įmonė',
	'Working time' => 'Darbo laikas nuo',
	'Working time until' => 'Darbo laikas iki',
	'Hired' => 'Įdarbinimo diena',
	'First working day' => 'Pirma darbo diena',
	'Trial period until' => 'Bandomasis laikotarpis',
	'Address' => 'Adresas',
	'City' => 'Miestas',
	'Phone' => 'Telefonas',
	'Work' => 'Darbo telefonas',
	'Personal' => 'Asmeninis telefonas',
	'Note' => 'Pastabos',
	'Working at the moment' => 'Dirba',
	'Director' => 'Direktorius',
	'Manager' => 'Vadybininkas',
	'Administrator' => 'Administratorius',
	'Sales Director' => 'Pardavimų direktorius',
	'Sales head' => 'Pardavimų vadovas',
	'Sellers' => 'Pardavėjai',
	'Accounting manager' => 'Askaitos vadovas / vadove',
	'Chief accountant' => 'Vyr. apskaitininkė',
	'Accountant' => 'Apskaitininke',
	'Accounting assistant' => 'Apskaitos asistente / Apskaita',
	'Hired at a price list' => 'Samdomas kainynų',
	'Service manager' => 'Serviso vadovas',
	'Service worker' => 'Serviso darbuotojas',
	'Warehouse manager' => 'Sandėlio vadovas',
	'Warehouse shift manager' => 'Sandėlio pamainos viršininkas',
	'Warehouse worker' => 'Sandėlio darbuotojas',
	'Transport head' => 'Transporto vadovas',
	'Transport manager' => 'Transporto vadybininkas',
	'Transport assistant' => 'Transporto asistentas',
	'Driver' => 'Vairuotojas',
	'Office administrator' => 'Biuro administratorė',
	'Moving head' => 'Perkraustymo vadovas',
	'Moving management' => 'Perkraustymo vadyba',
	'Mr.' => 'Vyras',
	'Ms.' => 'Moteris',	

);
	
?>
