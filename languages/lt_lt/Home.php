<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Home Page Components
	'ALVT'=>'Aukščiausio prioriteto organizacijos',
	'PLVT'=>'Aukščiausio prioriteto galimybės',
	'QLTQ' => 'Stambiausi pasiūlymai',
	'CVLVT'=>'Pagrindinė metrika',
	'HLT'=>'Aukščiausio prioriteto nesklandumų kortelės',
	'GRT'=>'Mano grupės išdėstymas',
	'OLTSO'=>'Didžiausi pardavimų užsakymai',
	'ILTI'=>'Didžiausios važtaražčio',
	'HDB'=>'Pradinio puslapio ataskaitos',
	'OLTPO'=>'Didžiausi pirkimo užsakymai',
	'LTFAQ'=>'Mano paskiausi DUK',
	'UA'=>'Būsimos veiklos',
	'PA'=>'Laukiančiosios veiklos',
    
	'Key Metrics'=>'Pagrindinės metrikos',
	'Tag Cloud'=>'Žymių debesis',
);
