<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Contacts'=>'Kontaktai',
	'SINGLE_Contacts'=>'kontaktą',
	'LBL_ADD_RECORD'=>'Pridėti kontaktą',
	'LBL_RECORDS_LIST'=>'Kontaktų sąrašas',

	// Blocks
	'LBL_CONTACT_INFORMATION'=>'Pagrindinė informacija',
	'LBL_CUSTOMER_PORTAL_INFORMATION'=>'Klientų portalo informacija',
	'LBL_IMAGE_INFORMATION'=>'Prekės paveikslo informacija:',
	'LBL_COPY_OTHER_ADDRESS'=>'Kopijuoti kitą adresą',
	'LBL_COPY_MAILING_ADDRESS'=>'Kopijuoti laiškų siuntimo adresą',

	//Field Labels
	'Office Phone'=>'Ofiso telefonas',
	'Home Phone'=>'Namų telefonas',
	'Title'=>'Pavadinimas',
	'Department'=>'Skyrius',
	'Birthdate'=>'Gimimo data',
	'Reports To'=>'Ataskaita pagal',
	'Assistant'=>'Administratorius',
	'Assistant Phone'=>'Administratoriaus telefonas',
	'Do Not Call'=>'Neskambinti',
	'Reference'=>'Nuoroda',
	'Portal User'=>'Portalo vartotojas',
	'Mailing Street'=>'Laiškų siuntimo gatvė',
	'Mailing City'=>'Laiškų siuntimo miestas',
	'Mailing State'=>'Laiškų siuntimo savivaldybė',
	'Mailing Zip'=>'Laiškų siuntimo ZIP',
	'Mailing Country'=>'Laiškų siuntimo šalis',
	'Mailing PO Box'=>'Laiškų siuntimo pašto dėžutė',
	'Other Street'=>'Kita gatvė',
	'Other City'=>'Kitas miestas',
	'Other State'=>'Kita savivaldybė',
	'Other Zip'=>'Kitas ZIP',
	'Other Country'=>'Kita šalis',
	'Other PO Box'=>'Kita pašto dėžutė',
	'Contact Image'=>'Kontakto nuotrauka',
	'Other Phone'=>'Kitas telefonas',
	'Email'=>'Pradinis el. pašto adresas',
	'Secondary Email'=>'Atsarginis el. paštas',
	'Contact Id'=>'Kontakto ID',
	
	//Added for Picklist Values
	'Mr.'=>'Pone',
	'Ms.'=>'Panele',
	'Mrs.'=>'Ponia',
	'Dr.'=>'Dr.',
	'Prof.'=>'Prof.',
	
	'User List' => 'Vartotojų sąrašas',
);

$jsLanguageStrings = array(
        'LBL_SYNC_BUTTON'=>'Sinchronizuoti dabar',
        'LBL_SYNCRONIZING'=>'Sinchronizuojama…',
        'LBL_NOT_SYNCRONIZED'=>'Dar nesusinchronizavote',
        'LBL_FIELD_MAPPING'=>'Laukų atvaizdavimas'
 );
