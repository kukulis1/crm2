<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Exportpurchaseinvoices' => 'Eksportuoti į agnum',
	'SINGLE_Exportpurchaseinvoices' => 'Exportpurchaseinvoices',
	'ModuleName ID' => 'Exportpurchaseinvoices ID',
	
	'LBL_ADD_RECORD' => 'Add Exportpurchaseinvoices',
	'LBL_RECORDS_LIST' => 'Exportpurchaseinvoices List',

	'vendor_id' => 'Tiekėjo pavadinimas',
	'invoice_date' => 'Sąskaitos data',
	'purchase_no' => 'Sąskaitos nr.',
	'total' => 'Suma',
	'duedate' => 'Mokėjimo data',
	'Show exported' => 'Rodyti ir eksportuotas saskaitas',
	'Yes' => 'Taip',
	'No' => 'Ne',

	'LBL_LEGEND' => 'Legenda',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
