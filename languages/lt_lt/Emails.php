<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'SINGLE_Emails'=>'laišką',
	'Emails'=>'Laiškai',
	'LBL_SELECT_EMAIL_IDS'=>'Pasirinkite el. pašto adresus',
	'LBL_SUBJECT'=>'Tema ',
	'LBL_ATTACHMENT'=>'Priedas',
	'LBL_BROWSE_CRM'=>'Naršyti CRM\'e',
	'LBL_SEND'=>'Siųsti',
	'LBL_SAVE_AS_DRAFT'=>'Įrašyti kaip juodraštį',
	'LBL_GO_TO_PREVIEW'=>'Eiti į atvaizdavimą',
	'LBL_SELECT_EMAIL_TEMPLATE'=>'Pasirinkite el. pašto šabloną',
	'LBL_COMPOSE_EMAIL'=>'Kurti laišką',
	'LBL_TO' => 'Kam:',
 	'LBL_CC'=>'Kopija:',
   	'LBL_BCC'=>'Namatomoji kopija:',
   	'LBL_ADD_CC'=>'Pridėti kopiją',
   	'LBL_ADD_BCC'=>'Pridėti nematomą kopiją',
	'LBL_MAX_UPLOAD_SIZE'=>'Didžiausia galima išsiunčiamo failo apimtis yra',
	'LBL_EXCEEDED'=>'Viršyta',
	'Parent ID' => 'Galimas klientas',
	'Time Start' => 'Išsiuntimo laikas',
	
	
	//Button Names translation
	'LBL_FORWARD'=>'Persiųsti',
	'LBL_PRINT'=>'Spausdinti',
	'LBL_DESCRIPTION'=>'2 žingsnis: Pridėkite aprašymą',
	'LBL_FROM'=>'Nuo',
	'LBL_INFO'=>'Info',
	'LBL_DRAFTED_ON'=>'Juodraštis',
	'LBL_SENT_ON'=>'Išsiųsta: ',
	'LBL_OWNER'=>'Savininkas',
	
	'Date & Time Sent'=>'Išsiuntimo data',
);
