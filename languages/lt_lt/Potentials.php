<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Potentials'=>'Galimybės',
	'SINGLE_Potentials'=>'Galimybės',
	'LBL_ADD_RECORD'=>'Pridėti galimybę',
	'LBL_RECORDS_LIST'=>'Galimybių sąrašas',

	// Blocks
	'LBL_OPPORTUNITY_INFORMATION'=>'Galimybės informacija:',

	//Field Labels
	'Potential No'=>'Galimybės nr.',
	'Amount'=>'Kiekis',
	'Next Step'=>'Kitas žingsnis',
	'Sales Stage'=>'Pardavimų būsena',
	'Probability'=>'Tikimybė',
	'Campaign Source'=>'Kampanijos šaltinis',
	'Forecast Amount'=>'Prognozuojamas kiekis',
	'Related To'=>'Organizacijos pavadinimas',
	'Contact Name'=>'Adresato vardas',
	
	//Dashboard widgets
	'Funnel'=>'Pardavimų piltuvėlis',
	'Potentials by Stage'=>'Galimybės pagal lygmenį',
	'Total Revenue'=>'Pajamos pagal pardavėją',
	'Top Potentials'=>'Aukščiausio prioriteto galimybės',
	'Forecast'=>'Pardavimų prognozė',

	//Added for Existing Picklist Strings

	'Prospecting' => 'Tyrimas',
	'Qualification'=>'Kvalifikacija',
	'Needs Analysis'=>'Poreikių analizė',
	'Value Proposition'=>'Kainos pasiūlymas',
	'Id. Decision Makers'=>'Identifikuoti sprendimų priėmėjus',
	'Perception Analysis'=>'Kliento nuomonės analizė',
	'Proposal/Price Quote'=>'Pasiūlymas',
	'Negotiation/Review'=>'Derybos/Ataskaita',
	'Closed Won'=>'Sėkmingai užbaigtas',
	'Closed Lost'=>'Nesėkmingai užbaigtas',

	'--None--'=>'N',
	'Existing Business'=>'Esamas verslas',
	'New Business'=>'Naujas verslas',
	'LBL_EXPECTED_CLOSE_DATE_ON'=>'Laukiama užbaigimo data',

	//widgets headers
	'LBL_RELATED_CONTACTS'=>'Susiję adresatai',
	'LBL_RELATED_PRODUCTS'=>'Susijusios prekės',
);
