<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Stevedoring' => 'Krovos darbai',
	'SINGLE_Stevedoring' => 'Krovos darbai',
	'ModuleName ID' => 'ID',

	'Date' => 'Data',
	'Order no' => 'Užsakymo nr.',
	'Client' => 'Užsakovas',
	'Shipper address' => 'Siuntėjo adresas',
	'Receiver address' => 'Gavėjo adresas',
	'weight' => 'Svoris',
	'Duration h' => 'Trukmė val.',
	'Order price' => 'Užsakymo kaina',
	'Stevedoring price' => 'Krovos darbų kaina',
	'Name last name' => 'Vardas pavardė',
	
	'LBL_ADD_RECORD' => 'Pridėti įrašą',
	'LBL_RECORDS_LIST' => 'Sąrašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
