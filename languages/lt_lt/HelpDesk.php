<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'HelpDesk'=>'Maršrutų kortelės',
	'SINGLE_HelpDesk'=>'Kortelė',
	'LBL_ADD_RECORD'=>'Pridėti kortelę',
	'LBL_RECORDS_LIST'=>'Kortelių sąrašas',

	// Blocks
	'LBL_TICKET_INFORMATION'=>'Kortelės informacija',
	'LBL_TICKET_RESOLUTION'=>'Sprendimo informacija',

	//Field Labels
	'Ticket No'=>'Kortelės nr.',
	'Severity'=>'Svarba',
	'Update History'=>'Atnaujininti registrą',
	'Hours'=>'Valandos',
	'Days'=>'Dienos',
	'Title'=>'Pavadinimas',
	'Solution'=>'Sprendimas',
	'From Portal'=>'Iš portalo',
	'Related To'=>'Organizacijos pavadinimas',
	'Contact Name'=>'Adresato vardas',
	//Added for existing picklist entries

	'Big Problem'=>'Didelė problema',
	'Small Problem'=>'Maža problema',
	'Other Problem'=>'Kita problema',

	'Normal'=>'Vidutinis',
	'High'=>'Aukštas',
	'Urgent'=>'Skubu',

	'Minor'=>'Nereikšmingas',
	'Major'=>'Reikšmingas',
	'Feature'=>'Savybė',
	'Critical'=>'Kritinis',

	'Open'=>'Atviras',
	'Wait For Response'=>'Laukti atsakymo',
	'Closed'=>'Baigtas',
	'LBL_STATUS'=>'Būsena',
	'LBL_SEVERITY'=>'Svarba',
	//DetailView Actions
	'LBL_CONVERT_FAQ'=>'Konvertuoti DUK',
	'LBL_RELATED_TO'=>'Susiję su',

	//added to support i18n in ticket mails
	'Ticket ID'=>'Kortelės ID',
	'Hi'=>'Sveki',
	'Dear'=>'Gerb.',
	'LBL_PORTAL_BODY_MAILINFO'=>'Kortelė yra',
	'LBL_DETAIL'=>'išsami informacija yra:',
	'LBL_REGARDS'=>'Linkėjimai',
	'LBL_TEAM'=>'Komanda',
	'LBL_TICKET_DETAILS'=>'Kortelės išsami informacija',
	'LBL_SUBJECT'=>'Tema:',
	'created'=>'Sukurta',
	'replied'=>'atsakė',
	'reply'=>'Tai yra atsakymas',
	'customer_portal'=>'klientų portale programoje Vtiger.',
	'link'=>'Norėdami peržiūrėti atsaymus, galite pasinaudoti šia nuoroda: ',
	'Thanks'=>'Dėkui',
	'Support_team'=>'Vtiger palaikymo grupė',
	'The comments are'=>'Pastabos:',
	'Ticket Title'=>'Kortelių pavadinimai',
	'Re'=>'Ats:',

	//This label for customerportal.
	'LBL_STATUS_CLOSED'=>'Baigtas',//Do not convert this label. This is used to check the status. If the status 'Closed' is changed in vtigerCRM server side then you have to change in customerportal language file also.
	'LBL_STATUS_UPDATE'=>'Kortelės būsena atnaujinta į ',
	'LBL_COULDNOT_CLOSED'=>'Kortelės nepavyko',
	'LBL_CUSTOMER_COMMENTS'=>'Klientas pateikė šią papildomą informaciją',
	'LBL_RESPOND'=>'Prašome atsakyti į viršuje pateiktą bilietą kaip galima greičiau.',
	'LBL_SUPPORT_ADMIN'=>'Palaikymo administratorius',
	'LBL_RESPONDTO_TICKETID'=>'Atsakyti kortelei ID',
	'LBL_RESPONSE_TO_TICKET_NUMBER'=>'Atsakyti kortelei nr.',
	'LBL_TICKET_NUMBER'=>'Kortelės numeris',
	'LBL_CUSTOMER_PORTAL'=>'Klientų portalas',
	'LBL_LOGIN_DETAILS'=>'Toliau yra Jūsų klientų portalo prisijungimo informacija:',
	'LBL_MAIL_COULDNOT_SENT'=>'Laiško išsiųsti nepavyko',
	'LBL_USERNAME'=>'Vartotojo vardas:',
	'LBL_PASSWORD'=>'Slaptažodis:',
	'LBL_SUBJECT_PORTAL_LOGIN_DETAILS'=>'Dėl Jūsų klientų portalo prisijungimo išsamios informacijos',
	'LBL_GIVE_MAILID'=>'Nurodykite savo el. pašto ID',
	'LBL_CHECK_MAILID'=>'Patikrinkite savo klientų portalo el. pašto ID',
	'LBL_LOGIN_REVOKED'=>'Jūsų prisijungimas atšauktas. Susisiekite su administratoriumi.',
	'LBL_MAIL_SENT'=>'Jums buvo išsiųstas laiškas su klientų portalo prisijungimo duomenimis.',
	'LBL_ALTBODY' => 'Tai yra pagrindinė laiško dalisThis is the body in plain text for non-HTML mail clients',
	'HelpDesk ID'=>'Nesklandumų kortelės ID',
    
    
	'Start Date'=>'Pasikrovimo data',
	'End Date'=>'Išsikrovimo data',
	'Start Time'=>'Pasikrovimo laikas',
	'End Time'=>'Išsikrovimo laikas',  
	'Pallets'=>'Paletės',
	'Weight'=>'Svoris',
	'Is Own'=>'Įmonės vairuotojas',
	'Price'=>'Maršruto kaina',
	'ID'=>'Maršruto ID',
	'LBL_ROUTE_INFORMATION'=>'Maršruto informacija',
	'Route'=>'Maršrutas',
	'Metrika Status'=>'Metrika įterpimo būsena',    
	'print_note' => 'Pastabos',
	'cost_agreed' => 'Sutarta kaina',
	'location_name' => 'Pradžios vieta',
	'driver1' => 'Vairuotojas 1',
	'visited' => 'Aplankyta',
	'distance_m' => 'Kilometražas',
	'route_points_count' => 'Reisų skaičius',
	'kg_unload' => 'Iškrauta kg',
	'max_kg' => 'Max kg',
	'max_pll' => 'Max pll',
	'max_m3' => 'Max m3',
	'pll_load' => 'Pakrauta pll',
	'load_kg' => 'Pakrauta kg',
	'm3_unload' => 'Iškrauta m3',
	'direction' => 'Kryptis'
);
