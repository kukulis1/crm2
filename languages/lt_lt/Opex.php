<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Opex' => 'Opex ataskaita',
	'SINGLE_Opex' => 'Opex ataskaita',
	'ModuleName ID' => 'ID',
	
	'LBL_ADD_RECORD' => 'Add opex',
	'LBL_RECORDS_LIST' => 'opex List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',
	'January' => 'Sausis',
	'February' => 'Vasaris',
	'March' => 'Kovas',
	'April' => 'Balandis',
	'May' => 'Gegužė',
	'June' => 'Birželis',
	'July' => 'Liepa',
	'August' => 'Rugpjūtis',
	'September' => 'Rugsėjis',
	'October' => 'Spalis',
	'November' => 'Lapkritis',
	'December' => 'Gruodis',	

	'January_' => 'sausį',
	'February_' => 'vasarį',
	'March_' => 'kovą',
	'April_' => 'balandį',
	'May_' => 'gegužę',
	'June_' => 'birželį',
	'July_' => 'liepą',
	'August_' => 'rugpjūtį',
	'September_' => 'rugsėjį',
	'October_' => 'spalį',
	'November_' => 'lapkritį',
	'December_' => 'gruodį',

);
	
?>
