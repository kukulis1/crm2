<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'LBL_TOTAL_EVENTS_IMPORTED'=>'Sėkmingai įkeltų taškų skaičius',
	// Basic Strings
	'SINGLE_Calendar'=>'Darbai',
	'SINGLE_Events'=>'Taškas',
	'LBL_ADD_TASK'=>'Pridėti užduotį',
	'LBL_ADD_EVENT'=>'Pridėti tašką',
	'LBL_RECORDS_LIST'=>'Kalendorių sąrašas',
	'LBL_EVENTS'=>'Taškai',
	'LBL_TODOS'=>'Darbai',
	'LBL_CALENDAR_SETTINGS'=>'Kalendoriaus nuostatos',
	'LBL_CALENDAR_SHARING'=>'Bendrinti kalendorių su',
	'LBL_DEFAULT_EVENT_DURATION'=>'Numatytoji įvykio trukmė',
	'LBL_CALL'=>'Konferencija',
	'LBL_OTHER_EVENTS'=>'Kitas taškas',
	'LBL_MINUTES'=>'min.',
	'LBL_SELECT_USERS'=>'Pažymėti vartotojus',
	'LBL_EVENT_OR_TASK'=>'Taškas / Užduotis',

	// Blocks
	'LBL_TASK_INFORMATION'=>'Užduoties informacija',

	//Fields
	'Subject'=>'Tema ',
	'Start Date & Time'=>'Pradžios data ir laikas',
	'Activity Type'=>'Taško tipas',
	'Send Notification'=>'Siųsti pranešimą',
	'Location'=>'Adresas',
	'End Date & Time'=>'Pabaigos data ir laikas',
	'Due Date' =>'Pabaigos data ir laikas',
	'Priority'=>'Neaptarnavimo priežastis',
	'Account'=>'Klientas',
    
	//Side Bar Names
	'LBL_ACTIVITY_TYPES'=>'Taško tipai',
	'LBL_CONTACTS_SUPPORT_END_DATE'=>'Palaikymo pabaigos data',
	'LBL_CONTACTS_BIRTH_DAY'=>'Gimimo data',
	'LBL_ADDED_CALENDARS'=>'Pridėti kalendoriai',


	//Activity Type picklist values
	'Call'=>'Konferenciją',
	'Meeting'=>'Susirinkimą',
	'Task'=>'Užduotis',
	'Load'=>'Pakrovimas',
	'Unload'=>'Iškrovimas',
    
	//Status picklist values
	'Planned'=>'Planuojama',
	'Completed'=>'Išvyko',
	'Pending Input'=>'Laukiamas įvedimas',
	'Not Started'=>'Nepradėta',
	'Deferred'=>'Atidėta',
	'In Progress'=>'Atvyko',
    
	//Priority picklist values
	'Medium'=>'Vidutinis',

	'LBL_CHANGE_OWNER'=>'Pakeisti savininką',

	'LBL_EVENT'=>'Taškas',
	'LBL_TASK'=>'Užduotis',
	'LBL_TASKS'=>'Užduotys',

	'LBL_RECORDS_LIST'=>'Sąrašo peržiūra',
	'LBL_CALENDAR_VIEW'=>'Mano kalendorius',
	'LBL_SHARED_CALENDAR'=>'Bendrinamas kalendorius',

	//Repeat Lables - used by getTranslatedString
	'LBL_DAY0'=>'Sekmadienis',
	'LBL_DAY1'=>'Pirmadienis',
	'LBL_DAY2'=>'Antradienis',
	'LBL_DAY3'=>'Trečiadienis',
	'LBL_DAY4'=>'Ketvirtadienis',
	'LBL_DAY5'=>'Penktadienis',
	'LBL_DAY6'=>'Šeštadienis',

	'first'=>'Pirmas',
	'last' => 'Paskiausias',
	'LBL_DAY_OF_THE_MONTH'=>'mėnesio diena',
	'LBL_ON'=>'Įjungta',

	'Daily'=>'Kasdien',
	'Weekly'=>'Kiekvieną savaitę',
	'Monthly'=>'Mėnesis (iai)',
	'Yearly'=>'Metai',
	
	//Import and Export Labels
	'LBL_IMPORT_RECORDS'=>'Importuoti įrašus',
	'LBL_RESULT'=>'Rezultatas',
	'LBL_FINISH'=>'Pabaigti',
	'LBL_TOTAL_TASKS_IMPORTED'=>'Sėkmingai įkeltų užduočių skaičius',
	'LBL_TOTAL_TASKS_SKIPPED'=>'Praleistų užduočių skaičius (dėl neužpildytų reikiamų laukų)',

	'LBL_TOTAL_EVENTS_SKIPPED'=>'Praleistų įvykių skaičius (dėl neužpildytų reikiamų laukų)',
	
	'ICAL_FORMAT'=>'iCal formatas',
	'LBL_LAST_IMPORT_UNDONE'=>'Jūsų paskutinis išsiuntimas nebuvo atliktas.',
	'LBL_UNDO_LAST_IMPORT'=>'Atšaukti paskutinę importaciją'

);

$jsLanguageStrings = array(
	'LBL_ADD_EVENT_TASK'=>'Pridėti įvykį/užduotį',
	'JS_TASK_IS_SUCCESSFULLY_ADDED_TO_YOUR_CALENDAR'=>'Užduotis sėkmingai pridėta kalendoriuje',
    'LBL_SYNC_BUTTON'=>'Sinchronizuoti dabar',
    'LBL_SYNCRONIZING'=>'Sinchronizuojama…',
    'LBL_NOT_SYNCRONIZED'=>'Dar nesusinchronizavote',
    'LBL_FIELD_MAPPING'=>'Laukų atvaizdavimas',
    'LBL_CANT_SELECT_CONTACT_FROM_LEADS'=>'Negalima pasirinkti galimų klientų adresatų',
    'JS_FUTURE_EVENT_CANNOT_BE_HELD'=>'Negali būti išlaikytas ateityje',
	
	//Calendar view label translation
	'LBL_MONTH'=>'Mėnesis',
	'LBL_TODAY'=>'Šiandien',
	'LBL_DAY'=>'Diena  ',
	'LBL_WEEK'=>'Savaitė ',
	
	'LBL_SUNDAY'=>'Sekmadienis',
	'LBL_MONDAY'=>'Pirmadienis',
	'LBL_TUESDAY'=>'Antradienis',
	'LBL_WEDNESDAY'=>'Trečiadienis',
	'LBL_THURSDAY'=>'Ketvirtadienis',
	'LBL_FRIDAY'=>'Penktadienis',
	'LBL_SATURDAY'=>'Šeštadienis',
	
	'LBL_SUN'=>'Sekm',
	'LBL_MON'=>'Pirm',
	'LBL_TUE'=>'Antr',
	'LBL_WED'=>'Treč',
	'LBL_THU'=>'Ketv',
	'LBL_FRI'=>'Penkt',
	'LBL_SAT'=>'Šešt',
	
	'LBL_JANUARY'=>'Sausis',
	'LBL_FEBRUARY'=>'Vasaris',
	'LBL_MARCH'=>'Kovas',
	'LBL_APRIL'=>'Balandis',
	'LBL_MAY'=>'Geg',
	'LBL_JUNE'=>'Birželis',
	'LBL_JULY'=>'Liepa',
	'LBL_AUGUST'=>'Rugpjūtis',
	'LBL_SEPTEMBER'=>'Rugsėjis',
	'LBL_OCTOBER'=>'Spalis',
	'LBL_NOVEMBER'=>'Lapkritis',
	'LBL_DECEMBER'=>'Gruodis',
	
	'LBL_JAN'=>'Sau',
	'LBL_FEB'=>'Vas',
	'LBL_MAR'=>'Kov',
	'LBL_APR'=>'Bal ',
	'LBL_MAY'=>'Geg',
	'LBL_JUN'=>'Bir',
	'LBL_JUL'=>'Lie ',
	'LBL_AUG'=>'Rugp',
	'LBL_SEP'=>'Rugs.',
	'LBL_OCT'=>'Spa',
	'LBL_NOV'=>'Lap',
	'LBL_DEC'=>'Gruo',
	
	'LBL_ALL_DAY'=>'Visa diena',
	//End
);
