<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Packages' => 'Paketai',
	'SINGLE_Packages' => 'Paketai',
	'ModuleName ID' => 'Packages ID',
	
	'LBL_ADD_RECORD' => 'Add Packages',
	'LBL_RECORDS_LIST' => 'Packages List',

	'SalesOrders' => 'Pardavimo užsakymai',
	'Tare' => 'Tara',
	'Kg' => 'Kg',
	'Length' => 'Ilgis',
	'Width' => 'Plotis',
	'Height' => 'Aukštis',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
