<?php

$languageStrings = array(
        'LBL_SP_INFORMATION' => 'Setting Position',
        'SINGLE_SettingPosition' => 'Setting Position',
        'SettingPosition' => 'Pareigos',
        'Setting Position' => 'Pareigos',
        'Name' => 'Rolės pavadinimas',
        'Reports To' => 'Rolė pavaldi',
);