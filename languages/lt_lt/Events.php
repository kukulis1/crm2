<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

/* NOTE: Should be inline with Calendar language translation but few variations */

$languageStrings = array(
	// Basic Strings
	'Events'=>'Taškai',
	'SINGLE_Events'=>'Taškas',
	'LBL_ADD_RECORD'=>'Pridėti tašką',
	'LBL_RECORDS_LIST'=>'Taškų sąrašas',
	'LBL_EVENTS'=>'Taškai',
	'LBL_TODOS'=>'Darbai',
	'LBL_HOLD_FOLLOWUP_ON'=>'Išlaikyti sekimą',
	
	// Blocks
	'LBL_EVENT_INFORMATION'=>'Taško informacija',
	'LBL_RECURRENCE_INFORMATION'=>'Pasikartojimo išsami informacija',
    'LBL_RELATED_TO'=>'Susiję su',
	
	//Fields
	'Start Date & Time'=>'Pradžios data ir laikas',
	'Recurrence'=>'Pasikartojimas',
	'Send Notification'=>'Siųsti pranešimą',
	'Location'=>'Adresas',
	'Send Reminder'=>'Siųsti priminimą prieš',
	'End Date & Time'=>'Pabaigos data ir laikas',
	'Activity Type'=>'Taško tipas',
	'Visibility'=>'Matomumas',
	'Recurrence'=>'Pakartoti',
	'Priority'=>'Neaptarnavimo priežastis',
	'Account'=>'Klientas',
    
	//Visibility picklist values
	'Private'=>'Privatu',
	'Public'=>'Viešas',
	
	//Activity Type picklist values
	'Call'=>'Konferenciją',
	'Meeting'=>'Susirinkimą',
	'Load'=>'Pakrovimas',
	'Unload'=>'Iškrovimas',
    
	//Status picklist values
	'Planned'=>'Planuojama',
	'Completed'=>'Išvyko',
	'Not Held'=>'Neįvykęs',
	'In Progress'=>'Atvyko',
    
	//Reminder Labels
	'LBL_DAYS'=>'diena (os)',
	'LBL_HOURS'=>'Valandos',
	
	//Repeat Labels
	'LBL_DAYS_TYPE'=>'Diena (os)',
	'LBL_WEEKS_TYPE'=>'Savaitės',
	'LBL_MONTHS_TYPE'=>'Mėnesis (iai)',
	'LBL_YEAR_TYPE'=>'Metai',
	
	'LBL_FIRST'=>'Pirmas',
	'LBL_LAST' => 'Paskiausias',
	
	'LBL_SM_SUN'=>'Sekm',
	'LBL_SM_MON'=>'Pirm',
	'LBL_SM_TUE'=>'Antr',
	'LBL_SM_WED'=>'Treč',
	'LBL_SM_THU'=>'Ketv',
	'LBL_SM_FRI'=>'Penkt',
	'LBL_SM_SAT'=>'Šešt',
	
	'LBL_DAY0'=>'Sekmadienis',
	'LBL_DAY1'=>'Pirmadienis',
	'LBL_DAY2'=>'Antradienis',
	'LBL_DAY3'=>'Trečiadienis',
	'LBL_DAY4'=>'Ketvirtadienis',
	'LBL_DAY5'=>'Penktadienis',
	'LBL_DAY6'=>'Šeštadienis',
	
	'Daily'=>'Kasdien',
	'Weekly'=>'Kiekvieną savaitę',
	'Monthly'=>'Mėnesis (iai)',
	'Yearly'=>'Metai',
	
	'LBL_REPEATEVENT'=>'Pakartoti kartą kiekviename',
	'LBL_UNTIL'=>'Iki',
	'LBL_DAY_OF_THE_MONTH'=>'mėnesio diena',
	'LBL_ON'=>'Įjungta',
	
	'LBL_RECORDS_LIST'=>'Sąrašo peržiūra',
	'LBL_CALENDAR_VIEW'=>'Kalendoriaus peržiūra',

    'LBL_INVITE_USER_BLOCK'=>'Pakvietsti',
    'LBL_INVITE_USERS'=>'Pakviesti vartotojų',
	'INVITATION'=>'Kvietimas',

);
