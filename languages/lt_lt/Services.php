<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Services'=>'Paslaugos',
	'SINGLE_Services'=>'paslaugą.',
	'LBL_ADD_RECORD'=>'Pridėti paslaugą',
	'LBL_RECORDS_LIST'=>'Paslaugų sąrašas',

	// Blocks
	'LBL_SERVICE_INFORMATION'=>'Paslaugos išsami informacija',
	
	'LBL_MORE_CURRENCIES'=>'daugiau valiutų', 
	'LBL_PRICES'=>'Paslaugos kaina',
	'LBL_PRICE'=>'Kaina',
	'LBL_RESET_PRICE'=>'Atstatyti kainą',
	'LBL_RESET'=>'Atsisakyti',
	
	//Services popup of pricebook
	'LBL_ADD_TO_PRICEBOOKS'=>'Įtrauktį į kainoraščius',

	//Field Labels
	'Service Name'=>'Paslaugos pavadinimas',
	'Service Active'=>'Aktyvus',
	'Service Category'=>'Kategorija',
	'Service No'=>'Paslaugos nr.',
	'Owner'=>'Savininkas',
	'No of Units' => 'Vienetų skaičius',
	'Commission Rate'=>'Komisinių kursas',
	'Price'=>'Kaina',
	'Usage Unit'=>'Naudojimosi vienetas',
	'Tax Class'=>'Mokesčių klasė',
	'Website'=>'Svetainė',
	
	//Services popup of pricebook
	'LBL_ADD_TO_PRICEBOOKS'=>'Įtrauktį į kainoraščius',
    
    'VAT'=>'1VAT',
    'Sales'=>'1Sales',
    'Service'=>'1Service',
    'Support'=>'1Support',
    'Installation'=>'1Installation',
    'Migration'=>'1Migration',
    'Customization'=>'1Customization',
    'Training'=>'1Training',
    'Hours'=>'1Hours',
    'Days'=>'1Days',
    'Incidents'=>'1Incidents',
    
	'Auto Number'=>'Automobilio numeris',
	'Type Value'=>'Tipo reikšmė',
	'Check'=>'Viskas gerai',
	'Odometer'=>'Odometras',  
	'Litre'=>'Litrai',
	'Support Start Date'=>'Data',  
	'Part Number'=>'Priekabos numeris',      
	'LBL_GAS_INFORMATION'=>'Kita informacija',
  
);
