<?php

/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
    $languageStrings = array(
        'Map' => 'Atvaizdavimas',
        'EXTENTIONNAME'=>'Google',
        'LBL_UPDATES_CRM'=>'Atnaujinimai programoje Vtiger',
        'LBL_UPDATES_GOOGLE'=>'Atnaujinimai Google',
        'LBL_UPDATED'=>'atnaujintas',
        'LBL_ADDED'=>'Pridėta',
        'LBL_DELETED'=>'Pašalinta',
        'LBL_SYNCRONIZED'=>'Sinchronizuota',
        'LBL_NOT_SYNCRONIZED'=>'Dar nesusinchronizavote',
        'LBL_SYNC_BUTTON'=>'Sinchronizuoti dabar',
        'LBL_MORE_VTIGER'=>'Yra daugiau įrašų nesusinchronizuotų su GOOGLE',
        'LBL_MORE_GOOGLE'=>'Yra daugiau įrašų nesusinchronizuotų su GOOGLE',
        'Contact Name'=>'Adresato vardas',
        'Email'=>'El. paštas',
        'Mobile Phone'=>'Mobilus telefonas',
        'Address'=>'Adresas',
        'Event Title'=>'Įvykio pavadinimas',
        'Start Date'=>'Pradžios data',
        'Until Date'=>'Iki datos:',
        'Description'=>'Aprašymas'
    );
    $jsLanguageStrings = array(
        'LBL_SYNC_BUTTON'=>'Sinchronizuoti dabar',
        'LBL_SYNCRONIZING'=>'Sinchronizuojama…',
        'LBL_NOT_SYNCRONIZE'=>'Dar nesusinchronizavote'
    );
