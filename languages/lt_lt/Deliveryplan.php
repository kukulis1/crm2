<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Deliveryplan' => 'Įteikimo planas',
	'DeliveryPlan' => 'Įteikimo planas',
	'SINGLE_Deliveryplan' => 'DeliveryPlan',
	'ModuleName ID' => 'DeliveryPlan ID',

	'Date' => 'Data',
	'load' => 'Pakrovimas',
	'unload' => 'Iškrovimas',
	'route' => 'Reisas',
	'load_date' => 'Pakr. data',
	'unload_date' => 'Iškr. data',
	
	'LBL_ADD_RECORD' => 'Add DeliveryPlan',
	'LBL_RECORDS_LIST' => 'DeliveryPlan List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
