<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	//DetailView Actions
	'SINGLE_Quotes'=>'Pasiūlymas',
	'LBL_EXPORT_TO_PDF'=>'Eksportuoti kaip PDF',
    'LBL_SEND_MAIL_PDF'=>'Siųsti laišką su PDF',

	//Basic strings
	'LBL_ADD_RECORD'=>'Pridėti pasiūlymą',
	'LBL_RECORDS_LIST'=>'Pasiūlymų sąrašas',

	// Blocks
	'LBL_QUOTE_INFORMATION'=>'Pasiūlymo informacija',
	
	//Field Labels
	'Quote No'=>'Pasiūlymo nr.',
	'Quote Stage'=>'Pasiūlymo lygis',
	'Valid Till'=>'Galioja iki',
	'Inventory Manager'=>'Inventoriaus tvarkyklė',
	//Added for existing Picklist Entries

	'Accepted'=>'Priimta',
	'Rejected'=>'Atmesta',
	
	'LBL_BILLING_ADDRESS_FROM'=>'Kopijuoti pakrovimo adresą iš',
	'LBL_SHIPPING_ADDRESS_FROM'=>'Kopijuoti iškrovimo adresą iš',
	'Billing Address'=>'Pakrovimo adresas',
	'Billing City'=>'Pakrovimo miestas',
	'Billing Code'=>'Pakrovimo pašto kodas',
	'Billing Country'=>'Pakrovimo šalis',
	'Billing Po Box'=>'Pakrovimo pašto dėžutė',
	'Billing State'=>'Pakrovimo savivaldybė', 
	'Load Company'=>'Pakrovimo įmonė',     
	'Shipping Address'=>'Iškrovimo adresas',
	'Shipping City'=>'Iškrovimo miestas',
	'Shipping State'=>'Iškrovimo savivaldybė',
	'Shipping Code'=>'Iškrovimo pašto kodas',
	'Shipping Country'=>'Iškrovimo šalis',
	'Shipping Po Box'=>'Iškrovimo pašto dėžutė',
	'Unload Company'=>'Iškrovimo įmonė',
    
	'Load Date From'=>'Pakrovimo data nuo',
	'Load Time From'=>'Pakrovimo laikas nuo',
	'Load Date To'=>'Pakrovimo data iki',
	'Load Time To'=>'Pakrovimo laikas iki',
	'Unload Date From'=>'Iškrovimo data nuo',
	'Unload Time From'=>'Iškrovimo laikas nuo',
	'Unload Date To'=>'Iškrovimo data iki',
	'Unload Time To'=>'Iškrovimo laikas iki',    
    
	//Translation for product not found
	'LBL_THIS'=>'Šis',
	'LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM'=>'yra pašalintas iš sistemos. Pašalinkite šį elementą ar pakeiskite kitu',
	'LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM' => 'Šis elementas buvo pašalintas iš sistemos. Prašome pašalinti jo nuorodą.',

);

$jsLanguageStrings = array(
	'JS_PLEASE_REMOVE_LINE_ITEM_THAT_IS_DELETED'=>'Prašome pašalinti nuorodą į nebeegzistuojantį elementą',
);
