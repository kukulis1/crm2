<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'LBL_SELECT_PICKLIST_IN'=>'Pasirinkite atrinkčių sąrašą',
	'LBL_ADD_VALUE'=>'Pridėti reikšmę',
	'LBL_RENAME_VALUE'=>'Pervadinti reikšmę',
	'LBL_DELETE_VALUE'=>'Pašalinti reikšmę',
	'LBL_ITEMS'=>'Reikšmės',
	'LBL_DRAG_ITEMS_TO_RESPOSITION'=>'Tempkite elementus, norėdami perkelti juos į kitą vietą',
	'LBL_SELECT_AN_ITEM_TO_RENAME_OR_DELETE'=>'Pažymėkite elementą pašalinimui arba pavadinimo keitimui',
	'LBL_TO_DELETE_MULTIPLE_HOLD_CONTROL_KEY'=>'Norėdami pašalinti daugiau įrašų vienu metu, pasirinkdami įrašus laikykite CTRL klavišą.',
	'LBL_ADD_ITEM_TO'=>'Pridėti elementą prie',
	'LBL_ITEM_VALUE'=>'Elemento reikšmė',
	'LBL_ITEM_TO_RENAME'=>'Pervadinami elementai',
	'LBL_ENTER_NEW_NAME'=>'Įrašykite naują pavadinimą',
	'LBL_RENAME_PICKLIST_ITEM'=>'Pervadinti atrinkčių sąrašo elementą',
	'LBL_DELETE_PICKLIST_ITEMS'=>'Pašalinti atrinkčių sąrašo elementus',
	'LBL_ITEMS_TO_DELETE'=>'Šalinami elementai',
	'LBL_REPLACE_IT_WITH'=>'Pakeisti',
	'LBL_ASSIGN_TO_ROLE'=>'Priskirti vaidmeniui',
	'LBL_ALL_ROLES'=>'Visi vaidmenys',
	'LBL_CHOOSE_ROLES'=>'Pasirinkti vaidmenis',
	'LBL_ALL_VALUES' => 'Visos reikšmės',
	'LBL_VALUES_ASSIGNED_TO_A_ROLE'=>'Reikšmėss priskirtos vaidmeniui',
	'LBL_ASSIGN_VALUE'=>'Priskirti reikšmę',
	'LBL_SAVE_ORDER'=>'Įrašyti užsakymą',
	'LBL_ROLE_NAME'=>'Vaidmens pavadinimas',
	'LBL_SELECTED_VALUES_MESSGAE'=>'bus rodomas šio vaidmens vartotojams',
	'LBL_ENABLE/DISABLE_MESSGAE'=>'Spustelėkite reikšmę, norėdami ją įjungti/išjungti. Tai padarę, pakeitimus įrašykite.',
	'LBL_ASSIGN_VALUES_TO_ROLES'=>'Priskirti reikšmes vaidmenims',
	'LBL_SELECTED_VALUES'=>'Pažymėtos reikšmės',
    'NO_PICKLIST_FIELDS'=>'neturi jokių atrinkčių sarašo laukų',
	'LBL_NON_EDITABLE_PICKLIST_VALUES'=>'Neredaguojamos reikšmės',
);
$jsLanguageStrings = array(
	//PickList 
	'JS_ITEM_RENAMED_SUCCESSFULLY'=>'Elementas pervadintas sėkmingai',
	'JS_ITEM_ADDED_SUCCESSFULLY'=>'Elementas pridėtas sėkmingai',
	'JS_NO_ITEM_SELECTED'=>'Nepasirinktas elementas',
	'JS_MORE_THAN_ONE_ITEM_SELECTED'=>'Pasirinkta daugiau nei vienas elementas',
	'JS_ITEMS_DELETED_SUCCESSFULLY'=>'Elementai pašalinti sėkmingai',
	'JS_YOU_CANNOT_DELETE_ALL_THE_VALUES'=>'Negalima pašainti visų verčių',
	'JS_ALL_ROLES_SELECTED'=>'Visi pasirinkti vaidmenys',
	'JS_LIST_UPDATED_SUCCESSFULLY'=>'Sąrašas atnaujintas sėkmingai',
	'JS_SEQUENCE_UPDATED_SUCCESSFULLY'=>'Seka atnaujinta sėkmingai',
	'JS_VALUE_ASSIGNED_SUCCESSFULLY'=>'Reikšmė priskirta sėkmingai',
    'JS_PLEASE_SELECT_MODULE'=>'Pasirinkite modulį',
	'JS_SPECIAL_CHARACTERS'=>'Ypatingi simboliai kaip',
	'JS_NOT_ALLOWED'=>'negalimi',
);	
