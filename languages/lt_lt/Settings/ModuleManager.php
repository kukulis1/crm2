<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'LBL_IMPORT_MODULE_FROM_FILE'=>'Importuoti modulį iš failo',
	'LBL_VTIGER_EXTENSION_STORE'=>'Vtiger programų parduotuvė',
	'LBL_PUBLISHER'=>'Leidėjas',
	'LBL_LICENSE'=>'Licencija',
	'LBL_PUBLISHED_ON'=>'Paskelbta',
	'LBL_INSTALL'=>'Diegti',
	'LBL_UPGRADE'=>'Naujovinti',
	'LBL_VERSION'=>'Versija',
	'LBL_DECLINE'=>'Nuosmukis',
	'LBL_ACCEPT_AND_INSTALL'=>'Sutikti ir diegti',
	'LBL_ALREADY_EXISTS'=>'Jau egzistuoja',
	'LBL_OK'=>'Gerai',
	'LBL_EXTENSION_NOT_COMPATABLE'=>'Vtiger nepalaiko tokio plėtinio',
	'LBL_INVALID_FILE'=>'Netinkamas failas',
	'LBL_NO_LICENSE_PROVIDED'=>'Licencija nepateikta',
	'LBL_INSTALLATION'=>'Diegimas',
	'LBL_FAILED'=>'Nepavyko  ',
	'LBL_SUCCESSFULL'=>'Sėkmingas',
	'LBL_INSTALLATION_LOG'=>'Diegimo žurnalas',
	//Install From file labels
	'LBL_VERIFY_IMPORT_DETAILS'=>'Patvirtinkite importavimo informaciją',
	'LBL_MODULE_NAME'=>'Organizacijos',
	'LBL_REQ_VTIGER_VERSION' => 'Vtiger versija',
	'LBL_LICENSE_ACCEPT_AGREEMENT'=>'Sutinku su sąlygomis',
	'LBL_EXISTS'=>'Egzistuoja',
	'LBL_PROCEED_WITH_IMPORT'=>'Ar norite tęsti importavimą?',
	'LBL_VERSION_NOT_SUPPORTED'=>'Modulio versija nepalaikoma šios Vtiger versijos',
	'LBL_FINISH'=>'Pabaigti',
	'LBL_IMPORT_ZIP'=>'Diegti iš failo',
	'LBL_IMPORT_MODULE'=>'Diegti iš programos Vtiger',
	'LBL_NOT_FOUND'=>"Sorry, no extensions are currently available for the current version of your CRM",
	'LBL_INVALID_IMPORT_TRY_AGAIN'=>'suteiktas moduliui importuoti. Bandykite dar kartą.',
	'LBL_IMPORTED_MODULE'=>'modulis importuotas sėkmingai.',
	'LBL__IMPORTING_MODULE'=>'Importuoti modulį  ',
	
	'LBL_UPDATE_MODULE_FROM_FILE'=>'Atnaujinti modulį iš failo',
	'LBL_UPDATED_MODULE'=>'modulis atnaujintas sėkmingai.',
	'LBL__UPDATING_MODULE'=>'Atnaujinamas modulis',
	
);
