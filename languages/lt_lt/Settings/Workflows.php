<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	//Basic Field Names
	'LBL_NEW'=>'Naujas',
	'LBL_WORKFLOW'=>'Automatiniai veiksmai',
	'LBL_CREATING_WORKFLOW'=>'Kuriama darbo eiga',
	'LBL_EDITING_WORKFLOW'=>'Redaguoti darbo eigą',
	'LBL_NEXT'=>'Toliau',
	'LBL_BASIC_INFORMATION' => 'Pagrindinė informacija',
	'LBL_WORKFLOW_TRIGGER' => 'Sužadinantis veiksmas',
	'LBL_TRIGGER_WORKFLOW_ON' => 'Įvyki sužadins',
	'LBL_CREATION' => 'sukūrimas',
	'LBL_UPDATED'=>'atnaujinimas',
	'LBL_INCLUDES_CREATION' => 'Įskaitant sukūrimą',
	'LBL_TIME_INTERVAL' => 'Laiko intervalais',
	'LBL_RECURRENCE' => 'Pasikartojimas',
	'LBL_TARGET_MODULE' => 'Sekamas modulis',
	'LBL_AT_TIME' => 'Laikas',
	'LBL_ON_THESE_DAYS' => 'Šiomis dienomis',
	'LBL_CHOOSE_DATE' => 'Pasirinkite data',
	'LBL_SELECT_MONTH_AND_DAY' => 'Pasirinkite data',

	'LBL_HOURLY' => 'Kas valandą',
	'LBL_DAILY' => 'Kasdien',
	'LBL_WEEKLY' => 'Savaitės dienomis',
	'LBL_ON_THESE_DAYS' => 'Šiomis dienomis',
	'LBL_MONTHLY_BY_DATE' => 'Mėnesio dienomis',
	'LBL_MONTHLY_BY_WEEKDAY' => 'Mėnesinis pagal savaitės dienas',
	'LBL_YEARLY' => 'Metinis',
	'LBL_SPECIFIC_DATE' => 'Tam tikrą dieną',
	'LBL_WORKFLOWS' => 'automatiniai veiksmai',
	'Module' => 'Sekamas modulis',
	'Workflow Name' => 'Užduoties pavadinimas',
	'Trigger' => 'Sužadinantis veiksmas',
	'Conditions' => 'Sąlygos',
	'LBL_WORKFLOW_SEARCH' => 'Ieškoti pagal pavadinimą',
	'LBL_ADD_RECORD' => 'Sukurti naują',


	'LBL_FIRST_TIME_CONDITION_MET' => 'Vieną kartą kai atitinka sąlygos',
	'LBL_EVERY_TIME_CONDITION_MET' => 'Kiekvieną kartą kai atitinka sąlygos',
	'LBL_FREQUENCY' => 'Dažnumas',
	'LBL_RUN_WORKFLOW' => 'Vykdyti veiksmą',
	'LBL_WORKFLOW_NAME' => 'Užduoties pavadinimas',
	'LBL_WORKFLOW_CONDITION' => 'Sąlygos',
	'LBL_WORKFLOW_ACTIONS' => 'Atliekamas veiksmas',
	'Send Mail' => 'Išsiųsti el. laišką',
	'Create Todo' => 'Sukurti užduotį',
	'Create Event' => 'Sukurti įvykį',
	'Update Fields' => 'Atnaujinti laukus',
	'Create Entity' => 'Sukurti įrašą',
	'Invoke Custom Function' => 'Iškviesti tam tikrą funkciją',

	//Edit view
	'LBL_STEP_1'=>'1 žingsnis',
	'LBL_ENTER_BASIC_DETAILS_OF_THE_WORKFLOW'=>'Įveskite pagrindinę darbo eigos informaciją',
	'LBL_SPECIFY_WHEN_TO_EXECUTE'=>'Nurodykite, kad vykdyti šią darbo eigą.',
	'ON_FIRST_SAVE'=>'Tik pirmą krtą įrašant',
	'ONCE'=>'Kol pirmą kartą bus patenkinta sąlyga',
	'ON_EVERY_SAVE'=>'Kaskart kai įvykis įrašomas',
	'ON_MODIFY'=>'Kaskart kai įvykis pakeičiamas',
	'MANUAL'=>'Sistema',
	'SCHEDULE_WORKFLOW'=>'Suplanuoti darbo eigą',
	'ADD_CONDITIONS'=>'Pridėti sąlygas',
	'ADD_TASKS'=>'Pridėti užduotis',

	//Step2 edit view
	'LBL_EXPRESSION'=>'Išraiška',
	'LBL_FIELD_NAME'=>'Laukas',
	'LBL_SET_VALUE'=>'Nustatyti reikšmę',
	'LBL_USE_FIELD'=>'Naudoti lauką',
	'LBL_USE_FUNCTION'=>'Naudoti funkciją',
	'LBL_RAW_TEXT'=>'Neapdorotas tekstas',
	'LBL_ENABLE_TO_CREATE_FILTERS'=>'Įjunkite norėdami kurti filtrus',
	'LBL_CREATED_IN_OLD_LOOK_CANNOT_BE_EDITED'=>'Ši darbo eiga sukurta pagal ankstesnę išvaizdą. Taip sukurtų sąlygų negalima redaguoti. Galite sąlygas sukurti iš naujo arba naudoti esamas, jų nekeičiant.',
	'LBL_USE_EXISTING_CONDITIONS'=>'Dirbti esamomis sąlygomis',
	'LBL_RECREATE_CONDITIONS'=>'Atkurti sąlygas',
	'LBL_SAVE_AND_CONTINUE'=>'Įrašyti ir tęsti',

	//Step3 edit view
	'LBL_ACTIVE'=>'Aktyvus',
	'LBL_TASK_TYPE'=>'Užduoties tipas',
	'LBL_TASK_TITLE'=>'Užduoties pavadinimas',
	'LBL_ADD_TASKS_FOR_WORKFLOW'=>'Pridėti užduotį darbo eigai',
	'LBL_TASK_TYPE'=>'Užduoties tipas',
	'LBL_EXECUTE_TASK'=>'Vykdyti užduotį',
	'LBL_SELECT_OPTIONS'=>'Pažymėti parinktis',
	'LBL_ADD_FIELD'=>'Pridėti lauką',
	'LBL_ADD_TIME'=>'Pridėti laiką',
	'LBL_TITLE'=>'Pavadinimas',
	'LBL_PRIORITY'=>'Prioritetas',
	'LBL_ASSIGNED_TO'=>'Priskirta:',
	'LBL_TIME'=>'Laikas',
	'LBL_DUE_DATE'=>'Įsipareigojimo data',
	'LBL_THE_SAME_VALUE_IS_USED_FOR_START_DATE'=>'Ta pati reikšmė įrašyta prie pradžios datos',
	'LBL_EVENT_NAME'=>'Įvykio pavadinimas',
	'LBL_TYPE'=>'Tipas',
	'LBL_METHOD_NAME'=>'Metodo pavadinimas',
	'LBL_RECEPIENTS'=>'Gavėjai',
	'LBL_ADD_FIELDS'=>'Pridėti laukus',
	'LBL_SMS_TEXT'=>'SMS žinutės tekstas',
	'LBL_SET_FIELD_VALUES'=>'Nustatyti laukų reikšmes',
	'LBL_ADD_FIELD'=>'Pridėti lauką',
	'LBL_IN_ACTIVE'=>'Tarp aktyvių',
	'LBL_SEND_NOTIFICATION'=>'Siųsti pranešimą',
	'LBL_START_TIME'=>'Pradžios laikas:',
	'LBL_START_DATE'=>'Pradžios data:',
	'LBL_END_TIME'=>'Pabaigos laikas',
	'LBL_END_DATE'=>'Pabaigos data',
	'LBL_ENABLE_REPEAT'=>'Įjungti kartojimą',
	'LBL_NO_METHOD_IS_AVAILABLE_FOR_THIS_MODULE'=>'Šiam moduliui galimų metodų nėra.',
	'LBL_FINISH'=>'Pabaigti',
	'LBL_NO_TASKS_ADDED'=>'Užduočių nėra',
	'LBL_CANNOT_DELETE_DEFAULT_WORKFLOW'=>'Negalite pašalinti numatytosios darbo eigos',
	'LBL_MODULES_TO_CREATE_RECORD' => 'Moduliai kurti įrašui',
	'LBL_EXAMPLE_EXPRESSION'=>'Išraiška',
	'LBL_EXAMPLE_RAWTEXT'=>'Neapdorotas tekstas',
	'LBL_VTIGER'=>'Vtiger',
	'LBL_EXAMPLE_FIELD_NAME'=>'Laukas',
	'LBL_NOTIFY_OWNER'=>'Informuoti savininką',
	'LBL_ANNUAL_REVENUE'=>'Metinės pajamos:',
	'LBL_EXPRESSION_EXAMPLE2'=>"if mailingcountry == 'India' then concat(firstname,' ',lastname) else concat(lastname,' ',firstname) end"

	
);

$jsLanguageStrings = array(
	'JS_STATUS_CHANGED_SUCCESSFULLY'=>'Būsena pakeista sėkmingai',
	'JS_TASK_DELETED_SUCCESSFULLY'=>'Užduotis pašalinta sėkmingai ',
	'JS_SAME_FIELDS_SELECTED_MORE_THAN_ONCE'=>'Tie patys laukai pasirinkti kelis kartus',
	'JS_WORKFLOW_SAVED_SUCCESSFULLY'=>'Darbo eiga įrašyta sėkmingai'
);
