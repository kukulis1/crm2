<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'SINGLE_Settings:Webforms'=>'Saityno forma',
	//Basic Field Names
	'WebForm Name'=>'Saityno formos pavadinimas',
	'Public Id'=>'Viešas ID',
	'Enabled'=>'Būsena',
	'Module'=>'Modulis',
	'Return Url'=>'Grąžinti URL',
	'Post Url'=>'Skelbti URL',
	'SINGLE_Webforms'=>'Saityno forma',

	//Actions
	'LBL_SHOW_FORM'=>'Rodyti formą',
	'LBL_DUPLICATES_EXIST'=>'Toks saityno formos pavadinimas jau egzistuoja',

	//Blocks
	'LBL_WEBFORM_INFORMATION'=>'Saityno formos informacija',
	'LBL_FIELD_INFORMATION'=>'Lauko infromacija:',
	'LBL_FIELD_NAME'=>'Lauko pavadinimas',
	'LBL_OVERRIDE_VALUE'=>'Nepaisoma reikšmė',
	'LBL_MANDATORY'=>'Privalomas',
	'LBL_WEBFORM_REFERENCE_FIELD'=>'Saityno formų nuorodų laukas',
	'LBL_SELECT_FIELDS_OF_TARGET_MODULE'=>'Pažymėkite laukus pasirinktam moduliui',
	'LBL_ALLOWS_YOU_TO_MANAGE_WEBFORMS'=>'Leidžia tvarkyti saityno formas',
	'LBL_ADD_FIELDS'=>'Pridėti laukus',
	'LBL_EMBED_THE_FOLLOWING_FORM_IN_YOUR_WEBSITE'=>'Įtraukti šią formą į savo svetainę',
	'LBL_SELECT_VALUE'=>'Pažymėti reikšmes',
	'LBL_LABEL'=>'žymė',
);
$jsLanguageStrings = array(
	'JS_WEBFORM_DELETED_SUCCESSFULLY'=>'Saityno forma pašailinta sėkmingai',
	'JS_LOADING_TARGET_MODULE_FIELDS'=>'Įkeliami pasirinktų modulių laukai',
	'JS_SELECT_AN_OPTION'=>'Pažymėkite parinktį',
	'JS_LABEL' => 'žymė',
);
