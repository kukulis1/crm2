<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'CronTasks'=>'Tvarkaraštis',

	//Basic Field Names
	'Id'=>'ID ',
	'Cron Job' => 'Periodinis darbas',
	'Frequency'=>'Dažnis',
	'Status'=>'Būsena',
	'Last Start'=>'Paskutinis skenavinmas pradėtas',
	'Last End'=>'Paskutinis skenavinmas baigtas',
	'Sequence'=>'Seka',

	//Actions
	'LBL_COMPLETED'=>'Žymėti kaip užbaigtą',
	'LBL_RUNNING'=>'Veikia',
	'LBL_ACTIVE'=>'Aktyvus',
	'LBL_INACTIVE'=>'Tarp aktyvių',
);
