<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'MailConverter'=>'Laiškų keitiklis',
	'MailConverter_Description'=>'Konvertuoti laiškus į atitinkamus įrašus',
	'MAILBOX'=>'Pašto dėžutė',
	'RULE'=>'Taisyklė',
	'LBL_ADD_RECORD'=>'Pridėti pašto dėžutę',
	'ALL'=>'Visi',
	'UNSEEN'=>'Neperskaityti',
	'LBL_MARK_READ'=>'Žymėti kaip perskaitytą',
	'SEEN'=>'Skaityti',
	'LBL_EDIT_MAILBOX'=>'Redaguoti pašto dėžutę',
    'LBL_CREATE_MAILBOX'=>'Sukurti pašto dėžutę',
	'LBL_BACK_TO_MAILBOXES'=>'Atgal į pašto dėžutę',
	'LBL_MARK_MESSAGE_AS'=>'Žymėti žinutę kaip',
	
	//Server Messages
	'LBL_MAX_LIMIT_ONLY_TWO'=>'Galima sukonkonfigūruoti tik dvi pašto dėžutes',
	'LBL_IS_IN_RUNNING_STATE'=>'Veikimo būsenoje',
	'LBL_SAVED_SUCCESSFULLY'=>'Įrašyta sėkmingai',
	'LBL_CONNECTION_TO_MAILBOX_FAILED'=>'Prisijungti prie pašto dėžutės nepavyko',
	'LBL_DELETED_SUCCESSFULLY'=>'Pašalinta sėkmingai',
	'LBL_RULE_DELETION_FAILED'=>'Taisyklės pašalinimas nepavyko',
	'LBL_RULES_SEQUENCE_INFO_IS_EMPTY'=>'Taisyklių sekos informacija neužpildyta',
	'LBL_SEQUENCE_UPDATED_SUCCESSFULLY'=>'Seka atnaujinta sėkmingai',
	'LBL_SCANNED_SUCCESSFULLY'=>'Nuskenuota sėkmingai',

	//Field Names
	'scannername'=>'Skenerio pavadinimas',
	'server'=>'Serverio pavadinimas',
	'protocol'=>'Protokolas',
	'username'=>'Vartotojo vardas',
	'password'=>'Slaptažodis',
	'ssltype'=>'SSL tipas',
	'sslmethod'=>'SSL metodas',
	'connecturl'=>'Prijungti URL',
	'searchfor'=>'Ieškoti',
	'markas'=>'Po skenavimo',

	//Field values & Messages
	'LBL_ENABLE'=>'Įjungti',
	'LBL_DISABLE'=>'Išjungti',
	'LBL_STATUS_MESSAGE'=>'Pažymėti, kad taptų aktyvus',
	'LBL_VALIDATE_SSL_CERTIFICATE'=>'Patvirtinti SSL sertifikatą',
	'LBL_DO_NOT_VALIDATE_SSL_CERTIFICATE'=>'Nepatvirtinti SSL sertifikato',
	'LBL_ALL_MESSAGES_FROM_LAST_SCAN'=>'Visos praeito skenavimo žinutės',
	'LBL_UNREAD_MESSAGES_FROM_LAST_SCAN'=>'Neperskaityti nuo paskutinės peržiūros',
	'LBL_MARK_MESSAGES_AS_READ' => 'Žymėti žinutes kaip perskaitytas',
	'LBL_I_DONT_KNOW'=>"I don't know",

	//Mailbox Actions
	'LBL_SCAN_NOW'=>'Skenuoti dabar',
	'LBL_RULES_LIST'=>'Taisyklių sąrašas',
	'LBL_SELECT_FOLDERS'=>'Pasirinkite katalogus',

	//Action Messages
	'LBL_DELETED_SUCCESSFULLY'=>'Pašalinta sėkmingai',
	'LBL_RULE_DELETION_FAILED'=>'Taisyklės pašalinimas nepavyko',
	'LBL_SAVED_SUCCESSFULLY'=>'Įrašyta sėkmingai',
	'LBL_SCANED_SUCCESSFULLY'=>'Nuskenuota sėkmingai',
	'LBL_IS_IN_RUNNING_STATE'=>'yra veikimo būsenoj',
	'LBL_FOLDERS_INFO_IS_EMPTY'=>'Katalogų informacija neužpildyta',
	'LBL_RULES_SEQUENCE_INFO_IS_EMPTY'=>'Taisyklių sekos informacija neužpildyta',

	//Folder Actions
	'LBL_UPDATE_FOLDERS'=>'Atnaujinti katalogus',

	//Rule Fields
	'fromaddress'=>'Nuo',
	'toaddress'=>'Kam',
	'subject'=>'Tema ',
	'body'=>'Pagrindinė dalis',
	'matchusing'=>'Pritaikyti',
	'action'=>'Veiksmas',

	//Rules List View labels
	'LBL_PRIORITY'=>'Prioritetas',
	'PRIORITISE_MESSAGE'=>'Tempkite blokus norėdami išdėlioti taisykles pagal prioritetus',

	//Rule Field values & Messages
	'LBL_ALL_CONDITIONS'=>'Visos sąlygos',
	'LBL_ANY_CONDITIOn'=>'Bet kokia sąlyga',

	//Rule Conditions
	'Contains'=>'turi simbolius:',
	'Not Contains'=>'nėra teksto:',
	'Equals'=>'Lygu',
	'Not Equals'=>'nelygu',
	'Begins With'=>'Pradėti',
	'Ends With'=>'Pabaiga',
	'Regex'=>'Reguliarusis reiškinys',

	//Rule Actions
	'CREATE_HelpDesk_FROM'=>'Sukurti kortelę',
	'UPDATE_HelpDesk_SUBJECT'=>'Atnaujinti kortelę',
	'LINK_Contacts_FROM'=>'Pridėti prie adresatų (NUO)',
	'LINK_Contacts_TO'=>'Pridėti prie adresatų (KAM)',
	'LINK_Accounts_FROM'=>'Pridėti prie organizacijų (NUO)',
	'LINK_Accounts_TO'=>'Pridėti prie organizacijų (KAM)',
    
    //Select Folder
    'LBL_UPDATE_FOLDERS'=>'Atnaujinti katalogus',
    'LBL_UNSELECT_ALL'=>'Nežymėti nieko',
	
	//Setup Rules
	'LBL_CONVERT_EMAILS_TO_RESPECTIVE_RECORDS'=>'Konvertuoti laiškus į atitinkamus įrašus',
	'LBL_DRAG_AND_DROP_BLOCK_TO_PRIORITISE_THE_RULE'=>'Tempkite blokus norėdami išdėlioti taisykles pagal prioritetus',
	'LBL_ADD_RULE'=>'Pridėti taisyklę',
	'LBL_PRIORITY'=>'Prioritetas',
	'LBL_DELETE_RULE'=>'Pašalinti taisyklę',
	'LBL_BODY'=>'Pagrindinė dalis',
	'LBL_MATCH'=>'Pritaikyti',
	'LBL_ACTION'=>'Veiksmas',
	'LBL_FROM'=>'Nuo',
);
$jsLanguageStrings = array(
	'JS_MAILBOX_DELETED_SUCCESSFULLY'=>'Pašto dėžutės įrašas pašalintas sėkmingai',
	'JS_MAILBOX_LOADED_SUCCESSFULLY'=>'Pašto dėžutės įrašas įkeltas sėkmingai'
);	
