<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Profiles'=>'Profiliai',
	'SINGLE_Profiles'=>'Profilis',
	'LBL_ADD_RECORD'=>'Pridėti profilį',
	'LBL_CREATE_PROFILE'=>'Sukurti profilį',
	'LBL_PROFILE_NAME'=>'Sukurti naują profilį:',
	'LBL_DESCRIPTION'=>'2 žingsnis: Pridėkite aprašymą',
	'LBL_EDIT_PRIVILIGES_FOR_THIS_PROFILE'=>'Redaguoti šio profilio privilegijas',
	'LBL_MODULES'=>'Moduliai',
	'LBL_PROFILE_VIEW'=>'Profilio peržiūra',
	'LBL_FIELDS'=>'Laukai',
	'LBL_TOOLS'=>'Įrankiai',
	'LBL_FIELD_AND_TOOL_PRVILIGES'=>'Lauko ir įrankio privilegijos',
	'LBL_EDIT_RECORD'=>'Redaguoti  ',
	'LBL_DUPLICATE_RECORD'=>'Dubliuoti',
	'LBL_DELETE_RECORD'=>'Pašalinti',
	
	'LBL_VIEW_PRVILIGE' => 'Peržiūra',
	'LBL_EDIT_PRVILIGE'=>'Kurti/redaguoti',
	'LBL_DELETE_PRVILIGE'=>'Pašalinti',
	'LBL_INIVISIBLE'=>'Nematomas',
	'LBL_READ_ONLY'=>'Tik skaitomas',
	'LBL_WRITE'=>'Rašyti',
	
	'LBL_DELETE_PROFILE'=>'Pašalinti profilį',
	'LBL_TRANSFER_ROLES_TO_PROFILE'=>'Perkelti vaidmenis profiliui',
	'LBL_PROFILES'=>'Profiliai',
	'LBL_CHOOSE_PROFILES'=>'Pasirinkti profilius',
    
    'LBL_VIEW_ALL'=>'Peržiūrėti viską',
    'LBL_EDIT_ALL'=>'Redaguoti visus',
    'LBL_VIEW_ALL_DESC'=>'Gali peržiūrėti visų modulių informaciją',
    'LBL_EDIT_ALL_DESC'=>'Gali redaguoti visų modulių informaciją',
	'LBL_DUPLICATES_EXIST'=>'Egzistuoja vienodi profiliai',
	
);

$jsLanguageStrings = array(
	'JS_RECORD_DELETED_SUCCESSFULLY'=>'Profilis pašalintas sėkmingai',
);
