<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Roles'=>'Vaidmenys',
	'SINGLE_Roles'=>'Vaidmuo ',
	'LBL_ADD_RECORD'=>'Pridėti vaidmenį',
	'LBL_DELETE_ROLE'=>'Pašalinti vaidmenį',
	'LBL_TRANSFER_OWNERSHIP'=>'Perkelti savininkystę',
	'LBL_TO_OTHER_ROLE'=>'Kitam vaidmeniu',
	'LBL_CLICK_TO_EDIT_OR_DRAG_TO_MOVE'=>'Spustelėkite, norėdami redaguoti / vilkite, norėdami perkelti',
	'LBL_ASSIGN_ROLE'=>'Priskirti vaidmenį',
	'LBL_CHOOSE_PROFILES'=>'Pasirinkti profilius',
	'LBL_COPY_PRIVILEGES_FROM'=>'Kopijuoti privilegijas iš',
	
	//Edit View
	'LBL_PROFILE'=>'Profilis',
	'LBL_REPORTS_TO'=>'Ataskaita pagal',
	'LBL_NAME'=>'Vardas',
	'LBL_ASSIGN_NEW_PRIVILEGES'=>'Priskirti privilegijas tiesiogiai vaidmeniui',
	'LBL_ASSIGN_EXISTING_PRIVILEGES'=>'Priskirti privilegijas iš egzistuojančių profilių',
	'LBL_PRIVILEGES'=>'Privilegijos',
	'LBL_DUPLICATES_EXIST'=>'Egzistuoja vienodos rolės',
	
	//Assign Records to
	'LBL_CAN_ASSIGN_RECORDS_TO'=>'Gali priskirti įrašus',
	'LBL_ALL_USERS' => 'Visi vartotojai',
	'LBL_USERS_WITH_LOWER_LEVEL'=>'Vartotojai su pavaldžiu vaidmeniu',
	'LBL_USERS_WITH_SAME_OR_LOWER_LEVEL'=>'Vartotojai su tuo pačiu arba pavaldžiu vaidmeniu',
);

$jsLanguageStrings = array(
	'JS_PERMISSION_DENIED'=>'Leidimai neduoti',
	'JS_NO_PERMISSIONS_TO_MOVE'=>'Teisių perkelti nesuteikta',
);
