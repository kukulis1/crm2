<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	//Actions
	'LBL_SAVED_SUCCESSFULLY'=>'Įrašyta sėkmingai',
	'LBL_DELETED_SUCCESSFULLY'=>'Pašalinta sėkmingai',
	'LBL_INVALID_MAPPING'=>'Netinkamas atvaizdavimas',
	'LBL_CONVERT_LEAD_FIELD_MAPPING'=>'Konvertuoti galimo kliento atvaizdavimą',
	'LBL_FIELD_LABEL'=>'Lauko žymė',
	'LBL_FIELD_TYPE'=>'Lauko tipas',
	'LBL_MAPPING_WITH_OTHER_MODULES'=>'Atvaizdavimas su kitais moduliais',
	'LBL_ORGANIZATIONS'=>'Organizacijos',
	'LBL_CONTACTS'=>'Adresatai',
	'LBL_OPPURTUNITIES'=>'Galimybės',
	'LBL_ADD_MAPPING'=>'Pridėti planą',
	'LBL_NONE'=>'N',
	
	//Field Type Translation
	'phone'=>'Telefonas',
	'picklist'=>'Atrinkčių sąrašas',
	'email'=>'El. paštas',
	'text'=>'Eilutė',
	'currency'=>'Valiuta',
	'multiSelectCombo'=>'Multi-Select Combo Box',
	'time'=>'Laikas',
	'textArea'=>'Teksto sritis',
	'url'=>'URL',
	'string'=>'Eilutė',
	'checkBox'=>'Žymimasis langelis',
	'date'=>'Data',
	'decimal'=>'Dešimtainis',
	'percent'=>'Procentas',
	'skype'=>'Skype',
);
$jsLanguageStrings = array(
	'JS_NONE'=>'N',
	'JS_IS_ALREADY_BEEN_MAPPED'=>'tai jau atvaizduota',
	'JS_CANT_MAP'=>'Negalima atvaizduoti',
	'JS_WITH'=>'su',
	'JS_MAPPING_DELETED_SUCCESSFULLY'=>'Atvaizdavimas pašalintas sėkmingai',
	
	//JS Field Type Translation
	'JS_phone'=>'Telefonas',
	'JS_picklist'=>'Atrinkčių sąrašas',
	'JS_email'=>'El. paštas',
	'JS_text'=>'Eilutė',
	'JS_currency'=>'Valiuta',
	'JS_multiSelectCombo' => 'Multi-Select Combo Box',
	'JS_time'=>'Laikas',
	'JS_textArea'=>'Teksto sritis',
	'JS_url'=>'URL',
	'JS_string'=>'Eilutė',
	'JS_checkBox'=>'Žymimasis langelis',
	'JS_date'=>'Data',
	'JS_decimal'=>'Dešimtainis',
	'JS_percent'=>'Procentas',
	'JS_skype'=>'Skype',
	'JS_None'=>'N',
	'JS_integer'=>'Sveikasis skaičius',
);
