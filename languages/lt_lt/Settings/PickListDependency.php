<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'LBL_ADD_RECORD'=>'Pridėti atrinkčių sąrašo priklausomybę',
	'LBL_PICKLIST_DEPENDENCY'=>'Atrinkčių sąrašo priklausomybė',
	'LBL_SELECT_MODULE'=>'Pažymėkite modulį',
	'LBL_SOURCE_FIELD'=>'Kilmės laukas',
	'LBL_TARGET_FIELD'=>'Paskirties laukas',
	'LBL_SELECT_FIELD'=>'Pasirinkite lauką',
	'LBL_CONFIGURE_DEPENDENCY_INFO'=>'Spustelėkite atitinkamą langelį norėdami pakeisti norimo lauko atrinkčių sąrašo verčių atvaizdavimą',
	'LBL_CONFIGURE_DEPENDENCY_HELP_1'=>'Tik atvaizduotos atrinkčių sąrašo reikšmės bus rodomos žemiau (išskyrus pirmąjį kartą).',
	'LBL_CONFIGURE_DEPENDENCY_HELP_2'=>"If you want to see or change the mapping for the other picklist values of Source field, <br/>
										then you can select the values by clicking on <b>'Select Source values'</b> button on the right side",
	'LBL_CONFIGURE_DEPENDENCY_HELP_3'=>'Pažymėtos paskirties lauko reikšmės paryškintos kaip',
	'LBL_SELECT_SOURCE_VALUES'=>'Pasirinkite pradines reikšmes',
	'LBL_SELECT_SOURCE_PICKLIST_VALUES'=>'Pasirinkite atrinkčių sąrašo pradines reikšmes',
	'LBL_ERR_CYCLIC_DEPENDENCY' => 'Tokia priklausomybių konfigūracija neleistina, nes yra cikliška.',
);

$jsLanguageStrings = array(
	'JS_LBL_ARE_YOU_SURE_YOU_WANT_TO_DELETE'=>'Ar tikrai norite pašalinti šią atrinkčių sąrašo priklausomybę?',
	'JS_DEPENDENCY_DELETED_SUEESSFULLY'=>'Priklausomybė pašalinta sėkmingai',
	'JS_PICKLIST_DEPENDENCY_SAVED'=>'Atrinkčių sąrašo priklausomybė įrašyta',
    'JS_DEPENDENCY_ATLEAST_ONE_VALUE'=>'Turite pasirinkti bent vieną reikšmę',
	'JS_SOURCE_AND_TARGET_FIELDS_SHOULD_NOT_BE_SAME'=>'Kilmės ir tikslo laukas neturi būti vienodi',
	'JS_SELECT_SOME_VALUE'=>'Pasirinkite reikšmę'
);
