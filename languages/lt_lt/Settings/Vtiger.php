<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'Settings'=>'Nuostatos',
	'LBL_SEARCH_SETTINGS_PLACEHOLDER'=>'Paieškos nuostatos',
	'LBL_SUMMARY'=>'Santrauka',
	'LBL_ACTIVE_USERS'=>'Vartotojai',
	'LBL_WORKFLOWS_ACTIVE'=> 'Automatiniai veiksmai',
	'LBL_MODULES'=>'Moduliai',
	'LBL_SETTINGS_SHORTCUTS'=>'Nuostatų trumpinys',
	'LBL_UNPIN' => 'Nusegti',
	'LBL_PIN'=>'Pritvirtinti',
	'LBL_WORKFLOWS2' => 'automatiniai veiksmai',
	'LBL_EDIT_WORKFLOWS' => '%s automatiniai veiksmai',


	// Blocks
	'LBL_STUDIO'=>'Studija',
	'LBL_COMMUNICATION_TEMPLATES'=>'Šablonai',
	'LBL_USER_MANAGEMENT'=>'Vartotojų ir prieigos tvarkymas',
	'LBL_ACCESS_MANAGEMENT'=>'Prieigos valdymas',
	'LBL_MODULE_MANAGER'=>'Modulių tvarkymas',
	'LBL_NOTIFICATIONS'=>'Pranešimai',
	'LBL_EXTERNAL_SERVER_SETTINGS'=>'Išorinio serverio nuostatos',
	'LBL_OTHER_SETTINGS'=>'Kitos nuostatos',
	'LBL_EXTENSIONS_PLACE'=>'Plėtinių vieta',
	'LBL_EXTENSIONS'=>'Plėtiniai',

	// Fields
	'LBL_DESCRIPTION'=>'2 žingsnis: Pridėkite aprašymą',

	// Other Strings
	'LBL_USER_DESCRIPTION'=>'Tvarkyti sistemos vartotojus',
	'LBL_ROLES'=>'Vaidmenys',
	'LBL_ROLE_DESCRIPTION'=>'Nustatyti vaidmenų hierarchiją ir priskirkite vartotojams',

	'LBL_PROFILES'=>'Profiliai',
	'LBL_PROFILE_DESCRIPTION'=>'Tvarkyti individualizuotą modulių prieigą skirtingo vaidmens vartotojas.',

	'USERGROUPLIST'=>'Grupių',
	'LBL_GROUP_DESCRIPTION'=>'Tvarkyti skirtingus komandų tipus (pagal vaidmenis, vartotojus ir profilius)',

	'LBL_SHARING_ACCESS'=>'Bendrinimo taisyklės',
	'LBL_SHARING_ACCESS_DESCRIPTION'=>'Tvarkyti modulių bendrinimo ir individualizuoto bendrinimo taisykles',

	'LBL_WORKFLOW_LIST'=>'Darbo eigų sąrašąs',
	'LBL_AVAILABLE_WORKLIST_LIST'=>'Galimos darbo eigos',
	'LBL_FIELDFORMULAS'=>'Lauko formulės',
	'LBL_FIELDFORMULAS_DESCRIPTION'=>'Pridėti individualizuotas lygtis pasirinktiems laukams',
    
	'VTLIB_LBL_MODULE_MANAGER'=>'Modulių tvarkyklė',
	'VTLIB_LBL_MODULE_MANAGER_DESCRIPTION'=>'Tvarkyti modulių elgseną sistemoje Vtiger',
	'LBL_PICKLIST_EDITOR'=>'Atrinkčių sąrašo redaktorius',
	'LBL_PICKLIST_DESCRIPTION'=>'Individualizuoti atrinkčių sąrašų reikšmes kiekviename modulyje',
	'LBL_PICKLIST_DEPENDENCY_SETUP'=>'Atrinkčių sąrašo priklausomybės diegimas',
	'LBL_PICKLIST_DEPENDENCY_DESCRIPTION'=>'Nustatyti atrinkčių sąrašų priklausomybę kiekviename modulyje',

	'LBL_FIELDFORMULAS'=>'Lauko formulės',
	'LBL_FIELDFORMULAS_DESCRIPTION'=>'Pridėti individualizuotas lygtis pasirinktiems laukams',

	'LBL_MENU_EDITOR'=>'Meniu redaktorius',
	'LBL_MENU_DESC'=>'Individualizuoti meniu juostos seką',

	'EMAILTEMPLATES'=>'Laiškų šablonai',
	'LBL_EMAIL_TEMPLATE_DESCRIPTION'=>'Tvarkyti pašto dėžutės modulio šablonus',

	'LBL_OTHER_SETTINGS' => 'Kiti nustatymai',
	'LBL_SELF_SERVICE_MESSAGE' => 'Savitarnos žinutės',
	'LBL_PRICE_COUNT_SETTINGS' => 'Bendri nustatymai',
	'LBL_NOTIFICATIONS_CENTER' => 'Pranešimų centras',
	'LBL_CARGO_NOT_STANDART' => 'Krovinys nestandartas nuo',
	'LBL_CARGO_MEASURE' => 'Mato vienetai',
	'LBL_METERS' => 'metrų',
	'LBL_PAY_INVOICE_REMINDER' => 'Sąskaitos apmokėjimo priminimo tekstas',
	'LBL_DEBTS_REMINDER' => 'Skolos apmokėjimo raginimo tekstas',
	'LBL_SEND_INVOICE_TEXT' => 'Sąskaitos siuntimo tekstas',
	'LBL_LOAD_WORK_PRICE' => 'Krovos darbų įprastinė kaina',
	'LBL_IDLE_PRICE' => 'Prastovų įprastinė kaina',
	'LBL_SEND_STICKERS_TEXT' => 'Lipdukų siuntimo tekstas',
	'LBL_WRONG_ADDRESS_PRICE' => 'Neteisingo adreso įprastinė kaina (%)',
	'LBL_BLOCKED_ENTRANCE_PRICE' => 'Negalimo privažiavimo įprastinė kaina (%)',
	'LBL_NOT_PREPARED_PRICE' => 'Neparuošto krovinio įprastinė kaina (%)',
	'LBL_WRONG_PACKAGE_PRICE' => 'Netinkamai supakuoto krovinio įprastinė kaina (%)',
	'LBL_BAD_PACKAGE_PRICE' => 'Užsakymo neatitinkačio krovinio įprastinė kaina (%)',
	'LBL_NOT_PRESENT_PRICE' => 'Vietoje nebūnant gavėjui ar siuntėjui įprastinė kaina (%)',
	'LBL_PARTIAL_DELIVERY_PRICE' => 'Dalinio pristatymo įprastinė kaina (%)',
	'LBL_PARTIAL_LOADING_PRICE' => 'Dalinio pakrovimo įprastinė kaina (%)',
	'LBL_INVOICE_PDF_SAVE_TIME' => 'Sąskaitos PDF kliento atsisiuntimui saugojimo dienos',
	'LBL_ROAD_TAX_FEE' => 'Kelių mokestis',
	'LBL_SALARY_TAX' => 'Atlyginimo priemoka (%)',
	'LBL_INFLATION_TAX' => 'Atsarginės-infliacija (%)',


	'LBL_COMPANY_DETAILS'=>'Įmonės išsami informacija',
	'LBL_COMPANY_DESCRIPTION'=>'Nurodykite įmonės adresą',

	'LBL_MAIL_MERGE'=>'Laiškų suliejimas',
	'LBL_MAIL_MERGE_DESCRIPTION'=>'Tvarkyti laiškų suliejimo šablonus',

	'LBL_TAX_SETTINGS'=>'Mokesčių skaičiuoklė',
	'LBL_TAX_DESCRIPTION'=>'Tvarkyti mokesčius ir atitinkamus mokesčių kursus',

	'LBL_MAIL_SERVER_SETTINGS'=>'Išsiunčiamų laiškų serveris',
	'LBL_MAIL_SERVER_DESCRIPTION'=>'Konfigūruoti išsiunciamųjų laiškų serverio išsamią informaciją',

	'INVENTORYTERMSANDCONDITIONS'=>'Inventorius: Terminai ir sąlygos',
	'LBL_INV_TANDC_DESCRIPTION'=>'Nurodykite terminus ir sąlygas pasiūlymams, Važtaraščiams, pardavimų užsakymams.',

	'LBL_ANNOUNCEMENT_DESCRIPTION'=>'Tvarkyti pranešimus įmonei',

	'LBL_CURRENCY_SETTINGS'=>'Valiutos',
	'LBL_CURRENCY_DESCRIPTION'=>'Tvarkyti tarptautines valiutas ir jų kursus',

	'LBL_CUSTOMIZE_MODENT_NUMBER'=>'Individualizuoti įrašų numeravimą',
	'LBL_CUSTOMIZE_MODENT_NUMBER_DESCRIPTION'=>'Modulio objekto numerio individualizavimas',

	'LBL_MAIL_SCANNER'=>'Laiškų keitiklis',
	'LBL_MAIL_SCANNER_DESCRIPTION'=>'Konfigūruoti pašto dėžutę skenavimui ir laiškų priedams',

	'LBL_LIST_WORKFLOWS'=>'Automatiniai veiksmai',
	'LBL_LIST_WORKFLOWS_DESCRIPTION'=>'Kurti ir redaguoti automatinius veiksmus',
    'LBL_SELECT_DATE'=>'Pasirinkite datas',

	'Configuration Editor'=>'Konfigūracijos rengyklė',
	'Update configuration file of the application'=>'Atnaujinti programos konfigūracijos failą',

	'LBL_CUSTOMER_PORTAL'=>'Klientų portalas',
	'PORTAL_EXTENSION_DESCRIPTION'=>'Leidžia diegti klientų portalo įskiepį',
	
	'LBL_LAYOUT_EDITOR_DESCRIPTION'=>'Keisti kiekvieno modulio išdėstymą',
	'LBL_WEBFORMS_DESCRIPTION'=>'Kurti ir tvarkyti interneto formas, naudojamas galimiems klientams rasti',
	'LBL_TO_ADD_NEW_USER_PLEASE_CLICK_ON'=>'Norėdami pridėti naują vartotoją spauskite',
	'LBL_SETTINGS_ICON'=>'Nuostatų ikona',
	'LBL_ADD_OR_DELETE_USERS'=>'Pridėti/Pašalinti vartotojus',
	'LBL_ADD_USER'=>'Pridėti vartotoją',
	

	'ModTracker'=>'Modulių sekiklis',
	'LBL_MODTRACKER_DESCRIPTION'=>'Pasirinkite modulius, kuriuos seksite',

	'Scheduler'=>'Tvarkaraštis',
	'Allows you to Configure Cron Task'=>'Leidžia konfigūruoti periodinį darbą',

	'Webforms'=>'Saityno formos',
	'Allows you to manage Webforms'=>'Tvarkyti saityno formas',
	'LBL_CREATING_NEW'=>'Kuriamas naujas',
	'LBL_EDITING'=>'Vartotojo redagavimas',
	'LBL_EDIT_RECORD'=>'Redaguoti  ',
	'LBL_DELETE_RECORD'=>'Pašalinti',

	//Menu Editor
	'LBL_ADD_MOVE_MENU_ITEMS'=>'Pridėti/Perkelti meniu elementus',
	'LBL_ADD_MENU_ITEM'=>'Pridėti atvaizdavimą',
	'LBL_MAX'=>'Daugiausiai',
	
	//Tax Calculations
	'LBL_TAX_CALCULATIONS'=>'Mokesčių skaičiuoklė',
	'LBL_TAX_DESC'=>'Tvarkyti įvairius mokesčių kursus, tokius kaip pardavimų mokestis, PVM ie pan.',
	'LBL_PRODUCT_SERVICE_TAXES'=>'Prekių ir paslaugų mokesčiai',
	'LBL_SHIPPING_HANDLING_TAXES'=>'Pristatymo ir tvarkymo mokesčiai',
	'LBL_ADD_NEW_TAX'=>'Pridėti naują mokestį',
	'LBL_EDIT_TAX'=>'Redaguoti mokestį',
	'LBL_TAX_NAME'=>'Mokesčio pavadinimas',
	'LBL_TAX_VALUE'=>'Mokesčio vertė',
	'LBL_STATUS'=>'Būsena',
	'LBL_TAX_STATUS_DESC'=>'Pažymėkite mokestį, norėdami jį aktyvuoti',
	'LBL_ENTER_TAX_NAME'=>'Įrašykite mokesčio pavadinimą',
	'LBL_ENTER_TAX_VALUE'=>'Įrašykite mokesčio vertę',
	'LBL_TAX_NAME_EXIST'=>'Toks mokesčio pavadinimas jau egzistuoja',
	
	//Terms & conditions
	'LBL_TERMS_AND_CONDITIONS'=>'Terminai ir sąlygos',
	'LBL_SPECIFY_TERMS_AND_CONDITIONS'=>'Nurodykite terminus ir sąlygas',
	//Announcements
	'LBL_ANNOUNCEMENTS'=>'Skelbimai',
	'LBL_ENTER_ANNOUNCEMENT_HERE'=>'Įveskite skelbimą čia',
	'LBL_ANNOUNCEMENT_DESC'=>'Pakeisti tekstą, esantį pranešimo kiekvieno puslapio viršuje',

	//Outgoing Server
	'LBL_RESET_TO_DEFAULT'=>'Atstatyti numatytą',
	'LBL_SERVER_NAME'=>'Serverio pavadinimas',
	'LBL_USER_NAME'=>'Vartotojo vardas',
	'LBL_PASSWORD'=>'Slaptažodis',
	'LBL_FROM_EMAIL'=>'Iš el. pašto',
	'LBL_REQUIRES_AUTHENTICATION'=>'Reikalauja patvirtinimo',
	'LBL_OUTGOING_SERVER'=>'Išsiunčiamų laiškų serveris',
	'LBL_OUTGOING_SERVER_DESC'=>'Konfigūruoti išsiunciamųjų laiškų serverio išsamią informaciją',
	'LBL_DEFAULT'=>'Numatyta',
	'LBL_OPTIONS1'=>'Send emails from your mail server<br>
					- Enter the SMTP account details (Server Name, User Name, Password) for the same account as entered in the FROM email address.',
	'LBL_OPTIONS2'=>"Add od1.vtiger.com server as a authorized sender for your domain.<br>
					- If you prefer to retain the vtiger server as the outgoing server, we recommend that you configure your domain's SPF record to include the vtiger server as a valid sender for your domain.
					  For additonal instruction on setting up SPF record, please email od-support@vtiger.com .",
	'LBL_MAIL_SERVER_SMTP'=>'Laiškų serverio nuostatos (SMTP)',
	'LBL_OUTGOING_SERVER_FROM_FIELD'=>'Pastaba: jei neužpildytas siuntėjo el. paštas, tuomet jis bus automatiškai užpildomas įrašant vartotojo el. paštą.',
	'LBL_TESTMAILSTATUS'=>'Test Mail Status : ',
	'LBL_MAILSENDERROR'=>'Laiško administratoriui nusiųsti nepavyko. Patikrinkite administratoriaus Mail could not be sent to the admin user. Please check the admin emailid/Server settings',
	
	//Configuration Editor
	'LBL_CONFIG_EDITOR'=>'Konfigūracijos rengyklė',
	'LBL_CONFIG_DESCRIPTION'=>'Redaguoti sistemos konfigūracijos išsamią informaciją',
	'LBL_CONFIG_FILE'=>'config.inc.php',
	'LBL_MB'=>'MB',

	//Config Editor Fields
	'LBL_MINI_CALENDAR_DISPLAY'=>'Mini kalendoriaus rodymas',
	'LBL_WORLD_CLOCK_DISPLAY'=>'Pasaulio laikrodžio rodymas',
	'LBL_CALCULATOR_DISPLAY'=>'Skaičiuotuvo rodymas',
	'LBL_USE_RTE'=>'Naudoti RTE',
	'LBL_HELPDESK_SUPPORT_EMAILID'=>'Konsultantų pagalbos el. pašto ID',
	'LBL_HELPDESK_SUPPORT_NAME'=>'Konsultantų pagalbos pavadinimas',
	'LBL_MAX_UPLOAD_SIZE'=>'Didžiausia galima išsiunčiamo failo apimtis (iki 5MB)',
	'LBL_MAX_HISTORY_VIEWED'=>'Registro limitas peržiūrai',
	'LBL_DEFAULT_MODULE'=>'Numatytasis modulis',
	'LBL_MAX_TEXT_LENGTH_IN_LISTVIEW'=>'Didžiausias teksto ilgis sąrašo peržiūroje',
	'LBL_MAX_ENTRIES_PER_PAGE_IN_LISTVIEW'=>'Daugiausiai eilučių puslapyje',
	'LBL_INVALID_EMAILID'=>'Netinkamas el. pašto ID',
	'LBL_INVALID_SUPPORT_NAME'=>'Netinkamas vardas',
	'LBL_INVALID_MODULE'=>'Netinkamas modulis',
	'LBL_INVALID_NUMBER'=>'Netinkamas numeris',
	'LBL_FIELDS_INFO_IS_EMPTY'=>'Laukų informacija neužpildyta',

	//CustomRecordNumbering
	'LBL_SUCCESSFULLY_UPDATED'=>'Sėkmingai atnaujinta',
	'LBL_CUSTOMIZE_RECORD_NUMBERING'=>'Individualizuoti įrašų numeravimą',
	'LBL_MODULE_ENTITY_NUMBER_CUSTOMIZATION'=>'Modulio objekto numerio individualizavimas',
	'LBL_UPDATE_MISSING_RECORD_SEQUENCE'=>'Atnaujinti trūkstamų įrašų seką',
	'LBL_USE_PREFIX'=>'Naudoti priešdėlį',
	'LBL_START_SEQUENCE'=>'Pradėti seką',

	//Company Details
	'organizationname'=>'Įmonės pavadinimas',
	'logoname'=>'Įmonės logotipas',
	'address'=>'Adresas',
	'city'=>'Miestas',
	'state'=>'Savivaldybė',
	'code'=>'Pašto kodas',
	'country'=>'Šalis',
	'phone'=>'Telefonas',
	'fax'=>'Faksas',
	'website'=>'Svetainė',
	'bankname' => 'Banko pavadinimas',
	'bankaccountnum' => 'Banko saskaitos numeris',
	'bankcode' => 'Banko kodas',

	'bankname2' => 'Banko pavadinimas 2',
	'bankaccountnum2' => 'Banko saskaitos numeris 2',
	'bankcode2' => 'Banko kodas 2',
	'companycode' => 'Įmonės kodas',
	'LBL_INVALID_IMAGE'=>'Netinkamas arba duomenų neturintis failas',
	'LBL_IMAGE_CORRUPTED'=>'Aptiktas virusas arba išsiųstas paveikslėlis sugadintas',
	'LBL_LOGO_RECOMMENDED_MESSAGE'=>'Rekomenduojami išmatavimai: 170X60 pikselių ( .jpeg , .jpg , .png , .gif , .pjpeg , .x-png formatai).',
	'LBL_COMPANY_INFORMATION'=>'Įmonės informacija',
	'LBL_UPDATE'=>'Atnaujinimas',
	'LBL_UPDATE_LOGO'=>'Atnaujinti logotipą',
	'LBL_COMPANY_LOGO'=>'Įmonės logotipas',
	'LBL_EDIT_COMPANY_DETAILS_MESSAGE'=>'Norėdami koreguoti įmonės informaciją, spustelėkite nuostatų ikonėlę, tuomet pasirinkite "Pridėti/Pašalinti vartotoją->Įmonė',
    
    'LBL_ACTIVE'=>'Aktyvus',
    'LBL_INACTIVE'=>'Tarp aktyvių',
    
    //show last scan in workflows and mailconverter
    'LBL_DISABLED'=>'Išjungta',
    'LBL_RUNNING'=>'Veikia',
    'LBL_LAST_SCAN_TIMED_OUT'=>'Paskiausias skenavimas nepavyko',
    'LBL_LAST_SCAN_AT'=>'Paskutinį kartą skenuota:',
    'LBL_TIME_TAKEN'=>'užtrukta laiko',
    'LBL_SHORT_SECONDS'=>'sek.',
	
	//Email Templates
	'EmailTemplate'=>'Laiško šablonas',
	'LBL_TEMPLATE_NAME'=>'Šablono pavadinimas',
	'LBL_DESCRIPTION'=>'2 žingsnis: Pridėkite aprašymą',
	'LBL_SUBJECT'=>'Tema ',
	
	//User Login History
	'LoginHistory'=>'Vartotojo prisijungimų žurnalas',
	'LBL_LOGIN_HISTORY_DETAILS'=>'Vartotojo prisijungimo registras',
	'LBL_LOGIN_HISTORY_DESCRIPTION'=>'Rodyti vartotojų prisijungimo registrą',
	'LBL_USER_NAME'=>'Vartotojo vardas',
	'LBL_USER_IP_ADDRESS'=>'Vartotojo IP adresas', 
	'LBL_LOGIN_TIME'=>'Prisijungimo laikas',
	'LBL_LOGGED_OUT_TIME'=>'Atsijungimo laikas', 
	'LBL_STATUS'=>'Būsena',
	'LBL_LOGIN_HISTORY_DETAILS'=>'Vartotojo prisijungimo registras',
	'LBL_LOGIN_HISTORY_DESCRIPTION'=>'Rodyti vartotojų prisijungimo registrą',
	
	'LBL_CONFIG_EDITOR_DESCRIPTION'=>'Atnaujinti programos konfigūracijos failą',
);

$jsLanguageStrings = array(
	'JS_PLEASE_SELECT_ATLEAST_ONE_MEMBER_FOR_A_GROUP'=>'Pasirinkite bent vieną narį grupei',
	'JS_GROUP_DELETED_SUCCESSFULLY'=>'Grupė pašalinta sėkmingai',
	'JS_TAX_SAVED_SUCCESSFULLY'=>'Mokestis įrašytas sėkmingai',
	'JS_TAX_DISABLED'=>'Mokestis išjungtas',
	'JS_TAX_ENABLED'=>'Mokestis įjugtas',
	'JS_EDIT'=>'Redaguoti  ',
	'JS_ANNOUNCEMENT_SAVED'=>'Skelbimas įrašytas',
	'JS_CONFIRM_DEFAULT_SETTINGS'=>'Ar tikrai norite pakeisti esamus serverio nustatymus į numatytuosius?',
	'JS_PLEASE_ENTER_NUMBER_IN_RANGE_1TO5'=>'Įrašykite skaičių nuo 1 iki 5',
	'JS_PLEASE_ENTER_NUMBER_IN_RANGE_1TO100'=>'Įrašykite skaičių nuo 1 iki 100',
	'JS_RECORD_NUMBERING_SAVED_SUCCESSFULLY_FOR'=>'Įrašų numeracija sėkmingai įrašyta',
	'LBL_PREFIX_IN_USE'=>'Priešdėlis naudojamas',
	'JS_RECORD_NUMBERING_UPDATED_SUCCESSFULLY_FOR'=>'Įrašų numeracija atnaujinta sėkmingai',
	'JS_SEQUENCE_NUMBER_MESSAGE'=>'Sekos numeris turi būti didesnis arba lygus',
	'LBL_WRONG_IMAGE_TYPE'=>'Nepalaikomas paveikslėlio tipas',
	'LBL_MAXIMUM_SIZE_EXCEEDS'=>'Didžiausia leistina išsiuntimo talpa yra 1 MB',
	'LBL_NO_LOGO_SELECTED'=>'Nepasirinktas logotipas',
	'JS_CONFIGURATION_DETAILS_SAVED'=>'Konfigūracijos išsami informacija įrašyta',
	'JS_TERMS_AND_CONDITIONS_SAVED'=>'Terminai ir sąlygos įrašyti',
);
