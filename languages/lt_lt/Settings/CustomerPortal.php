<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'CustomerPortal'=>'Klientų portalas',
	'LBL_PORTAL_DESCRIPTION'=>'Nustatykite portalo vartotojo privilegijas',

	'LBL_PRIVILEGES'=>'Privilegijos',
	'LBL_DEFAULT_ASSIGNEE'=>'Numatytasis įgaliotinis',
	'LBL_PORTAL_URL'=>'Portalo URL',

	//Fields
	'LBL_MODULE_NAME'=>'Organizacijos',
	'LBL_ENABLE_MODULE'=>'Įjungti modulį',
	'LBL_VIEW_ALL_RECORDS'=>'Peržiūrėti organizacijų įrašus',

	//Messages
	'LBL_PREVILEGES_MESSAGE'=>"This Role's privileges will be applied to the Portal User.",
	'LBL_DEFAULT_ASSIGNEE_MESSAGE'=>'Kortelės bus priskirtos vartotojams pagal numatytąjį planą Grupė/Vartotojas iš klientų portalo.',
	'LBL_PORTAL_URL_MESSAGE' => 'Šis URL yra portalo, kuriame Jūsų adresatai gali prisijungti ir pateikti/sekti korteles, pasiekti informacijos bazę, atlikti kitus veiksmus. Kai bus įgalinta portalo prieiga, adresatams bus išsiųsti prisijungimo duomenys pateikti adresatų išsamios informacijos puslapyje.',
	'LBL_DRAG_AND_DROP_MESSAGE'=>'Tempkite modulius norėdami pertvarkyti klientų portalą.',
);

$jsLanguageStrings = array(
	'JS_PORTAL_INFO_SAVED'=>'Klientų portalo nuostatos įrašytos',
);
