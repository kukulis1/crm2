<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'Accounts'=>'Organizacijos ir adresatai',
	'LBL_ADD_CUSTOM_RULE'=>'Pridėti individualizuotą privilegijos taisyklę',
	'Read Only'=>'Tik skaitomas',
	'Read Write'=>'Skaitomas/rašomas',
	'LBL_ADD_CUSTOM_RULE_TO'=>'Pridėti individualizuotą privilegijos taisyklę',
	'LBL_CAN_ACCESSED_BY'=>'Jo informacija gali būti prieinama',
	'LBL_PRIVILEGES'=>'Privilegijos',
	'LBL_SHARING_RULE'=>'Bendrinimo taisyklės',
	'LBL_RULE_NO'=>'Taisyklės nr.',
	'LBL_MODULE'=>'CRM modulis',
	'LBL_ADVANCED_SHARING_RULES'=>'Išplėstinės bendrinimo taisyklės',
	'LBL_WITH_PERMISSIONS'=>'su leidimais',
	'LBL_APPLY_NEW_SHARING_RULES'=>'Taikyti naujas bendrinimo taisykles',
	'LBL_READ'=>'Skaityti',
	'LBL_READ_WRITE'=>'Skaityti ir rašyti',
	'LBL_CUSTOM_ACCESS_MESG'=>'Neapibrėžtos individualizuotos prieigos taisyklės',
	'SINGLE_Groups'=>'Grupė',
	'SINGLE_Roles'=>'Vaidmuo ',
	'SINGLE_RoleAndSubordinates' => 'Vaidmuo ir jam žemesni vaidmenys',
);

$jsLanguageStrings = array(
	'JS_CUSTOM_RULE_SAVED_SUCCESSFULLY'=>'Individualizuota bendrinimo taisyklė įrašyta sėkmingai',
	'JS_SELECT_ANY_OTHER_ACCESSING_USER'=>'Pasirinkite kitą turitntį prieigą vartotoją',
	'JS_NEW_SHARING_RULES_APPLIED_SUCCESSFULLY'=>'Individualizuota bendrinimo taisyklė pritaikyta sėkmingai',
	'JS_DEPENDENT_PRIVILEGES_SHOULD_CHANGE'=>'Kai organizacijos prieiga yra privati, galimybės, kortelės, pasiūlymai, pardavimų užsakymai ir sąskaitų prieiga taip pat turi būti nustatyti kaip privatūs.',
);
