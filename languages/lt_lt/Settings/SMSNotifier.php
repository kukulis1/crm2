<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'LBL_ADD_RECORD'=>'Nauja konfigūracija',
	'SMSNotifier'=>'SMS žinučių tiekėjo konfigūracija',
	'LBL_ADD_CONFIGURATION'=>'Nauja konfigūracija',
	'LBL_EDIT_CONFIGURATION'=>'Redaguoti konfigūraciją',
	'LBL_SELECT_ONE'=>'Pažymėti vieną',
	
	//Fields
	'providertype'=>'Tiekėjas',
	'isactive'=>'Aktyvus',
	'username' => 'Vartotojo vardas',
	'password'=>'Slaptažodis',
);

$jsLanguageStrings = array(
	'LBL_DELETE_CONFIRMATION'=>'Ar tikrai norite pašalinti šią informavimo žinute taisyklę?',
	'JS_RECORD_DELETED_SUCCESSFULLY'=>'SMS žinučių tiekėjas pašalintas sėkmingai',
	'JS_CONFIGURATION_SAVED'=>'SMS žinučių tiekėjo konfigūracija įrašyta',
);	
