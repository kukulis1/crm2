<?php

$languageStrings = array(
        'LBL_LEAVE_INFORMATION' => 'Basic Information',
        'SINGLE_Leave' => 'Darbuotojų kokybinė analizė',
        // 'Leave' => 'Darbuotojų kokybinė analizė',
        'From Date' => 'Nuo',
        'To Date' => 'Iki',
        'Leave Type' => 'Neatvykimo priežastys',
        'Action' => 'Sprendimas',
        'Accept' => 'Patvirinti',
        'Reject' => 'Atmesti',
        'Partial Days' => 'Dalis dienos',
        'Sick leave' => 'Liga',
        'Personal leave' => 'Asmeninės priežastys',
        'Wedding' => 'Vestuvės',
        'Leave of Absence' => 'Neapmokamos atostogos',
);