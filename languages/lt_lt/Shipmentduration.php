<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Shipmentduration' => 'Užsibuvusios siuntos',
	'ShipmentDuration' => 'Užsibuvusios siuntos',
	'SINGLE_Shipmentduration' => 'Užsibuvusios siuntos',
	'ModuleName ID' => 'shipmentDuration ID',
	
	'LBL_ADD_RECORD' => 'Add shipmentDuration',
	'LBL_RECORDS_LIST' => 'shipmentDuration List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'shipment code' => 'Siuntos numeris',
	'customer' => 'Klientas',
	'load in terminal' => 'Pasikrovimas terminale',
	'unload in terminal' => 'Išsikrovimas terminale',
	'days in ' => 'Praleista dienų',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
