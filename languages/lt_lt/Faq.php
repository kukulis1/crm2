<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'Faq'=>'Pranešimai',
	'SINGLE_Faq'=>'Pranešimai',
	'LBL_RECORDS_LIST'=>'Pranešimų sąrašas',
	'LBL_ADD_RECORD'=>'Pridėti pranešimą',
	
	//Blocks
	'LBL_FAQ_INFORMATION'=>'Pranešimo informacija',
	'LBL_COMMENT_INFORMATION'=>'Pranešimo informacija',
	
	//Fields
	'Question'=>'Klausimas',
	'Answer'=>'Atsakymas',
	'Comments'=>'Pastabos',
	'Faq No'=>'Pranešimo numeris',
    
	'Message'=>'Pranešimas',
	'All Users'=>'Siųsti visiems',
	'User'=>'Gavėjas',	
    
	//Added for existing Picklist Entries
	'General'=>'Bendra',
	'Draft'=>'Juodraštis',
	'Published'=>'Paskelbta',
	'Obsolete'=>'Nebevartojamas',

	//EditView
	'LBL_SOLUTION' => 'SPRENDIMAS',
);
