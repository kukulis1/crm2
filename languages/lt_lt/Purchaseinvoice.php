<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Purchaseinvoice' => 'Neapm. pirkimų sąskaitos',
	'SINGLE_Purchaseinvoice' => 'neapmokėtą sąskaitą',
	'ModuleName ID' => 'neapmokėtos sąskaitos ID',

	'purchaseinvoice' => 'Neapm. pirkimų sąskaitos',
	
	'LBL_ADD_RECORD' => 'Pridėti neapmokėtą sąskaitą',
	'LBL_RECORDS_LIST' => 'Neapmokėtų sąskaitų sąrašas',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',


);
	
?>
