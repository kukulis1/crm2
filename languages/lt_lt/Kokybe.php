<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Kokybe' => 'Kokybė',
	'SINGLE_Kokybe' => 'Kokybė',
	'ModuleName ID' => 'Kokybės ID',
	
	'LBL_ADD_RECORD' => 'Pridėti Kokybę',
	'LBL_RECORDS_LIST' => 'Kokybės Sąrašą',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'Delivery fail reason' => 'Nepristatymo priežastis',
    'Delivery remarks'     => 'Pristatymo pastabos',
    'Delivery comment'    => 'Pristatymo komentaras',
    'Loading works'    => 'Krovos darbai',
    'Idle time'            => 'Prastovos (min.)'         

	

);
	
?>
