<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	
	'Asterisk' => 'Žvaigždutė',
	'PBXManager'=>'PBX tvarkyklė',
	'SINGLE_PBXManager'=>'PBX tvarkyklė',
	
	//Blocks
	'LBL_CALL_INFORMATION'=>'Skambučio išsami informacija',
	
	//Field Labels
	'Call From'=>'Skambutis nuo',
	'Call To'=>'Skambutis',
	'Time Of Call'=>'Skambučio laikas',
	'PBXManager ID'=>'PBX tvarkyklės ID',
);
