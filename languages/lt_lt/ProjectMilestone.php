<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'SINGLE_ProjectMilestone'=>'Projekto svarbiausias įvykis',
	'LBL_ADD_RECORD'=>'Pridėti projekto svarbiausią įvykį',
	'LBL_RECORDS_LIST'=>'Projekto svarbiausių įvykių sąrašas',

	// Blocks
	'LBL_PROJECT_MILESTONE_INFORMATION'=>'Projekto svarbiausio įvykio išsami nformacija',
	
	//Field Labels
	'Project Milestone Name'=>'Projekto svarbiausio įvykio pavadinimas',
	'Milestone Date' => 'Svarbiausio įvykio data',
	'Project Milestone No'=>'Projekto svarbiausio įvykio numeris',
	
);
