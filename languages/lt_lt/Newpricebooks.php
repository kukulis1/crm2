<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.3
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

$languageStrings = Array(
	'Newpricebooks' => 'Kainoraščiai',
	'newpricebooks' => 'Kainoraščiai',
	'SINGLE_Newpricebooks' => 'Kainoraščiai',
	'ModuleName ID' => 'ID',
	
	'LBL_ADD_RECORD' => 'Pridėti naują kainyną',
	'LBL_RECORDS_LIST' => 'newpricebooks List',
	
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_MODULEBLOCK_INFORMATION' => 'ModuleBlock Information',

	'ModuleFieldLabel' => 'ModuleFieldLabel Text',

	

);
	
?>
