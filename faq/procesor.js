const example_image_upload_handler = (blobInfo, progress) => new Promise((resolve, reject) => {
  let anchor = $('#anchor').val();   
  if(anchor == ''){
    $('#anchor').addClass('alert alert-danger');
    reject('Nenurodytas Inkaras!');   
  }else{ 
    let formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());
    formData.append('folder', anchor);

    $.ajax({
      url : 'uploadFile.php',
      type: "POST",
      data:  formData,
      contentType: false,
            cache: false,
      processData: false,
      success: function(data){   
        json = JSON.parse(data);    
        resolve(json.location);      
      }
    });
  }
});

tinymce.init({
  selector: 'textarea#upload',
  plugins: 'link image',
  images_upload_handler: example_image_upload_handler
});

$('#anchor').on('click', function(){
  $(this).removeClass('alert alert-danger');
});


