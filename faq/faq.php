<?php


try {

  include 'connect.php';
  
  $db->beginTransaction();   

  $get_faq_list = $db->query("SELECT * FROM app_faq ORDER BY id", PDO::FETCH_ASSOC)->fetchAll();

  $db->commit();

} catch (PDOException $e) {
  $db->rollBack();    
  echo "Error!";
  echo $e->getMessage();
}

$content = '<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DUK</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <style>
    html {
      scroll-behavior: smooth;
    }
    body {
      padding-top: 5rem;
    }
    .company-logo {
        height: 40px;
        width: 150px;
        margin: 0 0;
        display: inline-block;
        margin-left: 1px;
    }
    .company-logo img {
        max-height: 100%;
        max-width: 100%;
    }
    .responsive {
      width: 100%;
      height: auto;
    }


</style>
</head>
';

$content .= '

<nav class="navbar navbar-expand-md navbar-white bg-white fixed-top" style="border-bottom: 1px solid #F1C40F; height: 40px;">
<a class="navbar-brand company-logo" href="/" style="color: black;" title="Dažniausiai užduodami klausimai"><img class="responsive" src="../test/logo/parnasas.png"> D.U.K</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
</nav>
<div class="container-fluid">

<div id="list-example" class="list-group">';

  foreach ($get_faq_list AS $value) {
    $content .='<a class="list-group-item list-group-item-action" href="#'.$value['anchor'].'">'.$value['title'].'</a>';
  }


$content .= '</div>

<div style="height: 50px;"></div>

<div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">';

foreach ($get_faq_list AS $value) {
  $content .= '<hr>
  <h4 id="'.$value['anchor'].'">'.$value['title'].'</h4>
  <p>'.$value['content'].'</p>';
}

$content .= '</div>';

$content .= '</div>';

$content .= '
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
</script>
</body>
</html>';


echo $content;