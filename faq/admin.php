<?php

try {
  
  include 'connect.php';

  $db->beginTransaction();   

  $get_faq_list = $db->prepare("SELECT * FROM app_faq ORDER BY id");
  $get_faq_list->setFetchMode(PDO::FETCH_ASSOC);
  $get_faq_list->execute(array());      
                  
  $db->commit();

} catch (PDOException $e) {
  $db->rollBack();    
  echo "Error!";
  echo $e->getMessage();
}

$content = '

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>DUK Title</title>   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

  
  </head>
  <body>
    
  <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="admin.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="new.php">Pridėti naują</a>
      </li>

    </ul>
  </div>
</nav>

<main role="main" class="container">
  <div class="jumbotron">

<div id="list-example" class="list-group">';

  foreach ($get_faq_list AS $value) {
    $content .='<a class="list-group-item list-group-item-action" href="edit.php?recordid='.$value['id'].'">'.$value['title'].'</a>';
  }


$content .= '</div>
   
</div>
</main>      
  </body>
</html>';


echo $content;
