<?php


if(isset($_POST['save'])){

  try {

    include 'connect.php';

    $db->beginTransaction();   
    
    $insert_faq = $db->prepare("INSERT INTO app_faq (title, anchor, content, createdtime, updatedtime) 
                                  VALUES (:title, :anchor, :content, :createdtime, :updatedtime)");

    $insert_faq->execute(array(':title' => $_POST['title'], 
                                  ':anchor' => $_POST['anchor'], 
                                  ':content' => $_POST['content'], 
                                  ':createdtime' => date('Y-m-d H:i:s'), 
                                  ':updatedtime' => date('Y-m-d H:i:s')
                                  )
                          );

  
    $db->commit();

    header("Location: new.php?success=1");


  } catch (PDOException $e) {
    $db->rollBack();    
    echo "Error!";
    echo $e->getMessage();
  }

}


$content = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.2/skins/ui/oxide/content.min.css"/>

<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="admin.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="new.php">Pridėti naują</a>
      </li>

    </ul>
  </div>
</nav>

<main role="main" class="container-fluid">';

if(isset($_GET['success']) && $_GET['success'] == 1){
  $content .= ' <div id="show-message" class="alert alert-success  text-center">Išsaugota sėkmingai.</div>';
} 

$content .= '

<div class="jumbotron">

<form action="" method="POST">
  <input type="hidden" name="save">
  <div class="form-group">
    <label for="title">Title</label>
    <input class="form-control" name="title" type="text">
  </div>

  <div class="form-group">
  <label for="title">Inkaras</label>
  <input class="form-control" name="anchor" id="anchor" type="text">
</div>

  <div class="form-group">
  <label for="title">Aprašymas</label>
   <textarea class="form-control" name="content" rows="30" id="upload"></textarea>
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
  </form>
    
  </div>
</main>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.2/tinymce.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="procesor.js"></script>
';




echo $content;