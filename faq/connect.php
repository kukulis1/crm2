<?php

include '../config.inc.php';

ini_set('display_errors', 1);

$host = $dbconfig['db_server'];
$dbname = $dbconfig['db_name'] ;
$user = $dbconfig['db_username'];
$pass = $dbconfig['db_password'];

$db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->exec("set names utf8");