<?php

require 'vendor/autoload.php';
require_once "../../../config.inc.php";

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");
error_reporting(1);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//  Template processor instance creation
$reader = IOFactory::createReader('Xlsx');


$invoice = $_GET['invoice'];

$sql=  "SELECT subject,invoicedate,accountname,  CONCAT(billing_address,',',municipality) as address ,vtiger_account.legal_entity_code,vtiger_account.legal_vat_code, CONCAT(vtiger_users.title,' ',vtiger_users.first_name,' ',vtiger_users.last_name) as invoice_creator ,duedate,subtotal,total, vtiger_invoice.s_h_amount as vatprice,
CASE WHEN vtiger_invoice.region_id = 0 THEN 21 ELSE vtiger_taxregions.value END AS vatprocent,vtiger_invoicecf.cf_1277 as invoice_type,
CASE
						WHEN vtiger_products.productid = 121391 THEN 'Item'  
						WHEN vtiger_products.productid != 36641 THEN 'Products'					 
						ELSE 'Services' 
					END as entitytype
          FROM vtiger_invoice
					LEFT JOIN `vtiger_invoicecf` ON `vtiger_invoicecf`.`invoiceid`=`vtiger_invoice`.`invoiceid`
          LEFT JOIN `vtiger_account` ON `vtiger_account`.`accountid`=`vtiger_invoice`.`accountid`
          LEFT JOIN `vtiger_crmentity`  ON `vtiger_crmentity`.`crmid`=`vtiger_invoice`.`invoiceid`
					LEFT JOIN `vtiger_users` ON `vtiger_users`.`id`=`vtiger_crmentity`.`smownerid`
					LEFT JOIN `vtiger_user2role` ON `vtiger_user2role`.`userid`=`vtiger_crmentity`.`smownerid`
					LEFT JOIN `vtiger_role` ON vtiger_role.`roleid`=`vtiger_user2role`.`roleid`
					LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`vtiger_invoice`.`region_id` 
					LEFT JOIN `vtiger_inventoryproductrel` on `vtiger_inventoryproductrel`.`id`=`vtiger_invoice`.`invoiceid`
					LEFT JOIN `vtiger_products` ON `vtiger_products`.`productid`=`vtiger_inventoryproductrel`.`productid`
          WHERE `vtiger_invoice`.`invoiceid` = $invoice    
					GROUP BY vtiger_invoice.invoiceid";
// if($invoice == 815402){
// 	echo '<pre>';
// 	print_r($sql);
// 	echo '</pre>';
// 	die();
// }

$resql=$conn->query($sql);
$data=$resql->fetch_assoc();

if($data['entitytype'] == 'Item'){
	$spreadsheet = $reader->load(__DIR__ . '/template_item.xlsx');
}else{
	$spreadsheet = $reader->load(__DIR__ . '/template.xlsx');
}

	$invoice_type = ' PVM SĄSKAITA FAKTŪRA';

	if($data['invoice_type'] == 'Kreditinė'){
		$invoice_type = 'KREDITINĖ PVM SĄSKAITA FAKTŪRA';
	}
	if(empty($data['accountname'])) $data['accountname'] = '';
	if(empty($data['legal_entity_code'])) $data['legal_entity_code'] = '';
	if(empty($data['legal_vat_code'])) $data['legal_vat_code'] = '';
	if(empty($data['address'])) $data['address'] = '';

// $spreadsheet->getActiveSheet()->setTitle(substr($data['accountname'],0,31));
$spreadsheet->getActiveSheet()->setTitle(substr(preg_replace('/[\/\&%#\$]/','_',$data['accountname']),0,31));
$spreadsheet->getActiveSheet()->setCellValue('C4', $invoice_type);
$spreadsheet->getActiveSheet()->setCellValue('C5', "Nr. ".$data['subject']);
$spreadsheet->getActiveSheet()->setCellValue('C6', "Išrašymo data: ".$data['invoicedate']);
$spreadsheet->getActiveSheet()->setCellValue('C9', $data['accountname']);
$spreadsheet->getActiveSheet()->setCellValue('C10', $data['legal_entity_code']);
$spreadsheet->getActiveSheet()->setCellValue('C11', $data['legal_vat_code']);
$spreadsheet->getActiveSheet()->setCellValue('C12',  iconv(mb_detect_encoding($data['address']), "UTF-8", $data['address']));
$subject = $data['subject'];
$sql=  "SELECT (listprice * quantity) AS totalprice,
            CASE WHEN vtiger_products.productid != 36641 THEN ROUND(vtiger_inventoryproductrel.m3,3) ELSE '' END as m3_volume,
            CASE WHEN vtiger_products.productid != 36641 THEN vtiger_inventoryproductrel.cargo_wgt ELSE '' END as cargo_kg,
            CASE WHEN vtiger_products.productid != 36641 THEN vtiger_salesorder.shipment_code ELSE app_services_type.name END as shipment_code,      
            CASE WHEN vtiger_products.productid != 121391 THEN '' ELSE vtiger_inventoryproductrel.cargo_wgt END as item,      
            CASE WHEN vtiger_products.productid != 121391 THEN '' ELSE vtiger_inventoryproductrel.quantity END as quantity,           
						CASE WHEN vtiger_products.productid != 36641 THEN DATE_FORMAT(se.createdtime, '%Y-%m-%d')   ELSE 'Paslauga:' END as sale_createdtime,              
            vtiger_inventoryproductrel.listprice, DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') as createdtime, vtiger_inventoryproductrel.note,  vtiger_inventoryproductrel.bill_ads, vtiger_inventoryproductrel.ship_ads,cargo_measure,vtiger_products.productid, vtiger_invoicecf.cf_1277 as invoice_type,  vtiger_taxregions.value as vatprocent,
						vtiger_invoicecf.cf_1486 as print_template   
            FROM vtiger_inventoryproductrel				
            LEFT JOIN vtiger_salesorder ON vtiger_salesorder.salesorderid=vtiger_inventoryproductrel.order_id
            LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_inventoryproductrel.id
            LEFT JOIN vtiger_crmentity se ON se.crmid=vtiger_salesorder.salesorderid
            LEFT JOIN vtiger_products ON vtiger_products.productid=vtiger_inventoryproductrel.productid
            LEFT JOIN vtiger_service ON vtiger_service.serviceid=vtiger_inventoryproductrel.productid	      
						LEFT JOIN app_services_type  ON app_services_type.id=vtiger_inventoryproductrel.service
						LEFT JOIN `vtiger_invoicecf` ON `vtiger_invoicecf`.`invoiceid`=`vtiger_inventoryproductrel`.`id`
						LEFT JOIN vtiger_invoice ON vtiger_invoice.invoiceid=vtiger_inventoryproductrel.id	
						LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`vtiger_invoice`.`region_id`                  
            WHERE vtiger_inventoryproductrel.id= $invoice
            GROUP BY CASE WHEN vtiger_inventoryproductrel.source = 'Metrika' THEN external_load_id ELSE sequence_no END
            ORDER BY sequence_no";

$rsSql=$conn->query($sql);
$baseRow = 18;
$r=0;
while ($data2 = $rsSql->fetch_assoc()){


	if($data2['print_template'] == 'Transportas') $template_string = 'transporto';
	elseif($data2['print_template'] == 'Perkraustymas') $template_string = 'perkraustymo';
	else $template_string = 'transporto';

    $row = $baseRow + $r;
		$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);  
		if($data['entitytype'] == 'Item'){
			$spreadsheet->getActiveSheet()->setCellValue('A' . $row, $r + 1)
					->setCellValue('A' . $row, $data2['createdtime'])
					->setCellValue('C' . $row, $data2['item'])				
					->setCellValue('F' . $row, $data2['quantity'])
					->setCellValue('G' . $row, $data2['listprice'])
					->setCellValue('H' . $row, $data2['totalprice']);
		}else{

			$spreadsheet->getActiveSheet()->setCellValue('A' . $row, $r + 1)
					->setCellValue('A' . $row, $data2['sale_createdtime'])
					->setCellValue('B' . $row, $data2['shipment_code'])
					->setCellValue('C' . $row, iconv(mb_detect_encoding($data2['bill_ads']), "UTF-8", $data2['bill_ads']))
					->setCellValue('D' . $row, iconv(mb_detect_encoding($data2['ship_ads']), "UTF-8", $data2['ship_ads']))
					->setCellValue('E' . $row, $data2['cargo_measure'])
					->setCellValue('F' . $row, $data2['cargo_kg'])
					->setCellValue('G' . $row, $data2['m3_volume'])
					->setCellValue('H' . $row, $data2['listprice']);
					// ->setCellValue('I' . $row, $data2['note']);

		}
		$r= $r+1;  
}

$numberToWord = numberToWords($data['total'], 'lt', 'EUR');	

$spreadsheet->getActiveSheet()->setCellValue('G'.(29+$r), $data['subtotal']);
$spreadsheet->getActiveSheet()->setCellValue('F'.(30+$r), '('.$data['vatprocent'].'%)');
$spreadsheet->getActiveSheet()->setCellValue('G'.(30+$r), $data['vatprice']);
$spreadsheet->getActiveSheet()->setCellValue('G'.(31+$r), $data['total']);
$spreadsheet->getActiveSheet()->setCellValue('E'.(33+$r), $data['duedate']);
$spreadsheet->getActiveSheet()->setCellValue('B'.(36+$r),  $template_string);
$spreadsheet->getActiveSheet()->setCellValue('D'.(36+$r), $numberToWord);
$spreadsheet->getActiveSheet()->setCellValue('C'.(38+$r),  iconv(mb_detect_encoding($data['invoice_creator']), "UTF-8", $data['invoice_creator']));

$spreadsheet->getActiveSheet()->removeRow($baseRow - 1, 1);
$temp_file = tempnam(sys_get_temp_dir(), 'Excel');
// Save EXCEL
$writer = new Xlsx($spreadsheet);


$writer->save($temp_file);
// Save PDF
// $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf($spreadsheet);
// $writer->save($temp_file);
// ------------------ Operation with file result -------------------------------------------
$documento = file_get_contents($temp_file);
unlink($temp_file);  // delete file tmp

header("Content-Disposition: attachment; filename=$subject.xlsx");
header("Content-Type: application/excel;  charset=UTF-8");
echo $documento;



function numberToWords ( $digit, $lang, $curr) { 
	$digit = number_format($digit,2,'.','');
			$dparts = explode ( ".", $digit ); 
			$digit = $dparts[0]; 
			$ct = $dparts[1]; 
			$digitLength = (int)strlen( $digit ); 
			//apsirasome zodziu masyva 
			$textOnes['lt'] = array( '', ' vienas', ' du', ' trys', ' keturi', ' penki', ' šeši', ' septyni', ' aštuoni', ' devyni' ); 
								$textTens['lt'] = array( '', ' dešimt', ' dvidešimt', ' trisdešimt', 'keturiasdešimt', ' penkiasdešimt', ' šešiasdešimt', ' septyniasdešimt', ' aštuoniasdešimt', ' devyniasdešimt' ); 
								$textExepts['lt'] = array( ' vienuolika', ' dvylika', ' trylika', ' keturiolika', ' penkiolika', ' šešiolika', ' septyniolika', ' aštuoniolika', ' devyniolika' ); 
								$textHundrets['lt'] = array( '', ' šimtas', ' šimtai' ); 
								$textThousands['lt'] = array( ' tūkstančių', ' tūkstantis', ' tūkstančiai' ); 

								$words = ''; 
																// nu ble ir su neigiamais malames: 
								if ( $digit < 0 ) { 
												$words = 'Minus'; 
								} 
								if( $digitLength < 7 ){ 
									for( $d = 2; $d > 0; $d-- ){ 
											//checaks - tajp rejkia 
											if( ( $d == 2 ) && ( $digitLength < 4 ) ) continue; 
											//gaunam temp skaitmeny 
												$digitLocal = substr( $digit, -3 ); 
												if( $d == 2 ) $digitLocal = substr( $digit, 0, $digitLength - 3 ); 

											//gaunam rejkiamus skaicius 
												$digitLocalLength = strlen( $digitLocal ); 
												$hundrets = substr( $digitLocal, -3, 1 ); 
												$lastTwo = substr( $digitLocal, -2 ); 
												//cia konstruojam simtus 
												if( $digitLocalLength > 2 ){ 
														$words .= $textOnes[$lang][(int)$hundrets]; 
														if( $hundrets > 1 ){ 
																$words .= $textHundrets[$lang][2]; 
														} else { 
																$words .= $textHundrets[$lang][(int)$hundrets]; 
														} 
													} 
												//sia jai qrwos nuo 10 iki 20 
						if( ( $lastTwo > 10 ) && ( $lastTwo < 20 ) ){ 
								$words .= $textExepts[$lang][(int)substr( $lastTwo, -1 ) - 1]; 
								//cia jei rejkia pridedam zody 'tukstantis' pagal linksny 
								if( $d == 2 ) $words .= $textThousands[$lang][0]; 
						//cia bet kuriuo kitu atveju 
						} else { 
							//jai yra desimtys 
							if( $digitLocalLength > 1 ) $words .= $textTens[$lang] [(int)substr( $lastTwo, -2, 1 ) ] . ' '; 
							$words .= $textOnes[$lang][(int)substr( $lastTwo, -1 )]; 
							//cia jei rejkia pridedam zody 'tukstantis' pagal linksny 
							if( $d == 2 ){ 
									if( ( (int )substr( $lastTwo, -1 ) ) > 1 ){ 
											$words .= $textThousands[$lang][2]; 
									} else { 
											$words .= $textThousands[$lang][(int)substr( $lastTwo, -1 )]; 
									} 
							} 
					} 
			} 
			$words = trim( $words ); 
		if ( $words[0] . $words[1] == 'š' ) 
						$words = 'Š' . substr( $words, 2 ); 
		else 
						$words = ucfirst( $words ); 
		} 
		if ( $ct < 10 ) $ct.='0'; 
		return $words . ' ' . $curr . ', ' . substr($ct, 0, 2) . ' ct'; 
}
?>
