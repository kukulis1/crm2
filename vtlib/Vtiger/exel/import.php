<?php 
echo '<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Užsakymų importas</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/../../../resources/import.css">

</head>
<body>
<nav class="navbar navbar-expand-md navbar-white bg-white fixed-top" style="border-bottom: 1px solid #F1C40F; height: 40px;">
 <div class="container-fluid">
   <a class="navbar-brand company-logo" href="/index.php?module=SalesOrder&view=List" style="color: black;" title="Užsakymu importas"><img class="responsive" src="../../../test/logo/parnasas.png"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </nav>
<div class="container-fluid">';

require_once $_SERVER['DOCUMENT_ROOT']."/v1/external_data/getPriceFromPriceBook.php";
require 'vendor/autoload.php';
require_once "../../../config.inc.php";


$host = $dbconfig['db_server'];
$dbname = $dbconfig['db_name'];
$user = $dbconfig['db_username'];
$pass = $dbconfig['db_password'];

$db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->exec("set names utf8");   

$sql =$db->prepare("SELECT * FROM app_import_draft_json WHERE id = ?");
$sql->execute(array($_COOKIE['user_id']));
$result = $sql->fetch(PDO::FETCH_ASSOC); 


if(!$_POST){

       
        if(isset($_COOKIE['imported'])){     
                echo '<div class="card" style="margin: 15px;">';
                
                echo '<div class="card-body">';
                echo '<h5 style="font-weight: 600;">Sukurta crm '.$_COOKIE['success'].'</h5>';
                echo '<h5 style="font-weight: 600;">Įkelta į metriką '.$_COOKIE['success_metrika'].'</h5>';                 
                $errorMessages = json_decode($_COOKIE['errors'],true);
        
                if($_COOKIE['fail_metrika'] > 0){

                        if($_COOKIE['fail_metrika'] > 0 && $_COOKIE['fail_metrika'] < 2){
                                $status = 'užsakymas neįkeltas';
                        }elseif($_COOKIE['fail_metrika'] > 9){
                                $status = 'užsakymų neįkelta';
                        }elseif($_COOKIE['fail_metrika'] > 1){
                                $status = 'užsakymai neįkelti';
                        }

                        echo ' <h5 style="font-weight: 600;">'.$_COOKIE['fail_metrika'].' '.$status.' į metriką  dėl sekančių priežasčių</h5>';
                        echo '<ul>';
                        foreach($errorMessages AS $key => $error):
                                echo "<li>$error</li>";
                        endforeach;	
                        echo '</ul>';                       
                }
                
                echo '</div>

                <button type="button" class="btn btn-primary btn-lg btn-block btn-sm" onclick="window.location.href=\'/index.php?module=SalesOrder&view=List\'">Gryžti į crm</button></div>';  

                setcookie("success", '', time()+3600, "/","", 0);
                setcookie("success_metrika", '', time()+3600, "/","", 0);
                setcookie("fail_metrika", '', time()+3600, "/","", 0);
                setcookie("errors", '', time()+3600, "/","", 0);
                setcookie("imported", '', time()+3600, "/","", 0);

                $sql = $db->prepare("DELETE FROM app_import_draft_json WHERE id = ?");
                $sql->execute(array($_COOKIE['user_id'])); 

                
        }
        if(!isset($_COOKIE['imported'])){   
                 echo '<form action="" method="POST">';
        }

        if($result['orders_done'] == 0){
                echo showDraft($result,'metrika',$_COOKIE['user_id']);
        }
        
        if($result['critical_done'] == 0){
                echo showDraft($result,'critical',$_COOKIE['user_id']);
        }   
        if(!isset($_COOKIE['imported'])){   
                echo '<input type="submit" class="btn btn-success btn-sm" value="Įkelti į CRM">';
                echo "</form>";
                echo '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';  
        }
         

}

if($_POST['upload_to_crm']){    
         initLoop($db,$_POST);     
}


if ($_POST && $_FILES["fileToUpload"]) {          

        setcookie("user_id", '', time()+3600, "/","", 0);

        $user_id = $_POST['user_id'];
        setcookie("user_id", $user_id, time()+3600, "/","", 0);

        $sql =$db->prepare("DELETE FROM app_import_draft_json WHERE id = $user_id");
        $sql->execute(array()); 
        
        $data = importOrdersFromExel($db,$_FILES);  
        
        if(isset($data['wrongFormat'])){
                echo '<div class="card" style="margin: 15px;">
                <div class="card-body">
                                <h5 style="font-weight: 600; color: red;">'.$data['wrongFormat'].'</h5>
                                </div>
                </div>
                <button type="button" class="btn btn-primary btn-lg btn-block btn-sm" onclick="window.location.href=\'/index.php?module=SalesOrder&view=List\'">Gryžti į crm</button>';                
                die;
        }

    
        $errors = json_encode($data['errorLog']);

        $orders = json_encode($data['orders']);

        $criticalOrders = json_encode($data['criticalOrders']);

        $sql =$db->prepare("INSERT INTO app_import_draft_json (id,errorLog, orders, criticalOrders) VALUES (?,?,?,?)");
        $sql->execute(array($user_id,$errors,$orders,$criticalOrders));        

        $date = date("Y-m-d");
        $date2 = date('Y-m-d', strtotime("+1 day"));


        $sql =$db->prepare("SELECT * FROM app_import_draft_json WHERE id = ?");
        $sql->execute(array($user_id));

        $result = $sql->fetch(PDO::FETCH_ASSOC);

        $errorLog = json_decode($result['errorLog'],true);     
        $orders = json_decode($result['orders'],true); 
        $criticalOrders = json_decode($result['criticalOrders'],true); 
       

        if(count($errorLog) > 0){
                echo '<div class="card" style="margin: 15px;">
                <div class="card-body">
                <h5 style="font-weight: 600;">Viso užsakymų:  '.count($errorLog['total']).'</h5>';
                
                if(count($orders) > 0 && count($orders) < 2){
                        $orders_string = 'užsakymas pilnai užpildytas, jį';
                }elseif(count($orders) > 9){
                        $orders_string = 'užsakymų pilnai užpildyti, juos';
                }elseif(count($orders) > 1){
                        $orders_string = 'užsakymai pilnai užpildyti, juos';
                }
                
                if(count($orders) > 0){
                        echo  '<h5 style="font-weight: 600;margin-top: 20px;">'.count($orders).' '.$orders_string.' galima ikelti į crm</h5>';                                
                }  
        
                echo '</div></div>';
                echo '<form action="" method="POST">';
                echo showDraft($result,'metrika',$user_id);
                echo "<br><br>";
                echo showDraft($result,'critical',$user_id);
                echo '<input type="submit" class="btn btn-success btn-sm" value="Įkelti į CRM">';
                echo "</form>";                 
                echo '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';  
        }            
        
}


echo '</div>
</body>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="../../../resources/import.js"></script>
<script>
$(function () {
        $(\'.dateField\').datepicker({
          dateFormat: "yy-mm-dd",
          autoclose: true
        });
      });
</script>
</html>';


function clean($string) {
        // $string = str_replace('UAB', '', $string); 
        // $string = explode(' ',trim($string));       
        $string = str_replace("'", '', trim($string)); // Removes special chars.     
        return $string; 
}

function showDraft($data,$type,$user_id){   

        $which = ($type == 'metrika' ? 'orders' : 'criticalOrders');
   
        $errorLog = json_decode($data['errorLog'],true);
        $date = date("Y-m-d");
        $date2 = date('Y-m-d', strtotime("+1 day"));

        if($type == 'critical'){              

                if(count($errorLog['critical']) > 0 && count($errorLog['critical']) < 2){
                        $status = 'užsakymas negali buti sukurtas';
                }elseif(count($errorLog['critical']) > 9){
                        $status = 'užsakymų negali buti sukurta';
                }elseif(count($errorLog['critical']) > 1){
                        $status = 'užsakymai negali buti sukurti';
                }
        }else{
                if(count($errorLog['metrika']) > 0 && count($errorLog['metrika']) < 2){
                        $status = 'užsakymas neįkeltas';
                }elseif(count($errorLog['metrika']) > 9){
                        $status = 'užsakymų neįkelta';
                }elseif(count($errorLog['metrika']) > 1){
                        $status = 'užsakymai neįkelti';
                }
        }

if(count(json_decode($data[$which],true)) > 0){     

        if(count($errorLog[$type]) > 0){
                echo '<div class="card" style="margin: 15px;">
                <div class="card-body">';               
                echo '<h5 style="font-weight: 600; margin-top: 20px;">'.count($errorLog[$type]).'  '.$status.', dėl sekančių priežasčių:</h5>
                        <ul>';
                        foreach($errorLog[$type] AS $key => $error):
                        echo '<li><span style="font-weight: 600;">Excel failo '.$key.' eilutėje, nenurodyti privalomi METRIKAI laukai: </span>';                         
                                for($e=0; $e < count($error); $e++){
                                        echo  $error[$e];  if( count($error) > 1 && count($error)-1 != $e){ echo ', ';}   
                                }
                        echo ' </li>';
                        endforeach;	
                        echo '</ul>';
                        // echo ' <button type="button" class="btn btn-primary btn-sm btn-sm" onclick="location.reload();">Atnaujinti</button>';
                echo '</div></div>';
        }  

        // if($type == 'metrika'){
                $table = '<input type="hidden" name="upload_to_crm" value="1">';
        // }

        $table .='<table class="table listview-table" id="'.$which.'">
        <thead><tr class="listViewContentHeader header">     
                <th>Užsakymo grupė</th>
                <th>Užsakymo numeris</th>
                <th>Pasikrovimo data</th>
                <th>Pristatymo data</th>
                <th>Siuntėjo pavadinimas</th>
                <th>Siuntėjo adresas</th>
                <th>Siuntėjo miestas</th>
                <th>Siuntėjo rajonas</th>
                <th>Siuntėjo pašto kodas</th>
                <th>Siuntėjo kontaktinis asmuo</th>
                <th>Siuntėjo kontaktinio asmens telefonas</th>
                <th>Gavėjo pavadinimas</th>
                <th>Gavėjo įmonės kodas</th>
                <th>Gavėjo adresas</th>
                <th>Gavėjo miestas</th>
                <th>Gavėjo rajonas</th>
                <th>Gavėjo pašto kodas</th>
                <th>Gavėjo kontaktinis asmuo</th>
                <th>Gavėjo telefonas</th>
                <th>Kiekis</th>
                <th>Svoris</th>
                <th>Matavimo vnt.</th>
                <th>Pll</th>
                <th>Ilgis</th>
                <th>Plotis</th>
                <th>Aukštis</th>
                <th>Gražinimas</th>
                <th>Pastabos</th>
                </tr>
        </thead>
        <tbody class="input">  
        ';    

        $count = 0;

        if($type == 'critical'){
                $count = count(json_decode($data['orders'],true));
        }

        foreach(json_decode($data[$which],true) AS $order){
                $i = $count+1;    
                $table .= '<tr>';
                $table .= '<td><input type="text" name="group'.$i.'" class="inputElement" value="'.$order[0].'"></td>';
                $table .= '<td><input type="text" name="customer_order_code'.$i.'" class="inputElement" value="'.$order[1].'"></td>';
                $table .= '<td><input type="text" name="load_date'.$i.'" class="inputElement dateField" value="'.($order[2] != '1970-01-01' ? $order[2] : $date).'" style="width: 80px;"></td>';
                $table .= '<td><input type="text" name="unload_date'.$i.'" class="inputElement dateField" value="'.($order[3] != '1970-01-01' ? $order[3] : $date2).'" style="width: 80px;"></td>';
                
                $table .= '<td><input type="text" data-id="'.$i.'" id="load_company'.$i.'" name="load_company'.$i.'" class="inputElement load_company '.(!$order[4] || !$order['accountname'] ?'alert-danger':'').'" value="'.($order['accountname'] ?: $order[4]).'" style="width: 150px;" required> <input name="old_load_company'.$i.'" type="hidden" value="'.(!$order['accountname'] ? $order[4] : $order['accountname']).'"></td>';

                $table .= '<td><textarea class="inputElement"  name="bill_address'.$i.'" name="bill_address'.$i.'" style="width: 150px;">'.($order[5] ?: $order['billing_address']).'</textarea></td>';
                $table .= '<td><input type="text" name="bill_city'.$i.'" class="inputElement" value="'.($order[6] ?: $order['municipality']).'" style="width: 80px;"></td>';
                $table .= '<td><input type="text" name="bill_distric'.$i.'" class="inputElement" value="'.$order[7].'" style="width: 80px;"></td>';
                $table .= '<td><input type="text" name="bill_code'.$i.'" class="inputElement '.(!$order[8] && !$order['post_code'] ? 'alert-danger':'').'" value="'. ($order[8] ? (strlen($order[8])== 4 ? "0".$order[8] : $order[8]) : $order['post_code']).'" style="width: 70px;" required></td>';   

                $table .= '<td><input type="text" name="load_person'.$i.'" class="inputElement" value="'.$order[9].'" style="width: 80px;"></td>';
                $table .= '<td><input type="text" name="load_phone'.$i.'" class="inputElement" value="'.$order[10].'" style="width: 80px;"></td>';
                $table .= '<td><input type="text" name="unload_company'.$i.'" class="inputElement '.(!$order[11]?'alert-danger':'').'" value="'.$order[11].'" style="width: 100px;" required></td>';
                $table .= '<td><input type="text" name="company_code'.$i.'" class="inputElement" value="'.$order[12].'" style="width: 80px;"></td>';
                $table .= '<td><textarea class="inputElement '.(!$order[13]?'alert-danger':'').'" name="ship_address'.$i.'" style="width: 150px;" required>'.$order[13].'</textarea></td>';

                $table .= '<td><input type="text" name="ship_city'.$i.'" class="inputElement '.(!$order[14]?'alert-danger':'').'" value="'.$order[14].'" style="width: 80px;" required></td>';                
                $table .= '<td><input type="text" name="ship_district'.$i.'" class="inputElement" value="'.$order[15].'" style="width: 80px;"></td>';
                $table .= '<td><input type="text" name="ship_code'.$i.'" class="inputElement '.(!$order[16]?'alert-danger':'').'" value="'.(strlen($order[16])== 4 ? "0".$order[16] : $order[16]).'" style="width: 70px;" required></td>';

                $table .= '<td><input type="text" name="unload_contact'.$i.'" class="inputElement '.(!$order[17]?'alert-danger':'').'" value="'.$order[17].'" required></td>';
                $table .= '<td><input type="text" name="unload_phone'.$i.'" class="inputElement '.(!$order[18]?'alert-danger':'').'" value="'.$order[18].'" style="width: 100px;" required></td>';
                $table .= '<td><input type="text" name="quantity'.$i.'" class="inputElement '.(!$order[19]?'alert-danger':'').'" value="'.$order[19].'" required></td>';
                $table .= '<td><input type="text" name="weight'.$i.'" class="inputElement '.(!$order[20]?'alert-danger':'').'" value="'.$order[20].'" required></td>';
                $table .= '<td><select name="tare'.$i.'" class="inputElement">
                                <option value="1" '.($order[21] == 'pll' ? 'selected' : '').'>pll</option>
                                <option value="7" '.($order[21] == 'nestd' ? 'selected' : '').'>nestd</option>
                                <option value="2" '.($order[21] == 'RUS-pll' ? 'selected' : '').'>RUS-pll</option>
                                <option value="3" '.($order[21] == 'FIN-pll' ? 'selected' : '').'>FIN-pll</option>                           
                                <option value="4" '.($order[21] == 'statinė' ? 'selected' : '').'>statinė</option>
                                <option value="6" '.($order[21] == 'rulon' ? 'selected' : '').'>rulon</option>
                                <option value="9" '.($order[21] == 'pak' ? 'selected' : '').'>pak</option>
                                <option value="8" '.($order[21] == 'padang' ? 'selected' : '').'>padang</option>
                                <option value="10" '.($order[21] == 'vnt' ? 'selected' : '').'>vnt</option>
                                <option value="5" '.($order[21] == 'dėžė' ? 'selected' : '').'>dėžė</option>
                                <option value="11" '.($order[21] == 'šluostės' ? 'selected' : '').'>šluostės</option>
                                <option value="12" '.($order[21] == 'vokas' ? 'selected' : '').'>vokas</option>
                                <option value="13" '.($order[21] == 'kont' ? 'selected' : '').'>kont</option>
                                <option value="14" '.($order[21] == '1/2 pll' ? 'selected' : '').'>1/2 pll</option>
                        </select></td>';
                $table .= '<td><input type="text" name="pll'.$i.'" class="inputElement" value="'.($order[22] ?: 0).'" style="width: 40px;"></td>';
                $table .= '<td><input type="text" name="length'.$i.'" class="inputElement" value="'.($order[23] ?: 0.8).'"></td>';
                $table .= '<td><input type="text" name="width'.$i.'" class="inputElement" value="'.($order[24] ?: 1.2).'"></td>';
                $table .= '<td><input type="text" name="height'.$i.'" class="inputElement" value="'.($order[25] ?: 1).'"></td>';

                $table .= '<td>
                                <select name="return'.$i.'" class="inputElement">
                                        <option value="-">---</option>
                                        <option value="minifest" '.($order[26] == 'minifest' ? 'selected' : '').'>Minifest</option>
                                        <option value="cmr" '.(strtolower($order[26]) == 'cmr' ? 'selected' : '').'>CMR</option>
                                        <option value="invoice" '.($order[26] == 'invoice' ? 'selected' : '').'>Sąskaita</option>   
                                </select>               
                        </td>';

                $table .= '<td><textarea class="inputElement" name="note'.$i.'" style="width: 150px;">'.$order[27].'</textarea></td>';
                $table .= '<input type="hidden" name="accountid'.$i.'" class="inputElement accountid" value="'.$order['accountid'].'">';
                $table .= '<input type="hidden" name="customer_id'.$i.'" class="inputElement customer_id" value="'.$order['customer_id'].'">';
                $table .= '</tr>';
         $count++;
        }
        $table .= '</tbody>';
        $table .= '</table>';
        $table .= '<input type="hidden" name="which" value="'.$which.'">';  
      
        if($type == 'metrika' AND count(json_decode($data['criticalOrders'],true)) == 0){
                $table .= '<input type="hidden" name="totalRecordsCount" value="'.$count.'">'; 
                $table .= '<input type="hidden" name="user_id" value="'.$user_id.'"> ';  
        }else if($type == 'critical'){
                $table .= '<input type="hidden" name="totalRecordsCount" value="'.$count.'">'; 
                $table .= '<input type="hidden" name="user_id" value="'.$user_id.'"> ';  
        } 

        
}

        return $table;        
}


 function importOrdersFromExel($db,$files){       
    $errorLog = array();
    $arrayID = array();

    /* saugojama informacija */


        $target_file = strtolower($files["fileToUpload"]["name"]);
        $ext = pathinfo($target_file,PATHINFO_EXTENSION);      
    

        if($ext == 'xlsx' || $ext == 'xls'){}
        else{
                $errorLog['wrongFormat'] = 'Pasirinktas blogas failo formatas. Gali būti tik xlsx arba xls';
                return $errorLog;
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($files["fileToUpload"]["tmp_name"]);
        
        $sheet = $spreadsheet->getActiveSheet();

        $excel = array(1,$sheet->toArray(null,true,true,true));   
        $data = array();
        
        $colums = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB');
        $colums2 =array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD');

        $mandatoryColumns = array(4,11,13,14,16,17,18,19,20); 

        $notMandatoryColumns = array(0,1,7,8,9,10,12,15,21,22,23,24,25,26,27);

        $columsNameToHuman = array(
            'Užsakymo grupė',
            'Užsakymo numeris',
            'Pasikrovimo data',
            'Pristatymo data',
            'Siuntėjo pavadinimas',
            'Siuntėjo adresas',
            'Siuntėjo miestas',
            'Siuntėjo rajonas',
            'Siuntėjo pašto kodas',
            'Siuntėjo kontaktinis asmuo',
            'Siuntėjo kontaktinio asmens telefonas',
            'Gavėjo pavadinimas',
            'Gavėjo įmonės kodas',
            'Gavėjo adresas',
            'Gavėjo miestas',
            'Gavėjo rajonas',
            'Gavėjo pašto kodas',
            'Gavėjo kontaktinis_asmuo',
            'Gavėjo telefonas',
            'Kiekis',
            'Svoris',
            'Matavimo vnt.',
            'Pll',
            'Ilgis',
            'Plotis',
            'Aukštis',
            'Gražinimas',
            'Pastabos'
        );

        // $errorLog['columsNameToHuman'] = $columsNameToHuman;

        // Sutvarkom array    

        $n = 1;    
        foreach($excel[1] AS $row){ 
                foreach($colums2 as $let){   
                        if($let != 'H' && $let != 'Q'){                       
                         $data[$n][] = $row[$let];
                        }
                        
                }
            $n++;
        }

        $dataClean = array();
        
        $g = 1; 
        foreach($data as $item) {
                if(empty($item[9]) && empty($item[10]) && empty($item[11]) && empty($item[13]) && empty($item[14]) && empty($item[14]) && empty($item[16]) && empty($item[17])){ }else{
                        $dataClean[$g] = $item;
                }
                $g++;
        }     

        // Pasalinam title, sutvarkom datą, patikrinam ar privalomi laukai užpildyti
        $data2 = array();
        $arrayID = array();
        $x = 1;
        foreach($dataClean as $item) {
            if($x == 1) {
                $x++;
                continue;
            }
            $clientExist = (empty($item[4]) ? false : clientExist($db,$item[4]));
            for($y =0; $y < count($colums); $y++) {
                if(isset($item)) {                          
                    if($y == 2 || $y == 3) {  
                        if($y == 2){                                                                 
                                $data2[$x][$y] = (empty($item[$y]) ? date("Y-m-d") : date("Y-m-d",strtotime(str_replace(".", "-", $item[$y]))));  
                        }else{
                                $data2[$x][$y] = (empty($item[$y]) ? checkWeekend(date("Y-m-d"),1) :checkWeekend(date("Y-m-d",strtotime(str_replace(".", "-", $item[$y]))),1));  
                        } 

                        $excel_date = (empty($item[$y]) ? date("Y-m-d") : date("Y-m-d",strtotime(str_replace(".", "-", $item[$y]))));   

                        if($y == 2 && strtotime($excel_date) < strtotime(date("Y-m-d"))){                        
                                $errorLog['critical'][$x][] = "Pasikrovimo data negali būti anksčiau nei šiandien";                               
                                $arrayID[] = $x;                               
                        }else if($y == 3 && strtotime($excel_date) < strtotime(date("Y-m-d"))){
                                $errorLog['critical'][$x][] = "Išsikrovimo data negali būti anksčiau nei šiandien";
                                $arrayID[] = $x;
                        }      
                    }elseif($y == 5 ) {                                                  
                                if(empty($item[5])){
                                        if(empty($clientExist['billing_address'])){
                                                $errorLog['critical'][$x][] = $columsNameToHuman[$y];  
                                                $arrayID[] = $x;
                                        }
                                }else{
                                        $data2[$x][$y] = $item[$y];
                                }
                     
                    }elseif($y == 6 ) {                                                   
                                if(empty($item[6])){
                                        if(empty($clientExist['municipality'])){
                                                $errorLog['critical'][$x][] = $columsNameToHuman[$y]; 
                                                $arrayID[] = $x;
                                        }
                                }else{
                                        $data2[$x][$y] = $item[$y];
                                }
                        
                    }elseif($y == 8) {                                       
                                if(empty($item[8])){
                                        if(empty($clientExist['post_code'])){
                                                $errorLog['critical'][$x][] = $columsNameToHuman[$y]; 
                                                $arrayID[] = $x; 
                                        }
                                }else{
                                        $data2[$x][$y] = $item[$y];
                                }                        
                    } else {
                        // Surasom klaidas
                        if(in_array($y,$mandatoryColumns)){
                            if(empty($item[$y])){
                                $errorLog['critical'][$x][] = $columsNameToHuman[$y];
                                $arrayID[] = $x;
                            }
                        }else{
                            if(empty($errorLog['critical'][$x])){
                                if(!in_array($y,$notMandatoryColumns)){
                                    if(empty($item[$y])){
                                        $errorLog['metrika'][$x][] = $columsNameToHuman[$y];                                        
                                    }
                                }
                            }
                        }                   
                        $data2[$x][$y] = $item[$y];  
                    }
                } 

                if($y == 4){                       
                        if(!$clientExist){
                                $errorLog['critical'][$x][] = "Kliento $item[4] crm sistemoje nėra";
                                $arrayID[] = $x; 
                        }else{
                                $data2[$x]['accountid'] = $clientExist['accountid'];               
                                $data2[$x]['customer_id'] = $clientExist['customer_id'];               
                                $data2[$x]['accountname'] = $clientExist['accountname'];               
                        }
                }

                // $y++;
            }        
       

            $errorLog['total'][] = $x;
            $x++;
        }

        $criticalOrders = array();

  
        // Perkeliam į kita array jei turi kritine klaidą
        $arrayID = array_unique($arrayID);
        foreach($arrayID AS $id){
            $criticalOrders[] = $data2[$id];
            unset($data2[$id]);
        }


        $ordersData = array();

        $ordersData['errorLog'] = $errorLog;
        $ordersData['orders'] = $data2;
        $ordersData['criticalOrders'] = $criticalOrders;
        // $ordersData['key'] = $key;


        // echo "<pre>";
        //         print_R($data2);
        // echo "</pre>";
        // die;
 return $ordersData;   
}


function checkWeekend($date, $add) {
        if($add > 0){
                $date = date('Y-m-d', strtotime($date. " + $add days"));
        }
                
        $day = date('N', strtotime($date));

        if($day == 6){
          $date = date('Y-m-d', strtotime($date. ' + 2 days'));
        }else if($day == 0){
          $date = date('Y-m-d', strtotime($date. ' + 1 days'));
        }

        return $date;
}


function clientExist($db,$name){
    $get_all_remembered_names = $db->prepare("SELECT * FROM app_import_orders_implicit_customers");  
    $get_all_remembered_names->execute(array());
    $remembered_names = $get_all_remembered_names->fetchAll(PDO::FETCH_ASSOC);

    $remembered_names_array = [];

    foreach ($remembered_names AS $res) {
        $remembered_names_array[$res['name']] = $res['changed_name'];
    }

    $name = clean($name);
    $name = (!empty($remembered_names_array[$name]) ? $remembered_names_array[$name] : $name);
    
    $stmt = $db->prepare("SELECT COUNT(salesorderid) as orders, accountname,a.accountid,billing_address,municipality,post_code,customer_id 
                                        FROM vtiger_account a
                                        LEFT JOIN vtiger_crmentity ON crmid=a.accountid  
                                        LEFT JOIN vtiger_salesorder  s ON s.accountid=a.accountid
                                        WHERE deleted = 0 AND accountname LIKE '$name%' 
                                        GROUP BY accountid");
               
    $stmt->execute(array());
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $accountsArr = array();

    if($stmt->rowCount()){ 
        foreach($result as $row){               
                $accountsArr[$row['orders']] = $row;   
        }

      $rez =  max($accountsArr);

        return array('accountid' => $rez['accountid'], 'accountname' => $rez['accountname'], 'billing_address' => $rez['billing_address'], 'municipality' => $rez['municipality'], 'post_code' => $rez['post_code'],'customer_id' => $rez['customer_id']);
    }else{
        return false;
    }
}

function initLoop($db,$post){

        // Kuriam uzsakymus
        $orders = [];   
        $implicit_customers = [];       
        for($i =1; $i <= $post['totalRecordsCount']; $i++){                 
                $e = count($orders[$post['group'.$i]])+1;                                
                $orders[$post['group'.$i]][$e][] = $post['customer_order_code'.$i]; // 0
                $orders[$post['group'.$i]][$e][] = $post['load_date'.$i]; // 1
                $orders[$post['group'.$i]][$e][] = $post['unload_date'.$i]; // 2
                $orders[$post['group'.$i]][$e][] = $post['load_company'.$i]; // 3
                $orders[$post['group'.$i]][$e][] = $post['bill_address'.$i]; // 4
                $orders[$post['group'.$i]][$e][] = $post['bill_city'.$i]; //5
                $orders[$post['group'.$i]][$e][] = $post['bill_distric'.$i]; // 6
                $orders[$post['group'.$i]][$e][] = $post['bill_code'.$i]; // 7
                $orders[$post['group'.$i]][$e][] = $post['load_person'.$i]; // 8
                $orders[$post['group'.$i]][$e][] = $post['load_phone'.$i]; // 9
                $orders[$post['group'.$i]][$e][] = $post['unload_company'.$i]; // 10
                $orders[$post['group'.$i]][$e][] = $post['company_code'.$i]; // 11
                $orders[$post['group'.$i]][$e][] = $post['ship_address'.$i]; // 12
                $orders[$post['group'.$i]][$e][] = $post['ship_city'.$i]; // 13
                $orders[$post['group'.$i]][$e][] = $post['ship_district'.$i]; // 14
                $orders[$post['group'.$i]][$e][] = $post['ship_code'.$i]; // 15
                $orders[$post['group'.$i]][$e][] = $post['unload_contact'.$i]; // 16
                $orders[$post['group'.$i]][$e][] = $post['unload_phone'.$i]; // 17
                $orders[$post['group'.$i]][$e][] = $post['quantity'.$i]; // 18
                $orders[$post['group'.$i]][$e][] = $post['weight'.$i]; // 19
                $orders[$post['group'.$i]][$e][] = $post['tare'.$i]; // 20
                $orders[$post['group'.$i]][$e][] = $post['pll'.$i]; // 21
                $orders[$post['group'.$i]][$e][] = $post['length'.$i]; // 22
                $orders[$post['group'.$i]][$e][] = $post['width'.$i]; // 23
                $orders[$post['group'.$i]][$e][] = $post['height'.$i]; // 24
                $orders[$post['group'.$i]][$e][] = $post['return'.$i]; // 25
                $orders[$post['group'.$i]][$e][] = $post['note'.$i]; // 26
                $orders[$post['group'.$i]][$e][] = $post['accountid'.$i];  // 27
                $orders[$post['group'.$i]][$e][] = $post['customer_id'.$i];  //28
                $orders[$post['group'.$i]][$e]['user_id'] = $post['user_id'];        
              $errorLog[] = $i; 
              
              if($post['load_company'.$i] != $post['old_load_company'.$i]){
                $implicit_customers[$post['old_load_company'.$i]] = $post['load_company'.$i];
              }
        }


        $res = createOrderAndExportToMetrika($db,$orders); 
        $date = date('Y-m-d H:i:s');
        $insert_new_names = $db->prepare("INSERT INTO app_import_orders_implicit_customers (name, changed_name, createdtime) VALUES (?,?,?)");


        if(count($implicit_customers) > 0){
                $get_all_remembered_names = $db->prepare("SELECT * FROM app_import_orders_implicit_customers");  
                $get_all_remembered_names->execute(array());
                $remembered_names = $get_all_remembered_names->fetchAll(PDO::FETCH_ASSOC);
            
                $remembered_names_array = [];
            
                foreach ($remembered_names AS $res) {
                    $remembered_names_array[$res['name']] = $res['changed_name'];
                }

                foreach ($implicit_customers as $key => $value) {
                        if(empty($remembered_names_array[$value])){
                         $insert_new_names->execute(array($key,$value,$date));
                        }
                }
        }
    
                setcookie("success", count($errorLog), time()+3600, "/","", 0);
                setcookie("success_metrika", count($res['success']), time()+3600, "/","", 0);
                setcookie("fail_metrika", count($res['fail']), time()+3600, "/","", 0);
                setcookie("errors", json_encode($res['errors']), time()+3600, "/","", 0);
                setcookie("imported", 1, time()+3600, "/","", 0);              
                $order = ($post['which'] == 'orders' ? 'orders_done' : 'critical_done');
                $stmt = $db->prepare("UPDATE app_import_draft_json SET orders_done = 1, critical_done = 1 WHERE id = ?");
                $stmt->execute(array($post['user_id']));
                header("Location: /vtlib/Vtiger/exel/import.php");
        

}

function createOrderAndExportToMetrika($db,$orders){
        
        $grouped_orders = array();
        foreach($orders AS $key => $order){  
                for($i=1;$i<=count($order);$i++){          
                        $responce = createSalesOrder($db,$order[$i]);                                                         
                        $grouped_orders[$key][$order[$i][28]][] = array('salesorderid' => $responce['id'], 'price' => $responce['price'], 'order' => $order[$i]);
                }                             
        }

        $array_for_json = array();
        
        $i = 1;
        foreach($grouped_orders AS  $firstDimension){  
                foreach($firstDimension AS $key => $item){  
                        $array_for_json[$key.$i]['customer_id'] = $key;                       
                        $loads = array(); 
                        for($e=0;$e<count($item);$e++){                                
                                $array_for_json[$key.$i]['shipments'][$e]['crm_id'] = $item[$e]['salesorderid'];
                                $array_for_json[$key.$i]['shipments'][$e]['shipment_type'] = 'parcel';
                                $array_for_json[$key.$i]['shipments'][$e]['notes'] = $item[$e]['order'][26];
                                $array_for_json[$key.$i]['shipments'][$e]['price'] = $item[$e]['price'];
                                $array_for_json[$key.$i]['shipments'][$e]['price_agreed'] = '';
                                $array_for_json[$key.$i]['shipments'][$e]['finish_price'] = $item[$e]['price'];  
                                $array_for_json[$key.$i]['shipments'][$e]['load_date_from'] = $item[$e]['order'][1]." 08:00:00";     
                                $array_for_json[$key.$i]['shipments'][$e]['load_date_to'] = $item[$e]['order'][1]." 17:00:00"; 
                                $array_for_json[$key.$i]['shipments'][$e]['load_company'] = $item[$e]['order'][3];  
                                $array_for_json[$key.$i]['shipments'][$e]['load_address'] = $item[$e]['order'][4];     
                                $array_for_json[$key.$i]['shipments'][$e]['load_settlement'] = '';     
                                $array_for_json[$key.$i]['shipments'][$e]['load_municipality'] = $item[$e]['order'][5];     
                                $array_for_json[$key.$i]['shipments'][$e]['load_region'] = null;     
                                $array_for_json[$key.$i]['shipments'][$e]['load_zipcode'] = $item[$e]['order'][7];     
                                $array_for_json[$key.$i]['shipments'][$e]['load_country'] = "LTU";     
                                $array_for_json[$key.$i]['shipments'][$e]['load_person'] =  $item[$e]['order'][8];     
                                $array_for_json[$key.$i]['shipments'][$e]['load_phone'] =  $item[$e]['order'][9];     
                                $array_for_json[$key.$i]['shipments'][$e]['load_company_code'] = '';                        
                                $array_for_json[$key.$i]['shipments'][$e]['unload_date_from'] = $item[$e]['order'][2]." 08:00:00";     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_date_to'] = $item[$e]['order'][2]." 17:00:00"; 
                                $array_for_json[$key.$i]['shipments'][$e]['unload_company'] = $item[$e]['order'][10];  
                                $array_for_json[$key.$i]['shipments'][$e]['unload_address'] = $item[$e]['order'][12];     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_settlement'] = '';     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_municipality'] = $item[$e]['order'][13];     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_region'] = null;     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_zipcode'] = $item[$e]['order'][15];     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_country'] = "LTU";     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_person'] = $item[$e]['order'][16];     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_phone'] = $item[$e]['order'][17];     
                                $array_for_json[$key.$i]['shipments'][$e]['unload_company_code'] = $item[$e]['order'][11];                   
                                // cargo
                                $loads['load_id'] = '';
                                $loads['update_type'] = 1;
                                $loads['cargo_qty'] = round($item[$e]['order'][18],0);
                                $loads['cargo_measure'] = $item[$e]['order'][20];
                                       if(($item[$e]['order'][20] == 1 OR $item[$e]['order'][20] == 2 OR $item[$e]['order'][20] == 3) OR ($item[$e]['order'][20] == 'pll' OR $item[$e]['order'][20] == 'RUS-pll' OR $item[$e]['order'][20] == 'FIN-pll')){
                                                $loads['volume_unit2'] = intval($item[$e]['order'][18]);    
                                        }else if($item[$e]['order'][20] == 14 OR $item[$e]['order'][20] == '1/2 pll'){
                                                $loads['volume_unit2'] = (float)$item[$e]['order'][18]/2; 
                                        }elseif($item[$e]['order'][20] == 7 OR $item[$e]['order'][20] == 'nestd.'){
                                                $loads['volume_unit2'] = (float)ceil((($item[$e]['order'][22]*$item[$e]['order'][23]) * $item[$e]['order'][18])/0.96);
                                        }else{
                                                $loads['volume_unit2'] = 0;
                                        }                                         

                                $loads['cargo_wgt'] = (float)str_replace(",",".", $item[$e]['order'][19]);
                                $loads['cargo_length'] = (float)str_replace(",",".", $item[$e]['order'][22]);      
                                $loads['cargo_width'] = (float)str_replace(",",".", $item[$e]['order'][23]);      
                                $loads['cargo_height'] = (float)str_replace(",",".", $item[$e]['order'][24]);   
                                $loads['cargo_ldm'] = '0.00';
                                $loads['cargo_facilities'] = '';
                                $loads['cargo_termo'] = 0;
                                $loads['cargo_return_documents'] = $item[$e]['order'][25];
                                $loads['cargo_notes'] = '';                               
                                $array_for_json[$key.$i]['shipments'][$e]['cargos'][] = $loads;
                     }                
                }
                $i++;
        }
        $orders_indexed = array_values($array_for_json);  
        
        $res = json_encode($orders_indexed, JSON_UNESCAPED_UNICODE);  


        $ch = curl_init();
          // ANCHOR PAKEISTI
        // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders_v2.php");
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders_v2.php");           
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders' => $res));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);         

        $data = json_decode($result, true);      

        $stmt = $db->prepare('UPDATE vtiger_salesorder SET sostatus = ?, ivaz_no = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?');
        $stmt2 = $db->prepare('UPDATE vtiger_inventoryproductrel SET external_order_id = ?, external_load_id = ? WHERE id = ? AND sequence_no = ?');
        $stmt3 = $db->prepare('DELETE FROM vtiger_inventoryproductrel_flags WHERE flag_id = ?');
       

        $order_sent = 'Sent';   
        $order_not_sent = 'Not Sent';     
        $success = array();
        $fail = array();
        $errors_message = array();        
        foreach($data AS $result){
                if($result['status'] == 'INSERTED'){                       
                        $stmt->execute(array($order_sent, $result['ivaz_no'], $result['direction'], $result['status'],'',$result['shipment_id'],$result['shipment_code'],$result['crm_id']));       
                        $stmt2->execute(array($result['shipment_id'], $result['loads'][0],$result['crm_id'],1));   
                        updateFlags($db,$result['shipment_id'],$result['loads'][0],$result['crm_id']);  
                        $success[] = $result['crm_id'];                         
                }else{     
                        $errors_message_crm = array();                
                        foreach($result['errors'] as $message){                               
                                $errors_message[] = $message['error_message'];
                                $errors_message_crm[] = $message['error_message'];
                        }
                        $errors = implode(",", $errors_message_crm);
                        $stmt->execute(array($order_not_sent,'','',$result['status'],$errors,'','',$result['crm_id']));   
                        $stmt3->execute(array($result['crm_id']));                            
                        $fail[] = $result['crm_id'];     
                }
              
        }       
       
        
         return array('success' => $success, 'fail' => $fail, 'errors' => $errors_message);
}


function createSalesOrder($db,$order){
            $errors = '';                
            $customer_order_code = $order[0];
            $load_date = $order[1];
            $unload_date = $order[2];
        
            $load_company = $order[3];   
            $bill_address = $order[4];
            $bill_city = $order[5];
            $bill_code = (strlen($order[7])== 4 ? "0".$order[7] : $order[7]);
        
            $load_person = $order[8];
            $load_phone = $order[9];

            $unload_company = $order[10];
            $unload_company_code = $order[11];
            $ship_address = $order[12];
            $ship_city = $order[13];
            $ship_county = $order[14];

            $ship_code = (strlen($order[15])== 4 ? "0".$order[15] : $order[15]);
            $unload_contact = $order[16];
            $unload_phone = $order[17];
        
            $quantity = $order[18];
            $weight = $order[19];
            $tare = $order[20];
            $pll = $order[21];
            $length = $order[22];
            $width = $order[23];
            $height = $order[24];
            $return = $order[25]; 
            $note = $order[26];        
            $accountid = $order[27];

            $user_id = $order['user_id'];
            $distance = 0;
            $m3 = round(($length * $width * $height) * $quantity,2);
            $m2 = round(($length * $width ) * $quantity,2);
            $date = date("Y-m-d H:i:s");

        // ANCHOR ATKOMENTUOTI
            $result = getPriceFromPriceBook($db, $bill_code,$ship_code,$accountid,$weight,$m3, $m2,$pll,$distance,'LTU','LTU','CLIENT');

            if($result['price'] == 0){
                $result = getPriceFromPriceBook($db, $bill_code,$ship_code,$accountid,$weight,$m3, $m2,$pll,$distance,'LTU','LTU','BAZINIS');
            }
      
            $price = $result['price'];
            $pricebook = $result['pricebook'];

            if(empty($pricebook)){
                    $pricebook = $result['combination'];
            }
            
			$stmt = $db->prepare("INSERT INTO vtiger_salesorder (salesorderid,subject,total,subtotal,accountid,sostatus,load_date_from,load_time_from,load_date_to,load_time_to,unload_date_from,unload_time_from,unload_date_to,unload_time_to,errors,source,import_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");				
			
			$stmt2 = $db->prepare("INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,margin,comment,description,cargo_measure,cargo_wgt,cargo_length,cargo_width,cargo_height,pll,source,ordered_weight,ordered_length,ordered_width,ordered_height,revised_weight,revised_length,revised_width,revised_height,m3,service) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			$stmt3 = $db->prepare("INSERT INTO vtiger_sobillads  (sobilladdressid,bill_city,bill_code,bill_country,bill_street,load_company,load_contact,load_phone) VALUES (?,?,?,?,?,?,?, ?)");
			$stmt4 = $db->prepare("INSERT INTO vtiger_soshipads (soshipaddressid,ship_city,ship_code,ship_country,ship_street,unload_company,unload_contact,unload_phone) VALUES (?,?,?,?,?,?,?,?)");
			$stmt5 = $db->prepare("INSERT INTO vtiger_salesordercf (salesorderid,cf_855,cf_1297,cf_1374, cf_1661,cf_1663,cf_1564, cf_1566, cf_2032) VALUES (?,?,?,?,?,?,?,?,?)");
                                              

			$last_entity_record = last_entity_id($db);
			$insert_entity = insert_entity_import($db, 'SalesOrder', $last_entity_record, $user_id, NULL,  '', $date);  
		
			$load_time_from = '08:00:00';				
			$load_time_to = '17:00:00';    
                        $unload_time_from = '08:00:00';				
			$unload_time_to = '17:00:00';                     	
	
			$type = 'Transporto užsakymas';

			$stmt->execute(array($last_entity_record,'Užsakymas',$price,$price,$accountid, 'Sent',$load_date,$load_time_from,$load_date,$load_time_to,$unload_date,$unload_time_from,$unload_date,$unload_time_to,$errors,'Excel',$date));
			$stmt3->execute(array($last_entity_record,$bill_city,$bill_code,'LTU',$bill_address,$load_company,$load_person,$load_phone));
			$stmt4->execute(array($last_entity_record, $ship_city,$ship_code,'LTU',$ship_address,$unload_company,$unload_contact,$unload_phone));
			$stmt5->execute(array($last_entity_record,$type,$pricebook,$price,$user_id,$user_id,$load_phone,$unload_phone,$customer_order_code));                  
                       
	

            $stmt2->execute(array($last_entity_record, 
                                                    14244,
                                                    1,
                                                    $quantity,
                                                    $price,
                                                    $note,
                                                    $note,                                                  
                                                    $tare,
                                                    $weight,
                                                    $length,
                                                    $width,
                                                    $height,
                                                    $pll,
                                                    'Excel',
                                                    $weight,
                                                    $length,
                                                    $width,
                                                    $height,
                                                    $weight,
                                                    $length,
                                                    $width,
                                                    $height,
                                                    $m3,
                                                    2));

        insertFlags($db,$last_entity_record,$return);

        return array('id' => $last_entity_record, 'price' => $price);                                        

		
}

function last_entity_id($db){
    $stmt = $db->prepare("SELECT id FROM vtiger_crmentity_seq");   
    $stmt->execute(array());
    $crmid = $stmt->fetch(PDO::FETCH_ASSOC);
    $crmid = $crmid['id']+1;	

    // Update sequence values
    $stmt = $db->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
    $stmt->execute(array($crmid));

    return $crmid;
}

function insert_entity_import($db, $setype, $entity_id, $user_id, $description, $label, $date) {
    $date = date("Y-m-d H:i:s");  
    // Insert into vtiger_crmentity 
    $stmt = $db->prepare("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, createdtime, modifiedtime,source)  VALUES (?,?,?,?,?,?,?,?,?)");
    $stmt->execute(array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date, 'CRM'));	
}

function multiexplode ($delimiters,$data) {
    $MakeReady = str_replace($delimiters, $delimiters[0], $data);
    $Return    = explode($delimiters[0], $MakeReady);
    return  $Return;
} 

function updateFlags($adb,$external_order_id,$load_id,$order_id){
        $stmt = $adb->prepare('UPDATE vtiger_inventoryproductrel_flags SET external_order_id = ?, external_load_id = ? WHERE flag_id = ?');
        $stmt->execute(array($external_order_id,$load_id,$order_id)); 
        
        $stmt2 = $adb->prepare('SELECT minifest,cmr,invoice FROM vtiger_inventoryproductrel_flags WHERE flag_id = ?');
        $stmt2->execute(array($order_id)); 
        $result = $stmt2->fetch(PDO::FETCH_ASSOC);
        $minifest = $result['minifest'];
        $cmr = $result['cmr'];
        $invoice = $result['invoice'];

        sentFlags($adb,$minifest, $cmr,$invoice, $load_id);
}

function insertFlags($adb,$order_id,$return){
        
        $minifest = (strtolower($return) == 'minifest' ? 1 : 0);
        $cmr = (strtolower($return) == 'cmr' ? 1 : 0);
        $invoice = (strtolower($return) == 'invoice' ? 1 : 0);       
    
        $stmt = $adb->prepare('INSERT INTO vtiger_inventoryproductrel_flags (flag_id,minifest,cmr,invoice,import_date) VALUES (?,?,?,?,?)');
        $stmt->execute(array($order_id,$minifest,$cmr,$invoice,date("Y-m-d H:i:s")));    
}

function sentFlags($adb,$minifest, $cmr,$invoice, $load_id){        

        $order = array();              


        if($minifest > 0){
        $order['load_flags'][] = array('load_id' => $load_id, 'key' => 'flg_minifest','value' => $minifest);
        }
        if($cmr > 0){
        $order['load_flags'][] = array('load_id' => $load_id, 'key' => 'flg_cmr', 'value' => $cmr);
        }
        if($invoice > 0){
        $order['load_flags'][] = array('load_id' => $load_id, 'key' => 'flg_invoice','value' => $invoice);
        }
        $res = json_encode($order);
        $ch = curl_init();
          // ANCHOR PAKEISTI
        // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/load_flags.php");     
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/load_flags.php");                
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'load_flags' => $res));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $responce = curl_exec($ch);
        curl_close($ch);

}