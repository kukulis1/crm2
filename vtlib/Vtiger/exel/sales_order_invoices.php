<?php
global $_REQUEST;
require 'vendor/autoload.php';
require_once "../../../config.inc.php";

error_reporting(0);

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if(!empty($_REQUEST['search_parameters'])){
	$params = explode(":", $_REQUEST['search_parameters']);

	for($i = 0; count($params) > $i; $i++){
		$search_params[] = explode('=', $params[$i]);
	}
}

$viewname = $_REQUEST['viewname'];


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//  Template processor instance creation
$reader = IOFactory::createReader('Xlsx');

$spreadsheet = $reader->load(__DIR__ . '/template_salesorder_invoice.xlsx');

$columnsQuery = "SELECT vtiger_cvcolumnlist.columnname 
																					 FROM vtiger_cvcolumnlist 
																					 INNER JOIN vtiger_customview ON vtiger_customview.cvid = vtiger_cvcolumnlist.cvid 
																					 WHERE vtiger_customview.cvid = '$viewname' 
																					 ORDER BY vtiger_cvcolumnlist.columnindex";


$col = $conn->query($columnsQuery);
while ($row = $col->fetch_assoc()){
	$colTemp[] = explode(":", $row['columnname']);
}


$listPeriodQuery = "SELECT comparator FROM vtiger_cvadvfilter WHERE vtiger_cvadvfilter.cvid = '$viewname'";
$list = $conn->query($listPeriodQuery);
$list = $list->fetch_assoc();

if($list['comparator'] == 'today'){
	$listPeriod = array(date('Y-m-d'));
}elseif($list['comparator'] == 'yesterday'){
	$listPeriod = array(date('Y-m-d', strtotime('-1 days')));
}elseif($list['comparator'] == 'thismonth'){
	$first = date("Y-m-d", strtotime("first day of this month"));
	$last = date("Y-m-d", strtotime("last day of this month"));
	$listPeriod = array($first,$last);
}elseif($list['comparator'] == 'lastmonth'){
	$month_ini = new DateTime("first day of last month");
	$month_end = new DateTime("last day of last month");
	$first = $month_ini->format('Y-m-d'); 
	$last = $month_end->format('Y-m-d');
	$listPeriod = array($first,$last);
}


$defaultColumns = " vtiger_invoice.invoiceid, invoicedate as date, accountname, invoice_no as nr, subtotal as sub, s_h_amount as pvm, total, duedate as due ";

$sql = "SELECT ";
	for($l = 0; $l < count($colTemp); $l++){		
		if($colTemp[$l][1] == 'smownerid'){
			$sql .= " CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user_id ".(count($colTemp) > $l+1 ? ',' : '');
		}elseif($colTemp[$l][1] == 'accountid'){
			$sql .= " accountname AS account_id ".(count($colTemp) > $l+1 ? ',' : '');
		}else{
			$sql .= $colTemp[$l][0].".".($colTemp[$l][1] == $colTemp[$l][2] ? $colTemp[$l][1] : $colTemp[$l][1]." AS ".$colTemp[$l][2]).(count($colTemp) > $l+1 ? ',' : '');
		}		

	}

$sql	.= " ,$defaultColumns FROM vtiger_invoice 
					LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid		
					JOIN vtiger_users ON vtiger_users.id=vtiger_crmentity.smownerid			
					LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid
					LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_invoice.accountid
                    LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid = vtiger_invoice.invoiceid
					WHERE vtiger_crmentity.deleted=0 AND setype = 'Invoice' ";
					if(isset($listPeriod)){
						if(count($listPeriod) > 1){
							$sql .= " AND vtiger_crmentity.createdtime BETWEEN '$listPeriod[0] 00:00:00' AND '$listPeriod[1] 23:59:59' ";
						}else{
							$sql .= " AND vtiger_crmentity.createdtime LIKE '$listPeriod[0]%' ";
						}
					}

if(isset($search_params)){
	$sql .=  " HAVING ";

	for($e = 0; count($search_params) > $e; $e++){
		$column = $search_params[$e][0];
		$value = str_replace('"',"", $search_params[$e][1]);
		
		if($column == 'createdtime' OR $column == 'invoicedate' OR $column == 'duedate'){
			$period = explode(",",$value);
			$sql .=  " $column BETWEEN '$period[0] 00:00:00' AND '$period[1] 23:59:59' ".($e+1 < COUNT($search_params) ? 'AND ' :'');		
		}else{
			$sql .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
		}
	}
}

$sql .= " ORDER BY vtiger_invoice.invoiceid	 DESC";
	if(!isset($listPeriod)){
		$sql .= " LIMIT 1000";
	}

$rsSql=$conn->query($sql);


$baseRow = 4;
$r=0;
while ($data2 = $rsSql->fetch_assoc()){
		$payment_info = getInvoicePayInfo($data2['invoiceid'],$conn);
    $row = $baseRow + $r;
		$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);  
			$spreadsheet->getActiveSheet()->setCellValue('A' . $row, $r + 1)
					->setCellValue('A' . $row, $data2['date'])
					->setCellValue('B' . $row, $data2['accountname'])				
					->setCellValue('C' . $row, $data2['nr'])
					->setCellValue('D' . $row, $data2['sub'])
					->setCellValue('E' . $row, $data2['pvm'])
					->setCellValue('F' . $row, $data2['total'])
					->setCellValue('G' . $row, $data2['due'])
					->setCellValue('H' . $row, $payment_info['PAID_SUM'])
					->setCellValue('I' . $row, $payment_info['PAYED_DATE']);
	
		$r= $r+1;  
}


$spreadsheet->getActiveSheet()->removeRow($baseRow - 1, 1);
$temp_file = tempnam(sys_get_temp_dir(), 'Excel');
// Save EXCEL
$writer = new Xlsx($spreadsheet);


$writer->save($temp_file);
// Save PDF
// $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf($spreadsheet);
// $writer->save($temp_file);
// ------------------ Operation with file result -------------------------------------------
$documento = file_get_contents($temp_file);
unlink($temp_file);  // delete file tmp

$date = date("Y-m-d");

// header("Content-Disposition: attachment; filename=$subject.xlsx");
header("Content-Disposition: attachment; filename=saleorders_invoices_$date.xlsx");
header("Content-Type: application/excel;  charset=UTF-8");
echo $documento;


function getInvoicePayInfo($recordID,$conn){

	$result = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(debts_tks_suma),2) AS total_sum, MAX(debts_tks_data) AS payed_date
	FROM vtiger_debts 
	LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid=vtiger_debts.debtsid 	 
	INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_debts.debtsid
	WHERE deleted = 0 AND vtiger_crmentityrel.crmid = $recordID AND vtiger_crmentityrel.relmodule = 'Debts' AND vtiger_crmentity.setype = 'Debts'"));

	$total_sum = $result['total_sum'];
	$payed_date = $result['payed_date'];

	return array('PAID_SUM' => $total_sum, 'PAYED_DATE' => $payed_date);
}

?>