<?php
global $_REQUEST;
require 'vendor/autoload.php';
require_once "../../../config.inc.php";

error_reporting(0);

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


if(!empty($_REQUEST['search_parameters'])){
	$params = explode(":", $_REQUEST['search_parameters']);

	for($i = 0; count($params) > $i; $i++){
		$search_params[] = explode('=', $params[$i]);
	}
}


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//  Template processor instance creation
$reader = IOFactory::createReader('Xlsx');

$spreadsheet = $reader->load(__DIR__ . '/template_purchase_detail.xlsx');

$viewname = $_REQUEST['viewname'];
$columnsQuery = "SELECT vtiger_cvcolumnlist.columnname FROM vtiger_cvcolumnlist 
																					 INNER JOIN vtiger_customview ON vtiger_customview.cvid = vtiger_cvcolumnlist.cvid 
																					 WHERE vtiger_customview.cvid = '$viewname' 
																					 ORDER BY vtiger_cvcolumnlist.columnindex";


$col = $conn->query($columnsQuery);

while ($row = $col->fetch_assoc()){
	$colTemp[] = explode(":", $row['columnname']);
}



$listPeriodQuery = "SELECT comparator FROM vtiger_cvadvfilter WHERE vtiger_cvadvfilter.cvid = '$viewname'";
$list = $conn->query($listPeriodQuery);
$list = $list->fetch_assoc();

if($list['comparator'] == 'today'){
	$listPeriod = array(date('Y-m-d'));
}elseif($list['comparator'] == 'yesterday'){
	$listPeriod = array(date('Y-m-d', strtotime('-1 days')));
}elseif($list['comparator'] == 'thismonth'){
	$first = date("Y-m-d", strtotime("first day of this month"));
	$last = date("Y-m-d", strtotime("last day of this month"));
	$listPeriod = array($first,$last);
}elseif($list['comparator'] == 'lastmonth'){
	$month_ini = new DateTime("first day of last month");
	$month_end = new DateTime("last day of last month");
	$first = $month_ini->format('Y-m-d'); 
	$last = $month_end->format('Y-m-d');
	$listPeriod = array($first,$last);
}


$defaultColumns = " purchaseorder_no,vendorname,cf_1355,subtotal,s_h_amount,total,paid,balance,duedate,cf_1345,CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user ";


$sql = "SELECT ";
	for($l = 0; $l < count($colTemp); $l++){		
		if($colTemp[$l][1] == 'smownerid'){
			$sql .= " CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user_id ".(count($colTemp) > $l+1 ? ',' : '');
		}elseif($colTemp[$l][1] == 'vendorid'){
			$sql .= " vendorname AS vendor_id ".(count($colTemp) > $l+1 ? ',' : '');


		 }elseif($colTemp[$l][1] == 'glacct' OR $colTemp[$l][1] == 'category'){
			$sql .= '';
		}else{
			$sql .= $colTemp[$l][0].".".($colTemp[$l][1] == $colTemp[$l][2] ? $colTemp[$l][1] : $colTemp[$l][1]." AS ".$colTemp[$l][2]).(count($colTemp) > $l+1 ? ',' : '');
		}		

	}


$sql .=  " ,$defaultColumns FROM vtiger_purchaseorder 
						LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_purchaseorder.purchaseorderid					
						LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_purchaseorder.vendorid
						LEFT JOIN vtiger_crmentity aui ON aui.crmid=vtiger_vendor.vendorid
						LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=vtiger_purchaseorder.purchaseorderid
						LEFT JOIN vtiger_users ON vtiger_users.id=vtiger_crmentity.smownerid
						LEFT JOIN vtiger_users AS vtiger_users2 ON vtiger_users2.id=aui.smownerid				
						WHERE vtiger_crmentity.deleted=0 ";
						if(isset($list)){
							$sql .= " AND vtiger_purchaseordercf.cf_1345 BETWEEN '$listPeriod[0]' AND '$listPeriod[1]' ";
						}



						
$sql2 =  "SELECT vtiger_inventoryproductrel.cargo_wgt, vtiger_inventoryproductrel.cargo_length as user, purchase_date, costcenter_tks_cost, quantity, cargo_measure, 
CASE WHEN listprice IS NULL
	THEN 0
	ELSE listprice
END AS listprice2, ";


for($l = 0; $l < count($colTemp); $l++){		
	if($colTemp[$l][1] == 'smownerid'){
		$sql2 .= " CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user_id ".(count($colTemp) > $l+1 ? ',' : '');
	}elseif($colTemp[$l][1] == 'vendorid'){
		$sql2 .= " vendorname AS vendor_id ".(count($colTemp) > $l+1 ? ',' : '');
	}elseif($colTemp[$l][1] == 'glacct' OR $colTemp[$l][1] == 'category'){
		$sql2 .= '';
	}else{
		$sql2 .= $colTemp[$l][0].".".($colTemp[$l][1] == $colTemp[$l][2] ? $colTemp[$l][1] : $colTemp[$l][1]." AS ".$colTemp[$l][2]).(count($colTemp) > $l+1 ? ',' : '');
	}		

}
$sql2 .=  " ,$defaultColumns	FROM vtiger_purchaseorder 
					LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_purchaseorder.purchaseorderid					
					LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_purchaseorder.vendorid
					LEFT JOIN vtiger_crmentity aui ON aui.crmid=vtiger_vendor.vendorid
					LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=vtiger_purchaseorder.purchaseorderid
					LEFT JOIN vtiger_users ON vtiger_users.id=vtiger_crmentity.smownerid
					LEFT JOIN vtiger_users AS vtiger_users2 ON vtiger_users2.id=aui.smownerid		
					LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_purchaseorder.purchaseorderid
					LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=vtiger_inventoryproductrel.cargo_width
					WHERE vtiger_crmentity.deleted=0 AND vtiger_crmentity.setype = 'PurchaseOrder' ";
						if(isset($list)){
							$sql2 .= " AND vtiger_purchaseordercf.cf_1345 BETWEEN '$listPeriod[0]' AND '$listPeriod[1]' ";
						}
						$sql2 .=  " GROUP BY vtiger_purchaseorder.purchaseorder_no,vtiger_inventoryproductrel.sequence_no ";


if(isset($search_params)){
	$sql .=  " HAVING ";
	$sql2 .=  " HAVING ";

	for($e = 0; count($search_params) > $e; $e++){
		$column = $search_params[$e][0];
		$value = str_replace('"',"", $search_params[$e][1]);
		
		if($column == 'createdtime' OR $column == 'cf_1345' OR $column == 'duedate'){
			$period = explode(",",$value);
			$sql .=  " $column BETWEEN '$period[0] 00:00:00' AND '$period[1] 23:59:59' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
			$sql2 .=  " $column BETWEEN '$period[0] 00:00:00' AND '$period[1] 23:59:59' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
		}else{
			$sql .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
			$sql2 .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
		}
	}
}

$sql .= " ORDER BY vtiger_purchaseorder.purchaseorderid DESC";
$sql2 .= " ORDER BY vtiger_purchaseorder.purchaseorderid DESC";


if(!isset($search_params)){
	$sql .= " LIMIT 500";
	$sql2 .= " LIMIT 500";
}


// echo "<pre>";
// echo $sql2;
// echo "</pre>";
// die();
$rsSql=$conn->query($sql);
$rsSql2=$conn->query($sql2);

$arr = array();
$arr2 = array();


while ($row = $rsSql->fetch_assoc()){
	$arr['title'][] = $row;
}


while ($row = $rsSql2->fetch_assoc()){	
	$arr2['details'][] = $row;
}

$result = array(
	'title' => $arr,
	'details' => $arr2
);



$baseRow = 4;
$values = array();


for($n = 0; count($arr2['details']) > $n; $n++){
	$values[] = $arr2['details'][$n]['purchaseorder_no'];
}

$pur = array_count_values($values);

foreach($result AS $data){

		for($e = 0; count($data['details']) > $e; $e++){
			$row2 = $baseRow + $e;		
			$spreadsheet->getActiveSheet()->setCellValue('L'.$row2, $data['details'][$e]['cargo_wgt']);		
			$spreadsheet->getActiveSheet()->setCellValue('M'.$row2, $data['details'][$e]['purchase_date']);		
			$spreadsheet->getActiveSheet()->setCellValue('N'.$row2, $data['details'][$e]['costcenter_tks_cost']);		
			$spreadsheet->getActiveSheet()->setCellValue('O'.$row2, $data['details'][$e]['user']);		
			$spreadsheet->getActiveSheet()->setCellValue('P'.$row2, $data['details'][$e]['cargo_measure']);		
			$spreadsheet->getActiveSheet()->setCellValue('Q'.$row2, $data['details'][$e]['quantity']);		
			$spreadsheet->getActiveSheet()->setCellValue('R'.$row2, $data['details'][$e]['listprice2']);		
		}			

		$e = 0;
		for($i = 0; count($data['title']) > $i; $i++){		
				
			$row = $baseRow + $e;		
			$key = $data['title'][$i]['purchaseorder_no'];

			$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);  
				$spreadsheet->getActiveSheet()->setCellValue('A' . $row, $e + 1)
				->setCellValue('A' . $row, $data['title'][$i]['purchaseorder_no'])
				->setCellValue('B' . $row, $data['title'][$i]['vendorname'])				
				->setCellValue('C' . $row, $data['title'][$i]['cf_1355'])
				->setCellValue('D' . $row, $data['title'][$i]['subtotal'])
				->setCellValue('E' . $row, $data['title'][$i]['s_h_amount'])
				->setCellValue('F' . $row, $data['title'][$i]['hdnGrandTotal'])
				->setCellValue('G' . $row, $data['title'][$i]['paid'])
				->setCellValue('H' . $row, $data['title'][$i]['balance'])
				->setCellValue('I' . $row, $data['title'][$i]['duedate'])
				->setCellValue('J' . $row, $data['title'][$i]['cf_1345'])
				->setCellValue('K' . $row, $data['title'][$i]['assigned_user']);		

			$e = $e + $pur[$key];
		}
	  
}


$spreadsheet->getActiveSheet()->removeRow($baseRow - 1, 1);
$temp_file = tempnam(sys_get_temp_dir(), 'Excel');
// Save EXCEL
$writer = new Xlsx($spreadsheet);


$writer->save($temp_file);
// Save PDF
// $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf($spreadsheet);
// $writer->save($temp_file);
// ------------------ Operation with file result -------------------------------------------
$documento = file_get_contents($temp_file);
unlink($temp_file);  // delete file tmp

$date = date("Y-m-d");

// header("Content-Disposition: attachment; filename=$subject.xlsx");
header("Content-Disposition: attachment; filename=purchase_detail_orders_$date.xlsx");
header("Content-Type: application/excel;  charset=UTF-8");
echo $documento;

?>