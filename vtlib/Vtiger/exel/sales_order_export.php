<?php
global $_REQUEST;
require_once "../../../config.inc.php";

global $root_directory;

set_time_limit(900);
// error_reporting(E_ALL);

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if(!empty($_REQUEST['search_parameters'])){
	$params = explode(":", $_REQUEST['search_parameters']);

	for($i = 0; count($params) > $i; $i++){
		$search_params[] = explode('=', $params[$i]);
	}
}

$viewname = $_REQUEST['viewname'];



$columnsQuery = "SELECT vtiger_cvcolumnlist.columnname 
																					 FROM vtiger_cvcolumnlist 
																					 INNER JOIN vtiger_customview ON vtiger_customview.cvid = vtiger_cvcolumnlist.cvid 
																					 WHERE vtiger_customview.cvid = '$viewname' 
																					 ORDER BY vtiger_cvcolumnlist.columnindex";


$col = $conn->query($columnsQuery);

while ($row = $col->fetch_assoc()){
	$colTemp[] = explode(":", $row['columnname']);
}


$listPeriodQuery = "SELECT comparator FROM vtiger_cvadvfilter WHERE vtiger_cvadvfilter.cvid = '$viewname'";
$list = $conn->query($listPeriodQuery);
$list = $list->fetch_assoc();

if($list['comparator'] == 'today'){
	$listPeriod = array(date('Y-m-d'));
}elseif($list['comparator'] == 'yesterday'){
	$listPeriod = array(date('Y-m-d', strtotime('-1 days')));
}elseif($list['comparator'] == 'thismonth'){
	$first = date("Y-m-d", strtotime("first day of this month"));
	$last = date("Y-m-d", strtotime("last day of this month"));
	$listPeriod = array($first,$last);
}elseif($list['comparator'] == 'lastmonth'){
	$month_ini = new DateTime("first day of last month");
	$month_end = new DateTime("last day of last month");
	$first = $month_ini->format('Y-m-d'); 
	$last = $month_end->format('Y-m-d');
	$listPeriod = array($first,$last);
}


// echo "<pre>";
// print_R($listPeriod);
// echo "</pre>";
// echo "</pre>";
// die();



$defaultColumns = " shipment_code as shipment_id,accountname,load_company AS sender, unload_company AS receiver, REPLACE(total,'.',',') as sum, CONCAT(bill_street,', ',bill_city,', ',bill_code) AS load_loc, CONCAT(ship_street,', ',ship_city,', ',ship_code) AS unload_loc, DATE_FORMAT(cf_1504, '%Y-%m-%d') AS order_date ";

$sql = "SELECT DISTINCT ";
	for($l = 0; $l < count($colTemp); $l++){		
		if($colTemp[$l][1] == 'smownerid'){
			$sql .= " CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user_id ".(count($colTemp) > $l+1 ? ',' : '');
		}elseif($colTemp[$l][1] == 'accountid'){
			$sql .= " accountname AS account_id ".(count($colTemp) > $l+1 ? ',' : '');
		}else{
			$sql .= $colTemp[$l][0].".".($colTemp[$l][1] == $colTemp[$l][2] ? $colTemp[$l][1] : $colTemp[$l][1]." AS ".$colTemp[$l][2]).(count($colTemp) > $l+1 ? ',' : '');
		}		

	}

$sql	.= " ,$defaultColumns FROM vtiger_salesorder 
					LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid						
					LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
					LEFT JOIN vtiger_users ON vtiger_users.id=vtiger_crmentity.smownerid			
					LEFT JOIN	vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
					LEFT JOIN	vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
					LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_salesorder.accountid
					WHERE vtiger_crmentity.deleted=0 AND setype = 'SalesOrder' ";
					if(!isset($search_params)){
						if(count($listPeriod) > 1){
							$sql .= " AND DATE_FORMAT(vtiger_crmentity.createdtime,'%Y-%m-%d') BETWEEN '$listPeriod[0]' AND '$listPeriod[1]' ";
						}else{
							$sql .= " AND DATE_FORMAT(vtiger_crmentity.createdtime,'%Y-%m-%d') LIKE '$listPeriod[0]%' ";
						}
					}

if(isset($search_params)){
	$sql .=  " HAVING ";

	for($e = 0; count($search_params) > $e; $e++){
		$column = $search_params[$e][0];
		$value = str_replace('"',"", $search_params[$e][1]);
		
		if($column == 'createdtime' OR $column == 'cf_1345' OR $column == 'duedate' OR $column == 'load_date_from' OR $column == 'unload_date_from' OR $column == 'load_date_to' OR $column == 'unload_date_to'){
			$period = explode(",",$value);
			$sql .=  " DATE_FORMAT($column,'%Y-%m-%d') BETWEEN '$period[0]' AND '$period[1]' ".($e+1 < COUNT($search_params) ? 'AND ' :'');		
		}else{
			$sql .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
		}
	}
}

$sql .= " ORDER BY vtiger_salesorder.salesorderid	DESC";
	if(!isset($search_params)){
		$sql .= " LIMIT 500";
	}


$result = $conn->query($sql);

$list = [];
$list[] = ['Siuntos Nr.','Kliento pavadinimas','Siuntėjas','Gavėjas','Suma','Pakrovimas','Iškrovimas','Užsakymo data'];
	
foreach ($result as $info) {
	$list[] = [str_replace(PHP_EOL, '', $info['shipment_id']),
						 str_replace(PHP_EOL, '', $info['accountname']),
						 str_replace(PHP_EOL, '',$info['sender']),
						 str_replace(PHP_EOL, '', $info['receiver']),
						 $info['sum'],
						 str_replace(PHP_EOL, '', $info['load_loc']),
						 str_replace(PHP_EOL, '', $info['unload_loc']),
						 str_replace(PHP_EOL, '', $info['order_date'])];
}


$fp = fopen('file.csv', 'w');

foreach ($list as $fields) {
	fputcsv($fp, $fields,';');
}

fclose($fp);

$date = date("Y-m-d");

header('Content-Type: application/csv');
header("Content-Disposition: attachment; filename=sales_orders_$date.csv");
header('Pragma: no-cache');
readfile("$root_directory/vtlib/Vtiger/exel/file.csv");
unlink("$root_directory/vtlib/Vtiger/exel/file.csv");

