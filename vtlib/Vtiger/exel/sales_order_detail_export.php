<?php
global $_REQUEST;
require 'vendor/autoload.php';
require_once "../../../config.inc.php";

error_reporting(0);
set_time_limit(900);

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


if(!empty($_REQUEST['search_parameters'])){
	$params = explode(":", $_REQUEST['search_parameters']);

	for($i = 0; count($params) > $i; $i++){
		$search_params[] = explode('=', $params[$i]);
	}
}


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//  Template processor instance creation
$reader = IOFactory::createReader('Xlsx');

$spreadsheet = $reader->load(__DIR__ . '/template_salesorder_detail.xlsx');

$viewname = $_REQUEST['viewname'];
$columnsQuery = "SELECT vtiger_cvcolumnlist.columnname FROM vtiger_cvcolumnlist 
																					 INNER JOIN vtiger_customview ON vtiger_customview.cvid = vtiger_cvcolumnlist.cvid 
																					 WHERE vtiger_customview.cvid = '$viewname' 
																					 ORDER BY vtiger_cvcolumnlist.columnindex";


$col = $conn->query($columnsQuery);

while ($row = $col->fetch_assoc()){
	$colTemp[] = explode(":", $row['columnname']);
}


$listPeriodQuery = "SELECT `comparator` FROM vtiger_cvadvfilter WHERE vtiger_cvadvfilter.cvid = '$viewname'";
$list = $conn->query($listPeriodQuery);
$list = $list->fetch_assoc();

if($list['comparator'] == 'today'){
	$listPeriod = array(date('Y-m-d'));
}elseif($list['comparator'] == 'yesterday'){
	$listPeriod = array(date('Y-m-d', strtotime('-1 days')));
}elseif($list['comparator'] == 'thismonth'){
	$first = date("Y-m-d", strtotime("first day of this month"));
	$last = date("Y-m-d", strtotime("last day of this month"));
	$listPeriod = array($first,$last);
}elseif($list['comparator'] == 'lastmonth'){
	$month_ini = new DateTime("first day of last month");
	$month_end = new DateTime("last day of last month");
	$first = $month_ini->format('Y-m-d'); 
	$last = $month_end->format('Y-m-d');
	$listPeriod = array($first,$last);
}


$defaultColumns = " shipment_code as shipment_id,accountname,load_company AS sender, unload_company AS receiver, total as sum, CONCAT(bill_street,', ',bill_city,', ',bill_code) AS load_loc, CONCAT(ship_street,', ',ship_city,', ',ship_code) AS unload_loc, DATE_FORMAT(cf_1504, '%Y-%m-%d') AS order_date ";


$sql = "SELECT DISTINCT salesorder_no,";
	for($l = 0; $l < count($colTemp); $l++){		
		if($colTemp[$l][1] == 'smownerid'){
			$sql .= " CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user_id ".(count($colTemp) > $l+1 ? ',' : '');
		}elseif($colTemp[$l][1] == 'accountid'){
			$sql .= " accountname AS account_id ".(count($colTemp) > $l+1 ? ',' : '');
		}else{
			$sql .= $colTemp[$l][0].".".($colTemp[$l][1] == $colTemp[$l][2] ? $colTemp[$l][1] : $colTemp[$l][1]." AS ".$colTemp[$l][2]).(count($colTemp) > $l+1 ? ',' : '');
		}		

	}


	$sql	.= " ,$defaultColumns FROM vtiger_salesorder 
	LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid						
	LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
	LEFT JOIN vtiger_users ON vtiger_users.id=vtiger_crmentity.smownerid			
	LEFT JOIN	vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
	LEFT JOIN	vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
	LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_salesorder.accountid
	WHERE vtiger_crmentity.deleted=0 AND vtiger_crmentity.setype = 'SalesOrder' ";
	if(!isset($search_params)){
		if(count($listPeriod) > 1){
			$sql .= " AND vtiger_crmentity.createdtime BETWEEN '$listPeriod[0] 00:00:00' AND '$listPeriod[1] 23:59:59' ";
		}else{
			$sql .= " AND vtiger_crmentity.createdtime LIKE '$listPeriod[0]%' ";
		}
	}



						
$sql2 =  "SELECT DISTINCT salesorder_no, vtiger_inventoryproductrel.cargo_wgt, vtiger_inventoryproductrel.cargo_length, vtiger_inventoryproductrel.cargo_width, vtiger_inventoryproductrel.cargo_height, quantity, code,";


for($l = 0; $l < count($colTemp); $l++){		
	if($colTemp[$l][1] == 'smownerid'){
		$sql2 .= " CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user_id ".(count($colTemp) > $l+1 ? ',' : '');
	}elseif($colTemp[$l][1] == 'accountid'){
		$sql2 .= " accountname AS account_id ".(count($colTemp) > $l+1 ? ',' : '');
	}else{
		$sql2 .= $colTemp[$l][0].".".($colTemp[$l][1] == $colTemp[$l][2] ? $colTemp[$l][1] : $colTemp[$l][1]." AS ".$colTemp[$l][2]).(count($colTemp) > $l+1 ? ',' : '');
	}		

}
$sql2	.= " ,$defaultColumns FROM vtiger_salesorder 
														LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid	
														LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid						
														LEFT JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
														LEFT JOIN vtiger_users ON vtiger_users.id=vtiger_crmentity.smownerid			
														LEFT JOIN	vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
														LEFT JOIN	vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
														LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_salesorder.accountid
														LEFT JOIN app_measures ON app_measures.id=vtiger_inventoryproductrel.cargo_measure OR app_measures.code=vtiger_inventoryproductrel.cargo_measure
														WHERE vtiger_crmentity.deleted=0 AND setype = 'SalesOrder' ";
														if(!isset($search_params)){
															if(count($listPeriod) > 1){
																$sql2 .= " AND vtiger_crmentity.createdtime BETWEEN '$listPeriod[0] 00:00:00' AND '$listPeriod[1] 23:59:59' ";
															}else{
																$sql2 .= " AND vtiger_crmentity.createdtime LIKE '$listPeriod[0]%' ";
															}
														}

						$sql2 .=  " GROUP BY vtiger_salesorder.salesorder_no,vtiger_inventoryproductrel.sequence_no, vtiger_inventoryproductrel.external_load_id ";


if(isset($search_params)){
	$sql .=  " HAVING ";
	$sql2 .=  " HAVING ";

	for($e = 0; count($search_params) > $e; $e++){
		$column = $search_params[$e][0];
		$value = str_replace('"',"", $search_params[$e][1]);
		
		if($column == 'createdtime' OR $column == 'cf_1345' OR $column == 'duedate' OR $column == 'load_date_from' OR $column == 'unload_date_from' OR $column == 'load_date_to' OR $column == 'unload_date_to'){
			$period = explode(",",$value);
			$sql .=  " $column BETWEEN '$period[0] 00:00:00' AND '$period[1] 23:59:59' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
			$sql2 .=  " $column BETWEEN '$period[0] 00:00:00' AND '$period[1] 23:59:59' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
		}else{
			$sql .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
			$sql2 .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
		}
	}
}

$sql .= " ORDER BY vtiger_salesorder.salesorderid DESC";
$sql2 .= " ORDER BY vtiger_salesorder.salesorderid DESC";

if(!isset($search_params)){
	$sql .= " LIMIT 500";
	$sql2 .= " LIMIT 500";
}

// echo "<pre>";
// echo $sql2;
// echo "</pre>";
// die();
$rsSql=$conn->query($sql);
$rsSql2=$conn->query($sql2);

$arr = array();
$arr2 = array();


while ($row = $rsSql->fetch_assoc()){
	$arr['title'][] = $row;
}


while ($row = $rsSql2->fetch_assoc()){	
	$arr2['details'][] = $row;
}

$result = array(
	'title' => $arr,
	'details' => $arr2
);



$baseRow = 4;
$values = array();


for($n = 0; count($arr2['details']) > $n; $n++){
	$values[] = $arr2['details'][$n]['salesorder_no'];
}

$pur = array_count_values($values);

foreach($result AS $data){

		for($e = 0; count($data['details']) > $e; $e++){
			$row2 = $baseRow + $e;		
			$spreadsheet->getActiveSheet()->setCellValue('I'.$row2, $data['details'][$e]['quantity']);		
			$spreadsheet->getActiveSheet()->setCellValue('J'.$row2, $data['details'][$e]['cargo_wgt']);		
			$spreadsheet->getActiveSheet()->setCellValue('K'.$row2, $data['details'][$e]['cargo_length']);		
			$spreadsheet->getActiveSheet()->setCellValue('L'.$row2, $data['details'][$e]['cargo_width']);		
			$spreadsheet->getActiveSheet()->setCellValue('M'.$row2, $data['details'][$e]['cargo_height']);		
			$spreadsheet->getActiveSheet()->setCellValue('N'.$row2, $data['details'][$e]['code']);		
		}			

		$e = 0;
		for($i = 0; count($data['title']) > $i; $i++){		
				
			$row = $baseRow + $e;		
			$key = $data['title'][$i]['salesorder_no'];

			$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);  
				$spreadsheet->getActiveSheet()->setCellValue('A' . $row, $e + 1)				
				->setCellValue('A' . $row, $data['title'][$i]['shipment_id'])
				->setCellValue('B' . $row, $data['title'][$i]['accountname'])				
				->setCellValue('C' . $row, $data['title'][$i]['sender'])
				->setCellValue('D' . $row, $data['title'][$i]['receiver'])
				->setCellValue('E' . $row, $data['title'][$i]['sum'])
				->setCellValue('F' . $row, $data['title'][$i]['load_loc'])
				->setCellValue('G' . $row, $data['title'][$i]['unload_loc'])		
				->setCellValue('H' . $row, $data['title'][$i]['order_date']);	

			$e = $e + $pur[$key];
		}
	  
}


$spreadsheet->getActiveSheet()->removeRow($baseRow - 1, 1);
$temp_file = tempnam(sys_get_temp_dir(), 'Excel');
// Save EXCEL
$writer = new Xlsx($spreadsheet);


$writer->save($temp_file);
// Save PDF
// $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf($spreadsheet);
// $writer->save($temp_file);
// ------------------ Operation with file result -------------------------------------------
$documento = file_get_contents($temp_file);
unlink($temp_file);  // delete file tmp

$date = date("Y-m-d");

// header("Content-Disposition: attachment; filename=$subject.xlsx");
header("Content-Disposition: attachment; filename=sales_detail_export_$date.xlsx");
header("Content-Type: application/excel;  charset=UTF-8");
echo $documento;

?>
