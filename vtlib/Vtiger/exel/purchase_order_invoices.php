<?php
global $_REQUEST;
require 'vendor/autoload.php';
require_once "../../../config.inc.php";

error_reporting(0);

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if(!empty($_REQUEST['search_parameters'])){
	$params = explode(":", $_REQUEST['search_parameters']);

	for($i = 0; count($params) > $i; $i++){
		$search_params[] = explode('=', $params[$i]);
	}
}

$viewname = $_REQUEST['viewname'];

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//  Template processor instance creation
$reader = IOFactory::createReader('Xlsx');

$spreadsheet = $reader->load(__DIR__ . '/template_purchase.xlsx');

$columnsQuery = "SELECT vtiger_cvcolumnlist.columnname FROM vtiger_cvcolumnlist 
																					 INNER JOIN vtiger_customview ON vtiger_customview.cvid = vtiger_cvcolumnlist.cvid 
																					 WHERE vtiger_customview.cvid = '$viewname' 
																					 ORDER BY vtiger_cvcolumnlist.columnindex";


$col = $conn->query($columnsQuery);

while ($row = $col->fetch_assoc()){
	$colTemp[] = explode(":", $row['columnname']);
}



$listPeriodQuery = "SELECT comparator FROM vtiger_cvadvfilter WHERE vtiger_cvadvfilter.cvid = '$viewname'";
$list = $conn->query($listPeriodQuery);
$list = $list->fetch_assoc();

if($list['comparator'] == 'today'){
	$listPeriod = array(date('Y-m-d'));
}elseif($list['comparator'] == 'yesterday'){
	$listPeriod = array(date('Y-m-d', strtotime('-1 days')));
}elseif($list['comparator'] == 'thismonth'){
	$first = date("Y-m-d", strtotime("first day of this month"));
	$last = date("Y-m-d", strtotime("last day of this month"));
	$listPeriod = array($first,$last);
}elseif($list['comparator'] == 'lastmonth'){
	$month_ini = new DateTime("first day of last month");
	$month_end = new DateTime("last day of last month");
	$first = $month_ini->format('Y-m-d'); 
	$last = $month_end->format('Y-m-d');
	$listPeriod = array($first,$last);
}



$defaultColumns = " purchaseorder_no,vendorname,cf_1355,subtotal,s_h_amount,total,paid,balance,duedate,cf_1345,CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user ";

$sql = "SELECT ";
	for($l = 0; $l < count($colTemp); $l++){		
		if($colTemp[$l][1] == 'smownerid'){
			$sql .= " CONCAT(vtiger_users.first_name,' ',vtiger_users.last_name) AS assigned_user_id ".(count($colTemp) > $l+1 ? ',' : '');
		}elseif($colTemp[$l][1] == 'vendorid'){
			$sql .= " vendorname AS vendor_id ".(count($colTemp) > $l+1 ? ',' : '');
		}elseif($colTemp[$l][1] == 'glacct' OR $colTemp[$l][1] == 'category'){
			$sql .= '';
		}else{
			$sql .= $colTemp[$l][0].".".($colTemp[$l][1] == $colTemp[$l][2] ? $colTemp[$l][1] : $colTemp[$l][1]." AS ".$colTemp[$l][2]).(count($colTemp) > $l+1 ? ',' : '');
		}		

	}

$sql	.= " ,$defaultColumns FROM vtiger_purchaseorder 
					LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_purchaseorder.purchaseorderid					
					LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_purchaseorder.vendorid
					LEFT JOIN vtiger_crmentity aui ON aui.crmid=vtiger_vendor.vendorid
					LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=vtiger_purchaseorder.purchaseorderid
					LEFT JOIN vtiger_users ON vtiger_users.id=vtiger_crmentity.smownerid
					LEFT JOIN vtiger_users AS vtiger_users2 ON vtiger_users2.id=aui.smownerid				
					WHERE vtiger_crmentity.deleted=0 AND vtiger_crmentity.setype = 'PurchaseOrder' ";
					if(isset($list)){
						$sql .= " AND vtiger_purchaseordercf.cf_1345 BETWEEN '$listPeriod[0]' AND '$listPeriod[1]' ";
					}

if(isset($search_params)){
	$sql .=  " HAVING ";

	for($e = 0; count($search_params) > $e; $e++){
		$column = $search_params[$e][0];
		$value = str_replace('"',"", $search_params[$e][1]);
		
		if($column == 'createdtime' OR $column == 'cf_1345' OR $column == 'duedate'){
			$period = explode(",",$value);
			$sql .=  " $column BETWEEN '$period[0] 00:00:00' AND '$period[1] 23:59:59' ".($e+1 < COUNT($search_params) ? 'AND ' :'');		
		}else{
			$sql .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
		}
	}
}

$sql .= " ORDER BY vtiger_purchaseorder.purchaseorderid DESC";

if(!isset($search_params)){
	$sql .= " LIMIT 500";
}



$rsSql=$conn->query($sql);



// echo "<pre>";
// print_R($sql);
// echo "</pre>";
// die();

$baseRow = 4;
$r=0;
while ($data2 = $rsSql->fetch_assoc()){
    $row = $baseRow + $r;
		$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);  
			$spreadsheet->getActiveSheet()->setCellValue('A' . $row, $r + 1)
					->setCellValue('A' . $row, $data2['purchaseorder_no'])
					->setCellValue('B' . $row, $data2['vendorname'])				
					->setCellValue('C' . $row, $data2['cf_1355'])
					->setCellValue('D' . $row, $data2['subtotal'])
					->setCellValue('E' . $row, $data2['s_h_amount'])
					->setCellValue('F' . $row, $data2['total'])
					->setCellValue('G' . $row, $data2['paid'])
					->setCellValue('H' . $row, $data2['balance'])
					->setCellValue('I' . $row, $data2['duedate'])
					->setCellValue('J' . $row, $data2['cf_1345'])
					->setCellValue('K' . $row, $data2['assigned_user']);		
		$r= $r+1;  
}


$spreadsheet->getActiveSheet()->removeRow($baseRow - 1, 1);
$temp_file = tempnam(sys_get_temp_dir(), 'Excel');
// Save EXCEL
$writer = new Xlsx($spreadsheet);


$writer->save($temp_file);
// Save PDF
// $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf($spreadsheet);
// $writer->save($temp_file);
// ------------------ Operation with file result -------------------------------------------
$documento = file_get_contents($temp_file);
unlink($temp_file);  // delete file tmp

$date = date("Y-m-d");

// header("Content-Disposition: attachment; filename=$subject.xlsx");
header("Content-Disposition: attachment; filename=purchase_orders_$date.xlsx");
header("Content-Type: application/excel;  charset=UTF-8");
echo $documento;


?>