<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
include_once dirname(__FILE__) . '/../viewers/HeaderViewer.php';

class Vtiger_PDF_InventoryHeaderViewer extends Vtiger_PDF_HeaderViewer {

	function totalHeight($parent) {
		$height = 100;
		
		if($this->onEveryPage) return $height;
		if($this->onFirstPage && $parent->onFirstPage()) $height;
		return 0;
	}
	
	function display($parent) {	
		$pdf = $parent->getPDF();	
		$headerFrame = $parent->getHeaderFrame();
		if($this->model) {
			$headerColumnWidth = $headerFrame->w/3.0;	
			$modelColumns = $this->model->get('columns');
			
			// Column 1
			$offsetX = 5;
			
			$offsetY = 40;
			$modelColumn1 = $modelColumns[1]['customer_information'];
			$modelColumn12 = $modelColumns[1]['customer_information2'];

			$customer = $pdf->GetY()+$offsetY;

			if($pdf->getPage() <= 1){
				if(!empty($modelColumn1)) {
					if($modelColumns[1]['crm_entity']['lang'] == 'lt_lt'){
						$lang_space = 25;
						$pdf->SetFont('freeserif', 'B');
					}else{
						$lang_space = 40;
						$pdf->SetFont('freeserif', 'B',8);
					}

					$pdf->SetFont('freeserif', 'B');
					$pdf->MultiCell($headerColumnWidth+20, 7, $modelColumn12, 0, 'L', 0, 1, $headerFrame->x, 	$customer);					
					$pdf->SetFont('freeserif', '');
					$pdf->MultiCell($headerColumnWidth+10, 7, $modelColumn1, 0, 'L', 0, 1, $headerFrame->x+$lang_space, 	$customer);
					$offsetY = 2;
				}	
				if($modelColumns[1]['crm_entity']['lang'] != 'lt_lt'){	
					$pdf->SetFont('freeserif', 'B',9);
				}
			}


			
			// Column 2
			$modelColumn0 = $modelColumns[0];
			$offsetY = 0;
			list($imageWidth, $imageHeight, $imageType, $imageAttr) = $parent->getimagesize(
					$modelColumn0['logo']);
			//division because of mm to px conversion
			$w = $imageWidth/3;
			if($w > 60) {
				$w=60;
			}
			$h = $imageHeight/3;
			$h = 8;
			if($h > 30) {
				$h = 30;
			}

			$top_logo = 'test/logo/LogoBlueRGB_LT_PNG.png';		
			$top_logo2 = 'test/logo/stronger-without-bg.png';		

			// rekvizitai logo vietos deklaravimas

			$pdf->Image($top_logo, $headerFrame->x+5, $headerFrame->y-5, 35, 35);
			$pdf->Image($top_logo2, $headerFrame->x+32, $headerFrame->y-4, 33, 33);

	
			// Parnaso logo vietos deklaravimas
			$pdf->Image($modelColumn0['logo'], $headerFrame->x+70, $headerFrame->y, $w, $h);
			$imageHeightInMM = 15;

			$currentPage = $pdf->getPage();
			$pagegroup = $pdf->getaliasnbpages(); 


			$page = getTranslatedString('LBL_PAGE','Invoice',$modelColumns[1]['crm_entity']['lang']);		

			
			$pdf->SetFont('freeserif', '');
			$pdf->MultiCell($headerColumnWidth+20, 7, "$page $currentPage/$pagegroup", 0, 'L', 0, 1, $headerFrame->x+170, 	$customer-40);

			// ITOMA

			$date = $modelColumns[1]['crm_entity']['invoicedate'];
			
			
			$title = getTranslatedString('LBL_INVOICE_TITLE','Invoice',$modelColumns[1]['crm_entity']['lang']);
			$title_pre = getTranslatedString('LBL_PRE_INVOICE_TITLE','Invoice',$modelColumns[1]['crm_entity']['lang']);
			$title_credit = getTranslatedString('LBL_CREDIT_INVOICE_TITLE','Invoice',$modelColumns[1]['crm_entity']['lang']);
			$no = getTranslatedString('LBL_NO','Invoice',$modelColumns[1]['crm_entity']['lang']);
			$created_date = getTranslatedString('LBL_INVOICE_DATE','Invoice',$modelColumns[1]['crm_entity']['lang']).": ".$date;

			if($modelColumns[1]['crm_entity']['lang'] == 'en_us'){
				if($modelColumns[1]['crm_entity']['invoice_type'] == 'Debetinė'){
					$title_width = $headerColumnWidth-$offsetX+10;
					$title_move = $headerFrame->x+$headerColumnWidth+$offsetX-5;
				}else{
					$title_width = $headerColumnWidth-$offsetX+45;
					$title_move = $headerFrame->x+$headerColumnWidth+$offsetX-15;
				}

				$date_width = 5; 
			}else{	
				if($modelColumns[1]['crm_entity']['invoice_type'] == 'Debetinė'){
					$title_width = $headerColumnWidth-$offsetX;
					$title_move = $headerFrame->x+$headerColumnWidth+$offsetX;
				}elseif($modelColumns[1]['crm_entity']['invoice_type'] == 'I&scaron;ankstinė'){
					$title_width = $headerColumnWidth-$offsetX+15;
					$title_move = $headerFrame->x+$headerColumnWidth+$offsetX-3;
				}else{
					$title_width = $headerColumnWidth-$offsetX+10;
					$title_move = $headerFrame->x+$headerColumnWidth+$offsetX;
				}	
				$date_width = '';			
			}

			$offsetX = 10;
			$pdf->SetY($headerFrame->y);
			$offsetY = 15;
			$pdf->SetFont('freeserif', 'B');
			if($modelColumns[1]['crm_entity']['invoice_type'] == 'Debetinė'){
				$pdf->MultiCell($title_width, $contentHeight, $title, 0, 'C', 0, 1,	$title_move, $pdf->GetY()+$offsetY);
			}elseif($modelColumns[1]['crm_entity']['invoice_type'] == 'I&scaron;ankstinė'){
				$pdf->MultiCell($title_width, $contentHeight, $title_pre, 0, 'C', 0, 1,	$title_move, $pdf->GetY()+$offsetY);
			}else{
				$pdf->MultiCell($title_width, $contentHeight, $title_credit, 0, 'C', 0, 1,	$title_move, $pdf->GetY()+$offsetY);
			}
			$pdf->MultiCell($headerColumnWidth-$offsetX, $contentHeight, $no.''.$this->model->get('title'), 0, 'C', 0, 1,	$headerFrame->x+$headerColumnWidth+$offsetX, 30);
			$pdf->MultiCell($headerColumnWidth-$offsetX+$date_width, $contentHeight, $created_date, 0, 'C', 0, 1,	$headerFrame->x+$headerColumnWidth+$offsetX, 35);
			

			// Column 3
			$offsetX = 10;
			$offsetY = 9;

			$modelColumn2 = $modelColumns[2];
			
			$contentWidth = $pdf->GetStringWidth($this->model->get('title'));
			$contentHeight = $pdf->GetStringHeight($this->model->get('title'), $contentWidth);
			
			$roundedRectX = $headerFrame->w+$headerFrame->x-$contentWidth*2.0;
			$roundedRectW = $contentWidth*2.0;
			
		// Paslaugu tiekejas
			$contentX = $roundedRectX + (($roundedRectW - $contentWidth)/2.0);
		
			$supplier = $pdf->GetY()+6;

			if($modelColumns[1]['crm_entity']['lang'] == 'en_us'){
				$pdf->SetFont('freeserif', 'B',8);
				$service_provider_left = $contentX-$contentWidth-40;
				$service_provider_width = $headerColumnWidth-10;
				$supplier = $supplier-3;
				$suplier_width = '';
			}else{
				$pdf->SetFont('freeserif', 'B');
				$service_provider_left = $contentX-$contentWidth-25;
				$suplier_width = 15;
			}
				
			
			$contentHeight = $pdf->GetStringHeight( $modelColumn0['content'], $headerColumnWidth);
			if($pdf->getPage() <= 1){
			// Paslaugu tiekejo info
			$pdf->MultiCell($service_provider_width, $contentHeight-10, $modelColumn0['title'], 0, 'L', 0, 1, $service_provider_left, 	$supplier);
			
			if($modelColumns[1]['crm_entity']['lang'] == 'en_us'){
				$pdf->SetFont('freeserif', 'B',7);
			}else{
				$pdf->SetFont('freeserif', 'B',9);
			}
			$pdf->MultiCell($headerColumnWidth+$suplier_width, $contentHeight-10, $modelColumn0['content'], 0, 'L', 0, 1, $contentX-$contentWidth+3, $supplier);
			}
			$pdf->setFont('freeserif', '');

			// Add the border cell at the end
			// This is required to reset Y position for next write
			// $pdf->MultiCell($headerFrame->w, $headerFrame->h-$headerFrame->y, "", 0, 'L', 0, 1, $headerFrame->x, $headerFrame->y-10);
		}	
		
	}
	
}
