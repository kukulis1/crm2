<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

include_once dirname(__FILE__) . '/../viewers/ContentViewer.php';

class Vtiger_PDF_InventoryContentViewer extends Vtiger_PDF_ContentViewer {

	protected $headerRowHeight = 8;
	protected $onSummaryPage   = false;

	
	function initDisplay($parent) {

		$models = $this->contentModels;
		$totalModels = count($models);
		$model = $models[0];		
		$invoice_lang = $model->get("lang");

		$address_width = 48;
		$quantity_width = 18;

		if($model->get("client") == 9628){
			$address_width = 45;
			$quantity_width = 23;
		}

		if($model->get("PrintTemplate") == "PVM sąskaita-faktūra"){
			$this->celles = array( // Name => Width			
				'Order' =>  101,
				'Measure' => 25,
				'Quantity' => 15,
				'Sum' => 25,
				'Total' => 25
			);
		}else if($model->get("EntityType") == "Item"){
				$this->celles = array(
					'Date' => 25,
					'Order' => 101,
					'Quantity' => 15,
					'Sum' => 25,
					'Total' => 25
				);
		}else	if($model->get("PrintTemplate") == "Perkraustymas"){
			$this->celles = array( // Name => Width
				'Date'    => 13,
				'Order'    => 21,
				'Loading'    => 66,
				'Un-Load'=> 66,
				'Total'   => 25
			);
		}else{
			$this->celles = array( // Name => Width
				'Date'    =>  ($invoice_lang == 'lt_lt' ? 13 : 17),
				'Order'    =>  ($invoice_lang == 'lt_lt' ? 20 : 24),
				'Loading'    => ($invoice_lang == 'lt_lt' ? $address_width : 44),
				'Un-Load'=> ($invoice_lang == 'lt_lt' ? $address_width : 44),
				'Quantity'   => ($invoice_lang == 'lt_lt' ? $quantity_width : 13),
				'Kg'=> 10,
				'M3'     => 10,
				'Total'   => ($invoice_lang == 'lt_lt' ? 25 : 27)
			);

		}


			$this->service_cells = array(
				'Service' => 166,
				'Total' => 25
			);

		$pdf = $parent->getPDF();
		$contentFrame = $parent->getContentFrame();

		

		// $pdf->MultiCell($contentFrame->w, $contentFrame->h, "", 1, 'L', 0, 1, $contentFrame->x, $contentFrame->y);
		
		// Defer drawing the cell border later.
		// if(!$parent->onLastPage()) {
		// 	$this->displayWatermark($parent);
		// }
		
		// Header	

		

		$item_lang =	getTranslatedString('LBL_ITEM_SERVICE','Invoice',$invoice_lang);
		$order_lang =	getTranslatedString('LBL_ORDER','Invoice',$invoice_lang);
		$date_lang =	getTranslatedString('LBL_DATE','Invoice',$invoice_lang);
		$loading_lang =	getTranslatedString('LBL_LOADING','Invoice',$invoice_lang);
		$unload_lang =	getTranslatedString('LBL_UNLOAD','Invoice',$invoice_lang);
		$total_lang =	getTranslatedString('LBL_TOTAL','Invoice',$invoice_lang);
		$total_sum_lang =	getTranslatedString('LBL_TOTAL_SUM','Invoice',$invoice_lang);
		$measure_lang =	getTranslatedString('LBL_MEASURE','Invoice',$invoice_lang);
		$qty_lang =	getTranslatedString('LBL_QTY','Invoice',$invoice_lang);
		$measure2_lang =	getTranslatedString('LBL_MEASURE2','Invoice',$invoice_lang);
		$pll =	'Pll vietu';
		$weight = getTranslatedString('LBL_WEIGHT','Invoice',$invoice_lang);
		$volume = getTranslatedString('LBL_VOLUME','Invoice',$invoice_lang);

		$measure_title = $measure2_lang;

		if($model->get("client") == 9628){
			$measure_title = $measure2_lang.'/'.$pll;
		}


		if($model->get("PrintTemplate") == "PVM sąskaita-faktūra"){
			$cellLab = array(				
				'Order' => $item_lang,
				'Measure' => $measure_lang ,
				'Quantity' => $qty_lang,
				'Sum' => $total_lang,		
				'Total' => $total_sum_lang		
			);

		}else if($model->get("EntityType") == "Item"){
			$cellLab = array(
				'Date' => $date_lang,
				'Order' => $item_lang,
				'Quantity' => $qty_lang,
				'Sum' => $total_lang,		
				'Total' => $total_sum_lang		
			);
		}else if($model->get("PrintTemplate") == "Perkraustymas"){
			$cellLab = array(
				'Date' => $date_lang,
				'Order' => $order_lang,
				'Loading' => $loading_lang,
				'Un-Load' => $unload_lang,			
				'Total' => $total_lang		
			);

		}else{
			$cellLab = array(
				'Date' => $date_lang,
				'Order' => $order_lang,
				'Loading' => $loading_lang,
				'Un-Load' => $unload_lang,
				'Quantity' => $measure_title,
				'Kg'=> $weight,
				'M3' => $volume,
				'Total' => $total_lang		
			);
		}
	

		$offsetX = 0;


		if($invoice_lang == 'lt_lt'){
			$pdf->SetFont('','B',10);
		}else{
			$pdf->SetFont('','B',8);
		}

		foreach($this->celles as $cellName => $cellWidth) {
			foreach($cellLab as $cellLabelName => $cellLabel){	
				if($cellLabelName == $cellName){					
						$pdf->MultiCell($cellWidth, 0, $cellLabel, 'TB', 'T', 0, 1, $contentFrame->x+$offsetX, $contentFrame->y);										
						$offsetX += $cellWidth;									
				}	
			}			
		}
	
		$bottom_logo = 'test/logo/bottom-pdf.jpg';
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->Image($bottom_logo, 0, 281, 210, 16);
	
	
		$pdf->SetFont('','');
		// Reset the y to use
		$contentFrame->y += $this->headerRowHeight;	
		
	}
	
	function drawCellBorder($parent, $cellHeights=False) {		
		$pdf = $parent->getPDF();
		$contentFrame = $parent->getContentFrame();
		
		if(empty($cellHeights)) $cellHeights = array();

		$offsetX = 0;
		foreach($this->celles as $cellName => $cellWidth) {
			$cellHeight = isset($cellHeights[$cellName])? $cellHeights[$cellName] : $contentFrame->h;
			$offsetY = $contentFrame->y-$this->headerRowHeight;		
			$pdf->MultiCell($cellWidth, $cellHeight, '', '', '', 0, 1, $contentFrame->x+$offsetX, $contentFrame->y);
			$offsetX += $cellWidth;
		}
	}




	function display($parent) {
		$this->displayPreLastPage($parent);
		$this->displayLastPage($parent);
	}



	function displayPreLastPage($parent) {

		$models = $this->contentModels;
		$totalModels = count($models);
		$pdf = $parent->getPDF(); 

		$parent->createPage();
		$contentFrame = $parent->getContentFrame();
		$contentLineX = $contentFrame->x; $contentLineY = $contentFrame->y;
		$overflowOffsetH = 8; // This is offset used to detect overflow to next page
		for ($index = 0; $index < $totalModels; ++$index) {
			$model = $models[$index];

			$contentHeight = 1;
			// Determine the content height to use
			foreach($this->celles as $cellName => $cellWidth) {		
				$contentString = $model->get($cellName);
				if(empty($contentString)) continue;
				$contentStringHeight = $pdf->GetStringHeight($contentString, $cellWidth);
				if ($contentStringHeight > $contentHeight) $contentHeight = $contentStringHeight;			
			}
			$invoice_lang = $model->get("lang");
			$continued_on_next_page =	getTranslatedString('LBL_CONTINUED_ON_NEXT_PAGE','Invoice',$invoice_lang);
		
			// Are we overshooting the height?
			if(ceil($contentLineY + $contentHeight) > ceil($contentFrame->h+$contentFrame->y) - ($totalModels > 12 ? 10 : 5)  ) {
				$pdf->MultiCell(0, 0, $continued_on_next_page, 0, 'R', 0, 1, $contentFrame->x, 	$contentLineY);			
				$parent->createPage();
				$contentFrame = $parent->getContentFrame();
				$contentLineX = $contentFrame->x; 
				$contentLineY = $contentFrame->y;
			}elseif($pdf->GetY() > $parent->getTotalHeight()-30 AND $totalModels >= 10 AND $totalModels <= 12 ){				
			   $pdf->MultiCell(0, 0, $continued_on_next_page, 0, 'R', 0, 1, $contentFrame->x, 	$contentLineY);	
				 $parent->createPage();		
				 $contentFrame = $parent->getContentFrame();
				 $contentLineX = $contentFrame->x; 
				 $contentLineY = $contentFrame->y;		
			}
	
			$offsetX = 0;
			// Info
			// if($model->get("order_type") == 'Transporto užsakymas' AND $index == 0){
			// 	$pdf->SetFont('','B');
			// 	$pdf->MultiCell(0, 0,  $model->get("order_type").":", '0', '', 0, 1, $contentFrame->x, $contentFrame->y-15);			
			// }	

		$align = 'L';
		
			if($model->get("EntityType") == "Services"){	
				foreach($this->service_cells as $cellName => $cellWidth) {				
					$title = '';		
					if($cellName == 'Total') $align = 'R';				
					if($cellName == 'Service') $title = 'Paslauga: ';				
						$pdf->SetFont('','');										
						$pdf->MultiCell(0,  $contentHeight,$model->get($cellName), 0, $align, 0, 1, $contentLineX+$offsetX, $contentLineY);									
					if(empty($model->get("Note"))) $up = $contentLineY-5 ; else $up = $contentLineY;				
					$pdf->MultiCell($contentFrame->w, $contentHeight+5, "", 'B', '', 0, 1, $contentFrame->x, $up);
				}	
				if(!empty($model->get("Note"))){
					$pdf->MultiCell(190, 5, $model->get("Note"), 0, 'L', 0, 1, $contentFrame->x, $contentLineY+5);		
				}	
			}else{
				foreach($this->celles as $cellName => $cellWidth) {	
					if($cellName == 'Total') $align = 'R';						
					if($cellName == 'Quantity') $align = 'C';						
					$pdf->SetFont('','');					
					$pdf->MultiCell($cellWidth, $contentHeight, $model->get($cellName), 0, $align, 0, 1, $contentLineX+$offsetX, $contentLineY);			
					$offsetX += $cellWidth;	
					if(empty($model->get("Note"))) $up = $contentLineY-5 ; else $up = $contentLineY;	
					if($model->get("PrintTemplate") != "PVM sąskaita-faktūra"){
						$pdf->MultiCell($contentFrame->w, $contentHeight+5, "", 'B', '', 0, 1, $contentFrame->x, $up);	
					}				
				}
				if($model->get('CargoComments')){
					if( !empty($model->get("Note")) ){
                        if($contentHeight < 11){
                            $up = $contentLineY+10;
                        } elseif($contentHeight < 15){
                            $up = $contentLineY+14;
                        } else{
                            $up = $contentLineY+$contentHeight-3;
                        }
						if($model->get("EntityType") == "Item")	$up = $up-5;	
						$pdf->MultiCell(190, 10, $model->get("Note"), 0, 'L', 0, 1, $contentFrame->x, $up);		
					}
				}	
		 	}	

		 $pdf->SetFont('','',10);

			// if($pdf->GetY() > $parent->getTotalHeight()-50 AND $totalModels >= 10 AND $totalModels <= 12 ){			
			// 	$pdf->AddPage();					
			// 	$contentLineY= $pdf->GetY();			
			// }else{			
				$contentLineY = $pdf->GetY();		
			// }

			// if($totalModels == 10 AND $pdf->GetY() > $parent->getTotalHeight()-30){			
			// 	$pdf->AddPage();								
			// 	$contentLineY= $pdf->GetY();	
			// }elseif($totalModels >= 11 AND $pdf->GetY() > $parent->getTotalHeight()-20){				
			// 	$pdf->AddPage();								
			// 	$contentLineY= $pdf->GetY();
			// // }elseif($totalModels >= 12 AND $pdf->GetY() > $parent->getTotalHeight()-10){				
			// // 	$pdf->AddPage();								
			// // 	$contentLineY= $pdf->GetY();	
			// }else{			
			// 	$contentLineY = $pdf->GetY();		
			// }
		}

				

		$contentFrame->y = $contentFrame->h;
			
		// Summary
		$cellHeights = array();
		
		if ($this->contentSummaryModel) { 
			$summaryCellKeys = $this->contentSummaryModel->keys(); 
			$summaryCellCount = count($summaryCellKeys);

			
			$summaryCellLabelWidth = $this->cells['Quantity'] + $this->cells['Price'] + $this->cells['Discount'] + $this->cells['Tax'];
			$summaryCellHeight = $pdf->GetStringHeight("TEST", $summaryCellLabelWidth); // Pre-calculate cell height

	
			$summaryTotalHeight = ceil(($summaryCellHeight * $summaryCellCount));
			$summaryLineX = $contentLineX + $this->cells['Code'] + $this->cells['Name'];		
			$summaryLineY = ($contentFrame->h+$contentFrame->y-$this->headerRowHeight)-$summaryTotalHeight;
			$summaryLineY =  $pdf->GetY();

			// if (($contentFrame->h+$contentFrame->y) - ($contentLineY+$overflowOffsetH)  < $summaryTotalHeight) { //$overflowOffsetH is added so that last Line Item is not overlapping
			// 	$this->drawCellBorder($parent);
			// 	// $parent->createPage();
			// 	$pdf->addPage();
									
			// 	$contentFrame = $parent->getContentFrame();
			// 	$contentLineX = $contentFrame->x; 
			// 	$contentLineY = $contentFrame->y;
			// }
			
			// $pdf->MultiCell(0, 0, 'TESTAS', '0', '', 0, 1, $contentFrame->x, 266);	



			$summaryCells = array(
				'allWithOutPvm' => $summaryCellKeys[0],
				'ps' => $summaryCellKeys[1],
				'allWithPVM' => $summaryCellKeys[2],
				'allToWords' => $summaryCellKeys[3],
				'payBefore' => $summaryCellKeys[4],
				'invoiceWrite' => $summaryCellKeys[5]
			);
			
			$summaryCelles = array( // Name => Width
					'allWithOutPvm' =>	array(190,'R',50, $summaryLineX, 'R','B',1),
					'ps' =>	array(190,'L',50, $summaryLineX, 'R','',1),				
					'allWithPVM' =>	array(190,'R',50, $summaryLineX, 'R','B',1),
					'allToWords' =>	array(($invoice_lang == 'lt_lt' ? 105 : 140),'L',($invoice_lang == 'lt_lt' ? 135 : 100), $summaryLineX-($invoice_lang == 'lt_lt' ? 85 : 50) ,'B','B',1),
					'payBefore' =>	array(190,'R',50, $summaryLineX ,'C','B',1),
					'invoiceWrite' =>	array(($invoice_lang == 'lt_lt' ? 78 : 105),'L',($invoice_lang == 'lt_lt' ? 162 : 135), $summaryLineX-($invoice_lang == 'lt_lt' ? 112 : 85) ,'L','B',1)
			);

				
			if($summaryLineY > 230){
				$pdf->MultiCell(0, 0, $continued_on_next_page, 0, 'R', 0, 1, $contentFrame->x, 	$contentLineY);	
				$parent->createPage();
				$summaryLineY = $pdf->GetY();	
			}
	
			foreach($summaryCells as $key => $summaryLabel) {					
				foreach($summaryCelles as $cells => $cellesWidth) {
					if($key == $cells){

						$psHeight = 0;
						if(strlen($summaryLabel) >= 80){
							if($cells == 'ps') $psHeight = 10; $summaryLineY = $summaryLineY;
						}
						if($cells == 'ps'){
							$cellesWidth = array(50, 'L',50, $summaryLineX, 'R','','L');
							$pdf->SetFont('',$cellesWidth[5]);
							if($summaryCellKeys[6]){  
								$pdf->SetFont('','B');
							}							
							$pdf->MultiCell($cellesWidth[0], $psHeight, $summaryCellKeys[6], '', $cellesWidth[1], 0, 1, $cellesWidth[3]+($invoice_lang == 'lt_lt' ? 23 : 5), $summaryLineY);
						}

	
						if($cells == 'payBefore') $summaryLineY = $summaryLineY+6;
						if($cells == 'invoiceWrite') $summaryLineY = $summaryLineY+6;			
						if($cells == 'allWithOutPvm') $summaryLineY = $summaryLineY+6;					
						
						if($cells == 'ps'){
							$pdf->SetFont('',$cellesWidth[5],8);
							$pdf->MultiCell($cellesWidth[0]-50, $psHeight, $summaryLabel, $cellesWidth[6], $cellesWidth[1], 0,1, $contentFrame->x, $summaryLineY);
							$pdf->SetFont('',$cellesWidth[5],10);
						}else{
							if($cells == 'allWithPVM') $pdf->SetFont('','B');
							$pdf->MultiCell($cellesWidth[0]-50, 0, $summaryLabel, $cellesWidth[6], $cellesWidth[1], 0,1, $contentFrame->x, $summaryLineY);
						}
						$pdf->SetFont('','');

						if($summaryLineY > 230 && $cells == 'payBefore'){
			
							$parent->createPage();
							$summaryLineY = $pdf->GetY();	
						}
						if($cells == 'ps'){
							$pdf->MultiCell($cellesWidth[2], $psHeight, $this->contentSummaryModel->get($summaryLabel), 1, $cellesWidth[4], 0, 1,  $cellesWidth[3]+45, $summaryLineY);
						}else{
							if($key == 'allWithPVM') $pdf->SetFont('','B');
							$pdf->MultiCell($cellesWidth[2], 0, $this->contentSummaryModel->get($summaryLabel), 1, $cellesWidth[4], 0, 1,  $cellesWidth[3]+45, $summaryLineY);
						}
						if($pdf->GetY() < $parent->getTotalHeight()-10){						
							$summaryLineY = $pdf->GetY();																		
						}	
					}
				}	
			}
			// $pdf->MultiCell($contentFrame->w, $contentHeight, sprintf('-%s-', $pdf->getPage()), 0, 'L', 0, 1,$contentFrame->x+$contentFrame->w/2.0, $parent->getTotalHeight()-1);	


	
			// $summaryLineY = $this->headerRowHeight+$summaryLineY; 
			// $pdf->MultiCell($offsetX-20 , $summaryCellHeight+40, '', 1, 1, 0, 1, $contentLineX+10, $summaryLineY);		
			// $summaryLineY = $pdf->GetY()-46;	
			// foreach($summaryCells2 as $key => $summaryLabel) {	
			// 	foreach($summaryCelles2 as $cells => $cellesWidth) {
			// 		if($key == $cells ){						
			// 			$pdf->SetFont('',$cellesWidth[2],$cellesWidth[3]);
			// 			if($key != 'logo')
			// 			$pdf->MultiCell($cellesWidth[0], $cellesWidth[1], $summaryLabel, 0, 'C', 0,1,$cellesWidth[4], $summaryLineY);		
			// 			if($key == 'logo'){
			// 			$pdf->Image($summaryLabel, $headerFrame->x+80,$summaryLineY+7, 50, 35);		
			// 			$pdf->SetFont('','');
			// 			if($totalModels == 10 OR $totalModels == 11 OR $totalModels == 12 OR $pdf->getPage() > 2)
			// 			$pdf->MultiCell($contentFrame->w, $contentHeight, sprintf('-%s-', $pdf->getPage()), 0, 'L', 0, 1,$contentFrame->x+$contentFrame->w/2.0, $parent->getTotalHeight()-1);	
			// 			}
			// 			$summaryLineY = $pdf->GetY();				
			// 		}
			// 	}
			// }
		
		}
		$this->onSummaryPage = true;
		// $this->drawCellBorder($parent, $cellHeights);

	
	}

	function displayLastPage($parent) {
		 $pdf = $parent->getPDF();
		 $pos = $pdf->getY();
		// Add last page to take care of footer display
		if($parent->createLastPage($pos)) {
				$this->onSummaryPage = false;
		}
	}
}
