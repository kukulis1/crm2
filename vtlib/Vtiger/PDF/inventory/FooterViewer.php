<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
include_once dirname(__FILE__) . '/../viewers/FooterViewer.php';

class Vtiger_PDF_InventoryFooterViewer extends Vtiger_PDF_FooterViewer {

	static $DESCRIPTION_DATA_KEY = '__DES__DATA__';
	static $TERMSANDCONDITION_DATA_KEY = '__TANDC__DATA__';
	static $DESCRIPTION_LABEL_KEY = '__DES_LABEL__';
	static $DESCRIPTION_HEADER_KEY = '__DES_HEADER__';
	static $DESCRIPTION_FIRST_KEY = '__DES_FIRST__';
	static $DESCRIPTION_SECOND_KEY = '__DES_SECOND__';
	static $DESCRIPTION_THIRD_KEY = '__DES_THIRD__';
	static $DESCRIPTION_FOURT_KEY = '__DES_FOURT__';
	static $DESCRIPTION_IMAGE_KEY = '__DES_IMAGE__';
	static $TERMSANDCONDITION_LABEL_KEY = '__TANDC_LABEL__';

	function totalHeight($parent) {
		if($this->model && $this->onEveryPage()) {
			$pdf = $parent->getPDF();

			$footerTitleHeight = 8.0;
			
			$termsConditionText = $this->model->get(self::$TERMSANDCONDITION_DATA_KEY);
			$termsConditionHeight = $pdf->GetStringHeight($termsConditionText, $parent->getTotalWidth());

			if($termsConditionHeight) $termsConditionHeight += $footerTitleHeight;

			$descriptionText = $this->model->get(self::$DESCRIPTION_DATA_KEY);
			$descriptionHeight = $pdf->GetStringHeight($descriptionText, $parent->getTotalWidth());

			if($descriptionHeight) $descriptionHeight += $footerTitleHeight;

			return $termsConditionHeight + $descriptionHeight;
		}
		return parent::totalHeight($parent);
	}

	function display($parent) {

		$pdf = $parent->getPDF();
		$footerFrame = $parent->getFooterFrame();
		$pdf->SetAutoPageBreak(TRUE, 0);
		if($this->model) {
			$targetFooterHeight = ($this->onEveryPage())? $footerFrame->h : 0;

			$header = $this->labelModel->get(self::$DESCRIPTION_HEADER_KEY);
			$first = $this->labelModel->get(self::$DESCRIPTION_FIRST_KEY);
			$second = $this->labelModel->get(self::$DESCRIPTION_SECOND_KEY);
			$third = $this->labelModel->get(self::$DESCRIPTION_THIRD_KEY);
			$fourt = $this->labelModel->get(self::$DESCRIPTION_FOURT_KEY);
			$image = $this->labelModel->get(self::$DESCRIPTION_IMAGE_KEY);

			
			$descCells = array(
				'mess1' => $header,
				'mess2' => $first,
				'mess3' => $second,
				'mess4' => $third,
				'mess5' => $fourt,
				'logo' => $image
			);
			
			$descCelles = array( // Name => Width
				'mess1' =>	array(170,10,'B',20,$footerFrame->x+11),
				'mess2' =>	array(200,5,'B',10, $footerFrame+5),
				'mess3' =>	array(200,5,'B',10, $footerFrame+12),
				'mess4' =>	array(200,5,'B',10, $footerFrame+5),
				'mess5' =>	array(200,5,'B',10, $footerFrame+5),
				'logo' =>	array(200,5,'B',10, $footerFrame)
			);	

			$bottom_logo = 'test/logo/bottom-pdf.jpg';

			$summaryLineY = $pdf->GetY()+2;	
			foreach($descCells as $key => $descLabel) {	
				foreach($descCelles as $cells => $cellesWidth) {
					if($key == $cells ){						
						$pdf->SetFont('',$cellesWidth[2],$cellesWidth[3]);
						if($key != 'logo')
							$pdf->MultiCell($cellesWidth[0], $cellesWidth[1], $descLabel, 0, 'C', 0,1,$cellesWidth[4], $summaryLineY-2);		
						if($key == 'logo'){
							$pdf->Image($descLabel, $footerFrame->x+70.5,$summaryLineY, 50, 35);						
						}
						$summaryLineY = $pdf->GetY();				
					}
				}
			}		
			$pdf->Image($bottom_logo, 0, 281, 210, 16);
			
			if($pdf->getPage() > 1){
				$modelColumns = $this->model->get('columns');
				$currentPage = $pdf->getPage();
				$pagegroup = $pdf->getaliasnbpages(); 
				$page = getTranslatedString('LBL_PAGE','Invoice',$modelColumns[1]['crm_entity']['lang']);
				$pdf->SetFont('freeserif', '');
				if($currentPage == $pagegroup){
					$pdf->MultiCell(50, 7, "$page $currentPage/$pagegroup", 0, 'L', 0, 1, $footerFrame->x+170, 	10);
				}
			}
			
			// $descriptionString = $this->labelModel->get(self::$DESCRIPTION_LABEL_KEY);
			// $description = $this->model->get(self::$DESCRIPTION_DATA_KEY);
			// $descriptionHeight = $pdf->GetStringHeight($descriptionString, $footerFrame->w);
			// $pdf->SetFillColor(205,201,201);
			// $pdf->MultiCell($footerFrame->w, $descriptionHeight, $descriptionString, 1, 'L', 1, 1, $footerFrame->x, $footerFrame->y);
			// $pdf->MultiCell($footerFrame->w, $targetFooterHeight - $descriptionHeight, $description, 1, 'L',
			// 	0, 1, $footerFrame->x, $footerFrame->y + $descriptionHeight);
				
			// $termsAndConditionLabelString = $this->labelModel->get(self::$TERMSANDCONDITION_LABEL_KEY);
			// $termsAndCondition = $this->model->get(self::$TERMSANDCONDITION_DATA_KEY);
			// $offsetY = 2.0;
			// $termsAndConditionHeight = $pdf->GetStringHeight($termsAndConditionLabelString, $footerFrame->w);
			// $pdf->SetFillColor(205,201,201);
			// $pdf->MultiCell($footerFrame->w, $termsAndConditionHeight, $termsAndConditionLabelString, 1, 'L', 1, 1,
			// 	$pdf->GetX(), $pdf->GetY() + $offsetY);
			// $pdf->MultiCell($footerFrame->w, $targetFooterHeight - $termsAndConditionHeight, $termsAndCondition,1, 'L', 0, 1,
			// 	$pdf->GetX(), $pdf->GetY());
		}
	}
}
?>