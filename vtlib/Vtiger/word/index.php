<?php
  require_once "../../../config.inc.php";

  global $root_directory;

  require_once '../vendor/autoload.php';
  require_once  '../exel/vendor/autoload.php';


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  } 

  use PhpOffice\PhpSpreadsheet\IOFactory;
  
  $leadid = $_GET['lead'];
  $date = date("Y-m-d");

  $type = mysqli_fetch_assoc($conn->query("SELECT rating FROM crm.vtiger_leaddetails WHERE leadid = $leadid"));

  $query = mysqli_fetch_assoc($conn->query("SELECT CONCAT(l.firstname,' ',l.lastname) AS customer_name,cf_1801 as company_address, company, CONCAT(cf_1797,', ',cf_1799) AS company_code,phone, cf_1858 as load_company, cf_1860 as unload_company, cf_1793 as load_address, cf_1795 as unload_address, GROUP_CONCAT(m.code) as measure, SUM(quantity) AS quantity, GROUP_CONCAT(cargo_length,'x',cargo_width,'x',cargo_height SEPARATOR '/') AS dimensions,
                              SUM(cargo_wgt) AS cargo_wgt, SUM((cargo_length * cargo_width * cargo_height) * quantity) as volume, cf_1862 as note, 
                              CONCAT(u.first_name,' ',u.last_name) AS manager_name,rolename, total, cf_1797 as legal_code,cf_1799 as vat_code,email ,
                              u.first_name,u.last_name,l.firstname,l.lastname
                                  FROM vtiger_leaddetails l
                                  LEFT JOIN vtiger_leadscf d on d.leadid=l.leadid
                                  LEFT JOIN vtiger_leadaddress p ON p.leadaddressid=l.leadid
                                  LEFT JOIN vtiger_leads_cargo c ON c.id=l.leadid
                                  LEFT JOIN app_measures m ON m.id=c.measure
                                  LEFT JOIN vtiger_crmentity e ON e.crmid=l.leadid
                                  LEFT JOIN vtiger_users u ON u.id=e.smownerid
                                  LEFT JOIN vtiger_user2role ur ON ur.userid=u.id
                                  LEFT JOIN vtiger_role r ON r.roleid=ur.roleid
                                  WHERE l.leadid = $leadid
                                  GROUP BY l.leadid"));




if($type['rating'] == 'Perkraustymo užsakymas'){
  movement_invoice($conn,$query,$date,$leadid);
}else if($type['rating'] == 'Transporto užsakymas'){
  transport_invoice($conn,$query,$date,$leadid);
}

function movement_invoice($conn,$query,$date,$leadid){

  $months = array('January' => 'sausio','February' => 'vasario','March' => 'kovo','April' => 'balandžio','May' => 'Gegužės','June' => 'birželio','July' => 'liepos','August' => 'Rugpjūčio','September' => 'rugsėjo','October' => 'spalio','November' => 'lapkričio','December' => 'Gruodžio');

  $reader = IOFactory::createReader('Xls');
  $spreadsheet = $reader->load('template.xls');
  $spreadsheet->getActiveSheet()->setCellValue('E3', date("Y"));
  $spreadsheet->getActiveSheet()->setCellValue('F4', $months[date("F")]);
  $spreadsheet->getActiveSheet()->setCellValue('H4', "d. ".date("d"));
  $spreadsheet->getActiveSheet()->setCellValue('F7', $query["company"]);
  $spreadsheet->getActiveSheet()->setCellValue('F8', $query["company_address"]);
  $spreadsheet->getActiveSheet()->setCellValue('F10', $query['legal_code']);
  $spreadsheet->getActiveSheet()->setCellValue('F11', $query['vat_code']);
  $spreadsheet->getActiveSheet()->setCellValue('F12', $query['phone']);
  $spreadsheet->getActiveSheet()->setCellValue('F13', $query['email']);
  $spreadsheet->getActiveSheet()->setCellValue('B18', '');
  $spreadsheet->getActiveSheet()->setCellValue('B19', '');
  $spreadsheet->getActiveSheet()->setCellValue('D18', $query['load_address']);
  $spreadsheet->getActiveSheet()->setCellValue('D19', $query['unload_address']);
  $spreadsheet->getActiveSheet()->setCellValue('B20', $query['total']);
  $spreadsheet->getActiveSheet()->setCellValue('B21', $query['total']);
  $spreadsheet->getActiveSheet()->setCellValue('E21', numberToWords($query['total'], 'lt', 'EUR'));

  $spreadsheet->getActiveSheet()->setCellValue('B65', $query['first_name']);
  $spreadsheet->getActiveSheet()->setCellValue('D65', $query['last_name']);
  $spreadsheet->getActiveSheet()->setCellValue('B66', $query['rolename']);

  $spreadsheet->getActiveSheet()->setCellValue('F65', $query['firstname']);
  $spreadsheet->getActiveSheet()->setCellValue('H65', $query['lastname']);
  $spreadsheet->getActiveSheet()->setCellValue('F66', '');

  $temp_file = tempnam(sys_get_temp_dir(), 'Excel');

  $year = date("Y");
  $month = date("F");
  $dayofweek = "week".weekOfMonth(strtotime(date("Y-m-d"))); 
  $patch = "storage/$year/$month/$dayofweek/"; 
  $time = time();
  $filename = $query['company']."_".$time;
  $filefullname = $query['company']."_".$time.".xls";

  if (!file_exists("../../../storage/$year/$month/$dayofweek")) {
      mkdir("../../../storage/$year/$month/$dayofweek", 0777, true);
  }

  $writer = IOFactory::createWriter($spreadsheet, 'Xls');
  $writer->save("../../../$patch/$filename.xls");

  insertDocument($conn,$leadid,26,$patch,$filename,$filefullname);

}


function transport_invoice($conn,$query,$date,$leadid){

  $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template.docx');


  $templateProcessor->setValues(
          array(
              'contract_date' => $date,
              'customer' => $query['company'], 
              'vat_code' => $query['company_code'],
              'customer_address' => $query['company_address'],
              'customer_phone' => $query['phone'],
              'load_company' => $query['load_company'],
              'load_date' => '',
              'load_address' => $query['load_address'],
              'load_phone' => '',
              'unload_company' => $query['unload_company'],
              'unload_date' => '',
              'unload_address' => $query['unload_address'],
              'unload_phone' => '',
              'cargo' => $query['measure'],
              'quantity' => $query['quantity'],
              'dimensions' => $query['dimensions'],
              'weight' => $query['cargo_wgt'],
              'volume' => $query['volume'],
              'note' => $query['note'],
              'amount' => $query['total'],
              'price' => $query['total'],
              'customer2' => $query['company'],
              'self_role' => $query['rolename'],
              'customer_role' => '',
              'employee' => $query['manager_name'],
              'customer_name' => $query['customer_name'],
              'signatare' => '',
              'customer_phone2' => $query['phone']
          )
  );
  $year = date("Y");
  $month = date("F");
  $dayofweek = "week".weekOfMonth(strtotime(date("Y-m-d"))); 
  $patch = "storage/$year/$month/$dayofweek/"; 

  if (!file_exists("../../../storage/$year/$month/$dayofweek")) {
      mkdir("../../../storage/$year/$month/$dayofweek", 0777, true);
  } 

  $time = time();
  $filename = $query['company']."_".$time;
  $filefullname = $query['company']."_".$time.".docx";

  $templateProcessor->saveAs("../../../$patch/$filefullname");
  insertDocument($conn,$leadid,26,$patch,$filename,$filefullname);
}
 
header("Location:/index.php?module=Leads&view=Detail&record=$leadid");



function insertDocument($db,$recordid,$userid,$patch,$filename,$filefullname)
{
    $date_now = date('Y-m-d H:i:s');
    $entity_id = get_last_entity($db);
    insert_entity($db,'Documents', $entity_id, $userid, NULL, '', 'CRM', $date_now);
    insert_senotesrel($db, $entity_id, $recordid);
    insertNotes($db,$entity_id,$filename,$filefullname,1,'I',1);    
    insert_NoteCf($db, $entity_id, $date_now);
    
    $entity_id2 = get_last_entity($db);  
    if(renameFile($patch,$filefullname,$entity_id2)){
      insert_entity($db,'Documents Attachment', $entity_id2, $userid, NULL, '', 'CRM', $date_now);
      insertAttachments($db, $entity_id2,$filefullname,$patch,$recordid);
      insertSeAttachmentsrel($db, $entity_id,$entity_id2);	
    }  
}


function insert_senotesrel($conn, $entity_id, $moduleId)
{
  $conn->query("INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES ('$moduleId', '$entity_id')");
}

function insertNotes($conn, $entity_id,$title,$filename,$folderid,$locationType,$filestatus) 
{
  $conn->query("INSERT INTO vtiger_notes (notesid, title, filename, folderid, filelocationtype, filestatus) 
                           VALUES ('$entity_id', '$title', '$filename', '$folderid','$locationType','$filestatus')");
}

function insert_NoteCf($conn, $entity_id,$file_created)
{
  $conn->query("INSERT INTO vtiger_notescf (notesid,cf_1852) VALUES ('$entity_id', '$file_created')");
}


function get_last_entity($conn) 
{  
  $conn->query('LOCK TABLES vtiger_crmentity_seq READ');
  $row = mysqli_fetch_assoc($conn->query('SELECT * FROM vtiger_crmentity_seq'));
  $conn->query('UNLOCK TABLES');

  $crmoldid = $row['id'];
  $crmid = $row['id'] + 1;
  
  $conn->query('LOCK TABLES vtiger_crmentity_seq WRITE');
  $conn->query("UPDATE vtiger_crmentity_seq SET id = $crmid");

  $crmnewid = mysqli_fetch_assoc($conn->query('SELECT id FROM vtiger_crmentity_seq'));
  $conn->query('UNLOCK TABLES');
  
  if($crmoldid < $crmnewid['id']){
    return $crmid;
  }
}

function insert_entity($conn, $setype, $entity_id, $user_id, $description, $label,$source, $date)
{
  $conn->query("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, source, createdtime, modifiedtime) VALUES ('$entity_id', '$user_id', '$user_id', '$setype', '$description', '$label','$source','$date', '$date')");
}

function weekOfMonth($date){  
  $firstOfMonth = strtotime(date("Y-m-01", $date));
  return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}

function renameFile($location,$filename,$entity_id)
{ 
  global $root_directory;
  $oldName = $root_directory.'/'.$location.$filename;
  $newName = $root_directory.'/'.$location.$entity_id.'_'.$filename;
  if(rename("$oldName","$newName")){
    return true;
  }
}

function insertAttachments($conn, $entity_id,$title,$filename,$recordid)
{
  $conn->query("INSERT INTO vtiger_attachments (attachmentsid, name, path,related_id)  VALUES ('$entity_id', '$title', '$filename','$recordid')");
}

function insertSeAttachmentsrel($conn, $entity_id,$moduleId)
{
  $conn->query("INSERT INTO vtiger_seattachmentsrel (crmid, attachmentsid)  VALUES ('$entity_id', '$moduleId')");
}

function numberToWords ( $digit, $lang, $curr) { 
	$digit = number_format($digit,2,'.','');
			$dparts = explode ( ".", $digit ); 
			$digit = $dparts[0]; 
			$ct = $dparts[1]; 
			$digitWithoutMinus = str_replace('-','',$digit);
			$digitLength = (int)strlen( $digitWithoutMinus ); 
			 
			//apsirasome zodziu masyva 
			$textOnes['lt'] = array( '', ' vienas', ' du', ' trys', ' keturi', ' penki', ' šeši', ' septyni', ' aštuoni', ' devyni' ); 
								$textTens['lt'] = array( '', ' dešimt', ' dvidešimt', ' trisdešimt', 'keturiasdešimt', ' penkiasdešimt', ' šešiasdešimt', ' septyniasdešimt', 'aštuoniasdešimt', ' devyniasdešimt' ); 
								$textExepts['lt'] = array( ' vienuolika', ' dvylika', ' trylika', 'keturiolika', ' penkiolika', ' šešiolika', ' septyniolika', ' aštuoniolika', ' devyniolika' ); 
								$textHundrets['lt'] = array( '', ' šimtas ', ' šimtai ' ); 
								$textThousands['lt'] = array( ' tūkstančių', ' tūkstantis', ' tūkstančiai' ); 

								$words = ''; 
																// nu ble ir su neigiamais malames: 
								if ( $digit < 0 ) { 
												$words = 'Minus'; 
								} 
								if( $digitLength < 7 ){ 
									for( $d = 2; $d > 0; $d-- ){ 
											//checaks - tajp rejkia 
											if( ( $d == 2 ) && ( $digitLength < 4 ) ) continue; 
											//gaunam temp skaitmeny 
												$digitLocal = substr( $digit, -3 ); 
												if( $d == 2 ) $digitLocal = substr( $digit, 0, $digitLength - 3 ); 

											//gaunam rejkiamus skaicius 
												$digitLocalLength = strlen( $digitLocal ); 
												$hundrets = substr( $digitLocal, -3, 1 ); 
												$lastTwo = substr( $digitLocal, -2 ); 
												//cia konstruojam simtus 
												if( $digitLocalLength > 2 ){ 
														$words .= $textOnes[$lang][(int)$hundrets]; 
														if( $hundrets > 1 ){ 
																$words .= $textHundrets[$lang][2]; 
														} else { 
																$words .= $textHundrets[$lang][(int)$hundrets]; 
														} 
													} 
												//sia jai qrwos nuo 10 iki 20 
						if( ( $lastTwo > 10 ) && ( $lastTwo < 20 ) ){ 
								$words .= $textExepts[$lang][(int)substr( $lastTwo, -1 ) - 1]; 
								//cia jei rejkia pridedam zody 'tukstantis' pagal linksny 
								if( $d == 2 ) $words .= $textThousands[$lang][0]; 
						//cia bet kuriuo kitu atveju 
						} else { 
							//jai yra desimtys 
							if( $digitLocalLength > 1 ) $words .= $textTens[$lang] [(int)substr( $lastTwo, -2, 1 ) ] . ' '; 
							$words .= $textOnes[$lang][(int)substr( $lastTwo, -1 )]; 
							//cia jei rejkia pridedam zody 'tukstantis' pagal linksny 
							if( $d == 2 ){ 
									if( ( (int )substr( $lastTwo, -1 ) ) > 1 ){ 
											$words .= $textThousands[$lang][2]; 
									} else { 
											$words .= $textThousands[$lang][(int)substr( $lastTwo, -1 )]; 
									} 
							} 
					} 
			} 
			$words = trim( $words ); 
		if ( $words[0] . $words[1] == 'š' ) 
						$words = 'Š' . substr( $words, 2 ); 
		else 
						$words = ucfirst( $words ); 
		} 
		if ( $ct < 10 ) $ct.='0'; 
		return $words . ' ' . $curr . ', ' . substr($ct, 0, 2) . ' ct'; 
}

