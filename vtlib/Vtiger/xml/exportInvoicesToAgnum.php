<?php
  require_once "../../../config.inc.php";
  error_reporting(0);

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $search_params = array();
  
  if(!empty($_REQUEST['parameters'])){
    $params = explode(":", $_REQUEST['parameters']);

    for($i = 0; count($params) > $i; $i++){
        $search_params[] = explode('=', $params[$i]);
    }
  }


  $invoiceid = $_REQUEST['invoiceid'];
  $userId = $_REQUEST['userid'];
  $showExported = $_REQUEST['showExported'];
  $range = $_REQUEST['range'];

  $date = date("Y-m-d");
  $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><AgnumData CreatedOn=\"$date\" CreatedByLogin=\"1\" CreatedByApp=\"CRM\" Version=\"16\"></AgnumData>"); 


  $query_string = "SELECT i.invoiceid, a.accountname as account_id,  invoicedate,invoice_no, total as hdnGrandTotal, duedate,createdtime,
   a.post_code, a.billing_address, a.municipality,a.phone,a.legal_vat_code,a.legal_entity_code, a.email1, cf_2060 AS kod
                              FROM vtiger_invoice i
                              JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=i.invoiceid
                              LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid
                              JOIN vtiger_account a ON a.accountid=i.accountid
                              JOIN vtiger_accountscf cf ON cf.accountid=a.accountid
                              WHERE e.deleted = 0 AND cf_1277 != 'Išankstinė'";
                              if($invoiceid){
                                $query_string .= " AND i.invoiceid IN ($invoiceid) ";
                              }
                              if($range){
                                $period = explode(",",$range);
                                $query_string .= " AND i.invoicedate BETWEEN '$period[0]' AND '$period[1]' ";
                              }
                              $query_string .= " GROUP BY a.accountname ";
                            
                             


if(!empty($search_params)){
	$query_string .=  " HAVING ";

	for($e = 0; count($search_params) > $e; $e++){
		$column = $search_params[$e][0];
		$value = str_replace('"',"", $search_params[$e][1]);
		
		if($column == 'createdtime' OR $column == 'duedate' OR $column == 'invoicedate'){
			$period = explode(",",$value);
      $query_string .=  " DATE_FORMAT($column, '%Y-%m-%d') BETWEEN '$period[0]' AND '$period[1]' ".($e+1 < COUNT($search_params) ? 'AND ' :'');
		}else{
			$query_string .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
    }
        
	}
}


$customers_query = $conn->query($query_string);

$customers_count = mysqli_num_rows($customers_query);

$customers = $xml->addChild('Customers'); // Grupėje saugomas pilnas aprašymas visų klientų kuriems XML faile yra nors vienas pardavimo dokumentas
$customers->addAttribute('Count', $customers_count); // Laukas Count parodo įrašų (klientų) kiekį.

  // pirmiausia suformuojam visu klientu sarasą


$invoices_id = array();

foreach($customers_query AS $row):

  $item = $customers->addChild('Item'); // Kiekvienam klientui padaromas atskiras įrašas Item.
  $item->addAttribute('SALIS_KOD',''); // Kliento šalies kodas
  $item->addAttribute('DEFAULT_CURR','EUR'); // Kuria valiutą klientui rašomos sąskaitos. (`EUR`)  /*Būtinas
  $item->addAttribute('KOD_IS',''); // Tarp mokėtojo ir gavėjo sutartas kodas, kurio pagalba mokestinio pavedimo gavėjas gali identifikuoti mokėtoją
  $item->addAttribute('POZYMIAI','0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
  $item->addAttribute('AKTYVUS','Y'); // Klientas visur rodomas ir pasiekiamas ar paslėptas.( Y – rodomas, N - paslėptas)
  $item->addAttribute('DEB','Y'); // Ar apskaičiuoti klientą kaip pirkėją. Lauko reikšmė gali būti Y arba N. /*Būtinas
  $item->addAttribute('KRD','Y'); // Ar apskaičiuoti klientą kaip tiekėją. Lauko reikšmė gali būti Y arba N. /*Būtinas
  $item->addAttribute('APSKR',''); // Kliento buveinės apskrities kodas
  $item->addAttribute('SAVIVKOD',''); // Kliento buveinės savivaldybės kodas
  $item->addAttribute('PINDEX', $row['post_code']); // Kliento buveinės pašto indeksas
  $item->addAttribute('EMAIL', $row['email1']); // Kliento elektroninio pašto adresas
  $item->addAttribute('GATVE', $row['billing_address']); // Kliento buveinės gatvė
  $item->addAttribute('MIESTAS', $row['municipality']); // Kliento buveinės miestas
  $item->addAttribute('SALIS',''); // Kliento šalis
  $item->addAttribute('ACC12','');
  $item->addAttribute('ACC11','');
  $item->addAttribute('ACC10','');
  $item->addAttribute('ACC9','');
  $item->addAttribute('ACC8','');
  $item->addAttribute('ACC7','');
  $item->addAttribute('ACC6','');  // ACC1, ... ACC12 Buhalterinės sąskaitos, surištos su klientu
  $item->addAttribute('ACC5','');
  $item->addAttribute('ACC4','');
  $item->addAttribute('ACC3','');
  $item->addAttribute('ACC2','');
  $item->addAttribute('ACC1','');
  $item->addAttribute('INFO',''); // Tekstinė informacija (pastabos) apie klientą
  $item->addAttribute('F5','');
  $item->addAttribute('F4','');
  $item->addAttribute('F3',''); //  F1, .. F5  Papildomi tekstiniai kliento rekvizitai
  $item->addAttribute('F2','');
  $item->addAttribute('F1','');
  $item->addAttribute('BSASK',''); // Pagrindinė kliento banko sąskaita
  $item->addAttribute('BPAVAD',''); // Pagrindinės kliento banko sąskaitos banko pavadinimas
  $item->addAttribute('BKOD',''); // Pagrindinės kliento banko sąskaitos banko kodas
  $item->addAttribute('PVMKOD',$row['legal_vat_code']); // Kliento PVM mokėtojo kodas
  $item->addAttribute('RKOD',$row['legal_entity_code']); // Kliento registracinis kodas ar asmens kodas. Turi būti unikalus. /*Būtinas
  $item->addAttribute('FAX',''); // Kliento fakso numeris
  $item->addAttribute('TLF', $row['phone']); // Kliento kontaktinis telefonas
  $item->addAttribute('ADR',''); // Kliento registracinis adresas
  $item->addAttribute('CONTACT','');
  $item->addAttribute('PAVAD',$row['account_id']);  // Pilnas kliento pavadinimas
  $item->addAttribute('KOD', $row['kod']); // Unikalus kliento kodas AGNUM sistemoje /*Būtinas  *** programoje ivedame kliento pavadinima mazosiomis raidemis
  // $invoices_id[] = $row['invoiceid'];
endforeach;

 
  $departments = $xml->addChild('Departments');
  $departments->addAttribute('Count', '0'); // Laukas Count parodo įrašų (klientų) kiekį.

  // $invoiceid = implode(',', $invoices_id);

// ************************************************************************
  
  // $services_title_query = $conn->query("SELECT IF(productid = 121391,itemservice_tks_name,s.name) as service_name, 
  //                                       IF(productid = 121391,itemservice_tks_accountingcode,s.code) as code,
  //                                       IF(productid = 121391,cf_1938,s.storage_code) as storage_code, 
  //                                       ROUND(CASE WHEN i.region_id = 0 
  //                                                 THEN   ( IF(productid = 121391, listprice*quantity,listprice)/100 * 21)  + IF(productid = 121391, listprice*quantity,listprice)
  //                                                 ELSE   (IF(productid = 121391, listprice*quantity,listprice)/100 * vtiger_taxregions.value) + IF(productid = 121391, listprice*quantity,listprice)
  //                                             END,2) as margin,s_h_amount as vat,  IF(vtiger_taxregions.value > 0,vtiger_taxregions.value, 21) as vatprocent, IF(productid = 121391, '',cargo_wgt) as cargo_wgt, m3,cargo_measure
  //                                       FROM vtiger_invoice i
  //                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid	
  //                                       LEFT JOIN vtiger_inventoryproductrel inv ON inv.id=i.invoiceid
  //                                       LEFT JOIN app_services_type s ON s.id=inv.service 
  //                                       LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=inv.cargo_wgt
  //                                       LEFT JOIN vtiger_itemservicecf itcf ON itcf.itemserviceid=its.itemserviceid
  //                                       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=i.region_id    
  //                                       WHERE e.deleted = 0 AND i.invoiceid IN ($invoiceid)
  //                                       GROUP BY code
  //                                       -- inv.id,sequence_no
  //                                       ORDER BY i.invoiceid");

  // $count_goods = mysqli_num_rows($services_title_query); 

 
    $goods = $xml->addChild('Goods'); // Grupėje saugomas visų prekių ir paslaugų pilnas aprašymas, kurios naudojamos nors viename pardavimo dokumente siunčiamame XML faile.
    // $goods->addAttribute('Count', $count_goods); // Laukas Count parodo įrašų kiekį.
    $goods->addAttribute('Count', 0); // Laukas Count parodo įrašų kiekį.

// foreach($services_title_query AS $row){  
//   $item = $goods->addChild('Item'); // Kiekvienai prekės ar paslaugos kortelei padaromas atskiras įrašas Item.
//   // $item->addAttribute('ACC5','');
//   // $item->addAttribute('ACC4',''); // ACC1, .. ACC5 Buhalterinės sąskaitos, surištos su preke
//   // $item->addAttribute('ACC3','');
//   // $item->addAttribute('ACC2','');
//   // $item->addAttribute('ACC1','');
//   // $item->addAttribute('F5','');
//   // $item->addAttribute('F4',''); // F1, .. F5  Papildomi tekstiniai prekės rekvizitai
//   // $item->addAttribute('F3','');
//   // $item->addAttribute('F2','');
//   // $item->addAttribute('F1','');
//   // $item->addAttribute('BKOD',''); // Prekės barkodas, įtrauktas į dokumentą
//   $item->addAttribute('PAVAD', $row['service_name']); // Prekės ar paslaugos pavadinimas
//   $item->addAttribute('KOD',$row['code']); // Unikalus prekės/paslaugos kodas AGNUM sistemoje /*Būtinas 
//   // $item->addAttribute('GAMINT_KOD',''); // Kliento (Gamintojo) kodas, priskiriamas prekės kortelėje kaip pagrindinis gamintojas. Kliento kortelė turi būti įvesta Agnum duomenų bazėje.
//   // $item->addAttribute('TIEK_KOD',''); // Kliento (Tiekėjo) kodas, priskiriamas prekės kortelėje kaip pagrindinis tiekėjas. Kliento kortelė turi būti įvesta Agnum duomenų bazėje.
//   // $item->addAttribute('PVM_KOD','PVM'.$row['vatprocent']); // Mokesčio kodas pagal PVM klasifikatorių
//   // $item->addAttribute('VNT2',''); // Papildomi matavimo vienetai
//   // $item->addAttribute('F8','');
//   // $item->addAttribute('F7',''); // F6, .. F8 Papildomi skaitmeniniai prekės rekvizitai
//   // $item->addAttribute('F6','');
//   // $item->addAttribute('MEMO',$row['note']);  // Tekstinė informacija (pastabos)
//   // $item->addAttribute('MOK0',''); // Papildomas mokesčių tarifas (Lietuvoje nenaudojamas)
//   // $item->addAttribute('PVM5',$row['vatprocent']); // PVM tarifas parduodant 3 papildoma kaina
//   // $item->addAttribute('PVM4',$row['vatprocent']); // PVM tarifas parduodant 2 papildoma kaina
//   // $item->addAttribute('PVM3',$row['vatprocent']); // PVM tarifas parduodant 1 papildoma kaina
//   // $item->addAttribute('PVM2',$row['vatprocent']); // PVM tarifas parduodant mažmenine kaina
//   // $item->addAttribute('PVM',$row['vatprocent']); // PVM tarifas parduodant didmenine kaina
//   // $item->addAttribute('PVM0',$row['vatprocent']); // Pirkimo PVM tarifas
//   // $item->addAttribute('APMOKPVM',($row['vatprocent'] ? 'Y': 'N')); // Ar PVM apmokestinama
//   // $item->addAttribute('UZS','0'); // Minimalus užsakomas kiekis
//   // $item->addAttribute('REZ','0'); // Minimalus likutis sandėlyje
//   // $item->addAttribute('KNBVAL','EUR'); // Bazinės kainos valiuta  /*Būtinas
//   // $item->addAttribute('KNB',$row['margin']); // Bazinė kaina
//   // $item->addAttribute('KN5',$row['margin']); // Papildoma kaina 3
//   // $item->addAttribute('KN4',$row['margin']); // Papildoma kaina 2
//   // $item->addAttribute('KN3',$row['margin']); // Papildoma kaina 1
//   // $item->addAttribute('KN2',$row['margin']); // Mažmeninė kaina
//   // $item->addAttribute('KN1',$row['margin']); // Didmeninė kaina
//   // $item->addAttribute('KN0',$row['margin']); // Pirkimo kaina
//   // $item->addAttribute('IPAK','1'); // Įpakavimas
//   // $item->addAttribute('METOD','0'); // Apskaitos metodas:  0 – FIFO;  1 – LIFO;  2 – AVERAGE;
//   // $item->addAttribute('VIETA',''); // Vieta sandėlyje
//   // $item->addAttribute('TURIS',$row['m3']); // Vieneto turis
//   // $item->addAttribute('SVORIS1',$row['cargo_wgt']); // Svoris neto
//   // $item->addAttribute('SVORIS',$row['cargo_wgt']); // Svoris bruto
//   // $item->addAttribute('VNT', trim(preg_replace('/[0-9]+/', '', $row['cargo_measure']))); // Matavimo vienetai
//   // $item->addAttribute('PAVAD8','');
//   // $item->addAttribute('PAVAD7','');
//   // $item->addAttribute('PAVAD6','');
//   // $item->addAttribute('PAVAD5','');
//   // $item->addAttribute('PAVAD4',''); // PAVAD1, .. PAVAD8 Prekės pavadinimo vertimai
//   // $item->addAttribute('PAVAD3','');
//   // $item->addAttribute('PAVAD2','');
//   // $item->addAttribute('PAVAD1','');
//   // $item->addAttribute('POSAKIS',''); // Prekės pošakis
//   // $item->addAttribute('KRYPTIS',''); // Prekės kryptis
//   // $item->addAttribute('POGRUPIS',''); // Prekės pogrupis
//   // $item->addAttribute('GRUPE',''); // Prekės grupė
//   // $item->addAttribute('KATEG',''); // Prekės kategorija
//   $item->addAttribute('CLASS','1'); // Įrašo tipas. Gali būti tik sekančios reikšmės  0 - Prekė;  1 - Paslauga ;  2 – Gaminys;  3 – Pusgaminis;  5 – Ilgalaikis turtas;   6 – Kuras; /* 
//   $item->addAttribute('SND_KOD', $row['storage_code']); // Unikalus sandėlio kodas AGNUM sistemoje /*Būtinas
// }

// ************************************************************************************************************

  $objects = $xml->addChild('Objects'); 
  $objects->addAttribute('Count', '0');

  $accounts = $xml->addChild('Accounts');  // Grupėje saugomos visos banko sąskaitos, kurios naudojamos nors viename XML faile siunčiamame pardavimo dokumente.
  $accounts->addAttribute('Count', '1');
  $item = $accounts->addChild('Item');
  $item->addAttribute('SASK', 'LT127044060007983370');

  


  $query = "SELECT i.invoiceid,i.invoicedate,i.invoice_no, REPLACE(ROUND(total,2),'.',',') as total, REPLACE(ROUND(subtotal,2),'.',',') AS subtotal,REPLACE(ROUND(s_h_amount,2),'.',',') AS s_h_amount,a.accountname, 
                                    CASE WHEN i.region_id = 0 THEN 21 ELSE t.value END AS vatprocent,   
                                    CASE WHEN cf_1277 = 'Debetinė' THEN 'SF'
                                        WHEN cf_1277 = 'Kreditinė' THEN 'KS'
                                        ELSE 'SF'
                                        END invtype, 
                                        IF(productid = 121391,cf_1938,s.storage_code) as storage_code,
                                        IF(productid = 121391,	IF(itemservice_tks_type = 'Item', REPLACE(ROUND(subtotal,2),'.',','),'0'), '0') AS goods,                                    
                                  IF(productid != 121391, REPLACE(ROUND(subtotal,2),'.',','), IF(itemservice_tks_type = 'Service', REPLACE(ROUND(subtotal,2),'.',','),'0'))  AS service_price,
                                    CONCAT(first_name,' ',last_name) AS created_user, e.description,(duedate- invoicedate + cf_1279) as pay_delay,invoicedate,cf_2060 AS kod,  
                                    -- GROUP_CONCAT(DISTINCT IF(productid = 14244, IF(cf_1486 = 'Transportas',0,1), IF(productid =  36641, IF(code_id !='',code_id-1,''),IF(cf_2066 != '',cf_2066-1,'')))	) AS token
                                    CONCAT(CASE WHEN cf_1486 = 'Transportas' THEN 0 WHEN cf_1486 = 'Perkraustymas' THEN 1 ELSE '' END  ,',',
                                          GROUP_CONCAT(
                                          CASE WHEN productid = 14244 THEN ''
                                                WHEN productid = 36641 THEN IF(code_id,code_id-1,'')
                                            WHEN productid = 121391 THEN IF(cf_2066,cf_2066-1,'')		         
                                          END  
                                          )) AS token
                                    FROM vtiger_invoice i                   
                                    LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid                        
                                    LEFT JOIN vtiger_taxregions t ON t.regionid=i.region_id 
                                    LEFT JOIN vtiger_account a ON a.accountid=i.accountid
                                    LEFT JOIN vtiger_accountscf acf ON acf.accountid=a.accountid
                                    LEFT JOIN vtiger_invoicecf ic ON ic.invoiceid=i.invoiceid
                                    LEFT JOIN vtiger_users u ON u.id=e.smcreatorid                                   
                                    LEFT JOIN vtiger_inventoryproductrel inv ON inv.id=i.invoiceid 
                                    LEFT JOIN vtiger_invoice_exported_agnum ON vtiger_invoice_exported_agnum.invoiceid=i.invoiceid 
                                    LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=inv.cargo_wgt
                                    LEFT JOIN vtiger_itemservicecf itcf ON itcf.itemserviceid=its.itemserviceid
                                    LEFT JOIN app_services_type s ON s.id=inv.service 
                                    WHERE e.deleted = 0 AND i.invoiceid IN ($invoiceid) AND cf_1277 != 'Išankstinė' ";

                                    if($showExported == 'false'){
                                      $query .= " AND vtiger_invoice_exported_agnum.invoiceid IS NULL ";
                                    }
                                    $query .=  "GROUP BY invoiceid ORDER BY i.invoiceid";

  $query2 = "SELECT total FROM vtiger_invoice i
                          LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid                       
                          LEFT JOIN vtiger_invoice_exported_agnum ON vtiger_invoice_exported_agnum.invoiceid=i.invoiceid
                          WHERE e.deleted = 0 AND i.invoiceid IN ($invoiceid) AND total != 0 ";
                          if($showExported == 'false'){
                            $query2 .= " AND vtiger_invoice_exported_agnum.invoiceid IS NULL ";
                          }
                          $query2 .= " GROUP BY i.invoiceid";


  $invoices_query = $conn->query($query);                                            
  $invoices_query2 = $conn->query($query2);

  $invoices_count = mysqli_num_rows($invoices_query2);

  $documents = $xml->addChild('Documents');  // Grupėje saugomi pardavimo dokumentai.
  $documents->addAttribute('Count', $invoices_count); // Laukas Count parodo dokumentų kiekį
  $documents->addAttribute('Type', '4'); // laukas Type visada lygus 4.


  $errors = array();


  foreach($invoices_query AS $row):

    if($row['total'] != 0 && !empty($row['kod'])){

      $num_rows = mysqli_num_rows($conn->query("SELECT * FROM vtiger_invoice_exported_agnum WHERE invoiceid = '".$row['invoiceid']."'"));

      $token = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

      $tok = explode(",",trim($row['token'],','));
  
     
      foreach($tok AS $id){    
         if(is_numeric($id)){
          $token[$id] = 1;
        }
      }
      

      $token2 = implode("",$token);

      $item = $documents->addChild('Item');
      $item->addAttribute('Count', 0);
      $item->addAttribute('POZYMIAI', $token2); //*Būtinas
      $item->addAttribute('MOK0', '0'); // Papildomų mokesčių tarifas
      $item->addAttribute('PVM', $row['s_h_amount']); // PVM suma
      $item->addAttribute('SND_KOD', $row['storage_code'] ?: '05'); // Sandėlio kodas AGNUM sistemoje
      $item->addAttribute('SASK', 'LT127044060007983370'); // Banko sąskaitos numeris /*Būtinas
      $item->addAttribute('SUMAVISO',$row['subtotal']); // Suma viso be PVM
      $item->addAttribute('SUMAP', $row['service_price']); // Suma viso už paslaugas be PVM
      $item->addAttribute('SUMA', $row['goods']); // Suma viso už prekes be PVM
      $item->addAttribute('NUOL', '0'); // Nuolaida viso
      $item->addAttribute('DOKMEMO', $row['description']); // Pastabos tekstas, įrašomas pardavimo dokumente.
      $item->addAttribute('DOK_USER', '19'); // Duomenų bazės vartotojo kodas, kuris bus nurodytas kaip vartotojas, kuris paskutinis redagavo dokumentą
      $item->addAttribute('PVM_KL', '0');
      $item->addAttribute('SPEC_TAX', 'N'); // Specialaus apmokestinimo požymis (Y/N)
      $item->addAttribute('REF_DOK_DATA', ""); // Tikslinamos sąskaitos data
      $item->addAttribute('REF_DOK_NR', ""); // Tikslinamos sąskaitos numeris
      $item->addAttribute('SF_TIP', $row['invtype']); // Sąskaitos faktūros tipas: SF-PVM sąskaito-faktura, DS-debetine sf,KS-kreditine sf
      $item->addAttribute('POINTS_USED', '0'); // Panaudoti kliento taškai
      $item->addAttribute('POINTS_ADDED', '0'); // Pridedami klientui taškai
      $item->addAttribute('M10', 'Y'); // Ar įtraukti sąskaitą į I.SAF deklaraciją (Y/N)
      $item->addAttribute('SUMAK', '0'); // Suma prekių su žyme „Konsignacinės“
      $item->addAttribute('FNUM3', '0');
      $item->addAttribute('FNUM2', '0'); // FNUM1, .. FNUM3 Papildomi dokumento skaitmeniniai rekvizitai
      $item->addAttribute('FNUM1', '0');
      $item->addAttribute('SAVIK', '0');
      $item->addAttribute('SALISSIUNT', ''); // Informacija intrastatui, šalis siuntėja
      $item->addAttribute('TRANSPORTAS', ''); // Informacija intrastatui, transporto tipas
      $item->addAttribute('PRISTSAL', ''); // Informacija intrastatui, pristatymo sąlygos
      $item->addAttribute('SANDORIS', ''); // Informacija intrastatui, sandorio tipas
      $item->addAttribute('AG_KOD', ''); // Agento kodas
      $item->addAttribute('DRB1', ''); // Darbuotojo vardas, pareigos – kas išdavė prekes
      $item->addAttribute('DRBKOD1', ''); // Darbuotojo kodas – kas išdavė prekes
      $item->addAttribute('DRB', trim($row['created_user'])); // Darbuotojo vardas, pareigos – kas išrašė sąskaitą
      $item->addAttribute('DRBKOD', ''); // Darbuotojo kodas – kas išrašė sąskaitą
      $item->addAttribute('ADDR2', ''); // ADDR1, ADDR2 Papildomi tekstiniai dokumento rekvizitai
      $item->addAttribute('ADDR1', '');
      $item->addAttribute('CHECKD', ''); // Kasos kvito data. Datos formatas YYYY.MM.DD
      $item->addAttribute('KSNR', ''); //Kasos aparato numeris
      $item->addAttribute('CHECKNR', ''); // Kasos kvito numeris
      $item->addAttribute('SUMISR', 'Y'); // Ar suminis dokumentas Y – dokumentas yra sumine išraiška, N – dokumentas kiekinis (turi įrašus apie prekes ar paslaugas)
      $item->addAttribute('KIENO_TR', '0'); // Pristatymo transportas:  0 - Pardavėjo;  1 - Pirkėjo
      $item->addAttribute('PR_DATA', ''); // Pristatymo data. Datos formatas YYYY.MM.DD
      $item->addAttribute('PR_VIETA', ''); // Pristatymo vieta
      $item->addAttribute('KNTIP', '4'); // Kuriomis kainomis buvo išrašyta sąskaita:  0 – savikaina;  1 - faktine.savikaina;  2 – pirkimo kaina;  3 – didmenine kaina;  4 – mažmemine kaina;  5...6 – papildomomis kainomis
      $item->addAttribute('TERM', $row['pay_delay']); // Apmokėjimo terminas dienomis
      $item->addAttribute('APM_SAL', '1'); // Apmokėjimo sąlygos: 0 – grynais; 1 – skolon; 2 – pavedimu; /*Būtinas
      $item->addAttribute('APMSUM', '0'); // Kliento apmokėta ar padengta suma
      $item->addAttribute('SKOLA', $row['total']); // Kliento skola viso
      $item->addAttribute('SUMVISO', $row['subtotal']); // Suma viso be PVM
      $item->addAttribute('PVMPROC', $row['vatprocent']); // PVM viso procentais
      $item->addAttribute('NUOLPROC', '0'); // Nuolaida viso procentais
      $item->addAttribute('KURS', '1'); // Valiutos kursas /*Būtinas
      $item->addAttribute('VAL', 'EUR'); // Valiutos kodas /*Būtinas
      $item->addAttribute('PAD_KOD', ''); // Pirkėjo padalinio kodas AGNUM sistemoje
      $item->addAttribute('KL_KOD', $row['kod']); // Pirkėjo kodas AGNUM sistemoje /*Būtinas
      $item->addAttribute('GVNR', ''); // Gabenimo važtaraščio numeris
      $item->addAttribute('DOKNR2', $row['invoice_no']); // Registracinis dokumento numeris
      $item->addAttribute('DOKNR', $row['invoice_no']); // Dokumento numeris /*Būtinas
      $item->addAttribute('DATA', $row['invoicedate']); // Dokumento data. Datos formatas YYYY.MM.DD /*Būtinas

      // $services_query = $conn->query("SELECT ROUND(inv.listprice,2) AS price, m3,cargo_wgt,cargo_measure,quantity,
      //                               IF(productid = 121391,itemservice_tks_accountingcode,se.code) as code, 
      //                               ROUND(CASE WHEN inv.discount_percent > 0 THEN (listprice/100) * inv.discount_percent
      //                                   WHEN inv.discount_amount > 0 THEN inv.discount_amount
      //                                   ELSE 0
      //                               END,2) discount, DATE_FORMAT(unload_date_to, '%Y.%m.%d') AS unload_date  
      //                               FROM vtiger_inventoryproductrel inv
      //                               LEFT JOIN vtiger_crmentity e ON e.crmid=inv.id   
      //                               LEFT JOIN vtiger_salesorder s ON s.salesorderid=order_id
      //                               LEFT JOIN app_services_type se ON se.id=inv.service 
      //                               LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=inv.cargo_wgt                                                                  
      //                               WHERE e.deleted = 0 AND inv.id = '".$row['invoiceid']."'
      //                               GROUP BY inv.id,sequence_no");

    
      // $vatprocent = $row['vatprocent'];
      // foreach($services_query AS $rows){ 
      //   $row = $item->addChild('Row'); // Įrašas saugoja informaciją apie parduodamą prekę ir paslaugą
      //   $row->addAttribute('BKOD', ' '); // Barkodas su kuriuo buvo parduota prekė /*Būtinas
      //   $row->addAttribute('KOD', $rows['code']); // Prekės ar paslaugos kodas /*Būtinas
      //   $row->addAttribute('PVM_KOD', 'PVM'.$vatprocent); // Mokesčio kodas pagal PVM klasifikatorių, taikomas visam dokumentui
      //   $row->addAttribute('PVM', ($rows['price']/100) * $vatprocent); // Įrašo PVM suma
      //   $row->addAttribute('VNT', trim(preg_replace('/[0-9]+/', '', $rows['cargo_measure']))); // Pardavimo matavimo vienetai
      //   $row->addAttribute('NUOL', $rows['discount']); // Pritaikyta nuolaida prekei
      //   $row->addAttribute('COMMENT', $rows['note']); // Tekstinis komentaras prekei
      //   $row->addAttribute('OKOD', '');
      //   $row->addAttribute('ZYME', ''); // Tekstinė pastaba
      //   $row->addAttribute('UZS_SHOPNR', ''); // Parduotuvės kodas
      //   $row->addAttribute('UZS_PRKKOD', ''); // Prekės kodas užsakovo sistemoje
      //   $row->addAttribute('UZS_UZSNR', ''); // Užsakymo numeris užsakovo sistemoje
      //   $row->addAttribute('UZS_PRDATA', $rows['unload_date']); // Užsakymo pristatymo data. Datos formatas YYYY.MM.DD
      //   $row->addAttribute('NUOL2', '0'); // Papildomos bendros nuolaidos dalys
      //   $row->addAttribute('ORIGPARKN', $rows['price']); // Pardavimo kaina be nuolaidos
      //   $row->addAttribute('PARKN', $rows['price'] - $rows['discount']); // Pardavimo kaina su nuolaida
      //   $row->addAttribute('MOK0_LT', '0'); // Papildomo mokesčio suma
      //   $row->addAttribute('MOK0_PROC', '0'); // Taikomas papildomo mokesčio tarifas
      //   $row->addAttribute('PVM_PROC', $vatprocent); // Taikomas PVM tarifas
      //   $row->addAttribute('KIEKIS', $rows['quantity']); // Parduotas kiekis
      // }
      if(!$num_rows){
       $conn->query("INSERT INTO vtiger_invoice_exported_agnum (invoiceid) VALUES ('".$row['invoiceid']."')");
      }

    }else{
      $errors[] = $row['invoice_no'];
    }

endforeach;

  setcookie("errors", "", time()-3600);
  if(count($errors) > 0){
    $error = implode(",", $errors);
    setcookie("errors", $error, time()+3600, "/","", 0);
    // header('location:/index.php?module=Exportinvoices&view=List');
  }

  $barcodes = $xml->addChild('Barcodes'); // Grupėje išvardinti visi grupėje Goods nurodytų prekių ir paslaugų barkodai
  $barcodes->addAttribute('Count', '0');
  $timestamp = time();

  header( "Content-Encoding: UTF-8" );
  header( "Content-Disposition: attachment; filename=saskaitos_agnum_$timestamp.xml");
  header( "Content-Type: text/xml; charset=UTF-8" );
  header( "Content-Description: File Transfer" );


  $dom = dom_import_simplexml($xml)->ownerDocument;
  $dom->formatOutput = true;
  echo $dom->saveXML();

