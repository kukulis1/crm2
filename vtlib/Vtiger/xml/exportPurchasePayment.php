<?php

global $_REQUEST;
require_once "../../../config.inc.php";

error_reporting(0);

    $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
    $conn ->set_charset("utf8");

    if(!empty($_REQUEST['parameters'])){
        $params = explode(":", $_REQUEST['parameters']);
    
        for($i = 0; count($params) > $i; $i++){
            $search_params[] = explode('=', $params[$i]);
        }
    }

    $iban = str_replace(" ","",$_REQUEST['iban']);
    $format = $_REQUEST['format'];
    $showExported = $_REQUEST['showExported'];
    $purchaseorderid = $_REQUEST['purchaseid'];
    $userId = $_REQUEST['userid'];

    $bankName = ($iban == 'LT127044060007983370' ? 'SEB' : 'LUMINOR');

    $first = date("Y-m-d", strtotime("first day of this month"));
    $last = date("Y-m-d", strtotime("last day of this month"));	

$query_string = "SELECT DISTINCT vtiger_purchaseorder.purchaseorderid, cf_1458 as receiver_code,
                      CASE 
                        WHEN payment_tks_suma IS NULL
                        THEN FORMAT( SUM(ABS(total)),2)
                        ELSE FORMAT(( SUM(ABS(total)) - SUM(ABS(vtiger_payment.payment_tks_suma))),2)
                      END AS debt, 
                      ROUND(SUM(ABS(payment_tks_suma)),2) AS payment_tks_suma,vendorname AS vendor_id,
                      CASE 
                        WHEN payment_tks_suma IS NULL
                        THEN 0
                        ELSE FORMAT(SUM(ABS(vtiger_payment.payment_tks_suma)),2)
                      END AS payed, 
                      FORMAT(SUM(ABS(vtiger_payment.payment_tks_suma)),2) AS paid_sum, ROUND(SUM(total),2) as total,
                      SUM(ABS(total)) AS total2,
                      FORMAT(total, 2) AS total_with_vat, vtiger_purchaseorder.purchaseorder_no, vtiger_purchaseorder.postatus, 
                      FORMAT(SUM(vtiger_purchaseorder.total),2) as hdnGrandTotal, 
                      vtiger_purchaseorder.vendorid, cf_1345 as invoicedate, vtiger_purchaseorder.duedate, vtiger_crmentity.createdtime,
                      GROUP_CONCAT(vtiger_purchaseordercf.cf_1355) as invoice_nr,
                      payment_tks_mokėjimodata AS pay_date, cf_1610 as client_iban,cf_1355
                FROM vtiger_purchaseorder 
                LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid AND relmodule = 'Payment'
                LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid = vtiger_purchaseorder.purchaseorderid 
                LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 
                LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_purchaseorder.vendorid   
                LEFT JOIN vtiger_vendorcf ON vtiger_vendorcf.vendorid=vtiger_purchaseorder.vendorid         
                LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_payment.paymentid  AND setype = 'Payment'
                INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid 
                LEFT JOIN vtiger_crmentity as acc_owner ON acc_owner.crmid = vtiger_vendor.vendorid AND acc_owner.setype = 'Vendor'            
                LEFT JOIN vtiger_groups ON vtiger_crmentity.smownerid = vtiger_groups.groupid 
                LEFT JOIN vtiger_crmentity_user_field ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity_user_field.recordid AND vtiger_crmentity_user_field.userid=1 
                WHERE vtiger_crmentity.deleted=0 AND vtiger_crmentity.setype = 'PurchaseOrder'  AND ( de.deleted = 0 OR de.deleted IS NULL) AND payment_tks_tipas = 'Pavedimu' AND cf_1610 !=  '' ";
                if(!empty($purchaseorderid)){
                  $query_string .= " AND vtiger_purchaseorder.purchaseorderid IN ($purchaseorderid)";
                }
               
                 if($showExported == 'false'){
                    $query_string .= " AND vtiger_purchaseorder.exported IS NULL  ";
                  }
                $query_string .= " GROUP BY vtiger_purchaseorder.vendorid
                HAVING paid_sum != 0 AND (debt <= total2 OR payment_tks_suma IS NULL) ";

                if(empty($params)){
                  $query_string .= " AND pay_date BETWEEN '$first' AND '$last' ";
                }

if(isset($search_params)){
	$query_string .=  " AND ";

	for($e = 0; count($search_params) > $e; $e++){
		$column = $search_params[$e][0];
		$value = str_replace('"',"", $search_params[$e][1]);
		
		if($column == 'createdtime' OR $column == 'cf_1345' OR $column == 'duedate' OR $column == 'pay_date'){
			$period = explode(",",$value);
			$query_string .=  " $column BETWEEN '$period[0] 00:00:00' AND '$period[1] 23:59:59' ".($e+1 < COUNT($search_params) ? 'AND ' :'');		
		}else{
			$query_string .= " $column LIKE '$value%' ".($e+1 < COUNT($search_params) ? 'AND ' :'');	
    }
        
	}
}

$query_string .= " ORDER BY vtiger_crmentity.createdtime DESC";

$organization = mysqli_fetch_assoc($conn->query("SELECT organizationname,companycode FROM vtiger_organizationdetails"));
$organizationName = $organization['organizationname'];
$companycode = $organization['companycode'];


$purchaseResult = $conn->query($query_string);
$purchaseResult2 = $conn->query($query_string);


$info = mysqli_fetch_assoc($purchaseResult);

$num_rows = mysqli_num_rows($purchaseResult);


if($num_rows){
  $conn->query("UPDATE vtiger_purchaseorder SET exported = 1 WHERE purchaseorderid IN ($purchaseorderid)");
  $totalPriceArr = array();
  foreach ($purchaseResult2 as $row){
    $id = last_modtracker_record($conn);
    $viso = $row['total'];   
    $orderId = $row['purchaseorderid'];
    $date = date("Y-m-d H:i:s");     
    $conn->query("INSERT INTO vtiger_modtracker_basic (id, crmid, module, whodid, changedon,status) VALUES ('$id','$orderId','PurchaseOrder','$userId', '$date',0)");
    $conn->query("INSERT INTO vtiger_modtracker_detail (id, fieldname, prevalue, postvalue) VALUES ('$id','cf_1616', '', 'Eksportuota $viso € suma $bankName bankui')");
    $conn->query("UPDATE vtiger_modtracker_basic_seq SET id = $id");
    $totalPriceArr[] = $row['total'];
    
  }

  $totalPrice = array_sum($totalPriceArr);

  $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03\"></Document>"); 



  $CstmrCdtTrfInitn = $xml->addChild('CstmrCdtTrfInitn');
  $GrpHdr = $CstmrCdtTrfInitn->addChild('GrpHdr');
  $GrpHdr->addChild('MsgId', "M".$info['purchaseorderid'].time());
  $GrpHdr->addChild('CreDtTm', date("Y-m-d")."T".date('H').":00:00");
  $GrpHdr->addChild('NbOfTxs', $num_rows);
  $GrpHdr->addChild('CtrlSum', $totalPrice);
  $InitgPty =  $GrpHdr->addChild('InitgPty');
  $InitgPty->addChild('Nm',$organizationName);



$i = 1;
foreach ($purchaseResult as $row){
  
  $id = last_modtracker_record($conn);
  $orderId = $row['purchaseorderid'];  
  $generated_crm = $orderId.time();
  $date = date("Y-m-d H:i:s");     
  $conn->query("INSERT INTO vtiger_modtracker_basic (id, crmid, module, whodid, changedon,status) VALUES ('$id','$orderId','PurchaseOrder','$userId', '$date',0)");
  $conn->query("INSERT INTO vtiger_modtracker_detail (id, fieldname, prevalue, postvalue) VALUES ('$id','cf_1653', 0,1)");
  $conn->query("UPDATE vtiger_modtracker_basic_seq SET id = $id"); 
  $conn->query("INSERT INTO vtiger_purchaseorder_export_payments (purchaseorderid, uniqueid, export_date) VALUES ($orderId,$generated_crm,'$date')");
    
   
    $PmtInf = $CstmrCdtTrfInitn->addChild('PmtInf');
    $PmtInf->addChild('PmtInfId', "M".$row['purchaseorderid']);
    $PmtInf->addChild('PmtMtd', "TRF");
    $PmtInf->addChild('NbOfTxs', "1");
    $PmtInf->addChild('CtrlSum', $row['total']);
    $PmtInf->addChild('ReqdExctnDt', date("Y-m-d"));

    $Dbtr = $PmtInf->addChild('Dbtr');
    $Dbtr->addChild('Nm',$organizationName);
    if(!empty($companycode)){
      $userID = $Dbtr->addChild('Id');
      $OrgId = $userID->addChild('OrgId');
      $Othr = $OrgId->addChild('Othr');
      $Othr->addChild('Id', $companycode);
      $SchmeNm = $Othr->addChild('SchmeNm');
      $SchmeNm->addChild('Cd','COID');
    }

    $DbtrAcct = $PmtInf->addChild('DbtrAcct');
    $Id = $DbtrAcct->addChild('Id');
    $Id->addChild('IBAN',$iban);

 

    $DbtrAgt = $PmtInf->addChild('DbtrAgt');
    $FinInstnId = $DbtrAgt->addChild('FinInstnId');
    $FinInstnId->addChild('BIC','CBVILT2X');

    $CdtTrfTxInf = $PmtInf->addChild('CdtTrfTxInf');
    $PmtId = $CdtTrfTxInf->addChild('PmtId');
    $PmtId->addChild('InstrId', substr($row['invoice_nr'], 0, 35));
    $PmtId->addChild('EndToEndId', $generated_crm);

    $Amt = $CdtTrfTxInf->addChild('Amt');
    $InstdAmt = $Amt->addChild('InstdAmt', $row['total']);
    $InstdAmt->addAttribute('Ccy','EUR');

    $Cdtr = $CdtTrfTxInf->addChild('Cdtr');
    $Nm = $Cdtr->addChild('Nm', str_replace('&', '&amp;', $row['vendor_id']));

    if(!empty($row['receiver_code'])){
      $clientId = $Cdtr->addChild('Id');
      $PrvtId = $clientId->addChild('PrvtId');
      $Othr = $PrvtId->addChild('Othr');
      $Othr->addChild('Id',$row['receiver_code']);
      $SchmeNm2 = $Othr->addChild('SchmeNm');
      $SchmeNm2->addChild('Cd','COID');
    }


    $CdtrAcct = $CdtTrfTxInf->addChild('CdtrAcct');
    $Id = $CdtrAcct->addChild('Id');
    $Id->addChild('IBAN', $row['client_iban']);
    
    $RmtInf = $CdtTrfTxInf->addChild('RmtInf');
    $Ustrd = $RmtInf->addChild('Ustrd', substr($row['invoice_nr'], 0, 140));
  $i++;
}

  $date = strtotime(date("Y-m-d"));

  header( "Content-Encoding: UTF-8" );
  header( "Content-Disposition: attachment; filename=purchasePayments_$date.xml");
  header( "Content-Type: text/xml; charset=UTF-8" );
  header( "Content-Description: File Transfer" );


  $dom = dom_import_simplexml($xml)->ownerDocument;
  $dom->formatOutput = true;
  echo $dom->saveXML();

}else{
  setcookie("error", "empty_list", time()+3600, "/","", 0);
}



function last_modtracker_record($conn)
{

  $result = $conn->query('SELECT id FROM vtiger_modtracker_basic_seq');
  $productid1 = $result->fetch_assoc();
  $crmid = $productid1['id'] + 1;    
  
  return $crmid;
}
