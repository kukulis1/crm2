<?php
  require_once "../../../config.inc.php";
  error_reporting(0);

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $search_params = array();
  
  if(!empty($_REQUEST['parameters'])){
    $params = explode(":", $_REQUEST['parameters']);

    for($i = 0; count($params) > $i; $i++){
        $search_params[] = explode('=', $params[$i]);
    }
  }


  $purchaseorderid = $_REQUEST['invoiceid'];
  $userId = $_REQUEST['userid'];
  $showExported = $_REQUEST['showExported'];
  $range = $_REQUEST['range'];

  $date = date("Y-m-d");
  $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><AgnumData CreatedOn=\"$date\" CreatedByLogin=\"1\" CreatedByApp=\"CRM\" Version=\"16\"></AgnumData>"); 


  $query_string = "SELECT v.vendorname, cf_1458 AS rkod,
                      CASE 
                        WHEN vtiger_purchaseordercf.cf_1610 != ''
                        THEN 1
                        ELSE 0
                      END AS has_iban,cf_2060 AS kod
                    FROM vtiger_purchaseorder p
                    LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid	
                    LEFT JOIN vtiger_vendorcf cfv ON cfv.vendorid=v.vendorid
                    INNER JOIN vtiger_crmentity ON p.purchaseorderid = vtiger_crmentity.crmid 
                    INNER JOIN vtiger_purchaseordercf ON p.purchaseorderid = vtiger_purchaseordercf.purchaseorderid 
                    LEFT JOIN vtiger_accountscf a ON a.accountid=v.accountid
                    WHERE vtiger_crmentity.deleted=0 AND p.purchaseorderid IN ($purchaseorderid) AND cf_1343 != 'Išankstinė' ";            

                    if($showExported == 'false'){
                      $query_string .= " AND exported_agnum = 0 ";
                    }                            


$query_string .= " GROUP BY v.vendorname ";

$customers_query = $conn->query($query_string);

$customers_count = mysqli_num_rows($customers_query);

$customers = $xml->addChild('Customers'); // Grupėje saugomas pilnas aprašymas visų klientų kuriems XML faile yra nors vienas pardavimo dokumentas
$customers->addAttribute('Count', $customers_count); // Laukas Count parodo įrašų (klientų) kiekį.

  // pirmiausia suformuojam visu klientu sarasą
$invoices_id = array();


foreach($customers_query AS $row):
  $item = $customers->addChild('Item'); // Kiekvienam klientui padaromas atskiras įrašas Item.
  $item->addAttribute('KOD', $row['kod']); // Unikalus kliento kodas AGNUM sistemoje /*Būtinas  *** programoje ivedame kliento pavadinima mazosiomis raidemis
  $item->addAttribute('PAVAD',$row['vendorname']); // Pilnas kliento pavadinimas
  $item->addAttribute('CONTACT',''); // Kliento kontaktinis asmuo
  $item->addAttribute('ADR',''); // Kliento registracinis adresas
  $item->addAttribute('TLF','');
  $item->addAttribute('FAX','');  
  $item->addAttribute('RKOD',$row['rkod']); // Kliento registracinis kodas ar asmens kodas. Turi būti unikalus. /*Būtinas
  $item->addAttribute('PVMKOD','');
  $item->addAttribute('BKOD','');
  $item->addAttribute('BSASK','');
  $item->addAttribute('F1','');
  $item->addAttribute('F2','');
  $item->addAttribute('F3','');
  $item->addAttribute('F4','');
  $item->addAttribute('F5','');
  $item->addAttribute('INFO','');
  $item->addAttribute('ACC1','');
  $item->addAttribute('ACC2','');
  $item->addAttribute('ACC3','');
  $item->addAttribute('ACC4','');
  $item->addAttribute('ACC5','');
  $item->addAttribute('ACC6','');
  $item->addAttribute('ACC7','');
  $item->addAttribute('ACC8','');
  $item->addAttribute('ACC9','');
  $item->addAttribute('ACC10','');
  $item->addAttribute('ACC11','');
  $item->addAttribute('ACC12','');
  $item->addAttribute('SALIS_KOD','');
  $item->addAttribute('SALIS','');
  $item->addAttribute('MIESTAS','');
  $item->addAttribute('GATVE','');
  $item->addAttribute('EMAIL','');
  $item->addAttribute('PINDEX','');
  $item->addAttribute('SAVIVKOD','');
  $item->addAttribute('APSKR','');
  $item->addAttribute('KRD','Y');   // Ar apskaičiuoti klientą kaip tiekėją. Lauko reikšmė gali būti Y arba N. /*Būtinas
  $item->addAttribute('DEB','N');   // Ar apskaičiuoti klientą kaip pirkėją. Lauko reikšmė gali būti Y arba N. /*Būtinas
  $item->addAttribute('AKTYVUS','Y'); // Klientas visur rodomas ir pasiekiamas ar paslėptas.( Y – rodomas, N - paslėptas)
  $item->addAttribute('POZYMIAI','0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
  $item->addAttribute('KOD_IS',''); 
  $item->addAttribute('DEFAULT_CURR','EUR');  // Kuria valiutą klientui rašomos sąskaitos. (`EUR`)  /*Būtinas 
  $item->addAttribute('FIZ_ASM','');
  $item->addAttribute('VEZEJAS','');  
endforeach;


  $goods_query = "SELECT cf_1938 AS storage,s.itemservice_tks_name, IF(type = 'Prekė',0,1) AS class, REPLACE((listprice*quantity) + ((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100),'.',',') as margin,((listprice*quantity) * IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) AS vat, IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value)  AS vat_procent,cf_2157 AS kodas,itemservice_tks_accountingcode AS goods_kodas
                  FROM vtiger_purchaseorder p                
                  JOIN vtiger_inventoryproductrel i ON i.id=p.purchaseorderid
                  JOIN vtiger_itemservice s ON s.itemservice_tks_name=i.cargo_wgt
                  JOIN vtiger_itemservicecf scf ON scf.itemserviceid=s.itemserviceid
                  LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=p.region_id)      
                  INNER JOIN vtiger_crmentity ON p.purchaseorderid = vtiger_crmentity.crmid                  
                  WHERE vtiger_crmentity.deleted=0 AND ROUND(cf_1938) IN (2,3,4,5,6,7,8) AND p.purchaseorderid IN ($purchaseorderid)";

                  if($showExported == 'false'){
                    $goods_query .= " AND exported_agnum = 0 ";
                  }         
                  $goods_query .= "GROUP BY sequence_no";

  $goods_result = $conn->query($goods_query);

  $goods_count = mysqli_num_rows($goods_result);              

 
  $goods = $xml->addChild('Goods'); // Grupėje saugomas visų prekių ir paslaugų pilnas aprašymas, kurios naudojamos nors viename pajamavimo dokumente siunčiamame XML faile.
  $goods->addAttribute('Count', $goods_count); // Laukas Count parodo įrašų kiekį.

  foreach($goods_result AS $row){
    $item = $goods->addChild('Item');
    $item->addAttribute('KOD', $row['goods_kodas']);      
    $item->addAttribute('SND_KOD', $row['storage']); 
    $item->addAttribute('PAVAD', $row['itemservice_tks_name']); 
    $item->addAttribute('CLASS', $row['class']); 
    $item->addAttribute('GRUPE', ''); 
    $item->addAttribute('POGRUPIS', ''); 
    $item->addAttribute('KATEG', ''); 
    $item->addAttribute('PAVAD1', ''); 
    $item->addAttribute('PAVAD2', ''); 
    $item->addAttribute('PAVAD3', ''); 
    $item->addAttribute('PAVAD4', ''); 
    $item->addAttribute('PAVAD5', ''); 
    $item->addAttribute('PAVAD6', ''); 
    $item->addAttribute('PAVAD7', ''); 
    $item->addAttribute('PAVAD8', ''); 
    $item->addAttribute('VNT', 'VNT'); 
    $item->addAttribute('VNT2', ''); 
    $item->addAttribute('SVORIS', '0'); 
    $item->addAttribute('SVORIS1', '0'); 
    $item->addAttribute('TURIS', '0'); 
    $item->addAttribute('VIETA', ''); 
    $item->addAttribute('METOD', '0'); // Apskaitos metodas:  0 – FIFO;  1 – LIFO;  2 – AVERAGE;
    $item->addAttribute('IPAK', '0'); 
    $item->addAttribute('KN0', $row['margin']); 
    $item->addAttribute('KN1', '0'); 
    $item->addAttribute('KN2', '0'); 
    $item->addAttribute('KN3', '0'); 
    $item->addAttribute('KN4', '0'); 
    $item->addAttribute('KN5', '0'); 
    $item->addAttribute('KNB', '0'); 
    $item->addAttribute('KNBVAL', 'EUR'); 
    $item->addAttribute('REZ', '0'); 
    $item->addAttribute('UZS', '0'); 
    $item->addAttribute('APMOKPVM', ($row['vat'] > 0 ? 'Y' : 'N')); 
    $item->addAttribute('PVM0', '0'); 
    $item->addAttribute('PVM', $row['vat_procent']); 
    $item->addAttribute('PVM2', '0'); 
    $item->addAttribute('PVM3', '0'); 
    $item->addAttribute('PVM4', '0'); 
    $item->addAttribute('PVM5', '0'); 
    $item->addAttribute('MOK0', '0'); 
    $item->addAttribute('MEMO', ''); 
    $item->addAttribute('F0', '0'); 
    $item->addAttribute('F1', '0'); 
    $item->addAttribute('F2', '0'); 
    $item->addAttribute('F3', '0'); 
    $item->addAttribute('F4', '0'); 
    $item->addAttribute('F5', '0'); 
    $item->addAttribute('F6', '0'); 
    $item->addAttribute('F7', '0'); 
    $item->addAttribute('F8', '0'); 
    $item->addAttribute('ACC1', ''); 
    $item->addAttribute('ACC2', ''); 
    $item->addAttribute('ACC3', ''); 
    $item->addAttribute('ACC4', ''); 
    $item->addAttribute('ACC5', ''); 
    $item->addAttribute('PVM_KOD', ''); 
    $item->addAttribute('POZYMIAI', '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'); 
  }     



  $query = "SELECT v.vendorname, vtiger_purchaseordercf.cf_1345  AS invoicedate, vtiger_purchaseordercf.cf_1355 AS purchaseorder_no,REPLACE(ROUND(p.subtotal,2),'.',',') AS subtotal, REPLACE(ROUND(p.total,2),'.',',') AS total,duedate,createdtime, p.purchaseorderid,p.vendorid, cf_1458 AS rkod,cf_1938 AS storage_code,IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) AS vat, REPLACE(ROUND((subtotal *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100),2),'.',',') AS vat_price,
                                            CASE 
                                              WHEN vtiger_purchaseordercf.cf_1610 != ''
                                              THEN 1
                                              ELSE 0
                                            END AS has_iban,cf_2060 AS kod
                                          FROM vtiger_purchaseorder p
                                          LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid	
                                          LEFT JOIN vtiger_vendorcf cfv ON cfv.vendorid=v.vendorid
                                          INNER JOIN vtiger_crmentity ON p.purchaseorderid = vtiger_crmentity.crmid 
                                          INNER JOIN vtiger_purchaseordercf ON p.purchaseorderid = vtiger_purchaseordercf.purchaseorderid 
                                          LEFT JOIN vtiger_inventoryproductrel inv ON inv.id=p.purchaseorderid
                                          LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=inv.cargo_wgt
                                          LEFT JOIN vtiger_itemservicecf itcf ON itcf.itemserviceid=its.itemserviceid
                                          LEFT JOIN vtiger_taxregions ON IF(inv.tax2 > 0, vtiger_taxregions.regionid=inv.tax2 ,vtiger_taxregions.regionid=p.region_id)  
                                          LEFT JOIN vtiger_accountscf a ON a.accountid=v.accountid
                                          WHERE vtiger_crmentity.deleted=0  AND p.purchaseorderid IN ($purchaseorderid) AND cf_1343 != 'Išankstinė' ";
                                          
                                          if($showExported == 'false'){
                                            $query_string .= " AND exported_agnum = 0 ";
                                          }
                           
                                    $query .=  "GROUP BY p.purchaseorderid ORDER BY p.purchaseorderid";
                               

  $invoices_query = $conn->query($query);                                            


  $invoices_count = mysqli_num_rows($invoices_query);

  $documents = $xml->addChild('Documents');  // Grupėje saugomi pardavimo dokumentai.
  $documents->addAttribute('Count', $invoices_count); // Laukas Count parodo dokumentų kiekį
  $documents->addAttribute('Type', '2'); // laukas Type visada lygus 4.


  $errors = array();

  $token = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

  foreach($invoices_query AS $row):

    if($row['total'] != 0 && !empty($row['kod'])){
   
      $conn->query("UPDATE vtiger_purchaseorder SET exported_agnum = 1 WHERE purchaseorderid = '".$row['purchaseorderid']."'"); 

      $rows_query = $conn->query("SELECT REPLACE(ROUND(listprice,2),'.',',') AS listprice, ROUND(quantity) AS quantity, IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) AS vat, 
                                  REPLACE(ROUND(((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100),2),'.',',') AS vat_price, itemservice_tks_accountingcode AS kodas
                                  FROM vtiger_inventoryproductrel inv
                                  LEFT JOIN vtiger_purchaseorder p ON p.purchaseorderid=inv.id
                                  LEFT JOIN vtiger_taxregions ON IF(inv.tax2 > 0, vtiger_taxregions.regionid=inv.tax2 ,vtiger_taxregions.regionid=p.region_id)
                                  LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=inv.cargo_wgt
                                  WHERE id = '".$row['purchaseorderid']."'");

      $rows_num =  mysqli_num_rows($rows_query);                     

      $item = $documents->addChild('Item');
      $item->addAttribute('Count', $rows_num);      
      $item->addAttribute('POZYMIAI', '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'); //*Būtinas
      $item->addAttribute('DATA', $row['invoicedate']); // Dokumento data. Datos formatas YYYY.MM.DD /*Būtinas
      $item->addAttribute('DATA_G', $row['invoicedate']); // Dokumento data. Datos formatas YYYY.MM.DD /*Būtinas
      $item->addAttribute('DOKNR', $row['purchaseorder_no']); // Dokumento numeris /*Būtinas
      $item->addAttribute('DOKNR2', $row['purchaseorder_no']); 
      $item->addAttribute('KL_KOD', $row['kod']); // Pirkėjo kodas AGNUM sistemoje /*Būtinas
      $item->addAttribute('SND_KOD', $row['storage_code']); // Sandėlio kodas AGNUM sistemoje

      $item->addAttribute('TR_TIP','1');
      $item->addAttribute('NUOL1','0');
      $item->addAttribute('NUOL2','0');
      $item->addAttribute('MT','0');
      $item->addAttribute('AKC','0');
      $item->addAttribute('PVM_KL','0');
      $item->addAttribute('KT','0');
      $item->addAttribute('PR','0');
      $item->addAttribute('TRANSP','0');
      $item->addAttribute('DRB','');
      $item->addAttribute('TERM','0');
      $item->addAttribute('APMSUM','0');
      $item->addAttribute('SANDORIS','');
      $item->addAttribute('PRISTSAL','');
      $item->addAttribute('TRANSPORTAS','');
      $item->addAttribute('SALISSIUNT','');
      $item->addAttribute('TR_VVAL','Y');
 

      $item->addAttribute('PVM', $row['vat_price']);
      $item->addAttribute('SUMAVISO', $row['subtotal']);
      $item->addAttribute('SUMAP', $row['subtotal']);
      $item->addAttribute('SKOLA', $row['total']);
      $item->addAttribute('SUMVISO', $row['subtotal']);
      $item->addAttribute('PVMPROC', $row['vat']);

      $item->addAttribute('VAL', 'EUR'); // Valiutos kodas /*Būtinas
      $item->addAttribute('KURS', '1'); // Valiutos kursas /*Būtinas     
      $item->addAttribute('REF_DOK_DATA', ""); // Tikslinamos sąskaitos data
      $item->addAttribute('REF_DOK_NR', ""); // Tikslinamos sąskaitos numeris 
      

      foreach($rows_query AS $line){
        $ro = $item->addChild('row');
        $ro->addAttribute('F5','');
        $ro->addAttribute('F4','');
        $ro->addAttribute('F3','');
        $ro->addAttribute('F2','');
        $ro->addAttribute('F1','');
        $ro->addAttribute('KOD', $line['kodas']);
        $ro->addAttribute('PVM_KOD', 'PVM'.$line['vat']);
        $ro->addAttribute('PVM', $line['vat_price']);
        $ro->addAttribute('AKC','0');
        $ro->addAttribute('MT','0');
        $ro->addAttribute('OBJ_KOD','');
        $ro->addAttribute('PART_PARKIN','0');
        $ro->addAttribute('APSKIRTIS','');
        $ro->addAttribute('KILMESSALIS','');
        $ro->addAttribute('D3','');
        $ro->addAttribute('D2','');
        $ro->addAttribute('D1','');
        $ro->addAttribute('PVM_PROC', $line['vat']);
        $ro->addAttribute('PRKKN', $line['listprice']);
        $ro->addAttribute('KIEKIS', $line['quantity']);
      }


    
    }else{
      $errors[] = $row['purchaseorder_no'];
    }

endforeach;

  setcookie("errors", "", time()-3600);
  if(count($errors) > 0){
    $error = implode(",", $errors);
    setcookie("errors", $error, time()+3600, "/","", 0);
    // header('location:/index.php?module=Exportinvoices&view=List');
  }

  $barcodes = $xml->addChild('Barcodes'); // Grupėje išvardinti visi grupėje Goods nurodytų prekių ir paslaugų barkodai
  $barcodes->addAttribute('Count', '0');
  $timestamp = time();

  header( "Content-Encoding: UTF-8" );
  header( "Content-Disposition: attachment; filename=pirkimai_agnum_$timestamp.xml");
  header( "Content-Type: text/xml; charset=UTF-8" );
  header( "Content-Description: File Transfer" );


  $dom = dom_import_simplexml($xml)->ownerDocument;
  $dom->formatOutput = true;
  echo $dom->saveXML();

