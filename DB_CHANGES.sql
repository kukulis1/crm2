CREATE TABLE `app_metrika_route_point_failure_reason` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `route_point_operation_id` int NULL,
  `reason` varchar(255) NULL
) ENGINE='InnoDB';

CREATE TABLE `app_metrika_route_point_additional_data` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `route_point_operation_id` int NULL,
  `loading_works` tinyint NULL,
  `idle` int NULL,
  `remarks` longtext NULL,
  `comments` text NULL
) ENGINE='InnoDB';

ALTER TABLE `app_metrika_route_point_failure_reason`
ADD `operation_type_id` char(1) COLLATE 'latin1_swedish_ci' NULL;

CREATE TABLE `app_metrika_failure_reasons` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `external_reason_id` int unsigned NOT NULL,
  `reason` varchar(255) NOT NULL
) ENGINE='InnoDB';

ALTER TABLE `app_metrika_route_point_failure_reason`
CHANGE `reason` `failure_reason_id` int(11) NULL AFTER `route_point_operation_id`;

ALTER TABLE `app_metrika_failure_reasons`
CHANGE `reason` `reason` text COLLATE 'utf8mb4_general_ci' NOT NULL AFTER `external_reason_id`;

INSERT INTO `app_metrika_failure_reasons` (`id`, `external_reason_id`, `reason`) VALUES
(1,	1,	'Neteisingas adresas'),
(2,	2,	'Negalimas privažiavimas'),
(3,	3,	'Neparuoštas krovinis'),
(4,	4,	'Krovinis netinkamai supakuotas transportavimui'),
(5,	5,	'Krovinis neatitinka užsakymo  (per didelis svoris, arba tūris)'),
(6,	6,	'Nebuvo vietoje gavėjo/siuntėjo, arba nėra kontaktinio asmens'),
(7,	7,	'Tuščias važiavimas');

CREATE TABLE `app_metrika_route_point_failed_reason_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `row_id` int NOT NULL
) ENGINE='InnoDB';

CREATE TABLE `app_metrika_route_point_additional_data_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `row_id` int NOT NULL
) ENGINE='InnoDB';

CREATE TABLE vtiger_self_service_messages (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `customer_id` varchar(10),
  `message` varchar(255),
  `period` varchar(255),
  `date` timestamp
);


ALTER TABLE `vtiger_send_invoice_for_client` 
 ADD COLUMN  `status` varchar(100) DEFAULT NULL,
 ADD COLUMN `invoice_no` varchar(100) DEFAULT NULL,  
 ADD COLUMN `employee` int(11) DEFAULT NULL, 
 ADD COLUMN `receive_date` datetime DEFAULT NULL,
 ADD COLUMN `url` varchar(100) DEFAULT NULL,
 ADD COLUMN `expired` datetime DEFAULT NULL
 

 ALTER TABLE vtiger_salesorder ADD COLUMN created_person varchar(100);

 INSERT INTO `app_other_settings` (`id`, `title`, `value`) VALUES (NULL, 'LBL_LOAD_WORK_PRICE', '0.04'), (NULL, 'LBL_IDLE_PRICE', '17');

  INSERT INTO `app_other_settings` (`id`, `title`, `value`) VALUES (NULL, 'LBL_SEND_STICKERS_TEXT', 'Laba diena, <br /> Siunčiame jums lipdukus, juos galite rasti pridėtus prie laiško');

CREATE TABLE `app_standing_salesorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `execute_type` int(11) DEFAULT NULL,
  `execute` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `execute_time` time DEFAULT NULL,
  `order_type` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `load_delay` int(11) DEFAULT NULL,
  `load_time_from` time DEFAULT NULL,
  `load_time_to` time DEFAULT NULL,
  `unload_delay` int(11) DEFAULT NULL,
  `unload_time_from` time DEFAULT NULL,
  `unload_time_to` time DEFAULT NULL,
  `load_company` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `unload_company` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `load_contact` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `unload_contact` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `load_phone` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `unload_phone` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `bill_street` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `ship_street` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `bill_city` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `ship_city` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `bill_code` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `ship_code` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `bill_country` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `ship_country` varchar(100) COLLATE utf8_swedish_ci DEFAULT NULL,
  `pricebook_price` int(11) DEFAULT NULL,
  `agreed_price` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_person` INT,
  `createdtime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
);


CREATE TABLE `app_standing_salesorders_cargo` (
  `id` int(11) DEFAULT NULL,
  `sequence_no` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `cargo_wgt` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `cargo_length` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `cargo_width` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `cargo_height` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `measure` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL
);


CREATE TABLE app_executed_templates (
  id int,
  executed_date varchar(5)
);

CREATE TABLE `app_bank_balances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(255) NOT NULL,
  `xml_file_id` varchar(255) NOT NULL,
  `balance` decimal(9,2) unsigned NOT NULL,
  `xml_file_date` date NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;