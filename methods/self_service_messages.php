<?php

include "../config.inc.php";
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
  if($_REQUEST['password'] == 'raktas' && $_REQUEST['user'] == 123){

    try{
      $conn = new PDO("mysql:host=" . $dbconfig['db_server'] . ";dbname=" . $dbconfig['db_name'], $dbconfig['db_username'], $dbconfig['db_password']);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $conn->exec("set names utf8");

      $query = "SELECT accountname,message ,s.customer_id
                  FROM vtiger_self_service_messages s 
                  LEFT JOIN vtiger_account a ON a.customer_id=s.customer_id 
                  WHERE CURDATE() BETWEEN SUBSTRING_INDEX(`period`, ',', 1) AND SUBSTRING_INDEX(SUBSTRING_INDEX(`period`, ',', 3), ',', -1) ";

      if($_REQUEST['customer_id']){
        $query .= " AND s.customer_id = '".$_REQUEST['customer_id']."'";
      }

      $query2 = "SELECT `message`
                  FROM vtiger_self_service_messages                
                  WHERE customer_id = '---' AND (CURDATE() BETWEEN SUBSTRING_INDEX(`period`, ',', 1) AND SUBSTRING_INDEX(SUBSTRING_INDEX(`period`, ',', 3), ',', -1))";

      $stmt = $conn->prepare($query);
      $stmt->execute();
      $stmt->setFetchMode(PDO::FETCH_ASSOC); 

      $stmt2 = $conn->prepare($query2);
      $stmt2->execute();
      $result = $stmt2->fetch(PDO::FETCH_ASSOC);
      $rowCount = $stmt->rowCount();
    

      $main_message = $result['message'];

      $data = array();
  
      foreach($stmt as $row){
        if($row['customer_id'] == "---"){
          $data['main']['message'] = $row['message'];        
        }else{
          if($main_message){
            $data['main']['message'] = $main_message;    
          }
          $data['clients'][] = $row;
        }
      
      }  
      
      if(!$rowCount){
        $data['main']['message'] = $main_message;  
      }

      http_response_code(200);
      echo json_encode(array("message" => $data));


    }catch(PDOException $exception){
      echo "Connection error: " . $exception->getMessage();
    }
  }else{
    http_response_code(503);
    echo json_encode(array("message" => "Neteisingas vartotojas arba slaptažodis"));
  }
}else{
  http_response_code(404);
}