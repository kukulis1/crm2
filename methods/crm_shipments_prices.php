<?php

include "../config.inc.php";
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
  if($_REQUEST['password'] == 'raktas' && $_REQUEST['user'] == 123){

    if($_REQUEST['date_from']){

      try{
        $conn = new PDO("mysql:host=" . $dbconfig['db_server'] . ";dbname=" . $dbconfig['db_name'], $dbconfig['db_username'], $dbconfig['db_password']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->exec("set names utf8");

        $query = "SELECT DISTINCT s.external_order_id AS shipment_id,shipment_code, IF(total,round(total,2),0) AS price, IF(cf_1376, cf_1376,0) AS price_aggred , REPLACE(IF(i.listprice,SUM(ROUND(i.listprice,2)),0),',','.') AS invoice_price
                  FROM vtiger_salesorder s
                  JOIN vtiger_salesordercf c ON c.salesorderid=s.salesorderid
                  LEFT JOIN vtiger_inventoryproductrel i ON i.order_id=s.salesorderid
                  INNER JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                  WHERE (DATE_FORMAT(e.createdtime,'%Y-%m-%d') BETWEEN ? AND ?) AND (s.external_order_id != '' OR s.external_order_id IS NOT NULL) AND deleted = 0 AND setype='SalesOrder'
                  GROUP BY s.salesorderid";

        if(empty($_REQUEST['date_to'])){
          $date_to = date("Y-m-d");
        }else{
          $date_to = $_REQUEST['date_to'];
        }         

    

        $stmt = $conn->prepare($query);
        $stmt->execute(array($_REQUEST['date_from'],$date_to));
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $rowCount = $stmt->rowCount();
      
        $data = [];
    
        foreach($stmt as $row){       
            $data[] = $row;       
        }  
        
  
        http_response_code(200);
        echo json_encode(['state' => 'SUCCESS','total' => $rowCount,"orders" => $data]);

      }catch(PDOException $exception){
        http_response_code(503);
        echo json_encode(['state' => 'FAIL',"order" => [$exception->getMessage()]]);
        // echo "Connection error: " . $exception->getMessage();
      }
    }else{
      http_response_code(503);
      echo json_encode(array("error_message" => "Nenurodyta data nuo"));
    }
  }else{
    http_response_code(503);
    echo json_encode(array("error_message" => "Neteisingas vartotojas arba slaptažodis"));
  }
}else{
  http_response_code(404);
}