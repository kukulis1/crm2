<?php

include "../config.inc.php";
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if(empty($_SERVER['HTTP_AUTHORIZATION']) || $_SERVER['HTTP_AUTHORIZATION'] !== 'Basic '.base64_encode($API_USER.':'.$API_PASS)){
		http_response_code(401);
		echo json_encode([
			'status' => 401,
			'errors' => [
				'bad_credentials' => 'Bad credentials' 
			],
		]);
		return;
	} else {

		if($_REQUEST['customer_id']){

			try{

				$db = new PDO("mysql:host=" . $dbconfig['db_server'] . ";dbname=" . $dbconfig['db_name'], $dbconfig['db_username'], $dbconfig['db_password']);
				$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
				$db->exec("set names utf8");	
				
				if($_REQUEST['invoiceid']){
					$query = "SELECT shipment_code, productid, bill_ads, ship_ads, name AS service_name, note, costcenter_tks_cost AS costcenter, cargo_measure, cargo_wgt, m3, listprice, vtiger_inventoryproductrel.service
							FROM vtiger_invoice 							
							JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid
							JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_invoice.invoiceid
							LEFT JOIN vtiger_salesorder ON 	vtiger_salesorder.salesorderid=vtiger_inventoryproductrel.order_id
							LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=vtiger_inventoryproductrel.costcenter
							LEFT JOIN app_services_type ON app_services_type.id=vtiger_inventoryproductrel.service				
							INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid AND setype = 'Invoice'
							WHERE vtiger_account.customer_id = :customer_id AND vtiger_invoice.invoiceid = :invoiceid AND deleted = 0
							ORDER BY vtiger_inventoryproductrel.sequence_no";

					$stmt = $db->prepare($query);
					$stmt->execute([
						':customer_id' => $_REQUEST['customer_id'],
						':invoiceid' => $_REQUEST['invoiceid']
					]);	

					$data = [];
					foreach ($stmt as $details) {
						$data[] = $details;						
					}

				}else{
				
					
					$query = "SELECT vtiger_invoice.invoiceid, vtiger_invoice.invoice_no, invoicedate, duedate, subtotal, total, s_h_amount, hash, vtiger_send_invoice_for_client.deleted 
							FROM vtiger_invoice 
							JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
							JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid
							INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid AND setype = 'Invoice'
							INNER JOIN vtiger_send_invoice_for_client ON vtiger_send_invoice_for_client.invoiceid=vtiger_invoice.invoiceid
							WHERE vtiger_account.customer_id = :customer_id AND cf_1277 = 'Debetinė' AND vtiger_crmentity.deleted = 0
							ORDER BY vtiger_invoice.invoiceid DESC";

					$stmt = $db->prepare($query);
					$stmt->execute([
						':customer_id' => $_REQUEST['customer_id']
					]);	
					
					
					$data = [];
					$invoiceids = [];
					foreach ($stmt as $invoice) {
						$invoiceids[] = $invoice->invoiceid;
						$data[$invoice->invoiceid]['invoiceid'] = $invoice->invoiceid;
						$data[$invoice->invoiceid]['hash'] = $invoice->hash;
						$data[$invoice->invoiceid]['deleted'] = $invoice->deleted;
						$data[$invoice->invoiceid]['invoice_no'] = $invoice->invoice_no;
						$data[$invoice->invoiceid]['invoicedate'] = $invoice->invoicedate;
						$data[$invoice->invoiceid]['duedate'] = $invoice->duedate;
						$data[$invoice->invoiceid]['subtotal'] = round($invoice->subtotal, 2);
						$data[$invoice->invoiceid]['total'] = round($invoice->total, 2);
						$data[$invoice->invoiceid]['s_h_amount'] = round($invoice->s_h_amount, 2);
					}

					$imploded_invoiceids = implode(',', $invoiceids);

					$payments_query = "SELECT vtiger_invoice.invoiceid, SUM(vtiger_debts.debts_tks_suma) AS payed
									FROM vtiger_invoice 
									JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
									LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'	
									LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
									INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND vtiger_crmentity.setype = 'Invoice'  
									WHERE vtiger_invoice.invoiceid IN ($imploded_invoiceids)
									GROUP BY vtiger_invoice.invoiceid";

					$get_payed = $db->prepare($payments_query);
					$get_payed->execute();	
					
					foreach ($get_payed as $info) {
						$data[$info->invoiceid]['payed'] = ($info->payed ?:0);
					}

				}

		
				http_response_code(200);
				echo json_encode(['state' => 'SUCCESS', 'response' => $data]);

			}catch(PDOException $exception){
				http_response_code(503);
				echo json_encode(['state' => 'FAIL', 'response' => [$exception->getMessage()]]);				
			}
		}else{
			http_response_code(503);
			echo json_encode(array("error_message" => "Nenurodytas kliento id"));
		}

  	}
	  
}else{
  	http_response_code(404);
}