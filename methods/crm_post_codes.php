<?php

include "../config.inc.php";
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
  if(empty($_SERVER['HTTP_AUTHORIZATION']) || $_SERVER['HTTP_AUTHORIZATION'] !== 'Basic '.base64_encode($API_USER.':'.$API_PASS)){
    http_response_code(401);
    echo json_encode([
        'status' => 401,
        'errors' => [
            'bad_credentials' => 'Bad credentials' 
        ],
    ]);
    return;
  } else {

    if($_REQUEST['type']){

      try{
        $conn = new PDO("mysql:host=" . $dbconfig['db_server'] . ";dbname=" . $dbconfig['db_name'], $dbconfig['db_username'], $dbconfig['db_password']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->exec("set names utf8");

        $keyword = trim($_REQUEST['keyword']);
        
        if($_REQUEST['type'] == 'code'){          
          $query = "SELECT DISTINCT trim(post_code) AS label, trim(post_code) AS `value`, trim(city) AS city, trim(zone_customer) AS zone_customer FROM crm_post_codes WHERE trim(post_code) LIKE '$keyword%' LIMIT 500";

        }else if($_REQUEST['type'] == 'address'){  

          $exploded_keyword = explode(' ', $keyword);
          $exploded_keyword = array_unique($exploded_keyword);

          $query = "SELECT DISTINCT CONCAT(trim(street),' ', trim(house),', ', trim(city),', ', trim(post_code)) AS label, 
                                    CONCAT(trim(street),' ', trim(house)) AS `value` 
                    FROM crm_post_codes WHERE "; 

          foreach ($exploded_keyword as $keyword) { 
            $query .=  "CONCAT(trim(street),' ', trim(house),', ', trim(city),', ', trim(post_code)) LIKE '%$keyword%' AND ";
          }
          $query = rtrim($query, 'AND ');
          $query .= " LIMIT 10";

        }else if($_REQUEST['type'] == 'city'){          
          $query = "SELECT DISTINCT trim(city) AS city, trim(zone_customer) AS zone_customer FROM crm_post_codes WHERE trim(city) LIKE '$keyword%' OR trim(zone_customer) LIKE '$keyword%' LIMIT 20";
        
        }else if($_REQUEST['type'] == 'code_cities'){  
          $query = "SELECT DISTINCT trim(city) AS city, trim(zone_customer) AS zone_customer FROM crm_post_codes WHERE trim(post_code) LIKE '$keyword%' LIMIT 500";

        }
    
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);       
      
        $data = [];    
        $cities_data = [];    

          foreach($stmt as $row){     
            if($_REQUEST['type'] == 'code'){   
              $data[] = ['label' => $row['label'], 'value' => $row['value'], 'city' => $row['city'], 'city2' => $row['zone_customer']];    

            }elseif($_REQUEST['type'] == 'city' || $_REQUEST['type'] == 'code_cities'){            
              if(!empty($row['city'])){              
                $cities_data[$row['city']] = $row['city'];
              }
              
              if(!empty($row['zone_customer'])){                      
                $cities_data[$row['zone_customer']] = $row['zone_customer'];
              }

            }elseif($_REQUEST['type'] == 'address'){
              $data[] = ['label' => $row['label'], 'value' => $row['value']];
              $data[] = ['label' => $row['zone_customer'], 'value' => $row['zone_customer']];
            }
          }  


          if($_REQUEST['type'] == 'city'){  
            foreach($cities_data as $city){     
              $data[] = ['label' => $city, 'value' => $city];
            }
          }else if ($_REQUEST['type'] == 'code_cities'){
            foreach($cities_data as $city){     
              $data[] = $city;
            }
          }
        
  
        http_response_code(200);
        echo json_encode(['state' => 'SUCCESS', "response" => $data]);

      }catch(PDOException $exception){
        http_response_code(503);
        echo json_encode(['state' => 'FAIL',"order" => [$exception->getMessage()]]);
        // echo "Connection error: " . $exception->getMessage();
      }
    }else{
      http_response_code(503);
      echo json_encode(array("error_message" => "Nenurodytas tipas"));
    }

  }
}else{
  http_response_code(404);
}