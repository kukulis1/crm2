<?php
        
function syncSites($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
    require 'metrika.php';
    
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';

    $endpointUrl = $url;
	
    $logfile = 'sync_sites.log';
    WebserviceLog($params, $logfile);      

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            WebserviceLog($output, $logfile);
            
        } else {         
            
            $sth = $dbh->prepare('SELECT accesskey, user_name FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            
            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);
            
            $dbh->beginTransaction();

                    $crm_site_id = $params['crm_site_id'];
                    $site_status = $params['status'];
                    $not_visit_reason = $params['not_visit_reason'];
                    $comment = $params['comment'];
                    $note = $not_visit_reason . ' ' . $comment;
                    
                    $date = date("Y-m-d H:i:s");
                    
                    if(!empty($not_visit_reason)){
                        $sth = $dbh->prepare("UPDATE vtiger_activity SET eventstatus = ?, priority = ? WHERE activityid = ?");
                        $sth->execute(array($site_status, $not_visit_reason, $crm_site_id));           
                    } else {
                        $sth = $dbh->prepare("UPDATE vtiger_activity SET eventstatus = ? WHERE activityid = ?");
                        $sth->execute(array($site_status, $crm_site_id)); 
                    }
                    
                    $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ?, description = ? WHERE crmid = ?');
                    $sth->execute(array($date, $comment, $crm_site_id));  
                    
            $dbh->commit();	
            
            if($site_status == 'Completed') {
                Save_Site_Metrika($dbh, $crm_site_id, $note, $not_visit_reason);
            }
            
            $output = array(
                'status' => 200,
                'response' => array('crm_site_id' => $crm_site_id)
            );
           
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_sites.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
