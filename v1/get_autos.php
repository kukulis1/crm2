<?php

function getAutos($params) {

    global $config;
    require 'appc_utils.php';

    $logfile = 'get_autos.log';
    webservicelog($params, $logfile);

    $result_data = array();

    //$result_data = array('autos' => array());
    
    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {
            
            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {

            $sql_params = array();

            $user_id = $params['user_id'];
            
                $sql = "SELECT s.*
                                        FROM vtiger_assets s
                                        JOIN vtiger_crmentity e ON e.crmid = s.assetsid 									
                                        WHERE e.deleted = 0 AND (e.smownerid = ? OR s.user_id = ?) ORDER BY s.assetsid DESC";           

                array_push($sql_params, $user_id, $user_id);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);
            
                $row = $sth->fetch(); 
                
                $tasks['crm_auto_id'] = intval($row['assetsid']);
		$tasks['auto_id'] = $row['id'];            
		$tasks['auto_number'] = $row['assetname'];
                
                if($row['serialnumber'] != NULL && !empty($row['serialnumber']))                  
                $tasks['part_number'] = $row['serialnumber'];              

                $sth2 = $dbh->prepare('SELECT s.* FROM vtiger_service s
                                       JOIN vtiger_crmentity c ON c.crmid = s.serviceid 
                                       WHERE c.deleted = 0 AND s.auto_number = ? ORDER BY s.start_date DESC LIMIT 1');
                $sth2->setFetchMode(PDO::FETCH_ASSOC);
                $sth2->execute(array($tasks['auto_number']));

                $row2 = $sth2->fetch();  
                
                if($row2['odometer'] != NULL && !empty($row2['odometer']))                  
                $tasks['odometer'] = $row2['odometer'];  
                
                if($row2['odometer']!= NULL && !empty($row2['odometer']))                  
                $tasks['odometer_date'] = $row2['start_date'];  
                
                $result_data = $tasks;
                //$result_data['autos'][] = $tasks;
                
                        
            $output = array(
                'status' => 200,
                'response' => $result_data
            );
            
                $logfile = 'get_autos.log';
                webservicelog($output, $logfile);
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );
        
        $logfile = 'get_autos.log';
        webservicelog($output, $logfile);
    }
    return $output;
}
