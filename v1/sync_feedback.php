<?php
        
function syncFeedback($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
    
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';

    $endpointUrl = $url;
	
    $logfile = 'sync_feedback.log';
    WebserviceLog($params, $logfile);      

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            WebserviceLog($output, $logfile);
            
        } else {         
            
            $sth = $dbh->prepare('SELECT accesskey, user_name FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            
            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);
            
            $dbh->beginTransaction();

                    $crm_order_id = $params['crm_order_id'];
                    $feedback = $params['feedback'];
                
                    $date = date("Y-m-d H:i:s");
                    
                    $sth = $dbh->prepare("UPDATE vtiger_salesorder SET feedback = ? WHERE salesorderid = ?");
                    $sth->execute(array($feedback, $crm_order_id));           
                    
                    $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
                    $sth->execute(array($date, $crm_order_id));  
                    
            $dbh->commit();		
            
            $output = array(
                'status' => 200,
                'response' => array('crm_order_id' => $crm_order_id)
            );
           
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_feedback.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
