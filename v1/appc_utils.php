<?php

function guid() {

    if (function_exists('com_create_guid')) {
        return str_replace('-', '', trim(com_create_guid(), '{}'));
    } else {
        mt_srand((double) microtime() * 10000); 
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8) .
                substr($charid, 8, 4) .
                substr($charid, 12, 4) .
                substr($charid, 16, 4) .
                substr($charid, 20, 12);
        return $uuid;
    }
}

//------------------------------------------------------------------
//------------------------------------------------------------------

function account_authorization($dbh, $params) {

        // Check if user exists
        $sth = $dbh->prepare('SELECT u.* FROM vtiger_users u WHERE u.token = ? AND u.id = ?');
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($params['token'], $params['user_id']));

        if ($sth->rowCount() == 0) {

            $output = array(
                'status' => 401,
                'response' => array(
                    'type' => 'AUTHORIZATION',
                    'message' => 'Not logged in / Wrong password or username / Token expired'
                )
            );
            
        return $output;
    }

    return NULL;
}

//------------------------------------------------------------------
//------------------------------------------------------------------

// function insert_entity($dbh, $setype, $entity_id, $user_id, $description, $created_modified_date, $created_modified_date) {
function insert_entity($dbh, $setype, $entity_id, $user_id, $description, $created_modified_date) {


    // Insert into vtiger_crmentity 
    $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($entity_id, $user_id, $user_id, $setype, $description, $created_modified_date, $created_modified_date);
    $sth->execute($sql_params);

    // Update sequence values
    $sth = $dbh->prepare('UPDATE vtiger_crmentity_seq SET id = ?');
    $sth->execute(array($entity_id));
}

//------------------------------------------------------------------
//------------------------------------------------------------------

function webservicelog($log_variable, $logfile) {

	//echo ('Vienas');

    global $config;        
	
    $log_json = '--------------------------------' . date("Y-m-d H:i:s") . '--------------------------------' . "\n" . json_encode($log_variable) . "\n" . "\n";


	//$error_lines = $sth->errorInfo();
	
    file_put_contents($config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, $log_json, FILE_APPEND);
	
    if (filesize($config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile) > 1024 * 1024 * 10) {
        copy($config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, 
             $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('Y-m-d_H-i-s') . '_' . $logfile);
        unlink($config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile);
    }
}

//------------------------------------------------------------------
//------------------------------------------------------------------

function WorkingHours($date){ 
            
                date_default_timezone_set('Europe/Vilnius');

                $monsta = "07:00";
                $monend = "16:00";
                $tuessta = "07:00";
                $tuesend = "16:00";
                $wedssta = "07:00";
                $wedsend = "16:00";
                $thurssta = "07:00";
                $thursend = "16:00";
                $frista = "07:00";
                $friend = "14:45";
                $satsta = "07:00";
                $satend = "16:00";
                $sunsta = "07:00";
                $sunend = "16:00";

                $storeSchedule = array(
                    'Mon' => array("$monsta" => "$monend"),
                    'Tue' => array("$tuessta" => "$tuesend"),
                    'Wed' => array("$wedssta" => "$wedsend"),
                    'Thu' => array("$thurssta" => "$thursend"),
                    'Fri' => array("$frista" => "$friend")
                );

                // default time zone
                //$timestamp = time() - 18000;
                $timestamp = strtotime($date); 

                // default status
                $status = 'Closed';

                // loop through time ranges for current day
                foreach ($storeSchedule[date('D', $timestamp)] as $startTime => $endTime) {

                // create time objects from start/end times
                $startTime = strtotime($startTime);
                $endTime = strtotime($endTime);

                // check if current time is within a range
                if ($startTime <= $timestamp && $timestamp <= $endTime ) {
                    $status = 'Open';

                    }
                }   

                return $status;

            
        } 
      
//------------------------------------------------------------------
//------------------------------------------------------------------        
        
function AddDeadline($givenDate, $hours){
                $range = (ceil($hours/7)*120);
                $cnt=1;
                $days = array();
                $goodhours = array();
                $days[] = date("Y-m-d", strtotime($givenDate));
                foreach(range(1,$range) as $num){

                        $datetime = date("Y-m-d H:i:s", strtotime('+'.$num.' hour',strtotime($givenDate)));
                        $date = date("Y-m-d", strtotime('+'.$num.' hour',strtotime($givenDate)));
                        $time = date("Hi", strtotime('+'.$num.' hour',strtotime($givenDate)));
                        $day = date("D", strtotime('+'.$num.' hour', strtotime($givenDate)));
                        if($day == 'Fri' && $time >= 800 && $time <= 1700){

                                if(!in_array($date, $days)){
                                        $days[] = $date;
                                }else{
                                        $goodhours[$cnt] = $datetime;

                                        if($cnt >= $hours && array_key_exists($hours,$goodhours)){
                                                return $goodhours[$hours];
                                                break;
                                        };

                                        $cnt++;
                                }
                        } elseif ($day != 'Sat' && $day != 'Sun' && $day != 'Fri' && $time >= 800 && $time <= 1700){
                                if(!in_array($date, $days)){
                                        $days[] = $date;
                                }else{
                                        $goodhours[$cnt] = $datetime;

                                        if($cnt >= $hours && array_key_exists($hours,$goodhours)){
                                                return $goodhours[$hours];
                                                break;
                                        };

                                        $cnt++;
                                }  
                        }


                }
 }

//------------------------------------------------------------------
//------------------------------------------------------------------

function ticketStatus($str) {

    if($str == 'Open') {   
        $status = 'Naujas';
    }
    
    elseif($str == 'Accepted') {
        $status = 'Patvirtintas';
    }    
    
    elseif($str == 'In Progress') {
        $status = 'Vykdomas';
    }
    
    elseif($str == 'On Hold') {
        $status = 'Sustabdytas';
    }   
    
    elseif($str == 'Resolved') {
        $status = 'Išspręstas';
        
    }else {
      $status = $str; 
    }
    
        return $status;
}

