<?php

require_once 'vtwsclib/Vtiger/WSClient.php';
$site_URL  = 'https://gelsauga.saleseyes.com';//Tavo URL
$endpointUrl = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';

$userName = "admin";
$userKey = "ybet6C5HWRgriH";

//Login to WS library
$httpc = new Vtiger_WSClient($endpointUrl);
$result = $httpc->doLogin($userName, $userKey);
if(!$result) echo 'Login WS Failed';

//Register Object
/*$object_name = 'Naujas demo objektas';
$customer_id = 928808;
$contact = 'Jonas Jonaits';
$phone = '37069460956';
$email = 'jonas@gmail.com';
$address = 'Studentų g. 67, Kaunas';
$inventoriaus_nr = '123';
$unikalus_nr = '1';
$user_id = 88;

$values = array(
"firstname" => $object_name,
"lastname" => $contact, 
"account_id" => "11x$customer_id",
"phone" => $phone,
"email" => $email, 
"mailingstreet" => $address,   
"cf_877" => $unikalus_nr,  
"cf_879" => $inventoriaus_nr,    
"assigned_user_id" => "19x$user_id",
"created_user_id" => "19x$user_id");
    
$resultItem = $httpc->doCreate("Contacts", $values);

$wasError = $httpc->lastError();
if($wasError) {
print_r($httpc->lastError());
} else {
print_r($resultItem);
}*/

//Register Problem
$object_name = 'Naujas demo objektas';
$customer_id = 928808;
$object_id = 932335;
$contact = 'Jonas Jonaits';
$phone = '37069460956';
$email = 'jonas@gmail.com';
$problem_type = 'Gedimas';
$problem = 'Sugedo kažkas';
$service = 'Pastatų ir inžinerinių sistemų priežiūra';
$subservice = 'Bendrastatybinių darbų paslauga';
$work_hours = 24;
$not_work_hours = 7;
$crash = 2;
$user_id = 88;

$values = array(
"ticketstatus" => "Open",
"parent_id" => "11x$customer_id",
"contact_id" => "12x$object_id",
"ticketseverities" => $problem_type,
"ticketcategories" => "Kita",
"ticketpriorities" => $service,    
"contact" => $contact,
"phone" => $phone,         
"email" => $email,   
"cf_873" => 'Taip',    
"task_type_id" => $subservice,    
"description" => $problem, 
"hours" => $work_hours, 
"days" => $not_work_hours, 
"crash" => $crash,     
"assigned_user_id" => "19x$user_id",
"created_user_id" => "19x$user_id");
    
$resultItem = $httpc->doCreate("HelpDesk", $values);

$wasError = $httpc->lastError();
if($wasError) {
print_r($httpc->lastError());
} else {
print_r($resultItem);
}

//Created problem ID
$ticket_id = next(explode("x", $resultItem['id']));

//Register Comment
$comment = 'Testuojame';
$user_id = 88;

$values = array(
"related_to" => "17x$ticket_id",
"commentcontent" => $comment,
"assigned_user_id" => "19x$user_id",
"created_user_id" => "19x$user_id");
    
$resultItem = $httpc->doCreate("ModComments", $values);

$wasError = $httpc->lastError();
if($wasError) {
print_r($httpc->lastError());
} else {
print_r($resultItem);
}

?>