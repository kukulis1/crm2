<?php
        
function syncRoutes($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
    require 'metrika.php';
    
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';

    $endpointUrl = $url;
	
    $logfile = 'sync_routes.log';
    WebserviceLog($params, $logfile);      

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            WebserviceLog($output, $logfile);
            
        } else {         
            
            $sth = $dbh->prepare('SELECT accesskey, user_name, type, title FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            $type = $row['type'];            
            $code = $row['title'];   

            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);
            
            $dbh->beginTransaction();

                    $crm_route_id = $params['crm_route_id'];
                    $route_status = $params['status'];
                    $user_id = $params['user_id'];
                    $route_default_user = 26;
                    
                    $date = date("Y-m-d H:i:s");
                    
                        $sth = $dbh->prepare('SELECT driver1_id, smownerid FROM vtiger_troubletickets, vtiger_crmentity WHERE ticketid = crmid AND deleted = 0 AND ticketid = ?');
                        $sth->execute(array($crm_route_id));
                        
                        $row = $sth->fetch();                    
                        $driver_id = $row['driver1_id']; 
                        $smownerid = $row['smownerid'];                              
    
                    if($type == 1) {                        
                        $available = 1;    

                        $sth = $dbh->prepare("UPDATE vtiger_troubletickets SET status = ? WHERE ticketid = ?");
                        $sth->execute(array($route_status, $crm_route_id));           

                        $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ?, smownerid = ? WHERE crmid = ?');
                        $sth->execute(array($date, $user_id, $crm_route_id));                        
                        
                    } elseif ($type == 0) { 
                 
                        $available = 0;
                        
                            if($smownerid == $route_default_user) {
                                $available = 1; 
                                
                                $sth = $dbh->prepare("UPDATE vtiger_troubletickets SET status = ?, driver1_id = ? WHERE ticketid = ?");
                                $sth->execute(array($route_status, $code, $crm_route_id));           

                                $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ?, smownerid = ? WHERE crmid = ?');
                                $sth->execute(array($date, $user_id, $crm_route_id));                          
                            } elseif ($smownerid == $user_id) {
                                $available = 1; 
                                
                                $sth = $dbh->prepare("UPDATE vtiger_troubletickets SET status = ? WHERE ticketid = ?");
                                $sth->execute(array($route_status, $crm_route_id));           

                                $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ?, smownerid = ? WHERE crmid = ?');
                                $sth->execute(array($date, $user_id, $crm_route_id));           
                            } 
 
                        }                    
                    
            $dbh->commit();		
            
            Save_Route_Metrika($dbh, $crm_route_id, $vechicle_id);
            
            $output = array(
                'status' => 200,
                'response' => array('crm_route_id' => $crm_route_id,
                                    'available' => $available)
            );
           
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_routes.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
