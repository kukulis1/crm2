<?php

function last_entity_record($dbh) {

    $sth = $dbh->query('SELECT * FROM vtiger_crmentity_seq');

    while ($row = $sth->fetch()) {
    $crmid = $row['id'] + 1;
    }   
    
    return $crmid;
}

function insert_entity($dbh, $setype, $entity_id, $user_id, $description, $label, $date) {

    // Insert into vtiger_crmentity 
    $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date);
    $sth->execute($sql_params);

    // Update sequence values
    $sth = $dbh->prepare('UPDATE vtiger_crmentity_seq SET id = ?');
    $sth->execute(array($entity_id));
}


function update_entity($dbh, $entity_id, $user_id, $description, $label, $date) {

    $sth = $dbh->prepare('UPDATE vtiger_crmentity SET smownerid = ?, description = ?, label = ?, modifiedtime = ? WHERE crmid = ?');
    $sql_params = array($user_id, $description, $label, $date, $entity_id);
    $sth->execute($sql_params);

}


function insert_customer_address_billing($dbh, $accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name) {

    $sth = $dbh->prepare('INSERT INTO vtiger_accountbillads (accountaddressid, 
                                                                    bill_street,
                                                                    bill_code,
                                                                    location_id,
                                                                    bill_city, 
                                                                    bill_country,
                                                                    name) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name);
    $sth->execute($sql_params);
            
    $sth = $dbh->prepare('INSERT INTO vtiger_accountscf (accountid) VALUES (?)'); 
    $sql_params = array($accountaddressid);
    $sth->execute($sql_params);
}

function insert_customer_address_shipping($dbh, $accountaddressid, $ship_sreet, $ship_code, $location_id, $ship_city, $ship_country, $name) {

    $sth = $dbh->prepare('INSERT INTO vtiger_accountshipads (accountaddressid, 
                                                                    ship_street,
                                                                    ship_code,
                                                                    location_id,
                                                                    ship_city, 
                                                                    ship_country,
                                                                    name) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($accountaddressid, $ship_sreet, $ship_code, $location_id, $ship_city, $ship_country, $name);
    $sth->execute($sql_params);
           
}


function update_customer_address_billing($dbh, $accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name) {

    $sth = $dbh->prepare('UPDATE vtiger_accountbillads SET bill_street = ?, bill_code = ?, location_id = ?, bill_city = ?, bill_country = ?, name = ? WHERE accountaddressid = ?');
    $sql_params = array($bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name, $accountaddressid);
    $sth->execute($sql_params);

}

function update_customer_address_shipping($dbh, $accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name) {

    $sth = $dbh->prepare('UPDATE vtiger_accountshipads SET ship_street = ?, ship_code = ?, location_id = ?, ship_city = ?, ship_country = ?, name = ? WHERE accountaddressid = ?');
    $sql_params = array($bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name, $accountaddressid);
    $sth->execute($sql_params);

}

function insert_load_address($dbh, $sobilladdressid, $bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone) {

    $sth = $dbh->prepare('INSERT INTO vtiger_sobillads (sobilladdressid, 
                                                                    bill_street,
                                                                    bill_code,
                                                                    bill_city, 
                                                                    bill_country,
                                                                    load_company,
                                                                    load_company_code,
                                                                    load_contact,
                                                                    load_phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($sobilladdressid, $bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone);
    $sth->execute($sql_params);
            
    $sth = $dbh->prepare('INSERT INTO vtiger_salesordercf (salesorderid, cf_855) VALUES (?, ?)'); 
    $sql_params = array($sobilladdressid, 'Transporto užsakymas');
    $sth->execute($sql_params);
}

function insert_unload_address($dbh, $soshipaddressid, $ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone) {

    $sth = $dbh->prepare('INSERT INTO vtiger_soshipads (soshipaddressid, 
                                                                    ship_street,
                                                                    ship_code,
                                                                    ship_city, 
                                                                    ship_country,
                                                                    unload_company,
                                                                    unload_company_code,
                                                                    unload_contact,
                                                                    unload_phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($soshipaddressid, $ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone);
    $sth->execute($sql_params);
           
}


function update_load_address($dbh, $sobilladdressid, $bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone) {

    $sth = $dbh->prepare('UPDATE vtiger_sobillads SET bill_street = ?, bill_code = ?, bill_city = ?, bill_country = ?, load_company = ?, load_company_code = ?, load_contact = ?, load_phone = ? WHERE sobilladdressid = ?');
    $sql_params = array($bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone, $sobilladdressid);
    $sth->execute($sql_params);

}

function update_unload_address($dbh, $soshipaddressid, $ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone) {

    $sth = $dbh->prepare('UPDATE vtiger_soshipads SET ship_street = ?, ship_code = ?, ship_city = ?, ship_country = ?, unload_company = ?, unload_company_code = ?, unload_contact = ?, unload_phone = ? WHERE soshipaddressid = ?');
    $sql_params = array($ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone, $soshipaddressid);
    $sth->execute($sql_params);

}

function orders_log($log_variable, $logfile) {

    global $config;        
      
    $log_json = '--------------------------------' . date("Y-m-d H:i:s") . '--------------------------------' . "\n" . json_encode($log_variable) . "\n" . "\n";
    
    file_put_contents($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, $log_json, FILE_APPEND);
	
    if (filesize($config['paths']['api_path'] . DIRECTORY_SEPARATOR. 'logs' . DIRECTORY_SEPARATOR . $logfile) > 1024 * 1024 * 10) {
        copy($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, 
             $config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('Y-m-d_H-i-s') . '_' . $logfile);
        unlink($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile);
    }
}

function customers_log($log_variable, $logfile) {

    global $config;        
      
    $log_json = '--------------------------------' . date("Y-m-d H:i:s") . '--------------------------------' . "\n" . json_encode($log_variable) . "\n" . "\n";
    
    file_put_contents($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, $log_json, FILE_APPEND);
	
    if (filesize($config['paths']['api_path'] . DIRECTORY_SEPARATOR. 'logs' . DIRECTORY_SEPARATOR . $logfile) > 1024 * 1024 * 10) {
        copy($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, 
             $config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('Y-m-d_H-i-s') . '_' . $logfile);
        unlink($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile);
    }
}


?>