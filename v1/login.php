<?php


function login($params) {

    global $config;
    require 'appc_utils.php';
    require 'metrika.php';
    
    $logfile = 'login.log';
    webservicelog($params, $logfile);

    $result_data = array();

    try {

        require('db_connection.php');       
        
        $salt = substr($params['username'], 0, 2);
        $salt = '$1$' . str_pad($salt, 9, '0');
        $encrypted_password = crypt($params['password'], $salt);

        // Check if user exists
        $sth = $dbh->prepare('SELECT u.id, u.first_name, u.last_name, u.type, r.rolename FROM vtiger_users u
            		      LEFT JOIN vtiger_user2role ur ON ur.userid = u.id
			      LEFT JOIN vtiger_role r ON r.roleid = ur.roleid 
                              WHERE u.user_name = ? AND u.user_password = ?');
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($params['username'], $encrypted_password));

        $row = $sth->fetch();

        if ($sth->rowCount() == 0) {

            $output = array(
                'status' => 401,
                'response' => array(
                    'type' => 'AUTHORIZATION',
                    'message' => 'Not logged in / Wrong password or username / Token expired'
                )
            );

            $logfile = 'login.log';
            webservicelog($output, $logfile); 
        
        } else {

            // Generate new token 
            $new_token = guid();
            $user_id = $row['id'];
            
            // Update user with new token
            $sth = $dbh->prepare('UPDATE vtiger_users u SET u.token = ? WHERE u.user_name = ?');
            $sth->execute(array($new_token, $params['username']));

            $result_data['id'] = intval($row['id']);
            $result_data['token'] = $new_token;
            $result_data['type'] = intval($row['type']);

            if($row['first_name']!= NULL && !empty($row['first_name']))                 
            $result_data['first_name'] = $row['first_name'];
                
            if($row['last_name']!= NULL && !empty($row['last_name']))                 
            $result_data['last_name'] = $row['last_name'];  
            
            $result_data['gps_period'] = 5;  
            $result_data['alert_phone'] = "+370123456789"; 
            $result_data['server_time'] = date("Y-m-d H:i:s");
            
            if ($row['type'] == 0) {
                
                $get_existing_tracking_route_id = Get_Vehicle_Route_Metrika($dbh, $user_id); 

                if(empty($get_existing_tracking_route_id)) {
                    $new_route_id = Save_New_Route_Metrika($dbh, $user_id); 
                } else {
                    $new_route_id = $get_existing_tracking_route_id;
                }
                
                if($new_route_id!= NULL && !empty($new_route_id))
                $result_data['route_id'] = intval($new_route_id);
            }
                    // Check if user exists
            /*$sth2 = $dbh->prepare('SELECT f.appcfunctionsid, f.unique_code, f.value, f.description
                                        FROM vtiger_appcfunctions f 
                                        JOIN vtiger_crmentity e ON e.crmid = f.appcfunctionsid
                                        WHERE e.deleted = 0');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $function_id = intval($row['appcfunctionsid']);
                
            $functions[$function_id]['unique_code'] = $row['unique_code'];                
            $functions[$function_id]['description'] = $row['description'];
            $functions[$function_id]['value'] = $row['value'];

            $functions_indexed = array_values($functions);
            $result_data['function_options'] = $functions_indexed;
            
            }*/
            
            $sth2 = $dbh->prepare('SELECT * FROM vtiger_eventstatus WHERE presence = 1');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $site_status_id = intval($row['eventstatusid']);
                
            $site_status_list[$site_status_id]['value'] = $row['eventstatus'];               
            
            $site_status_list_indexed = array_values($site_status_list);
            $result_data['site_status_list'] = $site_status_list_indexed;
            
            }              
            
            $sth2 = $dbh->prepare('SELECT * FROM vtiger_taskpriority WHERE presence = 1');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $site_not_visit_id = intval($row['taskpriorityid']);
                
            $site_not_visit_list[$site_not_visit_id]['value'] = $row['taskpriority'];               
            
            $site_not_visit_list_indexed = array_values($site_not_visit_list);
            $result_data['site_not_visit_list'] = $site_not_visit_list_indexed;
            
            }                 
            
            $sth2 = $dbh->prepare('SELECT * FROM  vtiger_ticketstatus WHERE presence = 1');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $route_status_id = intval($row['ticketstatus_id']);
                
            $route_status_list[$route_status_id]['value'] = $row['ticketstatus'];               
            
            $route_status_list_indexed = array_values($route_status_list);
            $result_data['route_status_list'] = $route_status_list_indexed;
            
            }      
            
            $sth2 = $dbh->prepare('SELECT * FROM vtiger_sostatus WHERE presence = 1');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $order_status_id = intval($row['sostatusid']);
                
            $order_status_list[$order_status_id]['value'] = $row['sostatus'];               
            
            $order_status_list_indexed = array_values($order_status_list);
            $result_data['order_status_list'] = $order_status_list_indexed;
            
            }            
            
            $sth2 = $dbh->prepare('SELECT * FROM vtiger_carrier');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $order_not_delivery_list_id = intval($row['carrierid']);
                
            $order_not_delivery_list[$order_not_delivery_list_id]['value'] = $row['carrier'];               
            
            $order_not_delivery_list_indexed = array_values($order_not_delivery_list);
            $result_data['order_not_delivery_list'] = $order_not_delivery_list_indexed;
            
            }            
            
            $sth2 = $dbh->prepare('SELECT * FROM vtiger_glacct');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $load_shortage_list_id = intval($row['glacctid']);
                
            $load_shortage_list[$load_shortage_list_id]['value'] = $row['glacct'];               
            
            $load_shortage_list_indexed = array_values($load_shortage_list);
            $result_data['load_shortage_list'] = $load_shortage_list_indexed;
            
            }   
            
            $sth2 = $dbh->prepare('SELECT * FROM vtiger_servicecategory WHERE presence = 1');
            $sth2->execute(array());
            
            while ($row = $sth2->fetch()) {
                
            $check_id = intval($row['servicecategoryid']);
                
            $check_list[$check_id]['value'] = $row['servicecategory'];               
            
            $check_list_indexed = array_values($check_list);
            $result_data['check_list'] = $check_list_indexed;
            
            }               
            
            /*$sth2 = $dbh->prepare("SELECT * FROM vtiger_servicecategory a, vtiger_picklist_dependency b WHERE a.servicecategory = b.sourcevalue AND b.sourcefield = 'servicecategory' AND a.servicecategory IN('Nepilna patikra','Pilna patikra')");
            $sth2->execute(array());

            while ($row = $sth2->fetch()) {

            $check_type_list = array();
            $check = array();
            
            $check_type_category = $row['servicecategory']; 
            $json_raw = json_decode(html_entity_decode($row["targetvalues"], ENT_QUOTES, 'UTF-8'), true);

                foreach ($json_raw as $key => $value) {  

                    $sth3 = $dbh->prepare("SELECT * FROM vtiger_service_usageunit WHERE service_usageunit = ?");
                    $sth3->execute(array($value));
                    $row2 = $sth3->fetch();

                    $check['value'] = $value;
                    $check['type'] = $row2['alert_type'];
                    
                    $check_type_list[$check_type_category][] = $check;
                }
            

            $result_data['check_type_list'][] = $check_type_list;   

            }*/
            
            $sth2 = $dbh->prepare("SELECT * FROM vtiger_servicecategory a, vtiger_picklist_dependency b WHERE a.servicecategory = b.sourcevalue AND b.sourcefield = 'servicecategory' AND a.servicecategory IN('Nepilna patikra','Pilna patikra')");
            $sth2->execute(array());

            $check_type_list = array();
            $check = array();
            
            while ($row = $sth2->fetch()) {

            $json_raw = json_decode(html_entity_decode($row["targetvalues"], ENT_QUOTES, 'UTF-8'), true);

                foreach ($json_raw as $key => $value) {  

                    $sth3 = $dbh->prepare("SELECT * FROM vtiger_service_usageunit WHERE service_usageunit = ?");
                    $sth3->execute(array($value));
                    $row2 = $sth3->fetch();
                    
                    $check['check_list'] = $row['servicecategory'];
                    $check['value'] = $value;
                    $check['type'] = $row2['presence'];
                    
                    if($row2['presence'] == 0) {
                    $check['type'] = 1;  
                    } else {
                    $check['type'] = 0;      
                    }
                    
                    $check_type_list[] = $check;
                }
            
            $check_list_indexed = array_values($check_type_list);
            $result_data['check_type_list'] = $check_type_list;   

            }     

            $result = $result_data;
            
            $output = array(
                'status' => 200,
                'response' => $result
            );

            $logfile = 'login.log';
            webservicelog($output, $logfile);
           
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'login.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>