<?php
        
function syncOrders($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
    require 'metrika.php';
    
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';

    $endpointUrl = $url;
	
    $logfile = 'sync_orders.log';
    WebserviceLog($params, $logfile);      

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            WebserviceLog($output, $logfile);
            
        } else {         
            
            $sth = $dbh->prepare('SELECT accesskey, user_name FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            
            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);
            
            $dbh->beginTransaction();

                    $crm_order_id = $params['crm_order_id'];
                    $order_status = $params['status'];
                    $not_delivery_reason = $params['not_delivery_reason'];
                    $comment = $params['comment'];
                    
                    if(empty($not_delivery_reason)) {
                    $not_delivery_reason = NULL;
                    }
                
                    $date = date("Y-m-d H:i:s");
                    
                    $sth = $dbh->prepare("UPDATE vtiger_salesorder SET carrier = ?, sostatus = ?, source = ? WHERE salesorderid = ?");
                    $sth->execute(array($not_delivery_reason, $order_status, 'APP', $crm_order_id));           
                    
                    $sth = $dbh->prepare("UPDATE vtiger_crmentity SET modifiedtime = ?, description = CONCAT_WS(', ',description, '$comment') WHERE crmid = ?");
                    $sth->execute(array($date, $crm_order_id));  
                    
            $dbh->commit();		
            
            Save_Order_Metrika($dbh, $crm_order_id);
            
            $output = array(
                'status' => 200,
                'response' => array('crm_order_id' => $crm_order_id)
            );
           
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_orders.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
