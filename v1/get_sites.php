<?php

function getSites($params) {

    global $config;
    require 'appc_utils.php';

    $logfile = 'get_sites.log';
    webservicelog($params, $logfile);

    $result_data = array('sites' => array());

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {
            
            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {

            $sql_params = array();

            $type = $params['type'];
            $user_id = $params['user_id'];
            $route_default_user = 26;
            $timestamp = $params['timestamp'];
            
            if($type == 1) {
                
                $sql = "SELECT s.*, s.route_point_operation_id AS site_id, a.*, t.*, t.id AS route_id, s.contact, e.deleted
                                        FROM vtiger_activity s
                                        JOIN vtiger_crmentity e ON e.crmid = s.activityid 
                                        LEFT JOIN vtiger_account a ON s.related_to = a.accountid 
                                        JOIN vtiger_seactivityrel c ON c.activityid = s.activityid  
                                        JOIN vtiger_troubletickets t ON t.ticketid = c.crmid  
                                        JOIN vtiger_crmentity r ON r.crmid = t.ticketid 
                                        WHERE r.deleted = 0 AND t.category = 'Route' AND r.smownerid = ? AND t.is_own = 1";  
                
                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                }

                $sql.= " AND s.eventstatus != 'Completed'";                
                $sql.= " AND t.status != 'finished'"; 
                $sql.= " AND t.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";                   
                $sql.= " ORDER BY t.ticketid";
                
                array_push($sql_params, $user_id);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);
                
            } elseif ($type == 0) {
                
                $sql = "SELECT s.*, s.route_point_operation_id AS site_id, a.*, t.*, t.id AS route_id, s.contact, e.deleted
                                        FROM vtiger_activity s
                                        JOIN vtiger_crmentity e ON e.crmid = s.activityid 
                                        LEFT JOIN vtiger_account a ON s.related_to = a.accountid 
                                        JOIN vtiger_seactivityrel c ON c.activityid = s.activityid  
                                        JOIN vtiger_troubletickets t ON t.ticketid = c.crmid  
                                        JOIN vtiger_crmentity r ON r.crmid = t.ticketid 
                                        WHERE r.deleted = 0 AND t.category = 'Route' AND (r.smownerid = ? OR r.smownerid = ?) AND t.is_own = 0";
                
                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                }                
                
                $sql.= " AND s.eventstatus != 'Completed'";
                $sql.= " AND t.status != 'finished'"; 
                $sql.= " AND t.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";                   
                $sql.= " ORDER BY t.ticketid";                
                
                array_push($sql_params, $user_id, $route_default_user);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);                 
            }            

            $i = 0;
            
            while ($row = $sth->fetch()) {
                
            $i++;

                if(!empty($row['site_id'])) {
                    //--
                    $task_id = intval($row['site_id'] . $i);
                    //--
                    $count[$task_id][] = intval($row['site_id']);
                } else {
                    $task_id = intval($row['activityid']);
                    $count[$task_id][] = intval($row['activityid']);   
                }    
                
                if($crm_route_id_old == $row['ticketid']) {
                        $tasks[$task_id]['nr'] = count($count);     
                    } else {
                        $count = NULL;
                        if(!empty($row['site_id'])) {
                          //--
                          $task_id = intval($row['site_id'] . $i);
                          //--
                          $count[$task_id][] = intval($row['site_id']);
                        } else {
                          $task_id = intval($row['activityid']);
                          $count[$task_id][] = intval($row['activityid']);   
                        }  
                        $tasks[$task_id]['nr'] = count($count);
                }    
                 
                $tasks[$task_id]['crm_site_id'] = intval($row['activityid']);           
		$tasks[$task_id]['site_id'] = intval($row['site_id']);	
		$tasks[$task_id]['crm_route_id'] = intval($row['ticketid']);                
		$tasks[$task_id]['route_id'] = intval($row['route_id']);
		$tasks[$task_id]['customer_id'] = intval($row['accountid']); 
                
                if(empty($row['customer_id'])) {
                    $tasks[$task_id]['customer_name'] = $row['subject'];
                } else {
                    $tasks[$task_id]['customer_name'] = $row['accountname'];  
                }
                
                $tasks[$task_id]['address'] = $row['location'];   
                $tasks[$task_id]['city'] = trim(end(explode(',',$row['location'])));         
                $tasks[$task_id]['start_time'] = $row['time_start'];
                $tasks[$task_id]['end_time'] = $row['time_end'];                
                $tasks[$task_id]['status'] = $row['eventstatus'];                   
                $tasks[$task_id]['delivery_type'] = $row['activitytype'];                              
                                
                if($row['contact'] != NULL && !empty($row['contact']))                  
                $tasks[$task_id]['contact'] = $row['contact'];                 
                
                $tasks[$task_id]['deleted'] = intval($row['deleted']);                 
                
                $crm_route_id_old = intval($row['ticketid']);
                $crm_site_id_old = intval($row['site_id']);
                
                $tasks_indexed = array_values($tasks);
                $result_data['sites'] = $tasks_indexed;   
                    
            }
            
            $output = array(
                'status' => 200,
                'response' => $result_data
            );
            
                $logfile = 'get_sites.log';
                webservicelog($output, $logfile);
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );
        
        $logfile = 'get_sites.log';
        webservicelog($output, $logfile);
    }
    return $output;
}
