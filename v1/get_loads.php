<?php

function getLoads($params) {

    global $config;
    require 'appc_utils.php';

    $logfile = 'get_loads.log';
    webservicelog($params, $logfile);

    $result_data = array('loads' => array());

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {
            
            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {

            $sql_params = array();

            $type = $params['type'];
            $user_id = $params['user_id'];
            $route_default_user = 26;
            $timestamp = $params['timestamp'];
            
            if($type == 1) {            
                $sql = "SELECT s.*, i.*
                                        FROM vtiger_salesorder s
                                        JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
                                        JOIN vtiger_crmentity e ON e.crmid = s.salesorderid 
                                        JOIN vtiger_crmentityrel cr ON cr.relcrmid = s.salesorderid 
                                        JOIN vtiger_seactivityrel sc ON sc.activityid = cr.crmid  
                                        JOIN vtiger_activity ac ON ac.activityid = cr.crmid   
                                        JOIN vtiger_troubletickets t ON t.ticketid = sc.crmid  
                                        JOIN vtiger_crmentity cy ON cy.crmid = t.ticketid                                          
                                        WHERE e.deleted = 0 AND s.external_order_id IS NOT NULL AND cy.deleted = 0 AND t.category = 'Route' AND cy.smownerid = ? AND t.is_own = 1";           

                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                }                
                
                $sql.= " AND s.sostatus NOT IN('Delivered', 'Not Delivered')";
                $sql.= " AND (s.status != 'ERROR' OR s.status IS NULL)";   
                $sql.= " AND ac.eventstatus != 'Completed'";                
                $sql.= " AND t.status != 'finished'";                 
                $sql.= " AND t.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";   
                
                array_push($sql_params, $user_id);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);
            
            } elseif ($type == 0) {
                $sql = "SELECT s.*, i.*
                                        FROM vtiger_salesorder s
                                        JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
                                        JOIN vtiger_crmentity e ON e.crmid = s.salesorderid 
                                        JOIN vtiger_crmentityrel cr ON cr.relcrmid = s.salesorderid 
                                        JOIN vtiger_seactivityrel sc ON sc.activityid = cr.crmid  
                                        JOIN vtiger_activity ac ON ac.activityid = cr.crmid   
                                        JOIN vtiger_troubletickets t ON t.ticketid = sc.crmid  
                                        JOIN vtiger_crmentity cy ON cy.crmid = t.ticketid                                          
                                        WHERE e.deleted = 0 AND s.external_order_id IS NOT NULL AND cy.deleted = 0 AND t.category = 'Route' AND (cy.smownerid = ? OR cy.smownerid = ?) AND t.is_own = 0";           

                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                }   
                
                $sql.= " AND s.sostatus NOT IN('Delivered', 'Not Delivered')";   
                $sql.= " AND (s.status != 'ERROR' OR s.status IS NULL)";   
                $sql.= " AND ac.eventstatus != 'Completed'";                
                $sql.= " AND t.status != 'finished'";
                $sql.= " AND t.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";   
                
                array_push($sql_params, $user_id, $route_default_user);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);     
            }  
                
            while ($row = $sth->fetch()) {

                $task_id = intval($row['salesorderid'] . $row['lineitem_id']);
                
                $tasks[$task_id]['crm_order_id'] = intval($row['id']);   
                $tasks[$task_id]['crm_load_id'] = intval($row['lineitem_id']);           
		$tasks[$task_id]['product_id'] = intval($row['productid']);		
		$tasks[$task_id]['quantity'] = sprintf('%0.2f', $row['quantity']);  
		$tasks[$task_id]['measure'] = $row['cargo_measure'];
                
                if($row['cargo_wgt']!= NULL && !empty($row['cargo_wgt']))
		$tasks[$task_id]['weight'] = $row['cargo_wgt'];   
                
                if($row['cargo_length']!= NULL && !empty($row['cargo_length']))                
                $tasks[$task_id]['length'] = $row['cargo_length'];              
                
                if($row['cargo_width']!= NULL && !empty($row['cargo_width']))                
                $tasks[$task_id]['width'] = $row['cargo_width'];
                
                if($row['cargo_height']!= NULL && !empty($row['cargo_height']))    
                $tasks[$task_id]['height'] = $row['cargo_height'];
                
                $tasks[$task_id]['m3'] = sprintf('%0.2f', ($row['cargo_length'] * $row['cargo_width'] * $row['cargo_height']));    
                $tasks[$task_id]['cargo_load'] = $row['cargo_wgt'] . ' ' . $row['cargo_length'] . 'x' . $row['cargo_width'] . 'x' . $row['cargo_height'];    
                
                if($row['pll']!= NULL && !empty($row['pll']))                
                $tasks[$task_id]['pll'] = $row['pll'];  
                
                if($row['comment']!= NULL && !empty($row['comment']))                
                $tasks[$task_id]['note'] = $row['comment'];   
                
                if($row['description']!= NULL && !empty($row['description']))                
                $tasks[$task_id]['document'] = $row['description'];  
                                           
                if($row['ean']!= NULL && !empty($row['ean']))                
                $tasks[$task_id]['ean'] = current(explode(',', $row['ean']));              
                
                $tasks_indexed = array_values($tasks);
                $result_data['loads'] = $tasks_indexed;
            }    
                        
            $output = array(
                'status' => 200,
                'response' => $result_data
            );
            
                $logfile = 'get_loads.log';
                webservicelog($output, $logfile);
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );
        
        $logfile = 'get_loads.log';
        webservicelog($output, $logfile);
    }
    return $output;
}
