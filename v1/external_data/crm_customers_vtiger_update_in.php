<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_account SET       
                                                    account_no = ?,
                                                    accountname = ?,
                                                    account_type = ?, 
                                                    contact = ?, 
                                                    phone = ?, 
                                                    email1 = ?,
                                                    blocked = ?,
                                                    region = ?,
                                                    municipality = ?,
                                                    settlement = ?,
                                                    billing_address = ?,
                                                    post_code = ?,                                                    
                                                    industry = ?,
                                                    account_manager_id = ?,
                                                    modifydate = ?,
                                                    update_date = ?
                                                WHERE accountid = ?');
                    
    
    $sth = $dbh->prepare('SELECT t.*, c.accountid, c.pricebook, p.pricebookid
                            FROM crm_customers_in t
                            JOIN vtiger_account c ON c.customer_id = t.customer_id
                            JOIN vtiger_crmentity g ON g.crmid = c.accountid
                            LEFT JOIN vtiger_pricebook p ON p.pricebookid = t.pricebook                             
                            WHERE g.deleted = 0');

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {
        $subject = 'Fizinis'; 
        if($row['type'] == 'legal') {
            $subject = 'Juridinis';    
        }      
        
        $type = 'Pirkėjas';
        $ownership = 'Metrika';
        $date = date("Y-m-d H:i:s");
        $description = null;    
              
        $update_entity = update_entity($dbh, $row['accountid'], $description, $row['customer_name'], $date);

        $sth2->execute(array($row['customer_code'],
                            $row['customer_name'],                      
                            $subject, 
                            $row['contact_person_name'],
                            $row['phone'],
                            $row['email'],
                            $row['blocked'],
                            $row['region'],      
                            $row['municipality'], 
                            $row['settlement'],                     
                            $row['billing_address'], 
                            $row['post_code'],                            
                            $type,
                            $row['account_manager_id'],                                                         
                            $row['modifydate'],                        
                            $date,
                            $row['accountid']
        ));         
            
        $update_customer_address_billing = update_customer_address_billing($dbh, $row['accountid'], $row['sender_address'], $row['sender_postcode'], $row['default_sender_location_id'], $row['sender_city'], $row['country_code'], $row['sender_name']);
        $update_customer_address_shipping = update_customer_address_shipping($dbh, $row['accountid'], $row['consignee_address'], $row['consignee_postcode'], $row['default_consignee_location_id'], $row['consignee_city'], $row['country_code'], $row['consignee_name']);                    
       
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
