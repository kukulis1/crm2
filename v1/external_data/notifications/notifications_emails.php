<?php

include '../ws.config.php';
include $_SERVER['DOCUMENT_ROOT'].'/config.inc.php';
global $config;

global $hr_reminder_email; 

require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer(true);


  try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   

    $date = date("Y-m-d H:i:s");

    $insert_log = $dbh->prepare("INSERT INTO app_hr_reminder (reminder_text, ending_date, employid, sendtime) VALUES (?, ?, ?, ?)");

    $insert_check = $dbh->prepare("INSERT INTO app_hr_check_send_reminder (employid, type, ending_date ,send_date) VALUES (?,?,?,?)");
   
    $medical_certificate_ending = $dbh->prepare("SELECT firstname,lastname, c.employid, cf_2574 AS medical_certificate_ending
                                                       FROM vtiger_hrm_employcf c
                                                       JOIN vtiger_hrm_employee em ON em.employid=c.employid
                                                       LEFT JOIN app_hr_check_send_reminder r ON r.employid= em.employid AND type = 'cf_2574'
                                                       WHERE cf_2574 <= (CURDATE() + INTERVAL 2 WEEK) AND IF(ending_date IS NOT NULL, cf_2574 != ending_date, 1=1) AND r.type IS NULL");
    
    $driver_license_ending = $dbh->prepare("SELECT firstname,lastname, c.employid, cf_2578 AS driver_license_ending
                                                  FROM vtiger_hrm_employcf c
                                                  JOIN vtiger_hrm_employee em ON em.employid=c.employid
                                                  LEFT JOIN app_hr_check_send_reminder r ON r.employid= em.employid AND type = 'cf_2578'
                                                  WHERE cf_2578 <= (CURDATE() + INTERVAL 2 WEEK) AND IF(ending_date IS NOT NULL, cf_2574 != ending_date, 1=1) AND r.type IS NULL"); 
                                                       
    $c_cat_driver_license_ending = $dbh->prepare("SELECT firstname,lastname, c.employid, cf_2580 AS c_cat_driver_license_ending
                                                  FROM vtiger_hrm_employcf c
                                                  JOIN vtiger_hrm_employee em ON em.employid=c.employid
                                                  LEFT JOIN app_hr_check_send_reminder r ON r.employid= em.employid AND type = 'cf_2580'
                                                  WHERE cf_2580 <= (CURDATE() + INTERVAL 2 WEEK) AND IF(ending_date IS NOT NULL, cf_2580 != ending_date, 1=1) AND r.type IS NULL"); 
  
    $ce_cat_driver_license_ending = $dbh->prepare("SELECT firstname,lastname, c.employid, cf_2588 AS ce_cat_driver_license_ending
                                                   FROM vtiger_hrm_employcf c
                                                   JOIN vtiger_hrm_employee em ON em.employid=c.employid
                                                   LEFT JOIN app_hr_check_send_reminder r ON r.employid= em.employid AND type = 'cf_2588'
                                                   WHERE cf_2588 <= (CURDATE() + INTERVAL 2 WEEK) AND IF(ending_date IS NOT NULL, cf_2588 != ending_date, 1=1) AND r.type IS NULL");

    $ninety_nine_code_license_ending = $dbh->prepare("SELECT firstname,lastname, c.employid, cf_2582 AS ninety_nine_code_license_ending
                                                      FROM vtiger_hrm_employcf c
                                                      JOIN vtiger_hrm_employee em ON em.employid=c.employid
                                                      LEFT JOIN app_hr_check_send_reminder r ON r.employid= em.employid AND type = 'cf_2582'
                                                      WHERE cf_2582 <= (CURDATE() + INTERVAL 2 WEEK) AND IF(ending_date IS NOT NULL, cf_2582 != ending_date, 1=1) AND r.type IS NULL");

    $driver_cart_ending = $dbh->prepare("SELECT firstname,lastname, c.employid, cf_2584 AS driver_cart_ending
                                         FROM vtiger_hrm_employcf c
                                         JOIN vtiger_hrm_employee em ON em.employid=c.employid
                                         LEFT JOIN app_hr_check_send_reminder r ON r.employid= em.employid AND type = 'cf_2588'
                                         WHERE cf_2584 <= (CURDATE() + INTERVAL 2 WEEK) AND IF(ending_date IS NOT NULL, cf_2588 != ending_date, 1=1) AND r.type IS NULL"); 
                                                      
    $driver_medical_certificate_ending = $dbh->prepare("SELECT firstname,lastname, c.employid, cf_2586 AS driver_medical_certificate_ending
                                         FROM vtiger_hrm_employcf c
                                         JOIN vtiger_hrm_employee em ON em.employid=c.employid
                                         LEFT JOIN app_hr_check_send_reminder r ON r.employid= em.employid AND type = 'cf_2586'
                                         WHERE cf_2586 <= (CURDATE() + INTERVAL 2 WEEK) AND IF(ending_date IS NOT NULL, cf_2586 != ending_date, 1=1) AND r.type IS NULL");                                                    

    $medical_certificate_ending->setFetchMode(PDO::FETCH_ASSOC);
    $medical_certificate_ending->execute(array());

    foreach ($medical_certificate_ending as $row) {
      sendNotification($mail, $row['firstname'],$row['lastname'],$row['medical_certificate_ending'],'privalomo sveikatos patikrinimo medicininės pažymos galiojimo');
      $insert_log->execute(array('Privalomo sveikatos patikrinimo medicininės pažymos galiojimas',$row['medical_certificate_ending'],$row['employid'],$date));
      $insert_check->execute(array($row['employid'],'cf_2574', $row['medical_certificate_ending'], $date));
    }

    $driver_license_ending->setFetchMode(PDO::FETCH_ASSOC);
    $driver_license_ending->execute(array());

    foreach ($driver_license_ending as $row) {
      sendNotification($mail, $row['firstname'],$row['lastname'],$row['driver_license_ending'],'vairuotojo pažymėjimo galiojimo');
      $insert_log->execute(array('Vairuotojo pažymėjimo galiojimas',$row['driver_license_ending'],$row['employid'],$date));
      $insert_check->execute(array($row['employid'],'cf_2578', $row['driver_license_ending'], $date));
    }

    $c_cat_driver_license_ending->setFetchMode(PDO::FETCH_ASSOC);
    $c_cat_driver_license_ending->execute(array());

    foreach ($c_cat_driver_license_ending as $row) {
      sendNotification($mail, $row['firstname'],$row['lastname'],$row['c_cat_driver_license_ending'],'C kategorijos vairuotojo pažymėjimo galiojimo');
      $insert_log->execute(array('C kategorijos vairuotojo pažymėjimo galiojimas',$row['c_cat_driver_license_ending'],$row['employid'],$date));
      $insert_check->execute(array($row['employid'],'cf_2580', $row['c_cat_driver_license_ending'], $date));
    }

    $ce_cat_driver_license_ending->setFetchMode(PDO::FETCH_ASSOC);
    $ce_cat_driver_license_ending->execute(array());

    foreach ($ce_cat_driver_license_ending as $row) {
      sendNotification($mail, $row['firstname'],$row['lastname'],$row['ce_cat_driver_license_ending'],'CE kategorijos vairuotojo pažymėjimo galiojimo');
      $insert_log->execute(array('CE kategorijos vairuotojo pažymėjimo galiojimas',$row['ce_cat_driver_license_ending'],$row['employid'],$date));
      $insert_check->execute(array($row['employid'],'cf_2588', $row['ce_cat_driver_license_ending'], $date));
    }

    $ninety_nine_code_license_ending->setFetchMode(PDO::FETCH_ASSOC);
    $ninety_nine_code_license_ending->execute(array());

    foreach ($ninety_nine_code_license_ending as $row) {
      sendNotification($mail, $row['firstname'],$row['lastname'],$row['ninety_nine_code_license_ending'],'95 kodo galiojimo');
      $insert_log->execute(array('95 kodo galiojimas',$row['ninety_nine_code_license_ending'],$row['employid'],$date));
      $insert_check->execute(array($row['employid'],'cf_2582', $row['ninety_nine_code_license_ending'], $date));
    }

    $driver_cart_ending->setFetchMode(PDO::FETCH_ASSOC);
    $driver_cart_ending->execute(array());

    foreach ($driver_cart_ending as $row) {
      sendNotification($mail, $row['firstname'],$row['lastname'],$row['driver_cart_ending'],'vairuotojo kortelės galiojimo');
      $insert_log->execute(array('Vairuotojo kortelės galiojimas',$row['driver_cart_ending'],$row['employid'],$date));
      $insert_check->execute(array($row['employid'],'cf_2584', $row['driver_cart_ending'], $date));
    }

    $driver_medical_certificate_ending->setFetchMode(PDO::FETCH_ASSOC);
    $driver_medical_certificate_ending->execute(array());

    foreach ($driver_medical_certificate_ending as $row) {
      sendNotification($mail, $row['firstname'],$row['lastname'],$row['driver_medical_certificate_ending'],'vairuotojo sveikatos patikrinimo medicininės pažymos galiojimo');
      $insert_log->execute(array('Vairuotojo sveikatos patikrinimo medicininės pažymos galiojimas',$row['driver_medical_certificate_ending'],$row['employid'],$date));
      $insert_check->execute(array($row['employid'],'cf_2586', $row['driver_medical_certificate_ending'], $date));
    }

    $dbh->commit();

  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo "Error!";
    echo $e->getMessage();
  }

  function sendNotification($mail,$firstname,$lastname,$ending_date,$text){
    global $mailHost,$hr_reminder_pass, $hr_reminder_email;
  
    try {
        //Server settings
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      
        $mail->isSMTP();                                            
        $mail->Host       = $mailHost;                    
        $mail->SMTPAuth   = true;                                   
        $mail->Username   = $hr_reminder_email;                    
        $mail->Password   = $hr_reminder_pass;                               
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;        
        $mail->Port       = 465; 
        $mail->SMTPOptions = array('ssl' => array(
                                   'verify_peer' => false,
                                   'verify_peer_name' => false,
                                   'allow_self_signed' => true
                                    )
                              );
                              
        $mail->setLanguage('lt', $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = false;  
  
        //Recipients
        $mail->setFrom($hr_reminder_email, "Priminimas");
        $hr_reminder_email = 'zykas911@gmail.com';
        $mail->addAddress($hr_reminder_email);   
        
        $content = "<head>
        <style>
        span {
           font-family: Verdana, Geneva, sans-serif; 
           font-size:14px; 
           color:#323232;
        }
        </style>

        <span>Priminimas dėl <b>$firstname $lastname</b> $text pabaigos. Dokumentas baigia galioti <b>$ending_date</b></span>

        </head>";
       
  
        // Content
        $mail->isHTML(true); 
        $mail->Subject = "Priminimas dėl $text";
        $mail->Body    = $content;
        $mail->send();  
  
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
  }