<?php

include '../ws.config.php';
require '../utils.php';


  try {

    require('../mysql_connection.php');
    

    $dbh->beginTransaction();   

    $date = date("Y-m-d H:i:s");

    $insert_log = $dbh->prepare("INSERT INTO app_vendor_contracts_ending_reminder (reminder_text, ending_date, vendorid, remind_date) VALUES (?, ?, ?, ?)");
    $insert_check = $dbh->prepare("INSERT INTO app_vendor_contracts_ending_reminder_check (noteid, ending_date, createdate) VALUES (?, ?, ?)");
   
    $check_ending_contracts = $dbh->prepare("SELECT DISTINCT vendorid, vendorname,vtiger_notes.notesid, vtiger_notes.title, vtiger_notescf.cf_2177 AS end_date
                                                  FROM vtiger_notes 
                                                  INNER JOIN vtiger_senotesrel ON vtiger_senotesrel.notesid= vtiger_notes.notesid 
                                                  JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_senotesrel.crmid
                                                  LEFT JOIN vtiger_notescf ON vtiger_notescf.notesid= vtiger_notes.notesid 
                                                  INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid= vtiger_notes.notesid AND vtiger_crmentity.deleted=0 
                                                  LEFT JOIN vtiger_seattachmentsrel ON vtiger_seattachmentsrel.crmid =vtiger_notes.notesid 
                                                  LEFT JOIN app_vendor_contracts_ending_reminder_check en ON en.noteid=vtiger_notes.notesid 
                                                  WHERE cf_2177 IS NOT NULL AND ((cf_2177 <= (CURDATE() + INTERVAL 4 WEEK) AND cf_2177 < (CURDATE() + INTERVAL 5 WEEK)) AND cf_2177 >= (CURDATE() - INTERVAL 1 WEEK))                                                  
                                                  
                                                  AND  IF(en.noteid IS NOT NULL,cf_2177 !=ending_date,'1=1') ");

                                                  // OLD cf_2177 <= (CURDATE() + INTERVAL 4 WEEK) 
    $check_ending_contracts->setFetchMode(PDO::FETCH_ASSOC);
    $check_ending_contracts->execute();                                              


    foreach ($check_ending_contracts as $value) {
      $vendorname = $value['vendorname'];
      $title = $value['title'];
      $end_date = $value['end_date'];
      $text = "Tiekėjo $vendorname sutartis $title baigiasi $end_date";      
      createNotification($dbh, $value['vendorid'], $text, 26, 'tiekeju_sutartys');
      $insert_log->execute([$text, $end_date, $value['vendorid'], $date]);
      $insert_check->execute([$value['notesid'], $end_date, $date]);
    }
    


    $dbh->commit();

  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo "Error!";
    echo $e->getMessage();
  }

