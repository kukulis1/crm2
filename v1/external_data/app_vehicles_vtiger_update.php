<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_assets SET       
                                                                serialnumber = ?,
                                                                assetname = ?,
                                                                user_id = ?,
                                                                driver1_id = ?,
                                                                driver2_id = ?,     
                                                                update_date = ?
                                                    WHERE assetsid = ?');
                    
    
    $sth = $dbh->prepare('SELECT t.*, c.assetsid, u.id AS user1_id, y.id AS user2_id
                            FROM app_vehicles t
                            JOIN vtiger_assets c ON c.id = t.vehicle_id
                            JOIN vtiger_crmentity g ON g.crmid = c.id
                            LEFT JOIN vtiger_users u ON u.title = t.driver1_id
                            LEFT JOIN vtiger_users y ON y.title = t.driver2_id                            
                            WHERE g.deleted = 0');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {

            if(empty($row['user1_id']) || $row['user1_id'] == NULL) {
            $user_id = 26;
            } else {
            $user_id = $row['user1_id']; 
            }            
 
            if(empty($row['user2_id']) || $row['user2_id'] == NULL) {
            $user2_id = 26;
            } else {
            $user2_id = $row['user2_id']; 
            }                           
    
            $date = date("Y-m-d H:i:s");   
            
                    $update_entity = update_entity($dbh, $row['assetsid'], $user_id, NULL, $row['registration_number'], $date);
  
                    $sth2->execute(array($row['trailer_registration_number'],
                                        $row['registration_number'],                      
                                        $user2_id, 
                                        $row['driver1_id'],                         
                                        $row['driver2_id'],                                            
                                        $date,
                                        $row['assetsid']
                    ));         

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>