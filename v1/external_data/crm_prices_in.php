<?php

include 'ws.config.php';

global $config;

try {

    require('mysql_connection.php');

    $date = date("YmdHis");
    
    $filename = $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1'  . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'new_sync_data' . DIRECTORY_SEPARATOR . 'crm_pricebooks.csv';
    $imported = $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1'  . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'imported_sync_data' . DIRECTORY_SEPARATOR . 'crm_pricebooks_' . $date . '.' . 'csv';
    
    $fileExists = file_exists($filename);             

    if ($fileExists == true) {

        $sth1 = $dbh->prepare('DELETE FROM crm_prices_in');
        $sth1->execute(array());

        $sth2 = $dbh->prepare('ALTER TABLE crm_prices_in AUTO_INCREMENT = 1');
        $sth2->execute(array());   

        $date = date("Y-m-d H:i:s");
        
        $row = 0;
        $CSVfp = fopen($filename, "r");
            while(($data = fgetcsv($CSVfp, 0, ";")) !== FALSE) {
                $row++; 
                if ($row == 1) {
                    continue;
                }                    
                
                    $sth6 = $dbh->prepare('INSERT INTO crm_prices_in (
                                     pricebook, 
                                     product, 
                                     customer, 
                                     price,
                                     import_date

                                                              ) VALUES (?, ?, ?, ?, ?)');
                    $sth6->execute(array($data[1],
                                         $data[0],
                                         $data[3],
                                         $data[2],                        
                                         $date
            ));

            }
        fclose($CSVfp);

        copy($filename, $imported);
        unlink($filename);
        
    }

} catch (PDOException $e) {

    //print($e->getMessage());
}
?>