<?php

setlocale(LC_ALL, 'en_US.UTF8');
include 'ws.config.php';
global $config;

try {

    require('mysql_connection.php');

    $date = date("YmdHis");
       
    $filename = $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1'  . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'new_sync_data' . DIRECTORY_SEPARATOR . 'crm_post_codes_ventspilis.csv';
    $imported = $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'v1'  . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'imported_sync_data' . DIRECTORY_SEPARATOR .  'crm_post_codes_ventspilis_' . $date . '.' . 'csv';
       
    $fileExists = file_exists($filename);             

    if ($fileExists == true) {

        $sth1 = $dbh->prepare("DELETE FROM crm_post_codes_lv WHERE zone_base = 'Ventspilis'");
        $sth1->execute(array());

        //$sth2 = $dbh->prepare('ALTER TABLE crm_prices_in AUTO_INCREMENT = 1');
        //$sth2->execute(array());   
      
        $row = 0;
        $CSVfp = fopen($filename, "r");
            while(($data = fgetcsv($CSVfp, 0, ";")) !== FALSE) {
                $row++; 
                if ($row == 1) {
                    continue;
                }                    

                    $date = date("Y-m-d H:i:s");
                    $data2 = 'Ventspilis';
                    $data[3] = preg_replace('/^LV-/', '', $data[3]);
                    
                    $sth6 = $dbh->prepare("INSERT INTO crm_post_codes_lv (
                                     post_code, 
                                     street, 
                                     house, 
                                     city,
                                     state,
                                     zone_customer,
                                     zone_base,
                                     import_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                    $sth6->execute(array($data[3],
                                         $data[1],
                                         $data[2],
                                         $data2,
                                         $data2,
                                         $data2,
                                         $data2,                       
                                         $date
            ));

            }
        fclose($CSVfp);

        copy($filename, $imported);
        unlink($filename);
        
    }

} catch (PDOException $e) {

    print($e->getMessage());
}
?>