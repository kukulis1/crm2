<?php
        
function syncChecks($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
    
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';

    $endpointUrl = $url;
	
    $logfile = 'sync_checks.log';
    WebserviceLog($params, $logfile);      

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            WebserviceLog($output, $logfile);
            
        } else {         
            
            $sth = $dbh->prepare('SELECT accesskey, user_name FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            
            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);            

                    $crm_route_id = $params['crm_route_id'];
                    $route_id = $params['route_id'];
                    $route_code = $params['route_code'];
                    $check_android_id = $params['check_android_id'];
                    $check_date = $params['check_date'];
                    $check_type = $params['check_type']; 
                    $auto_number = $params['auto_number'];  
                    $part_number = $params['part_number'];                     
                    $odometer = $params['odometer']; 
                    $litre = $params['litre'];
                    $value = $params['value'];
                    
                    if($check_type == 'Kuras') {
                    $value = $odometer;  
                    } 
                    
                    $crm_order_id = $params['crm_order_id'];
                    $user_id = $params['user_id'];
                    $date = date("Y-m-d H:i:s"); 

            $checks_items_list = $params['check_items'];   

            $dbh->beginTransaction();
            
                if(!empty($checks_items_list) && $checks_items_list > 0) {

                        foreach ($checks_items_list as $checks_key => $checks_items) {

                          $check_name = $checks_items['check_name']; 
                          $checked = $checks_items['checked']; 
                          $comment = $checks_items['comment']; 

                          $sth1 = $dbh->query('SELECT * FROM vtiger_crmentity_seq');
                          while ($row = $sth1->fetch()) {
                          $crm_check_id = $row['id'] + 1;
                          }

                          insert_entity($dbh, 'Services', $crm_check_id, $user_id, $comment, $date, $date);                

                          $sth = $dbh->prepare('INSERT INTO vtiger_service (serviceid, 
                                                                                  service_no, 
                                                                                  servicename, 
                                                                                  servicecategory, 
                                                                                  start_date, 
                                                                                  service_usageunit, 
                                                                                  auto_number,
                                                                                  part_number,
                                                                                  odometer, 
                                                                                  litre, 
                                                                                  type_value,
                                                                                  checked,
                                                                                  crm_route_id,
                                                                                  route_id,
                                                                                  route_code
                                                                                  ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
                          $sth->execute(array($crm_check_id, 
                                              $check_android_id, 
                                              $auto_number, 
                                              $check_type, 
                                              $check_date,
                                              $check_name, 
                                              $auto_number,
                                              $part_number,                           
                                              $odometer, 
                                              $litre, 
                                              $value,
                                              $checked,
                                              $crm_route_id,
                                              $route_id,
                                              $route_code                
                              ));

                          $sth = $dbh->prepare('INSERT INTO vtiger_servicecf (serviceid) VALUES (?)');
                          $sth->execute(array($crm_check_id));		

                            if(!empty($crm_route_id)) {
                                $sth = $dbh->prepare('INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES (?, ?, ?, ?)');
                                $sth->execute(array($crm_route_id, 'HelpDesk', $crm_check_id, 'Services'));                    
                            }        
                          
               $checks[$crm_check_id]['name'] = $check_name;                             
               $checks[$crm_check_id]['id'] = $crm_check_id;        
               
                      }                 
                      
                } else {
                    
                    if(!empty($crm_order_id)) {
                        
                        if($check_type == 'Prastova') {
                            
                        $sth = $dbh->prepare("UPDATE vtiger_salesorder SET prastova = ? WHERE salesorderid = ?");
                        $sth->execute(array($value, $crm_order_id)); 
                        
                        } elseif ($check_type == 'Krovos darbai') {
                            
                        $sth = $dbh->prepare("UPDATE vtiger_salesorder SET pakrova = ? WHERE salesorderid = ?");
                        $sth->execute(array($value, $crm_order_id));
                        
                        }          
                    
                    $sth = $dbh->prepare("UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?");
                    $sth->execute(array($date, $crm_order_id));  
                    $crm_check_id = $crm_order_id;
                    
                    } else {
                          $sth1 = $dbh->query('SELECT * FROM vtiger_crmentity_seq');
                          while ($row = $sth1->fetch()) {
                          $crm_check_id = $row['id'] + 1;
                          }

                          insert_entity($dbh, 'Services', $crm_check_id, $user_id, $comment, $date, $date);                

                          $sth = $dbh->prepare('INSERT INTO vtiger_service (serviceid, 
                                                                                  service_no, 
                                                                                  servicename, 
                                                                                  servicecategory, 
                                                                                  start_date, 
                                                                                  service_usageunit, 
                                                                                  auto_number,
                                                                                  part_number,
                                                                                  odometer, 
                                                                                  litre, 
                                                                                  type_value,
                                                                                  checked,
                                                                                  crm_route_id,
                                                                                  route_id,
                                                                                  route_code
                                                                                  ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
                          $sth->execute(array($crm_check_id, 
                                              $check_android_id, 
                                              $auto_number, 
                                              $check_type, 
                                              $check_date,
                                              $check_name, 
                                              $auto_number,
                                              $part_number,                           
                                              $odometer, 
                                              $litre, 
                                              $value,
                                              $checked,
                                              $crm_route_id,
                                              $route_id,
                                              $route_code                
                              ));

                          $sth = $dbh->prepare('INSERT INTO vtiger_servicecf (serviceid) VALUES (?)');
                          $sth->execute(array($crm_check_id));	
                          
                            if(!empty($crm_route_id)) {
                                $sth = $dbh->prepare('INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES (?, ?, ?, ?)');
                                $sth->execute(array($crm_route_id, 'HelpDesk', $crm_check_id, 'Services'));                    
                            }                          
                    }
                    
               $checks[$crm_check_id]['name'] = $check_name;                             
               $checks[$crm_check_id]['id'] = $crm_check_id;                            
                }               
            
            $dbh->commit();
                             
            $output = array(
                'status' => 200,
                'response' => array('crm_check_id' => array_values($checks))
            );
           
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_checks.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
