<?php
error_reporting(1);
set_time_limit(500);
include 'ws.config.php';
global $config;
require 'utils.php';


try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_928 = ?, cf_1558 = ?, cf_1560 = ?, cf_1562 = ? WHERE   salesorderid = ? '); 
                    
        
    $sth = $dbh->prepare('SELECT t.*,s.salesorderid, ROUND(shipment_direct_km,2) AS shipment_km
                                FROM app_orders2 t
                                JOIN vtiger_inventoryproductrel c ON c.external_order_id = t.id
                                LEFT JOIN vtiger_salesorder s ON s.salesorderid=c.id');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {   

     $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);                    
                    
                       $sth3->execute(array(
                       	$distance,
                        $row['route_code'],                       
                        $row['vehicle_registration_number'], 
                        $row['driver1_name'],                     
                        $row['salesorderid']          
                    ));              

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
