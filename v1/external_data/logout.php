<?php

function logout($params) {

    global $config;
    require 'appc_utils.php';

    $logfile = 'logout.log';
    webservicelog($params, $logfile);

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {

            // Update user with new token
            $sth = $dbh->prepare('UPDATE vtiger_users u SET u.token = ? WHERE u.id = ?');
            $sth->execute(array(NULL, $params['user_id']));

            $output = array(
                'status' => 204,
                'response' => array(
            ));
         
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'logout.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>