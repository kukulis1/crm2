<?php
include '../ws.config.php';
global $config;
require '../utils.php';
set_time_limit(500);
error_reporting(1);

try {

    require('../mysql_connection.php');
        
    $dbh->beginTransaction();
      

    $sth = $dbh->prepare("SELECT crmid,setype
                           FROM vtiger_invoice 
                           JOIN vtiger_crmentity  ON crmid=invoiceid
                           WHERE deleted=0 AND setype != 'Invoice'
                           GROUP BY crmid");   


    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());
    $orders_count = $sth->rowCount();   

    if($orders_count > 0){
      checkDublicates($dbh,$sth,$orders_count);
    }


    $sth2 = $dbh->prepare("SELECT crmid,setype
                           FROM vtiger_salesorder 
                           JOIN vtiger_crmentity  ON crmid=salesorderid
                           WHERE deleted=0 AND setype NOT IN ('SalesOrder','Invoice')
                           GROUP BY crmid");

    $sth2->setFetchMode(PDO::FETCH_ASSOC);
    $sth2->execute(array());
    $orders_count2 = $sth2->rowCount();

    if($orders_count2 > 0){
      checkDublicates($dbh,$sth2,$orders_count2);
    }


    $sth3 = $dbh->prepare("SELECT crmid,setype
    FROM vtiger_salesorder 
    JOIN vtiger_crmentity  ON crmid=salesorderid
    WHERE deleted=0 AND setype NOT IN ('SalesOrder','Invoice','PurchaseOrder')
    GROUP BY crmid");

   $sth3->setFetchMode(PDO::FETCH_ASSOC);
   $sth3->execute(array());
   $orders_count3 = $sth3->rowCount();

   if($orders_count3 > 0){
   checkDublicates($dbh,$sth3,$orders_count3);
   }




   $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error!";
   echo $e->getMessage();
}

function checkDublicates($dbh,$sth,$orders_count){
   $insert_history = $dbh->prepare("INSERT INTO app_check_dublicates (module, crmid, new_crmid, place, date) VALUES (?,?,?,?,?)");

 

   $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
   $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count); 
   
   $date = date("Y-m-d H:i:s");

   while ($row = $sth->fetch()) {  
      
      $sth6->execute(array($last_entity_record)); 
      $record_exist = $sth6->rowCount();

      if($record_exist) {
         $last_entity_record_old = $last_entity_record;        
         $last_entity_record = get_last_entity_id_and_update($dbh,1); 
         $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
         $sth4->execute(array($last_entity_record));     
      }    


      if($row['setype'] == 'Accounts'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh);
         changeAccountId($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date));
      }elseif($row['setype'] == 'Vendors'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh);
         changeVendorId($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date));
      }elseif($row['setype'] == 'SalesOrder'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh);
         changeSalesOrderId($row['crmid'],$last_entity_record,$dbh);
         changeInventoryProductRelId($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date));
      }elseif($row['setype'] == 'Payment'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh);
         changeCrmentityRelId($row['setype'],$row['crmid'],$last_entity_record,$dbh);
         changeDebtId($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date));
      }elseif($row['setype'] == 'Documents'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh);            
         changeDocumentsId($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date));
      }elseif($row['setype'] == 'Documents Attachment'){
         // changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh);            
         // changeDocumentsAttachmentId($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date));
      }elseif($row['setype'] == 'Purchaseorderdraft'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh);            
         changePurchaseOrderDraftId($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date));
      }elseif($row['setype'] == 'Routepoints'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh); 
         changeCrmentityRelId($row['setype'],$row['crmid'],$last_entity_record,$dbh);  
         changeRoutePoint($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date)); 
      }elseif($row['setype'] == 'HelpDesk'){
         changeCrmentityId($row['setype'],$row['crmid'],$last_entity_record,$dbh); 
         changeCrmentityRelId($row['setype'],$row['crmid'],$last_entity_record,$dbh);  
         changeHelpDesk($row['crmid'],$last_entity_record,$dbh);
         $insert_history->execute(array($row['setype'],$row['crmid'],$last_entity_record,'Cron',$date)); 
      }    

      if($record_exist) {             
         $last_entity_record = $last_entity_record_old;    
      }         

      $last_entity_record++;
   }
}


function changeCrmentityId($module,$crmid,$new_crmid,$dbh){
   $update = $dbh->prepare("UPDATE vtiger_crmentity SET crmid = ?, label = '' WHERE crmid = ? AND setype = ?");
   $update->execute(array($new_crmid,$crmid,$module));   
}

function changeCrmentityRelId($relmodule,$crmid,$new_crmid,$dbh){
   $update = $dbh->prepare("UPDATE vtiger_crmentityrel SET crmid = ? WHERE crmid = ? AND module = ?");
   $update->execute(array($new_crmid,$crmid,$relmodule));   
}

function changeDebtId($paymentid,$new_paymentid,$dbh){
   $update = $dbh->prepare("UPDATE vtiger_payment SET paymentid = ? WHERE paymentid = ?");
   $update->execute(array($new_paymentid,$paymentid));   

   $update_paymentcf = $dbh->prepare("UPDATE vtiger_paymentcf SET paymentid = ? WHERE paymentid = ?");
   $update_paymentcf->execute(array($new_paymentid,$paymentid)); 
}

function changeRoutePoint($routepointsid,$new_routepointsid,$dbh){
   $update = $dbh->prepare("UPDATE vtiger_routepoints SET routepointsid = ? WHERE routepointsid = ?");
   $update->execute(array($new_routepointsid,$routepointsid));   

   $update_paymentcf = $dbh->prepare("UPDATE vtiger_routepointscf SET routepointsid = ? WHERE routepointsid = ?");
   $update_paymentcf->execute(array($new_routepointsid,$routepointsid)); 
}


function changeHelpDesk($ticketid,$new_ticketid,$dbh){
   $update = $dbh->prepare("UPDATE vtiger_troubletickets SET ticketid = ? WHERE ticketid = ?");
   $update->execute(array($new_ticketid,$ticketid));   

   $update_paymentcf = $dbh->prepare("UPDATE vtiger_ticketcf SET ticketid = ? WHERE ticketid = ?");
   $update_paymentcf->execute(array($ticketid,$ticketid)); 
}

function changeSalesOrderId($salesorderid,$new_salesorderid,$dbh){
   $update_salesorder = $dbh->prepare("UPDATE vtiger_salesorder SET salesorderid = ? WHERE salesorderid = ?");
   $update_salesorder->execute(array($new_salesorderid,$salesorderid));   

   $update_salesordercf = $dbh->prepare("UPDATE vtiger_salesordercf SET salesorderid = ? WHERE salesorderid = ?");
   $update_salesordercf->execute(array($new_salesorderid,$salesorderid));   

   $update_sobillads = $dbh->prepare("UPDATE vtiger_sobillads SET sobilladdressid = ? WHERE sobilladdressid = ?");
   $update_sobillads->execute(array($new_salesorderid,$salesorderid)); 

   $update_soshipads = $dbh->prepare("UPDATE vtiger_soshipads SET soshipaddressid = ? WHERE soshipaddressid = ?");    
   $update_soshipads->execute(array($new_salesorderid,$salesorderid));   
}

function changeInventoryProductRelId($id,$new_id,$dbh){
   $update_inventoryproductrel = $dbh->prepare("UPDATE vtiger_inventoryproductrel SET id = ? WHERE id = ? AND external_order_id IS NOT NULL");
   $update_inventoryproductrel->execute(array($new_id,$id));   
}

function changeAccountId($accountid,$new_accountid,$dbh){
   $update_account = $dbh->prepare("UPDATE vtiger_account SET accountid = ? WHERE accountid = ?");
   $update_account->execute(array($new_accountid,$accountid));   

   $update_accountscf = $dbh->prepare("UPDATE vtiger_accountscf SET accountid = ? WHERE accountid = ?");  
   $update_accountscf->execute(array($new_accountid,$accountid));   
}

function changeVendorId($vendorid,$new_vendorid,$dbh){
   $update_account = $dbh->prepare("UPDATE vtiger_vendor SET vendorid = ? WHERE vendorid = ?");
   $update_account->execute(array($new_vendorid,$vendorid));   

   $update_accountscf = $dbh->prepare("UPDATE vtiger_vendorcf SET vendorid = ? WHERE vendorid = ?");  
   $update_accountscf->execute(array($new_vendorid,$vendorid));   
}


function changeDocumentsId($notesid,$new_notesid,$dbh){
   $update_notes = $dbh->prepare("UPDATE vtiger_notes  SET notesid = ? WHERE notesid = ?");
   $update_notes->execute(array($notesid, $new_notesid));

   $update_notescf = $dbh->prepare("UPDATE vtiger_notescf SET notesid = ? WHERE notesid = ?");
   $update_notescf->execute(array($notesid, $new_notesid));

   $update_notesrel = $dbh->prepare("UPDATE vtiger_senotesrel SET notesid = ? WHERE notesid = ?");
   $update_notesrel->execute(array($notesid, $new_notesid));
   
   $update_seattachmentsrel = $dbh->prepare("UPDATE vtiger_seattachmentsrel SET crmid = ? WHERE crmid = ?");
   $update_seattachmentsrel->execute(array($notesid, $new_notesid));   

}

function changeDocumentsAttachmentId($attachmentsid,$new_attachmentsid,$dbh){
   $update_attachments = $dbh->prepare("UPDATE vtiger_attachments  SET attachmentsid = ? WHERE attachmentsid = ?");
   $update_attachments->execute(array($attachmentsid, $new_attachmentsid));

   $update_seattachmentsrel = $dbh->prepare("UPDATE vtiger_seattachmentsrel SET attachmentsid = ? WHERE attachmentsid = ?");
   $update_seattachmentsrel->execute(array($attachmentsid, $new_attachmentsid));  
}

function changePurchaseOrderDraftId($purchaseorderdraftid,$new_purchaseorderdraftid,$dbh){
   $update_attachments = $dbh->prepare("UPDATE vtiger_purchaseorderdraft  SET purchaseorderdraftid = ? WHERE purchaseorderdraftid = ?");
   $update_attachments->execute(array($purchaseorderdraftid, $new_purchaseorderdraftid));

   $update_seattachmentsrel = $dbh->prepare("UPDATE vtiger_purchaseorderdraftcf SET attachmentsid = ? WHERE attachmentsid = ?");
   $update_seattachmentsrel->execute(array($purchaseorderdraftid, $new_purchaseorderdraftid));  
}

?>
