<?php
include 'ws.config.php';
global $config;
require 'utils.php';
include 'getPriceFromPriceBook.php';
set_time_limit(500);

error_reporting(E_WARNING & ~E_NOTICE & ~E_DEPRECATED) ;

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();    

                   

$sth = $dbh->prepare("SELECT a.accountname, t.*, GROUP_CONCAT(l.ordered) AS ordered, GROUP_CONCAT(l.revised) AS revised, GROUP_CONCAT(l.quantity) AS quantity, a.accountid, SUM(l.weight)  AS weight , SUM(l.length * l.width * l.height)  as volume, SUM(l.length * l.width)  as square, ROUND(SUM(l.m3),3) AS m3, l.pll, c.salesorderid, ROUND(shipment_direct_km,2) AS shipment_km,load_country_code,unload_country_code
                                                        FROM app_orders2 t
                                                        JOIN vtiger_account a ON a.customer_id = t.customer_id 
                                                        JOIN vtiger_crmentity e ON e.crmid = a.accountid     
                                                        JOIN app_loads2 l ON l.order_id = t.id           
                                                        JOIN vtiger_salesorder c ON c.external_order_id = t.id  
                                                         JOIN vtiger_crmentity s ON s.crmid = c.salesorderid     
                                                        WHERE e.deleted = 0 AND c.accountid = 2762 
                                                        GROUP BY l.order_id");



    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                            WHERE salesorderid = ?');


    $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = ?, cf_1365 = ?, cf_1367 = ?, cf_1374 = ?, cf_1376 = ? WHERE   salesorderid = ? '); 
    $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ? WHERE id = ? '); 
    $sth5 = $dbh->prepare('UPDATE vtiger_crmentity  SET modifiedtime = ? WHERE crmid = ? '); 

    while ($row = $sth->fetch()) {

     
  set_time_limit(0);
        $id = $row['accountid'];
        $weight = $row['weight'];
        $volume = ($row['m3'] > $row['volume'] ? $row['m3'] : $row['volume']);
        $square = $row['square'];
        $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
        $row['load_post_code'] = preg_replace("/[^0-9]/", "",$row['load_post_code']);
        $row['unload_post_code'] = preg_replace("/[^0-9]/", "",$row['unload_post_code']);

        $date = date("Y-m-d");
        $date2 = date("Y-m-d H:i:s");
        $logfile = "count_price_$date.log";

        $quantity = explode(",", $row['quantity']);
       

        $ordered = explode(",",$row['ordered']);   
        
        for($i = 0; $i < count($ordered); $i++){
           $exploded_arr = explode(' ',$ordered[$i]);
           $weight_ordered[] = $exploded_arr[0];
           $dim_ordered[] = (array_product(explode('x', $exploded_arr[1]))) * $quantity[$i];     
        }

        $revised = explode(",",$row['revised']);
        
        for($i = 0; $i < count($revised); $i++){
           $exploded_arr2 = explode(' ',$revised[$i]);
           $weight_revised[] = $exploded_arr2[0];
           $dim_revised[] = (array_product(explode('x', $exploded_arr2[1]))) * $quantity[$i];     
        }


        $ordered_cargo_wgt = $exploded_arr[0];
        $revised_cargo_wgt = $exploded_arr2[0];

        $ordered_dim = explode('x', $exploded_arr[1]);
        $revised_dim = explode('x', $exploded_arr2[1]);

        $ordered_w = array_sum($weight_ordered);
        $ordered_m3 = array_sum($dim_ordered);

        $revised_w = array_sum($weight_revised);
        $revised_m3 = array_sum($dim_revised);

        // if(!empty($row['load_post_code']) AND !empty($row['unload_post_code']) AND (!empty($ordered_w) OR !empty($revised_w)) AND ($row['load_country_code'] != "EST" AND $row['unload_country_code'] != "EST") ){
        //     $getPrice_ordered = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$ordered_w,$ordered_m3, $square,$pll,$distance,$row['load_country_code'],$row['unload_country_code']);  

        //     $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $square,$pll,$distance,$row['load_country_code'],$row['unload_country_code']);  


        //     print_R($getPrice_ordered);
        //     print_R($getPrice_revised);

        //     if($getPrice_ordered['price'] > $getPrice_revised['price']){
        //         $getPrice['price'] = $getPrice_ordered['price'];
        //         $getPrice['pricebook'] =  $getPrice_ordered['combination'];
        //         $cargo_wgt = $ordered_cargo_wgt;
        //         $cargo_length = $ordered_dim[0];
        //         $cargo_width = $ordered_dim[1];
        //         $cargo_height = $ordered_dim[2];
        //     }elseif($getPrice_ordered['price'] < $getPrice_revised['price']){
        //         $getPrice['price'] = $getPrice_revised['price'];
        //         $getPrice['pricebook'] =  $getPrice_revised['combination'];
        //         $cargo_wgt = $revised_cargo_wgt;
        //         $cargo_length = $revised_dim[0];
        //         $cargo_width = $revised_dim[1];
        //         $cargo_height = $revised_dim[2];
        //     }elseif($getPrice_ordered['price'] == $getPrice_revised['price']){
        //         if($ordered_w <= $revised_w && $ordered_m3 <= $revised_m3){
        //             $getPrice['price'] = $getPrice_revised['price'];
        //             $getPrice['pricebook'] =  $getPrice_revised['combination'];
        //             $cargo_wgt = $revised_cargo_wgt;
        //             $cargo_length = $revised_dim[0];
        //             $cargo_width = $revised_dim[1];
        //             $cargo_height = $revised_dim[2];
        //         }elseif($ordered_w <= $revised_w && $ordered_m3 >= $revised_m3){
        //             $getPrice['price'] = $getPrice_ordered['price'];
        //             $getPrice['pricebook'] =  $getPrice_ordered['combination'];
        //             $cargo_wgt = $ordered_cargo_wgt;
        //             $cargo_length = $ordered_dim[0];
        //             $cargo_width = $ordered_dim[1];
        //             $cargo_height = $ordered_dim[2];
        //         }elseif($ordered_w >= $revised_w && $ordered_m3 >= $revised_m3){
        //             $getPrice['price'] = $getPrice_ordered['price'];
        //             $getPrice['pricebook'] =  $getPrice_ordered['combination'];
        //             $cargo_wgt = $ordered_cargo_wgt;
        //             $cargo_length = $ordered_dim[0];
        //             $cargo_width = $ordered_dim[1];
        //             $cargo_height = $ordered_dim[2];
        //         }elseif($ordered_w >= $revised_w && $ordered_m3 <= $revised_m3){ 
        //             $getPrice['price'] = $getPrice_revised['price'];
        //             $getPrice['pricebook'] =  $getPrice_revised['combination'];
        //             $cargo_wgt = $revised_cargo_wgt;
        //             $cargo_length = $revised_dim[0];
        //             $cargo_width = $revised_dim[1];
        //             $cargo_height = $revised_dim[2];   
        //         }
        //     }
        // }else{ 
        //     $getPrice = array('price' => 0);

        //     if(empty($row['load_post_code']) AND empty( $row['unload_post_code']) AND empty($weight)){
        //         $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
        //     }elseif(empty($row['load_post_code']) AND empty($row['unload_post_code'])){    
        //         $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
        //     }elseif(empty($row['load_post_code'])){
        //         $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
        //     }elseif(empty($row['unload_post_code'])){
        //         $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
        //     }elseif(empty($weight)){
        //             $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
        //     }elseif($row['load_country_code'] == "EST" OR $row['unload_country_code'] == "EST"){
        //         $getPrice = array('pricebook' => 'Estija','price' => 0);
        //      }
           
        // }          
        //  customers_log('Paskaiciavimas '.$row['id'].' '.$getPrice['pricebook'] .' '. $getPrice['price'], $logfile);   

        // print_R($getPrice);

        //  if(empty($row['price_agreed'])){
        //  	$price = $getPrice['price'];                        
        //  }else{
        //  	$price = $row['price_agreed'];
        //  } 

        

         // $margin = $getPrice['price'];

                    // $sth2->execute(array(
                    //                     $price,
                    //                     $price,                                                     
                    //                     $price,                                      
                    //                     $row['salesorderid']
                    // ));     

                     $sth3->execute(array(                       
                        $getPrice['pricebook'],
                        $row['m3'],
                        number_format($row['volume'],3),
                        $getPrice['price'],
                        $row['price_agreed'],
                        $row['salesorderid']          
                    ));    
                    
                    
         //            $sth4->execute(array(                                                                                          
         //                                $margin,                                                                        
         //                                $row['salesorderid']
         //            ));

         //            $sth5->execute(array(                                                                                          
         //                                $date2,                                                                        
         //                                $row['salesorderid']
         //            ));
                   
                 
                
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
