<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_assets (assetsid,
                                                                asset_no,
                                                                serialnumber,
                                                                dateinservice,
                                                                assetname,
                                                                user_id,
                                                                id,
                                                                driver1_id,
                                                                driver2_id,                                                     
                                                                import_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');    
    
    $sth = $dbh->prepare("SELECT t.*, u.id AS user1_id, y.id AS user2_id
                            FROM app_vehicles t
                            LEFT JOIN vtiger_users u ON u.title = t.driver1_id 
                            LEFT JOIN vtiger_users y ON y.title = t.driver2_id                         
                            WHERE t.vehicle_id NOT IN (SELECT c.id FROM vtiger_assets c, vtiger_crmentity d WHERE d.crmid = c.assetsid AND d.deleted = 0 AND c.id IS NOT NULL)");
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());
    
    while ($row = $sth->fetch()) {
            
            if(empty($row['user1_id']) || $row['user1_id'] == NULL) {
            $user_id = 26;
            } else {
            $user_id = $row['user1_id']; 
            }            
 
            if(empty($row['user2_id']) || $row['user2_id'] == NULL) {
            $user2_id = 26;
            } else {
            $user2_id = $row['user2_id']; 
            }                           
    
            $date = date("Y-m-d H:i:s");  

                 
                    $last_entity_record = last_entity_record($dbh);
                    $insert_entity = insert_entity($dbh, 'Assets', $last_entity_record, $user_id, NULL, $row['registration_number'], $date);                 
                    
                    $sth2->execute(array($last_entity_record, 
                                        $row['vehicle_id'], 
                                        $row['trailer_registration_number'], 
                                        $date,                               
                                        $row['registration_number'],
                                        $user2_id,
                                        $row['vehicle_id'],
                                        $row['driver1_id'],      
                                        $row['driver2_id'], 
                                        $date            
                    ));     
            
    }

    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error!";
   echo $e->getMessage();
}

?>