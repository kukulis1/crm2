<?php

require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function get_last_entity_id_and_update($dbh,$addEntity){
    // NOTE Numeracija papildom užsakymu kiekiu
    $sth3 = $dbh->prepare("SELECT id FROM `vtiger_crmentity_seq`");
    $sth3->setFetchMode(PDO::FETCH_ASSOC);
    $sth4 = $dbh->prepare("UPDATE `vtiger_crmentity_seq` SET id = ?");

    $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq READ'); // Uzrakinama lentele, kad joks kitas irasas nepasimtu id
    $lock->execute(array());     
    $sth3->execute(array());
    $seq = $sth3->fetch(); // Gaunam paskutini id
    $lock2 = $dbh->prepare('UNLOCK TABLES');
    $lock2->execute(array());

    $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq WRITE');
    $lock->execute(array());
    $new_seq = $seq['id'] + $addEntity;    // Pridedam prie paskutinio id uzsakymu kieki 
    $sth4->execute(array($new_seq)); 
    $lock2 = $dbh->prepare('UNLOCK TABLES');
    $lock2->execute(array());

    return $seq['id']+1;
}

function insert_senotesrel($dbh, $entity_id, $moduleId){
  $sth = $dbh->prepare("INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES (?,?)");
  $sth->execute(array($moduleId, $entity_id));
}

function insertNotes($dbh, $entity_id,$title,$filename,$description,$folderid,$locationType,$filestatus){
  $sth = $dbh->prepare("INSERT INTO vtiger_notes (notesid, title, `filename`, notecontent, folderid, filelocationtype, filestatus) 
                           VALUES (?,?,?,?,?,?,?)");
  $sth->execute(array($entity_id, $title, $filename,$description, $folderid,$locationType,$filestatus));  
}

function insert_NoteCf($dbh, $entity_id,$file_created){
  $sth = $dbh->prepare("INSERT INTO vtiger_notescf (notesid,cf_1852) VALUES (?, ?)");
  $sth->execute(array($entity_id, $file_created));
}


function deleteShipmentPackages($dbh,$salesorderid){  
    $get_package_id = $dbh->prepare("SELECT packagesid FROM vtiger_packages WHERE cf_20023 IN ($salesorderid)");    

    $get_package_id->setFetchMode(PDO::FETCH_ASSOC);
    $get_package_id->execute([]);

    $packagesid = [];
    if($get_package_id->rowCount()){

      foreach($get_package_id AS $row){   
        $packagesid[] = $row['packagesid'];
      }

      $packagesid = implode(',', $packagesid);

      $delete_crmentity = $dbh->prepare("DELETE FROM vtiger_crmentity WHERE crmid IN ($packagesid) AND setype = 'Packages'");
      $delete_crmentity->execute();

      $delete_packages = $dbh->prepare("DELETE FROM vtiger_packages WHERE packagesid IN ($packagesid)");
      $delete_packages->execute();

      $delete_packagescf = $dbh->prepare("DELETE FROM vtiger_packagescf WHERE packagesid IN ($packagesid)");
      $delete_packagescf->execute(); 
    }    
}


function getTable($table, $dbh, $where,$insert_query,$execute){  

    $sth = $dbh->prepare($insert_query);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'table' => $table, 'where' => $where));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);
    $values = array();
    foreach($data['response']['result'] AS $row){       
        for($i =0; $i < count($execute); $i++){
            $values[$i] = $row[$execute[$i]];
        }
        $sth->execute($values);
    }
}


function getTableWithSelectSimple($select,$table, $dbh, $where,$insert_query,$check_array,$execute,$execute2){  
    
    $sth = $dbh->prepare($insert_query);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);
    $values = array(); 

    if(!empty($data['response']['result'])){
    
        foreach($data['response']['result'] AS $row){   
            $route_id = $row[$execute2];        
            for($i =0; $i < count($execute); $i++){                 
            if(empty($check_array[$route_id])){                          
                $values[$i] = $row[$execute[$i]];
            }
            }   

            if($values){
            $sth->execute($values);
            }
        }

        return true;  
    } else{
        return false;
    }
}

function getTableInfo($table, $dbh, $where,$insert_query,$check_query,$execute,$execute2){   

    $sth = $dbh->prepare($insert_query);
    $sth2 = $dbh->prepare($check_query);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'table' => $table, 'where' => $where));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);
    $values = array();
    $shipment_id = array();


    foreach($data['response']['result'] AS $row){
        $shipment_id = $row[$execute2];
        $sth2->execute(array($shipment_id));
        for($i =0; $i < count($execute); $i++){  
           if(!$sth2->rowCount()){
              $values[$i] = ($row[$execute[$i]] ?: 0);
           }
        }        
       
        if($values){
          $sth->execute($values);
        }
    }
}

function getTableWithSelectInfo($select,$table, $dbh, $where,$insert_query,$check_query,$update_query,$execute,$execute2){   

    $sth = $dbh->prepare($insert_query);
    $sth2 = $dbh->prepare($check_query);
    $sth3 = $dbh->prepare($update_query);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas',  'select' => $select, 'table' => $table, 'where' => $where));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);
    $values = array();
    $values2 = array();
    $shipment_id = array();

    foreach($data['response']['result'] AS $row){      
        $shipment_id = array();
        for($e =0; $e < count($execute2); $e++){
            $shipment_id[] = $row[$execute2[$e]];
        }         
        $sth2->execute($shipment_id);
        for($i =0; $i < count($execute); $i++){  
           if(!$sth2->rowCount()){
              $values[$i] = $row[$execute[$i]];
           }else{              
              $values2[$i] = $row[$execute[$i]];            
           }
        }     
 
        $values2[]= array_shift($values2);
        
        if($values){
          $sth->execute($values);
        }
        if($values2){
         $sth3->execute($values2); 
        }
    
    }
}

function change_minutes($minute){
    if($minute < 15){
      $changed = "00";
    }elseif($minute < 30){
      $changed = "15";
    }elseif($minute < 45){
      $changed = "30";  
    }elseif($minute >= 45){
      $changed = "45"; 
    }
  
    return $changed;
}

function change_minutes_of_half($minute){
    if($minute < 30){
      $changed = "00";
    }elseif($minute >= 30){
      $changed = "30";    
    }
  
    return $changed;
}

function createNotification($dbh,$recordid,$reason,$type,$follow){

    $new_seq = get_last_entity_id_and_update($dbh,1);  

    if(in_array($follow,array('kokybe'))){
        $sth5 = $dbh->prepare("SELECT shipment_code FROM vtiger_salesorder WHERE salesorderid = ?");
    }else if($follow == 'pretenzijos'){
        $sth5 = $dbh->prepare("SELECT claimsno AS shipment_code FROM vtiger_claims WHERE claimsid = ?");
    }else if($follow == 'pirmas_uzsakymas'){
        $sth5 = $dbh->prepare("SELECT accountname AS shipment_code FROM vtiger_account WHERE accountid = ?");
    }else if($follow == 'tiekeju_sutartys'){ 
        $sth5 = $dbh->prepare("SELECT vendorname AS shipment_code FROM vtiger_vendor WHERE vendorid = ?");
    }

    $sth5->setFetchMode(PDO::FETCH_ASSOC);
    $sth5->execute(array($recordid));
    $shipment_code = $sth5->fetch()['shipment_code'];

    $source = (in_array($follow,array('kokybe')) ? 'Metrika' : 'CRM');

    insert_entity3($dbh, 'Notification', $new_seq, 26, NULL,$source, $shipment_code, date("Y-m-d H:i:s"));

    $sth6 = $dbh->prepare('INSERT INTO vtiger_notification (notificationid, notificationno, notification_tks_notes, notification_tks_relatedto, notification_tks_whodid) VALUES (?,?,?,?,?)'); 
    $sth7 = $dbh->prepare('INSERT INTO vtiger_notificationcf (notificationid, cf_2044,cf_2046) VALUES (?,?,?)'); 
    
    if(in_array($follow,array('kokybe'))){
        if($type == 'status'){
            $description = "būsena pasikeitė į nepriduotas, nes ";    
        }elseif($type == 'marks'){
            $description = " pridėtos pristatymo pastabos:  ";  
        }elseif($type == 'comments'){
            $description = " pridėti pristatymo komentarai:  ";  
        }elseif($type == 'idle'){
            $description = " pridėtos prastovos:  ";  
        }
    }else if($follow == 'pretenzijos'){
        $description = $reason;
        $reason = '';
    }else if($follow == 'pirmas_uzsakymas'){
        $description = $reason;
        $reason = '';
    }else if($follow == 'tiekeju_sutartys'){
        $description = $reason;
        $reason = '';
    }

    $whodid = (in_array($follow,array('kokybe')) ? 26 : $type);

    $sth6->execute(array($new_seq,$shipment_code,$description,$recordid,$whodid));
    $sth7->execute(array($new_seq,$reason,$follow));
}

function sendPriceToMetrika($data){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders_price.php");           
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders_price' => $data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);    
    curl_exec($ch);
    curl_close($ch);
}


function insert_modtracker_basic($dbh,$id,$orderId,$module,$userId,$date,$status){
    $result = $dbh->prepare("INSERT INTO vtiger_modtracker_basic (id, crmid, module, whodid, changedon,status) VALUES (?,?,?,?,?,?)");
    $result->execute(array($id,$orderId,$module,$userId,$date,$status));
}

function insert_modtracker_detail($dbh, $id, $field, $oldValue, $newValue){
    $result = $dbh->prepare("INSERT INTO vtiger_modtracker_detail (id, fieldname, prevalue, postvalue) VALUES (?,?,?,?)");
    $result->execute(array($id, $field, $oldValue, $newValue));
}

function update_modtracker_basic_seq($dbh, $id){
    $result = $dbh->prepare("UPDATE vtiger_modtracker_basic_seq SET id = ?"); 
    $result->execute(array($id)); 
}

function last_modtracker_record($dbh)
{
  $last_id = getlast_modtracker_record($dbh);
  $res = $dbh->prepare('SELECT id FROM vtiger_modtracker_basic WHERE id = ?');
  $res->setFetchMode(PDO::FETCH_ASSOC);

  $res->execute(array($last_id));
  $record_exist = $res->rowCount();

  if($record_exist){
    $last_id = getlast_modtracker_record($dbh);
  }  

  return $last_id;
}

function getlast_modtracker_record($dbh){
    $result = $dbh->prepare('SELECT id FROM vtiger_modtracker_basic_seq');
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $result->execute(array());
    $id = $result->fetch();   
    $crmid = $id['id']+1;    
    update_modtracker_basic_seq($dbh, $crmid);
    return $crmid;
}


function last_entity_record($dbh) {

    $sth = $dbh->query('SELECT * FROM vtiger_crmentity_seq');

    while ($row = $sth->fetch()) {
        $crmid = $row['id'] + 1;
    }  

    return $crmid;
    
}

function last_entity_record2($dbh) {

    $sth = $dbh->query('SELECT * FROM vtiger_crmentity_seq');

    $row = $sth->fetch();
    $crmoldid = $row['id'];
    $crmid = $row['id'] + 1;


     // Update sequence values
     $sth2 = $dbh->prepare('UPDATE vtiger_crmentity_seq SET id = ?');
     $sth2->execute(array($crmid));

     $sth3 = $dbh->query('SELECT id FROM vtiger_crmentity_seq');
     $crmnewid = $sth3->fetch();

    
    if($crmoldid < $crmnewid['id']){
        return $crmid;
    }
}

function insert_entity($dbh, $setype, $entity_id, $user_id, $description, $label, $date) {

    // Insert into vtiger_crmentity 
    $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, `description`, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date);
    $sth->execute($sql_params);

    // Update sequence values
    $sth = $dbh->prepare('UPDATE vtiger_crmentity_seq SET id = ?');
    $sth->execute(array($entity_id));
}

function insert_entity3($dbh, $setype, $entity_id, $user_id, $description,$source, $label, $date) {

    // Insert into vtiger_crmentity 
    $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, `description`,source, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($entity_id, $user_id, $user_id, $setype, $description,$source, $label, $date, $date);
    $sth->execute($sql_params);

}

function insert_entityrel($dbh, $crmid, $module, $relcrmid, $relmodul) {

    // Insert into vtiger_crmentity 
    $sth = $dbh->prepare('INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES (?, ?, ?, ?)');
    $sql_params = array($crmid, $module, $relcrmid, $relmodul);
    $sth->execute($sql_params);

}


function insert_entity2($dbh, $setype, $user_id, $description, $label, $date) {

    // Insert into vtiger_crmentity 
    $entity_id = last_entity_record2($dbh);
    if($entity_id){

        $sth2 = $dbh->query("SELECT * FROM vtiger_crmentity WHERE crmid = $entity_id");
        $crmnewid = $sth2->rowCount();
        
        // Insert into vtiger_crmentity 
       
        $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, source, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
        $sql_params = array($entity_id, $user_id, $user_id, $setype, $description,'Metrika', $label, $date, $date);

        if(!$crmnewid){
            $sth->execute($sql_params);
        }


        // // Update sequence values
        // $sth = $dbh->prepare('UPDATE vtiger_crmentity_seq SET id = ?');
        // $sth->execute(array($entity_id));

        return  $entity_id;
    }
}


function update_entity($dbh, $entity_id, $description, $label, $date) {

    $sth = $dbh->prepare('UPDATE vtiger_crmentity SET `description` = ?, label = ?, modifiedtime = ? WHERE crmid = ?');
    $sql_params = array($description, $label, $date, $entity_id);
    $sth->execute($sql_params);

}


function insert_customer_address_billing($dbh, $accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name) {

    $sth = $dbh->prepare('INSERT INTO vtiger_accountbillads (accountaddressid, 
                                                                    bill_street,
                                                                    bill_code,
                                                                    location_id,
                                                                    bill_city, 
                                                                    bill_country,
                                                                    `name`) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name);
    $sth->execute($sql_params);
            
}

function insert_customer_address_shipping($dbh, $accountaddressid, $ship_sreet, $ship_code, $location_id, $ship_city, $ship_country, $name) {

    $sth = $dbh->prepare('INSERT INTO vtiger_accountshipads (accountaddressid, 
                                                                    ship_street,
                                                                    ship_code,
                                                                    location_id,
                                                                    ship_city, 
                                                                    ship_country,
                                                                    `name`) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($accountaddressid, $ship_sreet, $ship_code, $location_id, $ship_city, $ship_country, $name);
    $sth->execute($sql_params);
           
}


function update_customer_address_billing($dbh, $accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name) {

    $sth = $dbh->prepare('UPDATE vtiger_accountbillads SET bill_street = ?, bill_code = ?, location_id = ?, bill_city = ?, bill_country = ?, `name` = ? WHERE accountaddressid = ?');
    $sql_params = array($bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name, $accountaddressid);
    $sth->execute($sql_params);

}

function update_customer_address_shipping($dbh, $accountaddressid, $bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name) {

    $sth = $dbh->prepare('UPDATE vtiger_accountshipads SET ship_street = ?, ship_code = ?, location_id = ?, ship_city = ?, ship_country = ?, `name` = ? WHERE accountaddressid = ?');
    $sql_params = array($bill_sreet, $bill_code, $location_id, $bill_city, $bill_country, $name, $accountaddressid);
    $sth->execute($sql_params);

}

function insert_load_address($dbh, $sobilladdressid, $bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone) {

    $sth = $dbh->prepare('INSERT INTO vtiger_sobillads (sobilladdressid, 
                                                                    bill_street,
                                                                    bill_code,
                                                                    bill_city, 
                                                                    bill_country,
                                                                    load_company,
                                                                    load_company_code,
                                                                    load_contact,
                                                                    load_phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($sobilladdressid, $bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone);
    $sth->execute($sql_params);           
   
}

function insert_unload_address($dbh, $soshipaddressid, $ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone) {

    $sth = $dbh->prepare('INSERT INTO vtiger_soshipads (soshipaddressid, 
                                                                    ship_street,
                                                                    ship_code,
                                                                    ship_city, 
                                                                    ship_country,
                                                                    unload_company,
                                                                    unload_company_code,
                                                                    unload_contact,
                                                                    unload_phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $sql_params = array($soshipaddressid, $ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone);
    $sth->execute($sql_params);
           
}


function update_load_address($dbh, $sobilladdressid, $bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone) {

    $sth = $dbh->prepare('UPDATE vtiger_sobillads SET bill_street = ?, bill_code = ?, bill_city = ?, bill_country = ?, load_company = ?, load_company_code = ?, load_contact = ?, load_phone = ? WHERE sobilladdressid = ?');
    $sql_params = array($bill_sreet, $bill_code, $bill_city, $bill_country, $load_company, $load_company_code, $load_contact, $load_phone, $sobilladdressid);
    $sth->execute($sql_params);

}

function update_unload_address($dbh, $soshipaddressid, $ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone) {

    $sth = $dbh->prepare('UPDATE vtiger_soshipads SET ship_street = ?, ship_code = ?, ship_city = ?, ship_country = ?, unload_company = ?, unload_company_code = ?, unload_contact = ?, unload_phone = ? WHERE soshipaddressid = ?');
    $sql_params = array($ship_sreet, $ship_code, $ship_city, $ship_country, $unload_company, $unload_company_code, $unload_contact, $unload_phone, $soshipaddressid);
    $sth->execute($sql_params);

}

function update_fail_reason($dbh, $sobilladdressid, $sostatus, $reason){
    if(empty($sostatus)){
        $sth = $dbh->prepare('UPDATE vtiger_salesorder SET carrier = ? WHERE salesorderid = ?'); 
        $sql_params = array($reason, $sobilladdressid);
    } else {
        $sth = $dbh->prepare('UPDATE vtiger_salesorder SET carrier = ?, sostatus=? WHERE salesorderid = ?'); 
        $sql_params = array($reason, $sostatus, $sobilladdressid);
    }
    $sth->execute($sql_params);
}

function update_fail_reason_pricing($dbh, $fail_id, $fail_reason, $soid){
    $org_price = $dbh->prepare("SELECT cf_1374 as default_price FROM vtiger_salesordercf WHERE salesorderid = ?");
    $org_price->setFetchMode(PDO::FETCH_ASSOC);     
    $org_price->execute(array($soid));
    $first_price = $org_price->fetch()['default_price'];
    
    $account_query = $dbh->prepare("SELECT vtiger_accountscf.cf_2089 as neteisingas_adresas,
                                    vtiger_accountscf.cf_2091 as negalimas_privaziavimas,
                                    vtiger_accountscf.cf_2093 as neparuostas_krovinys,
                                    vtiger_accountscf.cf_2095 as netinkamai_supakuota,
                                    vtiger_accountscf.cf_2097 as neatitinka_uzsakymo,
                                    vtiger_accountscf.cf_2099 as nebuvo_vietoje
                                    FROM vtiger_accountscf
                                    LEFT JOIN vtiger_salesorder ON vtiger_salesorder.accountid = vtiger_accountscf.accountid
                                    WHERE vtiger_salesorder.salesorderid = ?");
    $account_query->setFetchMode(PDO::FETCH_ASSOC);     
    $account_query->execute(array($soid));
    $account_results = $account_query->fetchAll();
    $other_settings_query = $dbh->prepare("SELECT value, title 
                                            FROM app_other_settings
                                            WHERE title = 'LBL_WRONG_ADDRESS_PRICE'
                                            OR title = 'LBL_BLOCKED_ENTRANCE_PRICE'
                                            OR title = 'LBL_NOT_PREPARED_PRICE'
                                            OR title = 'LBL_WRONG_PACKAGE_PRICE'
                                            OR title = 'LBL_BAD_PACKAGE_PRICE'
                                            OR title = 'LBL_NOT_PRESENT_PRICE'");
    $other_settings_query->setFetchMode(PDO::FETCH_ASSOC);     
    $other_settings_query->execute(); 
    $other_settings_values= $other_settings_query->fetchAll();
    $formated_array = [];
    foreach($other_settings_values as $value){
        $formated_array[$value['title']] = $value['value'];
    }
    $price = 0;
    $service_id = 0;
    if($fail_id == 1){
        $acc_query_val = $account_results[0]['neteisingas_adresas'];
        $service_id = 9;
        if(empty($acc_query_val)){
            $price = $first_price + (($first_price / 100) * $formated_array['LBL_WRONG_ADDRESS_PRICE']);
        } else {
            $price = $first_price + (($first_price / 100) * $acc_query_val);
        }

    } else if($fail_id == 2){
        $acc_query_val = $account_results[0]['negalimas_privaziavimas'];
        $service_id = 19;
        if(empty($acc_query_val)){
            $price = (($first_price / 100) * $formated_array['LBL_BLOCKED_ENTRANCE_PRICE']);
        } else {
            $price = $first_price + (($first_price / 100) * $acc_query_val);
        }
    } else if($fail_id == 3){
        $acc_query_val = $account_results[0]['neparuostas_krovinys'];
        $service_id = 20;
        if(empty($acc_query_val)){
            $price = (($first_price / 100) * $formated_array['LBL_NOT_PREPARED_PRICE']);
        } else {
            $price = $first_price + (($first_price / 100) * $acc_query_val);
        }
    } else if($fail_id == 4){
        $acc_query_val = $account_results[0]['netinkamai_supakuota'];
        $service_id = 21;
        if(empty($acc_query_val)){
            $price = (($first_price / 100) * $formated_array['LBL_WRONG_PACKAGE_PRICE']);
        } else {
            $price = $first_price + (($first_price / 100) * $acc_query_val);
        }
    } else if($fail_id == 5){
        $acc_query_val = $account_results[0]['neatitinka_uzsakymo'];
        $service_id = 22;
        if(empty($acc_query_val)){
            $price = (($first_price / 100) * $formated_array['LBL_BAD_PACKAGE_PRICE']);
        } else {
            $price = $first_price + (($first_price / 100) * $acc_query_val);
        }
    } else if($fail_id == 6){
        $acc_query_val = $account_results[0]['nebuvo_vietoje'];
        $service_id = 19;
        if(empty($acc_query_val)){
            $price = (($first_price / 100) * $formated_array['LBL_NOT_PRESENT_PRICE']);
        } else {
            $price = $first_price + (($first_price / 100) * $acc_query_val);
        }
    } else {
        return;
    }
    $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id,productid,sequence_no,quantity,lineitem_id,margin,`service`,inventory_type,comment, description, note) VALUES(?,?,?,?,?,?,?,?,?,?,?)");      

    $inv_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ?");
    $inv_num->setFetchMode(PDO::FETCH_ASSOC);   
     
    // $stevedoring_existing = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ? AND service = 2");  
    // $stevedoring_existing->setFetchMode(PDO::FETCH_ASSOC);
    // $stevedoring_existing->execute(array($salesorderid));
    // $stevedoring_checker = $stevedoring_existing->fetch()['id'];

    
    // foreach($stevedoring_query AS $row) {
        $inv_num->execute(array($soid));
        $count = $inv_num->rowCount();
        $insert_service->execute(array($soid,'36641',$count+1, 1, 0,$price,$service_id,'penalty', $fail_reason, $fail_reason, $fail_reason));
    // }
    
}

function update_vtiger_salesorder($dbh, $table, $field, $field_value, $salesorderid){
    $sth = $dbh->prepare("UPDATE $table SET $field = :value WHERE salesorderid = :salesorderid"); 
    $sth->execute([':value' => $field_value,
                   ':salesorderid' => $salesorderid
                ]);
}

function update_vtiger_invoice_stevedoring($dbh, $salesorderid){
    $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id,productid,sequence_no,quantity,lineitem_id,margin,`service`,inventory_type,source) VALUES(?,?,?,?,?,?,?,?,?)");      

    $stevedoring_query = $dbh->prepare("SELECT cf_1572 as stevedoring FROM vtiger_salesordercf WHERE salesorderid = ?");
    $stevedoring_query->setFetchMode(PDO::FETCH_ASSOC);     
    $stevedoring_query->execute(array($salesorderid));

    $weight_query = $dbh->prepare("SELECT cargo_wgt as `weight` FROM vtiger_inventoryproductrel WHERE id = ?");
    $weight_query->setFetchMode(PDO::FETCH_ASSOC);     
    

    $inv_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ?");
    $inv_num->setFetchMode(PDO::FETCH_ASSOC);   
     
    $stevedoring_existing = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ? AND `service` = 2");  
    $stevedoring_existing->setFetchMode(PDO::FETCH_ASSOC);
    $stevedoring_existing->execute(array($salesorderid));
    $stevedoring_checker = $stevedoring_existing->fetch()['id'];

    if(!$stevedoring_checker){
        foreach($stevedoring_query AS $row) {
            $inv_num->execute(array($salesorderid));
            $count = $inv_num->rowCount();
            if(!empty($row['stevedoring'])){ 
                $insert_service->execute(array($salesorderid,'36641',$count+1, 1, 0,$row['stevedoring'],2,'stevedoring','METRIKA'));   
            } else {
                $weight_query->execute(array($salesorderid));
                $weight = $weight_query->fetch()['weight'];

                $default_stevedoring_query = $dbh->prepare("SELECT value as stevedoring FROM app_other_settings WHERE title = 'LBL_LOAD_WORK_PRICE'");
                $default_stevedoring_query->setFetchMode(PDO::FETCH_ASSOC);     
                $default_stevedoring_query->execute(array());
                $default_stevedoring_price_rate = floatval($default_stevedoring_query->fetch()['stevedoring']);
                $stevedoring_price = $weight * $default_stevedoring_price_rate;
                $insert_service->execute(array($salesorderid,'36641',$count+1, 1, 0,$stevedoring_price,2,'stevedoring','METRIKA'));
            }   
        }
    }
}

function update_vtiger_invoice_delay($dbh, $salesorderid){
    
    $account_query = $dbh->prepare("SELECT vtiger_accountscf.cf_920 as prastova_kaina 
                                    FROM vtiger_accountscf
                                    LEFT JOIN vtiger_salesorder ON vtiger_salesorder.accountid = vtiger_accountscf.accountid
                                    WHERE vtiger_salesorder.salesorderid = ?");
    $account_query->setFetchMode(PDO::FETCH_ASSOC);     
    $account_query->execute(array($salesorderid));   
    $idle_price = 0;
    $account_prastova_price_rating = $account_query->fetch()['prastova_kaina'];

    $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id,productid,sequence_no,quantity,lineitem_id,margin,`service`,inventory_type) VALUES(?,?,?,?,?,?,?,?)");      
    
    $prastova_query = $dbh->prepare("SELECT prastova as prastova FROM vtiger_salesorder WHERE salesorderid = ?");
    $prastova_query->setFetchMode(PDO::FETCH_ASSOC);     
    $prastova_query->execute(array($salesorderid));

    $inv_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ?");  
    $inv_num->setFetchMode(PDO::FETCH_ASSOC);   
    
    $prastova_existing = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ? AND `service` = 3"); 
    $prastova_existing->setFetchMode(PDO::FETCH_ASSOC);
    $prastova_existing->execute(array($salesorderid));
    $prastova_checker = $prastova_existing->fetch()['id'];
    
    if(!$prastova_checker){
        foreach($prastova_query AS $row) {
            if($row['prastova'] > 0){
                if($account_prastova_price_rating === null || $account_prastova_price_rating === ''){
                    $default_prastova_query = $dbh->prepare("SELECT `value` as prastova FROM app_other_settings WHERE title = 'LBL_IDLE_PRICE'");
                    $default_prastova_query->setFetchMode(PDO::FETCH_ASSOC);     
                    $default_prastova_query->execute(array());
                    $account_prastova_price_rating = floatval($default_prastova_query->fetch()['prastova']);
                } 
            
                if($account_prastova_price_rating == 0){
                    $idle_price = 0;
                } else{
                    $hours = floor($row['prastova'] / 60);
                    $minutes = ($row['prastova'] % 60);
                    if($minutes < 15 ){
                        $idle_price = 0;
                    } else if ($minutes < 45){
                        $idle_price = $account_prastova_price_rating / 2;
                    } else {
                        $idle_price = $account_prastova_price_rating;
                    }
                    $idle_price = $idle_price + ($hours * $account_prastova_price_rating) ;
                } 
                if($idle_price > 0){
                    $inv_num->execute(array($salesorderid));
                    $count = $inv_num->rowCount();
                    $insert_service->execute(array($salesorderid,'36641',$count+1, 1, 0, $idle_price ,3,'stevedoring'));  
                }
            }    
        }
    }
}


function orders_log($log_variable, $logfile) {

    global $config;        
      
    $log_json = '--------------------------------' . date("Y-m-d H:i:s") . '--------------------------------' . "\n" . json_encode($log_variable) . "\n" . "\n";
    
    file_put_contents($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, $log_json, FILE_APPEND);
	
    if (filesize($config['paths']['api_path'] . DIRECTORY_SEPARATOR. 'logs' . DIRECTORY_SEPARATOR . $logfile) > 1024 * 1024 * 10) {
        copy($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, 
             $config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('Y-m-d_H-i-s') . '_' . $logfile);
        unlink($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile);
    }
}

function customers_log($log_variable, $logfile) {

    global $config;        
      
    $log_json = '--------------------------------' . date("Y-m-d H:i:s") . '--------------------------------' . "\n" . json_encode($log_variable) . "\n" . "\n";
    
    file_put_contents($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, $log_json, FILE_APPEND);
	
    if (filesize($config['paths']['api_path'] . DIRECTORY_SEPARATOR. 'logs' . DIRECTORY_SEPARATOR . $logfile) > 1024 * 1024 * 10) {
        copy($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, 
             $config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('Y-m-d_H-i-s') . '_' . $logfile);
        unlink($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile);
    }
}

function multiexplode ($delimiters,$data) {
    $MakeReady = str_replace($delimiters, $delimiters[0], $data);
    $Return    = explode($delimiters[0], $MakeReady);
    return  $Return;
} 

function sendReportMail($error,$place){

  
    $mailHost      = 'imap.gmail.com';   
    $mailUsername   = 'zykaskiets@gmail.com';
    $mailPassword   = 'testas1611'; 

    $answer = "Problemos kodas: $error <br><br> Vieta: $place";

    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      
        $mail->isSMTP();                                            
        $mail->Host       = $mailHost;                    
        $mail->SMTPAuth   = true;                                   
        $mail->Username   = $mailUsername;                    
        $mail->Password   = $mailPassword;                               
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;        
        $mail->Port       = 587; 
        $mail->SMTPOptions = array('ssl' => array(
                                   'verify_peer' => false,
                                   'verify_peer_name' => false,
                                   'allow_self_signed' => true
                                    )
                              );
        $mail->setLanguage('lt', '../vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = false;  

        //Recipients
        $mail->setFrom($mailUsername, 'Problemos crm');
        $mail->addAddress('linas@itoma.lt');         
       

        // Content
        $mail->isHTML(true); 
        $mail->Subject = "Problemos crm";
        $mail->Body    = $answer;
        $mail->send();  
        
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

}

function sentStatus($shipment_id, $orderid,$status){
    $edit_date = date("Y-m-d H:i:s");
    $res =  array('shipment_crm_price' => array(array('shipment_crm_id' => $orderid,'shipment_id' => $shipment_id,'status' => $status,'edit_date' => $edit_date)));
    $res = json_encode($res);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/shipment_crm_price.php");           
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'shipment_crm_price' => $res));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
}

function insert_loads_and_flags($dbh,$last_entity_record,$metrika_order_id,$order,$user_taxablemeter,$default_taxablemeter,$date,$doc_service){

    $insert_load = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel (id,
                                                                external_order_id,  
                                                                external_load_id, 
                                                                productid,
                                                                sequence_no,
                                                                quantity,                                                              
                                                                comment,
                                                                `description`,
                                                                cargo_measure,
                                                                cargo_wgt,
                                                                cargo_length,
                                                                cargo_width,                                                     
                                                                cargo_height, 
                                                                pll, 
                                                                document,                                                                
                                                                ean,
                                                                source,
                                                                import_date,
                                                                note,
                                                           	    `service`,
                                                                ordered_weight,
                                                                ordered_length,
                                                                ordered_width,
                                                                ordered_height,
                                                                revised_measure,
                                                                revised_quantity,
                                                                revised_weight,
                                                                revised_length,
                                                                revised_width,
                                                                revised_height,
                                                                m3,
                                                                inventory_type,
                                                                meters) 
                                                                VALUES (:id,
                                                                        :external_order_id,  
                                                                        :external_load_id, 
                                                                        :productid,
                                                                        :sequence_no,
                                                                        :quantity,                                                              
                                                                        :comment,
                                                                        :description,
                                                                        :cargo_measure,
                                                                        :cargo_wgt,
                                                                        :cargo_length,
                                                                        :cargo_width,                                                     
                                                                        :cargo_height, 
                                                                        :pll, 
                                                                        :document,                                                                
                                                                        :ean,
                                                                        :source,
                                                                        :import_date,
                                                                        :note,
                                                                        :service,
                                                                        :ordered_weight,
                                                                        :ordered_length,
                                                                        :ordered_width,
                                                                        :ordered_height,
                                                                        :revised_measure,
                                                                        :revised_quantity,
                                                                        :revised_weight,
                                                                        :revised_length,
                                                                        :revised_width,
                                                                        :revised_height,
                                                                        :m3,
                                                                        :inventory_type,
                                                                        :meters)');  
                                        
    $update_cargo_dimensions = $dbh->prepare('UPDATE vtiger_salesordercf  
                                              SET cf_1317 = :ordered, 
                                              cf_1319 = :revised,
                                              cf_1367 = :ordered_m3,
                                              cf_1365 = :revised_m3
                                              WHERE salesorderid = :salesorderid');
    
    
    $insert_flags = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel_flags (flag_id, 
                                                                        external_order_id, 
                                                                        external_load_id, 
                                                                        termo, 
                                                                        minifest, 
                                                                        cmr, 
                                                                        invoice, 
                                                                        palettes, 
                                                                        other_tare,
                                                                        import_date) 
                                                                        VALUES (:flag_id, 
                                                                                :external_order_id, 
                                                                                :external_load_id, 
                                                                                :termo, :minifest, 
                                                                                :cmr, :invoice, 
                                                                                :palettes, 
                                                                                :other_tare, 
                                                                                :import_date)');  

    $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel (id,
                                                                             sequence_no,
                                                                             external_order_id,
                                                                             external_load_id,
                                                                             productid,quantity,
                                                                             margin,`service`,
                                                                             inventory_type,
                                                                             source) VALUES(:id,
                                                                                            :sequence_no,
                                                                                            :external_order_id,
                                                                                            :external_load_id,
                                                                                            :productid,
                                                                                            :quantity,
                                                                                            :margin,
                                                                                            :service,
                                                                                            :inventory_type,
                                                                                            :source)");


    $ordered_meters_arr = [];
    $revised_meters_arr = [];
    $ordered = [];
    $revised = [];  

    $ordered_m3 = [];
    $revised_m3 = [];  

    $docsArr = [];
    $data = [];
      
    $sequence_no = 1;
    foreach($order['loads'] AS $metrika_load_id => $load){
      // INSERTINAM LOADUS

      if(empty($load['ordered_weight'])) $load['ordered_weight'] = 0;  
      if(empty($load['ordered_length'])) $load['ordered_length'] = 0;  
      if(empty($load['ordered_width'])) $load['ordered_width'] = 0;  
      if(empty($load['ordered_height'])) $load['ordered_height'] = 0;  

      if(empty($load['revised_weight'])) $load['revised_weight'] = 0;  
      if(empty($load['revised_length'])) $load['revised_length'] = 0;  
      if(empty($load['revised_width'])) $load['revised_width'] = 0;  
      if(empty($load['revised_height'])) $load['revised_height'] = 0;  

      if(empty($load['documents'])) $load['documents'] = '';
      if(empty($load['ean'])) $load['ean'] = '';

      $ordered[] = $load['ordered_quantity']." ".$load['ordered_measure']." ".$load['ordered_weight']." ".$load['ordered_length']."x".$load['ordered_width']."x".$load['ordered_height'];
      $revised[] = $load['revised_quantity']." ".$load['revised_measure']." ".$load['revised_weight']." ".$load['revised_length']."x".$load['revised_width']."x".$load['revised_height'];

      $revised_m3[] = $load['m3']; 
      $ordered_m3[] = ($load['ordered_length'] * $load['ordered_width'] * $load['ordered_height']) * $load['ordered_quantity']; 

      $ordered_meters_arr[] = $load['ordered_length'];
      $ordered_meters_arr[] = $load['ordered_width'];     

      $revised_meters_arr[] = $load['revised_length'];
      $revised_meters_arr[] = $load['revised_width'];   

      $max_ordered_meter = max($ordered_meters_arr);
      $max_revised_meter = max($revised_meters_arr);         


      $max_meter = ($max_ordered_meter > $max_revised_meter ? $max_ordered_meter : $max_revised_meter);
      $max_taxablemeter = ($user_taxablemeter == 0 || $user_taxablemeter == '' ? $default_taxablemeter : $user_taxablemeter);

      $meters = ($max_meter > $max_taxablemeter ? $max_meter : NULL); 


      $insert_load->execute([
        ':id' => $last_entity_record,
        ':external_order_id' => $metrika_order_id,  
        ':external_load_id' => $metrika_load_id, 
        ':productid' => 14244,
        ':sequence_no' => $sequence_no,
        ':quantity' => $load['ordered_quantity'],                                                              
        ':comment' => $load['note'],
        ':description' => $load['note'],
        ':cargo_measure' => $load['ordered_measure'],
        ':cargo_wgt' => $load['ordered_weight'],
        ':cargo_length' => $load['ordered_length'],
        ':cargo_width' => $load['ordered_width'],                                                     
        ':cargo_height' => $load['ordered_height'], 
        ':pll' => $load['pll'], 
        ':document' => $load['documents'],                                                                
        ':ean' => $load['ean'],
        ':source' => 'Metrika',
        ':import_date' => $date,
        ':note' => $load['note'],
        ':service' => 1,
        ':ordered_weight' => $load['ordered_weight'],
        ':ordered_length' => $load['ordered_length'],
        ':ordered_width' => $load['ordered_width'],
        ':ordered_height' => $load['ordered_height'],
        ':revised_measure' => $load['revised_measure'],
        ':revised_quantity' => $load['revised_quantity'],
        ':revised_weight' => $load['revised_weight'],
        ':revised_length' => $load['revised_length'],
        ':revised_width' => $load['revised_width'],
        ':revised_height' => $load['revised_height'],
        ':m3' => $load['m3'],
        ':inventory_type' => 'transport',
        ':meters' => $meters                                     
      ]); 


      $insert_flags->execute([
        ':flag_id' => $last_entity_record,
        ':external_order_id' => $metrika_order_id,
        ':external_load_id' => $metrika_load_id,
        ':termo' => $load['flags']['flg_termo'],
        ':minifest' =>  $load['flags']['flg_minifest'],
        ':cmr' =>  $load['flags']['flg_crm'],                                                                                               
        ':invoice' => $load['flags']['flg_invoice'],
        ':palettes' =>  $load['flags']['flg_palettes'],
        ':other_tare' =>  $load['flags']['flg_other_tare'],   
        ':import_date' => $date
      ]);

      $docsArr[$last_entity_record][] = ($load['flags']['flg_minifest'] == 1 || $load['flags']['flg_minifest'] == 0) ? $load['flags']['flg_minifest'] : 1;
      $docsArr[$last_entity_record][] = ($load['flags']['flg_crm'] == 1 || $load['flags']['flg_crm'] == 0) ? $load['flags']['flg_crm'] : 1;
      $docsArr[$last_entity_record][] = ($load['flags']['flg_invoice'] == 1 || $load['flags']['flg_invoice'] == 0) ? $load['flags']['flg_invoice'] : 1;
      $docs = array_sum($docsArr[$last_entity_record]);

      $data[$last_entity_record] =  array('salesorderid' => $last_entity_record,
                                           'order_id' => $metrika_order_id, 
                                           'load_id' => $metrika_load_id, 
                                           'docs' => $docs, 
                                           'charge' => $doc_service);      

      $sequence_no++;
    } 
    
    
     // NOTE Irasomos paslaugos
     $count = $sequence_no;
    foreach($data AS $item) {
        if($item['docs'] > 0){                 
            for($i=0; $i<$item['docs']; $i++){                                               
                $insert_service->execute([':id' => $item['salesorderid'],
                                          ':sequence_no' => $count,
                                          ':external_order_id' => $item['order_id'],
                                          ':external_load_id' => $item['load_id'],
                                          ':productid' => 36641,
                                          ':quantity' => 1,
                                          ':margin' => $item['charge'],
                                          ':service' => 12,
                                          ':inventory_type' => 'documents',
                                          ':source' => 'Metrika'
                                        ]);                      
                $count++;                                                    
            }  
           
        }
    }  


    $update_cargo_dimensions->execute([
        ':ordered' => implode(', ',$ordered),
        ':revised' => implode(', ',$revised),
        ':ordered_m3' => round(array_sum($ordered_m3),2),
        ':revised_m3' => round(array_sum($revised_m3),2),
        ':salesorderid' => $last_entity_record
    ]);
}


function calculatePriceByExternalOrderId($dbh,$shipments,$insert_or_update){
    
    include 'getPriceFromPriceBook.php';

    $get_loads_info = $dbh->prepare("SELECT accountname, GROUP_CONCAT(CONCAT(l.ordered_weight,' ',l.ordered_length,'x',l.ordered_width,'x',ordered_height)) AS ordered, 
    GROUP_CONCAT(CONCAT(l.revised_weight,' ',l.revised_length,'x',l.revised_width,'x',revised_height)) AS revised, 
    GROUP_CONCAT(DISTINCT l.quantity) AS quantity,
    GROUP_CONCAT(DISTINCT l.revised_quantity) AS revised_quantity, 
    ROUND(cf_928,2) AS shipment_km,
    ROUND(SUM(l.m3),2) AS m3, a.accountid, sa.salesorderid, bill_code,ship_code,
    CASE WHEN cargo_measure = 'pll' THEN SUM(l.quantity) ELSE 0 END AS  pll,
    CASE WHEN revised_measure = 'pll' THEN FLOOR(SUM(l.revised_quantity)) ELSE 0 END AS revised_pll,
    sa.external_order_id,bill_country,ship_country
            FROM vtiger_salesorder sa
            LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=sa.salesorderid
            LEFT JOIN vtiger_account a ON a.accountid = sa.accountid 
            LEFT JOIN vtiger_crmentity e ON e.crmid = sa.salesorderid     
            LEFT JOIN vtiger_inventoryproductrel l ON l.id = sa.salesorderid AND productid = 14244   
            LEFT JOIN vtiger_sobillads b ON b.sobilladdressid=sa.salesorderid   
            LEFT JOIN vtiger_soshipads s ON s.soshipaddressid=sa.salesorderid              
            WHERE sa.salesorderid IN ($shipments) AND (cf_1376 = '' AND cf_2663 = '') AND e.deleted = 0
            GROUP BY sa.salesorderid
            ORDER BY salesorderid ");

            // cf_2663 Apskaitininkiu keista kaina
            // cf_1376 Sutarta kaina

    $update_margin = $dbh->prepare('UPDATE vtiger_inventoryproductrel SET margin = :margin WHERE id = :id AND external_order_id = :external_order_id AND productid = 14244'); 
    $update_salesordercf = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = :pricebook, 
                                                                          cf_1365 = :revised_m3, 
                                                                          cf_1367 = :ordered_m3, 
                                                                          cf_1374 = :pricebook_price,
                                                                          cf_1572 = :stevedoring,
                                                                          cf_2718 = :pricebook_detail  
                                         WHERE salesorderid = :salesorderid'); 

    $update_salesorder = $dbh->prepare('UPDATE vtiger_salesorder SET total = :total,
                                                                     subtotal = :total,                                                         
                                                                     pre_tax_total = :total                                                     
                                        WHERE salesorderid = :salesorderid');
                                        
                                        
    $insert_log = $dbh->prepare('INSERT INTO vtiger_salesorder_dimensions_log (salesorderid, amount, pricebook, kg, m3, m2, pll, kg_revised, m3_revised, m2_revised, pll_revised, which, importdate) VALUES (:salesorderid, :amount, :pricebook, :kg, :m3, :m2, :pll, :kg_revised, :m3_revised, :m2_revised, :pll_revised, :which, :importdate)');    
    
            
    $get_loads_info->setFetchMode(PDO::FETCH_ASSOC);
    $get_loads_info->execute(array());

    $salesorderid = [];

    $date = date('Y-m-d H:i:s');

    $taxable_dimensions = [];

        foreach($get_loads_info AS $row){  

            $salesorderid[$row['salesorderid']] = $row['salesorderid'];  

            $accountid = $row['accountid'];     
            $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
            $row['bill_code'] = preg_replace("/[^0-9]/", "",$row['bill_code']);
            $row['ship_code'] = preg_replace("/[^0-9]/", "",$row['ship_code']);
            $pll = $row['pll'];               
            $revised_pll = $row['revised_pll'];       
            $date2 = date("Y-m-d H:i:s");            
            $external_order_id = $row['external_order_id'];   

            $quantity = explode(",", $row['revised_quantity']);
            $ordered_quantity = explode(",", $row['quantity']);
            

            $weight_ordered = array();
            $dim_ordered = array();
            $dim_ordered2 = array();
            $square_ordered = array();
            $square_ordered_temp = array();
            $weight_revised = array();           
            $square_revised = array();
            $square_revised_temp = array();


            $ordered = explode(",",$row['ordered']);  
            
            for($i = 0; $i < count($ordered); $i++){
                $exploded_arr = explode(' ',$ordered[$i]);
                $weight_ordered[] = $exploded_arr[0];         
                $dim_ordered[] = array_product(explode('x', $exploded_arr[1])) * $ordered_quantity[$i];     
                $dim_ordered2[] = explode('x', $exploded_arr[1]); 
                $square_ordered_temp[] = explode('x', $exploded_arr[1]); 
                unset($square_ordered_temp[$i][2]);           
                $square_ordered[] = array_product($square_ordered_temp[$i]) * $ordered_quantity[$i];
            }
    
            $revised = explode(",",$row['revised']);
            
            for($i = 0; $i < count($revised); $i++){
                $exploded_arr2 = explode(' ',$revised[$i]);
                $weight_revised[] = $exploded_arr2[0];       
                $square_revised_temp[] = explode('x', $exploded_arr2[1]); 
                unset($square_revised_temp[$i][2]);
                $square_revised[] = array_product($square_revised_temp[$i]) * $quantity[$i];         
            }
    
        
            $ordered_w = array_sum($weight_ordered);
            $ordered_m3 = round(array_sum($dim_ordered),2);
            $ordered_m2 = round(array_sum($square_ordered),2);
    
            $revised_w = array_sum($weight_revised);           
            $revised_m3 = round($row['m3'],2);
            $revised_m2 = round(array_sum($square_revised),2);

    
            $getPrice = array();
            $getPrice_revised = array();
            $getPrice_ordered = array();


            if(!empty($row['bill_code']) AND !empty($row['ship_code']) AND (!empty($ordered_w) OR !empty($revised_w)) AND ($row['bill_country'] != "EST" AND $row['ship_country'] != "EST") ){

                if($ordered_w == $revised_w && $ordered_m3 == $revised_m3 && $ordered_m2 == $revised_m2 && $pll == $revised_pll){
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'], 'CLIENT'); 

                    $detail_message = getPriceDetail($getPrice_revised, $getPrice_revised);
                    
                    if($getPrice_revised['price'] == 0){
                        $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS');
                    }

                    $getPrice['price'] = $getPrice_revised['price'];
                    $getPrice['pricebook'] =  $getPrice_revised['combination'];
                    $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                    $getPrice['type'] = 'fail';
                    if($getPrice_revised['price'] > 0){
                        $getPrice['type'] = 'revised';
                    }
                    $getPrice['salesorderid'] = $row['salesorderid'];
                }else{

                    $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'CLIENT'); 
                    
                    if($getPrice_ordered['price'] > 0){
                     $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'],'CLIENT'); 
                    }else{
                        $getPrice_revised['price'] = 0;
                        $getPrice_revised['combination'] = '';
                    }

                    $detail_message = getPriceDetail($getPrice_ordered, $getPrice_revised);
                 

                    if($getPrice_ordered['price'] == 0 || $getPrice_revised['price'] == 0){
                        $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS');
                        $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS'); 
                    }
    
                    if($getPrice_ordered['price'] > $getPrice_revised['price']){
                        $getPrice['price'] = $getPrice_ordered['price'];
                        $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                        $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];
                        $getPrice['type'] = 'fail';
                        if($getPrice_ordered['price'] > 0){
                            $getPrice['type'] = 'ordered';
                        }
                        $getPrice['salesorderid'] = $row['salesorderid'];

                    }elseif($getPrice_ordered['price'] < $getPrice_revised['price']){
                        $getPrice['price'] = $getPrice_revised['price'];
                        $getPrice['pricebook'] =  $getPrice_revised['combination'];
                        $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                        $getPrice['type'] = 'fail';
                        if($getPrice_revised['price'] > 0){
                            $getPrice['type'] = 'revised';
                        }
                        $getPrice['salesorderid'] = $row['salesorderid'];
                    }elseif($getPrice_ordered['price'] == $getPrice_revised['price']){                   
                            $getPrice['price'] = $getPrice_revised['price'];
                            $getPrice['pricebook'] =  $getPrice_revised['combination'];
                            $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                            $getPrice['type'] = 'fail';
                            if($getPrice_revised['price'] > 0){
                                $getPrice['type'] = 'revised';
                            }
                            $getPrice['salesorderid'] = $row['salesorderid'];
                    }

                }          

                
            }else{ 
                $getPrice = array('price' => 0);
                
                if(empty($row['bill_code']) AND empty( $row['ship_code']) AND !empty($ordered_w) AND !empty($revised_w)){
                    $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
                }elseif(empty($row['bill_code']) AND empty($row['ship_code'])){    
                    $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
                }elseif(empty($row['bill_code'])){
                    $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
                }elseif(empty($row['ship_code'])){
                    $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
                }elseif(empty($ordered_w) AND empty($revised_w)){
                    $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
                }elseif(strtoupper($row['bill_country']) == "EST" OR strtoupper($row['ship_country']) == "EST"){
                    $getPrice = array('pricebook' => 'Estija','price' => 0);
                }
                
                $getPrice['type'] = '';     
                $getPrice['salesorderid'] = $row['salesorderid'];       
            } 

            $taxable_dimensions[] = $row['salesorderid'];
            $taxable_dimensions[] = $getPrice['type'];

            $update_salesorder->execute(array(       
                ':total' => $getPrice['price'],       
                ':salesorderid' => $row['salesorderid']          
            )); 

            $update_salesordercf->execute(array(                       
                ':pricebook' => $getPrice['pricebook'],
                ':revised_m3' => $revised_m3,
                ':ordered_m3' => $ordered_m3,
                ':pricebook_price' => $getPrice['price'],       
                ':stevedoring' => ($getPrice['stevedoring'] ?: 0), 
                ':pricebook_detail' => ($detail_message ?: 'Nepavyko nustatyti'),      
                ':salesorderid' => $row['salesorderid']          
            )); 
            
            $update_margin->execute([':margin' => $getPrice['price'], ':id' => $row['salesorderid'], ':external_order_id' => $external_order_id]);

            $insert_log->execute(array(':salesorderid' => $row['salesorderid'],
                ':amount'  => $getPrice['price'],
                ':pricebook' => $getPrice['pricebook'],
                ':kg' => $ordered_w,
                ':m3' => $ordered_m3,
                ':m2' => $ordered_m2,
                ':pll' => $pll,
                ':kg_revised' => $revised_w,
                ':m3_revised' => $revised_m3,
                ':m2_revised' => $revised_m2,
                ':pll_revised' => $revised_pll,
                ':which' => (!empty($insert_or_update[$row['salesorderid']]) ? $insert_or_update[$row['salesorderid']] : 'Nenustatyta'),
                ':importdate' => $date2
            )); 

              if(!file_exists("../../logs/COUNT_PRICES/$date")){
                mkdir("../../logs/INSERT/UPDATE/$date", 0777, true); 
              }
                 
              $time = date("H:").$minutes;
              $dir = "COUNT_PRICES/$date";
              $logfile = "$dir/$time.log";
              customers_log('Uzsakymas: '.$row['salesorderid'].' kaina '.$getPrice['price'].' kainynas '.$getPrice['pricebook'], $logfile);  
        }

    if(count($salesorderid) > 0){    
        $salesorderid = implode(',',$salesorderid);
        $delete_taxable = $dbh->prepare("DELETE FROM app_taxable_dimensions WHERE salesorderid IN ($salesorderid)");   
        $delete_taxable->execute();

        $question_marks = implode(',', array_map(function($el) { return '('.implode(',', $el).')'; },array_chunk(array_fill(0, count($taxable_dimensions), '?'), 2))); 

        $insert_taxable = $dbh->prepare("INSERT INTO app_taxable_dimensions (salesorderid,`type`) VALUES $question_marks");   
        $insert_taxable->execute($taxable_dimensions);

    }
 
}


function getPriceDetail($ord, $rev){

    $message_ordered = '';
    $message_ordered_apm = '';

    $message_revised = '';  
    $message_revised_apm = ''; 

    if($ord['count_weight']['count'] > 0 && in_array($ord['price'], $ord['count_weight']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal kg, ';

    }else if($ord['count_weight']['count'] > 0 && !in_array($ord['price'], $ord['count_weight']['price'])){
        $message_ordered .= 'Pagal kg kainos nerasta, ';

    }else if($ord['count_weight']['count'] == 0 && !isset($ord['price']['count_weight']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno kg rėžio, ';               
    }             

    if($ord['count_m3']['count'] > 0 && in_array($ord['price'], $ord['count_m3']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal m3, ';

    }else if($ord['count_m3']['count'] > 0 && !in_array($ord['price'], $ord['count_m3']['price'])){
        $message_ordered .= 'Pagal m3 kainos nerasta, ';

    }else if($ord['count_m3']['count'] == 0 && !isset($ord['price']['count_m3']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno m3 rėžio, ';             
    }

    if($ord['count_m2']['count'] > 0 && in_array($ord['price'], $ord['count_m2']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal m2, ';

    }else if($ord['count_m2']['count'] > 0 && !in_array($ord['price'], $ord['count_m2']['price'])){
        $message_ordered .= 'Pagal m2 kainos nerasta, ';

    }else if($ord['count_m2']['count'] == 0 && !isset($ord['price']['count_m2']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno m2 rėžio, ';             
    }
    
    if($ord['count_pll']['count'] > 0 && in_array($ord['price'], $ord['count_pll']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal pll, ';

    }else if($ord['count_pll']['count'] > 0 && !in_array($ord['price'], $ord['count_pll']['price'])){
        $message_ordered .= 'Pagal pll kainos nerasta, ';

    }else if($ord['count_pll']['count'] == 0 && !isset($ord['price']['count_pll']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno pll rėžio, ';             
    }

    // Revised

    if($rev['count_weight']['count'] > 0 && in_array($rev['price'], $rev['count_weight']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal kg, ';

    }else if($rev['count_weight']['count'] > 0 && !in_array($rev['price'], $rev['count_weight']['price'])){
        $message_revised .= 'Pagal kg kainos nerasta, ';

    }else if($rev['count_weight']['count'] == 0 && !isset($rev['price']['count_weight']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno kg rėžio, ';               
    }             

    if($rev['count_m3']['count'] > 0 && in_array($rev['price'], $rev['count_m3']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal m3, ';

    }else if($rev['count_m3']['count'] > 0 && !in_array($rev['price'], $rev['count_m3']['price'])){
        $message_revised .= 'Pagal m3 kainos nerasta, ';

    }else if($rev['count_m3']['count'] == 0 && !isset($rev['price']['count_m3']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno m3 rėžio, ';             
    }

    if($rev['count_m2']['count'] > 0 && in_array($rev['price'], $rev['count_m2']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal m2, ';

    }else if($rev['count_m2']['count'] > 0 && !in_array($rev['price'], $rev['count_m2']['price'])){
        $message_revised .= 'Pagal m2 kainos nerasta, ';

    }else if($rev['count_m2']['count'] == 0 && !isset($rev['price']['count_m2']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno m2 rėžio, ';             
    }
    
    if($rev['count_pll']['count'] > 0 && in_array($rev['price'], $rev['count_pll']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal pll, ';

    }else if($rev['count_pll']['count'] > 0 && !in_array($rev['price'], $rev['count_pll']['price'])){
        $message_revised .= 'Pagal pll kainos nerasta, ';

    }else if($rev['count_pll']['count'] == 0 && !isset($rev['price']['count_pll']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno pll rėžio, ';             
    } 

    $why_bazinis = '';
    
    if($ord['price'] == 0){
        $why_bazinis .= 'Pereita į bazinį nes, pagal užsakytus duomenis nerasta kaina';
    }else if($rev['price'] == 0){
        $why_bazinis .= 'Pereita į bazinį nes, pagal faktinius duomenis nerasta kaina';
    }                     
    
    if(!empty($message_ordered_apm)){
        $message_ordered = rtrim($message_ordered_apm,', '); 
    }else{
        $message_ordered = rtrim($message_ordered,', ');             

    }
 
    
    $message_revised = rtrim($message_revised,', ');

    if(!empty($message_revised_apm)){
        $message_revised = rtrim($message_revised_apm,', '); 
    }else{
        $message_revised = rtrim($message_revised,', ');             

    }
    
    
    return "Užsakyti: $message_ordered -  ".$ord['combination']." / Faktas: $message_revised - ".$rev['combination']." / $why_bazinis";
}

?>
