<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

// ini_set('dispay_errors', 1);
// error_reporting(E_ALL);

    try {
        require('../mysql_connection.php');     

        $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update_for_update_orders` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
        $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
        $last_timestamp->execute(array());
        $date_from = $last_timestamp->fetch();
        $date_from = $date_from['date'];    

        // Preparing insert and update queries
        $insert_query = 'INSERT INTO app_metrika_route_point_failure_reason (route_point_operation_id,failure_reason_id, operation_type_id) VALUES (?,?,?)';
        $update_query = 'UPDATE app_metrika_route_point_failure_reason SET failure_reason_id = ? WHERE route_point_operation_id = ?';
        
        $sth_insert = $dbh->prepare($insert_query);
        $sth_update = $dbh->prepare($update_query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/route_point_operation_status.php");              
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
    
        curl_close($ch);
        $data = json_decode($result, true);


        if(!empty($data['response']['result'])){
        
            foreach($data['response']['result'] AS $row){                
                
                $sth2 = $dbh->prepare("SELECT id FROM app_metrika_failure_reasons WHERE external_reason_id = ?");
                $sth2->setFetchMode(PDO::FETCH_ASSOC);
                $sth2->execute(array($row['failure_reason_id']));
                $fail_id = $sth2->fetch()['id']; 

                if(!$fail_id){                    
                    $sth_fail = $dbh->prepare("INSERT INTO app_metrika_failure_reasons (external_reason_id, reason) VALUES (?, ?)");
                    $sth_fail->setFetchMode(PDO::FETCH_ASSOC);
                    $sth_fail->execute(array($row['failure_reason_id'], $row['reason']));
                    $fail_id = $dbh->lastInsertId();
                }           


                // Query to check if route point operation is already added                
                $sth3 = $dbh->prepare("SELECT route_point_operation_id FROM app_metrika_route_point_failure_reason WHERE route_point_operation_id = ?");
                $sth3->setFetchMode(PDO::FETCH_ASSOC);
                $sth3->execute(array($row['route_point_operation_id']));
                if(!$sth3->rowCount()){
                    $sth_insert->execute(array($row['route_point_operation_id'], $fail_id, $row['operation_type_id']));                    
                } else{
                    $sth_update->execute(array($fail_id ,$row['route_point_operation_id']));
                }
              
                
                $get_salesorderid = $dbh->prepare("SELECT salesorderid FROM vtiger_salesorder WHERE external_order_id = ?");

                $get_salesorderid->setFetchMode(PDO::FETCH_ASSOC);
                $get_salesorderid->execute(array($row['shipment_id']));
                $salesorderid = $get_salesorderid->fetch()['salesorderid'];               
            
                $sostatus = NULL;
                if($fail_id == 1 && $row['operation_type_id'] == 'U'){
                    $sostatus = "Not Delivered";
                    $row['reason'] = $row['reason'] . '(pristatymas)';
                } else if($fail_id == 1 && $row['operation_type_id'] == 'L'){
                    $sostatus = "Not Delivered";
                    $row['reason'] = $row['reason'] . '(pasikrovimas)';
                } else{
                    $sostatus = "Not Delivered";
                }

                
                if($salesorderid){
                    update_fail_reason($dbh, $salesorderid, $sostatus, $row['reason']);
                    sentStatus($row['shipment_id'],$salesorderid,'Nepriduotas');
                    update_fail_reason_pricing($dbh, $fail_id, $row['reason'], $salesorderid);
                }
                
                // // Kokybe card
                $dbh->beginTransaction(); 
          
                $kokybe_existing = $dbh->prepare("SELECT kokybeid as id FROM vtiger_kokybe WHERE kokybeno = ?");                   
                $kokybe_existing->setFetchMode(PDO::FETCH_ASSOC);
                $kokybe_existing->execute(array($salesorderid));
               
                if(!$kokybe_existing->rowCount()){
                    $insert_kokybe = $dbh->prepare('INSERT INTO vtiger_kokybe (kokybeid,
                                                                      kokybeno,
                                                                      kokybe_tks_deliveryfailreason) 
                                                                      VALUES (?,?,?)');
                    
                    $get_order = $dbh->prepare("SELECT shipment_code, e.smownerid, s.salesorderid   
                                          FROM vtiger_salesorder s
                                          JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                          WHERE e.deleted = 0  AND s.salesorderid = ?");
                                    
                    $get_order->setFetchMode(PDO::FETCH_ASSOC);
                    $get_order->execute(array($salesorderid));

                    $sth5 = $dbh->prepare("INSERT INTO vtiger_kokybecf (kokybeid) VALUES (?)");
                    $orders_count = $get_order->rowCount();
            
                    if($orders_count > 0){              
                        $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
                        $last_entity_record = get_last_entity_id_and_update($dbh, $orders_count);           
                    

                        createNotification($dbh, $salesorderid,$row['reason'],'status','kokybe');

                        while ($row2 = $get_order->fetch()) {  
                            
                            $date = date("Y-m-d H:i:s");
                    
                            $sth6->execute(array($last_entity_record)); 
                            $record_exist = $sth6->rowCount();
                        
                            if($record_exist) {
                            $last_entity_record_old = $last_entity_record;
                            $sth3->execute(array());
                            $seq = $sth3->fetch();
                            $last_entity_record = $seq['id']+1;
                            $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
                            $sth4->execute(array($last_entity_record));     
                            }
                    
                            $insert_entity = insert_entity3($dbh, 'Kokybe', $last_entity_record, $row2['smownerid'], NULL,'CRM',  $row2['shipment_code'], $date);
                                
                            $insert_kokybe->execute(array(  
                                $last_entity_record,  
                                $salesorderid,       
                                $row['reason']           
                            )); 
                            //   print_r($rel_id);
                            $insert_entity_rel = insert_entityrel($dbh, $salesorderid, 'SalesOrder', $last_entity_record, 'Kokybe');
                            $sth5->execute(array($last_entity_record));
                                        
                            if($record_exist) {             
                                $last_entity_record = $last_entity_record_old;    
                            }         
                            
                            $last_entity_record++;
                        }
                    }
                            
                } else {                  

                    $update_kokybe = $dbh->prepare("UPDATE vtiger_kokybe SET kokybe_tks_deliveryfailreason = ? WHERE  kokybeno = ?"); 
                    $update_kokybe->execute(array($row['reason'], $salesorderid));
                    createNotification($dbh, $salesorderid,$row['reason'],'status','kokybe');              
                }
                
                $dbh->commit();
            }
        }

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
