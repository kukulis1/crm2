<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');           

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_delivery_log 
                                LEFT JOIN app_metrika_delivery ON delivery_id=row_id
                                WHERE delivery_id IS NULL');


        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $table = 'delivery';       
        $where = "delivery_id IN (";

        while ($row = $sth->fetch()) {
        $where .= $row['row_id'].","; 
        }

        $where = rtrim($where, ',');
        $where .= ")";

        $insert_query = 'INSERT INTO app_metrika_delivery (shipment_id,delivery_id) VALUES (?,?)';
        $execute = array('shipment_id','delivery_id');

        getTable($table, $dbh, $where, $insert_query, $execute);    
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }