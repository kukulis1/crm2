<?php

include '../../ws.config.php';
global $config;
require '../../utils.php';

set_time_limit(900);
error_reporting(0);


    try {
        require('../../mysql_connection.php'); 

        $sth = $dbh->prepare("SELECT DISTINCT external_order_id
                                FROM vtiger_salesorder s 
                                LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
                                WHERE sostatus != 'delivered' AND (DATEDIFF(NOW(),createdtime) BETWEEN 2 AND 10) AND (external_order_id IS NOT NULL AND external_order_id > 1)");
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $shipment_id = '';

       
        while ($row = $sth->fetch()) {
            $shipment_id .= $row['external_order_id'].","; 
        }
    
            $shipment_id = rtrim($shipment_id, ','); 


        $truncate = $dbh->prepare('TRUNCATE app_metrika_statuses_night'); 
        $truncate->execute(array());

        $select = "DISTINCT shipment.shipment_id,departure_time_fact,
        CASE 
            WHEN location_id IS NULL THEN 1
            WHEN sender_location_id = location_id AND is_visited is false THEN 2
            WHEN sender_location_id != location_id AND is_visited is false AND consignee_location_id = location_id THEN 3
            WHEN sender_location_id != location_id AND is_visited is true AND consignee_location_id != location_id THEN 3			   
            WHEN sender_location_id = location_id AND is_visited is true AND consignee_location_id != location_id THEN 3
            WHEN sender_location_id != location_id AND is_visited is true AND consignee_location_id = location_id THEN 4
            WHEN consignee_location_id = location_id AND is_visited is true THEN 4
            ELSE 3
        END as status";
        $table = 'shipment
                    LEFT JOIN delivery ON delivery.shipment_id=shipment.shipment_id
                    LEFT JOIN route_point_operation ON  route_point_operation.delivery_id=delivery.delivery_id
                    LEFT JOIN route_point ON route_point.route_point_id=route_point_operation.route_point_id';
        $where = "shipment.shipment_id IN  ($shipment_id)  AND (failed = false OR failed IS NULL) ";   
        $where .= " GROUP BY shipment.shipment_id,location_id,is_visited,departure_time_fact
        ORDER BY departure_time_fact";      

        $insert_query = $dbh->prepare('INSERT INTO app_metrika_statuses_night (shipment_id, status) VALUES (?,?)');   
        $status = array('Sent' => 'Naujas','Approved' => 'Priimtas','in progress' => 'Vykdomas','delivered' => 'Atliktas');
       
    
        $statuses = [
            1 => 'Sent', 
            2 => 'Approved', 
            3 => 'in progress', 
            4 => 'delivered',
        ];
 

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);  

        $grouped_statuses = [];
        foreach($data['response']['result'] as $status){
            if($status['status'] > ($grouped_statuses[$status['shipment_id']] ?? 0)){
                $grouped_statuses[$status['shipment_id']] = $status['status'];
            }
        }    



        foreach($grouped_statuses as $shipment_id => $status_key){
            $insert_query->execute([$shipment_id, $statuses[$status_key]]);
        }       

        $set_session_wait_time = $dbh->prepare("SET SESSION innodb_lock_wait_timeout = 900");
        $set_session_wait_time->execute(array());

        $update_status = $dbh->prepare('UPDATE vtiger_salesorder SET sostatus = ?  WHERE salesorderid = ?');
        $change_status = $dbh->prepare("SELECT shipment_id,m.status,salesorderid
                                        FROM app_metrika_statuses_night m
                                        JOIN vtiger_salesorder ON external_order_id=shipment_id
                                        ORDER BY id DESC");

        $change_status->setFetchMode(PDO::FETCH_ASSOC);
        $change_status->execute(array());    
        
        foreach($change_status AS $st){
            $update_status->execute(array($st['status'],$st['salesorderid']));
            sentStatus($st['shipment_id'],$st['salesorderid'],$status[$st['status']]);
        }       
        
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
