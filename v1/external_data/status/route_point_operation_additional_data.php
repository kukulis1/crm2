<?php

set_time_limit(500);
error_reporting(0);

include '../ws.config.php';
global $config;
require '../utils.php';

// ini_set('dispay_errors', 1);
// error_reporting(E_ALL);


    try {
        require('../mysql_connection.php');    
        
        $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update_for_update_orders` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
        $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
        $last_timestamp->execute(array());
        $date_from = $last_timestamp->fetch();
        $date_from = $date_from['date']; 
       
        $select = 'itoma_route_point_additional_data.*, shipment_id';

        $table = 'itoma_route_point_additional_data
        JOIN route_point_operation ON route_point_operation.route_point_operation_id=itoma_route_point_additional_data.route_point_operation_id
        JOIN delivery ON delivery.delivery_id=route_point_operation.delivery_id 
        JOIN log ON log.row_id=itoma_route_point_additional_data.route_point_operation_id'; 

        $where = "table_id = 18 AND entry_date IS NOT NULL AND to_char(entry_date,'YYYY-MM-DD HH24:MI:SS') > '$date_from'
        GROUP BY itoma_route_point_additional_data.itoma_route_point_additional_data_id, itoma_route_point_additional_data.route_point_operation_id, 
        itoma_route_point_additional_data.loading_works, itoma_route_point_additional_data.idle, itoma_route_point_additional_data.comments, shipment_id";

        $insert_query = 'INSERT INTO app_metrika_route_point_additional_data (route_point_operation_id,loading_works,idle,remarks,comments) VALUES (?,?,?,?,?)';

        $update_query = 'UPDATE app_metrika_route_point_additional_data SET loading_works = ? , idle = ? , remarks = ? , comments = ?  WHERE route_point_operation_id = ?';
        
        $sth_insert = $dbh->prepare($insert_query);
        $sth_update = $dbh->prepare($update_query);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);


        foreach($data['response']['result'] AS $row){
            $remarks = $row['remarks'];

            // Query to check if route point operation is already added           
            $sth3 = $dbh->prepare("SELECT route_point_operation_id FROM app_metrika_route_point_additional_data WHERE route_point_operation_id = :route_point_operation_id");
            $sth3->setFetchMode(PDO::FETCH_ASSOC);            
            $sth3->execute([':route_point_operation_id' => $row['route_point_operation_id']]);

            if(!$sth3->rowCount()){
                $sth_insert->execute([$row['route_point_operation_id'], $row['loading_works'], $row['idle'], $remarks, $row['comments']]);
            } else{
                $sth_update->execute([$row['loading_works'], $row['idle'], $remarks, $row['comments'], $row['route_point_operation_id']]);
            }
           
                $get_salesorderid = $dbh->prepare("SELECT salesorderid FROM vtiger_salesorder WHERE external_order_id = ?");

                $get_salesorderid->setFetchMode(PDO::FETCH_ASSOC);
                $get_salesorderid->execute(array($row['shipment_id']));
                $salesorderid = $get_salesorderid->fetch()['salesorderid'];  
         
            // Salesorder card
            // Loading works
            if(!empty($row['loading_works']) && !empty($salesorderid)){
             update_vtiger_salesorder($dbh, 'vtiger_salesordercf', 'cf_926', $row['loading_works'], $salesorderid);
             update_vtiger_invoice_stevedoring($dbh, $salesorderid);
            }           
         

            // Idle
            if($row['idle'] > 0 && !empty($salesorderid)){
                update_vtiger_salesorder($dbh, 'vtiger_salesorder', 'prastova', $row['idle'], $salesorderid);
                update_vtiger_invoice_delay($dbh, $salesorderid);
            }

            $remarks_string = '';
            // Remarks           
            if(!empty($remarks)){
              $remarks = json_decode($remarks);
              foreach($remarks as $remark){
                  $remarks_string .= $remark . ', ' ;
              }
              $remarks_string = rtrim($remarks_string,',');
              if(!empty($salesorderid)){
               update_vtiger_salesorder($dbh, 'vtiger_salesordercf', 'cf_1940', $remarks_string, $salesorderid);
              }
            }

            // Comments
            if(!empty($row['comments']) && !empty($salesorderid)){
             update_vtiger_salesorder($dbh, 'vtiger_salesordercf', 'cf_1942', $row['comments'], $salesorderid);
            }
          

            // Kokybe card            

            
            $kokybe_existing = $dbh->prepare('SELECT kokybeid as id FROM vtiger_kokybe WHERE kokybeno = :salesorderid');   
             
            $kokybe_existing->setFetchMode(PDO::FETCH_ASSOC);
            $kokybe_existing->execute([':salesorderid' => $salesorderid]);
           
            if(!$kokybe_existing->rowCount()){
                $sth2 = $dbh->prepare('INSERT INTO vtiger_kokybe (kokybeid,
                                    kokybeno,
                                    kokybe_tks_deliveryremarks,
                                    kokybe_tks_deliverycomment,
                                    kokybe_tks_loadingworks, 
                                    kokybe_tks_idletime) 
                                    VALUES (?,?,?,?,?,?)');
                
                $sth = $dbh->prepare("SELECT shipment_code, e.smownerid, s.salesorderid   
                                FROM vtiger_salesorder s
                                JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                WHERE e.deleted = 0  AND s.salesorderid = :salesorderid");

                $sth->setFetchMode(PDO::FETCH_ASSOC);
                $sth->execute([':salesorderid' => $salesorderid]);

                $sth5 = $dbh->prepare("INSERT INTO vtiger_kokybecf (kokybeid) VALUES (?)");
                $orders_count = $sth->rowCount();
              
                if($orders_count > 0){    
                  $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
                  $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count);          
                }

              // // Kokybe card

                  if($remarks_string){
                    $notifications['marks'] = $remarks_string;                    
                  }

                  if($row['comments']){
                    $notifications['comments'] = $row['comments'];                   
                  }

                  if($row['idle']){
                    $notifications['idle'] = $row['idle'];                  
                  }

                  foreach($notifications AS $key => $notification){
                    createNotification($dbh, $salesorderid,$notification,$key,'kokybe');
                  }

                  
                  $notifications = array();
                  
                  if($remarks_string){
                      $notifications['marks'] = $remarks_string;                     
                    }
                    
                    if($row['comments']){
                      $notifications['comments'] = $row['comments'];                     
                    }

                    if($row['idle']){
                      $notifications['idle'] = $row['idle'];                     
                    }
                    
                    foreach($notifications AS $key => $notification){
                      createNotification($dbh, $operation_point['salesorderid'],$notification,$key,'kokybe');
                    }
                    
                    $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
            
                if($sth->rowCount()){
                  while ($row2 = $sth->fetch()) {  
                      
                      $date = date("Y-m-d H:i:s");
                
                      $sth6->execute(array($last_entity_record)); 
                      $record_exist = $sth6->rowCount();
                    
                      if($record_exist) {
                        $last_entity_record_old = $last_entity_record;
                        $sth3->execute(array());
                        $seq = $sth3->fetch();
                        $last_entity_record = $seq['id']+1;
                        $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
                        $sth4->execute(array($last_entity_record));     
                      }
                
                        $insert_entity = insert_entity3($dbh, 'Kokybe', $last_entity_record, $row2['smownerid'], NULL,'CRM',  $row2['shipment_code'], $date);
                          
                        $sth2->execute(array(  
                            $last_entity_record,  
                            $operation_point['salesorderid'],
                            $remarks_string,              
                            $row['comments'],             
                            ($row['loading_works'] ?:0),             
                            $row['idle']                
                        )); 
                    
                        $insert_entity_rel = insert_entityrel($dbh, $operation_point['salesorderid'], 'SalesOrder', $last_entity_record, 'Kokybe');
                        $sth5->execute(array($last_entity_record));
                                    
                        if($record_exist) {             
                            $last_entity_record = $last_entity_record_old;    
                        }         
                        
                      $sth2->execute(array(  
                          $last_entity_record,  
                          $salesorderid,
                          $remarks_string,              
                          $row['comments'],             
                          $row['loading_works'],             
                          $row['idle']                
                      )); 
                    
                      $insert_entity_rel = insert_entityrel($dbh, $salesorderid, 'SalesOrder', $last_entity_record, 'Kokybe');
                      $sth5->execute(array($last_entity_record));
                                  
                      if($record_exist) {             
                          $last_entity_record = $last_entity_record_old;    
                      }         
                      
                      $last_entity_record++;                   
                }
                           
            } else { 

                $sth = $dbh->prepare('UPDATE vtiger_kokybe SET kokybe_tks_deliveryremarks = ? ,
                                                               kokybe_tks_deliverycomment = ? , 
                                                               kokybe_tks_loadingworks = ? , 
                                                               kokybe_tks_idletime = ? 
                                                            WHERE  kokybeno = ?'); 
                $sth->execute(array($remarks_string, $row['comments'], $row['loading_works'], $row['idle'], $salesorderid));
                if($remarks_string){
                    $notifications['marks'] = $remarks_string;                    
                  }
                }
                            
              } else {
                  $query = 'UPDATE vtiger_kokybe SET kokybe_tks_deliveryremarks = ? ,
                                                    kokybe_tks_deliverycomment = ? , 
                                                    kokybe_tks_loadingworks = ? , 
                                                    kokybe_tks_idletime = ? 
                                                  WHERE  kokybeno = ?';

                  if($row['comments']){
                    $notifications['comments'] = $row['comments'];                    
                  }

                  if($row['idle']){
                    $notifications['idle'] = $row['idle'];                    
                  }
              
                foreach($notifications AS $key => $notification){
                    createNotification($dbh, $salesorderid,$notification,$key,'kokybe');
                }
            }
            
            
        }
          
          $dbh->commit();

      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
       sendReportMail($e->getMessage(),basename(__FILE__));
    }


    function getLastEntityRecord($dbh,$orders_count){     

        $sth3 = $dbh->prepare("SELECT id FROM `vtiger_crmentity_seq`");
        $sth3->setFetchMode(PDO::FETCH_ASSOC);
        $sth4 = $dbh->prepare("UPDATE `vtiger_crmentity_seq` SET id = ?");
  
        $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq READ');
        $lock->execute(array());     
        $sth3->execute(array());
        $seq = $sth3->fetch();
        $lock2 = $dbh->prepare('UNLOCK TABLES');
        $lock2->execute(array());
  
        $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq WRITE');
        $lock->execute(array());
        $new_seq = $seq['id'] + $orders_count;     
        $sth4->execute(array($new_seq)); 
        $lock2 = $dbh->prepare('UNLOCK TABLES');
        $lock2->execute(array());      
        
        $last_entity_record = $seq['id']+1;    

        return $last_entity_record;      
    }