<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');  
        
        // Patikrinam kurie yra ir tuos kuriu nera irasom

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_route_point_operation_log 
                                LEFT JOIN app_metrika_route_point_operation  ON route_point_operation_id=row_id
                                WHERE route_point_operation_id IS NULL');


        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $table = 'route_point_operation';       
        $where = "route_point_operation_id IN (";

        while ($row = $sth->fetch()) {
        $where .= $row['row_id'].","; 
        }

        $where = rtrim($where, ',');
        $where .= ")";

        $insert_query = 'INSERT INTO app_metrika_route_point_operation (delivery_id,route_point_id,failed,route_point_operation_id) VALUES (?,?,?,?)';
        $execute = array('delivery_id','route_point_id','failed','route_point_operation_id');

        getTable($table, $dbh, $where, $insert_query, $execute);    


        // Patrikrinam kurie yra ir tuos kurie yra atnaujinam

        $sth3 = $dbh->prepare('SET SESSION group_concat_max_len = 1000000');
        $sth3->execute(array());

        $sth2 = $dbh->prepare('SELECT row_id FROM app_metrika_route_point_operation_log 
                             WHERE row_id IN (SELECT route_point_operation_id FROM app_metrika_route_point_operation WHERE route_point_operation_id IS NOT NULL)');


        $sth2->setFetchMode(PDO::FETCH_ASSOC);
        $sth2->execute(array());

        $table2 = 'route_point_operation';       
        $where2 = "route_point_operation_id IN (";

        while ($row = $sth2->fetch()) {
        $where2 .= $row['row_id'].","; 
        }

        $where2 = rtrim($where2, ',');
        $where2 .= ")";

        $insert_query2 = 'UPDATE app_metrika_route_point_operation SET delivery_id = ?, route_point_id = ?, failed = ? WHERE route_point_operation_id = ?';
        $execute2 = array('delivery_id','route_point_id','failed','route_point_operation_id');

        getTable($table2, $dbh, $where2, $insert_query2, $execute2);    
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
       sendReportMail($e->getMessage(),basename(__FILE__));
    }