<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php'); 

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_route_point_failed_reason_log
                            WHERE row_id NOT IN(SELECT route_point_operation_id FROM app_metrika_route_point_failure_reason)');

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        // Get operation points that are not yet updated
        $where = '';
        while ($row = $sth->fetch()) {
            $where .= $row['row_id'].","; 
        }
        $where = rtrim($where, ',');
        // Preparing insert and update queries
        $insert_query = 'INSERT INTO app_metrika_route_point_failure_reason (route_point_operation_id,failure_reason_id, operation_type_id) VALUES (?,?,?)';
        $update_query = 'UPDATE app_metrika_route_point_failure_reason SET failure_reason_id = ? WHERE route_point_operation_id = ?';
        
        $sth_insert = $dbh->prepare($insert_query);
        $sth_update = $dbh->prepare($update_query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/route_point_operation_status.php");              
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'where_list' => $where));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        
        // print_r(curl_error($ch));
        curl_close($ch);
        $data = json_decode($result, true);
        foreach($data['response']['result'] AS $row){
            // print_r($row);
            $query = "SELECT id FROM app_metrika_failure_reasons WHERE external_reason_id = ?";
            $sth2 = $dbh->prepare($query);
            $sth2->setFetchMode(PDO::FETCH_ASSOC);
            $sth2->execute(array($row['failure_reason_id']));
            $fail_id = $sth2->fetch()['id'];
            
            if(!$fail_id){
                $query = "INSERT INTO app_metrika_failure_reasons (external_reason_id, reason) VALUES (?, ?)";
                $sth_fail = $dbh->prepare($query);
                $sth_fail->setFetchMode(PDO::FETCH_ASSOC);
                $sth_fail->execute(array($row['failure_reason_id'], $row['reason']));
                $fail_id = $dbh->lastInsertId();
            } 
            // Query to check if route point operation is already added
            $query = "SELECT route_point_operation_id FROM app_metrika_route_point_failure_reason WHERE route_point_operation_id = ?";
            $sth3 = $dbh->prepare($query);
            $sth3->setFetchMode(PDO::FETCH_ASSOC);
            $sth3->execute(array($row['route_point_operation_id']));
            if(!$sth3->fetch()){
                $sth_insert->execute(array($row['route_point_operation_id'], $fail_id, $row['operation_type_id']));
                
            } else{
                $sth_update->execute(array($fail_id ,$row['route_point_operation_id']));
            }

            $query = "SELECT route_point_operation.route_point_operation_id, s.salesorderid
                    FROM app_metrika_shipment as shipment
                    LEFT JOIN app_metrika_delivery as delivery ON delivery.shipment_id=shipment.shipment_id
                    LEFT JOIN app_metrika_route_point_operation as  route_point_operation ON  route_point_operation.delivery_id=delivery.delivery_id
                    LEFT JOIN app_metrika_route_point as route_point ON route_point.route_point_id=route_point_operation.route_point_id
                    JOIN vtiger_salesorder s ON s.external_order_id=shipment.shipment_id
                    WHERE route_point_operation.route_point_operation_id = ". $row['route_point_operation_id'];
            
            $sth6 = $dbh->prepare($query);
            $sth6->setFetchMode(PDO::FETCH_ASSOC);
            $sth6->execute(array());
            $operation_point = $sth6->fetch();
            print_r($operation_point['salesorderid']);
            $sostatus = NULL;
            if($fail_id == 1 && $row['operation_type_id'] == 'U'){
                $sostatus = "Not Delivered";
                $row['reason'] = $row['reason'] . '(pristatymas)';
            } else if($fail_id == 1 && $row['operation_type_id'] == 'L'){
                $sostatus = "Not Delivered";
                $row['reason'] = $row['reason'] . '(pasikrovimas)';
            } else{
                $sostatus = "Not Delivered";
            }
            // print_r($operation_point['salesorderid'] . " and " . $row['route_point_operation_id']);
            update_fail_reason($dbh, $operation_point['salesorderid'], $sostatus, $row['reason']);

            // Kokybe card
            $dbh->beginTransaction(); 

            $select_query = 'SELECT kokybeid as id FROM vtiger_kokybe WHERE kokybeno = '. $operation_point['salesorderid'];
            $kokybe_existing = $dbh->prepare($select_query);   
             
            $kokybe_existing->setFetchMode(PDO::FETCH_ASSOC);
            $kokybe_existing->execute(array());
            $kokybe_checker = $kokybe_existing->fetch()['id'];
            if(!$kokybe_checker){
                $sth2 = $dbh->prepare('INSERT INTO vtiger_kokybe (kokybeid,
                                    kokybeno,
                                    kokybe_tks_deliveryfailreason) 
                                    VALUES (?,?,?)');
                
                $sth = $dbh->prepare("SELECT shipment_code, e.smownerid, s.salesorderid   
                                FROM vtiger_salesorder s
                                JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                WHERE e.deleted = 0  AND s.salesorderid = ?");
                                
                $sth->setFetchMode(PDO::FETCH_ASSOC);
                $sth->execute(array($operation_point['salesorderid']));
                $sth5 = $dbh->prepare("INSERT INTO vtiger_kokybecf (kokybeid) VALUES (?)");
                $orders_count = $sth->rowCount();
                // print_r($orders_count);
                if($orders_count > 0){

                    $sth3 = $dbh->prepare("SELECT id FROM `vtiger_crmentity_seq`");
                    $sth3->setFetchMode(PDO::FETCH_ASSOC);
                    $sth4 = $dbh->prepare("UPDATE `vtiger_crmentity_seq` SET id = ?");
              
                    $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq READ');
                    $lock->execute(array());     
                    $sth3->execute(array());
                    $seq = $sth3->fetch();
                    $lock2 = $dbh->prepare('UNLOCK TABLES');
                    $lock2->execute(array());
              
                    $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq WRITE');
                    $lock->execute(array());
                    $new_seq = $seq['id'] + $orders_count;     
                    $sth4->execute(array($new_seq)); 
                    $lock2 = $dbh->prepare('UNLOCK TABLES');
                    $lock2->execute(array());
                  
              
                    $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
                    $last_entity_record = $seq['id']+1; 
                    
                    
                  }

                  createNotification($dbh, $operation_point['salesorderid'],$row['reason'],'status');

                  while ($row2 = $sth->fetch()) {  
                    
                    $date = date("Y-m-d H:i:s");
              
                    $sth6->execute(array($last_entity_record)); 
                    $record_exist = $sth6->rowCount();
                   
                    if($record_exist) {
                       $last_entity_record_old = $last_entity_record;
                       $sth3->execute(array());
                       $seq = $sth3->fetch();
                       $last_entity_record = $seq['id']+1;
                       $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
                       $sth4->execute(array($last_entity_record));     
                    }
              
                      $insert_entity = insert_entity3($dbh, 'Kokybe', $last_entity_record, $row2['smownerid'], NULL,'CRM',  $row2['shipment_code'], $date);
                        
                      $sth2->execute(array(  
                          $last_entity_record,  
                          $operation_point['salesorderid'],       
                          $row['reason']           
                      )); 
                    //   print_r($rel_id);
                      $insert_entity_rel = insert_entityrel($dbh, $operation_point['salesorderid'], 'SalesOrder', $last_entity_record, 'Kokybe');
                      $sth5->execute(array($last_entity_record));
                                  
                      if($record_exist) {             
                          $last_entity_record = $last_entity_record_old;    
                      }         
                      
                      $last_entity_record++;
                }
                           
            } else {
                $query = 'UPDATE vtiger_kokybe SET kokybe_tks_deliveryfailreason = ? WHERE  kokybeno = ?';

                $sth = $dbh->prepare($query); 
                $sth->execute(array( $row['reason'], $operation_point['salesorderid']));
                createNotification($dbh, $operation_point['salesorderid'],$row['reason'],'status');
                // print_r("update");
            }
            
            $dbh->commit();
        }

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
