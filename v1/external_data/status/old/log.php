<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');   
        
        $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update_for_update_orders` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
        $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
        $last_timestamp->execute(array());
        $date_from = $last_timestamp->fetch();
        $date_from = $date_from['date'];

        $table = 'log';
        $where = "table_id = 5 AND to_char(entry_date,'YYYY-MM-DD HH24:MI:SS') > '$date_from'"; 
      
        $insert_query = 'INSERT INTO app_metrika_shipments_log (row_id) VALUES (?)';
        $execute = array('row_id');

        $sth = $dbh->prepare('TRUNCATE app_metrika_shipments_log');
        $sth->execute(array());

        getTable($table, $dbh, $where,$insert_query,$execute);       
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
