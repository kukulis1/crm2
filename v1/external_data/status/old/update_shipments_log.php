<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');   

        $check = $dbh->prepare("INSERT INTO check_status_cron (date_time) VALUES (?)");
        $check->execute(array(date("Y-m-d H:i:s")));

        $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update_for_update_orders` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
        $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
        $last_timestamp->execute(array());
        $date_from = $last_timestamp->fetch();
        $date_from = $date_from['date'];   

        // $date_from =  '2020-10-29 18:00:00';

        $table = 'log';
        $where = "table_id = 5 AND del_date IS NULL AND  to_char(entry_date,'YYYY-MM-DD HH24:MI:SS') < to_char(edit_date,'YYYY-MM-DD HH24:MI:SS') AND to_char(edit_date,'YYYY-MM-DD HH24:MI:SS') > '$date_from' "; 
        
  
        $insert_query = 'INSERT INTO app_metrika_update_shipments_log (row_id) VALUES (?)';
        $execute = array('row_id');

        $sth = $dbh->prepare('TRUNCATE app_metrika_update_shipments_log');
        $sth->execute(array());

        getTable($table, $dbh, $where,$insert_query,$execute);       
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
