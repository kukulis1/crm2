<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');           

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_deleted_route_point_log');

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $where = "route_point_id IN (";

        while ($row = $sth->fetch()) {
         $where .= $row['row_id'].","; 
        }

        $where = rtrim($where, ',');
        $where .= ")";     

        $sth =  $dbh->prepare("DELETE FROM app_metrika_route_point WHERE $where");
        $sth->execute(array());
    
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
