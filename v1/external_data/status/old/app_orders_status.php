<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php'); 

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_shipments_log shipments_log 
                                            LEFT JOIN app_metrika_shipment ON  shipment_id=row_id  
                                            WHERE shipment_id IS NULL');


        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $where = "shipment_id IN (";

        while ($row = $sth->fetch()) {
            $where .= $row['row_id'].","; 
        }

        $where = rtrim($where, ',');
        $where .= ")";

        $table = 'shipment'; 

        $insert_query = 'INSERT INTO app_metrika_shipment (shipment_id,sender_location_id,consignee_location_id) VALUES (?,?,?)';
        $execute = array('shipment_id','sender_location_id','consignee_location_id');

        getTable($table, $dbh, $where,$insert_query,$execute);        
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
