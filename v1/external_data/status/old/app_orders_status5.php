<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');  
        
        $sth = $dbh->prepare('SELECT route_point.location_id FROM app_metrika_route_point route_point
                              LEFT JOIN app_metrika_location ON app_metrika_location.location_id=route_point.location_id
                              WHERE app_metrika_location.location_id IS NULL');

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $where = "location_id IN (";

        while ($row = $sth->fetch()) {
        $where .= $row['location_id'].","; 
        }

        $where = rtrim($where, ',');
        $where .= ")";

        $table = 'location';
        $insert_query = 'INSERT INTO app_metrika_location (location_id) VALUES (?)';
        $execute = array('location_id');
        getTable($table, $dbh, $where,$insert_query, $execute);       
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
 


