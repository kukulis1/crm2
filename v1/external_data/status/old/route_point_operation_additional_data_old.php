<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php'); 

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_route_point_additional_data_log
                            WHERE row_id NOT IN(SELECT route_point_operation_id FROM app_metrika_route_point_additional_data)');;



        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        
        $where = "route_point_operation_id IN (";

        while ($row = $sth->fetch()) {
            $where .= $row['row_id'].","; 
        }
        
        $where = rtrim($where, ',');
        $where .= ")";

        $table = 'itoma_route_point_additional_data'; 

        $insert_query = 'INSERT INTO app_metrika_route_point_additional_data (route_point_operation_id,loading_works,idle,remarks,comments) VALUES (?,?,?,?,?)';
        $update_query = 'UPDATE app_metrika_route_point_additional_data SET loading_works = ? , idle = ? , remarks = ? , comments = ?  WHERE route_point_operation_id = ?';
        
        $sth_insert = $dbh->prepare($insert_query);
        $sth_update = $dbh->prepare($update_query);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'table' => $table, 'where' => $where));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);


        foreach($data['response']['result'] AS $row){
            $remarks = $row['remarks'];
            // Query to check if route point operation is already added
            $query = "SELECT route_point_operation_id FROM app_metrika_route_point_additional_data WHERE route_point_operation_id = ?";
            $sth3 = $dbh->prepare($query);
            $sth3->setFetchMode(PDO::FETCH_ASSOC);
            $sth3->execute(array($row['route_point_operation_id']));
            if(!$sth3->fetch()){
                $sth_insert->execute(array($row['route_point_operation_id'], $row['loading_works'], $row['idle'], $remarks, $row['comments']));
            } else{
                $sth_update->execute(array($row['loading_works'], $row['idle'], $remarks, $row['comments'], $row['route_point_operation_id']));
            }
            // print_r($row['route_point_operation_id']);
            $query = "SELECT route_point_operation.route_point_operation_id, s.salesorderid
                    FROM app_metrika_shipment as shipment
                    LEFT JOIN app_metrika_delivery as delivery ON delivery.shipment_id=shipment.shipment_id
                    LEFT JOIN app_metrika_route_point_operation as  route_point_operation ON  route_point_operation.delivery_id=delivery.delivery_id
                    LEFT JOIN app_metrika_route_point as route_point ON route_point.route_point_id=route_point_operation.route_point_id
                    JOIN vtiger_salesorder s ON s.external_order_id=shipment.shipment_id
                    WHERE route_point_operation.route_point_operation_id = ". $row['route_point_operation_id'];
            $sth6 = $dbh->prepare($query);
            $sth6->setFetchMode(PDO::FETCH_ASSOC);
            $sth6->execute(array());
            $operation_point = $sth6->fetch();
            
            // Updating every value returned
            // Loading works
            
            update_vtiger_salesorder($dbh, 'vtiger_salesordercf', 'cf_926', $row['loading_works'], $operation_point['salesorderid']);
            if($row['loading_works']){
                update_vtiger_invoice_stevedoring($dbh, $operation_point['salesorderid']);
            }
            // Idle
            update_vtiger_salesorder($dbh, 'vtiger_salesorder', 'prastova', $row['idle'], $operation_point['salesorderid']);
            if(floatval($row['idle']) > 0){
                update_vtiger_invoice_delay($dbh, $operation_point['salesorderid']);
            }
            // Remarks
            // print_r(json_decode($remarks));
            $remarks = json_decode($remarks);
            $remarks_string = '';
            foreach($remarks as $remark){
                $remarks_string .= $remark . '. ' ;
            }
            update_vtiger_salesorder($dbh, 'vtiger_salesordercf', 'cf_1940', $remarks_string, $operation_point['salesorderid']);
            // Comments
            update_vtiger_salesorder($dbh, 'vtiger_salesordercf', 'cf_1942', $row['comments'], $operation_point['salesorderid']);

        }

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
