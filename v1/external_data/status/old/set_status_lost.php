<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php'); 

        $sth6 = $dbh->prepare("SELECT shipment.shipment_id, route_point.route_point_id, shipment.sender_location_id, shipment.consignee_location_id, 
        route_point.location_id, route_point.is_visited, s.salesorderid,
            CASE 
                WHEN location_id IS NULL THEN 'Sent'
                WHEN sender_location_id = location_id AND is_visited = 0 THEN 'Approved'
                WHEN sender_location_id != location_id AND is_visited = 0 AND consignee_location_id = location_id THEN 'in progress'
                WHEN sender_location_id != location_id AND is_visited = 1 AND consignee_location_id != location_id THEN 'in progress'			   
                WHEN sender_location_id = location_id AND is_visited = 1 AND consignee_location_id != location_id THEN 'in progress'
                WHEN sender_location_id != location_id AND is_visited = 1 AND consignee_location_id = location_id THEN 'delivered'
                WHEN consignee_location_id = location_id AND is_visited = 1 THEN 'delivered'
                ELSE 'in progress'
            END as status
                    FROM app_metrika_shipment as shipment
                    LEFT JOIN app_metrika_delivery as delivery ON delivery.shipment_id=shipment.shipment_id
                    LEFT JOIN app_metrika_route_point_operation as  route_point_operation ON  route_point_operation.delivery_id=delivery.delivery_id
                    LEFT JOIN app_metrika_route_point as route_point ON route_point.route_point_id=route_point_operation.route_point_id
                    JOIN vtiger_salesorder s ON s.external_order_id=shipment.shipment_id
                    WHERE shipment.shipment_id = ?  AND s.sostatus != 'delivered' AND s.carrier IS NULL AND (failed = 0 OR failed IS NULL)                  
                    HAVING status = 'delivered'
                    ORDER BY route_point.id DESC");                    

        $sth3 = $dbh->prepare('SELECT row_id FROM app_metrika_route_point_log');          
        
        $sth4 = $dbh->prepare("SELECT shipment.shipment_id
                            FROM app_metrika_shipment as shipment
                            LEFT JOIN app_metrika_delivery as delivery ON delivery.shipment_id=shipment.shipment_id
                            LEFT JOIN app_metrika_route_point_operation as  route_point_operation ON  route_point_operation.delivery_id=delivery.delivery_id
                            LEFT JOIN app_metrika_route_point as route_point ON route_point.route_point_id=route_point_operation.route_point_id
                            JOIN vtiger_salesorder s ON s.external_order_id=shipment.shipment_id                           
                            WHERE unload_date_from = '2021-04-04'
                            GROUP BY shipment.shipment_id
                            ORDER BY route_point.id DESC");



        $sth5 = $dbh->prepare('UPDATE vtiger_salesorder SET sostatus = ?  WHERE external_order_id = ?');
        $sth7 = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ?  WHERE crmid = ?');
       
        $sth9 = $dbh->prepare('INSERT INTO app_status_log (salesorderid, status, code) VALUES (?,?,?)');
        $sth11 = $dbh->prepare('SELECT code FROM app_status_log WHERE salesorderid = ?');
     
        $status = array('Sent' => 'Naujas','Approved' => 'Priimtas','in progress' => 'Vykdomas','delivered' => 'Atliktas');
        $status2 = array('Sent' => 1,'Approved' => 2,'in progress' => 3,'delivered' => 4);
        $date = date("Y-m-d H:i:s");

    
        $sth3->setFetchMode(PDO::FETCH_ASSOC);
        $sth3->execute(array());

        $sth4->setFetchMode(PDO::FETCH_ASSOC);
        $status_arr = array();
                       
            $sth4->execute(array());
            while ($row2 = $sth4->fetch()) {                          
                $sth6->execute(array($row2['shipment_id']));
                while ($rec = $sth6->fetch()) {                     
                    $sth11->execute(array($rec['salesorderid']));
                    while($item = $sth11->fetch()){
                        $status_arr[] =  $item['code'];
                    }  
                           
                    // if(!in_array($status2[$rec['status']],$status_arr)){  
                        $sth5->execute(array($rec['status'],$rec['shipment_id']));   
                        $sth7->execute(array($date,$rec['salesorderid']));  
                        $sth9->execute(array($rec['salesorderid'], $rec['status'],$status2[$rec['status']]));
                        sentStatus($rec['shipment_id'],$rec['salesorderid'],$status[$rec['status']]);                                                         
                        // $id = last_modtracker_record($dbh);
                        // insert_modtracker_basic($dbh, $id, $rec['salesorderid'], 'SalesOrder', 26, $date,0);
                        // insert_modtracker_detail($dbh, $id, 'sostatus', $sostatus['sostatus'], $rec['status']);
                    // }     
                }  
            }     
        

      
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
       sendReportMail($e->getMessage(),basename(__FILE__));
    }


    
 


