<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');   
        

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_route_point_log  
                              LEFT JOIN app_metrika_route_point ON route_point_id=row_id
                              WHERE route_point_id IS NULL');


        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $where = "route_point_id IN (";

        while ($row = $sth->fetch()) {
        $where .= $row['row_id'].","; 
        }

        $where = rtrim($where, ',');
        $where .= ")";


        $table = 'route_point';

        $insert_query = 'INSERT INTO app_metrika_route_point (route_id, route_point_id, location_id, is_visited) VALUES (?,?,?,?)';
        $execute = array('route_id', 'route_point_id', 'location_id', 'is_visited');  

        getTable($table, $dbh, $where,$insert_query,$execute);           
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
