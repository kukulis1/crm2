<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php');       
        
        $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update_for_update_orders` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
        $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
        $last_timestamp->execute(array());
        $date_from = $last_timestamp->fetch();
        $date_from = $date_from['date']; 

        $logfile = 'set_status.log';

        $log_data = 'BEGIN '.microtime().' | ';
        $log_data .= 'FROM '.$date_from.' | ';       
    

        $last_update = $dbh->prepare("INSERT INTO app_last_update_for_update_orders (status,date) VALUES (?,?)");
        $last_update->execute(array('success',date('Y-m-d H:i:').'00'));

        $truncate = $dbh->prepare('TRUNCATE app_metrika_statuses'); 
        $truncate->execute(array());

        $select = "DISTINCT shipment.shipment_id,departure_time_fact,
        CASE 
            WHEN location_id IS NULL THEN 1
            WHEN sender_location_id = location_id AND is_visited is false THEN 2
            WHEN sender_location_id != location_id AND is_visited is false AND consignee_location_id = location_id THEN 3
            WHEN sender_location_id != location_id AND is_visited is true AND consignee_location_id != location_id THEN 3			   
            WHEN sender_location_id = location_id AND is_visited is true AND consignee_location_id != location_id THEN 3
            WHEN sender_location_id != location_id AND is_visited is true AND consignee_location_id = location_id THEN 4
            WHEN consignee_location_id = location_id AND is_visited is true THEN 4
            ELSE 3
        END as status";
        $table = 'shipment
                    LEFT JOIN delivery ON delivery.shipment_id=shipment.shipment_id
                    LEFT JOIN route_point_operation ON  route_point_operation.delivery_id=delivery.delivery_id
                    LEFT JOIN route_point ON route_point.route_point_id=route_point_operation.route_point_id';       
        $where = "to_char(route_point.departure_time_fact,'YYYY-MM-DD HH24:MI:SS') > '$date_from'  AND (failed = false OR failed IS NULL) ";   
        $where .= " GROUP BY shipment.shipment_id,location_id,is_visited,departure_time_fact
        ORDER BY departure_time_fact";      

        $insert_query = $dbh->prepare('INSERT INTO app_metrika_statuses (shipment_id, status) VALUES (?,?)');   
        $status_lang = array('Sent' => 'Naujas','Approved' => 'Priimtas','in progress' => 'Vykdomas','delivered' => 'Atliktas');

        $status_lang_inverted = array('Naujas' => 1, 'Priimtas' => 2,'Vykdomas' => 3, 'Atliktas' => 4);
       
        $statuses = [
            1 => 'Sent', 
            2 => 'Approved', 
            3 => 'in progress', 
            4 => 'delivered',
        ];
 

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);  

        $grouped_statuses = [];
        foreach($data['response']['result'] as $status){
            if($status['status'] > ($grouped_statuses[$status['shipment_id']] ?? 0)){
                $grouped_statuses[$status['shipment_id']] = $status['status'];
            }
        }


        foreach($grouped_statuses as $shipment_id => $status_key){
            $insert_query->execute([$shipment_id, $statuses[$status_key]]);
        }       

        $set_session_wait_time = $dbh->prepare("SET SESSION innodb_lock_wait_timeout = 900");
        $set_session_wait_time->execute(array());

        $update_status = $dbh->prepare('UPDATE vtiger_salesorder SET sostatus = ?  WHERE salesorderid = ?');
        $change_status = $dbh->prepare("SELECT shipment_id,m.status,salesorderid
                                        FROM app_metrika_statuses m
                                        JOIN vtiger_salesorder ON external_order_id=shipment_id 
                                        WHERE vtiger_salesorder.sostatus != 'delivered'
                                        ORDER BY id DESC");

        $change_status->setFetchMode(PDO::FETCH_ASSOC);
        $change_status->execute(array()); 
        
        $log_data .= 'MIDDLE '.microtime().' STATUSES UPDATED | ';


        $status_to_send_in_metrika = [];

        $log_data .= 'SEND STATUSES TO METRIKA '.microtime().' | '; 
        
        foreach($change_status AS $status){
            $update_status->execute(array($status['status'], $status['salesorderid'])); 
            
            $log_data .= $status['salesorderid'].': '.$status_lang[$status['status']].' | ';
            
            $status_to_send_in_metrika[$status['shipment_id']] = [
                'shipment_crm_id' => $status['salesorderid'],
                'shipment_id' => $status['shipment_id'],
                'invoice_price' => null,
                'status' => $status_lang[$status['status']]
            ];
            
        }       

 
        $statuses_json = json_encode($status_to_send_in_metrika);          

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/shipment_crm_price_and_status.php");           
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'shipment_data' => $statuses_json));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $response = curl_exec($ch);
        curl_close($ch);

        $response_data = json_decode($response, true);

        $insert_status = $dbh->prepare("INSERT INTO app_status_log (salesorderid, status, code, timestamp) VALUES (:salesorderid, :status, :code, :timestamp)");

        foreach ($response_data as $value) {           
            $insert_status->execute([
                ':salesorderid' => $value['shipment_crm_id'], 
                ':status' => $value['status'], 
                ':code' => $status_lang_inverted[$value['status']], 
                ':timestamp' => date('Y-m-d H:i:s')
            ]);           
        }

        $log_data .= 'SEND STATUSES TO METRIKA END '.microtime().' | ';


        customers_log($log_data, $logfile);
         
        checkFirsOrderNotifications($dbh);
  

    } catch (PDOException $e) {
    //    echo "Error!";
    //    echo $e->getMessage();
       sendReportMail($e->getMessage(),basename(__FILE__));
    }
 

    function checkFirsOrderNotifications($dbh){
        $get_user_info = $dbh->prepare("SELECT COUNT(vtiger_salesorder.salesorderid) AS orders, vtiger_account.accountid,vtiger_account.accountname
        FROM vtiger_account
        JOIN vtiger_salesorder ON vtiger_salesorder.accountid=vtiger_account.accountid 
        LEFT JOIN vtiger_notification_first_order n ON n.accountid=vtiger_account.accountid 
        INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_account.accountid
        WHERE setype = 'Accounts' AND DATE_ADD(vtiger_crmentity.createdtime, INTERVAL 90 DAY) >= now() AND sostatus = 'delivered' AND deleted = 0 AND n.id IS NULL
        GROUP BY vtiger_account.accountid
        HAVING orders BETWEEN 0 AND 5");
      
        $get_user_info->execute(array());
        $changed_record_count = $get_user_info->rowCount();
      
        if($changed_record_count > 0){           
         
          $insert_notification_time = $dbh->prepare("INSERT INTO vtiger_notification_first_order (accountid,createdtime) VALUES (?,?)");
      
          foreach($get_user_info AS $user){  
            $insert_notification_time->execute(array($user['accountid'], date('Y-m-d H:i:s')));  
            $accountname = $user['accountname'];
            $text = "Kliento $accountname pirmas užsakymas pristatytas, paskambinkite klientui";
            
            createNotification($dbh, $user['accountid'], $text, 26, 'pirmas_uzsakymas');
          }
        }
    }
      