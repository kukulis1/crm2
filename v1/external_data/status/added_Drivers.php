<?php

set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

error_reporting(0);

    try {
        require('../mysql_connection.php'); 

        $dbh->beginTransaction();   

        $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update_for_update_orders` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
        $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
        $last_timestamp->execute(array());
        $date_from = $last_timestamp->fetch();
        $date_from = $date_from['date']; 
              
        $select = 'itoma_route_point_additional_data.route_point_operation_id, loading_works, idle, comments, shipment_id,driver1.driver_name AS driver1_name, driver2.driver_name AS driver2_name';

        $table = 'itoma_route_point_additional_data
        JOIN route_point_operation ON route_point_operation.route_point_operation_id=itoma_route_point_additional_data.route_point_operation_id
        JOIN delivery ON delivery.delivery_id=route_point_operation.delivery_id
        INNER JOIN route_point on route_point.route_point_id=route_point_operation.route_point_id
				INNER JOIN route ON route.route_id=route_point.route_id
        LEFT JOIN driver driver1 ON driver1.driver_id = route.driver1_id 
        LEFT JOIN driver driver2 ON driver2.driver_id = route.driver2_id
        JOIN log ON log.row_id=itoma_route_point_additional_data.route_point_operation_id'; 

        $where = "table_id = 18 AND entry_date IS NOT NULL AND to_char(entry_date,'YYYY-MM-DD HH24:MI:SS') > '$date_from'
        GROUP BY itoma_route_point_additional_data.route_point_operation_id, loading_works, idle, comments, shipment_id,driver1.driver_name,  driver2.driver_name ";


        $get_salesorderid = $dbh->prepare("SELECT salesorderid FROM vtiger_salesorder WHERE external_order_id = ?");
        $get_salesorderid->setFetchMode(PDO::FETCH_ASSOC);


        $get_all_operation_id = $dbh->prepare("SELECT route_point_operation_id FROM app_metrika_route_point_additional_data");
        $get_all_operation_id->setFetchMode(PDO::FETCH_ASSOC);
        $get_all_operation_id->execute(array());

        $operation_id = [];

        foreach ($get_all_operation_id as $value) {
          $operation_id[$value['route_point_operation_id']] = $value['route_point_operation_id'];
        }


        $check_drivers = $dbh->prepare('SELECT * FROM vtiger_loaderdriver
                                        INNER JOIN vtiger_crmentity ON crmid=loaderdriverid
                                        WHERE deleted = 0');
        $check_drivers->setFetchMode(PDO::FETCH_ASSOC);
        $check_drivers->execute(array());

        $all_drivers = [];
        foreach ($check_drivers as $value) {
          $all_drivers[$value['loaderdriver_tks_name'].$value['loaderdriver_tks_lastname']] =  $value['loaderdriver_tks_name']." ".$value['loaderdriver_tks_lastname'];
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);

        echo '<pre>';
        print_R($data);
        echo '</pre>';

        $drivers = [];
        foreach($data['response']['result'] AS $row){
          $drivers[$row['driver1_name']] = $row;
        }
     
        foreach($drivers AS $row){                   
          
            $get_salesorderid->execute(array($row['shipment_id']));
            $operation_point = $get_salesorderid->fetch();

            if($operation_point['salesorderid']){                
            
              $driver = explode(" ", $row['driver1_name']);                        
              
              $full_driver_name = $driver[0].' '.$driver[1];        
              $full_driver_name_withoutspaces = $driver[0].$driver[1];  
              update_vtiger_salesorder($dbh, 'vtiger_salesordercf', 'cf_1687', $full_driver_name, $operation_point['salesorderid']);

              if(empty($all_drivers[$full_driver_name_withoutspaces])){                 
                $last_entity_record = getLastEntityRecord($dbh,1);
                $insert_driver = $dbh->prepare("INSERT INTO vtiger_loaderdriver (loaderdriverid, loaderdriver_tks_name, loaderdriver_tks_lastname, loaderdriver_tks_position, loaderdriver_tks_chapter, loaderdriver_tks_visible) VALUES (?,?,?,?,?,?)");
                $insert_drivercf = $dbh->prepare("INSERT INTO vtiger_loaderdrivercf (loaderdriverid) VALUES (?)");
                $insert_driver->execute(array($last_entity_record,$driver[0],$driver[1],'Vairuotojas','','Yes'));
                $insert_drivercf->execute(array($last_entity_record));
                $insert_entity = insert_entity3($dbh, 'Loaderdriver', $last_entity_record, 26, NULL, 'Metrika',  $full_driver_name, date('Y-m-d H:i:s'));
               
              }             

            }               
        }
          
          $dbh->commit();

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }


    function getLastEntityRecord($dbh,$orders_count){     

        $sth3 = $dbh->prepare("SELECT id FROM `vtiger_crmentity_seq`");
        $sth3->setFetchMode(PDO::FETCH_ASSOC);
        $sth4 = $dbh->prepare("UPDATE `vtiger_crmentity_seq` SET id = ?");
  
        $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq READ');
        $lock->execute(array());     
        $sth3->execute(array());
        $seq = $sth3->fetch();
        $lock2 = $dbh->prepare('UNLOCK TABLES');
        $lock2->execute(array());
  
        $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq WRITE');
        $lock->execute(array());
        $new_seq = $seq['id'] + $orders_count;     
        $sth4->execute(array($new_seq)); 
        $lock2 = $dbh->prepare('UNLOCK TABLES');
        $lock2->execute(array());      
        
        $last_entity_record = $seq['id']+1;    

        return $last_entity_record;      
    }