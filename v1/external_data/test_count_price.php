<?php
include 'ws.config.php';
global $config;
require 'utils.php';
include 'getPriceFromPriceBook.php';
set_time_limit(500);
error_reporting(0);


try {

    require('mysql_connection.php');

    $dbh->beginTransaction();    

                   

$sth = $dbh->prepare("SELECT t.*, a.accountid, SUM(l.weight)  AS weight , SUM(l.length * l.width * l.height)  as volume, SUM(l.m3) AS m3, l.pll, c.salesorderid
                                FROM app_orders2 t
                                JOIN vtiger_account a ON a.customer_id = t.customer_id 
                                JOIN vtiger_crmentity e ON e.crmid = a.accountid     
                                JOIN app_loads2 l ON l.order_id = t.id           
                                JOIN vtiger_salesorder c ON c.external_order_id = t.id                               
                                LEFT JOIN vtiger_invoice_salesorders_list sl ON sl.salesorderid=c.salesorderid     
                                WHERE e.deleted = 0 AND sl.salesorderid IS NULL         
                                GROUP BY l.order_id");



    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                            WHERE salesorderid = ?');


    $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = ?, cf_1365 = ?, cf_1367 = ?, cf_1374 = ?, cf_1376 = ? WHERE   salesorderid = ? '); 
    $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ? WHERE id = ? '); 

    while ($row = $sth->fetch()) {

        $id = $row['accountid'];
        $weight = $row['weight'];
        $volume = ($row['m3'] > $row['volume'] ? $row['m3'] : $row['volume']);
        $volume_pll = $row['pll'] / 0.96;

        if(!empty($row['load_post_code']) AND !empty($row['unload_post_code']) AND !empty($weight)){
                $getPrice = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$weight,$volume, $volume_pll);  
                         

          if($getPrice['price'] == 0){
                $getPrice['pricebook'] =  $getPrice['combination'];                                       
          }
        }else{ 
            $getPrice = array('price' => 0);

            if(empty($row['load_post_code']) AND empty( $row['unload_post_code']) AND empty($weight)){
                $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
            }elseif(empty($row['load_post_code']) AND empty($row['unload_post_code'])){    
                $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
            }elseif(empty($row['load_post_code'])){
                $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
            }elseif(empty($row['unload_post_code'])){
                $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
            }elseif(empty($weight)){
                    $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
            }
           
        }          
       

         if(empty($row['price_agreed'])){
         	$price = $getPrice['price'];                        
         }else{
         	$price = $row['price_agreed'];
         }     
         

         $margin = $getPrice['price'];


         print_R($margin);

                    // $sth2->execute(array(
                    //                     $price,
                    //                     $price,                                                     
                    //                     $price,                                      
                    //                     $row['salesorderid']
                    // ));     

                    //  $sth3->execute(array(
                    //     $getPrice['pricebook'],
                    //     $row['m3'],
                    //     number_format($row['volume'],2),
                    //     $getPrice['price'],
                    //     $row['price_agreed'],
                    //     $row['salesorderid']          
                    // ));    
                    
                    
                    // $sth4->execute(array(                                                                                          
                    //                     $margin,                                                                        
                    //                     $row['salesorderid']
                    // ));
                   
                 
                
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
