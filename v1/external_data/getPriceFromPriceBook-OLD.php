<?php
  // NOTE Iterpiame countriesZones funkcija
  require_once $_SERVER['DOCUMENT_ROOT']."/price-algorithm/countries.php";   

function getPriceFromPriceBook($dbh,$post_from,$post_to,$accountid,$weight,$volume,$square,$pll,$distance,$country_from, $country_to,$which){

  // NOTE Gaunami miestai pagal pasto kodus
  if(!empty($post_from)){ 
    $post_code_from = $dbh->prepare("SELECT `zone_customer`,`zone_base`, `city`, `state`,`post_code` FROM `crm_post_codes` WHERE `post_code` = ".$post_from);

    if($country_from == 'LVA'){    
      $post_code_from = $dbh->prepare("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $post_from BETWEEN code_from AND code_to");
    }

    $post_code_from->execute();
    $from = $post_code_from->fetch();
  }

  if(!empty($post_to)){
    $post_code_to = $dbh->prepare("SELECT `zone_customer`,`zone_base`, `city`, `state`,`post_code` FROM `crm_post_codes` WHERE `post_code` = ".$post_to);

    if($country_to == 'LVA'){    
      $post_code_to = $dbh->prepare("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $post_to BETWEEN code_from AND code_to");
    }

    $post_code_to->execute();
    $to = $post_code_to->fetch();
  } 


  if(empty($from['zone_base']) AND empty($to['zone_base'])){
    return  array('price' => 0,  'combination' => 'Blogi pašto kodai', 'max_volume' => 0);
  }elseif(empty($from['zone_base'])){
      return  array('price' => 0,  'combination' => 'Blogas pakrovimo pašto kodas', 'max_volume' => 0);
  }elseif(empty($to['zone_base'])){
    return  array('price' => 0,  'combination' => 'Blogas iškrovimo pašto kodas', 'max_volume' => 0);
  }


  // NOTE Gaunamos kliento korteles zonos 
  $locations =  $dbh->prepare("SELECT * FROM `vtiger_accountscf`
                                                          LEFT JOIN `vtiger_account` on `vtiger_accountscf`.`accountid` = `vtiger_account`.`accountid`
                                                          LEFT JOIN crm_directions ON `crm_directions`.`directionid`=`vtiger_accountscf`.`accountid`
                                                          LEFT JOIN crm_address_post_code adr ON `adr`.`addressid`=`vtiger_accountscf`.`accountid`
                                                          WHERE `vtiger_accountscf`.`accountid` = ?");

  // NOTE gaunam visus klientui priskirtus kainorascius
  $pricebooks = $dbh->prepare("SELECT pricebook FROM vtiger_account WHERE accountid = ?");
  $pricebooks->execute([$accountid]);
  $pricebooks = $pricebooks->fetch();

  // NOTE Patikrinam ar klientas turi priskirta kainininka, jei ne priskiria bazini

  if(empty($pricebooks['pricebook'])){
    $pricebooks['pricebook'] = 10965;
  } 
 

    $pricebookArr = explode(',', $pricebooks["pricebook"]);
    // Jei klientas turi daugiau nei viena priskirta kainyna, suka cikla is kainynu
    if(count($pricebookArr) > 1){
      $pricesArr = array();   
      $pricesArr2 = array();   

      // Suka cikla paduodant skirtingus kainorasciu id i kainos skaiciavimo funkcija
      foreach($pricebookArr AS $pricebookid){
        $locations->execute([$pricebookid]);
        $loc = $locations->fetch();
        $direct = directLocationAndDirectAdr($loc);
        $pricesArr[] = initCalculatePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$distance,$country_from, $country_to,$which,$loc,$direct);
      }

      for($i =0; $i < count($pricesArr); $i++){
          $pricesArr2[$i] = $pricesArr[$i]['price']; 
      }  
      // Gauna didžiausia rasta kaina
      $maxVal = max($pricesArr2);
      $maxKey = array_search($maxVal, $pricesArr2);

      // Atiduoda kainyno informacija su kaina
      return $pricesArr[$maxKey];

    }else{     
      $locations->execute([$accountid]);
      $loc = $locations->fetch();
      $direct = directLocationAndDirectAdr($loc);
      return initCalculatePrice($dbh,$from,$to,$pricebooks["pricebook"],$weight,$volume,$square,$pll,$distance,$country_from, $country_to,$which,$loc,$direct);
    }
  
}

function initCalculatePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$distance,$country_from, $country_to,$which,$loc,$direct){
      // Issisaugo paletes
      $realPll = $pll;
      $pll = false;
      $pll2 = false;

      $countPriceArr = array();  
      $pricesArr = array();  

      //NOTE Vykdoma kainos paieska kliento kainyne
      if($which == 'CLIENT'){        
        // Patikrina ar svoris nera lygus 0 ir skaiciuoja kaina pagal svori
        if($weight > 0){
          $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, false, false, false,$distance, $country_from, $country_to,$loc,$direct);
        }
    
        // Patikrina ar kubai nera lygus 0 ir skaiciuoja kaina pagal kubus
        if($volume > 0){
          $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, true, false, false,$distance, $country_from, $country_to,$loc,$direct);  
        }

        // Patikrina ar kvadratai nera lygus 0 ir skaiciuoja kaina pagal kvadratus
        if($square > 0){
          $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2 , false, true, false,$distance, $country_from, $country_to,$loc,$direct);
        }
        
        // $pll kintamajam gražinama jo tikroji vertė, patikrinama ar paletes nera 0 ir skaiciuoja paleciu kaina
        $pll = $realPll; 
        if($pll > 0){     
          $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, false, false, false,$distance, $country_from, $country_to,$loc,$direct);

          $pll2 = true;
          $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, false, false, false,$distance, $country_from, $country_to,$loc,$direct);
        }

        for($i =0; $i < count($countPriceArr); $i++){     
          $pricesArr[$i] = $countPriceArr[$i]['price'];               
        }    
      
        // Isrenka didziausia kaina
        $maxVal = max($pricesArr);
        $maxKey = array_search($maxVal, $pricesArr);

      }else if($which == 'BAZINIS'){
       
        $countPriceArr = array();  
        $pricesArr = array();  
 
        $pll = false;
        $pll2 = false;
          // Bazinis 
        if($weight > 0){     			  
        	$countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, false, false, true,$distance, $country_from, $country_to,$loc,$direct);
        }
   		if($volume > 0){
        	$countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume, $square,$pll,$pll2,  true, false, true,$distance, $country_from, $country_to,$loc,$direct);  
        }   

        if($square > 0){
       	 	$countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, false, true,true,$distance, $country_from, $country_to,$loc,$direct);  
    	}
        
        $pll = $realPll;
        if($pll > 0){ 
	        $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, false, false, true,$distance, $country_from, $country_to,$loc,$direct);  
	        $pll2 = true;
	        $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2, false, false, true,$distance, $country_from, $country_to,$loc,$direct);  
	    }

        for($i =0; $i < count($countPriceArr); $i++){     
          $pricesArr[$i] = $countPriceArr[$i]['price'];               
        }    
        
      
        $maxVal = max($pricesArr);
        $maxKey = array_search($maxVal, $pricesArr);
      }
    
  return $countPriceArr[$maxKey];
}


function takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$pll2,$byVolume, $bySquare, $bazinis,$distance, $country_from, $country_to,$loc,$direct){
  

  if(!$bazinis){
    // NOTE Gaunamos kainyno zonos 
    if($pll > 0 && !$pll2){
      $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                        LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                        LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                        LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                        LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                        WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  AND 
                                        $pll = `vtiger_products`.`pll_pcs` ";
    }elseif($pll2){                                    
      $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                        LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                        LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                        LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                        LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                        WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  AND 
                                        $pll = `vtiger_products`.`pll_pcs` AND `vtiger_products`.`min_weight_kg` <=  $weight AND $weight <= `vtiger_products`.`max_weight_kg`";
    }elseif($byVolume){
      $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                        LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                        LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                        LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                        LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                        WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  
                                        AND (`vtiger_products`.`min_volume_m3` <= $volume AND $volume <= `vtiger_products`.`max_volume_m3`) ";
  
   }elseif($bySquare){
      $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                        LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                        LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                        LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                        LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                        WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  
                                        AND (`vtiger_products`.`min_square_m2` <= $square AND $square <= `vtiger_products`.`max_square_m2`) "; 
  }else{
      $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                    LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                    LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                    LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                    LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                    WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = $pricebookid  
                                    AND (`vtiger_products`.`min_weight_kg` <=  $weight AND  $weight <= `vtiger_products`.`max_weight_kg`) "; 
                                    
  }

  
  }else{

    if($pll > 0){
      $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                        LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                        LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                        LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                        LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                        WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = 10965  AND 
                                        $pll = `vtiger_products`.`pll_pcs` ";

                                
    }else
    if($byVolume){        
        $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = 10965 
                                AND (`vtiger_products`.`min_volume_m3` <= $volume AND $volume <= `vtiger_products`.`max_volume_m3`) ";
    }elseif($bySquare){  
        $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                      LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                      LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                      LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                      LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                      WHERE `vtiger_crmentity`.`deleted` = 0  AND `vtiger_pricebook`.`pricebookid` = 10965  
                                      AND (`vtiger_products`.`min_square_m2` <= $square AND $square <= `vtiger_products`.`max_square_m2`)"; 
  
    }else{  
        $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = 10965 
                                          AND (`vtiger_products`.`min_weight_kg` <=  $weight AND  $weight <= `vtiger_products`.`max_weight_kg`) ";  
    }
  }

  $met = 'pdo';
  

  if(!$bazinis){    
    // NOTE Visi duomenys siunciami sudaryti kombinacijas
    $get_price = countriesZones($dbh, $direct['direct_location'],$direct['direct_location_adr'], $met, $country_from, $country_to, $from, $to,$loc,$distance, $get_price_query_string, $from,$to);

    $pricebook_type = (($pricebookid == 10965 || $bazinis) ? 'BAZINISLT' : 'CLIENT');
    

    foreach($get_price as $get_prices){        
      $get =  $get_prices;           
    
      $result = array( 
        "price" => (float)$get_prices["listprice"],   
        'pricebook' => $get_prices['productname'],
        'which' => $pricebook_type,
        "min_weight" => $get_prices['min_weight_kg'],
        "max_weight" => $get_prices['max_weight_kg'],
        "min_volume" => $get_prices['min_volume_m3'],
        "max_volume" => $get_prices['max_volume_m3'],
        "min_square" => $get_prices['min_square_m2'],
        "max_square" => $get_prices['max_square_m2'],
        "pll" => $get_prices['pll_pcs'],
        "stevedoring" => (float)$get_prices['cf_954'],
        "combination" =>  $get_prices["productname"],
        "consignee" =>  $get_prices["consignee"]
      );                              
      break;  
    } 
  
  }else{
    


      require_once $_SERVER['DOCUMENT_ROOT']."/price-algorithm/base_catalog.php";
    
      $location = ":".trim($from["zone_customer"])." ".trim($to["zone_customer"]).":"; 
      $get_price = basecatalog($location,$get_price_query_string_bazinis, $dbh,$met);
    
      $location2 = ":".trim($from["zone_base"])." ".trim($to["zone_base"]).":"; 
      $get_price_base = basecatalog($location2,$get_price_query_string_bazinis, $dbh,$met);
    
      $location3 = ":".trim($from["zone_base"])." ".trim($to["zone_customer"]).":"; 
      $get_price_base2 = basecatalog($location3,$get_price_query_string_bazinis, $dbh,$met);  
    
      $location4 = ":".trim($from["zone_customer"])." ".trim($to["zone_base"]).":"; 
      $get_price_base3 = basecatalog($location4,$get_price_query_string_bazinis, $dbh,$met);  
    
      $get_base = array_merge($get_price, $get_price_base, $get_price_base2,$get_price_base3);

        foreach($get_base as $get_prices){      
            $result = array( 
              "price" => (float)$get_prices["listprice"],       
              'pricebook' => "BAZINISLT ". $get_prices["productname"],
              'combination' => "BAZINISLT ". $get_prices["productname"],
              'which' => "BAZINIS",
              "min_weight" => $get_prices['min_weight_kg'],
              "max_weight" => $get_prices['max_weight_kg'],
              "min_volume" => $get_prices['min_volume_m3'],
              "max_volume" => $get_prices['max_volume_m3'],
              "min_square" => $get_prices['min_square_m2'],
              "max_square" => $get_prices['max_square_m2'],
              "pll" => $get_prices['pll_pcs'],
              "stevedoring" => (float)$get_prices['cf_954'],
              "consignee" =>  $get_prices["consignee"]
            );                     
            break;
        }  
    
  }


  if(!empty($get_prices["listprice"])){
    return $result;
  }else{
    return  array('price' => 0,  'combination' => "Nėra kombinacijos ".$from['zone_base']." - ".$to['zone_base']." arba ".$from['zone_customer']." - ".$to['zone_customer']." arba ".$from['state']." - ".$to['state']."", 'max_volume' => 0);  
  } 

}

function directLocationAndDirectAdr($loc){
  $direct_location = Array();
  $direct_location_adr = Array();
  $columns = Array('1218','1222','1226','1230','1234','1238','1242','1246','1250','1254','1400','1404','1408','1412','1416','1420','1424','1428','1432','1436');

  $b = 0;

  // Pasidaromas tiesioginiu marsrutu pagal adresa array
  foreach($columns AS $col){
    if(isset($loc["post_cf_".$col])){
      $slashExplode = explode("/",$loc["post_cf_".$col]);        
      for($f =0; count($slashExplode) > $f; $f++){
       $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
      }      
      for($e =0; count($slashExplode) > $e; $e++){          
          $post_cf_[$col][$e] = multiexplode2(array(";","-"), $equalsExplode[$col."_".$e][1]);     
          if(!empty($equalsExplode[$col."_".$e][0])){       
           $direct_location[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]); 
          }       
      }
    }
    $b++;
  } 

// Pasidaromas tiesioginiu marsrutu pagal pasto koda array
  foreach($columns AS $col){
    if(isset($loc["code_cf_".$col])){
      $slashExplode = explode("/",$loc["code_cf_".$col]);        
      for($f =0; count($slashExplode) > $f; $f++){
       $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
      }      
      for($e =0; count($slashExplode) > $e; $e++){          
          $post_cf_[$col][$e] = multiexplode2(array(";","-"), $equalsExplode[$col."_".$e][1]);          
          if(!empty($equalsExplode[$col."_".$e][0])){         
            $direct_location_adr[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]);      
          }  
      }
    }
  } 

  return ['direct_location' => $direct_location, 'direct_location_adr' => $direct_location_adr];
}

function multiexplode2 ($delimiters,$data) {
  $MakeReady = str_replace($delimiters, $delimiters[0], $data);
  $Return    = explode($delimiters[0], $MakeReady);
  return  $Return;
}