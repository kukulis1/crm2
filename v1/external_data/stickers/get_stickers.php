<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
require '../../../config.inc.php';
global $config;
require '../utils.php';
error_reporting(E_ALL);

    try {
        require('../mysql_connection.php'); 
        $order_id = htmlspecialchars($_POST["record"]);
        $sth = $dbh->prepare('SELECT external_order_id, shipment_code FROM vtiger_salesorder
                             WHERE salesorderid = ?');

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($order_id));
        $soinfo = $sth->fetch();
        $external_order_id = $soinfo['external_order_id'];
        $shipment_code_full = $soinfo['shipment_code'];
		$shipment_code = substr($shipment_code_full, 0, strrpos($shipment_code_full, '-'));
        $label_type = $_POST["label_type"];
        $ch = curl_init();
        $filename = 'labels-'.$shipment_code. '-'. $label_type.'.pdf';
        $filepath = $root_directory."/label_files/". $filename;
        $urlpath = $site_URL . "/label_files/". $filename;
        $dirpath = $root_directory . "/label_files";
        if (!is_dir($dirpath))
        {
            mkdir($dirpath, 0755, true);
        }
        // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/export/crm/get_labels.php");    
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_labels.php");          
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'external_order_id' => $external_order_id, 'label_type' => $label_type));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        $data = json_decode($result, true);
        if(array_key_exists('errors', $data)){
            echo json_encode(['status' => 'Failed', 'error' => $data['errors'][0]]);
        } else{
            $pdf_converted = base64_decode($data['response']['result']['labels_pdf']);

            $fp = fopen ($filepath, 'w+');
            fwrite($fp, $pdf_converted);
            fclose($fp);

            echo json_encode(['status' => 'OK', 'URL' => $urlpath, 'file_name' => $filename, 'file_path' => $filepath]);
        }

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
