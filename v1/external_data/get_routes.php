<?php

function getRoutes($params) {

    global $config;
    require 'appc_utils.php';

    $logfile = 'get_routes.log';
    webservicelog($params, $logfile);

    $result_data = array('routes' => array());

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {
            
            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {

            $sql_params = array();
             
            $type = $params['type'];
            $user_id = $params['user_id'];
            $route_default_user = 26;
            $timestamp = $params['timestamp'];
            
            if($type == 1) {
                $sql = "SELECT s.*, s.status AS task_status, e.*
                                        FROM vtiger_troubletickets s
                                        JOIN vtiger_crmentity e ON e.crmid = s.ticketid 									
                                        WHERE e.deleted = 0 AND s.category = 'Route' AND e.smownerid = ? AND s.is_own = 1";           

                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                }                
                
                $sql.= " AND s.status != 'finished'"; 
                $sql.= " AND s.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";               
                
                array_push($sql_params, $user_id);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);
            } elseif ($type == 0) {
                $sql = "SELECT s.*, s.status AS task_status, e.*
                                        FROM vtiger_troubletickets s
                                        JOIN vtiger_crmentity e ON e.crmid = s.ticketid 								
                                        WHERE e.deleted = 0 AND s.category = 'Route' AND (e.smownerid = ? OR e.smownerid = ?) AND s.is_own = 0";           

                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                }   
                
                $sql.= " AND s.status != 'finished'"; 
                $sql.= " AND s.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";               
                
                array_push($sql_params, $user_id, $route_default_user);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);  
            }
            
            while ($row = $sth->fetch()) {

                $task_id = intval($row['ticketid']);
                
                $tasks[$task_id]['crm_route_id'] = intval($row['ticketid']);           
		$tasks[$task_id]['route_id'] = intval($row['id']);		
		$tasks[$task_id]['route_code'] = $row['title'];
		$tasks[$task_id]['status'] = $row['task_status'];             
                $tasks[$task_id]['start_date'] = $row['start_date'] . ' ' . $row['start_time'];                                 
                $tasks[$task_id]['end_date'] = $row['end_date'] . ' ' . $row['end_time'];                
                $tasks[$task_id]['weight'] = $row['weight'];               
                $tasks[$task_id]['pallets'] = $row['pallets'];  
                
                if($row['driver1_id']!= NULL && !empty($row['driver1_id']))                   
                $tasks[$task_id]['driver'] = $row['driver1_id']; 
                
                if($type == 0) {
                    $tasks[$task_id]['price'] = $row['price'];     
                }
                
                $tasks[$task_id]['user_id'] = intval($row['smownerid']);  
               
                $tasks_indexed = array_values($tasks);
                $result_data['routes'] = $tasks_indexed;
            }    
                        
            $output = array(
                'status' => 200,
                'response' => $result_data
            );
            
                $logfile = 'get_routes.log';
                webservicelog($output, $logfile);
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );
        
        $logfile = 'get_routes.log';
        webservicelog($output, $logfile);
    }
    return $output;
}
