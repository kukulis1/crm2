<?php

include '../ws.config.php';
include $_SERVER['DOCUMENT_ROOT'].'/config.inc.php';

// ini_set('display_errors', 1);
// error_reporting(E_ALL);

global $config;
global $mailHost,$mailHostQuality, $mailUsernameQuality,$mailPasswordQuality; 

// require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
// require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
// require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';

require_once $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/Email/vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


use PhpImap\Exceptions\ConnectionException;
use PhpImap\Mailbox;
require '../utils.php';

// $date = date("Y-m-d");
// if(!file_exists("../logs/claim_mail_cron/$date")){
//   mkdir("../logs/claim_mail_cron/$date", 0777, true); 
// }   
  
// $logfile = "claim_mail_cron/$date/runs.log";

// customers_log('Cron runs', $logfile);

if(date("H") != '00'){

  try {

      require('../mysql_connection.php');

      $dbh->beginTransaction();                 
                  
    
      $date = date("Y-m-d H:i:s");
      $setype = 'Claims';
      $claims_fetch_process = $dbh->prepare("INSERT INTO app_fetch_claims (date) VALUES (?)");
      $sql = $dbh->prepare("INSERT INTO vtiger_claims (claimsid,claims_tks_source,claims_tks_client) VALUES(?,?,?)");
      $sql1 = $dbh->prepare("INSERT INTO vtiger_claimscf (claimsid,cf_1837,cf_1839) VALUES(?,?,?)");
      $sql2 = $dbh->prepare("INSERT INTO vtiger_claim_emails (claimid,mailid,hash,type,subject,email,body) VALUES(?,?,?,?,?,?,?)");

      $sql3 = $dbh->prepare("SELECT c.claimsid FROM vtiger_claims c                     
                  LEFT JOIN vtiger_claim_emails em ON em.claimid=c.claimsid
                  LEFT JOIN vtiger_crmentity e ON e.crmid=c.claimsid
                  WHERE e.deleted = 0 AND (em.mailid = ?  OR em.email = ?) AND em.type = 1
                  ORDER BY claimsid DESC LIMIT 1");

      $sql4 = $dbh->prepare("SELECT claimid FROM vtiger_claim_emails WHERE type = 1 AND mailid = ? LIMIT 1");

      $sql9 = $dbh->prepare("SELECT claimid,hash FROM vtiger_claim_emails WHERE type = 1 AND hash = ? LIMIT 1");

      $sql5 = $dbh->prepare("SELECT claimid FROM vtiger_claim_emails WHERE type = 1 AND email = ? ORDER BY claimid DESC LIMIT 1");

      $sql6 = $dbh->prepare("SELECT cf_1837 FROM vtiger_claimscf WHERE claimsid = ?");       
      
      $sql7 = $dbh->prepare("UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?");

      $sql8 = $dbh->prepare("UPDATE vtiger_claims_seen SET seen = ?, seen_time = ?, reviewed_person = ? WHERE claim_id = ?");


      $claims_fetch_process->execute(array($date));

      $mailbox = new Mailbox($mailHostQuality,$mailUsernameQuality,$mailPasswordQuality);  
      $last_check = date("Ymd", strtotime("-3 days"));


      try {
      $mail_ids = $mailbox->searchMailbox('SINCE "'.$last_check.'"');

      foreach ($mail_ids AS $mail_id) {  

          $email = $mailbox->getMail(
              $mail_id, 
              true 
          );    
      
          $pattern = "/[[][a-z]{3}[:][_][A-Z]{5}[_]\d{10}[.]\d{1,9}[:][a-z]{3}[]]/";   

          $matches = array(); 
          preg_match($pattern, $email->subject, $matches);   
          if(!empty( $matches[0])){       
            $subject_hash = $matches[0]; 
          }else{
            $subject_hash = '';
          }

          // $sql3->execute(array($mail_id,$email->fromAddress));
          $sql4->execute(array($mail_id));       
          $sql9->execute(array($subject_hash));       

          // $sql5->execute(array($email->fromAddress));
          // $get_id = $sql5->fetch();

          $get_id2 = $sql9->fetch();

        

          // $num_rows =  $sql3->rowCount();
          $num_rows2 = $sql4->rowCount(); 
          $num_rows3 = $sql9->rowCount(); 
            

          // if(strpos($email->textPlain, "From")){
          //  $body = substr($email->textPlain, 0, strpos($email->textPlain, "From"));
          // }else{
          $body = shorter($email->textPlain,5000);
          // }      
          
          $emailSubDomain = substr($email->fromAddress, strpos($email->fromAddress, "@") + 1);


          if(!$num_rows2){
            if(!$num_rows3){       
              $hash = generateHash($mail_id);
              $entity_id = get_last_entity_id_and_update($dbh,1);  
              $sender_email = '';
              if($emailSubDomain != 'parnasas.lt'){
                $sender_email = $email->fromAddress;
              } 
                if(!empty($email->fromAddress)){
                  insert_entity_spec($dbh, $setype, $entity_id, 26, NULL, $email->fromName,'Email', $date);
                  $sql->execute(array($entity_id ,'Klientas',$email->fromName));
                  $sql1->execute(array($entity_id ,'Naujas',$sender_email));
                  $sql2->execute(array($entity_id,$mail_id,$hash,1 ,$email->subject,$sender_email,$body));
                  $letter_id = $dbh->lastInsertId();
                  setClaimNumber($dbh,$entity_id);
                  addAttachment($dbh,$email,$entity_id,$letter_id);
                  if($emailSubDomain != 'parnasas.lt'){
                      autoAnswerToClaim($email->fromAddress,$email->subject, $mailHost, $mailUsernameQuality, $mailPasswordQuality,$hash);
                  }
                }
            }else{      
              $claim_id = $get_id2['claimid'];     
              $hash = $get_id2['hash']; 
              $sql2->execute(array($claim_id,$mail_id,$hash,1 ,$email->subject,$email->fromAddress,$body));
              $letter_id = $dbh->lastInsertId();
              addAttachment($dbh,$email,$claim_id,$letter_id);
              setClaimNumber($dbh,$entity_id);
              update_entity_spec($dbh,$claim_id,$date);
              $sql8->execute(array(0,'','',$claim_id)); 
            }
          }  
      
          
      }    
  
      } catch (ConnectionException $ex) {
          die('IMAP connection failed: '.$ex->getMessage());
      } catch (Exception $ex) {
          die('An error occured: '.$ex->getMessage());
      } 


      $mailbox->disconnect();  
      
      checkNotifications($dbh);
      $dbh->commit();
      
  } catch (PDOException $e) {
    $dbh->rollBack();   
    
    // $date = date("Y-m-d");
    // if(!file_exists("../logs/clail_mail_errors/$date")){
    //   mkdir("../logs/clail_mail_errors/$date", 0777, true); 
    // }   
        
    // $logfile = "clail_mail_errors/$date/error.log";
    
    // customers_log($e->getMessage(), $logfile);
    sendReportMail($e->getMessage(),basename(__FILE__));


    //  echo "Error!";
    //  echo $e->getMessage();
  }

}


function generateHash($mail_id){
  $date = time();

  $string = "[ref:_CLAIM_";
  $string .= $date.".".$mail_id;
  $string .= ":ref]";

  return $string;
}

function shorter($text, $chars_limit)
{
    // Check if length is larger than the character limit
    if (strlen($text) > $chars_limit)
    {
        // If so, cut the string at the character limit
        $new_text = substr($text, 0, $chars_limit);
        // Trim off white space
        $new_text = trim($new_text);
        // Add at end of text ...
        return $new_text . "...";
    }
    // If not just return the text as is
    else
    {
    return $text;
    }
}


function autoAnswerToClaim($client_email,$subject, $mailHost, $mailUsername, $mailPassword, $hash)
{

  $answer = 'Jūsų pretenzija gauta, susisieksime 5d.d. laikotarpyje.';

  $mail = new PHPMailer(true);

  try {
      //Server settings
      $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      
      $mail->isSMTP();                                            
      $mail->Host       = $mailHost;                    
      $mail->SMTPAuth   = true;                                   
      $mail->Username   = $mailUsername;                    
      $mail->Password   = $mailPassword;                               
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;        
      $mail->Port       = 587; 
      $mail->SMTPOptions = array('ssl' => array(
                                 'verify_peer' => false,
                                 'verify_peer_name' => false,
                                 'allow_self_signed' => true
                                  )
                            );
      $mail->setLanguage('lt', $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
      $mail->CharSet = 'UTF-8';
      $mail->SMTPDebug = false;  

      //Recipients
      $mail->setFrom($mailUsername, 'Kokybe');
      $mail->addAddress($client_email);         
     

      // Content
      $mail->isHTML(true); 
      $mail->Subject = "RE:$subject $hash";
      $mail->Body    = $answer;
      $mail->send();  

  } catch (Exception $e) {
      echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
  }
}


function setClaimNumber($dbh,$recordId){
  
  $date = date('Y-m-d');
  $prefix = 'CLAIM-';		

  $sth = $dbh->prepare("SELECT claimsno FROM vtiger_claims
                            LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_claims.claimsid 							 
                            WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$date'  AND vtiger_crmentity.deleted = 0  AND claimsno != ''
                            ORDER BY vtiger_claims.claimsid  
                            DESC LIMIT 1");
  $sth->execute(array()); 
  
  $get_claimsno = $sth->fetch();	
  $curid = $get_claimsno['claimsno'];      


  if(!empty($curid)){
    $curid = substr($curid, -3);
  }else{
    $req_no = '001';
  }	

  $todayDate = date('ymd');

  if(!empty($curid)){
    $strip = strlen($curid) - strlen($curid + 1);
    if ($strip < 0)
      $strip = 0;
    $temp = str_repeat("0", $strip);
    $req_no.= $temp . ($curid + 1);
  }

  $prev_inv_no = $prefix.$todayDate."-".$req_no;

  $sql = $dbh->prepare("UPDATE vtiger_claims SET claimsno = ? WHERE claimsid=?");
  $sql->execute(array($prev_inv_no, $recordId));

  $sql2 = $dbh->prepare("UPDATE vtiger_crmentity SET label = ? WHERE crmid = ?");
  $sql2->execute(array($prev_inv_no, $recordId));

  $sql3 = $dbh->prepare("INSERT INTO  vtiger_claims_seen (claim_id, seen, seen_time, reviewed_person) VALUES (?,?,?,?)");
  $sql3->execute(array($recordId,0,'',''));

}




 function addAttachment($db,$email,$id,$letter_id){
        if ($email->hasAttachments()) {                 
        $attachments = $email->getAttachments();
        $year = date("Y");
        $month = date("F");
        $dayofweek = "week".weekOfMonth(strtotime(date("Y-m-d"))); 

        foreach ($attachments AS $attachment) {        
            
            if (!file_exists("../../../storage/$year/$month/$dayofweek")) {
            mkdir("../../../storage/$year/$month/$dayofweek", 0777, true);
            } 
            $ext = pathinfo($attachment->name, PATHINFO_EXTENSION); 
            $oldName = lt_chars(str_replace(' ', '',pathinfo($attachment->name, PATHINFO_FILENAME)));                             
                if(empty($ext)) $ext = $oldName;
                $newFileName = $oldName.'_'.time().'.'.$ext;          
                $filename = $oldName.'_'.time();          
                $attachment->setFilePath("../../../storage/$year/$month/$dayofweek/$newFileName");            
                $patch = "storage/$year/$month/$dayofweek/";        
                
                if ($attachment->saveToDisk()) {              
                insertDocument($db,$id,26,$patch,$filename, $newFileName,$letter_id);                            
                } else {
                echo "ERROR, could not save!<br>";
                }                      
        }  
                
        }
}

 function insertDocument($db,$recordid,$userid,$patch,$filename,$newFileName,$letter_id)
  {    
    $date_now = date('Y-m-d H:i:s');
    $entity_id = get_last_entity_id_and_update($db,1);  
    insert_entity_spec($db,'Documents', $entity_id, $userid, NULL, '', 'CRM', $date_now);
    insert_senotesrel($db, $entity_id, $recordid);
    insertNotes2($db,$entity_id,$filename,$newFileName,1,'I',1);    
    insert_NoteCf($db, $entity_id, date('Y-m-d H:i:s'));
    
    $entity_id2 = get_last_entity_id_and_update($db,1);  
    $sth = $db->prepare("INSERT INTO vtiger_claims_letters_attachments (letter_id,attachment_id) VALUES (?,?)");
    $sth->execute(array($letter_id,$entity_id2));
    if(renameFile($patch,$newFileName,$entity_id2)){
      insert_entity_spec($db,'Documents Attachment', $entity_id2, $userid, NULL, '', 'CRM', $date_now);
      insertAttachments($db, $entity_id2,$newFileName,$patch,$recordid);
      insertSeAttachmentsrel($db, $entity_id,$entity_id2);	
    }
  
  }


  function insertNotes2($db, $entity_id,$title,$filename,$folderid,$locationType,$filestatus)
  {
    $sth = $db->prepare("INSERT INTO vtiger_notes (notesid, title, filename, folderid, filelocationtype, filestatus) 
                            VALUES (?, ?, ?, ?, ?, ?)");
    $sth->execute(array($entity_id, $title, $filename, $folderid,$locationType,$filestatus));                        

  }

  function update_entity_spec($dbh, $entity_id, $date)
  {
    $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
    $sth->execute(array($date,$entity_id));
  }


 function insert_entity_spec($dbh, $setype, $entity_id, $user_id, $description, $label,$source, $date)
  {
    $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, source, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $sth->execute(array($entity_id, $user_id, $user_id, $setype, $description, $label,$source,$date, $date));
  }



 function insertAttachments($dbh, $entity_id,$title,$filename,$recordid)
  {
    $sth = $dbh->prepare("INSERT INTO vtiger_attachments (attachmentsid, name, path,related_id)  VALUES (?,?,?,?)");
    $sth->execute(array($entity_id, $title, $filename,$recordid));
  }

 function insertSeAttachmentsrel($dbh, $entity_id,$moduleId)
  {
    $sth = $dbh->prepare("INSERT INTO vtiger_seattachmentsrel (crmid, attachmentsid)  VALUES (?,?)");
    $sth->execute(array($entity_id, $moduleId));
  }




 function lt_chars($text) {   
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

    return $text; 
  }

 function weekOfMonth($date){  
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
 }

  function renameFile($location,$filename,$entity_id){ 
    global $root_directory;
    $oldName = $root_directory.'/'.$location.$filename;
    $newName = $root_directory.'/'.$location.$entity_id.'_'.$filename;
    if(rename("$oldName","$newName")){
      return true;
    }  
  }
  
function checkNotifications($dbh){
  $get_claim_changes = $dbh->prepare("SELECT crmid,modifiedby, e.modifiedtime, cf_1924 AS review
                        FROM vtiger_crmentity e
                        LEFT JOIN vtiger_notification_claims c ON claimid=crmid
                        LEFT JOIN vtiger_claimscf cf ON cf.claimsid=crmid
                        WHERE setype = 'Claims' AND (createdtime!=e.modifiedtime OR cf_1924 = '') AND IF(claimid IS NOT NULL, c.modifiedtime !=e.modifiedtime,1=1) AND deleted = 0
                        ORDER BY e.modifiedtime");

  $get_claim_changes->execute(array());
  $changed_record_count = $get_claim_changes->rowCount();

  if($changed_record_count > 0){
    $check_notification_exist = $dbh->prepare("SELECT id FROM vtiger_notification_claims WHERE claimid = ?");

    $update_notification_time = $dbh->prepare("UPDATE vtiger_notification_claims SET modifiedtime = ? WHERE claimid = ?");
    $insert_notification_time = $dbh->prepare("INSERT INTO vtiger_notification_claims (claimid,modifiedtime) VALUES (?,?)");

    foreach($get_claim_changes AS $changes){
      $check_notification_exist->execute(array($changes['crmid']));
      $record_count = $check_notification_exist->rowCount();

      if($record_count){
        $update_notification_time->execute(array($changes['modifiedtime'],$changes['crmid']));
      }else{
        $insert_notification_time->execute(array($changes['crmid'],$changes['modifiedtime']));
      }

      $text = (empty($changes['review']) ? 'Nauja neperžiūrėta pretenzija' : 'yra pasikeitimų');
      
      createNotification($dbh, $changes['crmid'], $text, $changes['modifiedby'], 'pretenzijos');
    }
  }
}
