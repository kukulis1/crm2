<?php

function getOrders($params) {

    global $config;
    require 'appc_utils.php';

    $logfile = 'get_orders.log';
    webservicelog($params, $logfile);

    $result_data = array('orders' => array());

    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {
            
            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {

            $sql_params = array();
            
            $type = $params['type'];
            $user_id = $params['user_id'];
            $route_default_user = 26;
            $timestamp = $params['timestamp'];
            
            if($type == 1) {            
                $sql = "SELECT s.*, e.*, l.*, h.*, a.accountname, ac.activityid AS crm_site_id, ac.id AS site_id
                                        FROM vtiger_salesorder s
                                        JOIN vtiger_account a ON a.accountid = s.accountid 
                                        JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
                                        JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid   
                                        JOIN vtiger_crmentity e ON e.crmid = s.salesorderid 	
                                        JOIN vtiger_crmentityrel cr ON cr.relcrmid = s.salesorderid 
                                        JOIN vtiger_seactivityrel sc ON sc.activityid = cr.crmid  
                                        JOIN vtiger_activity ac ON ac.activityid = cr.crmid   
                                        JOIN vtiger_troubletickets t ON t.ticketid = sc.crmid  
                                        JOIN vtiger_crmentity cy ON cy.crmid = t.ticketid                                       
                                        WHERE e.deleted = 0 AND s.external_order_id IS NOT NULL AND cy.deleted = 0 AND t.category = 'Route' AND cy.smownerid = ? AND t.is_own = 1";                 

                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                }     
                
                $sql.= " AND s.sostatus NOT IN('Delivered', 'Not Delivered')";
                $sql.= " AND (s.status != 'ERROR' OR s.status IS NULL)";          
                $sql.= " AND ac.eventstatus != 'Completed'";
                $sql.= " AND t.status != 'finished'"; 
                $sql.= " AND t.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";   
                
                array_push($sql_params, $user_id);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);
            
            } elseif ($type == 0) {
                $sql = "SELECT s.*, e.*, l.*, h.*, a.accountname, ac.activityid AS crm_site_id, ac.id AS site_id
                                        FROM vtiger_salesorder s
                                        JOIN vtiger_account a ON a.accountid = s.accountid 
                                        JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
                                        JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid   
                                        JOIN vtiger_crmentity e ON e.crmid = s.salesorderid 
                                        JOIN vtiger_crmentityrel cr ON cr.relcrmid = s.salesorderid 
                                        JOIN vtiger_seactivityrel sc ON sc.activityid = cr.crmid  
                                        JOIN vtiger_activity ac ON ac.activityid = cr.crmid   
                                        JOIN vtiger_troubletickets t ON t.ticketid = sc.crmid  
                                        JOIN vtiger_crmentity cy ON cy.crmid = t.ticketid  
                                        WHERE e.deleted = 0 AND s.external_order_id IS NOT NULL AND cy.deleted = 0 AND t.category = 'Route' AND (cy.smownerid = ? OR cy.smownerid = ?) AND t.is_own = 0";             

                if(!empty($timestamp)) {
                $sql.= " AND e.modifiedtime >= '$timestamp'";
                } 
                
                $sql.= " AND s.sostatus NOT IN('Delivered', 'Not Delivered')";    
                $sql.= " AND (s.status != 'ERROR' OR s.status IS NULL)";  
                $sql.= " AND ac.eventstatus != 'Completed'";                
                $sql.= " AND t.status != 'finished'"; 
                $sql.= " AND t.start_date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";   
                
                array_push($sql_params, $user_id, $route_default_user);
                $sth = $dbh->prepare($sql);
                $sth->execute($sql_params);     
            }  
            
            while ($row = $sth->fetch()) {

                $task_id = intval($row['salesorderid']);
                
                $tasks[$task_id]['crm_order_id'] = intval($row['salesorderid']); 
                
                if($row['shipment_code']!= NULL && !empty($row['shipment_code']))                
		$tasks[$task_id]['order_id'] = $row['shipment_code'];
                
		$tasks[$task_id]['order_date'] = date("Y-m-d H:i:s", strtotime($row['createdtime'])); 
		$tasks[$task_id]['customer_id'] = intval($row['accountid']);    
		$tasks[$task_id]['customer_name'] = $row['accountname'];                   
                $tasks[$task_id]['status'] = $row['sostatus'];  
                $tasks[$task_id]['price'] = sprintf('%0.2f', $row['total']);                 
                $tasks[$task_id]['crm_site_id'] = intval($row['crm_site_id']);
                $tasks[$task_id]['site_id'] = intval($row['site_id']);   
                
                if($row['load_company']!= NULL && !empty($row['load_company']))                 
                $tasks[$task_id]['load_company_name'] = $row['load_company'];
                
                if($row['load_contact']!= NULL && !empty($row['load_contact']))  
                $tasks[$task_id]['load_company_contact'] = $row['load_contact']; 
                
                if($row['load_phone']!= NULL && !empty($row['load_phone']))  
                $tasks[$task_id]['load_company_phone'] = $row['load_phone'];                 
                
                $tasks[$task_id]['load_address'] = $row['bill_street'];
                $tasks[$task_id]['load_municipality'] = $row['bill_city'];
                
                if($row['bill_code']!= NULL && !empty($row['bill_code']))                  
                $tasks[$task_id]['load_post_code'] = $row['bill_code'];  
                
                $tasks[$task_id]['load_country_code'] = $row['bill_country'];
                $tasks[$task_id]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'] . ' ' . $row['load_time_from'])); 
                $tasks[$task_id]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to'] . ' ' . $row['load_time_to'])); 
                
                if($row['unload_company']!= NULL && !empty($row['unload_company']))                  
                $tasks[$task_id]['unload_company_name'] = $row['unload_company'];  
                
                if($row['unload_contact']!= NULL && !empty($row['unload_contact']))                  
                $tasks[$task_id]['unload_company_contact'] = $row['unload_contact'];
                
                if($row['unload_phone']!= NULL && !empty($row['unload_phone']))  
                $tasks[$task_id]['unload_company_phone'] = $row['unload_phone'];                  
                
                $tasks[$task_id]['unload_address'] = $row['ship_street'];
                $tasks[$task_id]['unload_municipality'] = $row['ship_city'];
                
                if($row['ship_code']!= NULL && !empty($row['ship_code']))  
                $tasks[$task_id]['unload_post_code'] = $row['ship_code']; 
                
                $tasks[$task_id]['unload_country_code'] = $row['ship_country'];
                $tasks[$task_id]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from'] . ' ' . $row['unload_time_from'])); 
                $tasks[$task_id]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to'] . ' ' . $row['unload_time_to'])); 
                
                if($row['description']!= NULL && !empty($row['description']))  
                $tasks[$task_id]['note'] = $row['description'];                                                                
                
                $tasks_indexed = array_values($tasks);
                $result_data['orders'] = $tasks_indexed;
            }    
                        
            $output = array(
                'status' => 200,
                'response' => $result_data
            );
            
                $logfile = 'get_orders.log';
                webservicelog($output, $logfile);
        }
    } catch (PDOException $e) {

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );
        
        $logfile = 'get_orders.log';
        webservicelog($output, $logfile);
    }
    return $output;
}
