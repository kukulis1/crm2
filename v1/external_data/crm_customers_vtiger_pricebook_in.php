<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_account SET      
                                                                pricebook = ?,
                                                                update_date = ?
                                                    WHERE accountid = ?');
                    
    
    $sth = $dbh->prepare('SELECT DISTINCT t.*, c.accountid, p.pricebookid
                            FROM crm_customers_in t
                            JOIN vtiger_account c ON c.customer_id = t.customer_id
                            JOIN vtiger_crmentity g ON g.crmid = c.accountid
                            JOIN crm_prices_in d ON d.customer = t.customer_code          
                            JOIN vtiger_pricebook p ON p.bookname = d.pricebook  
                            WHERE g.deleted = 0');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {

            $date = date("Y-m-d H:i:s");

                    $sth2->execute(array($row['pricebookid'],                        
                                        $date,
                                        $row['accountid']
                    ));         
     
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>