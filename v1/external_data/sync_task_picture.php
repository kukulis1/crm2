<?php

function syncTaskPicture($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
   
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';    
    $endpointUrl = $url;
    
    $logfile = 'sync_task_picture.log';
    webservicelog($params, $logfile);
         
    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {

            $sth = $dbh->prepare('SELECT accesskey, user_name FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            
            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);
            
            $related_record = $params['task_id'];

            if (empty($related_record)) {

                $output = array(
                    'status' => 404,
                    'response' => array(
                        'type' => 'TASK_NOT_FOUND',
                        'message' => 'Task with such ID not found'
                    )
                );
                $logfile = 'sync_task_picture.log';
                webservicelog($output, $logfile);
            } else {
                // Initial information   
                $account = $params['user_id'];               
                $file_name = $params['filename'];
                $encoded_document = $params['photo']; 
                
                $module = $params['entity']; 
                $value = $params['value'];
                $crm_order_id = $params['crm_order_id'];
                $signed = $params['signed'];      
                $related_id = $related_record;
                
                $file_path = 'storage/';
                $created_modified_date = date("Y-m-d H:i:s");
                
                $decoded_document = base64_decode($encoded_document);
               
                    $file_size = strlen($decoded_document);
                    $file_title = current(explode('.', $file_name));
                    $finfo = finfo_open();
                    $file_type = finfo_buffer($finfo, $decoded_document, FILEINFO_MIME_TYPE);                
                    
                    if($module == 'load') { 
                        $file_title = $value;
                        $related_record = $crm_order_id;
                        $crm_load_id = $params['task_id']; 
                        $related_id = $crm_order_id . '-' . $crm_load_id;
                    }                     
                    
                $dbh->beginTransaction();
                
                    $sth = $dbh->query('SELECT * FROM vtiger_crmentity_seq');

                    while ($row = $sth->fetch()) {
                        $document_folder_id = $row['id'] + 1;
                    }

                    $sth = $dbh->prepare('UPDATE vtiger_crmentity_seq SET id = ?');
                    $sth->execute(array($document_folder_id));

                    insert_entity($dbh, 'Documents', $document_folder_id, $account, NULL, $created_modified_date, $created_modified_date);


                    $sth = $dbh->query('SELECT * FROM vtiger_crmentity_seq');

                    while ($row = $sth->fetch()) {
                        $entityid = $row['id'] + 1;
                    }

                    $sth = $dbh->prepare('UPDATE vtiger_crmentity_seq SET id = ?');
                    $sth->execute(array($entityid));

                    insert_entity($dbh, 'Documents Attachment', $entityid, $account, NULL, $created_modified_date, $created_modified_date);
                    
                    // Insert into documents table
                    $sth = $dbh->prepare('INSERT INTO vtiger_notes (notesid, note_no, title, filename, notecontent, folderid, filetype, filelocationtype, filedownloadcount, filestatus, filesize, fileversion) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
                    $sth->execute(array($document_folder_id, '', $file_title, $file_name, '', 1, $file_type, 'I', NULL, 1, $file_size, ''));

                    // Insert into attachments table
                    $sth = $dbh->prepare('INSERT INTO vtiger_attachments (attachmentsid, name, description, type, path, subject, related_id) VALUES (?, ?, ?, ?, ?, ?, ?)');
                    $sth->execute(array($entityid, $file_name, NULL, $file_type, $file_path, NULL, $related_id));

                    // Insert into attachments related table
                    $sth = $dbh->prepare('INSERT INTO vtiger_seattachmentsrel (crmid, attachmentsid) VALUES (?, ?)');
                    $sth->execute(array($document_folder_id, $entityid));

                    // Insert into documents related table
                    $sth = $dbh->prepare('INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES (?, ?)');
                    $sth->execute(array($related_record, $document_folder_id));
            
                    if($module == 'load') { 
                        $sth = $dbh->prepare("UPDATE vtiger_inventoryproductrel SET attachment_id = ?, attachment = ? WHERE lineitem_id = ? AND id = ?");
                        $sth->execute(array($entityid, $file_path . $file_name, $crm_load_id, $crm_order_id));    
                    }
                    
                    $file_name = $entityid . '_' . $file_name;
                    $filepath = $config['paths']['crm_storage_path'] . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $file_name;                    
                    
                    file_put_contents($filepath, $decoded_document);
         
                $dbh->commit();

                $output = array(
                    'status' => 200,
                    'response' => array('crm_attachment_id' => $entityid)
            );  
               
            }
        }
    } catch (PDOException $e) {

        $dbh->rollBack();

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_task_picture.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
