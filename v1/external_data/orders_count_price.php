<?php
include 'ws.config.php';
global $config;
require 'utils.php';
include 'getPriceFromPriceBook.php';
set_time_limit(500);
// error_reporting(0);


try {

    require('mysql_connection.php');

    $dbh->beginTransaction();    

                   

// $sth = $dbh->prepare("SELECT t.*, 
//                         GROUP_CONCAT(DISTINCT l.ordered) AS ordered, 
//                         GROUP_CONCAT(DISTINCT l.revised) AS revised, 
//                         GROUP_CONCAT(DISTINCT l.quantity) AS quantity,
//                         GROUP_CONCAT(DISTINCT l.id) AS external_load_id, 
//                         ROUND(shipment_direct_km,2) AS shipment_km,
//                         ROUND(SUM(DISTINCT l.m3),2) AS m3, a.accountid, c.salesorderid, load_country_code,unload_country_code,l.pll, t.id
//                                 FROM app_orders_handle t
//                                 JOIN vtiger_account a ON a.customer_id = t.customer_id 
//                                 JOIN vtiger_crmentity e ON e.crmid = a.accountid     
//                                 JOIN app_loads_handle l ON l.order_id = t.id           
//                                 JOIN vtiger_salesorder c ON c.external_order_id = t.id       
//                                 WHERE e.deleted = 0 AND l.meters IS NULL
//                                 GROUP BY l.order_id
//                                 ORDER BY app_id");


        $sth = $dbh->prepare("SELECT GROUP_CONCAT(t.ordered) AS ordered, 
                                GROUP_CONCAT(t.revised) AS revised, 	
                                GROUP_CONCAT(t.revised_quantity) AS quantity,  
                                GROUP_CONCAT(t.quantity) AS ordered_quantity,
                                GROUP_CONCAT(t.id) AS external_load_id,                            
                                ROUND(SUM(t.m3),2) AS m3, 
                                CASE WHEN revised_measure = 'pll' THEN SUM(t.revised_quantity) ELSE 0 END AS revised_pll,                      
                                CASE WHEN measure = 'pll' THEN SUM(t.quantity) ELSE 0 END AS pll, c.salesorderid, cf.cf_928 AS shipment_km, bill_code as load_post_code, ship_code as unload_post_code, so.bill_country AS load_country_code, sh.ship_country AS unload_country_code,(SELECT price_agreed FROM app_orders_update lu WHERE lu.id=t.order_id LIMIT 1) AS price_agreed, c.shipment_code, a.accountid , external_order_id AS id
                                    FROM app_loads_handle t                                            
                                    JOIN vtiger_salesorder c ON c.external_order_id = t.order_id   
                                    LEFT JOIN vtiger_account a ON a.accountid = c.accountid   
                                    LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid
                                    LEFT JOIN vtiger_crmentity e ON e.crmid = c.salesorderid       
                                    LEFT JOIN vtiger_sobillads so ON so.sobilladdressid=c.salesorderid  
                                    LEFT JOIN vtiger_soshipads sh ON sh.soshipaddressid=c.salesorderid                                  
                                    WHERE e.deleted = 0 
                                        AND (cf_2663 = '' OR cf_2663 IS NULL)  
                                    GROUP BY t.order_id
                                    ORDER BY t.app_id ASC");



    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    // $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
    //                                                     total = total+?,
    //                                                     subtotal = subtotal+?,                                                         
    //                                                     pre_tax_total = pre_tax_total+?                                                     
    //                                         WHERE salesorderid = ?');



    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                            WHERE salesorderid = ?'); 
                                            
    $sth7 = $dbh->prepare("SELECT meters FROM app_loads_update WHERE id = ? AND meters = 1");
    $sth7->setFetchMode(PDO::FETCH_ASSOC);                                        


    $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = ?, cf_1365 = ?, cf_1367 = ?, cf_1374 = ?, cf_1376 = ?, cf_1572 = ? WHERE   salesorderid = ? '); 
    // $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = margin+?  WHERE id = ?'); 
    $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ? WHERE id = ? AND productid = 14244'); 
    $sth5 = $dbh->prepare('UPDATE vtiger_crmentity  SET modifiedtime = ? WHERE crmid = ? '); 

    $insert_log = $dbh->prepare('INSERT INTO vtiger_salesorder_dimensions_log (salesorderid, amount, pricebook, kg, m3, m2, pll, kg_revised, m3_revised, m2_revised, pll_revised, which, importdate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)');

    $insert_taxable = $dbh->prepare('INSERT INTO app_taxable_dimensions (salesorderid,type) VALUES (?,?)');

    $update_taxable = $dbh->prepare('UPDATE app_taxable_dimensions SET type = ? WHERE salesorderid = ?');

    $get_all_orders_in_taxable = $dbh->prepare('SELECT salesorderid FROM app_taxable_dimensions');

    $get_all_orders_in_taxable->setFetchMode(PDO::FETCH_ASSOC);
    $get_all_orders_in_taxable->execute(array());

    $check_record = [];

    foreach($get_all_orders_in_taxable AS $value) {   
        $check_record[$value['salesorderid']] = $value['salesorderid'];
    }


    while ($row = $sth->fetch()) {        
        $id = $row['accountid'];     
        $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
        $row['load_post_code'] = preg_replace("/[^0-9]/", "",$row['load_post_code']);
        $row['unload_post_code'] = preg_replace("/[^0-9]/", "",$row['unload_post_code']);
        $pll = $row['pll'];               
        $revised_pll = $row['revised_pll'];       
        $date2 = date("Y-m-d H:i:s");
        $logfile = "count_price_$date.log";
        $external_load_id = explode(",", $row['external_load_id']);   

        $quantity = explode(",", $row['quantity']);
        $ordered_quantity = explode(",", $row['ordered_quantity']);
        

        $weight_ordered = array();
        $dim_ordered = array();
        $dim_ordered2 = array();
        $square_ordered = array();
        $square_ordered_temp = array();
        $weight_revised = array();
        $dim_revised = array();
        $dim_revised2 = array();
        $square_revised = array();
        $square_revised_temp = array();


        $ordered = explode(",",$row['ordered']);  
        
        for($i = 0; $i < count($ordered); $i++){
            $exploded_arr = explode(' ',$ordered[$i]);
            $weight_ordered[] = $exploded_arr[0];         
            $dim_ordered[] = array_product(explode('x', $exploded_arr[1])) * $ordered_quantity[$i];     
            $dim_ordered2[] = explode('x', $exploded_arr[1]); 
            $square_ordered_temp[] = explode('x', $exploded_arr[1]); 
            unset($square_ordered_temp[$i][2]);           
            $square_ordered[] = array_product($square_ordered_temp[$i]) * $ordered_quantity[$i];
         }
 
         $revised = explode(",",$row['revised']);
         
         for($i = 0; $i < count($revised); $i++){
            $exploded_arr2 = explode(' ',$revised[$i]);
            $weight_revised[] = $exploded_arr2[0];
            // $dim_revised[] = array_product(explode('x', $exploded_arr2[1])) * $quantity[$i];  
            $dim_revised2[] = explode('x', $exploded_arr2[1]);
            $square_revised_temp[] = explode('x', $exploded_arr2[1]); 
            unset($square_revised_temp[$i][2]);
            $square_revised[] = array_product($square_revised_temp[$i]) * $quantity[$i];         
         }
 
  
         $ordered_cargo_wgt = $exploded_arr[0];
         $revised_cargo_wgt = $exploded_arr2[0];
 
         $ordered_dim = explode('x', $exploded_arr[1]);
         $revised_dim = explode('x', $exploded_arr2[1]);
 
         $ordered_w = array_sum($weight_ordered);
         $ordered_m3 = array_sum($dim_ordered);
         $ordered_m2 = array_sum($square_ordered);
 
         $revised_w = array_sum($weight_revised);
        //  $revised_m3 = array_sum($dim_revised);
         $revised_m3 = $row['m3'];
         $revised_m2 = array_sum($square_revised);


        if(!empty($row['load_post_code']) AND !empty($row['unload_post_code']) AND (!empty($ordered_w) OR !empty($revised_w)) AND ($row['load_country_code'] != "EST" AND $row['unload_country_code'] != "EST") ){

            if($ordered_w == $revised_w && $ordered_m3 == $revised_m3){
                $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'], 'CLIENT'); 
                
                if($getPrice_revised['price'] == 0){
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'],'BAZINIS');
                }  

                $getPrice['price'] = $getPrice_revised['price'];
                $getPrice['pricebook'] =  $getPrice_revised['combination'];
                $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                $getPrice['type'] = 'revised';
            }else{
                $getPrice_ordered = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'],'CLIENT');  
                $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'],'CLIENT'); 

                if($getPrice_ordered['price'] == 0 && $getPrice_revised['price'] == 0){
                    $getPrice_ordered = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'],'BAZINIS');
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'],'BAZINIS'); 
                } 


              
                if($getPrice_ordered['price'] > $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_ordered['price'];
                    $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                    $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];     
                    $getPrice['type'] = 'ordered';             

                }elseif($getPrice_ordered['price'] < $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_revised['price'];
                    $getPrice['pricebook'] =  $getPrice_revised['combination'];
                    $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                    $getPrice['type'] = 'revised';

                }elseif($getPrice_ordered['price'] == $getPrice_revised['price']){                   
                        $getPrice['price'] = $getPrice_revised['price'];
                        $getPrice['pricebook'] =  $getPrice_revised['combination'];
                        $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                        $getPrice['type'] = 'revised';
                }

            }     
            
        
    
        }else{ 
            $getPrice = array('price' => 0);

            if(empty($row['load_post_code']) AND empty( $row['unload_post_code']) AND !empty($ordered_w) AND !empty($revised_w)){
                $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
            }elseif(empty($row['load_post_code']) AND empty($row['unload_post_code'])){    
                $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
            }elseif(empty($row['load_post_code'])){
                $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
            }elseif(empty($row['unload_post_code'])){
                $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
            }elseif(empty($ordered_w) AND empty($revised_w)){
                    $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
            }elseif($row['load_country_code'] == "EST" OR $row['unload_country_code'] == "EST"){
                $getPrice = array('pricebook' => 'Estija','price' => 0);
             }
           
        }   
        
        
        $sth7->execute(array($row['id']));
        $check = $sth7->fetch();
        

        // if(!empty($check['meters'])){
        //     $price = $getPrice['price'];    
        //     $sth2->execute(array(
        //        $price,
        //        $price,                                                     
        //        $price,                                      
        //        $row['salesorderid']
        //    ));                       
        // }else
        
        if(empty($row['price_agreed'])){
            $price = $getPrice['price'];  
            $sth2->execute(array(
               $price,
               $price,                                                     
               $price,                                      
               $row['salesorderid']
            ));   
        }else{
            $price = $row['price_agreed'];
            $sth2->execute(array(
               $price,
               $price,                                                     
               $price,                                      
               $row['salesorderid']
            ));   
        }         

         $margin = $getPrice['price'];                     

                     $sth3->execute(array(                       
                        $getPrice['pricebook'],
                        $row['m3'],
                        $ordered_m3,
                        $getPrice['price'],
                        $row['price_agreed'],
                        $getPrice['stevedoring'],
                        $row['salesorderid']          
                    ));    
 
                    
                    $sth4->execute(array(                                                                                          
                                        $margin,                                                                                                  
                                        $row['salesorderid']
                    ));

                    $sth5->execute(array(                                                                                          
                                        $date2,                                                                        
                                        $row['salesorderid']
                    ));

                    $insert_log->execute(array($row['salesorderid'],
                    $margin,
                    $getPrice['pricebook'],
                    $ordered_w,
                    $ordered_m3,
                    $ordered_m2,
                    $pll,
                    $revised_w,
                    $revised_m3,
                    $revised_m2,
                    $revised_pll,
                    'UPDATE 6',
                    $date2
                    ));



                    if(empty($check_record[$row['salesorderid']])){
                        $insert->execute(array($row['salesorderid'],$getPrice['type']));                         
                    }else{
                        $update_taxable->execute(array($getPrice['type'],$row['salesorderid']));  
                    }
            

                   
                   
                 
                
    }
         
    echo json_encode("Success");
    $dbh->commit();
    
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
