<?php 


function sendPriceToMetrikaCron($orderid, $price,$price_agreed,$finish_price,$dbh){
	$order = array();
	$order['order_id'] = $orderid;
	$order['price'] = $price;
	$order['price_agreed'] = $price_agreed;
	$order['finish_price'] = $finish_price;
	$orders[] = $order;
	$orders_indexed = array_values($orders);
	$result_data['orders_price'] = $orders_indexed;   
	$res = json_encode($result_data); 


	$ch = curl_init();
	// curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders_price.php");
	curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders_price.php");           
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders_price' => $res));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);	
	$result = curl_exec($ch);
	curl_close($ch);
	$data = json_decode($result, true);

	$sth =  $dbh->prepare("SELECT orderid FROM app_send_price_to_metrika_status WHERE orderid = ?");
	$sth2 = $dbh->prepare("INSERT INTO app_send_price_to_metrika_status (orderid,price,price_agreed,finish_price,status,created) VALUES (?,?,?,?,?,?)");
	$sth3 = $dbh->prepare("UPDATE app_send_price_to_metrika_status SET price = ?,price_agreed = ?,finish_price = ?,status = ?, updated = ? WHERE orderid = ?");
	$date = date("Y-m-d H:i:s");

	if($data[0]['status'] == 'INSERTED' or $data[0]['status'] == 'UPDATED'){							
		$sth->execute(array($orderid));
		$record_exist = $sth->rowCount();
		
		if($record_exist){
			$sth3->execute(array($price,$price_agreed,$finish_price,$data[0]['status'],$date, $orderid));
		}else{
			$sth2->execute(array($orderid,$price,$price_agreed,$finish_price,$data[0]['status'],$date));
		}			
	}



}

