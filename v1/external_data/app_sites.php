<?php

include 'ws.config.php';
global $config;
require 'utils.php';

    try {

    require('mysql_connection.php');

    $last_timestamp = $dbh->prepare("SELECT * FROM app_sites_last_update ORDER BY id DESC LIMIT 1");
    $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
    $last_timestamp->execute(array());
    $date_from = $last_timestamp->fetch();
    $date_from = $date_from['date'];            

    
    $sth = $dbh->prepare('DELETE FROM app_sites');
    $sth->execute(array());

    $sth = $dbh->prepare('ALTER TABLE app_sites AUTO_INCREMENT = 1');
    $sth->execute(array());            
    
    $sth2 = $dbh->prepare('INSERT IGNORE INTO app_sites  (
                                                          route_point_id,
                                                          route_point_operation_id,                                             
                                                          route_id,
                                                          location_id,
                                                          location_name,
                                                          location_address,
                                                          location_city,
                                                          arrival_time,
                                                          departure_time,
                                                          is_visited,
                                                          type_id,
                                                          note,
                                                          customer_name,
                                                          point_seq_no,
                                                          remarks,
                                                          kg_load,
                                                          m3_load,
                                                          pll_load,
                                                          kg_unload,
                                                          m3_unload,
                                                          pll_unload,
                                                          price_agreed,
                                                          update_metrika,     
                                                          import_date
                                                        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)');
                    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/route_points.php");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                $result = curl_exec($ch);
                curl_close($ch); 

                $data = json_decode($result, true); 
                //print_r($data);
                //die();
                $state = $data['state'];

                if($state == 'ERR') {          
                    $logfile = 'app_sites.log';
                    customers_log($data, $logfile);
                }

                foreach($data['route_points'] as $key => $row) {

                    $date = date("Y-m-d H:i:s");

                                                        $sth2->execute(array($row['route_point_id'],
                                                                    $row['route_point_operation_id'],
                                                                    $row['route_id'],
                                                                    $row['location_id'],
                                                                    $row['location_name'],
                                                                    $row['location_address'],                                                      
                                                                    $row['location_city'],
                                                                    $row['arrival_time'],
                                                                    $row['departure_time'],
                                                                    $row['is_visited'],
                                                                    $row['type_id'],
                                                                    $row['note'], 
                                                                    $row['customer_name'],
                                                                    $row['point_seq_no'],
                                                                    $row['remarks'],
                                                                    $row['kg_load'],
                                                                    $row['m3_load'],
                                                                    $row['pll_load'],
                                                                    $row['kg_unload'],
                                                                    $row['m3_unload'],
                                                                    $row['pll_unload'],
                                                                    $row['price_agreed'],
                                                                    $row['update_date'],                                                          
                                                                    $date                                                            
                                                         ));
                }
                
    } catch (PDOException $e) {

       echo "Error!";
       echo $e->getMessage();

       $logfile = 'app_sites.log';
       customers_log($e->getMessage(), $logfile);   
    }
 

