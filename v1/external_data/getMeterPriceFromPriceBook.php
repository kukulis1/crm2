<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/price-algorithm/countries.php";   


// error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
// ini_set('display_errors', 1);
// set_time_limit(900);

function getPriceFromPriceBook($dbh,$post_from,$post_to,$id,$meters,$country_from, $country_to){

   
      $takePrice = takePrice($dbh,$post_from,$post_to,$id,$meters,$bazinis = false, $country_from, $country_to);

       if($takePrice['price'] != 0){
        $price = $takePrice;
      }else{
          $takePrice = takePrice($dbh,$post_from,$post_to,$id,$meters,$bazinis = true, $country_from, $country_to);
          $price = $takePrice;
      }     

                                                 
  return $price;
}


function takePrice($dbh,$post_from,$post_to,$id,$meters,$bazinis, $country_from, $country_to){

if(!empty($post_from)){ 

  $p_from = $dbh->prepare("SELECT `zone_customer`,`zone_base`, `city`, `state`,`post_code` FROM `crm_post_codes` WHERE `post_code` = ".$post_from);

  if($country_from == 'LVA'){    
    $p_from = $dbh->prepare("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $post_from BETWEEN code_from AND code_to");
  }

  $p_from->execute();
  $from = $p_from->fetch();
 }


 if(!empty($post_to)){

  $p_to   = $dbh->prepare("SELECT `zone_customer`,`zone_base`, `city`, `state`,`post_code` FROM `crm_post_codes` WHERE `post_code` = ".$post_to);

  if($country_to == 'LVA'){    
    $p_to = $dbh->prepare("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $post_to BETWEEN code_from AND code_to");
  }

  $p_to->execute();
  $to = $p_to->fetch();
 }

 if(!empty($from) AND !empty($to)){

  $locations =  $dbh->prepare("SELECT * FROM `vtiger_accountscf`
                                                          LEFT JOIN `vtiger_account` on `vtiger_accountscf`.`accountid` = `vtiger_account`.`accountid`
                                                          LEFT JOIN crm_directions ON `crm_directions`.`directionid`=`vtiger_accountscf`.`accountid`
                                                          LEFT JOIN crm_address_post_code adr ON `adr`.`addressid`=`vtiger_accountscf`.`accountid`
                                                          WHERE `vtiger_accountscf`.`accountid` = ".$id);
  $locations->execute();
  $loc = $locations->fetch();

  $direct_location = Array();
  $direct_location_adr = Array();
  $columns = Array('1218','1222','1226','1230','1234','1238','1242','1246','1250','1254','1400','1404','1408','1412','1416','1420','1424','1428','1432','1436');

  $b = 0;


  foreach($columns AS $col){
    if(isset($loc["post_cf_".$col])){
      $slashExplode = explode("/",$loc["post_cf_".$col]);        
      for($f =0; count($slashExplode) > $f; $f++){
       $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
      }      
      for($e =0; count($slashExplode) > $e; $e++){          
          $post_cf_[$col][$e] = multiexplode(array(";","-"), $equalsExplode[$col."_".$e][1]);     
          if(!empty($equalsExplode[$col."_".$e][0])){       
           $direct_location[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]); 
          }       
      }
    }
    $b++;
  } 

  foreach($columns AS $col){
    if(isset($loc["code_cf_".$col])){
      $slashExplode = explode("/",$loc["code_cf_".$col]);        
      for($f =0; count($slashExplode) > $f; $f++){
       $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
      }      
      for($e =0; count($slashExplode) > $e; $e++){          
          $post_cf_[$col][$e] = multiexplode(array(";","-"), $equalsExplode[$col."_".$e][1]);          
          if(!empty($equalsExplode[$col."_".$e][0])){         
            $direct_location_adr[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]);      
          }  
      }
    }
  } 



  if(!$bazinis){

      $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                    LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                    LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                    LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                    LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                    WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = ".$loc["pricebook"]." AND
                                    $meters BETWEEN `vtiger_productcf`.`cf_1637`  AND `vtiger_productcf`.`cf_1639` ";                                     
  
  
  }else{  
  
      $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                    LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                    LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                    LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                    LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                    WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = 10965 AND  
                                    $meters BETWEEN `vtiger_productcf`.`cf_1637`  AND `vtiger_productcf`.`cf_1639`";                                      
    
  }

  $met = 'pdo';

  

if(!$bazinis){

  $distance = 0;
  // $get_price = countriesZones($dbh, $met, $country_from, $country_to, $from, $to,$loc,$distance, $get_price_query_string);
  $get_price = countriesZones($dbh, $direct_location, $direct_location_adr, $met, $country_from, $country_to, $from, $to,$loc,$distance, $get_price_query_string, $post_from,$post_to);
  

   foreach($get_price as $get_prices){        
    $get =  $get_prices;           
  
    $result = array("price" => (float)$get_prices["listprice"]);                              
    break;  
  } 
  
  }else{
  


    require_once $_SERVER['DOCUMENT_ROOT']."/price-algorithm/base_catalog.php";
  
    $location = ":".trim($from["zone_customer"])." ".trim($to["zone_customer"]).":"; 
    $get_price = basecatalog($location,$get_price_query_string_bazinis, $dbh,$met);
  
    $location2 = ":".trim($from["zone_base"])." ".trim($to["zone_base"]).":"; 
    $get_price_base = basecatalog($location2,$get_price_query_string_bazinis, $dbh,$met);
  
    $location3 = ":".trim($from["zone_base"])." ".trim($to["zone_customer"]).":"; 
    $get_price_base2 = basecatalog($location3,$get_price_query_string_bazinis, $dbh,$met);  
  
    $location4 = ":".trim($from["zone_customer"])." ".trim($to["zone_base"]).":"; 
    $get_price_base3 = basecatalog($location4,$get_price_query_string_bazinis, $dbh,$met);  
  
    $get_base = array_merge($get_price, $get_price_base, $get_price_base2,$get_price_base3);

      foreach($get_base as $get_prices){           
          $get =  $get_prices; 
          $result = array("price" => (float)$get_prices["listprice"]);                     
          break;
      }
  
  
  }

}

if(!empty($get)){
  $rez =  $result;   
}else{
  $rez =  array('price' => 0);
}  

return  $rez;

}

