<?php

function Save_Order_Metrika($dbh, $order_id){       

            $result_data = array('order' => array()); 
            
            $sql_params = array();             
            
            $sql = "SELECT
                                s.salesorderid as order_id,
                                s.sostatus AS order_status,                        
                                e.createdtime AS order_date,
                                a.accountname AS customer_name,                
                                a.account_no AS customer_code,
                                a.customer_id,
                                s.external_order_id,             
                                s.ivaz_no,
                                s.source,
                                e.description AS notes,
                                CONCAT(s.load_date_from, ' ', s.load_time_from) AS load_date_from,
                                CONCAT(s.load_date_to, ' ', s.load_time_to) AS load_date_to, 
                                CONCAT(s.unload_date_from, ' ', s.unload_time_from) AS unload_date_from,
                                CONCAT(s.unload_date_to, ' ', s.unload_time_to) AS unload_date_to, 
                                l.load_company,
                                l.bill_street AS load_address,
                                l.bill_city AS load_municipality,
                                l.bill_state AS load_region,
                                l.bill_code AS load_zipcode,
                                l.bill_country AS load_country,
                                h.unload_company,
                                h.ship_street AS unload_address,
                                h.ship_city AS unload_municipality,
                                h.ship_state AS unload_region,
                                h.ship_code AS unload_zipcode,
                                h.ship_country AS unload_country,
                                a.legal_vat_code AS unload_vat_code,
                                i.productid,                                
                                i.quantity AS cargo_qty,
                                i.cargo_measure,
                                i.cargo_wgt,
                                i.cargo_length,
                                i.cargo_width,
                                i.cargo_height,
                                i.comment AS cargo_notes,
                                i.delivery_quantity,
                                r.serviceid
                                    FROM vtiger_salesorder s
                                    JOIN vtiger_crmentity e ON e.crmid = s.salesorderid
                                    JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
                                    JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid                            
                                    JOIN vtiger_users u ON u.id = e.smownerid 
                                    JOIN vtiger_account a ON a.accountid = s.accountid 
                                    JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
                                    LEFT JOIN vtiger_products p ON p.productid = i.productid
                                    LEFT JOIN vtiger_service r ON r.serviceid = i.productid                                    
                                    WHERE e.deleted = 0 AND s.salesorderid = ? ";
            
            array_push($sql_params, $order_id);            
            $sth = $dbh->prepare($sql);
            $sth->execute($sql_params);
  
            $orders = array();

        while ($row = $sth->fetch()) {
            
            $date = date("Y-m-d H:i:s");
            $order_id = intval($row['order_id']);

            $orders[$order_id]['customer_id'] = intval($row['customer_id']);

            if(empty($row['external_order_id'])) {
                $orders[$order_id]['crm_id'] = intval($row['order_id']);  
                $orders[$order_id]['order_id'] = ''; 
            } else {
                $orders[$order_id]['crm_id'] = intval($row['order_id']);  
                $orders[$order_id]['order_id'] = $row['external_order_id']; 
            }       

            $orders[$order_id]['ivaz_no'] = $row['ivaz_no'];  
            $orders[$order_id]['notes'] = $row['notes'];
            $orders[$order_id]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'])); 

            if(!empty($row['load_date_to'])){       
                $orders[$order_id]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to']));   
            } else {
                $orders[$order_id]['load_date_to'] = '';      
            }  

            if(!empty($row['customer_id'])) {
                if(empty($row['load_company'])){       
                    $orders[$order_id]['load_company'] = $row['customer_name'];          
                } else {
                    $orders[$order_id]['load_company'] = $row['load_company'];
                }        
            } else {
                    $orders[$order_id]['load_company'] = $row['customer_name']; 
            }        

            $orders[$order_id]['load_address'] = $row['load_address'];
            $orders[$order_id]['load_settlement'] = '';
            $orders[$order_id]['load_municipality'] = $row['load_municipality'];   
            $orders[$order_id]['load_region'] = $row['load_region'];                      
            $orders[$order_id]['load_zipcode'] = $row['load_zipcode'];
            
                //if($row['load_zipcode'] == 'LTU') {
                //$orders[$order_id]['load_zipcode'] = ''; 
                //}
            
            $orders[$order_id]['load_country'] = $row['load_country'];  
            $orders[$order_id]['load_person'] = '';  
            $orders[$order_id]['load_phone'] = '';  

            if(empty($row['load_company'])){       
                $orders[$order_id]['load_company_code'] = '';            
            } else {
                $orders[$order_id]['load_company_code'] = '';
            }    

            $orders[$order_id]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from']));

            if(!empty($row['unload_date_to'])){       
                $orders[$order_id]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to']));  
            } else {
                $orders[$order_id]['unload_date_to'] = '';      
            }               

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_company'] = $row['customer_name'];          
            } else {
                $orders[$order_id]['unload_company'] = $row['unload_company'];
            }

            $orders[$order_id]['unload_address'] = $row['unload_address']; 
            $orders[$order_id]['unload_settlement'] = '';        
            $orders[$order_id]['unload_municipality'] = $row['unload_municipality'];         
            $orders[$order_id]['unload_region'] = $row['unload_region'];                      
            $orders[$order_id]['unload_zipcode'] = $row['unload_zipcode']; 
            
                //if($row['unload_zipcode'] == 'LTU') {
                //$orders[$order_id]['unload_zipcode'] = ''; 
                //}            
            
            $orders[$order_id]['unload_country'] = $row['unload_country']; 
            $orders[$order_id]['unload_person'] = '';  
            $orders[$order_id]['unload_phone'] = ''; 

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_company_code'] = '';            
            } else {
                $orders[$order_id]['unload_company_code'] = '';
            }

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_vat_code'] = '';            
            } else {
                $orders[$order_id]['unload_vat_code'] = '';
            }

            $orders_items = array();
            
            $orders_items['cargo']['cargo_qty'] = intval($row['cargo_qty']); 
            
            $delivery_quantity = intval($row['delivery_quantity']);
            if(!empty($delivery_quantity)) {
                $orders_items['cargo']['cargo_qty'] = $delivery_quantity;
            }

            if($row['cargo_measure'] == 'pll') {
                $orders_items['cargo']['cargo_measure'] = 1;   
            }     

            $orders_items['cargo']['cargo_wgt'] = sprintf('%0.2f', $row['cargo_wgt']);
            $orders_items['cargo']['cargo_length'] = sprintf('%0.2f', $row['cargo_length']);       
            $orders_items['cargo']['cargo_width'] = sprintf('%0.2f', $row['cargo_width']);      
            $orders_items['cargo']['cargo_height'] = sprintf('%0.2f', $row['cargo_height']);   
            $orders_items['cargo']['cargo_ldm'] = sprintf('%0.2f', $row['cargo_ldm']);
            $orders_items['cargo']['cargo_facilities'] = '';        
            $orders_items['cargo']['cargo_termo'] = 0;  
            $orders_items['cargo']['cargo_return_documents'] = intval($row['cargo_return_documents']);          
            $orders_items['cargo']['cargo_notes'] = $row['cargo_notes'];
            
            if($row['source'] == 'APP') {
                $orders_items['cargo']['update_type'] = 2;   
            }  
            
            if($row['serviceid'] == NULL) {  
                $orders[$order_id]['cargos'][] = $orders_items;
            }
            
            $orders_indexed = array_values($orders);
            $result_data['order'] = $orders_indexed;    

        }             
        
        
            $res = json_encode($result_data);          

            $ch = curl_init();
            //curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders.php");
            curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders.php");           
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders' => $res));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $result = curl_exec($ch);
            curl_close($ch);    

            $data = json_decode($result, true);
            //print_r($data);
            //die();
            
                $state = $data[0]['status'];
                
                if($state == 'ERROR') {          
                    $logfile = 'orders_error_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);
                    
                $order_id = $data[0]['crm_id'];
                $errors = serialize($data[0]['errors']);
                    
                            //Atnaujiname uzsakymo busena           
                            $sth = $dbh->prepare('UPDATE vtiger_salesorder SET status = ?, errors = ? WHERE salesorderid = ?');
                            $sth->execute(array($state, $errors, $order_id));

                            //Atnaujiname uzsakymo redagavimo data    
                            $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
                            $sth->execute(array($date, $order_id));                     
                    
                } else {    
                    $logfile = 'orders_good_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);  
                    
                $order_id = $data[0]['order']['crm_id'];
                $external_order_id = $data[0]['order']['order_id'];
                $ivaz_no = $data[0]['order']['ivaz_no'];
                $barcode = $data[0]['order']['barcode'];
                $direction = $data[0]['order']['direction'];     
                $errors = '';
                
                            //Atnaujiname uzsakymo busena           
                            $sth = $dbh->prepare('UPDATE vtiger_salesorder SET ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ? WHERE salesorderid = ?');
                            $sth->execute(array($ivaz_no, $barcode, $direction, $state, $errors, $external_order_id, $order_id));

                            //Atnaujiname uzsakymo redagavimo data    
                            $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
                            $sth->execute(array($date, $order_id)); 
      
                            }
            
}   

function Save_Route_Metrika($dbh, $route_id, $vechicle_id){       

            $result_data = array('route_status' => array());
            
            $sql_params = array();             
            
            $sql = "SELECT s.*, s.status AS task_status, e.*
                                FROM vtiger_troubletickets s
                                JOIN vtiger_crmentity e ON e.crmid = s.ticketid 									
                                WHERE e.deleted = 0 AND s.ticketid = ?";
            
            array_push($sql_params, $route_id);            
            $sth = $dbh->prepare($sql);
            $sth->execute($sql_params);
  
            $row = $sth->fetch();
            
            $date = date("Y-m-d H:i:s");

            $route['route_id'] = $row['id'];
            
            if($row['task_status'] == 'finished') {  
             $route['status'] = 1;
            } else {
             $route['status'] = 0;  
            };
            
            $route['odometer_km'] = '';
            //$route['vehicle_id'] = $vechicle_id;
            $route['time_updated'] = $row['modifiedtime'];

            array_push($result_data['route_status'], $route);          
        
            $res = json_encode($result_data);          
            //print_r($res);
            //die();
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/route_status.php"); 
            // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/route_status.php");           
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'route_status' => $res));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $result = curl_exec($ch);
            curl_close($ch);    

            $data = json_decode($result, true);
            //print_r($data);
            //die();
            
                $state = $data[0]['status'];
                
                if($state == 'ERROR') {          
                    $logfile = 'routes_error_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);

                    $errors = serialize($data[0]['errors']);
                    
                            //Atnaujiname uzsakymo busena           
                            $sth = $dbh->prepare('UPDATE vtiger_troubletickets SET metrika_status = ?, metrika_errors = ? WHERE ticketid = ?');
                            $sth->execute(array($state, $errors, $route_id));

                            //Atnaujiname uzsakymo redagavimo data    
                            $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
                            $sth->execute(array($date, $route_id));                    
                    
                } else {    
                    $logfile = 'routes_good_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);  
                
                            //Atnaujiname uzsakymo busena           
                            $sth = $dbh->prepare('UPDATE vtiger_troubletickets SET metrika_status = ? WHERE ticketid = ?');
                            $sth->execute(array($state, $route_id));

                            //Atnaujiname uzsakymo redagavimo data    
                            $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
                            $sth->execute(array($date, $route_id)); 
      
                            }
            
}   

function Save_Site_Metrika($dbh, $site_id, $note, $not_visit_reason){       

            $result_data = array('route_point_operation' => array());
            
            $sql_params = array();             
            
            $sql = "SELECT s.*, e.*
                                FROM vtiger_activity s
                                JOIN vtiger_crmentity e ON e.crmid = s.activityid 									
                                WHERE e.deleted = 0 AND s.activityid = ?";
            
            array_push($sql_params, $site_id);            
            $sth = $dbh->prepare($sql);
            $sth->execute($sql_params);
  
            $row = $sth->fetch();
            
            $date = date("Y-m-d H:i:s");

            $site['route_point_operation_id'] = $row['route_point_operation_id'];
            
            if($row['eventstatus'] == 'Completed') {  
             $site['status'] = 1;
            } else if ($row['eventstatus'] == 'In Progress') {
             $site['status'] = 2;  
            }
            
            if (!empty($not_visit_reason)) {
              $site['status'] = 3;     
            }
                        
            $site['note'] = $note;
            //$site['time_updated'] = $row['modifiedtime'];

            array_push($result_data['route_point_operation'], $site);          
        
            $res = json_encode($result_data);          
            //print_r($res);
            //die();
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/route_point_operation.php"); 
            // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/route_point_operation.php");           
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'route_point_operation' => $res));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $result = curl_exec($ch);
            curl_close($ch);    

            $data = json_decode($result, true);
            //print_r($res);
            //die();
            
                $state = $data[0]['status'];
                
                if($state == 'ERROR') {          
                    $logfile = 'sites_error_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);

                    $errors = serialize($data[0]['error']);
                    
                            //Atnaujiname uzsakymo busena           
                            $sth = $dbh->prepare('UPDATE vtiger_activity SET metrika_status = ?, metrika_errors = ? WHERE activityid = ?');
                            $sth->execute(array($state, $errors, $site_id));

                            //Atnaujiname uzsakymo redagavimo data    
                            $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
                            $sth->execute(array($date, $site_id));                    
                    
                } else {    
                    $logfile = 'sites_good_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);  
                
                    $state = $data[0][0]['status'];
                    
                            //Atnaujiname uzsakymo busena           
                            $sth = $dbh->prepare('UPDATE vtiger_activity SET metrika_status = ? WHERE activityid = ?');
                            $sth->execute(array($state, $site_id));

                            //Atnaujiname uzsakymo redagavimo data    
                            $sth = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
                            $sth->execute(array($date, $site_id)); 
      
                            }
            
}   

function Save_GPS_Metrika($dbh, $route_id, $latitude, $longitude, $datetime, $user_id){       

            $result_data = array('track_route_log' => array());           
            
            $date = date("Y-m-d H:i:s");

            $track['route_id'] = $route_id;
            $track['y'] = $latitude; 
            $track['x'] = $longitude; 
            $track['z'] = 1;             
            $track['time_updated'] = $datetime;

            array_push($result_data['track_route_log'], $track);          
        
            $res = json_encode($result_data);          
            //print_r($res);
            //die();
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/track_route_log.php"); 
            // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/track_route_log.php");           
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'track_route_log' => $res));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $result = curl_exec($ch);
            curl_close($ch);    

            $data = json_decode($result, true);
            //print_r($data);
            //die();
            
                $state = $data[0]['status'];
                
                if($state == 'ERROR') {          
                    $logfile = 'tracks_error_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);

                    //$errors = serialize($data[0]['errors']);                 
                    
                } else {    
                    $logfile = 'tracks_good_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);  
                
                            //Atnaujiname uzsakymo busena           
                            $sth = $dbh->prepare('UPDATE app_gps SET sent = ? WHERE datetime = ? AND sent = 0');
                            $sth->execute(array(1, $datetime));
      
                            }
            
}   

function Save_New_Route_Metrika($dbh, $user_id){       

            $sth = $dbh->prepare('SELECT s.*
                                        FROM vtiger_assets s
                                        JOIN vtiger_crmentity e ON e.crmid = s.assetsid 									
                                        WHERE e.deleted = 0 AND (e.smownerid = ? OR s.user_id = ?)');
            $sth->execute(array($user_id, $user_id));
  
            while ($row = $sth->fetch()) {    
    
                $result_data = array('new_route' => array());           

                $date = date("Y-m-d H:i:s");

                $new_route['vehicle_id'] = $row['id'];           
                //$new_route['time_updated'] = $date;

                array_push($result_data['new_route'], $new_route);          
         
            }
            
            $res = json_encode($result_data);          
            //print_r($res);
            //die();
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/new_route.php"); 
            // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/new_route.php");           
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'new_route' => $res));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $result = curl_exec($ch);
            curl_close($ch);    

            $data = json_decode($result, true);
            //print_r($data);
            //die();
            
                $state = $data[0]['status'];
                
                if($state == 'ERROR') {          
                    $logfile = 'new_routes_error_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);

                    //$errors = serialize($data[0]['errors']);                 
                    
                } else {    
                    $logfile = 'new_routes_good_out_app.log';
                    metrika_log($result_data, $logfile);  
                    metrika_log($data, $logfile);  
      
                            }
     
    return  $data[0]['new_route'];                       
}  

function Get_Vehicle_Route_Metrika($dbh, $user_id){       

            $date = date("Y-m-d");  
            
            $sth = $dbh->prepare('SELECT s.*
                                        FROM vtiger_assets s
                                        JOIN vtiger_crmentity e ON e.crmid = s.assetsid 									
                                        WHERE e.deleted = 0 AND (e.smownerid = ? OR s.user_id = ?)');
            $sth->execute(array($user_id, $user_id));
  
            while ($row = $sth->fetch()) {            
                $vehicle_id = $row['id'];            
            }

            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_vehicle_routes.php"); 
            // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/export/crm/get_vehicle_routes.php");           
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date' => $date, 'vehicle_id' => $vehicle_id));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $result = curl_exec($ch);
            curl_close($ch);    

            $data = json_decode($result, true);
            
                    $logfile = 'vehicle_routes_good_out_app.log';
                    metrika_log($vehicle_id, $logfile);  
                    metrika_log($data, $logfile);
                    
                    if(count($data) > 0 ) {
                        foreach($data['route'] as $key => $row) {
                            
                            if($row['status'] != 'finished') {
                            $route_id = $row['id'];    
                            break;
                            }
                        }        
                    }
                    
    return  $route_id;           
    
}  

function metrika_log($log_variable, $logfile) {

    global $config;        
      
    $log_json = '--------------------------------' . date("Y-m-d H:i:s") . '--------------------------------' . "\n" . json_encode($log_variable) . "\n" . "\n";
    
    file_put_contents($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'logs'. DIRECTORY_SEPARATOR . $logfile, $log_json, FILE_APPEND);
	
    if (filesize($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'logs'. DIRECTORY_SEPARATOR . $logfile) > 1024 * 1024 * 10) {
        copy($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'logs'. DIRECTORY_SEPARATOR . $logfile, 
             $config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'logs'. DIRECTORY_SEPARATOR . date('Y-m-d_H-i-s') . '_' . $logfile);
        unlink($config['paths']['api_path'] . DIRECTORY_SEPARATOR . 'external_data' . DIRECTORY_SEPARATOR . 'logs'. DIRECTORY_SEPARATOR . $logfile);
    }
}

