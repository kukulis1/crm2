<?php
include 'ws.config.php';
global $config;
require 'utils.php';
include 'getPriceFromPriceBook.php';
set_time_limit(500);

error_reporting(E_WARNING & ~E_NOTICE & ~E_DEPRECATED) ;

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();                       

    $sth = $dbh->prepare("SELECT shipment_code,crmid 
                          FROM vtiger_crmentity
                          JOIN vtiger_salesorder on salesorderid=crmid
                          WHERE deleted = 0 and  setype = 'SalesOrder' AND (label = '' OR label IS NULL) AND (shipment_code != '' OR shipment_code IS NOT NULL)");

    $sth2 = $dbh->prepare('UPDATE vtiger_crmentity SET label = ? WHERE crmid = ?');


    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {
        $sth2->execute(array(                                                                                          
                            $row['shipment_code'],                                                                        
                            $row['crmid']
        ));        
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
