<?php
include 'ws.config.php';
global $config;
require 'utils.php';
include 'getPriceFromPriceBook.php';
set_time_limit(500);

error_reporting(1) ;

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();    

                   

$sth = $dbh->prepare("SELECT GROUP_CONCAT(DISTINCT t.ordered) AS ordered, 
                             GROUP_CONCAT(DISTINCT t.revised) AS revised,                           
                             GROUP_CONCAT(DISTINCT l.external_load_id) AS external_load_id, c.salesorderid
                                    FROM app_loads2 t                                                                                                    
                                    LEFT JOIN vtiger_inventoryproductrel l ON l.external_load_id = t.id                     
                                    LEFT JOIN vtiger_salesorder c ON c.external_order_id = t.order_id                                                                        
                                    LEFT JOIN vtiger_crmentity e ON e.crmid = c.salesorderid                              
                                    LEFT JOIN app_orders2 lu on lu.id=c.external_order_id
                                    WHERE e.deleted = 0 AND c.accountid = 2762
                                    GROUP BY t.id");



    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {

        print_R($row);

        $external_load_id = explode(",", $row['external_load_id']);   

        $quantity = explode(",", $row['quantity']);
        

        $weight_ordered = array();
        $dim_ordered = array();
        $dim_ordered2 = array();
        $square_ordered = array();
        $square_ordered_temp = array();
        $weight_revised = array();
        $dim_revised = array();
        $dim_revised2 = array();
        $square_revised = array();
        $square_revised_temp = array();


        $ordered = explode(",",$row['ordered']);  
        
        for($i = 0; $i < count($ordered); $i++){
            $exploded_arr = explode(' ',$ordered[$i]);
            $weight_ordered[] = $exploded_arr[0];         
            $dim_ordered[] = array_product(explode('x', $exploded_arr[1])) * $quantity[$i];     
            $dim_ordered2[] = explode('x', $exploded_arr[1]); 
            $square_ordered_temp[] = explode('x', $exploded_arr[1]); 
            unset($square_ordered_temp[$i][2]);           
            $square_ordered[] = array_product($square_ordered_temp[$i]) * $quantity[$i];
         }
 
         $revised = explode(",",$row['revised']);
         
         for($i = 0; $i < count($revised); $i++){
            $exploded_arr2 = explode(' ',$revised[$i]);
            $weight_revised[] = $exploded_arr2[0];
            // $dim_revised[] = array_product(explode('x', $exploded_arr2[1])) * $quantity[$i];  
            $dim_revised2[] = explode('x', $exploded_arr2[1]);
            $square_revised_temp[] = explode('x', $exploded_arr2[1]); 
            unset($square_revised_temp[$i][2]);
            $square_revised[] = array_product($square_revised_temp[$i]) * $quantity[$i];         
         }

          updateOrderedCargo($dbh, $weight_ordered,$dim_ordered2,$row['salesorderid'],$external_load_id);   
          updateRevisedCargo($dbh, $weight_revised,$dim_revised2,$row['salesorderid'],$external_load_id);   
      
                    
                 
                
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}



function updateOrderedCargo($dbh, $weight,$dim,$salesorderid,$external_load_id){
    $sth = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET ordered_weight = ?, ordered_length = ?, ordered_width = ?, ordered_height = ?  WHERE id = ? AND external_load_id = ? ');

    for($e = 0; $e < COUNT($weight); $e++){
        $sth->execute(array(                                                                                          
            $weight[$e],
            $dim[$e][0],
            $dim[$e][1],
            $dim[$e][2],
            $salesorderid,
            $external_load_id[$e] 
        ));
    }
}

function updateRevisedCargo($dbh, $weight,$dim,$salesorderid,$external_load_id){
    $sth = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET revised_weight = ?, revised_length = ?, revised_width = ?, revised_height = ?  WHERE id = ? AND external_load_id = ? ');

    for($e = 0; $e < COUNT($weight); $e++){
        $sth->execute(array(                                                                                          
            $weight[$e],
            $dim[$e][0],
            $dim[$e][1],
            $dim[$e][2],
            $salesorderid,
            $external_load_id[$e] 
        ));
    }
}









?>
