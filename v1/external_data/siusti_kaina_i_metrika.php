<?php
include 'ws.config.php';
global $config;
require 'utils.php';
include 'sendPriceToMetrika.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();    

 
    $today = "2021-11-09"; 


    $sth = $dbh->prepare("SELECT s.external_order_id, FORMAT(s.total,2) as finish_price,  FORMAT(sc.cf_1374,2) as price, FORMAT(sc.cf_1376,2) as agreed_price    
                            FROM vtiger_salesorder s 
                            LEFT JOIN vtiger_salesordercf sc ON sc.salesorderid=s.salesorderid  
                            LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid                           
                            WHERE DATE_FORMAT(e.createdtime, '%Y-%m-%d') = '$today' AND  (total IS NOT NULL AND total > 0) AND  s.salesorderid = 1677340");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {
        if($row['finish_price'] != 0){
            sendPriceToMetrikaCron($row['external_order_id'], $row['price'], $row['agreed_price'], $row['finish_price'],$dbh);   
        }   
    }           
                    
    
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
