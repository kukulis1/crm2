<?php

include 'ws.config.php';
global $config;
require 'utils.php';

    try {


    require('mysql_connection.php');

    $last_timestamp = $dbh->prepare("SELECT * FROM app_sites_last_update ORDER BY id DESC LIMIT 1");
    $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
    $last_timestamp->execute(array());
    $date_from = $last_timestamp->fetch();
    $date_from = $date_from['date'];
    
    $sth = $dbh->prepare('DELETE FROM app_routes');
    $sth->execute(array());

    $sth = $dbh->prepare('ALTER TABLE app_routes AUTO_INCREMENT = 1');
    $sth->execute(array());            
    
    $sth2 = $dbh->prepare('INSERT IGNORE INTO app_routes  (
                                                          id,
                                                          route_code,
                                                          start_date,
                                                          end_date,
                                                          driver1_id,
                                                          status,
                                                          weight,
                                                          pallets,
                                                          is_own,
                                                          price,
                                                          print_note,
                                                          cost_agreed,
                                                          location_name, 
                                                          driver1, 
                                                          visited, 
                                                          distance_m, 
                                                          route_points_count, 
                                                          kg_unload, 
                                                          max_kg, 
                                                          max_pll, 
                                                          max_m3, 
                                                          pll_load, 
                                                          load_kg, 
                                                          m3_unload, 
                                                          direction,
                                                          registration_number,
                                                          start_date_fact,
                                                          end_date_fact,
                                                          update_metrika,
                                                          import_date
                                                        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');





                    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/routes.php");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                $result = curl_exec($ch);
                curl_close($ch); 

                $data = json_decode($result, true); 
                //print_r($data);
                //die();
                $state = $data['state'];

                if($state == 'ERR') {          
                    $logfile = 'app_routes.log';
                    customers_log($data, $logfile);
                }

                foreach($data['route'] as $key => $row) {
                    $date = date("Y-m-d H:i:s");

                                                        $sth2->execute(array($row['id'],
                                                                            $row['route_code'],
                                                                            $row['start_date'],
                                                                            $row['end_date'],
                                                                            $row['driver1_id'],
                                                                            $row['status'],
                                                                            $row['weight'],
                                                                            $row['pallets'],
                                                                            $row['company'],
                                                                            $row['price'],
                                                                            $row['print_note'],
                                                                            $row['cost_agreed'],
                                                                            $row['location_name'], 
                                                                            $row['driver1'], 
                                                                            $row['visited'], 
                                                                            $row['distance_m'], 
                                                                            $row['route_points_count'], 
                                                                            $row['kg_unload'], 
                                                                            $row['max_kg'], 
                                                                            $row['max_pll'], 
                                                                            $row['max_m3'], 
                                                                            $row['pll_load'], 
                                                                            $row['load_kg'], 
                                                                            $row['m3_unload'], 
                                                                            $row['direction'],
                                                                            $row['registration_number'],
                                                                            $row['start_time_fact'],
                                                                            $row['end_time_fact'],
                                                                            $row['update_date'],                                                            
                                                                            $date                                                            
                                                         ));
                }
                
    } catch (PDOException $e) {

       echo "Error!";
       echo $e->getMessage();

       $logfile = 'app_routes.log';
       customers_log($e->getMessage(), $logfile);   
    }
 

