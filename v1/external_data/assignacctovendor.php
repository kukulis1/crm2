<?php
set_time_limit(0);
include 'ws.config.php';
global $config;
require 'utils.php';

    try {

        require('mysql_connection.php');  

     
        $get_clients = $dbh->prepare("SELECT REPLACE(vtiger_account.accountname, ', UAB', '') as name,  vtiger_account.accountid
                                        FROM vtiger_account
                                        LEFT JOIN vtiger_vendor ON vtiger_account.accountid=vtiger_vendor.accountid
                                        INNER JOIN vtiger_crmentity ON crmid=vtiger_account.accountid
                                        WHERE deleted = 0 AND vtiger_vendor.accountid IS NULL
                                        ORDER BY vtiger_account.accountid DESC");
        
        $get_clients->setFetchMode(PDO::FETCH_ASSOC);
        $get_clients->execute(); 

      

        foreach ($get_clients AS $value) {            

            $name = str_replace(["'", ","],['',''], $value['name']);
            $search_vendor = $dbh->prepare("SELECT vendorid, vendorname FROM vtiger_vendor WHERE vendorname LIKE '$name%' ");
            $search_vendor->setFetchMode(PDO::FETCH_ASSOC);
            $search_vendor->execute(); 
            $info = $search_vendor->fetch();

            if(!empty($info['vendorid'])){
                echo "<br>";
                echo "UPDATE ".$info['vendorname'];
                $update = $dbh->prepare("UPDATE vtiger_vendor SET accountid = ? WHERE vendorid = ?");
                $update->execute([$value['accountid'], $info['vendorid']]);
            }else{
                echo "<br>";
                echo "INSERT ".$value['name'];
                SaveAccountToVendors($dbh, $value['accountid']);
            }     
        
        }



               
        
                
    } catch (PDOException $e) {

       echo "Error!";
       echo $e->getMessage();

       $logfile = 'crm_customers_in.log';
       customers_log($e->getMessage(), $logfile);   
    }
 


function SaveAccountToVendors($dbh, $account_id){  
    $date = date("Y-m-d H:i:s");    
    $result = $dbh->prepare('SELECT accountname,phone,email1,account_type,legal_entity_code,legal_vat_code,billing_address,municipality,post_code FROM vtiger_account WHERE accountid = ?');
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $result->execute(array($account_id)); 
    $account_info = $result->fetch();

    $accountname = $account_info['accountname'];
    $phone = $account_info['phone'];
    $email1 = $account_info['email1'];
    $billing_address = $account_info['billing_address'];
    $municipality = $account_info['municipality']; 
    $post_code = $account_info['post_code'];
    $legal_entity_code = $account_info['legal_entity_code'];
    $legal_vat_code = $account_info['legal_vat_code'];
    $account_type = $account_info['account_type'];

    $ven =  last_ven_id($dbh);      
    $last_entity_record =  get_last_entity_id_and_update($dbh, 1); 
    insert_entity3($dbh, 'Vendors', $last_entity_record, 26, NULL,'Metrika',  $accountname, $date);    

    $result2 = $dbh->prepare("INSERT INTO vtiger_vendor (vendorid,vendor_no,vendorname,phone,email,street,city,postalcode,accountid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $result2->execute(array($last_entity_record,$ven,$accountname, $phone, $email1, $billing_address,$municipality,$post_code,$account_id));

    $result = $dbh->prepare("INSERT INTO vtiger_vendorcf (vendorid,cf_1458, cf_1460, cf_1466) VALUES (?,?,?,?)");
    $result->execute(array($last_entity_record,$legal_entity_code,$legal_vat_code,$account_type));
}

function last_ven_id($dbh){
	$result = $dbh->prepare("SELECT vendor_no FROM vtiger_vendor ORDER BY vendorid DESC LIMIT 1"); 
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $result->execute();
    $num = substr($result->fetch()['vendor_no'], 3);

	$num = $num +1;
	$vel = 'VEN'.$num;
	return $vel;
}
