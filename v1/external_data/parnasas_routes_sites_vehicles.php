<?php
ini_set('max_execution_time', '0');
set_time_limit(0);
error_reporting(0);

$ch1 = curl_init();
curl_setopt($ch1, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_routes.php');
curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch1, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch1, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch1, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch1);
curl_close($ch1);

$ch2 = curl_init();
curl_setopt($ch2, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_routes_vtiger_insert.php');
curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch2, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch2, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch2);
curl_close($ch2);

$ch3 = curl_init();
curl_setopt($ch3, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_routes_vtiger_update.php');
curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch3, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch3, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch3, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch3);
curl_close($ch3);

$ch4 = curl_init();
curl_setopt($ch4, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_sites.php');
curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch4, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch4, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch4, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch4);
curl_close($ch4);

$ch5 = curl_init();
curl_setopt($ch5, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_sites_vtiger_insert.php');
curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch5, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch5, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch5, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch5);
curl_close($ch5);

$ch6 = curl_init();
curl_setopt($ch6, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_sites_vtiger_update.php');
curl_setopt($ch6, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch6, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch6, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch6, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch6);
curl_close($ch6);

$ch7 = curl_init();
curl_setopt($ch7, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_vehicles.php');
curl_setopt($ch7, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch7, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch7, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch7, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch7);
curl_close($ch7);

$ch8 = curl_init();
curl_setopt($ch8, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_vehicles_vtiger_insert.php');
curl_setopt($ch8, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch8, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch8, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch8, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch8);
curl_close($ch8);

$ch9 = curl_init();
curl_setopt($ch9, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/app_vehicles_vtiger_update.php');
curl_setopt($ch9, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch9, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch9, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch9, CURLOPT_CONNECTTIMEOUT, 30);
curl_exec($ch9);
curl_close($ch9);
