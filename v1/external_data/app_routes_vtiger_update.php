<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_troubletickets SET       
                                                                title = ?,
                                                                status = ?,
                                                                start_date = ?, 
                                                                end_date = ?, 
                                                                start_time = ?,
                                                                end_time = ?,                                                                  
                                                                weight = ?, 
                                                                pallets = ?,
                                                                is_own = ?,
                                                                price = ?,
                                                                driver1_id = ?,      
                                                                update_date = ?
                                                    WHERE ticketid = ?');

    $sth3 = $dbh->prepare("UPDATE vtiger_ticketcf SET cf_2495 = ?, cf_2497 = ?, cf_2499 = ?, cf_2501 = ?, cf_2503 = ?, cf_2505 = ?, cf_2507 = ?, cf_2509 = ?, cf_2511 = ?, cf_2513 = ?, cf_2515 = ?, cf_2517 = ?, cf_2519 = ?, cf_2521 = ?, cf_2523 = ?, cf_2552 = ?, cf_2554 = ?,cf_2556 = ? WHERE ticketid = ?");                                                        
                    
    
    $sth = $dbh->prepare('SELECT t.*, c.ticketid, u.id AS user_id, g.modifiedtime
                            FROM app_routes t
                            JOIN vtiger_troubletickets c ON c.id = t.id
                            JOIN vtiger_crmentity g ON g.crmid = c.ticketid
                            LEFT JOIN vtiger_users u ON u.title = t.driver1_id
                            WHERE g.deleted = 0');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {

            if(empty($row['user_id']) || $row['user_id'] == NULL) {
                $user_id = 26;
            } else {
                $user_id = $row['user_id']; 
            }          

            if($row['status'] == 'started') {
            $row['status'] = 'in progress';
            }              
            
            $start_date = date("Y-m-d", strtotime($row['start_date']));
            $end_date = date("Y-m-d", strtotime($row['end_date']));
            $start_time = date("H:i:s", strtotime($row['start_date']));
            $end_time = date("H:i:s", strtotime($row['end_date']));             
            
            $date = date("Y-m-d H:i:s");      
            
            if(!empty($row['update_metrika'])) {
            $update_time = $row['update_metrika'];     
            } else {
            $update_time = $date;    
            }
                    $update_entity = update_entity($dbh, $row['ticketid'], $user_id, NULL, $row['route_code'], $update_time);
  
                    $sth2->execute(array($row['route_code'],
                                        $row['status'],                      
                                        $start_date,
                                        $end_date,
                                        $start_time,
                                        $end_time,
                                        $row['weight'],
                                        $row['pallets'],
                                        $row['is_own'],      
                                        $row['price'], 
                                        $row['driver1_id'],                                            
                                        $date,
                                        $row['ticketid']
                    ));  
                    
                    $sth3->execute(array($row['print_note'], 
                                        $row['cost_agreed'], 
                                        $row['location_name'], 
                                        $row['driver1'], 
                                        $row['visited'], 
                                        $row['distance_m'], 
                                        $row['route_points_count'], 
                                        $row['kg_unload'], 
                                        $row['max_kg'], 
                                        $row['max_pll'], 
                                        $row['max_m3'], 
                                        $row['pll_load'], 
                                        $row['load_kg'], 
                                        $row['m3_unload'], 
                                        $row['direction'],
                                        $row['registration_number'],
                                        $row['start_date_fact'],
                                        $row['end_date_fact'],
                                        $row['ticketid']
                    ));

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>