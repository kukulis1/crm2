<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';


    try {
        require('../mysql_connection.php'); 

        $month_day = date("m-d");       
          

        $sth = $dbh->prepare('SELECT s.* FROM vtiger_accountscf af
                              JOIN app_standing_salesorders s ON s.account_id=af.accountid                            
                              WHERE cf_2002 = 1 AND enabled = 1');

        $sth2 = $dbh->prepare('SELECT id FROM app_executed_templates WHERE id = ? AND executed_date = ?');

        $sth3 = $dbh->prepare('SELECT id FROM app_executed_templates WHERE id = ?');

        $sth4 = $dbh->prepare('INSERT INTO app_executed_templates VALUES (?,?)');

        $sth5 = $dbh->prepare('UPDATE app_executed_templates SET executed_date = ? WHERE id = ?');
       

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());
     
        $execute = array();
        while ($row = $sth->fetch()) {  
          $sth2->execute(array($row['id'],$month_day));
          if(!$sth2->rowCount()){           
            if($row['execute_type'] == 1){		              
                if (date("l") == $row['execute'] AND time() >= strtotime($row['execute_time'])){        
                    $execute[] = $row['id'];                  
                }              
            }elseif($row['execute_type'] == 2){
              if(date('d') == $row['execute'] AND time() >= strtotime($row['execute_time'])){
               $execute[] = $row['id'];
              }
            }else{
              if(time() >= strtotime($row['execute_time'])){
                $execute[] = $row['id'];
               }
            }  
          }     
        }

     
        foreach ($execute as $value) {
          createSalesOrder($dbh, $value);
          $sth3->execute(array($value));
          if($sth3->rowCount()){
            $sth5->execute(array($month_day,$value));
          } else{
            $sth4->execute(array($value,$month_day));
          }   
        }

          print_R($execute);

      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }

function createSalesOrder($dbh, $id){
  
    $date = date("Y-m-d H:i:s");
    $order_date = date("Y-m-d");  
    $month_day = date("m-d");    

    $sth = $dbh->prepare('SELECT * FROM app_standing_salesorders WHERE id = ?');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array($id));

    $sth2 = $dbh->prepare('SELECT * FROM app_standing_salesorders_cargo WHERE id = ?');
    $sth2->setFetchMode(PDO::FETCH_ASSOC);
    $sth2->execute(array($id));     

    $user_id = 26;         

    $stmt = $dbh->prepare("INSERT INTO vtiger_salesorder (salesorderid,subject,total,subtotal,accountid,sostatus,load_date_from,load_time_from,load_date_to,load_time_to,unload_date_from,unload_time_from,unload_date_to,unload_time_to,source,import_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");				

    $stmt2 = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,margin,comment,description,cargo_measure,cargo_wgt,cargo_length,cargo_width,cargo_height,pll,source,ordered_weight,ordered_length,ordered_width,ordered_height,revised_weight,revised_length,revised_width,revised_height,m3,service) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    $stmt3 = $dbh->prepare("INSERT INTO vtiger_sobillads (sobilladdressid,bill_city,bill_code,bill_country,bill_street,load_company,load_contact,load_phone) VALUES (?,?,?,?,?,?,?,?)");
    $stmt4 = $dbh->prepare("INSERT INTO vtiger_soshipads (soshipaddressid,ship_city,ship_code,ship_country,ship_street,unload_company,unload_contact,unload_phone) VALUES (?,?,?,?,?,?,?,?)");
    $stmt5 = $dbh->prepare("INSERT INTO vtiger_salesordercf (salesorderid,cf_855,cf_1374, cf_1661,cf_1663, cf_1564,cf_1566) VALUES (?,?,?,?,?,?,?)");

    $last_entity_record = last_entity_record2($dbh);
    $insert_entity = insert_entity($dbh, 'SalesOrder', $last_entity_record, $user_id, NULL,  '', $date);  

    while ($row = $sth->fetch()) {
      $load_date = date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days"));
			$unload_date = date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days"));
      $stmt->execute(array($last_entity_record,'Užsakymas',$row['price'],$row['price'],$row['account_id'], 'Sent',$load_date,$row['load_time_from'],$load_date,$row['load_time_to'],$unload_date,$row['unload_time_from'],$unload_date,$row['unload_time_to'],'CRM',$date));
      $stmt3->execute(array($last_entity_record,$row['bill_city'],$row['bill_code'],$row['bill_country'],$row['bill_street'],$row['load_company'],$row['load_contact'],$row['load_phone']));
      $stmt4->execute(array($last_entity_record, $row['ship_city'],$row['ship_code'],$row['ship_country'],$row['ship_street'],$row['unload_company'],$row['unload_contact'],$row['unload_phone']));
      $stmt5->execute(array($last_entity_record,$row['order_type'],$row['pricebook_price'],$user_id,$user_id,$row['load_phone'],$row['unload_phone']));
    }

    $i = 1;
    while ($row = $sth2->fetch()) {
      $m3 = round(($row['cargo_length'] * $row['cargo_width'] * $row['cargo_height']) * $row['quantity'],2);
      $pll = ($row['measure'] == 1 ?  $row['quantity'] : 0) ;

      $stmt2->execute(array($last_entity_record, 
                                              14244,
                                              $i,
                                              $row['quantity'],
                                              $row['price'],
                                              $row['comment'],
                                              $row['comment'],                                                  
                                              $row['measure'],
                                              $row['cargo_wgt'],
                                              $row['cargo_length'],
                                              $row['cargo_width'],
                                              $row['cargo_height'],
                                              $pll,
                                              'CRM',
                                              $row['cargo_wgt'],
                                              $row['cargo_length'],
                                              $row['cargo_width'],
                                              $row['cargo_height'],
                                              $row['cargo_wgt'],
                                              $row['cargo_length'],
                                              $row['cargo_width'],
                                              $row['cargo_height'],
                                              $m3,
                                              2));
      $i++;
    }

 exportSalesOrderToMetrika($dbh,$last_entity_record); 

}


function exportSalesOrderToMetrika($adb,$salesorderid){
  $order_sent = 'Sent';  
  $order_not_sent = 'Not Sent'; 

  $result_data = array('order' => array()); 
  $stmt = $adb->prepare("SELECT
                                          s.salesorderid as order_id,
                                          s.total as finish_price,
                                          sc.cf_855 as order_type,
                                          sc.cf_1374 as price,
                                          sc.cf_1376 as agreed_price,
                                          sc.cf_1564 as load_phone,
                                          sc.cf_1566 as unload_phone,                                
                                          s.sostatus AS order_status,   
                                          s.status,                     
                                          e.createdtime AS order_date,
                                          a.accountname AS customer_name,                
                                          a.account_no AS customer_code,
                                          a.customer_id,
                                          s.external_order_id,             
                                          s.ivaz_no,
                                          s.source,                                
                                          e.description AS notes,
                                          CONCAT(s.load_date_from, ' ', s.load_time_from) AS load_date_from,
                                          CONCAT(s.load_date_to, ' ', s.load_time_to) AS load_date_to, 
                                          CONCAT(s.unload_date_from, ' ', s.unload_time_from) AS unload_date_from,
                                          CONCAT(s.unload_date_to, ' ', s.unload_time_to) AS unload_date_to,                                
                                          l.load_company,
                                          l.bill_street AS load_address,
                                          l.bill_city AS load_municipality,
                                          l.bill_state AS load_region,
                                          l.bill_code AS load_zipcode,
                                          l.bill_country AS load_country,
                                          l.load_contact,
                                          h.unload_company,
                                          h.ship_street AS unload_address,
                                          h.ship_city AS unload_municipality,
                                          h.ship_state AS unload_region,
                                          h.ship_code AS unload_zipcode,
                                          h.ship_country AS unload_country,
                                          h.unload_contact,
                                          a.legal_vat_code AS unload_vat_code,
                                          i.productid,  
                                          i.external_load_id,                              
                                          i.quantity AS cargo_qty,
                                          i.cargo_measure,
                                          i.cargo_wgt,
                                          i.cargo_length,
                                          i.cargo_width,
                                          i.cargo_height,
                                          i.comment AS cargo_notes,
                                          r.serviceid,
                                          i.service,
                                          i.margin
                                                  FROM vtiger_salesorder s
                                                  JOIN vtiger_salesordercf sc ON sc.salesorderid = s.salesorderid
                                                  JOIN vtiger_crmentity e ON e.crmid = s.salesorderid
                                                  JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
                                                  JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid                            
                                                  JOIN vtiger_users u ON u.id = e.smownerid 
                                                  JOIN vtiger_account a ON a.accountid = s.accountid 
                                                  JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
                                                  LEFT JOIN vtiger_products p ON p.productid = i.productid
                                                  LEFT JOIN vtiger_service r ON r.serviceid = i.productid                                    
                                                  WHERE e.deleted = 0 AND s.salesorderid = ?");

  $stmt->execute(array($salesorderid));

  $orders = array();

while($row = $stmt->fetch()) {

  $date = date("Y-m-d H:i:s");
  $salesorderid = intval($row['order_id']);

  $orders[$salesorderid]['customer_id'] = intval($row['customer_id']);

  if(empty($row['external_order_id'])) {
        $orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
        // $orders[$salesorderid]['order_id'] = ''; 
  } else {
        $orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
        $orders[$salesorderid]['order_id'] = $row['external_order_id']; 
  } 

  if($row['order_type'] == 'Transporto užsakymas'){
        $shipment_type = 'parcel';
  }else{
        $shipment_type = 'movement';
  }     


  $orders[$salesorderid]['ivaz_no'] = $row['ivaz_no'];  
  $orders[$salesorderid]['shipment_type'] = $shipment_type;
  $orders[$salesorderid]['notes'] = iconv(mb_detect_encoding($row['notes']), "UTF-8", $row['notes']);
  $orders[$salesorderid]['price'] = (float)$row['price'];
  $orders[$salesorderid]['price_agreed'] = (float)$row['agreed_price'];
  $orders[$salesorderid]['finish_price'] = (float)$row['finish_price'];

  $orders[$salesorderid]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'])); 

  if(!empty($row['load_date_to'])){       
        $orders[$salesorderid]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to']));   
  } else {
        $orders[$salesorderid]['load_date_to'] = '';      
  }  

  $load_company = preg_replace('/[^A-Za-z0-9 ]/', '', $row['load_company']);
  $load_company = str_replace("quot", "", $load_company);   


  if(!empty($row['customer_id'])) {
        if(empty($row['load_company'])){       
                $orders[$salesorderid]['load_company'] = $row['customer_name'];          
        } else {
                $orders[$salesorderid]['load_company'] = $load_company;
        }        
  } else {
                $orders[$salesorderid]['load_company'] = $row['customer_name']; 
  } 

  $load_address = str_replace('&Scaron;', 'Š', $row['load_address']);
  $load_address = str_replace('&scaron;', 'š', $row['load_address']);


  $orders[$salesorderid]['load_address'] = $load_address;
  $orders[$salesorderid]['load_settlement'] = '';
  $orders[$salesorderid]['load_municipality'] = ($row['load_municipality'] ?: "");   
  $orders[$salesorderid]['load_region'] = $row['load_region'];                      
  $orders[$salesorderid]['load_zipcode'] = $row['load_zipcode'];

        //if($row['load_zipcode'] == 'LTU') {
        //$orders[$salesorderid]['load_zipcode'] = ''; 
        //}

  $orders[$salesorderid]['load_country'] = 'LTU';  
  // $orders[$salesorderid]['load_country'] = $row['load_country'];  
  $orders[$salesorderid]['load_person'] = $row['load_contact'];  


  $orders[$salesorderid]['load_phone'] = $row['load_phone'];


  if(empty($row['load_company'])){       
        $orders[$salesorderid]['load_company_code'] = '';            
  } else {
        $orders[$salesorderid]['load_company_code'] = '';
  }    

  $orders[$salesorderid]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from']));

  if(!empty($row['unload_date_to'])){       
        $orders[$salesorderid]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to']));  
  } else {
        $orders[$salesorderid]['unload_date_to'] = '';      
  } 



  $unload_company = str_replace("quot", "", $row['unload_company']);   

  $unload_company = str_replace('&Scaron;', 'Š', $unload_company);
  $unload_company = str_replace('&scaron;', 'š', $unload_company);

  if(empty($row['unload_company'])){       
        $orders[$salesorderid]['unload_company'] = $row['customer_name'];          
  } else {
        $orders[$salesorderid]['unload_company'] = $unload_company;
  }

  $unload_address = str_replace('&Scaron;', 'Š', $row['unload_address']);
  $unload_address = str_replace('&scaron;', 'š', $row['unload_address']);

  $unload_address = str_replace('Scaron;', 'Š', $unload_address);
  $unload_address = str_replace('scaron;', 'š', $unload_address);

  $orders[$salesorderid]['unload_address'] = $unload_address; 
  $orders[$salesorderid]['unload_settlement'] = '';      
  $orders[$salesorderid]['unload_municipality'] = ($row['unload_municipality'] ?: "");        
  $orders[$salesorderid]['unload_region'] = $row['unload_region'];                      
  $orders[$salesorderid]['unload_zipcode'] = $row['unload_zipcode']; 

        //if($row['unload_zipcode'] == 'LTU') {
        //$orders[$salesorderid]['unload_zipcode'] = ''; 
        //}            

  // $orders[$salesorderid]['unload_country'] = $row['unload_country']; 
  $orders[$salesorderid]['unload_country'] = 'LTU'; 
  $orders[$salesorderid]['unload_person'] = $row['unload_contact'];  ;  
  $orders[$salesorderid]['unload_phone'] = $row['unload_phone'];

  if(empty($row['unload_company'])){       
        $orders[$salesorderid]['unload_company_code'] = '';            
  } else {
        $orders[$salesorderid]['unload_company_code'] = '';
  }

  if(empty($row['unload_company'])){       
        $orders[$salesorderid]['unload_vat_code'] = '';            
  } else {
        $orders[$salesorderid]['unload_vat_code'] = '';
  }

  $orders_items = array(); 
  $orders_services = array(); 
  $delete_service = array(); 


  if($row['productid'] == 36641){
        $orders_services['service_type_id'] = $row['service'];
        $orders_services['quantity_agreed'] = $row['cargo_qty'];
        $orders_services['price_agreed'] = $row['margin'];

        $orders_services['remarks'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

  }



        $orders_items['cargo']['load_id'] = $row['external_load_id'];
        $orders_items['cargo']['update_type'] = 2;

  if($row['productid'] != 36641){
        $orders_items['cargo']['cargo_qty'] = ($row['cargo_qty'] < 1 ) ? intval(1.000) : intval($row['cargo_qty']);
        if($row['cargo_measure'] == 0){
            $orders_items['cargo']['cargo_measure'] = 1;
        }else{
            $orders_items['cargo']['cargo_measure'] = $row['cargo_measure'];
    }


        if($row['cargo_measure'] == null){
                $orders_items['cargo']['cargo_measure'] = 1;
        }

        if(($row['cargo_measure'] == 1 OR $row['cargo_measure'] == 2 OR $row['cargo_measure'] == 3) OR ($row['cargo_measure'] == 'pll' OR $row['cargo_measure'] == 'RUS-pll' OR $row['cargo_measure'] == 'FIN-pll')){
                $orders_items['cargo']['volume_unit2'] = intval($row['cargo_qty']);
        }else{
                $orders_items['cargo']['volume_unit2'] = 0;
        }
    
        // itoma
        $orders_items['cargo']['cargo_wgt'] = (float)str_replace(",",".", $row['cargo_wgt']);
        $orders_items['cargo']['cargo_length'] = (float)str_replace(",",".", $row['cargo_length']);      
        $orders_items['cargo']['cargo_width'] = (float)str_replace(",",".", $row['cargo_width']);      
        $orders_items['cargo']['cargo_height'] = (float)str_replace(",",".", $row['cargo_height']);   

        $orders_items['cargo']['cargo_ldm'] = sprintf('%0.2f', $row['cargo_ldm']);
        $orders_items['cargo']['cargo_facilities'] = '';        
        $orders_items['cargo']['cargo_termo'] = 0;  
        $orders_items['cargo']['cargo_return_documents'] = intval($row['cargo_return_documents']);
    
        $orders_items['cargo']['cargo_notes'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

        if($row['source'] == 'CRM') {
                if($row['status'] == 'ERROR' OR $row['status'] == ''){ 
                        $orders_items['cargo']['update_type'] = 1;
                        $insertType = 1; 
                }else{				
                        $orders_items['cargo']['update_type'] = 1;  
                        $insertType = 1;  								
                }
        }  
  }    


    if($row['serviceid'] == NULL) {  
            if($row['productid'] != 36641){    
                    $orders[$salesorderid]['cargos'][] = $orders_items;        
            }

            if($row['productid'] == 36641){  
                    $orders[$salesorderid]['additional_services'][] = $orders_services;  
            }          
    }    
                                                
        $orders_indexed = array_values($orders);

        $result_data['order'] = $orders_indexed;  
  }
                
  $res = json_encode($result_data, JSON_UNESCAPED_UNICODE);   


  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders.php");
//   curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders.php");           
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders' => $res));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
  $result = curl_exec($ch);
  curl_close($ch);         

  $data = json_decode($result, true);

 
  $state = $data[0];
      
  if($state == 'ERROR') {    
          $order_id = $data[0]['crm_id'];
          $errors = $data[1]['errors'][0]['error_message'];     
      
          //Atnaujiname uzsakymo busena           
          $stmt2 = $adb->prepare('UPDATE vtiger_salesorder SET sostatus = ?, status = ?, errors = ? WHERE salesorderid = ?');
          $stmt2->execute(array($order_not_sent, $state, $errors, $salesorderid));

          //Atnaujiname uzsakymo redagavimo data    
          $stmt3 = $adb->prepare('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?');
          $stmt3->execute(array($date, $salesorderid));                      
          
  } else {    					
      $order_id = $data[0]['order']['crm_id'];
      $external_order_id = $data[0]['order']['order_id'];   
      $shipment_code = $data[0]['order']['shipment_code'];
      $ivaz_no = $data[0]['order']['ivaz_no'];
      $barcode = $data[0]['order']['barcode'];
      $direction = $data[0]['order']['direction']; 
      $status =  $data[0]['status']; 
      $errors = '';


                          //Atnaujiname uzsakymo busena           
          if($insertType == 1){
                  $stmt = $adb->prepare('UPDATE vtiger_salesorder SET sostatus = ?, ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?');
                  $stmt->execute(array($order_sent, $ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
          }else{
              $stmt = $adb->prepare('UPDATE vtiger_salesorder SET ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?');
              $stmt->execute(array($ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
          }
              $stmt2 = $adb->prepare('UPDATE vtiger_inventoryproductrel SET external_order_id = ?, external_load_id = ? WHERE id = ? AND sequence_no = ?');
                                      
              $seq = 1;
              foreach($data[0]['order']['loads'] as $load_id){                  
                      $stmt2->execute(array($external_order_id, $load_id, $order_id, $seq ));                     
                      $seq++;
              }								


          //Atnaujiname uzsakymo redagavimo data    
          $stmt3 = $adb->prepare('UPDATE vtiger_crmentity SET label = ?, modifiedtime = ? WHERE crmid = ?');
          $stmt3->execute(array($shipment_code, $date, $order_id));

        
  }
}
