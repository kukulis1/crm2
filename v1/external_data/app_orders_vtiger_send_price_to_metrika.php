<?php
include 'ws.config.php';
global $config;
require 'utils.php';

$date = date("Y-m-d");
if(!file_exists("logs/send_price_to_metrika/$date")){
  mkdir("logs/send_price_to_metrika/$date", 0777, true); 
}
   
$logfile = "send_price_to_metrika/$date.log";

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   
 
    $from = date('Y-m-d H',strtotime('-1 hour'));
    $to = date('Y-m-d H');

    $sth = $dbh->prepare("SELECT s.external_order_id AS order_id, FORMAT(s.total,2) as price, FORMAT(s.total,2) as finish_price, REPLACE(IF(cf_1376,cf_1376,0),',','.') AS price_agreed    
    FROM vtiger_salesorder s 
    LEFT JOIN vtiger_salesordercf sc ON sc.salesorderid=s.salesorderid  
    LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid AND setype = 'SalesOrder'             
    WHERE (DATE_FORMAT(e.modifiedtime, '%Y-%m-%d %H') BETWEEN '$from' AND '$to') AND external_order_id > 0 AND deleted = 0
    ORDER BY s.salesorderid");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $orders = [];
    $data = [];
    
    foreach ($sth as $row) {
     $data[] = $row;
    }
    
    $data = array_chunk($data, 100);
    
    foreach ($data as $item) {
      $result_data = [];
      $orders_indexed = array_values($item);
      $result_data['orders_price'] = $orders_indexed;
      $res = json_encode($result_data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
      sendPriceToMetrika($res); 
      customers_log($res, $logfile);          
    }  
                    
    
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
