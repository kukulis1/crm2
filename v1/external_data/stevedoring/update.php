<?php
error_reporting(1);
set_time_limit(500);
include '../ws.config.php';
global $config;
require '../utils.php';


try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_stevedoring SET stevedoringno = ? WHERE salesorderid = ?');
               
    $sth = $dbh->prepare("SELECT DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS createdtime, shipment_code, a.accountname, 
                                CONCAT(bill_street,' ',bill_city,' ',bill_code) AS  bill_address, 
                                CONCAT(ship_street,' ',ship_city,' ',ship_code) AS  ship_address, 
                                SUM(inv.cargo_wgt) as weight, 
                                ROUND(s.total,2) AS total, 
                                ROUND(i.margin,2) as stevedoring_price,  
                                REPLACE(cf_1687,'|##|',',') as loaders, 
                                e.smownerid, s.salesorderid, s.external_order_id   
                                FROM vtiger_salesorder s
                                JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid  AND i.inventory_type = 'stevedoring' AND i.service = 2
                                JOIN vtiger_inventoryproductrel inv ON inv.id= s.salesorderid  AND inv.inventory_type = ''
                                JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                JOIN vtiger_account a ON a.accountid=s.accountid
                                JOIN vtiger_sobillads bl ON bl.sobilladdressid=s.salesorderid
                                JOIN vtiger_soshipads sh ON sh.soshipaddressid=s.salesorderid
                                JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid        
                                WHERE e.deleted = 0 
                                GROUP BY i.id");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());
  

    while ($row = $sth->fetch()) {  

        $sth2->execute(array(  
          $row['salesorderid'], 
          $row['salesorderid']          
        )); 
       

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
