<?php
error_reporting(1);
set_time_limit(500);
include '../ws.config.php';
global $config;
require '../utils.php';

if(date("H") != '00'){

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_stevedoring (stevedoringid, 
                                                            stevedoringno,
                                                            stevedoring_tks_date, 
                                                            stevedoring_tks_orderno, 
                                                            stevedoring_tks_client, 
                                                            stevedoring_tks_shipperaddress, 
                                                            stevedoring_tks_receiveraddres, 
                                                            stevedoring_tks_weight, 
                                                            stevedoring_tks_durationh, 
                                                            stevedoring_tks_orderprice, 
                                                            stevedoring_tks_stevedoringpri, 
                                                            stevedoring_tks_namelastname) 
                                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');
               
    $sth = $dbh->prepare("SELECT DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS createdtime, shipment_code, a.accountname, 
                                CONCAT(bill_street,' ',bill_city,' ',bill_code) AS  bill_address, 
                                CONCAT(ship_street,' ',ship_city,' ',ship_code) AS  ship_address, 
                                SUM(inv.cargo_wgt) as weight, 
                                ROUND(s.total,2) AS total, 
                                ROUND(i.margin,2) as stevedoring_price,  
                                REPLACE(cf_1687,'|##|',',') as loaders, 
                                e.smownerid, s.salesorderid   
                                FROM vtiger_salesorder s
                                JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid  AND i.inventory_type = 'stevedoring' AND i.service = 2
                                JOIN vtiger_inventoryproductrel inv ON inv.id= s.salesorderid  AND inv.inventory_type = ''
                                JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                JOIN vtiger_account a ON a.accountid=s.accountid
                                JOIN vtiger_sobillads bl ON bl.sobilladdressid=s.salesorderid
                                JOIN vtiger_soshipads sh ON sh.soshipaddressid=s.salesorderid
                                JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid        
                                WHERE e.deleted = 0  AND s.salesorderid NOT IN (SELECT st.stevedoringno FROM vtiger_stevedoring st)
                                GROUP BY i.id");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth5 = $dbh->prepare("INSERT INTO vtiger_stevedoringcf (stevedoringid) VALUES (?)");

    $orders_count = $sth->rowCount();

    // $sth3 = $dbh->prepare("SELECT id FROM vtiger_crmentity_seq");
    // $sth3->setFetchMode(PDO::FETCH_ASSOC);
    // $sth3->execute(array());
    // $seq = $sth3->fetch();

    // $new_seq = $seq['id'] + $orders_count;

    if($orders_count > 0){    

      $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
      $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count);  

      // $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
      // $sth4->execute(array($new_seq)); 
    }

    // $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
    // $last_entity_record = $seq['id']+1;  
    

    while ($row = $sth->fetch()) {  

      $date = date("Y-m-d H:i:s");

      $sth6->execute(array($last_entity_record)); 
      $record_exist = $sth6->rowCount();

      if($record_exist) {
         $last_entity_record_old = $last_entity_record;
         $sth3->execute(array());
         $seq = $sth3->fetch();
         $last_entity_record = $seq['id']+1;
         $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
         $sth4->execute(array($last_entity_record));     
      }

        $insert_entity = insert_entity3($dbh, 'Stevedoring', $last_entity_record, $row['smownerid'], NULL,'CRM',  $row['shipment_code'], $date);    
        $sth2->execute(array(  
            $last_entity_record,  
            $row['salesorderid'],                  
            $row['createdtime'],             
            $row['shipment_code'],             
            $row['accountname'],             
            $row['bill_address'],             
            $row['ship_address'],             
            $row['weight'], 
            NULL,               
            $row['total'],             
            $row['stevedoring_price'],             
            $row['loaders']           
        )); 

        $sth5->execute(array($last_entity_record));
                    
        if($record_exist) {             
            $last_entity_record = $last_entity_record_old;    
        }         
        
        $last_entity_record++;

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
  //  $dbh->rollBack();    
  //  echo "Error!";
  //  echo $e->getMessage();
  sendReportMail($e->getMessage(),basename(__FILE__));
}

}

?>
