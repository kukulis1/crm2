<?php

  include '../ws.config.php';
  include $_SERVER['DOCUMENT_ROOT'].'/config.inc.php';
  global $config;

  global $mailHostQuality,$mailUsername,$mailPassword; 

  require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
  require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
  require $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';

  require_once $_SERVER['DOCUMENT_ROOT'].'/vtlib/Vtiger/Email/vendor/autoload.php';

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use PHPMailer\PHPMailer\Exception;


  use PhpImap\Exceptions\ConnectionException;
  use PhpImap\Mailbox;

if(date("H") != '00'){


  try {

      require('../mysql_connection.php');

      $dbh->beginTransaction();                 
                  
    
      $date = date("Y-m-d H:i:s");
      $setype = 'Clientsemails';

      $sql = $dbh->prepare("INSERT INTO vtiger_clientsemails (clientsemailsid,clientsemails_tks_sendername,clientsemails_tks_email) VALUES(?,?,?)");
      $sql1 = $dbh->prepare("INSERT INTO vtiger_clientsemailscf (clientsemailsid) VALUES(?)");
      $sql2 = $dbh->prepare("INSERT INTO vtiger_clientsemails_email (client_email_id,mailid,hash,type,subject,email,body) VALUES(?,?,?,?,?,?,?)");

      $sql4 = $dbh->prepare("SELECT client_email_id FROM vtiger_clientsemails_email WHERE type = 1 AND mailid = ? LIMIT 1");   

      $sql5 = $dbh->prepare("SELECT client_email_id FROM vtiger_clientsemails_email WHERE type = 1 AND email = ? OR cc = ? ORDER BY client_email_id DESC LIMIT 1");

      $sql6 = $dbh->prepare("INSERT INTO vtiger_clientsemails_seen (mail_id, seen, seen_time, reviewed_person) VALUES (?,?,?,?)");       
      
      // $sql7 = $dbh->prepare("UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?");

      $sql8 = $dbh->prepare("UPDATE vtiger_clientsemails_seen SET seen = ?, seen_time = ?, reviewed_person = ? WHERE mail_id = ?");
      
      $sql9 = $dbh->prepare("SELECT client_email_id,hash FROM vtiger_clientsemails_email WHERE type = 1 AND hash = ? LIMIT 1");

      $mailbox = new Mailbox($mailHostQuality,$mailUsername,$mailPassword);  
      $last_check = date("Ymd");

      try {
      $mail_ids = $mailbox->searchMailbox('SINCE "'.$last_check.'"');

      foreach ($mail_ids AS $mail_id) {  

          $email = $mailbox->getMail(
              $mail_id, 
              true 
          );    
    
      
          $pattern = "/[[][a-z]{3}[:][_][A-Z]{6}[_][A-Z]{5}[_]\d{10}[.]\d{1,9}[:][a-z]{3}[]]/";   

          $matches = array(); 
          preg_match($pattern, $email->subject, $matches);          
          $subject_hash = $matches[0]; 

          $sql4->execute(array($mail_id));       
          $sql9->execute(array($subject_hash));   
          $get_id2 = $sql9->fetch();
        
          $num_rows2 = $sql4->rowCount(); 
          $num_rows3 = $sql9->rowCount(); 
            

          if(strpos($email->textPlain, "From")){
          $body = substr($email->textPlain, 0, strpos($email->textPlain, "From"));
          }else{
          $body = $email->textPlain;
          }

      

          if(!$num_rows2){
            if(!$num_rows3){       
              $hash = generateHash($mail_id);
              $entity_id = get_last_entity_id_and_update($dbh,1);  
              insert_entity_spec($dbh, $setype, $entity_id, 26, NULL, $email->fromName,'Email', $date);
              $sql->execute(array($entity_id ,$email->fromName,$email->fromAddress));
              $sql1->execute(array($entity_id));
              $sql2->execute(array($entity_id,$mail_id,$hash,1 ,$email->subject,$email->fromAddress,$body));   
              $sql6->execute(array($entity_id,0,'',''));          
              addAttachment($dbh,$email,$entity_id);          
            }else{      
              $claim_id = $get_id2['claimid'];     
              $hash = $get_id2['hash']; 
              $sql2->execute(array($claim_id,$mail_id,$hash,1 ,$email->subject,$email->fromAddress,$body));
              addAttachment($dbh,$email,$claim_id);           
              $sql8->execute(array(0,'','',$claim_id)); 
            }
          }                
          
      }    

      } catch (ConnectionException $ex) {
          die('IMAP connection failed: '.$ex->getMessage());
      } catch (Exception $ex) {
          die('An error occured: '.$ex->getMessage());
      } 


  $dbh->commit();
  $mailbox->disconnect();  
              

      
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo "Error!";
    echo $e->getMessage();
  }


}


function generateHash($mail_id){
  $date = time();
  $string = "[ref:_CLIENT_EMAIL_";
  $string .= $date.".".$mail_id;
  $string .= ":ref]";
  return $string;
}


 function addAttachment($db,$email,$id){
  if ($email->hasAttachments()) {                 
    $attachments = $email->getAttachments();
    $year = date("Y");
    $month = date("F");
    $dayofweek = "week".weekOfMonth(strtotime(date("Y-m-d"))); 

    foreach ($attachments AS $attachment) {   
      if($attachment->name != 'jpeg'){          
        
        if (!file_exists($_SERVER['DOCUMENT_ROOT']."/storage/$year/$month/$dayofweek")) {
          mkdir($_SERVER['DOCUMENT_ROOT']."/storage/$year/$month/$dayofweek", 0777, true);
        } 
        $ext = pathinfo($attachment->name, PATHINFO_EXTENSION); 
        $oldName = lt_chars(str_replace(' ', '',pathinfo($attachment->name, PATHINFO_FILENAME)));                             
            if(empty($ext)) $ext = $oldName;
            $newFileName = $oldName.'_'.time().'.'.$ext;          
            $filename = $oldName.'_'.time();          
            $attachment->setFilePath($_SERVER['DOCUMENT_ROOT']."/storage/$year/$month/$dayofweek/$newFileName");            
            $patch = "storage/$year/$month/$dayofweek/";        
            
            if ($attachment->saveToDisk()) {              
            insertDocument($db,$id,26,$patch,$filename, $newFileName);                            
            } else {
            echo "ERROR, could not save!<br>";
            } 
      }                     
    }                  
  }
}

 function insertDocument($db,$recordid,$userid,$patch,$filename,$filefullname)
  {
    global $root_directory;
    $date_now = date('Y-m-d H:i:s');
    $entity_id = get_last_entity_id_and_update($db,1);  
    insert_entity_spec($db,'Documents', $entity_id, $userid, NULL, '', 'CRM', $date_now);
    insert_senotesrel($db, $entity_id, $recordid);
    insertNotes($db,$entity_id,$filename,$filefullname,1,'I',1);    
    insert_NoteCf($db, $entity_id);
    
    $entity_id2 = get_last_entity_id_and_update($db,1);    
    if(renameFile($patch,$filefullname,$entity_id2)){
      insert_entity_spec($db,'Documents Attachment', $entity_id2, $userid, NULL, '', 'CRM', $date_now);
      insertAttachments($db, $entity_id2,$filefullname,$patch,$recordid);
      insertSeAttachmentsrel($db, $entity_id,$entity_id2);	
    }
  
  }

 function insert_entity_spec($dbh, $setype, $entity_id, $user_id, $description, $label,$source, $date)
  {
    $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, source, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $sth->execute(array($entity_id, $user_id, $user_id, $setype, $description, $label,$source,$date, $date));
  }

 function insert_senotesrel($dbh, $entity_id, $moduleId)
  {
    $sth = $dbh->prepare("INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES (?,?)");
    $sth->execute(array($moduleId,$entity_id));
  }

 function insertAttachments($dbh, $entity_id,$title,$filename,$recordid)
  {
    $sth = $dbh->prepare("INSERT INTO vtiger_attachments (attachmentsid, name, path,related_id)  VALUES (?,?,?,?)");
    $sth->execute(array($entity_id, $title, $filename,$recordid));
  }

 function insertSeAttachmentsrel($dbh, $entity_id,$moduleId)
  {
    $sth = $dbh->prepare("INSERT INTO vtiger_seattachmentsrel (crmid, attachmentsid)  VALUES (?,?)");
    $sth->execute(array($entity_id, $moduleId));
  }

 function insertNotes($dbh, $entity_id,$title,$filename,$folderid,$locationType,$filestatus)
  {
    $sth = $dbh->prepare("INSERT INTO vtiger_notes (notesid, title, filename, folderid, filelocationtype, filestatus) 
                            VALUES (?,?,?,?,?,?)");
    $sth->execute(array($entity_id, $title, $filename, $folderid,$locationType,$filestatus));
  }

 function insert_NoteCf($dbh, $entity_id)
  {
    $sth = $dbh->prepare("INSERT INTO vtiger_notescf (notesid) VALUES (?)");
    $sth->execute(array($entity_id));
  }

  function get_last_entity_id_and_update($dbh,$addEntity){
    // NOTE Numeracija papildom užsakymu kiekiu
    $sth3 = $dbh->prepare("SELECT id FROM `vtiger_crmentity_seq`");
    $sth3->setFetchMode(PDO::FETCH_ASSOC);
    $sth4 = $dbh->prepare("UPDATE `vtiger_crmentity_seq` SET id = ?");

    $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq READ'); // Uzrakinama lentele, kad joks kitas irasas nepasimtu id
    $lock->execute(array());     
    $sth3->execute(array());
    $seq = $sth3->fetch(); // Gaunam paskutini id
    $lock2 = $dbh->prepare('UNLOCK TABLES');
    $lock2->execute(array());

    $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq WRITE');
    $lock->execute(array());
    $new_seq = $seq['id'] + $addEntity;    // Pridedam prie paskutinio id uzsakymu kieki 
    $sth4->execute(array($new_seq)); 
    $lock2 = $dbh->prepare('UNLOCK TABLES');
    $lock2->execute(array());

    return $seq['id']+1;
}

 function lt_chars($text) {   
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

    return $text; 
  }

 function weekOfMonth($date){  
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
 }

  function renameFile($location,$filename,$entity_id){ 
    global $root_directory;
    $oldName = $root_directory.'/'.$location.$filename;
    $newName = $root_directory.'/'.$location.$entity_id.'_'.$filename;
    if(rename("$oldName","$newName")){
      return true;
    }  
  }
  
