<?php

require '../../../config.inc.php';

$options = array(
  PDO::ATTR_PERSISTENT => true,
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

$dbh = new PDO("mysql:host={$dbconfig['db_server']};dbname={$dbconfig['db_name']}", $dbconfig['db_username'], $dbconfig['db_password'], $options);
$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
$dbh->exec("set names utf8");