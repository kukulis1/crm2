<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_troubletickets (ticketid,
                                                                id,
                                                                title,
                                                                ticket_no,
                                                                status,
                                                                start_date,
                                                                end_date,
                                                                start_time,
                                                                end_time,                                                     
                                                                weight, 
                                                                pallets, 
                                                                is_own, 
                                                                price,
                                                                driver1_id,
                                                                ownership,
                                                                category,
                                                                import_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'); 
                                                                
    $sth5 = $dbh->prepare("INSERT INTO vtiger_ticketcf (ticketid, cf_2495, cf_2497, cf_2499, cf_2501, cf_2503, cf_2505, cf_2507, cf_2509, cf_2511, cf_2513, cf_2515, cf_2517, cf_2519, cf_2521, cf_2523, cf_2552,cf_2554,cf_2556) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");  
    
    $sth = $dbh->prepare("SELECT t.*, u.id AS user_id
                            FROM app_routes t
                            LEFT JOIN vtiger_users u ON u.title = t.driver1_id                                              
                            LEFT JOIN vtiger_troubletickets c ON c.ticket_no=t.id
                            WHERE ticketid IS NULL");
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $orders_count = $sth->rowCount();

    if($orders_count > 0){
      
  
        $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
        $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count);     
    
        while ($row = $sth->fetch()) {
            $sth6->execute(array($last_entity_record)); 
            $record_exist = $sth6->rowCount();
   
            if($record_exist) {
               $last_entity_record_old = $last_entity_record;             
               $last_entity_record = get_last_entity_id_and_update($dbh,1);    
               $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
               $sth4->execute(array($last_entity_record));     
            }

                
                if(empty($row['user_id']) || $row['user_id'] == NULL) {
                    $user_id = 26;
                } else {
                    $user_id = $row['user_id']; 
                }            
                
                if($row['status'] == 'started') {
                    $row['status'] = 'in progress';
                }        
                
                $start_date = date("Y-m-d", strtotime($row['start_date']));
                $end_date = date("Y-m-d", strtotime($row['end_date']));
                $start_time = date("H:i:s", strtotime($row['start_date']));
                $end_time = date("H:i:s", strtotime($row['end_date']));            
                
                $ownership = 'Metrika';
                $category = 'Route';            
                $date = date("Y-m-d H:i:s");

                    
                        $last_entity_record = last_entity_record($dbh);
                        $insert_entity = insert_entity($dbh, 'HelpDesk', $last_entity_record, $user_id, NULL, $row['route_code'], $date);                 
                        
                        $sth2->execute(array($last_entity_record, 
                                            $row['id'], 
                                            $row['route_code'], 
                                            $row['id'], 
                                            $row['status'],       
                                            $start_date,
                                            $end_date,
                                            $start_time,
                                            $end_time,                       
                                            $row['weight'],
                                            $row['pallets'],
                                            $row['is_own'],
                                            $row['price'],      
                                            $row['driver1_id'], 
                                            $ownership,                     
                                            $category,
                                            $date            
                        ));

                        $sth5->execute(array($last_entity_record, 
                                             $row['print_note'], 
                                             $row['cost_agreed'], 
                                             $row['location_name'], 
                                             $row['driver1'], 
                                             $row['visited'], 
                                             $row['distance_m'], 
                                             $row['route_points_count'], 
                                             $row['kg_unload'], 
                                             $row['max_kg'], 
                                             $row['max_pll'], 
                                             $row['max_m3'], 
                                             $row['pll_load'], 
                                             $row['load_kg'], 
                                             $row['m3_unload'], 
                                             $row['direction'], 
                                             $row['registration_number'], 
                                             $row['start_date_fact'], 
                                             $row['end_date_fact'] 
                        ));

            if($record_exist) {             
                $last_entity_record = $last_entity_record_old;    
                }         
        
            $last_entity_record++;
        }
    }

    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error!";
   echo $e->getMessage();
}

?>