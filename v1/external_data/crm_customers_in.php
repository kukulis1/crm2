<?php
set_time_limit(0);
include 'ws.config.php';
global $config;
require 'utils.php';

    try {

        require('mysql_connection.php');  

        $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
        $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
        $last_timestamp->execute(array());
        $date_from = $last_timestamp->fetch();
        $date_from = date('Y-m-d H:i:s',strtotime($date_from['date']. '-15 minutes'));
    
        $date_to = date("Y-m-d H:i:s");              

        $sth = $dbh->prepare('DELETE FROM crm_customers_in');
        $sth->execute(array());

        $sth = $dbh->prepare('ALTER TABLE crm_customers_in AUTO_INCREMENT = 1');
        $sth->execute(array());            
        
        $sth2 = $dbh->prepare('INSERT IGNORE INTO crm_customers_in  (
                                                            customer_id,
                                                            customer_code,
                                                            customer_name,
                                                            type,
                                                            country_code,
                                                            region,
                                                            municipality,
                                                            settlement,
                                                            billing_address,
                                                            post_code,
                                                            legal_no,
                                                            vat_no,
                                                            contact_person_name,
                                                            phone,
                                                            email,
                                                            blocked,
                                                            default_sender_location_id,
                                                            sender_name,
                                                            sender_address,
                                                            sender_city,
                                                            sender_postcode,
                                                            default_consignee_location_id,
                                                            consignee_name,
                                                            consignee_address,
                                                            consignee_city,
                                                            consignee_postcode, 
                                                            account_manager_id,
                                                            pricebook,
                                                            import_date
                                                            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
                    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/clients.php");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from, 'date_to' => $date_to));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                $result = curl_exec($ch);
                curl_close($ch);    

                $data = json_decode($result, true); 
                $pricebook = 10965;


                $state = $data['state'];

                if($state == 'ERR') {          
                    $logfile = 'crm_customers_in.log';
                    customers_log($data, $logfile);
                }

            if(!empty($data['clients'])){

                foreach($data['clients'] as $row) {

                    $customer_name = str_replace('&Scaron;', 'Š', $row['customer_name']);
                    $customer_name = str_replace('&scaron;', 'š', $customer_name);

                    $date = date("Y-m-d H:i:s");

                                                        $sth2->execute(array($row['customer_id'],
                                                        $row['customer_code'],
                                                        $customer_name,
                                                        $row['type'],
                                                        $row['country_code'],
                                                        $row['region'],
                                                        $row['municipality'],
                                                        $row['settlement'],
                                                        $row['billing_address'],
                                                        $row['post_code'],
                                                        $row['legal_no'],
                                                        $row['vat_no'],
                                                        $row['contact_person_name'],
                                                        $row['phone'],
                                                        $row['email'],
                                                        $row['blocked'],
                                                        $row['default_sender_location_id'],
                                                        $row['sender_location_name'],
                                                        $row['sender_street_address'],
                                                        $row['sender_municipality'],
                                                        $row['sender_post_code'],
                                                        $row['default_consignee_location_id'],
                                                        $row['consignee_location_name'],
                                                        $row['consignee_street_address'],
                                                        $row['consignee_municipality'],
                                                        $row['consignee_post_code'],
                                                        $row['account_manager_id'],    
                                                     	$pricebook,
                                                        $date                                                            
                                                         ));
                }
            }
                
    } catch (PDOException $e) {

       echo "Error!";
       echo $e->getMessage();

       $logfile = 'crm_customers_in.log';
       customers_log($e->getMessage(), $logfile);   
    }
 

