<?php

$host = 'localhost';
$dbname = 'crm';
$user = 'root';
$pass = '***';

// header('Content-Type: application/json; charset=utf-8');

$dbh = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh->exec("set names utf8");
