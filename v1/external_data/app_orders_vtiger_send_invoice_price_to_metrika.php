<?php
include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   

    // NOTE Tikrina ar yra uzsakymu kurie istrinti is saskaitos ir tu uzsakymu saskaitos kaina keicia i 0
    $salesorders_removed_from_invoice = $dbh->prepare("SELECT s.external_order_id AS shipment_id, s.salesorderid AS shipment_crm_id
                          FROM vtiger_salesorder s
                          JOIN vtiger_salesordercf c ON c.salesorderid=s.salesorderid
                          LEFT JOIN vtiger_invoice_salesorders_list li ON li.salesorderid=s.salesorderid
                          INNER JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                          WHERE DATE_FORMAT(e.createdtime,'%Y') >= 2021 AND s.external_order_id > 0 AND invoiceid IS NULL AND c.cf_1944 > 0  AND deleted = 0
                          GROUP BY s.salesorderid");


    $update_order = $dbh->prepare("UPDATE vtiger_salesordercf SET cf_1944 = 0 WHERE salesorderid = :salesorderid");                      

    $salesorders_removed_from_invoice->setFetchMode(PDO::FETCH_ASSOC);
    $salesorders_removed_from_invoice->execute(array());



    $price_to_send_in_metrika = [];
        
    foreach($salesorders_removed_from_invoice AS $order){
        $update_order->execute([':salesorderid' => $order['shipment_crm_id']]);        
        $price_to_send_in_metrika[$order['shipment_id']] = [
            'shipment_crm_id' => $order['shipment_crm_id'],
            'shipment_id' => $order['shipment_id'],
            'invoice_price' => null,
            'invoice_price_reset' => true, 
            'status' => null
        ];
        
    }

    $price_json = json_encode($price_to_send_in_metrika);    


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/shipment_crm_price_and_status.php");           
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'shipment_data' => $price_json));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    $result = curl_exec($ch);
    curl_close($ch);           
    
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>
