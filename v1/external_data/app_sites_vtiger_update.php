<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   

    
    $now = date("Y-m-d H:i");
    $minute = date("i", strtotime($now));
    $minutes = change_minutes_of_half($minute);
    $time = date("Y-m-d H:$minutes:00", strtotime($now)); 

    $insert = $dbh->prepare("INSERT INTO app_sites_last_update (date) VALUES (?)");
    $insert->execute(array($time));
    
    $sth2 = $dbh->prepare('UPDATE vtiger_routepoints SET        
                                                                
                                                                routepoints_tks_customer_name = ?,
                                                                routepoints_tks_type = ? ,
                                                                routepoints_tks_location = ? ,
                                                                routepoints_tks_start_time = ? ,
                                                                routepoints_tks_end_time = ? ,
                                                                routepoints_tks_point_seq_no = ? ,
                                                                routepoints_tks_remarks = ? ,
                                                                routepoints_tks_kg_load = ? ,
                                                                routepoints_tks_m3_load = ? ,
                                                                routepoints_tks_pll_load = ? ,
                                                                routepoints_tks_kg_unload = ? ,
                                                                routepoints_tks_m3_unload = ? ,
                                                                routepoints_tks_pll_unload = ? ,
                                                                routepoints_tks_price_agreed = ?,
                                                                routepoints_tks_address =?,
                                                                routepoints_tks_status = ?
                                                    WHERE routepointsid = ?');
                    
    
    $sth = $dbh->prepare('SELECT t.*, c.routepointsid, u.id AS user_id, l.modifiedtime, y.relcrmid, s.salesorderid
                            FROM app_sites t
                            JOIN vtiger_routepoints c ON c.routepointsno = t.route_point_operation_id
                            JOIN vtiger_troubletickets h ON h.id = t.route_id
                            INNER JOIN vtiger_crmentity l ON l.crmid = h.ticketid  
                            LEFT JOIN vtiger_salesorder s ON s.external_order_id = c.routepointsid
                            LEFT JOIN vtiger_crmentity e ON e.crmid = s.salesorderid AND e.deleted = 0    
                            LEFT JOIN vtiger_crmentityrel y ON y.crmid = c.routepointsid AND y.relcrmid = s.salesorderid 
                            LEFT JOIN vtiger_users u ON u.title = h.driver1_id
                            INNER JOIN vtiger_crmentity g ON g.crmid = c.routepointsid
                            WHERE l.deleted = 0 AND g.deleted = 0');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {
            if(empty($row['user_id']) || $row['user_id'] == NULL) {
                $user_id = 26;
            } else {
                $user_id = $row['user_id']; 
            }            
            
            if($row['type_id'] == 'L') {
                $type_id = 'Pasikrovimas';
            } else  {
                $type_id = 'Išsikrovimas'; 
            }               
            
            if(empty($row['is_visited']) || $row['is_visited'] == NULL) {
                $status = 'Planuojama';
            } else  {
                $status = 'Išvyko'; 
            }   
            
            $address = $row['location_address'] . ', ' . $row['location_city'];         
                 
            if(!empty($row['update_metrika'])) {
            $update_time = $row['update_metrika'];     
            } else {
            $update_time = $row['modifiedtime'];    
            }            
            
            $date = date("Y-m-d H:i:s");   
            
                    $update_entity = update_entity($dbh, $row['routepointsid'], $user_id, NULL, $row['location_name'], $update_time);
  
                    $sth2->execute(array(
                                        $row['customer_name'],
                                        $type_id,
                                        $row['location_name'],                                        
                                        $row['arrival_time'], 
                                        $row['departure_time'],                                       
                                        $row['point_seq_no'],                                       
                                        $row['remarks'],  
                                        $row['kg_load'], 
                                        $row['m3_load'], 
                                        $row['pll_load'], 
                                        $row['kg_unload'], 
                                        $row['m3_unload'], 
                                        $row['pll_unload'],
                                        $row['price_agreed'],                                         
                                        $address,
                                        $status,
                                        $row['routepointsid']
                    ));     

                       
                    
                   
                    
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>