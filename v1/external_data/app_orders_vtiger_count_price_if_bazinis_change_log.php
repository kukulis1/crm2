<?php
include 'ws.config.php';
global $config;
require 'utils.php';
include 'getPriceFromPriceBook.php';
set_time_limit(500);

// error_reporting(E_WARNING & ~E_NOTICE & ~E_DEPRECATED) ;
$minutes = change_minutes(date("i"));
try {

    require('mysql_connection.php');

    $dbh->beginTransaction();    

    $today = date('Y-m-d H');             


    $sth = $dbh->prepare("SELECT accountname, GROUP_CONCAT(CONCAT(l.ordered_weight,' ',l.ordered_length,'x',l.ordered_width,'x',ordered_height)) AS ordered, 
                            GROUP_CONCAT(CONCAT(l.revised_weight,' ',l.revised_length,'x',l.revised_width,'x',revised_height)) AS revised, 
                            GROUP_CONCAT(DISTINCT l.quantity) AS quantity,
                            GROUP_CONCAT(DISTINCT l.revised_quantity) AS revised_quantity,
                            GROUP_CONCAT(DISTINCT l.id) AS external_load_id, 
                            ROUND(cf_928,2) AS shipment_km,
                            ROUND(SUM(DISTINCT l.m3),2) AS m3, a.accountid, sa.salesorderid, bill_code,ship_code,
                            CASE WHEN cargo_measure = 'pll' THEN l.quantity ELSE 0 END AS  pll, sa.external_order_id,bill_country,ship_country
                                    FROM vtiger_salesorder sa
                                    LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=sa.salesorderid
                                    LEFT JOIN vtiger_account a ON a.accountid = sa.accountid 
                                    LEFT JOIN vtiger_crmentity e ON e.crmid = sa.salesorderid     
                                    LEFT JOIN vtiger_inventoryproductrel l ON l.id = sa.salesorderid    
                                    LEFT JOIN vtiger_sobillads b ON b.sobilladdressid=sa.salesorderid   
                                    LEFT JOIN vtiger_soshipads s ON s.soshipaddressid=sa.salesorderid   
                                    WHERE  e.deleted = 0 AND  DATE_FORMAT(e.createdtime, '%Y-%m-%d %H') = '$today'  AND cf_1297 LIKE 'BAZINISLT%' AND e.source != 'crm'                   
                                    GROUP BY sa.salesorderid");



    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                            WHERE salesorderid = ?');                                            
                                  

    $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ?  WHERE id = ? AND productid = 14244'); 
    $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = ?, cf_1365 = ?, cf_1367 = ?, cf_1374 = ? WHERE   salesorderid = ? '); 
    $sth5 = $dbh->prepare('UPDATE vtiger_crmentity  SET modifiedtime = ? WHERE crmid = ? '); 
    
    $sth9 = $dbh->prepare('INSERT INTO app_taxable_dimensions (salesorderid,type) VALUES (?,?)');
 
    $sth11 = $dbh->prepare('UPDATE app_taxable_dimensions SET type = ? WHERE salesorderid = ?');

    $get_all_orders_in_taxable = $dbh->prepare('SELECT salesorderid FROM app_taxable_dimensions');

    $get_all_orders_in_taxable->setFetchMode(PDO::FETCH_ASSOC);
    $get_all_orders_in_taxable->execute(array());

    $check_record = [];

    foreach($get_all_orders_in_taxable AS $value) {   
        $check_record[$value['salesorderid']] = $value['salesorderid'];
    }


    $set_session_wait_time = $dbh->prepare("SET SESSION innodb_lock_wait_timeout = 900");
    $set_session_wait_time->execute(array());

    while ($row = $sth->fetch()) {          
        

        $id = $row['accountid'];
        $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
        $row['load_post_code'] = preg_replace("/[^0-9]/", "",$row['load_post_code']);
        $row['unload_post_code'] = preg_replace("/[^0-9]/", "",$row['unload_post_code']);
		$pll = $row['pll'];
        $date = date("Y-m-d");
        $date2 = date("Y-m-d H:i:s");
        $logfile = "count_price_$date.log";
        $quantity = explode(",", $row['quantity']);
        $revised_quantity = explode(",", $row['revised_quantity']);
        $external_load_id = explode(",", $row['external_load_id']);    
        $weight_ordered = array();
        $dim_ordered = array();
        $dim_ordered2 = array();
        $square_ordered = array();
        $square_ordered_temp = array();
        $weight_revised = array();
        $dim_revised = array();
        $dim_revised2 = array();
        $square_revised = array();
        $square_revised_temp = array();



        $ordered = explode(",",$row['ordered']); 
   
        for($i = 0; $i < count($ordered); $i++){
           $exploded_arr = explode(' ',$ordered[$i]);
           $weight_ordered[] = $exploded_arr[0];         
           $dim_ordered[] = array_product(explode('x', $exploded_arr[1])) * $quantity[$i];     
           $dim_ordered2[] = explode('x', $exploded_arr[1]); 
           $square_ordered_temp[] = explode('x', $exploded_arr[1]); 
           unset($square_ordered_temp[$i][2]);           
           $square_ordered[] = array_product($square_ordered_temp[$i]) * $quantity[$i];
        }

        $revised = explode(",",$row['revised']);
        
        for($i = 0; $i < count($revised); $i++){
           $exploded_arr2 = explode(' ',$revised[$i]);
           $weight_revised[] = $exploded_arr2[0];
        //    $dim_revised[] = array_product(explode('x', $exploded_arr2[1])) * $quantity[$i];  
           $dim_revised2[] = explode('x', $exploded_arr2[1]);
           $square_revised_temp[] = explode('x', $exploded_arr2[1]); 
           unset($square_revised_temp[$i][2]);
           $square_revised[] = array_product($square_revised_temp[$i]) * $revised_quantity[$i];         
        }

 
        $ordered_cargo_wgt = $exploded_arr[0];
        $revised_cargo_wgt = $exploded_arr2[0];

        $ordered_dim = explode('x', $exploded_arr[1]);
        $revised_dim = explode('x', $exploded_arr2[1]);

        $ordered_w = array_sum($weight_ordered);
        $ordered_m3 = ROUND(array_sum($dim_ordered),2);
        $ordered_m2 = array_sum($square_ordered);

        $revised_w = array_sum($weight_revised);
        // $revised_m3 = array_sum($dim_revised);
        $revised_m3 = $row['m3'];
        $revised_m2 = array_sum($square_revised);   
  
        $getPrice = array();
        if(!empty($row['bill_code']) AND !empty($row['ship_code']) AND (!empty($ordered_w) OR !empty($revised_w)) AND ($row['bill_country'] != "EST" AND $row['ship_country'] != "EST") ){

            if($ordered_w == $revised_w && $ordered_m3 == $revised_m3){
                $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'], 'CLIENT'); 
                
                if($getPrice_ordered['price'] == 0){
                    $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS');
                }

                $getPrice['price'] = $getPrice_ordered['price'];
                $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];
                $getPrice['type'] = 'revised';
                // updateCargo($dbh, $weight_revised,$dim_revised2,$row['salesorderid'],$external_load_id);
            }else{
                $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'CLIENT');  
                $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$id,$revised_w,$revised_m3, $revised_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'CLIENT'); 

                if($getPrice_ordered['price'] == 0 && $getPrice_revised['price'] == 0){
                    $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS');
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$id,$revised_w,$revised_m3, $revised_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS'); 
                }


              
                if($getPrice_ordered['price'] > $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_ordered['price'];
                    $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                    $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];
                    $getPrice['type'] = 'ordered';
                    // updateCargo($dbh, $weight_ordered,$dim_ordered2,$row['salesorderid'],$external_load_id);

                }elseif($getPrice_ordered['price'] < $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_revised['price'];
                    $getPrice['pricebook'] =  $getPrice_revised['combination'];
                    $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                    $getPrice['type'] = 'revised';
                    // updateCargo($dbh, $weight_revised,$dim_revised2,$row['salesorderid'],$external_load_id);

                }elseif($getPrice_ordered['price'] == $getPrice_revised['price']){                   
                        $getPrice['price'] = $getPrice_revised['price'];
                        $getPrice['pricebook'] =  $getPrice_revised['combination'];
                        $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                        $getPrice['type'] = 'revised';
                        // updateCargo($dbh, $weight_revised,$dim_revised2,$row['salesorderid'],$external_load_id);   
                }

            }     
          
            
        
    
        }else{ 
            $getPrice = array('price' => 0);

            if(empty($row['bill_code']) AND empty( $row['ship_code']) AND !empty($ordered_w) AND !empty($revised_w)){
                $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
            }elseif(empty($row['bill_code']) AND empty($row['ship_code'])){    
                $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
            }elseif(empty($row['bill_code'])){
                $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
            }elseif(empty($row['ship_code'])){
                $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
            }elseif(empty($ordered_w) AND empty($revised_w)){
                    $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
            }elseif($row['bill_code'] == "EST" OR $row['ship_code'] == "EST"){
                $getPrice = array('pricebook' => 'Estija','price' => 0);
             }
           
        }   
        
        if(empty($check_record[$row['salesorderid']])){
            $sth9->execute(array($row['salesorderid'],$getPrice['type']));
           
        }else{
            $sth11->execute(array($getPrice['type'],$row['salesorderid']));
        }

       if(empty($row['price_agreed'])){
            $price = $getPrice['price'];  
            $sth2->execute(array(
               $price,
               $price,                                                     
               $price,                                      
               $row['salesorderid']
            ));   
        }else{
            $price = $row['price_agreed'];
            $sth2->execute(array(
               $price,
               $price,                                                     
               $price,                                      
               $row['salesorderid']
            ));   
        } 

          $sth3->execute(array(                       
            $getPrice['pricebook'],
            $row['m3'],
            $ordered_m3,
            $getPrice['price'],       
            $row['salesorderid']          
        )); 

        
   	$margin = $getPrice['price'];

    if(!file_exists("logs/count_if_bazinis/$date")){
            mkdir("logs/count_if_bazinis/$date", 0777, true); 
      }
         
      $time = date("H:").$minutes;
      $dir = "count_if_bazinis/$date";
      $logfile = "$dir/$time.log";
      customers_log('Uzsakymas: '.$row['salesorderid'].' kaina '.$margin.' kainynas '.$getPrice['pricebook'], $logfile);  
       
                    
                    $sth4->execute(array(                                                                                          
                                        $margin,                                                                                                  
                                        $row['salesorderid']
                    ));

                    $sth5->execute(array(                                                                                          
                                        $date2,                                                                        
                                        $row['salesorderid']
                    ));
                   
                 
                
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    // sendReportMail($e->getMessage(),basename(__FILE__));
}


function updateCargo($dbh, $weight,$dim,$salesorderid,$external_load_id){
    $sth = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET cargo_wgt = ?, cargo_length = ?, cargo_width = ?, cargo_height = ?  WHERE id = ? AND external_load_id = ? AND productid = 14244');

    for($e = 0; $e < COUNT($weight); $e++){
        $sth->execute(array(                                                                                          
            $weight[$e],
            $dim[$e][0],
            $dim[$e][1],
            $dim[$e][2],
            $salesorderid,
            $external_load_id[$e] 
        ));
    }
}

?>
