<?php
include '../../ws.config.php';
global $config;
require '../../utils.php';
include '../../getMeterPriceFromPriceBook.php';
set_time_limit(900);

error_reporting(0) ;
$minutes = change_minutes(date("i"));

try {

    require('../../mysql_connection.php');

    $dbh->beginTransaction();    

                   

    $sth = $dbh->prepare("SELECT t.*, 
                            GROUP_CONCAT(DISTINCT l.ordered) AS ordered, 
                            GROUP_CONCAT(DISTINCT l.revised) AS revised,                        
                            GROUP_CONCAT(DISTINCT l.id) AS external_load_id,a.accountid, c.salesorderid, load_country_code,unload_country_code,l.quantity, l.quantity_revised
                                    FROM app_orders_change_log t
                                    JOIN vtiger_account a ON a.customer_id = t.customer_id 
                                    JOIN vtiger_crmentity e ON e.crmid = a.accountid       
                                    JOIN app_loads_change_log l ON l.order_id = t.id           
                                    JOIN vtiger_salesorder c ON c.external_order_id = t.id       
                                    WHERE e.deleted = 0 AND l.meters IS NOT NULL 
                                    GROUP BY l.order_id");



    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                            WHERE salesorderid = ?');


     $sth3 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET meters = ?  WHERE id = ? AND external_load_id = ? AND productid = 14244'); 
     $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ?  WHERE id = ? AND productid = 14244'); 
     $sth5 = $dbh->prepare('UPDATE app_loads_change_log SET meters = ? WHERE id = ?');


    while ($row = $sth->fetch()) {        
        $id = $row['accountid'];     
        $row['load_post_code'] = preg_replace("/[^0-9]/", "",$row['load_post_code']);
        $row['unload_post_code'] = preg_replace("/[^0-9]/", "",$row['unload_post_code']);
        $date = date("Y-m-d");
        $date2 = date("Y-m-d H:i:s");
        $logfile = "count_price_$date.log";       

        $external_load_id = explode(",", $row['external_load_id']);       

        $weight_ordered = array();
        $dim_ordered = array();
        $dim_ordered2 = array();
        $square_ordered = array();
        $square_ordered_temp = array();
        $weight_revised = array();
        $dim_revised = array();
        $dim_revised2 = array();
        $square_revised = array();
        $square_revised_temp = array();

        $ordered = explode(",",$row['ordered']); 
   
        for($i = 0; $i < count($ordered); $i++){
           $exploded_arr = explode(' ',$ordered[$i]);         
           $dim_ordered2[] = max(explode('x', $exploded_arr[1])); 
        }

        $revised = explode(",",$row['revised']);
        
        for($i = 0; $i < count($revised); $i++){
           $exploded_arr2 = explode(' ',$revised[$i]);        
           $dim_revised2[] = max(explode('x', $exploded_arr2[1]));            
        }   
 
    
        $ordered_meters = $dim_ordered2;
        $revised_meters = $dim_revised2;      

        $count = (max($ordered_meters) > max($revised_meters) ? $ordered_meters : $revised_meters);
        $price_array = array();
     
  
    if(!empty($row['load_post_code']) AND !empty($row['unload_post_code']) AND  ($row['load_country_code'] != "EST" AND $row['unload_country_code'] != "EST") ){

        for($i=0; $i < count($count); $i++){
            if($ordered_meters[$i] > $revised_meters[$i]){
                $getPrice = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$ordered_meters[$i],$row['load_country_code'],$row['unload_country_code']);  
                $quantity = $row['quantity'];
            }else{
                $getPrice = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_meters[$i],$row['load_country_code'],$row['unload_country_code']);  
                $quantity = $row['quantity_revised']; 
            }  
            
            $price_array[] = $getPrice['price'];
        }   


        $getPrice['price'] = array_sum($price_array);        
        
    
            $margin = $getPrice['price']  * $quantity;



         if(!file_exists("../../logs/insert_meter/$date")){
            mkdir("../../logs/insert_meter/$date", 0777, true); 
          }
             
          $time = date("H:").$minutes;
          $dir = "insert_meter/$date";
          $logfile = "$dir/$time.log";
          customers_log('Uzsakymas: '.$row['salesorderid'].' kaina '.$margin, $logfile); 



            $sth2->execute(array(
                                $margin,
                                $margin,                                                     
                                $margin,                                      
                                $row['salesorderid']
            )); 
            
            
                for($e = 0; $e < COUNT($external_load_id); $e++){
                    $true = 1;
                    if($price_array[$e] == 0){                    
                        $true = NULL;  
                    }
             
                    $sth5->execute(array($true,$external_load_id[$e])); 
                    $sth3->execute(array(                                                                                          
                        $true,                      
                        $row['salesorderid'],
                        $external_load_id[$e] 
                    ));
                }
            
            
            
            $sth4->execute(array(                                                                                          
                $margin,                                                                                                  
                $row['salesorderid']
            ));



    }    
             
     
    }
             
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
