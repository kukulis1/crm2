<?php
include '../../ws.config.php';
global $config;
require '../../utils.php';
include '../../sendPriceToMetrika.php';
include '../../getPriceFromPriceBook.php';
set_time_limit(900);

error_reporting(0);
$minutes = change_minutes(date("i"));
try {

    require('../../mysql_connection.php');

    $dbh->beginTransaction();    


    $sth = $dbh->prepare("SELECT 
    (SELECT GROUP_CONCAT(CONCAT(l.ordered_weight,' ',l.ordered_length,'x',l.ordered_width,'x',ordered_height)) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS ordered, 
    (SELECT GROUP_CONCAT(CONCAT(l.revised_weight,' ',l.revised_length,'x',l.revised_width,'x',revised_height)) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS revised, 
    (SELECT GROUP_CONCAT(l.revised_quantity) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS quantity,  
    (SELECT GROUP_CONCAT(l.quantity) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS ordered_quantity,
    (SELECT GROUP_CONCAT(l.external_load_id) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid GROUP BY id) AS external_load_id,  
                              
                                 ROUND((SELECT SUM(l.m3) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id),2) AS m3,  
                                 CASE WHEN measure_revised = 'pll' THEN (SELECT SUM(l.revised_quantity) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) ELSE 0 END AS revised_pll,                 
                                 CASE WHEN measure = 'pll' THEN (SELECT SUM(l.quantity) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) ELSE 0 END AS pll, c.salesorderid, cf.cf_928 AS shipment_km, bill_code as load_post_code, ship_code as unload_post_code, so.bill_country AS load_country_code, sh.ship_country AS unload_country_code,(SELECT price_agreed FROM app_orders_update lu WHERE lu.id=t.shipment_id  LIMIT 1) AS price_agreed, c.shipment_code, a.accountid ,c.external_order_id AS id
                                        FROM app_loads_update_without_order t                                            
                                        JOIN vtiger_salesorder c ON c.external_order_id = t.shipment_id                            
                                        LEFT JOIN vtiger_account a ON a.accountid = c.accountid   
                                        LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid = c.salesorderid       
                                        LEFT JOIN vtiger_sobillads so ON so.sobilladdressid=c.salesorderid  
                                        LEFT JOIN vtiger_soshipads sh ON sh.soshipaddressid=c.salesorderid                                  
                                        WHERE e.deleted = 0 -- AND t.meters IS NULL 
                                        AND (cf_2663 = '' OR cf_2663 IS NULL) (cf_1376 = '' OR cf_1376 IS NULL)
                                        GROUP BY t.shipment_id
                                        ORDER BY t.app_id ASC");



    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = total+?,
                                                        subtotal = subtotal+?,                                                         
                                                        pre_tax_total = pre_tax_total+?                                                     
                                            WHERE salesorderid = ?');

    $sth6 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                                        WHERE salesorderid = ?');                                          
    $sth7 = $dbh->prepare("SELECT meters FROM app_loads_update WHERE id = ? AND meters = 1");
    $sth7->setFetchMode(PDO::FETCH_ASSOC);
    

    $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = ?, cf_1365 = ?, cf_1367 = ?, cf_1374 = ?,  cf_1376 = ? WHERE   salesorderid = ? '); 
    $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = margin+? WHERE id = ? AND productid = 14244'); 
    $sth8 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ? WHERE id = ? AND productid = 14244'); 
    $sth5 = $dbh->prepare('UPDATE vtiger_crmentity  SET modifiedtime = ? WHERE crmid = ? '); 

    $sth9 = $dbh->prepare('INSERT INTO app_taxable_dimensions (salesorderid,type) VALUES (?,?)');   
    $sth11 = $dbh->prepare('UPDATE app_taxable_dimensions SET type = ? WHERE salesorderid = ?');

    $sth12 = $dbh->prepare('UPDATE vtiger_salesorder_updated SET status = ?, update_time = ? WHERE salesorderid = ?');


    $insert_log = $dbh->prepare('INSERT INTO vtiger_salesorder_dimensions_log (salesorderid, amount, pricebook, kg, m3, m2, pll, kg_revised, m3_revised, m2_revised, pll_revised, which, importdate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)');

    $get_all_orders_in_taxable = $dbh->prepare('SELECT vtiger_salesorder.salesorderid 
                                                FROM app_loads_update t                                            
                                                JOIN vtiger_salesorder ON vtiger_salesorder.external_order_id = t.shipment_id  
                                                JOIN app_taxable_dimensions ON app_taxable_dimensions.salesorderid=vtiger_salesorder.salesorderid                             
                                                GROUP BY vtiger_salesorder.salesorderid');

    $get_all_orders_in_taxable->setFetchMode(PDO::FETCH_ASSOC);
    $get_all_orders_in_taxable->execute(array());

    $check_record = [];

    foreach($get_all_orders_in_taxable AS $value) {   
        $check_record[$value['salesorderid']] = $value['salesorderid'];
    }

    $set_session_wait_time = $dbh->prepare("SET SESSION innodb_lock_wait_timeout = 900");
    $set_session_wait_time->execute(array());

    $date = date("Y-m-d");  

    while ($row = $sth->fetch()) {      
        $id = $row['accountid'];     
        $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
        $row['load_post_code'] = preg_replace("/[^0-9]/", "",$row['load_post_code']);
        $row['unload_post_code'] = preg_replace("/[^0-9]/", "",$row['unload_post_code']);
        $pll = $row['pll'];               
        $revised_pll = $row['revised_pll'];       
        $date2 = date("Y-m-d H:i:s");
        $logfile = "count_price_$date.log";
        $external_load_id = explode(",", $row['external_load_id']);   

        $quantity = explode(",", $row['quantity']);
        $ordered_quantity = explode(",", $row['ordered_quantity']);
        

        $weight_ordered = array();
        $dim_ordered = array();
        $dim_ordered2 = array();
        $square_ordered = array();
        $square_ordered_temp = array();
        $weight_revised = array();
        $dim_revised = array();
        $dim_revised2 = array();
        $square_revised = array();
        $square_revised_temp = array();


        $ordered = explode(",",$row['ordered']);  
        
        for($i = 0; $i < count($ordered); $i++){
            $exploded_arr = explode(' ',$ordered[$i]);
            $weight_ordered[] = $exploded_arr[0];         
            $dim_ordered[] = array_product(explode('x', $exploded_arr[1])) * $ordered_quantity[$i];     
            $dim_ordered2[] = explode('x', $exploded_arr[1]); 
            $square_ordered_temp[] = explode('x', $exploded_arr[1]); 
            unset($square_ordered_temp[$i][2]);           
            $square_ordered[] = array_product($square_ordered_temp[$i]) * $ordered_quantity[$i];
         }
 
         $revised = explode(",",$row['revised']);
         
         for($i = 0; $i < count($revised); $i++){
            $exploded_arr2 = explode(' ',$revised[$i]);
            $weight_revised[] = $exploded_arr2[0];
            // $dim_revised[] = array_product(explode('x', $exploded_arr2[1])) * $quantity[$i];  
            $dim_revised2[] = explode('x', $exploded_arr2[1]);
            $square_revised_temp[] = explode('x', $exploded_arr2[1]); 
            unset($square_revised_temp[$i][2]);
            $square_revised[] = array_product($square_revised_temp[$i]) * $quantity[$i];         
         }
 
  
         $ordered_cargo_wgt = $exploded_arr[0];
         $revised_cargo_wgt = $exploded_arr2[0];
 
         $ordered_dim = explode('x', $exploded_arr[1]);
         $revised_dim = explode('x', $exploded_arr2[1]);
 
         $ordered_w = array_sum($weight_ordered);
         $ordered_m3 = array_sum($dim_ordered);
         $ordered_m2 = array_sum($square_ordered);
 
         $revised_w = array_sum($weight_revised);
        //  $revised_m3 = array_sum($dim_revised);
         $revised_m3 = $row['m3'];
         $revised_m2 = array_sum($square_revised);


 
         $getPrice = array();
         $getPrice_revised = array();
         $getPrice_ordered = array();
         if(!empty($row['load_post_code']) AND !empty($row['unload_post_code']) AND (!empty($ordered_w) OR !empty($revised_w)) AND ($row['load_country_code'] != "EST" AND $row['unload_country_code'] != "EST") ){

            if($ordered_w == $revised_w && $ordered_m3 == $revised_m3){
                $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['load_country_code'],$row['unload_country_code'], 'CLIENT'); 
                
                if($getPrice_revised['price'] == 0){
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['load_country_code'],$row['unload_country_code'],'BAZINIS');
                }

                $getPrice['price'] = $getPrice_revised['price'];
                $getPrice['pricebook'] =  $getPrice_revised['combination'];
                $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];                
                $getPrice['type'] = 'fail';
                if($getPrice_revised['price'] > 0){
                    $getPrice['type'] = 'revised';
                }
            }else{
                $getPrice_ordered = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'],'CLIENT');  
                $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['load_country_code'],$row['unload_country_code'],'CLIENT'); 

                if($getPrice_ordered['price'] == 0 && $getPrice_revised['price'] == 0){
                    $getPrice_ordered = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['load_country_code'],$row['unload_country_code'],'BAZINIS');
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['load_country_code'],$row['unload_country_code'],'BAZINIS'); 
                }

  
                if($getPrice_ordered['price'] > $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_ordered['price'];
                    $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                    $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];              
                    $getPrice['type'] = 'fail';
                    if($getPrice_revised['price'] > 0){
                     $getPrice['type'] = 'ordered';
                    }
                }elseif($getPrice_ordered['price'] < $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_revised['price'];
                    $getPrice['pricebook'] =  $getPrice_revised['combination'];
                    $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];                    
                    $getPrice['type'] = 'fail';
                    if($getPrice_revised['price'] > 0){
                        $getPrice['type'] = 'revised';
                    }

                }elseif($getPrice_ordered['price'] == $getPrice_revised['price']){                   
                        $getPrice['price'] = $getPrice_revised['price'];
                        $getPrice['pricebook'] =  $getPrice_revised['combination'];
                        $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];                        
                        $getPrice['type'] = 'fail';
                        if($getPrice_revised['price'] > 0){
                            $getPrice['type'] = 'revised';
                        }                   
                }

            }          

    
        }else{ 
            $getPrice = array('price' => 0);

            if(empty($row['load_post_code']) AND empty( $row['unload_post_code']) AND !empty($ordered_w) AND !empty($revised_w)){
                $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
            }elseif(empty($row['load_post_code']) AND empty($row['unload_post_code'])){    
                $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
            }elseif(empty($row['load_post_code'])){
                $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
            }elseif(empty($row['unload_post_code'])){
                $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
            }elseif(empty($ordered_w) AND empty($revised_w)){
                    $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
            }elseif($row['load_country_code'] == "EST" OR $row['unload_country_code'] == "EST"){
                $getPrice = array('pricebook' => 'Estija','price' => 0);
             }

             $getPrice['type'] = '';
           
        }          
 
        $sth7->execute(array($row['id']));

        if(empty($check_record[$row['salesorderid']])){
            $sth9->execute(array($row['salesorderid'],$getPrice['type']));           
        }else{
            $sth11->execute(array($getPrice['type'],$row['salesorderid']));
        }
        
        $check = $sth7->fetch();

        if(empty($row['cf_2663'])){

            if(!empty($check['meters'])){
                $price = $getPrice['price'];    
                $sth2->execute(array(
                $price,
                $price,                                                     
                $price,                                      
                $row['salesorderid']
            ));                       
            }elseif(empty($row['price_agreed'])){
                $price = $getPrice['price'];  
                $sth6->execute(array(
                $price,
                $price,                                                     
                $price,                                      
                $row['salesorderid']
                ));   
            }else{
                $price = $row['price_agreed'];
                $sth6->execute(array(
                $price,
                $price,                                                     
                $price,                                      
                $row['salesorderid']
                ));   
            }      
        }   

        $margin = $getPrice['price']; 

        if(!file_exists("../../logs/update/$date")){
          mkdir("../../logs/update/$date", 0777, true); 
        }
           
        $time = date("H:").$minutes;
        $dir = "update/$date";
        $logfile = "$dir/$time.log";
        customers_log('Uzsakymas: '.$row['salesorderid'].' kaina '.$margin.' kainynas '.$getPrice['pricebook'], $logfile);  


 
        $sth8->execute(array(                                                                                          
            $margin,                                                                                     
            $row['salesorderid']
        ));
     
        $sth3->execute(array(                       
            $getPrice['pricebook'],
            $row['m3'],
            $ordered_m3,
            $getPrice['price'],
            $row['price_agreed'],
            $row['salesorderid']          
        )); 
 

        $sth5->execute(array(                                                                                          
                            $date2,                                                                        
                            $row['salesorderid']
        ));

        $sth12->execute(array(  
            1,                                                                                        
            $date2,                                                                        
            $row['salesorderid']
        ));
        // sendPriceToMetrikaCron($row['id'], $margin, $row['price_agreed'], $margin,$dbh);
        update_entity($dbh, $row['salesorderid'],null,$row['shipment_code'],$date2);

        $insert_log->execute(array($row['salesorderid'],
                                    $margin,
                                    $getPrice['pricebook'],
                                    $ordered_w,
                                    $ordered_m3,
                                    $ordered_m2,
                                    $pll,
                                    $revised_w,
                                    $revised_m3,
                                    $revised_m2,
                                    $revised_pll,
                                    'UPDATE 5',
                                    $date2
                                    ));
                   
                 
                
    }
       customers_log('Pasibaige kainu perskaiciavimas', $logfile);       
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    // sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
