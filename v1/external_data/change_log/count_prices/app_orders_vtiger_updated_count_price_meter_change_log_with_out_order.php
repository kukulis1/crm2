<?php
include '../../ws.config.php';
global $config;
require '../../utils.php';
include '../../sendPriceToMetrika.php';
include '../../getMeterPriceFromPriceBook.php';
set_time_limit(900);

error_reporting(0);
$minutes = change_minutes(date("i"));

try {

    require('../../mysql_connection.php');

    $dbh->beginTransaction();
    
    $sth = $dbh->prepare("SELECT 
    (SELECT GROUP_CONCAT(CONCAT(l.ordered_weight,' ',l.ordered_length,'x',l.ordered_width,'x',ordered_height)) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS ordered, 
    (SELECT GROUP_CONCAT(CONCAT(l.revised_weight,' ',l.revised_length,'x',l.revised_width,'x',revised_height)) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS revised, 
    (SELECT GROUP_CONCAT(l.revised_quantity) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS quantity,  
    (SELECT GROUP_CONCAT(l.quantity) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS ordered_quantity,
    (SELECT GROUP_CONCAT(l.external_load_id) FROM vtiger_inventoryproductrel l WHERE l.id=c.salesorderid AND productid = 14244 GROUP BY id) AS external_load_id, c.salesorderid, cf.cf_928 AS shipment_km, bill_code as load_post_code, ship_code as unload_post_code, so.bill_country AS load_country_code, sh.ship_country AS unload_country_code, a.accountid, c.external_order_id AS id
                                        FROM app_loads_update_without_order t                                            
                                        JOIN vtiger_salesorder c ON c.external_order_id = t.shipment_id                            
                                        LEFT JOIN vtiger_account a ON a.accountid = c.accountid   
                                        LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid = c.salesorderid       
                                        LEFT JOIN vtiger_sobillads so ON so.sobilladdressid=c.salesorderid  
                                        LEFT JOIN vtiger_soshipads sh ON sh.soshipaddressid=c.salesorderid                                  
                                        WHERE e.deleted = 0 AND t.meters IS NOT NULL AND (cf_2663 = '' OR cf_2663 IS NULL)
                                        GROUP BY t.shipment_id
                                        ORDER BY t.app_id ASC");

                


    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                            WHERE salesorderid = ?');


     $sth3 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET meters = ?  WHERE id = ? AND external_load_id = ? AND productid = 14244'); 
     $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ?  WHERE id = ? AND productid = 14244'); 
     $sth5 = $dbh->prepare('UPDATE app_loads_update_without_order SET meters = ? WHERE id = ?');


    while ($row = $sth->fetch()) {        
        $id = $row['accountid'];     
        $row['load_post_code'] = preg_replace("/[^0-9]/", "",$row['load_post_code']);
        $row['unload_post_code'] = preg_replace("/[^0-9]/", "",$row['unload_post_code']);
		
        $date = date("Y-m-d");
        $date2 = date("Y-m-d H:i:s");
        $logfile = "count_price_$date.log";
  

        $external_load_id = explode(",", $row['external_load_id']);         

        $weight_ordered = array();
        $dim_ordered = array();
        $dim_ordered2 = array();
        $square_ordered = array();
        $square_ordered_temp = array();
        $weight_revised = array();
        $dim_revised = array();
        $dim_revised2 = array();
        $square_revised = array();
        $square_revised_temp = array();

        $ordered = explode(",",$row['ordered']); 
   
        for($i = 0; $i < count($ordered); $i++){
           $exploded_arr = explode(' ',$ordered[$i]);         
           $dim_ordered2[] = max(explode('x', $exploded_arr[1])); 
        }

        $revised = explode(",",$row['revised']);
        
        for($i = 0; $i < count($revised); $i++){
           $exploded_arr2 = explode(' ',$revised[$i]);        
           $dim_revised2[] = max(explode('x', $exploded_arr2[1]));            
        }   
 
    
        $ordered_meters = $dim_ordered2;
        $revised_meters = $dim_revised2;      

        $count = (max($ordered_meters) > max($revised_meters) ? $ordered_meters : $revised_meters);
        $price_array = array();
     
  
    if(!empty($row['load_post_code']) AND !empty($row['unload_post_code']) AND  ($row['load_country_code'] != "EST" AND $row['unload_country_code'] != "EST") ){

        for($i=0; $i < count($count); $i++){
            if($ordered_meters[$i] > $revised_meters[$i]){
                $getPrice = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$ordered_meters[$i],$row['load_country_code'],$row['unload_country_code']);  
            }else{
                $getPrice = getPriceFromPriceBook($dbh, $row['load_post_code'],$row['unload_post_code'],$id,$revised_meters[$i],$row['load_country_code'],$row['unload_country_code']);  
            }  
            
            $price_array[] = $getPrice['price'];
        }          


        $getPrice['price'] = array_sum($price_array);        
        
    
        $margin = $getPrice['price'] * $row['quantity'];



        if(!file_exists("../../logs/update_meter/$date")){
            mkdir("../../logs/update_meter/$date", 0777, true); 
        }
             
        $time = date("H:").$minutes;
          $dir = "update_meter/$date";
          $logfile = "$dir/$time.log";
          customers_log('Uzsakymas: '.$row['salesorderid'].' kaina '.$margin, $logfile); 


            $sth2->execute(array(
                                $margin,
                                $margin,                                                     
                                $margin,                                      
                                $row['salesorderid']
            )); 
                     
                for($e = 0; $e < COUNT($external_load_id); $e++){
                    $true = 1;
                    if($price_array[$e] == 0){                    
                        $true = NULL;  
                    }                  
             
                    $sth5->execute(array($true,$external_load_id[$e])); 
                    $sth3->execute(array(                                                                                          
                        $true,                      
                        $row['salesorderid'],
                        $external_load_id[$e] 
                    ));
                }
            
            
            
            $sth4->execute(array(                                                                                          
                $margin,                                                                                                  
                $row['salesorderid']
            ));

            // sendPriceToMetrikaCron($row['id'], $margin, $row['price_agreed'], $margin,$dbh); 

    }    
             
     
    }
             
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
