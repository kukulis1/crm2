<?php
set_time_limit(900);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {

      require('../mysql_connection.php');

      $TRUNCATE1 = $dbh->prepare('TRUNCATE app_orders_handle');
      $TRUNCATE1->execute(array());      
        
      $TRUNCATE2 = $dbh->prepare('TRUNCATE app_loads_handle');
      $TRUNCATE2->execute(array());

      $insert = $dbh->prepare("INSERT INTO app_shipments_deleted_in_metrika (shipment_id, shipment_code, import_date) VALUES (?,?,?)");
      $insert_log = $dbh->prepare("INSERT INTO app_check_all_day_shipments (shipment_id, shipment_code, import_date) VALUES (?,?,?)");
      $insert_report = $dbh->prepare("INSERT INTO app_lost_shipments_report (shipment_id, shipment_code, import_date) VALUES (?,?,?)");

      $update = $dbh->prepare("UPDATE vtiger_crmentity SET deleted = 1 WHERE crmid = ?");

      $two_weeks_shipments = $dbh->prepare("SELECT external_order_id,salesorderid,shipment_code,DATE_FORMAT(createdtime,'%Y-%m-%d') AS date 
                                            FROM vtiger_salesorder s 
                                            INNER JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
                                            WHERE deleted = 0 
                                                AND DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN ? AND ? 
                                                AND external_order_id != '' 
                                                AND external_order_id IS NOT NULL 
                                                AND external_order_id != 1");



      $date_from = date('Y-m-d',strtotime("-14 days"));
      $date_to = date('Y-m-d',strtotime("-1 days"));

      $date_time = date("Y-m-d H:i:s");

  
      $two_weeks_shipments->setFetchMode(PDO::FETCH_ASSOC);
      $two_weeks_shipments->execute(array($date_from,$date_to));
      

      $crm_shipments = [];
      foreach ($two_weeks_shipments as $value) {
        $crm_shipments[$value['date']][$value['external_order_id']] = array('salesorderid' => $value['salesorderid'], 'shipment_code' => $value['shipment_code']);
      }
   

      $deleted_shipments = [];
      $deleted_shipments_shipment_code = [];

      $lost_shipments = [];

      $metrika_shipments = [];
  

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/all_period_orders.php");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from,'date_to' => $date_to,'by_date' => 1));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 900);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
      $result = curl_exec($ch);
      curl_close($ch);   
      $data = json_decode($result,true); 

      if($data['state'] == 'SUCCESS'){
        foreach ($data['response']['shipments'] as $shipment_date => $shipment_code) {
          $metrika_shipments[$shipment_date] = $shipment_code;            
        }  

        foreach ($crm_shipments AS $date => $value) {       
          foreach($value AS $external_order_id => $info){         
            if(empty($metrika_shipments[$date][$external_order_id])){
              $deleted_shipments[] = $info['salesorderid'];
              $deleted_shipments_shipment_code[$info['salesorderid']] = $info['shipment_code'];                
            }      
          }        
        }
  
        foreach ($metrika_shipments AS $date => $value) {       
          foreach($value AS $external_order_id => $info){  
            if(empty($crm_shipments[$date][$external_order_id])){
              $lost_shipments[] = $info;     
              $insert_log->execute(array($external_order_id,$code,$date_time)); 
              $insert_report->execute(array($external_order_id,$info,$date_time));          
            }      
          }        
        }   
  

        foreach($deleted_shipments AS $id){          
            $code =  $deleted_shipments_shipment_code[$id];
            if(checkShipmentCode($code) == null){
              $insert->execute(array($id,$code,$date_time));                       
              $update->execute(array($id));
            }
        }  
        
        
        $shipment_code = "'" .implode("', '",$lost_shipments). "'";                          
        getLostShipments($dbh,$shipment_code);


      }else{
        $date = date("Y-m-d");
        if(!file_exists("../logs/night_check/$date")){
          mkdir("../logs/night_check/$date", 0777, true); 
        }   
           
        $logfile = "night_check/$date/checked.log";
        
        customers_log($result, $logfile);  
      } 


    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }


    function checkShipmentCode($shipment_code){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas',  'shipment_code' => "'".$shipment_code."'"));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
      $result = curl_exec($ch);
      curl_close($ch);   

      $data = json_decode($result,true); 
      return $data['order']; 
    }

    function getLostShipments($dbh,$shipment_code){         

          $insert_orders = $dbh->prepare('INSERT IGNORE INTO app_orders_handle  (
            id,
            shipment_type,
            shipment_code,
            customer_id,
            order_date,
            status,
            route_point_id,
            price,
            price_agreed,
            load_company_code,
            load_company_name,
            load_company_contact,
            load_company_phone,                                                          
            load_address,
            load_settlement,
            load_municipality,
            load_post_code,
            load_country_code,
            load_date_from,
            load_date_to,
            unload_company_code,
            unload_company_name,
            unload_company_contact,
            unload_company_phone,                                                          
            unload_address,
            unload_settlement,
            unload_municipality,
            unload_post_code,
            unload_country_code,
            unload_date_from,
            unload_date_to,
            update_metrika,
            import_date,
            route_code,
            driver1_name,
            vehicle_registration_number
          ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

        $insert_loads = $dbh->prepare('INSERT IGNORE INTO app_loads_handle ( order_id,
              id,
              quantity,
              revised_quantity,
              measure,
              revised_measure,
              weight,
              length,
              width,
              height,
              m3,
              pll,
              note,
              document,
              ean,
              import_date,
              ordered,
              revised                                                       	 
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

      $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php?routes=1");
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas',  'shipment_code' => $shipment_code));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_TIMEOUT, 30);
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
          $result = curl_exec($ch);
          curl_close($ch);   

          $data2 = json_decode($result,true); 
          $state = $data2['state']; 

        

          foreach($data2['order'] as $row) {       
              $row['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$row['load_post_code']);
              $row['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$row['unload_post_code']);

              $row['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['load_post_code']);
              $row['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['unload_post_code']); 
              $date = date("Y-m-d H:i:s");

           

                                          $insert_orders->execute(array($row['id'],
                                                  $row['shipment_type'], 
                                                  $row['shipment_code'],                                                          
                                                  $row['customer_id'],
                                                  $row['order_date'],
                                                  $row['status'],
                                                  $row['route_point_id'],
                                                  $row['price'],
                                                  $row['price_agreed'],
                                                  $row['load_company_code'],
                                                  $row['load_company_name'],
                                                  $row['load_company_contact'],
                                                  $row['load_company_phone'],                                                            
                                                  $row['load_address'],
                                                  $row['load_settlement'],
                                                  $row['load_municipality'],
                                                  $row['load_post_code'],
                                                  $row['load_country_code'],
                                                  $row['load_date_from'],
                                                  $row['load_date_to'],
                                                  $row['unload_company_code'],
                                                  $row['unload_company_name'],
                                                  $row['unload_company_contact'],
                                                  $row['unload_company_phone'],                                                            
                                                  $row['unload_address'],
                                                  $row['unload_settlement'],
                                                  $row['unload_municipality'],
                                                  $row['unload_post_code'],
                                                  $row['unload_country_code'],
                                                  $row['unload_date_from'],
                                                  $row['unload_date_to'], 
                                                  $row['update_date'],                                                  
                                                  $date,
                                                  $row['route_code'],                                                            
                                                  $row['driver1_name'],
                                                  $row['vehicle_registration_number']                                                                  
                                           ));
    
                foreach($row['load'] as $row2) { 
                 
                    $ordered = array();
                    $revised = array(); 

                    if(empty($row2['ordered_weight'])) $row2['ordered_weight'] = 0;  
                    if(empty($row2['ordered_length'])) $row2['ordered_length'] = 0;  
                    if(empty($row2['ordered_width'])) $row2['ordered_width'] = 0;  
                    if(empty($row2['ordered_height'])) $row2['ordered_height'] = 0;  

                    if(empty($row2['revised_weight'])) $row2['revised_weight'] = 0;  
                    if(empty($row2['revised_length'])) $row2['revised_length'] = 0;  
                    if(empty($row2['revised_width'])) $row2['revised_width'] = 0;  
                    if(empty($row2['revised_height'])) $row2['revised_height'] = 0;  

                    $ordered = $row2['ordered_weight']." ".$row2['ordered_length']."x".$row2['ordered_width']."x".$row2['ordered_height'];
                    $revised = $row2['revised_weight']." ".$row2['revised_length']."x".$row2['revised_width']."x".$row2['revised_height'];


                    $insert_loads->execute(array($row['id'],
                                          $row2['load_id'],
                                          $row2['ordered_quantity'],
                                          $row2['revised_quantity'],
                                          $row2['ordered_measure'],
                                          $row2['revised_measure'],
                                          $row2['ordered_weight'],
                                          $row2['ordered_length'],
                                          $row2['ordered_width'],
                                          $row2['ordered_height'],
                                          $row2['m3'],
                                          $row2['pll'],
                                          $row2['note'],
                                          $row2['documents'], 
                                          $row2['ean'],                                                     
                                          $date ,
                                          $ordered,
                                          $revised                                                                                     
                                          ));
                                                                                        
                }                
                                      
          }   
          
            $ch2 = curl_init();        
            curl_setopt($ch2, CURLOPT_URL, 'https://crm.parnasas.lt/invoices/orders_insert.php');
            curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch2, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch2, CURLOPT_TIMEOUT, 6000);
            curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 900);
            curl_exec($ch2);
            curl_close($ch2); 

            $ch3 = curl_init();            
            curl_setopt($ch3, CURLOPT_URL, 'https://crm.parnasas.lt/invoices/loads_insert.php');
            curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch3, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch3, CURLOPT_TIMEOUT, 6000);
            curl_setopt($ch3, CURLOPT_CONNECTTIMEOUT, 900);
            curl_exec($ch3);
            curl_close($ch3); 



            $ch4 = curl_init();           
            curl_setopt($ch4, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/orders_count_price.php');
            curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch4, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch4, CURLOPT_TIMEOUT, 6000);
            curl_setopt($ch4, CURLOPT_CONNECTTIMEOUT, 900);
            curl_exec($ch4);
            curl_close($ch4);  

    }



