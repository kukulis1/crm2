<?php
set_time_limit(900);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {

      require('../mysql_connection.php');

      $TRUNCATE1 = $dbh->prepare('TRUNCATE app_orders_handle');
      $TRUNCATE1->execute(array());      
        
      $TRUNCATE2 = $dbh->prepare('TRUNCATE app_loads_handle');
      $TRUNCATE2->execute(array());

      $insert = $dbh->prepare("INSERT INTO app_mismatch_of_orders (shipment_id, shipment_code, importdate) VALUES (:shipment_id, :shipment_code, :importdate)");

      $getYeasterdayOrders = $dbh->prepare("SELECT external_order_id, load_date_from, unload_date_from, bill_street, ship_street, bill_city, ship_city, bill_code, ship_code,cf_1504 AS order_date
      FROM `vtiger_salesorder`                           
      LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
      LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
      LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
      INNER JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid                                                        
      WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN :date_from AND :date_to");

      $getYeasterdayLoads = $dbh->prepare("SELECT external_load_id, round(quantity) AS ordered_quantity, m.code AS ordered_measure, ordered_weight, ordered_length, ordered_width, ordered_height, revised_measure, revised_quantity, revised_weight, revised_length, revised_width, revised_height, m3 AS revised_m3
        FROM vtiger_inventoryproductrel         
        LEFT JOIN app_measures m ON m.id=cargo_measure OR m.code=cargo_measure           
        INNER JOIN vtiger_crmentity ON vtiger_inventoryproductrel.id=crmid                                                         
        WHERE DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN :date_from AND :date_to AND vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND productid = 14244 AND (external_load_id IS NOT NULL AND external_load_id != '')");

  
      $date_time = date("Y-m-d H:i:s");

      $date_from = date('Y-m-d',strtotime("-1 days"));
      $date_to = date('Y-m-d',strtotime("-1 days"));

      // $date_from = '2021-11-03';
      // $date_to = '2021-11-03';

      $getYeasterdayOrders->setFetchMode(PDO::FETCH_ASSOC);
      $getYeasterdayOrders->execute(['date_from' => $date_from, 'date_to' => $date_to]);

      $getYeasterdayLoads->setFetchMode(PDO::FETCH_ASSOC);
      $getYeasterdayLoads->execute(['date_from' => $date_from, 'date_to' => $date_to]);

      $crm_info = [];
      foreach ($getYeasterdayOrders as $value) {
        $crm_info[$value['external_order_id']] = $value;
      }

      $crm_loads = [];
      foreach ($getYeasterdayLoads as $value) {
        $crm_loads[$value['external_load_id']] = $value;
      }



      $compared_orders = [];
      $compared_loads = [];

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php");
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from,'date_to' => $date_to));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_TIMEOUT, 300);
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
          $result = curl_exec($ch);
          curl_close($ch);   
          $data = json_decode($result,true); 


          foreach ($data['order'] as $shipment) {            
            if(!empty($crm_info[$shipment['id']])){
              $load_time = explode(' ',$shipment['load_date_from']);
              $unload_time = explode(' ',$shipment['unload_date_from']);
              $order_date = explode(' ',$crm_info[$shipment['id']]['order_date']);

              if($load_time[0] != $crm_info[$shipment['id']]['load_date_from']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($unload_time[0] != $crm_info[$shipment['id']]['unload_date_from']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($shipment['load_address'] != $crm_info[$shipment['id']]['bill_street']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($shipment['unload_address'] != $crm_info[$shipment['id']]['ship_street']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($shipment['load_municipality'] != $crm_info[$shipment['id']]['bill_city']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($shipment['unload_municipality'] != $crm_info[$shipment['id']]['ship_city']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($shipment['load_post_code'] != $crm_info[$shipment['id']]['bill_code']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($shipment['unload_post_code'] != $crm_info[$shipment['id']]['ship_code']){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }else if($shipment['order_date'] != $order_date[0]){
                $compared_orders[$shipment['id']] = $shipment['shipment_code'];
              }
            }         

              foreach($shipment['load'] AS $load){   
                if(!empty($crm_loads[$load['load_id']])){

                  if($crm_loads[$load['load_id']]['ordered_quantity'] != $load['ordered_quantity']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['ordered_measure'] != $load['ordered_measure']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['ordered_weight'] != $load['ordered_weight']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['ordered_length'] != $load['ordered_length']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['ordered_width'] != $load['ordered_width']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['ordered_height'] != $load['ordered_height']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['revised_quantity'] != $load['revised_quantity']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['revised_measure'] != $load['revised_measure']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['revised_weight'] != $load['revised_weight']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['revised_length'] != $load['revised_length']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['revised_width'] != $load['revised_width']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['revised_height'] != $load['revised_height']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }else if($crm_loads[$load['load_id']]['revised_m3'] != $load['m3']){
                    $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                  }

                }
              }


            if(!empty($compared_orders[$shipment['id']]) OR !empty($compared_loads[$shipment['id']])){
              $insert->execute([':shipment_id' => $shipment['id'],':shipment_code' => $shipment['shipment_code'],':importdate' => $date_time]);         
            }
          }  


         $merged_arrays =  array_merge($compared_orders,$compared_loads);

         array_unique($merged_arrays);

          $shipment_code = "'" .implode("', '",array_filter($merged_arrays)). "'"; 
         
     
          $insert_orders = $dbh->prepare('INSERT IGNORE INTO app_orders_handle  (
                                                      id,
                                                      shipment_type,
                                                      shipment_code,
                                                      customer_id,
                                                      order_date,
                                                      status,                                                     
                                                      price,
                                                      price_agreed,
                                                      load_company_code,
                                                      load_company_name,
                                                      load_company_contact,
                                                      load_company_phone,                                                          
                                                      load_address,
                                                      load_settlement,
                                                      load_municipality,
                                                      load_post_code,
                                                      load_country_code,
                                                      load_date_from,
                                                      load_date_to,
                                                      unload_company_code,
                                                      unload_company_name,
                                                      unload_company_contact,
                                                      unload_company_phone,                                                          
                                                      unload_address,
                                                      unload_settlement,
                                                      unload_municipality,
                                                      unload_post_code,
                                                      unload_country_code,
                                                      unload_date_from,
                                                      unload_date_to,
                                                      update_metrika,
                                                      import_date,
                                                      route_code,
                                                      driver1_name,
                                                      vehicle_registration_number
                                                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

          $insert_loads = $dbh->prepare('INSERT IGNORE INTO app_loads_handle ( order_id,
                                                        id,
                                                        quantity,
                                                        revised_quantity,
                                                        measure,
                                                        revised_measure,
                                                        weight,
                                                        length,
                                                        width,
                                                        height,
                                                        m3,
                                                        pll,
                                                        note,
                                                        document,
                                                        ean,
                                                        import_date,
                                                        ordered,
                                                        revised                                                       	 
                                                      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
      

          $ch2 = curl_init();
          curl_setopt($ch2, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php?routes=1");
          curl_setopt($ch2, CURLOPT_POST, 1);
          curl_setopt($ch2, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas',  'shipment_code' => $shipment_code));
          curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch2, CURLOPT_TIMEOUT, 30);
          curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 1);
          $result = curl_exec($ch2);
          curl_close($ch2);   

          $data2 = json_decode($result,true); 
          $state = $data2['state'];  


          foreach($data2['order'] as $key => $row) {       
            $row['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$row['load_post_code']);
            $row['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$row['unload_post_code']);

            $row['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['load_post_code']);
            $row['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['unload_post_code']); 
            $date = date("Y-m-d H:i:s");

         

                                        $insert_orders->execute(array($row['id'],
                                                $row['shipment_type'], 
                                                $row['shipment_code'],                                                          
                                                $row['customer_id'],
                                                $row['order_date'],
                                                $row['status'],                                               
                                                $row['price'],
                                                $row['price_agreed'],
                                                $row['load_company_code'],
                                                $row['load_company_name'],
                                                $row['load_company_contact'],
                                                $row['load_company_phone'],                                                            
                                                $row['load_address'],
                                                $row['load_settlement'],
                                                $row['load_municipality'],
                                                $row['load_post_code'],
                                                $row['load_country_code'],
                                                $row['load_date_from'],
                                                $row['load_date_to'],
                                                $row['unload_company_code'],
                                                $row['unload_company_name'],
                                                $row['unload_company_contact'],
                                                $row['unload_company_phone'],                                                            
                                                $row['unload_address'],
                                                $row['unload_settlement'],
                                                $row['unload_municipality'],
                                                $row['unload_post_code'],
                                                $row['unload_country_code'],
                                                $row['unload_date_from'],
                                                $row['unload_date_to'], 
                                                $row['update_date'],                                                  
                                                $date,
                                                $row['route_code'],                                                            
                                                $row['driver1_name'],
                                                $row['vehicle_registration_number']                                                                  
                                         ));
  
              foreach($row['load'] as $key => $row2) {  

                    $zero = 0;
                    $ordered = array();
                    $revised = array();    
                  if(empty($row2['ordered_weight'])) $row2['ordered_weight'] = 0;  
                  if(empty($row2['ordered_length'])) $row2['ordered_length'] = 0;  
                  if(empty($row2['ordered_width'])) $row2['ordered_width'] = 0;  
                  if(empty($row2['ordered_height'])) $row2['ordered_height'] = 0;  

                  if(empty($row2['revised_weight'])) $row2['revised_weight'] = 0;  
                  if(empty($row2['revised_length'])) $row2['revised_length'] = 0;  
                  if(empty($row2['revised_width'])) $row2['revised_width'] = 0;  
                  if(empty($row2['revised_height'])) $row2['revised_height'] = 0;  

                  $ordered = $row2['ordered_weight']." ".$row2['ordered_length']."x".$row2['ordered_width']."x".$row2['ordered_height'];
                  $revised = $row2['revised_weight']." ".$row2['revised_length']."x".$row2['revised_width']."x".$row2['revised_height'];


                  $insert_loads->execute(array($row['id'],
                                        $row2['load_id'],
                                        $row2['ordered_quantity'],
                                        $row2['revised_quantity'],
                                        $row2['ordered_measure'],
                                        $row2['revised_measure'],
                                        $row2['ordered_weight'],
                                        $row2['ordered_length'],
                                        $row2['ordered_width'],
                                        $row2['ordered_height'],
                                        $row2['m3'],
                                        $row2['pll'],
                                        $row2['note'],
                                        $row2['documents'], 
                                        $row2['ean'],                                                     
                                        $date ,
                                        $ordered,
                                        $revised                                                                                     
                                        ));
                                                                                      
              }                
                                    
        }    
          
  


    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }



    $ch1 = curl_init();
    curl_setopt($ch1, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_update_handle.php');
    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch1, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch1, CURLOPT_TIMEOUT, 6000);
    curl_setopt($ch1, CURLOPT_CONNECTTIMEOUT, 30);
    curl_exec($ch1);
    curl_close($ch1);  

    $ch2 = curl_init();
    curl_setopt($ch2, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_update_handle.php');
    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch2, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch2, CURLOPT_TIMEOUT, 6000);
    curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 30);
    curl_exec($ch2);
    curl_close($ch2);  

    $ch3 = curl_init();           
    curl_setopt($ch3, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/orders_count_price.php');
    curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch3, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch3, CURLOPT_TIMEOUT, 6000);
    curl_setopt($ch3, CURLOPT_CONNECTTIMEOUT, 900);
    curl_exec($ch3);
    curl_close($ch3); 

