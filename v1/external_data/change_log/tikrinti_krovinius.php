<?php
set_time_limit(900);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {

      require('../mysql_connection.php');


      $date_from = "2021-11-11";
      $date_to = "2021-12-06";
     

      // $TRUNCATE1 = $dbh->prepare('TRUNCATE app_orders_handle');
      // $TRUNCATE1->execute(array());      
        
      // $TRUNCATE2 = $dbh->prepare('TRUNCATE app_loads_handle');
      // $TRUNCATE2->execute(array());

      $TRUNCATE3 = $dbh->prepare('TRUNCATE app_check_loads_count');
      $TRUNCATE3->execute(array());


      $insert_orders = $dbh->prepare('INSERT IGNORE INTO app_orders_handle  (
        id,
        shipment_type,
        shipment_code,
        customer_id,
        order_date,
        status,
        route_point_id,
        price,
        price_agreed,
        load_company_code,
        load_company_name,
        load_company_contact,
        load_company_phone,                                                          
        load_address,
        load_settlement,
        load_municipality,
        load_post_code,
        load_country_code,
        load_date_from,
        load_date_to,
        unload_company_code,
        unload_company_name,
        unload_company_contact,
        unload_company_phone,                                                          
        unload_address,
        unload_settlement,
        unload_municipality,
        unload_post_code,
        unload_country_code,
        unload_date_from,
        unload_date_to,
        update_metrika,
        import_date,
        route_code,
        driver1_name,
        vehicle_registration_number
      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

$insert_loads = $dbh->prepare('INSERT IGNORE INTO app_loads_handle ( order_id,
      id,
      quantity,
      revised_quantity,
      measure,
      revised_measure,
      weight,
      length,
      width,
      height,
      m3,
      pll,
      note,
      document,
      ean,
      import_date,
      ordered,
      revised                                                       	 
    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');


      

      $select = "DISTINCT shipment.shipment_id, COUNT(load_id) AS loads ";
      $table = "bill_of_lading 
      JOIN shipment ON shipment.bl_id=bill_of_lading.bl_id
      JOIN load ON shipment.shipment_id=load.shipment_id";
      $where = "order_date BETWEEN '$date_from' AND '$date_to' 
      GROUP BY shipment.shipment_id ";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
      $result = curl_exec($ch);
      curl_close($ch);
      $data = json_decode($result, true); 


      $insert_metrika_loads = $dbh->prepare('INSERT INTO app_check_loads_count (shipment_id, loads) VALUES (?,?)');


 
      foreach($data['response']['result'] as $row) {       
        $insert_metrika_loads->execute(array($row['shipment_id'],$row['loads']));
      }        
      

      $getOrders = $dbh->prepare("SELECT DISTINCT s.shipment_code,s.salesorderid, i.external_order_id, COUNT(i.id) AS loads_count,app_check_loads_count.loads
      FROM vtiger_salesorder s
      JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid AND productid = 14244
      JOIN app_check_loads_count ON shipment_id=s.external_order_id
      INNER JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
      WHERE (DATE_FORMAT(e.createdtime,'%Y-%m-%d') BETWEEN ? AND ?) AND i.external_order_id IS NOT NULL 
      GROUP BY i.id
      HAVING COUNT(i.id) != app_check_loads_count.loads");

      


      $getOrders->setFetchMode(PDO::FETCH_ASSOC);
      $getOrders->execute(array($date_from,$date_to));

      $crm_shipments = [];
      foreach ($getOrders as $value) {
        $crm_shipments[] = $value['shipment_code'];
      }     
      
      
      echo '<pre>';
      print_R($crm_shipments);
      echo '</pre>';
      die;

      $shipment_code = (count($crm_shipments) > 0 ? "'" .implode("','",$crm_shipments). "'" : '');



 
      
           
          $ch2 = curl_init();
          curl_setopt($ch2, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php?routes=1");
          curl_setopt($ch2, CURLOPT_POST, 1);
          curl_setopt($ch2, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas',  'shipment_code' => $shipment_code));
          curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch2, CURLOPT_TIMEOUT, 30);
          curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 1);
          $result = curl_exec($ch2);
          curl_close($ch2);   

          $data2 = json_decode($result,true);          
  
    
          foreach($data2['order'] as $key => $row) {       
              $row['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$row['load_post_code']);
              $row['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$row['unload_post_code']);

              $row['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['load_post_code']);
              $row['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['unload_post_code']); 
              $date = date("Y-m-d H:i:s");

           

                                          $insert_orders->execute(array($row['id'],
                                                  $row['shipment_type'], 
                                                  $row['shipment_code'],                                                          
                                                  $row['customer_id'],
                                                  $row['order_date'],
                                                  $row['status'],
                                                  $row['route_point_id'],
                                                  $row['price'],
                                                  $row['price_agreed'],
                                                  $row['load_company_code'],
                                                  $row['load_company_name'],
                                                  $row['load_company_contact'],
                                                  $row['load_company_phone'],                                                            
                                                  $row['load_address'],
                                                  $row['load_settlement'],
                                                  $row['load_municipality'],
                                                  $row['load_post_code'],
                                                  $row['load_country_code'],
                                                  $row['load_date_from'],
                                                  $row['load_date_to'],
                                                  $row['unload_company_code'],
                                                  $row['unload_company_name'],
                                                  $row['unload_company_contact'],
                                                  $row['unload_company_phone'],                                                            
                                                  $row['unload_address'],
                                                  $row['unload_settlement'],
                                                  $row['unload_municipality'],
                                                  $row['unload_post_code'],
                                                  $row['unload_country_code'],
                                                  $row['unload_date_from'],
                                                  $row['unload_date_to'], 
                                                  $row['update_date'],                                                  
                                                  $date,
                                                  $row['route_code'],                                                            
                                                  $row['driver1_name'],
                                                  $row['vehicle_registration_number']                                                                  
                                           ));
    
                foreach($row['load'] as $key => $row2) {  

                      $zero = 0;
                      $ordered = array();
                      $revised = array();    
                    if(empty($row2['ordered_weight'])) $row2['ordered_weight'] = 0;  
                    if(empty($row2['ordered_length'])) $row2['ordered_length'] = 0;  
                    if(empty($row2['ordered_width'])) $row2['ordered_width'] = 0;  
                    if(empty($row2['ordered_height'])) $row2['ordered_height'] = 0;  

                    if(empty($row2['revised_weight'])) $row2['revised_weight'] = 0;  
                    if(empty($row2['revised_length'])) $row2['revised_length'] = 0;  
                    if(empty($row2['revised_width'])) $row2['revised_width'] = 0;  
                    if(empty($row2['revised_height'])) $row2['revised_height'] = 0;  

                    $ordered = $row2['ordered_weight']." ".$row2['ordered_length']."x".$row2['ordered_width']."x".$row2['ordered_height'];
                    $revised = $row2['revised_weight']." ".$row2['revised_length']."x".$row2['revised_width']."x".$row2['revised_height'];


                    $insert_loads->execute(array($row['id'],
                                          $row2['load_id'],
                                          $row2['ordered_quantity'],
                                          $row2['revised_quantity'],
                                          $row2['ordered_measure'],
                                          $row2['revised_measure'],
                                          $row2['ordered_weight'],
                                          $row2['ordered_length'],
                                          $row2['ordered_width'],
                                          $row2['ordered_height'],
                                          $row2['m3'],
                                          $row2['pll'],
                                          $row2['note'],
                                          $row2['documents'], 
                                          $row2['ean'],                                                     
                                          $date ,
                                          $ordered,
                                          $revised                                                                                     
                                          ));
                                                                                        
                }                
                                      
          }    


    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();                         
    }



    // $ch3 = curl_init();
    // // curl_setopt($ch3, CURLOPT_URL, 'http://crm.test/invoices/orders_insert.php');
    // curl_setopt($ch3, CURLOPT_URL, 'https://crm.parnasas.lt/invoices/orders_insert.php');
    // curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($ch3, CURLOPT_FRESH_CONNECT, true);
    // curl_setopt($ch3, CURLOPT_TIMEOUT, 6000);
    // curl_setopt($ch3, CURLOPT_CONNECTTIMEOUT, 30);
    // curl_exec($ch3);
    // curl_close($ch3); 

    $ch4 = curl_init();
    // curl_setopt($ch4, CURLOPT_URL, 'http://crm.test/invoices/loads_insert.php');
    curl_setopt($ch4, CURLOPT_URL, 'https://crm.parnasas.lt/invoices/loads_insert.php');
    curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch4, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch4, CURLOPT_TIMEOUT, 6000);
    curl_setopt($ch4, CURLOPT_CONNECTTIMEOUT, 30);
    curl_exec($ch4);
    curl_close($ch4); 



    // $ch5 = curl_init();
    // // curl_setopt($ch5, CURLOPT_URL, 'http://crm.test/v1/external_data/orders_count_price.php');
    // curl_setopt($ch5, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/orders_count_price.php');
    // curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($ch5, CURLOPT_FRESH_CONNECT, true);
    // curl_setopt($ch5, CURLOPT_TIMEOUT, 6000);
    // curl_setopt($ch5, CURLOPT_CONNECTTIMEOUT, 30);
    // curl_exec($ch5);
    // curl_close($ch5);  
