<?php
set_time_limit(900);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {

      require('../mysql_connection.php');

      $TRUNCATE1 = $dbh->prepare('TRUNCATE app_orders_handle');
      $TRUNCATE1->execute(array());      
        
      $TRUNCATE2 = $dbh->prepare('TRUNCATE app_loads_handle');
      $TRUNCATE2->execute(array());



      $date_from = date('Y-m-d',strtotime("-5 days"));
      $date_to = date('Y-m-d');

      $date_from = '2021-11-05';
      $date_to = '2021-11-05';
  
  
      $check_dimensions = $dbh->prepare("SELECT l.*,s.external_order_id
      FROM vtiger_salesorder_dimensions_log l 
      JOIN vtiger_salesorder s ON s.salesorderid=l.salesorderid
      LEFT JOIN vtiger_invoice_salesorders_list li ON li.salesorderid=l.salesorderid
      INNER JOIN vtiger_crmentity e ON e.crmid=l.salesorderid
      WHERE (DATE_FORMAT(e.createdtime,'%Y-%m-%d') BETWEEN :date_from AND :date_to) AND li.invoiceid IS NULL AND deleted = 0
      ORDER BY id");
  
      $check_dimensions->setFetchMode(PDO::FETCH_ASSOC);
      $check_dimensions->execute([':date_from' => $date_from,':date_to' => $date_to]);
  
  
      $crm_loads = [];
  
      foreach($check_dimensions as $value) {
        $crm_loads[$value['external_order_id']] = $value;
      }


      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from,'date_to' => $date_to));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 300);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
      $result = curl_exec($ch);
      curl_close($ch);   
      $data = json_decode($result,true); 
  
      $compared_loads = [];
  
      foreach ($data['order'] as $shipment) {      
        $ordered_weight = '';
        $ordered_m2 = '';
        $ordered_m3 = '';
        $revised_weight = '';
        $revised_m2 = '';
        $revised_m3 = '';
  
          foreach($shipment['load'] AS $load){ 
            $ordered_weight += $load['ordered_weight'];    
            $ordered_m2 += ($load['ordered_length'] * $load['ordered_width']) * $load['ordered_quantity'];
            $ordered_m3 += ($load['ordered_length'] * $load['ordered_width'] * $load['ordered_height']) * $load['ordered_quantity'];
  
            $revised_weight += $load['revised_weight'];    
            $revised_m2 += ($load['revised_length'] * $load['revised_width']) * $load['revised_quantity'];        
            $revised_m3 += $load['m3'];          
          }  
  
  
          if(!empty($crm_loads[$shipment['id']])){
            if($crm_loads[$shipment['id']]['kg'] != $ordered_weight){
              $compared_loads[$shipment['id']] = $shipment['shipment_code'];
            }else if((int)(round($crm_loads[$shipment['id']]['m2'],2) * 100) != (int)(round($ordered_m2,2) *100) ){      
              $compared_loads[$shipment['id']] = $shipment['shipment_code'];
            }else if((int)(round($crm_loads[$shipment['id']]['m3'],2) * 100) != (int)(round($ordered_m3,2) *100) ){      
              $compared_loads[$shipment['id']] = $shipment['shipment_code'];
            }else if($crm_loads[$shipment['id']]['kg_revised'] != $revised_weight){
              $compared_loads[$shipment['id']] = $shipment['shipment_code'];
            }else if((int)(round($crm_loads[$shipment['id']]['m2_revised'],2) * 100) != (int)(round($revised_m2,2) *100) ){ 
              $compared_loads[$shipment['id']] = $shipment['shipment_code'];         
            }else if((int)(round($crm_loads[$shipment['id']]['m3_revised'],2) * 100) != (int)(round($revised_m3,2) *100) ){ 
              $compared_loads[$shipment['id']] = $shipment['shipment_code'];        
            }
          }  
      }     



          $shipment_code = "'" .implode("', '",array_filter($compared_loads)). "'"; 
         
     
          $insert_orders = $dbh->prepare('INSERT IGNORE INTO app_orders_handle  (
                                                      id,
                                                      shipment_type,
                                                      shipment_code,
                                                      customer_id,
                                                      order_date,
                                                      status,                                                     
                                                      price,
                                                      price_agreed,
                                                      load_company_code,
                                                      load_company_name,
                                                      load_company_contact,
                                                      load_company_phone,                                                          
                                                      load_address,
                                                      load_settlement,
                                                      load_municipality,
                                                      load_post_code,
                                                      load_country_code,
                                                      load_date_from,
                                                      load_date_to,
                                                      unload_company_code,
                                                      unload_company_name,
                                                      unload_company_contact,
                                                      unload_company_phone,                                                          
                                                      unload_address,
                                                      unload_settlement,
                                                      unload_municipality,
                                                      unload_post_code,
                                                      unload_country_code,
                                                      unload_date_from,
                                                      unload_date_to,
                                                      update_metrika,
                                                      import_date,
                                                      route_code,
                                                      driver1_name,
                                                      vehicle_registration_number
                                                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

          $insert_loads = $dbh->prepare('INSERT IGNORE INTO app_loads_handle ( order_id,
                                                        id,
                                                        quantity,
                                                        revised_quantity,
                                                        measure,
                                                        revised_measure,
                                                        weight,
                                                        length,
                                                        width,
                                                        height,
                                                        m3,
                                                        pll,
                                                        note,
                                                        document,
                                                        ean,
                                                        import_date,
                                                        ordered,
                                                        revised                                                       	 
                                                      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
      

          $ch2 = curl_init();
          curl_setopt($ch2, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php?routes=1");
          curl_setopt($ch2, CURLOPT_POST, 1);
          curl_setopt($ch2, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas',  'shipment_code' => $shipment_code));
          curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch2, CURLOPT_TIMEOUT, 30);
          curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 1);
          $result = curl_exec($ch2);
          curl_close($ch2);   

          $data2 = json_decode($result,true); 
          $state = $data2['state'];  


          foreach($data2['order'] as $key => $row) {       
            $row['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$row['load_post_code']);
            $row['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$row['unload_post_code']);

            $row['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['load_post_code']);
            $row['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['unload_post_code']); 
            $date = date("Y-m-d H:i:s");

         

                                        $insert_orders->execute(array($row['id'],
                                                $row['shipment_type'], 
                                                $row['shipment_code'],                                                          
                                                $row['customer_id'],
                                                $row['order_date'],
                                                $row['status'],                                               
                                                $row['price'],
                                                $row['price_agreed'],
                                                $row['load_company_code'],
                                                $row['load_company_name'],
                                                $row['load_company_contact'],
                                                $row['load_company_phone'],                                                            
                                                $row['load_address'],
                                                $row['load_settlement'],
                                                $row['load_municipality'],
                                                $row['load_post_code'],
                                                $row['load_country_code'],
                                                $row['load_date_from'],
                                                $row['load_date_to'],
                                                $row['unload_company_code'],
                                                $row['unload_company_name'],
                                                $row['unload_company_contact'],
                                                $row['unload_company_phone'],                                                            
                                                $row['unload_address'],
                                                $row['unload_settlement'],
                                                $row['unload_municipality'],
                                                $row['unload_post_code'],
                                                $row['unload_country_code'],
                                                $row['unload_date_from'],
                                                $row['unload_date_to'], 
                                                $row['update_date'],                                                  
                                                $date,
                                                $row['route_code'],                                                            
                                                $row['driver1_name'],
                                                $row['vehicle_registration_number']                                                                  
                                         ));
  
              foreach($row['load'] as $key => $row2) {  

                    $zero = 0;
                    $ordered = array();
                    $revised = array();    
                  if(empty($row2['ordered_weight'])) $row2['ordered_weight'] = 0;  
                  if(empty($row2['ordered_length'])) $row2['ordered_length'] = 0;  
                  if(empty($row2['ordered_width'])) $row2['ordered_width'] = 0;  
                  if(empty($row2['ordered_height'])) $row2['ordered_height'] = 0;  

                  if(empty($row2['revised_weight'])) $row2['revised_weight'] = 0;  
                  if(empty($row2['revised_length'])) $row2['revised_length'] = 0;  
                  if(empty($row2['revised_width'])) $row2['revised_width'] = 0;  
                  if(empty($row2['revised_height'])) $row2['revised_height'] = 0;  

                  $ordered = $row2['ordered_weight']." ".$row2['ordered_length']."x".$row2['ordered_width']."x".$row2['ordered_height'];
                  $revised = $row2['revised_weight']." ".$row2['revised_length']."x".$row2['revised_width']."x".$row2['revised_height'];


                  $insert_loads->execute(array($row['id'],
                                        $row2['load_id'],
                                        $row2['ordered_quantity'],
                                        $row2['revised_quantity'],
                                        $row2['ordered_measure'],
                                        $row2['revised_measure'],
                                        $row2['ordered_weight'],
                                        $row2['ordered_length'],
                                        $row2['ordered_width'],
                                        $row2['ordered_height'],
                                        $row2['m3'],
                                        $row2['pll'],
                                        $row2['note'],
                                        $row2['documents'], 
                                        $row2['ean'],                                                     
                                        $date ,
                                        $ordered,
                                        $revised                                                                                     
                                        ));
                                                                                      
              }                
                                    
        }    
          
  


    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }



    $ch1 = curl_init();
    curl_setopt($ch1, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_update_handle.php');
    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch1, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch1, CURLOPT_TIMEOUT, 6000);
    curl_setopt($ch1, CURLOPT_CONNECTTIMEOUT, 30);
    curl_exec($ch1);
    curl_close($ch1);  

    $ch2 = curl_init();
    curl_setopt($ch2, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_update_handle.php');
    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch2, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch2, CURLOPT_TIMEOUT, 6000);
    curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 30);
    curl_exec($ch2);
    curl_close($ch2);  

    $ch3 = curl_init();           
    curl_setopt($ch3, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/orders_count_price.php');
    curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch3, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch3, CURLOPT_TIMEOUT, 6000);
    curl_setopt($ch3, CURLOPT_CONNECTTIMEOUT, 900);
    curl_exec($ch3);
    curl_close($ch3); 

