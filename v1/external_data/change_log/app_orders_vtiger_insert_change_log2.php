<?php
include '../ws.config.php';
global $config;
require '../utils.php';
set_time_limit(500);
error_reporting(1);

try {

    require('../mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_salesorder (salesorderid,
                                                                subject,
                                                                salesorder_no,
                                                                shipment_code,                                                               
                                                                accountid,
                                                                sostatus,                                                                                               
                                                                load_date_from, 
                                                                load_time_from,
                                                                load_date_to, 
                                                                load_time_to,                                                               
                                                                unload_date_from, 
                                                                unload_time_from,
                                                                unload_date_to, 
                                                                unload_time_to, 
                                                                route_point_id,                                                                
                                                                external_order_id,
                                                                source,
                                                                import_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');    

    $date = date("Y-m-d");      

    $sth = $dbh->prepare("SELECT DISTINCT t.*, a.accountid, e.smownerid AS user_id, t.load_post_code,  t.unload_post_code, SUM(l.weight)  AS weight, SUM(l.length * l.width * l.height) as volume, l.m3, l.pll,ROUND(shipment_direct_km,2) AS shipment_km
                        FROM app_orders_change_log t
                        JOIN vtiger_account a ON a.customer_id = t.customer_id  
                        JOIN app_loads_change_log l ON l.order_id = t.id 
                        JOIN vtiger_crmentity e ON e.crmid = a.accountid                                              
                        WHERE e.deleted = 0 AND t.id NOT IN (SELECT c.external_order_id FROM vtiger_salesorder c, vtiger_crmentity d WHERE  d.crmid = c.salesorderid AND d.deleted = 0 AND c.external_order_id IS NOT NULL)
                        GROUP BY l.order_id");

    $sth5 = $dbh->prepare('UPDATE vtiger_salesordercf SET cf_855 = ?, cf_928 = ?, cf_1504 = ?, cf_1558 = ?, cf_1560 = ?, cf_1562 = ?, cf_1564 = ?, cf_1566 = ? WHERE salesorderid = ?');   

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());
    $orders_count = $sth->rowCount();


    $sth3 = $dbh->prepare("SELECT id FROM vtiger_crmentity_seq");
    $sth3->setFetchMode(PDO::FETCH_ASSOC);
    $sth3->execute(array());
    $seq = $sth3->fetch();

    $new_seq = $seq['id'] + $orders_count;

    if($orders_count > 0){

    $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
    $sth4->execute(array($new_seq)); 
    }


    
    $last_entity_record = $seq['id']+1;
    while ($row = $sth->fetch()) {         

         $id = $row['accountid'];
         $weight = $row['weight'];
         $volume = ($row['m3'] > $row['volume'] ? $row['m3'] : $row['volume']);
         $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
         $shipment_type = ($row['shipment_type'] == 'parcel' ? 'Transporto užsakymas' : 'Perkraustymo užsakymas');
            
            $load_date_from = date("Y-m-d", strtotime($row['load_date_from']));
            $load_date_to = date("Y-m-d", strtotime($row['load_date_to']));
            $load_time_from = date("H:i:s", strtotime($row['load_date_from']));
            $load_time_to = date("H:i:s", strtotime($row['load_date_to']));                        
            $unload_date_from = date("Y-m-d", strtotime($row['unload_date_from']));
            $unload_date_to = date("Y-m-d", strtotime($row['unload_date_to']));
            $unload_time_from = date("H:i:s", strtotime($row['unload_date_from']));
            $unload_time_to = date("H:i:s", strtotime($row['unload_date_to']));                
            $subject = 'Užsakymas';  
            $source = 'Metrika';             
            $date = date("Y-m-d H:i:s");    
            if(!empty($row['order_date'])){ 
               $order_date = date("Y-m-d", strtotime($row['order_date']))." ".date("H:i:s");
            }else{
               $order_date = date("Y-m-d H:i:s");
            }
           

               // $last_entity_record = insert_entity2($dbh, 'SalesOrder', $row['user_id'], NULL,  $row['id'], $order_date);   
               $insert_entity = insert_entity3($dbh, 'SalesOrder', $last_entity_record, $row['user_id'], NULL,'Metrika',  $row['id'], $date);                 
                    
               $sth2->execute(array($last_entity_record, 
                                    $subject, 
                                    $row['id'],                                      
                                    $row['shipment_code'],                                      
                                    $row['accountid'], 
                                    $row['status'],                                        
                                    $load_date_from,                       
                                    $load_time_from,
                                    $load_date_to,
                                    $load_time_to,
                                    $unload_date_from,      
                                    $unload_time_from, 
                                    $unload_date_to,      
                                    $unload_time_to,
                                    $row['route_point_id'],                    
                                    $row['id'],                     
                                    $source,
                                    $date            
               ));

                    
                    $insert_load_address = insert_load_address($dbh, $last_entity_record, $row['load_address'], $row['load_post_code'], $row['load_municipality'], $row['load_country_code'], $row['load_company_name'], $row['load_company_code'], $row['load_company_contact'], $row['load_company_phone']);
                    $insert_unload_address = insert_unload_address($dbh, $last_entity_record, $row['unload_address'], $row['unload_post_code'], $row['unload_municipality'], $row['unload_country_code'], $row['unload_company_name'], $row['unload_company_code'], $row['unload_company_contact'], $row['unload_company_phone']);     
                    
                    
                    $sth5->execute(array(                                                              
                     $shipment_type,
                     $distance,
                     $order_date,
                     $row['route_code'],                   
                     $row['vehicle_registration_number'],
                     $row['driver1_name'],
                     $row['load_company_phone'],
                     $row['unload_company_phone'], 
                     $last_entity_record
                  ));  

     $last_entity_record++;
    }

    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error!";
   echo $e->getMessage();
}

?>
