<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);


try {

    require('../mysql_connection.php');

    $dbh->beginTransaction(); 

     $stevedoring_query = $dbh->prepare("SELECT c.salesorderid, cf.cf_1572 as stevedoring                
                                        FROM app_orders_change_log t
                                        LEFT JOIN vtiger_salesorder c ON c.external_order_id = t.id   
                                        LEFT JOIN vtiger_account a ON a.accountid = c.accountid   
                                        LEFT JOIN vtiger_accountscf ac ON ac.accountid = a.accountid   
                                        LEFT JOIN vtiger_crmentity e ON e.crmid = a.accountid                           
                                        LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid               
                                        LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=c.salesorderid                                              
                                        WHERE e.deleted = 0 AND ac.cf_1574 = 'Automatu'  
                                        GROUP BY c.salesorderid");

     $inv_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ?");    
     $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id,productid,sequence_no,quantity,margin,service,inventory_type) VALUES(?,?,?,?,?,?,?)");      

     $stevedoring_query->setFetchMode(PDO::FETCH_ASSOC);     
     $stevedoring_query->execute(array());


     $inv_num->setFetchMode(PDO::FETCH_ASSOC);   
     

    foreach($stevedoring_query AS $row) {
        if($row['stevedoring'] > 0){           
            $inv_num->execute(array($row['salesorderid']));
            $count = $inv_num->rowCount();
            $insert_service->execute(array($row['salesorderid'],36641,$count+1,1,$row['stevedoring'],2,'stevedoring'));   
        }
    }


    $update_stevedoring = $dbh->prepare("SELECT c.salesorderid, cf.cf_1572 as stevedoring                
                                        FROM app_orders_update t
                                        LEFT JOIN vtiger_salesorder c ON c.external_order_id = t.id   
                                        LEFT JOIN vtiger_account a ON a.accountid = c.accountid   
                                        LEFT JOIN vtiger_accountscf ac ON ac.accountid = a.accountid   
                                        LEFT JOIN vtiger_crmentity e ON e.crmid = a.accountid                           
                                        LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid               
                                        LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=c.salesorderid                                              
                                        WHERE e.deleted = 0 AND ac.cf_1574 = 'Automatu'  
                                        GROUP BY c.salesorderid");

    $update_stevedoring->setFetchMode(PDO::FETCH_ASSOC);     
    $update_stevedoring->execute(array());  

    $update = $dbh->prepare("UPDATE vtiger_inventoryproductrel SET margin = ? WHERE id = ? AND productid = 36641 AND inventory_type = 'stevedoring'");

    $delete = $dbh->prepare("DELETE FROM vtiger_inventoryproductrel WHERE id = ? AND productid = 36641 AND inventory_type = 'stevedoring'");
    
    foreach($update_stevedoring AS $row) {
        if($row['stevedoring'] > 0){   
            $update->execute(array($row['stevedoring'],$row['salesorderid']));
        }else{
            $delete->execute(array($row['salesorderid']));
        }
    }


             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
