<?php
set_time_limit(900);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {

      require('../mysql_connection.php');


      $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update_for_update_orders` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
      $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
      $last_timestamp->execute(array());
      $date_from = $last_timestamp->fetch();
      $date_from = $date_from['date'];

      // $now = date("Y-m-d H:i");

      $minute = date("i");        
      
      $minutes = change_minutes($minute);
    
      $time = date("Y-m-d H:$minutes:00");     

      
            
      $sth = $dbh->prepare('TRUNCATE app_orders_update');
      $sth->execute(array());

      $sth = $dbh->prepare('TRUNCATE app_loads_update');
      $sth->execute(array());

      
      $sth = $dbh->prepare('TRUNCATE app_loads_update_without_order');
      $sth->execute(array());

      $sth = $dbh->prepare('TRUNCATE app_load_update_flags');
      $sth->execute(array());      
      
    
      $check = $dbh->prepare("SELECT `value` FROM app_other_settings WHERE title = 'LBL_CARGO_NOT_STANDART'");    
      $check->setFetchMode(PDO::FETCH_ASSOC);
      $check->execute(array());

      $meter = $check->fetch();
      $taxablemeter = $meter['value'];  

      $user_m = $dbh->prepare("SELECT cf_1635 as meters 
                                FROM vtiger_account a
                                LEFT JOIN vtiger_accountscf cf ON cf.accountid=a.accountid 
                                WHERE a.customer_id = ? AND cf_1635 IS NOT NULL");  
      $user_m->setFetchMode(PDO::FETCH_ASSOC);

      $user_m2 = $dbh->prepare("SELECT cf_1635 as meters 
                                FROM vtiger_salesorder s
                                LEFT JOIN vtiger_account a ON s.accountid=a.accountid
                                LEFT JOIN vtiger_accountscf cf ON cf.accountid=a.accountid 
                                WHERE external_order_id = ? AND cf_1635 IS NOT NULL"); 

      $user_m2->setFetchMode(PDO::FETCH_ASSOC);   
      
      

      $sth5 = $dbh->prepare("INSERT INTO app_last_update_for_update_orders  (status,date) VALUES (?,?)");

      $sth5_2 = $dbh->prepare("UPDATE app_last_update_for_update_orders SET status = ?, date = ? WHERE id = ?");

      $sth6 = $dbh->prepare('INSERT IGNORE INTO app_orders_update (
                                                                    id,
                                                                    shipment_type,
                                                                    shipment_code,
                                                                    customer_id,
                                                                    order_date,
                                                                    status,
                                                                    route_point_id,
                                                                    price,
                                                                    price_agreed,
                                                                    shipment_direct_km,
                                                                    load_company_code,
                                                                    load_company_name,
                                                                    load_company_contact,
                                                                    load_company_phone,                                                          
                                                                    load_address,
                                                                    load_settlement,
                                                                    load_municipality,
                                                                    load_post_code,
                                                                    load_country_code,
                                                                    load_date_from,
                                                                    load_date_to,
                                                                    unload_company_code,
                                                                    unload_company_name,
                                                                    unload_company_contact,
                                                                    unload_company_phone,                                                          
                                                                    unload_address,
                                                                    unload_settlement,
                                                                    unload_municipality,
                                                                    unload_post_code,
                                                                    unload_country_code,
                                                                    unload_date_from,
                                                                    unload_date_to,
                                                                    update_metrika,
                                                                    import_date,
                                                                    route_code,
                                                                    driver1_name,
                                                                    vehicle_registration_number
                                                                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)');


      $sth7 = $dbh->prepare('INSERT IGNORE INTO app_loads_update (  id,
                                                                          ordered_quantity,
                                                                          quantity,
                                                                          measure,
                                                                          measure_revised,
                                                                          weight,
                                                                          length,
                                                                          width,
                                                                          height,
                                                                          m3,
                                                                          pll,
                                                                          note,
                                                                          document,
                                                                          ean,
                                                                          import_date,
                                                                          ordered,
                                                                          revised,
                                                                          shipment_id,
                                                                          meters                                                       	 
                                                                          ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?,?)');


      $insert_load_without_order = $dbh->prepare('INSERT IGNORE INTO app_loads_update_without_order (  id,
                                                          ordered_quantity,
                                                          quantity,
                                                          measure,
                                                          measure_revised,
                                                          weight,
                                                          length,
                                                          width,
                                                          height,
                                                          m3,
                                                          pll,
                                                          note,
                                                          document,
                                                          ean,
                                                          import_date,
                                                          ordered,
                                                          revised,
                                                          shipment_id,
                                                          meters                                                       	 
                                                          ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?,?)');

    

      $sth12 = $dbh->prepare('INSERT IGNORE INTO app_load_update_flags (order_id, load_id, termo, minifest, cmr, invoice, palettes, other_tare) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
      

      $sth5->execute(array('success',$time));  
      $last_timestamp_id = $dbh->lastInsertId();
                    
                    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders_changes.php?routes=1");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 300);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                $result = curl_exec($ch);
                curl_close($ch);   

                $data = json_decode($result,true); 

                $values = array();
            
                if(!empty($data['response']['updated_shipments'])){
                  $values[] = 'updated_shipments';
                }
              
                if(!empty($data['response']['updated_loads'])){
                  $values[] = 'updated_loads';
                }               
                
              
                if(empty($values)){
                  die();
                }


                $state = $data['state']; 
               
        
                if($state == 'ERR') {          
                    $logfile = 'app_orders.log';
                    customers_log($data, $logfile);
                }




          if(!empty($data['response']['updated_shipments'])){ 

            foreach($data['response']['updated_shipments'] as $key => $row) {
              $row['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$row['load_post_code']);
              $row['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$row['unload_post_code']);

              $row['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['load_post_code']);
              $row['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['unload_post_code']);

              $date = date("Y-m-d H:i:s");

                                            $sth6->execute(array($row['id'],
                                                 $row['shipment_type'], 
                                                 $row['shipment_code'],                                                          
                                                 $row['customer_id'],
                                                 $row['order_date'],
                                                 $row['status'],
                                                 $row['route_point_id'],
                                                 $row['price'],
                                                 $row['price_agreed'],
                                                 $row['shipment_direct_km'],
                                                 $row['load_company_code'],
                                                 $row['load_company_name'],
                                                 $row['load_company_contact'],
                                                 $row['load_company_phone'],                                                            
                                                 $row['load_address'],
                                                 $row['load_settlement'],
                                                 $row['load_municipality'],
                                                 $row['load_post_code'],
                                                 $row['load_country_code'],
                                                 $row['load_date_from'],
                                                 $row['load_date_to'],
                                                 $row['unload_company_code'],
                                                 $row['unload_company_name'],
                                                 $row['unload_company_contact'],
                                                 $row['unload_company_phone'],                                                            
                                                 $row['unload_address'],
                                                 $row['unload_settlement'],
                                                 $row['unload_municipality'],
                                                 $row['unload_post_code'],
                                                 $row['unload_country_code'],
                                                 $row['unload_date_from'],
                                                 $row['unload_date_to'], 
                                                 $row['edit_date'],                                                  
                                                 $date,
                                                 $row['route_code'],  
                                                 $row['driver1_name'],  
                                                 $row['vehicle_registration_number']                                                        
                                          ));
   

            }

          }

  

          if(!empty($data['response']['updated_loads'])){ 
           

            foreach($data['response']['updated_loads'] as $key => $row) {

              $ordered_meters_arr = array();
              $revised_meters_arr = array();

                  $zero = 0;
                  $ordered = array();
                  $revised = array();    
                if(empty($row['ordered_weight'])) $row['ordered_weight'] = 0;  
                if(empty($row['ordered_length'])) $row['ordered_length'] = 0;  
                if(empty($row['ordered_width'])) $row['ordered_width'] = 0;  
                if(empty($row['ordered_height'])) $row['ordered_height'] = 0;  

                if(empty($row['revised_weight'])) $row['revised_weight'] = 0;  
                if(empty($row['revised_length'])) $row['revised_length'] = 0;  
                if(empty($row['revised_width'])) $row['revised_width'] = 0;  
                if(empty($row['revised_height'])) $row['revised_height'] = 0;  
                  

                 $ordered = $row['ordered_weight']." ".$row['ordered_length']."x".$row['ordered_width']."x".$row['ordered_height'];
                 $revised = $row['revised_weight']." ".$row['revised_length']."x".$row['revised_width']."x".$row['revised_height'];


                 $ordered_meters_arr[] = $row['ordered_length'];
                 $ordered_meters_arr[] = $row['ordered_width'];     

                 $revised_meters_arr[] = $row['revised_length'];
                 $revised_meters_arr[] = $row['revised_width'];       
                     
                 $user_m2->execute(array($row['shipment_id']));
          
                 $user2 = $user_m2->fetch();
                 $taxablemeter2 = $user2['meters'];                
           
                 $max_ordered_meter = max($ordered_meters_arr);
                 $max_revised_meter = max($revised_meters_arr);       

                 $max_meter = ($max_ordered_meter > $max_revised_meter ? $max_ordered_meter : $max_revised_meter);
                 $max_taxablemeter = ($taxablemeter2 == 0 || $taxablemeter2 == '' ? $taxablemeter : $taxablemeter2);

                 $meters = ($max_meter > $max_taxablemeter ? $max_meter : NULL);   


                    $insert_load_without_order->execute(array(
                                          $row['load_id'],
                                          $row['ordered_quantity'],
                                          $row['revised_quantity'],
                                          $row['ordered_measure'],
                                          $row['revised_measure'],
                                          $row['revised_weight'],
                                          $row['revised_length'],
                                          $row['revised_width'],
                                          $row['revised_height'],
                                          $row['m3'],
                                          $row['pll'],
                                          $row['note'],
                                          $row['documents'], 
                                          $row['ean'],                                                     
                                          $date ,
                                          $ordered,
                                          $revised,
                                          $row['shipment_id'],
                                          $meters                                                                                     
                    ));

                      $sth12->execute(array(
                          $row['shipment_id'],
                          $row['load_id'],
                          $row['flags']['flg_termo'],
                          $row['flags']['flg_minifest'],
                          $row['flags']['flg_crm'],                                                                                               
                          $row['flags']['flg_invoice'],                                                                                               
                          $row['flags']['flg_palettes'],                                                                                               
                          $row['flags']['flg_other_tare']                                                                                            
                      ));
                    

            }

          }



            // Added load from shipment
          if(!empty($data['response']['updated_shipments'])){ 
            $check_load = $dbh->prepare("SELECT id FROM app_loads_update WHERE id = ?");            

            foreach($data['response']['updated_shipments'] as $key => $row2) {

              if(!empty($row2['load'])){ 

                foreach($row2['load'] as $row) { 

                  $check_load->execute(array($row['load_id']));
                  $num_rows = $check_load->rowCount();

                  if(!$num_rows){

                      $ordered_meters_arr = array();
                      $revised_meters_arr = array();

                      $zero = 0;
                      $ordered = array();
                      $revised = array();    
                        if(empty($row['ordered_weight'])) $row['ordered_weight'] = 0;  
                        if(empty($row['ordered_length'])) $row['ordered_length'] = 0;  
                        if(empty($row['ordered_width'])) $row['ordered_width'] = 0;  
                        if(empty($row['ordered_height'])) $row['ordered_height'] = 0;  

                        if(empty($row['revised_weight'])) $row['revised_weight'] = 0;  
                        if(empty($row['revised_length'])) $row['revised_length'] = 0;  
                        if(empty($row['revised_width'])) $row['revised_width'] = 0;  
                        if(empty($row['revised_height'])) $row['revised_height'] = 0;                            

                        $ordered = $row['ordered_weight']." ".$row['ordered_length']."x".$row['ordered_width']."x".$row['ordered_height'];
                        $revised = $row['revised_weight']." ".$row['revised_length']."x".$row['revised_width']."x".$row['revised_height'];


                        $ordered_meters_arr[] = $row['ordered_length'];
                        $ordered_meters_arr[] = $row['ordered_width'];     

                        $revised_meters_arr[] = $row['revised_length'];
                        $revised_meters_arr[] = $row['revised_width'];       
                            
                        $user_m2->execute(array($row['shipment_id']));
                  
                        $user2 = $user_m2->fetch();
                        $taxablemeter2 = $user2['meters'];                
                  
                        $max_ordered_meter = max($ordered_meters_arr);
                        $max_revised_meter = max($revised_meters_arr);       

                        $max_meter = ($max_ordered_meter > $max_revised_meter ? $max_ordered_meter : $max_revised_meter);
                        $max_taxablemeter = ($taxablemeter2 == 0 || $taxablemeter2 == '' ? $taxablemeter : $taxablemeter2);

                        $meters = ($max_meter > $max_taxablemeter ? $max_meter : NULL);  
        
                        $sth7->execute(array(
                                              $row['load_id'],
                                              $row['ordered_quantity'],
                                              $row['revised_quantity'],
                                              $row['ordered_measure'],
                                              $row['revised_measure'],
                                              $row['revised_weight'],
                                              $row['ordered_length'],
                                              $row['ordered_width'],
                                              $row['ordered_height'],
                                              $row['m3'],
                                              $row['pll'],
                                              $row['note'],
                                              $row['documents'], 
                                              $row['ean'],                                                     
                                              $date ,
                                              $ordered,
                                              $revised,
                                              $row2['id'],
                                              $meters                                                                                     
                        ));
                      
                          $sth12->execute(array(
                              $row2['id'],
                              $row['load_id'],
                              $row['flags']['flg_termo'],
                              $row['flags']['flg_minifest'],
                              $row['flags']['flg_crm'],                                                                                               
                              $row['flags']['flg_invoice'],                                                                                               
                              $row['flags']['flg_palettes'],                                                                                               
                              $row['flags']['flg_other_tare']                                                                                            
                          ));
                        

                  }
                }
              }
            }
          }   
            


    } catch (PDOException $e) {

       echo "Error!";
       echo $e->getMessage();
       // $sth5_2->execute(array('fail',$time,$last_timestamp_id));
       $logfile = 'app_orders.log';
       customers_log($e->getMessage(), $logfile);   
       sendReportMail($e->getMessage(),basename(__FILE__));
    }
 
