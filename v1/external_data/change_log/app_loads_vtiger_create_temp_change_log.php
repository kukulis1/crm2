<?php
set_time_limit(300);
error_reporting(1);
include '../ws.config.php';
global $config;
require '../utils.php';

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
    $truncate = $dbh->prepare("TRUNCATE app_loads_update_temp");
    $truncate->execute(array());

    $truncate2 = $dbh->prepare("TRUNCATE app_loads_update_without_order_temp");
    $truncate2->execute(array());

    $sth = $dbh->prepare("SELECT GROUP_CONCAT(ordered_weight,' ',ordered_length,'x',ordered_width,'x',ordered_width) AS ordered,
                                 GROUP_CONCAT(revised_weight,' ',revised_length,'x',revised_width,'x',revised_width) AS revised,
                                 SUM(t.m3) AS m3, 
                                 SUM(t.length * t.width * t.height) * t.quantity as volume,
                                 s.salesorderid
                            FROM app_loads_update t
                            JOIN vtiger_inventoryproductrel c ON c.external_load_id = t.id
                            LEFT JOIN vtiger_salesorder s ON s.salesorderid=c.id
                            LEFT JOIN vtiger_invoice_salesorders_list sl ON sl.salesorderid=s.salesorderid     
                            WHERE sl.salesorderid IS NULL  AND s.salesorderid IS NOT NULL
                            GROUP BY t.shipment_id");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare("INSERT INTO app_loads_update_temp (cf_1317, cf_1319, cf_1365, cf_1367,salesorderid) VALUES (?,?,?,?,?)");    



    while ($row = $sth->fetch()) { 
        $sth2->execute(array($row['ordered'],$row['revised'], $row['m3'], $row['volume'],$row['salesorderid']));
    }



    $sth = $dbh->prepare("SELECT GROUP_CONCAT(ordered_weight,' ',ordered_length,'x',ordered_width,'x',ordered_width) AS ordered,
                                 GROUP_CONCAT(revised_weight,' ',revised_length,'x',revised_width,'x',revised_width) AS revised,
                                 SUM(t.m3) AS m3, 
                                 SUM(t.length * t.width * t.height) * t.quantity as volume,
                                 s.salesorderid
                            FROM app_loads_update_without_order t
                            JOIN vtiger_inventoryproductrel c ON c.external_load_id = t.id
                            LEFT JOIN vtiger_salesorder s ON s.salesorderid=c.id
                            LEFT JOIN vtiger_invoice_salesorders_list sl ON sl.salesorderid=s.salesorderid     
                            WHERE sl.salesorderid IS NULL  AND s.salesorderid IS NOT NULL
                            GROUP BY t.shipment_id");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $sth2 = $dbh->prepare("INSERT INTO app_loads_update_without_order_temp (cf_1317, cf_1319, cf_1365, cf_1367,salesorderid) VALUES (?,?,?,?,?)");    



    while ($row = $sth->fetch()) { 
        $sth2->execute(array($row['ordered'],$row['revised'], $row['m3'], $row['volume'],$row['salesorderid']));
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
   sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
