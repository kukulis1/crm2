<?php
set_time_limit(300);
error_reporting(1);
include '../ws.config.php';
global $config;
require '../utils.php';

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_inventoryproductrel_flags SET termo = ?, 
                                                                        minifest = ?, 
                                                                        cmr = ?, 
                                                                        invoice = ?, 
                                                                        palettes = ?, 
                                                                        other_tare = ?                                                    
                                                            WHERE external_load_id = ?');

    $sth3 = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel_flags (flag_id, 
                                                                            external_order_id, 
                                                                            external_load_id, 
                                                                            termo, 
                                                                            minifest, 
                                                                            cmr, 
                                                                            invoice, 
                                                                            palettes, 
                                                                            other_tare,
                                                                            import_date) 
                                                                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');    

    $check = $dbh->prepare("SELECT flag_id FROM vtiger_inventoryproductrel_flags WHERE external_load_id = ?");
                 
    
    $sth = $dbh->prepare('SELECT f.*, c.external_load_id,s.salesorderid
                            FROM app_load_update_flags f
                            JOIN vtiger_inventoryproductrel c ON c.external_load_id = f.load_id
                            LEFT JOIN vtiger_salesorder s ON s.salesorderid=c.id
                            LEFT JOIN vtiger_invoice_salesorders_list sl ON sl.salesorderid=s.salesorderid     
                            WHERE sl.salesorderid IS NULL AND s.salesorderid IS NOT NULL
                            GROUP BY f.load_id');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $date = date("Y-m-d H:i:s");

    while ($row = $sth->fetch()) {       

        $check->execute(array($row['external_load_id']));
        $num_rows = $check->rowCount();

        if(!$num_rows){
            $sth3->execute(array($row['salesorderid'],
                                    $row['order_id'],
                                    $row['load_id'],
                                    $row['termo'],
                                    $row['minifest'],       
                                    $row['cmr'],       
                                    $row['invoice'],       
                                    $row['palettes'],       
                                    $row['other_tare'],
                                    $date      
                                ));
        }else{
            $sth2->execute(array(                  
                        $row['termo'],
                        $row['minifest'],       
                        $row['cmr'],       
                        $row['invoice'],       
                        $row['palettes'],       
                        $row['other_tare'],
                        $row['external_load_id']                 
            ));         
        }          
            

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
