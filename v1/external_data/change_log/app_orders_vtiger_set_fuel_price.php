<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);


try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();      

     
     $update_fuel_price = $dbh->prepare('UPDATE vtiger_salesordercf SET cf_1556 = ?  WHERE salesorderid = ?');              
                                                
     // NOTE Gaunam uzsakymo ir kliento kortelės kuro priemokos rėžių informacija             
     $clients_ranges = $dbh->prepare("SELECT c.salesorderid,c.total, a.accountid, DATE_FORMAT(t.order_date,'%Y-%m-%d') AS order_date                 
                                            FROM app_orders_change_log t
                                            LEFT JOIN vtiger_salesorder c ON c.external_order_id = t.id   
                                            LEFT JOIN vtiger_account a ON a.accountid = c.accountid   
                                            LEFT JOIN vtiger_accountscf ac ON ac.accountid = a.accountid   
                                            LEFT JOIN vtiger_crmentity e ON e.crmid = a.accountid                           
                                            LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid
                                            LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = a.accountid 
                                            LEFT JOIN vtiger_clientfuelrange ON vtiger_clientfuelrange.clientfuelrangeid=vtiger_crmentityrel.relcrmid
                                            LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid   
                                            LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=c.salesorderid                                              
                                            WHERE e.deleted = 0 AND (cf_1556 IS NULL OR cf_1556 = 0) AND ac.cf_1568 = 'Taikoma užsakymui'
                                            AND clientfuelrange_tks_minkurokai IS NOT NULL 
                                            AND clientfuelrange_tks_maxkurokai IS NOT NULL
                                            AND clientfuelrange_tks_priemoka IS NOT NULL
                                            AND fuel_entity.deleted=0
                                            GROUP BY c.salesorderid");

     $inv_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ?");                   

     // NOTE Pasiimam kuro kainas ir palyginam su kliento rėžiais
     $fuel_prices = $dbh->prepare("SELECT 
       FORMAT( (SELECT clientfuelrange_tks_priemoka 
           FROM vtiger_clientfuelrange  
           LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_clientfuelrange.clientfuelrangeid   
           LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid                 
           WHERE fuel_entity.deleted=0 AND ( ? BETWEEN fuelpricelist_tks_galiojimopra AND fuelpricelist_tks_galiojimopab) OR ? < fuelpricelist_tks_galiojimopab
           AND (fuelpricelist_tks_kurokaina BETWEEN clientfuelrange_tks_minkurokai AND clientfuelrange_tks_maxkurokai) AND (vtiger_crmentityrel.crmid = ?) ORDER BY clientfuelrangeid DESC LIMIT 1),0) as surcharge           
           FROM vtiger_fuelpricelist
           HAVING surcharge IS NOT NULL");

     $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id,productid,sequence_no,quantity,margin,service,inventory_type) VALUES(?,?,?,?,?,?,?)");      


     $clients_ranges->setFetchMode(PDO::FETCH_ASSOC);     
     $clients_ranges->execute(array());

     $fuel_prices->setFetchMode(PDO::FETCH_ASSOC);
     $inv_num->setFetchMode(PDO::FETCH_ASSOC);   
     
    // NOTE Pridedam paslauga kuro priemoka
    foreach($clients_ranges AS $row) {
        if($row['total'] > 0){                   
            $fuel_prices->execute(array($row['order_date'],$row['order_date'],$row['accountid']));
            $fuel_price = $fuel_prices->fetch();
            $total = number_format(($row['total']/100 * $fuel_price['surcharge']),2);
            $inv_num->execute(array($row['salesorderid']));
            $count = $inv_num->rowCount();
            $insert_service->execute(array($row['salesorderid'],'36641',$count+1,1,$total,8,'FuelSurcharge'));
            $update_fuel_price->execute(array($total, $row['salesorderid']));       
        }    
        
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
