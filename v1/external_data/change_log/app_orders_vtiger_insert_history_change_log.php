<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);
error_reporting(0);

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();     
    
    $sth5 = $dbh->prepare("INSERT INTO app_orders_update_temp (bill_city, bill_code, bill_street, load_company, load_phone, ship_city, ship_code, ship_street, unload_company, unload_phone, sostatus, shipment_code, load_date_to, load_time_to, unload_date_to, unload_time_to, cf_855, cf_928, cf_1504, cf_1564, cf_1566, cf_1558, cf_1560, cf_1562, salesorderid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


    $sth6 = $dbh->prepare('SELECT bill_city, bill_code, bill_street, load_company, load_phone, ship_city, ship_code, ship_street, unload_company, unload_phone, sostatus, shipment_code, load_date_to, load_time_to, unload_date_to, unload_time_to, cf_855, cf_928, cf_1504, cf_1564, cf_1566, cf_1558, cf_1560, cf_1562 FROM app_orders_update_temp WHERE salesorderid = ?');     


    while ($item = $sth4->fetch()) {
        $sth5->execute(array($item['bill_city'],$item['bill_code'],$item['bill_street'],$item['load_company'],$item['load_phone'],$item['ship_city'],$item['ship_code'],$item['ship_street'],$item['unload_company'],$item['unload_phone'],$item['sostatus'],$item['shipment_code'],$item['load_date_to'],$item['load_time_to'],$item['unload_date_to'],$item['unload_time_to'],$item['cf_855'],$item['cf_928'],$item['cf_1504'],$item['cf_1564'],$item['cf_1566'],$item['cf_1558'],$item['cf_1560'],$item['cf_1562'],$item['salesorderid']));
    }
    
     
     $sth7 = $dbh->prepare('SELECT load_date_to,load_time_to, unload_date_to, unload_time_to FROM vtiger_salesorder WHERE salesorderid = ?');           
     $sth7->setFetchMode(PDO::FETCH_ASSOC);
     
     $sth8 = $dbh->prepare('SELECT cf_855  FROM vtiger_salesordercf WHERE salesorderid = ?');           
     $sth8->setFetchMode(PDO::FETCH_ASSOC); 
                  
    $sth = $dbh->prepare("SELECT t.*,  c.salesorderid, ROUND(shipment_direct_km,2) AS shipment_km
                                FROM app_orders_update t                       
                                LEFT JOIN vtiger_salesorder c ON c.external_order_id = t.id 
                                LEFT JOIN vtiger_crmentity e ON e.crmid = c.salesorderid 
                                LEFT JOIN vtiger_invoice_salesorders_list sl ON sl.salesorderid=c.salesorderid                   
                                WHERE e.deleted = 0  AND sl.salesorderid IS NULL");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());
                                


    while ($row = $sth->fetch()) {

           
        $date = date("Y-m-d H:i:s"); 

        $sth6->execute(array($row['salesorderid']));
        $result = $sth6->fetch();

        $values = array('load_address' => 'bill_street', 'load_municipality' => 'bill_city', 'load_post_code' => 'bill_code','load_company_name' => 'load_company','unload_address' => 'ship_street', 'unload_municipality' => 'ship_city', 'unload_post_code' => 'ship_code','unload_company_name' => 'unload_company',
        'status' => 'sostatus', 'shipment_code' => 'shipment_code', 'shipment_km' => 'cf_928','load_company_phone' => 'cf_1564', 'unload_company_phone' => 'cf_1566', 'route_code' => 'cf_1558', 'vehicle_registration_number' => 'cf_1560', 'driver1_name' => 'cf_1562');
    
        
        foreach($values as $new => $old){           
            if(!empty($result[$old]) AND $row[$new] != $result[$old]){           
                $id = last_modtracker_record($dbh);
                insert_modtracker_basic($dbh, $id, $row['salesorderid'], 'SalesOrder', 26, $date,0);
                insert_modtracker_detail($dbh, $id, $old, $result[$old], $row[$new]);
            }
        }

        $sth7->execute(array($row['salesorderid']));
        $order = $sth7->fetch();

        $load_date_to = date("Y-m-d", strtotime($row['load_date_to']));
        $load_time_to = date("H:i:s", strtotime($row['load_date_to']));       
        
        $unload_date_to = date("Y-m-d", strtotime($row['unload_date_to']));
        $unload_time_to = date("H:i:s", strtotime($row['unload_date_to']));




            if($load_date_to != $order['load_date_to']){                
                $id = last_modtracker_record($dbh);
                insert_modtracker_basic($dbh, $id, $row['salesorderid'], 'SalesOrder', 26, $date,0);
                insert_modtracker_detail($dbh, $id, 'load_date_to', $order['load_date_to'], $load_date_to);
            }  

            if($load_time_to != $order['load_time_to']){                
                $id = last_modtracker_record($dbh);
                insert_modtracker_basic($dbh, $id, $row['salesorderid'], 'SalesOrder', 26, $date,0);
                insert_modtracker_detail($dbh, $id, 'load_time_to', $order['load_time_to'], $load_time_to);
            }

            if($unload_date_to != $order['unload_date_to']){                
                $id = last_modtracker_record($dbh);
                insert_modtracker_basic($dbh, $id, $row['salesorderid'], 'SalesOrder', 26, $date,0);
                insert_modtracker_detail($dbh, $id, 'unload_date_to', $order['unload_date_to'], $unload_date_to);
            }
            
            if($unload_time_to != $order['unload_time_to']){                
                $id = last_modtracker_record($dbh);
                insert_modtracker_basic($dbh, $id, $row['salesorderid'], 'SalesOrder', 26, $date,0);
                insert_modtracker_detail($dbh, $id, 'unload_time_to', $order['unload_time_to'], $unload_time_to);
            }
        
            $sth8->execute(array($row['salesorderid']));
            $ordercf = $sth8->fetch();

            $shipment_type = ($row['shipment_type'] == 'parcel' ? 'Transporto užsakymas' : 'Perkraustymo užsakymas');

            if(trim($shipment_type) != trim($ordercf['cf_855'])){                
                $id = last_modtracker_record($dbh);
                insert_modtracker_basic($dbh, $id, $row['salesorderid'], 'SalesOrder', 26, $date,0);
                insert_modtracker_detail($dbh, $id, 'cf_855', $ordercf['cf_855'], $shipment_type);
            }     
  
               
                    
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}


?>
