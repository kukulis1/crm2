<?php
include '../ws.config.php';
global $config;
global $root_directory;
require '../utils.php';

set_time_limit(500);


try {

    require('../mysql_connection.php');

    $dbh->beginTransaction(); 

    $query = $dbh->prepare(
    "   SELECT d.*,c.salesorderid         
        FROM app_order_documents d
        LEFT JOIN vtiger_salesorder c 
            ON c.external_order_id = d.shipment_id                                
        LEFT JOIN vtiger_crmentity e 
            ON e.crmid = c.salesorderid                                                                        
        WHERE e.deleted = 0 
            AND d.file_name NOT IN (
                SELECT vtiger_notes.title 
                FROM vtiger_notes
                JOIN vtiger_senotesrel
                    ON vtiger_senotesrel.notesid = vtiger_notes.notesid
                JOIN vtiger_crmentity note_entity
                    ON vtiger_notes.notesid = note_entity.crmid
                WHERE vtiger_senotesrel.crmid = e.crmid
                    AND note_entity.deleted = 0
            )
        GROUP BY d.shipment_id, file_id
    ");

     $query->setFetchMode(PDO::FETCH_ASSOC);     
     $query->execute(array());   

    
  $date_now = date('Y-m-d H:i:s');

  foreach($query AS $row) {
    // Suformuojam img url kuria irasysim
    $filename = "https://mobil.parnasas.lt:8084/api/download?file_id=".$row['file_id']."";
    $filetitle = ($row['file_name'] ?:$row['file_id']);
    $folderid = ($row['type_id'] == 'pod' ? 2 : 3); // Pagal id nustatom i kuri folderi irasysim
    $entity_id = get_and_insert_intity($dbh,'Documents', 26, NULL, 'Document', $date_now);
    insert_senotesrel($dbh, $entity_id, $row['salesorderid']);
    insertNotes($dbh,$entity_id,$filetitle,$filename,$row['file_description'],$folderid,'E',1);    
    insert_NoteCf($dbh, $entity_id,$row['file_created']);
  }
 
  $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
   sendReportMail($e->getMessage(),basename(__FILE__));
}

function insert_senotesrel($dbh, $entity_id, $moduleId)
{
  $sth = $dbh->prepare("INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES (?,?)");
  $sth->execute(array($moduleId, $entity_id));
}

function insertNotes($dbh, $entity_id,$title,$filename,$description,$folderid,$locationType,$filestatus)
{
  $sth = $dbh->prepare("INSERT INTO vtiger_notes (notesid, title, filename, notecontent, folderid, filelocationtype, filestatus) 
                           VALUES (?,?,?,?,?,?,?)");
  $sth->execute(array($entity_id, $title, $filename,$description, $folderid,$locationType,$filestatus));                   

}

function insert_NoteCf($dbh, $entity_id,$file_created)
{
  $sth = $dbh->prepare("INSERT INTO vtiger_notescf (notesid,cf_1852) VALUES (?, ?)");
  $sth->execute(array($entity_id, $file_created));
}


function get_and_insert_intity($dbh, $setype, $user_id, $description, $label, $date) {

  // Insert into vtiger_crmentity 
  $entity_id = get_last_entity_id_and_update($dbh,1); 

  if($entity_id){
    $sth2 = $dbh->prepare("SELECT * FROM vtiger_crmentity WHERE crmid = ?");
    $sth2->execute(array($entity_id));
    $crmnewid = $sth2->rowCount();
    
      
      // Insert into vtiger_crmentity 
     
      $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, source, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
      $sql_params = array($entity_id, $user_id, $user_id, $setype, $description,'Metrika', $label, $date, $date);

      if(!$crmnewid){
          $sth->execute($sql_params);
      }
      return  $entity_id;
  }
}