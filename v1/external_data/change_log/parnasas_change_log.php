<?php
include '../ws.config.php';
require '../utils.php';
set_time_limit(900);
error_reporting(0);

if(date("H") != '00'){

$date = date("Y-m-d");
$minutes = change_minutes(date("i"));
$time = date("H:").$minutes;
if(!file_exists("../logs/row_no/$date")){
  mkdir("../logs/row_no/$date", 0777, true); 
}
   
$logfile = "row_no/$date/$time.log";

customers_log('Scripto pradzia', $logfile);  

$ch1 = curl_init();
curl_setopt($ch1, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_change_log.php');
curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch1, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch1, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch1, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch1);
curl_close($ch1);

customers_log('Ikelta i temp', $logfile);  

$ch2 = curl_init();
curl_setopt($ch2, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_insert_change_log.php');
curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch2, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch2, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch2);
curl_close($ch2);

customers_log('Ikelti uzsakymai', $logfile); 

$ch3 = curl_init();
curl_setopt($ch3, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_insert_change_log.php');
curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch3, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch3, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch3, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch3);
curl_close($ch3);

customers_log('Ikelti kroviniai', $logfile); 

$ch4 = curl_init();
curl_setopt($ch4, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_new_insert_change_log.php');
curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch4, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch4, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch4, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch4);
curl_close($ch4);

customers_log('Ikelti nauji kroviniai', $logfile); 

$ch5 = curl_init();
curl_setopt($ch5, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_flag_vtiger_insert_change_log.php');
curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch5, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch5, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch5, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch5);
curl_close($ch5);


customers_log('Ikelti flagai', $logfile); 

$ch6 = curl_init();
curl_setopt($ch6, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_insert_packages.php');
curl_setopt($ch6, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch6, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch6, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch6, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch6);
curl_close($ch6);


customers_log('Ikelti paketai', $logfile); 

$ch7 = curl_init();
curl_setopt($ch7, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_add_docs.php');
curl_setopt($ch7, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch7, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch7, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch7, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch7);
curl_close($ch7);


customers_log('Ikelti dokumentai', $logfile); 


$ch9 = curl_init();
curl_setopt($ch9, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_add_document_from_terminal_url.php');
curl_setopt($ch9, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch9, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch9, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch9, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch9);
curl_close($ch9);


customers_log('Ikeltos nuotraukos is terminalo', $logfile); 

$ch10 = curl_init();
curl_setopt($ch10, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_delete_change_log.php');
curl_setopt($ch10, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch10, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch10, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch10, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch10);
curl_close($ch10);

customers_log('Istrinti uzsakymai', $logfile);

$ch11 = curl_init();
curl_setopt($ch11, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_delete_change_log.php');
curl_setopt($ch11, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch11, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch11, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch11, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch11);
curl_close($ch11);

customers_log('Istrinti kroviniai', $logfile);

$ch12 = curl_init();
curl_setopt($ch12, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_set_fuel_price.php');
curl_setopt($ch12, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch12, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch12, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch12, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch12);
curl_close($ch12);

customers_log('Nustatytos degalu kainos', $logfile);



$ch13 = curl_init();
curl_setopt($ch13, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_insert_history_change_log.php');
curl_setopt($ch13, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch13, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch13, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch13, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch13);
curl_close($ch13);

customers_log('Ikelta uzsakymo istorija', $logfile);

$ch14 = curl_init();
curl_setopt($ch14, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_insert_history_change_log.php');
curl_setopt($ch14, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch14, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch14, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch14, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch14);
curl_close($ch14);

customers_log('Ikelta krovinio istorija', $logfile);

// $ch15 = curl_init();
// curl_setopt($ch15, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_count_price_meter_change_log.php');
// curl_setopt($ch15, CURLOPT_SSL_VERIFYPEER, false);
// curl_setopt($ch15, CURLOPT_FRESH_CONNECT, true);
// curl_setopt($ch15, CURLOPT_TIMEOUT, 6000);
// curl_setopt($ch15, CURLOPT_CONNECTTIMEOUT, 900);
// curl_exec($ch15);
// curl_close($ch15);

// customers_log('Paskaiciuota metru kaina', $logfile);

$ch16 = curl_init();
curl_setopt($ch16, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_count_price_change_log.php');
curl_setopt($ch16, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch16, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch16, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch16, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch16);
curl_close($ch16);

customers_log('Paskaiciuota kaina', $logfile);


$ch17 = curl_init();
curl_setopt($ch17, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_add_stevedoring.php');
curl_setopt($ch17, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch17, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch17, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch17, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch17);
curl_close($ch17);

customers_log('Prideti krovos darbai - insert pabaiga', $logfile);


if(!file_exists("../logs/row_no2/$date")){
  mkdir("../logs/row_no2/$date", 0777, true); 
}

   
$logfile = "row_no2/$date/$time.log";

customers_log('Antro scripto pradzia', $logfile);  


$ch18 = curl_init();
curl_setopt($ch18, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_create_temp_change_log.php');
curl_setopt($ch18, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch18, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch18, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch18, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch18);
curl_close($ch18);

customers_log('Ikelti atnaujinti uzsakymai i temp', $logfile);  

$ch19 = curl_init();
curl_setopt($ch19, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_create_temp_change_log.php');
curl_setopt($ch19, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch19, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch19, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch19, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch19);
curl_close($ch19);


customers_log('Issaugota uzsakymo informacija', $logfile);  


$ch20 = curl_init();
curl_setopt($ch20, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_flag_vtiger_update_change_log.php');
curl_setopt($ch20, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch20, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch20, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch20, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch20);
curl_close($ch20);

customers_log('Atnaujinti flagai', $logfile);  

$ch21 = curl_init();
curl_setopt($ch21, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_update_change_log.php');
curl_setopt($ch21, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch21, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch21, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch21, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch21);
curl_close($ch21);

customers_log('Atnaujintas uzsakymas', $logfile);  

$ch22 = curl_init();
curl_setopt($ch22, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_update_change_log.php');
curl_setopt($ch22, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch22, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch22, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch22, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch22);
curl_close($ch22);

customers_log('Issaugota krovinio informacija', $logfile); 

$ch23 = curl_init();
curl_setopt($ch23, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_loads_vtiger_update_without_order_change_log.php');
curl_setopt($ch23, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch23, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch23, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch23, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch23);
curl_close($ch23);

customers_log('Issaugota atnaujinta krovinio informacija', $logfile); 

$ch24 = curl_init();
curl_setopt($ch24, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_add_updated_docs.php');
curl_setopt($ch24, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch24, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch24, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch24, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch24);
curl_close($ch24);

customers_log('Atnaujinti documentai', $logfile);  

// $ch25 = curl_init();
// curl_setopt($ch25, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_updated_count_price_meter_change_log.php');
// curl_setopt($ch25, CURLOPT_SSL_VERIFYPEER, false);
// curl_setopt($ch25, CURLOPT_FRESH_CONNECT, true);
// curl_setopt($ch25, CURLOPT_TIMEOUT, 6000);
// curl_setopt($ch25, CURLOPT_CONNECTTIMEOUT, 900);
// curl_exec($ch25);
// curl_close($ch25);

// $ch26 = curl_init();
// curl_setopt($ch26, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_updated_count_price_meter_change_log_with_out_order.php');
// curl_setopt($ch26, CURLOPT_SSL_VERIFYPEER, false);
// curl_setopt($ch26, CURLOPT_FRESH_CONNECT, true);
// curl_setopt($ch26, CURLOPT_TIMEOUT, 6000);
// curl_setopt($ch26, CURLOPT_CONNECTTIMEOUT, 900);
// curl_exec($ch26);
// curl_close($ch26);

// customers_log('Paskaiciuota metru kaina', $logfile);  

$ch27 = curl_init();
curl_setopt($ch27, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_updated_without_order_count_price_change_log.php');
curl_setopt($ch27, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch27, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch27, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch27, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch27);
curl_close($ch27);

customers_log('Paskaiciuota kaina loadu atnaujinime', $logfile);  

$ch28 = curl_init();
curl_setopt($ch28, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_updated_count_price_change_log.php');
curl_setopt($ch28, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch28, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch28, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch28, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch28);
curl_close($ch28);

$ch29 = curl_init();
curl_setopt($ch29, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_updated_count_price_change_log30.php');
curl_setopt($ch29, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch29, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch29, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch29, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch29);
curl_close($ch29);


$ch30 = curl_init();
curl_setopt($ch30, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_updated_count_price_change_log60.php');
curl_setopt($ch30, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch30, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch30, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch30, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch30);
curl_close($ch30);

$ch31 = curl_init();
curl_setopt($ch31, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/count_prices/app_orders_vtiger_updated_count_price_change_log90.php');
curl_setopt($ch31, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch31, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch31, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch31, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch31);
curl_close($ch31);


customers_log('Paskaiciuota kaina - antro scripto pabaiga', $logfile);  


$ch8 = curl_init();
curl_setopt($ch8, CURLOPT_URL, 'https://crm.parnasas.lt/v1/external_data/change_log/app_orders_vtiger_add_document_url.php');
curl_setopt($ch8, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch8, CURLOPT_FRESH_CONNECT, true);
curl_setopt($ch8, CURLOPT_TIMEOUT, 6000);
curl_setopt($ch8, CURLOPT_CONNECTTIMEOUT, 900);
curl_exec($ch8);
curl_close($ch8);


customers_log('Ikeltos nuotraukos is appso', $logfile); 

}