<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);
error_reporting(0);

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();

    $sth6 = $dbh->prepare("TRUNCATE app_orders_update_temp");
    $sth6->execute(array());

    $sth4 = $dbh->prepare("SELECT DISTINCT bill.bill_city, bill.bill_code, bill.bill_street, bill.load_company, bill.load_phone, ship.ship_city, ship.ship_code, ship.ship_street, ship.unload_company, ship.unload_phone, c.sostatus,c.shipment_code,c.load_date_to,c.load_time_to, c.unload_date_to, c.unload_time_to,cf_855, cf_928, cf_1504,cf_1564,cf_1566,cf_1558,cf_1560,cf_1562,c.salesorderid
                        FROM app_orders_update t                       
                        LEFT JOIN vtiger_salesorder c ON c.external_order_id = t.id 
                        LEFT JOIN vtiger_sobillads bill ON bill.sobilladdressid=c.salesorderid 
                        LEFT JOIN vtiger_soshipads ship ON ship.soshipaddressid=c.salesorderid 
                        LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid
                        LEFT JOIN vtiger_crmentity e ON e.crmid = c.salesorderid 
                        LEFT JOIN vtiger_invoice_salesorders_list sl ON sl.salesorderid=c.salesorderid                   
                        WHERE  e.deleted = 0  AND sl.salesorderid IS NULL
                        GROUP BY c.salesorderid");
    
    $sth4->setFetchMode(PDO::FETCH_ASSOC);
    $sth4->execute(array());
    
    
    $sth5 = $dbh->prepare("INSERT INTO app_orders_update_temp (bill_city, bill_code, bill_street, load_company, load_phone, ship_city, ship_code, ship_street, unload_company, unload_phone, sostatus, shipment_code, load_date_to, load_time_to, unload_date_to, unload_time_to, cf_855, cf_928, cf_1504, cf_1564, cf_1566, cf_1558, cf_1560, cf_1562, salesorderid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    $date = date("Y-m-d");


    while ($item = $sth4->fetch()) {
        $sth5->execute(array($item['bill_city'],$item['bill_code'],$item['bill_street'],$item['load_company'],$item['load_phone'],$item['ship_city'],$item['ship_code'],$item['ship_street'],$item['unload_company'],$item['unload_phone'],$item['sostatus'],$item['shipment_code'],$item['load_date_to'],$item['load_time_to'],$item['unload_date_to'],$item['unload_time_to'],$item['cf_855'],$item['cf_928'],$item['cf_1504'],$item['cf_1564'],$item['cf_1566'],$item['cf_1558'],$item['cf_1560'],$item['cf_1562'],$item['salesorderid'])); 
    }     

      
         
               
                    
    
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}


?>
