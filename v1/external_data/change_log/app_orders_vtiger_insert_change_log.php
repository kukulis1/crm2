<?php

include '../ws.config.php';
global $config;
require '../utils.php';
set_time_limit(500);
error_reporting(1);

try {

    require('../mysql_connection.php');
        
    $dbh->beginTransaction();
    // NOTE Paruosiama uzklausa įrašyti užsakymą
    $sth2 = $dbh->prepare('INSERT INTO vtiger_salesorder (salesorderid,
                                                         subject,
                                                         salesorder_no,
                                                         shipment_code,                                                               
                                                         accountid,
                                                         sostatus,                                                                              
                                                         load_date_from, 
                                                         load_time_from,
                                                         load_date_to, 
                                                         load_time_to,                                                               
                                                         unload_date_from, 
                                                         unload_time_from,
                                                         unload_date_to, 
                                                         unload_time_to, 
                                                         route_point_id,                                                                
                                                         external_order_id,
                                                         source,
                                                         import_date,
                                                         created_person) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');    
  
     // NOTE Pagal metrikos shipment_id app_orders_change_log lentele sujungiama su vtiger_salesorder, patikrinama ar nėra tokio užsakymo crm'e
    $sth = $dbh->prepare("SELECT DISTINCT t.*, a.accountid, ac.cf_1659 AS user_id, e.smownerid as user_id2, t.load_post_code,  t.unload_post_code, SUM(l.weight)  AS weight, SUM(l.length * l.width * l.height) as volume, l.m3, l.pll,ROUND(shipment_direct_km,2) AS shipment_km, t.customer_order_code
                                    FROM app_orders_change_log t
                                    JOIN vtiger_account a ON a.customer_id = t.customer_id  
                                    JOIN vtiger_accountscf ac ON ac.accountid = a.accountid
                                    JOIN app_loads_change_log l ON l.order_id = t.id 
                                    JOIN vtiger_crmentity e ON e.crmid = a.accountid                                              
                                    WHERE e.deleted = 0 AND t.id NOT IN (SELECT c.external_order_id FROM vtiger_salesorder c, vtiger_crmentity d WHERE  d.crmid = c.salesorderid AND d.deleted = 0 AND c.external_order_id IS NOT NULL)                       
                                    GROUP BY l.order_id
                                    ORDER BY t.app_id");


     // NOTE Pasiruosiama įrašyti papildoma info
    $insert_salesorder_cf = $dbh->prepare('INSERT INTO vtiger_salesordercf (salesorderid, cf_855,cf_928, cf_1504, cf_1558, cf_1560, cf_1562, cf_1564, cf_1566, cf_1661, cf_1663, cf_2032) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');      


    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());
    $orders_count = $sth->rowCount(); // Suskaiciuojam kiek yra nauju užsakymu

    if($orders_count > 0){     

      $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");       
      
      $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count); 

      while ($row = $sth->fetch()) {   

         // Patikrinam ar visgi neatsirado kito iraso kuris turi si id
         $sth6->execute(array($last_entity_record)); 
         $record_exist = $sth6->rowCount(); 

         // Jei kazkas turi ta id gaunam nauja
         if($record_exist) {
            $last_entity_record_old = $last_entity_record;
            $sth3->execute(array());
            $seq = $sth3->fetch();
            $last_entity_record = $last_entity_record+1;
            $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
            $sth4->execute(array($last_entity_record));     
         }
            // Pasiruosiam informacija kuria rasysim
            $id = $row['accountid'];
            $weight = $row['weight'];
            $volume = ($row['m3'] > $row['volume'] ? $row['m3'] : $row['volume']);
            $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
            $shipment_type = ($row['shipment_type'] == 'parcel' ? 'Transporto užsakymas' : 'Perkraustymo užsakymas');
            $row['user_id']  = (!empty($row['user_id']) ? $row['user_id'] : !empty($row['user_id2'] ? $row['user_id2'] : 26));
               
               // Sutvarkomos datos
               $load_date_from = date("Y-m-d", strtotime($row['load_date_from']));
               $load_date_to = date("Y-m-d", strtotime($row['load_date_to']));
               $load_time_from = date("H:i:s", strtotime($row['load_date_from']));
               $load_time_to = date("H:i:s", strtotime($row['load_date_to']));                        
               $unload_date_from = date("Y-m-d", strtotime($row['unload_date_from']));
               $unload_date_to = date("Y-m-d", strtotime($row['unload_date_to']));
               $unload_time_from = date("H:i:s", strtotime($row['unload_date_from']));
               $unload_time_to = date("H:i:s", strtotime($row['unload_date_to']));                
              
               $subject = $row['shipment_code'];  
               $source = 'Metrika';             
               $date = date("Y-m-d H:i:s"); 

               if(!empty($row['order_date'])){ 
                  $order_date = date("Y-m-d", strtotime($row['order_date']))." ".date("H:i:s");
               }else{
                  $order_date = date("Y-m-d H:i:s");
               }            

                 
                  // Uzpildoma pragrindine crm lentele
                  $insert_entity = insert_entity3($dbh, 'SalesOrder', $last_entity_record, $row['user_id'], NULL,'Metrika',  $row['shipment_code'], $date);                 
                  
                  // Irasomas uzsakymas
                  $sth2->execute(array($last_entity_record, 
                                       $subject, 
                                       $row['id'],                                      
                                       $row['shipment_code'],                                      
                                       $row['accountid'], 
                                       'Sent',                                        
                                       $load_date_from,                       
                                       $load_time_from,
                                       $load_date_to,
                                       $load_time_to,
                                       $unload_date_from,      
                                       $unload_time_from, 
                                       $unload_date_to,      
                                       $unload_time_to,
                                       $row['route_point_id'],                    
                                       $row['id'],                     
                                       $source,
                                       $date,
                                       $row['person_name']            
                  ));

                  // Irasoma papildoma uzskymo informacija
                  $insert_salesorder_cf->execute(array($last_entity_record,
                                                       $shipment_type,
                                                       $distance,
                                                       $order_date,
                                                       $row['route_code'],
                                                       $row['vehicle_registration_number'],
                                                       $row['driver1_name'],
                                                       $row['load_company_phone'],
                                                       $row['unload_company_phone'],
                                                       26,
                                                       26,
                                                       $row['customer_order_code']
                                                      ));

                     // Uzpildomos pasikrovimo ir issikrovimo lenteles
                     $insert_load_address = insert_load_address($dbh, $last_entity_record, $row['load_address'], $row['load_post_code'], $row['load_municipality'], $row['load_country_code'], $row['load_company_name'], $row['load_company_code'], $row['load_company_contact'], $row['load_company_phone']);
                     $insert_unload_address = insert_unload_address($dbh, $last_entity_record, $row['unload_address'], $row['unload_post_code'], $row['unload_municipality'], $row['unload_country_code'], $row['unload_company_name'], $row['unload_company_code'], $row['unload_company_contact'], $row['unload_company_phone']);     
                     
   

         if($record_exist) {             
            $last_entity_record = $last_entity_record_old;    
         }         

         $last_entity_record++;
      }
      
   }

   $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error!";
   echo $e->getMessage();
   // NOTE Jei gaunama klaida siunciamas laiskas administratoriui
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
