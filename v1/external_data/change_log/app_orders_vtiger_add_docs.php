<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);


try {

    require('../mysql_connection.php');

    $dbh->beginTransaction(); 

     // NOTE Susirenkam flagus ir apmokestinima pagal klientui nustatyta kaina kliento korteleje
     $flag_query = $dbh->prepare("SELECT f.minifest,f.cmr,f.invoice,f.order_id,f.load_id,c.salesorderid, cf_1812 as doc_service           
        FROM app_load_flags f
        LEFT JOIN vtiger_salesorder c ON c.external_order_id = f.order_id                                   
        LEFT JOIN vtiger_accountscf ac ON ac.accountid = c.accountid  
        LEFT JOIN vtiger_crmentity e ON e.crmid = ac.accountid                                   
        LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=c.salesorderid                                              
        WHERE e.deleted = 0 AND cf_1812 > 0
        GROUP BY f.load_id");

      // NOTE Gaunam visus flag'us, kad galetume greitai patikrinti kurie yra crm
     $get_all_flags = $dbh->prepare("SELECT external_order_id, external_load_id FROM vtiger_inventoryproductrel_flags ORDER BY flag_id DESC");  
     $get_all_flags->setFetchMode(PDO::FETCH_ASSOC);     
     $get_all_flags->execute(array());

     $flags = [];     
     foreach ($get_all_flags as $flag) {
        $flags[$flag['external_order_id']][$flag['external_load_id']] = $flag['external_load_id'];
     }


     $inv_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ?");      
     $check_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE external_load_id = ? AND productid = 36641");      
     $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id,external_order_id,external_load_id,productid,sequence_no,quantity,margin,service,inventory_type,source) VALUES(?,?,?,?,?,?,?,?,?,?)");  
    
     $flag_query->setFetchMode(PDO::FETCH_ASSOC);     
     $flag_query->execute(array());
     $inv_num->setFetchMode(PDO::FETCH_ASSOC);   

     $docsArr = array();
     $data = array();

     // NOTE Suformuojamas array su užsakymo id, paslaugomis ir apmoketinima suma
     foreach($flag_query AS $row) {
        $docsArr[$row['salesorderid']][] = ($row['minifest'] == 1 || $row['minifest'] == 0) ? $row['minifest'] : 1;
        $docsArr[$row['salesorderid']][] = ($row['cmr'] == 1 || $row['cmr'] == 0) ? $row['cmr'] : 1;
        $docsArr[$row['salesorderid']][] = ($row['invoice'] == 1 || $row['invoice'] == 0) ? $row['invoice'] : 1;
        $docs = array_sum($docsArr[$row['salesorderid']]);
        $data[$row['salesorderid']] =  array('salesorderid' => $row['salesorderid'],
                                             'order_id' => $row['order_id'], 
                                             'load_id' => $row['load_id'], 
                                             'docs' => $docs, 
                                             'charge' => $row['doc_service']);        
     }

     // NOTE Irasomos paslaugos
     foreach($data AS $item) {
         if($item['docs'] > 0){
            if(empty($flags[$item['order_id']][$item['load_id']])){      
                for($i=0; $i<$item['docs']; $i++){                 
                    $inv_num->execute(array($item['salesorderid']));
                    $count = $inv_num->rowCount();                      
                    $insert_service->execute(array($item['salesorderid'],$item['order_id'],$item['load_id'],'36641',$count+1,1,$item['charge'],12,'documents','Metrika'));                                                     
                }    
            }            
            
         }
     }    
     
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    // sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
