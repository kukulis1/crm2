<?php
set_time_limit(500);
error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

try {

      require('../mysql_connection.php');

      // NOTE Pasiimamas paskutinio sėkmingo kreipimosi į metriką laikas
      $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
      $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
      $last_timestamp->execute(array());
      $date_from = $last_timestamp->fetch();
      $date_from = $date_from['date'];
      $newTime = strtotime($date_from.'-5 minutes'); // Metrikos laikas atsilieka 3min, todėl iš gauto laiko apsidraudimui atimam 5min
      $date_from = date('Y-m-d H:i:s', $newTime);      

      $now = date("Y-m-d H:i");

      $minute = date("i", strtotime($now));
    
      $minutes = change_minutes($minute); // Suapvalinam minutes, apsidraudimui jei kreipimasis vėlavo.
    
      $time = date("Y-m-d H:$minutes:00", strtotime($now));  // Gaunam laiką kurį įrašysim į last_update lentelę    
      
    // NOTE Išvalom laikinas lenteles
    $sth = $dbh->prepare('TRUNCATE app_orders_change_log');
    $sth->execute(array());    
    
    $sth = $dbh->prepare('TRUNCATE app_loads_change_log');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_additional_services_change_log');
    $sth->execute(array());
    
    $sth = $dbh->prepare('TRUNCATE app_orders_update');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_loads_update');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_loads_update_without_order');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_loads_new');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_orders_deleted');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_loads_deleted');
    $sth->execute(array());
    
    $sth = $dbh->prepare('TRUNCATE app_load_flags');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_load_update_flags');
    $sth->execute(array());

    $sth = $dbh->prepare('TRUNCATE app_shipment_packages');
    $sth->execute(array());
    
    $sth = $dbh->prepare('TRUNCATE app_shipment_packages_updated');
    $sth->execute(array());    

    $sth = $dbh->prepare('TRUNCATE app_order_documents');
    $sth->execute(array());

    // NOTE Gaunam bendrus nustatymus kada krovinys nestandartas
    $check = $dbh->prepare("SELECT `value` FROM app_other_settings WHERE title = 'LBL_CARGO_NOT_STANDART'");    
    $check->setFetchMode(PDO::FETCH_ASSOC);
    $check->execute(array());

    $meter = $check->fetch();
    $taxablemeter = $meter['value'];  

    // NOTE Gaunam kliento nustatyta nestandarto ilgį
    $user_m = $dbh->prepare("SELECT cf_1635 as meters 
                              FROM vtiger_account a
                              LEFT JOIN vtiger_accountscf cf ON cf.accountid=a.accountid 
                              WHERE a.customer_id = ? AND cf_1635 IS NOT NULL");  
    $user_m->setFetchMode(PDO::FETCH_ASSOC);

    $user_m2 = $dbh->prepare("SELECT cf_1635 as meters 
                              FROM vtiger_salesorder s
                              LEFT JOIN vtiger_account a ON s.accountid=a.accountid
                              LEFT JOIN vtiger_accountscf cf ON cf.accountid=a.accountid 
                              WHERE external_order_id = ? AND cf_1635 IS NOT NULL"); 

    $user_m2->setFetchMode(PDO::FETCH_ASSOC);


    // NOTE Paruosiamos užklausos duomenims saugoti
    $sth2 = $dbh->prepare('INSERT IGNORE INTO app_orders_change_log  (
                                                            id,
                                                            shipment_type,
                                                            shipment_code,
                                                            customer_id,
                                                            order_date,
                                                            person_name,
                                                            status,
                                                            route_point_id,
                                                            price,
                                                            price_agreed,
                                                            shipment_direct_km,
                                                            load_company_code,
                                                            load_company_name,
                                                            load_company_contact,
                                                            load_company_phone,                                                          
                                                            load_address,
                                                            load_settlement,
                                                            load_municipality,
                                                            load_post_code,
                                                            load_country_code,
                                                            load_date_from,
                                                            load_date_to,
                                                            unload_company_code,
                                                            unload_company_name,
                                                            unload_company_contact,
                                                            unload_company_phone,                                                          
                                                            unload_address,
                                                            unload_settlement,
                                                            unload_municipality,
                                                            unload_post_code,
                                                            unload_country_code,
                                                            unload_date_from,
                                                            unload_date_to,
                                                            update_metrika,
                                                            import_date,
                                                            route_code,
                                                            driver1_name,
                                                            vehicle_registration_number,
                                                            customer_order_code
                                                          ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)');
    
    $sth3 = $dbh->prepare('INSERT IGNORE INTO app_loads_change_log ( order_id,
                                                          id,
                                                          quantity,
                                                          quantity_revised,
                                                          measure,
                                                          measure_revised,
                                                          weight,
                                                          length,
                                                          width,
                                                          height,
                                                          m3,
                                                          pll,
                                                          note,
                                                          document,
                                                          ean,
                                                          import_date,
                                                          ordered,
                                                          revised,
                                                          meters                                                       	 
                                                        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');


    $sth4 = $dbh->prepare('INSERT IGNORE INTO app_additional_services_change_log ( order_id,
                                                        type_id,
                                                        quantity,
                                                        price,
                                                        note,
                                                        import_date                                                   	 
                                                      ) VALUES (?, ?, ?, ?, ?, ?)');

    $sth5 = $dbh->prepare("INSERT INTO app_last_update  (status,date) VALUES (?,?)");
   
    $sth6 = $dbh->prepare('INSERT IGNORE INTO app_orders_update (
                                                                  id,
                                                                  shipment_type,
                                                                  shipment_code,
                                                                  customer_id,
                                                                  order_date,
                                                                  status,
                                                                  route_point_id,
                                                                  price,
                                                                  price_agreed,
                                                                  shipment_direct_km,
                                                                  load_company_code,
                                                                  load_company_name,
                                                                  load_company_contact,
                                                                  load_company_phone,                                                          
                                                                  load_address,
                                                                  load_settlement,
                                                                  load_municipality,
                                                                  load_post_code,
                                                                  load_country_code,
                                                                  load_date_from,
                                                                  load_date_to,
                                                                  unload_company_code,
                                                                  unload_company_name,
                                                                  unload_company_contact,
                                                                  unload_company_phone,                                                          
                                                                  unload_address,
                                                                  unload_settlement,
                                                                  unload_municipality,
                                                                  unload_post_code,
                                                                  unload_country_code,
                                                                  unload_date_from,
                                                                  unload_date_to,
                                                                  update_metrika,
                                                                  import_date,
                                                                  route_code,
                                                                  driver1_name,
                                                                  vehicle_registration_number
                                                                  ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)');


    $sth7 = $dbh->prepare('INSERT IGNORE INTO app_loads_update (  id,
                                                                        ordered_quantity,
                                                                        quantity,                                                  
                                                                        measure,
                                                                        measure_revised,
                                                                        weight,
                                                                        length,
                                                                        width,
                                                                        height,
                                                                        m3,
                                                                        pll,
                                                                        note,
                                                                        document,
                                                                        ean,
                                                                        import_date,
                                                                        ordered,
                                                                        revised,
                                                                        shipment_id,
                                                                        meters) 
                                                                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?)');

$insert_load_without_order = $dbh->prepare('INSERT IGNORE INTO app_loads_update_without_order (  id,
                                                          ordered_quantity,
                                                          quantity,
                                                          measure,
                                                          measure_revised,
                                                          weight,
                                                          length,
                                                          width,
                                                          height,
                                                          m3,
                                                          pll,
                                                          note,
                                                          document,
                                                          ean,
                                                          import_date,
                                                          ordered,
                                                          revised,
                                                          shipment_id,
                                                          meters                                                       	 
                                                          ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?,?)');                                                                   


    $sth8 = $dbh->prepare('INSERT IGNORE INTO app_loads_new (  order_id,
                                                                    id,
                                                                    quantity,
                                                                    quantity_revised,
                                                                    measure,
                                                                    measure_revised,
                                                                    weight,
                                                                    length,
                                                                    width,
                                                                    height,
                                                                    m3,
                                                                    pll,
                                                                    note,
                                                                    document,
                                                                    ean,
                                                                    import_date,
                                                                    ordered,
                                                                    revised                                                       	 
                                                                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

    $sth9 = $dbh->prepare('INSERT IGNORE INTO app_orders_deleted (order_id) VALUES (?)');
    $sth10 = $dbh->prepare('INSERT IGNORE INTO app_loads_deleted (load_id) VALUES (?)');


    
    $sth11 = $dbh->prepare('INSERT IGNORE INTO app_load_flags (order_id, load_id, termo, minifest, cmr, invoice, palettes, other_tare) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');

    $sth12 = $dbh->prepare('INSERT IGNORE INTO app_load_update_flags (order_id, load_id, termo, minifest, cmr, invoice, palettes, other_tare) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');

    $sth13 = $dbh->prepare("INSERT IGNORE INTO app_order_documents (shipment_id,file_id,type_id,file_name,file_description,file_created ,timestamp) VALUES (?, ?, ?, ?, ?, ?, ?)");

    $sth14 = $dbh->prepare('INSERT INTO app_shipment_packages (load_id, package_id, package_code, seq_num, tare_type_id, weight_kg, length_m, width_m, height_m, damage_type_id, location_nme, comments, entry_date, edited_date, vvs_package_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');

    $sth15 = $dbh->prepare('INSERT INTO app_shipment_packages_updated (load_id, package_id, package_code, seq_num, tare_type_id, weight_kg, length_m, width_m, height_m, damage_type_id, location_nme, comments, entry_date, edited_date, vvs_package_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');	  
                    
                // NOTE Kreipiamasi į savitarnos API su data
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders_changes.php?routes=1");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 300);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                $result = curl_exec($ch);
                curl_close($ch);   

                $data = json_decode($result,true); 


                $values = array();    

                // NOTE Patikrinama ar yra įrašu
                if(!empty($data['response']['new_shipments']) ){
                  $values[] = 'new_shipments';
                } 
                if(!empty($data['response']['updated_shipments'])){
                  $values[] = 'updated_shipments';
                }
                if(!empty($data['response']['deleted_shipments'])){
                  $values[] = 'deleted_shipments';
                } 
                if(!empty($data['response']['new_loads'])){
                  $values[] = 'new_loads';
                }
                if(!empty($data['response']['updated_loads'])){
                  $values[] = 'updated_loads';
                }
                if(!empty($data['response']['deleted_loads'])){
                  $values[] = 'deleted_loads';
                }
                if(!empty($data['response']['images'])){
                  $values[] = 'images';
                }
                if(!empty($data['response']['new_shipment_packages'])){
                  $values[] = 'new_shipment_packages';
                }

                if(!empty($data['response']['edited_shipment_packages'])){
                  $values[] = 'edited_shipment_packages';
                }
                
              
                if(empty($values)){
                  die();
                }


                $state = $data['state'];                
               
                // NOTE Klaidos atveju įrašoma į log'ą
                if($state == 'ERR') {          
                    $logfile = 'app_orders.log';
                    customers_log($data, $logfile);
                }

       // NOTE Įrašomi užsakymai su kroviniais
       if(!empty($data['response']['new_shipments'])){    

            foreach($data['response']['new_shipments'] as $key => $row) {
            
                 	  $row['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$row['load_post_code']);
       				      $row['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$row['unload_post_code']);

                	  $row['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['load_post_code']);
                	  $row['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['unload_post_code']);

                    $date = date("Y-m-d H:i:s");

                                                $sth2->execute(array($row['id'],
                                                        $row['shipment_type'], 
                                                        $row['shipment_code'],                                                          
                                                        $row['customer_id'],
                                                        $row['order_date'],
                                                        $row['person_name'],
                                                        $row['status'],
                                                        $row['route_point_id'],
                                                        $row['price'],
                                                        $row['price_agreed'],
                                                        $row['shipment_direct_km'],
                                                        $row['load_company_code'],
                                                        $row['load_company_name'],
                                                        $row['load_company_contact'],
                                                        $row['load_company_phone'],                                                            
                                                        $row['load_address'],
                                                        $row['load_settlement'],
                                                        $row['load_municipality'],
                                                        $row['load_post_code'],
                                                        $row['load_country_code'],
                                                        $row['load_date_from'],
                                                        $row['load_date_to'],
                                                        $row['unload_company_code'],
                                                        $row['unload_company_name'],
                                                        $row['unload_company_contact'],
                                                        $row['unload_company_phone'],                                                            
                                                        $row['unload_address'],
                                                        $row['unload_settlement'],
                                                        $row['unload_municipality'],
                                                        $row['unload_post_code'],
                                                        $row['unload_country_code'],
                                                        $row['unload_date_from'],
                                                        $row['unload_date_to'], 
                                                        $row['update_date'],                                                  
                                                        $date,
                                                        $row['route_code'],                                                            
                                                        $row['driver1_name'],
                                                        $row['vehicle_registration_number'],
                                                        $row['customer_order_code']                                                            
                                                 ));
   
              

              $ordered_meters_arr = array();
              $revised_meters_arr = array();

                foreach($row['load'] as $key => $row2) {  

                  $user_m->execute(array($row['customer_id']));
          
                  $user = $user_m->fetch();
                  $taxablemeter2 = $user['meters'];                

                  $zero = 0;
                  $ordered = array();
                  $revised = array();    
                  if(empty($row2['ordered_weight'])) $row2['ordered_weight'] = 0;  
                  if(empty($row2['ordered_length'])) $row2['ordered_length'] = 0;  
                  if(empty($row2['ordered_width'])) $row2['ordered_width'] = 0;  
                  if(empty($row2['ordered_height'])) $row2['ordered_height'] = 0;  

                  if(empty($row2['revised_weight'])) $row2['revised_weight'] = 0;  
                  if(empty($row2['revised_length'])) $row2['revised_length'] = 0;  
                  if(empty($row2['revised_width'])) $row2['revised_width'] = 0;  
                  if(empty($row2['revised_height'])) $row2['revised_height'] = 0;  
                  
                 $ordered = $row2['ordered_weight']." ".$row2['ordered_length']."x".$row2['ordered_width']."x".$row2['ordered_height'];
                 $revised = $row2['revised_weight']." ".$row2['revised_length']."x".$row2['revised_width']."x".$row2['revised_height'];


                 if($row2['ordered_weight'] > $row2['revised_weight']){
                  $weight = $row2['ordered_weight'];
                 }else{
                  $weight = $row2['revised_weight'];
                 }

                 $ordered_m3 = $row2['ordered_length'] * $row2['ordered_width'] * $row2['ordered_height'];
                 $revised_m3 = $row2['revised_length'] * $row2['revised_width'] * $row2['revised_height'];

                 $ordered_meters_arr[] = $row2['ordered_length'];
                 $ordered_meters_arr[] = $row2['ordered_width'];     

                 $revised_meters_arr[] = $row2['revised_length'];
                 $revised_meters_arr[] = $row2['revised_width'];   

                 if($ordered_m3 < $revised_m3){
                  $row2['ordered_length'] = $row2['revised_length'];
                  $row2['ordered_width'] = $row2['revised_width'];
                  $row2['ordered_height'] =$row2['revised_height'];
                 }
                     

                 $max_ordered_meter = max($ordered_meters_arr);
                 $max_revised_meter = max($revised_meters_arr);

                 $max_meter = ($max_ordered_meter > $max_revised_meter ? $max_ordered_meter : $max_revised_meter);
                 $max_taxablemeter = ($taxablemeter2 == 0 || $taxablemeter2 == '' ? $taxablemeter : $taxablemeter2);

                 $meters = ($max_meter > $max_taxablemeter ? $max_meter : NULL);       

  
                      $sth3->execute(array($row['id'],
                        $row2['load_id'],
                        $row2['ordered_quantity'],
                        $row2['revised_quantity'],
                        $row2['ordered_measure'],
                        $row2['revised_measure'],
                        $weight,
                        $row2['ordered_length'],
                        $row2['ordered_width'],
                        $row2['ordered_height'],
                        $row2['m3'],
                        $row2['pll'],
                        $row2['note'],
                        $row2['documents'], 
                        $row2['ean'],                                                     
                        $date ,
                        $ordered,
                        $revised,
                        $meters                                                                                     
                      ));           
              
                        $sth11->execute(array(
                            $row['id'],
                            $row2['load_id'],
                            $row2['flags']['flg_termo'],
                            $row2['flags']['flg_minifest'],
                            $row2['flags']['flg_crm'],                                                                                               
                            $row2['flags']['flg_invoice'],                                                                                               
                            $row2['flags']['flg_palettes'],                                                                                               
                            $row2['flags']['flg_other_tare']                                                                                            
                        ));                  
                                                                                     
                  }   

                 foreach($row['additional_services'] as $key => $row3) {  

                    $sth4->execute(array($row['id'],
                        $row3['service_type_id'],
                        $row3['quantity_agreed'],
                        $row3['price_agreed'],
                        $row3['remarks'],                                                                      
                        $date                                                                                    
                    ));
      
                  }            
                  

            }

      }

          // NOTE Įrašomi atnaujinti užsakymai ir kroviniai
          if(!empty($data['response']['updated_shipments'])){ 

            foreach($data['response']['updated_shipments'] as $key => $row) {
              $row['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$row['load_post_code']);
              $row['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$row['unload_post_code']);

              $row['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['load_post_code']);
              $row['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['unload_post_code']);

              $date = date("Y-m-d H:i:s");

                                            $sth6->execute(array($row['id'],
                                                 $row['shipment_type'], 
                                                 $row['shipment_code'],                                                          
                                                 $row['customer_id'],
                                                 $row['order_date'],
                                                 $row['status'],
                                                 $row['route_point_id'],
                                                 $row['price'],
                                                 $row['price_agreed'],
                                                 $row['shipment_direct_km'],
                                                 $row['load_company_code'],
                                                 $row['load_company_name'],
                                                 $row['load_company_contact'],
                                                 $row['load_company_phone'],                                                            
                                                 $row['load_address'],
                                                 $row['load_settlement'],
                                                 $row['load_municipality'],
                                                 $row['load_post_code'],
                                                 $row['load_country_code'],
                                                 $row['load_date_from'],
                                                 $row['load_date_to'],
                                                 $row['unload_company_code'],
                                                 $row['unload_company_name'],
                                                 $row['unload_company_contact'],
                                                 $row['unload_company_phone'],                                                            
                                                 $row['unload_address'],
                                                 $row['unload_settlement'],
                                                 $row['unload_municipality'],
                                                 $row['unload_post_code'],
                                                 $row['unload_country_code'],
                                                 $row['unload_date_from'],
                                                 $row['unload_date_to'], 
                                                 $row['edit_date'],                                                  
                                                 $date,
                                                 $row['route_code'],  
                                                 $row['driver1_name'],  
                                                 $row['vehicle_registration_number']                                                        
                                          ));



                if(!empty($row['load'])){ 

                  foreach($row['load'] as $row2) {                

                        $ordered_meters_arr = array();
                        $revised_meters_arr = array();

                        $zero = 0;
                        $ordered = array();
                        $revised = array();    
                          if(empty($row2['ordered_weight'])) $row2['ordered_weight'] = 0;  
                          if(empty($row2['ordered_length'])) $row2['ordered_length'] = 0;  
                          if(empty($row2['ordered_width'])) $row2['ordered_width'] = 0;  
                          if(empty($row2['ordered_height'])) $row2['ordered_height'] = 0;  

                          if(empty($row2['revised_weight'])) $row2['revised_weight'] = 0;  
                          if(empty($row2['revised_length'])) $row2['revised_length'] = 0;  
                          if(empty($row2['revised_width'])) $row2['revised_width'] = 0;  
                          if(empty($row2['revised_height'])) $row2['revised_height'] = 0;                            

                          $ordered = $row2['ordered_weight']." ".$row2['ordered_length']."x".$row2['ordered_width']."x".$row2['ordered_height'];
                          $revised = $row2['revised_weight']." ".$row2['revised_length']."x".$row2['revised_width']."x".$row2['revised_height'];


                          $ordered_meters_arr[] = $row2['ordered_length'];
                          $ordered_meters_arr[] = $row2['ordered_width'];     

                          $revised_meters_arr[] = $row2['revised_length'];
                          $revised_meters_arr[] = $row2['revised_width'];       
                              
                          $user_m2->execute(array($row2['shipment_id']));
                    
                          $user2 = $user_m2->fetch();
                          $taxablemeter2 = $user2['meters'];                
                    
                          $max_ordered_meter = max($ordered_meters_arr);
                          $max_revised_meter = max($revised_meters_arr);       

                          $max_meter = ($max_ordered_meter > $max_revised_meter ? $max_ordered_meter : $max_revised_meter);
                          $max_taxablemeter = ($taxablemeter2 == 0 || $taxablemeter2 == '' ? $taxablemeter : $taxablemeter2);

                          $meters = ($max_meter > $max_taxablemeter ? $max_meter : NULL);  
          
                          $sth7->execute(array(
                                                $row2['load_id'],
                                                $row2['ordered_quantity'],
                                                $row2['revised_quantity'],
                                                $row2['ordered_measure'],
                                                $row2['revised_measure'],
                                                $row2['revised_weight'],
                                                $row2['revised_length'],
                                                $row2['revised_width'],
                                                $row2['revised_height'],
                                                $row2['m3'],
                                                $row2['pll'],
                                                $row2['note'],
                                                $row2['documents'], 
                                                $row2['ean'],                                                     
                                                $date ,
                                                $ordered,
                                                $revised,
                                                $row['id'],
                                                $meters                                                                                     
                          ));
                        
                            $sth12->execute(array(
                                $row['id'],
                                $row2['load_id'],
                                $row2['flags']['flg_termo'],
                                $row2['flags']['flg_minifest'],
                                $row2['flags']['flg_crm'],                                                                                               
                                $row2['flags']['flg_invoice'],                                                                                               
                                $row2['flags']['flg_palettes'],                                                                                               
                                $row2['flags']['flg_other_tare']                                                                                            
                            ));                          

                    
                  }
                }    
            }

          }

  
          // NOTE Įrašomi atnaujinti kroviniai be užsakymu
          if(!empty($data['response']['updated_loads'])){ 
           

            foreach($data['response']['updated_loads'] as $key => $row) {

              $ordered_meters_arr = array();
              $revised_meters_arr = array();

                  $zero = 0;
                  $ordered = array();
                  $revised = array();    
                if(empty($row['ordered_weight'])) $row['ordered_weight'] = 0;  
                if(empty($row['ordered_length'])) $row['ordered_length'] = 0;  
                if(empty($row['ordered_width'])) $row['ordered_width'] = 0;  
                if(empty($row['ordered_height'])) $row['ordered_height'] = 0;  

                if(empty($row['revised_weight'])) $row['revised_weight'] = 0;  
                if(empty($row['revised_length'])) $row['revised_length'] = 0;  
                if(empty($row['revised_width'])) $row['revised_width'] = 0;  
                if(empty($row['revised_height'])) $row['revised_height'] = 0;  
                  

                 $ordered = $row['ordered_weight']." ".$row['ordered_length']."x".$row['ordered_width']."x".$row['ordered_height'];
                 $revised = $row['revised_weight']." ".$row['revised_length']."x".$row['revised_width']."x".$row['revised_height'];


                 $ordered_meters_arr[] = $row['ordered_length'];
                 $ordered_meters_arr[] = $row['ordered_width'];     

                 $revised_meters_arr[] = $row['revised_length'];
                 $revised_meters_arr[] = $row['revised_width'];       
                     
                 $user_m2->execute(array($row['shipment_id']));
          
                 $user2 = $user_m2->fetch();
                 $taxablemeter2 = $user2['meters'];                
           
                 $max_ordered_meter = max($ordered_meters_arr);
                 $max_revised_meter = max($revised_meters_arr);       

                 $max_meter = ($max_ordered_meter > $max_revised_meter ? $max_ordered_meter : $max_revised_meter);
                 $max_taxablemeter = ($taxablemeter2 == 0 || $taxablemeter2 == '' ? $taxablemeter : $taxablemeter2);

                 $meters = ($max_meter > $max_taxablemeter ? $max_meter : NULL);   


                 $insert_load_without_order->execute(array(
                                    $row['load_id'],
                                    $row['ordered_quantity'],
                                    $row['revised_quantity'],
                                    $row['ordered_measure'],
                                    $row['revised_measure'],
                                    $row['revised_weight'],
                                    $row['revised_length'],
                                    $row['revised_width'],
                                    $row['revised_height'],
                                    $row['m3'],
                                    $row['pll'],
                                    $row['note'],
                                    $row['documents'], 
                                    $row['ean'],                                                     
                                    $date ,
                                    $ordered,
                                    $revised,
                                    $row['shipment_id'],
                                    $meters                                                                                     
                  ));

                      $sth12->execute(array(
                          $row['shipment_id'],
                          $row['load_id'],
                          $row['flags']['flg_termo'],
                          $row['flags']['flg_minifest'],
                          $row['flags']['flg_crm'],                                                                                               
                          $row['flags']['flg_invoice'],                                                                                               
                          $row['flags']['flg_palettes'],                                                                                               
                          $row['flags']['flg_other_tare']                                                                                            
                      ));
                    

            }

          }


      // NOTE Įrašomi naujai pridėti kroviniai
      if(!empty($data['response']['new_loads'])){ 
        foreach($data['response']['new_loads'] as $key => $row) {

              $zero = 0;
              $ordered = array();
              $revised = array();    
            if(empty($row['ordered_weight'])) $row['ordered_weight'] = 0;  
            if(empty($row['ordered_length'])) $row['ordered_length'] = 0;  
            if(empty($row['ordered_width'])) $row['ordered_width'] = 0;  
            if(empty($row['ordered_height'])) $row['ordered_height'] = 0;  

            if(empty($row['revised_weight'])) $row['revised_weight'] = 0;  
            if(empty($row['revised_length'])) $row['revised_length'] = 0;  
            if(empty($row['revised_width'])) $row['revised_width'] = 0;  
            if(empty($row['revised_height'])) $row['revised_height'] = 0;  
              

             $ordered = $row['ordered_weight']." ".$row['ordered_length']."x".$row['ordered_width']."x".$row['ordered_height'];
             $revised = $row['revised_weight']." ".$row['revised_length']."x".$row['revised_width']."x".$row['revised_height'];

                $sth8->execute(array($row['shipment_id'],
                                      $row['load_id'],
                                      $row['ordered_quantity'],
                                      $row['revised_quantity'],
                                      $row['ordered_measure'],
                                      $row['revised_measure'],
                                      $row['ordered_weight'],
                                      $row['ordered_length'],
                                      $row['ordered_width'],
                                      $row['ordered_height'],
                                      $row['m3'],
                                      $row['pll'],
                                      $row['note'],
                                      $row['documents'], 
                                      $row['ean'],                                                     
                                      $date ,
                                      $ordered,
                                      $revised                                                                                     
                                            ));
        
        }

      }


      // NOTE Įrašomas ištrintų užsakymu sąrašas
      if(!empty($data['response']['deleted_shipments'])){ 
         foreach($data['response']['deleted_shipments'] as $key => $row) {
            $sth9->execute(array($row['shipment_id']));
         }
        }

      // NOTE Įrašomas ištrintų krovinių sąrašas
      if(!empty($data['response']['deleted_loads'])){ 
         foreach($data['response']['deleted_loads'] as $key => $row) {
            $sth10->execute(array($row['load_id']));
         }
      }

      // NOTE Įrašomos nuotrauku nuorodos
      if(!empty($data['response']['images'])){ 
        foreach($data['response']['images'] as $key => $row) { 
           $sth13->execute(array($row['shipment_id'],$row['file_id'],$row['file_type'] ,$row['file_name'] ,$row['file_description'] ,$row['file_created'], $row['timestamp']));
        }
      }

      // NOTE Įrašomi paketų duomenys
      if(!empty($data['response']['new_shipment_packages'])){ 
        foreach($data['response']['new_shipment_packages'] as $row) { 
          $sth14->execute(array(                         
                  $row['load_id'],
                  $row['package_id'],
                  $row['package_code'],
                  $row['seq_num'],                                                                                               
                  $row['tare_type_id'],                                                                                               
                  $row['weight_kg'],                                                                                               
                  $row['length_m'],                                                                                            
                  $row['width_m'],                                                                                           
                  $row['height_m'],                                                                                            
                  $row['damage_type_id'],                                                                                           
                  $row['location_nme'],                                                                                           
                  $row['comments'],                                                                                           
                  $row['entry_date'],                                                                                           
                  $row['edited_date'],                                                                                           
                  $row['vvs_package_id']                                                                                           
              ));
        }
      }

      // NOTE Įrašomi redaguotų paketų duomenys
      if(!empty($data['response']['edited_shipment_packages'])){ 
        foreach($data['response']['edited_shipment_packages'] as $row) { 
          $sth15->execute(array(                         
                  $row['load_id'],
                  $row['package_id'],
                  $row['package_code'],
                  $row['seq_num'],                                                                                               
                  $row['tare_type_id'],                                                                                               
                  $row['weight_kg'],                                                                                               
                  $row['length_m'],                                                                                            
                  $row['width_m'],                                                                                           
                  $row['height_m'],                                                                                            
                  $row['damage_type_id'],                                                                                           
                  $row['location_nme'],                                                                                           
                  $row['comments'],                                                                                           
                  $row['entry_date'],                                                                                           
                  $row['edited_date'],                                                                                           
                  $row['vvs_package_id']                                                                                           
          ));
        }
      } 


    // NOTE Jei niekur negauta klaida įrašom paskutinio atnaujinimo datą
    $sth5->execute(array('success',$time));  
    $last_timestamp_id = $dbh->lastInsertId();
      
            


} catch (PDOException $e) {
    // NOTE Jei gauta klaida, įrašom atnaujinimo datą su žyma fail, log'ą ir išsiunčiam email su pranešimu administratoriui
    echo "Error!";
    echo $e->getMessage();
    $sth5_2 = $dbh->prepare("UPDATE app_last_update SET status = ?, date = ? WHERE id = ?");

    $sth5_2->execute(array('fail',$time,$last_timestamp_id));
    $logfile = 'app_orders.log';
    customers_log($e->getMessage(), $logfile);   
    sendReportMail($e->getMessage(),basename(__FILE__));
}
 