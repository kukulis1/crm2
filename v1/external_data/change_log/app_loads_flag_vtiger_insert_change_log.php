<?php
include '../ws.config.php';
global $config;
require '../utils.php';
 set_time_limit(500);
 error_reporting(1);


try {

    require('../mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel_flags (flag_id, 
                                                                        external_order_id, 
                                                                        external_load_id, 
                                                                        termo, 
                                                                        minifest, 
                                                                        cmr, 
                                                                        invoice, 
                                                                        palettes, 
                                                                        other_tare,
                                                                        import_date) 
                                                                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');    

    $sth = $dbh->prepare("SELECT DISTINCT f.*, s.salesorderid
                            FROM app_load_flags f
                            JOIN vtiger_salesorder s ON s.external_order_id = f.order_id 
                            JOIN vtiger_crmentity e ON e.crmid = s.salesorderid                                                                          
                            WHERE e.deleted = 0  AND f.load_id NOT IN (SELECT c.external_load_id FROM vtiger_inventoryproductrel_flags c WHERE c.external_load_id = f.load_id)");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $date = date("Y-m-d H:i:s");


    while ($row = $sth->fetch()) {
      
            $sth2->execute(array($row['salesorderid'],
                                    $row['order_id'],
                                    $row['load_id'],
                                    $row['termo'],
                                    $row['minifest'],       
                                    $row['cmr'],       
                                    $row['invoice'],       
                                    $row['palettes'],       
                                    $row['other_tare'],
                                    $date      
                                ));


                     

    }

    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error! ";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

      

?>
