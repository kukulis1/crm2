<?php
set_time_limit(500);
error_reporting(E_ALL);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {

      require('../mysql_connection.php');


      $sth = $dbh->prepare("SELECT package_id,package_code, code AS tare, p.weight_kg, p.length_m, p.width_m, p.height_m ,p.load_id, i.id AS salesorderid
                                  FROM app_shipment_packages p                                 
                                  JOIN app_measures m ON m.id=tare_type_id 
                                  JOIN vtiger_inventoryproductrel i ON i.external_load_id=p.load_id                               
                                  GROUP BY package_id");


      $sth->setFetchMode(PDO::FETCH_ASSOC);
      $sth->execute(array());
      $orders_count = $sth->rowCount();

      $sth2 = $dbh->prepare('INSERT INTO vtiger_packages (packagesid,load_id,packagesno, packages_tks_kg, packages_tks_length, packages_tks_width, packages_tks_height, packages_tks_tare,tags, cf_20023) VALUES (?,?,?,?,?,?,?,?,?,?)');
      $insert_packages = $dbh->prepare('INSERT INTO vtiger_packagescf (packagesid) VALUES (?)');
      

      if($orders_count > 0){

        $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count); 
    
        while ($row = $sth->fetch()) {    
          // Istrinami seni paketai pries rasant naujus
          if(deleteOldPackages($dbh,$row['salesorderid'])){     
            $last_entity_record =  get_last_entity_id_and_update($dbh,1);  
            $sth2->execute(array($last_entity_record,$row['load_id'],$row['package_id'],$row['weight_kg'],$row['length_m'],$row['width_m'],$row['height_m'],$row['tare'],$row['package_code'],$row['salesorderid']));
            $insert_packages->execute(array($last_entity_record));          
            insert_entity3($dbh, 'Packages', $last_entity_record, 26, NULL,'Metrika', $last_entity_record, date("Y-m-d H:i:s"));               $last_entity_record++;   
          }
        }
      }

    } catch (PDOException $e) {
       echo "Error!";
       sendReportMail($e->getMessage(),basename(__FILE__));
       echo $e->getMessage();             
    }

    function deleteOldPackages($dbh,$salesorderid){
    
        $sth = $dbh->prepare('SELECT packagesid FROM vtiger_packages WHERE cf_20023 = ?');
        $sth2 = $dbh->prepare('DELETE FROM vtiger_packages WHERE packagesid = ?');
        $sth3 = $dbh->prepare('DELETE FROM vtiger_packagescf WHERE packagesid = ?');
        $sth4 = $dbh->prepare('DELETE FROM vtiger_crmentity WHERE crmid = ? AND setype = ?');
  
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($salesorderid));

        $sth2->execute(array($salesorderid));
        $sth3->execute(array($salesorderid));

        if($sth->rowCount()){
          while ($row = $sth->fetch()) {   
            $sth4->execute(array($row['packagesid'],'Packages'));
          }
        }  
        
        return true;
  
    }

