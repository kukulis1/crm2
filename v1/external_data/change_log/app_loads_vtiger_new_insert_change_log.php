<?php
include '../ws.config.php';
global $config;
require '../utils.php';
 set_time_limit(500);
 error_reporting(1);


try {

    require('../mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel (id,
                                                                external_order_id,  
                                                                external_load_id, 
                                                                productid,
                                                                quantity,                                                              
                                                                comment,
                                                                description,
                                                                cargo_measure,
                                                                cargo_wgt,
                                                                cargo_length,
                                                                cargo_width,                                                     
                                                                cargo_height, 
                                                                pll, 
                                                                document,                                                                
                                                                ean,
                                                                source,
                                                                import_date,
                                                                note,
                                                           	    service,
                                                                inventory_type,
                                                                revised_measure,
                                                                revised_quantity) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');    

    $sth = $dbh->prepare("SELECT DISTINCT t.*, s.salesorderid, s.accountid
                            FROM app_loads_new t
                            JOIN vtiger_salesorder s ON s.external_order_id = t.order_id 
                            JOIN vtiger_crmentity e ON e.crmid = s.salesorderid  
                            LEFT JOIN vtiger_inventoryproductrel i ON i.external_load_id=t.id                                                                            
                            WHERE e.deleted = 0  AND i.id IS NULL");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());


    while ($row = $sth->fetch()) {

        $ordered = multiexplode(array("x","X"," "), $row['ordered']);
        $revised = multiexplode(array("x","X"," "), $row['revised']);

        $ordered_m3 = $ordered[1]*$ordered[2]*$ordered[3];
        $revised_m3 = $revised[1]*$revised[2]*$revised[3];

        $weight = $row['weight'];
        $length = $row['length'];
        $width = $row['width'];
        $height = $row['height'];


        if($ordered[0] <= $revised[0] && $ordered_m3 <= $revised_m3){
            $weight = $revised[0];
            $length = $revised[1];           
            $width = $revised[2];    
            $height = $revised[3];    
        }elseif($ordered[0] <= $revised[0] && $ordered_m3 >= $revised_m3){
            $weight = $ordered[0];
            $length = $ordered[1];           
            $width = $ordered[2];    
            $height = $ordered[3];  
        }elseif($ordered[0] >= $revised[0] && $ordered_m3 >= $revised_m3){
            $weight = $ordered[0];
            $length = $ordered[1];           
            $width = $ordered[2];    
            $height = $ordered[3];  
        }elseif($ordered[0] >= $revised[0] && $ordered_m3 <= $revised_m3){    
            $weight = $revised[0];
            $length = $revised[1];           
            $width = $revised[2];    
            $height = $revised[3];    
        }



            $default_product = 14244;
            $source = 'Metrika';        
            $date = date("Y-m-d H:i:s"); 
            $row['quantity'] = ( $row['quantity'] < 1) ? 1 :  $row['quantity'];     
            // customers_log('Loads insert '.$row['id'].' '.$row['weight'] , 'loads_insert_3.log');       
                    
                $sth2->execute(array($row['salesorderid'],
                                $row['order_id'],
                                $row['id'],
                                $default_product, 
                                $row['quantity'],                                        
                                $row['note'], 
                                $row['note'],       
                                $row['measure'],
                                $weight,
                                $length,
                                $width,                       
                                $height,
                                $row['pll'],
                                $row['document'],                                
                                $row['ean'], 
                                $source,                     
                                $date,
                                $note,
                                2,
                                '',
                                $row['measure_revised'],
                                $row['quantity_revised']                                         
                ));

                     

    }

    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error! ";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

      

?>
