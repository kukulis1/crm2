<?php
set_time_limit(900);

include '../ws.config.php';
global $config;
require '../utils.php';
$minutes = change_minutes(date("i"));
error_reporting(0);

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_inventoryproductrel SET       
                                                                productid = ?,
                                                                quantity = ?,
                                                                comment = ?, 
                                                                description = ?, 
                                                                cargo_measure = ?,                                                              
                                                                pll = ?,
                                                                document = ?,
                                                                tare = ?,
                                                                ean = ?,                                                              
                                                                update_date = ?, 
                                                                note = ?,                                                
                                                                ordered_weight = ?,
                                                                ordered_length = ?,
                                                                ordered_width = ?,
                                                                ordered_height = ?,
                                                                revised_measure = ?,
                                                                revised_quantity = ?,
                                                                revised_weight = ?,
                                                                revised_length = ?,
                                                                revised_width = ?,
                                                                revised_height = ?,
                                                                m3 = ?                                                              
                                                    WHERE external_load_id = ? AND productid = 14244');


    $insert_load = $dbh->prepare('INSERT INTO vtiger_inventoryproductrel (id,
                                                                    external_order_id,  
                                                                    external_load_id, 
                                                                    productid,
                                                                    quantity,                                                              
                                                                    comment,
                                                                    description,
                                                                    cargo_measure,
                                                                    cargo_wgt,
                                                                    cargo_length,
                                                                    cargo_width,                                                     
                                                                    cargo_height, 
                                                                    pll, 
                                                                    document,                                                                
                                                                    ean,
                                                                    source,
                                                                    import_date,
                                                                    note,
                                                                    service,
                                                                    ordered_weight,
                                                                    ordered_length,
                                                                    ordered_width,
                                                                    ordered_height,
                                                                    revised_measure,
                                                                    revised_quantity,
                                                                    revised_weight,
                                                                    revised_length,
                                                                    revised_width,
                                                                    revised_height,
                                                                    m3,
                                                                    inventory_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?)');    


    $check_load = $dbh->prepare('SELECT id FROM vtiger_inventoryproductrel WHERE external_load_id = ? AND productid = 14244');          


       
    $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1317 = ? , cf_1319 = ? WHERE   salesorderid = ? ');     
    $sth4 = $dbh->prepare('UPDATE vtiger_crmentity  SET modifiedtime = ? WHERE crmid = ? ');    
    
    $set_session_wait_time = $dbh->prepare("SET SESSION innodb_lock_wait_timeout = 900");
    $set_session_wait_time->execute(array());
    
    $sth = $dbh->prepare('SELECT t.*, s.salesorderid
                            FROM app_loads_update t
                            LEFT JOIN vtiger_inventoryproductrel c ON c.external_load_id = t.id AND c.productid = 14244
                            LEFT JOIN vtiger_salesorder s ON s.external_order_id=t.shipment_id                             
                            WHERE s.salesorderid IS NOT NULL 
                            GROUP BY t.id');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());


    // $sth5 = $dbh->prepare('SELECT  ROUND(SUM(t.m3),3) AS revised_m3, ROUND(SUM(t.length * t.width * t.height) * t.quantity,3) as volume, s.salesorderid
    //                             FROM app_loads_update t
    //                             JOIN vtiger_inventoryproductrel c ON c.external_load_id = t.id
    //                             LEFT JOIN vtiger_salesorder s ON s.salesorderid=c.id                                
    //                             GROUP BY t.shipment_id');

    // $sth5->setFetchMode(PDO::FETCH_ASSOC);
    // $sth5->execute(array());

    // $sth6 = $dbh->prepare('UPDATE vtiger_salesordercf SET cf_1365 = ?, cf_1367 = ?  WHERE salesorderid = ? ');
 
    // while ($row = $sth5->fetch()) { 
    //     $sth6->execute(array(
    //         $row['revised_m3'],    
    //         $row['volume'],                     
    //         $row['salesorderid']          
    //     ));    
    // }


    $ordered_2 = [];
    $revised_2 = [];

    while ($row = $sth->fetch()) {     

            $default_product = 14244;
            $date = date("Y-m-d H:i:s");

            
            $ordered = multiexplode(array("x","X"," "), $row['ordered']);
            $revised = multiexplode(array("x","X"," "), $row['revised']);
    
            $ordered_m3 = $ordered[1]*$ordered[2]*$ordered[3];
            $revised_m3 = $revised[1]*$revised[2]*$revised[3];    
            $weight = $row['weight'];
            $length = $row['length'];
            $width = $row['width'];
            $height = $row['height'];
           

            if($ordered[0] <= $revised[0] ){
                $weight = $revised[0];    
            }else{
                $weight = $ordered[0];  
            }   
            
            if($ordered_m3 >= $revised_m3){               
                $length = $ordered[1];           
                $width = $ordered[2];    
                $height = $ordered[3];  
            }else{                 
                $length = $revised[1];           
                $width = $revised[2];    
                $height = $revised[3];    
            }


  

            $weight_ordered = explode(' ' ,$row['ordered']);
            $dim_ordered = explode('x', $weight_ordered[1]);

            $weight_revised = explode(' ' ,$row['revised']);
            $dim_revised = explode('x', $weight_revised[1]);

           
            if(empty($dim_ordered[0])) $dim_ordered[0] = 0;
            if(empty($dim_ordered[1])) $dim_ordered[1] = 0;
            if(empty($dim_ordered[2])) $dim_ordered[2] = 0;

            if(empty($dim_revised[0])) $dim_revised[0] = 0;
            if(empty($dim_revised[1])) $dim_revised[1] = 0;
            if(empty($dim_revised[2])) $dim_revised[2] = 0;

            $check_load->execute(array($row['id']));    

            if(!$check_load->rowCount()){                
                $insert_load->execute(array($row['salesorderid'],
                                            $row['shipment_id'],
                                            $row['id'],
                                            $default_product, 
                                            $row['ordered_quantity'],                                        
                                            $row['note'], 
                                            $row['note'],       
                                            $row['measure'],
                                            $weight,
                                            $length,
                                            $width,                       
                                            $height,
                                            $row['pll'],
                                            $row['document'],                                
                                            $row['ean'], 
                                            'METRIKA',                     
                                            $date,
                                            $row['note'],
                                            1,
                                            $weight_ordered[0],
                                            $dim_ordered[0],
                                            $dim_ordered[1],
                                            $dim_ordered[2],
                                            $row['measure_revised'],
                                            $row['quantity'],
                                            $weight_revised[0],
                                            $dim_revised[0],
                                            $dim_revised[1],
                                            $dim_revised[2],
                                            $row['m3'],
                                            ''     
                ));                   
                    
            }else{   
                $sth2->execute(array($default_product,
                                    $row['ordered_quantity'],                      
                                    $row['note'],
                                    $row['note'],
                                    $row['measure'],                                       
                                    $row['pll'],
                                    $row['document'],
                                    null,      
                                    $row['ean'],                                            
                                    $date,
                                    $row['note'], 
                                    $weight_ordered[0],
                                    $dim_ordered[0],
                                    $dim_ordered[1],
                                    $dim_ordered[2],
                                    $row['measure_revised'],
                                    $row['quantity'],
                                    $weight_revised[0],
                                    $dim_revised[0],
                                    $dim_revised[1],
                                    $dim_revised[2],
                                    $row['m3'],                                                                  
                                    $row['id']                        
                ));


                $orderid =  $row['salesorderid'];
                $ordered_2[$orderid] .= $row['ordered_quantity']." ".$row['measure']." ".$row['ordered'].", ";

                $revised_quantity = $row['quantity'];
                if(in_array($row['revised_measure'], [1, 7, 'pll', 'nestd.'])){
                    $revised_quantity = $row['pll'];
                }
                $revised_2[$orderid] .= $revised_quantity." ".$row['measure_revised']." ".$row['revised'].", ";

                $sth3->execute(array(
                rtrim($ordered_2[$orderid], ', '),
                rtrim($revised_2[$orderid], ', '),                                         
                $row['salesorderid']          
                ));    

                $sth4->execute(array(
                $date,                 
                $row['salesorderid']          
                ));   

                $date_day = date("Y-m-d");               
                $time = date("H:").$minutes;
                if(!file_exists("../logs/atnaujinti_loadai/$date_day")){
                mkdir("../logs/atnaujinti_loadai/$date_day", 0777, true); 
                }

                $logfile = "atnaujinti_loadai/$date_day/$time.log";

                customers_log('Uzsakymas: '.$row['salesorderid'].' Ordered '.rtrim($ordered_2[$orderid], ', ').' Revised '.rtrim($revised_2[$orderid], ', '), $logfile);  
            }
            

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
    $date_day = date("Y-m-d");               
    $time = date("H:").$minutes;
    if(!file_exists("../logs/atnaujinti_loadai/$date_day")){
    mkdir("../logs/atnaujinti_loadai/$date_day", 0777, true); 
    }
    
    $logfile = "atnaujinti_loadai/$date_day/$time.log";

    customers_log($e->getMessage(), $logfile);  
}

?>
