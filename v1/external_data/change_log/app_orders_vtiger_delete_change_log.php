<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);
error_reporting(0);

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   

    $date = date("Y-m-d H:i:s");                                                   
                  
    $sth = $dbh->prepare("SELECT c.salesorderid
                            FROM app_orders_deleted t
                            JOIN vtiger_salesorder c ON c.external_order_id = t.order_id");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array(454067));

    $sth2 = $dbh->prepare('UPDATE vtiger_crmentity SET modifiedtime = ?, deleted = ?  WHERE crmid = ?');   

    while ($row = $sth->fetch()) {             
        $sth2->execute(array($date,1,$row['salesorderid']));                 
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
