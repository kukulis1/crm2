<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);
error_reporting(0);

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET       
    															accountid = ?,                                                                                         
                                                                load_date_from = ?,                                                                  
                                                                load_time_from = ?, 
                                                                load_date_to = ?,
                                                                load_time_to = ?,
                                                                unload_date_from = ?,
                                                                unload_time_from = ?,  
                                                                unload_date_to = ?,
                                                                unload_time_to = ?, 
                                                                route_point_id = ?,
                                                                shipment_code = ?,
                                                                update_date = ?
                                                    WHERE salesorderid = ?');
     
     
     $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf SET cf_855 = ?, cf_928 = ?, cf_1504 = ?, cf_1564 = ?, cf_1566 = ?, cf_1558 = ?, cf_1560 = ?, cf_1562 = ? WHERE salesorderid = ?');
                  
     $sth = $dbh->prepare("SELECT t.*, a.accountid,  c.salesorderid, g.modifiedtime, ROUND(t.shipment_direct_km,2) AS shipment_km
                            FROM app_orders_handle t
                            JOIN vtiger_account a ON a.customer_id = t.customer_id 
                            JOIN vtiger_accountscf ac ON ac.accountid = a.accountid                             
                            JOIN vtiger_crmentity e ON e.crmid = a.accountid 
                            JOIN vtiger_salesorder c ON c.external_order_id = t.id
                            JOIN vtiger_crmentity g ON g.crmid = c.salesorderid                                                                         
                            WHERE e.deleted = 0 AND g.deleted = 0
                            GROUP BY  t.id ");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());
    echo '<pre>';  
    while ($row = $sth->fetch()) {
        echo $row['salesorderid']."<br>";
            $shipment_type = ($row['shipment_type'] == 'parcel' ? 'Transporto užsakymas' : 'Perkraustymo užsakymas');
            $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
            $load_date_from = date("Y-m-d", strtotime($row['load_date_from']));
            $load_date_to = date("Y-m-d", strtotime($row['load_date_to']));
            $load_time_from = date("H:i:s", strtotime($row['load_date_from']));
            $load_time_to = date("H:i:s", strtotime($row['load_date_to']));            
            $unload_date_from = date("Y-m-d", strtotime($row['unload_date_from']));
            $unload_date_to = date("Y-m-d", strtotime($row['unload_date_to']));
            $unload_time_from = date("H:i:s", strtotime($row['unload_date_from']));
            $unload_time_to = date("H:i:s", strtotime($row['unload_date_to']));                
            $date = date("Y-m-d H:i:s");   
            if(empty($row['order_date']))  $order_date = date("Y-m-d H:i:s"); else  $order_date = date("Y-m-d", strtotime($row['order_date']))." ".date("H:i:s");  

                   
                
                  $update_entity = update_entity($dbh, $row['salesorderid'],  $row['shipment_code'], $row['id'], $date);
                 
                    $sth2->execute(array(                    					                                                             
                                        $row['accountid'],                                                        
                                        $load_date_from,
                                        $load_time_from ,      
                                        $load_date_to, 
                                        $load_time_to,  
                                        $unload_date_from,
                                        $unload_time_from ,      
                                        $unload_date_to, 
                                        $unload_time_to,
                                        $row['route_point_id'],
                                        $row['shipment_code'],                        
                                        $date,
                                        $row['salesorderid']
                    ));                   
 

                    $sth3->execute(array(                                                              
                        $shipment_type,
                        $distance,
                        $order_date,                       
                        $row['load_company_phone'],
                        $row['unload_company_phone'], 
                        $row['route_code'],
                        $row['vehicle_registration_number'],
                        $row['driver1_name'],
                        $row['salesorderid']
                     ));                     
                    
  
                    $update_load_address = update_load_address($dbh, $row['salesorderid'], $row['load_address'], $row['load_post_code'], $row['load_municipality'], $row['load_country_code'], $row['load_company_name'], $row['load_company_code'], $row['load_company_contact'], $row['load_company_phone']);
                    $update_unload_address = update_unload_address($dbh, $row['salesorderid'], $row['unload_address'], $row['unload_post_code'], $row['unload_municipality'], $row['unload_country_code'], $row['unload_company_name'], $row['unload_company_code'], $row['unload_company_contact'], $row['unload_company_phone']); 

                    
                       
        $date = date("Y-m-d");
        $logfile = "UpdateOrder$date.log";      
        // customers_log('Update order '.$row['salesorderid'], $logfile);      
                    
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}


?>
