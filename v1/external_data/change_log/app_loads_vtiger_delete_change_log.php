<?php
set_time_limit(300);
error_reporting(1);
include '../ws.config.php';
global $config;
require '../utils.php';

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('DELETE FROM vtiger_inventoryproductrel  WHERE external_load_id = ?');
       
          
    
    $sth = $dbh->prepare('SELECT load_id  FROM app_loads_deleted');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {      
         $sth2->execute(array($row['load_id']));      
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
