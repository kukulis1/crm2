<?php
set_time_limit(300);
error_reporting(1);
include '../ws.config.php';
global $config;
require '../utils.php';

try {

    require('../mysql_connection.php');

    $dbh->beginTransaction();   
    
 
    $sth2 = $dbh->prepare('SELECT  cf_1317, cf_1319,cf_1365,cf_1367 FROM app_loads_update_temp WHERE salesorderid = ?');  
    
    $sth = $dbh->prepare('SELECT GROUP_CONCAT(t.ordered) AS ordered, 
                                 GROUP_CONCAT(t.revised) AS revised,s.salesorderid,
                                 SUM(t.m3) as cf_1365,  
                                 SUM(t.length * t.width * t.height) * t.quantity  as cf_1367
                            FROM app_loads_update t
                            JOIN vtiger_inventoryproductrel c ON c.external_load_id = t.id AND productid = 14244
                            LEFT JOIN vtiger_salesorder s ON s.salesorderid=c.id
                            LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
                            LEFT JOIN vtiger_invoice_salesorders_list sl ON sl.salesorderid=s.salesorderid     
                            WHERE sl.salesorderid IS NULL  AND s.salesorderid IS NOT NULL
                            GROUP BY t.shipment_id');

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) { 

        $date = date("Y-m-d H:i:s"); 
        $sth2->execute(array($row['salesorderid']));
        $ordercf = $sth2->fetch();
        $ordercf_values = array('ordered' => 'cf_1317', 'revised' => 'cf_1319', 'cf_1365' => 'cf_1365', 'cf_1367' => 'cf_1367');                    
     
        foreach($ordercf_values as $new => $old){                       
            if(!empty($ordercf[$old]) AND ($row[$new] != $ordercf[$old])){           
                $id = last_modtracker_record($dbh);
                insert_modtracker_basic($dbh, $id, $row['salesorderid'], 'SalesOrder', 26, $date,0);
                insert_modtracker_detail($dbh, $id, $old, $ordercf[$old], $row[$new]);
                update_modtracker_basic_seq($dbh, $id);
            }
        }

    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
