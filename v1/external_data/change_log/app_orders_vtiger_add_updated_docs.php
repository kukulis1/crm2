<?php
include '../ws.config.php';
global $config;
require '../utils.php';

set_time_limit(500);


try {

    require('../mysql_connection.php');

    $dbh->beginTransaction(); 

     $stevedoring_query = $dbh->prepare("SELECT f.minifest,f.cmr,f.invoice,f.order_id,f.load_id,c.salesorderid, cf_1812 as doc_service           
                                    FROM app_load_update_flags f
                                    LEFT JOIN vtiger_salesorder c ON c.external_order_id = f.order_id                                   
                                    LEFT JOIN vtiger_accountscf ac ON ac.accountid = c.accountid  
                                    LEFT JOIN vtiger_crmentity e ON e.crmid = ac.accountid                                   
                                    LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=c.salesorderid                                              
                                    WHERE e.deleted = 0 AND cf_1812 > 0
                                    GROUP BY f.load_id");

     $inv_num = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id = ?");             
     $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id,external_order_id,external_load_id,productid,sequence_no,quantity,margin,service,inventory_type) VALUES(?,?,?,?,?,?,?,?,?)");
    
     $delete_services = $dbh->prepare("DELETE FROM vtiger_inventoryproductrel WHERE  external_load_id = ? AND productid = 36641 AND service = 12");  

     $stevedoring_query->setFetchMode(PDO::FETCH_ASSOC);     
     $stevedoring_query->execute(array());
     $inv_num->setFetchMode(PDO::FETCH_ASSOC);   

     $docsArr = array();
     $data = array();


     foreach($stevedoring_query AS $row) {
        $docsArr[$row['salesorderid']][] = ($row['minifest'] == 1 || $row['minifest'] == 0) ? $row['minifest'] : 1;

        $docsArr[$row['salesorderid']][] = ($row['cmr'] == 1 || $row['cmr'] == 0) ? $row['cmr'] : 1;

        $docsArr[$row['salesorderid']][] = ($row['invoice'] == 1 || $row['invoice'] == 0) ? $row['invoice'] : 1;

        $docs = array_sum($docsArr[$row['salesorderid']]);
        $data[$row['salesorderid']] =  array('salesorderid' => $row['salesorderid'],
                                             'order_id' => $row['order_id'], 
                                             'load_id' => $row['load_id'], 
                                             'docs' => $docs, 
                                             'charge' => $row['doc_service']);        
     }


     foreach($data AS $item) {    
        $delete_services->execute(array($item['load_id']));  
        if($delete_services){
            if($item['docs'] > 0){                              
                    for($i=0; $i<$item['docs']; $i++){                 
                        $inv_num->execute(array($item['salesorderid']));
                        $count = $inv_num->rowCount();                      
                        $insert_service->execute(array($item['salesorderid'],$item['order_id'],$item['load_id'],'36641',$count+1,1,$item['charge'],12,'documents'));                                                        
                    }
                
            }
        }        
     }    
     
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
    sendReportMail($e->getMessage(),basename(__FILE__));
}

?>
