<?php
set_time_limit(500);
// error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';

    try {
        require('../mysql_connection.php'); 

        $sth = $dbh->prepare('SELECT row_id FROM app_metrika_shipments_log');


        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $select = "ROUTE.ROUTE_CODE, DELIVERY.SHIPMENT_ID, DELIVERY.DELIVERY_DATE, DELIVERY.DELIVERY_CODE, DELIVERY.LOADING_LOCATION_ID, 
                    DELIVERY.UNLOADING_LOCATION_ID, DELIVERY.DELIVERY_TYPE_ID, DELIVERY.LOADING_START_TIME, DELIVERY.LOADING_END_TIME, 
                    DELIVERY.UNLOADING_START_TIME, DELIVERY.UNLOADING_END_TIME, DELIVERY.DELIVERY_SEQ_NO, 
                    DELIVERY.DELIVERY_ID, location_1.LOCATION_NAME AS LOAD_LOCATION_NAME, 
                    location_1.COUNTRY_CODE AS LOAD_COUNTRY_CODE, location_1.MUNICIPALITY AS LOAD_MUNICIPALITY, location_1.SETTLEMENT AS LOAD_SETTLEMENT, 
                    location_1.STREET_ADDRESS AS LOAD_STREET_ADDRESS, location_1.POST_CODE AS LOAD_POST_CODE, location_2.LOCATION_NAME AS UNLOAD_LOCATION_NAME, 
                    location_2.COUNTRY_CODE AS UNLOAD_COUNTRY_CODE, location_2.MUNICIPALITY AS UNLOAD_MUNICIPALITY, location_2.SETTLEMENT AS UNLOAD_SETTLEMENT, 
                    location_2.STREET_ADDRESS AS UNLOAD_STREET_ADDRESS, location_2.POST_CODE AS UNLOAD_POST_CODE";

        $table = 'location AS location_1 
                JOIN DELIVERY ON location_1.LOCATION_ID = DELIVERY.LOADING_LOCATION_ID 
                JOIN location AS location_2 ON delivery.UNLOADING_LOCATION_ID = location_2.LOCATION_ID   
                LEFT JOIN ROUTE_POINT_OPERATION ON ROUTE_POINT_OPERATION.DELIVERY_ID=DELIVERY.DELIVERY_ID
                LEFT JOIN ROUTE_POINT ON ROUTE_POINT.ROUTE_POINT_ID = ROUTE_POINT_OPERATION.ROUTE_POINT_ID 
                LEFT JOIN ROUTE ON ROUTE.ROUTE_ID = ROUTE_POINT.ROUTE_ID';             

        $where = "DELIVERY.SHIPMENT_ID IN (";

        while ($row = $sth->fetch()) {
            $where .= $row['row_id'].","; 
        }

        $where = rtrim($where, ',');
        $where .= ")";

        $where .= " GROUP BY ROUTE.ROUTE_CODE, DELIVERY.SHIPMENT_ID, DELIVERY.DELIVERY_DATE, DELIVERY.DELIVERY_CODE, DELIVERY.LOADING_LOCATION_ID, 
                    DELIVERY.UNLOADING_LOCATION_ID, DELIVERY.DELIVERY_TYPE_ID, DELIVERY.LOADING_START_TIME, DELIVERY.LOADING_END_TIME, 
                    DELIVERY.UNLOADING_START_TIME, DELIVERY.UNLOADING_END_TIME, DELIVERY.DELIVERY_SEQ_NO, 
                    DELIVERY.DELIVERY_ID, location_1.LOCATION_NAME, 
                    location_1.COUNTRY_CODE, location_1.MUNICIPALITY, location_1.SETTLEMENT, 
                    location_1.STREET_ADDRESS, location_1.POST_CODE, location_2.LOCATION_NAME, 
                    location_2.COUNTRY_CODE, location_2.MUNICIPALITY, location_2.SETTLEMENT, 
                    location_2.STREET_ADDRESS, location_2.POST_CODE ";

        $insert_query = 'INSERT INTO vtiger_salersorder_delivery_plan (shipment_id, delivery_date, delivery_code, loading_location_id, unloading_location_id, delivery_type_id, loading_start_time, loading_end_time, unloading_start_time, unloading_end_time, delivery_seq_no, delivery_id, load_location_name, load_country_code, load_municipality, load_settlement, load_street_address, load_post_code, unload_location_name, unload_country_code, unload_municipality, unload_settlement, unload_street_address, unload_post_code, route_code) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        
        $execute = array('shipment_id', 'delivery_date', 'delivery_code', 'loading_location_id', 'unloading_location_id', 'delivery_type_id', 'loading_start_time', 'loading_end_time', 'unloading_start_time', 'unloading_end_time', 'delivery_seq_no', 'delivery_id', 'load_location_name', 'load_country_code', 'load_municipality', 'load_settlement', 'load_street_address', 'load_post_code', 'unload_location_name', 'unload_country_code', 'unload_municipality', 'unload_settlement', 'unload_street_address', 'unload_post_code', 'route_code');

        $check_query = 'SELECT shipment_id FROM vtiger_salersorder_delivery_plan WHERE shipment_id = ? AND delivery_id = ?';

        $update_query = 'UPDATE vtiger_salersorder_delivery_plan SET delivery_date = ?, delivery_code = ?, loading_location_id = ?, unloading_location_id = ?, delivery_type_id = ?, loading_start_time = ?, loading_end_time = ?, unloading_start_time = ?, unloading_end_time = ?, delivery_seq_no = ?, delivery_id = ?, load_location_name = ?, load_country_code = ?, load_municipality = ?, load_settlement = ?, load_street_address = ?, load_post_code = ?, unload_location_name = ?, unload_country_code = ?, unload_municipality = ?, unload_settlement = ?, unload_street_address = ?, unload_post_code = ?, route_code = ? WHERE shipment_id = ?';

        getTableWithSelectInfo($select,$table, $dbh, $where,$insert_query,$check_query,$update_query,$execute,array('shipment_id','delivery_id'));      
      

    } catch (PDOException $e) {
       echo "Error!";
       echo $e->getMessage();
    }
