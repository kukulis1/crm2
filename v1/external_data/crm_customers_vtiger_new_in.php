<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');

    $dbh->beginTransaction();   
    
    $sth2 = $dbh->prepare('UPDATE vtiger_account SET       
                                                                account_no = ?,
                                                                accountname = ?,
                                                                account_type = ?, 
                                                                contact = ?, 
                                                                phone = ?, 
                                                                email1 = ?,
                                                                blocked = ?,
                                                                region = ?,
                                                                municipality = ?,
                                                                settlement = ?,
                                                                billing_address = ?,
                                                                post_code = ?,
                                                                legal_entity_code = ?,
                                                                legal_vat_code = ?,
                                                                industry = ?,
                                                                account_manager_id = ?,
                                                                pricebook = ?,
                                                                modifydate = ?,
                                                                update_date = ?
                                                    WHERE accountid = ?');
                    
    
    $sth = $dbh->prepare('SELECT t.*, c.accountid, p.pricebookid, u.id
                            FROM crm_customers_in t
                            JOIN vtiger_account c ON c.accountname = t.customer_name
                            JOIN vtiger_crmentity g ON g.crmid = c.accountid
                            LEFT JOIN vtiger_pricebook p ON p.bookname = t.pricebook  
                            LEFT JOIN vtiger_users u ON u.title = t.account_manager_id
                            WHERE g.deleted = 0 AND c.customer_id IS NULL');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    while ($row = $sth->fetch()) {
        
            if($row['type'] == 'legal') {
            $subject = 'Juridinis';    
            } else {
            $subject = 'Fizinis';    
            }
            
            if(empty($row['id']) || $row['id'] == NULL) {
            $user_id = 5;
            } else {
            $user_id = $row['id']; 
            }          
            
            $type = 'Pirkėjas';
            $ownership = 'Metrika';
            $date = date("Y-m-d H:i:s");;
                     
            $pricebookid = 10965;            
            
                    $update_entity = update_entity($dbh, $row['accountid'], $user_id, $description, $row['customer_name']);
  
                    $sth2->execute(array($row['customer_code'],
                                        $row['customer_name'],                      
                                        $subject, 
                                        $row['contact_person_name'],
                                        $row['phone'],
                                        $row['email'],
                                        $row['blocked'],
                                        $row['region'],      
                                        $row['municipality'], 
                                        $row['settlement'],                     
                                        $row['billing_address'], 
                                        $row['post_code'],
                                        $row['legal_no'],
                                        $row['vat_no'],
                                        $type,
                                        $row['account_manager_id'], 
                                        $row['pricebookid'] != NULL ? $row['pricebookid'] : $pricebookid,                      
                                        $row['modifydate'],                        
                                        $date,
                                        $row['accountid']
                    ));         
            
                    //$update_customer_address_billing = update_customer_address_billing($dbh, $row['accountid'], $row['sender_address'], $row['sender_postcode'], $row['default_sender_location_id'], $row['sender_city'], $row['country_code'], $row['sender_name']);
                    //$update_customer_address_shipping = update_customer_address_shipping($dbh, $row['accountid'], $row['consignee_address'], $row['consignee_postcode'], $row['default_consignee_location_id'], $row['consignee_city'], $row['country_code'], $row['consignee_name']);                    
                    $update_customer_address_billing = update_customer_address_billing($dbh, $row['accountid'], $row['billing_address'], $row['post_code'], $default_sender_location_id, $row['municipality'], $row['country_code'], $sender_name);
                    $update_customer_address_shipping = update_customer_address_shipping($dbh, $row['accountid'], $consignee_address, $consignee_postcode, $default_consignee_location_id, $consignee_city, $row['country_code'], $consignee_name);                 
                    
    }
             
    $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}

?>