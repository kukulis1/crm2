<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');
        
    $dbh->beginTransaction();
    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_account (accountid,
                                                                customer_id,
                                                                account_no,
                                                                accountname,
                                                                account_type, 
                                                                contact, 
                                                                phone, 
                                                                email1,
                                                                blocked,
                                                                region,
                                                                municipality,
                                                                settlement,
                                                                billing_address,
                                                                post_code,
                                                                legal_entity_code,
                                                                legal_vat_code,
                                                                industry,
                                                                account_manager_id,
                                                                pricebook,
                                                                modifydate,
                                                                ownership,
                                                                import_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');    

    $sth5 = $dbh->prepare('INSERT INTO vtiger_accountscf (accountid,cf_2060) VALUES (?,?)'); 


    $sth = $dbh->prepare("SELECT t.*, p.pricebookid
                                FROM crm_customers_in t                           
                                LEFT JOIN vtiger_pricebook p ON p.pricebookid = t.pricebook                         
                                LEFT JOIN vtiger_crmentity e ON e.crmid = p.pricebookid                           
                                WHERE e.deleted = 0 AND t.customer_id NOT IN (SELECT c.customer_id FROM vtiger_account c, vtiger_crmentity d WHERE d.crmid = c.accountid AND d.deleted = 0  AND c.customer_id IS NOT NULL)    AND t.customer_name NOT IN (SELECT c.accountname FROM vtiger_account c, vtiger_crmentity d WHERE d.crmid = c.accountid AND d.deleted = 0)                              
                                AND t.customer_code NOT IN (SELECT c.account_no FROM vtiger_account c, vtiger_crmentity d WHERE d.crmid = c.accountid AND d.deleted = 0 AND c.account_no IS NOT NULL) AND
                                t.legal_no NOT IN (SELECT c.legal_entity_code FROM vtiger_account c, vtiger_crmentity d WHERE d.crmid = c.accountid AND d.deleted = 0 AND c.account_no IS NOT NULL)
                                GROUP BY t.customer_id");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $orders_count = $sth->rowCount();


    $sth3 = $dbh->prepare("SELECT id FROM vtiger_crmentity_seq");
    $sth3->setFetchMode(PDO::FETCH_ASSOC);
    $sth3->execute(array());
    $seq = $sth3->fetch();

    $new_seq = $seq['id'] + $orders_count;

    if($orders_count > 0){
        $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
        $sth4->execute(array($new_seq)); 
    }

    
    $last_entity_record = $seq['id']+1;
    
    while ($row = $sth->fetch()) {

        if($row['type'] == 'legal') {
            $subject = 'Juridinis';    
        } else {
            $subject = 'Fizinis';    
        }            
    

        $user_id = 26;      
        
        $type = 'Pirkėjas';
        $ownership = 'Metrika';
        $date = date("Y-m-d H:i:s");
        $description = null;
        
        $pricebookid = 10965;                 
   
        $insert_entity = insert_entity3($dbh, 'Accounts', $last_entity_record, $user_id, NULL,'Metrika',  $row['customer_name'], $date);      
        
        $sth2->execute(array($last_entity_record, 
                            $row['customer_id'], 
                            $row['customer_code'], 
                            $row['customer_name'],                   
                            $subject, 
                            $row['contact_person_name'],
                            $row['phone'],
                            $row['email'],
                            $row['blocked'],
                            $row['region'],      
                            $row['municipality'], 
                            $row['settlement'],                     
                            $row['billing_address'], 
                            $row['post_code'],
                            $row['legal_no'],
                            $row['vat_no'],
                            $type,
                            $row['account_manager_id'],
                            $row['pricebookid'] != NULL ? $row['pricebookid'] : $pricebookid,                     
                            $row['modifydate'],                        
                            $ownership,
                            $date            
        ));

        $agnum_code = clean($row['customer_name']);
        $sth5->execute(array($last_entity_record,$agnum_code)); 
        
        $insert_customer_address_billing = insert_customer_address_billing($dbh, $last_entity_record, $row['sender_address'], $row['sender_postcode'], $row['default_sender_location_id'], $row['sender_city'], $row['country_code'], $row['sender_name']);
        $insert_customer_address_shipping = insert_customer_address_shipping($dbh, $last_entity_record, $row['consignee_address'], $row['consignee_postcode'], $row['default_consignee_location_id'], $row['consignee_city'], $row['country_code'], $row['consignee_name']);

        SaveAccountToVendors($dbh, $last_entity_record);
        
                    
        $last_entity_record++;         
    }

    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error!";
   echo $e->getMessage();
}

function clean($string) {          
    $string = str_replace(array('"','UAB',',',' '), array('','','',''), $string); // Removes special chars.     
    return $string; 
}

function SaveAccountToVendors($dbh, $account_id){  
    $date = date("Y-m-d H:i:s");    
    $result = $dbh->prepare('SELECT accountname,phone,email1,account_type,legal_entity_code,legal_vat_code,billing_address,municipality,post_code FROM vtiger_account WHERE accountid = ?');
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $result->execute(array($account_id)); 
    $account_info = $result->fetch();

    $accountname = $account_info['accountname'];
    $phone = $account_info['phone'];
    $email1 = $account_info['email1'];
    $billing_address = $account_info['billing_address'];
    $municipality = $account_info['municipality']; 
    $post_code = $account_info['post_code'];
    $legal_entity_code = $account_info['legal_entity_code'];
    $legal_vat_code = $account_info['legal_vat_code'];
    $account_type = $account_info['account_type'];

    $ven =  last_ven_id($dbh);      
    $last_entity_record =  get_last_entity_id_and_update($dbh, 1); 
    insert_entity3($dbh, 'Vendors', $last_entity_record, 26, NULL,'Metrika',  $accountname, $date);    

    $result2 = $dbh->prepare("INSERT INTO vtiger_vendor (vendorid,vendor_no,vendorname,phone,email,street,city,postalcode,accountid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $result2->execute(array($last_entity_record,$ven,$accountname, $phone, $email1, $billing_address,$municipality,$post_code,$account_id));

    $result = $dbh->prepare("INSERT INTO vtiger_vendorcf (vendorid,cf_1458, cf_1460, cf_1466) VALUES (?,?,?,?)");
    $result->execute(array($last_entity_record,$legal_entity_code,$legal_vat_code,$account_type));
}

function last_ven_id($dbh){
	$result = $dbh->prepare("SELECT vendor_no FROM vtiger_vendor ORDER BY vendorid DESC LIMIT 1"); 
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $result->execute();
    $num = substr($result->fetch()['vendor_no'], 3);

	$num = $num +1;
	$vel = 'VEN'.$num;
	return $vel;
}	

?>
