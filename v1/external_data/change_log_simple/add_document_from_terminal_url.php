<?php
include '../ws.config.php';
global $config;
global $root_directory;
require '../utils.php';

set_time_limit(900);


try {

    require('../mysql_connection.php');

    $dbh->beginTransaction(); 

    $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
    $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
    $last_timestamp->execute(array());
    $date_from = $last_timestamp->fetch();

    $formated_date = date("Y.m.d H:i:s",strtotime($date_from));

    $insert = $dbh->prepare('INSERT INTO app_terminal_images (load_id, file_id, importdate) VALUES (?,?,?)');
    $insert_shipment = $dbh->prepare('INSERT INTO app_terminal_images (shipment_id, file_id, importdate) VALUES (?,?,?)');

    $date_now = date('Y-m-d H:i:s');

    // NOTE Suformuojame MS SQL uzklausa i sandelio API

    // Uzklausa sandelio nuotraukoms
    $select = 'FILE_ID,LOAD_ID,FILE_NME,ENTRY_DATE,FILE_SYS_NME';
    $table = 'PACKAGES_FILES ';

    // Uzklausa terminalo pazeidimams
    $select2 = 'FILE_ID,SHIPMENT_ID,FILE_NME,ENTRY_DATE,FILE_SYS_NME';
    $table2 = 'ROUTES_SHIPMENTS_FILES_View ';

    $where = "ENTRY_DATE > '$formated_date'";   

    $headers = array(
      'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
    );

    // NOTE Siunciame uzklausa i sandeli
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => $select, 'table' => $table, 'where' => $where));				
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);   
    
    $date = date("Y-m-d");
    if(!file_exists("../logs/terminal_images/$date")){
      mkdir("../logs/terminal_images/$date", 0777, true); 
    }
       
    $dir = "terminal_images/$date";
    $logfile = "$dir/images.log"; 
    
    $terminal_images_load_id = [];
    foreach($data AS $row) {
      $terminal_images_load_id[$row['LOAD_ID']] = $row['LOAD_ID'];
    }

    $terminal_images_load_id = implode(',',$terminal_images_load_id);
    $check = $dbh->prepare("SELECT file_id, load_id FROM app_terminal_images WHERE load_id IN ($terminal_images_load_id)");

    $check->setFetchMode(PDO::FETCH_ASSOC);
    $check->execute();

    $terminal_existing_images = [];
    foreach($check AS $image) {
      $terminal_existing_images[$image['load_id']][$image['file_id']] = $image['file_id'];
    }

    $get_orderid = $dbh->query("SELECT id,external_load_id FROM vtiger_inventoryproductrel where external_load_id IN ($terminal_images_load_id) GROUP BY id");
    $get_orderid->setFetchMode(PDO::FETCH_ASSOC);
    $get_orderid->execute();

    $orderids = [];
    foreach($get_orderid AS $order){
      $orderids[$order['external_load_id']] = $order['id'];
    }  


  // Irasome sandelio nuotraukas
  foreach($data AS $row) {
    if(empty($terminal_existing_images[$row['LOAD_ID']][$row['FILE_ID']])){    
      $filename = "http://192.168.150.12:8080/sandelis/Files/".$row['FILE_NME']."";
      $filetitle = $row['FILE_SYS_NME'];
      $folderid = 4; 
      customers_log('File id: '.$row['FILE_ID'].' Name '.$row['FILE_SYS_NME'].' Load '.$row['LOAD_ID'] , $logfile);  
      if(!empty($orderids[$row['LOAD_ID']])){
        $salesorderid = $orderids[$row['LOAD_ID']];     
        $insert->execute(array($row['LOAD_ID'],$row['FILE_ID'],$date_now));
        $entity_id = get_and_insert_intity($dbh,'Documents', 26, NULL, 'Document', $date_now);
        insert_senotesrel($dbh, $entity_id, $salesorderid);
        insertNotes($dbh,$entity_id,$filetitle,$filename,$row['FILE_NME'],$folderid,'E',1);    
        insert_NoteCf($dbh, $entity_id,$row['ENTRY_DATE']);
    }
    }
  }


    // NOTE Kreipiames del sandelio pazeidimu nuotrauku

    $ch2 = curl_init();
    curl_setopt($ch2, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
    curl_setopt($ch2, CURLOPT_POST, 1);
    curl_setopt($ch2, CURLOPT_POSTFIELDS, array('select' => $select2, 'table' => $table2, 'where' => $where));				
    curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch2, CURLOPT_HEADER, 0);
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch2, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 1);
    $result2 = curl_exec($ch2);
    curl_close($ch2);
    $data2 = json_decode($result2, true); 




    $terminal_damages_shipment_id = [];
    foreach($data2 AS $row) {
      $terminal_damages_shipment_id[$row['SHIPMENT_ID']] = $row['SHIPMENT_ID'];
    }

    $terminal_damages_shipment_id = implode(',',$terminal_damages_shipment_id);   
    $check_shipment_id = $dbh->prepare("SELECT shipment_id,file_id FROM app_terminal_images WHERE shipment_id IN ($terminal_damages_shipment_id)");


    $check_shipment_id->setFetchMode(PDO::FETCH_ASSOC);
    $check_shipment_id->execute();

    $terminal_existing_damage_images = [];
    foreach($check_shipment_id AS $damage_image) {
      $terminal_existing_damage_images[$damage_image['shipment_id']][$damage_image['file_id']] = $damage_image['file_id'];
    }

    $get_orderid = $dbh->query("SELECT salesorderid,external_order_id FROM vtiger_salesorder where external_order_id IN ($terminal_damages_shipment_id) GROUP BY salesorderid");
    $get_orderid->setFetchMode(PDO::FETCH_ASSOC);
    $get_orderid->execute();

    $orderids = [];
    foreach($get_orderid AS $order){
      $orderids[$order['external_order_id']] = $order['salesorderid'];
    }  


  // Irasome sandelio pazeidimu nuotraukas
  foreach($data2 AS $row) {  
    if(empty($terminal_existing_damage_images[$row['SHIPMENT_ID']][$row['FILE_ID']])){   
      $filename = "http://192.168.150.12:8080/sandelis/Files/".$row['FILE_NME']."";
      $filetitle = $row['FILE_SYS_NME'];
      $folderid = 4; 

      customers_log('File id: '.$row['FILE_ID'].' Name '.$row['FILE_SYS_NME'].' Shipment '.$row['SHIPMENT_ID'] , $logfile);  

      $sth3 = $dbh->prepare('SELECT salesorderid FROM vtiger_salesorder where external_order_id = ? LIMIT 1');
      $sth3->setFetchMode(PDO::FETCH_ASSOC);
      $sth3->execute(array($row['SHIPMENT_ID']));
      $salesorderid = $sth3->fetch()['salesorderid']; 

      if(!empty($orderids[$row['SHIPMENT_ID']])){
        $salesorderid = $orderids[$row['SHIPMENT_ID']];
        $insert_shipment->execute(array($row['SHIPMENT_ID'],$row['FILE_ID'],$date_now));
        $entity_id = get_and_insert_intity($dbh,'Documents', 26, NULL, 'Document', $date_now);
        insert_senotesrel($dbh, $entity_id, $salesorderid);
        insertNotes($dbh,$entity_id,$filetitle,$filename,$row['FILE_NME'],$folderid,'E',1);    
        insert_NoteCf($dbh, $entity_id,$row['ENTRY_DATE']);
      }
    }
  }




  
  $dbh->commit();
    
} catch (PDOException $e) {
   $dbh->rollBack();    
   echo "Error!";
   echo $e->getMessage();
}



function get_and_insert_intity($dbh, $setype, $user_id, $description, $label, $date) {

  // Insert into vtiger_crmentity 
  $entity_id = get_last_entity_id_and_update($dbh,1); 

  if($entity_id){

      $sth2 = $dbh->prepare("SELECT * FROM vtiger_crmentity WHERE crmid = ?");
      $sth2->execute(array($entity_id));
      $crmnewid = $sth2->rowCount();
      
      // Insert into vtiger_crmentity 
     
      $sth = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, source, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
      $sql_params = array($entity_id, $user_id, $user_id, $setype, $description,'Metrika', $label, $date, $date);

      if(!$crmnewid){
          $sth->execute($sql_params);
      }
      return  $entity_id;
  }
}