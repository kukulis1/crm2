<?php

require '../PDO_CONNECT.php';

if(date("H") != '00'){

  try {

    $last_timestamp = $dbh->prepare("SELECT * FROM `app_hired_transport_last_update` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
    $last_timestamp->execute();
    $date_from = $last_timestamp->fetch();

    $select = "shipment.shipment_id, route.route_code, dr.driver_name, v.vehicle_code";
    $table = "shipment
    JOIN delivery ON delivery.shipment_id=shipment.shipment_id
    JOIN route_point_operation ON route_point_operation.delivery_id = delivery.delivery_id
    JOIN route_point ON route_point.route_point_id = route_point_operation.route_point_id
    JOIN route ON route.route_id = route_point.route_id
    JOIN log ON (log.table_id = 1 AND route.route_id = log.row_id)
    JOIN driver dr ON dr.driver_id=route.driver1_id
    JOIN vehicle v ON v.vehicle_id=route.vehicle_id";

    $where = "edit_date >= '$date_from->date' AND del_date IS NULL
    AND delivery.delivery_seq_no = (
        SELECT MAX(sub_delivery.delivery_seq_no)
        FROM delivery as sub_delivery
        WHERE sub_delivery.shipment_id = delivery.shipment_id
    )  
    GROUP BY shipment.shipment_id, route.route_id,  dr.driver_name, v.vehicle_code";


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'select' => $select, 'table' => $table, 'where' => $where));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);

    $routes = [];

    foreach ($data['response']['result'] ?? [] as $value) {
      $routes[$value['shipment_id']] = $value;
    }

    $shipment_ids = array_keys($routes);
    $imploded_shipment_ids = implode(',', ($shipment_ids ?? 0));

    $orders = $dbh->prepare("SELECT salesorderid, external_order_id FROM vtiger_salesorder WHERE external_order_id IN ($imploded_shipment_ids)");
    $orders->execute();

    $update_salesorder = $dbh->prepare("UPDATE vtiger_salesordercf SET cf_1558 = :route_code, 
                                                                       cf_1562 = :driver_name,
                                                                       cf_1560 = :vehicle_code
                                        WHERE salesorderid = :salesorderid");


    foreach ($orders ?? [] as $order) {
      if(!empty($routes[$order->external_order_id])){
        $update_salesorder->execute([
          ':route_code'   => $routes[$order->external_order_id]['route_code'] ?? '',
          ':driver_name'  => $routes[$order->external_order_id]['driver_name'] ?? '',
          ':vehicle_code' => $routes[$order->external_order_id]['vehicle_code'] ?? '',
          ':salesorderid' => $order->salesorderid
        ]);
      }
    }

  } catch (PDOException $e) {
    // NOTE Jei gauta klaida, išsiunčiam email su pranešimu administratoriui
    echo "Error!";
    echo $e->getMessage(); 
    sendReportMail($e->getMessage(),basename(__FILE__));
}



  }