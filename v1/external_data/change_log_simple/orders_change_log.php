<?php

error_reporting(0);
include '../ws.config.php';
global $config;
require '../utils.php';
require '../../../config.inc.php';

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

if(date("H") != '00'){

  try {

      require('../mysql_connection.php');

      // NOTE Pasiimamas paskutinio sėkmingo kreipimosi į metriką laikas
      $last_timestamp = $dbh->prepare("SELECT * FROM `app_last_update` WHERE status = 'success' ORDER BY id DESC LIMIT 1");
      $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
      $last_timestamp->execute(array());
      $date_from = $last_timestamp->fetch();
      $date_from = $date_from['date'];   
      $now = date("Y-m-d H:i");
      $minute = date("i", strtotime($now));  
      $minutes = change_minutes($minute); // Suapvalinam minutes, jei kreipimasis vėlavo.  
      $time = date("Y-m-d H:$minutes:00", strtotime($now));  // Gaunam laiką kurį įrašysim į last_update lentelę  

      // NOTE Gaunam bendrus nustatymus kada krovinys nestandartas
      $check = $dbh->prepare("SELECT `value` FROM app_other_settings WHERE title = 'LBL_CARGO_NOT_STANDART'");    
      $check->setFetchMode(PDO::FETCH_ASSOC);
      $check->execute(array());
      $default_taxablemeter = $check->fetch()['value'];

      $update_last_update_date = $dbh->prepare("INSERT INTO app_last_update  (`status`,`date`) VALUES (?,?)");

      $truncate_order_history = $dbh->prepare('TRUNCATE app_orders_update_temp');
      $truncate_order_history ->execute();

      $truncate_load_history = $dbh->prepare('TRUNCATE app_loads_update_temp');
      $truncate_load_history ->execute();

      $insert_old_values = $dbh->prepare("INSERT INTO app_orders_update_temp (bill_city, bill_code, bill_street, load_company, load_phone, ship_city, ship_code, ship_street, unload_company, unload_phone, sostatus, shipment_code, load_date_from, load_time_from, unload_date_from, unload_time_from, cf_855, cf_928, cf_1504, cf_1564, cf_1566, cf_1558, cf_1560, cf_1562, salesorderid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      $get_old_values_from_temp = $dbh->prepare('SELECT bill_city, bill_code, bill_street, load_company, load_phone, ship_city, ship_code, ship_street, unload_company, unload_phone, sostatus, shipment_code, load_date_from, load_time_from, unload_date_from, unload_time_from, cf_855, cf_928, cf_1504, cf_1564, cf_1566, cf_1558, cf_1560, cf_1562, salesorderid FROM app_orders_update_temp GROUP BY salesorderid'); 
      
      $insert_old_loads_values =  $dbh->prepare("INSERT INTO app_loads_update_temp (cf_1317, cf_1319, cf_1367, cf_1365, salesorderid) VALUES (?, ?, ?, ?, ?)");

      $get_old_loads_values =  $dbh->prepare("SELECT * FROM app_loads_update_temp GROUP BY salesorderid");

      $insert_order = $dbh->prepare('INSERT INTO vtiger_salesorder (salesorderid,
                                                          `subject`,
                                                          salesorder_no,
                                                          shipment_code,                                                               
                                                          accountid,
                                                          sostatus,                                                                              
                                                          load_date_from, 
                                                          load_time_from,
                                                          load_date_to, 
                                                          load_time_to,                                                               
                                                          unload_date_from, 
                                                          unload_time_from,
                                                          unload_date_to, 
                                                          unload_time_to, 
                                                          route_point_id,                                                                
                                                          external_order_id,
                                                          source,
                                                          import_date,
                                                          created_person) 
                                                            VALUES (:salesorderid, 
                                                                    :subject, 
                                                                    :salesorder_no, 
                                                                    :shipment_code, 
                                                                    :accountid, 
                                                                    :sostatus,                                        
                                                                    :load_date_from, 
                                                                    :load_time_from, 
                                                                    :load_date_to, 
                                                                    :load_time_to, 
                                                                    :unload_date_from, 
                                                                    :unload_time_from,
                                                                    :unload_date_to, 
                                                                    :unload_time_to, 
                                                                    :route_point_id,                                                                
                                                                    :external_order_id,
                                                                    :source,
                                                                    :import_date,
                                                                    :created_person)'); 

      $insert_order_cf = $dbh->prepare('INSERT INTO vtiger_salesordercf (salesorderid, cf_855,cf_928, cf_1504, cf_1558, cf_1560, cf_1562, cf_1564, cf_1566, cf_1661, cf_1663, cf_2032) VALUES (:salesorderid, :type, :mileage, :order_date, :route, :vehicle_number, :driver_name, :load_company_phone, :unload_company_code, :created_person, :edited_person, :customer_order_code)');                                      

      
      $update_salesorder = $dbh->prepare('UPDATE vtiger_salesorder SET    
                                                  accountid = :accountid, 
                                                  load_date_from = :load_date_from,                                                                  
                                                  load_time_from = :load_time_from, 
                                                  load_date_to = :load_date_to,
                                                  load_time_to = :load_time_to,
                                                  unload_date_from = :unload_date_from,
                                                  unload_time_from = :unload_time_from,  
                                                  unload_date_to = :unload_date_to,
                                                  unload_time_to = :unload_time_to, 
                                                  route_point_id = :route_point_id,
                                                  shipment_code = :shipment_code,
                                                  update_date = :update_date
                                        WHERE salesorderid = :salesorderid'); 
                                                  
      $update_salesordercf = $dbh->prepare('UPDATE vtiger_salesordercf SET 
                                                  cf_855 = :type, 
                                                  cf_928 = :mileage, 
                                                  cf_1504 = :order_date,                                  
                                                  cf_1558 = :route, 
                                                  cf_1560 = :vehicle_number, 
                                                  cf_1562 = :driver_name,
                                                  cf_1564 = :load_company_phone,
                                                  cf_1566 = :unload_company_code,
                                                  cf_2032 = :customer_order_code 
                                        WHERE salesorderid = :salesorderid'); 

      $update_entity = $dbh->prepare('UPDATE vtiger_crmentity SET label = :label, modifiedtime = :modifiedtime, deleted = 0 WHERE crmid = :crmid');

      $delete_cargos = $dbh->prepare('DELETE FROM vtiger_inventoryproductrel WHERE id = :id AND 
                                                                                  external_order_id = :external_order_id AND
                                                                                  productid = :productid');
      $delete_documents = $dbh->prepare('DELETE FROM vtiger_inventoryproductrel WHERE id = :id AND 
                                                                                external_order_id = :external_order_id AND
                                                                                productid = :productid AND
                                                                                inventory_type = :inventory_type');


      $delete_flags = $dbh->prepare('DELETE FROM vtiger_inventoryproductrel_flags WHERE flag_id = :flag_id'); 


      $insert_shipment_packages = $dbh->prepare('INSERT INTO vtiger_packages (packagesid,load_id,packagesno, packages_tks_kg, packages_tks_length, packages_tks_width, packages_tks_height, packages_tks_tare,tags, cf_20023) VALUES (:packagesid, :load_id, :packagesno, :packages_tks_kg, :packages_tks_length, :packages_tks_width, :packages_tks_height, :packages_tks_tare, :tags, :salesorderid)');

      $insert_shipment_packages_cf = $dbh->prepare('INSERT INTO vtiger_packagescf (packagesid) VALUES (:packagesid)');   
  

      // NOTE Kreipiamasi į savitarnos API su data
      $headers = array(
        'Authorization: Basic '.base64_encode("$API_USER:$API_PASS")
      );

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders_changes_v2.php");                    
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('date_from' => $date_from,'routes' => 1));	
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      $data = json_decode($result, true); 


    if($data['status'] == 200){
      
      $values = array();    

      // NOTE Patikrinama ar yra įrašu
      if(!empty($data['data']['shipments'])){
        $values[] = 'shipments';
      }  
      if(!empty($data['data']['deleted_shipments'])){
        $values[] = 'deleted_shipments';
      } 
      if(!empty($data['data']['deleted_loads'])){
        $values[] = 'deleted_loads';
      }
      if(!empty($data['data']['images'])){
        $values[] = 'images';
      }
      if(!empty($data['data']['shipment_packages'])){
        $values[] = 'shipment_packages';
      }
      
      // Jei nera nauju duomenu  irasom atnaujinimo data ir veiksmu toliau nebetesiam
      if(empty($values)){
        $update_last_update_date->execute(array('success',$time));  
        die();
      }     

      $date = date("Y-m-d H:i:s");

      if(!empty($data['data']['deleted_shipments'])){
        $deleted_external_order_id = implode(',',$data['data']['deleted_shipments']);

        $get_notdeleted_salesorders = $dbh->prepare("SELECT salesorderid FROM `vtiger_salesorder` WHERE external_order_id IN ($deleted_external_order_id)");

        $get_notdeleted_salesorders->setFetchMode(PDO::FETCH_ASSOC);
        $get_notdeleted_salesorders->execute(array());

        $notdeleted_salesorders = [];
        foreach($get_notdeleted_salesorders AS $id){
          $notdeleted_salesorders[$id['salesorderid']] = $id['salesorderid'];
        }

        if(count($notdeleted_salesorders) > 0){
          $imploded_notdeleted_salesorders = implode(',', $notdeleted_salesorders);


          $mark_order_deleted = $dbh->prepare("UPDATE vtiger_crmentity 
                                              SET modifiedtime = ?, deleted = 1
                                              WHERE crmid IN ($imploded_notdeleted_salesorders) 
                                              AND setype = 'SalesOrder'");  

          $delete_loads = $dbh->prepare("DELETE FROM vtiger_inventoryproductrel WHERE external_order_id IN ($deleted_external_order_id)");

          $mark_order_deleted->execute([$date]);
          $delete_loads->execute();
        }
      }

      if(!empty($data['data']['deleted_loads'])){
        $deleted_external_load_id = implode(',',$data['data']['deleted_loads']);
        $delete_loads = $dbh->prepare("DELETE FROM vtiger_inventoryproductrel WHERE external_load_id IN ($deleted_external_load_id)");
        $delete_loads->execute();
      }

      $orders_that_need_to_calculate_the_price = [];
      $insert_or_update = [];

    
      if(!empty($data['data']['shipments'])){
        $shipment_id = array_keys($data['data']['shipments']);
        $shipments = implode(',',array_keys($data['data']['shipments']));
    
        $check_existing_salesorders = $dbh->prepare("SELECT external_order_id,salesorderid FROM `vtiger_salesorder` WHERE external_order_id IN ($shipments)");
    
        $customer_ids = implode(',', $data['data']['customer_ids']);
        
        $get_accountid = $dbh->prepare("SELECT vtiger_account.accountid, vtiger_account.customer_id, cf_1635 AS meters,cf_1812 as doc_service 
                                        FROM vtiger_account 
                                        LEFT JOIN vtiger_accountscf ON vtiger_accountscf.accountid=vtiger_account.accountid 
                                        INNER JOIN vtiger_crmentity ON crmid=vtiger_account.accountid 
                                        WHERE vtiger_account.customer_id IN ($customer_ids)");
    
        $check_existing_salesorders->setFetchMode(PDO::FETCH_ASSOC);
        $check_existing_salesorders->execute(array()); 
        
        $external_order_id = [];
        foreach($check_existing_salesorders AS $id){
          $external_order_id[$id['external_order_id']] = $id['salesorderid'];
        }

        
        
        if(count($external_order_id) > 0){
          $imploded_salesorderid = implode(',', $external_order_id);
          
          // NOTE Surasom duomenis pries atnaujinima
          $get_old_values = $dbh->prepare("SELECT bill_city, bill_code, bill_street, load_company, load_phone, ship_city, ship_code, ship_street, unload_company, unload_phone, sostatus, shipment_code, load_date_from, load_time_from, unload_date_from, unload_time_from, cf_855, cf_928, cf_1504, cf_1564, cf_1566, cf_1558, cf_1560, cf_1562,cf_1317,cf_1319,cf_1367,cf_1365,vtiger_salesorder.salesorderid 
          FROM vtiger_salesorder
          JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
          JOIN vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
          JOIN vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
          WHERE vtiger_salesorder.salesorderid IN ($imploded_salesorderid)   
          GROUP BY vtiger_salesorder.salesorderid");

          $get_old_values->setFetchMode(PDO::FETCH_ASSOC);
          $get_old_values->execute();

          foreach($get_old_values AS $item){
            $insert_old_values->execute(array($item['bill_city'],$item['bill_code'],$item['bill_street'],$item['load_company'],$item['load_phone'],$item['ship_city'],$item['ship_code'],$item['ship_street'],$item['unload_company'],$item['unload_phone'],$item['sostatus'],$item['shipment_code'],$item['load_date_from'],$item['load_time_from'],$item['unload_date_from'],$item['unload_time_from'],$item['cf_855'],$item['cf_928'],$item['cf_1504'],$item['cf_1564'],$item['cf_1566'],$item['cf_1558'],$item['cf_1560'],$item['cf_1562'],$item['salesorderid']));
            $insert_old_loads_values->execute([$item['cf_1317'],$item['cf_1319'],$item['cf_1367'],$item['cf_1365'],$item['salesorderid']]);
          }

        }

        // Senu duomenu surasymo pabaiga

        $get_accountid->setFetchMode(PDO::FETCH_ASSOC);
        $get_accountid->execute();

        $accountid_arr = [];
        foreach($get_accountid AS $acc){       
          $accountid_arr[$acc['customer_id']] = ['accountid' => $acc['accountid'], 'meters' => $acc['meters'],'doc_service' => $acc['doc_service']];
        }      


        // Gaunam insertinamu uzsakymu kieki
        $orders_count = count($shipment_id) - count($external_order_id);

        foreach($data['data']['shipments'] AS $metrika_order_id => $order){ 

          $get_user_info = ['accountid' => 1, 'meters' => '', 'doc_service' => 0.5];

          if(!empty($accountid_arr[$order['customer_id']])){
            $get_user_info = $accountid_arr[$order['customer_id']];
          }     

          $user_taxablemeter = $get_user_info['meters'];
          $accountid = $get_user_info['accountid']; 
          $doc_service = $get_user_info['doc_service'];         

          $shipment_type = ($order['shipment_type'] == 'parcel' ? 'Transporto užsakymas' : 'Perkraustymo užsakymas');
          $order['load_post_code'] = str_replace(array('LT','lt', 'LV', 'lv','-', ' '), '',$order['load_post_code']);
          $order['unload_post_code'] = str_replace(array('LT', 'lt','LV','lv', '-', ' '), '',$order['unload_post_code']);
          $order['load_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $order['load_post_code']);
          $order['unload_post_code'] = preg_replace('/[^A-Za-z0-9\-]/', '', $order['unload_post_code']);

          $load_date_from = date("Y-m-d", strtotime($order['load_date_from']));
          $load_date_to = date("Y-m-d", strtotime($order['load_date_to']));
          $load_time_from = date("H:i:s", strtotime($order['load_date_from']));
          $load_time_to = date("H:i:s", strtotime($order['load_date_to']));                        
          $unload_date_from = date("Y-m-d", strtotime($order['unload_date_from']));
          $unload_date_to = date("Y-m-d", strtotime($order['unload_date_to']));
          $unload_time_from = date("H:i:s", strtotime($order['unload_date_from']));
          $unload_time_to = date("H:i:s", strtotime($order['unload_date_to'])); 

          if(empty($order['route_id'])) $order['route_id'] = '';
          if(empty($order['route_code'])) $order['route_code'] = '';
          if(empty($order['vehicle_registration_number'])) $order['vehicle_registration_number'] = '';
          if(empty($order['driver1_name'])) $order['driver1_name'] = '';


          // NOTE Patikrinam ar toks uzsakymas yra crme'e, jei yra atnaujinam, jei nera ikeliam
          if(!empty($external_order_id[$metrika_order_id])){
            // update 

            $salesorderid = $external_order_id[$metrika_order_id]; 
            $insert_or_update[$salesorderid] = 'UPDATE';          

            $update_salesorder->execute([
                                        ':accountid' => $accountid,
                                        ':load_date_from' => $load_date_from,
                                        ':load_time_from' => $load_time_from,
                                        ':load_date_to' => $load_date_to,
                                        ':load_time_to' => $load_time_to,
                                        ':unload_date_from' => $unload_date_from,
                                        ':unload_time_from' => $unload_time_from,
                                        ':unload_date_to' => $unload_date_to,
                                        ':unload_time_to' => $unload_time_to,
                                        ':route_point_id' => $order['route_id'],
                                        ':shipment_code' => $order['shipment_code'],
                                        ':update_date' => $date,
                                        ':salesorderid' => $salesorderid
                                      ]);

            $update_salesordercf->execute([
                                        ':type' => $shipment_type, 
                                        ':mileage' => round($order['shipment_direct_km'],2), 
                                        ':order_date' => $order['order_date'],                                      
                                        ':route' =>  $order['route_code'], 
                                        ':vehicle_number' => $order['vehicle_registration_number'], 
                                        ':driver_name' => $order['driver1_name'], 
                                        ':load_company_phone' => $order['load_company_phone'], 
                                        ':unload_company_code' => $order['unload_company_phone'],          
                                        ':customer_order_code' => $order['customer_order_code'],
                                        ':salesorderid' => $salesorderid
                                      ]);           
        
        
            $update_entity->execute([
              ':label' => $order['shipment_code'],
              ':modifiedtime' => $date,
              ':crmid' => $salesorderid
            ]);

            update_load_address($dbh, $salesorderid, $order['load_address'], $order['load_post_code'], $order['load_municipality'], $order['load_country_code'], $order['load_company_name'], $order['load_company_code'], ($order['load_company_contact'] ?: ''), $order['load_company_phone']);
            
            update_unload_address($dbh, $salesorderid, $order['unload_address'], $order['unload_post_code'], $order['unload_municipality'], $order['unload_country_code'], $order['unload_company_name'], $order['unload_company_code'], ($order['unload_company_contact'] ?: ''), $order['unload_company_phone']); 

            $delete_cargos->execute([
              ':id' => $salesorderid,
              ':external_order_id' => $metrika_order_id,
              ':productid' => 14244
            ]); 
            
            $delete_flags->execute([
              ':flag_id' => $salesorderid
            ]);

            $delete_documents->execute([
              ':id' => $salesorderid,
              ':external_order_id' => $metrika_order_id,
              ':productid' => 36641,
              ':inventory_type' => 'documents'
            ]); 

            insert_loads_and_flags($dbh,$salesorderid,$metrika_order_id,$order,$user_taxablemeter,$default_taxablemeter,$date,$doc_service);
          
          }else{
            // insert

            if($orders_count > 0){           
              
              if($order['order_date'] > '2019-12-31' || '1900-01-01' == $order['order_date']){
          
                $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count);   
                $insert_or_update[$last_entity_record] = 'INSERT';

                insert_entity3($dbh, 'SalesOrder', $last_entity_record, 26, NULL, 'Metrika', $order['shipment_code'], $date);     

                $orders_that_need_to_calculate_the_price[$last_entity_record] = $last_entity_record;

                $insert_order->execute([
                  ':salesorderid' => $last_entity_record, 
                  ':subject' => $order['shipment_code'], 
                  ':salesorder_no' => $metrika_order_id, 
                  ':shipment_code' => $order['shipment_code'], 
                  ':accountid' => $accountid, 
                  ':sostatus' => 'in progress',           
                  ':load_date_from' => $load_date_from, 
                  ':load_time_from' => $load_time_from, 
                  ':load_date_to' => $load_date_to, 
                  ':load_time_to' => $load_time_to, 
                  ':unload_date_from' => $unload_date_from, 
                  ':unload_time_from' => $unload_time_from,
                  ':unload_date_to' => $unload_date_to, 
                  ':unload_time_to' => $unload_time_to, 
                  ':route_point_id' =>  $order['route_id'],                                                                
                  ':external_order_id' => $metrika_order_id,
                  ':source' => 'Metrika',
                  ':import_date' => $date,
                  ':created_person' => $order['person_name']
                ]); 

                $insert_order_cf->execute([':salesorderid' => $last_entity_record, 
                                            ':type' => $shipment_type, 
                                            ':mileage' => round($order['shipment_direct_km'],2), 
                                            ':order_date' => $order['order_date'], 
                                            ':route' =>  $order['route_code'], 
                                            ':vehicle_number' => $order['vehicle_registration_number'], 
                                            ':driver_name' => $order['driver1_name'], 
                                            ':load_company_phone' => $order['load_company_phone'], 
                                            ':unload_company_code' => $order['unload_company_phone'], 
                                            ':created_person' => 26, 
                                            ':edited_person' => 26, 
                                            ':customer_order_code' => $order['customer_order_code']
                                          ]);


                insert_load_address($dbh, $last_entity_record, $order['load_address'], $order['load_post_code'], $order['load_municipality'], $order['load_country_code'], $order['load_company_name'], $order['load_company_code'], ($order['load_company_contact'] ?: ''), $order['load_company_phone']);

                insert_unload_address($dbh, $last_entity_record, $order['unload_address'], $order['unload_post_code'], $order['unload_municipality'], $order['unload_country_code'], $order['unload_company_name'], $order['unload_company_code'], ($order['unload_company_contact'] ?: ''), $order['unload_company_phone']);     

                insert_loads_and_flags($dbh,$last_entity_record,$metrika_order_id,$order,$user_taxablemeter,$default_taxablemeter,$date,$doc_service);

                $last_entity_record++;
                            
              }
            }          
          }
        }
            
      

        // NOTE insert HISTORY
        $get_old_values_from_temp->setFetchMode(PDO::FETCH_ASSOC);
        $get_old_values_from_temp->execute();

        if($get_old_values_from_temp->rowCount() > 0){

          // NOTE Gaunam naujas duomenis, nes uzsakymas jau atnaujintas
          $get_old_values->setFetchMode(PDO::FETCH_ASSOC);
          $get_old_values->execute();

          $new_values_arr = [];
          foreach($get_old_values AS $new_values){
            $new_values_arr[$new_values['salesorderid']] = $new_values;
          }

          $values = ['load_date_from','load_time_from','unload_date_from','unload_time_from','bill_street', 'bill_city', 'bill_code', 'load_company', 'ship_street', 'ship_city', 'ship_code', 'shipment_code', 'cf_928', 'cf_1564', 'cf_1566', 'cf_1558', 'cf_1560', 'cf_1562','cf_855'];  
      

          foreach($get_old_values_from_temp AS $result){ 
            foreach($values as $column){ 
              if(!empty($new_values_arr[$result['salesorderid']][$column])){              
                if($new_values_arr[$result['salesorderid']][$column] != $result[$column]){   
                  if(in_array($column,['bill_code','ship_code','cf_928'])){
                  $orders_that_need_to_calculate_the_price[$result['salesorderid']] = $result['salesorderid'];  
                  }
                  $last_modtracker_id = last_modtracker_record($dbh);
                  insert_modtracker_basic($dbh, $last_modtracker_id, $result['salesorderid'], 'SalesOrder', 26, $date,0);            
                  insert_modtracker_detail($dbh, $last_modtracker_id, $column, $result[$column],$new_values_arr[$result['salesorderid']][$column]);
                }
              }
            }            
          }

          $values2 = ['cf_1317', 'cf_1319', 'cf_1367', 'cf_1365'];  

          $get_old_loads_values->setFetchMode(PDO::FETCH_ASSOC);
          $get_old_loads_values->execute();
          
          foreach($get_old_loads_values AS $load){
            foreach($values2 as $column2){ 
              if(!empty($new_values_arr[$load['salesorderid']][$column2])){              
                if($new_values_arr[$load['salesorderid']][$column2] != $load[$column2]){     
                  $orders_that_need_to_calculate_the_price[$load['salesorderid']] = $load['salesorderid'];          
                  $last_modtracker_id = last_modtracker_record($dbh);
                  insert_modtracker_basic($dbh, $last_modtracker_id, $load['salesorderid'], 'SalesOrder', 26, $date,0);            
                  insert_modtracker_detail($dbh, $last_modtracker_id, $column2, $load[$column2],$new_values_arr[$load['salesorderid']][$column2]);
                }
              }
            }
          }
        }
      }

      // NOTE insert packages
      if(isset($data['data']['shipment_packages'])){     
        if(count($data['data']['shipment_packages']) > 0){     
          $shipment_packages = implode(',',array_keys($data['data']['shipment_packages']));
      
          $get_salesorderid = $dbh->prepare("SELECT external_order_id,salesorderid FROM `vtiger_salesorder` WHERE external_order_id IN ($shipment_packages)");

          $get_salesorderid->setFetchMode(PDO::FETCH_ASSOC);
          $get_salesorderid->execute(array()); 
          
          $shipment_packages_salesorderid = [];
          foreach($get_salesorderid AS $orderid){
            $shipment_packages_salesorderid[$orderid['external_order_id']] = $orderid['salesorderid'];
          }

          if(!empty($shipment_packages_salesorderid)){
            deleteShipmentPackages($dbh,implode(',',$shipment_packages_salesorderid));
          }

          $total_packages = array_reduce($data['data']['shipment_packages'],function($carry,$item){
            return $carry+count($item);
          });
    
          $last_entity_record = get_last_entity_id_and_update($dbh,$total_packages);       
          
          foreach($data['data']['shipment_packages'] AS $shipment_id => $packages){  
            foreach($packages AS $package){   
              if(!empty($shipment_packages_salesorderid[$shipment_id])){
                $insert_shipment_packages->execute([':packagesid' => $last_entity_record,
                                                    ':load_id' => $package['load_id'],
                                                    ':packagesno' => $package['package_id'],
                                                    ':packages_tks_kg' => $package['weight_kg'],
                                                    ':packages_tks_length' => $package['length_m'],
                                                    ':packages_tks_width' => $package['width_m'],
                                                    ':packages_tks_height' => $package['height_m'],
                                                    ':packages_tks_tare' => $package['tare_name'],
                                                    ':tags' => $package['package_code'],
                                                    ':salesorderid' => $shipment_packages_salesorderid[$shipment_id] 
                                                  ]);

                $insert_shipment_packages_cf->execute([':packagesid' => $last_entity_record]); 
                insert_entity3($dbh, 'Packages', $last_entity_record, 26, NULL,'Metrika', $last_entity_record, date("Y-m-d H:i:s"));        
              }  
              $last_entity_record++;    
            } 
          }
        }
      }

      // NOTE insert images from app
      if(count($data['data']['images']) > 0){
        $shipment_images = implode(',',array_keys($data['data']['images']));
        $get_salesorderid = $dbh->prepare("SELECT external_order_id,salesorderid FROM `vtiger_salesorder` WHERE external_order_id IN ($shipment_images)");

        $get_salesorderid->setFetchMode(PDO::FETCH_ASSOC);
        $get_salesorderid->execute([]);

        $shipment_images_salesorderid = [];
        foreach($get_salesorderid AS $orderid){
          $shipment_images_salesorderid[$orderid['external_order_id']] = $orderid['salesorderid'];
        }    
        
        $imploded_salesorderid = implode(',',$shipment_images_salesorderid);
        $get_exist_images = $dbh->prepare("SELECT title
                                            FROM vtiger_senotesrel
                                            JOIN vtiger_notes ON vtiger_notes.notesid=vtiger_senotesrel.notesid
                                            WHERE crmid IN ($imploded_salesorderid)");

        $get_exist_images->setFetchMode(PDO::FETCH_ASSOC);
        $get_exist_images->execute([]);                                 

        $existed_images = [];
        if($get_exist_images->rowCount() > 0){
          foreach($get_exist_images AS $image_titles){
            $existed_images[$image_titles['title']] = $image_titles['title'];
          }         
        }                               

        $total_packages = array_reduce($data['data']['images'],function($carry,$item){
          return $carry+count($item);
        });
  
        $last_entity_record = get_last_entity_id_and_update($dbh,$total_packages);

        foreach($data['data']['images'] AS $shipment_id => $images) {
          foreach($images AS $img){
            if(empty($existed_images[$img['file_name']]) && empty($existed_images[$img['file_id']])){
              // Suformuojam img url kuria irasysim
              $filename = "https://mobil.parnasas.lt:8084/api/download?file_id=".$img['file_id']."";
              $filetitle = ($img['file_name'] ?:$img['file_id']);
              $folderid = ($img['file_type'] == 'pod' ? 2 : 3); // Pagal id nustatom i kuri folderi irasysim
               if(!empty($shipment_images_salesorderid[$shipment_id])){
                insert_entity3($dbh, 'Documents', $last_entity_record, 26, NULL,'Metrika', $last_entity_record, date("Y-m-d H:i:s"));  
                insert_senotesrel($dbh, $last_entity_record, $shipment_images_salesorderid[$shipment_id]);
                insertNotes($dbh,$last_entity_record,$filetitle,$filename,$img['file_description'],$folderid,'E',1);    
                insert_NoteCf($dbh, $last_entity_record,$img['file_created']);
              }
            }
            $last_entity_record++;
          }
        }
      }      
    

      if(count($orders_that_need_to_calculate_the_price) > 0){
          // Paskaiciuojam kaina visiems ikeltiems/atnaujintiems uzsakymams        
          calculatePriceByExternalOrderId($dbh, implode(',',$orders_that_need_to_calculate_the_price),$insert_or_update);
      }


      if(!empty($data['data']['shipments'])){
        // NOTE Pridedam krovos darbus
        $stevedoring_query = $dbh->prepare("SELECT c.salesorderid, cf.cf_1572 as stevedoring, (SELECT count(id) FROM vtiger_inventoryproductrel WHERE vtiger_inventoryproductrel.id=c.salesorderid) AS total              
                                            FROM vtiger_salesorder c 
                                            LEFT JOIN vtiger_account a ON a.accountid = c.accountid   
                                            LEFT JOIN vtiger_accountscf ac ON ac.accountid = a.accountid   
                                            LEFT JOIN vtiger_crmentity e ON e.crmid = a.accountid                           
                                            LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=c.salesorderid                                    
                                            WHERE c.external_order_id IN ($shipments) AND e.deleted = 0 AND ac.cf_1574 = 'Automatu'
                                            GROUP BY c.salesorderid");  

        $get_salesorderid_instead_shipmentid = $dbh->prepare("SELECT CONCAT(salesorderid) AS id FROM vtiger_salesorder WHERE external_order_id IN ($shipments)");
        $get_salesorderid_instead_shipmentid->setFetchMode(PDO::FETCH_ASSOC);     
        $get_salesorderid_instead_shipmentid->execute(array());

        $salesorderid_arr = $get_salesorderid_instead_shipmentid->fetchAll();

        $salesorderids = implode(',', array_map(function($a) {  return array_pop($a); }, $salesorderid_arr));
        
        if(count($salesorderid_arr) > 0){              
    
          $check_service_stevedoring_exist_service_exist = $dbh->prepare("SELECT id FROM vtiger_inventoryproductrel WHERE id IN ($salesorderids) AND productid = 36641 AND `service` = 2 AND inventory_type = 'stevedoring'"); 
    
          $insert_service = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel  (id, productid, sequence_no, quantity, margin, `service`, inventory_type) VALUES (:id, :productid, :sequence_no, :quantity, :margin, :service, :inventory_type)");  
    
          $update_service = $dbh->prepare("UPDATE vtiger_inventoryproductrel SET margin = ? WHERE id = ? AND productid = 36641 AND inventory_type = 'stevedoring'");
    
          $delete_service = $dbh->prepare("DELETE FROM vtiger_inventoryproductrel WHERE id = ? AND productid = 36641 AND inventory_type = 'stevedoring'");
    
          $stevedoring_query->setFetchMode(PDO::FETCH_ASSOC);     
          $stevedoring_query->execute(array());
    
          $check_service_stevedoring_exist_service_exist->setFetchMode(PDO::FETCH_ASSOC);     
          $check_service_stevedoring_exist_service_exist->execute(array());
    
          $service_stevedoring_exist = [];
          foreach ($check_service_stevedoring_exist_service_exist as $service) {
            $service_stevedoring_exist[$service['id']] = $service['id'];
          }
    
          foreach($stevedoring_query AS $row) { 
            if(!isset($service_stevedoring_exist[$row['salesorderid']])){
              if(!empty($row['stevedoring']) && $row['stevedoring'] > 0){           
    
                $insert_service->execute([':id' => $row['salesorderid'],
                                          ':productid' => 36641,
                                          ':sequence_no' => $row['total']+1,
                                          ':quantity' => 1,
                                          ':margin' => $row['stevedoring'],
                                          ':service' => 2,
                                          ':inventory_type' => 'stevedoring'
                                          ]
                );   
              }
            }else{
              if(!empty($row['stevedoring']) && $row['stevedoring'] > 0){   
                $update_service->execute(array($row['stevedoring'], $row['salesorderid'])); 
              }else{
                $delete_service->execute(array($row['salesorderid']));
              }
            }
          }

        }
  
      }


      // NOTE Jei niekur negauta klaida įrašom paskutinio atnaujinimo datą
      $update_last_update_date->execute(array('success',$time));     

    }     


  } catch (PDOException $e) {
      // NOTE Jei gauta klaida, įrašom atnaujinimo datą su žyma fail ir išsiunčiam email su pranešimu administratoriui
      echo "Error!";
      echo $e->getMessage();
  
      $update_last_update_date->execute(array('fail',$time));  
      $logfile = 'app_orders.log';
      customers_log($e->getMessage(), $logfile);   
      sendReportMail($e->getMessage(),basename(__FILE__));
  }
}
