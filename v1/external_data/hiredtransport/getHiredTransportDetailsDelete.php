<?php
set_time_limit(500);

include '../ws.config.php';
global $config;
require '../utils.php';

// error_reporting(E_ALL);

try {
  require('../mysql_connection.php'); 

  $last_timestamp = $dbh->prepare("SELECT * FROM app_hired_transport_last_update ORDER BY id DESC LIMIT 1");
  $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
  $last_timestamp->execute(array());
  $date_from = $last_timestamp->fetch();
  $date_from = $date_from['date'];
  $date = date("Y-m-d H:i:s");

  $now = date("Y-m-d H:i");
  $minute = date("i", strtotime($now));
  $minutes = change_minutes($minute);
  $time = date("Y-m-d H:$minutes:00", strtotime($now)); 


  $truncate = $dbh->prepare("TRUNCATE app_hired_transport_delete");
  $truncate->execute(array());  


  $select = "r.route_id";
  $from = "route r 
          JOIN log ON (log.table_id = 1 AND r.route_id = log.row_id)     
          JOIN vehicle v ON v.vehicle_id=r.vehicle_id
          LEFT JOIN supplier s ON s.supplier_id=v.supplier_id
          JOIN driver dr ON dr.driver_id=r.driver1_id";

  $where = "to_char(log.del_date, 'YYYY-MM-DD HH24:MI') >= '$date_from'  AND CASE WHEN v.supplier_id IS NOT NULL THEN v.supplier_id != 1 
  WHEN v.supplier_id IS NULL THEN v.supplier_id IS NULL
  END
  GROUP BY  r.route_code, v.vehicle_code, r.print_note,r.route_date,v.registration_number, r.cost_agreed,r.route_id,v.supplier_id,s.legal_no,s.vat_no,dr.driver_name";


  $insert_query = 'INSERT INTO app_hired_transport_delete  (route_id) VALUES (?)';
  $execute = array('route_id');

  $check_array = [];
    
  $insert = $dbh->prepare("INSERT INTO app_hired_transport_last_update (date) VALUES (?)");
  $insert->execute(array($time));  
     
  $update_records = getTableWithSelectSimple($select,$from, $dbh, $where,$insert_query,$check_array,$execute,'route_id'); 

  if($update_records){
    $delete = $dbh->prepare("SELECT purchaseorderid
                              FROM app_hired_transport_delete t 
                              JOIN vtiger_purchaseorder_hired_transport d ON d.route_id=t.route_id
                              GROUP BY d.route_id
                              ORDER BY id");

     $delete->setFetchMode(PDO::FETCH_ASSOC);
     $delete->execute(array()); 

     $delete_entity = $dbh->prepare("UPDATE vtiger_crmentity SET modifiedtime = ?, deleted = 1 WHERE crmid = ?");

      foreach($delete AS $row){  
        $delete_entity->execute(array($date,$row['purchaseorderid']));
      }
  }
  
  
  


} catch (PDOException $e) {
 echo "Error!";
 echo $e->getMessage();
}