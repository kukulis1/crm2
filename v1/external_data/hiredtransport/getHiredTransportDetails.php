<?php
set_time_limit(500);

include '../ws.config.php';
global $config;
require '../utils.php';

error_reporting(E_ALL);

try {
  require('../mysql_connection.php'); 

  $last_timestamp = $dbh->prepare("SELECT * FROM app_hired_transport_last_update ORDER BY id DESC LIMIT 1");
  $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
  $last_timestamp->execute(array());
  $date_from = $last_timestamp->fetch();
  $date_from2 = $date_from['date'];
  $date_from = date("Y-m-d");


  // SELECT  route.route_id, route.route_code, location.direction AS direction_from, location.municipality AS location_from,
  // location2.direction AS direction_to, location2.municipality AS location_to, route.route_date, transaction_user
  //             FROM shipment
  //       LEFT JOIN delivery ON delivery.shipment_id=shipment.shipment_id
  //             LEFT JOIN route_point_operation 
  //                 ON route_point_operation.delivery_id = delivery.delivery_id
  //             LEFT JOIN route_point 
  //                 ON route_point.route_point_id = route_point_operation.route_point_id
  //             LEFT JOIN route 
  //                 ON route.route_id = route_point.route_id
  //           LEFT JOIN location 
  //         ON location.location_id=route.start_location_id
  //       LEFT JOIN location location2
  //         ON location2.location_id=shipment.consignee_location_id
  //             WHERE route.route_code LIKE 'Mar DR 267634%' 
  //                 AND delivery.delivery_seq_no = (
  //                     SELECT MAX(sub_delivery.delivery_seq_no)
  //                     FROM delivery as sub_delivery
  //                     WHERE sub_delivery.shipment_id = delivery.shipment_id
  //                 )
  //         AND location2.direction IS NOT NULL
  //             GROUP BY route.route_id, route.route_code,location.direction, location.municipality, location2.direction, location2.municipality,transaction_user
      



  // NOTE ieskom tu kurie sukurti siandien
  $select = "r.route_code, v.vehicle_code, r.print_note,r.route_date,v.registration_number, r.cost_agreed,r.route_id,v.supplier_id,s.legal_no,s.vat_no,dr.driver_name, transaction_user";
  $from = "route r          
          JOIN vehicle v ON v.vehicle_id=r.vehicle_id
          LEFT JOIN supplier s ON s.supplier_id=v.supplier_id
          JOIN driver dr ON dr.driver_id=r.driver1_id";

  $where = "r.route_date = '$date_from'  AND CASE WHEN v.supplier_id IS NOT NULL THEN v.supplier_id != 1 
  WHEN v.supplier_id IS NULL THEN v.supplier_id IS NULL
  END
  GROUP BY  r.route_code, v.vehicle_code, r.print_note,r.route_date,v.registration_number, r.cost_agreed,r.route_id,v.supplier_id,s.legal_no,s.vat_no,dr.driver_name,transaction_user";

  // NOTE ieskom tu kurie buvo sukurti atgaline data
  $select2 = "r.route_code, v.vehicle_code, r.print_note,r.route_date,v.registration_number, r.cost_agreed,r.route_id,v.supplier_id,s.legal_no,s.vat_no,dr.driver_name, transaction_user";
  $from2 = "route r 
          JOIN log ON (log.table_id = 1 AND r.route_id = log.row_id)     
          JOIN vehicle v ON v.vehicle_id=r.vehicle_id
          LEFT JOIN supplier s ON s.supplier_id=v.supplier_id
          JOIN driver dr ON dr.driver_id=r.driver1_id";

  $where2 = "to_char(log.edit_date, 'YYYY-MM-DD HH24:MI') >= '$date_from2'  AND CASE WHEN v.supplier_id IS NOT NULL THEN v.supplier_id != 1 
  WHEN v.supplier_id IS NULL THEN v.supplier_id IS NULL
  END
  GROUP BY  r.route_code, v.vehicle_code, r.print_note,r.route_date,v.registration_number, r.cost_agreed,r.route_id,v.supplier_id,s.legal_no,s.vat_no,dr.driver_name,transaction_user";



  $insert_query = 'INSERT INTO app_hired_transport (route_id, route_code, driver_name, vehicle_code, print_note,cost_agreed, registration_number, order_date, transaction_user) VALUES (?,?,?,?,?,?,?,?,?)';
  $execute = array('route_id','route_code','driver_name','vehicle_code','print_note','cost_agreed','registration_number','route_date','transaction_user');

  $check_query = $dbh->prepare('SELECT route_id FROM app_hired_transport');
  $check_query->setFetchMode(PDO::FETCH_ASSOC);
  $check_query->execute(array());


  $check_array = [];
  foreach($check_query AS $route){  
   $check_array[$route['route_id']] = $route['route_id'];
  }
  
  $get_records = getTableWithSelectSimple($select,$from, $dbh, $where,$insert_query,$check_array,$execute,'route_id'); 
  $get_records2 = getTableWithSelectSimple($select2,$from2, $dbh, $where2,$insert_query,$check_array,$execute,'route_id'); 


  if($get_records OR $get_records2){

    $insert = $dbh->prepare("SELECT t.route_id,order_date, cost_agreed, driver_name, print_note, registration_number,route_code, location_from, location_to, transaction_user
                             FROM app_hired_transport t                            
                             LEFT JOIN vtiger_purchaseorder_hired_transport d ON d.route_id=t.route_id
                             WHERE d.purchaseorderid IS NULL
                             GROUP BY route_id");

    $get_vendor = $dbh->prepare("SELECT v.vendorid,cf_1462 AS iban, count(purchaseorderid) AS purchases FROM vtiger_vendor v 
                                  JOIN vtiger_vendorcf vcf ON vcf.vendorid=v.vendorid
                                  INNER JOIN vtiger_crmentity ON crmid=v.vendorid
                                  LEFT JOIN vtiger_purchaseorder ON  vtiger_purchaseorder.vendorid=v.vendorid
                                  WHERE vendorname LIKE ? OR vendorname LIKE ? AND deleted = 0 
                                  ORDER BY purchases DESC LIMIT 1");

     $get_vendor->setFetchMode(PDO::FETCH_ASSOC);

     $insert->setFetchMode(PDO::FETCH_ASSOC);
     $insert->execute(array()); 
     $records = $insert->rowCount();   
     
    $insert_purchase = $dbh->prepare("INSERT INTO vtiger_purchaseorderdraft (purchaseorderdraftid, purchaseorderdraft_tks_vendor, purchaseorderdraft_tks_amount, purchaseorderdraft_tks_routedate, purchaseorderdraft_tks_registration_number, purchaseorderdraft_tks_route_code, purchaseorderdraft_tks_driver_name, purchaseorderdraft_tks_note, transaction_user) VALUES (?,?,?,?,?,?,?,?,?)");

  

     $check_insert = $dbh->prepare("INSERT INTO vtiger_purchaseorder_hired_transport (purchaseorderid, route_id, import_date) VALUES (?,?,?)");

     $last_entity_record = get_last_entity_id_and_update($dbh,$records); 
   
      foreach($insert AS $row){ 
        $date = date("Y-m-d H:i:s");  
       
        $driver_name = $row['driver_name'];
        $car_nr = $row['registration_number'];
       
        $get_vendor->execute(array("%$driver_name%","%$car_nr%"));
        $vendor_info = $get_vendor->fetch();        

        $insert_entity = insert_entity3($dbh, 'Purchaseorderdraft', $last_entity_record, 26, NULL,'Metrika', $row['route_code'], $date); 

        $insert_purchase->execute(array($last_entity_record, $vendor_info['vendorid'], $row['cost_agreed'], $row['order_date'], $row['registration_number'],$row['route_code'], $row['driver_name'], $row['print_note'], $row['transaction_user']));


        $check_insert->execute(array($last_entity_record, $row['route_id'], $date)); 
        $last_entity_record++;      
      }
  }
  


} catch (PDOException $e) {
 echo "Error!";
 echo $e->getMessage();
 sendReportMail($e->getMessage(),basename(__FILE__));
}
