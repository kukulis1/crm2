<?php
set_time_limit(500);

include '../ws.config.php';
global $config;
require '../utils.php';

// error_reporting(E_ALL);

try {
  require('../mysql_connection.php'); 

  $last_timestamp = $dbh->prepare("SELECT * FROM app_hired_transport_last_update ORDER BY id DESC LIMIT 1");
  $last_timestamp->setFetchMode(PDO::FETCH_ASSOC);
  $last_timestamp->execute(array());
  $date_from = $last_timestamp->fetch();
  $date_from = $date_from['date'];
  $date_from = strtotime($date_from.'-5 minutes');

  $date = date("Y-m-d H:i:s");
  $date2 = date("Y-m-d");

  $truncate = $dbh->prepare("TRUNCATE app_hired_transport_update");
  $truncate->execute(array());  

  

  $select = "r.route_code, v.vehicle_code, r.print_note,r.route_date,v.registration_number, r.cost_agreed,r.route_id,v.supplier_id,s.legal_no,s.vat_no,dr.driver_name, transaction_user ";
  $from = "route r 
          JOIN log ON (log.table_id = 1 AND r.route_id = log.row_id)     
          JOIN vehicle v ON v.vehicle_id=r.vehicle_id
          LEFT JOIN supplier s ON s.supplier_id=v.supplier_id
          JOIN driver dr ON dr.driver_id=r.driver1_id";

  $where = "to_char(log.edit_date, 'YYYY-MM-DD HH24:MI') >= '$date_from'  AND CASE WHEN v.supplier_id IS NOT NULL THEN v.supplier_id != 1 
  WHEN v.supplier_id IS NULL THEN v.supplier_id IS NULL
  END
  GROUP BY  r.route_code, v.vehicle_code, r.print_note,r.route_date,v.registration_number, r.cost_agreed,r.route_id,v.supplier_id,s.legal_no,s.vat_no,dr.driver_name, transaction_user";


  $insert_query = 'INSERT INTO app_hired_transport_update (route_id, driver_name, print_note, cost_agreed, registration_number,route_code,route_date, transaction_user) VALUES (?,?,?,?,?,?,?,?)';
  $execute = array('route_id','driver_name','print_note','cost_agreed','registration_number','route_code','route_date','transaction_user');

  $check_array = [];   

    
  $update_records = getTableWithSelectSimple($select,$from, $dbh, $where,$insert_query,$check_array,$execute,'route_id'); 

  if($update_records){

     $update = $dbh->prepare("SELECT t.route_id, driver_name,route_code,route_date, print_note, registration_number, cost_agreed, purchaseorderid, transaction_user
                              FROM app_hired_transport_update t 
                              JOIN vtiger_purchaseorder_hired_transport d ON d.route_id=t.route_id
                              GROUP BY route_id
                              ORDER BY id");


     $get_vendor = $dbh->prepare("SELECT v.vendorid,cf_1462 AS iban, count(purchaseorderid) AS purchases FROM vtiger_vendor v 
                                  JOIN vtiger_vendorcf vcf ON vcf.vendorid=v.vendorid
                                  INNER JOIN vtiger_crmentity ON crmid=v.vendorid
                                  LEFT JOIN vtiger_purchaseorder ON  vtiger_purchaseorder.vendorid=v.vendorid
                                  WHERE vendorname LIKE ? OR vendorname LIKE ? AND deleted = 0 
                                  ORDER BY purchases DESC LIMIT 1"); 
                                  
     $get_vendor->setFetchMode(PDO::FETCH_ASSOC);                             

     $update->setFetchMode(PDO::FETCH_ASSOC);
     $update->execute(array()); 

     $update_purchase = $dbh->prepare("UPDATE vtiger_purchaseorderdraft SET purchaseorderdraft_tks_vendor = ?, purchaseorderdraft_tks_amount = ?, purchaseorderdraft_tks_note = ?, purchaseorderdraft_tks_registration_number = ?, purchaseorderdraft_tks_route_code = ?,purchaseorderdraft_tks_routedate = ?, purchaseorderdraft_tks_driver_name = ?, transaction_user = ?  WHERE purchaseorderdraftid = ?");



     $update_entity = $dbh->prepare("UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?");
    
     $check_insert = $dbh->prepare("INSERT INTO vtiger_purchaseorder_hired_transport (purchaseorderid, route_id, import_date) VALUES (?,?,?)");

      foreach($update AS $row){          

        $driver_name = $row['driver_name'];
        $car_nr = $row['registration_number'];
       
        $get_vendor->execute(array("%$driver_name%","%$car_nr%"));
        $vendor_info = $get_vendor->fetch();     

        $update_purchase->execute(array($vendor_info['vendorid'],$row['cost_agreed'],$row['print_note'],$row['registration_number'],$row['route_code'],$row['route_date'],$row['driver_name'], $row['transaction_user'], $row['purchaseorderid']));                
      

        $update_entity->execute(array($date,$row['purchaseorderid']));

      }
  } 


} catch (PDOException $e) {
 echo "Error!";
 echo $e->getMessage();
}