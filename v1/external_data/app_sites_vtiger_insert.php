<?php

include 'ws.config.php';
global $config;
require 'utils.php';

try {

    require('mysql_connection.php');
        
    $dbh->beginTransaction();
                                                                    
    $sth2 = $dbh->prepare('INSERT INTO vtiger_routepoints (routepointsid, routepointsno, routepoints_tks_customer_name, routepoints_tks_type, routepoints_tks_location, routepoints_tks_start_time, routepoints_tks_end_time, routepoints_tks_point_seq_no, routepoints_tks_remarks, routepoints_tks_kg_load, routepoints_tks_m3_load, routepoints_tks_pll_load, routepoints_tks_kg_unload, routepoints_tks_m3_unload, routepoints_tks_pll_unload, routepoints_tks_price_agreed,routepoints_tks_address,routepoints_tks_status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');


    $get_all_route_point_id = $dbh->prepare("SELECT route_id FROM app_sites");
    $get_all_route_point_id->setFetchMode(PDO::FETCH_ASSOC);
    $get_all_route_point_id->execute(array());

    $route_point_id = [];
    foreach ($get_all_route_point_id as $value) {
        $route_point_id[$value['route_id']] = $value['route_id'];
    }

    $route_points = implode(',', $route_point_id);
    
    
    $getAllExistedId = $dbh->prepare("SELECT routepointsno 
                                        FROM vtiger_routepoints 
                                        INNER JOIN vtiger_crmentity ON crmid = routepointsid
                                        WHERE routepointsno IN ($route_points)  AND deleted = 0");

    $getAllExistedId->setFetchMode(PDO::FETCH_ASSOC);
    $getAllExistedId->execute(array());

    $existedIds = [];
    foreach ($getAllExistedId as $value) {
        $existedIds[$value['routepointsno']] = $value['routepointsno'];
    }

    
    $sth = $dbh->prepare("SELECT h.ticketid, t.*, u.id AS user_id
                            FROM app_sites t     
                            JOIN vtiger_troubletickets h ON h.id = t.route_id
                            LEFT JOIN vtiger_users u ON u.title = h.driver1_id  
                            LEFT JOIN vtiger_routepoints r on r.routepointsno=t.route_point_operation_id
                            INNER JOIN vtiger_crmentity l ON l.crmid = h.ticketid     
                            WHERE l.deleted = 0 AND routepointsid IS NULL");


    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array());

    $orders_count = [];
    foreach ($sth as $record) {
        if(empty($existedIds[$record['route_id']])){
            $orders_count[] = $record;
        }
    }

    $records = $orders_count;
    $orders_count = count($orders_count);


    if($orders_count > 0){    

      $sth6 = $dbh->prepare("SELECT crmid FROM vtiger_crmentity WHERE crmid = ?");    
      $last_entity_record = get_last_entity_id_and_update($dbh,$orders_count);  
    
      foreach ($records AS $row) {                  
           
                $sth6->execute(array($last_entity_record)); 
                $record_exist = $sth6->rowCount();
    
                if($record_exist) {
                $last_entity_record_old = $last_entity_record;
                $sth3->execute(array());
                $seq = $sth3->fetch();
                $last_entity_record = $seq['id']+1;
                $sth4 = $dbh->prepare("UPDATE vtiger_crmentity_seq SET id = ?");
                $sth4->execute(array($last_entity_record));     
                }
                    
                    if(empty($row['user_id']) || $row['user_id'] == NULL) {
                        $user_id = 26;
                    } else {
                        $user_id = $row['user_id']; 
                    }            
                    
                    if($row['type_id'] == 'L') {
                        $type_id = 'Pasikrovimas';
                    } else  {
                        $type_id = 'Išsikrovimas'; 
                    }              
                    
                    if(empty($row['is_visited']) || $row['is_visited'] == NULL) {
                        $status = 'Planuojama';
                    } else  {
                        $status = 'Išvyko'; 
                    }   
                    
                    $address = $row['location_address'] . ', ' . $row['location_city'];          
                        
                    $date = date("Y-m-d H:i:s");
                        
                            $last_entity_record = last_entity_record($dbh);
                            $insert_entity = insert_entity($dbh, 'Routepoints', $last_entity_record, $user_id, NULL, $row['location_name'], $date);                 
                            
                            $sth2->execute(array($last_entity_record, 
                                                $row['route_id'], 
                                                $row['customer_name'],
                                                $type_id,
                                                $row['location_name'],                                        
                                                $row['arrival_time'], 
                                                $row['departure_time'],                                       
                                                $row['point_seq_no'],                                       
                                                $row['remarks'],  
                                                $row['kg_load'], 
                                                $row['m3_load'], 
                                                $row['pll_load'], 
                                                $row['kg_unload'], 
                                                $row['m3_unload'], 
                                                $row['pll_unload'],
                                                $row['price_agreed'],                                         
                                                $address,
                                                $status                              
                            ));

                    

                $insert_entity_rel = $dbh->prepare('INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES (?, ?, ?, ?)');
                $insert_entity_rel->execute(array($row['ticketid'],'HelpDesk', $last_entity_record,'Routepoints')); 

                if($record_exist) {             
                    $last_entity_record = $last_entity_record_old;    
                }         
            
                    $last_entity_record++;
            
        }

    }

    $dbh->commit();
                
} catch (PDOException $e) {
   $dbh->rollBack(); 
   echo "Error!";
   echo $e->getMessage();
}

?>