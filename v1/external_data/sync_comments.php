<?php

function syncComments($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
    
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';
    $endpointUrl = $url;
    
    $date = date('Y-m-d H:i:s');
    $user_id = $params['user_id'];
    $task_id = $params['task_id'];
    $task_comment = $params['comment'];
    
    $logfile = 'sync_comments.log';
    WebserviceLog($params, $logfile);      
   
    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            WebserviceLog($output, $logfile);
            
        } else {   
            
            $sth = $dbh->prepare('SELECT accesskey, user_name FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            
            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);
            
            $dbh->beginTransaction();            
            
                          $sth1 = $dbh->query('SELECT * FROM vtiger_crmentity_seq');
                          while ($row = $sth1->fetch()) {
                          $crm_comment_id = $row['id'] + 1;
                          }

                          if(empty($task_id)) {
                                insert_entity($dbh, 'Faq', $crm_comment_id, $user_id, NULL, $date, $date);                

                                $sth = $dbh->prepare('INSERT INTO vtiger_faq (id, 
                                                                                        message, 
                                                                                        user,
                                                                                        all_users,
                                                                                        category,
                                                                                        faq_no
                                                                                        ) VALUES (?, ?, ?, ?, ?, ?)');
                                $sth->execute(array($crm_comment_id, 
                                                    $task_comment, 
                                                    $user_id,
                                                    0,
                                                    'Message',
                                                    $crm_comment_id
                                    )); 
                          
                          } else {
                                insert_entity($dbh, 'ModComments', $crm_comment_id, $user_id, NULL, $date, $date);                

                                $sth = $dbh->prepare('INSERT INTO vtiger_modcomments (modcommentsid, 
                                                                                        commentcontent, 
                                                                                        related_to 
                                                                                        ) VALUES (?, ?, ?)');
                                $sth->execute(array($crm_comment_id, 
                                                    $task_comment, 
                                                    $task_id                
                                    ));

                                $sth = $dbh->prepare('INSERT INTO vtiger_modcommentscf (modcommentsid) VALUES (?)');
                                $sth->execute(array($crm_comment_id));     
                          }
                          
            $dbh->commit();	
            
                    $output = array(
                        'status' => 200,
                        'response' => array('crm_comment_id' => $crm_comment_id)
                    );
                    
                }
        
    } catch (PDOException $e) {

        $dbh->rollBack();
        
        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_comments.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
