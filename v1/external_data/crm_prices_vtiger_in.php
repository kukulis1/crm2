<?php


try {

    require('mysql_connection.php');

    $admin_account = 1;
    $date = date("Y-m-d H:i:s");
        
    $sth = $dbh->prepare('DELETE FROM vtiger_pricebookproductrel');
    $sth->execute(array());
     

    $sth70 = $dbh->prepare("SELECT           
                                     p.product, 
                                     p.pricebook, 
                                     p.product, 
                                     p.customer,
                                     p.price,
                                     pro.productid,
                                     pa.pricebookid
                                FROM crm_prices_in p
                                JOIN vtiger_pricebook pa ON pa.bookname = p.pricebook
                                JOIN vtiger_products pro ON pro.productname = p.product
                                JOIN vtiger_crmentity e ON e.crmid = pro.productid
                                WHERE e.deleted = 0");    
    $sth70->execute(array());

    $new_temp2 = array();

    while ($row = $sth70->fetch()) {

        $temp2 = array();
              
            $temp2['productid'] = $row['productid'];
            $temp2['pricebookid'] = $row['pricebookid'];
            $temp2['customer'] = $row['customer'];
            $temp2['price'] = $row['price'];                     
            
        $new_temp2[] = $temp2;
    }
    
    $new_records2 = 0;

    foreach ($new_temp2 as $navision_record2) {                                       
            
            $sth80 = $dbh->prepare('INSERT INTO vtiger_pricebookproductrel (pricebookid,
                                                                productid,
                                                                listprice, 
                                                                import_date) VALUES (?, ?, ?, ?)');
            $sth80->execute(array($navision_record2['pricebookid'], 
                                $navision_record2['productid'],                             
                                $navision_record2['price'],          
                                $date,              
                ));
                      
  
            $sth60 = $dbh->prepare('UPDATE vtiger_account SET pricebook = ? WHERE account_no = ?');
            $sth60->execute(array($navision_record2['pricebookid'],$navision_record2['customer']));
            
            }                  

            
    } catch (PDOException $e) {
    
    //print($e->getMessage());
}

?>