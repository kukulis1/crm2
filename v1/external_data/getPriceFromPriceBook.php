<?php
  // NOTE Iterpiame countriesZones funkcija
  require_once $_SERVER['DOCUMENT_ROOT']."/price-algorithm/countries.php";   

function getPriceFromPriceBook($dbh,$post_from,$post_to,$accountid,$weight,$volume,$square,$pll,$distance,$country_from, $country_to,$which){

  // NOTE Gaunami miestai pagal pasto kodus
  if(!empty($post_from)){ 
    $post_code_from = $dbh->prepare("SELECT `zone_customer`,`zone_base`, `city`, `state`,`post_code` FROM `crm_post_codes` WHERE `post_code` = ".$post_from);

    if($country_from == 'LVA'){    
      $post_code_from = $dbh->prepare("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $post_from BETWEEN code_from AND code_to");
    }

    $post_code_from->execute();
    $from = $post_code_from->fetch();
  }

  if(!empty($post_to)){
    $post_code_to = $dbh->prepare("SELECT `zone_customer`,`zone_base`, `city`, `state`,`post_code` FROM `crm_post_codes` WHERE `post_code` = ".$post_to);

    if($country_to == 'LVA'){    
      $post_code_to = $dbh->prepare("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $post_to BETWEEN code_from AND code_to");
    }

    $post_code_to->execute();
    $to = $post_code_to->fetch();
  } 


  if(empty($from['zone_base']) AND empty($to['zone_base'])){
    return  array('price' => 0,  'combination' => 'Blogi pašto kodai', 'max_volume' => 0);
  }elseif(empty($from['zone_base'])){
      return  array('price' => 0,  'combination' => 'Blogas pakrovimo pašto kodas', 'max_volume' => 0);
  }elseif(empty($to['zone_base'])){
    return  array('price' => 0,  'combination' => 'Blogas iškrovimo pašto kodas', 'max_volume' => 0);
  }


  // NOTE Gaunamos kliento korteles zonos 
  $locations =  $dbh->prepare("SELECT * FROM `vtiger_accountscf`
                                                          LEFT JOIN `vtiger_account` on `vtiger_accountscf`.`accountid` = `vtiger_account`.`accountid`
                                                          LEFT JOIN crm_directions ON `crm_directions`.`directionid`=`vtiger_accountscf`.`accountid`
                                                          LEFT JOIN crm_address_post_code adr ON `adr`.`addressid`=`vtiger_accountscf`.`accountid`
                                                          WHERE `vtiger_accountscf`.`accountid` = ?");

  // NOTE gaunam visus klientui priskirtus kainorascius
  $pricebooks = $dbh->prepare("SELECT pricebook FROM vtiger_account WHERE accountid = ?");
  $pricebooks->execute([$accountid]);
  $pricebooks = $pricebooks->fetch();

  // NOTE Patikrinam ar klientas turi priskirta kainininka, jei ne priskiria bazini

  if(empty($pricebooks['pricebook']) && $which != 'BAZINIS'){
    return  array('price' => 0,  'combination' => 'Nenurodytas kliento kainininkas', 'max_volume' => 0);
  } 

  $pricebookid = $pricebooks['pricebook'];

  if($which == 'BAZINIS'){
    $pricebookid = 10965;
  }


  // NOTE GAUNAME KAINYNA
  $pricebook = $dbh->query("SELECT vtiger_products.productid, productname, consignee, min_weight_kg, max_weight_kg, min_volume_m3, max_volume_m3, min_square_m2, max_square_m2, pll_pcs ,vtiger_pricebookproductrel.listprice, cf_954
                              FROM `vtiger_pricebook`
                              LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                              LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                              LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                              INNER JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                              INNER JOIN `vtiger_crmentity` e ON e.`crmid`=`vtiger_pricebook`.`pricebookid`
                              WHERE `vtiger_pricebook`.`pricebookid` IN ($pricebookid) AND `vtiger_crmentity`.`deleted` = 0 AND e.deleted = 0",PDO::FETCH_ASSOC)->fetchAll(); 

    
    $locations->execute([$accountid]);
    $loc = $locations->fetch();
    $direct = directLocationAndDirectAdr($loc);
    return initCalculatePrice($dbh,$from,$to,$pricebook,$weight,$volume,$square,$pll,$distance,$country_from, $country_to,$which,$loc,$direct); 
}

function initCalculatePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$distance,$country_from, $country_to,$which,$loc,$direct){      
      $false_pll = false;   
      $countPriceArr = array();   
      
      //NOTE Vykdoma kainos paieska kliento kainyne
      if($which == 'CLIENT'){        
        // Patikrina ar svoris nera lygus 0 ir skaiciuoja kaina pagal svori
        if($weight > 0){
          $weightArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$false_pll, false, false, false, $distance, $country_from, $country_to,$loc,$direct);

          $countPriceArr = array_merge($countPriceArr, $weightArr);
          // Patikrinam ar rasta kaina uz kilogramus
          foreach ($weightArr as $value) {       
            if($value['count_weight']['count'] == 0){  
              return array('price' => 0,  'combination' => "Nerasta kaina pagal svorį");   
            }
          }
        }     
        
        // Patikrina ar kubai nera lygus 0 ir skaiciuoja kaina pagal kubus
        if($volume > 0){
          $volumeArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$false_pll, true, false, false, $distance, $country_from, $country_to,$loc,$direct);

          $countPriceArr = array_merge($countPriceArr, $volumeArr); 
          // Patikrinam ar rasta kaina uz kubus
          foreach ($volumeArr as $value) {              
            if($value['count_m3']['count'] == 0){  
              return array('price' => 0,  'combination' => "Nerasta kaina pagal kūbus");   
            }
          }
        }    


        // Patikrina ar kvadratai nera lygus 0 ir skaiciuoja kaina pagal kvadratus
        if($square > 0){
          $squareArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$false_pll, false, true, false, $distance, $country_from, $country_to,$loc,$direct);

          $countPriceArr = array_merge($countPriceArr, $squareArr); 
          // Patikrinam ar rasta kaina uz kvadratus
          foreach ($squareArr as $value) {       
            if($value['count_m2']['count'] == 0){  
              return array('price' => 0,  'combination' => "Nerasta kaina pagal kvadratus");   
            }
          }
        }  
        
        // // $pll kintamajam gražinama jo tikroji vertė, patikrinama ar paletes nera 0 ir skaiciuoja paleciu kaina      
        if($pll > 0){     
          $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll, false, false, false, $distance, $country_from, $country_to,$loc,$direct);   
        }    
        
        // Isrenka didziausia kaina
        $countPriceArr = array_reduce($countPriceArr,function($carry,$item){   
          return (($carry['price'] ?? 0) > $item['price']) ? $carry : $item;
        });        

      }else if($which == 'BAZINIS'){
       
        $countPriceArr = array();         
       

          // Bazinis 
        if($weight > 0){     			  
        	$countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$$false_pll, false, false, true,$distance, $country_from, $country_to,$loc,$direct);
        }
   	    if($volume > 0){
        	$countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume, $square,$$false_pll,  true, false, true,$distance, $country_from, $country_to,$loc,$direct);  
        }   

        if($square > 0){
       	 	$countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$$false_pll, false, true,true,$distance, $country_from, $country_to,$loc,$direct);  
    	  }       

        if($pll > 0){ 
	        $countPriceArr[] = takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll, false, false, true,$distance, $country_from, $country_to,$loc,$direct);  
	      }   
 
        $countPriceArr = array_reduce($countPriceArr,function($carry,$item){   
          return (($carry['price'] ?? 0) > $item['price']) ? $carry : $item;
        });
      }
  

    
  return $countPriceArr;
}


function takePrice($dbh,$from,$to,$pricebookid,$weight,$volume,$square,$pll,$byVolume, $bySquare, $bazinis,$distance, $country_from, $country_to,$loc,$direct){ 

  $pricebook_info = [];

  $count_weight = ['count' => 0, 'price' => []];
  $count_m3 = ['count' => 0, 'price' => []];
  $count_m2 = ['count' => 0, 'price' => []];
  $count_pll = ['count' => 0, 'price' => []];

  if(!$bazinis){ 
    if($pll > 0){   
      $check_pll_count = [];  
      $carry_pll_data = [];
      $carry_full_pll_data = [];
      foreach($pricebookid AS  $product){
        if($product['pll_pcs'] == $pll){
          $check_pll_count[$product['consignee']][] = $product['pll_pcs'];
          $carry_pll_data[$product['consignee']] = $product;
          $carry_full_pll_data[$product['consignee']][$product['productid']] = $product;
          $carry_full_pll_data[$product['consignee']][$product['productid']]['is_pll'] = 1; 
          $count_pll['count'] += 1;
          $count_pll['price'][] = round($product['listprice'],2);
        }
      }    

      $check_pll = [];   
      $check_pll_with_weight_and_m3 = 0;   
      foreach($pricebookid AS $product){

        if(count($check_pll_count[$product['consignee']]) > 0){
          $check_pll[] = $product['max_weight_kg'];

          if($product['pll_pcs'] == $pll AND ($product['min_weight_kg'] <= $weight AND $weight <= $product['max_weight_kg'])){
            $pricebook_info[$product['consignee']][$product['productid']] = $product;
            $count_pll['count'] += 1;
            $count_pll['price'][] = round($product['listprice'],2);
            $check_pll_with_weight_and_m3 += 1;
          }

          if($product['pll_pcs'] == $pll AND ($product['min_volume_m3'] <=  $volume AND $volume <= $product['max_volume_m3'])){
            $pricebook_info[$product['consignee']][$product['productid']] = $product;
            $count_pll['count'] += 1;
            $count_pll['price'][] = round($product['listprice'],2);
            $check_pll_with_weight_and_m3 += 1;
          }

        }else{
          $check_pll[] = $product['pll_pcs'];
          $pricebook_info[$product['consignee']][$product['productid']] = $carry_pll_data[$product['consignee']];
        }       
      }   

      if($check_pll_with_weight_and_m3 == 0){
        $pricebook_info = array_merge($pricebook_info, $carry_full_pll_data);
      }

      // Patikrinam ar suvesti i kainyna kilogramai su pll 
      if(max($check_pll) == 0){
        $count_pll['count'] = 1;
      }    
      
    }elseif($byVolume){   
      $check_volume = [];  
      foreach($pricebookid AS $product){   
        if($product['min_volume_m3'] <=  $volume AND $volume <= $product['max_volume_m3']){
          $pricebook_info[$product['consignee']][$product['productid']] = $product;
          $count_m3['count'] += 1;
          $count_m3['price'][] = round($product['listprice'],2);
        }
        $check_volume[] = $product['max_volume_m3'];
      }   

      // Patikrinam ar suvesti i kainyna kubai
      if(max($check_volume) == 0){
        $count_m3['count'] = 1;
      } 

    }elseif($bySquare){     
      $check_square = [];
      foreach($pricebookid AS $product){
        if($product['min_square_m2'] <=  $square AND $square <= $product['max_square_m2']){
          $pricebook_info[$product['consignee']][$product['productid']] = $product;
          $count_m2['count'] += 1;
          $count_m2['price'][] = round($product['listprice'],2);
        }

        $check_square[] = $product['max_square_m2'];
      }  

      // Patikrinam ar suvesti i kainyna kvadratai       
      if(max($check_square) == 0){
        $count_m2['count'] = 1;
      }    

    }else{        
      $check_weight = [];   
      foreach($pricebookid AS $product){
        if($product['min_weight_kg'] <=  $weight AND $weight <= $product['max_weight_kg']){
          $pricebook_info[$product['consignee']][$product['productid']] = $product;
          $count_weight['count'] += 1;
          $count_weight['price'][] = round($product['listprice'],2);
        }
        $check_weight[] = $product['max_weight_kg'];
      }  
      
      if(max($check_weight) == 0){
        $count_weight['count'] = 1;
      }  
    }      
  
  }else{
    if($pll > 0){     
      foreach($pricebookid AS $product){
        if($product['pll_pcs'] == $pll){
          $pricebook_info[$product['productid']] = $product;
        }
      }   
    
    }elseif($byVolume){     
      foreach($pricebookid AS $product){
        if($product['min_volume_m3'] <=  $volume AND $volume <= $product['max_volume_m3']){
          $pricebook_info[$product['productid']] = $product;
        }
      }   

    }elseif($bySquare){     
      foreach($pricebookid AS $product){
        if($product['min_square_m2'] <=  $square AND $square <= $product['max_square_m2']){
          $pricebook_info[$product['productid']] = $product;
        }
      }   

    }else{         
      foreach($pricebookid AS $product){
        if($product['min_weight_kg'] <=  $weight AND $weight <= $product['max_weight_kg']){
          $pricebook_info[$product['productid']] = $product;
        }
      }         
    }
  }

  $met = 'pdo';


  if(!$bazinis){  
      // NOTE Visi duomenys siunciami sudaryti kombinacijas
      $get_price = countriesZones($dbh, $direct['direct_location'],$direct['direct_location_adr'], $met, $country_from, $country_to, $from, $to,$loc,$distance, $pricebook_info, $from,$to);


      $pricebook_type = (($pricebookid == 10965 || $bazinis) ? 'BAZINISLT' : 'CLIENT');
      // foreach($get_prices as $get_price){  
        $result = [ 
            "price" => (float)$get_price["listprice"],   
            'pricebook' => $get_price['productname'],
            'which' => $pricebook_type,
            "min_weight" => $get_price['min_weight_kg'],
            "max_weight" => $get_price['max_weight_kg'],
            "min_volume" => $get_price['min_volume_m3'],
            "max_volume" => $get_price['max_volume_m3'],
            "min_square" => $get_price['min_square_m2'],
            "max_square" => $get_price['max_square_m2'],
            "pll" => $get_price['pll_pcs'],
            "stevedoring" => (float)$get_price['cf_954'],
            "combination" =>  $get_price["productname"],
            "consignee" =>  $get_price["consignee"],
            'count_weight' => $count_weight,
            'count_m3' => $count_m3,
            'count_m2' => $count_m2,
            'count_pll' => $count_pll,
          ];    
          // break;    
      // }   

  }else{   


      require_once $_SERVER['DOCUMENT_ROOT']."/price-algorithm/base_catalog.php";
    
      $location = ":".trim($from["zone_customer"])." ".trim($to["zone_customer"]).":"; 
      $get_price = basecatalog($location,$pricebook_info, $dbh,$met);
    
      $location2 = ":".trim($from["zone_base"])." ".trim($to["zone_base"]).":"; 
      $get_price_base = basecatalog($location2,$pricebook_info, $dbh,$met);
    
      $location3 = ":".trim($from["zone_base"])." ".trim($to["zone_customer"]).":"; 
      $get_price_base2 = basecatalog($location3,$pricebook_info, $dbh,$met);  
    
      $location4 = ":".trim($from["zone_customer"])." ".trim($to["zone_base"]).":"; 
      $get_price_base3 = basecatalog($location4,$pricebook_info, $dbh,$met);  
    
      $get_base = array_merge($get_price, $get_price_base, $get_price_base2,$get_price_base3);

      foreach($get_base as $get_price){   

        if(is_numeric($loc['cf_2708']) && $loc['cf_2708'] > 0){
          $price = (($get_price["listprice"] / 100) * $loc['cf_2708'])  + $get_price["listprice"];
        }else{
          $price = $get_price["listprice"];
        }

          $result = array( 
            "price" => (float)$price,       
            'pricebook' => "BAZINISLT ". $get_price["productname"],
            'combination' => "BAZINISLT ". $get_price["productname"],
            'which' => "BAZINIS",
            "min_weight" => $get_price['min_weight_kg'],
            "max_weight" => $get_price['max_weight_kg'],
            "min_volume" => $get_price['min_volume_m3'],
            "max_volume" => $get_price['max_volume_m3'],
            "min_square" => $get_price['min_square_m2'],
            "max_square" => $get_price['max_square_m2'],
            "pll" => $get_price['pll_pcs'],
            "stevedoring" => (float)$get_price['cf_954'],
            "consignee" =>  $get_price["consignee"]
          );                     
          break;
      }  
    
  }


  if($result["price"] > 0){
    return $result;
  }else{  
    return array('price' => 0,  'combination' => "Nėra kombinacijos ".$from['zone_base']." - ".$to['zone_base']." arba ".$from['zone_customer']." - ".$to['zone_customer']." arba ".$from['state']." - ".$to['state']."", 'max_volume' => 0, 'count_weight' => $count_weight, 'count_m3' => $count_m3, 'count_m2' => $count_m2, 'count_pll' => $count_pll);      
  } 
}

function directLocationAndDirectAdr($loc){
  $direct_location = Array();
  $direct_location_adr = Array();
  $columns = Array('1218','1222','1226','1230','1234','1238','1242','1246','1250','1254','1400','1404','1408','1412','1416','1420','1424','1428','1432','1436');

  $b = 0;

  // Pasidaromas tiesioginiu marsrutu pagal adresa array
  foreach($columns AS $col){
    if(isset($loc["post_cf_".$col])){
      $slashExplode = explode("/",$loc["post_cf_".$col]);        
      for($f =0; count($slashExplode) > $f; $f++){
       $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
      }      
      for($e =0; count($slashExplode) > $e; $e++){          
          $post_cf_[$col][$e] = multiexplode2(array(";","-"), $equalsExplode[$col."_".$e][1]);     
          if(!empty($equalsExplode[$col."_".$e][0])){       
           $direct_location[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]); 
          }       
      }
    }
    $b++;
  } 

// Pasidaromas tiesioginiu marsrutu pagal pasto koda array
  foreach($columns AS $col){
    if(isset($loc["code_cf_".$col])){
      $slashExplode = explode("/",$loc["code_cf_".$col]);        
      for($f =0; count($slashExplode) > $f; $f++){
       $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
      }      
      for($e =0; count($slashExplode) > $e; $e++){          
          $post_cf_[$col][$e] = multiexplode2(array(";","-"), $equalsExplode[$col."_".$e][1]);          
          if(!empty($equalsExplode[$col."_".$e][0])){         
            $direct_location_adr[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]);      
          }  
      }
    }
  } 

  return ['direct_location' => $direct_location, 'direct_location_adr' => $direct_location_adr];
}

function multiexplode2 ($delimiters,$data) {
  $MakeReady = str_replace($delimiters, $delimiters[0], $data);
  $Return    = explode($delimiters[0], $MakeReady);
  return  $Return;
}