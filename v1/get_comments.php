<?php

function getComments($params) {

    global $config;
    require 'appc_utils.php';

    $logfile = 'get_comments.log';

    $result_data = array('comments' => array());
    
        $user_id = $params['user_id'];
    
    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {
            
            $logfile = 'login.log';
            webservicelog($output, $logfile);
        } else {
            
            $sql_params = array();
            $timestamp = $params['timestamp'];
  
            $sql = "SELECT NULL AS related_to, c.createdtime, m.message AS commentcontent, CONCAT(u.first_name, ' ', u.last_name) AS author, m.id AS modcommentsid, c.smownerid, c.smcreatorid
                           FROM vtiger_faq m
                           JOIN vtiger_crmentity c ON c.crmid = m.id
                           JOIN vtiger_users u ON u.id = c.smownerid        
                           WHERE c.deleted = 0 AND (m.user = ? OR m.all_users = 1) AND m.category = 'Message'";            
            
            if(!empty($timestamp)) {
            $sql.= " AND c.createdtime >= '$timestamp'";
            } 
            
            $sql.= " ORDER BY c.createdtime DESC";
            
            array_push($sql_params, $user_id);
            $sth = $dbh->prepare($sql);
            $sth->execute($sql_params);

            while ($row = $sth->fetch()) {

                //$task_id = $row['related_to'];
                $task_comments['crm_comment_id'] = intval($row['modcommentsid']);
                
                if($row['related_to']!= NULL && !empty($row['related_to']))                   
                $task_comments['task_id'] = intval($row['related_to']); 
                
                $task_comments['author'] = $row['author']; 
                
                if($row['commentcontent']!= NULL && !empty($row['commentcontent']))
                $task_comments['comment'] = $row['commentcontent']; 
                
                $task_comments['timestamp'] = $row['createdtime']; 
                $task_comments['dispatcher'] = 0; 
                
                if($user_id != $row['smcreatorid']) {
                    $task_comments['dispatcher'] = 1;                     
                }
                
                array_push($result_data['comments'], $task_comments); 
            }                 
            
                $output = array(
                    'status' => 200,
                    'response' => $result_data
                );
            
                $logfile = 'get_comments.log';
                webservicelog($output, $logfile);
        }
    } catch (PDOException $e) {

        $dbh->rollBack();

        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );
        
        $logfile = 'get_commentslog';
        webservicelog($output, $logfile);
    }
    return $output;
}
