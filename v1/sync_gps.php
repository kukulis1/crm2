<?php

function syncGPS($params) {

    global $config;
    require $config['paths']['crm_path'] . DIRECTORY_SEPARATOR . 'config.inc.php';
    require 'appc_utils.php';
    require 'vtwsclib/Vtiger/WSClient.php';
    require 'metrika.php';
    
    $url = $site_URL . DIRECTORY_SEPARATOR . 'webservice.php';
    $endpointUrl = $url;
    
    $logfile = 'sync_gps.log';
    //WebserviceLog($params, $logfile);      
   
    try {

        require('db_connection.php');

        $output = account_authorization($dbh, $params);

        if (!empty($output)) {

            $logfile = 'login.log';
            WebserviceLog($output, $logfile);
            
        } else {   
            
            $sth = $dbh->prepare('SELECT accesskey, user_name, title FROM vtiger_users WHERE id = ?');
            $sth->execute(array($params['user_id']));
            $row = $sth->fetch();
                
            $userName = $row['user_name'];
            $userKey = $row['accesskey'];
            $userCode = $row['title'];
            
            $httpc = new Vtiger_WSClient($endpointUrl);
            
            $result = $httpc->doLogin($userName, $userKey);
            
            $gps_list = $params['gps_items']; 
            
            $dbh->beginTransaction();            
            
            if($gps_list > 0) {
                    
                        foreach ($gps_list as $gps_key => $gps_items) {
                            
                          $user_id = $params['user_id'];  
                          $user_code = $userCode;                           
                          $datetime = $gps_items['datetime']; 
                          $latitude = $gps_items['latitude']; 
                          $longitude = $gps_items['longitude'];                            
                          $address = $gps_items['address'];                                                  
                          $site_id = $gps_items['site_id'];                        
                          $route_id = $gps_items['route_id'];                                                  
                          $vehicle_id = $gps_items['auto_id'];   
                          
                          $sth = $dbh->prepare('INSERT INTO app_gps (datetime, 
                                                                                  latitude, 
                                                                                  longitude,
                                                                                  address,
                                                                                  site_id, 
                                                                                  user_id,
                                                                                  user_code,
                                                                                  route_id,
                                                                                  vehicle_id
                                                                                  ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
                          $sth->execute(array($datetime, 
                                              $latitude, 
                                              $longitude,
                                              $address,
                                              $site_id,                              
                                              $user_id, 
                                              $user_code,
                                              $route_id,
                                              $vehicle_id
                              ));
                          

                          Save_GPS_Metrika($dbh, $route_id, $latitude, $longitude, $datetime, $user_id);
                       
                          
                        }  
            }
            
            $dbh->commit();	
            
                    $output = array(
                        'status' => 200,
                        'response' => array()
                    );
                    
                }
        
    } catch (PDOException $e) {

        $dbh->rollBack();
        
        $output = array(
            'status' => 500,
            'response' => array(
                'type' => 'DB_ERROR',
                'message' => $e->getMessage()
            )
        );

        $logfile = 'sync_gps.log';
        webservicelog($output, $logfile);
    }

    return $output;
}

?>
