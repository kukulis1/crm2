<?php

require_once('include/database/PearDatabase.php');
require_once('include/utils/utils.php');

global $adb, $log;

$log->debug("Entering into Companies...");

if($_REQUEST["type"] == 'get_load_companies_list') {

$account_id = $_REQUEST["account_id"];
$load_company = $_REQUEST["company"];

$final = array();

$sql = "SELECT DISTINCT load_company, bill_code,bill_city,bill_street, load_contact AS load_contact,bill_country
                FROM vtiger_salesorder
                LEFT JOIN vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid                     
                WHERE vtiger_salesorder.accountid = ? AND load_company LIKE '%$load_company%'
                GROUP BY load_company
                ORDER BY vtiger_salesorder.salesorderid DESC";

$result = $adb->pquery($sql, array($account_id));       

            while($res = $adb->fetch_array($result))
            {

                            $temp['label'] = htmlspecialchars_decode(html_entity_decode($res["load_company"],  ENT_NOQUOTES, 'UTF-8'));
                            $temp['value'] = $res["load_company_code"];                            
                            $final[] = $temp;
            }        
 
    /*$arr = array('UAB "Testas"' => 1, 'UAB "Tekstūra"' => 2, 'UAB "Mano būstas"' => 3);
    $final = array();
    foreach($arr as $key=>$value){
      $final[]=array("label"=> $key,"value" => $value);
      
    }*/
} elseif($_REQUEST["type"] == 'get_load_company_info') {
    
$load_company_code = $_REQUEST["company_code"];     
    
$sql = "SELECT DISTINCT c.*
               FROM crm_companies c
               WHERE c.load_company_code = ?";

$result = $adb->pquery($sql, array($load_company_code));       

            while($res = $adb->fetch_array($result))
            {

                            $temp['contact'] = htmlspecialchars_decode(html_entity_decode($res["load_contact"],  ENT_NOQUOTES, 'UTF-8'));  
                            $temp['street'] = htmlspecialchars_decode(html_entity_decode($res["load_street"],  ENT_NOQUOTES, 'UTF-8'));
                            $temp['city'] = htmlspecialchars_decode(html_entity_decode($res["load_city"],  ENT_NOQUOTES, 'UTF-8'));  
                            $temp['post_code'] = $res['load_post_code']; 
                            $temp['country'] = $res['load_country'];                           
                            $final[] = $temp;
            }       
    
    $final = array(
            "street" => "Šilo g. 35",
            "city" => "Kaunas",
            "post_code" => "46463",
            "country" => "LTU",
    );
} elseif($_REQUEST["type"] == 'get_unload_companies_list') {
      
$account_id = $_REQUEST["account_id"];
$unload_company = $_REQUEST["company"];

$final = array();

$sql = "SELECT c.*
               FROM crm_companies c
	       JOIN vtiger_account a ON a.customer_id = c.customer_id     
	       JOIN vtiger_crmentity e ON e.crmid = a.accountid
               WHERE e.deleted = 0 AND c.unload_company LIKE '%$unload_company%' AND a.accountid = ?";

$result = $adb->pquery($sql, array($account_id));       

            while($res = $adb->fetch_array($result))
            {

                            $temp['label'] = htmlspecialchars_decode(html_entity_decode($res["unload_company"],  ENT_NOQUOTES, 'UTF-8'));
                            $temp['value'] = $res["unload_company_code"];                            
                            $final[] = $temp;
            }     
    
    /*$arr = array('UAB "Taketa"' => 1, 'UAB "Toko"' => 2, 'UAB "AAA"' => 3);
    $final = array();
    foreach($arr as $key=>$value){
      $final[]=array("label"=> $key,"value" => $value);
    }*/
    
} elseif($_REQUEST["type"] == 'get_unload_company_info') {

$unload_company_code = $_REQUEST["company_code"];    

$sql = "SELECT DISTINCT c.*
               FROM crm_companies c
               WHERE c.unload_company_code = ?";

$result = $adb->pquery($sql, array($unload_company_code));       

            while($res = $adb->fetch_array($result))
            {

                            $temp['contact'] = htmlspecialchars_decode(html_entity_decode($res["unload_contact"],  ENT_NOQUOTES, 'UTF-8'));                  
                            $temp['street'] = htmlspecialchars_decode(html_entity_decode($res["unload_street"],  ENT_NOQUOTES, 'UTF-8'));
                            $temp['city'] = htmlspecialchars_decode(html_entity_decode($res["unload_city"],  ENT_NOQUOTES, 'UTF-8'));  
                            $temp['post_code'] = $res['unload_post_code']; 
                            $temp['country'] = $res['unload_country'];                           
                            $final[] = $temp;
            }       
        
    
    /*$final = array(
            "street" => "Šilainių g. 25",
            "city" => "Kaunas",
            "post_code" => "53363",
            "country" => "LTU",
    );*/
}

echo json_encode($final);

$log->debug("Exiting from Companies...");