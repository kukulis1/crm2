<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

    $invoiceid = $_POST['invoiceid'];  
    $costcenter = $_POST['costcenter'];
    $productid = $_POST['productid'];


    $query = "SELECT REPLACE(ROUND(listprice*quantity,2),'.',',') AS listprice, 
                      REPLACE(ROUND(CASE WHEN vtiger_taxregions.value IS NULL 
                                      THEN (listprice*quantity/100 * 21)  + listprice*quantity
                                      ELSE (listprice*quantity/100 * vtiger_taxregions.value) + listprice*quantity
                                    END,2),'.',',') AS listprice_vat,                                     
                      CASE WHEN vtiger_taxregions.value IS NULL
                          THEN (listprice*quantity/100 * 21)  + listprice*quantity
                          ELSE (listprice*quantity/100 * vtiger_taxregions.value) + listprice*quantity
                        END AS withVat2,vendorname, inv.purchaseorder_no, inv.purchaseorderid, cf_1345 AS invoicedate, costcenter_tks_cost,  cargo_wgt AS service_name
                    FROM vtiger_inventoryproductrel i
                    LEFT JOIN vtiger_purchaseorder inv ON inv.purchaseorderid=i.id
                    LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=inv.purchaseorderid
                    LEFT JOIN vtiger_vendor a ON a.vendorid=inv.vendorid
                    LEFT JOIN vtiger_crmentity e ON e.crmid=i.id           
                    LEFT JOIN vtiger_costcenter c ON c.costcenterid=i.cargo_width 
                    LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
                    LEFT JOIN vtiger_taxregions ON IF(tax2 > 0, vtiger_taxregions.regionid=tax2 ,vtiger_taxregions.regionid=region_id)               
                    WHERE i.id IN ($invoiceid) AND e.deleted = 0 AND e.setype = 'PurchaseOrder' ";

                    if($productid > 0){
                      $query .= " AND its.itemserviceid = $productid  ";
                    }                 
                    if($costcenter){
                      $query .= " AND costcenterid IN ($costcenter) ";
                    }else{
                      $query .= " AND costcenter_tks_cost IS NULL ";
                    }                   
                    
                    $query .= " ORDER BY i.id,i.sequence_no ";

  $result = $conn->query($query);    

  $report = array();

  while($row = mysqli_fetch_assoc($result))
  {
    $report[] = $row;
  }

  echo json_encode($report);


}else{
  http_response_code(404);
}
