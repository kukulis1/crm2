<?php

require('../../v1/external_data/mysql_connection.php');


try {
  $dbh->beginTransaction();

  $check_order_dublikates = $dbh->prepare("SELECT `id`, `external_load_id`,productid, (COUNT(id) -1) AS `limitas`                                                
  FROM vtiger_inventoryproductrel
  JOIN vtiger_salesorder ON salesorderid=id 
  INNER JOIN vtiger_crmentity ON crmid=id
  WHERE setype = 'SalesOrder' AND productid = 14244 AND (vtiger_inventoryproductrel.external_order_id != '' or external_load_id != '')
  GROUP BY id,external_load_id
  HAVING COUNT(id) > 1 AND (COUNT(vtiger_inventoryproductrel.external_order_id) > 1 OR COUNT(external_load_id) > 1)
  ORDER BY id DESC");

  $check_order_dublikates->setFetchMode(PDO::FETCH_ASSOC);
  $check_order_dublikates->execute(array());

  $delete_row = $dbh->prepare("DELETE FROM vtiger_inventoryproductrel 
                               WHERE id = :id 
                               AND external_load_id = :external_load_id 
                               AND productid = :productid 
                               LIMIT :limitas");

  foreach ($check_order_dublikates AS $row) {
   $delete_row->bindValue(':id', (int) trim($row['id']), PDO::PARAM_INT);
   $delete_row->bindValue(':external_load_id', (int) trim($row['external_load_id']), PDO::PARAM_INT);
   $delete_row->bindValue(':productid', (int) trim($row['productid']), PDO::PARAM_INT);
   $delete_row->bindValue(':limitas', (int) trim($row['limitas']), PDO::PARAM_INT);
   $delete_row->execute();
  }

  $dbh->commit();

  header("Location:/reports/OrdersReport.php");

}catch(PDOException $e) {
  $dbh->rollBack();    
  echo "Error!";
  echo $e->getMessage();
}

