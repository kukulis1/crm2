<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $salesorderid = $_POST['salesorders'];  

  $query = "SELECT vtiger_salesorder.salesorderid, vtiger_salesorder.shipment_code,ROUND(vtiger_salesorder.total,2) AS total,accountname,createdtime 
            FROM vtiger_salesorder 
            LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_salesorder.accountid
            INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid
            WHERE vtiger_salesorder.salesorderid IN ($salesorderid)
            GROUP BY vtiger_salesorder.salesorderid
            ORDER BY vtiger_salesorder.salesorderid";

  $result = $conn->query($query);    

  $report = [];

  while($row = mysqli_fetch_assoc($result))
  {
    $report[] = $row;
  }

  echo json_encode($report);


}else{
  http_response_code(404);
}
