<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $type = $_POST['type'];
  $from = $_POST['from'];
  $to = $_POST['to'];


  if($type == 'main'){
  
    $query = "SELECT COUNT(vtiger_salesorder.salesorderid) as count, ROUND(SUM(total),2) as total, DATE_FORMAT(createdtime, '%Y-%m-%d') as order_date 
                      FROM vtiger_salesorder                                      
                      LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid                                               
                      LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid 
                      LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid                 
                      WHERE vtiger_crmentity.deleted = 0 AND setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456 = 1 AND DATE_FORMAT(createdtime, '%Y-%m-%d') BETWEEN '$from' AND '$to' AND vtiger_invoice_salesorders_list.salesorderid IS NULL
                      GROUP BY DATE_FORMAT(createdtime, '%Y-%m-%d') ";
  }elseif($type == 'base'){
    $query = "SELECT COUNT(vtiger_salesorder.salesorderid) as count, ROUND(SUM(total),2) as total, DATE_FORMAT(createdtime, '%Y-%m-%d') as order_date                    
                      FROM vtiger_salesorder                                      
                      LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid                                                
                      LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid   
                      LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid               
                      WHERE vtiger_crmentity.deleted = 0 AND AND setype = 'SalesOrder' vtiger_salesordercf.cf_1456 = 1 AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$from' AND '$to' and cf_1297 LIKE 'BAZINIS%' AND vtiger_invoice_salesorders_list.salesorderid IS NULL
                      GROUP BY DATE_FORMAT(createdtime, '%Y-%m-%d')";

  }elseif($type == 'zero'){
    $query = "SELECT COUNT(vtiger_salesorder.salesorderid) as count, ROUND(SUM(total),2) as total, DATE_FORMAT(createdtime, '%Y-%m-%d') as order_date
                      FROM vtiger_salesorder                                      
                      LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid                                
                      LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid   
                      LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid               
                      WHERE vtiger_crmentity.deleted = 0 AND AND setype = 'SalesOrder' vtiger_salesordercf.cf_1456 = 1 AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$from' AND '$to' AND vtiger_salesorder.total = 0 AND vtiger_invoice_salesorders_list.salesorderid IS NULL
                      GROUP BY DATE_FORMAT(createdtime, '%Y-%m-%d')";
  }


  $result = $conn->query($query);

  $report = array();

  while($row = mysqli_fetch_assoc($result))
  {
    $report[] = $row;
  }

  echo json_encode($report);


}else{
  http_response_code(404);
}