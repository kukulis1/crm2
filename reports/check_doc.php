<?php
  error_reporting(E_PARSE & ~ E_NOTICE & ~ E_STRICT);
  require('../v1/external_data/mysql_connection.php');
  require '../v1/external_data/utils.php';

  $dbh->beginTransaction();

  $check_doc_receive_price = $dbh->prepare("SELECT salesorderid
  FROM vtiger_inventoryproductrel 
  JOIN vtiger_salesorder on salesorderid=id
  INNER JOIN vtiger_crmentity on crmid=id 
  WHERE service IN (12,32) AND productid = 36641 AND setype = 'SalesOrder' AND deleted = 0 AND margin > IF(service = 12, 0.5,2)");
  $check_doc_receive_price->setFetchMode(PDO::FETCH_ASSOC);
  $check_doc_receive_price->execute(array());
  $check_doc_receive_price_count = $check_doc_receive_price->rowCount();  

  $docs = [];
  if($check_doc_receive_price_count){
    foreach ($check_doc_receive_price as $value) {
      $docs[] = $value['salesorderid'];
    }

    sendReportMail(implode(',',$docs),basename(__FILE__));
  }
  

  $dbh->commit();