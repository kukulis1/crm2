<?php
  error_reporting(E_PARSE & ~ E_NOTICE & ~ E_STRICT);
  require('../v1/external_data/mysql_connection.php');
  require '../v1/external_data/utils.php';

  $dbh->beginTransaction();

  $date_from = date('Y-m-d',strtotime("-8 days"));
  $date_to =  date('Y-m-d');

  $check_dimensions = $dbh->prepare("SELECT l.*,s.external_order_id
  FROM vtiger_salesorder_dimensions_log l 
  JOIN vtiger_salesorder s ON s.salesorderid=l.salesorderid
  LEFT JOIN vtiger_invoice_salesorders_list li ON li.salesorderid=l.salesorderid
  INNER JOIN vtiger_crmentity e ON e.crmid=l.salesorderid
  WHERE (DATE_FORMAT(e.modifiedtime,'%Y-%m-%d') BETWEEN :date_from AND :date_to) AND li.invoiceid IS NULL AND deleted = 0
  ORDER BY id");

  $check_dimensions->setFetchMode(PDO::FETCH_ASSOC);
  $check_dimensions->execute([':date_from' => $date_from,':date_to' => $date_to]);

  $crm_loads = [];

  foreach($check_dimensions as $value) {
    $crm_loads[$value['external_order_id']] = $value;
  }

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from,'date_to' => $date_to));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_TIMEOUT, 300);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
  $result = curl_exec($ch);
  curl_close($ch);   
  $data = json_decode($result,true); 

  $compared_loads = [];
  $diff = [];

  foreach ($data['order'] as $shipment) {      
    $ordered_weight = '';
    $ordered_m2 = '';
    $ordered_m3 = '';
    $revised_weight = '';
    $revised_m2 = '';
    $revised_m3 = '';

      if(isset($shipment['load'])){
        foreach($shipment['load'] AS $load){ 
          $ordered_weight += $load['ordered_weight'];    
          $ordered_m2 += ($load['ordered_length'] * $load['ordered_width']) * $load['ordered_quantity'];
          $ordered_m3 += ($load['ordered_length'] * $load['ordered_width'] * $load['ordered_height']) * $load['ordered_quantity'];

          $revised_weight += $load['revised_weight'];    
          $revised_m2 += ($load['revised_length'] * $load['revised_width']) * $load['revised_quantity'];        
          $revised_m3 += $load['m3'];          
        }  


        if(!empty($crm_loads[$shipment['id']])){
          if($crm_loads[$shipment['id']]['kg'] != $ordered_weight){
            $compared_loads[] = $shipment['id'];
          }else if((int)(round($crm_loads[$shipment['id']]['m2'],2) * 100) != (int)(round($ordered_m2,2) *100) ){      
            $compared_loads[] = $shipment['id'];     
          }else if((int)(round($crm_loads[$shipment['id']]['m3'],2) * 100) != (int)(round($ordered_m3,2) *100) ){      
            $compared_loads[] = $shipment['id']; 
          }else if($crm_loads[$shipment['id']]['kg_revised'] != $revised_weight){
            $compared_loads[] = $shipment['id'];
          }else if((int)(round($crm_loads[$shipment['id']]['m2_revised'],2) * 100) != (int)(round($revised_m2,2) *100) ){ 
            $compared_loads[] = $shipment['id']; 
          }else if((int)(round($crm_loads[$shipment['id']]['m3_revised'],2) * 100) != (int)(round($revised_m3,2) *100) ){ 
            $compared_loads[] = $shipment['id'];   
          }
        }  
      }

  }


  if(count($compared_loads) > 0){
    $shipments = implode(',',$compared_loads);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/shipment_fake_update.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'shipment_ids' => $shipments));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch); 

    $data = json_decode($result,true); 
    sendReportMail($shipments,basename(__FILE__));
  }
  

  $dbh->commit();