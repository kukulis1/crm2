<?php
  error_reporting(E_PARSE & ~ E_NOTICE & ~ E_STRICT);
  require('../v1/external_data/mysql_connection.php');
  $logged = false;

  if($_POST['user_name']){   
    $user_hash = md5($_POST['password']); 
    $check = $dbh->prepare("SELECT * FROM vtiger_users WHERE `user_name` = :user_name AND `user_hash` = :user_hash");
    $check->execute([':user_name' => $_POST['user_name'],':user_hash' => $user_hash]);
    
    if($check->rowCount()){        
      setcookie("report_session", md5(md5($user_hash)), time() + 28800, '/');     
      header("Location: OrdersReport.php");
    }  
  }else if($_GET['logout']){
    setcookie('report_session', null, -1, '/'); 
    header("Location: OrdersReport.php");
  }else if($_COOKIE['report_session']){
    $dbh->beginTransaction();
    $users_hash = $dbh->prepare("SELECT user_hash FROM vtiger_users WHERE status = :status");
    $users_hash->setFetchMode(PDO::FETCH_ASSOC);
    $users_hash->execute([':status' => 'Active']);
    $all_hash = [];

    foreach ($users_hash as $item) {
      $all_hashs[] = md5(md5($item['user_hash']));
    }

    if(in_array($_COOKIE['report_session'],$all_hashs)){
      $logged = true;
      $date_from = date('Y-m-d',strtotime("-8 days"));
      $date_to =  date('Y-m-d',strtotime("-1 days"));

      $check_dimensions = $dbh->prepare("SELECT l.*,s.external_order_id
      FROM vtiger_salesorder_dimensions_log l 
      JOIN vtiger_salesorder s ON s.salesorderid=l.salesorderid
      LEFT JOIN vtiger_invoice_salesorders_list li ON li.salesorderid=l.salesorderid
      INNER JOIN vtiger_crmentity e ON e.crmid=l.salesorderid
      WHERE (DATE_FORMAT(e.modifiedtime,'%Y-%m-%d') BETWEEN :date_from AND :date_to) AND li.invoiceid IS NULL AND e.source != 'CRM' AND deleted = 0
      ORDER BY id");

      $check_dimensions->setFetchMode(PDO::FETCH_ASSOC);
      $check_dimensions->execute([':date_from' => $date_from,':date_to' => $date_to]);

      $crm_loads = [];

      foreach($check_dimensions as $value) {
        $crm_loads[$value['external_order_id']] = $value;
      }

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date_from,'date_to' => $date_to));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 300);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
      $result = curl_exec($ch);
      curl_close($ch);   
      $data = json_decode($result,true); 

      $compared_loads = [];
      $diff = [];

      foreach ($data['order'] as $shipment) {      
        $ordered_weight = '';
        $ordered_m2 = '';
        $ordered_m3 = '';
        $revised_weight = '';
        $revised_m2 = '';
        $revised_m3 = '';

          if(isset($shipment['load'])){
            foreach($shipment['load'] AS $load){ 
              $ordered_weight += $load['ordered_weight'];    
              $ordered_m2 += ($load['ordered_length'] * $load['ordered_width']) * $load['ordered_quantity'];
              $ordered_m3 += ($load['ordered_length'] * $load['ordered_width'] * $load['ordered_height']) * $load['ordered_quantity'];

              $revised_weight += $load['revised_weight'];    
              $revised_m2 += ($load['revised_length'] * $load['revised_width']) * $load['revised_quantity'];        
              $revised_m3 += $load['m3'];          
            }  


            if(!empty($crm_loads[$shipment['id']])){
              if($crm_loads[$shipment['id']]['kg'] != $ordered_weight){
                $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                $diff[$shipment['shipment_code']] = 'ordered_weight '.$crm_loads[$shipment['id']]['kg']." / ".$ordered_weight;
              
              }else if((int)(round($crm_loads[$shipment['id']]['m2'],2) * 100) != (int)(round($ordered_m2,2) *100) ){      
                $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                $diff[$shipment['shipment_code']] = 'ordered_m2 '.$crm_loads[$shipment['id']]['m2']." / ".$ordered_m2;
            
              }else if((int)(round($crm_loads[$shipment['id']]['m3'],2) * 100) != (int)(round($ordered_m3,2) *100) ){      
                $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                $diff[$shipment['shipment_code']] = 'ordered_m3 '.$crm_loads[$shipment['id']]['m3']." / ".$ordered_m3;
              
              }else if($crm_loads[$shipment['id']]['kg_revised'] != $revised_weight){
                $compared_loads[$shipment['id']] = $shipment['shipment_code'];
                $diff[$shipment['shipment_code']] = 'revised_weight '.$crm_loads[$shipment['id']]['kg_revised']." / ".$revised_weight;
            
              }else if((int)(round($crm_loads[$shipment['id']]['m2_revised'],2) * 100) != (int)(round($revised_m2,2) *100) ){ 
                $compared_loads[$shipment['id']] = $shipment['shipment_code'];  
                $diff[$shipment['shipment_code']] = 'revised_m2 '.$crm_loads[$shipment['id']]['m2_revised']." / ".$revised_m2;       
              
              }else if((int)(round($crm_loads[$shipment['id']]['m3_revised'],2) * 100) != (int)(round($revised_m3,2) *100) ){ 
                $compared_loads[$shipment['id']] = $shipment['shipment_code']; 
                $diff[$shipment['shipment_code']] = 'revised_m3 '.$crm_loads[$shipment['id']]['m3_revised']." / ".$revised_m3;       
              }
            }  
          }
      }      

      $check_order_dublikates = $dbh->prepare("SELECT shipment_code                                           
      FROM vtiger_inventoryproductrel
      JOIN vtiger_salesorder ON salesorderid=id 
      INNER JOIN vtiger_crmentity ON crmid=id
      WHERE setype = 'SalesOrder' AND productid = 14244 AND (vtiger_inventoryproductrel.external_order_id != '' or external_load_id != '')
      GROUP BY id,external_load_id
      HAVING COUNT(id) > 1 AND (COUNT(vtiger_inventoryproductrel.external_order_id) > 1 OR COUNT(external_load_id) > 1)
      ORDER BY id DESC");

      $check_order_dublikates->setFetchMode(PDO::FETCH_ASSOC);
      $check_order_dublikates->execute(array());
      $cargo_dublikates = $check_order_dublikates->rowCount();


      $invoice_tag_on_salesorder = $dbh->prepare("SELECT (SELECT shipment_code FROM vtiger_salesorder s WHERE s.salesorderid=li.salesorderid LIMIT 1) AS shipment_code
      FROM vtiger_invoice_salesorders_list li
      GROUP BY li.salesorderid
      HAVING COUNT(li.salesorderid) > 1
      ORDER BY li.salesorderid DESC");

      $invoice_tag_on_salesorder->setFetchMode(PDO::FETCH_ASSOC);
      $invoice_tag_on_salesorder->execute(array());
      $invoice_tag_dublikates = $invoice_tag_on_salesorder->rowCount();

      $salesorders_with_out_loads = $dbh->prepare("SELECT s.shipment_code
      FROM vtiger_salesorder s
      LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid AND productid = 14244
      INNER JOIN vtiger_crmentity e ON e.crmid=salesorderid
      WHERE deleted = 0 AND i.id IS NULL AND (DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN :date_from AND :date_to)
      GROUP BY salesorderid");

      $salesorders_with_out_loads->setFetchMode(PDO::FETCH_ASSOC);
      $salesorders_with_out_loads->execute([':date_from' => $date_from,':date_to' => $date_to]);
      $salesorders_with_out_loads_count = $salesorders_with_out_loads->rowCount();  

      $orders_with_out_orders = [];
      foreach ($salesorders_with_out_loads as $order) {
        $orders_with_out_orders[] = $order['shipment_code'];
      }




      $shipment_code = "'" .implode("', '",$orders_with_out_orders). "'"; 
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'shipment_code' => $shipment_code));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 300);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
      $orders = curl_exec($ch);
      curl_close($ch);   

      $metrika_order_loads = [];

      $data2 = json_decode($orders,true); 

      foreach ($data2['order'] AS $value){
        if(isset($value['load'])){
          $metrika_order_loads[$value['shipment_code']] = 'Yra'; 
        }else{
          $metrika_order_loads[$value['shipment_code']] = 'Nėra'; 
        }
        
      }     

      $get_imported_lost_orders = $dbh->prepare("SELECT shipment_code,import_date FROM app_lost_shipments_report WHERE DATE_ADD(DATE_FORMAT(import_date,'%Y-%m-%d'), INTERVAL 7 DAY )  >= CURDATE() ORDER BY import_date DESC");
      $get_imported_lost_orders->setFetchMode(PDO::FETCH_ASSOC);
      $get_imported_lost_orders->execute(array());
      $get_imported_lost_orders_count = $get_imported_lost_orders->rowCount();  

      $check_doc_receive_price = $dbh->prepare("SELECT shipment_code,margin, IF(service = 12, 'Doc','Order') AS type
      FROM vtiger_inventoryproductrel 
      JOIN vtiger_salesorder on salesorderid=id
      INNER JOIN vtiger_crmentity on crmid=id 
      WHERE service IN (12,32) AND productid = 36641 AND setype = 'SalesOrder' AND deleted = 0 AND margin > IF(service = 12, 0.5,2)");
      $check_doc_receive_price->setFetchMode(PDO::FETCH_ASSOC);
      $check_doc_receive_price->execute(array());
      $check_doc_receive_price_count = $check_doc_receive_price->rowCount();  

      // Check vat price

      $date_from = date('Y-m-d',strtotime("-30 days"));

      $get_invoices_with_wrong_vat = $dbh->prepare("SELECT invoiceid,  ROUND(s_h_amount, 2)  as current, ROUND(subtotal * 0.21, 2) AS pvm
                                                    FROM vtiger_invoice
                                                    LEFT JOIN vtiger_taxregions on regionid=region_id
                                                    INNER JOIN vtiger_crmentity ON crmid=invoiceid
                                                    WHERE DATE_FORMAT(createdtime,'%Y-%m-%d') > '$date_from' AND region_id = 0 AND ROUND(subtotal * 0.21, 2) != ROUND(s_h_amount, 2) 
                                                    AND deleted=0");

      $get_invoices_with_wrong_vat->setFetchMode(PDO::FETCH_ASSOC);
      $get_invoices_with_wrong_vat->execute(array());   
      $get_invoices_with_wrong_vat_count = $get_invoices_with_wrong_vat->rowCount();                                            
 
      $dbh->commit();
    }
  }

  $content = '<!DOCTYPE html>
  <html lang="en">  
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
      Ataskaita
    </title>  
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <link id="pagestyle" href="./assets/css/material-dashboard.css?v=3.0.0" rel="stylesheet" />
    <style>  
     .diff {
        padding-left: 10px;
        font-size: 14px;
      }
    </style>
  </head>
  <body class="g-sidenav-show  bg-gray-200"><div class="container">';



  if ($logged){
    $content .= ' <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
              <!-- Navbar -->
              <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
              <div class="container-fluid py-1 px-3">
                <nav aria-label="breadcrumb">                  
                  <h6 class="font-weight-bolder mb-0">Ataskaita</h6>
                </nav>
                <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                  <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                    
                  </div>
                  <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                      <a href="javascript:;" class="nav-link text-body font-weight-bold px-0">                                  
                        <a href="OrdersReport.php?logout=1" class="d-sm-inline d-none">Atsijungti</span>
                      </a>
                    </li>
                    <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
                      <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                          <i class="sidenav-toggler-line"></i>
                          <i class="sidenav-toggler-line"></i>
                          <i class="sidenav-toggler-line"></i>
                        </div>
                      </a>
                    </li>           
                   </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            
            <div class="py-4">
            <div class="row">
              <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                  <div class="card-header p-3 pt-2">
                    <div class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                      <i class="material-icons opacity-10" title="Tikrinami visi crm užsakymai">live_help</i>
                    </div>
                    <div class="text-end pt-1">
                      <p class="text-sm mb-0">Kroviniai turintys dubliu</p>
                      <h4 class="mb-0">'.$cargo_dublikates.'</h4>
                    </div>
                  </div>
                  <hr class="dark horizontal my-0">
                  <div class="card-footer p-3">';
                  if($cargo_dublikates > 0){
                    $content .= '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#show_orders_with_dublicates" aria-expanded="false" aria-controls="show_orders_with_dublicates">Rodyti užsakymus</button>
                    <a href="functions/delete_dublicates.php" style="color: #e91e63;font-size: 12px">Taisyti</a>
                    <div class="collapse" id="show_orders_with_dublicates">
                    <ul>';
                      foreach ($check_order_dublikates as $code) {
                        $content .= '<li>'.$code['shipment_code'].'</li>';
                      }
                    $content .= '</ul>                  
                    </div>';
                  }

                  $content .= ' </div>
                </div>
              </div>


              <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                  <div class="card-header p-3 pt-2">
                    <div class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                      <i class="material-icons opacity-10" title="Tikrinamos visos sąskaitos">live_help</i>
                    </div>
                    <div class="text-end pt-1">
                      <p class="text-sm mb-0" title="Užsakymai kurie turi daugiau nei viena žymę, kad jiems išrašyta sąskaita">Saskaitos žymė užsakymui</p>
                      <h4 class="mb-0">'.$invoice_tag_dublikates.'</h4>
                    </div>
                  </div>
                  <hr class="dark horizontal my-0">
                  <div class="card-footer p-3">';
                  if($invoice_tag_dublikates > 0){
                    $content .= '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#show_orders_with_double_tag" aria-expanded="false" aria-controls="show_orders_with_double_tag">Rodyti užsakymus</button>
                    <div class="collapse" id="show_orders_with_double_tag">
                    <ul>';
                      foreach ($invoice_tag_on_salesorder as $code) {
                        $content .= '<li>'.$code['shipment_code'].'</li>';
                      }
                    $content .= '</ul>
                    
                    </div>';
                  }
                  $content .= '</div>
                </div>
              </div>

              <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                  <div class="card-header p-3 pt-2">
                    <div class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                      <i class="material-icons opacity-10" title="Tikrinama savaitė atgal neskaitant šiandienos">live_help</i>
                    </div>
                    <div class="text-end pt-1">
                      <p class="text-sm mb-0">Užsakymai be krovinių</p>
                      <h4 class="mb-0">'.$salesorders_with_out_loads_count.'</h4>
                    </div>
                  </div>
                  <hr class="dark horizontal my-0">
                  <div class="card-footer p-3">';
                  if($salesorders_with_out_loads_count > 0){
                  $content .= '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#show_orders_without_loads" aria-expanded="false" aria-controls="show_orders_without_loads">Rodyti užsakymus</button>

                    <div class="collapse" id="show_orders_without_loads">
                    <ul>';
                      foreach ($orders_with_out_orders as $code) {
                        $content .= '<li>'.$code.' Metrikoje: '.($metrika_order_loads[$code] ?:'Nėra').'</li>';
                      }
                    $content .= '</ul>                    
                    </div>';
                  }
                  $content .= '</div>             
              </div>
              </div>

              <div class="col-xl-3 col-sm-6">
                <div class="card">
                  <div class="card-header p-3 pt-2">
                    <div class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                      <i class="material-icons opacity-10" title="Tikrinama savaitė atgal neskaitant šiandienos">live_help</i>
                    </div>
                    <div class="text-end pt-1">
                      <p class="text-sm mb-0" title="Paskaičiuota kaina su senais matmenim">Kaina pagal senus matmenis</p>
                      <h4 class="mb-0">'.count($compared_loads).'</h4>
                    </div>
                  </div>
                  <hr class="dark horizontal my-0">
                  <div class="card-footer p-3">'; 
                  if(count($compared_loads) > 0){
                    $content .= '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#show_orders_with_diffs" aria-expanded="false" aria-controls="show_orders_with_diffs">Rodyti užsakymus</button>
                    <div class="collapse" id="show_orders_with_diffs">
                    <ul class="diff" data-shipments="'.implode(',',array_keys($compared_loads)).'">';
                      foreach ($compared_loads as $code) {
                        $content .= '<li>'.$code.' '.$diff[$code].'</li>';
                      }
                    $content .= '</ul>
                    
                    </div>';
                  }
                  $content .= '</div>
                </div>
              </div>

              <div class="col-xl-3 col-sm-6 mt-5">
              <div class="card">
                <div class="card-header p-3 pt-2">
                  <div class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10" title="Rodoma savaitė atgal">live_help</i>
                  </div>
                  <div class="text-end pt-1">
                    <p class="text-sm mb-0" title="Užsakymai kurie nebuvo įkelti į crm">Importuota naktiniu importu</p>
                    <h4 class="mb-0">'.$get_imported_lost_orders_count.'</h4>
                  </div>
                </div>
                <hr class="dark horizontal my-0">
                <div class="card-footer p-3">'; 
                if($get_imported_lost_orders_count > 0){
                  $content .= '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#show_night_import_orders" aria-expanded="false" aria-controls="show_night_import_orders">Rodyti užsakymus</button>
                  <div class="collapse" id="show_night_import_orders">
                  <ul class="diff">';
                    foreach ($get_imported_lost_orders as $code) {
                      $content .= '<li>'.$code['shipment_code'].' Įkelta '.$code['import_date'].'</li>';
                    }
                  $content .= '</ul>
                  
                  </div>';
                }
                $content .= '</div>
              </div>
            </div>

            <div class="col-xl-3 col-sm-6 mt-5">
            <div class="card">
              <div class="card-header p-3 pt-2">
                <div class="icon icon-lg icon-shape bg-gradient-success shadow-info text-center border-radius-xl mt-n4 position-absolute">
                  <i class="material-icons opacity-10" title="Rodomi visi crm įrašai">live_help</i>
                </div>
                <div class="text-end pt-1">
                  <p class="text-sm mb-0" title="Gražinamu dokumentu arba užsakymo suvedimo nelogiškos kainos">Paslaugu kainos</p>
                  <h4 class="mb-0">'.$check_doc_receive_price_count.'</h4>
                </div>
              </div>
              <hr class="dark horizontal my-0">
              <div class="card-footer p-3">'; 
              if($check_doc_receive_price_count > 0){
                $content .= '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#show_orders_docs" aria-expanded="false" aria-controls="show_orders_docs">Rodyti užsakymus</button>
                <div class="collapse" id="show_orders_docs">
                <ul class="diff">';
                  foreach ($check_doc_receive_price as $code) {
                    $content .= '<li>'.$code['shipment_code'].' '.$code['type'].' Kaina '.$code['margin'].'</li>';
                  }
                $content .= '</ul>
                
                </div>';
              }
              $content .= '</div>
            </div>
          </div>


          <div class="col-xl-3 col-sm-6 mt-5">
          <div class="card">
            <div class="card-header p-3 pt-2">
              <div class="icon icon-lg icon-shape bg-gradient-success shadow-info text-center border-radius-xl mt-n4 position-absolute">
                <i class="material-icons opacity-10" title="Rodomos 30 dienu senumo sąskaitos">live_help</i>
              </div>
              <div class="text-end pt-1">
                <p class="text-sm mb-0">Neteisingi saskaitu PVM</p>
                <h4 class="mb-0">'.$get_invoices_with_wrong_vat_count.'</h4>
              </div>
            </div>
            <hr class="dark horizontal my-0">
            <div class="card-footer p-3">'; 
            if($get_invoices_with_wrong_vat_count > 0){
              $content .= '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#show_orders_docs" aria-expanded="false" aria-controls="show_orders_docs">Rodyti sąskaitas</button>
              <div class="collapse" id="show_orders_docs">
              <ul class="diff">';
                foreach ($get_invoices_with_wrong_vat as $code) {
                  $content .= '<li><a target="_blank" href="/index.php?module=Invoice&view=Detail&record='.$code['invoiceid'].'">'.$code['invoiceid'].'</a> - '.$code['current'].' / '.$code['pvm'].'</li>';
                }
              $content .= '</ul>
              
              </div>';
            }
            $content .= '</div>
          </div>
        </div>


            </div> 
              
          </div>
        </main>
            
            ';


  
  }else{
    $content .= '  
        <h2>Prisijungimas</h2>
        <form action="" method="POST">
        <div class="form-group">
          <label for="exampleInputEmail1">El. pašto adresas</label>
          <input type="user_name" class="form-control" name="user_name" placeholder="Crm prisijungimo vardas">            
        </div>
        <div class="form-group mt-2">
          <label for="exampleInputPassword1">Slaptažodis</label>
          <input type="password" class="form-control" name="password" placeholder="Crm prisijungimo slaptažodis">
        </div>

        <button type="submit" class="btn btn-primary mt-3">Prisijungti</button>
      </form>';
  }  


 $content .='<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>   
 </main>
  </body>
</html>';
     


echo $content;
