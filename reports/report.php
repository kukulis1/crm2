<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $type = $_POST['type'];
  $from = $_POST['from'];
  $to = $_POST['to'];


  $invoice_type = $_POST['invoice_type'];
  $accountname = $_POST['accountname'];
  $invoice_no = $_POST['invoice_no'];
  $debt = $_POST['debt'];
  $payed = $_POST['payed'];
  $late_payment = $_POST['late_payment'];
  $duedate = $_POST['duedate'];
  $proc = $_POST['proc'];
  $withVat = $_POST['withVat'];
  $withoutVat =$_POST['withoutVat'];
  $invoicedate =$_POST['invoicedate'];
  $costcenter =$_POST['costcenter'];
  $employee =$_POST['employee'];
  $object =$_POST['object'];

  if($type == 'service'){
    $query = "SELECT CASE WHEN regionid IS NULL 
                      THEN 21 
                      ELSE vtiger_taxregions.value 
                    END AS tax,
                      CASE WHEN regionid IS NOT NULL    
                      THEN article
                      END AS article,      
                      CASE 
														WHEN productid = 121391 THEN cargo_wgt
														ELSE s.name						                                                   
                        END as accountname,        
               costcenter_tks_cost, employee,object,                           

              SUM(IF(productid = 121391, listprice*quantity,listprice) * (1+IFNULL(vtiger_taxregions.value, 21)/100))withVat,

              SUM(IF(productid = 121391, listprice*quantity,listprice) * (1+IFNULL(vtiger_taxregions.value, 21)/100) ) AS withVat2,

              ROUND(CASE WHEN productid = 121391 
              THEN SUM(listprice*quantity)
              ELSE SUM(listprice)              
              END,2) as withoutVat, GROUP_CONCAT(DISTINCT i.invoiceid) AS invoiceids, service AS services,GROUP_CONCAT(DISTINCT costcenterid) AS costcenterids,cf_1277 as invoice_type
              FROM vtiger_invoice i             
              LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=i.invoiceid
              LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid 
              LEFT JOIN vtiger_inventoryproductrel inv ON inv.id=i.invoiceid
              LEFT JOIN app_services_type s ON s.id=inv.service
              LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`i`.`region_id` AND i.region_id != 0  
              LEFT JOIN `vtiger_costcenter` c ON c.costcenterid=inv.costcenter
              WHERE setype = 'Invoice' AND cf_1277 != 'Išankstinė' AND invoicedate BETWEEN '$from' AND '$to' ";
              if($costcenter){
               $query .= " AND c.costcenterid = $costcenter ";
              }
              if($employee){
                $query .= " AND employee LIKE '$employee%' ";
              }
              if($object){
                $query .= " AND object LIKE '$object%' ";
              }
              if(!empty($invoice_type)){
                $query .= " AND cf_1277 = '$invoice_type' ";
              }                 
              $query .= " AND deleted = 0 GROUP BY service,i.region_id,invoice_type,s.name, c.costcenterid";                

              if(isset($proc) || !empty($withVat) || !empty($withoutVat) || !empty($accountname)){
                $query .= " HAVING 1+1 ";
              }             
              if(!empty($accountname)){
                $query .= " AND accountname LIKE '$accountname%' ";
              }
              if(isset($proc)){
                $query .= " AND tax LIKE '$proc%' ";
              }
              if(!empty($withVat)){
                $query .= " AND withVat LIKE '$withVat%' ";
              }
              if(!empty($withoutVat)){
                $query .= " AND withoutVat LIKE '$withoutVat%' ";
              }        

              $query .= " ORDER BY i.invoiceid ASC ";
                   
              $conn->query("SET SESSION group_concat_max_len = 1000000");
  }elseif($type == 'client'){
    $query = "SELECT IFNULL(vtiger_taxregions.value,21) tax,
                     CASE WHEN regionid IS NOT NULL THEN article END AS article, IFNULL(accountname,'--') AS accountname,                           
                     SUM(IF(productid = 121391, listprice*quantity,listprice) * (1+IFNULL(vtiger_taxregions.value, 21)/100)) withVat,
                     SUM(IF(productid = 121391, listprice*quantity,listprice) * (1+IFNULL(vtiger_taxregions.value, 21)/100) ) AS withVat2
                    FROM vtiger_invoice inv     
                    JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=inv.invoiceid  
                    LEFT JOIN vtiger_inventoryproductrel i ON i.id=inv.invoiceid        
                    LEFT JOIN vtiger_account a ON a.accountid=inv.accountid
                    LEFT JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid 
                    LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`inv`.`region_id` AND inv.region_id != 0  
                    WHERE setype = 'Invoice' AND invoicedate BETWEEN '$from' AND '$to' AND cf_1277 != 'Išankstinė' ";
                    if(!empty($accountname)){
                      $query .= " AND a.accountname LIKE '$accountname%' ";
                    }
                $query .= " AND deleted = 0 GROUP BY a.accountid ";
                if(isset($proc) || !empty($withVat) || !empty($withoutVat)){
                  $query .= " HAVING 1+1 ";
                }
                if(isset($proc)){
                  $query .= " AND tax LIKE '$proc%' ";
                }
                if(!empty($withVat)){
                  $query .= " AND withVat LIKE '$withVat%' ";
                }
                if(!empty($withoutVat)){
                  $query .= " AND withoutVat LIKE '$withoutVat%' ";
                }
                $query .= " ORDER BY inv.invoiceid DESC";

}elseif($type == 'main'){ 

  $query_invoice_per_day = "SELECT invoicedate,count(inv.invoiceid) AS total_invoice
          FROM vtiger_invoice inv   
          JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=inv.invoiceid        
          LEFT JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid                    
          WHERE setype = 'Invoice' AND invoicedate  BETWEEN '$from' AND '$to' AND cf_1277 != 'Išankstinė' ";

          if($accountname){
            $query_invoice_per_day .= " AND inv.invoicedate LIKE '$accountname%' ";
          }
          $query_invoice_per_day .= " AND deleted = 0 GROUP BY invoicedate "; 

          if(!empty($withVat) || !empty($withoutVat)){          
            $query_invoice_per_day .= " HAVING 1+1 ";
          }  
 
          if(!empty($withVat)){
            $query_invoice_per_day .= " AND withVat LIKE '$withVat%' ";
          }
          if(!empty($withoutVat)){
            $query_invoice_per_day .= " AND withoutVat LIKE '$withoutVat%' ";
          }
      $query_invoice_per_day .= " ORDER BY inv.invoiceid DESC";

      $result = $conn->query($query_invoice_per_day);

      $invoice_per_day = [];
      
      foreach ($result as $row) {
        $invoice_per_day[$row['invoicedate']] = $row['total_invoice'];
      }


  $query = "SELECT invoicedate,  
  

          REPLACE(SUM(IF(productid = 121391, listprice*quantity,listprice) * (1+IFNULL(vtiger_taxregions.value, 21)/100) ),'.',',') AS withVat,  

          SUM(IF(productid = 121391, listprice*quantity,listprice) * (1+IFNULL(vtiger_taxregions.value, 21)/100) ) AS withVat2,


          REPLACE(ROUND(CASE WHEN productid = 121391 
          THEN SUM(listprice*quantity)
          ELSE SUM(listprice)              
          END,2),'.',',') as withoutVat

          FROM vtiger_invoice inv   
          LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=inv.invoiceid        
          LEFT JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid     
          LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=inv.invoiceid    
          LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`inv`.`region_id` AND inv.region_id != 0  
          WHERE (invoicedate BETWEEN '$from' AND '$to') AND setype = 'Invoice' AND cf_1277 != 'Išankstinė' ";

  if($accountname){
    $query .= " AND inv.invoicedate LIKE '$accountname%' ";
  }
  $query .= " AND deleted = 0 GROUP BY invoicedate "; 
      // if(isset($proc) || !empty($withVat) || !empty($withoutVat)){
      if(!empty($withVat) || !empty($withoutVat)){
        $query .= " HAVING 1+1 ";
      }  
      // if(isset($proc)){
      //   $query .= " AND total_invoice LIKE '$proc%' ";
      // }
      if(!empty($withVat)){
        $query .= " AND withVat LIKE '$withVat%' ";
      }
      if(!empty($withoutVat)){
        $query .= " AND withoutVat LIKE '$withoutVat%' ";
      }
  $query .= " ORDER BY inv.invoiceid DESC";


  $result2 = $conn->query($query);

  $report = [];
  
  foreach ($result2 as $row) {
    $report[] = ['invoicedate' => $row['invoicedate'],
                 'withVat' => $row['withVat'],
                 'withVat2' => $row['withVat2'],
                 'withoutVat' => $row['withoutVat'],
                 'invoicedate' => $row['invoicedate'],
                 'invoicedate' => $row['invoicedate'],
                 'total_invoice' => $invoice_per_day[$row['invoicedate']],
                ];
  }

  echo json_encode($report);
  return;

}elseif($type == 'debt'){       
  $query = "SELECT vtiger_invoice.invoiceid, invoice_no, setype,
              CASE 
                WHEN debts_tks_suma IS NULL
                THEN IF(vtiger_invoice.region_id = 5,subtotal,total)
                ELSE IF(vtiger_invoice.region_id = 5,subtotal,total) - SUM(vtiger_debts.debts_tks_suma)
              END AS debt,             
              debts_tks_suma,accountname,
              CASE 
                WHEN debts_tks_suma IS NULL
                THEN 0
                ELSE ROUND(SUM(vtiger_debts.debts_tks_suma),2)
              END AS payed, 
              CASE 
                WHEN  vtiger_invoice.duedate < CURDATE()
                THEN DATEDIFF(CURDATE(), vtiger_invoice.duedate)
                ELSE 0
              END AS late_payment, MAX(debts_tks_data) AS payment,
              CASE WHEN regionid IS NULL 
                    THEN 21 
                    ELSE vtiger_taxregions.value 
              END AS tax,
              CASE WHEN regionid IS NOT NULL    
                THEN article
              END AS article, 
              IF(vtiger_invoice.region_id = 5,subtotal,total) AS withVat, vtiger_invoice.duedate, vtiger_invoice.subtotal as withoutVat, vtiger_invoice.invoicedate
          FROM vtiger_invoice 
          JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
          LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'	
          LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
          LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid          
          INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid               
          LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=vtiger_invoice.region_id 
          WHERE (duedate BETWEEN '$from' AND '$to') AND vtiger_crmentity.setype = 'Invoice'  AND cf_1277 = 'Debetinė' ";

          if(!empty($accountname)){
            $query .= " AND vtiger_account.accountname LIKE '$accountname%' ";
          }              
          if(!empty($invoicedate)){
            $query .= " AND vtiger_invoice.invoicedate LIKE '$invoicedate%' ";
          }
          if(!empty($invoice_no)){
            $query .= " AND vtiger_invoice.invoice_no LIKE '$invoice_no%' ";
          }
          if(!empty($duedate)){
            $query .= " AND vtiger_invoice.duedate LIKE '$duedate%' ";
          }

          $query .= " AND vtiger_crmentity.deleted=0 GROUP BY vtiger_invoice.invoiceid ";
          
          
          if(isset($proc) || !empty($withVat) || !empty($late_payment) || !empty($withoutVat) || !empty($debt) || $payed != 'visi'){
            $query .= " HAVING 1+1 ";
          }  

          if(!empty($debt)){
            $query .= " AND debt LIKE '$debt%' ";
          }

          if(isset($late_payment)){
            if($late_payment == 1){
              $query .= " AND (vtiger_invoice.duedate < payment OR (payment IS NULL AND vtiger_invoice.duedate < CURRENT_DATE())) ";
            }elseif($late_payment == 0){
              $query .= " AND vtiger_invoice.duedate >= CURRENT_DATE() ";
            }
          }

          if($payed == 'unpaid'){
            $query .= " AND ROUND(debt,2) > 0";
          } else if ($payed == 'paid'){
            $query .= " AND ROUND(debt,2) <= 0";
          }

          if(isset($proc)){
            $query .= " AND tax LIKE '$proc%' ";
          }
          if(!empty($withVat)){
            $query .= " AND withVat LIKE '$withVat%' ";
          }
          if(!empty($withoutVat)){
            $query .= " AND withoutVat LIKE '$withoutVat%' ";
          }

}else if($type == 'purchase-debt'){  
  $query = "SELECT purchaseorder_no AS invoice_no, vtiger_purchaseorder.purchaseorderid AS invoiceid, vtiger_crmentity.setype,           
                 IF(SUM(vtiger_payment.payment_tks_suma) > 0, (total - SUM(vtiger_payment.payment_tks_suma)), total) AS debt,
                 IF(SUM(vtiger_payment.payment_tks_suma) > 0, SUM(vtiger_payment.payment_tks_suma), 0) AS payed, 

                   vendorname AS accountname, cf_1345 AS invoicedate, duedate,       
                   total AS withVat,                  
                   subtotal AS withoutVat,           
  
                CASE 
                  WHEN  vtiger_purchaseorder.duedate < CURDATE()
                  THEN DATEDIFF(CURDATE(), vtiger_purchaseorder.duedate)
                  ELSE 0
                END AS late_payment, MAX(payment_tks_mokėjimodata) AS payment

                  FROM vtiger_purchaseorder 
                  LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_purchaseorder.vendorid
                  LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=vtiger_purchaseorder.purchaseorderid
                  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid -- AND relmodule = 'Payment'	
                  LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid  				 
                  INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_purchaseorder.purchaseorderid AND vtiger_crmentity.deleted = 0   
                  LEFT JOIN vtiger_crmentity pay ON pay.crmid=vtiger_payment.paymentid  AND pay.deleted = 0         
                  WHERE (vtiger_purchaseorder.duedate BETWEEN '$from' AND '$to') AND vtiger_crmentity.setype = 'PurchaseOrder' AND cf_1343 NOT IN ('Išankstinė', 'Kreditinė') "; 

                  if(!empty($accountname)){
                    $query .= " AND accountname LIKE '$accountname%' ";
                  }

                  if(!empty($invoicedate)){
                    $query .= " AND cf_1345 LIKE '$invoicedate%' ";
                  }
                  if(!empty($invoice_no)){
                    $query .= " AND vtiger_purchaseorder.purchaseorder_no LIKE '$invoice_no%' ";
                  }
                  if(!empty($duedate)){
                    $query .= " AND vtiger_purchaseorder.duedate LIKE '$duedate%' ";
                  }
                  
                  $query .= " GROUP BY vtiger_purchaseorder.purchaseorderid ";

                  if(isset($proc) || !empty($withVat) || isset($late_payment) || !empty($withoutVat) || $payed != 'visi'){
                   $query .= " HAVING 1+1 ";
                  }             
                 
                  if(isset($proc)){
                   $query .= " AND tax LIKE '$proc%' ";
                  }
                  if(!empty($withVat)){
                    $query .= " AND withVat LIKE '$withVat%' ";
                  }
                  if(!empty($withoutVat)){
                   $query .= " AND withoutVat LIKE '$withoutVat%' ";
                  } 
                  
                  if(isset($late_payment)){
                    if($late_payment == 1){
                      $query .= " AND (vtiger_purchaseorder.duedate < payment OR (payment IS NULL AND vtiger_purchaseorder.duedate < CURRENT_DATE())) ";
                    }elseif($late_payment == 0){
                      $query .= " AND vtiger_purchaseorder.duedate >= payment ";
                    }
                  }

                  if($payed == 'unpaid'){
                    $query .= " AND ROUND(debt,2) > 0 ";
                  } else if ($payed == 'paid'){
                    $query .= " AND ROUND(debt,2) <= 0 ";
                  }

                  $query .= " ORDER BY vtiger_purchaseorder.purchaseorderid ASC ";

                  $conn->query("SET SESSION group_concat_max_len = 1000000");


}



  $result = $conn->query($query);

  $report = array();

  while($row = mysqli_fetch_assoc($result))
  {
    $report[] = $row;
  }

  echo json_encode($report);


}else{
  http_response_code(404);
}
