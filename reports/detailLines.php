<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

    $invoiceid = $_POST['invoiceid'];
    $service = $_POST['service'];
    $costcenter = $_POST['costcenter'];


    $query = "SELECT REPLACE(ROUND(CASE WHEN productid = 121391 
                                    THEN listprice*quantity
                                    ELSE listprice            
                                  END,2),'.',',') AS listprice, 
                      REPLACE(ROUND(CASE WHEN inv.region_id = 0 
                                      THEN   ( IF(productid = 121391, listprice*quantity,listprice)/100 * 21)  + IF(productid = 121391, listprice*quantity,listprice)
                                      ELSE   (IF(productid = 121391, listprice*quantity,listprice)/100 * vtiger_taxregions.value) + IF(productid = 121391, listprice*quantity,listprice)
                                    END,2),'.',',') AS listprice_vat, 
                      CASE WHEN inv.region_id = 0 
                          THEN (IF(productid = 121391, listprice*quantity,listprice)/100 * 21)  + IF(productid = 121391, listprice*quantity,listprice)
                          ELSE (IF(productid = 121391, listprice*quantity,listprice)/100 * vtiger_taxregions.value) + IF(productid = 121391, listprice*quantity,listprice)
                        END AS withVat2, a.accountname, inv.invoice_no, inv.invoiceid,invoicedate, costcenter_tks_cost, 
                        CASE 
														WHEN productid = 121391 THEN cargo_wgt
														ELSE s.name								
													END as service_name
                    FROM vtiger_inventoryproductrel i
                    LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
                    LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=inv.invoiceid
                    LEFT JOIN vtiger_account a ON a.accountid=inv.accountid
                    LEFT JOIN vtiger_crmentity e ON e.crmid=i.id  
                    LEFT JOIN app_services_type s ON s.id=i.service  
                    LEFT JOIN vtiger_costcenter c ON c.costcenterid=i.costcenter 
                    LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=inv.region_id)             
                    WHERE i.id IN ($invoiceid) AND e.deleted = 0 AND e.setype = 'Invoice' ";

                    if($costcenter){
                      $query .= " AND costcenterid IN ($costcenter) ";
                    }else{
                      $query .= " AND costcenter_tks_cost IS NULL ";
                    }
                    if($service){
                      $query .= " AND i.service IN ($service) ";
                    }   
                    
                    $query .= " ORDER BY i.id,i.sequence_no ";

  $result = $conn->query($query);    

  $report = array();

  while($row = mysqli_fetch_assoc($result))
  {
    $report[] = $row;
  }

  echo json_encode($report);


} else {
  http_response_code(404);
}
