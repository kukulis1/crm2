<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

$from = $_POST['from'];
$to = $_POST['to'];

$invoice_type = $_POST['invoice_type'];
$accountname = $_POST['accountname'];
$invoice_no = $_POST['invoice_no'];
$debt = $_POST['debt'];
$payed = $_POST['payed'];
$late_payment = $_POST['late_payment'];
$duedate = $_POST['duedate'];
$proc = $_POST['proc'];
$withVat = $_POST['withVat'];
$withoutVat =$_POST['withoutVat'];
$invoicedate =$_POST['invoicedate'];
$costcenter =$_POST['costcenter'];
$employee =$_POST['employee'];
$object =$_POST['object'];


    $query = "SELECT CASE WHEN vtiger_taxregions.value IS NULL THEN 21 ELSE vtiger_taxregions.value END as tax,
                      CASE WHEN vtiger_taxregions.value IS NULL 
                      THEN article
                      END AS article,  
                        cargo_wgt as accountname,                
                    costcenter_tks_cost,cargo_length as  employee, cargo_measure as object,GROUP_CONCAT(DISTINCT costcenterid) AS costcenterids,
                    REPLACE(SUM((listprice*quantity) + ((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100)),'.',',') AS withVat,
                    SUM((listprice*quantity) + ((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100)) AS withVat2,
                    SUM(listprice*quantity) AS withoutVat,
                    REPLACE(SUM(listprice*quantity),'.',',') as withoutVat,  GROUP_CONCAT(DISTINCT i.purchaseorderid) AS invoiceids,cf_1343 as invoice_type, itemserviceid AS productid
                    FROM vtiger_purchaseorder i
                    LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=i.purchaseorderid
                    LEFT JOIN vtiger_crmentity e ON e.crmid=i.purchaseorderid 
                    LEFT JOIN vtiger_inventoryproductrel inv ON inv.id=i.purchaseorderid                     
                    LEFT JOIN vtiger_taxregions ON IF(tax2 > 0, vtiger_taxregions.regionid=tax2 ,vtiger_taxregions.regionid=region_id)    
                    LEFT JOIN `vtiger_costcenter` c ON c.costcenterid=inv.cargo_width
                    LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=inv.cargo_wgt
                    WHERE deleted = 0 AND setype = 'PurchaseOrder' AND cf_1345 BETWEEN '$from' AND '$to' ";
              if($costcenter){
               $query .= " AND c.costcenterid = $costcenter ";
              }
              if($employee){
                $query .= " AND cargo_length LIKE '$employee%' ";
              }
              if($object){
                $query .= " AND cargo_measure LIKE '$object%' ";
              }
              if(!empty($invoice_type)){
                $query .= " AND cf_1343 = '$invoice_type' ";
              }
              $query .= "  GROUP BY i.region_id,invoice_type, cargo_wgt,c.costcenterid ";                

              if(isset($proc) || !empty($withVat) || !empty($withoutVat) || !empty($accountname)){
                $query .= " HAVING 1+1 ";
              }             
              if(!empty($accountname)){
                $query .= " AND accountname LIKE '$accountname%' ";
              }
              if(isset($proc)){
                $query .= " AND tax LIKE '$proc%' ";
              }
              if(!empty($withVat)){
                $query .= " AND withVat LIKE '$withVat%' ";
              }
              if(!empty($withoutVat)){
                $query .= " AND withoutVat LIKE '$withoutVat%' ";
              }        

              $query .= " ORDER BY i.purchaseorderid ASC ";
                   
              $conn->query("SET SESSION group_concat_max_len = 1000000");
 
  $result = $conn->query($query);

  $report = array();

  while($row = mysqli_fetch_assoc($result))
  {
    $report[] = $row;
  }

  echo json_encode($report);


}else{
  http_response_code(404);
}
