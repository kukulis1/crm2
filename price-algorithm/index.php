<?php
require_once "../config.inc.php";
require_once "countries.php";
// error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
// ini_set('display_errors', 0);
if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}


if($_POST['distance'] == 100){
  $_POST['distance'] = $_POST['distance'] - 1;
}elseif($_POST['distance'] == 101){
  $_POST['distance'] = $_POST['distance'] - 2; 
}


$distance = $_POST['distance'];

if(is_numeric($_POST["accId"])){
  $id = $_POST["accId"];
}


if(is_numeric($_POST['fromPost'])){
  $from = $_POST["fromPost"];
}

if(is_numeric($_POST['toPost'])){
  $to = $_POST["toPost"];
}

if(is_numeric($_POST['toPost'])){
  $pll = $_POST["pll"];
}

$weight = $_POST['cargoKg'];
$volume = $_POST['cargoVolume'];
$square = $_POST['cargoSquare'];
$country_from = $_POST['from_country'];
$country_to = $_POST['to_country'];

// $country_from = 'LTU';
// $country_to = 'LTU';

// $id = '2762';

// $from = '02189'; // Vilnius
// $from = '52181'; // Kaunas

// $to = '02190'; // Vilnius 
// $to = '52181'; // Kaunas


// $from = '08403';
// $to = '44500';




// $weight = 300;
// $volume = 0.960 ;
// $square = 0.96;
// $meters = 0;
// $distance = 101;
// $pll = 1;

$pricebooks =  mysqli_fetch_assoc($conn->query("SELECT pricebook FROM vtiger_account WHERE accountid = $id"));

$pricebookArr = explode(',', $pricebooks["pricebook"]);
$pricesArr = array();   
$pricesArr2 = array();   

foreach($pricebookArr AS $pricebookid){
  $pricesArr[] = initCalculatePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter,$pll, $byVolume = false, $bySquare = false, $byMeter = false, $bazinis = false,$distance, $country_from, $country_to);
}

for($i =0; $i < count($pricesArr); $i++){
  $pricesArr2[$i] = $pricesArr[$i]['price'];  
}

$maxVal = max($pricesArr2);
$maxKey = array_search($maxVal, $pricesArr2);


echo json_encode($pricesArr[$maxKey]);



function initCalculatePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter,$pll, $byVolume = false, $bySquare = false, $byMeter = false, $bazinis = false,$distance, $country_from, $country_to){

  $realPll = $pll;
  $pll = false;
  $pll2 = false;

  $countPriceArr = array();  
  $pricesArr = array();  

  if($weight > 0){
   $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter,$pll,$pll2, $byVolume = false, $bySquare = false, $byMeter = false, $bazinis = false,$distance, $country_from, $country_to);
  }

  if($volume > 0){
    $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume, $square, $meter,$pll,$pll2, $byVolume = true, $bySquare = false,$byMeter = false, $bazinis = false,$distance, $country_from, $country_to);
  }
  if($square > 0){
    $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square,$meter, $pll,$pll2,$byVolume = false, $bySquare = true,$byMeter = false, $bazinis = false,$distance, $country_from, $country_to);    
  }   
        
  $pll = $realPll;
  if($pll > 0){  
   $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter,$pll,$pll2, $byVolume = false, $bySquare = false, $byMeter = false, $bazinis = false,$distance, $country_from, $country_to);
    $pll2 = true;
    $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter,$pll,$pll2, $byVolume = false, $bySquare = false, $byMeter = false, $bazinis = false,$distance, $country_from, $country_to);
  }

  for($i =0; $i < count($countPriceArr); $i++){     
    $pricesArr[$i] = $countPriceArr[$i]['price'];               
  }    
  

  $maxVal = max($pricesArr);
  $maxKey = array_search($maxVal, $pricesArr);

  if($maxVal == 0){  
      
  //BAZINIS
    $pll = false;
    $pll2 = false;
    if($weight > 0){
     $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square,$meter,$pll,$pll2,$byVolume = false, $bySquare = false,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);
    }

    if($volume > 0){
     $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume, $meter, $square,$pll,$pll2, $byVolume = true, $bySquare = false,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to); 
    } 

    if($square > 0){
      $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter, $pll,$pll2,$byVolume = false, $bySquare = true,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);
    }
        
    $pll = $realPll;
    if($pll > 0){ 
     $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter,$pll,$pll2,$byVolume = false, $bySquare = false,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);  
      $pll2 = true;             
      $countPriceArr[] = takePrice($conn,$from,$to,$id,$pricebookid,$weight,$volume,$square, $meter,$pll,$pll2,$byVolume = false, $bySquare = false,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);   
    }                              
 
    for($i =0; $i < count($countPriceArr); $i++){     
      $pricesArr[$i] = $countPriceArr[$i]['price'];               
    }    
    
  
    $maxVal = max($pricesArr);
    $maxKey = array_search($maxVal, $pricesArr);
  }

return $countPriceArr[$maxKey];

}


function takePrice($conn,$from,$to, $id,$pricebookid,  $weight, $volume, $square, $meter,$pll,$pll2,  $byVolume, $bySquare,$byMeter, $bazinis, $distance, $country_from, $country_to){

  if($country_from == 'LTU' OR $country_from == 'LT'){
    $post_from = mysqli_fetch_assoc($conn->query("SELECT `zone_customer`,`zone_base`, `city`, `state`,`post_code`  FROM `crm_post_codes` WHERE `post_code` = '$from'"));
  }elseif($country_from == 'LVA' OR $country_from == 'LV'){
    $post_from = mysqli_fetch_assoc($conn->query("SELECT zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $from BETWEEN code_from AND code_to"));
  }

  if($country_to == 'LTU' OR $country_to == 'LT'){
    $post_to = mysqli_fetch_assoc($conn->query("SELECT  `zone_customer`,`zone_base`, `city`, `state`,`post_code`  FROM `crm_post_codes` WHERE `post_code` = '$to'"));
  }elseif($country_to == 'LVA' OR $country_to == 'LV'){
    $post_to = mysqli_fetch_assoc($conn->query("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $to BETWEEN code_from AND code_to"));
  }

  $locations =  mysqli_fetch_assoc($conn->query("SELECT * FROM `vtiger_accountscf`
                                                          LEFT JOIN `vtiger_account` on `vtiger_accountscf`.`accountid` = `vtiger_account`.`accountid`
                                                          LEFT JOIN crm_directions ON `crm_directions`.`directionid`=`vtiger_accountscf`.`accountid`
                                                          LEFT JOIN crm_address_post_code adr ON `adr`.`addressid`=`vtiger_accountscf`.`accountid`
                                                          WHERE `vtiger_accountscf`.`accountid` = ".$id));
    $direct_location = Array();
    $direct_location_adr = Array();
    $columns = Array('1218','1222','1226','1230','1234','1238','1242','1246','1250','1254','1400','1404','1408','1412','1416','1420','1424','1428','1432','1436');

    foreach($columns AS $col){
      if(isset($locations["post_cf_".$col])){
        $slashExplode = explode("/",$locations["post_cf_".$col]);        
        for($f =0; count($slashExplode) > $f; $f++){
         $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
        }      
        for($e =0; count($slashExplode) > $e; $e++){          
            $post_cf_[$col][$e] = multiexplode(array(";","-"), $equalsExplode[$col."_".$e][1]);          
            if(!empty($equalsExplode[$col."_".$e][0])){         
              $direct_location[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]);      
            }  
        }
      }
    } 

    foreach($columns AS $col){
      if(isset($locations["code_cf_".$col])){
        $slashExplode = explode("/",$locations["code_cf_".$col]);        
        for($f =0; count($slashExplode) > $f; $f++){
         $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
        }      
        for($e =0; count($slashExplode) > $e; $e++){          
            $post_cf_[$col][$e] = multiexplode(array(";","-"), $equalsExplode[$col."_".$e][1]);          
            if(!empty($equalsExplode[$col."_".$e][0])){         
              $direct_location_adr[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]);      
            }  
        }
      }
    } 


if(!empty($post_from) AND !empty($post_to)){

    if(!$bazinis){

      if($pll > 0 && !$pll2){
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  AND`vtiger_products`.`pll_pcs` = $pll AND `vtiger_products`.`pll_pcs` != ''";
      
      }elseif($pll2){                                    
         $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  AND 
                                          $pll = `vtiger_products`.`pll_pcs` AND `vtiger_products`.`min_weight_kg` <=  $weight AND $weight <= `vtiger_products`.`max_weight_kg`";
         
      }elseif($byVolume){
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  
                                          AND (`vtiger_products`.`min_volume_m3` <= $volume AND $volume <= `vtiger_products`.`max_volume_m3`)
                                          "; 
    
    }elseif($bySquare){
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  
                                          AND (`vtiger_products`.`min_square_m2` <= $square AND $square <= `vtiger_products`.`max_square_m2`)
                                          "; 
    }elseif($byMeter){
      $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                        LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                        LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                        LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                        LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                        WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = $pricebookid  AND `vtiger_productcf`.`cf_1635` <= $meter  AND $meter <= `vtiger_productcf`.`cf_1637`"; 
    
    }else{
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                      LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                      LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                      LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                      LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                      WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = $pricebookid 
                                      AND (`vtiger_products`.`min_weight_kg` <=  $weight AND  $weight <= `vtiger_products`.`max_weight_kg`)
                                      "; 
                                      
    }
      
    // echo $get_price_query_string."<br><br>";
    
    }else{

      if($pll > 0 && !$pll2){
        $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = 10965  AND $pll = `vtiger_products`.`pll_pcs` ";

      }elseif($pll2){                                    
        $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                        LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                        LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                        LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                        LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                        WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = 10965  AND 
                                        $pll = `vtiger_products`.`pll_pcs` AND `vtiger_products`.`min_weight_kg` <=  $weight AND $weight <= `vtiger_products`.`max_weight_kg`";
      }elseif($byVolume){
            
            $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                    LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                    LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                    LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                    LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                    WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = 10965 
                                    AND (`vtiger_products`.`min_volume_m3` <= $volume AND $volume <= `vtiger_products`.`max_volume_m3`)
                                    "; 
      
      }elseif($bySquare){
      
            $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0  AND `vtiger_pricebook`.`pricebookid` = 10965 
                                          AND (`vtiger_products`.`min_square_m2` <= $square AND $square <= `vtiger_products`.`max_square_m2`) 
                                          ";
      }elseif($byMeter){
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = 10965  AND `vtiger_productcf`.`cf_1635` <= $meter  AND $meter <= `vtiger_productcf`.`cf_1637`";                         
      
      }else{
    
            $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                              LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                              LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                              LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                              LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                              WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = 10965 
                                              AND (`vtiger_products`.`min_weight_kg` <=  $weight AND  $weight <= `vtiger_products`.`max_weight_kg`) ";                              
      }
    }


    


 
  
    $met = 'mysqli';

    if(!$bazinis){
    
      
      $get_price = countriesZones($conn,$direct_location,$direct_location_adr, $met, $country_from, $country_to, $post_from, $post_to,$locations ,$distance, $get_price_query_string, $from,$to);

      foreach($get_price as $get_prices){        
        $get =  $get_prices;           
      
        $result = array( 
          "price" => (float)$get_prices["listprice"],   
          'pricebook' => $get_prices['productname'],
          "max_weight" => $get_prices['max_weight_kg'],
          "min_weight" => $get_prices['min_weight_kg'],  
          "max_volume" => $get_prices['max_volume_m3'],        
          "min_volume" => $get_prices['min_volume_m3'],
          "max_square" => $get_prices['max_square_m2'],
          "min_square" => $get_prices['min_square_m2'],          
          "combination" => $get_prices["productname"],
          "consignee" => $get_prices["consignee"],
          'stevedoring' => (float)$get_prices['cf_954'],
          "from" => $post_from["zone_customer"],
          'to' => $post_to["zone_customer"],
          "pll" => $get_prices["pll_pcs"]
        );                              
        break;  
      } 
      
    }else{
      
        require_once "base_catalog.php";
      
        $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_customer"]).":"; 
        $get_price = basecatalog($location,$get_price_query_string_bazinis, $conn,$met);
      
        $location2 = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_base"]).":"; 
        $get_price_base = basecatalog($location2,$get_price_query_string_bazinis, $conn,$met);
      
        $location3 = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":"; 
        $get_price_base2 = basecatalog($location3,$get_price_query_string_bazinis, $conn,$met);  
      
        $location4 = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":"; 
        $get_price_base3 = basecatalog($location4,$get_price_query_string_bazinis, $conn,$met);  
      
        $get_base = array_merge($get_price, $get_price_base, $get_price_base2,$get_price_base3);
      
          foreach($get_base as $get_prices){           
              $get =  $get_prices; 
              $result = array( 
                "price" => (float)$get_prices["listprice"],       
                'pricebook' => "BAZINISLT ". $get_prices["productname"],               
                "max_weight" => $get_prices['max_weight_kg'],
                "min_weight" => $get_prices['min_weight_kg'],                
                "max_volume" => $get_prices['max_volume_m3'],
                "min_volume" => $get_prices['min_volume_m3'],               
                "max_square" => $get_prices['max_square_m2'],
                "min_square" => $get_prices['min_square_m2'],
                "combination" => "BAZINISLT ".$get_prices["productname"],
                'stevedoring' => (float)$get_prices['cf_954'],
                "consignee" =>  $get_prices["consignee"],
                "pll" => $get_prices["pll_pcs"]
              );                     
              break;
          }
      
      
    }


}

if(!empty($get)){
  $rez =  $result;   
}else{

  if(empty($country_from) && empty($country_to)){
    $rez =  array('price' => 0,  'combination' => 'Nenurodytos pakrovimo ir iškrovimo šalys', 'max_volume' => 0);
  }elseif(empty($country_from)){
    $rez =  array('price' => 0,  'combination' => 'Nenurodyta pakrovimo šalis', 'max_volume' => 0);
  }elseif(empty($country_to)){
    $rez =  array('price' => 0,  'combination' => 'Nenurodyta iškrovimo šalis', 'max_volume' => 0);
  }else{


    if(empty($post_from['zone_base']) AND empty($post_to['zone_base'])){
      $rez =  array('price' => 0,  'combination' => 'Blogi pašto kodai', 'max_volume' => 0);
    }elseif(empty($post_from['zone_base'])){
      $rez =  array('price' => 0,  'combination' => 'Blogas pakrovimo pašto kodas', 'max_volume' => 0);
    }elseif(empty($post_to['zone_base'])){
      $rez =  array('price' => 0,  'combination' => 'Blogas iskrovimo pasto kodas', 'max_volume' => 0);
    }else{	
      $rez =  array('price' => 0,  'combination' => "Nėra kombinacijos ".$post_from['zone_base']." - ".$post_to['zone_base']." arba ".$post_from['zone_customer']." - ".$post_to['zone_customer']."", 'max_volume' => 0);
    }
  }
}  

return  $rez;
}   



function multiexplode ($delimiters,$data) {
  $MakeReady = str_replace($delimiters, $delimiters[0], $data);
  $Return    = explode($delimiters[0], $MakeReady);
  return  $Return;
} 
      
?>
