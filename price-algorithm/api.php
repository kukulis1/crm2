<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require_once "../config.inc.php";

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");
if($_GET['record']){
$acc = mysqli_fetch_array($conn->query("SELECT `accountid` FROM `vtiger_account` WHERE `pricebook` = ".$_GET['record']));


$locations =  $conn->query("SELECT * FROM `vtiger_accountscf`
LEFT JOIN `vtiger_account` on `vtiger_accountscf`.`accountid` = `vtiger_account`.`accountid`
WHERE `vtiger_accountscf`.`accountid` = ".$acc['accountid']);

$zones = array();

while ($user = $locations->fetch_assoc()){
  extract($user);

  if($cf_1216 == 'Miestas - Miestas'){
    $zone_city_to_city[] = "Zona 1";
   }
   if($cf_1220 == 'Miestas - Miestas'){
    $zone_city_to_city[] = "Zona 2";
   }
   if($cf_1224 == 'Miestas - Miestas'){   
    $zone_city_to_city[] = "Zona 3";
   }
   if($cf_1228 == 'Miestas - Miestas'){   
    $zone_city_to_city[] = "Zona 4";
   }
   if($cf_1232 == 'Miestas - Miestas'){   
    $zone_city_to_city[] = "Zona 5";
   }
   if($cf_1236 == 'Miestas - Miestas'){   
    $zone_city_to_city[] = "Zona 6";
   }
   if($cf_1240 == 'Miestas - Miestas'){      
    $zone_city_to_city[] = "Zona 7";  
   }
   if($cf_1244 == 'Miestas - Miestas'){   
    $zone_city_to_city[] = "Zona 8";
   }
   if($cf_1248 == 'Miestas - Miestas'){   
    $zone_city_to_city[] = "Zona 9";
   }
   if($cf_1252 == 'Miestas - Miestas'){   
    $zone_city_to_city[] = "Zona 10";
   }

   if($cf_1216 == 'Miestas - Rajonas'){
     $zone_city_to_district[] = "Zona 1";
   }
   if($cf_1220 == 'Miestas - Rajonas'){
      $zone_city_to_district[] = "Zona 2";
   }
   if($cf_1224 == 'Miestas - Rajonas'){   
      $zone_city_to_district[] = "Zona 3";
   }
   if($cf_1228 == 'Miestas - Rajonas'){   
      $zone_city_to_district[] = "Zona 4";
   }
   if($cf_1232 == 'Miestas - Rajonas'){   
     $zone_city_to_district[] = "Zona 5";
   }
   if($cf_1236 == 'Miestas - Rajonas'){   
      $zone_city_to_district[] = "Zona 6";
   }
   if($cf_1240 == 'Miestas - Rajonas'){   
       $zone_city_to_district[] = "Zona 7";   
   }
   if($cf_1244 == 'Miestas - Rajonas'){   
      $zone_city_to_district[] = "Zona 8";
   }
   if($cf_1248 == 'Miestas - Rajonas'){   
     $zone_city_to_district[] = "Zona 9";
   }
   if($cf_1252 == 'Miestas - Rajonas'){   
    $zone_city_to_district[] = "Zona 10";
  }
   
    if($cf_1216 == 'Miestas - Geozona'){
     $zone_city_to_geozone[] = "Zona 1";
   }
   if($cf_1220 == 'Miestas - Geozona'){
     $zone_city_to_geozone[] = "Zona 2";
   }
   if($cf_1224 == 'Miestas - Geozona'){   
     $zone_city_to_geozone[] = "Zona 3";
   }
   if($cf_1228 == 'Miestas - Geozona'){   
     $zone_city_to_geozone[] = "Zona 4";
   }
   if($cf_1232 == 'Miestas - Geozona'){   
     $zone_city_to_geozone[] = "Zona 5";
   }
   if($cf_1236 == 'Miestas - Geozona'){   
     $zone_city_to_geozone[] = "Zona 6";
   }
   if($cf_1240 == 'Miestas - Geozona'){   
      $zone_city_to_geozone[] = "Zona 7";     
   }
   if($cf_1244 == 'Miestas - Geozona'){   
     $zone_city_to_geozone[] = "Zona 8";
   }
   if($cf_1248 == 'Miestas - Geozona'){   
     $zone_city_to_geozone[] = "Zona 9";   
    }
  if($cf_1252 == 'Miestas - Geozona'){   
    $zone_city_to_geozone[] = "Zona 10";   
    }
  
    if($cf_1216 == 'Rajonas - Miestas'){
    $zone_district_to_city[] = "Zona 1";
   }
   if($cf_1220 == 'Rajonas - Miestas'){
    $zone_district_to_city[] = "Zona 2";
   }
   if($cf_1224 == 'Rajonas - Miestas'){   
    $zone_district_to_city[] = "Zona 3";
   }
   if($cf_1228 == 'Rajonas - Miestas'){   
    $zone_district_to_city[] = "Zona 4";
   }
   if($cf_1232 == 'Rajonas - Miestas'){   
    $zone_district_to_city[] = "Zona 5";
   }
   if($cf_1236 == 'Rajonas - Miestas'){   
    $zone_district_to_city[] = "Zona 6";
   }
   if($cf_1240 == 'Rajonas - Miestas'){   
     $zone_district_to_city[] = "Zona 7";   
   }
   if($cf_1244 == 'Rajonas - Miestas'){   
    $zone_district_to_city[] = "Zona 8";
   }
   if($cf_1248 == 'Rajonas - Miestas'){   
    $zone_district_to_city[] = "Zona 9";
   }
   if($cf_1252 == 'Rajonas - Miestas'){   
    $zone_district_to_city[] = "Zona 10";
   }
   
    if($cf_1216 == 'Geozona - Miestas'){
    $zone_geozone_to_city[] = "Zona 1";
   }
   if($cf_1220 == 'Geozona - Miestas'){
    $zone_geozone_to_city[] = "Zona 2";
   }
   if($cf_1224 == 'Geozona - Miestas'){   
    $zone_geozone_to_city[] = "Zona 3";
   }
   if($cf_1228 == 'Geozona - Miestas'){   
    $zone_geozone_to_city[] = "Zona 4";
   }
   if($cf_1232 == 'Geozona - Miestas'){   
    $zone_geozone_to_city[] = "Zona 5";
   }
   if($cf_1236 == 'Geozona - Miestas'){   
    $zone_geozone_to_city[] = "Zona 6";
   }
   if($cf_1240 == 'Geozona - Miestas'){   
      $zone_geozone_to_city[] = "Zona 7";   
   }
   if($cf_1244 == 'Geozona - Miestas'){   
    $zone_geozone_to_city[] = "Zona 8";
   }
   if($cf_1248 == 'Geozona - Miestas'){   
    $zone_geozone_to_city[] = "Zona 9";
   }  
   if($cf_1252 == 'Geozona - Miestas'){   
    $zone_geozone_to_city[] = "Zona 10";
   }
   
    if($cf_1216 == 'Rajonas - Rajonas'){
     $zone_district_to_district[] = "Zona 1";
   }
   if($cf_1220 == 'Rajonas - Rajonas'){
     $zone_district_to_district[] = "Zona 2";
   }
   if($cf_1224 == 'Rajonas - Rajonas'){   
     $zone_district_to_district[] = "Zona 3";
   }
   if($cf_1228 == 'Rajonas - Rajonas'){   
     $zone_district_to_district[] = "Zona 4";
   }
   if($cf_1232 == 'Rajonas - Rajonas'){   
     $zone_district_to_district[] = "Zona 5";
   }
   if($cf_1236 == 'Rajonas - Rajonas'){   
     $zone_district_to_district[] = "Zona 6";
   }
   if($cf_1240 == 'Rajonas - Rajonas'){   
     $zone_district_to_district[] = "Zona 7";      
   }
   if($cf_1244 == 'Rajonas - Rajonas'){   
     $zone_district_to_district[] = "Zona 8";
   }
   if($cf_1248 == 'Rajonas - Rajonas'){   
     $zone_district_to_district[] = "Zona 9";
   }
  if($cf_1252 == 'Rajonas - Rajonas'){   
    $zone_district_to_district[] = "Zona 10";
  }
   
    if($cf_1216 == 'LTU - Miestas'){
        $zone_ltu_to_city[] = "Zona 1";
   }
   if($cf_1220 == 'LTU - Miestas'){
        $zone_ltu_to_city[] = "Zona 2";
   }
   if($cf_1224 == 'LTU - Miestas'){   
        $zone_ltu_to_city[] = "Zona 3";
   }
   if($cf_1228 == 'LTU - Miestas'){   
        $zone_ltu_to_city[] = "Zona 4";
   }
   if($cf_1232 == 'LTU - Miestas'){   
        $zone_ltu_to_city[] = "Zona 5";
   }
   if($cf_1236 == 'LTU - Miestas'){   
        $zone_ltu_to_city[] = "Zona 6";
   }
   if($cf_1240 == 'LTU - Miestas'){   
        $zone_ltu_to_city[] = "Zona 7"; 
   }
   if($cf_1244 == 'LTU - Miestas'){   
        $zone_ltu_to_city[] = "Zona 8";
   }
   if($cf_1248 == 'LTU - Miestas'){   
        $zone_ltu_to_city[] = "Zona 9";
   }
   if($cf_1252 == 'LTU - Miestas'){   
    $zone_ltu_to_city[] = "Zona 10";
}

if($cf_1216 == 'Miestas - LTU'){
  $zone_city_to_ltu[] = "Zona 1";
}
if($cf_1220 == 'Miestas - LTU'){
  $zone_city_to_ltu[] = "Zona 2";
}
if($cf_1224 == 'Miestas - LTU'){   
  $zone_city_to_ltu[] = "Zona 3";
}
if($cf_1228 == 'Miestas - LTU'){   
  $zone_city_to_ltu[] = "Zona 4";
}
if($cf_1232 == 'Miestas - LTU'){   
  $zone_city_to_ltu[] = "Zona 5";
}
if($cf_1236 == 'Miestas - LTU'){   
  $zone_city_to_ltu[] = "Zona 6";
}
if($cf_1240 == 'Miestas - LTU'){   
  $zone_city_to_ltu[] = "Zona 7"; 
}
if($cf_1244 == 'Miestas - LTU'){   
  $zone_city_to_ltu[] = "Zona 8";
}
if($cf_1248 == 'Miestas - LTU'){   
  $zone_city_to_ltu[] = "Zona 9";
}
if($cf_1252 == 'Miestas - LTU'){   
$zone_city_to_ltu[] = "Zona 10";
}

if($cf_1216 == 'Atstumas'){
  $zone_distance[] = "Zona 1";
}
if($cf_1220 == 'Atstumas'){
  $zone_distance[] = "Zona 2";
}
if($cf_1224 == 'Atstumas'){   
  $zone_distance[] = "Zona 3";
}
if($cf_1228 == 'Atstumas'){   
  $zone_distance[] = "Zona 4";
}
if($cf_1232 == 'Atstumas'){   
  $zone_distance[] = "Zona 5";
}
if($cf_1236 == 'Atstumas'){   
  $zone_distance[] = "Zona 6";
}
if($cf_1240 == 'Atstumas'){   
  $zone_distance[] = "Zona 7"; 
}
if($cf_1244 == 'Atstumas'){   
  $zone_distance[] = "Zona 8";
}
if($cf_1248 == 'Atstumas'){   
  $zone_distance[] = "Zona 9";
}
if($cf_1252 == 'Atstumas'){   
$zone_distance[] = "Zona 10";
}

   
   if($cf_1216 == 'LTU - LTU'){
     $zone_ltu_to_ltu[] = "Zona 1";
        }
   if($cf_1220 == 'LTU - LTU'){
     $zone_ltu_to_ltu[] = "Zona 2";
        }
   if($cf_1224 == 'LTU - LTU'){   
     $zone_ltu_to_ltu[] = "Zona 3";
        }
   if($cf_1228 == 'LTU - LTU'){   
     $zone_ltu_to_ltu[] = "Zona 4";
        }
   if($cf_1232 == 'LTU - LTU'){   
     $zone_ltu_to_ltu[] = "Zona 5";
        }
   if($cf_1236 == 'LTU - LTU'){   
     $zone_ltu_to_ltu[] = "Zona 6";
        }
   if($cf_1240 == 'LTU - LTU'){   
     $zone_ltu_to_ltu[] = "Zona 7"; 
        }
   if($cf_1244 == 'LTU - LTU'){   
     $zone_ltu_to_ltu[] = "Zona 8";
        }
   if($cf_1248 == 'LTU - LTU'){   
     $zone_ltu_to_ltu[] = "Zona 9";
  }
  if($cf_1252 == 'LTU - LTU'){   
    $zone_ltu_to_ltu[] = "Zona 10";
 }


  $zones_list = [
    "city_to_city" => $zone_city_to_city,   
    "city_to_district" => $zone_city_to_district, 
    "city_to_geozone" => $zone_city_to_geozone,
    "district_to_city" => $zone_district_to_city,
    "geozone_to_city" => $zone_geozone_to_city,
    "district_to_district" => $zone_district_to_district,
    "ltu_to_city" => $zone_ltu_to_city,
    "ltu_to_ltu" => $zone_ltu_to_ltu,
    "distance" => $zone_distance,
    'city_to_ltu' => $zone_city_to_ltu 
  ];


  array_push($zones, $zones_list);
}

http_response_code(200);

echo json_encode($zones);
}