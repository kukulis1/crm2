<?php

require_once "locations.php";  


function countriesZones($conn, $direct_location,$direct_location_adr, $met,$country_from,$country_to,$post_from,$post_to,$locations,$distance,$get_price_query_string, $from, $to){

   include "countys.php"; // Iterpiamas apskriciu sudarymo scriptas

    // NOTE Patikrinamos pasikrovimo ir issikrovimo salys
    if($country_from == 'LTU' AND $country_to == 'LTU'){
        // Suformuojami rajonai
        $post_from["state"] = str_replace(" r. sav.", " r.", $post_from["state"]);
        $post_from["state"] = str_replace(" m. sav.", " r.", $post_from["state"]);
        $post_from["state"] = str_replace(" sav.", " r.", $post_from["state"]);

        $post_to["state"] = str_replace(" r. sav.", " r.", $post_to["state"]);
        $post_to["state"] = str_replace(" m. sav.", " r.", $post_to["state"]);
        $post_to["state"] = str_replace(" sav.", " r.", $post_to["state"]);
       
        // Suformuojamos kombinacijos
       $location = [
          'mm' => ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_customer"]).":",
          'rr' => ":".trim($post_from["state"])." ".trim($post_to["state"]).":",                          
          'ra' => ":".trim($post_from["state"])." ".trim($county_to).":",
          'ma' => ":".trim($post_from["zone_customer"])." ".trim($county_to).":",
          'ar' => ":".trim($county_from)." ".trim($post_to["state"]).":",
          'mr' => ":".trim($post_from["city"])." ".trim($post_to["state"]).":",
          'rm' => ":".trim($post_from["state"])." ".trim($post_to["zone_customer"]).":",
          'mg' => ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":",
          'gm' => ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":",
          'am' => ":".trim($county_from)." ".trim($post_to["zone_customer"]).":",
          'ar' => ":".trim($county_from)." ".trim($post_to["state"]).":",
          'lm' => ":LTU ".trim($post_to["zone_customer"]).":",
          'rl' => ":".trim($post_to["state"])." LTU:",
          'ml' => ":".trim($post_from["zone_customer"])." LTU:",
          'lr' => ":LTU ".trim($post_to["state"]).":",
          'ltu' => ":LTU LTU:",
          'ats' => $distance
       ];
        
      // I abi puses nustatoma false
      $sideToSide = false;  
      
      // Siunciami galutiniai duomenys i kainos skaivimo algoritma
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LTU");  

       // I abi puses nustatoma true
      $sideToSide = true;
     
      if(empty($get_price)){       
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LTU"); 
      }


    }elseif($country_from == 'LTU' AND ($country_to == 'LVA' OR $country_to == 'LV')){

        $post_from["state"] = str_replace(" r. sav.", " r.", $post_from["state"]);  
        $post_from["state"] = str_replace(" m. sav.", " r.", $post_from["state"]);
        $post_from["state"] = str_replace(" sav.", " r.", $post_from["state"]);
        

        $LV_post_to_district = $post_to["zone_customer"]; 
        $LV_post_to_district = $LV_post_to_district." r.";



        foreach($locations as $key => $loc){
          if (strpos($loc, '(LV)') !== false) {         
            $loc = str_replace(" (LV)","",$loc);         
          }
          $locations[$key] = $loc;
        }

        // City to City
        $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_customer"]).":";
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");  

        if(empty($get_price)){
          $location = ":".trim($post_from["city"])." ".trim($post_to["zone_customer"]).":";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");  
        }
        if(empty($get_price)){
          $location = ":".trim($post_from["state"])." ".trim($post_to["zone_customer"]).":";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");  
        }

        // City to Geozone 
        if(empty($get_price)){
          $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":";
          $get_pric = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }

        //  Geozone to City
        if(empty($get_price)){
          $location = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }

      
        // // District to Geozone
        if(empty($get_price)){
          $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }


        // // Geozone to District /
        if(empty($get_price)){
          $location = ":".trim($post_from["zone_base"])." ".trim($LV_post_to_district).":";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }

        // //  City to District
        if(empty($get_price)){
          $location = ":".trim($post_from["zone_customer"])." ".trim($LV_post_to_district).":";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }


        // // LTU to city
        if(empty($get_price)){
          $location = ":LTU ".trim($post_to["zone_customer"]).":";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }

        if(empty($get_price)){
          $location = ":".trim($post_from["zone_customer"])." LVA:";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }


        if(empty($get_price)){
          $location = ":".trim($post_from["city"])." LVA:";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");  
        }
        if(empty($get_price)){
          $location = ":".trim($post_from["state"])." LVA:";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");  
        }
  

        if(empty($get_price)){
          $location = ":LTU LVA:";
          $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }

        if(empty($get_price)){
          $get_price = checkZone($distance,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
        }     
        
        if(empty($get_price)){
          $get_price = Array();
        }

       

    }elseif(($country_from == 'LVA' OR $country_from == 'LV') AND $country_to == 'LTU'){
      $LV_post_from_district = $post_from["zone_customer"]; 
      $LV_post_from_district = $LV_post_from_district." r.";
    
        $post_to["state"] = str_replace(" r. sav.", " r.", $post_to["state"]);       
        $post_to["state"] = str_replace(" m. sav.", " r.", $post_to["state"]);
        $post_to["state"] = str_replace("  sav.", " r.", $post_to["state"]);

      foreach($locations as $key => $loc){
        if (strpos($loc, '(LV)') !== false) {         
          $loc = str_replace(" (LV)","",$loc);       
        }
        $locations[$key] = $loc;
      }

      // City to City
      $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_customer"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");  

    if(empty($get_price)){
      $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["city"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");  
    }
    if(empty($get_price)){
      $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["state"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");  
    }

    // City to Geozone 
    if(empty($get_price)){
      $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

      //  Geozone to City
    if(empty($get_price)){
      $location = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

    // District to City /
    if(empty($get_price)){
      $location = ":".trim($LV_post_from_district)." ".trim($post_to["zone_customer"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

    // District to Geozone
    if(empty($get_price)){
      $location = ":".trim($LV_post_from_district)." ".trim($post_to["zone_base"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

    // Geozone to District /
    if(empty($get_price)){
      $location = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

    // city to LTU
    if(empty($get_price)){
      $location = ":".trim($post_from["zone_customer"])." LTU:";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

    if(empty($get_price)){
      $location = ":LVA ".trim($post_to["zone_customer"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");  
    }

    if(empty($get_price)){
      $location = ":LVA ".trim($post_to["city"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");  
    }
    if(empty($get_price)){
      $location = ":LVA ".trim($post_to["state"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");  
    }

    if(empty($get_price)){
      $location = ":LVA LTU:";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

    if(empty($get_price)){
      $get_price = checkZone($distance,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide, "LVA");
    }

    if(empty($get_price)){
      $get_price = Array();
    }


    }elseif(($country_from == 'LVA' OR $country_from == 'LV') AND ($country_to == 'LVA' OR $country_to == 'LV')){

      $LV_post_from_district = $post_from["zone_customer"]; 
      $LV_post_from_district = $LV_post_from_district." r.";

      $LV_post_to_district = $post_to["zone_customer"]; 
      $LV_post_to_district = $LV_post_to_district." r.";


      foreach($locations as $key => $loc){
        if (strpos($loc, '(LV)') !== false) {         
          $loc = str_replace(" (LV)","",$loc);       
        }
        $locations[$key] = $loc;
      }

      // City to City
      $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_customer"]).":";
      $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");  

      // City to Geozone 
      if(empty($get_price)){
        $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":";
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
      }

      //  Geozone to City
      if(empty($get_price)){
        $location = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":";
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
      }

      // District to City /
      if(empty($get_price)){
        $location = ":".trim($LV_post_from_district)." ".trim($post_to["zone_customer"]).":";
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
      }

      // District to Geozone
      if(empty($get_price)){
        $location = ":".trim($LV_post_from_district)." ".trim($post_to["zone_base"]).":";
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
      }


      // Geozone to District 
      if(empty($get_price)){
        $location = ":".trim($post_from["zone_base"])." ".trim($LV_post_to_district).":";
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
      }

      //  City to District
      if(empty($get_price)){
        $location = ":".trim($post_from["zone_customer"])." ".trim($LV_post_to_district).":";
        $get_price = checkZone($location,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
      }

      if(empty($get_price)){
        $get_price = checkZone($distance,$locations,$get_price_query_string, $conn,$met,$direct_location,$direct_location_adr, $from, $to, $sideToSide,"LVA");
      }

      if(empty($get_price)){
        $get_price = Array();
      }

    }



  return $get_price;

}
