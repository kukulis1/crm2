<?php
require_once "../config.inc.php";
require_once "countries.php";
// error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
// ini_set('display_errors', 0);


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}



if($_POST['distance'] == 100){
  $_POST['distance'] = $_POST['distance'] - 1;
}elseif($_POST['distance'] == 101){
  $_POST['distance'] = $_POST['distance'] - 2; 
}


$distance = $_POST['distance'];



if(is_numeric($_POST['fromPost'])){
  $from = $_POST["fromPost"];
}

if(is_numeric($_POST['toPost'])){
  $to = $_POST["toPost"];
}

if(is_numeric($_POST['toPost'])){
  $pll = $_POST["pll"];
}

$weight = $_POST['cargoKg'];
$volume = $_POST['cargoVolume'];
$square = $_POST['cargoSquare'];
$country_from = $_POST['from_country'];
$country_to = $_POST['to_country'];

// $country_from = 'LTU';
// $country_to = 'LTU';

// $id = '4367';

// $from = '02189'; // Vilnius
// $from = '52181'; // Kaunas

// // $to = '02190'; // Vilnius 
// $to = '52181'; // Kaunas


// $from = '08403';
// $to = '08403';


// $weight = 4152;
// $volume = 1.92;
// $square = 0.96;
// $meters = 2;
// $distance = 101;
// $pll = 1;

$realPll = $pll;
$pll = false;
   


        //BAZINIS   
          $takePrice = takePrice($conn,$from,$to,$weight,$volume,$square,$pll,$byVolume = false, $bySquare = false,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);
          if($takePrice['price'] != 0 AND ($volume == 0  || ($takePrice['max_volume'] == 0 && $takePrice['max_square'] == 0) ? true : $takePrice['max_volume'] >= $volume)){
            $price = $takePrice;
          }else{  
              $volumePrice = takePrice($conn,$from,$to,$weight,$volume,  $square,$pll, $byVolume = true, $bySquare = false,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);  
            if($volumePrice['price'] != 0){
              $price = $volumePrice;
            }else{
              $squerePrice = takePrice($conn,$from,$to,$weight,$volume,$square,  $pll,$byVolume = false, $bySquare = true,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);
              if($squerePrice['price'] != 0){                
                $price = $squerePrice;  
              }else{           
                  $pll = $realPll;
                  $pllPrice = takePrice($conn,$from,$to,$weight,$volume,$square, $pll,$byVolume = false, $bySquare = false,$byMeter = false, $bazinis = true,$distance, $country_from, $country_to);               
                  $price = $pllPrice;                                
              }        
            }
          }          
  

          
// echo "<pre>";
//         print_R($price);  
// echo "</pre>";

echo json_encode($price);



function takePrice($conn,$from,$to, $weight, $volume, $square, $pll,  $byVolume, $bySquare,$byMeter, $bazinis, $distance, $country_from, $country_to){


  if($country_from == 'LTU' OR $country_from == 'LT'){
    $post_from = mysqli_fetch_assoc($conn->query("SELECT `zone_customer`,`zone_base`, `city`, `state` FROM `crm_post_codes` WHERE `post_code` = '$from'"));
  }elseif($country_from == 'LVA' OR $country_from == 'LV'){
    $post_from = mysqli_fetch_assoc($conn->query("SELECT zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $from BETWEEN code_from AND code_to"));
  }

  if($country_to == 'LTU' OR $country_to == 'LT'){
    $post_to = mysqli_fetch_assoc($conn->query("SELECT  `zone_customer`,`zone_base`, `city`, `state` FROM `crm_post_codes` WHERE `post_code` = '$to'"));
  }elseif($country_to == 'LVA' OR $country_to == 'LV'){
    $post_to = mysqli_fetch_assoc($conn->query("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $to BETWEEN code_from AND code_to"));
  }

if(!empty($post_from) AND !empty($post_to)){

      if($pll){
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = 10965  AND 
                                          $pll = `vtiger_products`.`pll_pcs` ";
      }elseif($byVolume){
            
            $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                    LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                    LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                    LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                    LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                    WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = 10965 AND 
                                    $volume BETWEEN `vtiger_products`.`min_volume_m3`  AND `vtiger_products`.`max_volume_m3`"; 
      
      }elseif($bySquare){
      
            $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0  AND `vtiger_pricebook`.`pricebookid` = 10965  AND 
                                          $square BETWEEN `vtiger_products`.`min_square_m2`  AND `vtiger_products`.`max_square_m2`"; 
      }else{
    
            $get_price_query_string_bazinis = "SELECT * FROM `vtiger_pricebook`
                                              LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                              LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                              LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                              LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                              WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = 10965 AND
                                              $weight BETWEEN `vtiger_products`.`min_weight_kg`  AND `vtiger_products`.`max_weight_kg` ";                             
      }
    

    $met = 'mysqli';   
      
        require_once "base_catalog.php";
      
        $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_customer"]).":"; 
        $get_price = basecatalog($location,$get_price_query_string_bazinis, $conn,$met);
      
        $location2 = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_base"]).":"; 
        $get_price_base = basecatalog($location2,$get_price_query_string_bazinis, $conn,$met);
      
        $location3 = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":"; 
        $get_price_base2 = basecatalog($location3,$get_price_query_string_bazinis, $conn,$met);  
      
        $location4 = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":"; 
        $get_price_base3 = basecatalog($location4,$get_price_query_string_bazinis, $conn,$met);  
      
        $get_base = array_merge($get_price, $get_price_base, $get_price_base2,$get_price_base3);
      
          foreach($get_base as $get_prices){           
              $get =  $get_prices; 
              $result = array( 
                "price" => (float)$get_prices["listprice"],       
                'pricebook' => "BAZINISLT ". $get_prices["productname"],               
                "max_weight" => $get_prices['max_weight_kg'],
                "min_weight" => $get_prices['min_weight_kg'],                
                "max_volume" => $get_prices['max_volume_m3'],
                "min_volume" => $get_prices['min_volume_m3'],               
                "max_square" => $get_prices['max_square_m2'],
                "min_square" => $get_prices['min_square_m2'],
                "combination" => "BAZINISLT ".$get_prices["productname"],
                'stevedoring' => (float)$get_prices['cf_954'],
                "consignee" =>  $get_prices["consignee"],
                "pll" => $get_prices["pll_pcs"]
              );                     
              break;
          }
      
      
}
  if(!empty($get)){
    $rez =  $result;   
  }else{
    if(empty($post_from['zone_base']) AND empty($post_to['zone_base'])){
      $rez =  array('price' => 0,  'combination' => 'Blogi pašto kodai', 'max_volume' => 0);
    }elseif(empty($post_from['zone_base'])){
      $rez =  array('price' => 0,  'combination' => 'Blogas pakrovimo pašto kodas', 'max_volume' => 0);
    }elseif(empty($post_to['zone_base'])){
      $rez =  array('price' => 0,  'combination' => 'Blogas iškrovimo pašto kodas', 'max_volume' => 0);
    }else{	
      $rez =  array('price' => 0,  'combination' => "Nėra kombinacijos ".$post_from['zone_base']." - ".$post_to['zone_base']." arba ".$post_from['zone_customer']." - ".$post_to['zone_customer']."", 'max_volume' => 0);
    }
  }  

  return  $rez;
   

}



function multiexplode ($delimiters,$data) {
  $MakeReady = str_replace($delimiters, $delimiters[0], $data);
  $Return    = explode($delimiters[0], $MakeReady);
  return  $Return;
} 
      