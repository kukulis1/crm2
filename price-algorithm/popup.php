<?php
require_once "../config.inc.php";
error_reporting(E_ALL);
ini_set('display_errors', 0);
// if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $locations_string_from = "SELECT DISTINCT * FROM ";
  $locations_string_to = "SELECT DISTINCT * FROM ";
  $zonos = $_POST['whoZone'];
  $col = $_POST['col'];

  $inputs = 1;

  if($_POST["inputs"]) $inputs = $_POST["inputs"];

  if($_POST['change'] && $_POST["inputs"]) $inputs = $_POST["inputs"];


  if($_POST['start']) $start_from = 0; else $start_from = $inputs-1;

  $type_sql = "SELECT cf_1216 AS type FROM vtiger_cf_1216 ORDER BY sortorderid";

  $zona = explode(",",$zonos);

  $locations_string_from_arr = array();
  $locations_string_to_arr = array();

for($v =0; $v < count($zona); $v++){

  if(count($zona) < $inputs) $zona[] = $zona[0];

  if($zona[$v] == "Miestas - Miestas"){
    $locations_string_from_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
    $locations_string_to_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
  }

  if($zona[$v] == "Miestas - Rajonas"){
    $locations_string_from_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
    $locations_string_to_arr[] = " `districts`  ORDER BY district";
  }
  
  if($zona[$v] == "Miestas - Apskritis"){
    $locations_string_from_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
    $locations_string_to_arr[] = " `county`  ORDER BY location";
  }

  if($zona[$v] == "Miestas - Geozona"){
    $locations_string_from_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
    $locations_string_to_arr[] = " `zones` ORDER BY id,location";
  }
  
  if($zona[$v] == "Rajonas - Miestas"){
    $locations_string_from_arr[] =  " `districts` ORDER BY district";
    $locations_string_to_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
  }

  if($zona[$v] == "Rajonas - Apskritis"){
    $locations_string_from_arr[] =  " `districts` ORDER BY district";
    $locations_string_to_arr[] = " `county`  ORDER BY location";
  }

  if($zona[$v] == "Apskritis - Rajonas"){
    $locations_string_from_arr[] =  " `county` ORDER BY location";
    $locations_string_to_arr[] = " `districts`  ORDER BY district";
  }

  if($zona[$v] == "Apskritis - Miestas"){
    $locations_string_from_arr[] =  " `county` ORDER BY location";
    $locations_string_to_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
  }

  if($zona[$v] == "Apskritis - Geozona"){
    $locations_string_from_arr[] =  " `county` ORDER BY location";
    $locations_string_to_arr[] = "  `zones` ORDER BY id,location";
  }

  if($zona[$v] == "Geozona - Miestas"){
    $locations_string_from_arr[] = " `zones` ORDER BY id,location";
    $locations_string_to_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
  }

  if($zona[$v] == "Rajonas - Rajonas"){
    $locations_string_from_arr[] =  " `districts` ORDER BY district";
    $locations_string_to_arr[] =  " `districts` ORDER BY district";
  }

  if($zona[$v] == "Miestas - LTU"){
    $locations_string_from_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 GROUP BY crmid ORDER BY cities_tks_city";
    $locations_string_to_arr[] = " `ltu` ORDER BY id";
  }

  if($zona[$v] == "LTU - Miestas"){
    $locations_string_from_arr[] = " `ltu` ORDER BY id";
    $locations_string_to_arr[] = " `vtiger_cities` LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_cities.citiesid WHERE deleted = 0 ORDER BY cities_tks_city";
  }

  if($zona[$v] == "Rajonas - LTU"){
    $locations_string_from_arr[] =  " `districts` ORDER BY district";
    $locations_string_to_arr[] = " `ltu` ORDER BY id";
  }

  if($zona[$v] == "LTU - Rajonas"){
    $locations_string_from_arr[] = " `ltu` ORDER BY id";
    $locations_string_to_arr[] =  " `districts` ORDER BY district";
  }

  if($zona[$v] == "Geozona - Geozona"){
    $locations_string_from_arr[] = " `zones` ORDER BY id,location";
    $locations_string_to_arr[] = " `zones` ORDER BY id,location";
  }  

  if($zona[$v] == "LTU - LTU"){
    $locations_string_from_arr[] = " `ltu` ORDER BY id";
    $locations_string_to_arr[] = " `ltu` ORDER BY id";
  }  

  if($zona[$v] == "Atstumas"){
    $locations_string_from_arr[] = " `ltu` ORDER BY id";
    $locations_string_to_arr[] = " `ltu` ORDER BY id";
  }  
}


  $locations_from = array();
  $locations_to = array();
  $types = array();

  for($j = 0; $j < $inputs; $j++){
    $from = $locations_string_from.$locations_string_from_arr[$j];  
    $to = $locations_string_to.$locations_string_to_arr[$j];
    $locations_from[$j] = $conn->query($from); 
    $locations_to[$j] = $conn->query($to);
    $types[$j] = $conn->query($type_sql);
  }



for($i = $start_from; $i < $inputs; $i++){
  $h = $i;
  if($i > 0) $h = $i;
  $g = $i;
  for($m = 0; $m <= $inputs; $m++){
    if($i > $m) $g = $g +1; 
  }

  if(count($zona) < $inputs) $zona[] = $zona[0];


  $e = $g +1;
  $b = $i;


  if($_POST['ids']) $b = $_POST['ids'];  
  if($_POST['ids']) $g = $e +1;
  if($_POST['ids']) $e = $g +1;



  if(!$_POST['start']) $res = ''; 

  if(!$_POST['change']) $res .='<tr data-append>';   

  if($zona[$i] == 'Atstumas'){ 

      $res .='
      <td>
        <select class="form-control except" onchange="changeFieldType(event)">
          <option>Pasirinkite</option>';
          while($type = mysqli_fetch_array($types[$h])):
            $res .= '<option '.($zona[$h] == $type['type'] ? 'selected' : '').' value="'.$type['type'].'">'.$type['type'].'</option>';
          endwhile;
        $res .= '</select>
          </td>        
        <td><input id="number_inp'.$i.'" class="inputElement form-control inputas type" type="number" data-np-checked="1"><td>
        <td></td>
        <td></td>
        <td><button onclick="deleteRow(event);" type="button" class="btn delete_field_btn" style="background: transparent;outline: 0; padding: 0;"> <i class="fa fa-trash" style="cursor: pointer;"></i></button></td>';
  }else{

    $res .='<td>
        <select class="form-control except" onchange="changeFieldType(event)">
          <option>Pasirinkite</option>';
          while($type = mysqli_fetch_array($types[$h])):
            $res .= '<option '.($zona[$h] == $type['type'] ? 'selected' : '').' value="'.$type['type'].'">'.$type['type'].'</option>';
          endwhile;
        $res .= '</select>
          </td>
        <td>
        <select class="form-control selectas type" data-nr="'.$g.'" name="loc'.$g.'">
          <option>Pasirinkite</option>';
        
          while($loc = mysqli_fetch_array($locations_from[$h])):         
            if($zona[$h] == "Miestas - Miestas" || $zona[$h] == "Miestas - Rajonas" || $zona[$h] == "Miestas - Apskritis"  || $zona[$h] == "Miestas - Geozona" || $zona[$h] == "Miestas - LTU"){
            $res .= '<option value="'.$loc["cities_tks_city"].'">'.$loc["cities_tks_city"].'</option>';
            }elseif($zona[$h] == "Rajonas - Miestas" || $zona[$h] == "Rajonas - Apskritis" || $zona[$h] == "Rajonas - Rajonas" || $zona[$h] == "Rajonas - LTU"){
              $res .= '<option value="'.$loc["district"].'">'.$loc["district"].'</option>';
            }else{
              $res .= '<option value="'.$loc["location"].'">'.$loc["location"].'</option>';
            }
          endwhile;
          $res .= '
        </select>
      </td>
    <td>
    <select class="form-control selectas type" data-nr="'.$e.'" name="loc'.$e.'">';
      $res .=' <option>Pasirinkite</option>';
        while($loc = mysqli_fetch_array($locations_to[$h])):       
          if($zona[$h] == "Miestas - Miestas" || $zona[$h] == "Rajonas - Miestas" || $zona[$h] == "Apskritis - Miestas"  || $zona[$h] == "Geozona - Miestas" || $zona[$h] == "LTU - Miestas"){
            $res .= '<option value="'.$loc["cities_tks_city"].'">'.$loc["cities_tks_city"].'</option>';
          }elseif($zona[$h] == "Miestas - Rajonas" || $zona[$h] == "Rajonas - Rajonas" || $zona[$h] == "Apskritis - Rajonas" || $zona[$h] == "LTU - Rajonas"){
            $res .= '<option value="'.$loc["district"].'">'.$loc["district"].'</option>';
          }else{
            $res .= '<option value="'.$loc["location"].'">'.$loc["location"].'</option>';
          }
        endwhile;  
        $res .= '
      </select>
      </td>
      <td><input type="checkbox" data-col="'.$col.'" onclick="directPostCodesCheckBox(event,'.$b.');" title="Tik į nurodytus pašto kodus"></td>
      <td><input type="checkbox" data-column="'.$col.'" onclick="directAddressCheckBox(event,'.$b.');" title="Tik į konkretų adresą"></td>
      <td><button onclick="deleteRow(event);" type="button" class="btn delete_field_btn" style="background: transparent;outline: 0; padding: 0;"> <i class="fa fa-trash" style="cursor: pointer;"></i></button></td>';
  }
    if(!$_POST['change']) $res .='</tr>';
}


if(empty($zona)){
  $responce = null;
}else{
  $responce = [ 
    "res" => $res
  ];
}

// echo "<pre>";
//   echo $res;
// echo "</pre>";

echo json_encode($responce);

// }