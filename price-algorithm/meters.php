<?php
require_once "../config.inc.php";
require_once "countries.php";
// error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
// ini_set('display_errors', 0);
if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

if(is_numeric($_POST["accId"])){
  $id = $_POST["accId"];
}


if(is_numeric($_POST['fromPost'])){
  $from = $_POST["fromPost"];
}

if(is_numeric($_POST['toPost'])){
  $to = $_POST["toPost"];
}

$meter = $_POST['meters'];

$country_from = $_POST['from_country'];
$country_to = $_POST['to_country'];

// $country_from = 'LTU';
// $country_to = 'LTU';

// $id = '95183';


// $to = '02190';
// $from = '02189';

   

$takePrice = takePrice($conn,$from,$to,$id,$meter, $bazinis = false, $country_from, $country_to);

if($takePrice['price'] != 0){
  $price = $takePrice;
}else{
  $takePrice = takePrice($conn,$from,$to,$id,$meter, $bazinis = true, $country_from, $country_to);
  $price = $takePrice;
}

     

echo json_encode($price);

function takePrice($conn,$from,$to, $id, $meter, $bazinis, $country_from, $country_to){

  if($country_from == 'LTU' OR $country_from == 'LT'){
    $post_from = mysqli_fetch_assoc($conn->query("SELECT `zone_customer`,`zone_base`, `city`, `state` FROM `crm_post_codes` WHERE `post_code` = '$from'"));
  }elseif($country_from == 'LVA' OR $country_from == 'LV'){
    $post_from = mysqli_fetch_assoc($conn->query("SELECT zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $from BETWEEN code_from AND code_to"));
  }

  if($country_to == 'LTU' OR $country_to == 'LT'){
    $post_to = mysqli_fetch_assoc($conn->query("SELECT  `zone_customer`,`zone_base`, `city`, `state` FROM `crm_post_codes` WHERE `post_code` = '$to'"));
  }elseif($country_to == 'LVA' OR $country_to == 'LV'){
    $post_to = mysqli_fetch_assoc($conn->query("SELECT  zone_customer, zone_base FROM `crm_post_codes_lv` WHERE $to BETWEEN code_from AND code_to"));
  }

  $locations =  mysqli_fetch_assoc($conn->query("SELECT * FROM `vtiger_accountscf`
                                                          LEFT JOIN `vtiger_account` on `vtiger_accountscf`.`accountid` = `vtiger_account`.`accountid`
                                                          LEFT JOIN crm_directions ON `crm_directions`.`directionid`=`vtiger_accountscf`.`accountid`
                                                          WHERE `vtiger_accountscf`.`accountid` = ".$id));

    $direct_location = Array();
    $columns = Array('1218','1222','1226','1230','1234','1238','1242','1246','1250','1254','1400','1404','1408','1412','1416','1420','1424','1428','1432','1436');

    $b = 0;

    foreach($columns AS $col){
      if(isset($locations["post_cf_".$col])){
        $slashExplode = explode("/",$locations["post_cf_".$col]);        
        for($f =0; count($slashExplode) > $f; $f++){
         $equalsExplode[$col."_".$f] = explode("=", $slashExplode[$f]);
        }      
        for($e =0; count($slashExplode) > $e; $e++){          
            $post_cf_[$col][$e] = multiexplode(array(";","-"), $equalsExplode[$col."_".$e][1]);          
            if(!empty($equalsExplode[$col."_".$e][0])){         
              $direct_location[$col][$e] = array('num' => $equalsExplode[$col."_".$e][0], 'code' => $post_cf_[$col][$e]);      
            }  
        }
      }
      $b++;
    } 

if(!empty($post_from) AND !empty($post_to)){

    if(!$bazinis){
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = ".$locations["pricebook"]."  AND 
                                          $meter BETWEEN `vtiger_productcf`.`cf_1637`  AND `vtiger_productcf`.`cf_1639`"; 

    
    }else{     
        $get_price_query_string = "SELECT * FROM `vtiger_pricebook`
                                          LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                                          LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                                          LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                                          WHERE `vtiger_crmentity`.`deleted` = 0 AND  `vtiger_pricebook`.`pricebookid` = 10965  AND 
                                          $meter BETWEEN `vtiger_productcf`.`cf_1637`  AND `vtiger_productcf`.`cf_1639`";                                     

    }


    


 
    $direct_location_adr = Array();
    $met = 'mysqli';

    if(!$bazinis){
    
      $distance = 0;
      $get_price = countriesZones($conn,$direct_location, $direct_location_adr,$met, $country_from, $country_to, $post_from, $post_to,$locations ,$distance, $get_price_query_string, $from,$to);

      foreach($get_price as $get_prices){        
        $get =  $get_prices;           
      
        $result = array( 
          "price" => (float)$get_prices["listprice"],   
          'pricebook' => $get_prices['productname'],
          "min_weight" => $get_prices['min_weight_kg'],
          "max_weight" => $get_prices['max_weight_kg'],
          "min_volume" => $get_prices['min_volume_m3'],
          "max_volume" => $get_prices['max_volume_m3'],
          "min_square" => $get_prices['min_square_m2'],
          "max_square" => $get_prices['max_square_m2'],
          "combination" =>  $get_prices["productname"],
          "consignee" =>  $get_prices["consignee"],
          'stevedoring' => (float)$get_prices['cf_954'],
          "from" => $post_from["zone_customer"],
          'to' => $post_to["zone_customer"],
          "pll" => $get_prices["pll_pcs"]
        );                              
        break;  
      } 
      
    }else{
      
        require_once "base_catalog.php";
      
        $location = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_customer"]).":"; 
        $get_price = basecatalog($location,$get_price_query_string_bazinis, $conn,$met);
      
        $location2 = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_base"]).":"; 
        $get_price_base = basecatalog($location2,$get_price_query_string_bazinis, $conn,$met);
      
        $location3 = ":".trim($post_from["zone_base"])." ".trim($post_to["zone_customer"]).":"; 
        $get_price_base2 = basecatalog($location3,$get_price_query_string_bazinis, $conn,$met);  
      
        $location4 = ":".trim($post_from["zone_customer"])." ".trim($post_to["zone_base"]).":"; 
        $get_price_base3 = basecatalog($location4,$get_price_query_string_bazinis, $conn,$met);  
      
        $get_base = array_merge($get_price, $get_price_base, $get_price_base2,$get_price_base3);
      
          foreach($get_base as $get_prices){           
              $get =  $get_prices; 
              $result = array( 
                "price" => (float)$get_prices["listprice"],       
                'pricebook' => "BAZINISLT ". $get_prices["productname"],
                "min_weight" => $get_prices['min_weight_kg'],
                "max_weight" => $get_prices['max_weight_kg'],
                "min_volume" => $get_prices['min_volume_m3'],
                "max_volume" => $get_prices['max_volume_m3'],
                "min_square" => $get_prices['min_square_m2'],
                "max_square" => $get_prices['max_square_m2'],
                "combination" => "BAZINISLT ".$get_prices["productname"],
                'stevedoring' => (float)$get_prices['cf_954'],
                "consignee" =>  $get_prices["consignee"],
                "pll" => $get_prices["pll_pcs"]
              );                     
              break;
          }
      
      
    }


}

if(!empty($get)){
  $rez =  $result;   
}else{
	if(empty($post_from['zone_base']) AND empty($post_to['zone_base'])){
		$rez =  array('price' => 0,  'combination' => 'Blogi pašto kodai', 'max_volume' => 0);
	}elseif(empty($post_from['zone_base'])){
 		$rez =  array('price' => 0,  'combination' => 'Blogas pakrovimo pašto kodas', 'max_volume' => 0);
	}elseif(empty($post_to['zone_base'])){
		$rez =  array('price' => 0,  'combination' => 'Blogas iskrovimo pasto kodas', 'max_volume' => 0);
	}else{	
    $rez =  array('price' => 0,  'combination' => "Nėra kombinacijos ".$post_from['zone_base']." - ".$post_to['zone_base']." arba ".$post_from['zone_customer']." - ".$post_to['zone_customer']."", 'max_volume' => 0);
	}
}  

return  $rez;
}   



function multiexplode ($delimiters,$data) {
  $MakeReady = str_replace($delimiters, $delimiters[0], $data);
  $Return    = explode($delimiters[0], $MakeReady);
  return  $Return;
} 
      
?>
