<?php
error_reporting(0);

function checkZone($location,$locations,$get_price_query_string, $conn,$met, $direct_location, $direct_location_adr, $from, $to, $sideToSide,$which) {
  // NOTE Nusistatomi duomenu bazes stulpeliu ir zonu atitikmenys
  $columns = Array('Zona 1' => '1216','Zona 2' => '1220','Zona 3' => '1224','Zona 4' => '1228','Zona 5' => '1232','Zona 6' => '1236','Zona 7' => '1240','Zona 8' => '1244','Zona 9' => '1248','Zona 10' => '1252','Zona 11' => '1398','Zona 12' => '1402','Zona 13' => '1406','Zona 14' => '1410','Zona 15' => '1414','Zona 16' => '1418','Zona 17' => '1422','Zona 18' => '1426','Zona 19' => '1430','Zona 20' => '1434');

  // NOTE  Nusistatomi kainos zonos ir kombinaciju rysiai pagal duomenu bazes stulpelius
  $columns2 = Array('1216' => '1218', '1220' => '1222', '1224' => '1226', '1228' => '1230', '1232' => '1234','1236' => '1238', '1240' => '1242', '1244' => '1246', '1248' => '1250','1252' => '1254','1398' => '1400', '1402' => '1404', '1406' => '1408', '1410' => '1412', '1414' => '1416', '1418' => '1420', '1422' => '1424', '1426' => '1428', '1430' => '1432', '1434' => '1436');

  $cities = array();
  $numbs = array();;
  
  // Nustatomos poros
  $couples = array('mm' => 'Miestas - Miestas',
                   'mr' => 'Miestas - Rajonas', 
                   'ma' => 'Miestas - Apskritis',
                   'mg' => 'Miestas - Geozona', 
                   'ag' => 'Apskritis - Geozona', 
                   'rm' => 'Rajonas - Miestas',
                   'ra' => 'Rajonas - Apskritis',
                   'am' => 'Apskritis - Miestas',
                   'ar' => 'Apskritis - Rajonas',
                   'gm' => 'Geozona - Miestas',
                   'rr' => 'Rajonas - Rajonas',
                   'ml' => 'Miestas - LTU',
                   'lm' => 'LTU - Miestas',
                   'rl' => 'Rajonas - LTU',
                   'lr' => 'LTU - Rajonas',
                   'ats' => 'Atstumas',
                   'ltu' => 'LTU - LTU'
                  );

// Sukam cikla is poru 
foreach($couples AS $ini => $couple){
  $i=1;  // Papildomi ciklas is stulpeliu 
  foreach($columns AS $key => $col){
    // Gaunam kliento korteles zonu tipus, juos pasiverciam i array
    $double_types = explode(",",$locations['cf_'.$col]); 
    // Sukam kliento korteles zonu tipu cikla   
    foreach($double_types AS $dblType){ 
      // Tikrinam kokie zonu tipai yra pas klienta kortelėje
      if($dblType == $couple){
        $numbs[] = $ini.$i; 
        // Isskiriam tipa atstumas
        if($couple == 'Atstumas'){
          $dis = explode(":", trim($locations['cf_'.$columns2[$col]],":"));         
          for($m = 0; $m < count($dis); $m++){
            if(is_numeric($dis[$m])){
              $cities[$ini.$i]['loc'] =  $dis[$m];  
              $cities[$ini.$i]['type'] = 'Atstumas';
            }
          }
          $cities[$ini.$i]['zone'] = $key;
        }else{
          // Surenkam kliento korteles zonas
          $cities[$ini.$i]['loc'] =  $locations['cf_'.$columns2[$col]];       
          $cities[$ini.$i]['zone'] = $key;
          $cities[$ini.$i]['type'] = '';
          $bothSides = $locations['check_'.$columns2[$col]];
        }
        // Pasto kodas i pasto koda ir kombinaciju apsukimo netaikom LTU- LTU ir atstumas tipam
        if($couple != 'LTU - LTU' && $couple != 'Atstumas'){
          // Patikrinam ar yra nustatyti tiesioginiai vaziavimai ir kombinaciju apsukimas
          if(isset($direct_location_adr[$columns2[$col]])){
            $cities[$ini.$i]['loc'] = filterCitiesByPostCodes($locations,$direct_location_adr,$from,$to,$columns2[$col],$bothSides,$sideToSide);   
          }elseif(isset($direct_location[$columns2[$col]])){
            $cities[$ini.$i]['loc'] = filterCitiesByPostCodes($locations,$direct_location,$from,$to,$columns2[$col],$bothSides,$sideToSide);   
          }elseif($bothSides){  
            $cities[$ini.$i]['loc'] = reverseCities($locations,$columns2[$col],$ini);
          }
        }
      }
      if(count($double_types) > 1) $i++;
    }
    $i++;
  }
}

$query_string = array();


if($which == "LTU"){

  $distance = (float)$location['ats'];

   foreach($numbs AS $couple){
    $query = '';
    $query = $get_price_query_string;  
    $key = preg_replace('/\d+/u', '', $couple);
    // Lyginamas uzsakymo atstumas su kliento korteles nustatytu atstumu ir gaunama zona
    if(strpos($cities[$couple]['type'],'Atstumas') !== false){
      if((int)$distance !== 0){              
          if($distance <= (int)$cities[$couple]['loc']){  
            $query_string[] = $query .= " AND `consignee` = '".$cities[$couple]["zone"]."' ORDER BY listprice DESC";
          }        
      }
      // Lyginamas uzsakymas su kliento korteles zonomis
    }elseif(strpos(trim($cities[$couple]['loc']), "$location[$key]") !== false){    
      $query_string[] = $query .= " AND `consignee` = '".$cities[$couple]["zone"]."' ORDER BY listprice DESC";
    }else{
      continue;
    }
  }

}else{
  $distance = (float)$location;

  foreach($numbs AS $couple){
    $query = '';
    $query = $get_price_query_string;  

    if(strpos($cities[$couple]['loc'],'Atstumas') !== false){
      if((int)$distance !== 0){
        for($l = 0; $l < count($cities[$couple]['loc']); $l++){
          if($distance <= (int)$cities[$couple]['loc'][$l]){
            $query_string[] = $query .= " AND `consignee` = '".$cities[$couple]["zone"]."' ORDER BY listprice DESC";
          }
        }
      }
    }elseif(strpos(trim($cities[$couple]['loc']), "$location") !== false){    
      $query_string[] = $query .= " AND `consignee` = '".$cities[$couple]["zone"]."' ORDER BY listprice DESC";
    }else{
      continue;
    }
  }
}


// Isvalom visas gautas kombinacijas paliktimi tik po viena unikalia
$query_string = array_unique($query_string);

// Gaunam ir grazinam kainyno kaina
$get_price = array();
for($j=0; $j < count($query_string); $j++){

  if($met != 'pdo' AND !empty($query_string[$j])){
    $result = $conn->query($query_string[$j]);
    if($result) {
      while($row = $result->fetch_assoc()) {
        $get_price[] = $row;
      }
    }
  }elseif(!empty($query_string[$j])){
    $result = $conn->prepare($query_string[$j]);  
    $result->execute();
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $data = $result->fetchAll();
    if($data) {
      foreach($data as $row) {
        $get_price[] = $row;
      }
    }
  }
}
  return  $get_price;       
}


function reverseCities($locations, $col, $couple){
  $temp = substr($locations['cf_'.$col], 1, -1);
  $temp = explode(":",$temp);
  $temp2 = '';
  $return = $locations['cf_'.$col];
  $temp3 = Array();  
  $i = 1;
    for($r = 0; count($temp) > $r; $r++){
      $temporary = explode(" ",$temp[$r]);
      if($temporary > 2){
        if(in_array($couple,array('ma','ra'))){
           // :Vilnius-0  Marijampolės-1 apskr-2.:
         $temp3[] = array($temporary[1]." ".$temporary[2]." ".$temporary[0]);
        }elseif(in_array($couple,array('ag','am','ar'))){
          // Marijampolės-0 apskr-1 Vilnius-2:
          $temp3[] = array($temporary[2]." ".$temporary[0]." ".$temporary[1]);
        }
      }else{
        $temp3[] = array_reverse($temporary);
      }

      if(!empty($temp3[$r])){
        $temp2 .= $temp3[$r][0]."".$temp3[$r][1].":";  
      }
      $i++;
    }

    $return =  $return.$temp2;
    return $return;
}


function filterCitiesByPostCodes($locations,$direct_location,$from,$to,$col,$bothSides,$sideToSide){ 
  $temp = substr($locations['cf_'.$col], 1, -1);
  $temp = explode(":",$temp);

  if($bothSides && $sideToSide){
    $temp3 = Array();  
    for($r = 0; count($temp) > $r; $r++){
      $temporary = explode(" ",$temp[$r]);
      $temp3[] = array_reverse($temporary);
    }
    $temp = Array();  
    for($r = 0; count($temp3) > $r; $r++){ 
      $temp[] = $temp3[$r][0]." ".$temp3[$r][1];
    }

    $from_temp = $from;
    $to_temp = $to;

    $from = $to_temp;
    $to = $from_temp;
  }

  $numbers = array();
  $check = array();


  for($n = 0; count($direct_location[$col]) > $n; $n++){
    $num = $direct_location[$col][$n]['num'];
    $numbers[$num-1] = $num-1;
    for($h = 0; count($direct_location[$col][$n]['code']) > $h; $h++){
      $explode[][$num] = explode(",",$direct_location[$col][$n]['code'][$h]);       

        for($b = 0; count($explode) > $b; $b++){     

          if(!$bothSides){           
              if(count($explode[$b][$num]) > 2){
                if (in_array($from, $explode[$b][$num])){             
                  $check[] = $num-1;
                }
              }else{                               
                if ($from == $explode[$b][$num][0]){                    
                  $check[] = $num-1;
                }                
              }

              if(count($explode[$b][$num]) > 2){                
                if (in_array($to, $explode[$b][$num])){                    
                  $check[] = $num-1;
                }                  
              }else{                                                       
                if ($to == $explode[$b][$num][1]){                     
                  $check[] = $num-1;
                }              
              }  

          }else{
            if(count($explode[$b][$num]) > 2){
              if (in_array($to, $explode[$b][$num])){             
                $check[] = $num-1;
              }
            }else{                                           
              if ($to == $explode[$b][$num][0]){                    
                $check[] = $num-1;
              }                
            }

            if(count($explode[$b][$num]) > 2){                
              if (in_array($from, $explode[$b][$num])){                    
                $check[] = $num-1;
              }                  
            }else{                                                       
              if ($from == $explode[$b][$num][1]){                     
                $check[] = $num-1;
              }              
            }  
          }
        }         
    }
  } 

  $check = array_unique($check);

  foreach($numbers as $key => $one) {
    for($l=0; count($check) > $l; $l++){
    if(strpos($one, (string)$check[$l]) !== false)
        unset($numbers[$key]);
    }
  }
  foreach($numbers AS $numb){
    unset($temp[$numb]);
  }

  $i=1;
  $temp2 = '';
  foreach($temp AS $key => $temporary){
    $temp2 .= ":".$temp[$key].(count($temp) == $i ? ':' : '');
    $i++;
  }

  return $temp2;  
}
