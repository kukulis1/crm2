<?php

function basecatalog($location,$get_price_query_string, $conn,$met){

  $get_price = [];
  if($met == 'mysqli'){
    $result = $conn->query($get_price_query_string);
    if($result) {
      while($row = $result->fetch_assoc()) {
        if (strpos(trim($row['consignee']), $location) !== false) {
           $get_price[] = $row;
         }
      }
    }  
  }else{
    foreach($get_price_query_string as $row) {
      if (strpos(trim($row['consignee']), $location) !== false) {
        $get_price[] = $row;
      }
    }   
  }  
    
  return  $get_price;  
}