<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $accountid = $_POST['accountid'];

  $getClientCartZones = $conn->query("SELECT f.*,a.accountname 
                                      FROM vtiger_accountscf f 
                                      LEFT JOIN vtiger_account a on a.accountid=f.accountid 
                                      WHERE f.accountid = $accountid");
  
  $getZoneTypes = $conn->query("SELECT cf_1216 as types FROM vtiger_cf_1216");

  $zone_types = Array();

  foreach($getZoneTypes as $all_records){
    $zone_types[] = $all_records['types'];
  }
 
  $records = array(
      'client_zones' => mysqli_fetch_assoc($getClientCartZones), 
      'zone_types' => $zone_types
  );

  
  
  echo  json_encode($records);



}else{
  http_response_code(404);
}


