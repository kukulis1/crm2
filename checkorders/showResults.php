<?php

set_time_limit(500);
if($_POST){
  require_once "../config.inc.php";
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");




if($_POST['show_results']){
  $date = date("Y-m-d");
  $not_in_metrika =  $conn->query("SELECT DISTINCT vtiger_salesorder.salesorderid,vtiger_salesorder.external_order_id,vtiger_salesorder.shipment_code
                                      FROM vtiger_salesorder  
                                      LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid
                                      LEFT JOIN vtiger_crmentity on vtiger_crmentity.crmid=vtiger_salesorder.salesorderid  AND vtiger_crmentity.setype = 'SalesOrder' 
                                      WHERE vtiger_crmentity.deleted = 0 AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$date' AND vtiger_salesorder.source = 'CRM' AND
                                      vtiger_salesorder.external_order_id NOT IN (SELECT app_metrika.id  FROM app_metrika WHERE app_metrika.id = vtiger_salesorder.external_order_id)");

  $order_not_in_crm = $conn->query("SELECT DISTINCT  shipment_code, order_date,load_company_name,unload_company_name,update_metrika FROM app_metrika where id NOT IN (SELECT app_metrika.id FROM app_metrika,vtiger_salesorder WHERE app_metrika.id = vtiger_salesorder.external_order_id)");

  $not_metrika = mysqli_num_rows($not_in_metrika);
  $check_order_not_in_crm = mysqli_num_rows($order_not_in_crm);

  if($not_metrika > 0){
    $orders_not_inserted_to_metrika = array();
    while($row = $not_in_metrika->fetch_assoc()){  
      $orders_not_inserted_to_metrika[] = $row['salesorderid'];  
    }
  

  $list = implode(",",$orders_not_inserted_to_metrika);

  $query3 = "SELECT s.shipment_code,e.createdtime, s.update_date, a.accountname, b.load_company, sh.unload_company FROM vtiger_salesorder s 
                              LEFT JOIN vtiger_account a ON a.accountid=s.accountid
                              LEFT JOIN vtiger_sobillads b ON b.sobilladdressid=s.salesorderid 
                              LEFT JOIN vtiger_soshipads sh ON sh.soshipaddressid=s.salesorderid
                              LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid where s.salesorderid in ($list)";
  $salesOrders = $conn->query($query3);	


  $not_inserted_to_metrika = array();
  while($row = mysqli_fetch_array($salesOrders)){  
    $not_inserted_to_metrika[] = $row;
  }

  }



  $not_inserted_to_crm = array();
  if($check_order_not_in_crm > 0){
    while($row = $order_not_in_crm->fetch_assoc()){  
      $not_inserted_to_crm[] =$row;
    }
  }



  $result = array(
    'not_inserted_to_crm' => $not_inserted_to_crm, 
    'not_inserted_to_metrika' => $not_inserted_to_metrika,
    'list' => $list 
  );


  echo json_encode($result);

  mysqli_close($conn);
  $conn = null;

}	


}