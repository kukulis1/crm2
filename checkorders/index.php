  <?php 
require_once "../config.inc.php";

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

$date = date("Y-m-d");
$not_in_metrika =  $conn->query("SELECT DISTINCT vtiger_salesorder.salesorderid,vtiger_salesorder.external_order_id
                                      FROM vtiger_salesorder  
                                      LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid
                                      LEFT JOIN vtiger_crmentity on vtiger_crmentity.crmid=vtiger_salesorder.salesorderid  AND vtiger_crmentity.setype = 'SalesOrder' 
                                      WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$date' AND 
                                      NOT EXISTS (SELECT app_metrika.id FROM app_metrika WHERE app_metrika.id = vtiger_salesorder.external_order_id)  ");


$not_in_orders =  $conn->query("SELECT DISTINCT id FROM app_metrika WHERE EXISTS (SELECT external_order_id FROM vtiger_salesorder     
                                            WHERE app_metrika.id = vtiger_salesorder.external_order_id  OR vtiger_salesorder.external_order_id is null) ");  

$imported_orders = $conn->query("SELECT * FROM app_orders");

$salesorders_imported = $conn->query("SELECT DISTINCT salesorderid FROM vtiger_salesorder 
                                                                   LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid
                                                                   LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid AND vtiger_crmentity.setype = 'SalesOrder'
                                                                   WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$date'  
                                                                   AND vtiger_inventoryproductrel.id IS NOT NULL");

$order_not_in_crm = $conn->query("SELECT * FROM crm.app_metrika where id NOT IN (SELECT app_metrika.id FROM app_metrika,vtiger_salesorder WHERE app_metrika.id = vtiger_salesorder.external_order_id)");

$imported_num_rows = mysqli_num_rows($imported_orders);

$salesorders_imported_num_rows = mysqli_num_rows($salesorders_imported);

$num_rows = mysqli_num_rows($not_in_metrika);
$not_in_orders_num_rows = mysqli_num_rows($not_in_orders);


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/orders.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'date_from' => $date, 'date_to' => $date));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
$result = curl_exec($ch);
curl_close($ch);   

$data = json_decode($result,true); 

echo "Metrika: ". count($data['order'])."<br>";

echo "app_orders ".$imported_num_rows."<br>";
echo "SalesOrders ".$salesorders_imported_num_rows."<br>";

$not_in_order = ceil($not_in_orders_num_rows - $salesorders_imported_num_rows);
if($not_in_order < 0) $not_in_order = 0;

echo "Nera metrikoje ".$num_rows."<br>";
echo "Nera orderiuose ".$not_in_order."<br>";

$conn->query("INSERT INTO app_salesorders (external_id, source) VALUES('llllll','Metrika')");	

$conn->query("Truncate app_salesorders");
while($row = $not_in_orders->fetch_assoc()){
	$id = $row['id'];
	$conn->query("INSERT INTO app_salesorders (external_id, source) VALUES('$id','Metrika')");	
}

$conn->query("Truncate app_metrika");
foreach($data['order'] as $row){
  $date = date("Y-m-d H:i:s");
  $conn->query("INSERT INTO app_metrika (id,shipment_code,shipment_type,order_date,status,price,price_agreed,load_company_name,import_date ) 
                VALUES('".$row['id']."', '".$row['shipment_code']."','".$row['shipment_type']."','".$row['order_date']."','".$row['status']."','".$row['price']."','".$row['price_agreed']."','".$row['load_company_name']."', '$date') ");  	
}


// Uzsakymai kuriu nera metrikoje

// echo "<br><br>";
// echo "Nera Metrikoje";
$orders_not_inserted_to_metrika = array();
while($row = $not_in_metrika->fetch_assoc()){  
  $orders_not_inserted_to_metrika[] = $row['salesorderid'];  
}

$list = implode(",",$orders_not_inserted_to_metrika);

echo "<pre>";
print_R($list);
$salesOrders = $conn->query("SELECT s.*,e.createdtime,a.accountname FROM vtiger_salesorder s 
                            LEFT JOIN vtiger_account a ON a.accountid=s.accountid
                            LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid where s.salesorderid in ($list)");

while($row = $salesOrders->fetch_assoc()){  
  echo "<pre>";
print_R($row);
}



// echo "Nera crm";

// while($row = $order_not_in_crm->fetch_assoc()){  
//   echo "<pre>";
// print_R($row);
// }
