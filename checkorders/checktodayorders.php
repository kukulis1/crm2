<?php 
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $date = date("Y-m-d");
  $salesorders_imported = $conn->query("SELECT DISTINCT salesorderid FROM vtiger_salesorder 
                                                                   LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid AND vtiger_crmentity.setype = 'SalesOrder'
                                                                   LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid
                                                                   WHERE  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') =  CURDATE() AND vtiger_inventoryproductrel.id IS NOT NULL");

  // $salesorders_imported = $conn->query("SELECT salesorderid FROM vtiger_salesorder WHERE DATE_FORMAT(vtiger_salesorder.import_date, '%Y-%m-%d') = '".$date."' AND vtiger_salesorder.source = 'Metrika' AND vtiger_salesorder.external_order_id IN (SELECT id FROM app_orders)"//);
}

echo mysqli_num_rows($salesorders_imported);
