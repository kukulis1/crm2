// total records show number
$('.totalNumberOfRecords2').on('click', function () {
  $('.showTotalCountIcon2').addClass('hide');

  setTimeout(function () {
    $('#count_show').removeClass('hide');
  }, 1000);
});

$(document).ready(function () {
  DebtsFieldsCheker();
  // if (document.getElementById('alert_message') != null) {
  //   removeAlert(10000);
  // }

  $('#exportPaymentBtn').on('click', function () {
    exportPaymentBtn();
    showErrors('errors');
    removeExportedInvoices();
  });

});

$(document).ajaxComplete(function () {
  DebtsFieldsCheker();
});


function showErrors(name) {
  let myTimer = setInterval(() => {
    let cookie = getCookie(name);
    if (cookie) {
      cookie = cookie.replace(/\%2F/g, "/");
      cookie = cookie.replace(/\%2C/g, ",");
      cookies = cookie.split(',');

      if (document.getElementById('alert_message') != null && cookies.length > 0) {
        clearInterval(myTimer);
        let message = document.getElementById('alert_message');
        let html = '<p style="margin-bottom: 20px;">Neįkeltos saskaitos, kurios neturi sumos arba agnum kodo</p><ul>';
        cookies.forEach(err => {
          html += `<li>${err}</li>`;
        });
        html += '</ul>';
        message.classList.remove('hide');
        message.innerHTML = html;
        eraseCookie(name);
      }
    }
  }, 2000);

}

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=0'
}


function DebtsFieldsCheker() {
  $('.listViewEntriesCheckBox').click(function () {
    $('#exportPaymentBtn').attr('disabled',false);
  });
}

function checkAllBox(event) {
  let checkboxes = $('.listViewEntriesCheckBox');
  if ($(event.target).is(':checked')) {  
    $('#exportPaymentBtn').attr('disabled',false);   
    for(var i=0, n=checkboxes.length;i<n;i++) {      
      checkboxes.eq(i).attr('checked', true);  
    }
  }else{
    $('#exportPaymentBtn').attr('disabled',true);
    for(var i=0, n=checkboxes.length;i<n;i++) {      
      checkboxes.eq(i).attr('checked', false);  
    }
  }
  
}

function removeAlert(sec) {
  setTimeout(() => {
    document.getElementById('alert_message').remove();
  }, sec);
}

function removeExportedInvoices() {
  let body = $('#listview-table tbody tr');
  let check = false;
  let invoiceid = new Array();
  for (let i = 0; i < body.length; i++) {
    check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
    if (check) {
      invoiceid.push($('#listview-table tbody tr').eq(i).data('id'));
    }
  }
  for (let e = 0; e < invoiceid.length; e++) {
    $(`[data-id=${invoiceid[e]}]`).remove();
  }
}


function exportPaymentBtn() {
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let parameters = '';
  let range = document.getElementById('range').value;
  let format = document.getElementById('format').value;
  let showExported = document.querySelector('#showExported').checked;
  let userid = _USERMETA.id;

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      parameters += `${form_key}=${form_value}:`;
    }
  }

  let body = $('#listview-table tbody tr');

  if (!document.querySelector('#fake_search').classList.contains('hide')) {
    body = $('#fake_search tr');
  }

  let invoiceid = '';
  let check = false;

  for (let i = 0; i < body.length; i++) {
    check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
    if (check) {
      invoiceid += $('#listview-table tbody tr').eq(i).data('id') + ",";
    }
  }

  parameters = parameters.slice(0, -1);
  invoiceid = invoiceid.slice(0, -1);

  if (invoiceid != '') {
    window.location.href = `vtlib/Vtiger/xml/exportInvoicesToAgnum.php?parameters=${parameters}&range=${range}&format=${format}&userid=${userid}&showExported=${showExported}${(invoiceid != '') ? '&invoiceid=' + invoiceid : ''}`;
  } else {
    document.querySelector('#exportPaymentBtn').setAttribute('disabled', 'disabled');
  }
}


function filterInvoicesForAgnumExport() {
  $('#wait3').show();
  $('#messageBar').removeClass('hide'); 
  $('.mainListViewEntriesCheckBox').attr('checked',false); 
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let form_array = new Array();
  let html = '';
  let fake_search = document.getElementById('fake_search');
  let showExported = document.querySelector('#showExported').checked;

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      form_array.push({ [form_key]: form_value });
    }
  }


  let assigned_users = $('.select2.listSearchContributor:not([multiple])');

  for (let i = 0; i < assigned_users.length; i++) {
    let form_key = '';
    if (i == 0) form_key = 'created_user';
    if (i == 1) form_key = 'debts_owner';
    if (i == 2) form_key = 'orders_owner';

    let form_value = assigned_users[i].innerText;
    if (form_value != '') {
      form_array.push({ [form_key]: form_value });
    }
  }


  document.querySelector('#table-content').style.height = '100%';

  $.ajax({
    type: "POST",
    url: '/invoices/filterInvoicesForAgnumExport.php',
    data: { form_array: form_array, showExported: showExported },
    dataType: "JSON",
    success: function (response) {
      response.forEach(res => {
        html += `
        <tr style="${(res.exported == 1) ? 'background: #fafad2;' : ''}" class="listViewEntries" data-id="${res.invoiceid}"
        data-recordurl="index.php?module=Invoice&view=Detail&record=${res.invoiceid}"
        id="Unpaidinvoices_listView_row_1">
        <td class="listViewRecordActions">
          <!--LIST VIEW RECORD ACTIONS-->
          <div class="table-actions">
            <span class="input">
              <input type="checkbox" value="${res.invoiceid}" class="listViewEntriesCheckBox" /></span><span>            
            </span>           
          </div>
        </td>    

        <td class="listViewEntryValue" data-invoiceid="${res.account_id}" data-name="account_id" title="${res.account_id}" data-rawvalue="${res.account_id}"
        data-field-type="reference">
        <span class="fieldValue">
          <span class="value">
            <a class="js-reference-display-value" href="index.php?module=Accounts&view=Detail&record=${res.accountid}" title="Klientai">${res.account_id}</a>
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>

      <td class="listViewEntryValue" data-name="invoicedate" title="${res.invoicedate}" data-rawvalue="${res.invoicedate}"
      data-field-type="date">
      <span class="fieldValue">
        <span class="value">
          ${res.invoicedate}
        </span>
      </span>
      <span class="hide edit"> </span>
    </td>

      <td class="listViewEntryValue" data-name="invoice_no" title="${res.invoice_no}" data-rawvalue="${res.invoice_no}"
      data-field-type="string">
      <span class="fieldValue">
        <span class="value">
        <a target="_blank" href="index.php?module=Invoice&view=Detail&record=${res.invoiceid}">${res.invoice_no}</a>
        </span>
      </span>
    </td>

      <td class="listViewEntryValue" data-name="hdnGrandTotal" title="" data-rawvalue="" data-field-type="currency">
      <span class="fieldValue">
        <span class="value">
          ${res.hdnGrandTotal}
        </span>
      </span>
    </td>  

       <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${res.duedate}</span>
        </span>
      </td>
      <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${(res.created_user ? res.created_user : '')}</span>
        </span>
      </td>
        <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${(res.debts_owner ? res.debts_owner : '')}</span>
          </span>
        </td>

        <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${(res.orders_owner ? res.orders_owner : '')}</span>
        </span>
      </td>

      <td class="listViewEntryValue">
      <span class="fieldValue">
        <span class="picklist-color picklist-453-Created">${res.createdtime}</span>
      </span>
    </td>
      </tr>
        `;
      });

      fake_search.innerHTML = html;
      setTimeout(() => {
        $('#real_search').addClass('hide');
        fake_search.classList.remove('hide');
        $('#messageBar').addClass('hide');
        $('#wait3').hide();
      }, 1500);
    }
  });

}