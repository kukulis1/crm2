$(document).on('click', '.repair_price', function(){
  initModalTrigger(this);
});

function initModalTrigger(target){
  let id = $(target).data('purchase');
  let car = $(target).data('carno');
  let serviceid = $(target).data('serviceid');

  let body = $('#orderbody');
  body.html('');
  let html = '';
  if(id != '' || id != null){
    $('#orderautobody').modal('show');
    $.ajax({
      type: "POST",
      url: "modules/Reports/ajax/showPurchases.php",
      data: {id:id,car:car,serviceid:serviceid},
      dataType: "JSON",
      beforeSend: function(){
        $('#wait88').show();
      },
      success: function (response) {        
        if(response != ''){      
          response.forEach(element => {
            html += '<tr>';
            html += `<td><a target="_blank" href="index.php?module=PurchaseOrder&view=Detail&record=${element.purchaseorderid}">${element.purchaseorder_no}</a></td>`;
            html += `<td>${element.vendorname}</td>`;
            html += `<td>${element.services}</td>`;
            html += `<td>${element.listprice}</td>`;
            html += `<td>${element.createdtime}</td>`;
            html += '</tr>';
          });
          body.html(html);
        }
        $('#wait88').hide();
      }
    });
  }
}