{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<link rel="stylesheet" href="layouts/v7/modules/Reports/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Reports/resources/main.js"></script>


{if $REPORT['role'] eq 'H2' OR $REPORT['role'] eq 'H11'}

{if $REPORT['filter']}<div>{$REPORT['filter']}</div>{/if}

{if $REPORT['orders'] neq false}

<div class="modal fade" id="salesOrderReportDetalization" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 900px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pardavimo užsakymai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <table class="table">
					<thead>
					<th>Siuntos Nr.</th>
					<th>Suma</th>
					<th>Klientas</th>
          <th>Dokumentai gauti</th>
					<th>Sukūrimo laikas</th>
					</thead>
					<tbody id="salesOrderReportDetalizationContent"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

<table id='table' style='margin-top:10px;'>
    <thead>
      <tr class='listViewContentHeader'>
        <th>#</th>
        <th>Suma</th>
        <th>%</th>
				{foreach item=value from=$REPORT['period']}        
          {$date = $value->format('m-d')}
         	<th style="padding-left: 5px;">{$date}</th>
        {/foreach}  
        <th width="50" style="background: transparent;border: 0;">---</th>    
    </tr>
    </thead>
		<tbody onclick="showSalesOrdersReport(event);">
      <tr>
        <td class='listViewEntryValue'>Užsakymu viso <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Viso šį mėnesį užsakymų ir kiek kiekviena diena"></i></td> 
        <td class='listViewEntryValue'>{$REPORT['total_orders']}</td>
        <td class='listViewEntryValue'>-</td>       
      	{foreach item=value from=$REPORT['period']}    
        	{$date = $value->format('m-d')}
          <td class='listViewEntryValue'>{($REPORT['orders'][$date]['movement']['total'] + $REPORT['orders'][$date]['parcel']['total'])}</td>                 
        {/foreach}   
        <td width="50" style="padding:0;border: 0;"></td>      
			</tr>
    <tr>
      <td class='listViewEntryValue'>PLL <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Viso šį mėnesį palečių ir kiek kiekviena diena"></i></td>
      <td class='listViewEntryValue'>{$REPORT['total_pll']}</td>
      <td class='listViewEntryValue'>-</td>
      {foreach item=value from=$REPORT['period']}      
     		{$date = $value->format('m-d')}
        <td class='listViewEntryValue'>{($REPORT['orders'][$date]['movement']['pll'] + $REPORT['orders'][$date]['parcel']['pll'])}</td>
      {/foreach}   
    </tr>
		  <tr>
      <td class='listViewEntryValue'>EUR <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Viso šį mėnesį pajamų iš užsakymų ir kiek kiekviena diena"></i></td>
      <td class='listViewEntryValue'>{$REPORT['total_sum']}</td>
      <td class='listViewEntryValue'>-</td>
       {foreach item=value from=$REPORT['period']}   
        {$date = $value->format('m-d')}  
        {$SUM = str_replace(".",",",$REPORT['orders'][$date]['movement']['sum'] + $REPORT['orders'][$date]['parcel']['sum'])} 
        <td class='listViewEntryValue'>{$SUM}</td>
      {/foreach}   
    </tr>
  <tr>
    <td class='listViewEntryValue'>Transportas <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Viso šį mėnesį transporto užsakymų koks procentas ir kiek kiekviena diena"></i></td>
    <td class='listViewEntryValue'>{$REPORT['total_parcel_orders']}</td>
    <td class='listViewEntryValue'>{str_replace(".",",",$REPORT['total_parcel_procent'])} %</td>
   	{foreach item=value from=$REPORT['period']}   
			{$date = $value->format('m-d')}      
			<td class='listViewEntryValue'>{if $REPORT['orders'][$date]['parcel']['total']}{$REPORT['orders'][$date]['parcel']['total']}{else}0{/if}</td>                 
    {/foreach}    
    </tr>
		<tr>
    <td class='listViewEntryValue'>Perkraustymas <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Viso šį mėnesį perkraustymo užsakymų koks procentas ir kiek kiekviena diena"></i></td>
    <td class='listViewEntryValue'>{$REPORT['total_movement_orders']}</td>
    <td class='listViewEntryValue'>{str_replace(".",",",$REPORT['total_movement_procent'])} %</td>
   {foreach item=value from=$REPORT['period']}   
      {$date = $value->format('m-d')}    
      <td class='listViewEntryValue'>{if $REPORT['orders'][$date]['movement']['total']}{$REPORT['orders'][$date]['movement']['total']}{else}0{/if}</td>                
   {/foreach}  
    </tr>
		{foreach key=index item=row from=$REPORT['done_in_days']} 
      {if $index <= 9} {$day_string = 'dienas'} {else if $index <= 20} {$day_string = 'dienų'} {else} {$day_string = 'dieną'}{/if}
      {if $index == 0} {$day_string_sum = "ta pačią dieną"} {else if $index == 1} {$day_string_sum = "kita dieną"} {else}{assign var='day_string_sum' value="per {$index} {$day_string}"}{/if}
     <tr>
      <td class='listViewEntryValue'>Atlikta {$day_string_sum} <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek praėjo dienų nuo užsąkymo gavimo ir statuso ,,Atlikta''"></i></td>
      <td class='listViewEntryValue'>{array_sum($REPORT['done_in_days_total'][$index])}</td>
      <td class='listViewEntryValue'>{str_replace(".",",",ROUND(array_sum($REPORT['done_in_days_total'][$index]) / $REPORT['total_orders'] * 100,2))} %</td>
       {foreach item=value from=$REPORT['period']}   
           {$date = $value->format('m-d')}             
            <td style="cursor:pointer;" data-salesorderid="{if $row[$date]['salesorderids']}{$row[$date]['salesorderids']}{else}0{/if}" class='listViewEntryValue'>{if $row[$date]['viso']}{$row[$date]['viso']}{else}0{/if}</td>                
       {/foreach}  
     </tr>
   {/foreach}  
  </tr>
    <tr>
    <td class='listViewEntryValue'>Vykdoma <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek užsakymų vis dar vykdoma suma, procentas ir atskirai pagal dienas"></i></td>
    <td class='listViewEntryValue'>{$REPORT['total_in_progress']}</td>
    <td class='listViewEntryValue'>{str_replace(".",",",$REPORT['in_progress_procent'])} %</td>
     {foreach item=value from=$REPORT['period']}   
      {$date = $value->format('m-d')}            
      <td style="cursor:pointer;" data-salesorderid="{if $REPORT['in_progress'][$date]['salesorderids']}{$REPORT['in_progress'][$date]['salesorderids']}{else}0{/if}" class='listViewEntryValue'>{if $REPORT['in_progress'][$date]['viso']}{$REPORT['in_progress'][$date]['viso']}{else}0{/if}</td>                 
     {/foreach}  
  </tr>
	<tr>
    <td class='listViewEntryValue'>Išrašyta sąskaitų užsakymams <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek iš viso išrašyta sąskaitų, kiek procentaliai liko ir keliems užsakymams išrašytos saskaitos pagal sukurimo dieną"></i></td>
    <td class='listViewEntryValue'>{$REPORT['total_invoices_from_sales']}</td>
    <td class='listViewEntryValue'>{$REPORT['writed_invoice_procent']} %</td>
     {foreach item=value from=$REPORT['period']}   
        {$date = $value->format('m-d')}              
        <td class='listViewEntryValue'>{if $REPORT['invoices_arr']['total'][$date]} {COUNT($REPORT['invoices_arr']['total'][$date])}{else}0{/if}</td>                
     {/foreach}
  </tr>
 <tr>
    <td class='listViewEntryValue'>Suma dienu <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Sąskaitų suma ir atskirai kiekvienos dienos išrašytų sąskaitų suma pagal tą dieną sukurtus užsakymus"></i></td>
    <td class='listViewEntryValue'>{$REPORT['total_day']}</td>
    <td class='listViewEntryValue'>--</td>
    {$invoice_sum = array()}
     {foreach item=value from=$REPORT['period']} 
        {$date = $value->format('m-d')}
        {$listprice = array_sum($REPORT['listprice'][$date])}  
				{$invoice_sum[$date] = $listprice}          
        <td class='listViewEntryValue'>{str_replace(".",",",ROUND($listprice,2))}</td>               
     {/foreach}
  </tr>
  <tr>
    <td class='listViewEntryValue'>Skirtumas <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kainu sumos skirtumas tarp užsakymu kainos ir sąskaitos detaliu eilučiu sumos"></i></td>
    <td class='listViewEntryValue'>--</td>
    <td class='listViewEntryValue'>--</td>
      {foreach item=value from=$REPORT['period']} 
        {$date = $value->format('m-d')}  
        {$total_sum = array_sum($REPORT['total_sales_sum'][$date])}
        {$diff = str_replace(".",",",ROUND($total_sum - $invoice_sum[$date],2))}
        <td class='listViewEntryValue'>{if $diff}{$diff}{else}0{/if}</td>                  
     {/foreach} 
    </tr>
	<tr>
    <td class='listViewEntryValue'>Trukmė nuo atlikta iki sąskaitos (vid.) <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek vidutiniškai praeina dienų nuo statuso atlikta iki sąskaitos išrašymo"></i></td>
    <td class='listViewEntryValue'>--</td>
    <td class='listViewEntryValue'>--</td>
     {foreach item=value from=$REPORT['period']} 
       {$date = $value->format('m-d')}   
        {$total_days = array_sum($REPORT['time_from_done_to_invoice'][$date])}
        {$total_days_num = count($REPORT['time_from_done_to_invoice'][$date])}
        {$average = ROUND($total_days / $total_days_num)}      
        <td class='listViewEntryValue'>{if !empty($total_days)} {if $average}{$average} {else}-{/if}{else}-{/if}</td>
     {/foreach} 
    </tr>
		<tr>
    <td class='listViewEntryValue'>Užsakymai be sąskaitos <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek užsakymu dar neturi sąskaitos"></i></td>
    <td class='listViewEntryValue'>{$REPORT['dont_have_invoice']}</td>
    <td class='listViewEntryValue'>--</td>
      {foreach item=value from=$REPORT['period']} 
       	{$date = $value->format('m-d')}      
       	{$sum_total = count($REPORT['dont_have_invoice_arr_full'][$date])}
        {$salesorderid = implode(",",$REPORT['dont_have_invoice_arr_full'][$date])}
        <td style="cursor:pointer;" data-salesorderid="{if $salesorderid}{$salesorderid}{else}0{/if}" class='listViewEntryValue'>{if $sum_total}{$sum_total}{else}0{/if}</td>
      {/foreach} 
    </tr>
	<tr>
    <td class='listViewEntryValue'>Liko iki pilno mėnesio uždarymo <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Keliems užsakymams liko išrašyti sąskaitas, viso procentas ir kiekvienos dienos atskirai"></i></td>
    <td class='listViewEntryValue' {if !$REPORT['dont_have_invoice']}style="background: greenyellow;"{/if}>{if $REPORT['dont_have_invoice']}{$REPORT['dont_have_invoice']}{else}{$REPORT['close_date']}{/if}</td>
    <td class='listViewEntryValue'>{$REPORT['untill_done_procent']} %</td>
     {foreach item=value from=$REPORT['period']} 
       	{$date = $value->format('m-d')}           
        {$salesorderid = implode(",",$dont_have_invoice_arr_full[$date])}       
        {$total_sales_has_invoice_one = $REPORT['total_sales_has_invoice_date'][$date]}
        {$total_sales_with_done_one1 = implode(",",$REPORT['total_sales_arr'][$date])}			
        {$total_sales_with_done_one2 = explode(",",$total_sales_with_done_one1)}
        {$total_sales_with_done_one = array_unique($total_sales_with_done_one2)}       
        {$untill_done_procent_each =  ROUND((count($total_sales_with_done_one) - count($total_sales_has_invoice_one)) / count($total_sales_with_done_one)  * 100,2)}
        <td class='listViewEntryValue'>{if $untill_done_procent_each}{$untill_done_procent_each}{/if} %</td>
      {/foreach} 
    </tr>

		<tr>
    <td class='listViewEntryValue'>Apmokėti užsakymai <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek yra apmokėtų užsakymų"></i></td>
    <td class='listViewEntryValue'>{$REPORT['total_sum_payed_invoices']}</td>
    <td class='listViewEntryValue'>{str_replace(".",",",$REPORT['total_sales_has_payment_procent'])} %</td>
     {foreach item=value from=$REPORT['period']} 
       	{$date = $value->format('m-d')}                     
        <td style="cursor:pointer;" data-salesorderid="{if $REPORT['total_sum_payed_invoices_by_date'][$date]}{implode(",",$REPORT['total_sum_payed_invoices_by_date'][$date])}{else}0{/if}" class='listViewEntryValue'>{count($REPORT['total_sum_payed_invoices_by_date'][$date])}</td>                 
      {/foreach} 
    </tr>
    <tr>
    <td class='listViewEntryValue'>Trukmė nuo užsakymo iki apmokėjimo (vid.) <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek vidutiniškai praeina dienų tarp užsakymo gavimo ir jo apmokėjimo"></i></td>
    <td class='listViewEntryValue'>-</td>
    <td class='listViewEntryValue'>-</td>
      {foreach item=value from=$REPORT['period']} 
       	{$date = $value->format('m-d')}       
        {$total_payments = count($REPORT['diff_between_created_and_payed'][$date])}          
        {$total_days = array_sum($REPORT['diff_between_created_and_payed'][$date])}         
        {$average = ROUND($total_days / $total_payments)}      
        <td class='listViewEntryValue'>{if !empty($total_days)}{$average}{else}-{/if}</td>               
       {/foreach} 
    </tr>    
    <tr>
    <td class='listViewEntryValue'>Liko neapmokėta <i style="float: right;font-size: 16px;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Kiek užsakymu dar neapmokėta"></i></td>
    <td class='listViewEntryValue'>{count($REPORT['dont_payed_yet'])}</td>
    <td class='listViewEntryValue'>{if $REPORT['total_sales_has_payment_procent'] eq 100}0{else}{100 - $REPORT['total_sales_has_payment_procent']}{/if} %</td>
      {foreach item=value from=$REPORT['period']} 
       	{$date = $value->format('m-d')}      
        {$dont_payed = count($REPORT['dont_payed_yet_dates'][$date])}      
        <td style="cursor:pointer;" data-salesorderid="{if $REPORT['dont_payed_yet_dates'][$date]}{implode(",",$REPORT['dont_payed_yet_dates'][$date])}{else}0{/if}" class='listViewEntryValue'>{if $dont_payed}{$dont_payed}{else}-{/if}</td>             
      {/foreach} 
    </tr>


			</tbody>
</table>
      {else if $REPORT['no-records']} 
	      <span class="noDataMsg" style="font-size: 30px;">Nėra įrašų</span>
			{else}
		<span class="noDataMsg" style="font-size: 30px;">{str_replace(":","",vtranslate('LBL_SELECT_DATE', $MODULE_NAME))}</span>
{/if}

{else}
	<span class="noDataMsg">{vtranslate('LBL_NO_RIGHTS', $MODULE_NAME)}</span>
{/if}
