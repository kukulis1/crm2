{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

	<div>Įrašai: {$removedOrders['filter']}</div>

{if $removedOrders['user'] eq 1 OR $removedOrders['user'] eq 5}

	{if $removedOrders['content'] neq false}
		<div class="row entry clearfix" style="padding: 2px 20px 2px 14px;">    
			<div class="col-lg-3 pull-left"><b>Įrašas</b></div>
			<div class="col-lg-2 pull-left"><b>Pavadinimas</b></div>
			<div class="col-lg-2 pull-left"><b>Priežastis</b></div>
			<div class="col-lg-2 pull-left"><b>Ištrynęs asmuo</b></div>
			<div class="col-lg-2 pull-left"><b>Laikas</b></div>
		</div>
	<div style='padding:10px;'>
		{foreach key=$index item=ORDER from=$removedOrders['content']} 
			<div class="row entry clearfix" style="padding: 10px 3px 6px;border-bottom: 1px solid #ddd;">    	
				<div class="col-lg-3 pull-left"><a target="_blank" href="index.php?module={$ORDER['setype']}&view=Detail&record={$ORDER['record_id']}">{vtranslate({$ORDER['setype']}, $MODULE_NAME)}</a></div>
				<div class="col-lg-2 pull-left">{$ORDER['code']}</div>      
				<div class="col-lg-2 pull-left">{$ORDER['reason']}</div>   
				<div class="col-lg-2 pull-left">{$ORDER['employee']}</div>   
				<div class="col-lg-2 pull-left">{$ORDER['created_time']}</div>  
			</div>
    {/foreach}
 </div>
			{else}
		<span class="noDataMsg">{vtranslate('LBL_NO_REMOVED_RECORD', $MODULE_NAME)}</span>
{/if}

{else}
	<span class="noDataMsg">{vtranslate('LBL_NO_RIGHTS', $MODULE_NAME)}</span>
{/if}