{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<script src="layouts/v7/modules/Reports/resources/autoReport.js"></script>
<!-- Modal -->
<div class="modal fade" id="orderautobody" tabindex="-1" role="dialog" aria-labelledby="orderautobodyTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Užsakymai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="wait88" style="display:none;position:absolute;top:30%;left:40%;padding:2px;"><img src="/resources/loading.gif"></div>
        <table class="table">
            <thead>
                <th>Nr.</th>
                <th>Tiekėjas</th>
                <th>Paslauga</th>
                <th>Suma</th>
                <th>Sukūrimo data</th>
            </thead>
            <tbody id="orderbody"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>


<div class="dashboardWidgetHeader clearfix">
    <div class="title">
        <div class="dashboardTitle" title="{vtranslate($WIDGET->getTitle(), $MODULE_NAME)}"><b>&nbsp;&nbsp;{vtranslate($WIDGET->getTitle())}</b></div>
    </div>
</div>
<div class="dashboardWidgetContent scrollContainer" style="padding-top:15px;">
{include file="dashboards/AutoCostContent.tpl"|@vtemplate_path:$MODULE_NAME}
</div>

<div class="widgeticons dashBoardWidgetFooter">
    <div class="filterContainer boxSizingBorderBox">
        <div class="row">
            <div class="col-sm-12">
                <span class="col-lg-5">
                        <span>
                            <strong>{vtranslate('LBL_SELECT_DATE', $MODULE_NAME)}</strong>
                        </span>
                </span>
                <span class="col-lg-7">
                    <div class="input-daterange input-group dateRange widgetFilter" id="datepicker" name="modifiedtime">
                        <input type="text" class="input-sm form-control" autocomplete="off" name="start" style="height:30px;"/>
                        <span class="input-group-addon">iki</span>
                        <input type="text" class="input-sm form-control" autocomplete="off" name="end" style="height:30px;"/> 
                    </div>
                </span>
            </div>
        </div>
    </div>
    <div class="footerIcons pull-right">
        {include file="dashboards/DashboardFooterIcons.tpl"|@vtemplate_path:$MODULE_NAME SETTING_EXIST=true}
    </div>
</div>