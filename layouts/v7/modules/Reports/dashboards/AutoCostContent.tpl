{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<link rel="stylesheet" href="layouts/v7/modules/Reports/resources/style.css" type="text/css" />
	<div>Įrašai: {$AUTOCOSTS['filter']}</div>

{if $AUTOCOSTS['user'] eq 1 OR $AUTOCOSTS['user'] eq 5}

	{if $AUTOCOSTS['data'] neq false}
	<table id='table' style='margin-top:10px;'>
		<thead>
				<tr class='listViewContentHeader'>   	
						<th style="padding-left: 5px;width: 200px;">Automobilis</th>
						<th style="padding-left: 5px;width: 60px;">Viso</th>
						{foreach item=date from=$AUTOCOSTS['invoice_date']}
						{* {$date = $value->format('m-d')} *}
							<th style="padding-left: 5px;">{date("m-d",strtotime($date))}</th>
						{/foreach}								
			</tr>
			</thead>
		<tbody>	
		 {$count = 1}
			{foreach item=item key=car from=$AUTOCOSTS['data']}		
					<tr> 
						<td class='listViewEntryValue cursorPointer' data-toggle="collapse" data-target="#collapse{$count}" aria-expanded="true" aria-controls="collapse{$count}">{$car}</td>
						<td class='listViewEntryValue'>{$AUTOCOSTS['total_sum'][$car]}</td>						
						{$TOTAL_ARR = ARRAY()}
						{foreach item=date from=$AUTOCOSTS['invoice_date']}
							{* {$date = $value->format('Y-m-d')} *}
							{$TOTAL_ARR[] = $item[$date]}
							<td class='listViewEntryValue'>{if $item[$date]}{$item[$date]}{else}0{/if}</td>
						{/foreach}						
					</tr>

					{* detalizacija pradzia *}
					<tr id="collapse{$count}" class="collapse except" aria-labelledby="heading{$count}" data-parent="#accordion">  					
						<td style="padding-right: 0px;"> 
								{foreach from=$AUTOCOSTS['detalization'][$car] item=DETALISE}   
									<span style="display:block;padding-top: 5px;padding-bottom: 5px;border-bottom:1px solid #ddd;">{$DETALISE['services']}</span> 
								{/foreach}						
						</td>
										<td></td>
						{foreach item=date2 from=$AUTOCOSTS['invoice_date']}
							{* {$date2 = $value->format('Y-m-d')} *}			
							<td style="padding-left:0;padding-right: 0;">
								{foreach from=$AUTOCOSTS['detalization'][$car] item=DETALISE}  
									{assign var=REPAIR_COST value=$AUTOCOSTS['detalization_price'][$date2][$car][$DETALISE['services']]['total']}								
									{assign var=PURCHASE_ID value=$AUTOCOSTS['detalization_price'][$date2][$car][$DETALISE['services']]['id']}								
									{assign var=SERVICE_ID value=$AUTOCOSTS['detalization_price'][$date2][$car][$DETALISE['services']]['serviceid']}								
									{assign var=CAR_NO value=$AUTOCOSTS['detalization_price'][$date2][$car][$DETALISE['services']]['car_no']}								
									<span style="display:block;padding-top: 5px;padding-left:5px;padding-bottom: 5px;cursor:pointer;border-bottom:1px solid #ddd;" class="repair_price" data-carno="{$CAR_NO}" data-serviceid="{$SERVICE_ID}" data-purchase="{implode(',',$PURCHASE_ID)}">{if $REPAIR_COST}{$REPAIR_COST}{else}0{/if}</span>   
								{/foreach}														
							</td>
						{/foreach}					
					</tr>
			{* detalizacija pabaiga *}


					{assign var=count value=$count+1}
			{/foreach}	
			</tbody>
	</table>


	
{else}
		<span class="noDataMsg">{vtranslate('LBL_NO_RECORDS', $MODULE_NAME)}</span>
{/if}

{else}
	<span class="noDataMsg">{vtranslate('LBL_NO_RIGHTS', $MODULE_NAME)}</span>
{/if}