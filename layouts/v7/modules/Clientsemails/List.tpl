<link rel="stylesheet" href="layouts/v7/modules/Claims/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Claims/resources/main.js"></script>

{assign var=MESSAGES value=$messages}

{* <div class="editViewContents" style="padding: 20px 10px;">
<div class="fieldBlockContainer">
<h4 class="fieldBlockHeader">Pretenzijos</h4>
<hr>
<div class="container-fluid"> *}


<table class="table  listview-table  floatThead-table" style="border-collapse: collapse; display: table; margin: 0px; border-bottom-width: 0px; table-layout: fixed; width: 100%;margin-top: 20px;">
{foreach from=$MESSAGES['fields'] item=field} 
  <colgroup>
    <col style="width: 147px;">
  </colgroup>
{/foreach}
   <thead>
					<tr class="listViewContentHeader">
          	<th>
              <div class="table-actions">
                <div class="dropdown" style="float:left;">
                  <span class="input dropdown-toggle" data-toggle="dropdown" title="Select all records in this page">
                    <input class="listViewEntriesMainCheckBox" type="checkbox">
                  </span>
                </div>	
              </div>
						</th>
          {foreach from=$MESSAGES['fields'] item=field}          
						<th>
              <a href="#" class="listViewContentHeaderValues" data-nextsortorderval="ASC" data-columnname="loaderdriver_tks_name" data-field-id="1676">
							<i class="fa fa-sort customsort"></i>{vtranslate($field['fieldlabel'], 'Claims')}</a>
						</th>	
          {/foreach}					
				</tr>

	<tr class="searchRow">
		<th class="inline-search-btn">
      <div class="table-actions">
        <button class="btn btn-success btn-sm" data-trigger="listSearch">Paieška</button>
      </div>
	  </th>
    {foreach from=$MESSAGES['fields'] item=field} 
			<th>
				<div class=""><input type="text" name="loaderdriver_tks_name" class="listSearchContributor inputElement" value="" data-field-type="string"></div>
							<input type="hidden" class="operatorValue" value="">
				</th>
				 {/foreach}						
	</tr>
	</thead>

  </table>



 {* {foreach from=$MESSAGES['fields'] item=field}   

  <div class="card text-left" style="margin-bottom: 20px;">
      <div class="card-header">
        {$MESSAGES['result'][$len]['label']}
      </div>
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">{$row['mailid']}</p>
      </div>
      <div class="card-footer text-muted">
        2 days ago
      </div>  
    </div>  

   {/foreach}  *}



{* </div>
</div>
</div> *}

