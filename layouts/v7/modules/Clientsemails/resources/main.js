function checkEmailExist() {
  let emailField = document.querySelector('#Clientsemails_detailView_fieldValue_clientsemails_tks_email span a').innerHTML;
  let answer = document.querySelector('[name="answer"]');

  if (answer.value == '') {
    answer.className = 'inputElement alert alert-danger';
  } else {
    answer.className = 'inputElement';
  }

  if (emailField != '') {
    document.getElementById('detailView').submit();
  } else {
    $('#Clientsemails_detailView_fieldValue_clientsemails_tks_email span a').click();
    setTimeout(() => {
      document.querySelector('[name="clientsemails_tks_email"]').className = 'inputElement form-control alert alert-danger';
    }, 100);
  }
}
