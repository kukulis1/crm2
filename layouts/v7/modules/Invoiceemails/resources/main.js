let pageInfo = new URLSearchParams(window.location.search);

$(document).ready(function(){
  $('.list-group-item').on('click',
   function() {  
      if($(this).find('.seen').val() == 0){
        setSeen(this);
      }
    }
  );

  var input = document.getElementById("email_search");
  input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("submitBtn").click();
    }
  });

  $('.search-links-container').hide();
  
  $(document).on('click', '.saveButton', function(){
    let boxId = $(this).data('textbox');
    let boxValue = $(`#comment${boxId}`).val();
    let p = $(this).parent().find('.comment');
    p.html('<i class="fa fa-pencil editButton" title="Redaguoti"></i> <span class="innerText">'+boxValue+'</span>');

    if(boxValue != ''){
      p.removeClass('hide');
      $(this).addClass('hide');
      $(`#comment${boxId}`).addClass('hide');
      $(this).parent().parent().find('.action-group').removeClass('hide');
    }else{
      $(this).parent().find('.info_message').removeClass('hide');
      setTimeout(() => {
        $(this).parent().find('.info_message').addClass('hide');
      }, 5000);
    }
    $.ajax({
      type: "POST",
      url: "modules/Invoiceemails/ajax/addComment.php",
      data: {mailid:boxId,comment:boxValue},
      success: function (response) {        
        console.log(response);
      }
    }); 
  });

  $(document).on('click', '.editButton', function(){
    let text = $(this).parent().find('.innerText').html();
    $(this).parent().addClass('hide');
    $(this).parent().parent().find('[textarea]');
    $(this).parent().parent().find('textarea').removeClass('hide');
    $(this).parent().parent().find('textarea').val(text);
    $(this).parent().parent().find('button').removeClass('hide');
  });
});

function deleteDocument(id,doc,page){
  if(confirm('Ar tikrai norite ištrinti?')){
   return window.location.href=`index.php?module=Invoiceemails&view=Delete&delete=${id}&document=${doc}&page=${page}`;
  }
}


function setSeen(e){
  let id = $(e).find('.mailid').val();
  $.ajax({
    url: "modules/Invoiceemails/ajax/seen.php",
    type: "POST",
    data: { id: id },
    success: function (data) {
      if(data == 'success'){
        $(e).find('.seen').val(1)
        $(e).removeClass("list-group-item-success");
      }
    }
  });
}

function searchEmail(){
  let searchText = document.getElementById('email_search').value;
  let list = document.getElementById('search-list');
  let page = pageInfo.get('page');

  if(searchText == ''){
    $('#default-list').show();
    $('.pagination').show();
    $('#search-list').hide();
  }else{
      $.ajax({
        url: "modules/Invoiceemails/ajax/search.php",
        type: "POST",
        data: { searchText: searchText },
        dataType: 'JSON',
        beforeSend: function () {
          $('#default-list').hide();
          $('.pagination').hide();
          $('#wait').show();
          $('.editViewContents').css('opacity', '0.2');
        },
        success: function (data) {          
          setTimeout(() => {
            let html = '';
            if (data != null) {
              $('#search-list').show();
              $('#wait').hide(); 
              $('.editViewContents').css('opacity', '1');                   
              data.forEach(row => {                
                html += ` <table class="table table-borderless">
                <tr>
                <td style="width: 50%;">
                <a href="javascript:void(0);" 
                class="list-group-item list-group-item-action flex-column align-items-start ${(row.seen == 0 ? 'list-group-item-success' : '')}">
                <input type="hidden" class="mailid" value="${row.id}">
                <input type="hidden" class="seen" value="${row.seen}">
              <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"  style="font-weight: 700;">${row.title}</h5>
                <small>${row.createdate}</small>       
              </div>
                <div class="d-flex w-100">
                  <div class="d-flex justify-content-start flex-column w-50">`;
                  let filename = row.filename.split("|##|")
                  let original_file_name = row.original_file_name.split("|##|")
                  for(let i =0; i < filename.length; i++){        
                  html += ` <p onclick="window.open('${filename[i]}' , '_blank');" class="mb-1 link-hover">${original_file_name[i]}</p>`;
                  }
                    html += `</div>
                  <div class="d-flex justify-content-end w-50">
                      <i title="Sukurti sąskaitą" style="max-height: 30px;" class="fa fa-plus plus" onclick="window.open('index.php?module=PurchaseOrder&view=Edit&Email=${row.email_id}&edited=0' , '_blank');"></i>
                      <i title="Ištrinti" style="max-height: 30px;" class="fa fa-trash delete" onclick="deleteDocument(${row.email_id},'${row.filenameDelete}', '${(page != null ? page : 1)}');"></i>
                    </div> 
                </div>`;  
                          
            if(row.body != ''){
                html += `<div class="d-flex justify-content-start flex-column w-100">       
                  <p> ${row.body.substr(0,150)} ...</p>`;
                if( row.body.length > 150){
                html += ` <p><button href="#block-id${row.email_id}" class="btn btn-info btn-sm" data-toggle="collapse" aria-expanded="false" aria-controls="block-id">
                      <span class="collapsed">Rodyti daugiau</span>
                      <span class="expanded">Rodyti mažiau</span>
                    </button></p> 
                  <p class="collapse" id="block-id${row.email_id}">${row.body}</p>
                  </div>   
                `;    
                }              
            } 

            html += ` <small>${row.name} < ${row.from_address} ></small></a>`;
            html += `</td>
              <td style="width: 50%;">
                <div style="height: 210px;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="d-flex w-100">
                    <div class="d-flex justify-content-start flex-column w-50">     
                      <input type="file" class="document" id="document${row.email_id}" onchange="fileUpload(event);" style="display:none;"/>              
                      <label for="document${row.email_id}" class="btn btn-default selectBtn ${(row.new_file != null ? 'hide' :'')}" style="width:200px;">Pasirinkite dokumentą</label> 
                      <span class="file-chosen">${(row.new_file != null ? row.new_file :'')}</span>
                      <input type="hidden" class="mail_id" value="${row.email_id}">
                     
                      <p class="info_message hide">Išsaugota</p>
                      <textarea class="inputElement ${(row.comments != null ? 'hide' : '')}" name="comment${row.email_id}" id="comment${row.email_id}" rows="5" style="width:200%;${(row.new_file == null ? 'margin-top:10px;' : '')}"></textarea>

                      <button style="width:200%;" type="button" data-textbox="${row.email_id}" class="btn btn-success btn-block saveButton ${(row.comments != null ? 'hide' : '')}">Išsaugoti</button>

                      <div class="comment ${(row.comments == null ? 'hide' : '')}" style="width: 200%;overflow-x: auto;height: 130px;${(row.new_file == null ? 'margin-top:10px;' : '')}"><i class="fa fa-pencil editButton" title="Redaguoti"></i> <span class="innerText">${row.comments}</span>
                      </div>

                    </div>
                    <div class="d-flex justify-content-end w-50">
                    <div class="action-group ${(row.new_file == null ? 'hide' :'')}">
                      <i title="Sukurti sąskaitą" style="max-height: 30px;" class="fa fa-plus plus" onclick="window.open('index.php?module=PurchaseOrder&view=Edit&Email=${row.email_id}&edited=1' , '_blank');"></i>
                      <i title="Ištrinti" style="max-height: 30px;" class="fa fa-trash delete" onclick="deleteDocument(${row.email_id},'${row.new_path}${row.new_file}',1);"></i>
                    </div>
                    </div>
                  </div>
                </div>      
              </td></tr></table> `;


            });
            if(data.length == 0){
              list.innerHTML = '<div style="text-align: center;">Nieko nerasta</div>';
            }else{
              list.innerHTML = html;
            }
            }       
        }, 1000);
        }
      });

  }
}

 function fileUpload(event){
    let thisInstance = event.target;
    var file_data = $(thisInstance).prop('files')[0];   
    var form_data = new FormData();                  
    let mail_id = $(thisInstance).parent().find('.mail_id').val();                
    form_data.append('file', file_data);                            
    form_data.append('mail_id', mail_id);       
    $.ajax({
        url: 'modules/Invoiceemails/ajax/upload.php', 
        dataType: 'JSON',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'POST',
        beforeSend: function () {        
          $('#wait').show();
          $('.editViewContents').css('opacity', '0.2');
        },
        success: function(response){             
           if(response.status == 'success'){      
            $('#wait').hide();
            $('.editViewContents').css('opacity', '1'); 
             $(thisInstance).parent().find('.selectBtn').addClass('hide');
             $(thisInstance).parent().parent().find('.action-group').removeClass('hide');
             $(thisInstance).parent().parent().find('.action-group .delete').removeClass('hide');
             $(thisInstance).parent().find('.file-chosen').html(thisInstance.files[0].name);   
             $(thisInstance).parent().find('.uploaded').html(`<label>Įkelta:</label> ${response.date}`);   
             $(thisInstance).parent().parent().find('.delete').attr('onclick',`deleteDocument(${response.mail_id},'${response.filepath}',1)`);            
           }else{
             alert('Įkėlimas nepavyko');
           }
        }
    });
 }
