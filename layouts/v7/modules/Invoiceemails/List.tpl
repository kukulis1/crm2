<link rel="stylesheet" href="layouts/v7/modules/Invoiceemails/resources/main.css" type="text/css" />
<script src="layouts/v7/modules/Invoiceemails/resources/main.js"></script>

{assign var=MAILS value=$MAILS}
{assign var=PAGINATION value=$pagination}
{assign var=page value=$page}


<div id="wait" style="display:none;position:absolute;top:35%;left:43%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>

<div class="editViewContents" style="padding: 20px 10px;">
<div class="fieldBlockContainer">
<h4 class="fieldBlockHeader">El. paštu gautos sąskaitos</h4>
<hr>
<div class="container">

  <div class="list-group">
    {if $MAILS->rowCount() == 0}
      <h3>Dėžutė tuščia</h3>
    {/if}
    <div id="search-list" style="display:none;"></div>
    
    <div id="default-list">
    {foreach from=$MAILS item=row}  
      <table class="table table-borderless">
        <tr>
        <td style="width: 50%;">
          {assign var=original_file_name value=explode('|##|',$row['original_file_name'])}
          {assign var=filename value=explode('|##|',$row['filename'])}
          <a href="javascript:void(0);" class="list-group-item list-group-item-action flex-column align-items-start {if $row['seen'] eq 0}list-group-item-success{/if}">
            <input type="hidden" class="mailid" value="{$row['email_id']}">
            <input type="hidden" class="seen" value="{$row['seen']}">
          <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1" style="font-weight: 700;">{$row['title']}</h5>
            <small>{$row['createdate']}</small>       
          </div>
        <div class="d-flex w-100">
          <div class="d-flex justify-content-start flex-column w-50">
          {for $len=0 to count($original_file_name)-1}  
                <p onclick="window.open('{$filename[$len]}' , '_blank');" class="mb-1 link-hover">{$original_file_name[$len]}</p>
            {/for}
          </div>
          <div class="d-flex justify-content-end w-50">
              <i title="Sukurti sąskaitą" style="max-height: 30px;" class="fa fa-plus plus" onclick="window.open('index.php?module=PurchaseOrder&view=Edit&Email={$row['email_id']}&edited=0' , '_blank');"></i>
              <i title="Ištrinti" style="max-height: 30px;" class="fa fa-trash delete" onclick="deleteDocument(event,{$row['email_id']},'{$row['filenameDelete']}',{$page});"></i>
            </div>       
        </div>  
        {if $row['body']}  
        <div class="d-flex justify-content-start flex-column w-100">       
          <p> {substr($row['body'], 0, 150)} ...</p>
        {if strlen($row['body']) > 150}
        <p><button href="#block-id{$row['email_id']}" class="btn btn-info btn-sm" data-toggle="collapse" aria-expanded="false" aria-controls="block-id">
              <span class="collapsed">Rodyti daugiau</span>
              <span class="expanded">Rodyti mažiau</span>
            </button></p> 
          <p class="collapse" id="block-id{$row['email_id']}">{htmlspecialchars($row['body'])}</p>
          </div>    
        {/if}     
        {/if} 
          <small>{$row['name']} < {$row['from_address']} ></small>
        </a>  
      </td>
      <td style="width: 50%;">
        <div style="height: 210px;" class="list-group-item list-group-item-action flex-column align-items-start">
          <div class="d-flex w-100">
            <div class="d-flex justify-content-start flex-column w-50">     
              <input type="file" class="document" id="document{$row['email_id']}" onchange="fileUpload(event);" style="display:none;"/>              
              <label for="document{$row['email_id']}" class="btn btn-default selectBtn {if !empty($row['new_file'])}hide{/if}" style="width:200px;">Pasirinkite dokumentą</label> 
              <span class="file-chosen">{if !empty($row['new_file'])}{$row['new_file']}{/if}</span>
              <input type="hidden" class="mail_id" value="{$row['email_id']}">
              {if !empty($row['new_file'])}<p><label>Įkelta:</label> {$row['createtime']}</p>{else}<p class="uploaded"></p> {/if}
              <p class="info_message hide">Išsaugota</p>
              <textarea class="inputElement {if !empty($row['comments'])}hide{/if}" name="comment{$row['email_id']}" id="comment{$row['email_id']}" rows="5" style="width:200%;{if empty($row['new_file'])}margin-top:10px;{/if}"></textarea>
              <button style="width:200%;" type="button" data-textbox="{$row['email_id']}" class="btn btn-success btn-block saveButton {if !empty($row['comments'])}hide{/if}">Išsaugoti</button>
              <div class="comment {if empty($row['comments'])}hide{/if}" style="width: 200%;overflow-x: auto;height: 130px;{if empty($row['new_file'])}margin-top:10px;{/if}"><i class="fa fa-pencil editButton" title="Redaguoti"></i> <span class="innerText">{$row['comments']}</span></div>
              
            </div>
            <div class="d-flex justify-content-end w-50">
             <div class="action-group {if empty($row['new_file']) AND empty($row['comments'])}hide{/if}">
               <i title="Sukurti sąskaitą" style="max-height: 30px;" class="fa fa-plus plus" onclick="window.open('index.php?module=PurchaseOrder&view=Edit&Email={$row['email_id']}&edited=1' , '_blank');"></i>
              <i title="Ištrinti" style="max-height: 30px;" class="fa fa-trash delete {if empty($row['new_file'])}hide{/if}" onclick="deleteDocument({$row['email_id']},'{$row['new_path']}{$row['new_file']}',1);"></i>
             </div>
            </div>
          </div>
        </div>      
      </td>
      </tr>   
    {/foreach}
  </div>
  </div>


</table>

</div>
</div>

{$PAGINATION}
