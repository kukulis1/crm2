{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	{assign var="FIELD_INFO" value=$FIELD_MODEL->getFieldInfo()}
	{assign var="SPECIAL_VALIDATOR" value=$FIELD_MODEL->getValidator()}


	{if (!$FIELD_NAME)}
		{assign var="FIELD_NAME" value=$FIELD_MODEL->getFieldName()}
	{/if}
	<input {if $FIELD_MODEL->getFieldName() eq 'debts_tks_suma'}oninput="checkCreditSum(this);"{/if} id="{$MODULE}_editView_fieldName_{$FIELD_NAME}" type="{if $FIELD_NAME eq 'subject' and ($MODULE eq 'Invoice' or $MODULE eq 'PurchaseOrder')}hidden{else}text{/if}" data-fieldname="{$FIELD_NAME}" {if $MODULE eq 'Accounts'}data-column="{$FIELD_NAME}" data-type-column="{$COLUMNS[$FIELD_NAME]}"{/if} data-fieldtype="string" class="inputElement {if $FIELD_MODEL->isNameField()}nameField{/if}" name="{$FIELD_NAME}" value="{$FIELD_MODEL->get('fieldvalue')}"
		{if $FIELD_MODEL->get('uitype') eq '3' || $FIELD_MODEL->get('uitype') eq '4'|| $FIELD_MODEL->isReadOnly()}
			{if $FIELD_MODEL->get('uitype') neq '106'}
				readonly
			{else if $FIELD_MODEL->get('uitype') eq '106' && $MODE eq 'edit'}
				readonly
			{/if}
		{/if}
		{if !empty($SPECIAL_VALIDATOR)}data-validator="{Zend_Json::encode($SPECIAL_VALIDATOR)}"{/if}
		{if $FIELD_INFO["mandatory"] eq true} data-rule-required="true" {/if}
		{if count($FIELD_INFO['validator'])}
			data-specific-rules='{ZEND_JSON::encode($FIELD_INFO["validator"])}'
		{/if}

			{if $MODULE eq 'Debts' && $FIELD_MODEL->getFieldName() eq 'debts_tks_suma'}  		
				oninput="checkCreditSum(this);"
			{/if}  

					

		   />
	
{/strip}
