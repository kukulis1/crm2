<link rel="stylesheet" href="layouts/v7/modules/Automaticinvoicing/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Automaticinvoicing/resources/main.js?u=2"></script>
<link rel="stylesheet" href="layouts/v7/modules/Automaticinvoicing/resources/daterangepicker.min.css">
<script type="text/javascript" src="layouts/v7/modules/Automaticinvoicing/resources/moment.min.js"></script>
<script type="text/javascript" src="layouts/v7/modules/Automaticinvoicing/resources/jquery.daterangepicker.min.js"></script>

{assign var=INVOICES value=$INVOICES}
{assign var=all_records value=$all_records}
{assign var=PAGINATION value=$pagination}
{assign var=CLIENTS value=$CLIENTS}

  <div class="modal fade" id="autoInvoicingModal" tabindex="-1" role="dialog" aria-labelledby="autoInvoicingModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 700px;">
    <div class="modal-content">
    <div id="wait2" style="display:none;position:absolute;top: 60%;left:30%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Automatinis sąskaitų generavimas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">  
       <table id="auto-listview-table" class="table listview-table">
       <tr>
        <td><div style="float:left;">Papildomai grupuoti pagal pasikrovimo įmonę  <input id="byLoadCompany" class="inputElement" type="checkbox" style="margin-left: 10px;"></div></td>
        <td>Filtruoti užsakymus pagal:
          <select id="select_filter_by" class="inputElement" style="width: 120px;">
            <option value="order_date">Užsakymo datą</option>
            <option value="load_date">Pasikrovimo datą</option>
            <option value="unload_date">Išsikrovimo datą</option>
          </select>
        </td>
       </tr>
        </table>
      



        <table id="auto-listview-table" class="table listview-table">
          <thead>
           <tr class="listViewContentHeader">
            <th>Klientas</th>           
            <th>Periodas</th>           
           </tr>
          </thead>
          <tbody>
            <tr>
              <td style="padding-left:30px;">
                <select class="inputElement" id="client_name">
                  {foreach from=$CLIENTS item=client}
                    <option value="{$client['accountid']}">{$client['accountname']}</option>
                  {/foreach}
                </select>              
              </td>             
              <td style="padding-left:30px; padding-right: 30px;"><input id="date_range" class="inputElement search_date_range" autocomplete="off" style="padding-left: 5px;"></td>            
            </tr>
          </tbody>
        </table>
          <div id="rule" style="margin-top: 20px;"></div>
          <input type="hidden" id="table">
        <div id="ordersList" style="margin-top:20px;"></div>
      </div>
       <div id="errors" class="alert alert-danger" style="display: none;"></div>
      <div class="modal-footer">
        <span style="float:left;">Pažymėtom užsakymų grupėm išrašys atskiras sąskaitas</span>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
        <button type="button" class="btn btn-secondary" onclick="getOrdersList();">Atnaujinti</button>
        <button type="button" class="btn btn-primary" id="generate-btn" onclick="initGenerateBtnEvent();" disabled>Generuoti sąskaitas</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="sendInvoice" tabindex="-1" role="dialog" aria-labelledby="sendInvoiceLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 35%;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendInvoiceLabel">Sąskaitos siuntimas klientui el. paštu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="sendEmail">
         <div class="form-group">
          <input name="emailAddress" type="email" class="form-control" placeholder="Kliento el. pašto adresas" required>
            <input type="hidden" name="invoiceid" id="invoice_id2">
            </div>
          <div class="form-group">
            <textarea class="form-control" rows="5" name="message" placeholder="Žinutė"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Atšaukti</button>        
        <button type="button" class="btn btn-primary" onclick="sendInvoice(event)">Siųsti Sąskaitą <i class="fa fa-paper-plane"></i></button>
        <input type="hidden" id="invoice_id">
      </div>
    </div>
  </div>
</div>


<div id="table-content" class="table-container">
<div id="wait" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>
	<div class="filter_invoices" style="margin-top: 10px;">
  <span style="margin-left: 20px; margin-right: 10px; font-family: 'OpenSans-Semibold', 'ProximaNova-Semibold', sans-serif; font-weight: normal; font-size: 1.1em;">Filtruoti sąskaitas pagal:</span>
  <select id="select_filter_by" class="inputElement" style="width: 120px;">
    <option value="invoice_date">Sąskaitos datą</option>
    <option value="due_date">Mokėjimo datą</option>
  </select>
    <input type="text" id="invoices_from" class="listSearchContributor inputElement search_date" placeholder="Nuo" style="width: 150px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off"> 
    <input type="text" id="invoices_to" class="listSearchContributor inputElement search_date" placeholder="Iki" style="width: 150px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
    <button class="btn btn-success" type="button" onclick="searchByPeriod();">Ieškoti</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       

     <button class="btn btn-success" type="button" data-toggle="modal" data-target="#autoInvoicingModal">Automatinis sąskaitų generavimas</button> 
  </div>

    <form name='list' id='submit-invoice' action='' onsubmit="return false;">
			<table id="listview-table" class="table listview-table">
				<thead>
					<tr class="listViewContentHeader">
          <th>
          <div class="dropdown" style="float:left;">
							<span class="input dropdown-toggle" title="{vtranslate('LBL_CLICK_HERE_TO_SELECT_ALL_RECORDS',$MODULE)}">
								<input class="listViewEntriesMainCheckBox" onClick="toggle(this)" type="checkbox">
							</span>
						</div>
          </th>
            <th style="width: 80px;">Veiksmas</th>
            <th style="width: 100px;">Veiksmas</th>
            <th style="width: 300px;">Kliento pavadinimas</th>
            <th>Sąskaitos data</th>        
            <th>Sąskaitos nr.</th>          
            <th>Suma</th>
            <th>Mokėjimo data</th>
            <th>Sąskaitą išrašė</th>
            <th>Atsakingas už skolas</th>
            <th>Atsakingas už užsakymus</th>
            <th>Sukūrimo laikas</th>
          </tr>	
				</thead>
        <tr>
          <th></th>
          <th class="inline-search-btn"><div><button class="btn btn-success btn-sm hide" data-trigger="listSearch"></button></div><span id="old_rec"> Viso: {$all_records}</span> <span style="display: none;" id="filter_rec"></span></th>
          <th></th>
          <th><input type="text" id="customer" class="listSearchContributor inputElement search_input"></th>        
          <th><input type="text" id="invoicedate" class="listSearchContributor inputElement search_input search_date" data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></th>
          <th><input type="text" id="invoice_no" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="total" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="duedate" class="listSearchContributor inputElement search_input search_date" data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></th>
          <th><input type="text" id="invoice_created" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="resp_debts" class="listSearchContributor inputElement search_input"></th>                 
          <th><input type="text" id="resp_orders" class="listSearchContributor inputElement search_input"></th>                 
          <th><input type="text" id="createdtime" class="listSearchContributor inputElement search_input search_date"  data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></th>                 
        </tr>       
				<tbody id="firstRecords">           
           {$count = 1}         
           {foreach from=$INVOICES item=invoice}  
          <tr style="cursor:pointer" onClick="action(event);">
              <input type="hidden" class="invoice_id" value="{$invoice['invoiceid']}"/> 
          	<td class="except" style="cursor: default !important;"><input type="checkbox" name="list" value="" class="listViewEntriesCheckBox"/></td>
          	<td class="listViewEntryValue except" style="cursor: default !important;"><button class="btn btn-success btn-sm print" style="cursor: pointer !important;" onclick="window.location.href = 'index.php?module=Invoice&action=ExportPDF&record={$invoice['invoiceid']}'">Spausdinti</button></td>          	
            <td class="listViewEntryValue except" style="cursor: default !important;"><button class="btn btn-success btn-sm send_to_client" style="cursor: pointer !important;" data-toggle="modal" data-target="#sendInvoice" onclick="getUserEmail(event);">Siųsti klientui</button> <input type="hidden" class="accountid" value="{$invoice['accountid']}"/> 
            </td>
          	<td class="listViewEntryValue" id="customer_name"><a href="/index.php?module=Accounts&view=Detail&record={$invoice['accountid']}">{$invoice["accountname"]}</a></td>               
            <td class="listViewEntryValue">{date('Y-m-d',strtotime($invoice["invoicedate"]))}</td>              
          	<td class="listViewEntryValue except">{if !empty($invoice["invoice_no"])}<a target="_blank" class="except" href="/index.php?module=Invoice&view=Detail&record={$invoice["invoiceid"]}">{$invoice["invoice_no"]}</a> {else} --- {/if}</td>           
            <td class="listViewEntryValue" id="total">{if !empty($invoice["total"])}{$invoice["total"]} {else} --- {/if}</td>      
          	<td class="listViewEntryValue">{$invoice["duedate"]}</td>
          	<td class="listViewEntryValue">{$invoice["created_invoice"]}</td>       	
          	<td class="listViewEntryValue">{if $invoice["resp_debts"]}{$invoice["resp_debts"]}{else}---{/if}</td>          	
          	<td class="listViewEntryValue">{if $invoice["resp_orders"]}{$invoice["resp_orders"]}{else}---{/if}</td>          	
          	<td class="listViewEntryValue">{$invoice["createdtime"]}</td>          	
        </tr>	
       {assign var=count value=$count+1}
          {/foreach}          
				</tbody>
			</table> 
     
        {if $count <= 1}<div id="no_records" style="text-align: center;"><h5>Sąskaitų nėra</h5></div>{/if} 
    
         <div id="no_records" style="text-align: center; display: none;"><h5>Nieko nerasta</h5></div>     
</form>

{$pagination}

</div>

