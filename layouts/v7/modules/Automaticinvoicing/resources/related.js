function myNewFunction(sel) {
  // alert(sel.options[sel.selectedIndex].text);
}


function saveFields(which, table) {
  let accountid = $('[name="record_id"]').val();
  let selectCount = document.querySelectorAll('#' + which + ' select:not(.except)').length;
  let selectValue = document.querySelectorAll('#' + which + ' select:not(.except)');
  let tr = document.querySelectorAll('#' + which + ' tr:not(.except)');
  let string = new Array();;
  for (let i = 0; i < selectCount; i++) {
    string.push(selectValue[i].value);
  }

  let insertData = '';
  $.ajax({
    type: "POST",
    url: "modules/Autoinvoicingrules/ajax/saveAddress.php",
    data: { accountid: accountid, string: string, table: table },
    dataType: 'JSON',
    success: function (result) {
      let i = 0;
      if (result.status == 'success') {
        string.forEach(address => {
          let replaceAddress = address.replace('|##|', ',');
          insertData += `
          <tr class="listViewEntries">
            <td style="cursor: default !important; padding:5px !important;"><i onclick="deleteRecord('${table}',event);" data-accountid="${accountid}" data-record="${result.id[i]}" style="color: #ec6262; cursor: pointer;" title="Ištrinti" class="fa fa-times"></i></td>
            <td style="cursor: default !important; padding:5px !important;" class="relatedListEntryValues">${replaceAddress}</td>
          </tr>`;
          i++;
        });
        $('.dismiss').click();
        let list = document.getElementById(`${table}_list`);
        list.innerHTML = list.innerHTML + insertData;
      } else if (result.status == 'Fail') {
        console.log(result);
      } else {
        selectValue[result.line].setAttribute('class', 'alert alert-danger');
        selectValue[result.line].setAttribute('style', 'width: 100%;');
        if (!tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0]) {
          let message = document.createElement('p');
          message.setAttribute('style', 'font-size: 13px; color: #a94442;');
          message.innerHTML = `Toks adresas jau yra`;
          tr[result.line].getElementsByTagName('td')[0].append(message);
          setTimeout(() => {
            tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0].remove();
            selectValue[result.line].setAttribute('class', 'inputElement');
          }, 2000);
        }
      }
    }
  });
}


function saveFieldsEmployee() {
  let accountid = $('[name="record_id"]').val();
  let selectCount = document.querySelectorAll('#employeeTable select:not(.except)').length;
  let selectValue = document.querySelectorAll('#employeeTable select:not(.except)');
  let tr = document.querySelectorAll('#employeeTable tr:not(.except)');
  let string = new Array();
  for (let i = 0; i < selectCount; i++) {
    string.push(selectValue[i].value);
  }
  let insertData = '';
  $.ajax({
    type: "POST",
    url: "modules/Autoinvoicingrules/ajax/saveEmployee.php",
    data: { accountid: accountid, string: string },
    dataType: 'JSON',
    success: function (result) {
      if (result.status == 'success') {
        let i = 0;
        string.forEach(employee => {
          insertData += `
          <tr class="listViewEntries">
          <td style="cursor: default !important; padding:5px !important;"><i onclick="deleteRecordEmployee(event);" data-accountid="${accountid}" data-record="${result.id[i]}" style="color: #ec6262; cursor: pointer;" title="Ištrinti" class="fa fa-times"></i></td>
          <td style="cursor: default !important; padding:5px !important;" class="relatedListEntryValues">${employee}</td>
        </tr>`;
          i++;
        });
        $('.dismiss').click();
        let list = document.getElementById('employee_list');
        list.innerHTML = list.innerHTML + insertData;
      } else if (result.status == 'Fail') {
        console.log(result);
      } else {
        selectValue[result.line].setAttribute('class', 'alert alert-danger');
        selectValue[result.line].setAttribute('style', 'width: 100%;');
        if (!tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0]) {
          let message = document.createElement('p');
          message.setAttribute('style', 'font-size: 13px; color: #a94442;');
          message.innerHTML = `Toks darbuotojas jau yra`;
          tr[result.line].getElementsByTagName('td')[0].append(message);
          setTimeout(() => {
            tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0].remove();
            selectValue[result.line].setAttribute('class', 'inputElement');
          }, 2000);
        }
      }
    }
  });
}


function deleteRecord(table, e) {
  if (confirm('Ar tikrai norite ištrinti įrašą?')) {
    let accountid = e.target.dataset.accountid;
    let record = e.target.dataset.record;

    $.ajax({
      type: "POST",
      url: "modules/Autoinvoicingrules/ajax/deleteAddress.php",
      data: { accountid: accountid, record: record, table: table },
      dataType: 'JSON',
      success: function (result) {
        if (result == 'success') {
          e.target.parentElement.parentElement.remove();
        } else {
          console.log(result);
        }
      }
    });
  }
}

function deleteRecordEmployee(e) {
  if (confirm('Ar tikrai norite ištrinti įrašą?')) {
    let accountid = e.target.dataset.accountid;
    let record = e.target.dataset.record;

    $.ajax({
      type: "POST",
      url: "modules/Autoinvoicingrules/ajax/deleteEmployee.php",
      data: { accountid: accountid, record: record },
      dataType: 'JSON',
      success: function (result) {
        if (result == 'success') {
          e.target.parentElement.parentElement.remove();
        } else {
          console.log(result);
        }
      }
    });
  }
}