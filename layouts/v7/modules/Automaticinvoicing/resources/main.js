$(document).ready(function () {


  $(function () {
    $('.search_date').datepicker({
      dateFormat: "yy-mm-dd",
      autoclose: true,
    });
  });

  $('.search_date_range').dateRangePicker({
    separator: ',',
    autoClose: true,
  }).on('datepicker-change', function () {
    getOrdersList();
  });

  $('.search_input').each(function () {
    $(this).keydown(function (e) {
      var key = e.which;
      if (key == 13 || key == 9) {
        searchFilter();
      }
    });
    $(this).change(function () {
      if ($(this).val() != '') {
        searchFilter();
      }
    });
  });

  $('#client_name').on('change', function () {
    if ($('.search_date_range').val() != '') {
      getOrdersList();
    }
  });

  $('#select_filter_by').on('change', function () {
    if ($('.search_date_range').val() != '') {
      getOrdersList();
    }
  });


  $('#byLoadCompany').on('click', function () {
    if ($('.search_date_range').val() != '') {
      getOrdersList();
    }
  });

});


function getOrdersList() {
  let accountid = $('#client_name').val();
  let dateRange = $('#date_range').val();
  let byLoadCompany = ($('#byLoadCompany').is(":checked") ? 1 : 0);
  let filterBy = $('#select_filter_by').val();
  if (accountid != '' && dateRange != '') {

    let html = `<table id="auto-listview-table" class="table listview-table">
                <thead>
                <tr class="listViewContentHeader">
                  <th>#</th>
                  <th>Užsakymų</th>
                  <th>Suma</th>
                  <th>Pasikrovimas</th>
                  <th>Gatvė</th>
                </tr>
                </thead>
                <tbody>`;
    let ordersList = document.querySelector('#ordersList');

    $.ajax({
      type: "POST",
      url: "modules/Automaticinvoicing/ajax/ordersList.php",
      data: { accountid: accountid, dateRange: dateRange, byLoadCompany: byLoadCompany, filterBy: filterBy },
      dataType: "JSON",
      beforeSend: function () {
        ordersList.innerHTML = '';
        $('#rule').hide();
        $('#errors').hide();
        $('#wait2').show();
        $('#table-content').css('opacity', '0.2');
      },
      success: function (response) {
        if (response.status == 'success') {
          $('#rule').show();
          response.res.forEach(res => {
            html += `<tr>
                    <td class="listViewEntryValue" data-check="${(res.gruped == 1 ? '1' : res.street)}" data-salesorderid="${res.salesorderids}"><input type="checkbox" class="inputElement checkbox" ${(res.gruped == 1 ? 'checked' : '')}></td>
                    <td class="listViewEntryValue">${res.orders}</td>
                    <td class="listViewEntryValue">${res.total}</td>
                    <td class="listViewEntryValue" title="${res.grouped_load_company}">${res.load_company}</td>
                    <td class="listViewEntryValue">${res.accountname}, ${res.street}, ${res.city} ${(res.employee ? '/<span style="color:blue;"> Sukūrė ' + res.employee + '</span>' : '')}</td>                    
                   </tr>`;

          });
          html += `</tbody></table>`;

          ordersList.innerHTML = html;
          $('#rule').html(`Taikoma taisyklė: pagal ${response.rule}`);
          $('#table').val(response.table);
          $('#wait2').hide();
          $('#table-content').css('opacity', '1');
          $('#generate-btn').prop('disabled', false);

        } else {
          $('#wait2').hide();
          $('#table-content').css('opacity', '1');
          $('#errors').show();
          $('#errors').html(`${response.res}`);
        }
      }
    });

  }
}

function initGenerateBtnEvent() {
  $('#wait2').show();
  $('#table-content').css('opacity', '0.2');
  let accountid = $('#client_name').val();
  let userId = _USERMETA.id;
  let orders_cheked = new Array();
  let orders_uncheked = new Array();

  $('.checkbox').each(function () {
    if ($(this).is(':checked')) {     
      orders_cheked.push($(this).parent().data('salesorderid'));
    } else {
      orders_uncheked.push($(this).parent().data('salesorderid'));
    }
  });

  $.ajax({
    type: "POST",
    url: "modules/Automaticinvoicing/ajax/generateInvoices.php",
    data: { orders_cheked: orders_cheked, orders_uncheked: orders_uncheked, accountid: accountid, userId: userId },
    dataType: "JSON",
    success: function (response) {
      console.log(response);
      if (response == 'success') {
        window.location.reload();
      } else {
        console.log(response);
        $('#errors').html(`Klaida, sąskaitu sugeneruoti nepavyko`);
        $('#wait2').hide();
        $('#table-content').css('opacity', '1');
      }
    }
  });



}


// function initGenerateBtnEvent() {
//   let accountid = $('#client_name').val();
//   let table = $('#table').val();
//   let orders = '';
//   let groupOrders = '';
//   $('#wait2').show();
//   $('#table-content').css('opacity', '0.2');

//   $('.checkbox').each(function () {
//     if ($(this).is(':checked')) {
//       orders += $(this).parent().data('salesorderid') + ',';
//       string = $(this).parent().data('check');
//       if (string != 1) {
//         $.when(insertNewAddress(accountid, table, new Array(string))).done(function (res) {
//           if (res.status == 'success') {
//             setTimeout(() => {
//               window.location.href = `/invoices/invoice_all_global.php?accountid=${accountid}&orders=${orders}`;
//             }, 5000);
//           } else {
//             $('#errors').show();
//             $('#errors').html('Įvyko klaida');
//           }
//         });
//       } else {
//         setTimeout(() => {
//           window.location.href = `/invoices/invoice_all_global.php?accountid=${accountid}&orders=${orders}`;
//         }, 5000);
//       }
//     } else {
//       groupOrders += $(this).parent().data('salesorderid') + ',';
//     }
//   });

//   orders += groupOrders;
//   orders = orders.slice(0, -1);
// }

// function insertNewAddress(accountid, table, string) {
//   return $.ajax({
//     type: "POST",
//     url: "modules/Autoinvoicingrules/ajax/saveAddress.php",
//     data: { accountid: accountid, string: string, table: table },
//     dataType: 'JSON',
//     success: function (result) {

//     },
//     error: function (err) {
//       reject(err);
//     }
//   });
// }

function getUserEmail(e) {
  let accoundid = e.target.parentElement.querySelector('.accountid').value;
  let invoiceid = e.target.parentElement.parentElement.querySelector('.invoice_id').value;
  $('#invoice_id').val(invoiceid);
  $('#invoice_id2').val(invoiceid);
  let input = document.querySelector('[type="email"]');
  $('[name="message"]').val('');
  input.value = '';

  $.ajax({
    type: "POST",
    url: "invoices/getClientEmail.php",
    data: { accoundid: accoundid },
    success: function (result) {
      if (result != 'empty') {
        input.value = result;
      }
    }
  });

}

function sendInvoice(e) {
  if (document.querySelector('[name="emailAddress"]').value) {
    let record = e.target.parentElement.querySelector('#invoice_id').value;
    $.ajax({
      type: "POST",
      url: `index.php?module=Invoice&view=SendEmail&mode=composeMailData&record=${record}`,
      data: { 1: 1 },
      beforeSend: function () {
        app.helper.showProgress();
      },
      success: function (result) {
        if (result != '') {
          document.getElementById('sendEmail').submit();
        }
      }
    });
    $('[name="emailAddress"]').removeClass('alert alert-danger');
  } else {
    $('[name="emailAddress"]').addClass('alert alert-danger');
  }
}


function toggle(source) {
  checkboxes = document.getElementsByName('list');
  for (var i = 0, n = checkboxes.length; i < n; i++) {
    checkboxes[i].checked = source.checked;
  }
}


function action(e) {
  let orderId;
  if (!$(e.target).closest('input').length && !$(e.target).closest('.print').length && !$(e.target).closest('.send_to_client').length && !$(e.target).hasClass('except')) {
    orderId = e.target.parentElement.querySelector(".invoice_id").value;
    return window.location.href = `index.php?module=Invoice&view=Detail&record=${orderId}`;
  }
}


function searchFilter() {
  let filterBy, from, to, customer, invoicedate, invoice_no, total, duedate, invoice_created, resp_debts, resp_orders, createdtime;


  from = $('#invoices_from').val();
  to = $('#invoices_to').val();
  filterBy = $('#select_filter_by').val();

  $('.listSearchContributor').each(function () {

    if ($(this).val() != '') {
      if ($(this).attr("id") == 'customer') {
        customer = $(this).val();
      }

      if ($(this).attr("id") == 'invoicedate') {
        invoicedate = $(this).val();
      }

      if ($(this).attr("id") == 'invoice_no') {
        invoice_no = $(this).val();
      }

      if ($(this).attr("id") == 'total') {
        total = $(this).val();
      }

      if ($(this).attr("id") == 'duedate') {
        duedate = $(this).val();
      }

      if ($(this).attr("id") == 'invoice_created') {
        invoice_created = $(this).val();
      }

      if ($(this).attr("id") == 'resp_debts') {
        resp_debts = $(this).val();
      }

      if ($(this).attr("id") == 'resp_orders') {
        resp_orders = $(this).val();
      }

      if ($(this).attr("id") == 'createdtime') {
        createdtime = $(this).val();
      }


    } else {
      $('#old_rec').css('display', 'block');
      $('#filter_rec').css('display', 'none');
    }
  });

  if ((from != '' && to != '') || customer != null || invoicedate != null || invoice_no != null || total != null || duedate != null || invoice_created != null || resp_debts != null || resp_orders != null || createdtime != null) {
    let no_records = $('#no_records');
    $.ajax({
      url: "modules/Automaticinvoicing/ajax/filter_auto_created_invoices.php",
      type: "POST",
      data: { from: from, to: to, filterBy: filterBy, customer: customer, invoicedate: invoicedate, invoice_no: invoice_no, total: total, duedate: duedate, invoice_created: invoice_created, resp_debts: resp_debts, resp_orders: resp_orders, createdtime: createdtime },
      dataType: 'JSON',
      beforeSend: function () {
        $('#submit-invoice').css('opacity', '0.2');
        $("#wait").show();
      },
      success: function (data) {
        if (data == 'no_results') {
          $("#wait").hide();
          $('#filter-records').remove();
          $('#firstRecords').hide();
          document.querySelector('.pagination').classList.add('hide');
          $('#submit-invoice').css('opacity', '1');
          no_records.show();
          $('#filter_rec').html(`Viso: 0`);
          $('#old_rec').css('display', 'none');
          $('#filter_rec').css('display', 'block');
        } else if (data == 'empty') {
          $('#filter-records').remove();
          $("#wait").hide();
          $('#submit-invoice').css('opacity', '1');
          no_records.hide();
          $('#filter_rec').html(`Viso: 0`);
          $('#old_rec').css('display', 'none');
          $('#filter_rec').css('display', 'block');
        } else {
          no_records.hide();
          $('#firstRecords').hide();
          let table = $('#listview-table');
          let tbody = document.createElement('tbody');
          tbody.setAttribute('id', 'filter-records');
          let html = '';
          let index = 1;
          if (data != null) {
            $('#filter_rec').html(`Viso: ${data.length}`);
            $('#old_rec').css('display', 'none');
            $('#filter_rec').css('display', 'block');
            data.forEach(record => {
              html += `
            <tr class="listViewContentHeader" style="cursor:pointer;" onClick="action(event);">
               <input type="hidden" class="invoice_id" value="${record.invoiceid}"/> 
              <td class="listViewEntryValue except" style="cursor: default !important;"><input type="checkbox" name="list" value="" class="listViewEntriesCheckBox"/></td>
              <td class="listViewEntryValue except" style="cursor: default !important;"><button style="cursor: pointer !important;" class="btn btn-success btn-sm except write_invoice" onclick="window.location.href = 'index.php?module=Invoice&action=ExportPDF&record=${record.invoiceid}'">Spaudinti</button></td>
              <td class="listViewEntryValue except" style="cursor: default !important;"><button style="cursor: pointer !important;" class="btn btn-success except btn-sm write_invoice_all" data-toggle="modal" data-target="#sendInvoice" onclick="getUserEmail(event);">Siųsti klientui</button> <input type="hidden" class="accountid" value="${record.accountid}"/></td>
              <td class="listViewEntryValue">${record.accountname}</td>
                <input type="hidden" class="user_id" value="${record.accountid}"/> 
              <td class="listViewEntryValue">${record.invoicedate}</td>             
              <td class="listViewEntryValue">${(record.invoice_no != '') ? `<a target="_blank" href="/index.php?module=Invoice&view=Detail&record=${record.invoiceid}">` + record.invoice_no + '</a>' : '---'}</td>
              <input type="hidden" class="order_id" value="${record.invoiceid}"/>                 
              <td class="listViewEntryValue">${record.total}</td>       
              <td class="listViewEntryValue">${record.duedate}</td>       
              <td class="listViewEntryValue">${record.created_invoice}</td>
              <td class="listViewEntryValue">${(record.resp_debts ? record.resp_debts : '---')}</td>
              <td class="listViewEntryValue">${(record.resp_orders ? record.resp_orders : '---')}</td>
              <td class="listViewEntryValue">${record.createdtime}</td>     
            </tr>`;
              index++;
            });
          }
          if ($('#filter-records') != null) {
            $('#filter-records').remove();
            tbody.innerHTML = html;
          } else {
            tbody.innerHTML = html;
          }
          table.append(tbody);
          document.querySelector('.pagination').classList.add('hide');
          setTimeout(() => {
            $('#submit-invoice').css('opacity', '1');
            $("#wait").hide();
          }, 1000);
        }
      }
    });
  } else {
    $('#filter-records').remove();
    $('#firstRecords').show();
    $('#no_records').hide();
    document.querySelector('.pagination').classList.remove('hide');
  }

}


function searchByPeriod() { searchFilter(); }

