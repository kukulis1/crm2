<link rel="stylesheet" href="layouts/v7/modules/Salesordersdocuments/resources/style.css?u=1" type="text/css" />
<script src="layouts/v7/modules/Salesordersdocuments/resources/main.js?u=15"></script>
<link rel="stylesheet" href="layouts/v7/modules/Automaticinvoicing/resources/daterangepicker.min.css">
<script type="text/javascript" src="layouts/v7/modules/Automaticinvoicing/resources/moment.min.js"></script>
<script type="text/javascript" src="layouts/v7/modules/Automaticinvoicing/resources/jquery.daterangepicker.min.js"></script>

{assign var=ORDERS value=$orders}
{assign var=TRANSIT value=$transit}
{assign var=PARAMS value=$params}
{assign var=DOCUMENTS value=$documents}
{assign var=all_records value=$all_records}

{assign var=FROM_NUMBER value=$FROM_NUMBER}
{assign var=TO_NUMBER value=$TO_NUMBER}

{assign var=PAGINATION value=$pagination}

{assign var=doc_url value=$DOCUMENTS['doc_url']}
{assign var=doc_title value=$DOCUMENTS['doc_title']}
{assign var=doc_date value=$DOCUMENTS['doc_date']}
{assign var=pod_url value=$DOCUMENTS['pod_url']}
{assign var=pod_title value=$DOCUMENTS['pod_title']}
{assign var=pod_date value=$DOCUMENTS['pod_date']}


<div id="table-content" class="table-container">
<form name='list' method="GET" id='submit-invoice' action='index.php'>
  <input type="hidden" name="module" value="Salesordersdocuments">
  <input type="hidden" name="view" value="List">
  <div id="wait" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>
	<div class="filter_salesorders" style="margin-top: 10px;">
  <span style="margin-left: 20px; margin-right: 10px; font-family: 'OpenSans-Semibold', 'ProximaNova-Semibold', sans-serif; font-weight: normal; font-size: 1.1em;">Filtruoti užsakymus pagal:</span>
  <select id="select_filter_by" name="filter_by" class="inputElement onchange_input" style="width: 120px;">
    <option value="order_date" {if $PARAMS['filter_by'] eq 'order_date'}selected{/if}>Užsakymo datą</option>
    <option value="load_date" {if $PARAMS['filter_by'] eq 'load_date'}selected{/if}>Pasikrovimo datą</option>
    <option value="unload_date" {if $PARAMS['filter_by'] eq 'unload_date'}selected{/if}>Išsikrovimo datą</option>
  </select>
    <input type="text" id="salesorders_between" name="date_from_to" class="listSearchContributor inputElement search_date_range" placeholder="Nuo,Iki" style="width: 170px;" autocomplete="off" value="{if isset($PARAMS['date_from_to'])}{$PARAMS['date_from_to']}{/if}">
    <button class="btn btn-success" type="button" onclick="searchByPeriod();">Ieškoti</button> 

     <button class="btn btn-success" data-accountid="" type="button" onclick="checkSelectedDocsReceive();" 
    style="margin-left: 100px;">Pažymėti dokumentai gauti</button>
  </div>  
			<table id="listview-table" class="table listview-table">
				<thead>       
					<tr class="listViewContentHeader">
          <th>
          <div class="dropdown" style="float:left;">
							<span class="input dropdown-toggle" title="{vtranslate('LBL_CLICK_HERE_TO_SELECT_ALL_RECORDS',$MODULE)}">
								<input class="listViewEntriesMainCheckBox" onClick="toggle(this)" type="checkbox">
							</span>
						</div>
          </th>
            <th>Veiksmas</th>
            <th>Užsakymo data</th>        
            <th>Siuntos nr.</th>
            <th>Užsakovas</th>
            <th>Kainynas</th>
            <th>Tipas</th>
            <th>Pasikrovimo data</th>
            <th>Pasikrovimo adresas</th>
            <th>Išsikrovimo data</th>
            <th>Išsikrovimo adresas</th>
            <th>U-vnt/kg/m3/pll</th>
            <th>F-vnt/kg/m3/pll</th>
            <th>Apm-vnt/kg/m3/pll</th>
            <th>Parašas</th>          
            <th>Papildomi darbai</th>          
            <th>Pap. Kaina</th>
            <th>Bendra kaina</th>
            <th>Reisas</th>
            <th>Vairuotojas</th>
            <th>Transportas</th>
            <th>Statusas</th>
            <th>Atsakingas asmuo</th>
          </tr>	
				</thead>
         <tbody> 
        <tr>
          <th></th>
          <th><span id="old_rec" style="font-size: 12px;font-weight: normal;font-family: 'OpenSans-Regular', sans-serif;">{$FROM_NUMBER} iki {$TO_NUMBER} iš {$all_records}</span> <span style="display: none;" id="filter_rec"></span></th>
          
          <th><input type="text" name="order_date" id="order_date" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off" value="{if isset($PARAMS['order_date'])}{$PARAMS['order_date']}{/if}"></th>        
         
          <th><input type="text" name="shipment_code" id="item_num" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['shipment_code'])}{$PARAMS['shipment_code']}{/if}"></th>
          
          <th><input type="text" name="customer" id="customer" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['customer'])}{$PARAMS['customer']}{/if}"></th>
          
          <th><input type="text" name="pricebook" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['pricebook'])}{$PARAMS['pricebook']}{/if}"></th>
          
          <th><input type="text" name="type" id="type" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['type'])}{$PARAMS['type']}{/if}"></th>
         
          <th><input type="text" name="load_date" id="load_date" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off" value="{if isset($PARAMS['load_date'])}{$PARAMS['load_date']}{/if}"></th>
         
          <th><input type="text" name="load_address" id="load_address" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['load_address'])}{$PARAMS['load_address']}{/if}"></th>
         
          <th><input type="text" name="unload_date" id="unload_date" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off" value="{if isset($PARAMS['unload_date'])}{$PARAMS['unload_date']}{/if}"></th>
         
          <th><input type="text" name="unload_address" id="unload_address" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['unload_address'])}{$PARAMS['unload_address']}{/if}"></th>
         
          <th><input type="text" class="listSearchContributor inputElement" readonly></th>
          <th><input type="text" class="listSearchContributor inputElement" readonly></th>
          <th><input type="text" class="listSearchContributor inputElement" readonly></th>
         
          <th>
             <select name="signatares" id="signatare" class="listSearchContributor inputElement search_input onchange_input">         
              <option {if $PARAMS['signatares'] eq ''}selected{/if}></option>
              <option value="true"  {if $PARAMS['signatares'] eq 'true'}selected{/if}>Yra</option>
              <option value="false"  {if $PARAMS['signatares'] eq 'false'}selected{/if}>Nėra</option>
            </select>
          </th>
         
          <th>
            <select name="documents" id="document" class="listSearchContributor inputElement search_input onchange_input">         
              <option  {if $PARAMS['documents'] eq ''}selected{/if}></option>
              <option value="true"  {if $PARAMS['documents'] eq 'true'}selected{/if}>Yra</option>
              <option value="false"  {if $PARAMS['documents'] eq 'false'}selected{/if}>Nėra</option>
            </select></th>
         
          <th><input type="text" class="listSearchContributor inputElement" readonly></th>         
          <th><input type="text" id="overall_price" class="listSearchContributor inputElement" readonly></th>

          <th><input type="text" name="route" id="route" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['route'])}{$PARAMS['route']}{/if}"></th>

          <th><input type="text" name="drivers" id="driver" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['drivers'])}{$PARAMS['drivers']}{/if}"></th>
          
          <th><input type="text" name="transport" id="transport" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['transport'])}{$PARAMS['transport']}{/if}"></th>
          
          <th>           
            <select  name="status" id="status" class="listSearchContributor inputElement search_input onchange_input">         
              <option {if $PARAMS['status'] eq ''}selected{/if}></option>
              <option value="Sent" {if $PARAMS['status'] eq 'Sent'}selected{/if}>Naujas</option>
              <option value="Approved" {if $PARAMS['status'] eq 'Approved'}selected{/if}>Priimtas</option>
              <option value="in progress" {if $PARAMS['status'] eq 'in progress'}selected{/if}>Vykdomas</option>
              <option value="delivered" {if $PARAMS['status'] eq 'delivered'}selected{/if}>Atliktas</option>
            </select>
          </th>
          
          <th><input name="manager" type="text" id="manager" class="listSearchContributor inputElement search_input" value="{if isset($PARAMS['manager'])}{$PARAMS['manager']}{/if}"></th>
          
        </tr>    
        </tbody>   
				<tbody id="firstRecords">           
           {$count = 1}               
           {foreach from=$orders item=order}
            {assign var=DOC_URL value=explode('|##|', $doc_url[$order['salesorderid']])}
            {assign var=DOC_TITLE value=explode('|##|', $doc_title[$order['salesorderid']])}
            {assign var=DOC_DATE value=explode('|##|',  $doc_date[$order['salesorderid']])}
            {assign var=POD_URL value=explode('|##|', $pod_url[$order['salesorderid']])}
            {assign var=POD_TITLE value=explode('|##|', $pod_title[$order['salesorderid']])}
            {assign var=POD_DATE value=explode('|##|',  $pod_date[$order['salesorderid']])}
            {assign var=revised_cargos value=explode('#:#',  $order['order_cargos'])}  

            {assign var=post_code_location value=$MODULE_MODEL->getPostCodeInfo([$order["bill_code"], $order["ship_code"]], [$order["bill_city"], $order["ship_city"]])}           
         

           <tr style="cursor:pointer;{if $order['update_status'] eq 0} background:##faffcf;{/if}" onclick="action(event);">
          	<td><input type="checkbox" name="list" value="" class="listViewEntriesCheckBox"/></td>
          	<td class=""><button type="button" class="btn btn-success btn-sm docs_received" ondblclick="docsReceive({$order['salesorderid']},event);">Dokumentai gauti</button></td>
          	
            <td class="listViewEntryValue">{if !empty($order["preinvoice"])}  <i class="fa fa-history" title="Yra išankstinė saskaita" style="font-size: 15px;"></i>{/if} {date('Y-m-d',strtotime($order["createdtime"]))}</td>
               <input type="hidden" class="order_id" value="{$order['salesorderid']}"/>    
            
            <td class="listViewEntryValue linkas">{if !empty($order["shipment_code"])}<a target="_blank" href="/index.php?module=SalesOrder&view=Detail&record={$order["salesorderid"]}">{$order["shipment_code"]}</a> {else} --- {/if}</td>     
          	
            <td class="listViewEntryValue" id="customer_name">{$order["accountname"]}</td>   
               <input type="hidden" class="user_id" value="{$order['accountid']}"/>   
            
            <td class="listViewEntryValue pricebook" {if !empty($order["pricebook_details"])}title="{$order["pricebook_details"]}"{/if}>{if !empty($order["pricebook"])}{$order["pricebook"]} {else} --- {/if}</td>      
            
            <td class="listViewEntryValue">{$order["type"]} {if $TRANSIT[$order['salesorderid']] eq 0} <span><i class="fa fa-flag-checkered" title="Užsakymas ne lietuvos teritorijoje" style="color: crimson;"></i></span>{/if} {if !empty($order['termo'])} <i class="fa fa-fire" title="Termo užsakymas" style="color: blue; margin-left: 0.25rem;"></i> {{/if}}</td>      
          	
            <td class="listViewEntryValue">{$order["load_date_from"]}</td>
          	
            <td class="listViewEntryValue" style="text-align: left!important;">{$order["load_company"]}, {$order["bill_street"]}, {$order["bill_city"]}, {$order["bill_code"]}            
            {if $post_code_location['bill']['status'] eq 1}
              <span style="color: red;" title="{$post_code_location['bill']['city']}">Pašto kodo miestai: {$post_code_location['bill']['location']}</span>
            {/if}
            </td>
          	
            <td class="listViewEntryValue">{$order["unload_date_from"]}</td>	           

            <td class="listViewEntryValue" style="text-align: left!important;">{$order["unload_company"]}, {$order["ship_street"]}, {$order["ship_city"]}, {$order["ship_code"]}     
            {if $post_code_location['ship']['status'] eq 1}
              <span style="color: red;" title="{$post_code_location['ship']['city']}">Pašto kodo miestai: {$post_code_location['ship']['location']}</span>
            {/if}
            </td>
          	
            <td class="listViewEntryValue">{if !empty($order["ordered_weight"])} {(float)round($order['quantity'], 2)}/{(float)round($order['ordered_weight'], 2)}/{(float)round($order['ordered_volume'], 2)}/{(float)round($order['ordered_pll'], 2)} {else} --- {/if}</td>
          	
            <td class="listViewEntryValue">
              <span rel="popover2" data-toggle="popover" data-content="
                <ul>
                    {for $leng=0 to COUNT($revised_measures[$order['salesorderid']])-1}
                        <li style='list-style: none;'>{$revised_measures[$order['salesorderid']][$leng]}</li>
                    {/for}
                </ul>">
                {if !empty($order['revised_weight'])} {(float)$order['revised_quantity']}/{(float)round($order['revised_weight'], 2)}/{(float)round($order['revised_volume'], 2)}/{(float)round($order['revised_pll'], 2)} {else} --- {/if}
                </span>
              </td>
          	
            <td class="listViewEntryValue taxable taxable_{$order['salesorderid']}">
            {if $order['show_taxable'] eq 1}
              {if !empty($order["ordered_weight"])} 
                <b>{(float)round($order['taxable_quantity'], 2)}/{(float)round($order['taxable_weight'], 2)}/{(float)round($order['taxable_volume'], 2)}/{(float)round($order['taxable_pll'], 2)}</b>

                {if $order['taxable_dims_change_permisions']}
                  <span rel="popover3" data-toggle="popover"
                    data-content="
                      <p title='Keisti apmokestinamus matmenis'>Keisti apmokestinamus</p>         
                      <button class='btn btn-warning btn-sm' data-place='edit' data-dimensions='{(float)round($order['quantity'], 2)}/{(float)round($order['ordered_weight'], 2)}/{(float)round($order['ordered_volume'], 2)}/{(float)round($order['ordered_pll'], 2)}' onclick='setTaxableDimensionsOrdered(this,{$order['salesorderid']});'>Užsakyti</button> 
                      <button class='btn btn-success btn-sm' data-place='edit' data-dimensions='{(float)$order['revised_quantity']}/{(float)round($order['revised_weight'], 2)}/{(float)round($order['revised_volume'], 2)}/{(float)round($order['revised_pll'], 2)}' onclick='setTaxableDimensionsRevised(this,{$order['salesorderid']});'>Faktiniai</button>
                    "> <i class="fa fa-pencil"></i> 
                  </span>
                {/if}

                
                <i class="fa fa-exclamation-circle {if $order['meters'] == 0}hide{/if}" title="Kaina paskaičiuota atsižvelgiant į metrus" style="font-size: 20px;color:#fc631a;"></i>
                

              {else} --- {/if}              
            {else}
              <span class="taxable_popover" rel="popover3" data-toggle="popover"
              data-content="
                <p title='Kainų skaičiavimo metu nebuvo nustatyta kainyno kaina, todėl reikia pasirinkti kurie matmenys bus apmokestinami'>Pasirinkite matmenis</p>         
                <button class='btn btn-warning btn-sm' data-place='first-time' data-dimensions='{(float)round($order['quantity'], 2)}/{(float)round($order['ordered_weight'], 2)}/{(float)round($order['ordered_volume'], 2)}/{(float)round($order['ordered_pll'], 2)}' onclick='setTaxableDimensionsOrdered(this,{$order['salesorderid']});'>Užsakyti</button> 
                <button class='btn btn-success btn-sm' data-place='first-time' data-dimensions='{(float)$order['revised_quantity']}/{(float)round($order['revised_weight'], 2)}/{(float)round($order['revised_volume'], 2)}/{(float)round($order['revised_pll'], 2)}' onclick='setTaxableDimensionsRevised(this,{$order['salesorderid']});'>Faktiniai</button>
              "> <i class="fa fa-ellipsis-h ellipsis-style"></i> </span>
            {/if}
            </td>            
            
            <td class="listViewEntryValue documents">                            
                {if !empty($POD_URL[0])}
                  <span rel="popover2" data-toggle="popover"
                  data-content="<ul>{for $len=0 to COUNT($POD_URL)-1}<li style='list-style: none;'><a href='{$POD_URL[$len]}'>{$POD_TITLE[$len]}, {$POD_DATE[$len]}</a></li>{/for}</ul>">
                  <i class="fa fa-picture-o pop" style="font-size: 20px;"></i>
                  </span>
                {/if}
            </td>
          	
            <td class="listViewEntryValue documents">
            {if empty($DOC_URL[0])}
              <span style="color: red">
                {if $order['minifest']}KV{/if} {if $order['cmr']}CMR{/if} {if $order['invoice']}SF{/if}
              </span>
            {else}              
                <span class="pop" rel="popover2" data-toggle="popover" style="color: green"
                data-content="<ul>{for $len=0 to COUNT($DOC_URL)-1}<li style='list-style: none;'><a href='{$DOC_URL[$len]}'>{$DOC_TITLE[$len]}, {$DOC_DATE[$len]}</a></li>{/for}</ul>">
                 {if $order['minifest']}KV{/if} {if $order['cmr']}CMR{/if} {if $order['invoice']}SF{/if}                 
                  </span>               
            {/if}
            {if $order['minifest'] eq '2' OR $order['cmr'] eq '2' OR $order['invoice'] eq '2'} <i class='fa fa-check-circle' title="Dokumentų originalai gauti" style="font-size: 20px;color: #52d652;"></i>{/if}
             </td>
          	
            <td class="listViewEntryValue">---</td>
          	
            <td class="listViewEntryValue change_price">          
            <img id="spin{$count}" style="display: none;" src="/resources/ajax-loader.gif"> 
            <span id="{$count}" class="price-bold amount{$count}" rel="popover" data-toggle="popover" data-content='<input id="price_value{$count}" style="width:55px; display: flex; margin-left: 3px;" value="{$order["totalprice"]}"> 
            <button style="margin-top:5px" class="btn btn-success btn-sm" id="save_price{$count}">Saugoti</button>'>{$order["totalprice"]}</span>
            {if $order['agreed_price'] != null}<span style="font-weight: normal;" title="Sutarta kaina">* 
              {if $order['warning'] eq 1} <i style="color:red;" title="Kažkuris faktinis matmuo 10% didesnis už užsakyta">???</i>{/if}
            </span>
            {else}
              <i class="fa fa-calculator" title="Kainos perskaičiavimas"  data-spinid="{$count}" onclick="reCount({$order['salesorderid']},event);"></i>
            {/if}       
            </td>
            <td class="listViewEntryValue">{if $order["route"]}{$order["route"]}{/if}</td>
            <td class="listViewEntryValue">{if $order["driver"]}{$order["driver"]}{/if}</td>
            <td class="listViewEntryValue">{if $order["transport"]}{$order["transport"]}{/if}</td>
          	<td class="listViewEntryValue">{vtranslate($order["sostatus"], 'SalesOrder')}</td>
          	<td class="listViewEntryValue">{if !empty($order["user_name"])} {$order["first_name"]} {$order["last_name"]} {else} --{/if}</td>
        </tr>	        
       {assign var=count value=$count+1}
          {/foreach}          
				</tbody>
			</table>       
      <div id="no_records" style="text-align: center; display: none;"><h5>Nieko nerasta</h5></div>     
</form>

{$pagination}

</div>

