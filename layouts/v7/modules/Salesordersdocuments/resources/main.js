let upSide = document.querySelector('#appnav');
// Hide unnecessary buttons
if (upSide.querySelector('#Salesordersdocuments_listView_basicAction_LBL_ADD_RECORD') != null) {
  upSide.querySelector('#Salesordersdocuments_listView_basicAction_LBL_ADD_RECORD').style.display = 'none';
}
if (upSide.querySelector('#Salesordersdocuments_basicAction_LBL_IMPORT') != null) {
  upSide.querySelector('#Salesordersdocuments_basicAction_LBL_IMPORT').style.display = 'none';
}

if (document.querySelector('#appnav').querySelector('.settingsIcon') != null) {
  upSide.querySelector('.settingsIcon').style.display = 'none';
}

$(document).ready(function () {
  var popOverSettings = {
    placement: 'left',
    container: 'body',
    html: true,
    selector: '[rel="popover"]',
    content: function () {
      return $('#popover-content').html();
    }
  }

  $('.search_date_range').dateRangePicker({
    separator: ',',
    autoClose: true,
  });


  //popover
  $(function () {
    $('body').popover(popOverSettings).on("show.bs.popover", function (e) {
      $("[rel=popover]").not(e.target).popover("destroy");
      $(".popover").remove();
    });
  });

  loadOnHoverPopover();
  initTaxableDimensionsPopUp();

  $(function () {
    $(document).on('click', function () {
      $('[rel=popover2]').not(this).popover('hide');
    });
  });



  $(function () {
    $('.search_date').datepicker({
      dateFormat: "yy-mm-dd",
      autoclose: true
    });
  });


  $('.search_input').each(function () {
    $(this).keydown(function (e) {
      var key = e.which;
      if (key == 13 || key == 9) {
        // searchFilter();
        searchByPeriod();
      }
    });
    // $(this).change(function () {
    //   if ($(this).val() != '') {
    //     searchFilter();
    //   }
    // }); 
  });

  $('.onchange_input').each(function () {
    $(this).change(function () {
      searchByPeriod();  
    });  
  });

});

function toggle(source) {
  checkboxes = document.getElementsByName('list');
  for (var i = 0, n = checkboxes.length; i < n; i++) {
    checkboxes[i].checked = source.checked;
  }
}

function loadOnHoverPopover() {
  $('[rel="popover2"]').popover({
    placement: 'left',
    html: true,
    content: function () {
      return $("#popover-content").html();
    }
  }).mouseenter(function () {
    if (!$(this).parent().find('.popover').length) {
      $(this).popover('show');
    }
  });
}



function action(e) {

  if (!$(e.target).closest('button').length && !$(e.target).closest('input').length && $(e.target).closest('.change_price').length && !$(e.target).closest('.fa-calculator').length ) {
    change_price(e);
  } else if (!$(e.target).closest('button').length && !$(e.target).closest('input').length && $(e.target).closest('.documents').length) {
  } else if (!$(e.target).closest('button').length && !$(e.target).closest('input').length && $(e.target).closest('.taxable').length) {
  } else if ($(e.target).closest('button').length && !$(e.target).closest('input').length && $(e.target).closest('.linkas').length) {
  } else if ($(e.target).closest('.docs_received').length) { 
  } else if (!$(e.target).closest('button').length && !$(e.target).closest('input').length) {
    if (e.target.parentElement.querySelector(".order_id") != null) {
      let orderId = e.target.parentElement.querySelector(".order_id").value;
      return window.location.href = `index.php?module=SalesOrder&view=Detail&record=${orderId}`;
    }

  } else {
    // console.log('checkbox');
  }
}

function docsReceive(orderId,e){ 
  $(e.target).attr('disabled','disabled');
  setDocsReceive(orderId, e, 'one');   
}

function setDocsReceive(orderId, e, type){
  let userId = _USERMETA.id;   
  $.ajax({
    url: "/modules/Salesordersdocuments/ajax/docs_received.php",
    type: "POST",
    data: { orderId: orderId, userId: userId, type:type },
    dataType: 'JSON',
    beforeSend: function () {
      $('#wait').show();
    },
    success: function (data) {          
      if (data.status == 'success') {
        setTimeout(() => {
          $('#wait').hide();
          if(type == 'one'){
            $(e.target).parent().parent().remove();
          }else{
            e.forEach(item => {
              $(item).remove();
            });           
          }
        }, 500);

      } else {
        $('#wait').hide();
        console.log('error');
      }
    }
  });
}

function checkSelectedDocsReceive(){
  let salesorderid = new Array();
  let multiEvent = new Array();

  $(".listViewEntriesCheckBox:checked").each(function () {     
    if($(this).is(':checked')){     
      salesorderid.push($(this).parent().parent().find('.order_id').val());
      multiEvent.push($(this).parent().parent());
    }
  });
  if(salesorderid.length > 0){
    if(confirm('Ar tikrai norite uždėti žymą dokumentai gauti pasirinktiems užsakymams?')){
      setDocsReceive(salesorderid, multiEvent, 'multi');
    }
  }else{
    alert('Nepasirinktas nei vienas užsakymas');
  }
}

function reCount(salesorderid,e){  
  if(confirm('Ar tikrai norite perskaičiuoti?')){
    let id = $(e.target).data('spinid');
    $.ajax({
      type: "POST",
      url: "modules/Salesordersdocuments/ajax/re_count.php",
      data: {salesorderid:salesorderid},
      dataType: "JSON",
      beforeSend: function(){
        $(e.target).parent().find(`#spin${id}`).css('display', 'inline-block');
        $(e.target).parent().find(`.amount${id}`).css('display', 'none');
      },
      success: function (response) {    
        if(response.status == 'success'){
          $(e.target).parent().find(`#spin${id}`).css('display', 'none');
          $(e.target).parent().find(`.amount${id}`).css('display', 'inline-block');
          $(e.target).parent().find(`.amount${id}`).html(response.price);
          $(e.target).parent().parent().css('background','white');        
          $(e.target).parent().parent().find('.pricebook').html(response.pricebook);        
          $(e.target).parent().parent().find('.taxable').html(`<b>${response.taxable}</b>`);        
        }else{
          $(e.target).parent().find(`#spin${id}`).css('display', 'inline-block');
          $(e.target).parent().find(`.amount${id}`).html('<span style="color:red;">Įvyko klaida</span>');          
          $(e.target).parent().parent().find('.pricebook').html('<span style="color:red;">Įvyko klaida</span>');       
        }
      }
    });
  }
}

function initTaxableDimensionsPopUp(){
  $('[rel="popover3"]').popover({
    container: 'body',
    placement: 'bottom',
    html: true
  })
}

function setTaxableDimensionsOrdered(event,salesorderid){ 
  setTaxableDimensions(event,salesorderid,'ordered')
}

function setTaxableDimensionsRevised(event,salesorderid){
  setTaxableDimensions(event,salesorderid,'revised')
}


function setTaxableDimensions(event,salesorderid,type){
  let dimensions = $(event).data('dimensions');
  let place = $(event).data('place');
  let userId = _USERMETA.id;

  $.ajax({
    type: "POST",
    url: "modules/Salesordersdocuments/ajax/setTaxableDimensions.php",
    data: {salesorderid:salesorderid,type:type,userId:userId},
    dataType: "JSON",
    success: function (response) {
      console.log(response);
      if(response.status == 'success'){
        if(place == 'first-time'){         
          $(event).parent().parent().popover("destroy");
          $(`.taxable_${salesorderid}`).html(`<b>${dimensions}</b>`);
        }else{
          $(event).parent().parent().popover("hide");
          $(`.taxable_${salesorderid}`).find('b').html(`${dimensions}`);
        }
      }
    }
  });
}

function change_price(e) {
  let orderId = $(e.target).parent().parent().find(".order_id").val();
  let id = e.target.id;
  let userId = _USERMETA.id;

  setTimeout(() => {  
    $(`#save_price${id}`).on('click', function() {
      let  newPrice = document.querySelector(`#price_value${id}`).value;
      newPrice = newPrice.replace(',', '.');

      $(`.amount${id}`).popover('hide');
      $.ajax({
        url: "/invoices/change_price.php",
        type: "POST",
        beforeSend : function(){
          $(`.amount${id}`).hide();
          $(e.target).parent().find(`#spin${id}`).css('display','inline-block');
        },
        data: { orderId: orderId, newPrice: newPrice, userId: userId },
        success: function (data) {
          console.log(data);
          if (data) {           
            setTimeout(() => {
              $(`.amount${id}`).css('display','inline-block');              
              $(e.target).parent().find(`#spin${id}`).hide();          
              fixprice = parseFloat(data).toFixed(2);
              fixprice = fixprice.replace('.', ',');
              $(e.target).html(fixprice);
            }, 1000);
          } else {
            console.log('error');
          }
        }
      });
    });

  }, 200);

}

function searchFilter() {
  let filterBy, between, orderDate, orderNumber, customer, pricebook, type, loadDate, loadAddress, unloadDate, unloadAddress, signatare, documents, route, driver, transport, status, manager;

  between = $('#salesorders_between').val();

  filterBy = $('#select_filter_by').val();

  $('.listSearchContributor').each(function () {

    if ($(this).val() != '') {
      if ($(this).attr("id") == 'order_date') {
        orderDate = $(this).val();
      }
      if ($(this).attr("id") == 'item_num') {
        orderNumber = $(this).val();
      }
      if ($(this).attr("id") == 'customer') {
        customer = $(this).val();
      }
      if ($(this).attr("id") == 'pricebook') {
        pricebook = $(this).val();
      }
      if ($(this).attr("id") == 'type') {
        type = $(this).val();
      }
      if ($(this).attr("id") == 'load_date') {
        loadDate = $(this).val();
      }
      if ($(this).attr("id") == 'load_address') {
        loadAddress = $(this).val();
      }
      if ($(this).attr("id") == 'unload_date') {
        unloadDate = $(this).val();
      }
      if ($(this).attr("id") == 'unload_address') {
        unloadAddress = $(this).val();
      }
      if ($(this).attr("id") == 'signatare') {
        signatare = $(this).val();
      }
      if ($(this).attr("id") == 'document') {
        documents = $(this).val();
      }
      if ($(this).attr("id") == 'route') {
        route = $(this).val();
      }
      if ($(this).attr("id") == 'driver') {
        driver = $(this).val();
      }
      if ($(this).attr("id") == 'transport') {
        transport = $(this).val();
      }
      if ($(this).attr("id") == 'status') {
        status = $(this).val();
      }
      if ($(this).attr("id") == 'manager') {
        manager = $(this).val();
      }
    } else {
      $('#old_rec').css('display', 'block');
      $('#filter_rec').css('display', 'none');
    }
  });


  if ((between != '') || orderDate != null || orderNumber != null || customer != null || pricebook != null || type != null || loadDate != null || loadAddress != null || unloadDate != null || unloadAddress != null || signatare != null || documents != null || route != null || driver != null || transport != null || status != null || manager) {
    let no_records = $('#no_records');

    $('[data-toggle=popover]').each(function () {
      $(this).popover('hide');
    });
    $.ajax({
      url: "/modules/Salesordersdocuments/ajax/filter_invoices_without_docs.php",
      type: "POST",
      data: { between: between, filterBy: filterBy, orderDate: orderDate, orderNumber: orderNumber, customer: customer, pricebook: pricebook, type: type, loadDate: loadDate, loadAddress: loadAddress, unloadDate: unloadDate, unloadAddress: unloadAddress, signatare: signatare, documents: documents, route: route, driver: driver, transport: transport, status: status, manager: manager },
      dataType: 'JSON',
      beforeSend: function () {
        $('#submit-invoice').css('opacity', '0.2');
        $("#wait").show();
        no_records.hide();
      },
      success: function (data) {     
        if (data == 'no_results') {
          $("#wait").hide();
          $('#filter-records').remove();
          $('#firstRecords').hide();
          document.querySelector('.pagination').classList.add('hide');
          $('#submit-invoice').css('opacity', '1');
          no_records.show();
          $('#filter_rec').html(`Viso: 0`);
          $('#old_rec').css('display', 'none');
          $('#filter_rec').css('display', 'block');
        } else if (data == 'empty') {
          $('#filter-records').remove();
          $("#wait").hide();
          $('#submit-invoice').css('opacity', '1');
          no_records.hide();
          $('#filter_rec').html(`Viso: 0`);
          $('#old_rec').css('display', 'none');
          $('#filter_rec').css('display', 'block');

        } else {
          no_records.hide();
          $('#firstRecords').hide();
          let table = $('#listview-table');
          let tbody = document.createElement('tbody');
          tbody.setAttribute('id', 'filter-records');
          let html = '';
          let index = 1;
          if (data.orders != null) {
            $('#filter_rec').html(`Viso: ${data.orders.length} / ${data.filtered_count}`);
            $('#old_rec').css('display', 'none');
            $('#filter_rec').css('display', 'block');
            data.orders.forEach(record => {
              let status;
              let documents = new Array();
              switch (record.sostatus) {
                case 'delivered':
                  status = 'Atliktas';
                  break;
                case 'in progress':
                  status = 'Vykdomas';
                  break;
                case 'Approved':
                  status = 'Priimtas';
                  break;
                default:
                  status = 'Naujas';
              }
              let salesorderid = record.salesorderid;
              let pod_url = data.pod_url[salesorderid];
              let pod_title = data.pod_title[salesorderid];
              let pod_date = data.pod_date[salesorderid];

              let doc_url = data.doc_url[salesorderid];
              let doc_title = data.doc_title[salesorderid];
              let doc_date = data.doc_date[salesorderid];

              let checkTransit = data.check_transit[salesorderid];
              let revised_cargos = new Array();
              if(record.order_cargos != null){
                revised_cargos = record.order_cargos.split('#:#');
              }

              let termo_icon_block = '';
              if(record.termo === '1'){
                termo_icon_block = '<i class="fa fa-fire" title="Termo užsakymas" style="color: blue; margin-left: 0.25rem;"></i>';
              }


              let revised_measures_list_block = '';
              if(data.revised_measures[record.salesorderid] !== undefined && data.revised_measures[record.salesorderid].length > 0){
                  $.each(data.revised_measures[record.salesorderid], function(index, element){
                      revised_measures_list_block += `<li style='list-style: none;'>${element}</li>`;
                  });
              }

              html += `
              <tr class="listViewContentHeader" style="cursor:pointer;${(record.update_status == 0) ? ' background:##faffcf;' : ''}" onClick="action(event);">
                <td class="listViewEntryValue"><input type="checkbox" name="list" value="" class="listViewEntriesCheckBox"/></td>
                <td class="listViewEntryValue"><button class="btn btn-success btn-sm docs_received" ondblclick="docsReceive(${record.salesorderid},event);">Dokumentai gauti</button></td>
                <td class="listViewEntryValue">${(record.invoice_type != null) ? '<i class="fa fa-history" title="Yra išankstinė saskaita" style="font-size: 15px;"></i>' : ''} ${record.createdtime}</td>
                  <input type="hidden" class="order_id" value="${record.salesorderid}"/> 
                  <td class="listViewEntryValue">${(record.shipment_code != '') ? `<a target="_blank" href="/index.php?module=SalesOrder&view=Detail&record=${record.salesorderid}">` + record.shipment_code + '</a>' : '---'}</td>
                <td class="listViewEntryValue">${record.accountname}</td>
                  <input type="hidden" class="user_id" value="${record.accountid}"/>  
                <td class="listViewEntryValue pricebook" title="${(record.pricebook_details != '' ? record.pricebook_details : '')}">${record.pricebook}</td>       
                <td class="listViewEntryValue">${record.type} ${(checkTransit == 0 ? '<span><i class="fa fa-flag-checkered" title="Užsakymas ne lietuvos teritorijoje" style="color: crimson;"></i></span>' : '')}${termo_icon_block}</td>       
                <td class="listViewEntryValue">${record.load_date_from}</td>
                <td class="listViewEntryValue" title="${(record.location_from ? record.location_from : '')}">${record.load_company} ${record.bill_street} ${record.bill_city} ${record.bill_code}</td>
                <td class="listViewEntryValue">${record.unload_date_from}</td>
                <td class="listViewEntryValue" title="${(record.location_to ? record.location_to : '')}">${record.unload_company} ${record.ship_street} ${record.ship_city} ${record.ship_code}</td>
                <td class="listViewEntryValue">${(record.ordered_weight != null) ? record.quantity + "/" + record.ordered_weight + "/" + record.ordered_volume+"/"+record.ordered_pll : '---'}</td>              
                <td class="listViewEntryValue">
                  <span rel="popover2" data-toggle="popover" data-content="
                      <ul>${revised_measures_list_block}</ul>">${(record.revised_weight != null) ? record.revised_quantity + "/" + record.revised_weight + "/" + record.revised_volume+"/"+record.revised_pll : '---'}
                  </span>
                </td>

              <td class="listViewEntryValue taxable taxable_${record.salesorderid}">`;
              if(record.show_taxable == 1){
                html +=  `<b>${(record.taxable_weight != null) ? record.taxable_quantity + "/" + record.taxable_weight + "/" + (record.taxable_volume != null ? record.taxable_volume : record.volume)+"/"+record.taxable_pll : '---'}</b>
                <i class="fa fa-exclamation-circle ${(record.meters > 0 ? '' : 'hide')}" title="Kaina paskaičiuota atsižvelgiant į metrus" style="font-size: 20px;color:#fc631a;"></i>`;
              }else{                
                html += `<span class="taxable_popover" rel="popover3" data-toggle="popover"
                data-content="
                  <p title='Kainų skaičiavimo metu nebuvo nustatyta kainyno kaina, todėl reikia pasirinkti kurie matmenys bus apmokestinami'>Pasirinkite matmenis</p>         
                  <button class='btn btn-warning btn-sm' data-dimensions='${record.quantity + "/" + record.ordered_weight + "/" + record.ordered_volume+"/"+record.ordered_pll}' onclick='setTaxableDimensionsOrdered(this,${record.salesorderid});'>Užsakyti</button> 
                  <button class='btn btn-success btn-sm' data-dimensions='${record.revised_quantity + "/" + record.revised_weight + "/" + record.revised_volume+"/"+record.revised_pll}' onclick='setTaxableDimensionsRevised(this,${record.salesorderid});'>Faktiniai</button>
                "> <i class="fa fa-ellipsis-h ellipsis-style"></i> </span>`;             
              }
              html +=  `</td>
              <td class="listViewEntryValue documents">`;
                if (pod_url != null) {
                  html += `<span id="${index}" rel="popover2" data-toggle="popover" 
              data-content="<ul>`;
                  signatare_url = pod_url.split('|##|');
                  if (pod_title != null) {
                    signatare_title = pod_title.split('|##|');
                  }
                  if (pod_date != null) {
                    signatare_date = pod_date.split('|##|');
                  }
                  for (let i = 0; i < signatare_url.length; i++) {
                    html += `<li style='list-style: none;'>
                  <a href='${signatare_url[i]}&annotate=true'>${(signatare_title[i])}${(signatare_date[i] == undefined || signatare_date[i] == '' ? '' : ', ' + signatare_date[i])}</a>
                    </li>`;
                  }
                  html += `</ul>"><i class="fa fa-picture-o pop" style="font-size: 20px;"></i></span>`;
                }
              html += `</td>
              <td class="listViewEntryValue documents">`;
              if (doc_url != null) {
                html += `<span class="pop" id="${index}" rel="popover2" data-toggle="popover"  style="color:green;"
              data-content="<ul>`;
                documents = doc_url.split('|##|');
                if (doc_title != null) {
                  documents_title = doc_title.split('|##|');
                }
                if (doc_date != null) {
                  documents_date = doc_date.split('|##|');
                }
                for (let i = 0; i < documents.length; i++) {
                  html += `<li style='list-style: none;'>
                  <a href='${documents[i]}&annotate=true'>${documents_title[i]}${(documents_date[i] == undefined || documents_date == '' ? '' : ', ' + documents_date[i])}</a>
                  </li>`;
                }
                html += `</ul>">${(record.minifest == 1 || record.minifest == 2) ? 'KV' : ''} ${(record.cmr == 1 || record.cmr == 2) ? 'CMR' : ''} ${(record.invoice == 1 || record.invoice == 2) ? 'SF' : ''}</span>`;
              } else {
                html += `<span style="color:red;">${(record.minifest == 1 || record.minifest == 2) ? 'KV' : ''} ${(record.cmr == 1 || record.cmr == 2) ? 'CMR' : ''} ${(record.invoice == 1 || record.invoice == 2) ? 'SF' : ''}</span>`;
              }
              html += `  
               ${(record.minifest == 2 || record.cmr == 2 || record.invoice == 2 ? `<i class='fa fa-check-circle' title="Dokumentų originalai gauti" style="font-size: 20px;color: #52d652;"></i>` : '')}      
               </td>
                <td class="listViewEntryValue">${'---'}</td>
                <td class="listViewEntryValue change_price">
                  <img id="spin${index}" style="display: none;" src="/resources/ajax-loader.gif">            
                  <span id="${index}" class="price-bold amount${index}" rel="popover" data-toggle="popover" data-content='<input id="price_value${index}" style="width:55px; display: flex; margin-left: 3px;" value="${record.totalprice}"> <button style="margin-top:5px" class="btn btn-success btn-sm" id="save_price${index}">Saugoti</button>'>${record.totalprice}</span> 
                  ${(record.agreed_price > 0) ? `<span title="Sutarta kaina">* ${(record.warning == 1 ? '<i style="color:red;" title="Kažkuris faktinis matmuo 10% didesnis už užsakyta">???</i>' : '')}</span>` : `<i class="fa fa-calculator" title="Kainos perskaičiavimas" data-spinid="${index}" onclick="reCount(${record.salesorderid},event);"></i>`}
                </td>
                <td class="listViewEntryValue">${(record.route != null && record.route != '') ? record.route : '---'} </td>
                <td class="listViewEntryValue">${(record.driver != null && record.driver != '') ? record.driver : '---'}</td>
                <td class="listViewEntryValue">${(record.transport != null && record.transport != '') ? record.transport : '---'}</td>
                <td class="listViewEntryValue">${status}</td>
                <td class="listViewEntryValue">${record.first_name} ${record.last_name}</td>
              </tr>`;
              index++;
            });
          }
          if ($('#filter-records') != null) {
            $('#filter-records').remove();
            tbody.innerHTML = html;
          } else {
            tbody.innerHTML = html;
          }
          table.append(tbody);
          document.querySelector('.pagination').classList.add('hide');
          setTimeout(() => {
            $('#submit-invoice').css('opacity', '1');
            $("#wait").hide();
            loadOnHoverPopover();
            initTaxableDimensionsPopUp();
          }, 1000);
        }
      }
    });
  } else {
    $('#firstRecords').show();
    document.querySelector('.pagination').classList.remove('hide');
  }

}


function searchByPeriod() { 
  // searchFilter(); 
  $('#no_records').hide();
  $('#submit-invoice').css('opacity', '0.2');
  $("#wait").show();
  $('#submit-invoice').submit();
}

