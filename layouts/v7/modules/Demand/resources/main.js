$(document).ready(function(){
  $('#addNewBlock').on('click', function () {
    let num = $('.main-block').length + 1;
    let appendTo = $('.block-parent');
    let copyElement = $('.main-block-CloneCopy').clone(true, true);
    copyElement.removeClass('hide main-block-CloneCopy');
    copyElement.addClass('main-block');
  
    copyElement.find('#source0').attr('id', `source${num}`).attr('name', `source${num}`);
    copyElement.find('#price0').attr('id', `price${num}`).attr('name', `price${num}`);
    copyElement.appendTo(appendTo);
    $('#number_of_sources').val(num);
  });

  $('.remove').on('click', function(){
    $(this).parent().parent().remove();
    setTimeout(() => {
      updateNames();
    }, 100);
  });
});


function updateNames(){
  let num = $('.main-block').length;
  for(let i = 0; i < num; i++){
    document.querySelectorAll('.main-block .source')[i].id = `source${i+1}`;
    document.querySelectorAll('.main-block .price')[i].id = `price${i+1}`;
    document.querySelectorAll('.main-block .source')[i].name = `source${i+1}`;
    document.querySelectorAll('.main-block .price')[i].name = `price${i+1}`;
  }
  $('#number_of_sources').val(num);
}