<link rel="stylesheet" href="layouts/v7/modules/Storagepricebook/resources/style.css" type="text/css" />


{assign var=pricebooks value=$pricebooks}

  <div id="table-content" class="table-container">
    {if $operation_created}
      <div class="alert alert-success text-center">Nauja operacija sukurta sėkmingai</div>
    {/if}
  <table id="listview-table" class="table listview-table">
      <thead>
        <tr class="listViewContentHeader">
          <th>Pavadinimas</th>
          <th>Sukūrė</th>
          <th>Sukūrimo laikas</th>
          <th>Pakeitimo laikas</th>
        </tr>
    </thead>
    <tbody>
    {$count = 1}      
      {foreach from=$pricebooks item=pricebook}  
      <tr onclick="action(event);" style="cursor: pointer;">
          <td style="padding: 10px; width: 20%;"  class="listViewEntryValue"><a href="index.php?module=Storagepricebook&view=PriceBook&record={$pricebook['storagepricebookid']}"> {$pricebook['pricebookname']}</a></td>          
          <td style="padding: 10px;" class="listViewEntryValue">{$pricebook['creator']}</td> 
          <td style="padding: 10px;" class="listViewEntryValue">{$pricebook['createdtime']}</td> 
          <td style="padding: 10px;" class="listViewEntryValue">{$pricebook['modifiedtime']}</td> 
      </tr>  
      {assign var=count value=$count+1}        
      {/foreach}   
    </tbody>
  </table>  
        {if $count <= 1}<div id="no_records" style="text-align: center;"><h5>Nieko nerasta</h5></div>{/if} 
  </div>

