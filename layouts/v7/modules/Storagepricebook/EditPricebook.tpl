{* <link rel="stylesheet" href="layouts/v7/modules/{$MODULE}/resources/style.css" type="text/css" /> *}
<script src="layouts/v7/modules/{$MODULE}/resources/add.js"></script> 

{assign var=CLASIFICATOR value=$clasificator}
{assign var=MEASURE value=$measure}
{assign var=PRICEBOOK value=$pricebook}
{assign var=PRICEBOOKINFO value=$priceBookInfo}
{assign var=ACCOUNT_ID value=$account_id}
{assign var=NUMROWS value=$num_rows}
{assign var=type value=$type}
{assign var=managers value=$MANAGERS}
{assign var=ROLE value=$role}

{assign var=PRICEBOOKNAME value=$PRICEBOOKINFO['pricebookname']}
{assign var=ACCOUNTNAME value=$PRICEBOOKINFO['accountname']}
{assign var=ACCOUNTID value=$PRICEBOOKINFO['accountid']}

{if $PRICEBOOKNAME == 'BAZINIS' && !in_array($ROLE,$MANAGERS) && $type != 'dublicate'}
  <div>Neturite teisių redaguoti bazinį kainyną</div>
{else}

{if $type == 'dublicate'}
  {assign var=ACCOUNTNAME value=''}
{/if}

<form method="POST" id="pricebook_form">
  <div style="float:left;margin-left: 5%;">   
    <label style="display: block;margin-top:20px;">Kainoraščio pavadinimas</label>  
    {if $PRICEBOOKNAME == 'BAZINIS' && $type == 'edit'}
      <h3>{$PRICEBOOKNAME}</h3>
      <input type="hidden" name="pricebook_name" value="{$PRICEBOOKNAME}">
    {else}
      <input class="inputElement" style="width: 400px;" name="pricebook_name" autocomplete="off" value="{if $type == 'dublicate'}{else}{$PRICEBOOKNAME}{/if}" required>
    {/if}

  {if $PRICEBOOKNAME != 'BAZINIS' OR $type == 'dublicate'}
    <label style="display: block;margin-top:20px;">Klientas</label>
    <div class="referencefield-wrapper" id="account_parent">     
      <input id="popupReferenceModule" type="hidden" value="Accounts">     
      <div class="input-group" style="width: 240px;">     
        <input name="account_id" type="hidden" value="{$ACCOUNTID}" class="sourceField">       
        <input id="account_id_display" name="account_id_display" data-fieldname="account_id" data-fieldtype="reference" type="text" class="marginLeftZero autoComplete inputElement ui-autocomplete-input" value="{$ACCOUNTNAME}" placeholder="Rašykite, norėdami pasinaudoti paieška" data-rule-required="true" data-rule-reference_required="true" autocomplete="off" aria-required="true" aria-invalid="true" data-hasqtip="12" aria-describedby="qtip-12" {if $ACCOUNTNAME} disabled="disabled"{/if}><a href="#" class="clearReferenceSelection {if !$ACCOUNTNAME}hide{/if}"> x </a>
      </div>
    </div>
  {else}
      <input name="account_id" type="hidden" value="" class="sourceField">  
  {/if}


  </div>  

  <div class="container"> 
      <input type="hidden" name="edit" value="1">
      <table id="listview-table" class="table listview-table" style="margin-top: 40px;margin-bottom: 20px;">
        <thead>       
          <tr class="listViewContentHeader">
            <th>Sandėlio operacija</th>  
            <th style="width: 200px !important;">Mato vnt.</th>                  
            <th style="width: 100px !important;">Įkainis</th>                                  
            <th style="width: 20px;"></th>                                  
          </tr>	
        </thead>        
        <tbody id="tbody"> 
         <tr class="listViewEntriesCopy hide">
              <td>
                <select class="inputElement operation" name="operation">
                  {foreach from=$CLASIFICATOR item=item}
                    <option value="{$item['id']}">{$item['title']}</option>
                  {/foreach}
                </select>
              </td>
              <td>
                <select class="inputElement unit" name="unit">
                  {foreach from=$MEASURE item=row}
                    <option value="{$row['id']}">{$row['code']}</option>
                  {/foreach}
                </select>
              </td>
              <td><input type="text" class="form-control" name="fee" placeholder="1.2" onkeypress="return isNumber(event)"></td>
              <td><i class="fa fa-trash remove" style="margin-left: 10px;"></i></td>
           </tr>

         {$count = 1}
           {foreach from=$PRICEBOOK item=book} 
              <tr class="listViewEntries">
                <td>
                  <select class="inputElement operation" name="operation{$count}">
                    {foreach from=$CLASIFICATOR item=item}
                      <option value="{$item['id']}" {if $item['id'] eq $book['operation_id']}selected{/if}>{$item['title']}</option>
                    {/foreach}
                  </select>
                </td>
                <td>
                  <select class="inputElement unit" name="unit{$count}">
                    {foreach from=$MEASURE item=row}
                      <option value="{$row['id']}" {if $row['id'] eq $book['unit']}selected{/if}>{$row['code']}</option>
                    {/foreach}
                  </select>
                </td>
                <td><input type="text" class="form-control fee" name="fee{$count}" placeholder="4.8" onkeypress="return isNumber(event)" value="{$book['fee']}"></td>
                <td><i class="fa fa-trash remove" style="margin-left: 10px;"></i></td>
              </tr>	
                {assign var=count value=$count+1}
          {/foreach}
        </tbody>   
      </table>
    <div class='modal-overlay-footer clearfix' style="border-left: unset !important;">
        <div class="row clearfix">
            <div class='textAlignCenter col-lg-12 col-md-12 col-sm-12'>
            <button class="btn btn-success saveButton" type="submit" form="pricebook_form">{vtranslate('LBL_SAVE', $MODULE)}</button> &nbsp;&nbsp;
                <a class='cancelLink' href="javascript:history.back()">{vtranslate('LBL_CANCEL', $MODULE)}</a>
            </div>
        </div>
    </div>
      <button type="button" class="btn btn-primary btn-group" id="add_field_btn" style="margin-bottom:200px;">Pridėti eilutę</button> <input id="lines" type="hidden" name="lines" value="1">
      <input id="num_rows" type="hidden" name="num_rows" value="{$NUMROWS}">
  </div>
</form>

{/if}
