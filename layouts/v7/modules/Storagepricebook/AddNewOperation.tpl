<form method="POST" id="pricebook_form">
 

  <div class="container"> 
   <div class="form-group">
      <input type="hidden" name="addNewOperation" value="1">
      <label for="operation_name">Operacijos pavadinimas</label>
      <input id="operation_name" class="form-control" name="operation_name" type="text">
  </div>
    <div class='modal-overlay-footer clearfix' style="border-left: unset !important;">
        <div class="row clearfix">
            <div class='textAlignCenter col-lg-12 col-md-12 col-sm-12'>
            <button class="btn btn-success saveButton" type="submit" form="pricebook_form">{vtranslate('LBL_SAVE', $MODULE)}</button> &nbsp;&nbsp;
                <a class='cancelLink' href="javascript:history.back()">{vtranslate('LBL_CANCEL', $MODULE)}</a>
            </div>
        </div>
    </div>        
  </div>
</form>
