function checkHide() {
	return document.querySelector('.app-menu.hide');
}

function toggleAppMenu(type) {
	var appMenu = jQuery('.app-menu');
	var appNav = jQuery('.app-nav');
	appMenu.appendTo('#page');
	appMenu.css({
		'top': appNav.offset().top + appNav.height(),
		'left': 0
	});
	if (typeof type === 'undefined') {
		type = appMenu.is(':hidden') ? 'show' : 'hide';
	}
	if (type == 'show') {
		document.querySelector('.app-menu').classList.remove('hide');
		appMenu.show(200, function () { });
	} else {
		document.querySelector('.app-menu').classList.add('hide');
		appMenu.hide(200, function () { });
	}
}

jQuery('.app-modules-dropdown-container').hover(function (e) {
	var dropdownContainer = jQuery(e.currentTarget);
	jQuery('.dropdown').removeClass('open');
	if (dropdownContainer.length) {
		if (dropdownContainer.hasClass('dropdown-compact')) {
			dropdownContainer.find('.app-modules-dropdown').css('top', dropdownContainer.position().top - 8);
		} else {
			dropdownContainer.find('.app-modules-dropdown').css('top', '');
		}
		dropdownContainer.addClass('open').find('.app-item').addClass('active-app-item');
	}
}, function (e) {
	var dropdownContainer = jQuery(e.currentTarget);
	dropdownContainer.find('.app-item').removeClass('active-app-item');
	setTimeout(function () {
		if (dropdownContainer.find('.app-modules-dropdown').length && !dropdownContainer.find('.app-modules-dropdown').is(':hover') && !dropdownContainer.is(':hover')) {
			dropdownContainer.removeClass('open');
		}
	}, 500);

});

jQuery('.app-item').on('click', function () {
	var url = jQuery(this).data('defaultUrl');
	if (url) {
		window.location.href = url;
	}
});


function initSideBar() {
  jQuery('.app-trigger, .app-icon, .app-navigator').on('click', function (e) {
    e.stopPropagation();
    if (checkHide()) {
      toggleAppMenu('show');
    } else {
      toggleAppMenu('hide');
    }
  });

  jQuery('body').on('click', function (e) {
    if (!checkHide()) {
      toggleAppMenu('hide');
    }
  });
}

function isNumber(evt) {
  let charCode = (event.which) ? event.which : event.keyCode;
  evt.target.value = evt.target.value.replace(/,/g, '.');


  if (charCode != 46 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  } else {
    //if dot sign entered more than once then don't allow to enter dot sign again. 46 is the code for dot sign
    let parts = evt.srcElement.value.split('.');

    if (parts.length > 1 && (charCode == 46 || charCode == 44)) {
      return false;
    }
    return true;
  }
}


function registerAutoCompleteFields() {

  $('#account_id_display').autocomplete({
    'minLength' : '3',
    'source' : function(request, response){
      //element will be array of dom elements
      //here this refers to auto complete instance
      var inputElement = jQuery(this.element[0]);
      var searchValue = request.term;
      var params = getReferenceSearchParams(inputElement);
      params.module = app.getModuleName();
      if (jQuery('#QuickCreate').length > 0) {
        params.module = container.find('[name="module"]').val();
      }
      params.search_value = searchValue;
      if(params.search_module && params.search_module!= 'undefined') {
        searchModuleNames(params).then(function(data){
          var reponseDataList = new Array();
          var serverDataFormat = data;
          if(serverDataFormat.length <= 0) {
              jQuery(inputElement).val('');
              serverDataFormat = new Array({
                  'label' : 'No Results Found',
                  'type'	: 'no results'
              });
          }
          for(var id in serverDataFormat){
              var responseData = serverDataFormat[id];
              reponseDataList.push(responseData);
          }
          response(reponseDataList);
        });
      } else {
        jQuery(inputElement).val('');
        serverDataFormat = new Array({
          'label' : 'No Results Found',
          'type'	: 'no results'
        });
        response(serverDataFormat);
      }
    },
    'select' : function(event, ui ){
      var selectedItemData = ui.item;
      //To stop selection if no results is selected
      if(typeof selectedItemData.type != 'undefined' && selectedItemData.type=="no results"){
          return false;
      }
      var element = jQuery(this);
      var parent = $('#account_parent');

      $(this).trigger("blur"); // itoma
      var sourceField = parent.find('.sourceField');
      selectedItemData.record = selectedItemData.id;
      selectedItemData.source_module = parent.find('input[name="popupReferenceModule"]').val();
      selectedItemData.selectedName = selectedItemData.label;
      var fieldName = sourceField.attr("name");
      parent.find('input[name="'+fieldName+'"]').val(selectedItemData.id);
      $('#'+fieldName+'_hidden_id').val(selectedItemData.id); // itoma
      element.attr("value",selectedItemData.id);
      element.data("value",selectedItemData.id);
      parent.find('.clearReferenceSelection').removeClass('hide');
      parent.find('.referencefield-wrapper').addClass('selected');
      element.attr("disabled","disabled");
      registerClearReferenceSelectionEvent(parent);
      //trigger reference field selection event
      sourceField.trigger(Vtiger_Edit_Js.referenceSelectionEvent,selectedItemData);
      //trigger post reference selection
      sourceField.trigger(Vtiger_Edit_Js.postReferenceSelectionEvent,{'data':selectedItemData});
    }
  });
}

function getReferenceSearchParams(element){
  var params = {};
  var referenceModuleElement = jQuery('#popupReferenceModule');
  var searchModule = referenceModuleElement.val();
  params.search_module = searchModule;
  return params;
}

function searchModuleNames(params) {
  var aDeferred = jQuery.Deferred();

  if(typeof params.module == 'undefined') {
    params.module = 'Accounts';
  }

  if(typeof params.action == 'undefined') {
    params.action = 'BasicAjax';
  }

  if(typeof params.base_record == 'undefined') {
    var record = jQuery('[name="record"]');
    var recordId = app.getRecordId();
    if(record.length) {
      params.base_record = record.val();
    } else if(recordId) {
      params.base_record = recordId;
    } else if(app.view() == 'List') {
      var editRecordId = jQuery('#listview-table').find('tr.listViewEntries.edited').data('id');
      if(editRecordId) {
        params.base_record = editRecordId;
      }
    }
  }
  app.request.get({data:params}).then(
    function(err, res){
      aDeferred.resolve(res);
    },
    function(error){
      //TODO : Handle error
      aDeferred.reject();
    }
  );
  return aDeferred.promise();
}

function registerClearReferenceSelectionEvent(container) {
  container.off('click', '.clearReferenceSelection');
  container.on('click', '.clearReferenceSelection',function(e){
    e.preventDefault();
    var element = jQuery(e.currentTarget);  
    var inputElement = container.find('.inputElement');
    var fieldName = container.find('.sourceField').attr("name");
    container.find('.referencefield-wrapper').removeClass('selected');
    inputElement.removeAttr("disabled").removeAttr('readonly');
    inputElement.attr("value","");
    inputElement.data('value','');
    inputElement.val("");
    container.find('input[name="'+fieldName+'"]').val("");
    element.addClass('hide');
    element.trigger(Vtiger_Edit_Js.referenceDeSelectionEvent);
  });
}
