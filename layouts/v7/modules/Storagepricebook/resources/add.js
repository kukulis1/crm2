$(document).ready(function () {
  initSideBar();
  registerAutoCompleteFields();

  $('#add_field_btn').on('click', function () {
    let listElements = $('.listViewEntries').length + 1;
    let row = $(".listViewEntriesCopy").clone(true).removeClass('hide listViewEntriesCopy').addClass('listViewEntries');
    row.find('[name="operation"]').attr('name', `operation${listElements}`);
    row.find('[name="unit"]').attr('name', `unit${listElements}`);
    row.find('[name="fee"]').attr('name', `fee${listElements}`);
    row.appendTo("#tbody");
    $('#num_rows').val(listElements);
  });

  $('.operation').on('change', function () {
    let value = $(this).val();
    if (value == 1) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 2) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 3) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 4) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 5) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 6) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 7) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 8) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 9) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 10) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 11) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 12) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 13) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 14) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 15) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 16) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 17) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 18) {
      $(this).parent().parent().find('.unit').val(10);
    } else if (value == 19) {
      $(this).parent().parent().find('.unit').val(1);
    } else if (value == 20) {
      $(this).parent().parent().find('.unit').val(1);
    }
  });

  $('.remove').on('click', function () {
    $(this).parent().parent().remove();
    let listElements = $('.listViewEntries').length
    $('#num_rows').val(listElements);
    for (let i = 0; i < listElements; i++) {
      document.querySelectorAll('.listViewEntries')[i].querySelector('.operation').name = `operation${i + 1}`;
      document.querySelectorAll('.listViewEntries')[i].querySelector('.unit').name = `unit${i + 1}`;
      document.querySelectorAll('.listViewEntries')[i].querySelector('.fee').name = `fee${i + 1}`;
    }
  });

});
