<link rel="stylesheet" href="layouts/v7/modules/{$MODULE}/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/{$MODULE}/resources/pricebook.js?u=1"></script> 

{assign var=PRICEBOOK value=$pricebook}
{assign var=PRICEBOOKINFO value=$priceBookInfo}
{assign var=PRICEBOOKNAME value=$PRICEBOOKINFO['pricebookname']}
{assign var=ACCOUNTNAME value=$PRICEBOOKINFO['accountname']}

  <div style="float:left;margin-left: 5%;">   
   <h3>{$PRICEBOOKNAME}</h3>
   {if $ACCOUNTNAME}
    <p>Klientas: {$ACCOUNTNAME}<p>
   {/if}
  </div>  

      <table id="listview-table" class="table listview-table" style="margin-top: 40px;margin-bottom: 20px;">
        <thead>       
          <tr class="listViewContentHeader">
            <th>Sandėlio operacija</th>  
            <th>Mato vnt.</th>                  
            <th>Įkainis</th>                                  
          </tr>	
        </thead>        
        <tbody> 
          {foreach from=$PRICEBOOK item=row}         
            <tr class="listViewEntries">
              <td>{$row['operation']}</td>
              <td>{$row['measure']}</td>
              <td>{$row['fee']}</td>
            </tr>	
          {/foreach}
        </tbody>   
      </table>     
  </div>

