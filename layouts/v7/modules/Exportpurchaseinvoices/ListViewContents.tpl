{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Vtiger/views/List.php *}

{* START YOUR IMPLEMENTATION FROM BELOW. Use {debug} for information *}
		{include file="PicklistColorMap.tpl"|vtemplate_path:$MODULE}
		<div class="col-sm-12 col-xs-12 ">
			{if $MODULE neq 'EmailTemplates' && $SEARCH_MODE_RESULTS neq true}
			{assign var=LEFTPANELHIDE value=$CURRENT_USER_MODEL->get('leftpanelhide')}

			<div class="essentials-toggle" title="{vtranslate('LBL_LEFT_PANEL_SHOW_HIDE', 'Vtiger')}">
				<span
					class="essentials-toggle-marker fa {if $LEFTPANELHIDE eq '1'}fa-chevron-right{else}fa-chevron-left{/if} cursorPointer"></span>
			</div>
			{/if}
	
			{if !empty($PICKIST_DEPENDENCY_DATASOURCE)}
			<input type="hidden" name="picklistDependency"
				value='{Vtiger_Util_Helper::toSafeHTML($PICKIST_DEPENDENCY_DATASOURCE)}' />
			{/if}
			{if !$SEARCH_MODE_RESULTS}		
			{include file="ListViewActions.tpl"|vtemplate_path:$MODULE}
			{/if}
	
			<div id="table-content" class="table-container">
				<form name='list' id='listedit' action='' onsubmit="return false;">		
					<table id="listview-table"
						class="table {if $LISTVIEW_ENTRIES_COUNT eq '0'}listview-table-norecords {/if} listview-table ">
						<thead>
							<tr class="listViewContentHeader">
								<th>
									{if !$SEARCH_MODE_RESULTS}
									<div class="table-actions">
										<div class="dropdown" style="float:left;">
										<span class="input dropdown-toggle" data-toggle="dropdown"
												title="{vtranslate('LBL_CLICK_HERE_TO_SELECT_ALL_RECORDS',$MODULE)}">
												<input class="listViewEntriesMainCheckBox" type="checkbox" onclick="checkAllBox();">
											</span>
											<span class="input dropdown-toggle" data-toggle="dropdown"
												title="{vtranslate('LBL_CLICK_HERE_TO_SELECT_ALL_RECORDS',$MODULE)}">												
											</span>
										</div>
										{if $MODULE_MODEL->isFilterColumnEnabled()}
										<div id="listColumnFilterContainer">			
											<div	class="listColumnFilter {if $CURRENT_CV_MODEL and !($CURRENT_CV_MODEL->isCvEditable())}disabled{/if}"
												{if $CURRENT_CV_MODEL->isCvEditable()}
												title="{vtranslate('LBL_CLICK_HERE_TO_MANAGE_LIST_COLUMNS',$MODULE)}"
												{else}
												{if $CURRENT_CV_MODEL->get('viewname') eq 'All' and !$CURRENT_USER_MODEL->isAdminUser()}
												title="{vtranslate('LBL_SHARED_LIST_NON_ADMIN_MESSAGE',$MODULE)}"
												{elseif !$CURRENT_CV_MODEL->isMine()}
												{assign var=CURRENT_CV_USER_ID value=$CURRENT_CV_MODEL->get('userid')}
												{if !Vtiger_Functions::isUserExist($CURRENT_CV_USER_ID)}
												{assign var=CURRENT_CV_USER_ID value=Users::getActiveAdminId()}
												{/if}
												title="{vtranslate('LBL_SHARED_LIST_OWNER_MESSAGE',$MODULE,
												getUserFullName($CURRENT_CV_USER_ID))}"
												{/if}
												{/if}
												{if $MODULE eq 'Documents'}style="width: 10%;"{/if}
												data-toggle="tooltip" data-placement="bottom" data-container="body">
												<i class="fa fa-th-large"></i>
											</div>								
										</div>
										{/if}
									</div>
									{elseif $SEARCH_MODE_RESULTS}
									{vtranslate('LBL_ACTIONS',$MODULE)}
									{/if}
								</th>
					
								{foreach item=LISTVIEW_HEADER from=$LISTVIEW_HEADERS}
									{if $SEARCH_MODE_RESULTS || ($LISTVIEW_HEADER->getFieldDataType() eq 'multipicklist')}
									{assign var=NO_SORTING value=1}
									{else}
									{assign var=NO_SORTING value=0}
									{/if}
									<th {if $COLUMN_NAME eq $LISTVIEW_HEADER->get('name')} nowrap="nowrap" {/if}>
										<a href="#" class="noSorting"	data-columnname="{$LISTVIEW_HEADER->get('name')}" data-field-id='{$LISTVIEW_HEADER->getId()}'>
									
										&nbsp;{vtranslate($LISTVIEW_HEADER->get('label'), $LISTVIEW_HEADER->getModuleName())}&nbsp;
									</a>
									{if $COLUMN_NAME eq $LISTVIEW_HEADER->get('name')}
									<a href="#" class="removeSorting"><i class="fa fa-remove"></i></a>
									{/if}
								</th>
								{/foreach}
							</tr>

							{if $MODULE_MODEL->isQuickSearchEnabled() && !$SEARCH_MODE_RESULTS}
							<tr class="searchRow">
								<th class="inline-search-btn">
									<div class="table-actions">
										<button class="btn btn-success btn-sm"
											data-trigger="listSearch">{vtranslate("LBL_SEARCH",$MODULE)}</button>
									</div>
								</th>
							
								{foreach item=LISTVIEW_HEADER from=$LISTVIEW_HEADERS}
								<th>
									{assign var=FIELD_UI_TYPE_MODEL value=$LISTVIEW_HEADER->getUITypeModel()}
									{include file=vtemplate_path($FIELD_UI_TYPE_MODEL->getListSearchTemplateName(),$MODULE) FIELD_MODEL=
									$LISTVIEW_HEADER SEARCH_INFO=$SEARCH_DETAILS[$LISTVIEW_HEADER->getName()]
									USER_MODEL=$CURRENT_USER_MODEL}
									<input type="hidden" class="operatorValue"
										value="{$SEARCH_DETAILS[$LISTVIEW_HEADER->getName()]['comparator']}">
								</th>
								{/foreach}
							</tr>
							{/if}
						</thead>
						<tbody class="overflow-y">

							{foreach item=LISTVIEW_ENTRY from=$LISTVIEW_ENTRIES['listResult'] name=listview}						
								
							<tr class="listViewEntries" data-id="{$LISTVIEW_ENTRY['purchaseorderid']}" style="background:{if !$LISTVIEW_ENTRY['has_iban']}#8dac76;{else if $LISTVIEW_ENTRY['exported_agnum']}#fafad2;{else if !$LISTVIEW_ENTRY['kod']}#f2dede;{/if}"> 															
								<td	class="listViewRecordActions">
								<div class="table-actions">
									<span class="input" >
										<input type="checkbox" value="{$LISTVIEW_ENTRY['purchaseorderid']}" class="listViewEntriesCheckBox"/>
									</span>
								</div>
								</td>
								{* start *}
								{foreach item=LISTVIEW_HEADER from=$LISTVIEW_HEADERS}

								{assign var=LISTVIEW_HEADERNAME value=$LISTVIEW_HEADER->get('name')}	

								<td class="listViewEntryValue" style="cursor:default;">	
									<span class="fieldValue">
									{if $LISTVIEW_HEADERNAME eq 'exportpurchaseinvoices_tks_vendor_id'}
										<span class="fieldValue">
											<span class="value">							
												<a class="js-reference-display-value" href="?module=Vendors&view=Detail&record={$LISTVIEW_ENTRY['vendorid']}" title="Tiekėjai">{$LISTVIEW_ENTRY['vendorname']}</a>
											</span>
										</span>	
									{else if  $LISTVIEW_HEADERNAME eq 'exportpurchaseinvoices_tks_invoice_date'}
										<span class="value">{$LISTVIEW_ENTRY['invoicedate']}</span>
									{else if  $LISTVIEW_HEADERNAME eq 'exportpurchaseinvoices_tks_purchase_no'}
										<span class="fieldValue">
										<span class="value">							
											<a class="js-reference-display-value" target="_blank" href="?module=PurchaseOrder&view=Detail&record={$LISTVIEW_ENTRY['purchaseorderid']}" title="Sąskaita">{$LISTVIEW_ENTRY['purchaseorder_no']}</a>
										</span>
									</span>		
									{else if  $LISTVIEW_HEADERNAME eq 'exportpurchaseinvoices_tks_total'}
										<span class="value">{$LISTVIEW_ENTRY['total']}</span>
									{else if  $LISTVIEW_HEADERNAME eq 'exportpurchaseinvoices_tks_duedate'} 
										<span class="value">{$LISTVIEW_ENTRY['duedate']}</span>
									{else if  $LISTVIEW_HEADERNAME eq 'createdtime'} 
										<span class="value">{$LISTVIEW_ENTRY['createdtime']}</span>	
									{/if}
										</span>						
								</td>					
								{/foreach}

								{* end *}
							</tr>
							{/foreach}




							{if $LISTVIEW_ENTRIES_COUNT eq '0'}
							<tr class="emptyRecordsDiv">
								{assign var=COLSPAN_WIDTH value={count($LISTVIEW_HEADERS)}+1}
								<td colspan="{$COLSPAN_WIDTH}">
									<div class="emptyRecordsContent">
										{assign var=SINGLE_MODULE value="SINGLE_$MODULE"}
										{vtranslate('LBL_RECORDS_IN_MODEL')} {vtranslate($MODULE, $MODULE)} {vtranslate('LBL_FOUND')}.
										{if $IS_CREATE_PERMITTED}
										<a style="color:blue" href="{$MODULE_MODEL->getCreateRecordUrl()}">{vtranslate('LBL_CREATE')}</a>
										{if Users_Privileges_Model::isPermitted($MODULE, 'Import') && $LIST_VIEW_MODEL->isImportEnabled()}
										{vtranslate('LBL_OR', $MODULE)}
										<a style="color:blue" href="#"
											onclick="return Vtiger_Import_Js.triggerImportAction()">{vtranslate('LBL_IMPORT', $MODULE)}</a>
										{vtranslate($MODULE, $MODULE)}
										{else}
										{vtranslate($SINGLE_MODULE, $MODULE)}
										{/if}
										{/if}
									</div>
								</td>
							</tr>
							{/if}
						</tbody>
					</table>
				</form>

			</div>
			<div id="scroller_wrapper" class="bottom-fixed-scroll">
				<div id="scroller" class="scroller-div"></div>
			</div>
		</div>