{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

<style>
	h6 {
		font-size: 16px;
	}

.salesorders a {
		font-size: 13px !important;
	}

.salesorders small {
	font-size: 13px !important;
}
</style>
	{if $REPORTS['orders'] neq false}
	{$orders = implode(',',$REPORTS['orders'])}
	{$pll_orders = implode(',',$REPORTS['pll_orders'])}
	{$days = implode(',', $REPORTS['days'])}
	<div>Užsakymai: {$REPORTS['filter']}</div>
	<div style='padding:5px;'>
		<canvas id="salesorderChar"></canvas>
		<script src="/../resources/Chart.js"></script>  
		<script>
		var ctx = document.getElementById('salesorderChar').getContext('2d');
		var myChart = new Chart(ctx, {
				type: 'line',
				data: {
						labels: [{$days}],
						datasets: [{
								label: 'Pardavimo užsakymai',
								data: [{$orders}],
								backgroundColor: [
										'rgba(255, 99, 132, 0.2)'									
								],
								borderColor: [
										'rgba(255, 99, 132, 1)'									
								],
								borderWidth: 1
						},
							{
								label: 'Pll kiekis užsakymuose',
								data: [{$pll_orders}],
								backgroundColor: [
										'rgb(110, 135, 190)'									
								],
								borderColor: [
										'rgb(45, 110, 212)'									
								],
								borderWidth: 1
							}
					],
				},
				options: {
						scales: {
								yAxes: [{
										ticks: {
												beginAtZero: true
										}
								}]
						}
	
				}
		});
		</script>

 </div>
			{else}
		<span class="noDataMsg">		
				{vtranslate('LBL_NO_SALESORDERS', $MODULE_NAME)}			
		</span>
{/if}