{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

<style>
h6 {
	font-size: 16px;
}

.salesorders a {
	font-size: 13px !important;
}

.salesorders small {
	font-size: 13px !important;
}
</style>



<div class="modal fade" id="ordersModal" tabindex="-1" role="dialog" aria-labelledby="ordersModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ordersModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
					<thead>
					<th>Siuntos Nr.</th>
					<th>Suma</th>
					<th>Klientas</th>
					<th>Sukūrimo laikas</th>
					</thead>
					<tbody id="ordersTbody"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
        {* <button type="button" class="btn btn-primary">Save changes</button> *}
      </div>
    </div>
  </div>
</div>

	{if $REPORTS['orders'] neq false}
	<div style="padding-left: 15px;">Užsakymai: {$REPORTS['filter']}</div>
	   <div style='padding:10px;'>
		{foreach key=$index item=REPORT from=$REPORTS['orders']} 

		<div class="row entry clearfix salesorders" style="padding: 2px 5px;">
			
    	<div class="col-lg-8 pull-left">
				<a type="button"  data-toggle="modal" data-target="#ordersModal" data-salesorderid="{$REPORT['salesorderid']}" onclick="showSalesOrdersByStatus(event);">
  					{vtranslate($REPORT['sostatus'], $MODULE_NAME)}
				</a>			
			</div>  

    	<div class="col-lg-4 pull-right muted"><small title="">{$REPORT['orders_num']}</small></div> 
		</div>
    {/foreach}
	<div class="col-lg-8 pull-left"><h6>Viso:</h6></div>
	<div class="col-lg-4 pull-right muted"><h6>{$REPORTS['count']}</h6></div>
 </div>
			{else}
		<span class="noDataMsg">		
				{vtranslate('LBL_NO_SALESORDERS', $MODULE_NAME)}			
		</span>
{/if}