{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
<link rel="stylesheet" href="layouts/v7/modules/SalesOrder/resources/style.css" type="text/css" />

{strip}
	{assign var="FIELD_INFO" value=$FIELD_MODEL->getFieldInfo()}
	{assign var="SPECIAL_VALIDATOR" value=$FIELD_MODEL->getValidator()}

	{if (!$FIELD_NAME)}
		{assign var="FIELD_NAME" value=$FIELD_MODEL->getFieldName()}
	{/if}
	{* {if $FIELD_MODEL->getFieldName() eq 'load_company' || $FIELD_MODEL->getFieldName() eq 'unload_company'}
	<input id="{$MODULE}_{$FIELD_NAME}" type="text" data-fieldname="{$FIELD_NAME}" data-fieldtype="string" class="inputElement">
	{else} *}
	<input id="{$MODULE}_editView_fieldName_{$FIELD_NAME}" type="text" data-fieldname="{$FIELD_NAME}" data-fieldtype="string" class="inputElement {if $FIELD_MODEL->isNameField()}nameField{/if}" name="{$FIELD_NAME}" value="{$FIELD_MODEL->get('fieldvalue')}"
		{if $FIELD_MODEL->get('uitype') eq '3' || $FIELD_MODEL->get('uitype') eq '4'|| $FIELD_MODEL->isReadOnly()}
			{if $FIELD_MODEL->get('uitype') neq '106'}
				readonly
			{else if $FIELD_MODEL->get('uitype') eq '106' && $MODE eq 'edit'}
				readonly
			{/if}
		{/if}
                
		{if $FIELD_MODEL->getFieldName() eq 'load_company_code'}
				readonly
		{/if}  
                
		{if $FIELD_MODEL->getFieldName() eq 'unload_company_code'}
				readonly
		{/if}
			{if $FIELD_MODEL->getFieldName() eq 'load_company'}  		
				maxlength="50"      
			{/if}    
			{if $FIELD_MODEL->getFieldName() eq 'load_contact'}
					maxlength="50" 
				{/if} 
  
				{if $FIELD_MODEL->getFieldName() eq 'unload_contact'}
					maxlength="50" 
				{/if}

				{if $FIELD_MODEL->getFieldName() eq 'cf_1564'}
					maxlength="50" 
				{/if}
				{if $FIELD_MODEL->getFieldName() eq 'cf_1566'}
					maxlength="50" 
				{/if} 

				{if $FIELD_MODEL->getFieldName() eq 'bill_street'}
					maxlength="255" 
				{/if} 
			 {if $FIELD_MODEL->getFieldName() eq 'ship_street'}
					maxlength="255" 
				{/if}  

				{if $FIELD_MODEL->getFieldName() eq 'bill_city'}
					maxlength="50" 
				{/if} 
								{if $FIELD_MODEL->getFieldName() eq 'ship_city'}
					maxlength="50" 
				{/if} 
          {if $FIELD_NAME eq 'cf_1365' && empty($GET['record'])}style="display:none;"{/if}
			{if $FIELD_MODEL->getFieldName() eq 'bill_code'}  		
				minlength="4"  maxlength="6" onpaste="isString(this,'SalesOrder_editView_fieldName_bill_code');" onkeypress="isString(this,'SalesOrder_editView_fieldName_bill_code');" onkeyup="isString(this,'SalesOrder_editView_fieldName_bill_code');"
			{/if} 

				{if $FIELD_MODEL->getFieldName() eq 'ship_code'}  		
				minlength="4"  maxlength="6" onpaste="isString(this,'SalesOrder_editView_fieldName_ship_code');" onkeypress="isString(this,'SalesOrder_editView_fieldName_ship_code');" onkeyup="isString(this,'SalesOrder_editView_fieldName_ship_code');"
			{/if}      

			{if $FIELD_MODEL->getFieldName() eq 'bill_country'}  		
				minlength="3"  maxlength="3" onpaste="isInt(this,'SalesOrder_editView_fieldName_bill_country');" onkeypress="isInt(this,'SalesOrder_editView_fieldName_bill_country');" onkeyup="isInt(this,'SalesOrder_editView_fieldName_bill_country');"
			{/if}      
				{if $FIELD_MODEL->getFieldName() eq 'ship_country'}
					minlength="3" maxlength="3" onpaste="isInt(this,'SalesOrder_editView_fieldName_ship_country');" onkeypress="isInt(this,'SalesOrder_editView_fieldName_ship_country');" onkeyup="isInt(this,'SalesOrder_editView_fieldName_ship_country');"
				{/if}      
		{if !empty($SPECIAL_VALIDATOR)}data-validator="{Zend_Json::encode($SPECIAL_VALIDATOR)}"{/if}
		{if $FIELD_INFO["mandatory"] eq true} data-rule-required="true" {/if}
		{if count($FIELD_INFO['validator'])}
			data-specific-rules='{ZEND_JSON::encode($FIELD_INFO["validator"])}'
		{/if}

		{if $FIELD_MODEL->getFieldName() eq 'cf_1376'}  		
				onpaste="isString(this,'SalesOrder_editView_fieldName_cf_1376');" onkeypress="isString(this,'SalesOrder_editView_fieldName_cf_1376');" onkeyup="isString(this,'SalesOrder_editView_fieldName_cf_1376');"
			{/if} 

		 {if $FIELD_NAME eq 'cf_1374'}readonly{/if}  />

		 {* {/if} *}
{/strip}
