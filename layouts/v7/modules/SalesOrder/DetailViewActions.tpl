{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}

{strip}
{assign var=CHECK_SALESORDER_INVOICE value=$CHECK_SALESORDER_INVOICE}
{assign var=ROLEID value=$roleid}
{assign var=user_id value=$user_id}

    <div class="col-lg-6 detailViewButtoncontainer">
        <div class="pull-right btn-toolbar">
            <div class="btn-group">

            {if (!$CHECK_SALESORDER_INVOICE && $ROLEID eq 'H24') OR $ROLEID neq 'H24'}
             <button class="btn btn-default" onclick="window.location.href='index.php?module=Invoice&view=Edit&app=SALES&account_id={$user_id}&order_ids={$SALESORDER_ID}&filterBy=salesorderid&pre=1&create'">Išrašyti saskaitą</button>
            {/if}
          
           
            {foreach item=DETAIL_VIEW_BASIC_LINK from=$DETAILVIEW_LINKS['DETAILVIEWBASIC']}              
              {if $DETAIL_VIEW_BASIC_LINK->getLabel() eq 'LBL_EDIT' && !$CHECK_SALESORDER_INVOICE && $ROLEID}
                <button class="btn btn-default" id="{$MODULE_NAME}_detailView_basicAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_BASIC_LINK->getLabel())}"
                        {if $DETAIL_VIEW_BASIC_LINK->isPageLoadLink()}
                            onclick="window.location.href = '{$DETAIL_VIEW_BASIC_LINK->getUrl()}&app={$SELECTED_MENU_CATEGORY}{if Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_BASIC_LINK->getLabel()) eq 'LBL_EDIT' && $RECORD->checkInvoiceType()}&newRecord=true{/if}'"
                        {else}
                            onclick="{$DETAIL_VIEW_BASIC_LINK->getUrl()}"
                        {/if}
                        {if $MODULE_NAME eq 'Documents' && $DETAIL_VIEW_BASIC_LINK->getLabel() eq 'LBL_VIEW_FILE'}
                            data-filelocationtype="{$DETAIL_VIEW_BASIC_LINK->get('filelocationtype')}" data-filename="{$DETAIL_VIEW_BASIC_LINK->get('filename')}"
                        {/if}>
                    {vtranslate($DETAIL_VIEW_BASIC_LINK->getLabel(), $MODULE_NAME)}
                </button>
               {/if}
            {/foreach}            

   {if $DETAILVIEW_LINKS['DETAILVIEW']|@count gt 0}
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                   {vtranslate('LBL_MORE', $MODULE_NAME)}&nbsp;&nbsp;<i class="caret"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    {foreach item=DETAIL_VIEW_LINK from=$DETAILVIEW_LINKS['DETAILVIEW']}
                        {if $DETAIL_VIEW_LINK->getLabel() eq ""} 
                            <li class="divider"></li>	
                            {else}
                            <li id="{$MODULE_NAME}_detailView_moreAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())}">
                            {if {Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())} neq 'Spausdinti'}
                            {if $MODULE_NAME eq 'Invoice' && $DETAIL_VIEW_LINK->getUrl() eq 'none'} 
                               <a href="#" data-toggle="modal" data-target="#sendInvoice" onclick="getUserEmailAndSalesOrderFilesByInvoiceId();">{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>                
                             {else}
                                {if $DETAIL_VIEW_LINK->getUrl()|strstr:"javascript"} 
                                    <a href='{$DETAIL_VIEW_LINK->getUrl()}'>{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>
                                {else}
                                    <a href='{$DETAIL_VIEW_LINK->getUrl()}&app={$SELECTED_MENU_CATEGORY}' >{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>
                                {/if}
                            {/if}

                            {/if}  
                            </li>                           
                                {if {Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())} eq 'Eksportuoti_kaip_PDF'}
                                    <li id="{$MODULE_NAME}_detailView_moreAction_Spausdinti_lipdukus">
                                        <a href="#" data-toggle="modal" data-target="#printStickers">Spausdinti lipdukus</a>
                                    </li>
                                    <li id="{$MODULE_NAME}_detailView_moreAction_Siusti_lipdukus">
                                        <a href="#" data-toggle="modal" onclick="disableToolbox();" data-target="#sendStickers">Siųsti lipdukus</a>
                                    </li>
                                {/if}                          
                        {/if}
                    {/foreach}
                </ul>
            {/if}
            </div>
            {if !{$NO_PAGINATION}}
            <div class="btn-group pull-right">
                <button class="btn btn-default " id="detailViewPreviousRecordButton" {if empty($PREVIOUS_RECORD_URL)} disabled="disabled" {else} onclick="window.location.href = '{$PREVIOUS_RECORD_URL}&app={$SELECTED_MENU_CATEGORY}'" {/if} >
                    <i class="fa fa-chevron-left"></i>
                </button>
                <button class="btn btn-default  " id="detailViewNextRecordButton"{if empty($NEXT_RECORD_URL)} disabled="disabled" {else} onclick="window.location.href = '{$NEXT_RECORD_URL}&app={$SELECTED_MENU_CATEGORY}'" {/if}>
                    <i class="fa fa-chevron-right"></i>
                </button>
            </div>
            {/if}        
        </div>
        <input type="hidden" name="record_id" value="{$RECORD->getId()}">
    </div>

{assign var=CLIENT_EMAIL value=$CLIENT_EMAIL}
{assign var=STICKER_EMAIL_TEMPLATE value=$STICKER_EMAIL_TEMPLATE}
{if $MODULE_NAME eq 'SalesOrder'}   
<div class="modal fade" id="sendStickers" tabindex="-1" role="dialog" aria-labelledby="sendStickerLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 35%;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendInvoiceLabel">Lipdukų siuntimas klientui el. paštu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="sendStickerEmail" enctype="multipart/form-data">
          <div style="display: none;" id="send_labels_alert" class="alert alert-danger">
          </div>
         <div class="form-group">
          <input name="emailAddress" type="email" value="{$CLIENT_EMAIL}" class="form-control" placeholder="Kliento el. pašto adresas" required>
            </div>
          <div class="form-group">
            {* <textarea class="form-control" rows="5" name="message" placeholder="Žinutė"><div>{$STICKER_EMAIL_TEMPLATE}</div></textarea> *}
            <div class="form-control" contenteditable="true" id="message" style="width: 100%; height: 200px; border: 1px solid #ccc; padding: 5px;">{$STICKER_EMAIL_TEMPLATE|unescape:"html"}</div>
            <input type="hidden" id="message_input" name="message">
          </div>
          <div class="form-group">
              <input type="radio" id="email_single_label" name="label_email" value="single" checked>
              <label for="email_single_label" style="margin-left: 10px; margin-right: 10px;"> Lipdukai</label>
              <input type="radio" id="email_a4_label" name="label_email" value="a4">
              <label for="email_a4_label" style="margin-left: 10px; margin-right: 10px;"> Lipdukai (A4)</label>
              <input type="radio" id="email_tape_label" name="label_email" value="tape">
              <label for="email_tape_label" style="margin-left: 10px; margin-right: 10px;"> Lipdukai (Juosta)</label>
          </div>
          <input type="hidden" id="file_path" name="file_path">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Atšaukti</button>
        <button type="button" class="btn btn-primary" onclick="sendStickers()">Siųsti Lipdukus <i class="fa fa-paper-plane"></i></button>
      </div>
    </div>
  </div>
</div>

{* Spausdinimas *}
<div class="modal fade" id="printStickers" tabindex="-1" role="dialog" aria-labelledby="printStickers" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 35%;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendInvoiceLabel">Pasirinkite lipdukų formatą</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="printStickersForm">
          <div style="display: none;" id="print_labels_alert" class="alert alert-danger">
          </div>
         <div class="form-group">
            <input type="radio" id="single_label" name="label" value="single" checked>
            <label for="single_label" style="margin-left: 10px;"> Spausdinti lipdukus</label><br>
            <input type="radio" id="a4_label" name="label" value="a4">
            <label for="a4_label" style="margin-left: 10px;"> Spausdinti lipdukus (A4)</label><br>
            <input type="radio" id="tape_label" name="label" value="tape">
            <label for="tape_label" style="margin-left: 10px;"> Spausdinti lipdukus (Juosta)</label>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Atšaukti</button>
        <button type="button" class="btn btn-primary" onclick="printStickers()">Spausdinti Lipdukus <i class="fa fa-print"></i></button>
      </div>
    </div>
  </div>
</div>
{/if}
{strip}
