/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Inventory_Edit_Js("SalesOrder_Edit_Js", {}, {


  /**
 * Function to get popup params
 */
  getPopUpParams: function (container) {
    var params = this._super(container);
    var sourceFieldElement = jQuery('input[class="sourceField"]', container);
    if (!sourceFieldElement.length) {
      sourceFieldElement = jQuery('input.sourceField', container);
    }

    if (sourceFieldElement.attr('name') == 'contact_id' || sourceFieldElement.attr('name') == 'potential_id') {
      var form = this.getForm();
      var parentIdElement = form.find('[name="account_id"]');
      if (parentIdElement.length > 0 && parentIdElement.val().length > 0 && parentIdElement.val() != 0) {
        var closestContainer = parentIdElement.closest('td');
        params['related_parent_id'] = parentIdElement.val();
        params['related_parent_module'] = closestContainer.find('[name="popupReferenceModule"]').val();
      } else if (sourceFieldElement.attr('name') == 'potential_id') {
        parentIdElement = form.find('[name="contact_id"]');
        if (parentIdElement.length > 0 && parentIdElement.val().length > 0) {
          closestContainer = parentIdElement.closest('td');
          params['related_parent_id'] = parentIdElement.val();
          params['related_parent_module'] = closestContainer.find('[name="popupReferenceModule"]').val();
        }
      }
    }
    return params;
  },

  /**
 * Function to register event for enabling recurrence
 * When recurrence is enabled some of the fields need
 * to be check for mandatory validation
 */
  registerEventForEnablingRecurrence: function () {
    var thisInstance = this;
    var form = this.getForm();
    var enableRecurrenceField = form.find('[name="enable_recurring"]');
    var fieldNamesForValidation = new Array('recurring_frequency', 'start_period', 'end_period', 'payment_duration', 'invoicestatus');
    var selectors = new Array();
    for (var index in fieldNamesForValidation) {
      selectors.push('[name="' + fieldNamesForValidation[index] + '"]');
    }
    var selectorString = selectors.join(',');
    var validationToggleFields = form.find(selectorString);
    enableRecurrenceField.on('change', function (e) {
      var element = jQuery(e.currentTarget);
      var addValidation;
      if (element.is(':checked')) {
        addValidation = true;
      } else {
        addValidation = false;
      }

      //If validation need to be added for new elements,then we need to detach and attach validation
      //to form
      if (addValidation) {
        thisInstance.AddOrRemoveRequiredValidation(validationToggleFields, true);
      } else {
        thisInstance.AddOrRemoveRequiredValidation(validationToggleFields, false);
      }
    })
    if (!enableRecurrenceField.is(":checked")) {
      thisInstance.AddOrRemoveRequiredValidation(validationToggleFields, false);
    } else if (enableRecurrenceField.is(":checked")) {
      thisInstance.AddOrRemoveRequiredValidation(validationToggleFields, true);
    }
  },

  AddOrRemoveRequiredValidation: function (dependentFieldsForValidation, addValidation) {
    jQuery(dependentFieldsForValidation).each(function (key, value) {
      var relatedField = jQuery(value);
      if (addValidation) {
        relatedField.removeClass('ignore-validation').data('rule-required', true);
        if (relatedField.is("select")) {
          relatedField.attr('disabled', false);
        } else {
          relatedField.removeAttr('disabled');
        }
      } else if (!addValidation) {
        relatedField.addClass('ignore-validation').removeAttr('data-rule-required');
        if (relatedField.is("select")) {
          relatedField.attr('disabled', true).trigger("change");
          var select2Element = app.helper.getSelect2FromSelect(relatedField);
          select2Element.trigger('Vtiger.Validation.Hide.Messsage');
          select2Element.find('a').removeClass('input-error');
        } else {
          relatedField.attr('disabled', 'disabled').trigger('Vtiger.Validation.Hide.Messsage').removeClass('input-error');
        }
      }
    });
  },

  /**
 * Function to search module names
 */
  searchModuleNames: function (params) {
    var aDeferred = jQuery.Deferred();
    if (typeof params.module == 'undefined') {
      params.module = app.getModuleName();
    }
    if (typeof params.action == 'undefined') {
      params.action = 'BasicAjax';
    }

    if (typeof params.base_record == 'undefined') {
      var record = jQuery('[name="record"]');
      var recordId = app.getRecordId();
      if (record.length) {
        params.base_record = record.val();
      } else if (recordId) {
        params.base_record = recordId;
      } else if (app.view() == 'List') {
        var editRecordId = jQuery('#listview-table').find('tr.listViewEntries.edited').data('id');
        if (editRecordId) {
          params.base_record = editRecordId;
        }
      }
    }

    // Added for overlay edit as the module is different
    if (params.search_module == 'Products' || params.search_module == 'Services') {
      params.module = 'SalesOrder';
    }

    app.request.get({ 'data': params }).then(
      function (error, data) {
        if (error == null) {
          aDeferred.resolve(data);
        }
      },
      function (error) {
        aDeferred.reject();
      }
    )
    return aDeferred.promise();
  },

  /**
 * Function which will register event for Reference Fields Selection
 */
  registerReferenceSelectionEvent: function (container) {
    this._super(container);
    var self = this;

    jQuery('input[name="account_id"]', container).on(Vtiger_Edit_Js.referenceSelectionEvent, function (e, data) {
      self.referenceSelectionEventHandler(data, container);
    });
  },

  //Logtime
  load_company_lookup: function (container) {

    container.find('#SalesOrder_editView_fieldName_load_company').autocomplete({

      'minLength': "3",
      'source': function (request, response) {
        $.ajax({
          url: "test2.php",
          dataType: "json",
          data: {
            company: request.term,
            account_id: $('input[name="account_id"]').val(),
            type: "get_load_companies_list"
          },
          success: function (data) {
            $("#SalesOrder_editView_fieldName_load_company_code").val('');
            $("#SalesOrder_editView_fieldName_load_contact").val('');
            $("#SalesOrder_editView_fieldName_bill_street").val('');
            $("#SalesOrder_editView_fieldName_bill_city").val('');
            $("#SalesOrder_editView_fieldName_bill_code").val('');
            $("#SalesOrder_editView_fieldName_bill_country").val('');
            response(data);
          }
        });
      },
      select: function (event, ui) {

        $.ajax({
          url: "test2.php",
          dataType: "json",
          data: {
            company_code: ui.item.value,
            type: "get_load_company_info"
          },
          success: function (data) {
            //console.log(data);
            $("#SalesOrder_editView_fieldName_load_company").val(ui.item.label);
            $("#SalesOrder_editView_fieldName_load_company_code").val(ui.item.value);
            $("#SalesOrder_editView_fieldName_load_contact").val(data[0].contact);
            $("#SalesOrder_editView_fieldName_bill_street").val(data[0].street);
            $("#SalesOrder_editView_fieldName_bill_city").val(data[0].city);
            $("#SalesOrder_editView_fieldName_bill_code").val(data[0].post_code);
            $("#SalesOrder_editView_fieldName_bill_country").val(data[0].country);
            //$("#SalesOrder_editView_fieldName_bill_country").val(data[1]);                                        
          }
        });
      },
    });
  },

  //Logtime
  unload_company_lookup: function (container) {

    container.find('#SalesOrder_editView_fieldName_unload_company').autocomplete({

      'minLength': "3",
      'source': function (request, response) {
        $.ajax({
          url: "test2.php",
          dataType: "json",
          data: {
            company: request.term,
            account_id: $('input[name="account_id"]').val(),
            type: "get_unload_companies_list"
          },
          success: function (data) {
            $("#SalesOrder_editView_fieldName_unload_company_code").val('');
            $("#SalesOrder_editView_fieldName_unload_contact").val('');
            $("#SalesOrder_editView_fieldName_ship_street").val('');
            $("#SalesOrder_editView_fieldName_ship_city").val('');
            $("#SalesOrder_editView_fieldName_ship_code").val('');
            $("#SalesOrder_editView_fieldName_ship_country").val('');
            response(data);
          }
        });
      },
      select: function (event, ui) {

        $.ajax({
          url: "test2.php",
          dataType: "json",
          data: {
            company_code: ui.item.value,
            type: "get_unload_company_info"
          },
          success: function (data) {
            //console.log(data);
            $("#SalesOrder_editView_fieldName_unload_company").val(ui.item.label);
            $("#SalesOrder_editView_fieldName_unload_company_code").val(ui.item.value);
            $("#SalesOrder_editView_fieldName_unload_contact").val(data[0].contact);
            $("#SalesOrder_editView_fieldName_ship_street").val(data[0].street);
            $("#SalesOrder_editView_fieldName_ship_city").val(data[0].city);
            $("#SalesOrder_editView_fieldName_ship_code").val(data[0].post_code);
            $("#SalesOrder_editView_fieldName_ship_country").val(data[0].country);
            //$("#SalesOrder_editView_fieldName_bill_country").val(data[1]);                                        
          }
        });
      },
    });
  },

  getBussinessDays: function (days) {
    var cDate = new Date();
    for (var i = 1; i <= days; i++) {
      var counter = 1;
      cDate.setDate(cDate.getDate() + counter);
      if (cDate.getDay() == 6 || cDate.getDay() == 0) days++;
    }
    return cDate;
  },

  addBusinessDaysToDate: function (date) {
    const day = date.getDay(); 
    let days = 1;   
    date.setDate(date.getDate() + days + (day === 6 ? 2 : +!day) + (Math.floor((days - 1 + (day % 6 || 1)) / 5) * 2));

    return date;
  },


  //Logtime
  order_date_time: function (container) {
    var d = this.getBussinessDays(1);
    var datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);

    var load_date_from = $("#SalesOrder_editView_fieldName_load_date_from").val();
    var load_time_from = $("#SalesOrder_editView_fieldName_load_time_from").val();
    var load_time_to = $("#SalesOrder_editView_fieldName_load_time_to").val();
    var unload_date_from = $("#SalesOrder_editView_fieldName_unload_date_from").val();
    var unload_time_from = $("#SalesOrder_editView_fieldName_unload_time_from").val();
    var unload_time_to = $("#SalesOrder_editView_fieldName_unload_time_to").val();

    if (load_date_from.replace(/\s/g, "") == "") {
      $("#SalesOrder_editView_fieldName_load_date_from").val(`${new Date().toISOString().split('T')[0]}, ${new Date().toISOString().split('T')[0]}`);
    }
    if (load_time_from.replace(/\s/g, "") == "") {
      $("#SalesOrder_editView_fieldName_load_time_from").val('08:00');
    }

    if (load_time_to.replace(/\s/g, "") == "") {
      $("#SalesOrder_editView_fieldName_load_time_to").val('17:00');
    }
    if (unload_date_from.replace(/\s/g, "") == "") {
      $("#SalesOrder_editView_fieldName_unload_date_from").val(`${datestring},${datestring}`);
    }
    if (unload_time_from.replace(/\s/g, "") == "") {
      $("#SalesOrder_editView_fieldName_unload_time_from").val('08:00');
    }

    if (unload_time_to.replace(/\s/g, "") == "") {
      $("#SalesOrder_editView_fieldName_unload_time_to").val('17:00');
    }

    this.checkDateFields();

  },

  checkDateFields: function () {
    let thisInstance = this;
    $('.dateField').on('datepicker-change', function () {
      thisInstance.mainValidation(thisInstance,this);
    });
  },

  checkDateIsValid: function(){
    let thisInstance = this;
    $('.dateField').on('change', function () {     
      thisInstance.mainValidation(thisInstance,this);
    });
  },

  mainValidation: function(thisInstance,event){
    let input = $(event);
    let groupField, load, unload, warning;

    let thisInput = input.val();
    let fieldName = input.data('fieldname');

    let thisInputFrom = thisInput.split(",");
    let splitDate1 = thisInputFrom[0].split('-');
    let splitDate2 = thisInputFrom[1].split('-');
    let year1 = splitDate1[0];
    let year2 = splitDate2[0];      

    let dateFormat = /^\d{4}[-]\d{2}[-]\d{2}$/;    
    let y = new Date();
    let currentYear = y.getFullYear();
    let plusOneYearFromNow = y.getFullYear() + 1;
    let minusOneYearFromNow = y.getFullYear() - 1;
    let years = [minusOneYearFromNow,currentYear,plusOneYearFromNow];

    if( (thisInputFrom[0].match(dateFormat) && thisInputFrom[1].match(dateFormat)) && (years.includes(parseInt(year1)) && years.includes(parseInt(year2)))) {
      if (fieldName == 'load_date_from') {
        groupField = $('#SalesOrder_editView_fieldName_unload_date_from');
        load = new Date(thisInputFrom[0]);
        load2 = new Date(thisInputFrom[1]);
        let groupFieldVal = groupField.val();
        let to = groupFieldVal.split(',');
        unload = new Date(to[0]);
        unload2 = new Date(to[1]);
        warning = "Pasikrovimo data negali būti vėlesnė";
        // warning2 = "Pasikrovimo data negali būti vėlesnė arba ankstesnė nei 30 dienų nuo šiandien";
        warning2 = "Pasikrovimo data vėlesnė arba ankstesnė daugiau kaip 30 dienų";
      } else if (fieldName == 'unload_date_from') {
        groupField = $('#SalesOrder_editView_fieldName_load_date_from');
        let groupFieldVal = groupField.val();
        let from = groupFieldVal.split(',');
        load = new Date(from[0]);
        load2 = new Date(from[1]);
        unload = new Date(thisInputFrom[0]);
        unload2 = new Date(thisInputFrom[1]);
        warning = "Išsikrovimo data negali būti ankstesnė";
        warning2 = "Išsikrovimo data vėlesnė arba ankstesnė daugiau kaip 30 dienų";
        // warning2 = "Išsikrovimo data negali būti vėlesnė arba ankstesnė nei 30 dienų nuo šiandien";
      }    

      if(new Date(thisInputFrom[0]) != 'Invalid Date' && new Date(thisInputFrom[1]) != 'Invalid Date'){
        
        let unloadFromDate;
        let unloadToDate;
        let loadFromDatePlusOne;
        let loadToDatePlusOne;

        if(fieldName == 'unload_date_from'){
          unloadFromDate = thisInputFrom[0];
          unloadToDate = thisInputFrom[1];
          loadFromDatePlusOne = thisInstance.addDays(new Date(thisInputFrom[0]), 1);
          loadToDatePlusOne = thisInstance.addDays(new Date(thisInputFrom[1]), 1);
        }else{
          // loadFromDatePlusOne = thisInstance.addDays(new Date(thisInputFrom[0]), 1);
          loadFromDatePlusOne =thisInstance.addBusinessDaysToDate(new Date(thisInputFrom[0]));
          let dateFromToString = loadFromDatePlusOne.toISOString();
          unloadFromDate = dateFromToString.split('T');
          unloadFromDate = unloadFromDate[0];     

          // loadToDatePlusOne = thisInstance.addDays(new Date(thisInputFrom[1]), 1);
          loadToDatePlusOne = thisInstance.addBusinessDaysToDate(new Date(thisInputFrom[1]));
          let dateToToString = loadToDatePlusOne.toISOString();
          unloadToDate = dateToToString.split('T');
          unloadToDate = unloadToDate[0];
        }

     
        console.log(unloadFromDate);
        // console.log(unloadToDate);
      
        $("#SalesOrder_editView_fieldName_unload_date_from").val(unloadFromDate + ',' + unloadToDate);
  
        if(thisInstance.validatedate(load) && thisInstance.validatedate(load2)){
          $('#info').remove();
          // $(event).addClass('alert alert-danger');
          $('<p id="info" style="color: orange;">' + warning2 + '</p>').insertBefore(input.parent());
        }else if(thisInstance.validatedate(loadFromDatePlusOne) && thisInstance.validatedate(loadToDatePlusOne)){
            $('#info').remove();
            // $(event).addClass('alert alert-danger');
            $('<p id="info" style="color: orange;">' + warning2 + '</p>').insertBefore(input.parent());
        }else{
          if (load <= loadFromDatePlusOne && load2 <= loadToDatePlusOne) {
            $(event).removeClass('alert-danger');
            groupField.removeClass('alert-danger');
            $('#info').remove();
          } else {
            $('#info').remove();
            $(event).addClass('alert alert-danger');
            $('<p id="info" style="color: red;">' + warning + '</p>').insertBefore(input.parent());
          }
        }

      }else{
        let d = thisInstance.getBussinessDays(1);
        let datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
        input.val(datestring+','+datestring);
      }
    }else{
      let d = thisInstance.getBussinessDays(1);
      let datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
      input.val(datestring+','+datestring);
    }
    
  },
   addDays: function(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  },

  minusDays: function(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() - days);
    return result;
  },

  validatedate: function(dateObject){
    let d = this.getBussinessDays(1);
    let today = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);

    let plus30 = this.addDays(today,30);
    let minus30 = this.minusDays(today,30);

    let loadString = dateObject.toISOString();
    loadString = loadString.split('T');
    let date = loadString[0];

    if(plus30 > new Date(date) && minus30 < new Date(date)){
     return false;
    }else{
      return true;
    }

  },

  registerBasicEvents: function (container) {
    this._super(container);
    this.registerEventForEnablingRecurrence();
    this.registerForTogglingBillingandShippingAddress();
    this.registerEventForCopyAddress();
    this.load_company_lookup(container);
    this.unload_company_lookup(container);
    this.order_date_time(container);
    this.checkDateIsValid(container);
  },

});