{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
********************************************************************************/
-->*}
{strip}
{if $FOLDER_INFO}
	{assign var=FOLDER_INFO value=$FOLDER_INFO}
	{assign var=HAS_PERMISION value=explode(",",$ROLE_LIST['role'])}
{/if}
{if $RECORD}
	{assign var=RECORD_ID value=$RECORD}
{/if}
{if $FOLDER}
	{assign var=FOLDER value=$FOLDER}
{/if}
	<div name='editContent'>
		{if $DUPLICATE_RECORDS}
			<div class="fieldBlockContainer duplicationMessageContainer">
				<div class="duplicationMessageHeader"><b>{vtranslate('LBL_DUPLICATES_DETECTED', $MODULE)}</b></div>
				<div>{getDuplicatesPreventionMessage($MODULE, $DUPLICATE_RECORDS)}</div>
			</div>
		{/if}

		<div class='fieldBlockContainer'>
		<h4 class='fieldBlockHeader'>{if !$FOLDER_INFO}Pridėti naują{else}Redaguoti aplanka{/if}</h4>
		<hr>
			<table class="table table-borderless">
				<tr>
					<td class="fieldLabel alignMiddle">Pavadinimas <span class="redColor">*</span></td>
					<td class="fieldValue"><input type="text" name="title" class="inputElement" value="{$FOLDER_INFO['title']}" required></td>

					<td class="fieldLabel alignMiddle">Tipas</td>
					<td class="fieldValue">
					 <select class="inputElement" name="type">
					 	<option value="1" {if $FOLDER_INFO['type'] eq 1}selected{/if}>{vtranslate('LBL_PUBLIC', $MODULE)}</option>
					 	<option value="2" {if $FOLDER_INFO['type'] eq 2}selected{/if}>{vtranslate('LBL_PRIVATE', $MODULE)}</option>
					 </select>
					</td>
				</tr>		
			</table>
			<input type="hidden" name="controller" value="{if !$FOLDER_INFO}addNewFolder{else}editFolder{/if}">
			<input type="hidden" name="recordid" value="{if $FOLDER_INFO}{$FOLDER_INFO['id']}{else if $RECORD_ID}{$RECORD_ID}{/if}">
			<input type="hidden" name="folder" value="{$FOLDER}">       
		</div>

		<div class='fieldBlockContainer permisions_block {if !$FOLDER_INFO['type'] eq 2}hide{/if}'>
		<h4 class='fieldBlockHeader'>Galinčios matyti aplanka rolės</h4>
		<hr>
				<table class="table detailview-table no-border">
							<tbody>	
							{assign var=COUNTER value=1}
							{foreach from=$ROLE_LIST['list'] item=ROLE}																		
									{if $COUNTER is odd}
									<tr>		
									{/if}								
										<td class="fieldLabel textOverflowEllipsis">
											<span class='muted'>{$ROLE['rolename']}</span> 
										</td>	
										<td class="fieldLabel textOverflowEllipsis">
											<input type="checkbox" name="{$ROLE['roleid']}" value="{$ROLE['roleid']}" {if $HAS_PERMISION} {if in_array($ROLE['roleid'],$HAS_PERMISION)}checked{/if} {/if}class="listViewEntriesCheckBox">
										</td>	
									{if $COUNTER is even}																													
									</tr>
									{/if}															
									{assign var=COUNTER value=$COUNTER+1}
								{/foreach}
							</tbody>
						</table>
      
		</div>
	</div>
<script type="text/javascript" src="layouts/v7/modules/{$MODULE}/resources/js/addFolder.js"></script>
	
{/strip}
