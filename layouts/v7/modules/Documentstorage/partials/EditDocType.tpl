 {if $DOC_TYPE}
	{assign var=DOC_TYPE value=$DOC_TYPE}
 {/if}

	<input type="hidden" name="controller" value="EditDocType">
 <input type="hidden" name="recordid" value="{$DOC_TYPE['id']}">
        <div class="detailViewContainer viewContent clearfix" style="margin-bottom: 150px;">
            <div class="col-sm-12 col-xs-12">
                <div class="detailview-content container-fluid" style="margin-top: 20px">            									
									<div class="block">													
											<div>	<h4 class="textOverflowEllipsis maxWidth50">Redagavimas</h4></div>
											<hr>
												<div class="blockData">
													<table class="table detailview-table no-border">
														<tbody>	
															<tr>
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Tipo pavadinimas</span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<input class="inputElement" name="title" style="width: 50%;" value="{if $DOC_TYPE}{$DOC_TYPE['file_type']}{/if}">																
																</td>	
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Veikimas</span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<select class="inputElement" name="type" style="width: 50%;">
																		<option value="0" {if $DOC_TYPE['type'] eq 0}selected{/if}>Automatiškai nepildomas</option>																	
																		<option value="1" {if $DOC_TYPE['type'] eq 1}selected{/if}>Pildomas automatiškai</option>																	
																	</select>
																</td>	
															<tr>
														</tbody>
													</table>
												</div>
									</div>
									<br>		
							</div>
         </div>		
			</div>