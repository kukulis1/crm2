{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
********************************************************************************/
-->*}
{strip}
{if $FOLDER_INFO}
	{assign var=FOLDER_INFO value=$FOLDER_INFO}
{/if}

{if $FOLDER}
	{assign var=FOLDER value=$FOLDER}
{/if}

	{assign var=USER_ID value=$USER_ID}
	  <link rel="stylesheet" type="text/css" href="layouts/v7/modules/Documentstorage/resources/css/dropzone.css"/>
    <link rel="stylesheet" type="text/css" href="layouts/v7/modules/Documentstorage/resources/css/basic.css"/>

			<div name='editContent' class="info_content">
			<div class='fieldBlockContainer'>
			<h4 class='fieldBlockHeader'>Failo informacija</h4>
			<hr>	
				<table class="table detailview-table no-border">
					<tbody>				
						<tr>

						<td class="fieldLabel textOverflowEllipsis">
								<span class='muted'>Automatiškai užsipildo</span>
							</td>
							<td class="fieldValue">
								<select class="inputElement" name="type" style="width: 50%;">
										<option value="0">Ne</option>
										<option value="1">Taip</option>										
								</select>
							</td>		

							<td class="template_selector fieldLabel textOverflowEllipsis hide">
								<span class='muted'>Šablonas</span>
							</td>
							<td class="template_selector fieldValue hide">
								<select class="inputElement" name="template" style="width: 50%;">
										<option value="0">---</option>
										{foreach from=$TEMPLATE_LIST item=type}
											<option value="{$type['id']}">{$type['title']}</option>
										{/foreach}
								</select>
							</td>	
						</tr>
						</tbody>
				</table>

		</div>
	</div>	

	<div name='editContent'>
		{if $DUPLICATE_RECORDS}
			<div class="fieldBlockContainer duplicationMessageContainer">
				<div class="duplicationMessageHeader"><b>{vtranslate('LBL_DUPLICATES_DETECTED', $MODULE)}</b></div>
				<div>{getDuplicatesPreventionMessage($MODULE, $DUPLICATE_RECORDS)}</div>
			</div>
		{/if}
	


		<div class='fieldBlockContainer'>
		<h4 class='fieldBlockHeader'>Failų įkėlimas</h4>
		<hr>	

			<div id="DropZoneDiv" class="dropzone">      
		</div>
		</div>
	</div>
			<input type="hidden" name="controller" value="addNewFile">
			<input type="hidden" name="recordid" value="{if $FOLDER_INFO}{$FOLDER_INFO['id']}{/if}">
			<input type="hidden" name="user_id" value="{$USER_ID}">
			<input type="hidden" name="folder" value="{$FOLDER}"> 

			<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/dropzone.js"></script>
			<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/main.js"></script>
{/strip}
