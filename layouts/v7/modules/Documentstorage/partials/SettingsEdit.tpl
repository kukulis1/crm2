{assign var=ROLE_LIST value=$ROLE_LIST}
{assign var=HAS_PERMISION value=explode(",",$ROLE_LIST['role'])}
		<div name='editContent'>
		<div class='fieldBlockContainer'>
		<h4 class='fieldBlockHeader'>Visus aplankus mato</h4>
		<hr>
			<input type="hidden" name="controller" value="SettingsEdit">			
				<table class="table detailview-table no-border">
							<tbody>	
							{assign var=COUNTER value=1}
							{foreach from=$ROLE_LIST['list'] item=ROLE}																		
									{if $COUNTER is odd}
									<tr>		
									{/if}								
										<td class="fieldLabel textOverflowEllipsis">
											<span class='muted'>{$ROLE['rolename']}</span> 
										</td>	
										<td class="fieldLabel textOverflowEllipsis">
											<input type="checkbox" name="{$ROLE['roleid']}" value="{$ROLE['roleid']}" {if in_array($ROLE['roleid'],$HAS_PERMISION)}checked{/if} class="listViewEntriesCheckBox">
										</td>	
									{if $COUNTER is even}																													
									</tr>
									{/if}															
									{assign var=COUNTER value=$COUNTER+1}
								{/foreach}
							</tbody>
						</table>      
		</div>
	</div>