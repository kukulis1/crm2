{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
********************************************************************************/
-->*}
{strip}

{assign var=FILE_INFO value=$FILE_INFO}
{assign var=FOLDER value=$FOLDER}
{assign var=USER_ID value=$USER_ID}
{assign var=RECORD value=$RECORD}
{assign var=POSITION_LIST value=$POSITION_LIST}

	<link rel="stylesheet" type="text/css" href="layouts/v7/modules/Documentstorage/resources/css/dropzone.css"/>
	<link rel="stylesheet" type="text/css" href="layouts/v7/modules/Documentstorage/resources/css/basic.css"/>

		<div name='editContent' class="info_content">
			<div class='fieldBlockContainer'>
			<h4 class='fieldBlockHeader'>Failo informacija</h4>
			<hr>	
				<table class="table detailview-table no-border">
					<tbody>				
						<tr>						
							<td class="fieldLabel textOverflowEllipsis">
								<span class='muted'>Pavadinimas</span>
							</td>
							<td class="fieldValue">
								<div class="title_group">
								<span class="value">
									<a href="index.php?module={$MODULE}&view=DownloadFile&file_id={$FILE_INFO['documentstorageid']}">{$FILE_INFO['filename']}.{$FILE_INFO['extension']}</a>
								</span>
								<span style="margin-left: 10px;">
									<a class="value" style="max-width:20%;" href="javascript:void(0);"><i title="Ištrinti failą" class="fa fa-trash alignMiddle" style="font-size: 20px;" onclick="deleteOldFile()"></i></a>
								</span>
								</div>
							</td>	

							<td class="fieldLabel textOverflowEllipsis">
								<span class='muted'>Automatiškai užsipildo</span>
							</td>
							<td class="fieldValue">
								<select class="inputElement" name="type" style="width: 50%;">
										<option value="0" {if $FILE_INFO['file_type'] eq 0}selected{/if}>Ne</option>
										<option value="1" {if $FILE_INFO['file_type'] > 0}selected{/if}>Taip</option>										
								</select>
							</td>	
						</tr>
						<tr>
							<td class="template_selector fieldLabel textOverflowEllipsis {if $FILE_INFO['file_type'] eq 0}hide{/if}">
								<span class='muted'>Šablonas</span>
							</td>
							<td class="template_selector fieldValue {if $FILE_INFO['file_type'] eq 0}hide{/if}">
								<select class="inputElement" name="template" style="width: 50%;">
										<option value="0">---</option>
										{foreach from=$TEMPLATE_LIST item=type}
											<option value="{$type['id']}" {if $type['id'] eq $FILE_INFO['file_type']}selected{/if}>{$type['title']}</option>
										{/foreach}
								</select>
							</td>	
						</tr>
						</tbody>
				</table>

		</div>
	</div>

	<div name='editContent2' class=" upload_content hide">	
			<div class='fieldBlockContainer'>
			<h4 class='fieldBlockHeader'>Failų įkėlimas</h4>
			<hr>	
			<div id="DropZoneDiv" class="dropzone"></div>
		</div>
	</div>

		<input type="hidden" name="controller" value="editFile">
		<input type="hidden" name="recordid" value="{$RECORD}">
		<input type="hidden" name="file_id" value="{$FILE_INFO['documentstorageid']}">
		<input type="hidden" name="user_id" value="{$USER_ID}">
		<input type="hidden" name="folder" value="{$FOLDER}">	  
		<input type="hidden" name="old_file" value="{$FILE_INFO['path']}/{$FILE_INFO['temp_filename']}.{$FILE_INFO['extension']}" disabled>	  

	<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/dropzone.js"></script>
	<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/editFile.js"></script>
{/strip}
