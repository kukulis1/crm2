{assign var=POSITION_LIST value=$POSITION_LIST}
{assign var=TEMPLATE_MODULES_LIST value=$TEMPLATE_MODULES_LIST}

        <div class="detailViewContainer viewContent clearfix" style="margin-bottom: 150px;">
					<div class="detailview-header-block">
						<div class="detailview-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
											<div class="record-header clearfix">
												{if !$MODULE}
													{assign var=MODULE value=$MODULE_NAME}
												{/if}
												<div class="hidden-sm hidden-xs recordImage bg_{$MODULE} app-{$SELECTED_MENU_CATEGORY}">
													<div class="name"><span><i class="vicon-documents" title="Dokumentai"></i></span></div>
												</div>

												<div class="recordBasicInfo">
													<div class="info-row">
														<h4>
															<span class="recordLabel pushDown" title="name">							
																		<span>Word automatiškai užsipildantys dokumentai</span>&nbsp;						
															</span>
														</h4>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
													<div class="pull-right btn-toolbar">
														<div class="btn-group"> 		
																<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=List'">Gryžti</button> 						   
														</div>      
												</div>
										</div>
							</div>
						</div>
					</div>

            <div class="col-sm-12 col-xs-12">
                <div class="detailview-content container-fluid" style="margin-top: 20px">            									
									<div class="block">													
											<div>	<h4 class="textOverflowEllipsis maxWidth50">Tipas/Modulis</h4></div>
											<hr>
												<div class="blockData">
													<table class="table detailview-table no-border">
														<tbody>	
														<tr>
															<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																<span class='muted'>Šablono pavadinimas <span class="redColor">*</span></span>
															</td>	
															<td class="fieldValue" style="width:10%;">																
																<input type="text" name="template_name" class="inputElement" value="" style="width: 50%;" required>
															</td>	
															<td class="fieldLabel textOverflowEllipsis" style="width:10%;">	</td>	
															<td class="fieldValue" style="width:10%;"></td>														
														</tr>
															<tr>															
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Šablonas moduliui</span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<select class="inputElement" name="select_module" style="width: 50%;">
																		<option value="0">---</option>
																		{foreach from=$TEMPLATE_MODULES_LIST item=module}
																			<option value="{$module['tabid']}">{vtranslate({$module['module']},$MODULE)}</option>
																		{/foreach}
																	</select>
																</td>	
																<td class="position fieldLabel textOverflowEllipsis hide" style="width:10%;">
																	<span class='muted'>Kokiai pareigybei skirta</span>
																</td>	
																<td class="position fieldValue hide" style="width:10%;">
																	<select class="inputElement" name="position" style="width: 50%;" required multiple>
																		<option value="0" selected>Visi</option>
																		{foreach from=$POSITION_LIST item=type}
																			<option value="{$type['id']}">{$type['name']}</option>
																		{/foreach}
																	</select>
																</td>	
															<tr>
														</tbody>
													</table>
												</div>
									</div>
									<br>		
							</div>
         </div>			 	

				    <div class="col-sm-12 col-xs-12 classificator_block hide">
                <div class="detailview-content container-fluid" style="margin-top: 20px">            									
									<div class="block">													
											<div>	<h4 class="textOverflowEllipsis maxWidth50">Klasifikatorius</h4></div>
											<hr>
												<div class="blockData">
													<table class="table detailview-table no-border">
														<tbody id="table_tbody">	
														<tr class="hide" id="cloneFields">
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Žymė </span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<span style="font-size:20px;">${literal}{{/literal}</span> 
																	<input type="text" name="variable0" class="inputElement" style="width: 50%;" value=""> <span style="font-size:20px;">{literal}}{/literal}</span>
																	<p style="font-size: 10px;padding-left:23px;color: red;">Žymę įvesti be dolerio ženklo ir riestinių skliaustų</p>
																</td>														

																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Laukas</span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<select class="inputElement" name="fields0" style="width: 50%;"></select>
																</td>	
																	<td class="fieldValue" style="width:5%;">
																	<a class="value"style="max-width:20%;" href="javascript:void(0);">
																		<i title="Ištrinti eilutę" class="fa fa-trash alignMiddle removeLine" style="font-size: 20px;"></i>
																	</a>
																</td>	
															</tr>

															<tr>
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Vadovo žymė</span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<span style="font-size:20px;">${literal}{{/literal}</span> 
																	<input type="text" name="variable1" class="inputElement" style="width: 50%;background: #ddd;" value="vadovas" readonly> <span style="font-size:20px;">{literal}}{/literal}</span>
																	<p style="font-size: 10px;padding-left:23px;color: red;">Žymę įvesti be dolerio ženklo ir riestinių skliaustų</p>
																</td>														

																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Pasirinkite vadovą <small>(Neprivaloma)</small></span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<select class="inputElement" name="fields1" style="width: 50%;"></select>
																</td>	
																<td class="fieldValue" style="width:1%;"></td>	
															</tr>

															<tr>
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Žymė </span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<span style="font-size:20px;">${literal}{{/literal}</span> 
																	<input type="text" name="variable2" class="inputElement" style="width: 50%;" value="" required> <span style="font-size:20px;">{literal}}{/literal}</span>
																	<p style="font-size: 10px;padding-left:23px;color: red;">Žymę įvesti be dolerio ženklo ir riestinių skliaustų</p>
																</td>														

																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Laukas</span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<select class="inputElement" name="fields2" style="width: 50%;"></select>
																</td>	
																<td class="fieldValue" style="width:1%;"></td>	
															</tr>



														</tbody>													
													</table>
														<div style="display:flex;justify-content: flex-end;padding:20px;">
															<input type="button" class="btn btn-primary" id="addLine" value="Pridėti laukelį">
														</div>
													<input type="hidden" name="count_inputs" value="1">
														<input type="hidden" name="controller" value="AddTemplate">
												</div>
									</div>
									<br>		
							</div>
			</div>


<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/templates.js"></script>