{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{assign var=FOLDERS value=$FOLDERS}

<link rel="stylesheet" type="text/css" href="layouts/v7/modules/{$MODULE}/resources/css/sideBar.css"/>
<div class="sidebar-menu">
    <div class="module-filters" id="module-filters">
        <div class="sidebar-container lists-menu-container">
            <div class="sidebar-header clearfix">
                <h5 class="pull-left">{vtranslate('LBL_FOLDERS',$MODULE)}</h5>
                <button class="btn btn-sm btn-default pull-right sidebar-btn" title="{vtranslate('LBL_CREATE_FOLDER',$MODULE)}" onclick="window.location.href='index.php?module=Documentstorage&view=AddNewFolder'">
                    <div class="fa fa-plus" aria-hidden="true"></div>
                </button> 
            </div>
            <hr>
            <div>
                <input class="search-list" type="text" placeholder="{vtranslate('LBL_SEARCH_FOR_LIST',$MODULE)}">
            </div>
            <div class="menu-scroller" style="position:relative; top:0; left:0;">
				<div class="list-menu-content">	                                    
                    <div class="list-group" id="{if $GROUP_LABEL eq 'Mine'}myList{else}sharedList{/if}">   
                        <h6 class="lists-header {if count($GROUP_CUSTOM_VIEWS) <=0} hide {/if}" >
                            {if $GROUP_LABEL eq 'Mine'}
                                {vtranslate('LBL_FOLDERS',$MODULE)}
                            {else}
                                {vtranslate('LBL_FOLDERS',$MODULE)}
                            {/if}
                        </h6>                        
                        <ul class="lists-menu">
                        {if $FOLDERS}
                            {foreach from=$FOLDERS item=FOLDER}    
                                {$SUBFOLDERS = $MODULE_MODEL->getSubFolders($FOLDER['id'])}          
                                <li style="font-size:12px;">
                                    <a class="filterName listViewFilterElipsis" href="index.php?module={$MODULE}&view=Catalog&record={$FOLDER['id']}" title="{$FOLDER['title']}">{$FOLDER['title']}</a>{if $SUBFOLDERS}<span class="fa fa-plus show-subfolders">{/if}</span>
                                    <ul class="submenu hide">
                                        {foreach from=$SUBFOLDERS item=SUBFOLDER}                               
                                            <li><a href="index.php?module={$MODULE}&view=SubCatalog&record={$SUBFOLDER['id']}" title="{$SUBFOLDER['title']}">{$SUBFOLDER['title']}</a></li>                                   
                                        {/foreach}
                                  </ul>
                                 </li>                        
                            {/foreach}
                        {/if}
                        </ul>					
                    </div>                     
                    <div class="list-group {if $FOLDERS}hide{/if} noLists" style="height:50px">
                        <h6 class="lists-header"><center> {vtranslate('LBL_EMPTY',$MODULE)}... </center></h6>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<script src="layouts/v7/modules/{$MODULE}/resources/js/sideBar.js"></script>
