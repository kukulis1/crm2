{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
{assign var=TEMPLATES value=$TEMPLATES}
 <div id="wait" style="display:none;position:absolute;top: 32%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>
<div class="modal fade" id="documentsModal" tabindex="-1" role="dialog" aria-labelledby="documentsModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Rinktis dokumentą</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="lists-menu" id="list-ul"></ul>
			</div>
      <div class="modal-footer">			
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="margin-top: 25px;">Uždaryti</button>
      </div>
    </div>
  </div>
</div>
	<div class="relatedHeader">
		<div class="btn-toolbar row">
			<div class="col-lg-6 col-md-6 col-sm-6 btn-toolbar">
				<div class="col-sm-3">
					<div class="dropdown">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<span class="fa fa-plus" title="Generuoti dokumentą"></span>&nbsp;&nbsp;Generuoti dokumentą&nbsp; <span class="caret"></span>
						</button>						
						<ul class="dropdown-menu">						
						{foreach from=$TEMPLATES item=TEMPLATE}	
							{$position = explode(',',$TEMPLATE['position'])}
							{if in_array(0,$position)}
								<li><a data-toggle="modal" data-target="#documentsModal" onclick="documentGenerator({$TEMPLATE['id']});" ><i class="fa fa-file-text"></i> {$TEMPLATE['title']}</a></li>
							{else}
								{if in_array($EMPLOYEE_POSITION,$position)}	
									<li><a data-toggle="modal" data-target="#documentsModal" onclick="documentGenerator({$TEMPLATE['id']});" ><i class="fa fa-file-text"></i> {$TEMPLATE['title']}</a></li>
								{/if}
							{/if}
						{/foreach}
						</ul>
					</div>
				</div>

	<div class="col-sm-3">
		<input type="file" name="upload_file" id="upload_file" style="display:none;" onchange="uploadFile(event);">
		<button type="button" class="btn btn-default" onclick="document.getElementById('upload_file').click();">
			<span class="fa fa-plus" title="Įkelti failą"></span>&nbsp;&nbsp;Įkelti failą&nbsp;
		</button>	
	</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/modalSelect.js"></script>
{/strip}