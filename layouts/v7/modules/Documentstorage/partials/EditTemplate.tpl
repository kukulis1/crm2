{assign var=POSITION_LIST value=$POSITION_LIST}
{assign var=TEMPLATE_MODULES_LIST value=$TEMPLATE_MODULES_LIST}
{assign var=TEMPLATE value=$TEMPLATE}
{assign var=MODULE_FIELDS value=$MODULE_FIELDS}
{assign var=MANAGERS value=$MANAGERS}
{assign var=classificator value=explode(':',$TEMPLATE['classificator'])}
{$positions = explode(',',$TEMPLATE['position'])}

<div name='editContent'>	
	<div class='fieldBlockContainer'>
		<h4 class='fieldBlockHeader'>Tipas/Modulis</h4>
		<hr>	
		<table class="table detailview-table no-border">
			<tbody>	
				<tr>
					<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
						<span class='muted'>Šablono pavadinimas <span class="redColor">*</span></span>
					</td>	
					<td class="fieldValue" style="width:10%;">																
						<input type="text" name="template_name" class="inputElement" value="{$TEMPLATE['title']}" style="width: 50%;" required>
					</td>	
					<td class="fieldLabel textOverflowEllipsis" style="width:10%;">	</td>	
					<td class="fieldValue" style="width:10%;"></td>														
				</tr>
				<tr>				
					<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
						<span class='muted'>Šablonas moduliui</span>
					</td>	
					<td class="fieldValue" style="width:10%;">
						<select class="inputElement" name="select_module" style="width: 50%;">
							<option value="0">---</option>
							{foreach from=$TEMPLATE_MODULES_LIST item=module}
								<option value="{$module['tabid']}" {if $TEMPLATE['module'] eq $module['tabid']}selected{/if}>{vtranslate({$module['module']},$MODULE)}</option>
							{/foreach}
						</select>
					</td>	
					<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
						<span class='muted'>Kokiai pareigybei skirta</span>
					</td>	
					<td class="fieldValue" style="width:10%;">
						<select class="inputElement" name="position[]" style="width: 50%;height: 100px;" required multiple>
							<option value="0" {if in_array(0,$positions)}selected{/if}>Visi</option>
							{foreach from=$POSITION_LIST item=position}								
								<option value="{$position['id']}" {if in_array($position['id'],$positions)}selected{/if}>{$position['name']}</option>
							{/foreach}
						</select>
					</td>	
				<tr>
			</tbody>
		</table>		
		</div>
	</div>



<div name='editContent'>	
	<div class='fieldBlockContainer'>
		<h4 class='fieldBlockHeader'>Klasifikatorius</h4>
		<hr>

													<table class="table detailview-table no-border">
														<tbody id="table_tbody">	
															<tr class="hide" id="cloneFields">
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Žymė </span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<span style="font-size:20px;">${literal}{{/literal}</span> 
																	<input type="text" name="variable0" class="inputElement" style="width: 50%;" value=""> <span style="font-size:20px;">{literal}}{/literal}</span>
																	<p style="font-size: 10px;padding-left:23px;color: red;">Žymę įvesti be dolerio ženklo ir riestinių skliaustų</p>
																</td>	
																<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
																	<span class='muted'>Laukas</span>
																</td>	
																<td class="fieldValue" style="width:10%;">
																	<select class="inputElement" name="fields0" style="width: 50%;">
																		{foreach from=$MODULE_FIELDS item=FIELD}
																					<option value="{$FIELD['columnname']}">{vtranslate($FIELD['fieldlabel'],$TEMPLATE['module_name'])}</option>
																		{/foreach}
																	</select>
																</td>	
																	<td class="fieldValue" style="width:5%;">
																	<a class="value"style="max-width:20%;" href="javascript:void(0);">
																		<i title="Ištrinti eilutę" class="fa fa-trash alignMiddle removeLine" style="font-size: 20px;"></i>
																	</a>
																</td>	
															</tr>
													{assign var=COUNTER value=1}
												{foreach from=$classificator item=class}	
													{$tag = explode('=',$class)}									
													<tr>
														<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
															<span class='muted'>{if $COUNTER eq 1}Vadovo žymė	{else}Žymė{/if} </span>
														</td>	
														<td class="fieldValue" style="width:10%;">
															<span style="font-size:20px;">${literal}{{/literal}</span> 
															<input type="text" name="variable{$COUNTER}" class="inputElement" style="width: 50%;{if $COUNTER eq 1}background:#ddd;{/if}" value="{$tag[0]}" required {if $COUNTER eq 1}readonly{/if}> <span style="font-size:20px;">{literal}}{/literal}</span>
															<p style="font-size: 10px;padding-left:23px;color: red;">Žymę įvesti be dolerio ženklo ir riestinių skliaustų</p>
														</td>														

														<td class="fieldLabel textOverflowEllipsis" style="width:10%;">
															<span class='muted'>{if $COUNTER eq 1}Pasirinkite vadovą <small>(Neprivaloma)</small>{else}Laukas{/if}</span>
														</td>	
														
														<td class="fieldValue" style="width:10%;">
															<select class="inputElement" name="fields{$COUNTER}" style="width: 50%;">
															{if $COUNTER eq 1}
																<option value="">---</option>	
																{foreach from=$MANAGERS item=MANAGER}
																	<option value="{$MANAGER['employid']}" {if $tag[1] eq $MANAGER['employid']}selected{/if}>{$MANAGER['firstname']} {$MANAGER['lastname']}</option>	
																{/foreach}
															{else}															
																{foreach from=$MODULE_FIELDS item=FIELD}
																		<option value="{$FIELD['columnname']}" {if $tag[1] eq $FIELD['columnname']}selected{/if}>{vtranslate($FIELD['fieldlabel'],$TEMPLATE['module_name'])}</option>
																{/foreach}
															{/if}
															</select>
														</td>	
														<td class="fieldValue" style="width:1%;">													
															<a class="value"style="max-width:20%;" href="javascript:void(0);">
																<i title="Ištrinti eilutę" class="fa fa-trash alignMiddle removeLine 	{if $COUNTER neq {count($classificator)}}hide{/if}" style="font-size: 20px;"></i>
															</a>															
														</td>	
													</tr>
														{assign var=COUNTER value=$COUNTER+1}
												{/foreach}

														</tbody>													
													</table>

														<div style="display:flex;justify-content: flex-end;padding:20px;">
															<input type="button" class="btn btn-primary" id="addLine" value="Pridėti laukelį">
														</div>
														<input type="hidden" name="count_inputs" value="{count($classificator)}">
														<input type="hidden" name="controller" value="EditTemplate">
														<input type="hidden" name="recordid" value="{$TEMPLATE['id']}">
					</div>
	</div>

	


<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/templates.js"></script>