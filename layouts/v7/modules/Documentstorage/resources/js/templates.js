$('[name="select_module"]').on('change', openClasificator);
$('#addLine').on('click', addField);
$('.removeLine').on('click', removeLine);

function openClasificator(){
	let module = $('[name="select_module"]').val();
	if(module > 0){		
		$('.classificator_block').removeClass('hide');
		if(module == 107){
			$('.position').removeClass('hide');
		}else{
			$('.position').addClass('hide');
		}
		getManager();
		getModuleFields(module);
	}else{
		$('.classificator_block').addClass('hide');
		$('.position').addClass('hide');
	}		
}

function getManager(){
	let html = '<option value="">---</option>';
	$.ajax({
		type: "POST",
		url: "modules/Documentstorage/actions/getManagers.php",
		data: {1:1},
		dataType: "JSON",
		success: function (response) {
				if(response.status == 'success'){
					response.data.forEach(item => {
						html += `<option value="${item.employid}">${item.firstname} ${item.lastname}</option>`;
					});
					$('[name="fields1"]').html(html);
				}
		}
	});
}


function getModuleFields(module){
	let html = '';
	$.ajax({
		type: "POST",
		url: "modules/Documentstorage/actions/moduleFields.php",
		data: {module:module},
		dataType: "JSON",
		success: function (response) {
				if(response.status == 'success'){
					response.data.forEach(item => {
						html += `<option value="${item.columnname}">${app.vtranslate(item.fieldlabel)}</option>`;
					});
					$('[name="fields0"]').html(html);
					$('[name="fields2"]').html(html);
				}
		}
	});
}

function addField(){
	if(parseInt($('.removeLine:not(.hide)').length) == 2){
		$('.removeLine').last().addClass('hide');
	}	
	let num = $('#table_tbody tr:not(#cloneFields)').length + 1;
	let appendTo = $('#table_tbody');
	let cloneTr = $('#cloneFields');	
	let copyElement = cloneTr.clone(true, true);
	copyElement.removeClass('hide');
	copyElement.removeAttr('id');
	copyElement.find('[name="variable0"]').attr('name', `variable${num}`);
	copyElement.find('[name="fields0"]').attr('name', `fields${num}`);	
	copyElement.appendTo(appendTo);
	$('[name="count_inputs"]').val(num);	
}

function removeLine(){
	$(this).parent().parent().parent().remove();
	let num = $('#table_tbody tr:not(#cloneFields)').length;
	$('[name="count_inputs"]').val(num);
	$('.removeLine').last().removeClass('hide');
}