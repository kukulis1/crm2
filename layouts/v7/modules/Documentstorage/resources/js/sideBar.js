$(document).on('click', '.show-subfolders', function(){
  if($(this).hasClass('fa-plus')){
    $(this).parent().find('.submenu').removeClass('hide');
    $(this).removeClass('fa-plus');
    $(this).addClass('fa-minus');
  }else{
    $(this).parent().find('.submenu').addClass('hide');
    $(this).removeClass('fa-minus');
    $(this).addClass('fa-plus');
  } 
})