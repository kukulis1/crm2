$(document).ready(function(){

  if($('.title_group')){
    $('.saveButton').on('click', function () {
      $('#EditView').submit();
    });
  }

  let fileList = new Array();
  let folderid = $('[name="recordid"]').val();
  let userid = $('[name="user_id"]').val();
  let folder = $('[name="folder"]').val();

  // Dropzone.autoDiscover = false;
  const myDropzone = new Dropzone('#DropZoneDiv', {
                              url: "modules/Documentstorage/actions/uploadDocument.php",
                              dictDefaultMessage: "Pasirinkite arba vilkite failą įkėlimui", 
                              dictFallbackMessage: "Jūsų naršyklė nepalaiko vilkti ir mesti funkcijos",
                              dictFileTooBig: "Failas yra per didelis ({{filesize}}MiB). Maksimalus failo dydis: {{maxFilesize}}MiB.",
                              dictInvalidFileType: "Neleidžiamas failo formatas",
                              dictCancelUpload: "Atšaukti įkėlimą",
                              dictUploadCanceled: "Įkėlimas atšauktas",
                              dictCancelUploadConfirmation: "Ar tikrai norite atšaukti?",
                              dictRemoveFile: "Pašalinti failą",
                              addRemoveLinks: true,
                              dictRemoveFileConfirmation:  "Ar tikrai norite ištrinti faila?",
                              autoProcessQueue: false,
                              params: {folderid:folderid,userid:userid,folder:folder},
                              init: function () {
                                this.on("complete", function (file) {
                                  if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                                    $('#EditView').submit();
                                    $('.editViewBody').css('opacity',0.2);
                                    $('#wait').css('display','block');
                                  }
                                });
                              },
                              success:function(file, serverFileName)
                              {                                             
                                fileList[file.upload.uuid] = serverFileName;                             
                              },                  
                              removedfile: function(file)
                              {                            
                                let removeFile = fileList[file.upload.uuid];                               
                                $.post('modules/Documentstorage/actions/removeFile.php', {removeFile:removeFile},function(data,status){                                 
                                  console.log(status);
                                });
                                file.previewElement.remove();
                              }                              
  })

  $('.saveButton').on('click', function () {
    const acceptedFiles = myDropzone.getAcceptedFiles()
    for (let i = 0; i < acceptedFiles.length; i++) {
      setTimeout(function () {
        myDropzone.processFile(acceptedFiles[i])
      }, i * 2000)
    }
  });

  $('[name="type"]').on('change', function(){
    if($(this).val() > 0){
      $('.template_selector').removeClass('hide');
    }else{
      $('.template_selector').addClass('hide');
    }
  });
});



function deleteOldFile(){
  if(confirm("Ar tikrai norite ištrinti failą?")){
    $('.title_group').remove();
    $('.upload_content').removeClass('hide');
    $('[name="old_file"]').attr('disabled',false);
  }
}
