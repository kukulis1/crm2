$(document).ready(function () {

  $('.search_input').each(function () {
    $(this).keydown(function (e) {
      var key = e.which;
      if (key == 13) { // || key == 9
        searchFilter();
      }
    });
    // $(this).change(function () {
    //   if ($(this).val() != '') {
    //     searchFilter();
    //   }
    // });
  });

  $('#NextPageButton').on('click', function(){
    pagination('next');
  });

  $('#PreviousPageButton').on('click', function(){
    pagination('prev');
  });

  $('#pageToJumpSubmit').on('click', function(){
    if(parseInt($('#total_pages').val()) >= parseInt($('#pageToJump').val())){
      pagination('jump');
    }
  });

  $(document).on('click', '#PageJumpDropDown', function (e) {
    e.stopPropagation();
  });

});


function pagination(type){
      let recordid = $('#recordid').val();
      let folder = $('#folder').val();
      let from = parseInt($('#from_number').val());
      let to = parseInt($('#to_number').val());
      let perPage = parseInt($('#perPage').val());     
      let total_pages = parseInt($('#total_pages').val());  
      let total_records = parseInt($('#total_records').val());  
      let pageToJump = $('#pageToJump').val();


      if(type == 'next'){       
        if(parseInt($('#currentPage').val()) != total_pages){ 
          $('#currentPage').val(parseInt($('#currentPage').val())+1);
        }
      }else if(type == 'prev'){       
        if(parseInt($('#currentPage').val()) != 1){
          $('#currentPage').val(parseInt($('#currentPage').val())-1);
        }
      }else if(type == 'jump'){
        $('#currentPage').val(pageToJump);
        $('.jumper').removeClass('open');               
      }   
     
      let startAt = perPage * (parseInt($('#currentPage').val())-1);

      if(parseInt($('#currentPage').val()) == total_pages){
        $('#NextPageButton').attr('disabled',true);
        $('#currentPage').val()       
      }else if(parseInt($('#currentPage').val()) == 1){       
        $('#PreviousPageButton').attr('disabled',true);
      }  

      if(parseInt($('#currentPage').val()) < total_pages){
        $('#NextPageButton').attr('disabled',false); 
      }

      if(parseInt($('#currentPage').val()) > 1){
        $('#PreviousPageButton').attr('disabled',false);
      } 

      $('#curr_page').html(parseInt($('#currentPage').val()));

      if(type == 'next'){  
        if(from >= 1){
          from = from+perPage;
        }

        if(to != total_records){
          to = to+perPage;
          if(to >= total_records){
            $('#old_to_number').val(to);
            to = total_records;
          }
        }
      }else if(type == 'prev'){        
          from = from-perPage;
          if(to == total_records){
            to = parseInt($('#old_to_number').val())-perPage;
          }else{
            to = to-perPage;
          }
      }else if(type == 'jump'){        
        from = perPage * pageToJump - (perPage-1);        
        to = perPage * pageToJump;
      }
     
      $.ajax({
        type: "POST",
        url: "modules/Documentstorage/actions/pagination.php",
        data: {recordid:recordid,folder:folder,perPage:perPage,startAt:startAt},
        dataType: "JSON",
        beforeSend: function () {
          $('.files-list').css('opacity', '0.2');
          $("#wait").show();
        },
        success: function (response) {
          $('#from_rec').html(from);
          $('#to_rec').html(to);
          $('#from_number').val(from);
          $('#to_number').val(to);

          if (response == 'no_results') {
            $("#wait").hide();
            $('#filter-records').remove();
            $('.started-list').hide();
            // document.querySelector('.pagination').classList.add('hide');
            $('.files-list').css('opacity', '1');
            no_records.removeClass('hide');
            // $('#filter_rec').html(`Viso: 0`);
            $('.started-list').css('display', 'none');
            $('#filter_rec').css('display', 'block');
          } else if (response == 'empty') {
            $('#filter-records').remove();
            $("#wait").hide();
            $('.files-list').css('opacity', '1');
            no_records.addClass('hide');
            // $('#filter_rec').html(`Viso: 0`);
            $('.started-list').css('display', 'none');
            $('#filter_rec').css('display', 'block'); 
          } else {

            setTimeout(() => {

              $('.started-list').remove();  
              $("#wait").hide(); 
              let table = $('#listview-table');
              let tbody = document.createElement('tbody');
              tbody.setAttribute('id', 'filter-records');
              tbody.setAttribute('class', 'overflow-y files-list started-list');
              let html = '';
    
              if(response != undefined){
                response.forEach(record => {            
                  html += `<tr class="listViewEntries">
                  <td class="listViewEntryValue">
                    <span class="fieldValue"><span class="value">${record.filename}</span></span>
                  </td>
                  <td class="listViewEntryValue">
                    <span class="fieldValue"><span class="value">${record.owner}</span></span>
                  </td>
                  <td class="listViewEntryValue">
                    <span class="fieldValue"><span class="value">${record.extension}</span></span>
                  </td>
                  <td class="listViewEntryValue"> 
                    <span class="fieldValue"><span class="value">${formatBytes(record.size)}</span></span>
                  </td>
                  <td class="listViewEntryValue">
                    <span class="fieldValue"><span class="value">${record.createdtime}</span></span>
                  </td>
                  <td class="listViewEntryValue">
                    <span class="fieldValue">               
                    <a class="value" href="index.php?module=Documentstorage&view=DownloadFile&file_id=${record.id}"><i title="Parsisiųsti failą" class="fa fa-download alignMiddle" style="font-size: 25px;"></i></a>
                    </span>
                  </td>
                </tr>`;
                });
                
                if ($('#filter-records') != null) {
                  $('#filter-records').remove();
                  tbody.innerHTML = html;
                } else {
                  tbody.innerHTML = html;
                }
                table.append(tbody);
              }                      
            }, 2000);
            
          }
        }
      });
}


function searchFilter() {
  let title, owner, extension, size, createdtime;
  let recordid = $('#recordid').val();
  let folder = $('#folder').val();
  let no_records = $('#no_records');
  let record_id = $('#page_record_id').val();

  $('.listSearchContributor').each(function () {
    if ($(this).val() != '') {
      if ($(this).attr("id") == 'title') {
        title = $(this).val();
      }
      if ($(this).attr("id") == 'owner') {
        owner = $(this).val();
      }
      if ($(this).attr("id") == 'extension') {
        extension = $(this).val();
      }
      if ($(this).attr("id") == 'size') {
        size = $(this).val();
      }
      if ($(this).attr("id") == 'createdtime') {
        createdtime = $(this).val();
      }
    } else {
      no_records.addClass('hide');
      $('.started-list').css('display', 'block');
      $('.filter_rec').css('display', 'none');
      $('#filter-records').remove();
      $('.listViewActions').removeClass('hide');
    }
  });


  if (title != undefined || owner != undefined || extension != undefined || size != undefined || createdtime != undefined){   
    $.ajax({
      type: "POST",
      url: "modules/Documentstorage/actions/searchFile.php",
      data: {recordid:recordid,folder:folder,title:title,owner:owner,extension:extension,size:size,createdtime:createdtime},
      dataType: "JSON",
      beforeSend: function () {
        $('.files-list').css('opacity', '0.2');
        $("#wait").show();
        $('.listViewActions').addClass('hide');
      },
      success: function (response) {       
        if (response == 'no_results') {
          $("#wait").hide();
          $('#filter-records').remove();
          $('.started-list').hide();
          // document.querySelector('.pagination').classList.add('hide');
          $('.files-list').css('opacity', '1');
          no_records.removeClass('hide');
          // $('#filter_rec').html(`Viso: 0`);
          $('.started-list').css('display', 'none');
          $('#filter_rec').css('display', 'block');
        } else if (response == 'empty') {
          $('#filter-records').remove();
          $("#wait").hide();
          $('.files-list').css('opacity', '1');
          no_records.addClass('hide');
          // $('#filter_rec').html(`Viso: 0`);
          $('.started-list').css('display', 'none');
          $('#filter_rec').css('display', 'block'); 
        } else {
          $('.started-list').hide();

          let table = $('#listview-table');
          let tbody = document.createElement('tbody');
          tbody.setAttribute('id', 'filter-records');
          tbody.setAttribute('class', 'overflow-y files-list');
          let html = '';

          if(response != undefined){
            response.forEach(record => {            
              html += `<tr class="listViewEntries">
              <td class="listViewEntryValue">
                <span class="fieldValue"><span class="value">${record.filename}</span></span>
              </td>
              <td class="listViewEntryValue">
                <span class="fieldValue"><span class="value">${record.owner}</span></span>
              </td>
              <td class="listViewEntryValue">
                <span class="fieldValue"><span class="value">${record.extension}</span></span>
              </td>
              <td class="listViewEntryValue"> 
                <span class="fieldValue"><span class="value">${formatBytes(record.size)}</span></span>
              </td>
              <td class="listViewEntryValue">
                <span class="fieldValue"><span class="value">${record.createdtime}</span></span>
              </td>
              <td class="listViewEntryValue">
                <span class="fieldValue">
                <a class="value" style="max-width:20%;" href="index.php?module=Documentstorage&view=EditFile&folder=sub&file_id=${record.documentstorageid}&record=${record_id}"><i title="Redaguoti" class="fa fa-edit alignMiddle" style="font-size: 25px;"></i></a>

                 <a class="value" style="max-width:20%;" href="index.php?module=Documentstorage&view=DownloadFile&file_id=${record.documentstorageid}"><i title="Parsisiųsti failą" class="fa fa-download alignMiddle" style="font-size: 25px;"></i></a>

                 <a class="value"style="max-width:20%;" href="javascript:void(0);"><i title="Ištrinti failą" class="fa fa-trash alignMiddle" style="font-size: 25px;" onclick="deleteFile(event,${record.documentstorageid})"></i></a>
                </span>
              </td>
            </tr>`;
            });
            
            if ($('#filter-records') != null) {
              $('#filter-records').remove();
              tbody.innerHTML = html;
            } else {
              tbody.innerHTML = html;
            }
            table.append(tbody);
          }
          setTimeout(() => {
            $('.started-list').css('opacity', '1');
            $("#wait").hide();            
          }, 1000);
          
        }
      }
    });

  }
}

function formatBytes(bytes, precision = 2) { 
  let units = new Array('B', 'KB', 'MB', 'GB', 'TB'); 
  bytes = Math.max(bytes, 0); 
  let pow = Math.floor((bytes ? Math.log(bytes) : 0) / Math.log(1024)); 
  pow = Math.min(pow, units.length - 1); 
  bytes /= (1 << (10 * pow)); 
  return bytes.toFixed(precision) + ' ' + units[pow]; 
}

function deleteFolder(folder,id){
  let warning_text = (folder == 'parent' ? "Ar tikrai norite ištrinti aplanką? Ištrynus pagrindinį aplanką bus ištrinti visi jame esantys failai ir sub-aplankai su failais" : "Ar tikrai norite ištrinti aplanką? Ištrynus aplanką bus ištrinti visi jame esantys failai");
  if(confirm(warning_text)){
    window.location.href=`index.php?module=Documentstorage&view=DeleteFolder&folder=${folder}&record=${id}`;
  }
}

function deleteFile(event,id){
  if(confirm("Ar tikrai norite ištrinti failą?")){
    deleteFileProcess(event,id);
  }
}

function deleteFileProcess(event,recordid){
  $.ajax({
    type: "POST",
    url: "modules/Documentstorage/actions/removeFileFromList.php",
    data: {recordid:recordid},
    success: function (response) {
      if(response = 'success'){
        $(event.target).parent().parent().parent().parent().remove();
      }else{
        console.log(response);
      }
    }
  });
}