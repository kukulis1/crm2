function documentGenerator(template){
  // $('[name="generate_type"]').val(type);
  const recordid = $('[name="record_id"]').val();
  // let template = $('#template').val();
  $.ajax({
    type: "POST",
    url: "modules/Documentstorage/actions/getDocumentsByType.php",
    data: {template:template},
    dataType: "JSON",
    success: function (response) {
      let html = '';
      if(response.status == 'success'){
        if(response.data != ''){
          response.data.forEach(item => {
              html += `<li class="listViewFilter">
                          <a class="filterName listViewFilterElipsis" href="index.php?module=Documentstorage&view=GenerateDoc&templateid=${template}&docid=${item.documentstorageid}&record=${recordid}">${item.filename}</a>
                        </li>`;
        });

        }else{
          html += `<li class="listViewFilter" style="list-style: none;"><h5>Nėra šablonui priskirtų failų</h5></li>`;
        }

        $('#list-ul').html(html);
      }
    }
  });
}

// $('#template').on('change', function(){
//   let type = $('[name="generate_type"]').val();
//   documentGenerator(type);
// });

function uploadFile(e){
  let file = $(e.target)[0].files[0];
  let userid = _USERMETA.id;
  let recordid = $('[name="record_id"]').val();

  var formData = new FormData();
  formData.append("file", file);
  formData.append("userid", userid);
  formData.append("recordid", recordid);

  $.ajax({
    type: "POST",
    url: "modules/Employee/actions/uploadFile.php",
    data: formData,
    processData: false, 
    contentType: false,
    beforeSend: function(){
      $('#page').css('opacity',0.2);
      $('#wait').css('display','block');
    },
    success: function (response) {
      if(response == 'success'){
        window.location.href = `index.php?module=Employee&relatedModule=Documentstorage&view=Detail&record=${recordid}&mode=showRelatedList`;
      }else{
        alert('Įvyko klaida');
      }
    }
  });
}