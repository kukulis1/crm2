{assign var=ROLE_LIST value=$ROLE_LIST}
{assign var=HAS_PERMISION value=explode(",",$ROLE_LIST['role'])}

<div class="container-fluid main-container">
    <div class="row">
        <div id="modnavigator" class="module-nav detailViewModNavigator clearfix">
            <div class="hidden-xs hidden-sm mod-switcher-container">
                {include file="partials/Menubar.tpl"|vtemplate_path:$MODULE}
            </div>
        </div>
        <div class="detailViewContainer viewContent clearfix" style="margin-bottom: 150px;">
					<div class="detailview-header-block" style="margin-left: 37px;">
						<div class="detailview-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
											<div class="record-header clearfix">
												{if !$MODULE}
													{assign var=MODULE value=$MODULE_NAME}
												{/if}
												<div class="hidden-sm hidden-xs recordImage bg_{$MODULE} app-{$SELECTED_MENU_CATEGORY}">
													<div class="name"><span><i class="vicon-documents" title="Dokumentai"></i></span></div>
												</div>

												<div class="recordBasicInfo">
													<div class="info-row">
														<h4>
															<span class="recordLabel pushDown" title="name">							
																		<span>Nustatymai</span>&nbsp;						
															</span>
														</h4>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
													<div class="pull-right btn-toolbar">
														<div class="btn-group">           
																
																<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=List'">Gryžti į sąrašą</button> 						   
														</div>      
												</div>
										</div>
							</div>
						</div>
					</div>

            <div class="col-sm-12 col-xs-12">
                <div class="detailview-content container-fluid" style="margin-top: 20px">  

									<div class="block" style="margin-left: 20px;">										
										<div class="row">	
											<div class="col-sm-10 col-xs-10"><h4 class="textOverflowEllipsis ">Darbo sutarties numerio eilė</h4></div>										
											<div class="col-sm-2 col-xs-2">
												<div style="float: right;">
												<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=SettingsEditRowNo'" style="margin-top: 9px;">Redaguoti</button>
												</div>
											</div>
										</div>
											<hr>
												<div class="blockData">
													<table class="table detailview-table no-border">
														<tbody>															
																<tr>																									
																	<td class="fieldLabel textOverflowEllipsis">
																		<span class='muted'>{$NUMBER}</span>
																	</td>																																													
																</tr>												
														</tbody>
													</table>
												</div>
									</div>
									<br>
								          									
									<div class="block" style="margin-left: 20px;">	
										<div class="row">	
											<div class="col-sm-10 col-xs-10"><h4 class="textOverflowEllipsis ">Visus aplankus mato</h4></div>										
											<div class="col-sm-2 col-xs-2">
												<div style="float: right;">
												<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=SettingsEdit'" style="margin-top: 9px;">Redaguoti</button>
												</div>
											</div>
										</div>
											<hr>
												<div class="blockData">
													<table class="table detailview-table no-border">
														<tbody>	
														{assign var=COUNTER value=1}
														{foreach from=$ROLE_LIST['list'] item=ROLE}	
															{if in_array($ROLE['roleid'],$HAS_PERMISION)}			
																{if $COUNTER is odd}
																<tr>		
																{/if}								
																	<td class="fieldLabel textOverflowEllipsis">
																		<span class='muted'>{$ROLE['rolename']}</span>
																	</td>	
																{if $COUNTER is even}																													
																</tr>
																{/if}
															{/if}
																{assign var=COUNTER value=$COUNTER+1}
															{/foreach}
														</tbody>
													</table>
												</div>
									</div>
									<br>		
							</div>
         </div>
			</div>
	</div>
</div>
<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/menu.js"></script>