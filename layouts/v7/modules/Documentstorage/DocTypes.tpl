{assign var=DOC_TYPES value=$DOC_TYPES}

<div class="container-fluid main-container">
    <div class="row">
        <div id="modnavigator" class="module-nav detailViewModNavigator clearfix">
            <div class="hidden-xs hidden-sm mod-switcher-container">
                {include file="partials/Menubar.tpl"|vtemplate_path:$MODULE}
            </div>
        </div>
        <div class="detailViewContainer viewContent clearfix" style="margin-bottom: 150px;">
					<div class="detailview-header-block" style="margin-left: 37px;">
						<div class="detailview-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
											<div class="record-header clearfix">
												{if !$MODULE}
													{assign var=MODULE value=$MODULE_NAME}
												{/if}
												<div class="hidden-sm hidden-xs recordImage bg_{$MODULE} app-{$SELECTED_MENU_CATEGORY}">
													<div class="name"><span><i class="vicon-documents" title="Dokumentai"></i></span></div>
												</div>

												<div class="recordBasicInfo">
													<div class="info-row">
														<h4>
															<span class="recordLabel pushDown" title="name">							
																		<span>Dokumentų kategorijos</span>&nbsp;						
															</span>
														</h4>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
													<div class="pull-right btn-toolbar">
														<div class="btn-group">            
																<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=AddDocType'">Sukurti naują</button> 		
																<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=List'">Gryžti</button> 						   
														</div>      
												</div>
										</div>
							</div>
						</div>
					</div>

	<div class="col-sm-12 col-xs-12">
			<div class="detailview-content container-fluid" style="margin-top: 20px">  			
	{if $DOC_TYPES}      									
		{foreach from=$DOC_TYPES item=DOC_TYPE}	
			<div class="block" style="margin-left: 20px;">	
				<div class="row">	
					<div class="col-sm-10 col-xs-10"><h4 class="textOverflowEllipsis ">{$DOC_TYPE['file_type']}</h4></div>										
					<div class="col-sm-2 col-xs-2">
						<div style="float: right;">
							<input type="button" onclick="window.location.href='index.php?module=Documentstorage&view=EditDocType&record={$DOC_TYPE['id']}'" class="btn btn-default" value="Redaguoti" style="margin-top: 9px;">
							<a href="index.php?module=Documentstorage&view=DeleteDocType&record={$DOC_TYPE['id']}" onclick="return confirm('Ar tikrai norite ištrinti?');" class="btn btn-default" style="margin-top: 9px;">Trinti</a>
						</div>
					</div>
				</div>
				<hr>	
				<div class="blockData">
					<table class="table detailview-table no-border">
				<tbody>				
						<tr>																												
							<td class="fieldLabel textOverflowEllipsis">
								<span class='muted'>Automatiškai užsipildo</span>
							</td>	
							<td class="listViewEntryValue">
									<span class="fieldValue"><span class="value">{if $DOC_TYPE['type'] eq '1'}Taip{else}Ne{/if}</span></span>
							</td>	
								<td class="fieldLabel textOverflowEllipsis">
									<span class='muted'></span>
							</td>	
							<td class="listViewEntryValue">
									
							</td>																																									
						</tr>	
						<tr>
							<td class="fieldLabel textOverflowEllipsis">
								<span class='muted'>Sukurtas</span>
							</td>	
							<td class="listViewEntryValue">
									<span class="fieldValue"><span class="value">{$DOC_TYPE['created_at']}</span></span>
							</td>
								<td class="fieldLabel textOverflowEllipsis">
								<span class='muted'>Sukūrė</span>
							</td>	
							<td class="listViewEntryValue">
									<span class="fieldValue"><span class="value">{$DOC_TYPE['owner']}</span></span>
							</td>		
						</tr>		
							</tbody>
						</table>						
					</div>	
				</div>
				<br>		
		{/foreach}	
		{else}
			<div class="block" style="margin-left: 20px;">	
				<h6>Dokumentų kategorijų kol kas nėra</h6>
			</div>
		{/if}									
							
					
		</div>
</div>
			</div>
	</div>
</div>
<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/menu.js"></script>