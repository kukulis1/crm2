{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}
{assign var=CURRENT_USER value=$CURRENT_USER}
{strip}
	<div class="col-sm-11 col-xs-10 padding0 module-action-bar clearfix coloredBorderTop">
		<div class="module-action-content clearfix {$MODULE}-module-action-content">
			<div class="col-lg-7 col-md-6 col-sm-5 col-xs-11 padding0 module-breadcrumb module-breadcrumb-{$smarty.request.view} transitionsAllHalfSecond">
				{assign var=MODULE_MODEL value=Vtiger_Module_Model::getInstance($MODULE)}
				{if $MODULE_MODEL->getDefaultViewName() neq 'List'}
					{assign var=DEFAULT_FILTER_URL value=$MODULE_MODEL->getDefaultUrl()}
				{else}
					{assign var=DEFAULT_FILTER_ID value=$MODULE_MODEL->getDefaultCustomFilter()}
					{if $DEFAULT_FILTER_ID}
						{assign var=CVURL value="&viewname="|cat:$DEFAULT_FILTER_ID}
						{assign var=DEFAULT_FILTER_URL value=$MODULE_MODEL->getListViewUrl()|cat:$CVURL}
					{else}
						{assign var=DEFAULT_FILTER_URL value=$MODULE_MODEL->getListViewUrlWithAllFilter()}
					{/if}
				{/if}
				<a title="{vtranslate($MODULE, $MODULE)}" href='{$DEFAULT_FILTER_URL}&app={$SELECTED_MENU_CATEGORY}'><h4 class="module-title pull-left text-uppercase"> {vtranslate($MODULE, $MODULE)} </h4>&nbsp;&nbsp;</a>
                                {if $smarty.session.lvs.$MODULE.viewname}
					{assign var=VIEWID value=$smarty.session.lvs.$MODULE.viewname}
				{/if}
				{if $VIEWID}
					{foreach item=FILTER_TYPES from=$CUSTOM_VIEWS}
						{foreach item=FILTERS from=$FILTER_TYPES}
							{if $FILTERS->get('cvid') eq $VIEWID}
								{assign var=CVNAME value=$FILTERS->get('viewname')}
								{break}
							{/if}
						{/foreach}
					{/foreach}
					<p class="current-filter-name filter-name pull-left cursorPointer" title="{$CVNAME}"><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a href='{$MODULE_MODEL->getListViewUrl()}&viewname={$VIEWID}&app={$SELECTED_MENU_CATEGORY}'>&nbsp;&nbsp;{$CVNAME}&nbsp;&nbsp;</a> </p>
				{/if}
				{assign var=SINGLE_MODULE_NAME value='SINGLE_'|cat:$MODULE}
				{if $RECORD and $smarty.request.view eq 'Edit'}
					<p class="current-filter-name filter-name pull-left "><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a title="{$RECORD->get('label')}">&nbsp;&nbsp;{vtranslate('LBL_EDITING', $MODULE)} : {$RECORD->get('label')} &nbsp;&nbsp;</a></p>
				{else if $smarty.request.view eq 'Edit'}
					<p class="current-filter-name filter-name pull-left "><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a>&nbsp;&nbsp;{vtranslate('LBL_ADDING_NEW', $MODULE)}&nbsp;&nbsp;</a></p>
				{/if}
				{if $smarty.request.view eq 'Detail'}
					<p class="current-filter-name filter-name pull-left"><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a title="{$RECORD->get('label')}">&nbsp;&nbsp;{$RECORD->get('label')} &nbsp;&nbsp;</a></p>
				{/if}
			</div>
			<div class="col-lg-5 col-md-6 col-sm-7 col-xs-1 padding0 pull-right">
				<div id="appnav" class="navbar-right">
					<nav class="navbar-inverse border0 margin0">
						{if $MODULE_BASIC_ACTIONS|@count gt 0}
						<div class="container-fluid">
							<div class="navbar-header bg-white marginTop5px">
								<button type="button" class="navbar-toggle collapsed margin0" data-toggle="collapse" data-target="#appnavcontent" aria-expanded="false">
									<i class="fa fa-ellipsis-v"></i>
								</button>
							</div>

							<div class="navbar-collapse collapse" id="appnavcontent" aria-expanded="false" style="height: 1px;">
								<ul class="nav navbar-nav">
									<button style="padding: 6px 12px; margin-right: 10px; margin-top: 3px;" class="btn addButton btn-primary" onclick="window.open('/faq/faq.php#Documentstorage' , '_blank');">Pagalba <i class="fa fa-question"></i></button>
									{if $smarty.request.view eq 'List'}	
													<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;"	class="btn addButton btn-default module-buttons" 
															onclick="window.location.href='index.php?module={$MODULE}&view=Templates'">
																<div class="fa fa-file" aria-hidden="true"></div>&nbsp;&nbsp;Šablonai
													</button>	
												{* <button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;"	class="btn addButton btn-default module-buttons" 
															onclick="window.location.href='index.php?module={$MODULE}&view=DocTypes'">
																<div class="fa fa-file" aria-hidden="true"></div>&nbsp;&nbsp;Dokumentų kategorijos
												</button>	 *}
									{/if}
									{if $smarty.request.view eq 'List' && $CURRENT_USER->roleid eq 'H2'}
										<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;"	class="btn addButton btn-default module-buttons" 
														onclick="window.location.href='index.php?module={$MODULE}&view=Settings'">
															<div class="fa fa-wrench" aria-hidden="true"></div>&nbsp;&nbsp;Nustatymai
												</button>									
									{else if $smarty.request.view eq 'Catalog' OR $smarty.request.view eq 'SubCatalog'}
											<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;"	class="btn addButton btn-default module-buttons" 
														onclick="window.location.href='index.php?module={$MODULE}&view=AddNewFile&folder={if $smarty.request.view eq 'Catalog'}parent{else}sub{/if}&record={$smarty.request.record}'">
															<div class="fa fa-plus" aria-hidden="true"></div>&nbsp;&nbsp;Pridėti failus
												</button>									
									{/if}						
								</ul>

							</div>
						</div>
						{/if}
					</nav>
				</div>
			</div>
		</div>
		{if $FIELDS_INFO neq null}
			<script type="text/javascript">
				var uimeta = (function () {
					var fieldInfo = {$FIELDS_INFO};
					return {
						field: {
							get: function (name, property) {
								if (name && property === undefined) {
									return fieldInfo[name];
								}
								if (name && property) {
									return fieldInfo[name][property]
								}
							},
							isMandatory: function (name) {
								if (fieldInfo[name]) {
									return fieldInfo[name].mandatory;
								}
								return false;
							},
							getType: function (name) {
								if (fieldInfo[name]) {
									return fieldInfo[name].type
								}
								return false;
							}
						},
					};
				})();
			</script>
		{/if}
	</div>     
{/strip}
