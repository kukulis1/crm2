{assign var=FOLDER_INFO value=$FOLDER_INFO}
{assign var=FOLDER_FILES value=$FOLDER_FILES}
{assign var=FOLDER_FILES_NUM_ROWS value=$FOLDER_FILES_NUM_ROWS}
{assign var=SUBFOLDER_INFO value=$SUBFOLDER_INFO}
{assign var=SUBFOLDER_INFO_NUM_ROWS value=$SUBFOLDER_INFO_NUM_ROWS}
{assign var=TOTAL_FILES value=$TOTAL_FILES}
{assign var=PER_PAGE value=$PER_PAGE}
{assign var=TOTAL_PAGES value=$TOTAL_PAGES}
{assign var=EDIT_PERMISION value=$EDIT_PERMISION}

<input type="hidden" id="recordid" value={$FOLDER_INFO['id']}>
<input type="hidden" id="folder" value='2'>
<input type="hidden" id="page_record_id" value="{$smarty.request.record}">
<link rel="stylesheet" type="text/css" href="layouts/v7/modules/Documentstorage/resources/css/main.css"/>
<div class="container-fluid main-container">
    <div class="row">
        <div id="modnavigator" class="module-nav detailViewModNavigator clearfix">
            <div class="hidden-xs hidden-sm mod-switcher-container">
                {include file="partials/Menubar.tpl"|vtemplate_path:$MODULE}
            </div>
        </div>
        <div class="detailViewContainer viewContent clearfix" style="margin-bottom: 150px;">
					<div class="detailview-header-block" style="margin-left: 37px;">
						<div class="detailview-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
											<div class="record-header clearfix">
												{if !$MODULE}
													{assign var=MODULE value=$MODULE_NAME}
												{/if}
												<div class="hidden-sm hidden-xs recordImage bg_{$MODULE} app-{$SELECTED_MENU_CATEGORY}">
													<div class="name"><span><i class="vicon-documents" title="Dokumentai"></i></span></div>
												</div>

												<div class="recordBasicInfo">
													<div class="info-row">
														<h4>
															<span class="recordLabel pushDown" title="name">							
																		<span>{$FOLDER_INFO['title']}</span>&nbsp;						
															</span>
														</h4>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 detailViewButtoncontainer">
													<div class="pull-right btn-toolbar">
														<div class="btn-group">
															<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=Catalog&record={$FOLDER_INFO['parent']}'">Gryžti į pagrindinį aplanką</button> 		
															{if $EDIT_PERMISION}                 
																<button class="btn btn-default" onclick="window.location.href='index.php?module={$MODULE}&view=EditCatalog&folder={if $smarty.request.view eq 'Catalog'}parent{else}sub{/if}&record={$smarty.request.record}'">Redaguoti</button> 	
																 <button class="btn btn-danger" onclick="deleteFolder('sub',{$smarty.request.record})">Trinti</button>  
															{/if}																				   
														</div>      
												</div>
										</div>
							</div>
						</div>
					</div>

            <div class="col-sm-12 col-xs-12">
                <div class="detailview-content container-fluid" style="margin-top: 20px">            									
									<div class="block" style="margin-left: 20px;">													
											<div>	<h4 class="textOverflowEllipsis maxWidth50">Informacija</h4></div>
											<hr>
												<div class="blockData">
													<table class="table detailview-table no-border">
														<tbody>				
															<tr>						
																<td class="fieldLabel textOverflowEllipsis">
																	<span class='muted'>Pavadinimas</span>
																</td>
																<td class="fieldValue">
																	<span class="value">{$FOLDER_INFO['title']}</span>
																</td>	

																<td class="fieldLabel textOverflowEllipsis">
																	<span class='muted'>Tipas</span>
																</td>
																<td class="fieldValue">
																	<span class="value">{if $FOLDER_INFO['type'] eq 1}Viešas{else}Privatus{/if}</span>
																</td>	
															</tr>

																<tr>						
																<td class="fieldLabel textOverflowEllipsis">
																	<span class='muted'>Sukurta</span>
																</td>
																<td class="fieldValue">
																	<span class="value">{$FOLDER_INFO['createdtime']}</span>
																</td>	

																<td class="fieldLabel textOverflowEllipsis">
																	<span class='muted'>Redaguota</span>
																</td>
																<td class="fieldValue">
																	<span class="value">{$FOLDER_INFO['modifiedtime']}</span>
																</td>	
															</tr>
																<tr>						
																<td class="fieldLabel textOverflowEllipsis">
																	<span class='muted'>Sukūrė</span>
																</td>
																<td class="fieldValue">
																	<span class="value">{$FOLDER_INFO['owner']}</span>
																</td>	

																<td class="fieldLabel textOverflowEllipsis"></td>
																<td class="fieldValue"></td>	
															</tr>
														</tbody>
													</table>
												</div>
									</div>
									<br>		
							</div>
         </div>

				  <div class="col-sm-12 col-xs-12">
					<div class="detailview-content container-fluid" style="margin-top: 20px">            									
									<div class="block" style="margin-left: 20px;">													
											<div class="col-sm-10 col-xs-10"><h4 class="textOverflowEllipsis maxWidth50">Failai</h4></div>
											<div class="col-sm-2 col-xs-2">
											<input type="hidden" id="total_records" value="{$TOTAL_FILES}">																			
											<input type="hidden" id="perPage" value="{$PER_PAGE}">																			
											<input type="hidden" id="currentPage" value="1">																			
											<input type="hidden" id="total_pages" value="{$TOTAL_PAGES}">																			
											<input type="hidden" id="from_number" value="1">																			
											<input type="hidden" id="to_number" value="{$PER_PAGE}">																			
											<input type="hidden" id="old_to_number" value="">																			
											{assign var=SHOWPAGEJUMP value=1}
												<div class="listViewActions" style="margin-top: 10px;">
													<div class="btn-group pull-right jumper">
													<button type="button" id="PreviousPageButton" class="btn btn-default" disabled><i class="fa fa-caret-left"></i></button>
															{if $SHOWPAGEJUMP}
																	<button type="button" id="PageJump" data-toggle="dropdown" class="btn btn-default">
																			<i class="fa fa-ellipsis-h icon" title="{vtranslate('LBL_LISTVIEW_PAGE_JUMP',$moduleName)}"></i>
																	</button>

																	<ul class="{$CLASS_VIEW_BASIC_ACTION} dropdown-menu" id="PageJumpDropDown">
																			<li>
																					<div class="listview-pagenum">
																							<span >{vtranslate('LBL_PAGE',$moduleName)}&nbsp;&nbsp;<strong id="curr_page">1</strong></span>
																							<strong><span>{$PAGE_NUMBER}</span></strong>&nbsp;
																							<span >{vtranslate('LBL_OF',$moduleName)}&nbsp;&nbsp; <strong>{$TOTAL_PAGES}</strong></span>&nbsp;
																							<strong><span class="totalPageCount"></span></strong>
																					</div>
																					<div class="listview-pagejump">
																							<input type="text" id="pageToJump" placeholder="{vtranslate('LBL_LISTVIEW_JUMP_TO',$moduleName)}" class="listViewPagingInput text-center"/>&nbsp;
																							<button type="button" id="pageToJumpSubmit" class="btn btn-success {$CLASS_VIEW_PAGING_INPUT_SUBMIT} text-center">{'GO'}</button>
																					</div>    
																			</li>
																	</ul>
															{/if}
															<button type="button" id="NextPageButton" class="btn btn-default" {if $TOTAL_FILES <= $PER_PAGE}disabled{/if}><i class="fa fa-caret-right"></i></button>
														</div>
															<span class="pageNumbers  pull-right" style="position:relative;top:7px;">
																	<span class="pageNumbersText">
																			<span id="from_rec">1</span> {vtranslate('LBL_TO', $MODULE)} <span id="to_rec">{if $TOTAL_FILES<$PER_PAGE}{$TOTAL_FILES}{else}{$PER_PAGE}{/if}</span>
																	</span>
																	&nbsp;       
																	<span class="totalNumberOfRecords3 cursorPointer {if !$SUBFOLDER_INFO_NUM_ROWS} hide{/if}" title="{vtranslate('LBL_SHOW_TOTAL_NUMBER_OF_RECORDS', $MODULE)}">{vtranslate('LBL_OF', $MODULE)}&nbsp;<span id="countShow">{$TOTAL_FILES}</span></span>&nbsp;&nbsp;
																
															</span>
													</div>
											</div>
											
								
												<div class="blockData">
												<div id="wait" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>	
													<table id="listview-table" class="table {if !$FOLDER_FILES}listview-table-norecords{/if} listview-table">
															<thead>
																<tr class="listViewContentHeader">
																	<th>
																		<span class="listViewContentHeaderValues">Pavadinimas</span>
																	</th>
																	<th>
																		<span class="listViewContentHeaderValues">Įkėles asmuo</span>
																	</th>
																	<th>
																		<span class="listViewContentHeaderValues">Plėtinys</span>
																	</th>
																	<th>
																		<span class="listViewContentHeaderValues">Dydis</span>
																	</th>
																	<th>
																		<span class="listViewContentHeaderValues">Įkėlimo data</span>
																	</th>
																	<th>
																		<span class="listViewContentHeaderValues">Veiksmas</span>
																	</th>
																</tr>
																<tr class="searchRow listViewSearchContainer">													
																	<th><input type="text" id="title" class="listSearchContributor inputElement search_input" value=""></th>
																	<th><input type="text" id="owner" class="listSearchContributor inputElement search_input" value=""></th>
																	<th><input type="text" id="extension" class="listSearchContributor inputElement search_input" value=""></th>
																	<th><input type="text" id="size" class="listSearchContributor inputElement search_input" value=""></th>
																	<th><input type="text" id="createdtime" class="listSearchContributor inputElement search_input dateField" data-date-format="yyyy-mm-dd" data-calendar-type="range"autocomplete="off"  value=""></th>
																	<th></th>																													
																</tr>
															</thead>
															<tbody class="{if $SUBFOLDER_INFO_NUM_ROWS}overflow-y{/if} files-list started-list">
															{if $SUBFOLDER_INFO_NUM_ROWS}
																{foreach from=$FOLDER_FILES item=item}
																		<tr class="listViewEntries">
																			<td class="listViewEntryValue">
																				<span class="fieldValue"><span class="value"><a href="index.php?module={$MODULE}&view=Detail&record={$item['documentstorageid']}">{$item['filename']}</a></span></span>
																			</td>
																			<td class="listViewEntryValue">
																				<span class="fieldValue"><span class="value">{$item['owner']}</span></span>
																			</td>
																			<td class="listViewEntryValue">
																				<span class="fieldValue"><span class="value">{$item['extension']}</span></span>
																			</td>
																			<td class="listViewEntryValue"> 
																				<span class="fieldValue"><span class="value">{$MODULE_MODEL->formatBytes($item['size'])}</span></span>
																			</td>
																				<td class="listViewEntryValue">
																				<span class="fieldValue"><span class="value">{$item['createdtime']}</span></span>
																			</td>
																			<td class="listViewEntryValue">
																				<span class="fieldValue">
																					<a class="value" style="max-width:20%;" href="index.php?module={$MODULE}&view=DownloadFile&file_id={$item['documentstorageid']}"><i title="Parsisiųsti failą" class="fa fa-download alignMiddle" style="font-size: 25px;"></i></a>
																					{if $MODULE_MODEL->checkFileEditPermision($item['documentstorageid'],$CURRENT_USER)}
																						<a class="value" style="max-width:20%;" href="index.php?module={$MODULE}&view=EditFile&folder=sub&file_id={$item['documentstorageid']}&record={$smarty.request.record}"><i title="Redaguoti" class="fa fa-edit alignMiddle" style="font-size: 25px;"></i></a>							
																						<a class="value" style="max-width:20%;" href="javascript:void(0);"><i title="Ištrinti failą" class="fa fa-trash alignMiddle" style="font-size: 25px;" onclick="deleteFile(event,{$item['documentstorageid']})"></i></a>
																					{/if}
																				</span>
																			</td>
																		</tr>
																	{/foreach}
																{/if}																
															</body>
													</table>
													<h6 id="no_records" class="lists-header {if $SUBFOLDER_INFO_NUM_ROWS}hide{/if}"><center> {vtranslate('LBL_EMPTY',$MODULE)}</center></h6>
												</div>
									</div>
									<br>		
							</div>
					</div>


			</div>
	</div>
</div>
<script type="text/javascript" src="layouts/v7/modules/{$MODULE}/resources/js/catalog.js"></script>
<script type="text/javascript" src="layouts/v7/modules/Documentstorage/resources/js/menu.js"></script>