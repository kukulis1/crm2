
// total records show number
$('.totalNumberOfRecords2').on('click', function () {
  $('.showTotalCountIcon2').addClass('hide');

  setTimeout(function () {
    $('#count_show').removeClass('hide');
  }, 1000);
});


function filterPurchaseOrdersPayment() {
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let form_array = new Array();
  let html = '';
  let fake_search = document.getElementById('fake_search');
  $('#messageBar').removeClass('hide');
  $('#wait3').show();

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      form_array.push({ [form_key]: form_value });
    }
  }

  $.ajax({
    type: "POST",
    url: '/purchase/filterPurchaseOrdersPayment.php',
    data: { form_array: form_array },
    dataType: "JSON",
    success: function (response) {
      response.forEach(res => {
        html += `
        <tr ${(res.exported == '1') ? 'style="background: lightgoldenrodyellow;"' : ''} class="listViewEntries" data-id="${res.purchaseorderid}"
        data-recordurl="index.php?module=Purchaseinvoice&view=Detail&record=${res.purchaseorderid}&app=SALES&independent=true"
        id="Unpaidinvoices_listView_row_1">
        <td class="listViewRecordActions">
          <!--LIST VIEW RECORD ACTIONS-->
          <div class="table-actions">
            <span class="input">
              <input type="checkbox" value="${res.purchaseorderid}" class="listViewEntriesCheckBox" /></span><span>            
            </span>           
          </div>
        </td>    



        <td class="listViewEntryValue" data-vendorid="${res.vendorid}" data-name="vendor_id" title="${res.vendor_id}" data-rawvalue="${res.vendorid}"
        data-field-type="reference">
        <span class="fieldValue">
          <span class="value">
            <a class="js-reference-display-value" href="?module=Vendors&view=Detail&record=${res.vendorid}" title="Klientai">${res.vendor_id}</a>
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>

      <td class="listViewEntryValue" data-name="purchaseorder_no" title="${res.invoice_nr}" data-rawvalue="${res.invoice_nr}"
      data-field-type="string">
      <span class="fieldValue">
        <span class="value">
         ${res.invoice_nr}
        </span>
      </span>
    </td>

    <td class="listViewEntryValue" data-name="invoicedate" title="${res.invoicedate}" data-rawvalue="${res.invoicedate}"
    data-field-type="date">
    <span class="fieldValue">
      <span class="value">
        ${res.invoicedate}
      </span>
    </span>
    <span class="hide edit"> </span>
  </td>


      <td class="listViewEntryValue" data-name="hdnGrandTotal" title="" data-rawvalue="" data-field-type="currency">
      <span class="fieldValue">
        <span class="value">
          ${res.hdnGrandTotal}
        </span>
      </span>
    </td>     


          <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${res.debt}</span>
          </span>
        </td>

      <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${res.pay_date}</span>
        </span>
      </td>


        <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${res.payed}</span>
          </span>
        </td>

        <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${res.pay_type}</span>
        </span>
      </td>



      </tr>
        `;
      });

      fake_search.innerHTML = html;
      setTimeout(() => {
        fake_search.classList.remove('hide');
        $('#messageBar').addClass('hide');
        $('#wait3').hide();
      }, 1500);
    }
  });

}
