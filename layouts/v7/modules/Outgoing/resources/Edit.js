$(document).on('blur', '[name="account_id_display"]', function(){
  const address = $('[name="CUSTOMER_ADDRESS"]');
  const country = $('[name="CONSIGNEE_COUNTRY"]');
  const city = $('[name="CUSTOMER_CITY"]');
  const postCode = $('[name="CUSTOMER_POST_CODE"]');
  const customer_id = $('#customer_id');
  const goods_json = $('#goods_json');

  setTimeout(() => {
    const accountid = $(this).parent().find('[name="account_id"]').val();
    $('[name="editContent"]').css('opacity',0.2);
    $('#wait').show();
    $.post("modules/Arriving/actions/getClientInfo.php", {accountid:accountid,which:'outgoing'},
      function (res, textStatus, jqXHR) {
        if(res.status == 'success'){
          address.val(res.data.bill_street);
          country.val(res.data.bill_country);
          city.val(res.data.bill_city);
          postCode.val(res.data.bill_code);
          customer_id.val(res.data.customer_id);
          goods_json.val(res.rests);
          $('[name="editContent"]').css('opacity',1);
          $('#wait').hide();
        }
      },
      "JSON"
    );
  }, 100);  
});

$(document).on('click', '[name="REQUIRED_TRANSPORT"]', function(){
  if($(this).is(':checked')){
    $('#load_section').css('display','block');
    $('[name="CARGO_NO"]').attr('disabled', true);
    $('[name="UNLOAD_DATE"]').attr('required',true);
  }else{
    $('#load_section').css('display','none');
    $('[name="CARGO_NO"]').attr('disabled', false);
    $('[name="UNLOAD_DATE"]').attr('required',false);
  }
});

$(document).on('click','[name="generateManifest"]', function(){
  if($(this).is(':checked')){
    $('[name="MANIFEST_NO"]').attr('disabled',true);
    $('[name="MANIFEST_NO"]').attr('required',false);
  }else{
    $('[name="MANIFEST_NO"]').attr('disabled',false);
    $('[name="MANIFEST_NO"]').attr('required',true);
  }
});

$(document).on('change', '[name="WAREHOUSE_NME"]', function(){
  let name = $(this).find(':selected').text();
  $('[name="WAREHOUSE_TEXT"]').val(name);
});


(function() {
  $('[name="SENDER_NAME"]').autocomplete({   
    'minLength': "3",
    'source': function (request, response) {
      $.ajax({
        url: "modules/Arriving/actions/getCompaniesHistory.php",
        dataType: "JSON",
        data: {
          company: request.term,
          account_id: $('[name="account_id"]').val(),
          type: "get_unload_companies_list"
        },
        success: function (data) {   
          $('[name="SENDER_ADDRESS"]').val('');         
          $('[name="SENDER_CITY"]').val('');
          $('[name="SENDER_POST_CODE"]').val('');
          $('[name="SENDER_COUNTRY"]').val('');
          $('[name="SENDER_CONTACT"]').val('');
          $('[name="SENDER_PHONE"]').val('');
          response(data);
        }
      });
    },
    select: function (event, ui) {

      $.ajax({
        url: "modules/Arriving/actions/getCompaniesHistory.php",
        dataType: "JSON",
        data: {
          company_code: ui.item.code,
          type: "get_unload_company_info"
        },
        success: function (data) {      
          $('[name="SENDER_ADDRESS"]').val(data[0].street);         
          $('[name="SENDER_CITY"]').val(data[0].city);
          $('[name="SENDER_POST_CODE"]').val(data[0].post_code);
          $('[name="SENDER_COUNTRY"]').val(data[0].country);
          $('[name="SENDER_CONTACT"]').val(data[0].contact);
          $('[name="SENDER_PHONE"]').val(data[0].phone);                                  
        }
      });
    },
  });

})();

function openGoodsModal(){
  const customer_id = $('#customer_id').val();
  if(customer_id != ''){
    $('#goodsModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    $('#is_editing').val(0);
  }else{
    alert('Pirma pasirinkite savininką');
  }
}

function initAddTare(){
    let json = $('#tares_json').val();
    let tares = JSON.parse(json);
    let count = $('.lineItem').length+1;
    let p = 10*count;  
    let html = `<tr class="lineItem">
                  <td style="padding-left:${p}px;">
                    <select class="inputElement" id="Packing${count}" style="width: 100%;">
                      <option value="0">Be taros</option>`;
                      tares.forEach(element => {
                        if(element.id != 1 && element.id != 2 && element.id != 3 && element.id != 13 && element.id != 14){
                         html += `<option value="${element.id}">${element.code}</option>`;
                        }
                      });
              html += ` </select>
                  </td>
                  <td><input type="text" class="inputElement" id="qty${count}" value="1" style="width:50px;"></td>
                  <td><input type="text" class="inputElement" id="barcode${count}"></td>`;
                  html += `</tr>`;
     if(count == 3){             
       $('.addTare').attr('disabled',true);
     }
    $('#good_body').append(html);
}

function initGetGoods(){
  const customer_id = $('#customer_id').val();  
  if(customer_id != ''){
    const goods = JSON.parse($('#goods_json').val());
    let html = ''; 
    if(goods.length > 0){
      $('.addTare').attr('disabled',true);
      let count = $('.goodsItem').length+1;     
        html += `<tr class="goodsItem" data-id="${count}">
          <td>
            <select class="inputElement goodsSelector" id="goods${count}" style="width:100%" onchange="initSelector(event);">`;
            goods.forEach(ITEM => {
              html += `<option data-qty="${ITEM.I_QTY}" data-itemcode="${ITEM.ITEM_CODE}" data-weight="${ITEM.ITEM_WEIGHT}" data-qtyinpll="${ITEM.I_PLL_QTY}" value="${ITEM.ITEM_ID}">${ITEM.ITEM_NME}</option>`;
            });
          html += `</select>
            </td>
            <td><input type="text" class="inputElement goodsQty" onchange="initQtyCheck(event);" id="goods_qty${count}" value="${goods[0].I_QTY}" style="width:50px" onkeypress="return isNumber(event);" onpaste="return isNumber(event);" onkeyup="return isNumber(event);"></td>
            <td class="code">${(goods[0].ITEM_CODE != null ? goods[0].ITEM_CODE : '')}</td>
            <td class="weight">${(goods[0].ITEM_WEIGHT != null ? goods[0].ITEM_WEIGHT : 0)}</td>
            <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteGood(event);"></i></td>
          </tr>`; 
        if($('#weight').val() == ''){
          $('#weight').val((goods[0].ITEM_WEIGHT != null ? goods[0].ITEM_WEIGHT : 0));          
        }

        $('#goods-tbody').append(html);        
        $('#goods_table').removeClass('hide');
    }else{
      alert('Pasirinktas savininkas neturi prekių');
    }
  }else{
    alert('Pirma pasirinkite savininką');
  }
}

function saveGoods(){
  let html = '';
  const tares = $('.lineItem').length;
  const goods = $('.goodsItem').length;
  const is_editing = $('#is_editing').val();
  let tbodys;  

  if(is_editing == 1){
    tbodys = $('#whish_editing').val();
  }else{
    tbodys = $('.tr-border').length+1;
  }   

  html += '<tbody class="tr-border">';
  for(i=1;i<=tares;i++){
    html += `<tr>
    <td ${(i > 1 ? `style="padding-left:${i}0px;"` : '')}>${(i > 1 ? `<img src="resources/level-up-alt-solid.svg" alt="Arrow" style="height: 15px;padding-left: 6px;margin-top: -9px;" class="fa-rotate-90">` : '')} ${$(`#Packing${i}`).find(":selected").text()} 
    <input type="hidden" class="group_tare" name="group_${tbodys}package${i}" value="${$(`#Packing${i}`).find(":selected").val()}"></td>   
    <td><input type="text" class="inputElement group_qty" name="group_${tbodys}qty${i}" value="${$(`#qty${i}`).val()}"></td>   
    <td><input type="text" class="inputElement group_barcode" name="group_${tbodys}barcode${i}" value="${$(`#barcode${i}`).val()}"></td>
    <td>`;
    if(i == 1){
      html += `  ${($(`#weight`).val() == '' ? 0 : $(`#weight`).val())} ${($('#length').val() == '' ? 0 : $('#length').val())}x${($('#width').val() == '' ? 0 : $('#width').val())}x${($('#height').val() == '' ? 0 : $('#height').val())}`;
    }
    html += `</td>
    <td>${(i == 1 ? `<i class="fa fa-pencil cursorPointer editIcon" onclick="openEditModal(event,${tbodys});" title="Redaguoti" style="margin-right: 10px;"></i>
                     <i class="fa fa-trash cursorPointer" onclick="deleteBlock(event);" title="Trinti"></i>` : '') }
    </td>
      <input type="hidden" class="hidden_weight" name="group_${tbodys}weight"  value="${($(`#weight`).val() == '' ? 0 : $(`#weight`).val())}">
      <input type="hidden" class="hidden_length" name="group_${tbodys}length" value="${($('#length').val() == '' ? 0 : $('#length').val())}">   
      <input type="hidden" class="hidden_width" name="group_${tbodys}width" value="${($('#width').val() == '' ? 0 : $('#width').val())}">   
      <input type="hidden" class="hidden_height" name="group_${tbodys}height" value="${($('#height').val() == '' ? 0 : $('#height').val())}">   
    </tr>`;
  }

  for(e=1;e<=goods;e++){
    html += `<tr class="goods-tr">
      <td>${$(`#goods${e}`).find(":selected").text()}<input type="hidden" class="goods_item" name="group_${tbodys}good${e}" value="${$(`#goods${e}`).find(":selected").val()}"></td>
      <td><input type="text" class="inputElement goods_qty" name="group_${tbodys}goods_qty${e}" value="${$(`#goods_qty${e}`).val()}"></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>`;
  }
  html += `<input type="hidden" class="group_tares_count" name="group_tares${tbodys}" value="${tares}">
          <input type="hidden" class="group_goods_count" name="group_goods${tbodys}" value="${goods}">
          </tbody>`;

  if(is_editing == 1){
    $(html).insertAfter( $('.tr-border').eq(tbodys-1) );
    $('.tr-border').eq(tbodys-1).remove();   
  }else{
    $('#goods_content').append(html);  
    $('[name="groups_count"]').val(tbodys);
  }


  $('#goodsModal').modal('hide');
  $('.deleteRow').attr('disabled',false);
  clearModal();
}

function openEditModal(event,count){
  $('#whish_editing').val(count);
  const tare_count = parseInt($(`[name="group_tares${count}"]`).val());
  const goods_count = parseInt($(`[name="group_goods${count}"]`).val());
  const block = $(event.target).parent().parent().parent();
  $('#is_editing').val(1);
  $('#goodsModal').modal({
    backdrop: 'static',
    keyboard: false
  });

  for (i=1; i < tare_count; i++) {
    $('.addTare').click();
  }

  for (i=1; i <= tare_count; i++) {
   let tareid = block.find(`[name="group_${count}package${i}"]`).val(); 
   let qty = block.find(`[name="group_${count}qty${i}"]`).val(); 
   let barcode = block.find(`[name="group_${count}barcode${i}"]`).val(); 
   let weight = block.find(`[name="group_${count}weight"]`).val(); 
   let length = block.find(`[name="group_${count}length"]`).val(); 
   let width = block.find(`[name="group_${count}width"]`).val(); 
   let height = block.find(`[name="group_${count}height"]`).val(); 

   $(`#Packing${i}`).val(tareid);
   $(`#qty${i}`).val(qty);
   $(`#barcode${i}`).val(barcode);

   $(`#weight`).val(weight);
   $(`#length`).val(length);
   $(`#width`).val(width);
   $(`#height`).val(height);
  }

  for (i=1; i <= goods_count; i++) {
    $('.addGood').click();
    let goodid = block.find(`[name="group_${count}good${i}"]`);
    let goodqty = block.find(`[name="group_${count}goods_qty${i}"]`); 


    $(`#goods${i}`).val(goodid.val());
    $(`#goods_qty${i}`).val(goodqty.val());

    const itemcode =  $(`#goods${i}`).find(":selected").data('itemcode');
    const weight =  $(`#goods${i}`).find(":selected").data('weight');

    $(`#goods${i}`).parent().parent().find('.code').text(itemcode);
    $(`#goods${i}`).parent().parent().find('.weight').text(weight); 
  }
}

function deleteBlock(event){
  if(confirm('Ar tikrai norite ištrinti?')){
    $(event.target).parent().parent().parent().remove();

    const count = $('.tr-border').length;
    $('[name="groups_count"]').val(count);   

    for (i=1; i <= count; i++) {
      const tare_count = parseInt($('.group_tares_count').eq(i-1).val());
      const goods_count = parseInt($('.group_goods_count').eq(i-1).val());

      for (e=1; e <= tare_count; e++) {  
        $('.tr-border').eq(i-1).find('.group_tare').eq(e-1).attr('name', `group_${i}package${e}`);
        $('.tr-border').eq(i-1).find('.group_qty').eq(e-1).attr('name', `group_${i}qty${e}`);
        $('.tr-border').eq(i-1).find('.group_barcode').eq(e-1).attr('name', `group_${i}barcode${e}`); 
        
        $('.tr-border').eq(i-1).find('.hidden_weight').eq(e-1).attr('name', `group_${i}weight`); 
        $('.tr-border').eq(i-1).find('.hidden_length').eq(e-1).attr('name', `group_${i}length`); 
        $('.tr-border').eq(i-1).find('.hidden_width').eq(e-1).attr('name', `group_${i}width`); 
        $('.tr-border').eq(i-1).find('.hidden_height').eq(e-1).attr('name', `group_${i}height`); 

        $('.tr-border').eq(i-1).find('.editIcon').eq(e-1).attr('onclick', `openEditModal(event,${i});`); 

        $('.tr-border').eq(i-1).find('.group_tares_count').eq(e-1).attr('name', `group_tares${i}`);      
        $('.tr-border').eq(i-1).find('.group_tares_count').eq(e-1).val(tare_count);              
      }

      for (h=1; h <= goods_count; h++) {
        $('.tr-border').eq(i-1).find('.goods_item').eq(h-1).attr('name', `group_${i}good${h}`);
        $('.tr-border').eq(i-1).find('.goods_qty').eq(h-1).attr('name', `group_${i}goods_qty${h}`);

        $('.tr-border').eq(i-1).find('.group_goods_count').eq(h-1).attr('name', `group_goods${i}`);
        $('.tr-border').eq(i-1).find('.group_goods_count').eq(h-1).val(goods_count);
      }
    }

  }
}

function initTareSelector(e){
  const tare = $(e.target).find(":selected").val();

  if (tare == 1) {
    $(`#length`).val(0.8);
    $(`#width`).val(1.2);
  } else if (tare == 2) {
    $(`#length`).val(1.2);
    $(`#width`).val(1);
  } else if (tare == 3) {
    $(`#length`).val(1.2);
    $(`#width`).val(1.2);
  } else if (tare == 14) {
    $(`#length`).val(0.8);
    $(`#width`).val(0.6);
  } else {
    $(`#length`).val('');
    $(`#width`).val('');
  }
}

function initSelector(e){
  const itemcode = $(e.target).find(":selected").data('itemcode');
  const weight = $(e.target).find(":selected").data('weight');
  const qty = $(e.target).find(":selected").data('qty');
  let weightArr = new Array(); 

   $(e.target).parent().parent().find('.code').text((itemcode != null ? itemcode : 0));
   $(e.target).parent().parent().find('.weight').text((weight != null ? weight : 0));  
   $(e.target).parent().parent().find('.goodsQty').val((qty != null ? qty : 0));  

   $('.goodsSelector').each(function(i,element){     
    weightArr.push($(element).find(':selected').data('weight'));
   });

   if(weightArr.length > 0){
    let weightSum = weightArr.map(Number).reduce(function (a, b) { return a + b; });
    $('#weight').val(weightSum);
   }
}

function initQtyCheck(e){
  const radio = $('[name="qty_type"]:checked').val();
  let id = $(e.target).parent().parent().data('id');
  let qty = $(`#qty${id}`).val();
  let goodsQty = $(e.target).val();
    if(radio == 2){      
      if(!isNaturalNumber(goodsQty/qty)){
        $('.save_btn').attr('disabled',true);
        $('#alert').text('Prekių kiekis neišsidalina ant nurodyto pakuočių skaičiaus.');
      }else{
        $('.save_btn').attr('disabled',false);
        $('#alert').text('');
      }   
    }
   
    const qtyInStock = $(e.target).parent().parent().find('.goodsSelector :selected').data('qty');

    if(parseInt(qtyInStock) < parseInt(goodsQty)){
      $(e.target).parent().parent().find('.goods_alert').remove();
      $(e.target).parent().parent().find('.goodsSelector').parent().append(`<span class="goods_alert" style="color:red;">Likučių mažiau nei nurodytas prekių skaičius</span>`);
    }else{
      $(e.target).parent().parent().find('.goods_alert').html('');
    }


}

function isNaturalNumber(n) {
  n = n.toString(); // force the value incase it is not
  var n1 = Math.abs(n),
      n2 = parseInt(n, 10);
  return !isNaN(n1) && n2 === n1 && n1.toString() === n;
}

function deleteLine(){
  let count = $('.lineItem').length;  
  if(count > 1){     
    $('.lineItem').last().remove();   
    $('.addTare').attr('disabled',false);
    $('#goods_table').addClass('hide');
    $('.goodsItem').remove();  
  }
}

function deleteGood(e){
  $(e.target).parent().parent().remove();
  let count = $('.goodsItem').length;  
  let weightArr = new Array();

  for(i=1; i<= count;i++){
    $('.goodsSelector').eq(i-1).attr('id',`goods${i}`);
    $('.goodsQty').eq(i-1).attr('id',`goods_qty${i}`);
  }

  if(count == 0){
    $('.addTare').attr('disabled',false);
    $('#goods_table').addClass('hide');    
  }

  $('.goodsSelector').each(function(i,element){     
    weightArr.push($(element).find(':selected').data('weight'));
   });

   if(weightArr.length > 0){
    let weightSum = weightArr.map(Number).reduce(function (a, b) { return a + b; });
    $('#weight').val(weightSum);
   }

}

function deleteRow(){
  $('.tr-border').last().remove();
  if($('.tr-border').length == 0){
   $('.deleteRow').attr('disabled',true);
  }
}


function clearModal(){
  $('#weight').val('');
  $('#length').val('');
  $('#width').val('');
  $('#height').val('');

  $('.lineItem').each(function(index,element){
    if(index == 0){
      $('#Packing1').val(1);
      $('#Packing1').parent().parent().find('#length').val('0.8');
      $('#Packing1').parent().parent().find('#width').val('1.2');
      $('#qty1').val(1);
      $('#barcode1').val('');
    }else{
      $(element).remove();
    }
  });

  $('.goodsItem').each(function(index,element){
    $(element).remove();
  });
  $('#goods_table').addClass('hide');
  $('.addTare').attr('disabled',false);
}

function isNumber(evt) {  
  let charCode = (evt.which) ? evt.which : evt.keyCode;
  evt.target.value = evt.target.value.replace(/,/g, '.');


  if (charCode != 46 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  } else {
    //if dot sign entered more than once then don't allow to enter dot sign again. 46 is the code for dot sign
    let parts = evt.srcElement.value.split('.');

    if (parts.length > 1 && (charCode == 46 || charCode == 44)) {
      return false;
    }
    return true;
  }
}