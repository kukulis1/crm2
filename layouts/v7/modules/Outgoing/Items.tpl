<script src="layouts/v7/modules/Outgoing/resources/main.js"></script> 
{assign var=SHIPMENT_INFO value=$SHIPMENT['shipment']}
{assign var=GOODS value=$SHIPMENT['goods']}

  <div class="container"> 
      <input type="hidden" name="edit" value="1">
      <h6>Krovinio Nr. {$SHIPMENT_INFO['SHIPMENT_CODE']}</h6>
      <table id="listview-table" class="table listview-table" style="margin-top: 40px;margin-bottom: 20px;">             
        <tbody>          
          <tr class="listViewEntires">
            <td style="border:0;width: 160px;"><label>Prekių savininkas</label> </td>
          </tr>
          <tr class="listViewEntries">
            <td>Pavadinimas </td>
            <td>{$SHIPMENT_INFO['CUSTOMER_NAME']}</td>        
            <td>Šalis</td>
            <td>{$SHIPMENT_INFO['CONSIGNEE_COUNTRY']}</td>
          </tr>
          <tr class="listViewEntries">
            <td>Adresas </td>
            <td>{$SHIPMENT_INFO['CUSTOMER_ADDRESS']}</td>        
            <td>Miestas</td>
            <td>{$SHIPMENT_INFO['CUSTOMER_CITY']} {$SHIPMENT_INFO['CUSTOMER_POST_CODE']}</td>
          </tr>
          <tr class="listViewEntires">
            <td style="border:0;width: 160px;"><label>Siuntėjas</label> </td>
          </tr>
          <tr class="listViewEntries">
            <td>Pavadinimas </td>
            <td>{$SHIPMENT_INFO['SENDER_NAME']}</td>        
            <td>Šalis</td>
            <td>{$SHIPMENT_INFO['SENDER_COUNTRY']}</td>
          </tr>
          <tr class="listViewEntries">
            <td>Adresas </td>
            <td>{$SHIPMENT_INFO['SENDER_ADDRESS']}</td>        
            <td>Miestas</td>
            <td>{$SHIPMENT_INFO['SENDER_CITY']} {$SHIPMENT_INFO['SENDER_POST_CODE']}</td>
          </tr>
          <tr class="listViewEntries">
            <td>Asmuo </td>
            <td>{$SHIPMENT_INFO['SENDER_CONTACT']}</td>        
            <td>Telefonas</td>
            <td>{$SHIPMENT_INFO['SENDER_PHONE']}</td>
          </tr>

        <tr class="listViewEntires">
          <td style="border:0;width: 160px;"><label>Gavėjas</label> </td>
        </tr>
        <tr class="listViewEntries">
          <td>Pavadinimas </td>
          <td>{$SHIPMENT_INFO['CONSIGNEE_NAME']}</td>        
          <td>Šalis</td>
          <td>{$SHIPMENT_INFO['CONSIGNEE_COUNTRY']}</td>
        </tr>
        <tr class="listViewEntries">
          <td>Adresas </td>
          <td>{$SHIPMENT_INFO['CONSIGNEE_ADDRESS']}</td>        
          <td>Miestas</td>
          <td>{$SHIPMENT_INFO['CONSIGNEE_CITY']} {$SHIPMENT_INFO['CONSIGNEE_POST_CODE']}</td>
        </tr>
        <tr class="listViewEntries">
          <td>Asmuo </td>
          <td>{$SHIPMENT_INFO['CONSIGNEE_CONTACT']}</td>        
          <td>Telefonas</td>
          <td>{$SHIPMENT_INFO['CONSIGNEE_PHONE']}</td>
        </tr>

        <tr class="listViewEntires">
          <td style="border:0;width: 160px;"><label>Krovinys ir kiti duomenys</label> </td>
        </tr>
        <tr class="listViewEntries">
          <td>Sandėlis</td>
          <td>{$SHIPMENT_INFO['WAREHOUSE_NME']}</td>  
          <td></td> 
          <td></td> 
        </tr>
        <tr class="listViewEntries">
          <td>Krovinio kiekis</td>
          <td>{$SHIPMENT_INFO['PLAN_PKG']} {$SHIPMENT_INFO['TARE_TYPE_NAME']}</td>        
          <td>Svoris, kg</td>
          <td>{$SHIPMENT_INFO['PLAN_WGT']}</td>
        </tr>      
        </tbody>   
      </table>

      <label>Prekės informacija iš kortelės</label> 
      <table id="listview-table" class="table listview-table" style="margin-top: 10px;margin-bottom: 20px;">       
      <thead>
        <tr> 
          <th>Nr.</th>     
          <th>Pavadinimas</th>     
          <th>Kodas</th>     
          <th>Kiekis paletėje</th>     
          <th>Mat.vnt.</th>            
        </tr>      
      </thead>      
      <tbody>    
      {$count = 1}           
            {foreach item=GOOD from=$GOODS}              
            <tr class="listViewEntries">             
              <td>{$count}.</td>      
              <td>{$GOOD['ITEM_NME']}</td>      
              <td>{$GOOD['ITEM_CODE']}</td>
              <td>{$GOOD['I_IN_PLL_QTY']}</td>
              <td>{$GOOD['MEASURE']}</td>
            </tr>
            {assign var=count value=$count+1}
        {/foreach}
      </tbody>   
      </table

  </div>