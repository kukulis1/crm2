{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
  {assign var="productName" value="productName"|cat:$row_no}
  {assign var="productid" value="productid"|cat:$row_no}
  {assign var="hdnProductId" value="hdnProductId"|cat:$row_no}
  {assign var="comment" value="comment"|cat:$row_no}
  {assign var="qty" value="qty"|cat:$row_no}
  {assign var="purchase_date" value="purchase_date"|cat:$row_no}
  {assign var="costcenter" value="costcenter"|cat:$row_no}
  {assign var="object" value="object"|cat:$row_no}
  {assign var="employee" value="employee"|cat:$row_no}
  {assign var="listPrice" value="listprice"|cat:$row_no}
  {assign var="region" value="region"|cat:$row_no}
 

  <td style="text-align:center; width: 100px;">
    <i class="fa fa-trash deleteRow cursorPointer" title="{vtranslate('LBL_DELETE',$MODULE)}"></i>
    &nbsp;<a><img src="{vimage_path('drag.png')}" border="0" title="{vtranslate('LBL_DRAG',$MODULE)}" /></a>
    <input type="hidden" class="rowNumber" value="{$row_no}" />
  </td>

  <td style="width: 400px;">
    <div class="itemNameDiv form-inline">
      <div class="row">
        <div class="col-lg-10">
          <div class="input-group2 input-group" style="width:350px;">
          <input type="text" name="productName{$row_no}" value="{if $data.$productName}{$data.$productName}{/if}" class="productName form-control" autocomplete="off" aria-required="true" data-rule-required="true" placeholder="Rašykite, norėdami pasinaudoti paieška">
            <span class="input-group-addon cursorPointer clearLineItem" title="Išvalyti"><i class="fa fa-times-circle"></i></span>           
            <input type="hidden" name="hdnProductId{$row_no}" value="{if $data.$productid}{$data.$productid}{else}36641{/if}" class="selectedModuleId hdnProductId">
            <div class="col-lg-2"></div>
          </div>
        </div>
      </div>

      <div> <br>
        <textarea style="width: 340px;" name="{$comment}" class="lineItemCommentBox form-control comment">{decode_html($data.$comment)}</textarea>
      </div>
    </div>
    </div>
  </td>
	{* <td>
		<div class="input-group inputElement" style="min-width: 120px;width: 120px;">
            <input name="purchase_date{$row_no}" id="purchase_date{$row_no}" type="text" class="form-control purchase_date" data-fieldtype="date" data-date-format="yyyy-mm-dd" data-rule-date="true" value="{if $data.$purchase_date}{$data.$purchase_date}{/if}" autocomplete="off" aria-required="true"><span class="input-group-addon"><i class="fa fa-calendar "></i></span>
		</div>
	</td> *}
 <td style="width: 220px;">
			<select class="inputElement costcenter" data-fieldtype="picklist" type="picklist" name="costcenter{$row_no}" style="display: block;" required>
			{foreach  item=COST_LIST from=$COSTCENTERS}	
				<option value="{$COST_LIST['costid']}" {if $data.$costcenter eq $COST_LIST['costid']}selected{/if}>{$COST_LIST['costname']}</option>
			{/foreach}
			</select>
			<div style="display: block; margin-top: 5px;">
			<input type="text" name="object{$row_no}" placeholder="Objektas" class="inputElement object" autoComplete="off" value="{if !empty($data.$object)}{$data.$object}{/if}">
			</div>
			<div style="display: block; margin-top: 5px;">
			<input type="text" name="employee{$row_no}" placeholder="Darbuotojas" class="inputElement employee" autoComplete="off" value="{if !empty($data.$employee)}{$data.$employee}{/if}">		
			</div>	
 </td>
  <td>
    <input name="{$qty}" type="text" class="qty smallInputBox inputElement" data-num="{$row_no}" data-rule-required=true value="{if !empty($data.$qty)}{$data.$qty}{else}1{/if}" style="width:50px;"/>
  </td>
	<td><input name="fix-price{$row_no}" value="{$data.$listPrice}" class="smallInputBox inputElement fix-price" type="text"  style="width:70px;"></td>
	  <td>													
				<select class="inputElement region_id region" name="region{$row_no}" style="width: 120px;">
					<option data-proc="21" value="0">{vtranslate('LBL_SELECT_OPTION', $MODULE)}</option>
					{foreach item=TAX_REGION_INFO from=$TAX_REGIONS}
						<option {if $data.$region == $TAX_REGION_INFO['regionid']}selected{/if} data-proc="{$TAX_REGION_INFO['value']}" value="{$TAX_REGION_INFO['regionid']}">{$TAX_REGION_INFO['name']}</option>
					{/foreach}
				</select>
				<input type="hidden" value="{$RECORD->get('region_id')}" />							
				</td>
{/strip}