{strip}
  <script src="layouts/v7/lib/jquery/fullcalendar/lib/jquery-ui.min.js"></script>
  <script src="layouts/v7/modules/Vendors/resources/Orders.js"></script>


  {assign var="TEMPLATES" value=$GET_TEMPLATES}
  {$weekDays = ['Monday' => 'Pirmadienį', 'Tuesday' => 'Antradienį', 'Wednesday' => 'Trečiadienį', 'Thursday' => 'Ketvirtadienį', 'Friday' => 'Penktadienį', 'Saturday' => 'Šestadienį', 'Sunday' => 'Sekmadienį']}

  {if !$TEMPLATES}
    <div style="display: flex;justify-content: center;border: 1px solid #ddd;margin-bottom: 30px;">
      <h5>Šablonų nėra</h5>
    </div>
  {else}
    <table class="table detailview-table">
      <tr class="listViewContentHeader">
        <th style="width: 80px;">#</th>
        <th>Šablonas</th>
        <th>Saskaitos tipas</th>
        <th>Saskaitos data</th>        
        <th>Sukūrė</th>
        <th>Sukūrimo laikas</th>
      </tr>
      {foreach from=$TEMPLATES item=item key=key}
        <tr>
          <td class="listViewEntryValue" style="padding:8px 0 0;">
            <span class="actionImages">&nbsp;&nbsp;&nbsp;
              <a><i data-toggle="modal" data-record="{$item['record']}" data-target="#editOrderFormModal_{$item['record']}" title="Redaguoti" class="fa fa-pencil editBtn"></i></a> &nbsp;&nbsp;
              <a onclick="deleteRecord({$item['record']},event);"><i title="Pašalinti" class="fa fa-trash"></i></a>
            </span>
            <label class="switch" style="width: 30px;height: 15px;margin-top: 0px;margin-left: 10px;">
              <input data-record="{$item['record']}" type="checkbox" class="eachStandingOrdersRule"
                {if $item['enabled'] eq 1}checked{/if}>
              <span class="slider2 round"></span>
            </label>
          </td>
          <td class="listViewEntryValue">{$item['template_name']}</td>
          <td class="listViewEntryValue">{$item['invoice_type']}</td>
          <td class="listViewEntryValue">{$item['invoice_date']}</td>      
          <td class="listViewEntryValue">{$item['creator']}</td>
          <td class="listViewEntryValue">{$item['createdtime']}</td>
        </tr>
      {/foreach}
    </table>


    {foreach from=$TEMPLATES item=item key=key}
      <form id="templateForm{$item['record']}" method="POST">
        <div class="modal fade" id="editOrderFormModal_{$item['record']}" tabindex="-1" role="dialog"
          aria-labelledby="editOrderFormModalTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="width: 1100px;">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="editOrderFormModalTitle">Pirkimo užsakymo forma</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <input type="hidden" class="edit" name="edit" value="1">
                <input type="hidden" class="template_id" name="template_id" value="{$item['record']}">
                <table class="table detailview-table no-border">
								<tr>
                <td class="fieldLabel alignMiddle">Šablono pavadinimas</td>
                <td><input style="width:240px;" type="text" name="template_name" class="inputElement" value="{$item['template_name']}"></td>
                <td class="fieldLabel alignMiddle"><span class="redColor">*</span> Sąskaitos tipas</td>
                <td class="fieldValue">
                  <select class="inputElement" name="type" style="width:240px;">
                    <option value="Debetinė" {if $item['invoice_type'] eq 'Debetinė'}selected{/if}>Debetinė</option>
                    <option value="Kreditinė" {if $item['invoice_type'] eq 'Kreditinė'}selected{/if}>Kreditinė</option>
                    <option value="Išankstinė" {if $item['invoice_type'] eq 'Išankstinė'}selected{/if}>Išankstinė</option>
                    <option value="Užsakymas" {if $item['invoice_type'] eq 'Užsakymas'}selected{/if}>Užsakymas</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="fieldLabel alignMiddle">Sąskaitos diena&nbsp;&nbsp;</td>

                <td class="fieldValue">
                  <div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;border:0">
                    <select style="width:240px;" class="inputElement" name="invoice_date">
                      {for $day=1 to 31}
											<option value="{$day}" {if $item['day'] eq $day}selected{/if}>{$day}</option>
                      {/for}
                    </select>
                  </div>
                </td>

                <td class="fieldLabel alignMiddle">Pastabos&nbsp;&nbsp;</td>
                <td class="fieldValue">
                  <textarea rows="3" class="inputElement textAreaElement col-lg-12 " name="description">{$item['description']}</textarea>
                </td>
              </tr>
                </table>
                <div name='editContent' style="margin-top: 50px;">
                  <div class='fieldBlockContainer edit_popup'>
                    <table class="table table-bordered" id="lineItemTab">
                      <tr>
												<th></th>
												<th><span class="redColor">*</span>Prekė/Paslauga</th>
												{* <th>Data</th> *}
												<th>Kaštų centras </th>
												<th>Kiekis</th>
												<th>Kaina</th>
												<th>Mok. Regionas</th>
                      </tr>
                      <tr id="row0" class="hide lineItemCloneCopy" data-row-num="0">
                        {include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Vendors' row_no=0 data=[] IGNORE_UI_REGISTRATION=true}
                      </tr>
                      {foreach key=row_no item=data from=$item['loads']}
                        <tr id="row{$row_no}" data-row-num="{$row_no}" class="lineItemRow">
                          {include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Vendors' row_no=$row_no data=$data}
                        </tr>
                      {/foreach}
                      {if count($item['loads']) eq 0}
                        <tr id="row1" class="lineItemRow" data-row-num="1">
                          {include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Vendors' row_no=1 data=[] IGNORE_UI_REGISTRATION=false}
                        </tr>
                      {/if}
                    </table>
                  </div>
                </div>

                <div class="btn-toolbar">
                  <span class="btn-group">
                    <button type="button" class="btn btn-default addProduct" data-which="{$item['record']}" data-module-name="Products">
                      <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Pridėti paslaugą</strong>
                    </button>
                  </span>
                </div>
                <br>

                <div class="fieldBlockContainer">
								<table class="table table-bordered blockContainer lineItemTable" id="lineItemResult">

                <tr>
                  <td width="83%">
                    <div class="pull-right"><strong>{vtranslate('LBL_ITEMS_TOTAL',$MODULE)}</strong></div>
                  </td>
                  <td>
                    <div id="netTotal" class="pull-right netTotal">{if !empty($item.total)}{$item.total}{else}0{/if}</div>
											<input type="hidden" name="netTotal" value="{if !empty($item.total)}{$item.total}{else}0{/if}">
                  </td>
                </tr>                
                <tr valign="top">
                  <td width="83%">
                    <span class="pull-right">                     
                        <strong>{vtranslate('LBL_GRAND_TOTAL',$MODULE)}</strong>                     
                    </span>
                  </td>
                  <td>
											<span id="grandTotal" class="pull-right grandTotal">{if !empty($item.total_with_vat)}{$item.total_with_vat}{else}0{/if}</span>
											<input type="hidden" name="grandTotal" value="{if !empty($item.total_with_vat)}{$item.total_with_vat}{else}0{/if}">
                  </td>
                </tr>
              </table>
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden" class="totalProductCount" name="totalProductCount" value="{count($item['loads'])}">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
                <button type="button" class="btn btn-primary saveTemplateBtn"
                  onclick="editTemplate(this,{$item['record']});">Redaguoti</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    {/foreach}

  {/if}



  <form id="templateForm" method="POST">
    <div class="modal fade" id="orderFormModal" tabindex="-1" role="dialog" aria-labelledby="orderFormModalTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document" style="width: 1100px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="orderFormModalTitle">Pirkimo užsakymo forma</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" id="edit" name="edit" value="0">
            <input type="hidden" id="template_id" name="template_id" value="0">
            <table class="table detailview-table no-border">
              <tr>
                <td class="fieldLabel alignMiddle">Šablono pavadinimas</td>
                <td><input style="width:240px;" type="text" name="template_name" class="inputElement"></td>
                <td class="fieldLabel alignMiddle"><span class="redColor">*</span> Sąskaitos tipas</td>
                <td class="fieldValue">
                  <select class="inputElement" name="type" style="width:240px;">
                    <option value="Debetinė" {if $item['order_type'] eq 'Debetinė'}selected{/if}>Debetinė</option>
                    <option value="Kreditinė" {if $item['order_type'] eq 'Kreditinė'}selected{/if}>Kreditinė</option>
                    <option value="Išankstinė" {if $item['order_type'] eq 'Išankstinė'}selected{/if}>Išankstinė</option>
                    <option value="Užsakymas" {if $item['order_type'] eq 'Užsakymas'}selected{/if}>Užsakymas</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="fieldLabel alignMiddle">Sąskaitos diena&nbsp;&nbsp;</td>

                <td class="fieldValue">
                  <div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;border:0">
                    <select style="width:240px;" class="inputElement" name="invoice_date">
                      {for $day=1 to 31}
                        <option value="{$day}">{$day}</option>
                      {/for}
                    </select>
                  </div>
                </td>

                <td class="fieldLabel alignMiddle">Pastabos&nbsp;&nbsp;</td>
                <td class="fieldValue">
                  <textarea rows="3" class="inputElement textAreaElement col-lg-12 " name="description"></textarea>
                </td>
              </tr>
            </table>

            <div name='editContent' style="margin-top: 50px;">
              <div class='fieldBlockContainer new_popup'>
                <table class="table table-bordered" id="lineItemTab">
                  <tr>
                    <th></th>
                    <th><span class="redColor">*</span>Prekė/Paslauga</th>
                    {* <th>Data</th> *}
                    <th>Kaštų centras </th>
                    <th>Kiekis</th>
                    <th>Kaina</th>
                    <th>Mok. Regionas</th>
                  </tr>
                  <tr id="row0" class="hide lineItemCloneCopy" data-row-num="0">
                    {include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Vendors' row_no=0 data=[] IGNORE_UI_REGISTRATION=true}
                  </tr>
                  {foreach key=row_no item=data from=$RELATED_PRODUCTS}
                    <tr id="row{$row_no}" data-row-num="{$row_no}" class="lineItemRow"
                      {if $data["entityType$row_no"] eq 'Products'}data-quantity-in-stock={$data["qtyInStock$row_no"]}{/if}>
                      {include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Vendors' row_no=$row_no data=$data}
                    </tr>
                  {/foreach}
                  {if count($RELATED_PRODUCTS) eq 0}
                    <tr id="row1" class="lineItemRow" data-row-num="1">
                      {include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Vendors' row_no=1 data=[] IGNORE_UI_REGISTRATION=false}
                    </tr>
                  {/if}
                </table>
              </div>
            </div>

            <div class="btn-toolbar">
              <span class="btn-group">
                <button type="button" class="btn btn-default addProduct" data-which="" id="addProduct" data-module-name="Products">
                  <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Pridėti paslaugą</strong>
                </button>
              </span>
            </div>
            <br>

            <div class="fieldBlockContainer">
              <table class="table table-bordered blockContainer lineItemTable" id="lineItemResult">

                <tr>
                  <td width="83%">
                    <div class="pull-right"><strong>{vtranslate('LBL_ITEMS_TOTAL',$MODULE)}</strong></div>
                  </td>
                  <td>
                    <div id="netTotal" class="pull-right netTotal">0</div>
											<input type="hidden" name="netTotal" value="0">
                  </td>
                </tr>                
                <tr valign="top">
                  <td width="83%">
                    <span class="pull-right">                     
                        <strong>{vtranslate('LBL_GRAND_TOTAL',$MODULE)}</strong>                     
                    </span>
                  </td>
                  <td>
											<span id="grandTotal" class="pull-right grandTotal">{if !empty($FINAL.grandTotal)}{$FINAL.grandTotal}{else}0{/if}</span>
											<input type="hidden" name="grandTotal" value="{if !empty($FINAL.grandTotal)}{$FINAL.grandTotal}{else}0{/if}">
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" id="totalProductCount" name="totalProductCount">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
            <button type="button" class="btn btn-primary saveTemplateBtn">Sukurti</button>
          </div>
        </div>
      </div>
    </div>
  </form>

{/strip}