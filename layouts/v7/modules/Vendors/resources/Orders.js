$(document).ready(function () {



  $(function () {
    $("#purchase_date1").datepicker({
      dateFormat: "yy-mm-dd",
      autoclose: true
    });
  });

  $('.clearLineItem').on('click',function(){
    $(this).parent().find('.productName').val('');
  });


  $('.deleteRow').on('click', function (e) {
    let body = $(this).parent().parent().parent();
    $(this).parent().parent().remove();
    let tr = body.find('.lineItemRow');

    tr.each(function (i) {
      $(this).attr('id', `row${i + 1}`);
      $(this).attr("data-row-num", i + 1);
      $(this).find('.productName').attr('id', `productName${i + 1}`).attr('name', `productName${i + 1}`);
      $(this).find('.comment').attr('id', `comment${i + 1}`).attr('name', `comment${i + 1}`);
      $(this).find('.hdnProductId').attr('id', `hdnProductId${i + 1}`).attr('name', `hdnProductId${i + 1}`);
      $(this).find('.lineItemType').attr('id', `lineItemType${i + 1}`).attr('name', `lineItemType${i + 1}`);
      $(this).find('.purchase_date').attr('id', `purchase_date${i + 1}`).attr('name', `purchase_date${i + 1}`);
      $(this).find('.costcenter').attr('id', `costcenter${i + 1}`).attr('name', `costcenter${i + 1}`);
      $(this).find('.object').attr('id', `object${i + 1}`).attr('name', `object${i + 1}`);
      $(this).find('.employee').attr('id', `employee${i + 1}`).attr('name', `employee${i + 1}`);
      $(this).find('.qty').attr('id', `qty${i + 1}`).attr('name', `qty${i + 1}`);
      $(this).find('.fix-price').attr('id', `fix-price${i + 1}`).attr('name', `fix-price${i + 1}`);
      $(this).find('.region').attr('id', `region${i + 1}`).attr('name', `region${i + 1}`);
      $(this).find('.qty').attr('id', `qty${i + 1}`).attr('name', `qty${i + 1}`);
    });
  });

  $('.saveTemplateBtn').on('click', function () {
    let count = $(this).parent().parent().find('.lineItemRow').length;
    $(this).parent().find('#totalProductCount').val(count);
    if (templateValidate()) {
      document.getElementById('templateForm').submit();
    }
  });

  $('.addProduct').on('click', function () {
    let which = $(this).data('which');
    let num = $(this).parent().parent().parent().find('.lineItemRow').length + 1;
    let appendTo = $(this).parent().parent().parent().find('#lineItemTab tbody');
    let copyElement = $(this).parent().parent().parent().find('.lineItemCloneCopy').clone(true, true);
    copyElement.removeClass('hide lineItemCloneCopy');
    copyElement.addClass('lineItemRow');
    copyElement.attr('id', `row${num}`);
    copyElement.attr("data-row-num", num);
    copyElement.find('[name="productName0"]').attr('name', `productName${num}`);
    copyElement.find('[name="comment0"]').attr('name', `comment${num}`);
    copyElement.find('[name="hdnProductId0"]').attr('name', `hdnProductId${num}`);
    // copyElement.find('#purchase_date0"]').attr('id', `purchase_date${num}`).attr('name', `purchase_date${num}`);
    copyElement.find('[name="costcenter0"]').attr('name', `costcenter${num}`);
    copyElement.find('[name="object0"]').attr('name', `object${num}`);
    copyElement.find('[name="employee0"]').attr('name', `employee${num}`);
    copyElement.find('[name="qty0"]').attr('name', `qty${num}`);
    copyElement.find('[name="fix-price0"]').attr('name', `fix-price${num}`);
    copyElement.find('[name="region0"]').attr('name', `region${num}`);
    copyElement.appendTo(appendTo);

    loadPurchaseAjaxLists(which);
    initPriceChangeEvent(which);
    $("#purchase_date"+num).datepicker({
      dateFormat: "yy-mm-dd",
      autoclose: true
    });
  });

  $('.addTemplate').on('click',function(){
    setTimeout(() => {
      loadPurchaseAjaxLists('');
      initPriceChangeEvent('');
    }, 500);
  });

  $('.editBtn').on('click', function () {
    setTimeout(() => {
     loadPurchaseAjaxLists($(this).data('record'));
     initPriceChangeEvent($(this).data('record'));
    },500);
  });

  $('.qty').on('keyup', function () {
    let val = $(this).val();
    let id = $(this).data('num');
    $(`#qty${id}`).val(val);
  });

  $('#purchaseOrderRule').on('click', function () {
    let status;
    if ($(this).is(":checked")) {
      status = 1;
    } else {
      status = 0;
    }

    let vendorid = $('[name="record_id"]').val();
    $.ajax({
      type: "POST",
      url: "modules/Vendors/ajax/enable.php",
      data: { vendorid: vendorid, status: status },
      success: function (response) {
        console.log(response);
      }
    });


  });

  $('.eachStandingOrdersRule').on('click', function () {
    let status;
    if ($(this).is(":checked")) {
      status = 1;
    } else {
      status = 0;
    }

    let record = $(this).data('record');
    $.ajax({
      type: "POST",
      url: "modules/Vendors/ajax/eachEnable.php",
      data: { record: record, status: status },
      success: function (response) {
        console.log(response);
      }
    });
  });


  

});


function initPriceChangeEvent(which){
  // $(`.${which} .lineItemRow .fix-price`).unbind().keyup(function (e) {  
  //   calculePrices($(this),which);
  // });

  $(`#templateForm${which} .lineItemRow .fix-price`).unbind().change(function (e) {
    if($(this).val() == '') $(this).val(0);
    calculePrices($(this),which);
  });

  $(`#templateForm${which} .lineItemRow .qty`).unbind().on('change', function(){
    if($(this).val() == '') $(this).val(1);
    calculePrices($(this),which);
  });

  $(`#templateForm${which} .lineItemRow .region`).unbind().on('change', function(){
    calculePrices($(this),which);
  });
}

function calculePrices(thisInstance,which){
  let tr = thisInstance.parent().parent().parent().find('.lineItemRow');

  let priceWithVatArray = new Array();
  let priceArray = new Array();

  tr.each(function (i) {
    let no = i+1;
    let qty = parseFloat($(`#templateForm${which} [name="qty${no}"]`).val());
    let price = parseFloat($(`#templateForm${which} [name="fix-price${no}"]`).val());
    let vat = parseFloat($(`#templateForm${which} [name="region${no}"]`).find(':selected').data('proc'));
    let final = (price * qty) + ((price * qty) * vat / 100);
    let final2 = (price * qty);
    priceWithVatArray.push(final);
    priceArray.push(final2);
  });

  let netTotal = priceArray.reduce((a, b) => a + b, 0);
  let grandTotal = priceWithVatArray.reduce((a, b) => a + b, 0);

  $(`#templateForm${which}`).find(`#netTotal`).html(parseFloat(netTotal).toFixed(2));
  $(`#templateForm${which}`).find(`#grandTotal`).html(parseFloat(grandTotal).toFixed(2));   
  $(`#templateForm${which}`).find(`[name="netTotal"]`).val(parseFloat(netTotal).toFixed(2));
  $(`#templateForm${which}`).find(`[name="grandTotal"]`).val(parseFloat(grandTotal).toFixed(2)); 
}


function editTemplate(event, id) {
  let count = $(event).parent().parent().find('.lineItemRow').length;
  $(event).parent().find('#totalProductCount').val(count);
  if (eachTemplateValidate(id)) {
    document.getElementById(`templateForm${id}`).submit();
  }
}

function deleteRecord(id, event) {
  if (confirm('Ar tikrai norite ištrinti šabloną?')) {
    $.ajax({
      type: "POST",
      url: "modules/Vendors/ajax/deleteTemplate.php",
      data: { id: id },
      success: function (response) {
        if (response == 'success') {
          $(event.target).parent().parent().parent().parent().remove();
        } else {
          console.log(response);
        }
      }
    });
  }
}

function loadPurchaseAjaxLists(num) {
  $.ajax({
    type: "POST",
    url: "purchase/items.php",
    data: { id: 1 },
    dataType: 'JSON',
    success: function (history) {
      rows = document.querySelectorAll(`#templateForm${num} #lineItemTab .lineItemRow`);
      for (i = 1, len = rows.length; i <= len; i++) {
        autocomplete(document.querySelector(`#templateForm${num} [name="productName${i}"]`), history, 'history');
      }
    }
  });

  $.ajax({
    type: "POST",
    url: "purchase/users_list.php",
    data: { id: 1 },
    dataType: 'JSON',
    success: function (user_list) {
      rows = document.querySelectorAll(`#templateForm${num} #lineItemTab .lineItemRow`);
      for (i = 1, len = rows.length; i <= len; i++) {
        autocomplete(document.querySelector(`#templateForm${num} [name="employee${i}"]`), user_list, 'users');
      }
    }
  });

  $.ajax({
    type: "POST",
    url: "purchase/object.php",
    data: { id: 1 },
    dataType: 'JSON',
    success: function (object) {
      rows = document.querySelectorAll(`#templateForm${num} #lineItemTab .lineItemRow`);
      for (i = 1, len = rows.length; i <= len; i++) {
        autocomplete(document.querySelector(`#templateForm${num} [name="object${i}"]`), object, 'object');
      }
    }
  });
}


function autocomplete(inp, arr, place) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function (e) {
    var a, b, i, val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    if (place == 'history') {
      a.setAttribute("class", "autocomplete-items");
    } else {
      a.setAttribute("class", "autocomplete-items-employee");
    }
    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < arr.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (((arr[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
        // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        /*create a DIV element for each matching element:*/
        b = document.createElement("DIV");
        b.setAttribute('class', 'listas');
        /*make the matching letters bold:*/
        b.innerHTML = arr[i].substr(0, val.length);
        b.innerHTML += arr[i].substr(val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener("click", function (e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.getElementsByTagName("input")[0].value;
          /*close the list of autocompleted values,
          (or any other open lists of autocompleted values:*/
          closeAllLists();
        });
        a.appendChild(b);
        let listas = document.querySelectorAll('.listas');
        for (let i = 0; i < listas.length; i++) {
          listas[i].onclick = () => {
            // inp.readOnly = true;
            inp.classList.add('selected');
          }
        }

      }
      inp.onchange = (e) => {
        if (!inp.classList.contains('selected')) {
          if (inp.placeholder != 'Darbuotojas' && inp.placeholder != 'Objektas') {
            inp.value = '';
          }
          $(e.target.parentElement).find('.add_new_entity').addClass('alert-danger');
        } else {
          $(e.target.parentElement).find('.add_new_entity').removeClass('alert-danger');
        }
      }
    }
  });




  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
      /*If the arrow DOWN key is pressed,
      increase the currentFocus variable:*/
      currentFocus++;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 38) { //up
      /*If the arrow UP key is pressed,
      decrease the currentFocus variable:*/
      currentFocus--;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    if (place == 'history') {
      var x = document.getElementsByClassName("autocomplete-items");
    } else {
      var x = document.getElementsByClassName("autocomplete-items-employee");
    }
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);

  });
}


function eachTemplateValidate(id) {
  let errors = new Array();
  let n = 1;
  $(`#templateForm${id} [data-require="true"]`).each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(n);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    n++;
  });

  let m = 1;
  $(`#templateForm${id} .lineItemRow [data-rule-required=true]`).each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(m);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    m++;
  });

  if (errors.length == 0) {
    return true;
  } else {
    return false;
  }
}

function templateValidate() {
  let errors = new Array();
  let n = 1;
  $('#templateForm [data-require="true"]').each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(n);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    n++;
  });

  let m = 1;
  $('#templateForm .lineItemRow [data-rule-required=true]').each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(m);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    m++;
  });

  if (errors.length == 0) {
    return true;
  } else {
    return false;
  }
}



