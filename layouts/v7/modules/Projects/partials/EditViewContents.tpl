{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
********************************************************************************/
-->*}
{* <script type="text/javascript" src="{vresource_url('layouts/v7/modules/'|cat:{$MODULE}|cat:'/resources/calculate.js')}"></script> *}
{strip}
	{if !empty($PICKIST_DEPENDENCY_DATASOURCE)}
		<input type="hidden" name="picklistDependency" value='{Vtiger_Util_Helper::toSafeHTML($PICKIST_DEPENDENCY_DATASOURCE)}' />
	{/if}

	<div name='editContent'>
		{if $DUPLICATE_RECORDS}
			<div class="fieldBlockContainer duplicationMessageContainer">
				<div class="duplicationMessageHeader"><b>{vtranslate('LBL_DUPLICATES_DETECTED', $MODULE)}</b></div>
				<div>{getDuplicatesPreventionMessage($MODULE, $DUPLICATE_RECORDS)}</div>
			</div>
		{/if}
			<input type="hidden" id="totalProductCount" name="totalProductCount">
		{foreach key=BLOCK_LABEL item=BLOCK_FIELDS from=$RECORD_STRUCTURE name=blockIterator}	
			{foreach key=FIELD_NAME item=FIELD_MODEL from=$BLOCK_FIELDS name=blockfields}
							{if $FIELD_NAME eq 'projects_tks_workinghours'}			
									{assign var=WORKING_HOURS value=$FIELD_MODEL->get('fieldvalue')}								
							{/if}
								{if $FIELD_NAME eq 'projects_tks_amountofpeople'}									
									{assign var=AMOUNT_OF_PEOPLE value=$FIELD_MODEL->get('fieldvalue')}					
							{/if}
			{/foreach}				

			{if $BLOCK_FIELDS|@count gt 0}
				<div class='fieldBlockContainer' data-block="{$BLOCK_LABEL}">
					<h4 class='fieldBlockHeader'>{vtranslate($BLOCK_LABEL, $MODULE)}</h4>
					<hr>
					<table class="table table-borderless">
						<tr>
							{assign var=COUNTER value=0}
							{foreach key=FIELD_NAME item=FIELD_MODEL from=$BLOCK_FIELDS name=blockfields}

							{if $FIELD_NAME neq 'projects_tks_workinghours' && $FIELD_NAME neq 'projects_tks_amountofpeople'}
							
								{assign var="isReferenceField" value=$FIELD_MODEL->getFieldDataType()}
                                                                {assign var=FIELD_INFO value=$FIELD_MODEL->getFieldInfo()}
								{assign var="refrenceList" value=$FIELD_MODEL->getReferenceList()}
								{assign var="refrenceListCount" value=count($refrenceList)}
								{if $FIELD_MODEL->isEditable() eq true}
									{if $FIELD_MODEL->get('uitype') eq "19"}
										{if $COUNTER eq '1'}
											<td></td><td></td></tr><tr>
											{assign var=COUNTER value=0}
										{/if}
									{/if}
									{if $COUNTER eq 2}
									</tr><tr>
										{assign var=COUNTER value=1}
									{else}
										{assign var=COUNTER value=$COUNTER+1}
									{/if}
									<td class="fieldLabel alignMiddle">
										{if $MASS_EDITION_MODE}
											<input class="inputElement" id="include_in_mass_edit_{$FIELD_MODEL->getFieldName()}" data-update-field="{$FIELD_MODEL->getFieldName()}" type="checkbox">&nbsp;
										{/if}
										{if $isReferenceField eq "reference"}
											{if $refrenceListCount > 1}
												{assign var="DISPLAYID" value=$FIELD_MODEL->get('fieldvalue')}
												{assign var="REFERENCED_MODULE_STRUCTURE" value=$FIELD_MODEL->getUITypeModel()->getReferenceModule($DISPLAYID)}
												{if !empty($REFERENCED_MODULE_STRUCTURE)}
													{assign var="REFERENCED_MODULE_NAME" value=$REFERENCED_MODULE_STRUCTURE->get('name')}
												{/if}
												<select style="width: 140px;" class="select2 referenceModulesList">
													{foreach key=index item=value from=$refrenceList}
														<option value="{$value}" {if $value eq $REFERENCED_MODULE_NAME} selected {/if}>{vtranslate($value, $value)}</option>
													{/foreach}
												</select>
											{else}
												{vtranslate($FIELD_MODEL->get('label'), $MODULE)}
											{/if}
										{else if $FIELD_MODEL->get('uitype') eq "83"}
											{include file=vtemplate_path($FIELD_MODEL->getUITypeModel()->getTemplateName(),$MODULE) COUNTER=$COUNTER MODULE=$MODULE}
											{if $TAXCLASS_DETAILS}
												{assign 'taxCount' count($TAXCLASS_DETAILS)%2}
												{if $taxCount eq 0}
													{if $COUNTER eq 2}
														{assign var=COUNTER value=1}
													{else}
														{assign var=COUNTER value=2}
													{/if}
												{/if}
											{/if}
										{else}	
													{vtranslate($FIELD_MODEL->get('label'), $MODULE)}{if $FIELD_NAME eq 'projects_tks_workingdays'} / Valandų / Žmonių kiekis{/if}										{/if}
										&nbsp;{if $FIELD_MODEL->isMandatory() eq true} <span class="redColor">*</span> {/if}
									</td>
									{if $FIELD_MODEL->get('uitype') neq '83'}		
													
										<td class="fieldValue">
										{include file=vtemplate_path($FIELD_MODEL->getUITypeModel()->getTemplateName(),$MODULE)}
											{if $FIELD_NAME eq 'projects_tks_workingdays'}
												<input id="Projects_editView_fieldName_projects_tks_workinghours" type="text" data-fieldname="projects_tks_workinghours" data-fieldtype="string" class="inputElement " name="projects_tks_workinghours" value="{$WORKING_HOURS}" style="width: 70px; margin-right: 3px;">
												<input id="Projects_editView_fieldName_projects_tks_amountofpeople" type="text" data-fieldname="projects_tks_amountofpeople" data-fieldtype="string" class="inputElement " name="projects_tks_amountofpeople" value="{$AMOUNT_OF_PEOPLE}" style="width: 70px;">									
											{/if}
										</td>
									{/if}
								{/if}
								{/if}
							{/foreach}
							{*If their are odd number of fields in edit then border top is missing so adding the check*}
							{if $COUNTER is odd}
								<td></td>
								<td></td>
							{/if}
						</tr>
					</table>
				</div>
			{/if}			
		{/foreach}
	</div>

	{assign var="RELATED_PRODUCTS" value=$RELATED_PRODUCTS}
	{assign var="MEASURE" value=$MEASURE}
	{assign var="FINAL" value=$PRICES}

		<div name='editContent'>
		<div class='fieldBlockContainer'>
			<table class="table table-bordered" id="lineItemTab">	
				<tr>
					<th>Įrankiai</th>
					<th>
						<strong>
							<span style="margin-right: 20px;"></span>SVORIS
							<span style="margin-right: 10px;"></span> ILGIS 
							<span style="margin-right: 10px;"></span>PLOTIS
							<span style="margin-right: 10px;"></span>AUKŠTIS
						</strong>
					</th>
					<th>Tara</th>
					<th>Kiekis</th>	
					</tr>								
					<tr id="row0" class="hide lineItemCloneCopy" data-row-num="0">
						{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Leads' row_no=0 data=[] IGNORE_UI_REGISTRATION=true}
					</tr>					
					{foreach key=row_no item=data from=$RELATED_PRODUCTS}
						<tr id="row{$row_no}" data-row-num="{$row_no}" class="lineItemRow" {if $data["entityType$row_no"] eq 'Products'}data-quantity-in-stock={$data["qtyInStock$row_no"]}{/if}>
							{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Leads' row_no=$row_no data=$data}
						</tr>						
					{/foreach}
					{if count($RELATED_PRODUCTS) eq 0}
						<tr id="row1" class="lineItemRow" data-row-num="1">					
							{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Leads' row_no=1 data=[] IGNORE_UI_REGISTRATION=false}
						</tr>									
					{/if}		
				</table>
		</div>
	</div>
		<br>
		<div>
					<div class="btn-toolbar">     					                                   
						<span class="btn-group">
							<button type="button" class="btn btn-default" id="addProduct" data-module-name="Products" {if $MODULE eq 'Invoice'}disabled{/if}>
								<i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Pridėti krovinį</strong>
							</button>
						</span>				
						<span class="btn-group">
							<button type="button" class="btn btn-default" id="addService" data-module-name="Services" >
								<i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>{vtranslate('LBL_ADD_SERVICE',$MODULE)}</strong>
							</button>
						</span>                                    
					</div>	
					<br>

	<div class="fieldBlockContainer">
			<table class="table table-bordered blockContainer lineItemTable" id="lineItemResult">				
				<tr>			
					<td>
						<div class="pull-right">
							<strong>{vtranslate('LBL_AGREED_PRICE',$MODULE)}&nbsp;&nbsp;</strong>
						</div>	
					</td>
					<td>
						<span class="pull-right">
							<input type="text" id="agreedprice" name="agreedprice" class="form-control lineItemInputBox" value="{if $FINAL.agreed_price}{$FINAL.agreed_price}{else}0{/if}">
						</span>
					</td>						
				</tr>
				<tr valign="top">
					<td width="83%">					
						<span class="pull-right"><strong>{vtranslate('LBL_GRAND_TOTAL',$MODULE)}</strong></span>			
					</td>			
				
					<td>
						<span id="grandTotal2" name="grandTotal" class="pull-right grandTotal">{if $FINAL.total}{$FINAL.total}{else}0{/if}</span>
						<input type="hidden" name="total" value="{if $FINAL.total}{$FINAL.total}{else}0{/if}">
					</td>
				</tr>

			</table>
		</div> 
{/strip}
