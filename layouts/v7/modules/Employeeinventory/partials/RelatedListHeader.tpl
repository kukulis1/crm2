{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}

<div id="wait" style="display:none;position:absolute;top: 32%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>

<div class="modal fade" id="individualDocumentsModal" tabindex="-1" role="dialog" aria-labelledby="individualDocumentsModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Sukurti inventorių</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<form action="" method="POST" id="inventory_popup">
					<input type="hidden" name="save_item" value="1">
					<div class="form-group">
						<label for="exampleInputEmail1">Daiktas</label>
						<input type="text" name="item" class="inputElement" required>			
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Kodas</label>
						<input type="text" name="code" class="inputElement" required>			
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Vertė</label>
						<input type="text" name="value" class="inputElement">			
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Tiekėjas</label>
						<input type="text" name="vendor" class="inputElement">			
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Mato Vnt. </label>
						<input type="text" name="unit" class="inputElement">			
					</div>			
				
				</form>
			</div>
      <div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="document.getElementById('inventory_popup').submit();">Saugoti</button>			
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

	<div class="relatedHeader">
		<div class="btn-toolbar row">
			<div class="col-lg-6 col-md-6 col-sm-6 btn-toolbar">
				<div class="col-sm-3">					
						<button type="button" class="btn btn-default"  data-toggle="modal" data-target="#individualDocumentsModal">
							<span class="fa fa-plus" title="Pridėti individualiai"></span>&nbsp;&nbsp;Pridėti individualiai&nbsp;</span>
						</button>			
				</div>

			</div>
		</div>
	</div>

{/strip}