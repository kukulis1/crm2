{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

<style>
 h5 {
		font-size: 13px;
		font-weight: bolder;
		padding: 0 7px;
	}
.users a {
		font-size: 13px !important;
	}

.users small {
	font-size: 13px !important;
}
</style>

<div class="modal fade" id="ordersModalByUser" tabindex="-1" role="dialog" aria-labelledby="ordersModalByUserLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Užsakymai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
					<thead>
					<th>Siuntos Nr.</th>
					<th>Suma</th>
					<th>Klientas</th>
					<th>Sutarta kaina</th>
					<th>Sutartos kainos aprašymas</th>
					<th>Sukūrimo laikas</th>
					</thead>
					<tbody id="ordersTbody2"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
        {* <button type="button" class="btn btn-primary">Save changes</button> *}
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ordersModalCorrectionsByUser" tabindex="-1" role="dialog" aria-labelledby="ordersModalCorrectionsByUserLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Užsakymai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
					<thead>
					<th>Siuntos Nr.</th>
					<th style="width: 250px;">Kainynas</th>
					<th>Kainyno kaina</th>
					<th>Galutinė suma</th>
					<th>Klientas</th>
					<th>Sutarta kaina</th>
					<th>Sutartos kainos aprašymas</th>
					<th>Sukūrimo laikas</th>
					</thead>
					<tbody id="ordersTbody3"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
        {* <button type="button" class="btn btn-primary">Save changes</button> *}
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ordersInfoModalByUser" tabindex="-1" role="dialog" aria-labelledby="ordersInfoModalByUserLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Užsakymai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div id="wait" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1050;"><img src="/resources/loading.gif"></div>
        <table class="table info_table">
					<thead>
					<th>Siuntos Nr.</th>
					<th>Dabartinė suma</th>
					<th>Buvusi suma</th>
					<th>Kainynas</th>					
					<th>Klientas</th>
					<th>Sutarta kaina</th>
					<th>Sutartos kainos aprašymas</th>
					<th>Sukūrimo laikas</th>
					</thead>
					<tbody id="ordersInfoTbody2"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
        {* <button type="button" class="btn btn-primary">Save changes</button> *}
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="invoicesModalByUser" tabindex="-1" role="dialog" aria-labelledby="invoicesModalByUserLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Sąskaitos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
					<thead>
					<th>Sąskaitos Nr.</th>
					<th>Klientas</th>
					<th>Suma</th>				
					<th>Sąskaitos data</th>				
					<th>Sukūrimo laikas</th>
					</thead>
					<tbody id="invoicesTbody2"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>


	<div style="padding-left: 15px;">Ataskaita: {$REPORTS['filter']}</div>
	<h5>Užsakymų vedimas</h5>	
		<div class="row entry clearfix users" style="padding: 4px 7px;">
    	<div class="col-lg-4 pull-left" style="font-size: 14px;">Darbuotojas</div>   
    	<div class="col-lg-2 pull-left" style="font-size: 14px;">Suma</div>   
    	<div class="col-lg-3 pull-left" style="font-size: 14px;">Taisyta</div>   
    	<div class="col-lg-3 pull-right muted" style="font-size: 14px;">Užsakymų</div>
		</div>
{if $REPORTS['sales'] neq false}
		{foreach key=$index item=REPORT from=$REPORTS['sales']} 
		 <div class="row entry clearfix users" style="padding: 0px 9px">
    	<div class="col-lg-4 pull-left"><a target="_blank" href="index.php?module=Users&parent=Settings&view=Detail&record={$REPORT['id']}&parentblock=LBL_USER_MANAGEMENT">{vtranslate($REPORT['employee'], $MODULE_NAME)}</a></div>   
			<div class="col-lg-2 pull-left muted">{$REPORT['total']}</div>
			<div class="col-lg-3 pull-left muted">
		{if $REPORTS['managers_corr'][$REPORT['employee']]['corr']}
			<a type="button" data-toggle="modal" data-target="#ordersInfoModalByUser" data-salesorderid="{$REPORTS['managers_corr'][$REPORT['employee']]['salesorderid']}" onclick="showSalesOrdersInfoByUser(event);">{$REPORTS['managers_corr'][$REPORT['employee']]['corr']}</a>
		{else}
			0
		{/if}
		</div>		
    	<div class="col-lg-3 pull-right muted"><a type="button" data-toggle="modal" data-target="#ordersModalByUser" data-salesorderid="{$REPORT['salesorderid']}" onclick="showSalesOrdersByUser(event);">{$REPORT['number']}</a></div>
		 </div>		 
    {/foreach}
{else}
		<span class="noDataMsg" style="margin-top: 20px; padding-left: 10px;">		
				{vtranslate('LBL_NO_SALESORDERS', $MODULE_NAME)}	
{/if}		

			<h5 style="margin-top: 20px;">Pirkimo saskaitų vedimas</h5>	
			<div class="row entry clearfix users" style="padding: 4px 7px;">
				<div class="col-lg-8 pull-left" style="font-size: 14px;">Darbuotojas</div>   
				<div class="col-lg-4 pull-right muted" style="font-size: 14px;">Saskaitos</div>
 			</div>
	{if $REPORTS['purchase'] neq false}
		{foreach key=$index item=REPORT from=$REPORTS['purchase']} 
		<div class="row entry clearfix users" style="padding: 0px 9px">
    	<div class="col-lg-8 pull-left"><a target="_blank" href="index.php?module=Users&parent=Settings&view=Detail&record={$REPORT['id']}&parentblock=LBL_USER_MANAGEMENT">{vtranslate($REPORT['employee'], $MODULE_NAME)}</a></div>   
    	<div class="col-lg-4 pull-right muted"><small>
					<a type="button" data-toggle="modal" data-target="#invoicesModalByUser"	data-purchaseorderid="{$REPORT['purchaseorderid']}" onclick="showPurchaseOrderByUser(event);">{$REPORT['number']}</a>
				</small></div>
		 </div>
    {/foreach}

{else}
		<span class="noDataMsg" style="margin-top: 20px; padding-left: 10px;">		
				{vtranslate('LBL_NO_PURCHASEORDERS', $MODULE_NAME)}			
		</span>
{/if}


	<h5 style="margin-top: 20px;">Pardavimo saskaitų vedimas</h5>	
			<div class="row entry clearfix users" style="padding: 4px 7px;">
				<div class="col-lg-6 pull-left" style="font-size: 14px;">Darbuotojas</div>  
				<div class="col-lg-3 pull-left muted" style="font-size: 14px;" title="Patikrinta kainų">Pakeista kainų</div> 
				<div class="col-lg-3 pull-right muted" style="font-size: 14px;">Saskaitos</div>			
 			</div>
{if $REPORTS['invoice'] neq false}
		{foreach key=$index item=REPORT from=$REPORTS['invoice']['users']} 
		 <div class="row entry clearfix users" style="padding: 0px 9px">
    	<div class="col-lg-6 pull-left"><a target="_blank" href="index.php?module=Users&parent=Settings&view=Detail&record={$REPORT['id']}&parentblock=LBL_USER_MANAGEMENT">{$REPORT['name']}</a></div>   
			<div class="col-lg-3 pull-left muted corrections">
				<small>
					<a type="button" data-toggle="modal" data-target="#ordersModalCorrectionsByUser"	data-salesorderid="{$REPORTS['invoice'][$REPORT['name']]['corr']['salesorderid']}" onclick="showSalesOrdersCorrectionsByUser(event);">
						{if $REPORTS['invoice'][$REPORT['name']]['corr']['corr']}{$REPORTS['invoice'][$REPORT['name']]['corr']['corr']}{else}0{/if}
					</a>				
				</small>
			</div>
    	<div class="col-lg-3 pull-right muted invoices">
				<small>
					{if $REPORTS['invoice'][$REPORT['name']]['num']['num']}
						<a type="button" data-toggle="modal" data-target="#invoicesModalByUser"	data-invoiceid="{$REPORTS['invoice'][$REPORT['name']]['num']['invoiceid']}" onclick="showInvoicesByUser(event);">{$REPORTS['invoice'][$REPORT['name']]['num']['num']}</a>
					{else}0{/if}
				</small>
			</div>    	
		 </div>
    {/foreach}

{else}
		<span class="noDataMsg" style="margin-top: 20px; padding-left: 10px;">		
				{vtranslate('LBL_NO_INVOICES', $MODULE_NAME)}			
		</span>
{/if}
