$(document).ready(function () {

  let url = new URLSearchParams(window.location.search);

  if (url.get('record') == null){   
      $(`#cargo_length1`).val(0.8);
      $(`#cargo_width1`).val(1.2);
      $(`#cargo_height1`).val(1);
  }


  if (url.get('view') == 'Edit') {
    setTimeout(() => {
      $('#lineItemTab').sortable('destroy');
    }, 1000);

  }

  if (url.get('view') == 'Edit') {
    const totalHidden = document.querySelector('#grandTotal').innerHTML;

    $('span.clearLineItem').attr('id', 'clearfield-1');
    $('span.clearLineItem').removeClass('clearLineItem');

    const blockPrice = document.querySelectorAll("[name='editContent']")[0];

    if (totalHidden !== '') {
      setTimeout(function () {
        $('#grandTotal').html(totalHidden);
      }, 200);
    }
    

    const divas = document.createElement("div");
    divas.setAttribute('id', 'wait');
    divas.setAttribute('style', 'display:none;position:absolute;top: 70%;left:50%;padding:2px;z-index: 1001;');
    const img = document.createElement('img');
    img.setAttribute('src', '/resources/loading.gif');
    divas.append(img);
    blockPrice.append(divas);

    const alert = document.createElement("div");
    alert.setAttribute("id", "alert");
    alert.setAttribute("display", "block");
    alert.setAttribute("style", "color:red;margin-left: 90px;margin-top: 10px;");


    var radios = $('input[type="radio"]');
    function checkRadio() {
      return radios.filter(':checked').filter(function () {
        return (this.value == '+');
      }).length > 0;
    }


    let total2 = '';

    function bindButton() {
      $("#adjustment").unbind('mouseover').one("mouseover", function () {
        total2 = $("#grandTotal").html();
      });
    }
    bindButton();


    let total = '';
    function totalForQanty() {
      total = $('#grandTotal').html();
    }




    function inRange() {
      if (checkRadio()) {
        adjustment = $("#adjustment").val();
        grandTotal = (parseFloat(total2) + parseFloat(adjustment));
        $("#grandTotal").html(parseFloat(grandTotal).toFixed(2));
      } else {
        adjustment = $("#adjustment").val();
        total = $("#grandTotal").html();
        grandTotal = (parseFloat(total2) - parseFloat(adjustment));
        $("#grandTotal").html(parseFloat(grandTotal).toFixed(2));

      }
    }

    if ($("#grandTotal").html() != '0.00') {
      inRange();
    }

    $('#adjustment').change(inRange);
    radios.change(inRange);

    function getPrice(i) {

      let rows = document.querySelectorAll('#lineItemTab .lineItemRow:not(.except)');
      let cargoKgArray = new Array();
      let cargoKgArray2 = new Array();
      let values = new Array();
      let values_m2 = new Array();
      let cargoVolume = new Array();
      let cargoVolume2 = new Array();
      let cargoSquare;
      let cargoSquare2 = new Array();
      let cargoTemp = new Array();
      let cargoTemp2 = new Array();
      let cargoKg;
      let dimensions;
      let dimensions_m2;
      let square;
      let weight;
      let length;
      let width;
      let height;
      let measure;
      let measures = new Array();

      for (let e = 0, len = rows.length; e < len; e++) {
        c = rows[e].dataset.rowNum;
        weight = $('#cargo_wgt' + c).val();
        length = $('#cargo_length' + c).val();
        width = $('#cargo_width' + c).val();
        height = $('#cargo_height' + c).val();

        measure = $('#measure' + c).val();

        dimensions = `${weight} ${length}x${width}x${height}`;
        dimensions_m2 = length * width;

        values.push(dimensions);
        values_m2.push(dimensions_m2);
        measures.push(Number(measure));
               
        values = values.join();
        values = values.split(" ").join();
        values = values.split(",");

      }

  
      measures = measures.map(function (item) { return item == 2 || item == 3 ? 1 : item; });

      const found = measures.includes(1);
      let allEqual = false;

      if (found) {
        allEqual = measures.every((val, i, arr) => val === arr[0]);
      }   

      let qantity = new Array();

      for (j = 0; j <= values.length; j = j + 2) {
        if (values[j] !== undefined) {
          cargoKgArray.push(Number(values[j]));
          cargoTemp.push(values[j + 1]);
        }
      }


      for (let m = 0; m < rows.length; m++) {
        b = rows[m].dataset.rowNum;
        qantity[m] = parseInt(document.getElementById(`qty${b}`).value);
        cargoTemp2[m] = cargoTemp[m];

        cargoTemp2[m] = cargoTemp2[m].replace(/X/g, 'x');
        cargoTemp2[m] = cargoTemp2[m].replace(/,/g, '.');
        cargoTemp2[m] = cargoTemp2[m].split("x");

        cargoTemp2[m] = cargoTemp2[m].map(Number).reduce(function (a, b) { return a * b; });
        cargoVolume[m] = cargoTemp2[m];

      
        cargoVolume2[m] = parseFloat(cargoVolume[m]) * parseFloat(qantity[m]);
        cargoSquare2[m] = parseFloat(values_m2[m]) * parseFloat(qantity[m]);
        cargoKgArray2[m] = parseFloat(cargoKgArray[m]); 
      }    


      cargoKg = cargoKgArray2.map(Number).reduce((a, b) => a + b, 0);
      cargoVolume = cargoVolume2.map(Number).reduce(function (a, b) { return a + b; });
      cargoSquare = cargoSquare2.map(Number).reduce(function (a, b) { return a + b; });

      cargoVolume = parseFloat(cargoVolume).toFixed(3);

      let pll = 0;

      if (allEqual) {
        pll = qantity.map(Number).reduce((a, b) => a + b, 0);
      }

      console.log(cargoKg+" kg");
      console.log(cargoVolume+" m3");
      console.log(cargoSquare+" m2");
      console.log(pll+" pll");

      rows = undefined;


      let fromPost = $('#Leads_editView_fieldName_cf_1785').val();
      let toPost = $('#Leads_editView_fieldName_cf_1787').val();    

      let from_country = 'LTU';
      let to_country = 'LTU';
      let distance = 0;
   
   
      const appendAlert = document.querySelector(`#row${i} .input-group2`);
      if (fromPost == '' && toPost == '') {
        appendAlert.append(alert);
        $("#alert").html("Pirma įveskite pašto kodą");
        $("#wait").css("display", "none");
        $('#lineItemTab').css('opacity','1');
      } else {
        $.ajax({
          type: "POST",
          url: "price-algorithm/directBazinis.php",
          data: {fromPost:fromPost,toPost:toPost,cargoKg:cargoKg,cargoVolume:cargoVolume,distance:distance,cargoSquare:cargoSquare,from_country:from_country,to_country:to_country,pll:pll},
          dataType: "JSON",
          success: function (result) {
            console.log(result);
            if (result.price !== 0) {
              $(document).ajaxComplete(function () {
                setTimeout(function () {
                  $("#wait").css("display", "none");
                  $('#lineItemTab').css('opacity','1');
                }, 500);
              });
              setMarginPrice(result.price);
              $('#pricebookprice').html(parseFloat(result.price).toFixed(2));
              $('[name="total"]').val(parseFloat(result.price).toFixed(2));
              $('[name="pricebookprice"]').val(parseFloat(result.price).toFixed(2));
              let serviceMargin = document.querySelectorAll('.except');
              if (serviceMargin.length != 0) {
                let sM = checkServiceMargins();
                result.price = parseFloat(result.price) + parseFloat(sM);

              }
              // $(`#hdnProductId${i}`).val(result.productid);
              // $(`#lineItemType${i}`).val('Products');
              // insertValue(result.price, i);
              // $(`#productTotal${i}`).html(parseFloat(result.price).toFixed(2));
              // $(`#totalAfterDiscount${i}`).html(parseFloat(result.price).toFixed(2));
              // $(`#netPrice${i}`).html(parseFloat(result.price).toFixed(2));
              // $(`#listPrice${i}`).val(parseFloat(result.price).toFixed(2));    


              let grand = $("#grandTotal");    
              grand.html(parseFloat(result.price).toFixed(2));  
              

              postTrig = '';               
            
            } else {
           
    
              $(document).ajaxComplete(function () {
                setTimeout(function () {
                  $("#wait").css("display", "none");
                  $('#lineItemTab').css('opacity','1');
                }, 1000);
              });
              appendAlert.append(alert);
              setMarginPrice(result.price);
              $("#grandTotal").html(parseFloat(result.price).toFixed(2));

              $('.input-group2').click(function () {
                alert.remove();
              });

              $("#alert").html(result.combination);                   
              $("#adjustment").val('0');
            }
          }
        });
      }
    }


    function setMarginPrice(price) {
      let rows = document.querySelectorAll('#lineItemTab .lineItemRow:not(.except)');

      if (rows.length == 1) {
        $(`[name='margin${rows.length}']`).val(price);
      } else {

        let marArray = new Array();
        let totalMargin
        for (i = 0; i < rows.length; i++) {
          c = rows[i].dataset.rowNum;
          marArray.push($(`[name='margin${c}']`).val());
        }
        totalMargin = marArray.map(Number).reduce((a, b) => { return a + b; });
        if (price >= totalMargin) {
          price = price - totalMargin;
        }
        c = rows[rows.length - 1].dataset.rowNum;
        $(`[name='margin${c}']`).val(price);

      }
    }

    function checkServiceMargins() {
      let row = document.querySelectorAll('.except');
      let rowArr = new Array();
      for (i = 0; i < row.length; i++) {
        let count = row[i].dataset.rowNum;
        rowArr.push($(`[name='margin${count}']`).val());
      }

      let rez = rowArr.map(Number).reduce((a, b) => { return a + b });
      return rez;
    }



    function insertValue(result, pro) {
      $(`#hideinput${pro}`).val(parseFloat(result).toFixed(2));      
    }

    $('#lineItemTab').click(function () {
      alert.remove();
    });

    document.getElementById('Leads_editView_fieldName_cf_1785').classList.add('enter_trig');
    document.getElementById('Leads_editView_fieldName_cf_1787').classList.add('enter_trig');
    document.getElementById('lineItemTab').classList.add('enter_trig');



    var input = document.querySelectorAll(".enter_trig");
    for (let trigId = 0; trigId < input.length; trigId++) {
      input[trigId].addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
          event.preventDefault();
          trigger();
          postTrigger();
        }
      });
    }


    $('#lineItemTab').change(function (e) {
      let id;
      if ($(e.target).hasClass("tare")) {   
        id = e.target.parentElement.parentElement.dataset.rowNum;  
        let tare = e.target.value;
        if (url.get('record') == null){
          if(tare == 1){
            $(`#cargo_length${id}`).val(0.8);
            $(`#cargo_width${id}`).val(1.2);
            $(`#cargo_height${id}`).val(1);
          }else if(tare == 2){
            $(`#cargo_length${id}`).val(1.2);
            $(`#cargo_width${id}`).val(1);
            $(`#cargo_height${id}`).val(1);
          }else if(tare == 3){
            $(`#cargo_length${id}`).val(1.2);
            $(`#cargo_width${id}`).val(1.2);
            $(`#cargo_height${id}`).val(1);
          }else if(tare == 14){
            $(`#cargo_length${id}`).val(0.8);
            $(`#cargo_width${id}`).val(0.6);
            $(`#cargo_height${id}`).val(1);
          }else{
            $(`#cargo_length${id}`).val('');
            $(`#cargo_width${id}`).val(''); 
            $(`#cargo_height${id}`).val('');
          }
        }
      }else{
        id = e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.dataset.rowNum;
        if ($(`#cargo_wgt${id}`).val() && $(`#cargo_length${id}`).val() && $(`#cargo_width${id}`).val() && $(`#cargo_height${id}`).val()) {
          if (!$(event.target).hasClass("tare")) {
            trigger();
            postTrigger();
          }
        }
      }

    });

    let postTrig = '';
    function postTrigger() {
      if ($('#cargo_wgt1').val() && $('#cargo_length1').val() && $('#cargo_width1').val() && $('#cargo_height1').val()) {
        $('#Leads_editView_fieldName_cf_1785').change(function () {
          postTrig = 1;
          trigger();
        });

        $('#Leads_editView_fieldName_cf_1787').change(function () {
          postTrig = 1;
          trigger();
        });
      }
    }

    postTrigger();

    function trigger() {
      $("#wait").css("display", "block");
      $('#lineItemTab').css('opacity','0.2');
      let product = document.querySelectorAll('.lineItemRow');
      let pro;
      for (let i = 0, len = product.length; i <= len - 1; i++) {
        pro = document.querySelectorAll('.lineItemRow')[i].dataset.rowNum;
      }

      if (typeof ($(`#hideinput${pro}`).val()) === "undefined") {
        const hideinput = document.createElement('input');
        hideinput.setAttribute('id', `hideinput${pro}`);
        hideinput.setAttribute('class', `hideprice`);
        hideinput.setAttribute('type', 'hidden');
        hideinput.setAttribute('value', '');
        blockPrice.append(hideinput);

        const qanty = document.createElement('input');
        qanty.setAttribute('id', 'qanty' + pro);
        qanty.setAttribute('type', 'hidden');
        qanty.setAttribute('value', '1');
        blockPrice.append(qanty);
      }
      if (typeof ($(`#faster`).val()) === "undefined") {
        const faster = document.createElement('input');
        faster.setAttribute('id', 'faster');
        faster.setAttribute('type', 'hidden');
        faster.setAttribute('value', '');
        blockPrice.append(faster);
      }
      if (typeof ($(`#termo`).val()) === "undefined") {
        const termo = document.createElement('input');
        termo.setAttribute('id', 'termo');
        termo.setAttribute('type', 'hidden');
        termo.setAttribute('value', '');
        blockPrice.append(termo);
      }
      if (typeof ($(`#downtime`).val()) === "undefined") {
        const downtime = document.createElement('input');
        downtime.setAttribute('id', 'downtime');
        downtime.setAttribute('type', 'hidden');
        downtime.setAttribute('value', '');
        blockPrice.append(downtime);
      }
      if (typeof ($(`#loading_work`).val()) === "undefined") {
        const loading_work = document.createElement('input');
        loading_work.setAttribute('id', 'loading_work');
        loading_work.setAttribute('type', 'hidden');
        loading_work.setAttribute('value', '');
        blockPrice.append(loading_work);
      }

      getPrice(pro);
 
    }


    const addProduct = $('#addProduct');
    addProduct.click(function () {
      let product = document.querySelectorAll('*[placeholder="Pvz: 30 0,7x0,4x0,3"]');
      for (let i = 1, len = product.length; i <= len; i++) {
        $("#adjustment").on("mouseover", function () {
          bindButton();
        });
        $('#qty' + i).attr("readonly", "readonly");
        // $('#productName'+i).attr('disabled', 'disabled');
        document.getElementById('totalProductCount').value = i;
      }
    });
  }
});


function isNumber(evt) {
  let charCode = (event.which) ? event.which : event.keyCode;
  evt.target.value = evt.target.value.replace(/,/g, '.');


  if (charCode != 46 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  } else {
    //if dot sign entered more than once then don't allow to enter dot sign again. 46 is the code for dot sign
    let parts = evt.srcElement.value.split('.');

    if (parts.length > 1 && (charCode == 46 || charCode == 44)) {
      return false;
    }
    return true;
  }
}
