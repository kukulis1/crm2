function formatInvoice(id){  
  if($.trim($('#Leads_detailView_fieldValue_rating .value span').html()) == 'Transporto užsakymas' || $.trim($('#Leads_detailView_fieldValue_rating .value span').html()) == 'Perkraustymo užsakymas'){    
    if(confirm('Ar tikrai norite suformuoti pasiūlymą?')){
      window.location.href= `/vtlib/Vtiger/word/index.php?lead=${id}`;
    }
  }else{
    alert("Pasirinkite užsakymo tipą. Generuoti pasiūlymą galima tik iš tipų 'Transporto užsakymas' ir 'Perkraustymo užsakymas'");
    $('#Leads_detailView_fieldValue_rating .editAction').click();    
  }
}