{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
	    {assign var="cargo_wgt" value="cargo_wgt"|cat:$row_no}
	    {assign var="cargo_length" value="cargo_length"|cat:$row_no}
	    {assign var="cargo_width" value="cargo_width"|cat:$row_no}
	    {assign var="cargo_height" value="cargo_height"|cat:$row_no}
			{assign var="productName" value="productName"|cat:$row_no}
			{assign var="hdnProductId" value="hdnProductId"|cat:$row_no}
			{assign var="comment" value="comment"|cat:$row_no}
  		{assign var="qty" value="qty"|cat:$row_no}
  		{assign var="measure" value="measure"|cat:$row_no}

	<td style="text-align:center; width: 100px;">
		<i class="fa fa-trash deleteRow cursorPointer" title="{vtranslate('LBL_DELETE',$MODULE)}"></i>
		&nbsp;<a><img src="{vimage_path('drag.png')}" border="0" title="{vtranslate('LBL_DRAG',$MODULE)}"/></a>
		<input type="hidden" class="rowNumber" value="{$row_no}" />
	</td>

	<td style="width: 600px;">
			<div class="itemNameDiv form-inline">
				<div class="row">
					<div class="col-lg-10">
						<div class="input-group2" style="width: 100%;margin-left: 20px;">
	<input type="hidden" id="{$productName}" name="{$productName}" value="1" class="productName form-control" autoComplete="off">
		<select id="service_select{$row_no}" name="service_select{$row_no}" class="inputElement tare" value="" style="display: none;">	
								<option value="0">{vtranslate('LBL_SELECT_OPTION', $MODULE)}</option>	    	
							{foreach item=service from=$services}	
							{if {$service['id']} neq '999'}
								<option {if $service['id'] eq $data.$service_id}selected{/if} value="{$service['id']}" >{$service['name']}</option>			
								{/if}
							{/foreach}
					</select>		
	
	<input type="text" id="{$cargo_wgt}" name="{$cargo_wgt}" value="{if $data.$cargo_wgt}{$data.$cargo_wgt}{else}{if empty($GET['record'])}{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 60px;margin-right: 10px;" autoComplete="off" data-rule-required=true>

	<input type="text" id="{$cargo_length}" name="{$cargo_length}" value="{if $data.$cargo_length}{$data.$cargo_length}{else}{if empty($GET['record'])}{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 40px;margin-right: 10px;" autoComplete="off" data-rule-required=true>
	<input type="text" id="{$cargo_width}" name="{$cargo_width}" value="{if $data.$cargo_width}{$data.$cargo_width}{else}{if empty($GET['record'])}{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 40px;margin-right: 10px;" autoComplete="off" data-rule-required=true>
	<input type="text" id="{$cargo_height}" name="{$cargo_height}" value="{if $data.$cargo_height}{$data.$cargo_height}{else}{if empty($GET['record'])}{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 40px;" autoComplete="off" data-rule-required=true>
		
			<input type="hidden" id="{$hdnProductId}" name="{$hdnProductId}" 	value="{$data.$hdnProductId}" class="selectedModuleId"/>

				</div>
			</div>
		</div>
		
		<div>	<br>
			<textarea style="width: 500px;" id="{$comment}" name="{$comment}" class="lineItemCommentBox form-control tare">{decode_html($data.$comment)}</textarea></div>
		</div>
</div>
	</td>

		<td>
		<div id="service_div{$row_no}" style="display: none;">Paslaugos kaina: <input id="service_price{$row_no}" class="smallInputBox inputElement tare" type="text" value="{$data.$margin}" style="display: block;"></div>
			<select id="measure{$row_no}" name="measure{$row_no}" class="inputElement tare">	
				<option value="1">pll</option>
					{foreach item=row from=$MEASURE}	
				<option value="{$row['id']}" {if $data.$measure eq $row['id']}selected{/if}>{$row['code']}</option>			
			{/foreach}			
		</select>
	</td>

	<td>
			<input id="{$qty}" name="{$qty}" type="text" class="qty smallInputBox inputElement" data-num="{$row_no}"
			   data-rule-required=true  value="{if !empty($data.$qty)}{$data.$qty}{else}1{/if}"  {if $MODULE eq 'Invoice'}style="width: 50px;"{/if}/>
	</td>
{/strip}