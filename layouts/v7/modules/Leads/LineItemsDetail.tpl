{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Inventory/views/Detail.php *}

{assign var=ITEM_DETAILS_BLOCK value=$BLOCK_LIST['LBL_ITEM_DETAILS']}

{assign var=COL_SPAN1 value=0}
{assign var=COL_SPAN2 value=0}
{assign var=COL_SPAN3 value=2}

{assign var="RELATED_PRODUCTS" value=$RELATED_PRODUCTS}

<input type="hidden" class="isCustomFieldExists" value="false">

{assign var="FINAL" value=$PRICES}

<div class="details block">
    <div class="lineItemTableDiv">
        <table class="table table-bordered lineItemsTable" style = "margin-top:15px">
            <thead>
            <th colspan="{$COL_SPAN1}" class="lineItemFieldName">             
              <strong>Krovinys: SVORIS ILGISxPLOTISxAUKŠTIS</strong>
            </th>
            <th colspan="{$COL_SPAN2}" class="lineItemFieldName">
                     <strong>Tara</strong>               
            </th>
            <th colspan="{$COL_SPAN3}" class="lineItemFieldName">
               <strong>Kiekis</strong>      
            </th>
            </thead>
            <tbody>

                {foreach key=INDEX item=LINE_ITEM_DETAIL from=$RELATED_PRODUCTS}
                    <tr>
                         <td>
                            <div><h5>{$LINE_ITEM_DETAIL["productName$INDEX"]}</h5></div>
                            <div>{decode_html($LINE_ITEM_DETAIL["comment$INDEX"])|nl2br}</div>
                          </td>                    
                            <td>
                                {$LINE_ITEM_DETAIL["measure_code$INDEX"]}
                            </td>          
                            <td>
                                {$LINE_ITEM_DETAIL["qty$INDEX"]}
                            </td> 
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>


    {* Next summary *}
    <table class="table table-bordered lineItemsTable">
        <tr>
            <td width="83%">
                <div class="pull-right">
                    <strong>Kainyno kaina</strong>
                </div>
            </td>
            
          <td>
            <div align="right">{$FINAL.pricebook_price}</div>
         </td>
        </tr> 
         <tr>
            <td width="83%">
                <div class="pull-right">
                    <strong>Sutarta kaina</strong>
                </div>
            </td>
            
          <td>
            <div align="right">{$FINAL.agreed_price}</div>
         </td>
        </tr> 
         <tr>
            <td width="83%">
                <div class="pull-right">
                    <strong>Mokama suma</strong>
                </div>
            </td>
            
          <td>
            <div align="right">{$FINAL.total}</div>
         </td>

        </tr> 
    </table>
</div>