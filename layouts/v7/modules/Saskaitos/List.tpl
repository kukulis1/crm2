<link rel="stylesheet" href="layouts/v7/modules/Saskaitos/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Saskaitos/resources/main.js?u=16"></script>

{assign var=ORDERS value=$orders}
{assign var=all_records value=$all_records}
{assign var=PAGINATION value=$pagination}
{assign var=boses value=$BOSE}
{assign var=role value=$ROLE}

<div id="table-content" class="table-container">
<div id="wait" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>
    <div class="row">
    	<div class="col-sm-7 filter_salesorders" style="margin-top: 10px;">
            <span style="margin-left: 20px; margin-right: 10px; font-family: 'OpenSans-Semibold', 'ProximaNova-Semibold', sans-serif; font-weight: normal; font-size: 1.1em;">Filtruoti užsakymus pagal:</span>

            <select id="select_filter_by" class="inputElement" style="width: 120px;">
                <option value="order_date">Užsakymo datą</option>
                <option value="load_date">Pasikrovimo datą</option>
                <option value="unload_date">Išsikrovimo datą</option>
            </select>
            
            <input type="text" id="salesorders_from" class="listSearchContributor inputElement search_date" placeholder="Nuo" style="width: 150px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off"> 
            <input type="text" id="salesorders_to" class="listSearchContributor inputElement search_date" placeholder="Iki" style="width: 150px;" data-fieldtype="date" data-date-format="yyyy-mm-dd" autocomplete="off">
            <button class="btn btn-success" type="button" onclick="searchByPeriod();">Ieškoti</button>      

            <button class="btn btn-success" data-accountid="" type="button" id="write_all_selected" onclick="writeSelectedInvoices();" 
            style="float:right;" disabled>Išrašyti visas pažymėtas</button>           
        </div>
        <input type="hidden" id="" value="">
        <input type="hidden" id="hidden_today" value="{$today_writed_invoices}">      
        <div class="col-sm-5" style="font-size: 15px;margin-top:13px;display:flex;">
            <div style="margin-right:10px;">Reikia išrašyti: 
            <span id="plan"></span> 
            <img id="spin" style="display: none;" src="../resources/ajax-loader.gif"></div>
            <div style="margin-right:10px;">Šiandien jau išrašyta: {$today_writed_invoices}</div>           
        </div>
    </div>

     <form name='list' id='submit-invoice' action='' onsubmit="return false;">
			<table id="listview-table" class="table listview-table">
				<thead>
					<tr class="listViewContentHeader">
          <th>
          <div class="dropdown" style="float:left;">
							<span class="input dropdown-toggle" title="{vtranslate('LBL_CLICK_HERE_TO_SELECT_ALL_RECORDS',$MODULE)}">
								<input class="listViewEntriesMainCheckBox" onClick="toggle(this)" type="checkbox">
							</span>
						</div>
          </th>
            <th>Veiksmas</th>
            <th>Veiksmas</th>
            <th>Užsakymo data</th>        
            <th>Siuntos nr.</th>
            <th>Užsakovas</th>
            <th>Kainynas</th>
            <th>Tipas</th>
            <th>Pasikrovimo data</th>
            <th>Pasikrovimo adresas</th>
            <th>Išsikrovimo data</th>
            <th>Išsikrovimo adresas</th>
            <th>U-vnt/kg/m3</th>
            <th>F-vnt/kg/m3</th>
            <th>Apm-vnt/kg/m3</th>
            <th>Papildomi darbai</th>          
            <th>Pap. Kaina</th>
            <th>Bendra kaina</th>
            <th>Statusas</th>
            <th>Atsakingas asmuo</th>
          </tr>	
				</thead>
        <tr>
          <th></th>
        <th class="inline-search-btn">
					<div><button class="btn btn-success btn-sm hide" data-trigger="listSearch"></button>
          {if in_array($role,$boses)}
           <button class="btn btn-warning btn-sm" id="withOutInvoice" type="button" onclick="withOutInvoiceBtn();" disabled>Be sąskaitos</button>	</div>
          {/if}
					</th>
          <th><span id="old_rec"> Viso: {$all_records}</span> <span style="display: none;" id="filter_rec"></span></th>
          <th><input type="text" id="order_date" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></th>        
          <th><input type="text" id="item_num" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="customer" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="pricebook" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="type" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="load_date" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></th>
          <th><input type="text" id="load_address" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" id="unload_date" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></th>
          <th><input type="text" id="unload_address" class="listSearchContributor inputElement search_input"></th>
          <th><input type="text" class="listSearchContributor inputElement"></th>
          <th><input type="text" class="listSearchContributor inputElement"></th>
          <th><input type="text" class="listSearchContributor inputElement"></th>
          <th><input type="text" class="listSearchContributor inputElement"></th>
          <th><input type="text" class="listSearchContributor inputElement"></th>
          <th><input type="text" id="overall_price" class="listSearchContributor inputElement"></th>
          <th>
          
           <select id="status" class="listSearchContributor inputElement search_input">         
              <option selected></option>
              <option value="Sent">Naujas</option>
              <option value="Approved">Priimtas</option>
              <option value="in progress">Vykdomas</option>
              <option value="delivered">Atliktas</option>
            </select>
          </th>
          <th><input type="text" id="manager" class="listSearchContributor inputElement search_input"></th>
        </tr>       
		<tbody id="firstRecords">           
           {$count = 1}         
           {foreach from=$orders item=order}  
          <tr style="cursor:pointer" onClick="action(event);">
          	<td class="except" style="cursor: default !important;">
              <input type="hidden" class="accountid" value="{$order['accountid']}"/>  
              <input type="checkbox" onclick="checkCustomer();" name="list" value="" class="listViewEntriesCheckBox"/>
            </td>
          	<td class="listViewEntryValue except" style="cursor: default !important;"><button class="btn btn-success btn-sm write_invoice" style="cursor: pointer !important;">Išrašyti</button></td>
          	<td class="listViewEntryValue except" style="cursor: default !important;"><button class="btn btn-success btn-sm write_invoice_all" style="cursor: pointer !important;">Išrašyti visas</button></td>
          	<td class="listViewEntryValue"> {if !empty($order["preinvoice"])}  <i class="fa fa-history" title="Yra išankstinė saskaita" style="font-size: 15px;"></i>{/if}   {date('Y-m-d',strtotime($order["createdtime"]))}</td>
               <input type="hidden" class="order_id" value="{$order['salesorderid']}"/>    
            <td class="listViewEntryValue">{if !empty($order["shipment_code"])}<a target="_blank" href="/index.php?module=SalesOrder&view=Detail&record={$order["salesorderid"]}">{$order["shipment_code"]}</a> {else} --- {/if}</td>   
          	<td class="listViewEntryValue" id="customer_name">{$order["accountname"]}</td>   
               <input type="hidden" class="user_id" value="{$order['accountid']}"/>   
            <td class="listViewEntryValue" id="pricebook" {if !empty($order["pricebook_details"])}title="{$order["pricebook_details"]}"{/if}>{if !empty($order["pricebook"])}{$order["pricebook"]} {else} --- {/if}</td>      
          	<td class="listViewEntryValue">{$order["type"]}</td>
          	<td class="listViewEntryValue">{$order["load_date_from"]}</td>
          	<td class="listViewEntryValue" style="text-align: left!important;" title="{$MODULE_MODEL->getPostCodeInfo($order["bill_code"])}">{$order["bill_street"]}, {$order["bill_city"]}, {$order["bill_code"]}</td>
          	<td class="listViewEntryValue">{$order["unload_date_from"]}</td>	
            <td class="listViewEntryValue" style="text-align: left!important;" title="{$MODULE_MODEL->getPostCodeInfo($order["ship_code"])}">{$order["ship_street"]}, {$order["ship_city"]}, {$order["ship_code"]}</td>
          	<td class="listViewEntryValue">{if !empty($order["ordered_weight"])} {$order['quantity']}/{$order['ordered_weight']}/{number_format($order['ordered_volume'],2)} {else} --- {/if}</td>
          	<td class="listViewEntryValue">{if !empty($order['revised_weight'])} {$order['quantity']}/{$order['revised_weight']}/{number_format($order['revised_volume'],2)} {else} --- {/if}</td>
          	<td class="listViewEntryValue">
             {if $order['show_taxable'] eq 1}
              {if !empty($order["ordered_weight"])} <b>{$order['quantity']}/{$order['taxable_weight']}/{number_format($order['taxable_volume'],2)}  </b>{else}---{/if}
              {else}
                ---
              {/if}
              </td>
          	<td class="listViewEntryValue">---</td>
          	<td class="listViewEntryValue">---</td>
          	<td class="listViewEntryValue change_price">          
              <img id="spin{$count}" style="display: none;" src="/resources/ajax-loader.gif"> 
              <span id="{$count}" class="price-bold amount{$count}" rel="popover" data-toggle="popover" data-content='<input id="price_value{$count}" style="width:55px; display: flex; margin-left: 3px;" value="{$order["totalprice"]}"> <button style="margin-top:5px" class="btn btn-success btn-sm" id="save_price{$count}">Saugoti</button>'>{$order["totalprice"]}  {if $order['agreed_price'] != null}<span style="font-weight: normal;" title="Sutarta kaina">*</span>{/if}</span>
            </td>
          	<td class="listViewEntryValue">{vtranslate($order["sostatus"], 'SalesOrder')}</td>
          	<td class="listViewEntryValue">{if !empty($order["user_name"])} {$order["first_name"]} {$order["last_name"]} {else} --{/if}</td>
        </tr>	
       {assign var=count value=$count+1}
          {/foreach}          
				</tbody>
			</table> 
     
        {if $count <= 1}<div id="no_records" style="text-align: center;"><h5>Užsakymų su dokumentais nėra</h5></div>{/if} 
    
         <div id="no_records" style="text-align: center; display: none;"><h5>Nieko nerasta</h5></div>     
</form>

{$pagination}

</div>

