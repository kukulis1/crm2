let upSide = document.querySelector('#appnav');
// Hide unnecessary buttons
if (upSide.querySelector('#Saskaitos_listView_basicAction_LBL_ADD_RECORD') != null) {
  	upSide.querySelector('#Saskaitos_listView_basicAction_LBL_ADD_RECORD').style.display = 'none';
}
if (upSide.querySelector('#Saskaitos_basicAction_LBL_IMPORT') != null) {
  	upSide.querySelector('#Saskaitos_basicAction_LBL_IMPORT').style.display = 'none';
}

if (document.querySelector('#appnav').querySelector('.settingsIcon') != null) {
  	upSide.querySelector('.settingsIcon').style.display = 'none';
}

function searchByPeriod() { 
 	searchFilter(); 
}

$(document).ready(function () {
	var invoicePlan = $.ajax({
		type: "POST",
		url: "modules/Saskaitos/ajax/getAllTimeInvoicePlan.php",
		data: {1:1},
		beforeSend: function(){
			$('#spin').show();
			$('#plan').hide();			
		},
		success: function(response){					
			$('#spin').hide();			
			$('#plan').text(response != '' ? response : 0);
			$('#hidden_plan').text(response != '' ? response : 0);
			$('#plan').show();		
		}
	});

	// Create new buttons
	let btnLi = document.createElement('li');
	let button = document.createElement('button');
	button.setAttribute('class', 'btn addButton btn-default module-buttons');
	button.setAttribute('id', 'write-global');
	button.innerText = 'Išrašyti visas saskaitas automatu';
	btnLi.append(button);

	var popOverSettings = {
		placement: 'left',
		container: 'body',
		html: true,
		selector: '[rel="popover"]',
		content: function () {
			return $('#popover-content').html();
		}
	}

	//popover
	$(function () {
		$('body').popover(popOverSettings)
	});

	$(function () {
		$('.search_date').datepicker({
			dateFormat: "yy-mm-dd",
			autoclose: true
		});
	});

  $('.search_input').each(function () {
    $(this).keydown(function (e) {
      var key = e.which;
      if (key == 13 || key == 9) {
		if(invoicePlan){
			invoicePlan.abort();
		}        
        	searchFilter();
      }
    });
    $(this).change(function () {
      if ($(this).val() != '') {
        if(invoicePlan){
          invoicePlan.abort();
        }
        searchFilter();        
      }
    });
  }); 


});

function toggle(source) {
  $('#filter-records .listViewEntriesCheckBox').each(function(){
    $(this).attr('checked', true);
  });

  checkCustomer();
}


function checkCustomer(){
	let orderCustomerId = new Array();  
	
	$(".listViewEntriesCheckBox:checked").each(function () {     
		if($(this).is(':checked')){
			orderCustomerId.push($(this).parent().find('.accountid').val());     
		}
	});

	const allEqual = orderCustomerId => orderCustomerId.every(v => v === orderCustomerId[0]); 

	if(orderCustomerId.length > 0 && allEqual(orderCustomerId)){
		accountid = orderCustomerId[0];   
		$('#write_all_selected').attr('disabled',false); 
		$('#write_all_selected').data('accountid',`${accountid}`);
	}else if(orderCustomerId.length == 0){
		$('#write_all_selected').attr('disabled',true);
		$('#write_all_selected').data('accountid','');
	}else{
		$('#write_all_selected').attr('disabled',true); 
		$('#write_all_selected').data('accountid','');
	}    
}

function writeSelectedInvoices() {
	let user = $('#write_all_selected').data('accountid');
	let filterBy = 'salesorderid';
	let salesorderid = new Array();

	$(".listViewEntriesCheckBox:checked").each(function () {     
		if($(this).is(':checked')){     
			salesorderid.push($(this).parent().parent().find('.order_id').val());
		}
	});
 
  return window.location.href = `index.php?module=Invoice&view=Edit&app=SALES&account_id=${user}&create&filterBy=${filterBy}&order_ids=${salesorderid}`;
}

function action(e) {
	let orderId;
	let user;

	if (!$(e.target).closest('button').length && !$(e.target).closest('input').length && $(e.target).closest('.change_price').length) {
		change_price(e);
	} else if (!$(e.target).closest('button').length && !$(e.target).closest('input').length && !$(e.target).hasClass('except')) {
		orderId = e.target.parentElement.querySelector(".order_id").value;
	return window.location.href = `index.php?module=SalesOrder&view=Detail&record=${orderId}`;
	} else if (!$(e.target).closest('input').length && !$(e.target).closest('.write_invoice_all').length && !$(e.target).hasClass('except')) {
		orderId = e.target.parentElement.parentElement.querySelector(".order_id").value;
		user = e.target.parentElement.parentElement.querySelector(".user_id").value;  
		return window.location.href = `index.php?module=Invoice&view=Edit&app=SALES&account_id=${user}&order_ids=${orderId}&filterBy=salesorderid&create`;
	} else if ($(e.target).closest('.write_invoice_all').length) {
		user = e.target.parentElement.parentElement.querySelector(".user_id").value;

		let filterBy, from, to, orderDate, orderNumber, customer, pricebook, loadDate, loadAddress, unloadDate, unloadAddress, status, manager;

		from = $('#salesorders_from').val();
		to = $('#salesorders_to').val();
		filterBy = $('#select_filter_by').val();

		$('.listSearchContributor').each(function () {
			if ($(this).val() != '') {
				if ($(this).attr("id") == 'order_date') {
					orderDate = '&orderDate=' + $(this).val();
				}
				if ($(this).attr("id") == 'item_num') {
					orderNumber = '&orderNumber=' + $(this).val();
				}
				if ($(this).attr("id") == 'customer') {
					customer = '&customer=' + $(this).val();
				}
				if ($(this).attr("id") == 'pricebook') {
					pricebook = '&pricebook=' + $(this).val();
				}
				if ($(this).attr("id") == 'load_date') {
					loadDate = '&loadDate=' + $(this).val();
				}
				if ($(this).attr("id") == 'load_address') {
					loadAddress = '&loadAddress=' + $(this).val();
				}
				if ($(this).attr("id") == 'unload_date') {
					unloadDate = '&unloadDate=' + $(this).val();
				}
				if ($(this).attr("id") == 'unload_address') {
					unloadAddress = '&unloadAddress=' + $(this).val();
				}
				if ($(this).attr("id") == 'status') {
					status = '&status=' + $(this).val();
				}
				if ($(this).attr("id") == 'manager') {
					manager = '&manager=' + $(this).val();
				}
			}
		});

		return window.location.href = `index.php?module=Invoice&view=Edit&app=SALES&account_id=${user}&create${(orderDate) ? orderDate : ''}${(orderNumber) ? orderNumber : ''}${(customer) ? customer : ''}${(pricebook) ? pricebook : ''}${(loadDate) ? loadDate : ''}${(loadAddress) ? loadAddress : ''}${(unloadDate) ? unloadDate : ''}${(unloadAddress) ? unloadAddress : ''}${(status) ? status : ''}${(manager) ? manager : ''}${(filterBy) ? '&filterBy=' + filterBy : ''}${(from) ? '&from=' + from : ''}${(to) ? '&to=' + to : ''}`;
	} else {

		let withOutInvoice = $('#withOutInvoice');
		if (e.target.checked) {
			if (withOutInvoice.prop('disabled', true)) {
			withOutInvoice.prop('disabled', false);
			}
		} else {
			let inputArray = new Array();
			$(".listViewEntriesCheckBox:checked").each(function () {
				inputArray.push($(this).val());
			});
			if (inputArray.length == 0) {
				withOutInvoice.prop('disabled', true);
			}
		}

	}
}

function withOutInvoiceBtn() {
	if (confirm('Ar tikrai norite pažymėti be sąskaitos?')) {
		let inputArray = new Array();
		$(".listViewEntriesCheckBox:checked").each(function () {
			inputArray.push($(this).parent().parent().find('.order_id').val());
			$(this).parent().parent().remove();
		});

		let userId = _USERMETA.id;

		$.ajax({
			type: "POST",
			url: "/invoices/setWithOutDoc.php",
			data: { orders: inputArray, userId: userId },
			dataType: 'JSON',
			success: function (response) {}
		});
	}
}


function change_price(e) {
	let orderId = e.target.parentElement.parentElement.querySelector(".order_id").value;
	let newPrice;
	let amount;
	let spinner;
	let save;
	let id = e.target.id;
	let userId = _USERMETA.id;

	setTimeout(() => {
		save = document.querySelector(`#save_price${id}`);
		amount = document.querySelector(`.amount${id}`);
		spinner = document.querySelector(`#spin${id}`);

		save.onclick = () => {
			newPrice = document.querySelector(`#price_value${id}`).value;
			newPrice = newPrice.replace(',', '.');
			$(`.amount${id}`).popover('hide');
			$.ajax({
			url: "/invoices/change_price.php",
			type: "POST",
			data: { orderId: orderId, newPrice: newPrice, userId: userId },
			beforeSend: function () {
				amount.style = "display: none;";
				spinner.style = "display: inline-block;";
			},
			success: function (data) {
				if (data) {
					setTimeout(() => {
						amount.style = "display: inline-block;";
						spinner.style = "display: none;";
						fixprice = parseFloat(data).toFixed(2);
						fixprice = fixprice.replace('.', ',');
						e.target.innerText = fixprice;
					}, 1000);
				} else {
					console.log('error');
				}
			}
			});

		}
	}, 100);
}

function searchFilter() {

	let filterBy, from, to, orderDate, orderNumber, customer, pricebook, type, loadDate, loadAddress, unloadDate, unloadAddress, status, manager;

	$('#write_all_selected').attr('disabled',true);
	$('#write_all_selected').data('accountid','');
	$('#write_all_selected').data('salesorderid','');

	$('.listViewEntriesCheckBox:checked').each(function(){
		$(this).attr('checked',false);
	});

	from = $('#salesorders_from').val();
	to = $('#salesorders_to').val();
	filterBy = $('#select_filter_by').val();

	$('.listSearchContributor').each(function () {
		if ($(this).val() != '') {
			if ($(this).attr("id") == 'order_date') {
				orderDate = $(this).val();
			}
			if ($(this).attr("id") == 'item_num') {
				orderNumber = $(this).val();
			}
			if ($(this).attr("id") == 'customer') {
				customer = $(this).val();
			}
			if ($(this).attr("id") == 'pricebook') {
				pricebook = $(this).val();
			}
			if ($(this).attr("id") == 'type') {
				type = $(this).val();
			}
			if ($(this).attr("id") == 'load_date') {
				loadDate = $(this).val();
			}
			if ($(this).attr("id") == 'load_address') {
				loadAddress = $(this).val();
			}
			if ($(this).attr("id") == 'unload_date') {
				unloadDate = $(this).val();
			}
			if ($(this).attr("id") == 'unload_address') {
				unloadAddress = $(this).val();
			}
			if ($(this).attr("id") == 'status') {
				status = $(this).val();
			}
			if ($(this).attr("id") == 'manager') {
				manager = $(this).val();
			}
		} else {
			$('#old_rec').css('display', 'block');
			$('#filter_rec').css('display', 'none');
		}
	});

  let plan = $('#hidden_plan').val();
  $('#plan').text(plan);    


  if ((from != '' && to != '') || orderDate != null || orderNumber != null || customer != null || pricebook != null || type != null || loadDate != null || loadAddress != null || unloadDate != null || unloadAddress != null || status != null || manager) {
    let no_records = $('#no_records');
	$.ajax({
		url: "modules/Saskaitos/ajax/filter_invoices.php",
		type: "POST",
		data: { from: from, to: to, filterBy: filterBy, orderDate: orderDate, orderNumber: orderNumber, customer: customer, pricebook: pricebook, type: type, loadDate: loadDate, loadAddress: loadAddress, unloadDate: unloadDate, unloadAddress: unloadAddress, status: status, manager: manager },
		dataType: 'JSON',
		beforeSend: function () {
			$('#submit-invoice').css('opacity', '0.2');
			$("#wait").show();  
			$('#spin').show();
			$('#plan').hide();      
		},
		success: function (data) {  	
			console.log(data.dates);		         
			console.log(data.salesorderids);		         
			if (data.rows == 'no_results') {         
				$("#wait").hide();
				$('#filter-records').remove();
				$('#firstRecords').hide();
				document.querySelector('.pagination').classList.add('hide');
				$('#submit-invoice').css('opacity', '1');
				no_records.show();
				$('#filter_rec').html(`Viso: 0`);
				$('#old_rec').css('display', 'none');
				$('#filter_rec').css('display', 'block');

			} else {
				if(invoicePlanSearch){
					invoicePlanSearch.abort();
				}

				var invoicePlanSearch = $.ajax({
					type: "POST",
					url: "modules/Saskaitos/ajax/getInvoicePlan.php",
					data: {dates:data.dates,salesorderids:data.salesorderids},		
					success: function(response){
						$('#spin').hide();
						$('#plan').text(response != '' ? response : 0);   
						$('#plan').show();              
					}
				});

				
				no_records.hide();
				$('#firstRecords').hide();
				let table = $('#listview-table');
				let tbody = document.createElement('tbody');
				tbody.setAttribute('id', 'filter-records');
				let html = '';
				let index = 1;
				if (data.rows != null) {
					$('#filter_rec').html(`Viso: ${data.rows.length}`);
					$('#old_rec').css('display', 'none');
					$('#filter_rec').css('display', 'block');

					data.rows.forEach(record => {
						let sostatus;
						switch (record.sostatus) {
							case 'delivered':
							sostatus = 'Atliktas';
							break;
							case 'in progress':
							sostatus = 'Vykdomas';
							break;
							case 'Approved':
							sostatus = 'Priimtas';
							break;
							default:
							sostatus = 'Naujas';
						}
						html += `
						<tr class="listViewContentHeader" style="cursor:pointer;" onClick="action(event);">
						<td class="listViewEntryValue except" style="cursor: default !important;">
						<input type="hidden" class="accountid" value="${record.accountid}"/>  
						<input type="checkbox" onclick="checkCustomer();" name="list" value="" class="listViewEntriesCheckBox"/></td>
						<td class="listViewEntryValue except" style="cursor: default !important;"><button style="cursor: pointer !important;" class="btn btn-success btn-sm write_invoice">Išrašyti</button></td>
						<td class="listViewEntryValue except" style="cursor: default !important;"><button style="cursor: pointer !important;" class="btn btn-success btn-sm write_invoice_all">Išrašyti visas</button></td>
						<td class="listViewEntryValue">${(record.invoice_type != null) ? '<i class="fa fa-history" title="Yra išankstinė saskaita" style="font-size: 15px;"></i>' : ''}  ${record.createdtime}</td>
							<input type="hidden" class="order_id" value="${record.salesorderid}"/> 
							<td class="listViewEntryValue">${(record.shipment_code != '') ? `<a target="_blank" href="/index.php?module=SalesOrder&view=Detail&record=${record.salesorderid}">` + record.shipment_code + '</a>' : '---'}</td>
						<td class="listViewEntryValue">${record.accountname}</td>
							<input type="hidden" class="user_id" value="${record.accountid}"/>  
						<td class="listViewEntryValue" title="${(record.pricebook_details != '' ? record.pricebook_details : '')}">${record.pricebook}</td>       
						<td class="listViewEntryValue">${record.type}</td>       
						<td class="listViewEntryValue">${record.load_date_from}</td>
						<td class="listViewEntryValue" title="${(record.location_from ? record.location_from : '')}">${record.bill_street} ${record.bill_city} ${record.bill_code}</td>
						<td class="listViewEntryValue">${record.unload_date_from}</td>
						<td class="listViewEntryValue" title="${(record.location_to ? record.location_to : '')}">${record.ship_street} ${record.ship_city} ${record.ship_code}</td>
						<td class="listViewEntryValue">${(record.ordered_weight != null) ? record.quantity + "/" + parseFloat(record.ordered_weight).toFixed(2) + "/" + parseFloat(record.ordered_volume) : '---'}</td>
						<td class="listViewEntryValue">${(record.revised_weight != null) ? record.revised_quantity + "/" + parseFloat(record.revised_weight).toFixed(2) + "/" + parseFloat(record.revised_volume) : '---'}</td>
						<td class="listViewEntryValue">`;
						if(record.show_taxable == 1){
						html += `<b>${(record.taxable_weight != null) ? record.taxable_quantity + "/" + parseFloat(record.taxable_weight).toFixed(2) + "/" + (record.taxable_volume != null ? parseFloat(record.taxable_volume).toFixed(2) : parseFloat(record.volume).toFixed(2)) : '---'}</b>
						<i class="fa fa-exclamation-circle ${(record.meters > 0 ? '' : 'hide')}" title="Kaina paskaičiuota atsižvelgiant į metrus" style="font-size: 20px;color:#fc631a;"></i>`;
						}else{
							html +=`---`;
						}              
						html +=`</td>
						<td class="listViewEntryValue">${'---'}</td>
						<td class="listViewEntryValue">${'---'}</td>
						<td class="listViewEntryValue change_price">
						<img id="spin${index}" style="display: none;" src="/resources/ajax-loader.gif">

						
						<span id="${index}" class="price-bold amount${index}" rel="popover" data-toggle="popover" data-content='<input id="price_value${index}" style="width:55px; display: flex; margin-left: 3px;" value="${(record.totalprice == null ? '0,00' : record.totalprice)}"> <button style="margin-top:5px" class="btn btn-success btn-sm" id="save_price${index}">Saugoti</button>'>${(record.totalprice == null ? '0,00' : record.totalprice)}</span>  
						${(record.agreed_price == null || record.agreed_price == '') ? '' : '<span title="Sutarta kaina">*</span>'}
						
						
						
						</td>
						<td class="listViewEntryValue">${sostatus}</td>
						<td class="listViewEntryValue">${record.first_name} ${record.last_name}</td>
						</tr>`;
						index++;
					});
				}
				if ($('#filter-records') != null) {
					$('#filter-records').remove();
					tbody.innerHTML = html;
				} else {
					tbody.innerHTML = html;
				}
				table.append(tbody);
				document.querySelector('.pagination').classList.add('hide');
				setTimeout(() => {
					$('#submit-invoice').css('opacity', '1');
					$("#wait").hide();
				}, 1000);
			}
		}
	});
  } else {
	$('#firstRecords').show();
	document.querySelector('.pagination').classList.remove('hide');
  }

}

