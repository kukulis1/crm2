let pageIs = new URLSearchParams(window.location.search);

if (pageIs.get('module') == 'Metrikaimport' && pageIs.get('view') == 'List') {

  let upSide = document.querySelector('#appnav');
  // Hide unnecessary buttons
  if(upSide.querySelector('#Metrikaimport_listView_basicAction_LBL_ADD_RECORD')){
    upSide.querySelector('#Metrikaimport_listView_basicAction_LBL_ADD_RECORD').style.display = 'none';
  }
  if(upSide.querySelector('#Metrikaimport_basicAction_LBL_IMPORT')){
   upSide.querySelector('#Metrikaimport_basicAction_LBL_IMPORT').style.display = 'none';
  }

  if (document.querySelector('#appnav').querySelector('.settingsIcon') != null) {
    upSide.querySelector('.settingsIcon').style.display = 'none';
  }

  $(document).ready(function () {
    $(function () {
      $('.search_date').datepicker({
        dateFormat: "yy-mm-dd",
        autoclose: true
      });
    });
  });



  function importOrders() {

    let from = $('#from').val();
    let to = $('#to').val();
    let shipment_code = $('#shipment_code').val();
    if (shipment_code != '') {
      shipment_code = shipment_code.trim().split(/\s*,\s*/);
    }

    if ((from != '' && to != '') || shipment_code != '') {
      if ($('#progres').children()) {
        $('#progres').children().remove();
      }

      $.ajax({
        url: "invoices/app_orders_handle.php",
        type: "POST",
        data: { from: from, to: to, shipment_code: shipment_code },
        dataType: 'json',
        beforeSend: function () {
          $("#wait").show();
          $('#shipment_code').val('');
        },
        success: function (data) {
          if (data == 'Success') {
            let node = document.createElement("h6");
            node.setAttribute('style', 'color: #35aa47');
            let textnode = document.createTextNode("Užsakymai gauti...");
            node.appendChild(textnode);
            document.getElementById('progres').appendChild(node);
            importSalesOrders();
          } else {
            setTimeout(() => {
              $("#wait").hide();
              let node = document.createElement("h6");
              node.setAttribute('style', 'color: red');
              let textnode = document.createTextNode("Įvyko klaida...");
              node.appendChild(textnode);
              document.getElementById('progres').appendChild(node);
            }, 2000);
          }

        }
      });
    }
  }


  function importSalesOrders() {
    $.ajax({
      url: "/invoices/orders_insert.php",
      type: "POST",
      data: { 1: 1 },
      dataType: 'json',
      success: function (data) {
        if (data == 'Success') {
          let node = document.createElement("h6");
          node.setAttribute('style', 'color: #35aa47');
          let textnode = document.createTextNode("Užsakymai įkelti...");
          node.appendChild(textnode);
          document.getElementById('progres').appendChild(node);
          importSalesOrdersLoads();
        } else {
          $("#wait").hide();
          let node = document.createElement("h6");
          node.setAttribute('style', 'color: red');
          let textnode = document.createTextNode("Užsakymas su tokiu id jau yra arba pasirinkta dieną nėra užsakymų");
          node.appendChild(textnode);
          document.getElementById('progres').appendChild(node);
        }

      }
    });
  }


  function importSalesOrdersLoads() {
    $.ajax({
      url: "/invoices/loads_insert.php",
      type: "POST",
      data: { 1: 1 },
      dataType: 'json',
      success: function (data) {
        if (data == 'Success') {
          let node = document.createElement("h6");
          node.setAttribute('style', 'color: #35aa47');
          let textnode = document.createTextNode("Užsakymu kroviniai įkelti...");
          node.appendChild(textnode);
          document.getElementById('progres').appendChild(node);        

          // let node2 = document.createElement("h5");
          // node2.setAttribute('style', 'color: #35aa47');
          // let textnode2 = document.createTextNode("Įkėlimas baigtas");
          // node2.appendChild(textnode2);
          // document.getElementById('progres').appendChild(node2);

          countOrderPrice();
        } else {
          $("#wait").hide();
        }

      }
    });
  }


  function countOrderPrice() {
    $.ajax({
      url: "/v1/external_data/orders_count_price.php",
      type: "POST",
      data: { 1: 1 },
      dataType: 'json',
      success: function (data) {       
        if (data == 'Success') {
          let node = document.createElement("h6");
          node.setAttribute('style', 'color: #35aa47');
          let node2 = document.createElement("h5");
          node2.setAttribute('style', 'color: #35aa47');
          let textnode = document.createTextNode("Paskaičiuota kaina...");
          let textnode2 = document.createTextNode("Įkėlimas baigtas");
          node.appendChild(textnode);
          document.getElementById('progres').appendChild(node);
          $("#wait").hide();
          node2.appendChild(textnode2);
          document.getElementById('progres').appendChild(node2);

        } else {
          $("#wait").hide();
        }

      }
    });
  }


}

