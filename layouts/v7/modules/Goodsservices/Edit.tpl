<link rel="stylesheet" href="layouts/v7/modules/Goodsservices/resources/style.css" type="text/css" />
{assign var="service" value=$service}

<form method="POST">
  <table id="listview-table" class="table listview-table">
    <thead>       
      <tr class="listViewContentHeader">
        <th>Paslauga</th>  
        <th>Buhalteris kodas</th>
        <th>Buhalterinio požymio ID</th>                    
        <th>Sandėlio kodas</th>                  
        <th>Veiksmas</th>                  
      </tr>	
    </thead>        
    <tbody> 
        <tr class="listViewContentHeader">
          <td>{$service['name']}</td>
          <td><input style="width:unset;" class="inputElement" name="code" value="{$service['code']}"></td>
          <td><input style="width:unset;" class="inputElement" name="code_id" value="{$service['code_id']}"></td>
          <td><input style="width:unset;" class="inputElement" name="storage_code" value="{$service['storage_code']}"></td>
          <td><button type="submit" class="btn btn-success btn-sm">Išsaugoti</button></td>
        </tr>	
    </tbody>   
  </table>
  <input type="hidden" name="editCode" value='1'>
</form>
