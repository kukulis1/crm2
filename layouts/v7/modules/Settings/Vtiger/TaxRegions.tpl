{*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************}
{* modules/Settings/Vtiger/views/TaxIndex.php *}

{strip}
<div class="taxRegionsContainer">
	<div class="tab-pane active">
		<div class="tab-content overflowVisible">
			{assign var=WIDTHTYPE value=$CURRENT_USER_MODEL->get('rowheight')}
			<div class="col-lg-12 marginLeftZero textOverflowEllipsis">
				<div class="marginBottom10px">
					<button type="button" class="btn btn-default addRegion addButton module-buttons" data-url="?module=Vtiger&parent=Settings&view=TaxAjax&mode=editTaxRegion" data-type="1">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;{vtranslate('LBL_ADD_NEW_REGION', $QUALIFIED_MODULE)}</button>
				</div>
				<table class="table table-bordered taxRegionsTable" style="table-layout: fixed">
					<tr>
						<th width="10">
                <strong>{vtranslate('LBL_AVAILABLE_REGIONS', $QUALIFIED_MODULE)}</strong>
						</th>
						<th width="10">
                <strong>{vtranslate('LBL_REGION_PROC_VALUE', $QUALIFIED_MODULE)}</strong>
						</th>
						<th width="70">
                <strong>{vtranslate('LBL_REGION_ARTICLE', $QUALIFIED_MODULE)}</strong>
						</th>
						<th width="10"> <strong>{vtranslate('LBL_REGION_ACTION', $QUALIFIED_MODULE)}</strong></th>
					<tr>

				

					{foreach item=TAX_REGION_MODEL from=$TAX_REGIONS}									
						{* {assign var=TAX_REGION_NAME value=$TAX_REGION_MODEL->getName()} *}

						<tr class="opacity" data-key-name="{$TAX_REGION_MODEL['name']}" data-key="{$TAX_REGION_MODEL['name']}">
							<td>
								<span class="taxRegionName">{$TAX_REGION_MODEL['name']}</span>
							</td>
							<td >
								<span class="taxRegionProc">{$TAX_REGION_MODEL['value']}</span>
							</td>

							<td>
								<span class="taxRegionArticle">{$TAX_REGION_MODEL['article']}</span>
							</td>

							<td>
							{if $TAX_REGION_MODEL['regionid'] != 0}
								<div class="pull-right actions">
									<a class="editRegion" data-url='?module=Vtiger&parent=Settings&view=TaxAjax&mode=editTaxRegion&taxRegionId={$TAX_REGION_MODEL['regionid']}'><i title="{vtranslate('LBL_EDIT', $QUALIFIED_MODULE)}" class="fa fa-pencil alignMiddle"></i></a>&nbsp;&nbsp;
									<a class="deleteRegion" data-url='?module=Vtiger&parent=Settings&action=TaxAjax&mode=deleteTaxRegion&taxRegionId={$TAX_REGION_MODEL['regionid']}'><i title="{vtranslate('LBL_DELETE', $QUALIFIED_MODULE)}" class="fa fa-trash alignMiddle"></i></a>
								</div>
								{/if}
							</td>
						</tr>
					{/foreach}
				</table>

			</div>

					<div class="col-lg-12"><i class="fa fa-info-circle"></i> {vtranslate('LBL_TAX_REGION_DESC', $QUALIFIED_MODULE)}</div>

			</div>
		</div>
	</div>
</div>
{/strip}