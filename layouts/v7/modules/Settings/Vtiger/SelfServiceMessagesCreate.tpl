{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Settings/Vtiger/views/OtherSettings.php *}
{assign var=records value=$records}

{* START YOUR IMPLEMENTATION FROM BELOW. Use {debug} for information *}

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="layouts/v7/modules/Settings/Vtiger/resources/SelfServiceMessages.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.12/dist/js/bootstrap-select.min.js"></script>

<style>
.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
	width:100%;
}
</style>

{strip}

	<div class=" col-lg-12 col-md-12 col-sm-12">
		<input type="hidden" id="supportedImageFormats" value='{ZEND_JSON::encode(Settings_Vtiger_CompanyDetails_Model::$logoSupportedFormats)}' />

		<div class="clearfix">
			<div class="btn-group pull-right editbutton-container">
				{* <button class="btn btn-default" onclick="window.location.href='/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessagesCreate'" style="margin-right: 10px;">{vtranslate('LBL_CREATE',$QUALIFIED_MODULE)}</button>
				<button class="btn btn-default" onclick="window.location.href='/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessagesEdit'">{vtranslate('LBL_EDIT',$QUALIFIED_MODULE)}</button> *}
			</div>
		</div>

		<div id="CompanyDetailsContainer" class="detailViewContainer">
			<div class="block">
				<div>
					<h4>{vtranslate('LBL_SELF_SERVICE_MESSAGE',$QUALIFIED_MODULE)}</h4>
				</div>
				<hr>
				<div class="blockData" style="padding: 0px 0 30px;width:30%;">
				<form method="POST" action="/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessagesCreate">
					<div class="form-group">
						<label for="account" style="display:block;">Klientas</label>
						<select name="customer_id" id="account" class="selectpicker" data-live-search="true">
						{foreach from=$records item=ITEM key=FIELD}		
							<option value="{$ITEM['customer_id']}">{$ITEM['accountname']} || id: {$ITEM['customer_id']}</option>	
						{/foreach}
						</select>	
					</div>
					<div class="form-group">
						<label for="message">Žinutė</label>
						<textarea type="text" class="inputElement" id="message" name="message" rows="5" style="height: unset;"></textarea>					
					</div>
					<div class="form-group" style="width: 30%;">
						<label for="period">Datos periodas</label>
						<input type="text" class="inputElement dateField" id="period" name="period" data-date-format="yyyy-mm-dd" data-calendar-type="range" data-field-type="date">
					</div>				
					<button type="submit" class="btn btn-block btn-success">Saugoti</button>
				</form>								
				</div>
			</div>
		</div>


</div>
</div>
{/strip}
