{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Settings/Vtiger/views/OtherSettings.php *}
{assign var=records value=$records}

{* START YOUR IMPLEMENTATION FROM BELOW. Use {debug} for information *}
<script src="layouts/v7/modules/Settings/Vtiger/resources/SelfServiceMessages.js"></script>
{strip}

	<div class=" col-lg-12 col-md-12 col-sm-12">
		<input type="hidden" id="supportedImageFormats" value='{ZEND_JSON::encode(Settings_Vtiger_CompanyDetails_Model::$logoSupportedFormats)}' />

		<div class="clearfix">
			<div class="btn-group pull-right editbutton-container">
				<button class="btn btn-default" onclick="window.location.href='/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessagesCreate'" style="margin-right: 10px;">Pridėti žinutę</button>
				{* <button class="btn btn-default" onclick="window.location.href='/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessagesEdit'">{vtranslate('LBL_EDIT',$QUALIFIED_MODULE)}</button> *}
			</div>
		</div>

		<div id="CompanyDetailsContainer" class="detailViewContainer">
			<div class="block">
				<div>
					<h4>{vtranslate('LBL_SELF_SERVICE_MESSAGE',$QUALIFIED_MODULE)}</h4>
				</div>
				<hr>
				<div class="blockData">
					<table class="table listview-table">
					<thead>
						<tr class="listViewContentHeader">
						<th>Klientas</th>
						<th style="width: 75%;">Žinutė</th>
						<th>Periodas</th>
						<th>Veiksmas</th>
						</tr>
					</thead>
						<tbody>
							{foreach from=$records item=ITEM}		
									<tr>
										<td class="fieldLabel" style="width:12%;padding:10px;">{$ITEM['accountname']}</td>
										<td style="word-wrap:break-word;padding:10px;">{$ITEM['message']}</td>
										<td style="word-wrap:break-word;padding:10px;">{$ITEM['period']}</td>
										<td style="word-wrap:break-word;padding:10px;">
									
										<span class="actionImages">&nbsp;&nbsp;&nbsp;
											<a name="relationEdit" href="/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessagesEdit&record={$ITEM['id']}"><i class="fa fa-pencil" title="Redaguoti"></i></a> &nbsp;&nbsp;
										{if $ITEM['customer_id'] neq '---'}
											<a onclick="deleteRecord({$ITEM['id']})"><i title="Pašalinti" class="fa fa-trash"></i></a>
										{/if}
										</span>
										
										</td>
									</tr>							
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>


</div>
</div>
{/strip}

<script>
	function deleteRecord(id){
		if(confirm('Ar tikrai norite pašalinti?')){
			window.location.href="index.php?parent=Settings&module=Vtiger&view=SelfServiceMessagesDelete&record="+id;
		}
	}
</script>
