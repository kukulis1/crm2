{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Settings/Vtiger/views/OtherSettings.php *}
{assign var=records value=$records}

{* START YOUR IMPLEMENTATION FROM BELOW. Use {debug} for information *}
<script src="layouts/v7/modules/Settings/Vtiger/resources/OtherSettings.js"></script>
{strip}

	<div class=" col-lg-12 col-md-12 col-sm-12">
		<input type="hidden" id="supportedImageFormats" value='{ZEND_JSON::encode(Settings_Vtiger_CompanyDetails_Model::$logoSupportedFormats)}' />

		<div class="clearfix">
			<div class="btn-group pull-right editbutton-container">
				<button id="updateCompanyDetails" class="btn btn-default ">{vtranslate('LBL_EDIT',$QUALIFIED_MODULE)}</button>
			</div>
		</div>

		<div id="CompanyDetailsContainer" class="detailViewContainer" >
			<div class="block">
				<div>
					<h4>{vtranslate('LBL_PRICE_COUNT_SETTINGS',$QUALIFIED_MODULE)}</h4>
				</div>
				<hr>
				<div class="blockData">
					<table class="table detailview-table no-border">
						<tbody>
							{foreach from=$records item=ITEM key=FIELD}							
									<tr>
										<td class="fieldLabel" style="width:12%"><label >{vtranslate($ITEM['title'],$QUALIFIED_MODULE)}</label></td>
										<td style="word-wrap:break-word;">{$ITEM['value']} {if $ITEM['title'] eq 'LBL_CARGO_NOT_STANDART'}{vtranslate('LBL_METERS',$QUALIFIED_MODULE)}{/if}</td>
									</tr>							
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="editViewContainer">
			<form class="form-horizontal hide" id="updateCompanyDetailsForm" method="post" action="index.php">
				<input type="hidden" name="module" value="Vtiger" />
				<input type="hidden" name="parent" value="Settings" />
				<input type="hidden" name="action" value="OtherSettingsSave" />

					<div class="form-group companydetailsedit">
							<label class="col-sm-2 fieldLabel control-label ">
								<h4>{vtranslate('LBL_PRICE_COUNT_SETTINGS',$QUALIFIED_MODULE)}</h4>
							</label>
					</div>
				{$key = 1}	
				{foreach from=$records item=ITEM}						
						<div class="form-group companydetailsedit">
							<label class="col-sm-2 fieldLabel control-label">{vtranslate($ITEM['title'],$QUALIFIED_MODULE)}</label><small>{if $ITEM['title'] eq 'LBL_CARGO_MEASURE'}Vienetus atskirti kableliu{/if} {if $ITEM['title'] eq 'LBL_LOAD_WORK_PRICE'}1kg kaina EUR{/if} {if $ITEM['title'] eq 'LBL_IDLE_PRICE'}1 valandos kaina EUR{/if}</small>
							<div class="fieldValue col-sm-5">	
									<input type="hidden" name="record{$key}" value="{$ITEM['id']}">						
									<input type="text" class="inputElement" name="value{$key}" value="{$ITEM['value']}"/>
							</div>
						</div>													
						{assign var=key value=$key+1}			
				{/foreach}
						<input type="hidden" name="count" value="{$key}">		
				<div class="modal-overlay-footer clearfix">
					<div class="row clearfix">
						<div class="textAlignCenter col-lg-12 col-md-12 col-sm-12">
							<button type="submit" class="btn btn-success saveButton">{vtranslate('LBL_SAVE', $MODULE)}</button>&nbsp;&nbsp;
							<a class="cancelLink" data-dismiss="modal" href="#">{vtranslate('LBL_CANCEL', $MODULE)}</a>
						</div>
					</div>
				</div>
			</form>
		</div>



</div>
</div>
{/strip}
