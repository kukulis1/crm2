{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Settings/Vtiger/views/OtherSettings.php *}
{assign var=records value=$records}
{assign var=USERS value=$USERS}

{strip}

	<div class=" col-lg-12 col-md-12 col-sm-12">
		<input type="hidden" id="supportedImageFormats" value='{ZEND_JSON::encode(Settings_Vtiger_CompanyDetails_Model::$logoSupportedFormats)}' />

		<div class="clearfix">
			<div class="btn-group pull-right editbutton-container">
				{* <button id="updateCompanyDetails" class="btn btn-default">{vtranslate('LBL_EDIT',$QUALIFIED_MODULE)}</button> *}
			</div>
		</div>

				<div id="CompanyDetailsContainer" class="detailViewContainer">
			<div class="block">
				<div>
					<h4>Redaguoti {vtranslate('LBL_NOTIFICATIONS_CENTER',$QUALIFIED_MODULE)}</h4>
				</div>
				<hr>
				<div class="blockData" style="padding: 0px 0 30px;width:30%;">
				<form method="POST" action="/index.php?parent=Settings&module=Vtiger&view=NotificationCenterPermisionsEdit&record={$records['id']}">			
						{$responsibles = explode(",",$records['responsibles'])}
						{$managers = explode(",",$records['managers'])}
						{$person = explode(",",$records['person'])}
					<div class="form-group">	
						<label for="name" class="col-sm-3 control-label">
								Atsakingi
						</label>				
							<select name="responsibles[]" class="inputElement" multiple>
								<option value="1" {if in_array(1,$responsibles)}selected{/if}>Atsakingas už užsakymus</option>
								<option value="2" {if in_array(2,$responsibles)}selected{/if}>Kliento vadybininkas</option>
								<option value="3" {if in_array(3,$responsibles)}selected{/if}>Atsakingas už skolas</option>
								<option value="4" {if in_array(4,$responsibles)}selected{/if}>Užsąkymą sukūres asmuo</option>
							</select>				

						<label for="name" class="col-sm-3 control-label" style="margin-top: 30px;">
								Vadovai
						</label>					
							<select name="managers[]" class="inputElement" multiple>            
								<option value="H2" {if in_array(H2,$managers)}selected{/if}>CEO</option>
								<option value="H15" {if in_array(H15,$managers)}selected{/if}>Transporto vadovas</option>
								<option value="H31" {if in_array(H31,$managers)}selected{/if}>Perkraustymo vadovas</option>  
								<option value="H11" {if in_array(H11,$managers)}selected{/if}>Apskaitos vadovas</option>
								<option value="H13" {if in_array(H13,$managers)}selected{/if}>Serviso vadovas</option>
								<option value="H14" {if in_array(H14,$managers)}selected{/if}>Sandėlio vadovas</option>            
								<option value="H21" {if in_array(H21,$managers)}selected{/if}>Biuro administratorė</option>            
							</select> 

						<label for="name" class="col-sm-3 control-label" style="margin-top: 30px;">
								Asmenys
						</label>	
							<select name="person[]" class="inputElement" multiple>
								{foreach from=$USERS item=user}      
									<option value="{$user['id']}" {if in_array($user['id'],$person)}selected{/if}>{$user['person']}</option>
								{/foreach}           
							</select> 

						
					<button type="submit" class="btn btn-block btn-success" style="margin-top: 30px;">Saugoti</button>
				</form>								
				</div>
			</div>
		</div>
		
</div>
</div>
{/strip}
