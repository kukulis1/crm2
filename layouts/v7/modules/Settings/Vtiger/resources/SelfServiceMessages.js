/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

jQuery(document).ready(function () {
  // registerEvents();

  jQuery('.app-trigger, .app-icon, .app-navigator').on('click', function (e) {
    e.stopPropagation();
    if (checkHide()) {
      toggleAppMenu('show');
    } else {
      toggleAppMenu('hide');
    }
  });

  jQuery('body').on('click', function (e) {
    if (!checkHide()) {
      toggleAppMenu('hide');
    }
  });

  function checkHide() {
    return document.querySelector('.app-menu.hide');
  }

  function toggleAppMenu(type) {
    var appMenu = jQuery('.app-menu');
    var appNav = jQuery('.app-nav');
    appMenu.appendTo('#page');
    appMenu.css({
      'top': appNav.offset().top + appNav.height(),
      'left': 0
    });
    if (typeof type === 'undefined') {
      type = appMenu.is(':hidden') ? 'show' : 'hide';
    }
    if (type == 'show') {
      document.querySelector('.app-menu').classList.remove('hide');
      appMenu.show(200, function () { });
    } else {
      document.querySelector('.app-menu').classList.add('hide');
      appMenu.hide(200, function () { });
    }
  };


  jQuery('.app-modules-dropdown-container').hover(function (e) {
    var dropdownContainer = jQuery(e.currentTarget);
    jQuery('.dropdown').removeClass('open');
    if (dropdownContainer.length) {
      if (dropdownContainer.hasClass('dropdown-compact')) {
        dropdownContainer.find('.app-modules-dropdown').css('top', dropdownContainer.position().top - 8);
      } else {
        dropdownContainer.find('.app-modules-dropdown').css('top', '');
      }
      dropdownContainer.addClass('open').find('.app-item').addClass('active-app-item');
    }
  }, function (e) {
    var dropdownContainer = jQuery(e.currentTarget);
    dropdownContainer.find('.app-item').removeClass('active-app-item');
    setTimeout(function () {
      if (dropdownContainer.find('.app-modules-dropdown').length && !dropdownContainer.find('.app-modules-dropdown').is(':hover') && !dropdownContainer.is(':hover')) {
        dropdownContainer.removeClass('open');
      }
    }, 500);

  });

  jQuery('.app-item').on('click', function () {
    var url = jQuery(this).data('defaultUrl');
    if (url) {
      window.location.href = url;
    }
  });

});




// function registerUpdateDetailsClickEvent() {
// 	jQuery('#updateCompanyDetails').on('click',function(e){
// 		jQuery('#CompanyDetailsContainer').addClass('hide');
// 		jQuery('#updateCompanyDetailsForm').removeClass('hide');
// 		jQuery('#updateCompanyDetails').addClass('hide');
// 	});
// }

// function registerCancelClickEvent() {
// 	jQuery('.cancelLink').on('click',function() {
// 		jQuery('#CompanyDetailsContainer').removeClass('hide');
// 		jQuery('#updateCompanyDetailsForm').addClass('hide');
// 		jQuery('#updateCompanyDetails').removeClass('hide');
// 	});
// }

// function registerEvents() {
// 	registerUpdateDetailsClickEvent();
// 	registerCancelClickEvent();
// }


