<link rel="stylesheet" href="layouts/v7/modules/Transfers/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Transfers/resources/main.js?u=14"></script>

{assign var=getHistory value=$getHistory}



{if $smarty.cookies.import_errors}
<div class="alert alert-danger" data-import="import_errors" id="alert_message" style="text-align: center;">
  {$smarty.cookies.import_errors}
</div>
{elseif $smarty.cookies.import_success}
<div class="alert alert-success" data-import="import_success" id="alert_message" style="text-align: center;">
  {$smarty.cookies.import_success}
</div>
{/if}
<div id="wait" style="display:none;position:fixed;top:50%;left:45%;padding:2px;"><img src="/resources/loading.gif"></div>
<div id="table-content" class="table-container">
    
    	<table class="table listview-table">
      	<thead>
      <tr>
        <th>#</th>
        <th>Failas</th>
        <th>Eilutės</th>
        <th>Įkelta eilučių</th>
        <th>Įkėlimo data</th>
      </tr>
      		</thead>       
				<tbody id="list">

        {$count = 0}
        {foreach from=$getHistory item=HIS key=key}       
          <tr onclick="window.location.href = 'index.php?module=Transfers&view=Draft&record={$HIS['id']}'" style="cursor: pointer;">
              <td>{$key+1}</td>
              <td>{$HIS['filename']}</td>
              <td>{if $HIS['rows']}{$HIS['rows']}{else}0{/if}</td>
              <td>{if $HIS['inserted_rows']}{$HIS['inserted_rows']}{else}0{/if}</td>
              <td>{$HIS['import_date']}</td>       
          </tr>
          {assign var=count value=$count+1}
        {/foreach}        
				</tbody>
			</table>      
{if !$count}<div style="text-align: center;"><h4>Šiandien įkeltų mokėjimų nėra</h4><p>Senesnius mokėjimus galima pasiekti paspaudus ,,Mokėjimų istorija''</p></div>{/if}
  




</div>

