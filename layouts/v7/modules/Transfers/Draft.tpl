<link rel="stylesheet" href="layouts/v7/modules/Transfers/resources/style.css?u=2" type="text/css" />
<script src="layouts/v7/modules/Transfers/resources/main.js?u=15"></script>
{assign var=invoice_arr value=$invoice_arr}
{assign var=recordid value=$record_id}
{assign var=XML_NAME value=$xml_name}
{assign var=post value=$post}

<div id="wait" style="display:none;position:fixed;top:50%;left:45%;padding:2px;"><img src="/resources/loading.gif"></div>
<div id="table-content" class="table-container">
<input type="hidden" id="recordid" value="{$recordid}">  
<input type="hidden" id="submited_post" value="{if empty($post)}0{else}1{/if}">

{if $smarty.cookies.same_invoice == 'has'}
  <div class="alert alert-danger text-center" style="margin-top: 10px;">
    Klaida! Bandoma sudengti tas pačias sąskaitas du kartus - {$smarty.cookies.invoice_no}
  </div>
{/if}

<form action="" method="POST" id="draft_form">
  <input type="hidden" id="cover" name="cover" value="0">
  <input type="hidden" id="xml_name" name="xml_name" value="{$XML_NAME}">
  <table class="table listview-table">
    <thead>
      <tr>
        <th>#</th>
        <th>Mokėtojas</th>
        <th>Data</th>
        <th>Pav. Nr.</th>
        <th>Suma</th>
        <th>Valiuta</th>
        <th>Kursas</th>
        <th>Suma, Eur</th>
        <th>Įskaityta, Eur</th>
        <th>Pastabos</th>
        <th style="width: 80px;">Sąskaita</th>
        <th style="width: 80px;">Data</th>
        <th style="width: 80px;">Viso, Eur</th>
        <th style="width: 80px;">Skola, Eur</th>
        <th style="width: 80px;">Mok. suma, Eur</th>
        <th style="width: 80px;">Likutis, Eur</th>
      </tr>
      		</thead>       
				<tbody> 
            {foreach from=$invoice_arr key=key item=payment_info }  
             <tr data-row_id="{$key+1}">           
              <td>{$key+1}</td>
              <td>
                <div id="{$payment_info['client_info']['id']}" class="list-group">
                    <span {if $payment_info['client_info']['client_name'] neq 'PARNASAS UAB'} data-show="span" onClick="changeClientName(event);"{/if} id="client{$payment_info['client_info']['id']}">{$payment_info['client_info']['client_name']}</span>
                    <input data-show="input" id="client_input{$payment_info['client_info']['id']}" autocomplete="off" style="display:none;">
                </div>      
              </td>
              <td data-date="{$key}">{$payment_info['pay_date']}
              <input type="hidden" name="pay_date{$key+1}" value="{$payment_info['pay_date']}">
              </td>
              <td class="transfer_id">{$payment_info['transfer_num']}
                <input type="hidden" name="transfer_id{$key+1}" value="{$payment_info['transfer_num']}">
              </td>
              <td>{$payment_info['amount']}</td>
              <td>{$payment_info['currency']}</td>
              <td>{$payment_info['rate']}</td>
              <td>{$payment_info['amount_eur']}</td>
              <td>{$payment_info['included']}</td>
              <td style="word-break: break-all;">{$payment_info['comment']}</td>
             {$col = 1}
              {foreach from=$payment_info['invoices_info'] item=invoice}                 
               <td style="{if $col == 1}vertical-align: top;width: 115px;{else if $col == 2}vertical-align: top;width:70px;{else if $col == 3}vertical-align: top;width:80px;{else if $col == 4}vertical-align: top;width:80px;{else if $col == 5}vertical-align: top;width:195px;{else if $col == 6}width:70px;{/if}">             
               
               {if $col < 6}
                <ul class="list-group {if $col eq 3}total{else if $col eq 4}debt{else if $col eq 5}payed{/if}" style="{if $col == 1}min-width: 115px;{else if $col == 2 || $col == 3 || $col == 4 || $col == 5}min-width: 70px;{/if}"> 
                  {foreach from=$invoice['invoice_no'] item=invoice_no key=invoiceid}
                  <li class="list-group-item"> <a class="remove_a_class" target="_blank" href="index.php?module=Invoice&view=Detail&record={$invoiceid}&mode=showDetailViewByMode&requestMode=full&tab_label=Sąskaita%20Išsami%20informacija&app=SALES">{$invoice_no}</a></li>                
                  {/foreach}
          
                  {foreach from=$invoice['invoicedate'] item=invoicedate}
                  <li class="list-group-item not-hover"> {$invoicedate}</li>                
                  {/foreach}
                        
                {$len = 1}
                  {foreach from=$invoice['total'] item=total}
                  <li data-num="{$len}" class="list-group-item not-hover"> {$total}</li>  
                  {assign var=len value=$len+1}                
                  {/foreach}
                {$len2 = 1}
                {foreach from=$invoice['debt'] item=debt key=invid}
                  <li class="list-group-item not-hover debt{$len2}">{$debt}</li>   
                  <input type="hidden" class="debthidden{$len2}" value="{$payment_info['client_info']['hidden_debt'][$invid]}">          
                   {assign var=len2 value=$len2+1}                
                 {/foreach}
        
                {$len3 = 1}
                  {foreach from=$invoice['amount_to_be_paid'] item=amount_to_be_paid key=invoice_id}
                    <li class="list-group-item2" style="display: inline-flex;">  
                     <input                      
                      name="amount{$key+1}-{$len3}"                   
                      class="amount_input"
                      data-num="{$len3}" data-invoiceid="{$invoice_id}"                                        
                      value="{if isset($post["amount`$key+1`-`$len3`"])}{$post["amount`$key+1`-`$len3`"]}{else}{round($amount_to_be_paid['amount'],2)}{/if}" 
                      type="text"                    
                      max="{round($amount_to_be_paid['total'],2)}" 
                      onChange="checkSum({$len3},this)"
                      onkeypress="return isNumber(event);" onpaste="return isNumber(event);" onkeyup="return isNumber(event);">
                      <span data-num="{$len3}" class="fa fa-plus-circle" onClick="plusSum(this);" title="Sudengti visa suma" style="padding:5px;cursor:pointer;font-size: 14px;"></span>
                    </li>
                    <input class="except" type="hidden" name="invoice_id{$key+1}-{$len3}" value="{$invoice_id}">                  
                              
                    {assign var=len3 value=$len3+1}
                  {/foreach}  
                      <input type="hidden" class="except" name="lens{$key+1}" value="{$len3-1}">         
                </ul>  
                {else}               
                <span class="balance">{$invoice['payed']}</span>
                <input class="hidden_balance" type="hidden" value="{$payment_info['client_info']['amount_balance']}">
                {/if}
              </td> 
              {assign var=col value=$col+1}
              {/foreach}              
              </tr>       
            {/foreach} 
            <input type="hidden" name="keys" value="{$key+1}">        
				</tbody>
			</table>  
</form>     
<div style="height: 100px;"></div>



</div>

