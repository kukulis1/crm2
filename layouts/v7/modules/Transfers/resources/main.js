$(document).ready(function(){

if(document.getElementById('alert_message') != null){
  const name = $('#alert_message').data('import');
  removeAlert(5000,name);
}

$('[name="submit"]').on('click', function(){
  $('#wait').show();
  $('#table-content').css('opacity', '0.2');
});

$('#upload_payments').on('click', function(){
  if(confirm("Ar tikrai norite įkelti mokėjimus?")){
    // payInvoices();
    $('#cover').val(1);
    setTimeout(() => {
      $('#draft_form').submit();
    }, 500);
  }
});

$('body').on('click', function (e) {
  if ($(e.target).data('show') !== 'span' && $(e.target).data('show') !== 'input') { 
      $('[data-show="input"]').hide();
      $('[data-show="span"]').show();
  }
});


let pageId = new URLSearchParams(window.location.search);

if (pageId.get('view') == 'Draft') {
	jQuery('.app-trigger, .app-icon, .app-navigator').on('click', function (e) {
		e.stopPropagation();
		if (checkHide()) {
			toggleAppMenu('show');
		} else {
			toggleAppMenu('hide');
		}
	});

	jQuery('body').on('click', function (e) {
		if (!checkHide()) {
			toggleAppMenu('hide');
		}
	});

}

  function checkHide() {
    return document.querySelector('.app-menu.hide');
  }

  function toggleAppMenu(type) {
    var appMenu = jQuery('.app-menu');
    var appNav = jQuery('.app-nav');
    appMenu.appendTo('#page');
    appMenu.css({
      'top': appNav.offset().top + appNav.height(),
      'left': 0
    });
    if (typeof type === 'undefined') {
      type = appMenu.is(':hidden') ? 'show' : 'hide';
    }
    if (type == 'show') {
      document.querySelector('.app-menu').classList.remove('hide');
      appMenu.show(200, function () { });
    } else {
      document.querySelector('.app-menu').classList.add('hide');
      appMenu.hide(200, function () { });
    }
  };


  jQuery('.app-modules-dropdown-container').hover(function (e) {
    var dropdownContainer = jQuery(e.currentTarget);
    jQuery('.dropdown').removeClass('open');
    if (dropdownContainer.length) {
      if (dropdownContainer.hasClass('dropdown-compact')) {
        dropdownContainer.find('.app-modules-dropdown').css('top', dropdownContainer.position().top - 8);
      } else {
        dropdownContainer.find('.app-modules-dropdown').css('top', '');
      }
      dropdownContainer.addClass('open').find('.app-item').addClass('active-app-item');
    }
  }, function (e) {
    var dropdownContainer = jQuery(e.currentTarget);
    dropdownContainer.find('.app-item').removeClass('active-app-item');
    setTimeout(function () {
      if (dropdownContainer.find('.app-modules-dropdown').length && !dropdownContainer.find('.app-modules-dropdown').is(':hover') && !dropdownContainer.is(':hover')) {
        dropdownContainer.removeClass('open');
      }
    }, 500);

  });

  jQuery('.app-item').on('click', function () {
    var url = jQuery(this).data('defaultUrl');
    if (url) {
      window.location.href = url;
    }
  });

  if($('#submited_post').val() == 1){
    $('.amount_input').each(function(){
      $(this).trigger('change');
    });
  }
});


function removeAlert(sec,name){
  eraseCookie(name);
  setTimeout(() => {
    $('#alert_message').hide('slow');  
  }, sec);
}


function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=0'
}

function checkSum(id, e){
  const max = parseFloat(e.max).toFixed(2);
  let amount = $(e).val();
  if(amount == ''){
    amount = 0;
    $(e).val(amount);
  } 
  
  const parent = $(e).parent().parent().parent().parent();
  const debt = parent.find(`.debt .debt${id}`);
  const balance = parent.find('.balance');
  const hiddenBalance = Number(parent.find('.hidden_balance').val());
  const debthidden = parent.find(`.debt .debthidden${id}`).val();

  let amountArr = new Array();  

  parent.find('.payed input:not(.except)').each(function(index,input){
    amountArr.push(Number(input.value));
  });
  console.log(amountArr);

  let amountSum = amountArr.map(Number).reduce(function (a, b) { return a + b; });
  amountSum = amountSum.toFixed(2);



  let checkBalance = hiddenBalance - amountSum; 

 
  if(Number(checkBalance) < 0){
    $(e).addClass('alert-warning-input'); 
    // let bal = Number(balance.html());
   
    amountSum = amountSum - amount;   
    let getBalance = hiddenBalance - amountSum; 

    // amount = parseFloat(bal + getBalance).toFixed(2);
          
    $(e).val(parseFloat(getBalance).toFixed(2));
  }

  if(Number(amount) > max){   
    $(e).addClass('alert-danger-input');       
    $(e).val(max);
    setTimeout(() => {   
      debt.html(max - amount);     
      $(e).removeClass('alert-danger-input');
    }, 300);

    amount = max;
  }

  let payedArr = new Array();  

  parent.find('.payed input:not(.except)').each(function(index,input){
    payedArr.push(Number(input.value));
  });

 let payed = payedArr.map(Number).reduce(function (a, b) { return a + b; });




 let newDebt = Number(debthidden) - Number(amount);
 if(newDebt < 0) newDebt = 0;
//  debt.html(Number(newDebt).toFixed(2));

 let newBalance = hiddenBalance - payed;



  if (Number(newBalance).toFixed(2) < 0){
    balance.html(0);
  }else{
    if(Number(newBalance).toFixed(2) == '-0.00'){
      balance.html(0);
    }else{
      balance.html(Number(newBalance).toFixed(2));
    }
  }

}

function plusSum(event){  
  let parent = $(event).parent();
  let max = parent.find('input').attr('max');
  parent.find('input').val(max); 
  let input = $(event).parent().find('input').get(0);
  let id = $(event).data('num'); 
  checkSum(id, input);   
}

function saveDraft(){
  let recordid = $('#recordid').val();
  let rows = new Array(); 

  $('tbody tr').each(function(i,element){
    let values = new Array();      
    let invoiceid = new Array();      
    $(element).find('.payed input').each(function(index,input){
      values.push(Number(input.value));   
      invoiceid.push($(input).data('invoiceid'));   
    });
    rows.push({id: $(element).data('row_id'), val: values,invoiceid:invoiceid});
  });

  $.ajax({
		type: "POST",
		url: "modules/Transfers/ajax/saveDraft.php",
		data: { recordid: recordid, rows: rows },
    beforeSend: function () {
      $('#wait').show();
      $('#table-content').css('opacity', '0.2');
    },
		success: function (result) {
      window.location.href = 'index.php?module=Transfers&view=Draft&record='+recordid;     
		}
	});  
}

function saveDraftToDb(transfer,info){
  $.ajax({
		type: "POST",
		url: "modules/Transfers/ajax/transfers.php",
		data: { transfer: transfer, info: info },
    dataType: "JSON",
		success: function (result) {
      // console.log(result);
		}
	});
}


function payInvoices(){
  let recordid = $('#recordid').val();
  let date = $('[data-date="0"]').html();
  let invoiceid = new Array();
  let total = new Array();
  let transfer = new Array();  


  $('[data-invoiceid]').each(function(index,input){    
    if(input.value != '' && input.value > 0){
      let transferno = $(input).parents().eq(3).find('.transfer_id').html();
      let invoice = $(input).data('invoiceid');

      total.push(input.value);
      invoiceid.push(invoice);
      transfer.push(transferno);
    }
  });


  // console.log(invoiceid);
  // console.log(transfer);

  total = total.join("|");
  invoiceid = invoiceid.join(",");
  const debtno = $('#recordid').val();
  const xml_name = $('#xml_name').val();
  const user_id = _USERMETA.id;


  if(total != ''){
    $.ajax({
      type: "POST",
      url: 'modules/Transfers/ajax/payInvoices.php',
      data: { invoiceid: invoiceid, total:total, date: date, transfer:transfer,debtno:debtno, xml_name:xml_name,user_id:user_id },
      dataType: 'JSON',
      // beforeSend: function () {
      //   $('#wait').show();
      //   $('#table-content').css('opacity', '0.2');
      // },
      success: function (res) {    
        if (res.status == 'success') {
          window.location.href = 'index.php?module=Transfers&view=Draft&record='+recordid;     
        } else {
          alert('Įvyko klaida, praneškite puslapio administratoriui.');
        }
      }
    });
  }
}


function changeClientName(e){
  let id = e.target.parentElement.id;
  let clientSpan = document.getElementById(`client${id}`)
  let clientInput = document.getElementById(`client_input${id}`)
  clientSpan.setAttribute('style','display: none;')
  clientInput.removeAttribute('style');
  getClientNamesSuggestions(id);
}


function getClientNamesSuggestions(id){
  $.ajax({ 
    type: "POST",
    url: "modules/Transfers/ajax/suggestions.php",
    data: { 1: 1 }, 
    dataType : 'JSON',
    success: function(clients){ 
      findClientAndGetId(document.getElementById(`client_input${id}`), clients.client, id);                         
    }            
  });
}

function updateClient(accountid,accountname,rowid){
  $.ajax({ 
    type: "POST",
    url: "modules/Transfers/ajax/updateAccountId.php",
    data: { accountid: accountid,accountname:accountname,rowid:rowid }, 
    success: function(rez){ 
      if(rez == 'true'){
        // location.reload();
        $('#draft_form').submit();
      }
    }            
  });
}


function findClientAndGetId(inp, arr, rowid) {
   /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
	var currentFocus;
	let accountname = arr.accountname;
	let accountid = arr.accountid;

	/*execute a function when someone writes in the text field:*/
	inp.addEventListener("input", function (e) {
		$('#pricebook_name').removeClass('selected2');
		if (inp.value == '') {
			$('#client_block').remove();
			$('#accountid').val('');
		}

		var a, b, i, val = this.value;
		/*close any already open lists of autocompleted values*/
		closeAllLists();

		if (val.length >= 3) {

			if (!val) { return false; }
			currentFocus = -1;
			/*create a DIV element that will contain the items (values):*/
			a = document.createElement("DIV");
			a.setAttribute("id", this.id + "autocomplete-list");

			a.setAttribute("class", "autocomplete-items-employee");

			/*append the DIV element as a child of the autocomplete container:*/
			this.parentNode.appendChild(a);
			/*for each item in the array...*/
			for (i = 0; i < accountname.length; i++) {
				/*check if the item starts with the same letters as the text field value:*/
				if (((accountname[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
					// if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
					/*create a DIV element for each matching element:*/
					b = document.createElement("DIV");
					b.setAttribute('class', 'listas');
					/*make the matching letters bold:*/
					b.innerHTML = accountname[i].substr(0, val.length);
					b.innerHTML += accountname[i].substr(val.length);
					/*insert a input field that will hold the current array item's value:*/
					b.innerHTML += "<input type='hidden' value='" + accountname[i] + "'>";
					/*execute a function when someone clicks on the item value (DIV element):*/
					b.addEventListener("click", function (e) {
						/*insert the value for the autocomplete text field:*/
						inp.value = this.getElementsByTagName("input")[0].value;
						/*close the list of autocompleted values,
						(or any other open lists of autocompleted values:*/
						closeAllLists();
					});

					accid = document.createElement("input");
					accid.setAttribute('class', 'accid');
					accid.setAttribute('type', 'hidden');
					accid.setAttribute('value', `${accountid[i]}`);

					a.appendChild(b);
					b.appendChild(accid);
					let accidClass = document.querySelectorAll('.accid');
					let listas = document.querySelectorAll('.listas');

					for (let i = 0; i < listas.length; i++) {
						listas[i].onclick = () => {
							// inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
							inp.classList.add('selected2');
							if (accountid !== undefined) {	
                				$(`#client${rowid}`).text(inp.value);	                							
								updateClient(accidClass[i].value,inp.value,rowid);
							}
						}
					}

				}
				inp.onchange = () => {
					if (!inp.classList.contains('selected2')) {
						// inp.value = ''; jei nieko nepasirinkama is saraso istustina laukeli
					}
				}
			}
		}
	});




	/*execute a function presses a key on the keyboard:*/
	inp.addEventListener("keydown", function (e) {
		var x = document.getElementById(this.id + "autocomplete-list");
		if (x) x = x.getElementsByTagName("div");
		if (e.keyCode == 40) {
			/*If the arrow DOWN key is pressed,
			increase the currentFocus variable:*/
			currentFocus++;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 38) { //up
			/*If the arrow UP key is pressed,
			decrease the currentFocus variable:*/
			currentFocus--;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 13) {
			/*If the ENTER key is pressed, prevent the form from being submitted,*/
			e.preventDefault();
			if (currentFocus > -1) {
				/*and simulate a click on the "active" item:*/
				if (x) x[currentFocus].click();
			}
		}
	});
	function addActive(x) {
		/*a function to classify an item as "active":*/
		if (!x) return false;
		/*start by removing the "active" class on all items:*/
		removeActive(x);
		if (currentFocus >= x.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (x.length - 1);
		/*add class "autocomplete-active":*/
		x[currentFocus].classList.add("autocomplete-active");
	}
	function removeActive(x) {
		/*a function to remove the "active" class from all autocomplete items:*/
		for (var i = 0; i < x.length; i++) {
			x[i].classList.remove("autocomplete-active");
		}
	}
	function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
		var x = document.getElementsByClassName("autocomplete-items-employee");
		for (var i = 0; i < x.length; i++) {
			if (elmnt != x[i] && elmnt != inp) {
				x[i].parentNode.removeChild(x[i]);
			}
		}
	}
	/*execute a function when someone clicks in the document:*/
	document.addEventListener("click", function (e) {
		closeAllLists(e.target);

	});
}


function isNumber(evt) {  
  let charCode = (evt.which) ? evt.which : evt.keyCode;
  evt.target.value = evt.target.value.replace(/,/g, '.');


  if (charCode != 46 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  } else {
    //if dot sign entered more than once then don't allow to enter dot sign again. 46 is the code for dot sign
    let parts = evt.srcElement.value.split('.');

    if (parts.length > 1 && (charCode == 46 || charCode == 44)) {
      return false;
    }
    return true;
  }
}