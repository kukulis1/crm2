<link rel="stylesheet" href="layouts/v7/modules/Transfers/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Transfers/resources/main.js?u=14"></script>

{assign var=getHistory value=$getHistory}
{assign var=PAGINATION value=$pagination}

<div id="table-content" class="table-container">
    
    	<table class="table listview-table">
      	<thead>
      <tr>
        <th>#</th>
        <th>Failas</th>
        <th>Eilutės</th>
        <th>Įkelta eilučių</th>
        <th>Įkėlimo data</th>
      </tr>
      		</thead>       
				<tbody id="list">
        {$count = 0}
        {foreach from=$getHistory item=HIS key=key}       
          <tr onclick="window.location.href = 'index.php?module=Transfers&view=Draft&record={$HIS['id']}'" style="cursor: pointer;">
              <td>{$HIS['id']}</td>
              <td>{$HIS['filename']}</td>
              <td>{if $HIS['rows']}{$HIS['rows']}{else}0{/if}</td>
              <td>{if $HIS['inserted_rows']}{$HIS['inserted_rows']}{else}0{/if}</td>
              <td>{$HIS['import_date']}</td>       
          </tr>   
            {assign var=count value=$count+1}
        {/foreach}        
				</tbody>
			</table>      
{if !$count}<div style="text-align: center;"><h4>Nėra mokėjimų</h4></div>{/if}
  
  {$pagination}




</div>

