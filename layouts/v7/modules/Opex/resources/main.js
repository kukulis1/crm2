$(document).ready(function () {

  let year = $('[name="years"]').val();
  let vat = $('[name="vat"]').val();
  $('#excel').attr('onclick', `window.location.href='index.php?module=Opex&view=Excel&year=${year}&vat=${vat}'`);
  $("#update_opex_stats").attr('onclick', `window.location.href='opex/updateStats.php?year=${year}&vat=${vat}'`);

  $('.opex-table tbody td:first-child .fa-pencil:not(.select_invoice_cost)').on('click', function (e) {
    let name = e.target.id;
    let title = e.target.parentElement.parentElement.parentElement.getElementsByTagName('td')[1].innerHTML;
    let costCenter = e.target.parentElement.parentElement.parentElement.getElementsByTagName('td')[2].dataset.type;
    $('#opex_cost_listLabel').html(title);
    $('#cost_name').val(name);
    opexCostPickList(name, costCenter);
  });

  $('.opex-table tbody td:first-child .select_invoice_cost').on('click', function (e) {
    let name = e.target.id;
    let title = e.target.parentElement.parentElement.parentElement.getElementsByTagName('td')[1].innerHTML;

    if (e.target.dataset.which == 'invoice') {
      opexInvoiceForwardingCostPickList(name);
      $('#opex_invoice_forwarding_cost_listLabel').html(title);
      $('#invoice_forwarding_cost_name').val(name);
    }
  });

  $("#update_opex_stats").popover({ trigger: "hover",html:true });
  $("#update_opex_stats_with_service").popover({ trigger: "hover",html:true });

  $('.opex-table tbody td:not(:first-child)').not('.independent').on('click', function (e) {
    let purchaseorderid = $(this).data('purchaseorderid');
    let service_key = $(this).data('service_key');
    let type = $(this).data('type');
    let invoiceid = $(this).data('invoiceid');
    let cost = $(this).data('cost');
    let service = $(this).data('service');
    let month = $(this).data('month');
    let year = $(this).data('year');

    let title = e.target.parentElement.getElementsByTagName('td')[1].innerHTML;
    let other_value = e.target.innerHTML;

    if (purchaseorderid != undefined && purchaseorderid != '') {
      $('#detailLinesLabel').html(title);
      getPurchaseOrderInfo(purchaseorderid, service_key, type);
    } else if (invoiceid != undefined && invoiceid != '' && cost != undefined) {
      $('#detailLinesLabel').html(title);
      getInvoiceInfo(invoiceid, cost, type);
    } else if (invoiceid != undefined && invoiceid != '' && service != undefined) {
      $('#detailLinesLabel').html(title);
      getInvoiceInfoService(invoiceid, service, type);
    } else if (month != undefined && month != '' && year != undefined && year != '' && other_value != '') {
      $('#detailLinesLabel').html(title);
      getInvoiceProfitWithOutListInfo(year, month);
       initMoreBox();
    } else if (month != undefined && month != '' && other_value != '') {
      $('#detailLinesLabel').html(title);
      getOtherPurchaseOrderInfo(month);
    }
  });

  function initMoreBox() {
    setTimeout(() => {
      $('.more').on('click', function () {
        let invoiceid = $(this).data('invoiceid');
        let sequence_no = $(this).data('row');
        getInvoiceProfitWithOutListInfoMore(invoiceid, sequence_no);
      });
    }, 1000);
  }

  $('#update_opex_stats').on('click', function () {
    $('#wait').show();
    $('#table-content').css('opacity', '0.2');
  });


  $('#update_opex_years_stats').on('click', function () {
    if (confirm("Ar tikrai norite atnaujinti visų metų pirkimų ataskaitą?") == true) {
      window.location.href = `opex/updateOpex.php?year=${year}`;
      console.log(year);
      $('#wait').show();
      $('#table-content').css('opacity', '0.2');
    }
  });

  $('#update_opex_years_stats_invoice').on('click', function () {
    if (confirm("Ar tikrai norite atnaujinti visų metų pardavimų ataskaitą?") == true) {
      window.location.href = `opex/updateOpexInvoices.php?year=${year}`;
      console.log(year);
      $('#wait').show();
      $('#table-content').css('opacity', '0.2');
    }
  });


  $('.update_month').on('click', function () {
    let month = $(this).data('month');  
    window.location.href = `opex/updateOpex.php?year=${year}&month=${month}&vat=${vat}`;
    $('#wait').show();
    $('#table-content').css('opacity', '0.2');
  });

  $('#update_opex_stats_with_service').on('click', function () {
    window.location.href = `opex/assignUncategorizedRows.php?year=${year}&vat=${vat}`;
    $('#wait').show();
    $('#table-content').css('opacity', '0.2');
  });

  // $(function () {
  //   $('[data-toggle="popover"]').popover(
  //     {
  //       container: 'body',
  //       html: true
  //     }
  //   )
  // });

  // $('body').on('click', function (e) {
  //   //did not click a popover toggle or popover
  //   if ($(e.target).data('toggle') !== 'popover'
  //     && $(e.target).parents('.popover.in').length === 0) {
  //     $('[data-toggle="popover"]').popover('hide');
  //   }
  // });

  $('[data-toggle="popover"]').popover({ trigger: "hover", html: true });

  $('.collapse_expand').on('click', function () {
    $('.expand_item').collapse('toggle');
  });

  $('.clickable td:not(.not-clickable) span').on('click',function(){
    var sum = $.trim($(this).text());
    $(this).parent().find('.hiddenInfoInput').val(sum);
    $(this).parent().find('.hiddenInputBlock').show();
    $(this).hide();
  });
});

function btnOk(event){  
  let parent = $(event.target).parent().parent();
  let number = parent.find('input').val();
  let title = parent.parent().parent().data('row-title');
  let id = parent.parent().data('row-id');
  let year = $('[name="years"]').val();

  $.ajax({
    type: "POST",
    url: "opex/writeRows.php",
    data: {number:number,title:title,id:id,year:year},
    dataType: "JSON",
    beforeSend: function(){
      parent.hide();
      parent.parent().find('img').show();
    },
    success: function (response) {
        console.log(response);
        if(response.status == 'success'){
          setTimeout(() => {
            parent.parent().find('img').hide();
            parent.parent().find('span').html(number);
            parent.parent().find('span').show();
          }, 500);
        }else{
          alert('Įvyko klaida. Praneškite puslapio administratoriui');
        }
    }
  });   
}

function btnCancel(event){
  $(event.target).parent().hide();
  $(event.target).parent().parent().find('span').show();
}



function opexCostPickList(name, costCenter) {
  let detailLinesContent = document.querySelector('#detailLinesOpexContent');
  let detailLinesContent2 = document.querySelector('#detailLinesOpexContent2');

  let costsCenterName = new Array('---', 'ADMINISTRACIJA', 'KAUNO SANDĖLIS', 'KLAIPĖDOS SANDĖLIS', 'VILNIAUS SANDĖLIS', 'PERKRAUSTYMAS', 'SERVISAS', 'EKSPEDIJAVIMAS', 'TRANSPORTAS');
  let costCenterId = new Array('', 103921, 103920, 103919, 103918, 103917, 103916, 103915, 103914);

  $('#cost_center_id').val(costCenter);
  let type = 'All';
  let html = '';
  let html2 = '';
  $.ajax({
    type: "POST",
    url: "opex/opexCostPickList.php",
    data: { name: name, type: type },
    dataType: 'JSON',
    success: function (result) {

      if (result.list.cost_center != null) {
        let costCenterList = result.list.cost_center;
        costCenterList = costCenterList.split(",");
        html += `        
        <table class="table" id="formTable"><tbody id="formTbody">`;
        for (let index = 0; index < costCenterList.length; index++) {
          html += `  <tr>
                  <td class="listViewEntryValue">
                    <select class="inputElement" name="invoice_cost${index + 1}">`;
          costsCenterName.forEach((row, id) => {
            html += `<option value="${costCenterId[id]}" ${(costCenterId[id] == costCenterList[index] ? 'selected' : '')}>${row}</option>`;
          });
          html += `</select></td>               
                </tr>`;
        }
        html += `</tbody></table>`;
        detailLinesContent.innerHTML = html;
      } else {
        html += `<table class="table" id="formTable"><tbody id="formTbody">`;
        html += `  <tr>
          <td class="listViewEntryValue">
            <select class="inputElement" name="invoice_cost1">`;
        costsCenterName.forEach((row, id) => {
          html += `<option value="${costCenterId[id]}">${row}</option>`;
        });
        html += `</select></td></tr></tbody></table>`;
        detailLinesContent.innerHTML = html;
      }

      if (result.list.cost != null) {
        let costList = result.list.cost;
        costList = costList.split(",");
        html2 += `        
            <table class="table" id="formTable2"><tbody id="formTbody2">`;
        for (let index = 0; index < costList.length; index++) {
          html2 += `  <tr>
                <td class="listViewEntryValue">
                  <select class="inputElement" name="cost${index + 1}">`;
          result.costs.forEach(row => {
            html2 += `<option value="${row.itemserviceid}" ${(row.itemserviceid == costList[index] ? 'selected' : '')}>${row.itemservice_name}</option>`;
          });
          html2 += `</select></td>
              <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event,'Purchase');"></i></td>
              </tr>`;
        }
        html2 += `</tbody></table>`;
        detailLinesContent2.innerHTML = html2;
      } else {
        html2 += `<table class="table" id="formTable2"><tbody id="formTbody2"></tbody></table>`;
        detailLinesContent2.innerHTML = html2;
        addOneMoreField('Purchase', 'formTable2', 'formTbody2');
      }
    }
  });

  $('#opex_cost_list').modal('show');
}

// function opexInvoiceCostPickList(name) {
//   let detailLinesContent = document.querySelector('#detailLinesOpexInvoiceContent');

//   let type = 'All';
//   let html = '';
//   $.ajax({
//     type: "POST",
//     url: "opex/opexInvoiceCostPickList.php",
//     data: { name: name, type: type },
//     dataType: 'JSON',
//     success: function (result) {
//       if (result.list.cost != null) {
//         let costList = result.list.cost;
//         costList = costList.split(",");
//         html += `        
//               <table class="table" id="formTableInvoice"><tbody id="formTbodyInvoice">`;
//         for (let index = 0; index < costList.length; index++) {
//           html += `  <tr>
//                   <td class="listViewEntryValue">
//                     <select class="inputElement" name="invoice_cost${index + 1}">`;
//           result.costs.forEach(row => {
//             html += `<option value="${row.itemserviceid}" ${(row.itemserviceid == costList[index] ? 'selected' : '')}>${row.itemservice_name}</option>`;
//           });

//           html += `</select></td>
//                 <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
//                 </tr>`;
//         }
//         html += `</tbody></table>`;

//         detailLinesContent.innerHTML = html;
//       } else {
//         html += `<table class="table" id="formTableInvoice"><tbody id="formTbodyInvoice">`;
//         detailLinesContent.innerHTML = html;
//       }
//     }
//   });

//   $('#opex_invoice_cost_list').modal('show');
// }

function opexInvoiceForwardingCostPickList(name) {
  let detailLinesContent = document.querySelector('#detailLinesOpexInvoiceForwardingContent');
  let detailLinesContent2 = document.querySelector('#detailLinesOpexInvoiceForwardingContent2');

  let costsCenterName = new Array('---', 'ADMINISTRACIJA', 'KAUNO SANDĖLIS', 'KLAIPĖDOS SANDĖLIS', 'VILNIAUS SANDĖLIS', 'PERKRAUSTYMAS', 'SERVISAS', 'EKSPEDIJAVIMAS', 'TRANSPORTAS');
  let costCenterId = new Array('', 103921, 103920, 103919, 103918, 103917, 103916, 103915, 103914);

  let type = 'All';
  let html = '';
  let html2 = '';

  $.ajax({
    type: "POST",
    url: "opex/checkCostcenterWithServices.php",
    data: { name: name, type: type },
    dataType: "JSON",
    success: function (result) {

      if (result.list.cost_center != null) {
        let costCenterList = result.list.cost_center;
        costCenterList = costCenterList.split(",");

        html += `        
        <table class="table" id="formTableInvoice"><tbody id="formTbodyInvoiceForwarding">`;
        for (let index = 0; index < costCenterList.length; index++) {
          html += `  <tr>
                  <td class="listViewEntryValue">
                    <select class="inputElement" name="invoice_cost${index + 1}">`;
          costsCenterName.forEach((row, id) => {
            html += `<option value="${costCenterId[id]}" ${(costCenterId[id] == costCenterList[index] ? 'selected' : '')}>${row}</option>`;
          });

          html += `</select></td>
                <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
                </tr>`;
        }
        html += `</tbody></table>`;
        detailLinesContent.innerHTML = html;
      } else {
        html += `<table class="table" id="formTableInvoice"><tbody id="formTbodyInvoiceForwarding">`;
        html += `  <tr>
          <td class="listViewEntryValue">
            <select class="inputElement" name="invoice_cost1">`;
        costsCenterName.forEach((row, id) => {
          html += `<option value="${costCenterId[id]}">${row}</option>`;
        });
        html += `</select></td></tr></tbody></table>`;
        detailLinesContent.innerHTML = html;
      }

      if (result.list.cost != null) {
        let costList = result.list.cost;
        costList = costList.split(",");
        html2 += `        
        <table class="table" id="formTableInvoice"><tbody id="formTbodyInvoiceForwarding2">`;
        for (let index = 0; index < costList.length; index++) {
          html2 += `  <tr>
                <td class="listViewEntryValue" style="width: 89%;">
                  <select class="inputElement" name="invoice_cost${index + 1}">`;
          result.costs.forEach(row => {
            html2 += `<option value="${row.id}" ${(row.id == costList[index] ? 'selected' : '')}>${row.name}</option>`;
          });

          html2 += `</select></td>
              <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event,'Invoice');"></i></td>
              </tr>`;
        }
        html2 += `</tbody></table>`;
        detailLinesContent2.innerHTML = html2;
      } else {
        html2 += `<table class="table" id="formTableInvoice"><tbody id="formTbodyInvoiceForwarding2"></tbody></table>`;
        detailLinesContent2.innerHTML = html2;
        addOneMoreField('Invoice', 'formTbodyInvoiceForwarding2', 'formTbodyInvoiceForwarding2');
      }

    }
  });
  $('#opex_invoice_forwarding_cost_list').modal('show');
}



// function opexInvoiceStorageCostPickList(name) {
//   let detailLinesContent = document.querySelector('#detailLinesOpexInvoiceStorageContent');
//   let detailLinesContent2 = document.querySelector('#detailLinesOpexInvoiceStorageContent2');

//   let type = 'All';
//   $.ajax({
//     type: "POST",
//     url: "opex/opexInvoiceStorageCostPickList.php",
//     data: { name: name, type: type },
//     dataType: 'JSON',
//     success: function (result) {
//       if (result.list.cost != null) {
//         let costList = result.list.cost;
//         costList = costList.split(",");

//         let costList2 = result.list2.cost;
//         costList2 = costList2.split(",");

//         let html = '';
//         let html2 = '';
//         html += `        
//             <table class="table" id="formTableInvoice"><tbody id="formTbodyInvoiceStorage">`;
//         for (let index = 0; index < costList.length; index++) {
//           html += `  <tr>
//                 <td class="listViewEntryValue" style="width: 89%;">
//                   <select class="inputElement" name="invoice_cost${index + 1}">`;
//           result.costs.forEach(row => {
//             html += `<option value="${row.itemserviceid}" ${(row.itemserviceid == costList[index] ? 'selected' : '')}>${row.itemservice_name}</option>`;
//           });

//           html += `</select></td>
//               <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
//               </tr>`;
//         }
//         html += `</tbody></table>`;

//         html2 += `        
//         <table class="table" id="formTableInvoice"><tbody id="formTbodyInvoiceStorage2">`;
//         for (let index = 0; index < costList2.length; index++) {
//           html2 += `  <tr>
//                   <td class="listViewEntryValue">
//                     <select class="inputElement" name="invoice_cost${index + 1}">`;
//           result.costs2.forEach(row => {
//             html2 += `<option value="${row.id}" ${(row.id == costList2[index] ? 'selected' : '')}>${row.name}</option>`;
//           });

//           html2 += `</select></td>
//                 <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
//                 </tr>`;
//         }
//         html2 += `</tbody></table>`;

//         detailLinesContent.innerHTML = html;
//         detailLinesContent2.innerHTML = html2;
//       }
//     }
//   });

//   $('#opex_invoice_storage_cost_list').modal('show');
// }


function deleteField(e,which = false) { 
  if(confirm('Ar tikrai norite pašalinti eilutę?')){
    let serviceid = e.target.parentElement.parentElement.querySelector('.inputElement').value;
    let costcenterid = e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.querySelector('.inputElement').value;
    let costcenter = e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.querySelector('.inputElement');
    let costcentername =  costcenter.options[costcenter.selectedIndex].text;  

    if(which == 'Invoice'){
      moveToUnsigned(which,e,serviceid,costcenterid,costcentername);      
    }else if(which == 'Purchase'){
      moveToUnsigned(which,e,serviceid,costcenterid,costcentername);   
    }else{
      e.target.parentElement.parentElement.remove();      
    }    
  }
}

function moveToUnsigned(which,e,serviceid,costcenterid,costcentername){
  console.log(which,serviceid,costcenterid,costcentername);
  $.ajax({
    type: "POST",
    url: "opex/moveToUnsigned.php",
    data: {serviceid:serviceid,costcenterid:costcenterid,costcentername:costcentername,which:which},       
    success: function (response) {          
      if(response == 'success'){
        e.target.parentElement.parentElement.remove();
      } 
    }
  });
}

function addOneMoreField(which, table, body) {
  let type = 'One';
  let selectCount = document.querySelectorAll(`#${table} select`).length;
  let tbody = document.getElementById(body);
  let tr = document.createElement('tr');
  let td = document.createElement('td');
  let td2 = document.createElement('td');
  td.classList.add('listViewEntryValue');
  td2.classList.add('listViewEntryValue');
  let select = document.createElement('select');
  select.classList.add('inputElement');
  select.setAttribute('name', `cost${selectCount + 1}`);

  let url = (which == 'Invoice' ? 'opexInvoiceCostPickList.php' : 'opexCostPickList.php');
  select.innerHTML += `<option value="">---</option>`;

  $.ajax({
    type: "POST",
    url: `opex/${url}`,
    data: { type: type },
    dataType: 'JSON',
    success: function (result) {
      result.forEach(row => {
        select.innerHTML += `<option value="${row.itemserviceid}">${row.itemservice_name}</option>`;
      });
    }
  });

  td2.innerHTML = `<i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event,'${which}');"></i>`;
  td.append(select);
  tr.append(td);
  tr.append(td2);
  tbody.append(tr);
}

function addOneMoreCostCenter() {
  let detailLinesContent = document.querySelector('#formTbodyInvoiceForwarding');

  let costsCenterName = new Array('ADMINISTRACIJA', 'KAUNO SANDĖLIS', 'KLAIPĖDOS SANDĖLIS', 'VILNIAUS SANDĖLIS', 'PERKRAUSTYMAS', 'SERVISAS', 'EKSPEDIJAVIMAS', 'TRANSPORTAS');
  let costCenterId = new Array(103921, 103920, 103919, 103918, 103917, 103916, 103915, 103914);

  let index = document.querySelectorAll('#formTbodyInvoiceForwarding select').length;
  let html = '';
  html += `<tr>
            <td class="listViewEntryValue">
              <select class="inputElement" name="invoice_cost${index + 1}">`;
  costsCenterName.forEach((row, id) => {
    html += `<option value="${costCenterId[id]}">${row}</option>`;
  });
  html += `</select></td>
            <td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
            </tr>`;
  detailLinesContent.innerHTML += html;
}

// function saveInvoiceStorageCostFields() {
//   let selectCount = document.querySelectorAll('#detailLinesOpexInvoiceStorageContent select').length;
//   let selectValue = document.querySelectorAll('#detailLinesOpexInvoiceStorageContent select');

//   let selectCount2 = document.querySelectorAll('#detailLinesOpexInvoiceStorageContent2 select').length;
//   let selectValue2 = document.querySelectorAll('#detailLinesOpexInvoiceStorageContent2 select');


//   let costName = $('#invoice_storage_cost_name').val();
//   let numbersArr = new Array();
//   let numbersArr2 = new Array();
//   for (let i = 0; i < selectCount; i++) {
//     numbersArr.push(selectValue[i].value);
//   }

//   for (let i = 0; i < selectCount2; i++) {
//     numbersArr2.push(selectValue2[i].value);
//   }

//   $.ajax({
//     type: "POST",
//     url: "opex/opexInvoiceStorageCostListSave.php",
//     data: { costName: costName, numbersArr: numbersArr, numbersArr2: numbersArr2 },
//     dataType: 'JSON',
//     success: function (result) {
//       if (result == 'success') {
//         $('.dismiss').click();
//       } else if (result == 'Fail') {
//         console.log(result);
//       }
//     }
//   });
// }

function saveInvoiceForwardingCostFields() {
  let selectCount = document.querySelectorAll('#detailLinesOpexInvoiceForwardingContent select').length;
  let selectValue = document.querySelectorAll('#detailLinesOpexInvoiceForwardingContent select');

  let selectCount2 = document.querySelectorAll('#detailLinesOpexInvoiceForwardingContent2 select').length;
  let selectValue2 = document.querySelectorAll('#detailLinesOpexInvoiceForwardingContent2 select');
  let tr = document.querySelectorAll('#formTbodyInvoiceForwarding2 tr');

  let costName = $('#invoice_forwarding_cost_name').val();
  let title = $('#opex_invoice_forwarding_cost_listLabel').html();
  let numbersArr = new Array();
  let numbersArr2 = new Array();
  for (let i = 0; i < selectCount; i++) {
    numbersArr.push(selectValue[i].value);
  }

  for (let i = 0; i < selectCount2; i++) {
    numbersArr2.push(selectValue2[i].value);
  }

  $.ajax({
    type: "POST",
    url: "opex/opexInvoiceForwardingCostListSave.php",
    data: { costName: costName, numbersArr: numbersArr, numbersArr2: numbersArr2, title: title },
    dataType: 'JSON',
    success: function (result) {
      if (result == 'success') {
        $('.dismiss').click();
      } else if (result == 'Fail') {
        console.log(result);
      }else{
        console.log(result.line);
        selectValue2[result.line].setAttribute('class', 'alert alert-danger');
        selectValue2[result.line].setAttribute('style', 'width: 100%;');
        if (!tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0]) {
          let message = document.createElement('p');
          message.setAttribute('style', 'font-size: 13px; color: #a94442;');
          message.innerHTML = `Pajamos jau yra priskirtos prie skilties: ${result.exist}`;
          tr[result.line].getElementsByTagName('td')[0].append(message);
          setTimeout(() => {
            tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0].remove();
            selectValue2[result.line].setAttribute('class', 'inputElement');
          }, 2000);
        }
      }
    }
  });
}


// function saveInvoiceCostFields() {
//   let selectCount = document.querySelectorAll('#formTableInvoice select').length;
//   let selectValue = document.querySelectorAll('#formTableInvoice select');
//   let tr = document.querySelectorAll('#formTable tr');
//   let costName = $('#invoice_cost_name').val();
//   let numbersArr = new Array();
//   for (let i = 0; i < selectCount; i++) {
//     numbersArr.push(selectValue[i].value);
//   }

//   $.ajax({
//     type: "POST",
//     url: "opex/opexInvoiceCostListSave.php",
//     data: { costName: costName, numbersArr: numbersArr },
//     dataType: 'JSON',
//     success: function (result) {
//       if (result == 'success') {
//         $('.dismiss').click();
//       } else if (result == 'Fail') {
//         console.log(result);
//       } else {
//         selectValue[result.line].setAttribute('class', 'alert alert-danger');
//         selectValue[result.line].setAttribute('style', 'width: 100%;');
//         if (!tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0]) {
//           let message = document.createElement('p');
//           message.setAttribute('style', 'font-size: 13px; color: #a94442;');
//           message.innerHTML = `Paslauga jau yra priskirtas prie skilties: ${result.exist}`;
//           tr[result.line].getElementsByTagName('td')[0].append(message);
//           setTimeout(() => {
//             tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0].remove();
//             selectValue[result.line].setAttribute('class', 'inputElement');
//           }, 2000);
//         }
//       }
//     }
//   });
// }




function saveCostFields() {
  let selectCount = document.querySelectorAll('#formTable2 select').length;
  let selectValue = document.querySelectorAll('#formTable2 select');

  let tr = document.querySelectorAll('#formTable2 tr');
  let costName = $('#cost_name').val();
  let costCenter = $('#formTable select').val();
  let title = $('#opex_cost_listLabel').html();

  let numbersArr = new Array();
  for (let i = 0; i < selectCount; i++) {
    numbersArr.push(selectValue[i].value);
  }


  $.ajax({
    type: "POST",
    url: "opex/opexCostListSave.php",
    data: { costName: costName, numbersArr: numbersArr, costCenter: costCenter, title: title },
    dataType: 'JSON',
    success: function (result) {
      if (result == 'success') {
        $('.dismiss').click();
      } else if (result == 'Fail') {
        console.log(result);
      } else {
        selectValue[result.line].setAttribute('class', 'alert alert-danger');
        selectValue[result.line].setAttribute('style', 'width: 100%;');
        if (!tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0]) {
          let message = document.createElement('p');
          message.setAttribute('style', 'font-size: 13px; color: #a94442;');
          message.innerHTML = `Paslauga jau yra priskirta prie skilties: ${result.exist}`;
          tr[result.line].getElementsByTagName('td')[0].append(message);
          setTimeout(() => {
            tr[result.line].getElementsByTagName('td')[0].getElementsByTagName('p')[0].remove();
            selectValue[result.line].setAttribute('class', 'inputElement');
          }, 2000);
        }
      }
    }
  });
}


function getPurchaseOrderInfo(purchaseorderid, service_key, type) {

  let detailLinesContent = document.querySelector('#detailLinesContent');
  let numbers = new Array();
  let numbers2 = new Array();
  $.ajax({
    type: "POST",
    url: "opex/detailInfo.php",
    data: { purchaseorderid: purchaseorderid, service_key: service_key, type: type },
    dataType: 'JSON',
    success: function (result) {
      console.log(result);
      let html = '';
      html += `
          <table class="table">
          <tr>
            <th>PO nr.</th>
            <th>Sąskaitos data</th>
            <th>Tiekėjas</th>
            <th>Paslauga</th>
            <th>Suma be PVM</th>
            <th>Suma su PVM</th>       
          </tr>`;
      result.forEach(row => {
        let listprice = row.listprice.map(Number).reduce((a, b) => { return a + b; }).toFixed(2);
        let listprice_vat = row.listprice_vat.map(Number).reduce((a, b) => { return a + b; }).toFixed(2);
        html += `<tr>
                    <td class="listViewEntryValue">${(row.purchaseorder_no ? '<a target="_blank" href="index.php?module=PurchaseOrder&view=Detail&record=' + row.purchaseorderid + `&mode=showDetailViewByMode&requestMode=full&tab_label=Pirkimo%20užsakymai%20Išsami%20informacija&app=INVENTORY&anchor_sequence=${row.sequence_no}">` + row.purchaseorder_no + '</a>' : '---')}</td>
                    <td class="listViewEntryValue">${(row.invoicedate ? row.invoicedate : '---')}</td>
                    <td class="listViewEntryValue">${(row.vendorname ? row.vendorname : '---')}</td>
                    <td class="listViewEntryValue">${(row.cargo_wgt ? row.cargo_wgt : '---')}</td>   
                    <td class="listViewEntryValue">${(listprice ? listprice : '---')}</td>
                    <td class="listViewEntryValue">${(listprice_vat ? listprice_vat : '')}</td>                  
              </tr>`;
        numbers.push(listprice_vat);
        numbers2.push(listprice);
      });
      let totalPrice = (numbers.length != '') ? numbers.map(Number).reduce((a, b) => { return a + b; }) : 0;
      let totalWithOutVatPrice = (numbers2.length != '') ? numbers2.map(Number).reduce((a, b) => { return a + b; }) : 0;
      html += `<tr>
          <td></td>
          <td></td>
          <td></td>
          <td style="font-weight: 600;">Viso be/su PVM</td>
          <td>${(totalWithOutVatPrice) ? totalWithOutVatPrice.toFixed(2) : ''}</td>
          <td>${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>
          </tr>`;
      html += `  </table>`;

      detailLinesContent.innerHTML = html;
    }
  });

  $('#detailLines').modal('show');
}


function getInvoiceInfo(invoiceid, cost, type) {
  let detailLinesContent = document.querySelector('#detailLinesContent');
  let numbers = new Array();
  let numbers2 = new Array();
  $.ajax({
    type: "POST",
    url: "opex/detailInfoStorage.php",
    data: { invoiceid: invoiceid, cost: cost, type: type },
    dataType: 'JSON',
    success: function (result) {
      let html = '';
      html += `
          <table class="table">
          <tr>
            <th>Saskaitos nr.</th>
            <th>Sąskaitos data</th>
            <th>Klientas</th>
            <th>Paslauga</th>
            <th>Suma be PVM</th>
            <th>Suma su PVM</th>       
          </tr>`;
      result.forEach(row => {
        html += `
                <tr>
                    <td class="listViewEntryValue">${(row.invoice_no ? `<a target="_blank" href="index.php?module=Invoice&view=Detail&record=${row.invoiceid}&mode=showDetailViewByMode&requestMode=full&tab_label=Sąskaita%20Išsami%20informacija&app=SALES"> ${row.invoice_no}</a>` : '---')}</td>
                    <td class="listViewEntryValue">${(row.invoicedate ? row.invoicedate : '---')}</td>
                    <td class="listViewEntryValue">${(row.accountname ? row.accountname : '---')}</td>                   
                    <td class="listViewEntryValue">${(row.service_name ? row.service_name : '---')}</td>   
                    <td class="listViewEntryValue">${(row.listprice ? row.listprice : '---')}</td>
                    <td class="listViewEntryValue">${(row.listprice_vat ? row.listprice_vat : '')}</td>                  
              </tr>`;
        numbers.push(row.listprice_vat);
        numbers2.push(row.listprice);
      });
      let totalPrice = (numbers.length != '') ? numbers.map(Number).reduce((a, b) => { return a + b; }) : 0;
      let totalPriceWithOutVat = (numbers2.length != '') ? numbers2.map(Number).reduce((a, b) => { return a + b; }) : 0;
      html += `<tr>
          <td></td>
          <td></td>
          <td></td>
          <td style="font-weight: 600;">Viso be/su PVM</td>
          <td>${(totalPriceWithOutVat) ? totalPriceWithOutVat.toFixed(2) : ''}</td>
          <td>${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>
          </tr>`;
      html += `  </table>`;

      detailLinesContent.innerHTML = html;
    }
  });

  $('#detailLines').modal('show');
}

function getInvoiceInfoService(invoiceid, service, type) {
  let detailLinesContent = document.querySelector('#detailLinesContent');
  let numbers = new Array();
  let numbers2 = new Array();
  $.ajax({
    type: "POST",
    url: "opex/detailInfoInvoice.php",
    data: { invoiceid: invoiceid, service: service, type: type },
    dataType: 'JSON',
    success: function (result) {
      console.log(result);
      let html = '';
      html += `
          <table class="table">
          <tr>
            <th>Saskaitos nr.</th>
            <th>Sąskaitos data</th>
            <th>Klientas</th>
            <th>Paslauga</th>
            <th>Suma be PVM</th>
            <th>Suma su PVM</th>       
          </tr>`;
      result.forEach(row => {
        let listprice = parseFloat(row.listprice).toFixed(2);
        let listprice_vat = parseFloat(row.listprice_vat).toFixed(2);
        html += `
                <tr>
                    <td class="listViewEntryValue">${(row.invoice_no ? `<a target="_blank" href="index.php?module=Invoice&view=Detail&record=${row.invoiceid}&mode=showDetailViewByMode&requestMode=full&tab_label=Sąskaita%20Išsami%20informacija&app=SALES&anchor_sequence=${row.sequence_no}#row${row.sequence_no}">${row.invoice_no}</a>` : '---')}</td>
                    <td class="listViewEntryValue">${(row.invoicedate ? row.invoicedate : '---')}</td>
                    <td class="listViewEntryValue">${(row.accountname ? row.accountname : '---')}</td>                   
                    <td class="listViewEntryValue">${(row.service_name ? row.service_name : '---')}</td>   
                    <td class="listViewEntryValue">${(row.listprice ? listprice : '---')}</td>
                    <td class="listViewEntryValue">${(row.listprice_vat ? listprice_vat : '')}</td>                  
              </tr>`;
        numbers.push(row.listprice_vat);
        numbers2.push(row.listprice);
      });
      let totalPrice = (numbers.length != '') ? numbers.map(Number).reduce((a, b) => { return a + b; }) : 0;
      let totalPriceWithOutVat = (numbers2.length != '') ? numbers2.map(Number).reduce((a, b) => { return a + b; }) : 0;
      html += `<tr>
          <td></td>
          <td></td>
          <td></td>
          <td style="font-weight: 600;">Viso be/su PVM</td>
          <td>${(totalPriceWithOutVat) ? totalPriceWithOutVat.toFixed(2) : ''}</td>
          <td>${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>
          </tr>`;
      html += `  </table>`;

      detailLinesContent.innerHTML = html;
    }
  });

  $('#detailLines').modal('show');
}



function getOtherPurchaseOrderInfo(month) {

  let detailLinesContent = document.querySelector('#detailLinesContent');
  let numbers = new Array();
  let numbers2 = new Array();
  $.ajax({
    type: "POST",
    url: "opex/detailOtherInfo.php",
    data: { month: month },
    dataType: 'JSON',
    success: function (result) {

      let html = '';
      html += `
          <table class="table">
          <tr>
            <th>PO nr.</th>
            <th>Tiekėjas</th>
            <th>Kaštų centras</th>
            <th>Paslauga</th>
            <th>Suma be PVM</th>
            <th>Suma su PVM</th>       
          </tr>`;
      result.forEach(row => {
        let listprice = parseFloat(row.listprice).toFixed(2);
        let listprice_vat = parseFloat(row.listprice_vat).toFixed(2);
        html += `
                <tr>
                    <td class="listViewEntryValue">${(row.purchaseorder_no ? `<a target="_blank" href="index.php?module=PurchaseOrder&view=Detail&record=${row.purchaseorderid}&anchor_sequence=${row.sequence_no}#row${row.sequence_no}">${row.purchaseorder_no}</a` : '---')}</td>
                    <td class="listViewEntryValue">${(row.vendorname ? row.vendorname : '---')}</td>
                    <td class="listViewEntryValue">${(row.cost_center ? row.cost_center : '---')}</td>   
                    <td class="listViewEntryValue">${(row.service_name ? row.service_name : '---')}</td>   
                    <td class="listViewEntryValue">${(row.listprice ? listprice : '---')}</td>
                    <td class="listViewEntryValue">${(row.listprice_vat ? listprice_vat : '')}</td>                  
              </tr>`;
        numbers.push(row.listprice_vat);
        numbers2.push(row.listprice);
      });
      let totalPrice = (numbers.length != '') ? numbers.map(Number).reduce((a, b) => { return a + b; }) : 0;
      let totalPriceWithOutVat = (numbers2.length != '') ? numbers2.map(Number).reduce((a, b) => { return a + b; }) : 0;
      html += `<tr>
          <td></td>
          <td></td>
          <td></td>
          <td style="font-weight: 600;">Viso be/su PVM</td>
          <td>${(totalPriceWithOutVat) ? totalPriceWithOutVat.toFixed(2) : ''}</td>
          <td>${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>
          </tr>`;
      html += `  </table>`;

      detailLinesContent.innerHTML = html;
    }
  });

  $('#detailLines').modal('show');
}

function getInvoiceProfitWithOutListInfo(year, month) {
  let detailLinesContent = document.querySelector('#detailLinesContent');
  let numbers = new Array();
  let numbers2 = new Array();
  $.ajax({
    type: "POST",
    url: "opex/detailInvoiceProfitWithOutListInfo.php",
    data: { year: year, month: month },
    dataType: 'JSON',
    success: function (result) {

      let html = '';
      html += `
          <table class="table">
          <tr>        
            <th>Veiksmas</th>          
            <th>Kaštų centras</th>
            <th>Paslauga</th>
            <th>Suma be PVM</th>
            <th>Suma su PVM</th>       
          </tr>`;
      result.forEach(row => {
        let listprice = parseFloat(row.listprice).toFixed(2);
        let listprice_vat = parseFloat(row.listprice_vat).toFixed(2);
        html += `
                <tr>   
                    <td class="listViewEntryValue hover more" data-invoiceid="${row.invoiceid}" data-toggle="modal" data-target="#exampleModalCenter" data-row=${row.sequence_no}>Smulkiau</td>                
                    <td class="listViewEntryValue">${(row.cost ? row.cost : '---')}</td>   
                    <td class="listViewEntryValue">${(row.name ? row.name : '---')}</td>   
                    <td class="listViewEntryValue">${(listprice ? listprice : '---')}</td>
                    <td class="listViewEntryValue">${(listprice_vat ? listprice_vat : '')}</td>                            
              </tr>`;
        numbers.push(row.listprice_vat);
        numbers2.push(row.listprice);
      });
      let totalPrice = (numbers.length != '') ? numbers.map(Number).reduce((a, b) => { return a + b; }) : 0;
      let totalPriceWithOutVat = (numbers2.length != '') ? numbers2.map(Number).reduce((a, b) => { return a + b; }) : 0;
      html += `<tr>
          <td></td>
          <td></td>
          <td style="font-weight: 600;">Viso be/su PVM</td>
          <td>${(totalPriceWithOutVat) ? totalPriceWithOutVat.toFixed(2) : ''}</td>
          <td>${(totalPrice) ? totalPrice.toFixed(2) : ''}</td>   
          </tr>`;
      html += `  </table>`;

      detailLinesContent.innerHTML = html;
    }
  });

  $('#detailLines').modal('show');
}

function getInvoiceProfitWithOutListInfoMore(invoiceid, sequence_no) {
  let detailLinesContent = document.querySelector('#unlistedListContent');
  $.ajax({
    type: "POST",
    url: "opex/getInvoiceProfitWithOutListInfoMore.php",
    data: { invoiceid: invoiceid },
    dataType: 'JSON',
    success: function (result) {
      let html = '';
      html += `
          <table class="table">
          <tr>   
            <th>Saskaita</th>            
          </tr>`;
      result.forEach(row => {
        html += `
              <tr>   
                    <td class="listViewEntryValue" data-invoiceid="${row.invoiceid}"><a target="_blank" href="/index.php?module=Invoice&view=Detail&record=${row.invoiceid}&anchor_sequence=${sequence_no}#row${sequence_no}">${row.invoice_no}</a></td>                    
              </tr>`;
      });
      detailLinesContent.innerHTML = html;
    }
  });
  $('#unlistedList').modal('show');
}
