{assign var="cost_center" value=$cost_center}

<div id="wait" style="display:none;position:fixed;top:50%;left:45%;padding:2px;"><img src="/resources/loading.gif"></div>
<div id="table-content" class="table-container">
<form method="POST">
    <table id="listview-table" class="table opex-table" >
     <thead>
      <tr>
      <th style="width: 100px;">Pavadinimas</th>
      <th style="width: 100px;">Kaštų centras</th>
       <th style="width: 500px;">Kaštai</th>
      </tr>
      </thead>
      <tbody>
      <tr>
       <td><input name="name" class="inputElement"></td>
      <td>      
        <select name="cost_center" class="inputElement">
          {foreach from=$cost_center item=item}
           <option value="{$item['costcenterid']}">{$item['costcenter_tks_cost']}</option> 
          {/foreach}
        </select>
        </td>
        <td><input name="cost" class="inputElement"></td>
      </tr>
      </tbody>
    </table>
    <button type="submit" class="btn btn-success btn-block">Saugoti</button>
</form>