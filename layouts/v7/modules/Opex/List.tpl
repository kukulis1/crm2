<link rel="stylesheet" href="layouts/v7/modules/Opex/resources/main.css?u=1" type="text/css" />
<script src="layouts/v7/modules/Opex/resources/main.js?u=14"></script>

{assign var="driver_bank" value=$driver_bank}
{assign var="getHandWritedSums" value=$getHandWritedSums}

{assign var="company_fees" value=$company_fees}
{assign var="company_employees_fees" value=$company_employees_fees}
{assign var="company_backup_driver" value=$company_backup_driver}


{assign var="trv_bank" value=$trv_bank}
{assign var="trv_company_fees" value=$trv_company_fees}
{assign var="trv_company_employee_fees" value=$trv_company_employee_fees}
{assign var="trv_company_backup" value=$trv_company_backup}

{assign var="front_forwarding_bank" value=$front_forwarding_bank}  
{assign var="front_forwarding_company_fees" value=$front_forwarding_company_fees}  
{assign var="front_forwarding_company_employees_fees" value=$front_forwarding_company_employees_fees}  
{assign var="front_forwarding_company_backup" value=$front_forwarding_company_backup}  

{assign var="servis_bank" value=$servis_bank}
{assign var="servis_fees" value=$servis_fees}
{assign var="servis_employee_fees" value=$servis_employee_fees}
{assign var="servis_backup" value=$servis_backup}

{assign var="pm_bank" value=$pm_bank}
{assign var="pm_company_fees" value=$pm_company_fees}
{assign var="pm_employee_fees" value=$pm_employee_fees}
{assign var="movement_backup" value=$movement_backup}

{assign var="movement_material_storage" value=$movement_material_storage}
{assign var="pkv_zp" value=$pkv_zp}
{assign var="pkv_fees" value=$pkv_fees}
{assign var="pkv_employee_fees" value=$pkv_employee_fees}

{assign var="vilnius_bank" value=$vilnius_bank}
{assign var="vilnius_fees" value=$vilnius_fees}
{assign var="vilnius_employee_fees" value=$vilnius_employee_fees}
{assign var="vilnius_backup" value=$vilnius_backup}

{assign var="last_update" value=$last_update}
{assign var="total_profit" value=$total_profit}
{assign var="month_profit" value=$month_profit}
{assign var="transport_profit_detalization" value=$transport_profit_detalization}

{assign var="servis_profit" value=$servis_profit}
{assign var="servis_profit_detalization" value=$servis_profit_detalization}

{assign var="storage_profit" value=$storage_profit}
{assign var="storage_profit_detalization" value=$storage_profit_detalization}

{assign var="num_rows" value=$num_rows}

{assign var="months_array" value=$month}
{assign var="transport_dyzel" value=$transport_dyzel}
{assign var="total_dyzel" value=$total_dyzel}

{assign var="transport_remont" value=$transport_remont}
{assign var="total_remont" value=$total_remont}

{assign var="transport_other_expense" value=$transport_other_expense}
{assign var="transport_other_expense_detalization" value=$transport_other_expense_detalization}

{assign var="transport_other_expense2" value=$transport_other_expense2}
{assign var="transport_other_expense_detalization2" value=$transport_other_expense_detalization2}



{assign var="hiring_transport_forwarding" value=$hiring_transport_forwarding}
{assign var="total_hiring_transport_forwarding" value=$total_hiring_transport_forwarding}
{assign var="hiring_transport_forwarding_detalization" value=$hiring_transport_forwarding_detalization}

{assign var="forwarding_self_transport" value=$forwarding_self_transport}
{assign var="total_forwarding_self_transport" value=$total_forwarding_self_transport}

{assign var="transport_parts" value=$transport_parts}
{assign var="total_parts" value=$total_parts}

{assign var="transport_hired" value=$transport_hired}
{assign var="total_hired_transport" value=$total_hired_transport}

{assign var="transport_insurance_servis" value=$transport_insurance_servis}
{assign var="total_transport_insurance_servis" value=$total_transport_insurance_servis}
{assign var="transport_road_tol" value=$transport_road_tol}
{assign var="total_transport_road_tol" value=$total_transport_road_tol}
{assign var="transport_square_rent" value=$transport_square_rent}
{assign var="total_transport_square_rent" value=$total_transport_square_rent}
{assign var="transport_other_taxes" value=$transport_other_taxes}
{assign var="total_transport_other_taxes" value=$total_transport_other_taxes}
{assign var="transport_bank_charges" value=$transport_bank_charges}
{assign var="total_bank_charges_transport" value=$total_bank_charges_transport}

{assign var="sum_month_transport_profit" value=$sum_month_transport_profit}
{assign var="total_transport_orders" value=$total_transport_orders}

{assign var="transport_ads" value=$transport_ads}
{assign var="total_transport_ads" value=$total_transport_ads}

{assign var="forwarding" value=$forwarding}
{assign var="forwarding_detalization" value=$forwarding_detalization}

{assign var="klaipeda_bank" value=$klaipeda_bank}
{assign var="klaipeda_bank_detalization" value=$klaipeda_bank_detalization}

{assign var="klaipeda_fees" value=$klaipeda_fees}
{assign var="klaipeda_employee_fees" value=$klaipeda_employee_fees}
{assign var="klaipeda_storage_backup" value=$klaipeda_storage_backup}


{assign var="kaunas_bank" value=$kaunas_bank}
{assign var="kaunas_fees" value=$kaunas_fees}
{assign var="kaunas_employee_fees" value=$kaunas_employee_fees}
{assign var="kaunas_storage_backup" value=$kaunas_storage_backup}

{assign var="admin_bank" value=$admin_bank}
{assign var="admin_fees" value=$admin_fees}
{assign var="admin_employee_fees" value=$admin_employee_fees}
{assign var="admin_storage_backup" value=$admin_storage_backup}

{assign var="office_water" value=$office_water}
{assign var="office_phone" value=$office_phone}

{assign var="cell_phones" value=$cell_phones}
{assign var="cell_phones_detalization_admin" value=$cell_phones_detalization_admin}

{assign var="fees" value=$fees}
{assign var="fees_detalization_admin" value=$fees_detalization_admin}

{assign var="post_fees" value=$post_fees}
{assign var="post_fees_detalization" value=$post_fees_detalization}

{assign var="utilities_servis" value=$utilities_servis}
{assign var="total_utilities_servis" value=$total_utilities_servis}

{assign var="tools_servis" value=$tools_servis}
{assign var="total_tools_servis" value=$total_tools_servis}

{assign var="other_servis" value=$other_servis}


{assign var="parts_servis" value=$parts_servis}
{assign var="total_parts_servis" value=$total_parts_servis}

{assign var="place_rent_security_servis" value=$place_rent_security_servis}
{assign var="total_place_rent_security_servis" value=$total_place_rent_security_servis}

{assign var="sum_month_servis_profit" value=$sum_month_servis_profit}

{assign var="hire_loader_movement" value=$hire_loader_movement}
{assign var="total_hire_loader_movement" value=$total_hire_loader_movement}

{assign var="hire_transport_movement" value=$hire_transport_movement}
{assign var="total_hire_transport_movement" value=$total_hire_transport_movement}

{assign var="ads_movement" value=$ads_movement}
{assign var="total_ads_movement" value=$total_ads_movement}

{assign var="pack_material_movement" value=$pack_material_movement}
{assign var="total_pack_material_movement" value=$total_pack_material_movement}

{assign var="other_movement" value=$other_movement}
{assign var="total_other_movement" value=$total_other_movement}

{assign var="sum_month_servis_movement" value=$sum_month_servis_movement}
{assign var="movement_services_with_other_services" value=$movement_services_with_other_services}
{assign var="movement_services_with_other_services_all" value=$movement_services_with_other_services_all}
{assign var="movement_services_with_other_services_detalization" value=$movement_services_with_other_services_detalization}


{assign var="electricity_vilnius" value=$electricity_vilnius}
{assign var="total_electricity_vilnius" value=$total_electricity_vilnius}

{assign var="place_repair_vilnius" value=$place_repair_vilnius}
{assign var="total_place_repair_vilnius" value=$total_place_repair_vilnius}

{assign var="pack_material_vilnius" value=$pack_material_vilnius}
{assign var="total_pack_material_vilnius" value=$total_pack_material_vilnius}

{assign var="security_vilnius" value=$security_vilnius}
{assign var="total_security_vilnius" value=$total_security_vilnius}

{assign var="rent_vilnius" value=$rent_vilnius}
{assign var="total_rent_vilnius" value=$total_rent_vilnius}

{assign var="technique_rent_vilnius" value=$technique_rent_vilnius}
{assign var="total_technique_rent_vilnius" value=$total_technique_rent_vilnius}

{assign var="technique_repair_parts_vilnius" value=$technique_repair_parts_vilnius}
{assign var="total_technique_repair_parts_vilnius" value=$total_technique_repair_parts_vilnius}

{assign var="sum_month_cost_vilnius" value=$sum_month_cost_vilnius}

{assign var="storage_vilnius" value=$storage_vilnius}
{assign var="storage_vilnius_detalization" value=$storage_vilnius_detalization}

{assign var="other_profit" value=$other_profit}
{assign var="other_profit_detalization" value=$other_profit_detalization}

{assign var="stevedoring_vilnius" value=$stevedoring_vilnius}
{assign var="stevedoring_vilnius_detalization" value=$stevedoring_vilnius_detalization}

{assign var="hire_employees_klaipeda" value=$hire_employees_klaipeda}
{assign var="total_electricity__klaipeda" value=$total_electricity__klaipeda}

{assign var="electricity_klaipeda" value=$electricity_klaipeda}
{assign var="total_electricity_klaipeda" value=$total_electricity_klaipeda}

{assign var="place_repair_klaipeda" value=$place_repair_klaipeda}
{assign var="total_place_repair_klaipeda" value=$total_place_repair_klaipeda}

{assign var="rent_klaipeda" value=$rent_klaipeda}
{assign var="total_rent_klaipeda" value=$total_rent_klaipeda}

{assign var="technique_rent_klaipeda" value=$technique_rent_klaipeda}
{assign var="total_technique_rent_klaipeda" value=$total_technique_rent_klaipeda}

{assign var="technique_repair_parts_klaipeda" value=$technique_repair_parts_klaipeda}
{assign var="total_technique_repair_parts_klaipeda" value=$total_technique_repair_parts_klaipeda}

{assign var="other_klaipeda" value=$other_klaipeda}
{assign var="total_other_klaipeda" value=$total_other_klaipeda}

{assign var="sum_month_cost_klaipeda" value=$sum_month_cost_klaipeda}

{assign var="storage_klaipeda" value=$storage_klaipeda}
{assign var="storage_klaipeda_detalization" value=$storage_klaipeda_detalization}
{assign var="stevedoring_klaipeda" value=$stevedoring_klaipeda}
{assign var="stevedoring_klaipeda_detalization" value=$stevedoring_klaipeda_detalization}


{assign var="electricity_kaunas" value=$electricity_kaunas}
{assign var="total_electricity_kaunas" value=$total_electricity_kaunas}

{assign var="place_repair_kaunas" value=$place_repair_kaunas}
{assign var="total_place_repair_kaunas" value=$total_place_repair_kaunas}

{assign var="security_kaunas" value=$security_kaunas}
{assign var="total_security_kaunas" value=$total_security_kaunas}

{assign var="rent_kaunas" value=$rent_kaunas}

{assign var="technique_rent_kaunas" value=$technique_rent_kaunas}
{assign var="total_technique_rent_kaunas" value=$total_technique_rent_kaunas}

{assign var="technique_repair_parts_kaunas" value=$technique_repair_parts_kaunas}
{assign var="total_technique_repair_parts_kaunas" value=$total_technique_repair_parts_kaunas}


{assign var="sum_month_cost_kaunas" value=$sum_month_cost_kaunas}

{assign var="storage_kaunas" value=$storage_kaunas}
{assign var="storage_kaunas_detalization" value=$storage_kaunas_detalization}
{assign var="stevedoring_kaunas" value=$stevedoring_kaunas}
{assign var="stevedoring_kaunas_detalization" value=$stevedoring_kaunas_detalization}

{assign var="electricity_office" value=$electricity_office}
{assign var="total_electricity_office" value=$total_electricity_office}

{assign var="connection_office" value=$connection_office}
{assign var="total_connection_office" value=$total_connection_office}

{assign var="rent_repair_office" value=$rent_repair_office}
{assign var="total_rent_repair_office" value=$total_rent_repair_office}

{assign var="security_office" value=$security_office}
{assign var="total_security_office" value=$total_security_office}

{assign var="leasing_office" value=$leasing_office}
{assign var="total_leasing_office" value=$total_leasing_office}

{assign var="sum_month_cost_office" value=$sum_month_cost_office}


{assign var="sum_all_places_and_services_cost" value=$sum_all_places_and_services_cost}


{assign var="insurance_servis_admin" value=$insurance_servis_admin}
{assign var="total_insurance_servis_admin" value=$total_insurance_servis_admin}

{assign var="inventory_admin" value=$inventory_admin}
{assign var="total_inventory_admin" value=$total_inventory_admin}

{assign var="ads_admin" value=$ads_admin}
{assign var="total_ads_admin" value=$total_ads_admin}

{assign var="fuel_admin" value=$fuel_admin}
{assign var="total_fuel_admin" value=$total_fuel_admin}

{assign var="other_admin" value=$other_admin}
{assign var="total_other_admin" value=$total_other_admin}

{assign var="other2_admin" value=$other2_admin}
{assign var="total_other2_admin" value=$total_other2_admin}

{assign var="other_fees_admin" value=$other_fees_admin}
{assign var="total_other_fees_admin" value=$total_other_fees_admin}

{assign var="representation_admin" value=$representation_admin}
{assign var="total_representation_admin" value=$total_representation_admin}

{assign var="it_admin" value=$it_admin}
{assign var="total_it_admin" value=$total_it_admin}

{assign var="sum_month_cost_office_other" value=$sum_month_cost_office_other}

{assign var="total_real" value=$total_real}

{assign var="other_cost" value=$other_cost}
{assign var="sum_other_cost" value=$sum_other_cost}

{assign var="invoice_profit_without_list" value=$invoice_profit_without_list}
{assign var="sum_invoice_profit_without_list" value=$sum_invoice_profit_without_list}


{* pop up *}
{assign var="detalization" value=$detalization}
{assign var="invoiceDetalization" value=$invoiceDetalization}


{* detalization *}
{assign var="transport_detalization" value=$transport_detalization}
{assign var="transport_remont_detalization" value=$transport_remont_detalization}
{assign var="transport_parts_detalization" value=$transport_parts_detalization}
{assign var="transport_hired_detalization" value=$transport_hired_detalization}
{assign var="transport_insurance_detalization" value=$transport_insurance_detalization}
{assign var="transport_road_tol_detalization" value=$transport_road_tol_detalization}
{assign var="transport_square_rent_detalization" value=$transport_square_rent_detalization}
{assign var="transport_other_taxes_detalization" value=$transport_other_taxes_detalization}
{assign var="transport_bank_charges_detalization" value=$transport_bank_charges_detalization}
{assign var="transport_ads_detalization" value=$transport_ads_detalization}
{assign var="utilities_detalization_servis" value=$utilities_detalization_servis}
{assign var="tools_detalization_servis" value=$tools_detalization_servis}
{assign var="other_detalization_servis" value=$other_detalization_servis}
{assign var="parts_detalization_servis" value=$parts_detalization_servis}
{assign var="place_rent_security_detalization_servis" value=$place_rent_security_detalization_servis}
{assign var="hire_loader_detalization_movement" value=$hire_loader_detalization_movement}
{assign var="hire_transport_detalization_movement" value=$hire_transport_detalization_movement}
{assign var="ads_detalization_movement" value=$ads_detalization_movement}
{assign var="pack_material_detalization_movement" value=$pack_material_detalization_movement}
{assign var="other_detalization_movement" value=$other_detalization_movement}
{assign var="electricity_detalization_vilnius" value=$electricity_detalization_vilnius}
{assign var="place_repair_detalization_vilnius" value=$place_repair_detalization_vilnius}
{assign var="pack_material_detalization_vilnius" value=$pack_material_detalization_vilnius}
{assign var="security_detalization_vilnius" value=$security_detalization_vilnius}
{assign var="rent_detalization_vilnius" value=$rent_detalization_vilnius}
{assign var="technique_detalization_vilnius" value=$technique_detalization_vilnius}
{assign var="technique_repair_parts_detalization_vilnius" value=$technique_repair_parts_detalization_vilnius}
{assign var="hire_employees_detalization_klaipeda" value=$hire_employees_detalization_klaipeda}
{assign var="electricity_detalization_klaipeda" value=$electricity_detalization_klaipeda}
{assign var="place_repair_detalization_klaipeda" value=$place_repair_detalization_klaipeda}
{assign var="rent_detalization_klaipeda" value=$rent_detalization_klaipeda}
{assign var="technique_rent_detalization_klaipeda" value=$technique_rent_detalization_klaipeda}
{assign var="technique_repair_parts_detalization_klaipeda" value=$technique_repair_parts_detalization_klaipeda}
{assign var="other_detalization_klaipeda" value=$other_detalization_klaipeda}
{assign var="electricity_detalization_kaunas" value=$electricity_detalization_kaunas}
{assign var="place_repair_detalization_kaunas" value=$place_repair_detalization_kaunas}
{assign var="security_detalization_kaunas" value=$security_detalization_kaunas}
{assign var="rent_detalization_kaunas" value=$rent_detalization_kaunas}
{assign var="technique_rent_detalization_kaunas" value=$technique_rent_detalization_kaunas}
{assign var="technique_repair_parts_detalization_kaunas" value=$technique_repair_parts_detalization_kaunas}
{assign var="electricity_detalization_office" value=$electricity_detalization_office}
{assign var="connection_detalization_office" value=$connection_detalization_office}
{assign var="rent_repair_detalization_office" value=$rent_repair_detalization_office}
{assign var="security_detalization_office" value=$security_detalization_office}
{assign var="leasing_detalization_office" value=$leasing_detalization_office}
{assign var="insurance_servis_detalization_admin" value=$insurance_servis_detalization_admin}
{assign var="inventory_detalization_admin" value=$inventory_detalization_admin}
{assign var="ads_detalization_admin" value=$ads_detalization_admin}
{assign var="fuel_detalization_admin" value=$fuel_detalization_admin}
{assign var="other_detalization_admin" value=$other_detalization_admin}
{assign var="other2_detalization_admin" value=$other2_detalization_admin}
{assign var="other_fees_detalization_admin" value=$other_fees_detalization_admin}
{assign var="representation_detalization_admin" value=$representation_detalization_admin}
{assign var="it_detalization_admin" value=$it_detalization_admin}
{assign var="start_year" value='2020'}
{assign var="year" value=$year}
{assign var="vat" value=$vat}

<style>
  .alert {
    margin-bottom: 3px;
  }
</style>
<div class="modal fade" id="detailLines" tabindex="-1" role="dialog" aria-labelledby="detailLinesLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detailLinesLabel"></h5>       
      </div>
      <div class="modal-body" id="detailLinesContent" style="max-height: 600px;overflow-y:auto;"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="opex_cost_list" tabindex="-1" role="dialog" aria-labelledby="opex_cost_listLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 25%;">
  <input type="hidden" id="cost_name">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="opex_cost_listLabel"></h5>             
      </div>
      <input type="hidden" id="cost_center_id">
       <h6 style="padding: 0 10px;color: red;">Dėmesio. Atlikus pakitimus jie matysis tik po to kai bus paspaustas mygtukas viršuje ,,Atnaujinti ataskaitą po paslaugų korekcijos''</h6>

      <p style="padding: 0 20px;">Kaštų centras</p>
        <div style="padding: 0 15px;" class="modal-body" id="detailLinesOpexContent"></div> 

      <p style="padding: 0 20px;">Paslaugos</p>
        <div class="modal-body" id="detailLinesOpexContent2"></div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="addOneMoreField('Purchase','formTable2','formTbody2');">Pridėti kaštą</button>
        <button type="button" class="btn btn-success" onclick="saveCostFields();">Išsaugoti</button>
        <button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="opex_invoice_forwarding_cost_list" tabindex="-1" role="dialog" aria-labelledby="opex_invoice_forwarding_cost_listLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 25%;">
  <input type="hidden" id="invoice_forwarding_cost_name">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="opex_invoice_forwarding_cost_listLabel"></h5>             
      </div>
       <h6 style="padding: 0 10px;color: red;">Dėmesio. Atlikus pakitimus jie matysis tik po to kai bus paspaustas mygtukas viršuje ,,Atnaujinti ataskaitą po paslaugų korekcijos''</h6>
      <p style="padding: 0 20px;">Kaštų centrai</p>
      <div style="padding: 0 15px;" class="modal-body" id="detailLinesOpexInvoiceForwardingContent"></div>
      <div style="display: flex; padding: 0px 20px 20px 20px;">
        <button type="button" class="btn btn-primary justify-content-right" onclick="addOneMoreCostCenter();">Pridėti kaštų centrą</button>
      </div>

      <p style="padding: 0 20px;">Paslaugos</p>
      <div style="padding: 0 15px;" class="modal-body" id="detailLinesOpexInvoiceForwardingContent2"></div>
      <div style="display: flex; padding: 0px 20px 20px 20px;">
        <button type="button" class="btn btn-primary" onclick="addOneMoreField('Invoice','formTbodyInvoiceForwarding2','formTbodyInvoiceForwarding2');">Pridėti paslaugą</button>
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-success" onclick="saveInvoiceForwardingCostFields();">Išsaugoti</button>
        <button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="unlistedList" tabindex="-1" role="dialog" aria-labelledby="unlistedListTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="width: 200px;">
      <div class="modal-header">
        <h5 class="modal-title" id="unlistedListTitle">Saskaitų sarašas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="unlistedListContent">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>


<div id="wait" style="display:none;position:fixed;top:50%;left:45%;padding:2px;"><img src="/resources/loading.gif"></div>
<div id="table-content" class="table-container" style="display: table;">

    <table id="listview-table" class="table opex-table">
     <thead>
      <tr>
        <td width="10"></td>
        <td class="titles"><span style="float:right;font-size: 15px;color: #EF5E29;margin-top: 10px;">Atnaujina visa mėnesį <i class="fa fa-arrow-right"></i></span></td>
        {foreach from=$months_array item=month} 
        {* <td class="titles"><h4>{$TOTAL}</h4></td>     *}
        {$month_name =  "`$month['months']`_"}
        <td class="titles"><button data-month="{$month['month_num']}" type="button" class="btn addButton btn-success module-buttons update_month">Atnaujinti {vtranslate($month_name,$MODULE)}</button></td>    
        {/foreach}     
        <td class="titles"></td>    
      </tr>
      <tr>
        <th>
        <form method="POST" style="width:70px;">
          <select name="vat" class="inputElement" onchange='this.form.submit()' style="color: black;">   
              <option {if $vat eq 1}selected{/if} value="1">Su PVM</option>                   
              <option {if $vat eq 2}selected{/if} value="2">Be PVM</option>                   
            </select>
          </form>
        </th>
         <th>Paskutinį kartą atnaujinta: {$last_update}</th>
        {foreach from=$months_array item=month} 
        <th>{$month['month_num']}</th>    
        {/foreach}    
        <th>
         <form method="POST">
          <select name="years" class="inputElement" onchange='this.form.submit()' style="color: black;">          
              {for $nYear=$start_year to date('Y')}  
                  <option {if $nYear eq $year}selected{/if}>{$nYear}</option>
              {/for}          
            </select>
          </form>
          </th>
      </tr>
      </thead>
      <thead>
      <tr class="side-border">
       <td class="titles">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">4</span>
        </div>
       </td>
        <td class="titles">Mėnuo</td>
          {foreach from=$months_array item=month}  
          <td class="titles">{vtranslate($month['months'],$MODULE)}</td>
          {/foreach} 
        <td class="titles">TOTAL {$year}</td>    
      </tr>
      </thead>
    <tbody>
    {* Kol kas crm nera siu duomenu *}

  {$total_employees_count_arr = []}

    <tr data-row-title="driver_count" class="clickable">
      <td class="not-clickable">
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">5</span>
        </div>
      </td>
      <td class="titles red-color not-clickable">Skaičius DRIVER</td> 
      {for $len=1 to $num_rows}  
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
            {if $getHandWritedSums['driver_count'][$len]}{$getHandWritedSums['driver_count'][$len]}{else}0{/if}
            {$total_employees_count_arr[$len][] = $getHandWritedSums['driver_count'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>   
      {/for}
      <td class="not-clickable">---</td>
    </tr>

    <tr data-row-title="car_mileage" class="clickable">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">6</span>
        </div>
      </td>
      <td class="titles not-clickable">RIDA automobilių</td>   
      {$total_car_mileage = []}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
             {if $getHandWritedSums['car_mileage'][$len]}{$getHandWritedSums['car_mileage'][$len]}{else}0{/if}
             {$total_car_mileage[] = $getHandWritedSums['car_mileage'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>   
      {/for}
      <td class="not-clickable">{round(array_sum($total_car_mileage), 2)}</td>
    </tr>

     <tr data-row-title="fuel_litres" class="clickable">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">7</span>
        </div>
      </td>
      <td class="titles not-clickable">Kuras litrai</td>   
      {$total_fuel_litres = []}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
             {if $getHandWritedSums['fuel_litres'][$len]}{$getHandWritedSums['fuel_litres'][$len]}{else}0{/if}
             {$total_fuel_litres[] = $getHandWritedSums['fuel_litres'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>   
      {/for}
      <td class="not-clickable">{round(array_sum($total_fuel_litres), 2)}</td>
    </tr>

    <tr data-row-title="car_count" class="clickable">
      <td class="not-clickable">
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">8</span>
      </div>
      </td>
      <td class="titles not-clickable">Automobilių kiekis</td>   
      {* {$total_car_count = []} *}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
             {if $getHandWritedSums['car_count'][$len]}{$getHandWritedSums['car_count'][$len]}{else}0{/if}
             {* {$total_car_count[] = $getHandWritedSums['car_count'][$len]} *}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">---</td>
    </tr>   

    <tr class="light-green-background clickable" data-row-title="driver_bank">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">9</span>
          {* <i id="driver_bank" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
      {$total_driver_sum = []}
      <td class="titles not-clickable">driver / bank</td>
      {* <td class="titles">driver / bank <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['driver_bank']['costcenter']}<ul>{foreach from=$detalization['driver_bank']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
     {foreach from=$driver_bank item=SUM}       
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach} *}
      {$total_driver_bank = []}
       {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
             {if $getHandWritedSums['driver_bank'][$len]}{$getHandWritedSums['driver_bank'][$len]}{else}0{/if}
             {$total_driver_bank[] = $getHandWritedSums['driver_bank'][$len]}
             {$total_driver_sum[$len][] = $getHandWritedSums['driver_bank'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td> 
      {/for}
      <td class="not-clickable">{round(array_sum($total_driver_bank), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="company_fees">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">10</span>
          {* <i id="company_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i></td> *}
      </div>
       <td class="titles not-clickable">Mokesciai imones</td>
      {* <td class="titles">Mokesciai imones <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['company_fees']['costcenter']}<ul>{foreach from=$detalization['company_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$company_fees item=SUM}       
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach} *}
      {$total_company_fees = []}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
             {if $getHandWritedSums['company_fees'][$len]}{$getHandWritedSums['company_fees'][$len]}{else}0{/if}
             {$total_company_fees[] = $getHandWritedSums['company_fees'][$len]}
             {$total_driver_sum[$len][] = $getHandWritedSums['company_fees'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">{round(array_sum($total_company_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="company_employees_fees">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">11</span>
        {* <i id="company_employees_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </div>
        </td>
      <td class="titles not-clickable">Mokesciai  darbuot mok</td>
      {* <td class="titles">Mokesciai  darbuot mok <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['company_employees_fees']['costcenter']}<ul>{foreach from=$detalization['company_employees_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$company_employees_fees item=SUM}       
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach} *}
      {$total_company_employees_fees = []}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
             {if $getHandWritedSums['company_employees_fees'][$len]}{$getHandWritedSums['company_employees_fees'][$len]}{else}0{/if}
             {$total_company_employees_fees[] = $getHandWritedSums['company_employees_fees'][$len]}
             {$total_driver_sum[$len][] = $getHandWritedSums['company_employees_fees'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">{round(array_sum($total_company_employees_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="company_backup_driver">
      <td class="not-clickable">
        <div style="display:flex;">
            <span style="padding:5px 5px 0;">12</span>
          {* <i id="company_backup_driver" class="fa fa-pencil" style="padding:5px 5px 0;"></i></td> *}
        </div>
      <td class="titles green-color not-clickable">Atsarginis driver</td>
      {* <td class="titles green-color">Atsarginis driver<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['company_backup_driver']['costcenter']}<ul>{foreach from=$detalization['company_backup_driver']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
     {foreach from=$company_backup_driver item=SUM}       
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach} *}
      {$total_company_backup_driver = []}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['company_backup_driver'][$len]}{$getHandWritedSums['company_backup_driver'][$len]}{else}0{/if}
              {$total_company_backup_driver[] = $getHandWritedSums['company_backup_driver'][$len]}
              {$total_driver_sum[$len][] = $getHandWritedSums['company_backup_driver'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">{round(array_sum($total_company_backup_driver), 2)}</td>
    </tr>

    {$TOTAL_ZP_ALL_TALL_arr = []}
  {$transport_total_costs = array()}

   <tr class="yealow-background red-color">
      <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">13</span>
      </div>    
      </td>
      {$final_driver_sum = []}
      <td class="titles">Total Driver</td>   
      {for $len=1 to $num_rows}       
        <td>{array_sum($total_driver_sum[$len])}</td>   
        {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($total_driver_sum[$len])}
        {$final_driver_sum[] = array_sum($total_driver_sum[$len])}
        {$transport_total_costs[$len][] = array_sum($total_driver_sum[$len])}
      {/for}
      <td>{round(array_sum($final_driver_sum), 2)}</td>
    </tr>
    {* Nera duomenu pabaiga *}

    <tr>
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">14</span>
          <i id="transport_dyzel" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Dyzelinas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_dyzel']['costcenter']}<ul>{foreach from=$detalization['transport_dyzel']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>   
      {$COUNT30 = 1}
      {foreach from=$transport_dyzel item=SUM}       
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_total_costs[$COUNT30][] = $SUM['sum']}
        {assign var="COUNT30" value=$COUNT30 +1}
      {/foreach}
      <td>{round($total_dyzel, 2)}</td>
    </tr>

{* detalizacija pradzia *}
    <tr id="collapse1" class="collapse except" aria-labelledby="heading1" data-parent="#accordion">  
     <td></td>
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
   
    <tr> 
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">15</span>
         <i id="transport_remont" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">Remontas automobilių <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_remont']['costcenter']}<ul>{foreach from=$detalization['transport_remont']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>   
      {$COUNT31 = 1}
      {foreach from=$transport_remont item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_total_costs[$COUNT31][] = $SUM['sum']}
        {assign var="COUNT31" value=$COUNT31 +1}
      {/foreach}
      <td>{round($total_remont, 2)}</td>
    </tr>

    {* detalizacija pradzia *}
    <tr id="collapse2" class="collapse except" aria-labelledby="heading2" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_remont_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_remont_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_remont_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    <tr>
     <td>
     <div style="display:flex;">
      <span style="padding:5px 5px 0;">16</span>
      <i id="transport_parts" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">Detalės automobilių <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_parts']['costcenter']}<ul>{foreach from=$detalization['transport_parts']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
        {$COUNT32 = 1}
      {foreach from=$transport_parts item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_total_costs[$COUNT32][] = $SUM['sum']}
        {assign var="COUNT32" value=$COUNT32 +1}
      {/foreach}     
      <td>{round($total_parts, 2)}</td>
    </tr>

{* detalizacija pradzia *}
    <tr id="collapse3" class="collapse except" aria-labelledby="heading3" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_parts_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_parts_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_parts_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    <tr class="orange-background">
     <td>
     <div style="display:flex;">
      <span style="padding:5px 5px 0;">17</span>
      <i id="transport_hired" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">SAMDOMAS TRANSPORTAS <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_hired']['costcenter']}<ul>{foreach from=$detalization['transport_hired']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT33 = 1}
      {foreach from=$transport_hired item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_total_costs[$COUNT33][] = $SUM['sum']}
        {assign var="COUNT33" value=$COUNT33 +1}
      {/foreach}     
      <td>{round($total_hired_transport, 2)}</td>
    </tr>

  {* detalizacija pradzia *}
    <tr id="collapse4" class="collapse except" aria-labelledby="heading4" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_hired_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_hired_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_hired_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    <tr>
      <td> 
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">18</span>
        <i id="transport_insurance_servis" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">Draudimas krov.transporto, kroviniu <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_insurance_servis']['costcenter']}<ul>{foreach from=$detalization['transport_insurance_servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT34 = 1}
      {foreach from=$transport_insurance_servis item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
       {$transport_total_costs[$COUNT34][] = $SUM['sum']}
        {assign var="COUNT34" value=$COUNT34 +1}
      {/foreach}     
      <td>{round($total_transport_insurance_servis, 2)}</td>
    </tr>

{* detalizacija pradzia *}
    <tr id="collapse5" class="collapse except" aria-labelledby="heading5" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_insurance_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_insurance_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_insurance_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    <tr>
      <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">19</span>
         <i id="transport_road_tol" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
        </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">Mokesciai (keliu, naudot !!!., techapziura) krovin.automob <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_road_tol']['costcenter']}<ul>{foreach from=$detalization['transport_road_tol']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT35 = 1}
      {foreach from=$transport_road_tol item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_total_costs[$COUNT35][] = $SUM['sum']}
        {assign var="COUNT35" value=$COUNT35 +1}
      {/foreach}     
      <td>{round($total_transport_road_tol, 2)}</td>
    </tr> 
    {* detalizacija pradzia *}
    <tr id="collapse6" class="collapse except" aria-labelledby="heading6" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_road_tol_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_road_tol_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_road_tol_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
    <tr>
    <td>
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">20</span>
      <i id="transport_square_rent" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">Aiksteles, plovyklos automobiliu <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_square_rent']['costcenter']}<ul>{foreach from=$detalization['transport_square_rent']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
     {$COUNT36 = 1}
      {foreach from=$transport_square_rent item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
         {$transport_total_costs[$COUNT36][] = $SUM['sum']}
        {assign var="COUNT36" value=$COUNT36 +1}
      {/foreach}     
      <td>{$total_transport_square_rent}</td>
    </tr> 
        {* detalizacija pradzia *}
    <tr id="collapse7" class="collapse except" aria-labelledby="heading7" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_square_rent_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_square_rent_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_square_rent_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
    <tr>
     <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">21</span>
      <i id="transport_other_taxes" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">Kiti mokesciai (Ruptela, kiti) <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_other_taxes']['costcenter']}<ul>{foreach from=$detalization['transport_other_taxes']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT37 = 1}
      {foreach from=$transport_other_taxes item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_total_costs[$COUNT37][] = $SUM['sum']}
        {assign var="COUNT37" value=$COUNT37 +1}
      {/foreach}     
      <td>{round($total_transport_other_taxes, 2)}</td>
    </tr> 
            {* detalizacija pradzia *}
    <tr id="collapse8" class="collapse except" aria-labelledby="heading8" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_other_taxes_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_other_taxes_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_other_taxes_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
    <tr>
     <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">22</span>
        <i id="transport_bank_charges" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">LIZING automobilių krovininiu <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_bank_charges']['costcenter']}<ul>{foreach from=$detalization['transport_bank_charges']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT36 = 1}
      {foreach from=$transport_bank_charges item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_total_costs[$COUNT36][] = $SUM['sum']}
        {assign var="COUNT36" value=$COUNT36 +1}
      {/foreach}     
      <td>{round($total_bank_charges_transport, 2)}</td>
    </tr> 
      {* detalizacija pradzia *}
    <tr id="collapse9" class="collapse except" aria-labelledby="heading9" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_bank_charges_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_bank_charges_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_bank_charges_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
    <tr class="grey-background green-color">
     <td> 
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">23</span></div>
      </td>
      <td class="titles">Transporto užsakymu kiekis</td>  
      {foreach from=$total_transport_orders item=SUM} 
        <td>{$SUM['total_orders']}</td>   
      {/foreach}     
      <td></td>
    </tr>

    <tr class="grey-background green-color">
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">24</span>
      </div>
     </td>
      <td class="titles">Palečių kiekis</td>
      {$total_pll_arr = array()}  
      {foreach from=$total_transport_orders item=SUM} 
        <td>{$SUM['total_orders_pll_places']}</td>   
        {$total_pll_arr[] = $SUM['total_orders_pll_places']}
      {/foreach}  
      {$TOTAL_PLL = array_sum($total_pll_arr)}   
      <td>{round($TOTAL_PLL, 2)}</td>
    </tr>

    <tr class="grey-background green-color">
     <td>
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">25</span>
       </div>
     </td>
      <td class="titles">Pervežtu kg kiekis</td>  
      {foreach from=$total_transport_orders item=SUM} 
        <td>{$SUM['total_weight']}</td>       
      {/foreach}     
      <td></td>
    </tr>

        {* Kol kas crm nera siu duomenu *}

    <tr class="clickable" data-row-title="transport_manager">
     <td class="not-clickable">
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">26</span>
        </div>
     </td>
      <td class="titles red-color not-clickable">Transporto vadyba skaicius (TrV)</td>   
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['transport_manager'][$len]}{$getHandWritedSums['transport_manager'][$len]}{else}0{/if}      
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">---</td>
    </tr>

    {$TOTAL_zzz_TrV = []}

    <tr class="light-green-background clickable" data-row-title="trv_bank">
      <td class="not-clickable">
         <div style="display:flex;">
        <span style="padding:5px 5px 0;">27</span>
        {* <i id="trv_bank" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
        <td class="titles not-clickable">TrV / bank</td>
        </td>        
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['trv_bank'][$len]}{$getHandWritedSums['trv_bank'][$len]}{else}0{/if}             
              {$TOTAL_zzz_TrV[$len][] = $getHandWritedSums['trv_bank'][$len]}
              {$transport_total_costs[$len][] = $getHandWritedSums['trv_bank'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      {* <td class="titles">TrV / bank <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$invoiceDetalization['trv_bank']['costcenter']}<ul>{foreach from=$invoiceDetalization['trv_bank']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>   
       {foreach from=$trv_bank item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}    *}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="trv_company_fees">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">28</span>
          {* <i id="trv_company_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>      
      </td>
       <td class="titles not-clickable">TrV imones mokesciai</td>
      {* <td class="titles">TrV imones mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$invoiceDetalization['trv_company_fees']['costcenter']}<ul>{foreach from=$invoiceDetalization['trv_company_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$trv_company_fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}   *}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['trv_company_fees'][$len]}{$getHandWritedSums['trv_company_fees'][$len]}{else}0{/if}             
              {$TOTAL_zzz_TrV[$len][] = $getHandWritedSums['trv_company_fees'][$len]}
              {$transport_total_costs[$len][] = $getHandWritedSums['trv_company_fees'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="trv_company_employee_fees">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">29</span>
          {* <i id="trv_company_employee_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </div>    
    </td>
    <td class="titles not-clickable">TrV darbuotoju mokesciai</td>
      {* <td class="titles">TrV darbuotoju mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$invoiceDetalization['trv_company_employee_fees']['costcenter']}<ul>{foreach from=$invoiceDetalization['trv_company_employee_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$trv_company_employee_fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}   *}
       {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['trv_company_employee_fees'][$len]}{$getHandWritedSums['trv_company_employee_fees'][$len]}{else}0{/if}             
              {$TOTAL_zzz_TrV[$len][] = $getHandWritedSums['trv_company_employee_fees'][$len]}
              {$transport_total_costs[$len][] = $getHandWritedSums['trv_company_employee_fees'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="trv_company_backup">
    <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">30</span>
         {* <i id="trv_company_backup" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
       </div>
    </td>
    <td class="titles green-color not-clickable">TrV atsarginis</td>
      {* <td class="titles green-color">TrV atsarginis<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$invoiceDetalization['trv_company_backup']['costcenter']}<ul>{foreach from=$invoiceDetalization['trv_company_backup']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
          {foreach from=$trv_company_backup item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}   *}
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['trv_company_backup'][$len]}{$getHandWritedSums['trv_company_backup'][$len]}{else}0{/if}             
              {$TOTAL_zzz_TrV[$len][] = $getHandWritedSums['trv_company_backup'][$len]}
              {$transport_total_costs[$len][] = $getHandWritedSums['trv_company_backup'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="yealow-background">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">31</span>
      </div>
     </td>
      <td class="titles">TOTAL zzz TrV</td>   
      {$final_TOTAL_zzz_TrV = []}
      {for $len=1 to $num_rows}       
        <td>{array_sum($TOTAL_zzz_TrV[$len])}</td>   
        {$final_TOTAL_zzz_TrV[] = array_sum($TOTAL_zzz_TrV[$len])}      
         {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($TOTAL_zzz_TrV[$len])}
      {/for}
      <td>{round(array_sum($final_TOTAL_zzz_TrV), 2)}</td>
    </tr>

    {* Nera duomenu pabaiga *}

{$TOTAL_real_array = array()}

{$TOTAL_SUM_ARRAY = array()}
    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">32</span>
      </div>
     </td>
      <td class="titles">VISO kaštai transportas</td>  
      {for $len=1 to $num_rows}   
         {$SUM = array_sum($transport_total_costs[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_SUM_ARRAY[] = $SUM}
         {$TOTAL_real_array[$len][] = $SUM}   
      {/for} 
      {$TOTAL_SUM = array_sum($TOTAL_SUM_ARRAY)}   
      <td>{round($TOTAL_SUM, 2)}</td>
    </tr> 

    {$TOTAL_TRANSPORT_SUM_ARRAY = array()}
    <tr class="light-blue-background blue-color">
      <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">33</span>
          <i id="transport" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse56" aria-expanded="true" aria-controls="collapse56">Transporto paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['transport']['costcenter']}<ul>{foreach from=$invoiceDetalization['transport']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 

      {foreach from=$month_profit item=SUM} 
        <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>   
        {$TOTAL_TRANSPORT_SUM_ARRAY[] = $SUM['sum']}   
      {/foreach}   
      {$transport_total_sum = array_sum($TOTAL_TRANSPORT_SUM_ARRAY)}     
      <td>{round($transport_total_sum, 2)}</td>
    </tr>

          {* detalizacija pradzia *}
    <tr id="collapse56" class="collapse except" aria-labelledby="heading56" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul> 
          {foreach from=$transport_profit_detalization['accounts'] item=ACCOUNT}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$ACCOUNT}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_profit_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_profit_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}




    {$MARGE_SUM_ARRAY = array()}
    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">34</span>
        </div>
     </td>
      <td class="titles blue-color">Transportas Marge</td>  
      {$COUNT = 0}

       {for $len=1 to $num_rows}   
       {$marge_sum = $TOTAL_TRANSPORT_SUM_ARRAY[$COUNT] - $TOTAL_SUM_ARRAY[$COUNT]}      
        <td class="red-color">{$marge_sum}</td> 
        {$MARGE_SUM_ARRAY[] = $marge_sum}  
        {assign var=COUNT value=$COUNT+1}
      {/for}  


       {$total_marge = array_sum($MARGE_SUM_ARRAY)}     
      <td class="red-color">{round($total_marge, 2)}</td>
    </tr> 

 {* Kol kas crm nera siu duomenu *}

    <tr class="clickable" data-row-title="front_forwarding_employees">
     <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">35</span>
        <div>
     </td>
      <td class="titles red-color not-clickable">FRONT/Ekspedicija darbuotojų skaičius</td>  
      {$total_front_forwarding_employees = []} 
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['front_forwarding_employees'][$len]}{$getHandWritedSums['front_forwarding_employees'][$len]}{else}0{/if}             
              {$total_front_forwarding_employees[] = $getHandWritedSums['front_forwarding_employees'][$len]}     
              {$total_employees_count_arr[$len][] = $getHandWritedSums['front_forwarding_employees'][$len]}         
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">{round(array_sum($total_front_forwarding_employees), 2)}</td>
    </tr>

  {$total_front_forwarding = []}
    <tr class="light-green-background clickable" data-row-title="front_forwarding_bank">
      <td class="not-clickable">
         <div style="display:flex;">
          <span style="padding:5px 5px 0;">36</span>
          {* <i id="front_forwarding_bank" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
      <td class="titles not-clickable">FRONT/Ekspedicija bank</td>
       {$total_front_forwarding_bank = []} 
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['front_forwarding_bank'][$len]}{$getHandWritedSums['front_forwarding_bank'][$len]}{else}0{/if}             
              {$total_front_forwarding_bank[] = $getHandWritedSums['front_forwarding_bank'][$len]}  
              {$total_front_forwarding[$len][] = $getHandWritedSums['front_forwarding_bank'][$len]}            
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      {* <td class="titles">FRONT/Ekspedicija bank<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['transport']['costcenter']}<ul>{foreach from=$detalization['transport']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>   
       {foreach from=$front_forwarding_bank item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_front_forwarding_bank), 2)}</td>
    </tr> 

    <tr class="light-green-background clickable" data-row-title="front_forwarding_company_fees">
      <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">37</span>
          {* <i id="front_forwarding_company_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
        </td>
        <td class="titles not-clickable">FRONT/Ekspedicija Imones mokesciai</td>
         {$total_front_forwarding_company_fees = []} 
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['front_forwarding_company_fees'][$len]}{$getHandWritedSums['front_forwarding_company_fees'][$len]}{else}0{/if}             
              {$total_front_forwarding_company_fees[] = $getHandWritedSums['front_forwarding_company_fees'][$len]}   
              {$total_front_forwarding[$len][] = $getHandWritedSums['front_forwarding_company_fees'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      {* <td class="titles">FRONT/Ekspedicija Imones mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['front_forwarding_company_fees']['costcenter']}<ul>{foreach from=$detalization['front_forwarding_company_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {foreach from=$front_forwarding_company_fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_front_forwarding_company_fees), 2)}</td>
    </tr> 

    <tr  class="light-green-background clickable" data-row-title="front_forwarding_company_employees_fees">
     <td class="not-clickable">
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">38</span>
        {* <i id="front_forwarding_company_employees_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </div>
      </td>
      <td class="titles not-clickable">FRONT/Ekspedicija darbuotoju mokesciai</td>
       {$total_front_forwarding_company_employees_fees = []} 
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['front_forwarding_company_employees_fees'][$len]}{$getHandWritedSums['front_forwarding_company_employees_fees'][$len]}{else}0{/if}             
              {$total_front_forwarding_company_employees_fees[] = $getHandWritedSums['front_forwarding_company_employees_fees'][$len]}
              {$total_front_forwarding[$len][] = $getHandWritedSums['front_forwarding_company_employees_fees'][$len]}               
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td> 
        {/for} 
      {* <td class="titles">FRONT/Ekspedicija darbuotoju mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['front_forwarding_company_employees_fees']['costcenter']}<ul>{foreach from=$detalization['front_forwarding_company_employees_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>   
     {foreach from=$front_forwarding_company_employees_fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}  *}
      <td class="not-clickable">{round(array_sum($total_front_forwarding_company_employees_fees), 2)}</td>
    </tr> 

    <tr class="light-green-background clickable" data-row-title="front_forwarding_company_backup">
     <td class="not-clickable">
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">39</span>
        {* <i id="front_forwarding_company_backup" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
      <td class="titles green-color not-clickable">FRONT / Eksedicija atsarginis</td>
      {$total_front_forwarding_company_backup = []} 
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['front_forwarding_company_backup'][$len]}{$getHandWritedSums['front_forwarding_company_backup'][$len]}{else}0{/if}             
              {$total_front_forwarding_company_backup[] = $getHandWritedSums['front_forwarding_company_backup'][$len]}  
              {$total_front_forwarding[$len][] = $getHandWritedSums['front_forwarding_company_backup'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
        {/for}  
      {* <td class="titles green-color">FRONT / Eksedicija atsarginis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['front_forwarding_company_backup']['costcenter']}<ul>{foreach from=$detalization['front_forwarding_company_backup']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
     {foreach from=$front_forwarding_company_backup item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}  *}
      <td class="not-clickable">{round(array_sum($total_front_forwarding_company_backup), 2)}</td>
    </tr> 

    {$transport_forwarding_costs = array()}   
    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">40</span>
      </div>  
     </td>
      <td class="titles">FRONT/Ekspedicija TOTAL zzz</td>   
      {$final_total_front_forwarding = []}
      {for $len=1 to $num_rows}       
        <td>{array_sum($total_front_forwarding[$len])}</td>   
        {$final_total_front_forwarding[] = array_sum($total_front_forwarding[$len])}
        {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($total_front_forwarding[$len])}
        {$TOTAL_real_array[$len][] = array_sum($total_front_forwarding[$len])} 
        {$transport_forwarding_costs[$len][] = array_sum($total_front_forwarding[$len])}         
      {/for}
      <td>{round(array_sum($final_total_front_forwarding), 2)}</td>
    </tr>
  {* Nera duomenu pabaiga *}

    <tr class="grey-background">
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">41</span>
        <i id="hiring_transport_forwarding" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
     <td class="titles" data-toggle="collapse" data-target="#collapse76" aria-expanded="true" aria-controls="collapse76">Samdomas transportas Ekspedicija <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['hiring_transport_forwarding']['costcenter']}<ul>{foreach from=$detalization['hiring_transport_forwarding']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
        {$COUNT60 = 1}
        {foreach from=$hiring_transport_forwarding item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
          {$transport_forwarding_costs[$COUNT60][] = $SUM['sum']}
            {$TOTAL_real_array[$COUNT60][] = $SUM['sum']}          
          {assign var="COUNT60" value=$COUNT60 +1}
        {/foreach}   
      <td>{round($total_hiring_transport_forwarding, 2)}</td>
    </tr>  

       {* detalizacija pradzia *}
    <tr id="collapse76" class="collapse except" aria-labelledby="heading76" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$hiring_transport_forwarding_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($hiring_transport_forwarding_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$hiring_transport_forwarding_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
  


    <tr class="grey-background">
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">42</span>
        <i id="forwarding_self_transport" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
    </td>
      <td class="titles green-color">Ekspedicija musu transportui<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['forwarding_self_transport']['costcenter']}<ul>{foreach from=$detalization['forwarding_self_transport']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>        
      </td>   
      {$COUNT61 = 1}
      {foreach from=$forwarding_self_transport item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
         {$transport_forwarding_costs[$COUNT61][] = $SUM['sum']}
         {assign var="COUNT61" value=$COUNT61 +1}
      {/foreach}   
      <td>{round($total_forwarding_self_transport, 2)}</td>
    </tr>  
  

    <tr class="grey-background">
      <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">43</span>
          <i id="transport_ads" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">Reklama Ekspedicija / <span style="color: red;">Transportas</span> <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_ads']['costcenter']}<ul>{foreach from=$detalization['transport_ads']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT62 = 1}
      {foreach from=$transport_ads item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$transport_forwarding_costs[$COUNT62][] = $SUM['sum']}
        {$TOTAL_real_array[$COUNT62][] = $SUM['sum']}          
        {assign var="COUNT62" value=$COUNT62 +1}
        {/foreach}     
      <td>{round($total_transport_ads, 2)}</td>
    </tr>

   {* detalizacija pradzia *}
    <tr id="collapse10" class="collapse except" aria-labelledby="heading10" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_ads_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_ads_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_ads_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
  

    <tr class="grey-background">
      <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">44</span>
          <i id="transport_other_expense" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      {* <td class="titles">Kitos išlaidos</td>    *}
       <td class="titles" data-toggle="collapse" data-target="#collapse57" aria-expanded="true" aria-controls="collapse57">Kitos išlaidos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_other_expense']['costcenter']}<ul>{foreach from=$detalization['transport_other_expense']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
    {$TRANSPORT_OTHER_EXPENSE_SUM_ARRAY = array()}  
    {$COUNT63 = 1}     
     {foreach from=$transport_other_expense item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$TRANSPORT_OTHER_EXPENSE_SUM_ARRAY[] = $SUM['sum']}
        {$transport_forwarding_costs[$COUNT63][] = $SUM['sum']}
        {$TOTAL_real_array[$COUNT63][] = $SUM['sum']}          
        {assign var="COUNT63" value=$COUNT63 +1}  
     {/foreach}     
     {$total_transport_other_expense = array_sum($TRANSPORT_OTHER_EXPENSE_SUM_ARRAY)}
      <td>{round($total_transport_other_expense, 2)}</td>
    </tr>


     {* detalizacija pradzia *}
    <tr id="collapse57" class="collapse except" aria-labelledby="heading57" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_other_expense_detalization['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_other_expense_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_other_expense_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

{$TOTAL_FORWARDING_SUM_ARRAY = []}
    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">45</span>
        </div>
     </td>
      <td class="titles red-color">VISO kaštai Ekspedicija</td>  
      {for $len=1 to $num_rows}   
         {$SUM = array_sum($transport_forwarding_costs[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_FORWARDING_SUM_ARRAY[] = $SUM}
         {* {$TOTAL_real_array[$len][] = $SUM}    *}
      {/for} 
      {$TOTAL_SUM = array_sum($TOTAL_FORWARDING_SUM_ARRAY)}   
      <td>{round($TOTAL_SUM, 2)}</td>
    </tr>

        {* Kol kas crm nera siu duomenu *}
    <tr class="light-blue-background">
      <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">46</span>
          <i id="forwarding_profit" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      {* <td class="titles blue-color">EKSPEDICIJA paslaugų pardavimas</td>  *}
 <td class="titles blue-color" data-toggle="collapse" data-target="#collapse58" aria-expanded="true" aria-controls="collapse58">EKSPEDICIJA paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['forwarding_profit']['costcenter']}<ul>{foreach from=$invoiceDetalization['forwarding_profit']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {$FORWARDING_SUM_ARRAY = array()}            
     {foreach from=$forwarding item=SUM} 
      <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">0</td>   
      {$FORWARDING_SUM_ARRAY[] = $SUM['sum']}    
      {/foreach}   
       {$total_forwarding = array_sum($FORWARDING_SUM_ARRAY)}      
      <td>{$total_forwarding}</td>
    </tr>

              {* detalizacija pradzia *}
    <tr id="collapse58" class="collapse except" aria-labelledby="heading58" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$forwarding_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($forwarding_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$forwarding_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}


    {* Nera duomenu pabaiga *}

    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">47</span>
        </div>
     </td>
      <td class="titles blue-color">EKSPEDICIJA Marge</td>  
      {$total_expedition_marge_sum = 0}
      {for $len=1 to $num_rows} 
        {$expedition_marge_sum = 0 - array_sum($transport_forwarding_costs[$len])}
        {$total_expedition_marge_sum = $total_expedition_marge_sum + $expedition_marge_sum}
        <td>{$expedition_marge_sum}</td>    
      {/for}    
      <td>{round($total_expedition_marge_sum, 2)}</td>
    </tr>

   {* Kol kas crm nera siu duomenu *}
  
    <tr class="clickable" data-row-title="servis_employees_count">
     <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">48</span>
        <div>
     </td>
      <td class="titles red-color not-clickable">SERVIS darbuotoju skaičius</td>   
      {$total_servis_employees_count = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['servis_employees_count'][$len]}{$getHandWritedSums['servis_employees_count'][$len]}{else}0{/if}             
              {$total_servis_employees_count[] = $getHandWritedSums['servis_employees_count'][$len]}    
              {$total_employees_count_arr[$len][] = $getHandWritedSums['servis_employees_count'][$len]}                        
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
        {/for}  
      <td>{round(array_sum($total_servis_employees_count), 2)}</td>
    </tr class="not-clickable">

 {$TOTAL_zzz_SERVIS_VILNIUS = []}

    <tr class="light-green-background clickable" data-row-title="servis_bank">
      <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">49</span>
          {* <i id="servis_bank" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
       <td class="titles not-clickable">servis bank</td>
       {$total_servis_bank = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['servis_bank'][$len]}{$getHandWritedSums['servis_bank'][$len]}{else}0{/if}             
              {$total_servis_bank[] = $getHandWritedSums['servis_bank'][$len]}  
              {$TOTAL_zzz_SERVIS_VILNIUS[$len][] = $getHandWritedSums['servis_bank'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
        {/for}  
      {* <td class="titles">servis bank <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['servis_bank']['costcenter']}<ul>{foreach from=$detalization['servis_bank']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$servis_bank item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {/foreach}    *}
      <td class="not-clickable">{round(array_sum($total_servis_bank), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="servis_fees">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">50</span>
          {* <i id="servis_fees" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
      <td class="titles not-clickable">SERVIS imones mokesciai</td>
       {$total_servis_fees = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['servis_fees'][$len]}{$getHandWritedSums['servis_fees'][$len]}{else}0{/if}             
              {$total_servis_fees[] = $getHandWritedSums['servis_fees'][$len]}  
              {$TOTAL_zzz_SERVIS_VILNIUS[$len][] = $getHandWritedSums['servis_fees'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
        {/for}  
      {* <td class="titles">SERVIS imones mokesciai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['servis_fees']['costcenter']}<ul>{foreach from=$detalization['servis_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {foreach from=$servis_fees item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_servis_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="servis_employee_fees">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">51</span>
          {* <i id="servis_employee_fees" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i> *}
      </td>
      <td class="titles not-clickable">SERVIS darbuotoju mokesciai</td>
      {$total_servis_employee_fees = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['servis_employee_fees'][$len]}{$getHandWritedSums['servis_employee_fees'][$len]}{else}0{/if}             
              {$total_servis_employee_fees[] = $getHandWritedSums['servis_employee_fees'][$len]}  
              {$TOTAL_zzz_SERVIS_VILNIUS[$len][] = $getHandWritedSums['servis_employee_fees'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
        {/for}  
      {* <td class="titles">SERVIS darbuotoju mokesciai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['servis_employee_fees']['costcenter']}<ul>{foreach from=$detalization['servis_employee_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
       {foreach from=$servis_employee_fees item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_servis_employee_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="servis_backup">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">52</span>          
        </div>
      </td>
      <td class="titles green-color not-clickable">Servis atsarginis</td>
      {$total_servis_backup = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['servis_backup'][$len]}{$getHandWritedSums['servis_backup'][$len]}{else}0{/if}             
              {$total_servis_backup[] = $getHandWritedSums['servis_backup'][$len]}  
              {$TOTAL_zzz_SERVIS_VILNIUS[$len][] = $getHandWritedSums['servis_backup'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}  
      {* <td class="titles green-color">Servis atsarginis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['servis_backup']['costcenter']}<ul>{foreach from=$detalization['servis_backup']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
     {foreach from=$servis_backup item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_servis_backup), 2)}</td>
    </tr>

    {$servis_total_sum = array()}

    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">53</span>
       </div>
     </td>
      <td class="titles">TOTAL zzz SERVIS VILNIUS</td>   
      {$final_TOTAL_zzz_SERVIS_VILNIUS = []}
      {for $len=1 to $num_rows}       
        <td>{array_sum($TOTAL_zzz_SERVIS_VILNIUS[$len])}</td>   
        {$final_TOTAL_zzz_SERVIS_VILNIUS[] = array_sum($TOTAL_zzz_SERVIS_VILNIUS[$len])}
        {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($TOTAL_zzz_SERVIS_VILNIUS[$len])}
        {$servis_total_sum[$len][] = array_sum($TOTAL_zzz_SERVIS_VILNIUS[$len])}  
      {/for}
      <td>{round(array_sum($final_TOTAL_zzz_SERVIS_VILNIUS), 2)}</td>
    </tr>    
    {* Nera duomenu pabaiga *}  

    {$total_rent_services_costs = array()}   
    <tr>
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">54</span>
          <i id="utilities_servis" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">Elektra, vanduo servis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['utilities_servis']['costcenter']}<ul>{foreach from=$detalization['utilities_servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT25 = 1}
      {foreach from=$utilities_servis item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
          {$servis_total_sum[$COUNT25][] = $SUM['sum']}  
          {$total_rent_services_costs[$COUNT25][] = $SUM['sum']}
          {assign var=COUNT25 value=$COUNT25+1}   
      {/foreach}     
      <td>{round($total_utilities_servis, 2)}</td>
    </tr>

{* detalizacija pradzia *}
    <tr id="collapse11" class="collapse except" aria-labelledby="heading11" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$utilities_detalization_servis['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($utilities_detalization_servis['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$utilities_detalization_servis['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    <tr>
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">55</span>
          <i id="tools_servis" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">Įrankių, inventoriaus pirkimas servis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['tools_servis']['costcenter']}<ul>{foreach from=$detalization['tools_servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT26 = 1}
      {foreach from=$tools_servis item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
          {$servis_total_sum[$COUNT26][] = $SUM['sum']}  
            {$total_rent_services_costs[$COUNT26][] = $SUM['sum']}
          {assign var=COUNT26 value=$COUNT26+1}    
      {/foreach}     
      <td>{round($total_tools_servis, 2)}</td>
    </tr>

    {* detalizacija pradzia *}
    <tr id="collapse12" class="collapse except" aria-labelledby="heading12" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$tools_detalization_servis['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($tools_detalization_servis['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$tools_detalization_servis['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">56</span>
        <i id="other_servis" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
    </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">Servis sand.,dujos, kitos islaidos (drab.valymas) <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['other_servis']['costcenter']}<ul>{foreach from=$detalization['other_servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT27 = 1}
      {foreach from=$other_servis item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
         {$servis_total_sum[$COUNT27][] = $SUM['sum']}  
           {$total_rent_services_costs[$COUNT27][] = $SUM['sum']}
          {assign var=COUNT27 value=$COUNT27+1}    
      {/foreach}     
      <td>{round($total_other_servis, 2)}</td>
    </tr>

        {* detalizacija pradzia *}
    <tr id="collapse13" class="collapse except" aria-labelledby="heading13" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$other_detalization_servis['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($other_detalization_servis['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$other_detalization_servis['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">57</span>
        <i id="parts_servis" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
    </td>
      <td class="titles green-color" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">Serviso automobilių detalių sąnaudos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['parts_servis']['costcenter']}<ul>{foreach from=$detalization['parts_servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
       {$COUNT28 = 1} 
      {foreach from=$parts_servis item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
           {$servis_total_sum[$COUNT28][] = $SUM['sum']}  
             {* {$total_rent_services_costs[$COUNT28][] = $SUM['sum']} *}
          {assign var=COUNT28 value=$COUNT28+1}     
      {/foreach}     
      <td>{round($total_parts_servis, 2)}</td>
    </tr>

            {* detalizacija pradzia *}
    <tr id="collapse14" class="collapse except" aria-labelledby="heading14" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$parts_detalization_servis['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($parts_detalization_servis['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$parts_detalization_servis['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">58</span>
          <i id="place_rent_security_servis" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">Nuoma servis, <span class="green-color">apsauga servis</span> <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['place_rent_security_servis']['costcenter']}<ul>{foreach from=$detalization['place_rent_security_servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT29 = 1} 
      {foreach from=$place_rent_security_servis item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
          {$servis_total_sum[$COUNT29][] = $SUM['sum']}  
            {$total_rent_services_costs[$COUNT29][] = $SUM['sum']}
          {assign var=COUNT29 value=$COUNT29+1}    
      {/foreach}     
      <td>{round($total_place_rent_security_servis, 2)}</td>
    </tr>

 {* detalizacija pradzia *}
    <tr id="collapse15" class="collapse except" aria-labelledby="heading15" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$place_rent_security_detalization_servis['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($place_rent_security_detalization_servis['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$place_rent_security_detalization_servis['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    {$TOTAL_SERVIS_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">59</span>
      </div>
     </td>
      <td class="titles">VISO kaštai servis</td>  
        {for $len=1 to $num_rows}   
         {$SUM = array_sum($servis_total_sum[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_SERVIS_ARR[] = $SUM}  
         {$TOTAL_real_array[$len][] = $SUM}  
      {/for} 
      {$TOTAL_SERVIS = array_sum($TOTAL_SERVIS_ARR)}     
      <td>{round($TOTAL_SERVIS, 2)}</td>
    </tr>                 

        {* Kol kas crm nera siu duomenu *}

    <tr class="light-blue-background blue-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">60</span>
          <i id="servis" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>  
      </td>
       <td class="titles" data-toggle="collapse" data-target="#collapse59" aria-expanded="true" aria-controls="collapse59">Servis paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['servis']['costcenter']}<ul>{foreach from=$invoiceDetalization['servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$servis_profit item=SUM} 
        <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">0</td>   
        {$TOTAL_SERVIS_SUM_ARRAY[] = $SUM['sum']}   
      {/foreach}   
      {$servis_total_suma = array_sum($TOTAL_SERVIS_SUM_ARRAY)}     
      <td>{round($servis_total_suma, 2)}</td>
    </tr>

                  {* detalizacija pradzia *}
    <tr id="collapse59" class="collapse except" aria-labelledby="heading59" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$servis_profit_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($servis_profit_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$servis_profit_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    {* Nera duomenu pabaiga *}

    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">61</span>
       </div>   
     </td>
      <td class="titles blue-color">SERVIS marge</td>
       {for $len=1 to $num_rows}   
         {$SUM = 0 - array_sum($servis_total_sum[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_SERVIS_ARR[] = $SUM}  
      {/for}    
      <td>{round($TOTAL_SERVIS, 2)}</td>
    </tr> 

  {$TOTAL_PARCEL_ORDERS_ARR = array()}
    <tr>
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">62</span>
      </div>
     </td>
      <td class="titles">Perkraustymo uzsakimu kiekis (ofis+privat)</td>  
      {foreach from=$total_transport_orders item=SUM} 
        <td>{$SUM['total_parcel_orders']}</td>
        {$TOTAL_PARCEL_ORDERS_ARR[] = $SUM['total_parcel_orders']}
      {/foreach}
      {$TOTAL_PARCEL_ORDERS = array_sum($TOTAL_PARCEL_ORDERS_ARR)}    
      <td>{round($TOTAL_PARCEL_ORDERS, 2)}</td>
    </tr> 

      {* Kol kas crm nera siu duomenu *}

    <tr class="clickable" data-row-title="parcel_masters_count">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">63</span>
      </div>
     </td>
      <td class="titles red-color not-clickable">Perkraustymo meistrų Skaičius</td>   
       {$total_parcel_masters_count = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['parcel_masters_count'][$len]}{$getHandWritedSums['parcel_masters_count'][$len]}{else}0{/if}             
              {$total_parcel_masters_count[] = $getHandWritedSums['parcel_masters_count'][$len]}  
              {$TOTAL_zzz_SERVIS_VILNIUS[$len][] = $getHandWritedSums['parcel_masters_count'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      <td class="not-clickable">{round(array_sum($total_parcel_masters_count), 2)}</td>
    </tr>

    {$TOTAL_zzz_meistru = []}
    <tr class="light-green-background clickable" data-row-title="pm_bank">
      <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">64</span>
          {* <i id="pm_bank" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </div>
      </td>
      <td class="titles not-clickable">PM bank</td>
      {$total_pm_bank = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['pm_bank'][$len]}{$getHandWritedSums['pm_bank'][$len]}{else}0{/if}             
              {$total_pm_bank[] = $getHandWritedSums['pm_bank'][$len]}  
              {$TOTAL_zzz_meistru[$len][] = $getHandWritedSums['pm_bank'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}  
      {* <td class="titles">PM bank<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['pm_bank']['costcenter']}<ul>{foreach from=$detalization['pm_bank']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {foreach from=$pm_bank item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
      {/foreach}     *}
      <td class="not-clickable">{round(array_sum($total_pm_bank), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="pm_company_fees">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">65</span>
          {* <i id="pm_company_fees" class="fa fa-pencil"  style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
       <td class="titles">PM imones mokesciai</td>
       {$total_pm_company_fees = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['pm_company_fees'][$len]}{$getHandWritedSums['pm_company_fees'][$len]}{else}0{/if}             
              {$total_pm_company_fees[] = $getHandWritedSums['pm_company_fees'][$len]}  
              {$TOTAL_zzz_meistru[$len][] = $getHandWritedSums['pm_company_fees'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}  
      {* <td class="titles">PM imones mokesciai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['pm_company_fees']['costcenter']}<ul>{foreach from=$detalization['pm_company_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>      
      {foreach from=$pm_company_fees item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
      {/foreach}    *}
      <td class="not-clickable">{round(array_sum($total_pm_company_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="pm_employee_fees">
     <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">66</span>
          {* <i id="pm_employee_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
        </td>
        <td class="titles">PM darbuotoju mokesciai</td>
        {$total_pm_employee_fees = []}
        {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['pm_employee_fees'][$len]}{$getHandWritedSums['pm_employee_fees'][$len]}{else}0{/if}             
              {$total_pm_employee_fees[] = $getHandWritedSums['pm_employee_fees'][$len]}  
              {$TOTAL_zzz_meistru[$len][] = $getHandWritedSums['pm_employee_fees'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}  
      {* <td class="titles">PM darbuotoju mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['pm_employee_fees']['costcenter']}<ul>{foreach from=$detalization['pm_employee_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
     {foreach from=$pm_employee_fees item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
      {/foreach}    *}
      <td class="not-clickable">{round(array_sum($total_pm_employee_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="movement_backup">
      <td class="not-clickable">
         <div style="display:flex;">
          <span style="padding:5px 5px 0;">67</span>
          {* <i id="movement_backup" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </td>
      <td class="titles green-color">Perkaustymas atsarginis</td>
      {$total_movement_backup = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['movement_backup'][$len]}{$getHandWritedSums['movement_backup'][$len]}{else}0{/if}             
              {$total_movement_backup[] = $getHandWritedSums['movement_backup'][$len]}  
              {$TOTAL_zzz_meistru[$len][] = $getHandWritedSums['movement_backup'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}  
      {* <td class="titles green-color">Perkaustymas atsarginis<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['movement_backup']['costcenter']}<ul>{foreach from=$detalization['movement_backup']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
       {foreach from=$movement_backup item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
      {/foreach}    *}
      <td class="not-clickable">{round(array_sum($total_movement_backup), 2)}</td>
    </tr>

{$total_movement_costs = array()}
    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">68</span>
        </div>
     </td>
      <td class="titles">TOTAL zzz meistrų</td>   
      {$final_TOTAL_zzz_meistru = []}
      {for $len=1 to $num_rows}       
        <td>{array_sum($TOTAL_zzz_meistru[$len])}</td> 
        {$final_TOTAL_zzz_meistru[] = array_sum($TOTAL_zzz_meistru[$len])}  
        {$total_movement_costs[$len][] = array_sum($TOTAL_zzz_meistru[$len])} 
          {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($TOTAL_zzz_meistru[$len])}
      {/for}
      <td>{round(array_sum($final_TOTAL_zzz_meistru), 2)}</td>
    </tr>                     

    {* Nera duomenu pabaiga *}
  
  {$TOTAL_HIRE_LOADER_ARR = array()}
    <tr>
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">69</span>
          <i id="hire_loader_movement" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse16" aria-expanded="true" aria-controls="collapse16">Samdomi loader <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['hire_loader_movement']['costcenter']}<ul>{foreach from=$detalization['hire_loader_movement']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$len = 1}
      {foreach from=$hire_loader_movement item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$TOTAL_HIRE_LOADER_ARR[] = $SUM['sum']}        
        {$total_movement_costs[$len][] = $SUM['sum']} 
        {assign var="len" value=$len +1}
      {/foreach}
      {$TOTAL_HIRE_LOADER = array_sum($TOTAL_HIRE_LOADER_ARR)}    
      <td>{round($TOTAL_HIRE_LOADER, 2)}</td>
    </tr>     
                {* detalizacija pradzia *}
    <tr id="collapse16" class="collapse except" aria-labelledby="heading16" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$hire_loader_detalization_movement['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($hire_loader_detalization_movement['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$hire_loader_detalization_movement['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">70</span>
          <i id="hire_transport_movement" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse17" aria-expanded="true" aria-controls="collapse17">Samdomas transportas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['hire_transport_movement']['costcenter']}<ul>{foreach from=$detalization['hire_transport_movement']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT50 = 1}
      {foreach from=$hire_transport_movement item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
         {$total_movement_costs[$COUNT50][] = $SUM['sum']}   
           {assign var="COUNT50" value=$COUNT50 +1}     
      {/foreach}    
      <td></td>
    </tr>
                    {* detalizacija pradzia *}
    <tr id="collapse17" class="collapse except" aria-labelledby="heading17" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$hire_transport_detalization_movement['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($hire_transport_detalization_movement['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$hire_transport_detalization_movement['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
      {$TOTAL_ADS_ARR = array()}
    <tr>
    <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">71</span>
          <i id="ads_movement" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
       </div>   
    </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse18" aria-expanded="true" aria-controls="collapse18">Reklama Perkraustymas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['ads_movement']['costcenter']}<ul>{foreach from=$detalization['ads_movement']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$len = 1}
      {foreach from=$ads_movement item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$TOTAL_ADS_ARR[] = $SUM['sum']}       
         {$total_movement_costs[$len][] = $SUM['sum']}   
       {assign var="len" value=$len +1}
      {/foreach}
      {$TOTAL_ADS = array_sum($TOTAL_ADS_ARR)}    
      <td>{round($TOTAL_ADS, 2)}</td>
    </tr>

 {* detalizacija pradzia *}
    <tr id="collapse18" class="collapse except" aria-labelledby="heading18" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$ads_detalization_movement['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($ads_detalization_movement['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$ads_detalization_movement['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
    <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">72</span>
          <i id="pack_material_movement" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse19" aria-expanded="true" aria-controls="collapse19">Pakavimo medziaga <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['pack_material_movement']['costcenter']}<ul>{foreach from=$detalization['pack_material_movement']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT51 = 1}
      {foreach from=$pack_material_movement item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$total_movement_costs[$COUNT51][] = $SUM['sum']} 
          {assign var="COUNT51" value=$COUNT51 +1}          
      {/foreach}     
      <td>{$total_pack_material_movement}</td>
    </tr> 
     {* detalizacija pradzia *}
    <tr id="collapse19" class="collapse except" aria-labelledby="heading19" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$pack_material_detalization_movement['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($pack_material_detalization_movement['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$pack_material_detalization_movement['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">73</span>
          <i id="other_movement" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
        </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse20" aria-expanded="true" aria-controls="collapse20">Kitos išlaidos (tel, …)  <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['other_movement']['costcenter']}<ul>{foreach from=$detalization['other_movement']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT52 = 1}
      {foreach from=$other_movement item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
        {$total_movement_costs[$COUNT52][] = $SUM['sum']} 
          {assign var="COUNT52" value=$COUNT52 +1}       
      {/foreach}     
      <td>{$total_other_movement}</td>
    </tr> 
     {* detalizacija pradzia *}
    <tr id="collapse20" class="collapse except" aria-labelledby="heading20" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$other_detalization_movement['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($other_detalization_movement['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$other_detalization_movement['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
    
      {* Kol kas crm nera siu duomenu *}

    <tr>
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">74</span>
          <i id="movement_material_storage" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles green-color">Perkraustymo medziagu sandeliavimas<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['movement_material_storage']['costcenter']}<ul>{foreach from=$detalization['movement_material_storage']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {$len = 1}
     {foreach from=$movement_material_storage item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {assign var="len" value=$len +1}    
      {/foreach}  
      <td>---</td>
    </tr>

    <tr class="clickable" data-row-title="movement_manager_count">
     <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">75</span>
        </div>
     </td>
      <td class="titles red-color not-clickable">Perkraustymo vadyba skaicius (PkV)</td>   
      {* {$total_movement_manager_count = []} *}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['movement_manager_count'][$len]}{$getHandWritedSums['movement_manager_count'][$len]}{else}0{/if}             
              {* {$total_movement_manager_count[] = $getHandWritedSums['movement_manager_count'][$len]}                         *}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      <td class="not-clickable">---</td>
    </tr>

   {$TOTAL_zzz_pkv = []}   

    <tr class="clickable" data-row-title="pkv_zp">
     <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">76</span>
          {* <i id="pkv_zp" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </div>
    </td>
    <td class="titles">PkV zp</td>
    {$total_pkv_zp = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['pkv_zp'][$len]}{$getHandWritedSums['pkv_zp'][$len]}{else}0{/if}             
              {$total_pkv_zp[] = $getHandWritedSums['pkv_zp'][$len]}  
              {$TOTAL_zzz_pkv[$len][] = $getHandWritedSums['pkv_zp'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">PkV zp<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['pkv_zp']['costcenter']}<ul>{foreach from=$detalization['pkv_zp']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$pkv_zp item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>       
      {/foreach}   *}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="clickable" data-row-title="pkv_fees">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">77</span>
          {* <i id="pkv_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
      <td class="titles not-clickable">PkV Imones mokesciai</td>
      {$total_pkv_fees = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['pkv_fees'][$len]}{$getHandWritedSums['pkv_fees'][$len]}{else}0{/if}             
              {$total_pkv_fees[] = $getHandWritedSums['pkv_fees'][$len]}  
              {$TOTAL_zzz_pkv[$len][] = $getHandWritedSums['pkv_fees'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">PkV Imones mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['pkv_fees']['costcenter']}<ul>{foreach from=$detalization['pkv_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {foreach from=$pkv_fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>       
      {/foreach}   *}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="clickable" data-row-title="pkv_employee_fees">
      <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">78</span>
      {* <i id="pkv_employee_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </div>
      </td>
       <td class="titles">PkV darbuotoju mokesciai</td>
       {$total_pkv_employee_fees = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['pkv_employee_fees'][$len]}{$getHandWritedSums['pkv_employee_fees'][$len]}{else}0{/if}             
              {$total_pkv_employee_fees[] = $getHandWritedSums['pkv_employee_fees'][$len]}  
              {$TOTAL_zzz_pkv[$len][] = $getHandWritedSums['pkv_employee_fees'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">PkV darbuotoju mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['pkv_employee_fees']['costcenter']}<ul>{foreach from=$detalization['pkv_employee_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>   
      {foreach from=$pkv_employee_fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>       
      {/foreach}   *}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="grey-background clickable" data-row-title="movement_manager_count_backup">
      <td >
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">79</span>
        </div>
      </td>
      <td class="titles green-color not-clickable">Perkaustymas vadyba atsarginis</td>   
    {$total_movement_manager_count_backup = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['movement_manager_count_backup'][$len]}{$getHandWritedSums['movement_manager_count_backup'][$len]}{else}0{/if}             
              {$total_movement_manager_count_backup[] = $getHandWritedSums['movement_manager_count_backup'][$len]}  
              {$TOTAL_zzz_pkv[$len][] = $getHandWritedSums['movement_manager_count_backup'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      <td class="not-clickable">---</td>
    </tr>

    <tr class="yealow-background">
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">80</span>
      </div>
     </td>
      <td class="titles">TOTAL zzz PkV</td>   
      {$final_TOTAL_zzz_pkv = []}
      {for $len=1 to $num_rows}       
        <td>{array_sum($TOTAL_zzz_pkv[$len])}</td>   
        {$final_TOTAL_zzz_pkv[] =  array_sum($TOTAL_zzz_pkv[$len])}
        {$total_movement_costs[$len][] = array_sum($TOTAL_zzz_pkv[$len])}
         {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($TOTAL_zzz_pkv[$len])}
      {/for}
      <td>{round(array_sum($final_TOTAL_zzz_pkv), 2)}</td>
    </tr>                           

    {* Nera duomenu pabaiga *}     

    {$TOTAL_MOVEMENT_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">81</span>
        </div>
     </td>
      <td class="titles">VISO kaštai Perkraustymas</td>  
      {for $len=1 to $num_rows}   
         {$SUM = array_sum($total_movement_costs[$len])}
         {$TOTAL_real_array[$len][] = $SUM}
        <td>{$SUM}</td>   
         {$TOTAL_MOVEMENT_ARR[] = $SUM}  
      {/for}    
      {$TOTAL_MOVEMENT = array_sum($TOTAL_MOVEMENT_ARR)}    
      <td>{round($TOTAL_MOVEMENT, 2)}</td>
    </tr>  

      {$TOTAL_MOVEMENT_SUM_ARRAY = array()}
    <tr class="light-blue-background blue-color">
    <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">82</span>
          <i id="movement" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
      </div>
    </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse60" aria-expanded="true" aria-controls="collapse60">Perkraustymo paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
        data-content="{$invoiceDetalization['movement']['costcenter']}<ul>{foreach from=$invoiceDetalization['movement']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$movement_services_with_other_services item=SUM} 
        <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>     
          {$TOTAL_MOVEMENT_SUM_ARRAY[] = $SUM['sum']}  
      {/foreach} 
        {$total_parcel_marge = array_sum($TOTAL_MOVEMENT_SUM_ARRAY)}        
      <td>{round($total_parcel_marge, 2)}</td>
    </tr> 

      {* detalizacija pradzia *}
    <tr id="collapse60" class="collapse except" aria-labelledby="heading60" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$movement_services_with_other_services_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($movement_services_with_other_services_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$movement_services_with_other_services_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}


      {$PARCEL_MARGE_SUM_ARRAY = array()}
    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">83</span>
      <div>
     </td>
      <td class="titles blue-color">Perkraustymas Marge</td>  
      {$COUNT2 = 0}
      {foreach from=$sum_month_servis_movement item=SUM} 
      {$marge_parcel_sum = $TOTAL_MOVEMENT_SUM_ARRAY[$COUNT2] - $TOTAL_MOVEMENT_ARR[$COUNT2]}
        <td>{$marge_parcel_sum}</td> 
        {$PARCEL_MARGE_SUM_ARRAY[] = $marge_parcel_sum}  
        {assign var=COUNT2 value=$COUNT2+1}
      {/foreach}   
       {$total_movement_marge = array_sum($PARCEL_MARGE_SUM_ARRAY)}     
      <td>{round($total_movement_marge, 2)}</td>
    </tr> 



 {* Kol kas crm nera siu duomenu *}

    <tr class="clickable" data-row-title="employee_count_vilnius_sand">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">84</span>
      </div>
     </td>
      <td class="titles red-color not-clickable">darbuotoju skaičiusVILNIUS sand.</td>   
      {$total_employee_count_vilnius_sand = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['employee_count_vilnius_sand'][$len]}{$getHandWritedSums['employee_count_vilnius_sand'][$len]}{else}0{/if}             
              {$total_employee_count_vilnius_sand[] = $getHandWritedSums['employee_count_vilnius_sand'][$len]}        
              {$total_employees_count_arr[$len][] = $getHandWritedSums['employee_count_vilnius_sand'][$len]}                
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      <td class="not-clickable">{round(array_sum($total_employee_count_vilnius_sand), 2)}</td>
    </tr>


   {$VILNIUS_TOTAL_zzz_SAND = []}

    <tr class="light-green-background clickable" data-row-title="vilnius_bank">
      <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">85</span>
      {* <i id="vilnius_bank" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
      </div>
      </td>
      <td class="titles not-clickable">VILNIUS sand bank</td>
      {$total_vilnius_bank = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['vilnius_bank'][$len]}{$getHandWritedSums['vilnius_bank'][$len]}{else}0{/if}             
              {$total_vilnius_bank[] = $getHandWritedSums['vilnius_bank'][$len]}  
              {$VILNIUS_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['vilnius_bank'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">VILNIUS sand bank<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['vilnius_bank']['costcenter']}<ul>{foreach from=$detalization['vilnius_bank']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {foreach from=$vilnius_bank item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}      *}
      <td class="not-clickable">{round(array_sum($total_vilnius_bank), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="vilnius_fees">
       <td class="not-clickable">
         <div style="display:flex;">
            <span style="padding:5px 5px 0;">86</span>           
        </div>
       </td>
       <td class="titles not-clickable">VILNIUS imones mokesciai</td>
       {$total_vilnius_fees = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['vilnius_fees'][$len]}{$getHandWritedSums['vilnius_fees'][$len]}{else}0{/if}             
              {$total_vilnius_fees[] = $getHandWritedSums['vilnius_fees'][$len]}  
              {$VILNIUS_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['vilnius_fees'][$len]}              
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">VILNIUS imones mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['vilnius_fees']['costcenter']}<ul>{foreach from=$detalization['vilnius_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
     {foreach from=$vilnius_fees item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}      *}
      <td class="not-clickable">{round(array_sum($total_vilnius_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="vilnius_employee_fees">
      <td class="not-clickable">
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">87</span>        
      </div>
      </td> 
      <td class="titles">VILNIUS darbuotoju mokesciai</td> 
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['vilnius_employee_fees'][$len]}{$getHandWritedSums['vilnius_employee_fees'][$len]}{else}0{/if}             
              {$total_vilnius_employee_fees[] = $getHandWritedSums['vilnius_employee_fees'][$len]}         
                {$VILNIUS_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['vilnius_employee_fees'][$len]}            
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">VILNIUS darbuotoju mokesciai<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['vilnius_employee_fees']['costcenter']}<ul>{foreach from=$detalization['vilnius_employee_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {foreach from=$vilnius_employee_fees item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}     *}
      <td class="not-clickable">{round(array_sum($total_vilnius_employee_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="employee_count_vilnius_sand">
     <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">88</span>          
      </div>
     </td>  
     <td class="titles not-clickable">VILNIUS sand. Atsarginis</td>
     {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['employee_count_vilnius_sand'][$len]}{$getHandWritedSums['employee_count_vilnius_sand'][$len]}{else}0{/if}             
              {$total_employee_count_vilnius_sand[] = $getHandWritedSums['employee_count_vilnius_sand'][$len]}    
                {$VILNIUS_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['employee_count_vilnius_sand'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles green-color">VILNIUS sand. Atsarginis<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['vilnius_backup']['costcenter']}<ul>{foreach from=$detalization['vilnius_backup']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$vilnius_backup item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}     *}
      <td class="not-clickable">{round(array_sum($total_employee_count_vilnius_sand), 2)}</td>
    </tr>

  {$vilnius_total_sum = array()}
  
    <tr class="yealow-background red-color">
     <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">89</span>
        </div>
     </td>
      <td class="titles">VILNIUS TOTAL zzz SAND </td> 
      {$final_VILNIUS_TOTAL_zzz_SAND = []} 
      {for $len=1 to $num_rows}       
        <td>{array_sum($VILNIUS_TOTAL_zzz_SAND[$len])}</td>   
        {$final_VILNIUS_TOTAL_zzz_SAND[] = array_sum($VILNIUS_TOTAL_zzz_SAND[$len])}
        {$vilnius_total_sum[$len][] = array_sum($VILNIUS_TOTAL_zzz_SAND[$len])}
        {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($VILNIUS_TOTAL_zzz_SAND[$len])}
      {/for}
      <td>{round(array_sum($final_VILNIUS_TOTAL_zzz_SAND), 2)}</td>
    </tr>
    {* Nera duomenu pabaiga *}  

    <tr>
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">90</span>
        <i id="electricity_vilnius" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
      <td class="titles"  data-toggle="collapse" data-target="#collapse21" aria-expanded="true" aria-controls="collapse21">VILNIUS elektra, vanduo sand <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['electricity_vilnius']['costcenter']}<ul>{foreach from=$detalization['electricity_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT18 = 1}
      {foreach from=$electricity_vilnius item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
         {$vilnius_total_sum[$COUNT18][] = $SUM['sum']}
           {$total_rent_services_costs[$COUNT18][] = $SUM['sum']}
         {assign var=COUNT18 value=$COUNT18+1} 
      {/foreach}     
      <td>{round($total_electricity_vilnius, 2)}</td>
    </tr>
         {* detalizacija pradzia *}
    <tr id="collapse21" class="collapse except" aria-labelledby="heading21" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$electricity_detalization_vilnius['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($electricity_detalization_vilnius['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$electricity_detalization_vilnius['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
    <tr>
    <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">91</span>
         <i id="place_repair_vilnius" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse22" aria-expanded="true" aria-controls="collapse22">VILNIUS  valymas, buitinių atliekų išvežimas sand <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['place_repair_vilnius']['costcenter']}<ul>{foreach from=$detalization['place_repair_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT19 = 1}
      {foreach from=$place_repair_vilnius item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td> 
         {$vilnius_total_sum[$COUNT19][] = $SUM['sum']}
           {$total_rent_services_costs[$COUNT19][] = $SUM['sum']}
         {assign var=COUNT19 value=$COUNT19+1}   
      {/foreach}     
      <td>{round($total_place_repair_vilnius, 2)}</td>
    </tr>
             {* detalizacija pradzia *}
    <tr id="collapse22" class="collapse except" aria-labelledby="heading22" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$place_repair_detalization_vilnius['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($place_repair_detalization_vilnius['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$place_repair_detalization_vilnius['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
     <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">92</span>
        <i id="pack_material_vilnius" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse23" aria-expanded="true" aria-controls="collapse23">VILNIUS pakav.medz/pll/lipdukai/vaztarasciai/kiti <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['pack_material_vilnius']['costcenter']}<ul>{foreach from=$detalization['pack_material_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT20 = 1}
      {foreach from=$pack_material_vilnius item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
         {$vilnius_total_sum[$COUNT20][] = $SUM['sum']}
           {$total_rent_services_costs[$COUNT20][] = $SUM['sum']}
         {assign var=COUNT20 value=$COUNT20+1}    
      {/foreach}     
      <td>{round($total_pack_material_vilnius, 2)}</td>
    </tr>
                 {* detalizacija pradzia *}
    <tr id="collapse23" class="collapse except" aria-labelledby="heading23" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$pack_material_detalization_vilnius['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($pack_material_detalization_vilnius['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$pack_material_detalization_vilnius['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
     <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">93</span>
        <i id="security_vilnius" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse24" aria-expanded="true" aria-controls="collapse24">VILNIUS apsauga <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['security_vilnius']['costcenter']}<ul>{foreach from=$detalization['security_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
        {$COUNT21 = 1}
      {foreach from=$security_vilnius item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
          {$vilnius_total_sum[$COUNT21][] = $SUM['sum']}
            {$total_rent_services_costs[$COUNT21][] = $SUM['sum']}
         {assign var=COUNT21 value=$COUNT21+1}    
      {/foreach}     
      <td>{round($total_security_vilnius, 2)}</td>
    </tr>

{* detalizacija pradzia *}
    <tr id="collapse24" class="collapse except" aria-labelledby="heading24" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$security_detalization_vilnius['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($security_detalization_vilnius['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$security_detalization_vilnius['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}   
    <tr>
    <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">94</span>
          <i id="rent_vilnius" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
    </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse25" aria-expanded="true" aria-controls="collapse25">VILNIUS Nuoma sand <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['rent_vilnius']['costcenter']}<ul>{foreach from=$detalization['rent_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT22 = 1}
      {foreach from=$rent_vilnius item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$vilnius_total_sum[$COUNT22][] = $SUM['sum']}
          {$total_rent_services_costs[$COUNT22][] = $SUM['sum']}
         {assign var=COUNT22 value=$COUNT22+1}   
      {/foreach}     
      <td>{round($total_rent_vilnius, 2)}</td>
    </tr>  

    {* detalizacija pradzia *}
    <tr id="collapse25" class="collapse except" aria-labelledby="heading25" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$rent_detalization_vilnius['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($rent_detalization_vilnius['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$rent_detalization_vilnius['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">95</span>
      <i id="technique_rent_vilnius" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse26" aria-expanded="true" aria-controls="collapse26">VILNIUS technikos nuoma <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['technique_rent_vilnius']['costcenter']}<ul>{foreach from=$detalization['technique_rent_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT23 = 1}
      {foreach from=$technique_rent_vilnius item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
         {$vilnius_total_sum[$COUNT23][] = $SUM['sum']}
         {assign var=COUNT23 value=$COUNT23+1}   
      {/foreach}     
      <td>{round($total_technique_rent_vilnius, 2)}</td>
    </tr>  

        {* detalizacija pradzia *}
    <tr id="collapse26" class="collapse except" aria-labelledby="heading26" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$technique_detalization_vilnius['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($technique_detalization_vilnius['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$technique_detalization_vilnius['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">96</span>
        <i id="technique_repair_parts_vilnius" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
    </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse27" aria-expanded="true" aria-controls="collapse27">VILNIUS technikos remontas/detales <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['technique_repair_parts_vilnius']['costcenter']}<ul>{foreach from=$detalization['technique_repair_parts_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
        {$COUNT24 = 1}
      {foreach from=$technique_repair_parts_vilnius item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
         {$vilnius_total_sum[$COUNT24][] = $SUM['sum']}
         {assign var=COUNT24 value=$COUNT24+1}   
      {/foreach}     
      <td>{round($total_technique_repair_parts_vilnius, 2)}</td>
    </tr>

            {* detalizacija pradzia *}
    <tr id="collapse27" class="collapse except" aria-labelledby="heading27" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$technique_repair_parts_detalization_vilnius['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($technique_repair_parts_detalization_vilnius['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$technique_repair_parts_detalization_vilnius['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    {$TOTAL_COST_VILNIUS_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">97</span>
      </div>
     </td>
      <td class="titles">VILNIUS sandėlių patalpų nuoma+paslaugos+islaidos</td>  
      {for $len=1 to $num_rows}   
         {$SUM = array_sum($vilnius_total_sum[$len]) - array_sum($VILNIUS_TOTAL_zzz_SAND[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_COST_VILNIUS_ARR[] = $SUM}  
      {/for} 
      {$TOTAL_COST_VILNIUS = array_sum($TOTAL_COST_VILNIUS_ARR)}  
      <td>{round($TOTAL_COST_VILNIUS, 2)}</td>
    </tr>

    {$TOTAL_COST_VILNIUS_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">98</span>
      </div>
     </td>
      <td class="titles">VISO kastai sand VILNIUS</td>  
      {for $len=1 to $num_rows}   
         {$SUM = array_sum($vilnius_total_sum[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_COST_VILNIUS_ARR[] = $SUM}  
         {$TOTAL_real_array[$len][] = $SUM}  
      {/for}       
      {$TOTAL_COST_VILNIUS = array_sum($TOTAL_COST_VILNIUS_ARR)}
      <td>{round($TOTAL_COST_VILNIUS, 2)}</td>
    </tr>    

       {$STORAGE_TOTAL_ARR = array()}                
    <tr class="light-blue-background">
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">99</span>
          <i id="storage_profit_vilnius" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      {* <td class="titles blue-color">Sandeliavimas pajamos</td>   *}
       <td class="titles blue-color" data-toggle="collapse" data-target="#collapse61" aria-expanded="true" aria-controls="collapse61">Sandeliavimas pajamos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['storage_profit_vilnius']['costcenter']}<ul>{foreach from=$invoiceDetalization['storage_profit_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$storage_vilnius item=SUM} 
         <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>
         {$STORAGE_TOTAL_ARR[] =$SUM['sum']}   
      {/foreach}   
      {$STORAGE_TOTAL = array_sum($STORAGE_TOTAL_ARR)}  
      <td>{round($STORAGE_TOTAL, 2)}</td>
    </tr>  

      {* detalizacija pradzia *}
    <tr id="collapse61" class="collapse except" aria-labelledby="heading61" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$storage_vilnius_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($storage_vilnius_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$storage_vilnius_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}


  {$STEVEDORING_TOTAL_ARR = array()}           
    <tr class="light-blue-background">
     <td> 
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">100</span>
          <i id="stevedoring_profit_vilnius" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
      </div>
      </td>      
       <td class="titles blue-color" data-toggle="collapse" data-target="#collapse62" aria-expanded="true" aria-controls="collapse62">Krovos darbai pajamos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['stevedoring_profit_vilnius']['costcenter']}<ul>{foreach from=$invoiceDetalization['stevedoring_profit_vilnius']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$stevedoring_vilnius item=SUM} 
       <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>
        {$STEVEDORING_TOTAL_ARR[] = $SUM['sum']}   
      {/foreach} 
      {$STEVEDORING_TOTAL = array_sum($STEVEDORING_TOTAL_ARR)}    
      <td>{round($STEVEDORING_TOTAL, 2)}</td>
    </tr>
              {* detalizacija pradzia *}
    <tr id="collapse62" class="collapse except" aria-labelledby="heading62" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$stevedoring_vilnius_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($stevedoring_vilnius_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$stevedoring_vilnius_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}
   

    <tr class="grey-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">101</span>
        </div>
     </td>
      <td class="titles grey-color">VILNIUS Sandelio paslaugų suma pajamos</td>  
     
      {for $len=0 to $num_rows-1}       
        {$SUM = $STORAGE_TOTAL_ARR[$len] + $STEVEDORING_TOTAL_ARR[$len]}
        <td>{$SUM}</td> 
        {$STORAGE_STEVEDORING_TOTAL_ARR[] = $STORAGE_TOTAL_ARR[$len] + $STEVEDORING_TOTAL_ARR[$len]}   
      {/for}
        {$STORAGE_STEVEDORING_TOTAL = array_sum($STORAGE_STEVEDORING_TOTAL_ARR)}   
      <td>{round($STORAGE_STEVEDORING_TOTAL, 2)}</td>
    </tr>

    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">102</span>
      </div>
     </td>
      <td class="titles blue-color">SAND.VILNIUS marge </td>       
      {$COUNT3 = 0}
        {foreach from=$storage_vilnius item=SUM}       
          {$total_storage_vilnius_profit = $STORAGE_STEVEDORING_TOTAL_ARR[$COUNT3] - array_sum($vilnius_total_sum[$COUNT3+1])}    
          {$TOTAL_STORAGE_PROFIT_ARR[] = $total_storage_vilnius_profit}
        <td>{$total_storage_vilnius_profit}</td> 
         {assign var=COUNT3 value=$COUNT3+1}   
      {/foreach}   
       {$TOTAL_STORAGE_PROFIT = array_sum($TOTAL_STORAGE_PROFIT_ARR)}  
      <td>{round($TOTAL_STORAGE_PROFIT, 2)}</td>
    </tr>  


 {* Kol kas crm nera siu duomenu *}


    <tr class="clickable" data-row-title="klaipeda_sand_employee">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">103</span>
        </div>
     </td>
      <td class="titles red-color not-clickable">KLAIPEDA sand darbuotoju skaicius</td>   
      {$total_klaipeda_sand_employee_count = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['klaipeda_sand_employee'][$len]}{$getHandWritedSums['klaipeda_sand_employee'][$len]}{else}0{/if}             
              {$total_klaipeda_sand_employee_count[] = $getHandWritedSums['klaipeda_sand_employee_'][$len]}    
              {$total_employees_count_arr[$len][] = $getHandWritedSums['klaipeda_sand_employee'][$len]}   
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      <td class="not-clickable">{round(array_sum($total_klaipeda_sand_employee_count), 2)}</td>
    </tr>


    {$klaipeda_TOTAL_zzz_SAND = []}
    
    <tr class="light-green-background clickable" data-row-title="klaipeda_bank">
    <td class="not-clickable">
    <div style="display:flex;">
      <span style="padding:5px 5px 0;">104</span>     
    </div>
    </td>    
    <td class="titles not-clickable">KLAIPEDA bank</td>
    {$total_klaipeda_bank_count = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['klaipeda_bank'][$len]}{$getHandWritedSums['klaipeda_bank'][$len]}{else}0{/if}             
              {$total_klaipeda_bank_count[] = $getHandWritedSums['klaipeda_bank'][$len]}    
              {$klaipeda_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['klaipeda_bank'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
    {* <td class="titles">KLAIPEDA bank <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$detalization['klaipeda_bank']['costcenter']}<ul>{foreach from=$detalization['klaipeda_bank']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
       {foreach from=$klaipeda_bank item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>     
      {/foreach}     *}
      <td class="not-clickable">{round(array_sum($total_klaipeda_sand_employee_count), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="klaipeda_fees">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">105</span>        
      </div>
     </td>
     <td class="titles not-clickable">KLAIPEDA imones mokesciai</td>
     {$total_klaipeda_fees = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['klaipeda_fees'][$len]}{$getHandWritedSums['klaipeda_fees'][$len]}{else}0{/if}             
              {$total_klaipeda_fees[] = $getHandWritedSums['klaipeda_fees'][$len]}    
              {$klaipeda_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['klaipeda_fees'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
     {* <td class="titles">KLAIPEDA imones mokesciai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$detalization['klaipeda_fees']['costcenter']}<ul>{foreach from=$detalization['klaipeda_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$klaipeda_fees item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>     
      {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_klaipeda_fees), 2)}</td>
    </tr>


    <tr class="light-green-background clickable" data-row-title="klaipeda_employee_fees">
      <td class="not-clickable">
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">106</span>       
      </div>
      </td>
      <td class="titles not-clickable">KLAIPEDA darbuotoju mokesciai</td>
      {$total_klaipeda_employee_fees = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['klaipeda_employee_fees'][$len]}{$getHandWritedSums['klaipeda_employee_fees'][$len]}{else}0{/if}             
              {$total_klaipeda_employee_fees[] = $getHandWritedSums['klaipeda_employee_fees'][$len]}    
              {$klaipeda_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['klaipeda_employee_fees'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">KLAIPEDA darbuotoju mokesciai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$detalization['klaipeda_employee_fees']['costcenter']}<ul>{foreach from=$detalization['klaipeda_employee_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>   
      {foreach from=$klaipeda_employee_fees item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>     
      {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_klaipeda_employee_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="klaipeda_storage_backup">
      <td class="not-clickable">
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">107</span>       
      </div>
      </td>
      <td class="titles not-clickable">KLAIPEDA sand. Atsarginis</td>
      {$total_klaipeda_storage_backup = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['klaipeda_storage_backup'][$len]}{$getHandWritedSums['klaipeda_storage_backup'][$len]}{else}0{/if}             
              {$total_klaipeda_storage_backup[] = $getHandWritedSums['klaipeda_storage_backup'][$len]}    
              {$klaipeda_TOTAL_zzz_SAND[$len][] = $getHandWritedSums['klaipeda_storage_backup'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
       {* <td class="titles">KLAIPEDA sand. Atsarginis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$detalization['klaipeda_storage_backup']['costcenter']}<ul>{foreach from=$detalization['klaipeda_storage_backup']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$klaipeda_storage_backup item=SUM} 
          <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>     
      {/foreach}   *}
      <td class="not-clickable">{round(array_sum($total_klaipeda_storage_backup), 2)}</td>
    </tr>

  {$klaipeda_total_costs = array()}

    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">108</span>
      </div>
     </td>
      {$final_klaipeda_TOTAL_zzz_SAND = []}
      <td class="titles">KLAIPEDA TOTAL zzz SAND</td>   
      {for $len=1 to $num_rows}       
        <td>{array_sum($klaipeda_TOTAL_zzz_SAND[$len])}</td>   
        {$final_klaipeda_TOTAL_zzz_SAND[] = array_sum($klaipeda_TOTAL_zzz_SAND[$len])}
        {$klaipeda_total_costs[$len][] = array_sum($klaipeda_TOTAL_zzz_SAND[$len])}
        {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($klaipeda_TOTAL_zzz_SAND[$len])}
      {/for}
      <td>{round(array_sum($final_klaipeda_TOTAL_zzz_SAND), 2)}</td>
    </tr>

    {* Nera duomenu pabaiga *}  
    <tr>
       <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">109</span>
        <i id="hire_employees_klaipeda" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
       </div>
       </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse28" aria-expanded="true" aria-controls="collapse28">KLAIPEDA Samdomi darbuotojai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['hire_employees_klaipeda']['costcenter']}<ul>{foreach from=$detalization['hire_employees_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT5 = 1}
      {foreach from=$hire_employees_klaipeda item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
      {$klaipeda_total_costs[$COUNT5][] = $SUM['sum']} 
      {assign var=COUNT5 value=$COUNT5+1}   
      {/foreach}     
      <td>{round($total_hire_employees_klaipeda, 2)}</td>
    </tr>

{* detalizacija pradzia *}
    <tr id="collapse28" class="collapse except" aria-labelledby="heading28" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$hire_employees_detalization_klaipeda['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($hire_employees_detalization_klaipeda['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$hire_employees_detalization_klaipeda['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
      <td>
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">110</span>
        <i id="electricity_klaipeda" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse29" aria-expanded="true" aria-controls="collapse29">KLAIPEDA dujos, elektra <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['electricity_klaipeda']['costcenter']}<ul>{foreach from=$detalization['electricity_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT6 = 1}
      {foreach from=$electricity_klaipeda item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
      {$klaipeda_total_costs[$COUNT6][] = $SUM['sum']} 
        {$total_rent_services_costs[$COUNT6][] = $SUM['sum']}
      {assign var=COUNT6 value=$COUNT6+1}     
      {/foreach}     
      <td>{round($total_electricity_klaipeda, 2)}</td>
    </tr> 

    {* detalizacija pradzia *}
    <tr id="collapse29" class="collapse except" aria-labelledby="heading29" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$electricity_detalization_klaipeda['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($electricity_detalization_klaipeda['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$electricity_detalization_klaipeda['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">111</span>
          <i id="place_repair_klaipeda" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
        </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse30" aria-expanded="true" aria-controls="collapse30">KLAIPEDA vanduo, valymas, kitos paslaugos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['place_repair_klaipeda']['costcenter']}<ul>{foreach from=$detalization['place_repair_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT7 = 1}
      {foreach from=$place_repair_klaipeda item=SUM}      
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {$klaipeda_total_costs[$COUNT7][] = $SUM['sum']} 
        {$total_rent_services_costs[$COUNT7][] = $SUM['sum']}
      {assign var=COUNT7 value=$COUNT7+1}      
      {/foreach}     
      <td>{round($total_place_repair_klaipeda, 2)}</td>
    </tr>

        {* detalizacija pradzia *}
    <tr id="collapse30" class="collapse except" aria-labelledby="heading30" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$place_repair_detalization_klaipeda['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($place_repair_detalization_klaipeda['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$place_repair_detalization_klaipeda['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">112</span>
          <i id="rent_klaipeda" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse31" aria-expanded="true" aria-controls="collapse31">KLAIPEDA sandėlio Nuoma <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['rent_klaipeda']['costcenter']}<ul>{foreach from=$detalization['rent_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT8 = 1}
      {foreach from=$rent_klaipeda item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
        {$klaipeda_total_costs[$COUNT8][] = $SUM['sum']} 
          {$total_rent_services_costs[$COUNT8][] = $SUM['sum']}
      {assign var=COUNT8 value=$COUNT8+1}       
      {/foreach}     
      <td>{round($total_rent_klaipeda, 2)}</td>
    </tr>
            
            {* detalizacija pradzia *}
    <tr id="collapse31" class="collapse except" aria-labelledby="heading31" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$rent_detalization_klaipeda['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($rent_detalization_klaipeda['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$rent_detalization_klaipeda['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">113</span>
         <i id="technique_rent_klaipeda" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse32" aria-expanded="true" aria-controls="collapse32">KLAIPEDA technikos nuoma <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['technique_rent_klaipeda']['costcenter']}<ul>{foreach from=$detalization['technique_rent_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT9 = 1}
      {foreach from=$technique_rent_klaipeda item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {$klaipeda_total_costs[$COUNT9][] = $SUM['sum']} 
      {assign var=COUNT9 value=$COUNT9+1}    
      {/foreach}     
      <td>{round($total_technique_rent_klaipeda, 2)}</td>
    </tr>
                {* detalizacija pradzia *}
    <tr id="collapse32" class="collapse except" aria-labelledby="heading32" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$technique_rent_detalization_klaipeda['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($technique_rent_detalization_klaipeda['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$technique_rent_detalization_klaipeda['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>     
    </tr>

{* detalizacija pabaiga *}
    <tr>
     <td>
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">114</span>
        <i id="technique_repair_parts_klaipeda" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse33" aria-expanded="true" aria-controls="collapse33">KLAIPEDA technikos remontas/detales  <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['technique_repair_parts_klaipeda']['costcenter']}<ul>{foreach from=$detalization['technique_repair_parts_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT10 = 1}
      {foreach from=$technique_repair_parts_klaipeda item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>    
         {$klaipeda_total_costs[$COUNT10][] = $SUM['sum']} 
      {assign var=COUNT10 value=$COUNT10+1}     
      {/foreach}     
      <td>{round($total_technique_repair_parts_klaipeda, 2)}</td>
    </tr>
                    {* detalizacija pradzia *}
    <tr id="collapse33" class="collapse except" aria-labelledby="heading33" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$technique_repair_parts_detalization_klaipeda['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($technique_repair_parts_detalization_klaipeda['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$technique_repair_parts_detalization_klaipeda['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr>
       <td>
         <div style="display:flex;">
          <span style="padding:5px 5px 0;">115</span>
          <i id="other_klaipeda" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
       </div>
       </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse34" aria-expanded="true" aria-controls="collapse34">KLAIPEDA kitos islaidos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['other_klaipeda']['costcenter']}<ul>{foreach from=$detalization['other_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT11 = 1}
      {foreach from=$other_klaipeda item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$klaipeda_total_costs[$COUNT11][] = $SUM['sum']} 
        {assign var=COUNT11 value=$COUNT11+1}    
      {/foreach}     
      <td>{round($total_other_klaipeda, 2)}</td>
    </tr> 
                        {* detalizacija pradzia *}
    <tr id="collapse34" class="collapse except" aria-labelledby="heading34" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$other_detalization_klaipeda['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($other_detalization_klaipeda['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$other_detalization_klaipeda['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}  

    {$TOTAL_COST_KLAIPEDA_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">116</span>
        </div>
     </td>
      <td class="titles">KLAIPEDA sandėlių patalpų nuoma+paslaugos+islaidos</td>  
    {for $len=1 to $num_rows}   
         {$SUM = array_sum($klaipeda_total_costs[$len]) - array_sum($klaipeda_TOTAL_zzz_SAND[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_COST_KLAIPEDA_ARR[] = $SUM}  
    {/for} 
      {$TOTAL_COST_KLAIPEDA = array_sum($TOTAL_COST_KLAIPEDA_ARR)}  
      <td>{round($TOTAL_COST_KLAIPEDA, 2)}</td>
    </tr>

    {$TOTAL_COST_KLAIPEDA_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">117</span>
        </div>
     </td>
      <td class="titles">VISO kastai sand KLAIPEDA</td>  
    {for $len=1 to $num_rows}   
         {$SUM = array_sum($klaipeda_total_costs[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_COST_KLAIPEDA_ARR[] = $SUM}  
         {$TOTAL_real_array[$len][] = $SUM}  
    {/for}      
    {$TOTAL_COST_KLAIPEDA = array_sum($TOTAL_COST_KLAIPEDA_ARR)}
    <td>{round($TOTAL_COST_KLAIPEDA, 2)}</td>
    </tr>

  {$STORAGE_KLAIPEDA_TOTAL_ARR = array()}                
    <tr class="light-blue-background">
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">118</span>
          <i id="storage_profit_klaipeda" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
          </td>
      {* <td class="titles blue-color">Sandeliavimas pajamos</td>   *}
       <td class="titles blue-color" data-toggle="collapse" data-target="#collapse63" aria-expanded="true" aria-controls="collapse63">Sandeliavimas pajamos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['storage_profit_klaipeda']['costcenter']}<ul>{foreach from=$invoiceDetalization['storage_profit_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$storage_klaipeda item=SUM} 
       <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>
         {$STORAGE_KLAIPEDA_TOTAL_ARR[] =$SUM['sum']}   
      {/foreach}    
      {$STORAGE_KLAIPEDA_TOTAL = array_sum($STORAGE_KLAIPEDA_TOTAL_ARR)} 
      <td>{round($STORAGE_KLAIPEDA_TOTAL, 2)}</td>
    </tr> 

      {* detalizacija pradzia *}
    <tr id="collapse63" class="collapse except" aria-labelledby="heading63" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$storage_klaipeda_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($storage_klaipeda_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$storage_klaipeda_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

  {$TOTAL_STEVEDORING_ARR = array()}
    <tr class="light-blue-background">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">119</span>
          <i id="stevedoring_profit_klaipeda" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
     </td>
      {* <td class="titles blue-color">Krovos darbai pajamos</td>   *}
        <td class="titles blue-color" data-toggle="collapse" data-target="#collapse64" aria-expanded="true" aria-controls="collapse64">Krovos darbai pajamos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['stevedoring_profit_klaipeda']['costcenter']}<ul>{foreach from=$invoiceDetalization['stevedoring_profit_klaipeda']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$stevedoring_klaipeda item=SUM} 
           <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>
        {$TOTAL_STEVEDORING_ARR[] = $SUM['sum']} 
      {/foreach}     
      {$TOTAL_STEVEDORING = array_sum($TOTAL_STEVEDORING_ARR)}
      <td>{round($TOTAL_STEVEDORING, 2)}</td>
    </tr> 

          {* detalizacija pradzia *}
    <tr id="collapse64" class="collapse except" aria-labelledby="heading64" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$stevedoring_klaipeda_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($stevedoring_klaipeda_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$stevedoring_klaipeda_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}
    <tr class="light-blue-background">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">120</span>
          <i id="storage_rent" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
      </td>
      {* <td class="titles blue-color">Ploto/sandeliu nuoma</td>   *}
      <td class="titles blue-color" data-toggle="collapse" data-target="#collapse65" aria-expanded="true" aria-controls="collapse65">Ploto/sandeliu nuoma <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['storage_rent']['costcenter']}<ul>{foreach from=$invoiceDetalization['storage_rent']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
       {foreach from=$storage_profit item=SUM} 
        <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>   
        {$TOTAL_STORAGE_SUM_ARRAY[] = $SUM['sum']}   
      {/foreach}   
      {$storage_total_sum = array_sum($TOTAL_STORAGE_SUM_ARRAY)}     
      <td>{round($storage_total_sum, 2)}</td>
    </tr> 

              {* detalizacija pradzia *}
    <tr id="collapse65" class="collapse except" aria-labelledby="heading65" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$storage_profit_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($storage_profit_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$storage_profit_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

    {$TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR = array()}
    <tr class="grey-background red-color">
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">121</span>
        </div>
     </td>
      <td class="titles grey-color">KLAIPEDA Sandelio paslaugų suma pajamos</td>  
     {for $len=0 to $num_rows-1}  
        {$SUM = $STORAGE_KLAIPEDA_TOTAL_ARR[$len] + $TOTAL_STEVEDORING_ARR[$len]}
        <td>{$SUM}</td> 
        {$TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR[] = $SUM}   
      {/for}  
      {$TOTAL_STEVEDORING_STORAGE_KLAIPEDA = array_sum($TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR)}   
      <td>{round($TOTAL_STEVEDORING_STORAGE_KLAIPEDA, 2)}</td>
    </tr>

    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
            <span style="padding:5px 5px 0;">122</span>
        </div>
     </td>
      <td class="titles blue-color">KLAIPEDA.SAND.Marge </td>       
      {$COUNT4 = 0}
        {foreach from=$storage_klaipeda item=SUM}       
          {$total_storage_klaipeda_profit = $TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR[$COUNT4] - array_sum($klaipeda_total_costs[$COUNT4+1])}    
          {$TOTAL_KLAIPEDA_STORAGE_PROFIT_ARR[] = $total_storage_klaipeda_profit}
        <td>{$total_storage_klaipeda_profit}</td> 
         {assign var=COUNT4 value=$COUNT4+1}   
      {/foreach}   
       {$TOTAL_KLAIPEDA_STORAGE_PROFIT = array_sum($TOTAL_KLAIPEDA_STORAGE_PROFIT_ARR)}  
      <td>{round($TOTAL_KLAIPEDA_STORAGE_PROFIT, 2)}</td>
    </tr>   

 
    <tr data-row-title="kaunas_employee_count" class="clickable">
     <td class="not-clickable">
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">123</span>
        </div>
     </td>
      <td class="titles red-color not-clickable">KAUNAS sand darbuotoju skaicius</td>   
       {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['kaunas_employee_count'][$len]}{$getHandWritedSums['kaunas_employee_count'][$len]}{else}0{/if}      
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}
      <td class="not-clickable">---</td>
    </tr>

    <tr data-row-title="kaunas_bank" class="light-green-background clickable">
     <td class="not-clickable">
     <div style="display:flex;">
          <span style="padding:5px 5px 0;">124</span>         
     </div>
     </td>
      <td class="titles not-clickable">KAUNAS sand bank</i></td>     
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['kaunas_bank'][$len]}{$getHandWritedSums['kaunas_bank'][$len]}{else}0{/if}      
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}    
      <td class="not-clickable">---</td>
    </tr>

    <tr data-row-title="kaunas_fees" class="light-green-background clickable">
      <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">125</span>         
      </div>
      </td>
      <td class="titles not-clickable">KAUNAS imones mokesciai</td>       
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['kaunas_fees'][$len]}{$getHandWritedSums['kaunas_fees'][$len]}{else}0{/if}      
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}    
      <td class="not-clickable">---</td>
    </tr>

    <tr data-row-title="kaunas_employee_fees" class="light-green-background clickable">
      <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">126</span>         
      </div>
      </td>
      <td class="titles not-clickable">KAUNAS darbuotoju mokesciai</td>        
      {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['kaunas_employee_fees'][$len]}{$getHandWritedSums['kaunas_employee_fees'][$len]}{else}0{/if}      
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}    
      <td class="not-clickable">---</td>
    </tr>

    <tr  data-row-title="kaunas_storage_backup" class="light-green-background clickable">
      <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">127</span>          
      </div>
      </td>
      <td class="titles green-color not-clickable">KAUNAS sand. Atsarginis</td>      
     {for $len=1 to $num_rows}       
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['kaunas_storage_backup'][$len]}{$getHandWritedSums['kaunas_storage_backup'][$len]}{else}0{/if}      
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>  
      {/for}    
      <td class="not-clickable">---</td>
    </tr>

    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">128</span>
      </div>
     </td>
      <td class="titles">KAUNAS TOTAL zzz SAND </td>   
      {for $len=1 to $num_rows}       
        <td>---</td>  
        {* {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($klaipeda_TOTAL_zzz_SAND[$len])} NOTE kai bus *} 
      {/for}
      <td>---</td>
    </tr>
    {* Nera duomenu pabaiga *}  


    {$kaunas_total_costs = array()}
    <tr>
     <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">129</span>
          <i id="electricity_kaunas" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse35" aria-expanded="true" aria-controls="collapse35">KAUNAS elektra,dujos sandelis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['electricity_kaunas']['costcenter']}<ul>{foreach from=$detalization['electricity_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT12 = 1}
      {foreach from=$electricity_kaunas item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$kaunas_total_costs[$COUNT12][] = $SUM['sum']}
          {$total_rent_services_costs[$COUNT12][] = $SUM['sum']}
        {assign var=COUNT12 value=$COUNT12+1}  
      {/foreach}     
      <td>{round($total_electricity_kaunas, 2)}</td>
    </tr> 

   {* detalizacija pradzia *}
    <tr id="collapse35" class="collapse except" aria-labelledby="heading35" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$electricity_detalization_kaunas['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($electricity_detalization_kaunas['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$electricity_detalization_kaunas['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">130</span>
          <i id="place_repair_kaunas" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse36" aria-expanded="true" aria-controls="collapse36">KAUNAS vanduo sandelis , remontai, ukio <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['place_repair_kaunas']['costcenter']}<ul>{foreach from=$detalization['place_repair_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT13 = 1}      
      {foreach from=$place_repair_kaunas item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$kaunas_total_costs[$COUNT13][] = $SUM['sum']}
          {$total_rent_services_costs[$COUNT13][] = $SUM['sum']}
        {assign var=COUNT13 value=$COUNT13+1}  
      {/foreach}     
      <td>{round($total_place_repair_kaunas, 2)}</td>
    </tr>

 {* detalizacija pradzia *}
    <tr id="collapse36" class="collapse except" aria-labelledby="heading36" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$place_repair_detalization_kaunas['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($place_repair_detalization_kaunas['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$place_repair_detalization_kaunas['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
       <td> 
         <div style="display:flex;">
           <span style="padding:5px 5px 0;">131</span>
           <i id="security_kaunas" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
       </div>
       </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse37" aria-expanded="true" aria-controls="collapse37">KAUNAS apsauga sandelis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['security_kaunas']['costcenter']}<ul>{foreach from=$detalization['security_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT14 = 1} 
      {foreach from=$security_kaunas item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
       {$kaunas_total_costs[$COUNT14][] = $SUM['sum']}
         {$total_rent_services_costs[$COUNT14][] = $SUM['sum']}
        {assign var=COUNT14 value=$COUNT14+1}   
      {/foreach}     
      <td>{round($total_security_kaunas, 2)}</td>
    </tr>

     {* detalizacija pradzia *}
    <tr id="collapse37" class="collapse except" aria-labelledby="heading37" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$security_detalization_kaunas['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($security_detalization_kaunas['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$security_detalization_kaunas['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
      <td>
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">132</span>
        <i id="rent_kaunas" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse38" aria-expanded="true" aria-controls="collapse38">KAUNAS Nuoma sandelis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['rent_kaunas']['costcenter']}<ul>{foreach from=$detalization['rent_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT15 = 1} 
      {foreach from=$rent_kaunas item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
       {$kaunas_total_costs[$COUNT15][] = $SUM['sum']}
         {$total_rent_services_costs[$COUNT15][] = $SUM['sum']}
        {assign var=COUNT15 value=$COUNT15+1}  
      {/foreach}     
      <td></td>
    </tr>

         {* detalizacija pradzia *}
    <tr id="collapse38" class="collapse except" aria-labelledby="heading38" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$rent_detalization_kaunas['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($rent_detalization_kaunas['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$rent_detalization_kaunas['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
     <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">133</span>
        <i id="technique_rent_kaunas" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse39" aria-expanded="true" aria-controls="collapse39">KAUNAS technikos nuoma <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['technique_rent_kaunas']['costcenter']}<ul>{foreach from=$detalization['technique_rent_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT16 = 1} 
      {foreach from=$technique_rent_kaunas item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
       {$kaunas_total_costs[$COUNT16][] = $SUM['sum']}
        {assign var=COUNT16 value=$COUNT16+1}   
      {/foreach}     
      <td>{round($total_technique_rent_kaunas, 2)}</td>
    </tr>

             {* detalizacija pradzia *}
    <tr id="collapse39" class="collapse except" aria-labelledby="heading39" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$technique_rent_detalization_kaunas['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($technique_rent_detalization_kaunas['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$technique_rent_detalization_kaunas['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
      <td>
       <div style="display:flex;">
        <span style="padding:5px 5px 0;">134</span>
        <i id="technique_repair_parts_kaunas" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse40" aria-expanded="true" aria-controls="collapse40">KAUNAS technikos remontas/detales <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['technique_repair_parts_kaunas']['costcenter']}<ul>{foreach from=$detalization['technique_repair_parts_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
       {$COUNT17 = 1}  
      {foreach from=$technique_repair_parts_kaunas item=SUM} 
      <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
       {$kaunas_total_costs[$COUNT17][] = $SUM['sum']}
        {assign var=COUNT17 value=$COUNT17+1}   
      {/foreach}     
      <td>{round($total_technique_repair_parts_kaunas, 2)}</td>
    </tr>

             {* detalizacija pradzia *}
    <tr id="collapse40" class="collapse except" aria-labelledby="heading40" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$technique_repair_parts_detalization_kaunas['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($technique_repair_parts_detalization_kaunas['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$technique_repair_parts_detalization_kaunas['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 

             {$TOTAL_COST_KAUNAS_ARR = array()}
    <tr class="yealow-background red-color">
    <td>
     <div style="display:flex;">
           <span style="padding:5px 5px 0;">135</span>
    </div>
    </td>
      <td class="titles">KAUNAS patalpų nuoma+paslaugos</td>  
    {for $len=1 to $num_rows}   
         {$SUM = array_sum($kaunas_total_costs[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_COST_KAUNAS_ARR[] = $SUM}  
    {/for} 
      {$TOTAL_COST_KAUNAS = array_sum($TOTAL_COST_KAUNAS_ARR)}  
      <td>{round($TOTAL_COST_KAUNAS, 2)}</td>
    </tr>

    <tr class="yealow-background red-color">
    <td>
       <div style="display:flex;">
           <span style="padding:5px 5px 0;">136</span>
        </div>
    </td>
      <td class="titles">VISO kastai sand KAUNAS</td>  
    {for $len=1 to $num_rows}   
            {$SUM = array_sum($kaunas_total_costs[$len])}
            <td>{$SUM}</td>   
            {$TOTAL_COST_KAUNAS_ARR[] = $SUM} 
            {$TOTAL_real_array[$len][] = $SUM}  

    {/for}        
      <td>{round($TOTAL_COST_KAUNAS, 2)}</td>
    </tr>

                     
  {$STORAGE_KAUNAS_TOTAL_ARR = array()}                
    <tr class="light-blue-background">
      <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">137</span>
          <i id="storage_profit_kaunas" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
        </td>
      {* <td class="titles blue-color">Sandeliavimas pajamos</td>   *}
        <td class="titles blue-color" data-toggle="collapse" data-target="#collapse66" aria-expanded="true" aria-controls="collapse66">Sandeliavimas pajamos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['storage_profit_kaunas']['costcenter']}<ul>{foreach from=$invoiceDetalization['storage_profit_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$storage_kaunas item=SUM} 
       <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>
         {$STORAGE_KAUNAS_TOTAL_ARR[] =$SUM['sum']}   
      {/foreach}    
      {$STORAGE_KAUNAS_TOTAL = array_sum($STORAGE_KAUNAS_TOTAL_ARR)} 
      <td>{round($STORAGE_KAUNAS_TOTAL, 2)}</td>
    </tr>  

    {* detalizacija pradzia *}
    <tr id="collapse66" class="collapse except" aria-labelledby="heading66" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$storage_kaunas_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($storage_kaunas_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$storage_kaunas_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}

  {$TOTAL_STEVEDORING_KAUNAS_ARR = array()}
    <tr class="light-blue-background">
      <td>
         <div style="display:flex;">
          <span style="padding:5px 5px 0;">138</span>
          <i id="stevedoring_profit_kaunas" data-which="invoice" class="fa fa-pencil select_invoice_cost"></i>
        </div>
      </td>
      {* <td class="titles blue-color">Krovos darbai pajamos</td>   *}
      <td class="titles blue-color" data-toggle="collapse" data-target="#collapse67" aria-expanded="true" aria-controls="collapse67">Krovos darbai pajamos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['stevedoring_profit_kaunas']['costcenter']}<ul>{foreach from=$invoiceDetalization['stevedoring_profit_kaunas']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$stevedoring_kaunas item=SUM} 
        <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>
        {$TOTAL_STEVEDORING_KAUNAS_ARR[] = $SUM['sum']} 
      {/foreach}     
      {$TOTAL_KAUNAS_STEVEDORING = array_sum($TOTAL_STEVEDORING_KAUNAS_ARR)}
      <td>{round($TOTAL_KAUNAS_STEVEDORING, 2)}</td>
    </tr> 

{* detalizacija pradzia *}
    <tr id="collapse67" class="collapse except" aria-labelledby="heading67" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$stevedoring_kaunas_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($stevedoring_kaunas_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$stevedoring_kaunas_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

     {$TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR = array()}
    <tr class="yealow-background red-color">
    <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">139</span>
        </div>
    </td>
      <td class="titles blue-color">KAUNAS Sandelio paslaugų suma</td>  
     {for $len=0 to $num_rows-1}  
        {$SUM = $STORAGE_KAUNAS_TOTAL_ARR[$len] + $TOTAL_STEVEDORING_KAUNAS_ARR[$len]}
        <td>{$SUM}</td> 
        {$TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR[] = $SUM}   
      {/for}  
      {$TOTAL_STEVEDORING_STORAGE_KAUNAS = array_sum($TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR)}   
      <td>{round($TOTAL_STEVEDORING_STORAGE_KAUNAS, 2)}</td>
    </tr>

    <tr class="yealow-background red-color">
    <td>
     <div style="display:flex;">
          <span style="padding:5px 5px 0;">140</span>
     </div>     
    </td>
      <td class="titles blue-color">KAUNAS.SAND.Marge </td>       
      {$COUNT4 = 0}
        {foreach from=$stevedoring_kaunas item=SUM}       
          {$total_storage_kaunas_profit = $TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR[$COUNT4] - $TOTAL_COST_KAUNAS_ARR[$COUNT4]}    
          {$TOTAL_KAUNAS_STORAGE_PROFIT_ARR[] = $total_storage_kaunas_profit}
        <td>{$total_storage_kaunas_profit}</td> 
         {assign var=COUNT4 value=$COUNT4+1}   
      {/foreach}   
       {$TOTAL_KAUNAS_STORAGE_PROFIT = array_sum($TOTAL_KAUNAS_STORAGE_PROFIT_ARR)}  
      <td>{round($TOTAL_KAUNAS_STORAGE_PROFIT, 2)}</td>
    </tr>  


    {* Kol kas crm nera siu duomenu *}
    <tr class="clickable" data-row-title="admin_count">
     <td class="not-clickable">
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">141</span>
       </div>   
     </td>
      <td class="titles red-color not-clickable">Skaičius admin (Apskaita, Dir, administratore, personal,IT)</td>   
     {$total_admin_count = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['admin_count'][$len]}{$getHandWritedSums['admin_count'][$len]}{else}0{/if}             
              {$total_admin_count[] = $getHandWritedSums['admin_count'][$len]}      
              {$total_employees_count_arr[$len][] = $getHandWritedSums['admin_count'][$len]}                        
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      <td class="not-clickable">{round(array_sum($total_admin_count), 2)}</td>
    </tr>

    {$Admin_TOTAL_zzz = []}

    <tr class="light-green-background clickable" data-row-title="admin_bank">
    <td class="not-clickable">
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">142</span>
        {* <i id="admin_bank" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
    </div>
    </td>
     <td class="titles not-clickable">Admin bank</td>
      {$total_admin_bank = []}
     {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['admin_bank'][$len]}{$getHandWritedSums['admin_bank'][$len]}{else}0{/if}             
              {$total_admin_bank[] = $getHandWritedSums['admin_bank'][$len]}    
              {$Admin_TOTAL_zzz[$len][] = $getHandWritedSums['admin_bank'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">Admin bank <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['admin_bank']['costcenter']}<ul>{foreach from=$detalization['admin_bank']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {foreach from=$admin_bank item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}  *}
      <td class="not-clickable">{round(array_sum($total_admin_bank), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="admin_fees">
      <td class="not-clickable">
         <div style="display:flex;">
          <span style="padding:5px 5px 0;">143</span>
          {* <i id="admin_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
       <td class="titles not-clickable">Admin imones mokesciai</td>
        {$total_admin_fees = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['admin_fees'][$len]}{$getHandWritedSums['admin_fees'][$len]}{else}0{/if}             
              {$total_admin_fees[] = $getHandWritedSums['admin_fees'][$len]}    
              {$Admin_TOTAL_zzz[$len][] = $getHandWritedSums['admin_fees'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">Admin imones mokesciai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['admin_fees']['costcenter']}<ul>{foreach from=$detalization['admin_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>      
     {foreach from=$admin_fees item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}  *}
      <td class="not-clickable">{round(array_sum($total_admin_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="admin_employee_fees">
      <td class="not-clickable">
         <div style="display:flex;">
          <span style="padding:5px 5px 0;">144</span>
          {* <i id="admin_employee_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
      <td class="titles not-clickable">Admin darbuotoju mokesciai</td>
       {$total_admin_employee_fees = []}
      {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['admin_employee_fees'][$len]}{$getHandWritedSums['admin_employee_fees'][$len]}{else}0{/if}             
              {$total_admin_employee_fees[] = $getHandWritedSums['admin_employee_fees'][$len]}    
              {$Admin_TOTAL_zzz[$len][] = $getHandWritedSums['admin_employee_fees'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles">Admin darbuotoju mokesciai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['admin_employee_fees']['costcenter']}<ul>{foreach from=$detalization['admin_employee_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>      
     {foreach from=$admin_employee_fees item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}  *}
      <td class="not-clickable">{round(array_sum($total_admin_employee_fees), 2)}</td>
    </tr>

    <tr class="light-green-background clickable" data-row-title="admin_storage_backup">
      <td class="not-clickable">
         <div style="display:flex;">
          <span style="padding:5px 5px 0;">145</span>
          {* <i id="admin_storage_backup" class="fa fa-pencil" style="padding:5px 5px 0;"></i> *}
        </div>
      </td>
       <td class="titles green-color not-clickable">Admin atsarginis</td>
        {$total_admin_storage_backup = []}
       {for $len=1 to $num_rows}       
         <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
              {if $getHandWritedSums['admin_storage_backup'][$len]}{$getHandWritedSums['admin_storage_backup'][$len]}{else}0{/if}             
              {$total_admin_storage_backup[] = $getHandWritedSums['admin_storage_backup'][$len]}    
              {$Admin_TOTAL_zzz[$len][] = $getHandWritedSums['admin_storage_backup'][$len]}                 
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>
      {/for}
      {* <td class="titles green-color">Admin atsarginis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['admin_storage_backup']['costcenter']}<ul>{foreach from=$detalization['admin_storage_backup']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {foreach from=$admin_storage_backup item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
      {/foreach}  *}
      <td class="not-clickable">{round(array_sum($total_admin_storage_backup), 2)}</td>
    </tr>

    <tr class="yealow-background red-color">
    <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">146</span>
       </div>
    </td>
      <td class="titles">Admin TOTAL zzz ADMIN</td> 
      {$final_Admin_TOTAL_zzz = []}  
      {for $len=1 to $num_rows}       
        <td>{array_sum($Admin_TOTAL_zzz[$len])}</td>  
        {$final_Admin_TOTAL_zzz[] = array_sum($Admin_TOTAL_zzz[$len])} 
        {$TOTAL_ZP_ALL_TALL_arr[$len][] = array_sum($Admin_TOTAL_zzz[$len])} 
        {$TOTAL_real_array[$len][] = array_sum($Admin_TOTAL_zzz[$len])}   
      {/for}
      <td>{round(array_sum($final_Admin_TOTAL_zzz), 2)}</td>
    </tr>
    
{$ofis_total_costs = array()}

    <tr class="yealow-background red-color">
    <td>
     <div style="display:flex;">
          <span style="padding:5px 5px 0;">147</span>
      </div>
    </td>
      <td class="titles">TOTAL ZP ALL+TALL</td>   
      {$final_TOTAL_ZP_ALL_TALL_arr = []}
      {for $len=1 to $num_rows}       
        <td>{array_sum($TOTAL_ZP_ALL_TALL_arr[$len])}</td>  
        {$final_TOTAL_ZP_ALL_TALL_arr[] = array_sum($TOTAL_ZP_ALL_TALL_arr[$len])} 
      {/for}
      <td>{round(array_sum($final_TOTAL_ZP_ALL_TALL_arr), 2)}</td>
    </tr>    
    {* Nera duomenu pabaiga *}   

    <tr>
      <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">148</span>
        <i id="electricity_office" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse41" aria-expanded="true" aria-controls="collapse41">Elekra ofis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['electricity_office']['costcenter']}<ul>{foreach from=$detalization['electricity_office']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT53 = 1}
      {foreach from=$electricity_office item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td> 
          {$ofis_total_costs[$COUNT53][] = $SUM['sum']}  
          {$total_rent_services_costs[$COUNT53][] = $SUM['sum']}
        {assign var="COUNT53" value=$COUNT53 +1}
      {/foreach}     
      <td>{round($total_electricity_office, 2)}</td>
    </tr> 

{* detalizacija pradzia *}
    <tr id="collapse41" class="collapse except" aria-labelledby="heading41" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$electricity_detalization_office['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($electricity_detalization_office['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$electricity_detalization_office['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *} 

      {* Kol kas crm nera siu duomenu *}
    <tr>
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">149</span>
      <i id="office_water" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles">Vanduo ofis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['office_water']['costcenter']}<ul>{foreach from=$detalization['office_water']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
      {$COUNT58 = 1}
        {foreach from=$office_water item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
          {$total_rent_services_costs[$COUNT58][] = $SUM['sum']}
          {assign var="COUNT58" value=$COUNT58 +1}
      {/foreach}     
      <td>---</td>
    </tr>

      <tr>
        <td>
          <div style="display:flex;">
            <span style="padding:5px 5px 0;">150</span>
            <i id="office_phone" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
        </div>
        </td>
      <td class="titles">TG telefon ofis<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
    data-content="{$detalization['office_phone']['costcenter']}<ul>{foreach from=$detalization['office_phone']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>     
      {$COUNT56 = 1}
       {foreach from=$office_phone item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
          {$total_rent_services_costs[$COUNT56][] = $SUM['sum']}
          {assign var="COUNT56" value=$COUNT56 +1}
      {/foreach}   
      <td>---</td>
    </tr>
  {* Nera duomenu pabaiga *} 

    <tr>
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">151</span>
          <i id="connection_office" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse42" aria-expanded="true" aria-controls="collapse42">Telekom ofis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['connection_office']['costcenter']}<ul>{foreach from=$detalization['connection_office']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT54  = 1}
      {foreach from=$connection_office item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td> 
          {$ofis_total_costs[$COUNT54][] = $SUM['sum']}  
          {$total_rent_services_costs[$COUNT54][] = $SUM['sum']}
        {assign var="COUNT54" value=$COUNT54 +1}  
      {/foreach}     
      <td>{round($total_connection_office, 2)}</td>
    </tr>

    {* detalizacija pradzia *}
    <tr id="collapse42" class="collapse except" aria-labelledby="heading42" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$connection_detalization_office['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($connection_detalization_office['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$connection_detalization_office['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
     <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">152</span>
          <i id="rent_repair_office" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse43" aria-expanded="true" aria-controls="collapse43">Nuoma ofis <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['rent_repair_office']['costcenter']}<ul>{foreach from=$detalization['rent_repair_office']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT55 = 1}
      {foreach from=$rent_repair_office item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$ofis_total_costs[$COUNT55][] = $SUM['sum']}  
        {$total_rent_services_costs[$COUNT55][] = $SUM['sum']}
        {assign var="COUNT55" value=$COUNT55 +1}  
      {/foreach}     
      <td>{round($total_rent_repair_office, 2)}</td>
    </tr>

        {* detalizacija pradzia *}
    <tr id="collapse43" class="collapse except" aria-labelledby="heading43" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$rent_repair_detalization_office['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($rent_repair_detalization_office['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$rent_repair_detalization_office['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
    <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">153</span>
        <i id="security_office" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse44" aria-expanded="true" aria-controls="collapse44">Apsauga ofis  <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['security_office']['costcenter']}<ul>{foreach from=$detalization['security_office']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT59 = 1}
      {foreach from=$security_office item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$ofis_total_costs[$COUNT59][] = $SUM['sum']}  
        {$total_rent_services_costs[$COUNT59][] = $SUM['sum']}
        {assign var="COUNT59" value=$COUNT59 +1}  
      {/foreach}     
      <td>{round($total_security_office, 2)}</td>
    </tr>

            {* detalizacija pradzia *}
    <tr id="collapse44" class="collapse except" aria-labelledby="heading44" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$security_detalization_office['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($security_detalization_office['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$security_detalization_office['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
    <tr>
      <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">154</span>
          <i id="leasing_office" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse45" aria-expanded="true" aria-controls="collapse45">LIZING ofiso auto <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['leasing_office']['costcenter']}<ul>{foreach from=$detalization['leasing_office']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
      {$COUNT57 = 1}
      {foreach from=$leasing_office item=SUM} 
        <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
         {$ofis_total_costs[$COUNT57][] = $SUM['sum']}  
        {assign var="COUNT57" value=$COUNT57 +1}  
      {/foreach}     
      <td>{round($total_leasing_office, 2)}</td>
    </tr> 

 {* detalizacija pradzia *}
    <tr id="collapse45" class="collapse except" aria-labelledby="heading45" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$leasing_detalization_office['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($leasing_detalization_office['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$leasing_detalization_office['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}         

      {$TOTAL_COST_OFFICE_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
      <div style="display:flex;">
          <span style="padding:5px 5px 0;">155</span>
      </div>
     </td>
      <td class="titles">VISO kaštai ofis</td>    
       {for $len=1 to $num_rows}   
         {$SUM = array_sum($ofis_total_costs[$len])}
        <td>{$SUM}</td>   
         {$TOTAL_COST_OFFICE_ARR[] = $SUM} 
        {$TOTAL_real_array[$len][] = $SUM}   
    {/for} 

      {$TOTAL_COST_OFFICE = array_sum($TOTAL_COST_OFFICE_ARR)}  
      <td>{round($TOTAL_COST_OFFICE, 2)}</td>
    </tr>

 {$TOTAL_COST_PLACES_AND_SERVICES_ARR = array()}
    <tr class="yealow-background red-color">
     <td>
        <div style="display:flex;">
            <span style="padding:5px 5px 0;">156</span>
        </div>
     </td>
      <td class="titles">Total visu patalpų nuoma+paslaugos</td>  
       {for $len=1 to $num_rows}   
         {$SUM = array_sum($total_rent_services_costs[$len])}
        <td>{$SUM}</td>   
       {$TOTAL_COST_PLACES_AND_SERVICES_ARR[] = $SUM} 
    {/for} 


     {$TOTAL_COST_PLACES_AND_SERVICES = array_sum($TOTAL_COST_PLACES_AND_SERVICES_ARR)}
      <td>{round($TOTAL_COST_PLACES_AND_SERVICES, 2)}</td>
    </tr> 


 {$real_total_costs = array()}

  <tr>
  {$COUNT38 = 1}
    <td>
     <div style="display:flex;">
      <span style="padding:5px 5px 0;">157</span>
      <i id="cell_phones" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
     <td class="titles" data-toggle="collapse" data-target="#collapse72" aria-expanded="true" aria-controls="collapse72">Mob.telefonai <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top"  data-content="{$detalization['cell_phones']['costcenter']}<ul>{foreach from=$detalization['cell_phones']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>    
   {foreach from=$cell_phones item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td> 
        {$real_total_costs[$COUNT38][] = $SUM['sum']}
        {assign var="COUNT38" value=$COUNT38 +1}   
    {/foreach}    
    <td>---</td>
  </tr>

  {* detalizacija pradzia *}
    <tr id="collapse72" class="collapse except" aria-labelledby="heading72" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$cell_phones_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($cell_phones_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$cell_phones_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *} 

   <tr>
    <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">158</span>
        <i id="fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>  
     <td class="titles" data-toggle="collapse" data-target="#collapse73" aria-expanded="true" aria-controls="collapse73">Mokesciai <span class="green-color">(be keliu, naudot techapziuros) <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top"  data-content="{$detalization['fees']['costcenter']}<ul>{foreach from=$detalization['fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT39 = 1}
    {foreach from=$fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td> 
       {$real_total_costs[$COUNT39][] = $SUM['sum']}
        {$total_rent_services_costs[$COUNT39][] = $SUM['sum']}
        {assign var="COUNT39" value=$COUNT39 +1}     
    {/foreach}    
    <td>---</td>
  </tr>

   {* detalizacija pradzia *}
    <tr id="collapse73" class="collapse except" aria-labelledby="heading73" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$fees_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($fees_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$fees_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *} 

{* Nera duomenu pabaiga *}   

  <tr>
    <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">159</span>
        <i id="insurance_servis_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse46" aria-expanded="true" aria-controls="collapse46">Draudimas <span class="green-color">(be krovininio transporto, kitas)</span> <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['insurance_servis_admin']['costcenter']}<ul>{foreach from=$detalization['insurance_servis_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT40 = 1}
    {foreach from=$insurance_servis_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$real_total_costs[$COUNT40][] = $SUM['sum']}
        {assign var="COUNT40" value=$COUNT40 +1}     
    {/foreach}     
    <td>{round($total_insurance_servis_admin, 2)}</td>
  </tr>

   {* detalizacija pradzia *}
    <tr id="collapse46" class="collapse except" aria-labelledby="heading46" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$insurance_servis_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($insurance_servis_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$insurance_servis_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *}  
  <tr>
    <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">160</span>
        <i id="inventory_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse47" aria-expanded="true" aria-controls="collapse47">Inventorius <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['inventory_admin']['costcenter']}<ul>{foreach from=$detalization['inventory_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
     {$COUNT41 = 1}  
    {foreach from=$inventory_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
       {$real_total_costs[$COUNT41][] = $SUM['sum']}
        {assign var="COUNT41" value=$COUNT41 +1}      
    {/foreach}     
    <td>{round($total_inventory_admin, 2)}</td>
  </tr>  

     {* detalizacija pradzia *}
    <tr id="collapse47" class="collapse except" aria-labelledby="heading47" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$inventory_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($inventory_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$inventory_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
  <tr>
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">161</span>
        <i id="ads_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
    </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse48" aria-expanded="true" aria-controls="collapse48">Reklama <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['ads_admin']['costcenter']}<ul>{foreach from=$detalization['ads_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT42 = 1}  
    {foreach from=$ads_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td> 
        {$real_total_costs[$COUNT42][] = $SUM['sum']}
        {assign var="COUNT42" value=$COUNT42 +1}     
    {/foreach}     
    <td>{round($total_ads_admin, 2)}</td>
  </tr> 

       {* detalizacija pradzia *}
    <tr id="collapse48" class="collapse except" aria-labelledby="heading48" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$ads_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($ads_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$ads_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *} 

  {* Kol kas crm nera siu duomenu *}
    <tr>
     <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">162</span>
          <i id="post_fees" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
     </div>
     </td>
      <td class="titles" data-toggle="collapse" data-target="#collapse73" aria-expanded="true" aria-controls="collapse73">Pašto išlaidos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['post_fees']['costcenter']}<ul>{foreach from=$detalization['post_fees']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
        {$COUNT43 = 1}     
    {foreach from=$post_fees item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
       {$real_total_costs[$COUNT43][] = $SUM['sum']}
        {assign var="COUNT43" value=$COUNT43 +1}   
    {/foreach}    
    <td>---</td>
  </tr>

       {* detalizacija pradzia *}
    <tr id="collapse73" class="collapse except" aria-labelledby="heading73" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$post_fees_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($post_fees_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$post_fees_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *} 


{* Nera duomenu pabaiga *}  

  <tr>
  <td>
    <div style="display:flex;">
      <span style="padding:5px 5px 0;">163</span>
      <i id="fuel_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
  </div>
  </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse49" aria-expanded="true" aria-controls="collapse49">BLANKAI krov vazt / admin kuras <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['fuel_admin']['costcenter']}<ul>{foreach from=$detalization['fuel_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
          {$COUNT44 = 1} 
    {foreach from=$fuel_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
          {$real_total_costs[$COUNT44][] = $SUM['sum']}
        {assign var="COUNT44" value=$COUNT44 +1}   
    {/foreach}     
    <td>{$total_fuel_admin}</td>
  </tr>
         {* detalizacija pradzia *}
    <tr id="collapse49" class="collapse except" aria-labelledby="heading49" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$fuel_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($fuel_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$fuel_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
  <tr>
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">164</span>
        <i id="other_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse50" aria-expanded="true" aria-controls="collapse50">Ūkio prekės <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['other_admin']['costcenter']}<ul>{foreach from=$detalization['other_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT45 = 1} 
    {foreach from=$other_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
        {$real_total_costs[$COUNT45][] = $SUM['sum']}
        {assign var="COUNT45" value=$COUNT45 +1}    
    {/foreach}     
    <td>{round($total_other_admin, 2)}</td>
  </tr>  

           {* detalizacija pradzia *}
    <tr id="collapse50" class="collapse except" aria-labelledby="heading50" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$other_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($other_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$other_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
  <tr>
  <td>
    <div style="display:flex;">
        <span style="padding:5px 5px 0;">165</span>
      <i id="other2_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
  </div>
  </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse51" aria-expanded="true" aria-controls="collapse51">Kanceliarinės prekės <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['other2_admin']['costcenter']}<ul>{foreach from=$detalization['other2_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>
        {$COUNT46 = 1}   
    {foreach from=$other2_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>  
        {$real_total_costs[$COUNT46][] = $SUM['sum']}
        {assign var="COUNT46" value=$COUNT46 +1}    
    {/foreach}     
    <td>{round($total_other2_admin, 2)}</td>
  </tr> 

{* detalizacija pradzia *}
    <tr id="collapse51" class="collapse except" aria-labelledby="heading51" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$other2_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($other2_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$other2_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>


{* detalizacija pabaiga *} 
  <tr>
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">166</span>
        <i id="other_fees_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse52" aria-expanded="true" aria-controls="collapse52">Kava, arbata, vanduo <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['other_fees_admin']['costcenter']}<ul>{foreach from=$detalization['other_fees_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
        {$COUNT47 = 1}  
    {foreach from=$other_fees_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
        {$real_total_costs[$COUNT47][] = $SUM['sum']}
        {assign var="COUNT47" value=$COUNT47 +1}  
    {/foreach}     
    <td>{round($total_other_fees_admin, 2)}</td>
  </tr>


  {* detalizacija pradzia *}
    <tr id="collapse52" class="collapse except" aria-labelledby="heading52" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$other_fees_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($other_fees_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$other_fees_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
  <tr>
   <td>
     <div style="display:flex;">
      <span style="padding:5px 5px 0;">167</span>
      <i id="representation_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
   </div>
   </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse53" aria-expanded="true" aria-controls="collapse53">Reprezentacinės išlaidos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['representation_admin']['costcenter']}<ul>{foreach from=$detalization['representation_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>
        {$COUNT48 = 1}    
    {foreach from=$representation_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>   
       {$real_total_costs[$COUNT48][] = $SUM['sum']}
        {assign var="COUNT48" value=$COUNT48 +1} 
    {/foreach}     
    <td>{round($total_representation_admin, 2)}</td>
  </tr>  

    {* detalizacija pradzia *}
    <tr id="collapse53" class="collapse except" aria-labelledby="heading53" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$representation_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($representation_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$representation_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>

{* detalizacija pabaiga *} 
  <tr>
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">168</span>
      <i id="it_admin" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
    </div>
    </td>
    <td class="titles" data-toggle="collapse" data-target="#collapse54" aria-expanded="true" aria-controls="collapse54">Kitos išlaidos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['it_admin']['costcenter']}<ul>{foreach from=$detalization['it_admin']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>  
       {$COUNT49 = 1}  
    {foreach from=$it_admin item=SUM} 
       <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>
         {$real_total_costs[$COUNT49][] = $SUM['sum']}
        {assign var="COUNT49" value=$COUNT49 +1}    
    {/foreach}     
    <td>{round($total_it_admin, 2)}</td>
  </tr> 

      {* detalizacija pradzia *}
    <tr id="collapse54" class="collapse except" aria-labelledby="heading54" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$it_detalization_admin['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($it_detalization_admin['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$it_detalization_admin['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *} 



  <tr>
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">169</span>
          <i id="transport_other_expense2" class="fa fa-pencil" style="padding:5px 5px 0;"></i>
      </div>
      </td>
       <td class="titles" data-toggle="collapse" data-target="#collapse75" aria-expanded="true" aria-controls="collapse75">Kitos išlaidos 2<i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$detalization['transport_other_expense2']['costcenter']}<ul>{foreach from=$detalization['transport_other_expense2']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {foreach from=$transport_other_expense2 item=SUM} 
         <td data-type="{$SUM['type']}" data-service_key="{$SUM['service_key']}" data-purchaseorderid="{$SUM['purchaseorderid']}">{$SUM['sum']}</td>
         {$transport_other_expense2_arr[] = $SUM['sum']}   
      {/foreach}   
      {$transport_other_expense2 = array_sum($transport_other_expense2_arr)}  
      <td>{round($transport_other_expense2, 2)}</td>
    </tr> 

     <tr id="collapse75" class="collapse except" aria-labelledby="heading75" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$transport_other_expense_detalization2['vendors'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($transport_other_expense_detalization2['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$transport_other_expense_detalization2['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}


      {$TOTAL_COST_OFFICE_OTHER_ARR = array()}
    <tr class="yealow-background red-color">
    <td>
      <div style="display:flex;">
        <span style="padding:5px 5px 0;">170</span>
      </div>  
    </td>
      <td class="titles">subTotal Exp real</td>  
     {for $len=1 to $num_rows}   
         {$SUM = array_sum($real_total_costs[$len]) + $transport_other_expense2_arr[$len-1]}
        <td>{$SUM}</td>   
         {$TOTAL_COST_OFFICE_OTHER_ARR[] = $SUM}  
         {$TOTAL_real_array[$len][] = $SUM}  
    {/for} 
      {$TOTAL_COST_OFFICE_OTHER = array_sum($TOTAL_COST_OFFICE_OTHER_ARR)}  
      <td>{round($TOTAL_COST_OFFICE_OTHER, 2)}</td>
    </tr>
    
    <tr class="light-blue-background">
      <td>
        <div style="display:flex;">
          <span style="padding:5px 5px 0;">171</span>
          <i id="other_profit" data-which="invoice" class="fa fa-pencil select_invoice_cost" style="padding:5px 5px 0;"></i>
        </div>
      </td>       
       <td class="titles blue-color" data-toggle="collapse" data-target="#collapse74" aria-expanded="true" aria-controls="collapse74">Kitos pajamos <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="{$invoiceDetalization['other_profit']['costcenter']}<ul>{foreach from=$invoiceDetalization['other_profit']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td>       
      {foreach from=$other_profit item=SUM} 
         <td data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>
         {$OTHER_PROFIT_TOTAL_ARR[] = $SUM['sum']}   
      {/foreach}   
      {$OTHER_PROFIT_TOTAL = array_sum($OTHER_PROFIT_TOTAL_ARR)}  
      <td>{round($OTHER_PROFIT_TOTAL, 2)}</td>
    </tr>  
          {* detalizacija pradzia *}
    <tr id="collapse74" class="collapse except" aria-labelledby="heading74" data-parent="#accordion">  
     <td></td>  
      <td style="padding-right: 0px;">         
        <ul>
          {foreach from=$other_profit_detalization['accounts'] item=VENDOR}   
            <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$VENDOR}</li> 
          {/foreach}
        </ul>
      </td>
      {for $len=1 to COUNT($other_profit_detalization['sum'])}    
          <td style="padding-left: 0px; padding-right: 0px;">
            <ul style="padding: 0px;">
              {foreach from=$other_profit_detalization['sum'][$len] item=SUM}                
                <li style="list-style: none; border-bottom: 1px solid #ddd; padding: 5px 0 5px 20px; height: 27px;">{$SUM}</li>               
              {/foreach}
            </ul>
          </td>
      {/for}
      <td></td>
    </tr>
{* detalizacija pabaiga *}

      {* Gaunam 184 eilutes skaicius *}     
      {foreach from=$other_cost key=key item=SUM}      
        {$TOTAL_real_array[$key+1][] = $SUM['sum']}       
      {/foreach}   

    {$TOTAL_REAL_ARR_SUM = array()}
    <tr class="yealow-background red-color">
    <td>
       <div style="display:flex;">
          <span style="padding:5px 5px 0;">172</span>
        </div>
    </td>
      <td class="titles">TOTAL real</td>  
     {for $len=1 to $num_rows}   
       {$SUM = array_sum($TOTAL_real_array[$len])}
        <td>{$SUM}</td>   
        {$TOTAL_REAL_ARR_SUM[] = $SUM} 
        {/for}   
      {$TOTAL_REAL = array_sum($TOTAL_REAL_ARR_SUM)}  
      <td>{round($TOTAL_REAL, 2)}</td>
    </tr>

    {* Gaunam 184 eilutes skaicius *}
    {$total_profit_without_list = []}
    {foreach from=$invoice_profit_without_list item=SUM} 
      {$total_profit_without_list[] = $SUM['sum']}      
    {/foreach}   


  {$TOTAL_SAILS_ARR = array()}
  <tr class="light-blue-background blue-color">
    <td>
     <div style="display:flex;">
          <span style="padding:5px 5px 0;">173</span>
      </div>
    </td>
    <td class="titles collapse_expand">Total Sails <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" 
      data-content="<ul>
                      <li>Transporto paslaugų pardavimas</li>                      
                      <li>EKSPEDICIJA paslaugų pardavimas</li>
                      <li>Servis paslaugų pardavimas</li>
                      <li>Perkraustymo paslaugų pardavimas</li>
                      <li>VILNIUS Sandelio paslaugų suma pajamos</li>
                      <li>KLAIPEDA Sandelio paslaugų suma pajamos</li>
                      <li>KAUNAS Sandelio paslaugų suma</li>   
                      <li>Kitos pajamos</li>                                
                    </ul>"</i></td>   
    {for $len=0 to $num_rows-1}    
     {$SUM = $TOTAL_TRANSPORT_SUM_ARRAY[$len] + $TOTAL_SERVIS_SUM_ARRAY[$len] + $FORWARDING_SUM_ARRAY[$len] + $TOTAL_MOVEMENT_SUM_ARRAY[$len] + $STORAGE_STEVEDORING_TOTAL_ARR[$len] + $TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR[$len] + $TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR[$len] + $OTHER_PROFIT_TOTAL_ARR[$len] + $total_profit_without_list[$len]}  
      <td>{$SUM}</td>       
      {$TOTAL_SAILS_ARR[] = $SUM} 
    {/for}

    {$TOTAL_SAILS = array_sum($TOTAL_SAILS_ARR)}
    <td>{round($TOTAL_SAILS, 2)}</td>
 </tr>

   {* detalizacija pradzia *}
    <tr class="collapse except expand_item">  
      <td></td>
      <td class="titles">Transporto paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$invoiceDetalization['transport']['costcenter']}<ul>{foreach from=$invoiceDetalization['transport']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
       {$TOTAL_TRANSPORT_SUM_ARRAY2 = array()}          
      {foreach from=$month_profit item=SUM} 
        <td style="cursor:pointer;" data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>   
        {$TOTAL_TRANSPORT_SUM_ARRAY2[] = $SUM['sum']}   
      {/foreach}   
      {$transport_total_sum2 = array_sum($TOTAL_TRANSPORT_SUM_ARRAY2)}     
      <td>{round($transport_total_sum2, 2)}</td>
    </tr>

    <tr class="collapse except expand_item"> 
        <td></td>
        <td class="titles blue-color">EKSPEDICIJA paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$invoiceDetalization['forwarding_profit']['costcenter']}<ul>{foreach from=$invoiceDetalization['forwarding_profit']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
      {$FORWARDING_SUM_ARRAY2 = array()}            
     {foreach from=$forwarding item=SUM} 
      <td style="cursor:pointer;" data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>   
      {$FORWARDING_SUM_ARRAY2[] = $SUM['sum']}    
      {/foreach}   
       {$total_forwarding2 = array_sum($FORWARDING_SUM_ARRAY2)}      
      <td>{round($total_forwarding2, 2)}</td>
    </tr>

    <tr class="collapse except expand_item"> 
      <td></td>
       <td class="titles">Servis paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$invoiceDetalization['servis']['costcenter']}<ul>{foreach from=$invoiceDetalization['servis']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
       {$TOTAL_SERVIS_SUM_ARRAY2 = array()}   
      {foreach from=$servis_profit item=SUM} 
        <td style="cursor:pointer;" data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>   
        {$TOTAL_SERVIS_SUM_ARRAY2[] = $SUM['sum']}   
      {/foreach}   
      {$servis_total_sum2 = array_sum($TOTAL_SERVIS_SUM_ARRAY2)}     
      <td>{round($servis_total_sum2, 2)}</td>
    </tr>

    <tr class="collapse except expand_item"> 
      <td></td>
      <td class="titles">Perkraustymo paslaugų pardavimas <i class="fa fa-info-circle" style="font-size: 15px;float: right;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$invoiceDetalization['movement']['costcenter']}<ul>{foreach from=$invoiceDetalization['movement']['cost'] item=COST}<li>{$COST}</li>{/foreach}</ul>"></i></td> 
       {$TOTAL_MOVEMENT_SUM_ARRAY2 = array()}   
      {foreach from=$movement_services_with_other_services item=SUM} 
        <td style="cursor:pointer;" data-type="{$SUM['type']}" data-service="{$SUM['service_key']}" data-invoiceid="{$SUM['invoiceid']}">{$SUM['sum']}</td>     
          {$TOTAL_MOVEMENT_SUM_ARRAY2[] = $SUM['sum']}  
      {/foreach} 
        {$total_parcel_marge2 = array_sum($TOTAL_MOVEMENT_SUM_ARRAY2)}        
      <td>{round($total_parcel_marge2, 2)}</td>
    </tr>

    <tr class="collapse except expand_item"> 
      <td></td>
      <td class="titles grey-color">VILNIUS Sandelio paslaugų suma pajamos</td>  
      {$STORAGE_STEVEDORING_TOTAL_ARR2 = array()}   
      {for $len=0 to $num_rows-1}       
        {$SUM = $STORAGE_TOTAL_ARR[$len] + $STEVEDORING_TOTAL_ARR[$len]}
        <td>{$SUM}</td> 
        {$STORAGE_STEVEDORING_TOTAL_ARR2[] = $STORAGE_TOTAL_ARR[$len] + $STEVEDORING_TOTAL_ARR[$len]}   
      {/for}  
      <td>{round(array_sum($STORAGE_STEVEDORING_TOTAL2_ARR2), 2)}</td>    
    </tr>

    <tr class="collapse except expand_item"> 
      <td></td>
      <td class="titles grey-color">KLAIPEDA Sandelio paslaugų suma pajamos</td>  
      {$TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR2 = array()}   
     {for $len=0 to $num_rows-1}  
        {$SUM = $STORAGE_KLAIPEDA_TOTAL_ARR[$len] + $TOTAL_STEVEDORING_ARR[$len]}
        <td>{$SUM}</td> 
        {$TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR2[] = $SUM}   
      {/for}        
      <td>{round(array_sum($TOTAL_STEVEDORING_STORAGE_KLAIPEDA_ARR2), 2)}</td>
    </tr>

    <tr class="collapse except expand_item"> 
      <td></td>
      <td class="titles blue-color">KAUNAS Sandelio paslaugų suma</td>  
      {$TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR2 = array()}   
     {for $len=0 to $num_rows-1}  
        {$SUM = $STORAGE_KAUNAS_TOTAL_ARR[$len] + $TOTAL_STEVEDORING_KAUNAS_ARR[$len]}
        <td>{$SUM}</td> 
        {$TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR2[] = $SUM}   
      {/for}       
      <td>{round(array_sum($TOTAL_STEVEDORING_STORAGE_KAUNAS_ARR2), 2)}</td>
    </tr>

    <tr class="collapse except expand_item"> 
      <td></td>
      <td class="titles blue-color">Kitos pajamos</td>  
      {$OTHER_PROFIT_TOTAL_ARR_2 = array()}   
     {for $len=0 to $num_rows-1}         
        <td>{$OTHER_PROFIT_TOTAL_ARR[$len]}</td> 
        {$OTHER_PROFIT_TOTAL_ARR_2[] = $SUM}   
      {/for}       
      <td>{round(array_sum($OTHER_PROFIT_TOTAL_ARR_2), 2)}</td>
    </tr>  
{* detalizacija pabaiga *}

  {* Kol kas crm nera siu duomenu *}
  <tr class="yealow-background">
  <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">174</span>
    </div>
  </td>
    <td class="titles ">TOTAL darbuotoju skaičius</td>  
    {$final_total_employees_count_arr = []} 
    {for $len=1 to $num_rows}       
      <td>{array_sum($total_employees_count_arr[$len])}</td>   
      {$final_total_employees_count_arr[] = array_sum($total_employees_count_arr[$len])}
    {/for}
    <td>{round(array_sum($final_total_employees_count_arr), 2)}</td>
  </tr>

  <tr class="clickable" data-row-title="working_days_number">
   <td class="not-clickable">
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">177</span>
    </div>
   </td>
    <td class="titles not-clickable">Darbo dienų  mėnesio skaičius</td>   
     {$total_working_days_number = []} 
      {for $len=1 to $num_rows}  
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
            {if $getHandWritedSums['working_days_number'][$len]}{$getHandWritedSums['working_days_number'][$len]}{else}0{/if}
            {$total_working_days_number[] = $getHandWritedSums['working_days_number'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>   
      {/for}
    <td class="not-clickable">{round(array_sum($total_working_days_number), 2)}</td>
  </tr>
{* Nera duomenu pabaiga *}

  <tr class="yealow-background">
  <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">178</span>
    </div>
  </td>
    <td class="titles">pelningumas %</td>   
    {for $len=0 to $num_rows-1}     
    {$SUM = ($TOTAL_SAILS_ARR[$len]-$TOTAL_REAL_ARR_SUM[$len])/$TOTAL_SAILS_ARR[$len]*100}
      <td>{round($SUM, 2)}%</td>
    {/for}
    <td>{round((($TOTAL_SAILS-$TOTAL_REAL)/$TOTAL_SAILS*100), 2)}%</td>
  </tr> 

  {$TOTAL_PROFIT_BEFORE_FEES_ARR = array()}
  <tr class="yealow-background">
  <td>
   <div style="display:flex;">
        <span style="padding:5px 5px 0;">179</span>
    </div>
  </td>
    <td class="titles">pelnas pries mokescius</td>   
    {for $len=0 to $num_rows-1} 
     {$SUM = $TOTAL_SAILS_ARR[$len]-$TOTAL_REAL_ARR_SUM[$len]}      
      <td>{round($SUM, 2)}</td>  
      {$TOTAL_PROFIT_BEFORE_FEES_ARR[] = $SUM} 
    {/for}
    {$TOTAL_PROFIT_BEFORE_FEES = array_sum($TOTAL_PROFIT_BEFORE_FEES_ARR)}
    <td>{round($TOTAL_PROFIT_BEFORE_FEES, 2)}</td>
  </tr>   


  {* Kol kas crm nera siu duomenu *}
  <tr class="clickable" data-row-title="total_vat">
   <td class="not-clickable">
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">180</span>
    </div>
   </td>
    <td class="titles blue-color not-clickable">PVM 21%</td>   
    {$total_total_vat = []} 
      {for $len=1 to $num_rows}  
        <td data-row-id="{$len}" class="independent">
          <span class="rowValue">
            {if $getHandWritedSums['total_vat'][$len]}{$getHandWritedSums['total_vat'][$len]}{else}0{/if}
            {$total_total_vat[] = $getHandWritedSums['total_vat'][$len]}
          </span> 
          <img style="display: none;" src="/resources/ajax-loader.gif"> 
          <div class="hiddenInputBlock" style="display:none;">
            <input class="hiddenInfoInput" type="text"> 
            <button type="button" class="btn btn-success btn-sm" style="padding:1px 4px;" ><i class="fa fa-check" onclick="btnOk(event);"></i></button>
            <button type="button" class="btn btn-danger btn-sm" style="padding:1px 5px;" onclick="btnCancel(event);">x</button>
          </div>
        </td>   
      {/for}
    <td class="not-clickable">{round(array_sum($total_total_vat), 2)}</td>
  </tr>
{* Nera duomenu pabaiga *}



  <tr>
   <td>
     <div style="display:flex;">
        <span style="padding:5px 5px 0;">183</span>
    </div>
   </td>
    <td class="titles">Kaštai nepakliuve į sarašą</td>        
      {foreach from=$other_cost item=SUM}      
       <td data-month="{$SUM['month']}">{$SUM['sum']}</td>   
      {/foreach}   
    <td>{round($sum_other_cost, 2)}</td>
  </tr>  

    <tr>
   <td>
    <div style="display:flex;">
        <span style="padding:5px 5px 0;">184</span>
    </div>
   </td>
    <td class="titles">Pajamos nepakliuvę i sąrašą</td>        
      {foreach from=$invoice_profit_without_list item=SUM}       
       <td data-year="{$year}" data-month="{$SUM['month']}">{$SUM['sum']}</td>   
      {/foreach}   
    <td>{round($sum_invoice_profit_without_list, 2)}</td>
  </tr>   


</tbody>
</table>  

</table>  
 
 
<div style="height: 100px;"></div>
</div>

