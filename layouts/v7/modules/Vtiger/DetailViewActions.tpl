{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	{assign var=AGNUM_EXPORTED value=$AGNUM_EXPORTED}
	{assign var=INVOICE_TYPE value=$INVOICE_TYPE}
	{assign var=CONVERTED value=$CONVERTED}
    <div class="col-lg-6 detailViewButtoncontainer">
        <div class="pull-right btn-toolbar">
            <div class="btn-group">
            {if $MODULE_NAME eq 'Invoice'}                         
              {if (trim($INVOICE_TYPE) neq 'Debetinė' && trim($INVOICE_TYPE) neq 'Kreditinė') && empty($CONVERTED)}
               <button class="btn btn-default" onclick="window.location.href = 'index.php?module=Invoice&view=Detail&record={$RECORD->getId()}&generateToDebit=true'">{vtranslate('LBL_CONVERT_INVOICE_TO_DEBIT', $MODULE_NAME)}</button>       
              {/if}
            {/if}

            {assign var=STARRED value=$RECORD->get('starred')}
            {if $MODULE_MODEL->isStarredEnabled()}
                <button class="btn btn-default markStar {if $STARRED} active {/if}" id="starToggle" style="width:100px;">
                    <div class='starredStatus' title="{vtranslate('LBL_STARRED', $MODULE)}">
                        <div class='unfollowMessage'>
                            <i class="fa fa-star-o"></i> &nbsp;{vtranslate('LBL_UNFOLLOW',$MODULE)}
                        </div>
                        <div class='followMessage'>
                            <i class="fa fa-star active"></i> &nbsp;{vtranslate('LBL_FOLLOWING',$MODULE)}
                        </div>
                    </div>
                    <div class='unstarredStatus' title="{vtranslate('LBL_NOT_STARRED', $MODULE)}">
                        {vtranslate('LBL_FOLLOW',$MODULE)}
                    </div>
                </button>
            {/if}
           
            {foreach item=DETAIL_VIEW_BASIC_LINK from=$DETAILVIEW_LINKS['DETAILVIEWBASIC']}
                {if !$AGNUM_EXPORTED}
                <button class="btn btn-default" id="{$MODULE_NAME}_detailView_basicAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_BASIC_LINK->getLabel())}"
                        {if $DETAIL_VIEW_BASIC_LINK->isPageLoadLink()}
                            onclick="window.location.href = '{$DETAIL_VIEW_BASIC_LINK->getUrl()}&app={$SELECTED_MENU_CATEGORY}{if Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_BASIC_LINK->getLabel()) eq 'LBL_EDIT' && $RECORD->checkInvoiceType()}&newRecord=true{/if}'"
                        {else}
                            onclick="{$DETAIL_VIEW_BASIC_LINK->getUrl()}"
                        {/if}
                        {if $MODULE_NAME eq 'Documents' && $DETAIL_VIEW_BASIC_LINK->getLabel() eq 'LBL_VIEW_FILE'}
                            data-filelocationtype="{$DETAIL_VIEW_BASIC_LINK->get('filelocationtype')}" data-filename="{$DETAIL_VIEW_BASIC_LINK->get('filename')}"
                        {/if}>
                    {vtranslate($DETAIL_VIEW_BASIC_LINK->getLabel(), $MODULE_NAME)}
                </button>
                {/if}
            {/foreach}   


{if $MODULE_NAME eq 'Invoice'}   
<div class="modal fade" id="sendInvoice" tabindex="-1" role="dialog" aria-labelledby="sendInvoiceLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 35%;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendInvoiceLabel">Sąskaitos siuntimas klientui el. paštu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="sendEmail" enctype="multipart/form-data"  onkeydown="return event.key != 'Enter';">
         <div class="form-group">
          <input name="emailAddress" type="email" class="form-control" placeholder="Kliento el. pašto adresas" required>
            </div>
          <div class="form-group">
            <textarea class="form-control" rows="5" name="message" placeholder="Žinutė"></textarea>
          </div>
        <div class="form-group">
          	<input type="file" class="form-control" name="files[]" style="margin-top: 15px;" multiple>
        </div>
        <div class="form-group">
            <label for="ordersFiles">Failai iš užsakymų</label>
            <small style="display:block;">Norint pasirinkti daugiau nei vieną laikykite nuspaudę Ctrl mygtuką.</small>
            <select name="ordersFiles[]" id="ordersFiles" class="form-control" multiple></select>
            <input type="hidden" id="deleteFiles" name="deleteFiles" value="">
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Atšaukti</button>
        <button type="button" class="btn btn-primary" onclick="sendInvoice()">Siųsti Sąskaitą <i class="fa fa-paper-plane"></i></button>
      </div>
    </div>
  </div>
</div>
{/if}

            {* ITOMA     *} 
             {if $MODULE_NAME eq 'Invoice'}                
                {foreach item=DETAIL_VIEW_LINK from=$DETAILVIEW_LINKS['DETAILVIEW']}              
                {if {Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())} eq 'Spausdinti' or {Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())} eq 'Print'}                 
                    <button class="btn btn-default" onclick="window.location.href = '{$DETAIL_VIEW_LINK->getUrl()}'">{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</button>    
                         <button class="btn btn-default" onclick="window.location.href = 'vtlib/Vtiger/exel?invoice={$RECORD->getId()}'">Spausdinti Excel</button>             
                {/if}
                {/foreach}
            {/if}
            {if $DETAILVIEW_LINKS['DETAILVIEW']|@count gt 0}
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                   {vtranslate('LBL_MORE', $MODULE_NAME)}&nbsp;&nbsp;<i class="caret"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    {foreach item=DETAIL_VIEW_LINK from=$DETAILVIEW_LINKS['DETAILVIEW']}
                        {if $DETAIL_VIEW_LINK->getLabel() eq ""} 
                            <li class="divider"></li>	
                            {else}
                            <li id="{$MODULE_NAME}_detailView_moreAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())}">
                            {if {Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())} neq 'Spausdinti'}
                            {if $MODULE_NAME eq 'Invoice' && $DETAIL_VIEW_LINK->getUrl() eq 'none'} 
                               <a href="#" data-toggle="modal" data-target="#sendInvoice" onclick="getUserEmailAndSalesOrderFilesByInvoiceId();">{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>                
                             {else}
                                {if $DETAIL_VIEW_LINK->getUrl()|strstr:"javascript"} 
                                    <a href='{$DETAIL_VIEW_LINK->getUrl()}'>{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>
                                {else}
                                    <a href='{$DETAIL_VIEW_LINK->getUrl()}&app={$SELECTED_MENU_CATEGORY}' >{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>
                                {/if}
                            {/if}

                            {/if}  
                            </li>
                     
                        {/if}
                    {/foreach}
                </ul>
            {/if}
            </div>
            {if !{$NO_PAGINATION}}
            <div class="btn-group pull-right">
                <button class="btn btn-default " id="detailViewPreviousRecordButton" {if empty($PREVIOUS_RECORD_URL)} disabled="disabled" {else} onclick="window.location.href = '{$PREVIOUS_RECORD_URL}&app={$SELECTED_MENU_CATEGORY}'" {/if} >
                    <i class="fa fa-chevron-left"></i>
                </button>
                <button class="btn btn-default  " id="detailViewNextRecordButton"{if empty($NEXT_RECORD_URL)} disabled="disabled" {else} onclick="window.location.href = '{$NEXT_RECORD_URL}&app={$SELECTED_MENU_CATEGORY}'" {/if}>
                    <i class="fa fa-chevron-right"></i>
                </button>
            </div>
            {/if}        
        </div>
        <input type="hidden" name="record_id" value="{$RECORD->getId()}">
    </div>


{strip}
