{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
	{if !empty($PICKIST_DEPENDENCY_DATASOURCE)}
		<input type="hidden" name="picklistDependency" value='{Vtiger_Util_Helper::toSafeHTML($PICKIST_DEPENDENCY_DATASOURCE)}' />
	{/if}
	 {assign var=CHECK_SALESORDER_INVOICE value=$CHECK_SALESORDER_INVOICE}
	 	{assign var=PREINVOICE value=$CHECK_SALESORDER_PREINVOICE}
	 	{assign var=external_order_id value=$external_order_id}
	 	{assign var=withoutinvoice value=$withoutinvoice}
	 	{assign var=RECORD_ID value=$salesorderid}
		{assign var=AGNUM_EXPORTED value=$AGNUM_EXPORTED}
  <input type="hidden"  id="show_edit_button" value="{if $CHECK_SALESORDER_INVOICE}1{else}0{/if}">



	{foreach key=BLOCK_LABEL_KEY item=FIELD_MODEL_LIST from=$RECORD_STRUCTURE}
		{assign var=BLOCK value=$BLOCK_LIST[$BLOCK_LABEL_KEY]}
		{if $BLOCK eq null or $FIELD_MODEL_LIST|@count lte 0}{continue}{/if}
		{if $BLOCK_LABEL_KEY neq 'LBL_ADDRESS_INFORMATION' && $BLOCK_LABEL_KEY neq 'LBL_TERMS_INFORMATION'}
		<div class="block block_{$BLOCK_LABEL_KEY}" data-block="{$BLOCK_LABEL_KEY}" data-blockid="{$BLOCK_LIST[$BLOCK_LABEL_KEY]->get('id')}">
			{assign var=IS_HIDDEN value=$BLOCK->isHidden()}
			{assign var=WIDTHTYPE value=$USER_MODEL->get('rowheight')}
			<input type=hidden name="timeFormatOptions" data-value='{$DAY_STARTS}' />
			<div>
				<h4 class="textOverflowEllipsis maxWidth50">
					<img class="cursorPointer alignMiddle blockToggle {if !($IS_HIDDEN)} hide {/if}" src="{vimage_path('arrowRight.png')}" data-mode="hide" data-id={$BLOCK_LIST[$BLOCK_LABEL_KEY]->get('id')}>
					<img class="cursorPointer alignMiddle blockToggle {if ($IS_HIDDEN)} hide {/if}" src="{vimage_path('arrowdown.png')}" data-mode="show" data-id={$BLOCK_LIST[$BLOCK_LABEL_KEY]->get('id')}>&nbsp;
					{vtranslate({$BLOCK_LABEL_KEY},{$MODULE_NAME})}					
				</h4>
				{if $PREINVOICE && $BLOCK_LIST[$BLOCK_LABEL_KEY]->get('id') eq '61'} <p style="color: red;">Yra išrašyta išankstinė saskaita</p>{/if}
				{if (empty($external_order_id) && $withoutinvoice eq 0) && $BLOCK_LIST[$BLOCK_LABEL_KEY]->get('id') eq '61'}
				<i title="Įvyko klaida užsąkymas neįkeltas į metriką!" style="font-size: 60px; color: red;" class="fa fa-exclamation-circle"></i>
				<p style="position: absolute;color: red;display: inline-flex;margin-left: 14px;margin-top: 22px;">Įvyko klaida užsąkymas neįkeltas į metriką!</p>
				{/if}
			</div>
			<hr>

			<div class="blockData">
				<table class="table detailview-table no-border">
					<tbody {if $IS_HIDDEN} class="hide" {/if}>
						{assign var=COUNTER value=0}
						<tr>					
							{foreach item=FIELD_MODEL key=FIELD_NAME from=$FIELD_MODEL_LIST}
								{assign var=fieldDataType value=$FIELD_MODEL->getFieldDataType()}						
								{if !$FIELD_MODEL->isViewableInDetailView()}
									{continue}
								{/if}
								{if $FIELD_MODEL->get('uitype') eq "83"}
									{foreach item=tax key=count from=$TAXCLASS_DETAILS}
										{if $COUNTER eq 2}
											</tr><tr>
											{assign var="COUNTER" value=1}
										{else}
											{assign var="COUNTER" value=$COUNTER+1}
										{/if}
										<td class="fieldLabel {$WIDTHTYPE}">
											<span class='muted'>{vtranslate($tax.taxlabel, $MODULE)}(%)</span>
										</td>
										<td class="fieldValue {$WIDTHTYPE}">
											<span class="value textOverflowEllipsis" data-field-type="{$FIELD_MODEL->getFieldDataType()}" >
												{if $tax.check_value eq 1}
													{$tax.percentage}
												{else}
													0
												{/if} 
											</span>
										</td>
									{/foreach}
								{else if $FIELD_MODEL->get('uitype') eq "69" || $FIELD_MODEL->get('uitype') eq "105"}
									{if $COUNTER neq 0}
										{if $COUNTER eq 2}
											</tr>
											<tr>
											{assign var=COUNTER value=0}
										{/if}
									{/if}

									

									<td class="fieldLabel {$WIDTHTYPE}"><span class="muted">{vtranslate({$FIELD_MODEL->get('label')},{$MODULE_NAME})}</span></td>
									<td class="fieldValue {$WIDTHTYPE}">
										<ul id="imageContainer">
											{foreach key=ITER item=IMAGE_INFO from=$IMAGE_DETAILS}
												{if !empty($IMAGE_INFO.path) && !empty({$IMAGE_INFO.orgname})}
													<li><img src="{$IMAGE_INFO.path}_{$IMAGE_INFO.orgname}" title="{$IMAGE_INFO.orgname}" width="400" height="300" /></li>
												{/if}
											{/foreach}
										</ul>
									</td>
							
									{assign var=COUNTER value=$COUNTER+1}
								{else}
									{if $FIELD_MODEL->get('uitype') eq "20" or $FIELD_MODEL->get('uitype') eq "19" or $fieldDataType eq 'reminder' or $fieldDataType eq 'recurrence'}
										{if $COUNTER eq '1'}
											<td class="fieldLabel {$WIDTHTYPE}"></td><td class="{$WIDTHTYPE}"></td></tr><tr>
											{assign var=COUNTER value=0}
										{/if}
									{/if}
									{if $COUNTER eq 2}
										</tr><tr>
										{assign var=COUNTER value=1}
									{else}
										{assign var=COUNTER value=$COUNTER+1}
									{/if}

								{if $FIELD_MODEL->getName() neq 'cf_20023' && $FIELD_MODEL->getName() neq 'cf_2720'}
									<td class="fieldLabel textOverflowEllipsis {$WIDTHTYPE}" id="{$MODULE_NAME}_detailView_fieldLabel_{$FIELD_MODEL->getName()}" {if $FIELD_MODEL->getName() eq 'description' or $FIELD_MODEL->get('uitype') eq '69'} style='width:8%'{/if}>
										<span class="muted">
											{if $MODULE_NAME eq 'Documents' && $FIELD_MODEL->get('label') eq "File Name" && $RECORD->get('filelocationtype') eq 'E'}
												{vtranslate("LBL_FILE_URL",{$MODULE_NAME})}
											{else}
												{vtranslate({$FIELD_MODEL->get('label')},{$MODULE_NAME})}
											{/if}
											{if ($FIELD_MODEL->get('uitype') eq '72') && ($FIELD_MODEL->getName() eq 'unit_price')}
												({$BASE_CURRENCY_SYMBOL})
											{/if}
										</span>
									</td>{/if}
									<td class="fieldValue {$WIDTHTYPE}" id="{$MODULE_NAME}_detailView_fieldValue_{$FIELD_MODEL->getName()}" {if $FIELD_MODEL->get('uitype') eq '19' or $fieldDataType eq 'reminder' or $fieldDataType eq 'recurrence'} colspan="3" {assign var=COUNTER value=$COUNTER+1} {/if}>
										{assign var=FIELD_VALUE value=$FIELD_MODEL->get('fieldvalue')}
										{if $fieldDataType eq 'multipicklist'}
											{assign var=FIELD_DISPLAY_VALUE value=$FIELD_MODEL->getDisplayValue($FIELD_MODEL->get('fieldvalue'))}
										{else}
											{assign var=FIELD_DISPLAY_VALUE value=Vtiger_Util_Helper::toSafeHTML($FIELD_MODEL->getDisplayValue($FIELD_MODEL->get('fieldvalue')))}
										{/if}
										{if $FIELD_MODEL->getName() eq 'cf_2722'}
											<input type="hidden" id="road_tax_fee" value="{$ROAD_TAX_FEE}">
										{/if}
										<span class="value {if $FIELD_MODEL->getName() == 'consignee'}consignee_value{/if}" data-field-type="{$FIELD_MODEL->getFieldDataType()}" {if $FIELD_MODEL->get('uitype') eq '19' or $FIELD_MODEL->get('uitype') eq '21'} style="white-space:normal;" {/if}>
											{include file=vtemplate_path($FIELD_MODEL->getUITypeModel()->getDetailViewTemplateName(),$MODULE_NAME) FIELD_MODEL=$FIELD_MODEL USER_MODEL=$USER_MODEL MODULE=$MODULE_NAME RECORD=$RECORD}
										</span>									
										{if $IS_AJAX_ENABLED && $FIELD_MODEL->isEditable() eq 'true' && $FIELD_MODEL->isAjaxEditable() eq 'true'}
											<span class="hide edit pull-left">
												{if $fieldDataType eq 'multipicklist'}
													<input type="hidden" class="fieldBasicData" data-name='{$FIELD_MODEL->get('name')}[]' data-type="{$fieldDataType}" data-displayvalue='{$FIELD_DISPLAY_VALUE}' data-value="{$FIELD_VALUE}" />
												{else}
													<input type="hidden" class="fieldBasicData" data-name='{$FIELD_MODEL->get('name')}' data-type="{$fieldDataType}" data-displayvalue='{$FIELD_DISPLAY_VALUE}' data-value="{$FIELD_VALUE}" />
												{/if}
											</span>

											{if $FIELD_MODEL->getName() neq 'cf_1374' && $FIELD_MODEL->getName() neq 'cf_1376' && $FIELD_MODEL->getName() neq 'cf_1378' && $FIELD_MODEL->getName() neq 'cf_1663' && $FIELD_MODEL->getName() neq 'cf_1661' && $FIELD_MODEL->getName() neq 'stevedoringno' && $FIELD_MODEL->getName() neq 'cf_20023' && $FIELD_MODEL->getName() neq 'pricebook' && $FIELD_MODEL->getName() neq 'cf_2674'}

												{if !$CHECK_SALESORDER_INVOICE}
													{if $MODULE eq 'Accounts'}
														{$COLUMNS=['cf_1218' => 'cf_1216', 'cf_1222' => 'cf_1220', 'cf_1226' => 'cf_1224', 'cf_1230' => 'cf_1228', 'cf_1234' => 'cf_1232','cf_1238' => 'cf_1236', 'cf_1242' => 'cf_1240', 'cf_1246' => 'cf_1244', 'cf_1250' => 'cf_1248', 'cf_1254' => 'cf_1252','cf_1400' => 'cf_1398', 'cf_1404' => 'cf_1402', 'cf_1408' => 'cf_1406', 'cf_1412' => 'cf_1410', 'cf_1416' => 'cf_1414', 'cf_1420' => 'cf_1418', 'cf_1424' => 'cf_1422', 'cf_1428' => 'cf_1426', 'cf_1432' => 'cf_1430', 'cf_1436' => 'cf_1434']}										
													{/if}	

													{if $FIELD_MODEL->getName() neq 'load_date_from' AND  $FIELD_MODEL->getName() neq 'unload_date_from' AND $FIELD_MODEL->getName() neq 'cf_2667' AND $FIELD_MODEL->getName() neq 'cf_1277'}
														{if !$AGNUM_EXPORTED}
															<span class="action pull-right"><a href="#" onclick="return false;" class="editAction fa fa-pencil" {if $MODULE eq 'Accounts'}data-column="{$FIELD_MODEL->getName()}" 
														data-type-column="{$COLUMNS[$FIELD_MODEL->getName()]}"{/if}></a></span>
														{/if}
													{/if}
												{/if}

											{/if}
									{/if}
										
										{if $MODULE eq 'SalesOrder' && ($FIELD_MODEL->getName() eq 'load_date_from' OR  $FIELD_MODEL->getName() eq 'unload_date_from')}
											<span class="value" style="margin-left: 20px;">									
												{$TIME_FIELD_NAME_FROM = ($FIELD_NAME == 'load_date_from') ? 'load_time_from' : 'unload_time_from'} &nbsp;{$FIELD_MODEL->getOrderTime($RECORD_ID, $TIME_FIELD_NAME_FROM)} /	{$TIME_FIELD_NAME_TO = ($FIELD_NAME == 'load_date_from') ? 'load_time_to' : 'unload_time_to'}	{$FIELD_MODEL->getOrderTime($RECORD_ID, $TIME_FIELD_NAME_TO)}
											</span>
											{/if}			
									
									</td>
								{/if}							

								{if $FIELD_MODEL_LIST|@count eq 1 and $FIELD_MODEL->get('uitype') neq "19" and $FIELD_MODEL->get('uitype') neq "20" and $FIELD_MODEL->get('uitype') neq "30" and $FIELD_MODEL->get('name') neq "recurringtype" and $FIELD_MODEL->get('uitype') neq "69" and $FIELD_MODEL->get('uitype') neq "105"}
									<td class="fieldLabel {$WIDTHTYPE}"></td><td class="{$WIDTHTYPE}"></td>
								{/if}
							{/foreach}
							{* adding additional column for odd number of fields in a block *}
							{if $FIELD_MODEL_LIST|@end eq true and $FIELD_MODEL_LIST|@count neq 1 and $COUNTER eq 1}
								<td class="fieldLabel {$WIDTHTYPE}"></td><td class="{$WIDTHTYPE}"></td>
							{/if}
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<br>
		{/if}
	
	{/foreach}
{/strip}
