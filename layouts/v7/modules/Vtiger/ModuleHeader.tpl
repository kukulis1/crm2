{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
		{assign var=real_module_name value=$REAL_MODULE_NAME}
		{assign var=SHOW_EXPORT value=$SHOW_EXPORT}
		{assign var=ROLE value=$ROLE_ID}	



	<div class="col-sm-12 col-xs-12 module-action-bar clearfix coloredBorderTop">
		<div class="module-action-content clearfix {$MODULE}-module-action-content">
			<div class="col-lg-5 col-md-5 module-breadcrumb module-breadcrumb-{$smarty.request.view} transitionsAllHalfSecond">
				{assign var=MODULE_MODEL value=Vtiger_Module_Model::getInstance($MODULE)}
				{if $MODULE_MODEL->getDefaultViewName() neq 'List'}
					{assign var=DEFAULT_FILTER_URL value=$MODULE_MODEL->getDefaultUrl()}
				{else}
					{assign var=DEFAULT_FILTER_ID value=$MODULE_MODEL->getDefaultCustomFilter()}
					{if $DEFAULT_FILTER_ID}
						{assign var=CVURL value="&viewname="|cat:$DEFAULT_FILTER_ID}
						{assign var=DEFAULT_FILTER_URL value=$MODULE_MODEL->getListViewUrl()|cat:$CVURL}
					{else}
						{assign var=DEFAULT_FILTER_URL value=$MODULE_MODEL->getListViewUrlWithAllFilter()}
					{/if}
				{/if}
				<a title="{vtranslate($MODULE, $MODULE)}" href='{$DEFAULT_FILTER_URL}&app={$SELECTED_MENU_CATEGORY}'><h4 class="module-title pull-left text-uppercase"> {vtranslate($MODULE, $MODULE)} </h4>&nbsp;&nbsp;</a>
				{if $smarty.session.lvs.$MODULE.viewname}
					{assign var=VIEWID value=$smarty.session.lvs.$MODULE.viewname}
				{/if}
				{if $VIEWID}
					{foreach item=FILTER_TYPES from=$CUSTOM_VIEWS}
						{foreach item=FILTERS from=$FILTER_TYPES}
							{if $FILTERS->get('cvid') eq $VIEWID}
								{assign var=CVNAME value=$FILTERS->get('viewname')}
								{break}
							{/if}
						{/foreach}
					{/foreach}
					<p class="current-filter-name filter-name pull-left cursorPointer" title="{$CVNAME}"><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a href='{$MODULE_MODEL->getListViewUrl()}&viewname={$VIEWID}&app={$SELECTED_MENU_CATEGORY}'>&nbsp;&nbsp;{$CVNAME}&nbsp;&nbsp;{if $MODULE eq 'Newpricebooks'}Visi{/if}</a> </p>
				{/if}
				{assign var=SINGLE_MODULE_NAME value='SINGLE_'|cat:$MODULE}
				{if $RECORD and $smarty.request.view eq 'Edit'}
					<p class="current-filter-name filter-name pull-left "><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a title="{$RECORD->get('label')}">&nbsp;&nbsp;{vtranslate('LBL_EDITING', $MODULE)} : {$RECORD->get('label')} &nbsp;&nbsp;</a></p>
				{else if $smarty.request.view eq 'Edit'}
					<p class="current-filter-name filter-name pull-left "><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a>&nbsp;&nbsp;{vtranslate('LBL_ADDING_NEW', $MODULE)}&nbsp;&nbsp;</a></p>
				{/if}
				{if $smarty.request.view eq 'Detail'}
					<p class="current-filter-name filter-name pull-left"><span class="fa fa-angle-right pull-left" aria-hidden="true"></span><a title="{$RECORD->get('label')}">&nbsp;&nbsp;{$RECORD->get('label')} &nbsp;&nbsp;</a></p>
				{/if}
			</div>
			<div class="col-lg-7 col-md-7 pull-right">
				{if $real_module_name neq 'Unpaidinvoices' && $real_module_name neq 'Purchaseinvoice' && $real_module_name neq 'Payment' && $real_module_name neq 'Exportpayment'}
				<div id="appnav" class="navbar-right">
					<ul class="nav navbar-nav">
					{if $MODULE eq 'Accounts'}
						<li>
							<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;" class="btn addButton btn-primary" onclick="window.open('/faq/faq.php#Accounts' , '_blank');">Pagalba <i class="fa fa-question"></i></button>
						</li>
					{else if $MODULE eq 'Invoice'}		
						<li>
							<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;" class="btn addButton btn-primary" onclick="window.open('/faq/faq.php#Invoice' , '_blank');">Pagalba <i class="fa fa-question"></i></button>
						</li>
					{else if $MODULE eq 'Newpricebooks'}		
						<li>
							<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;" class="btn addButton btn-primary" onclick="window.open('/faq/faq.php#PriceBooks' , '_blank');">Pagalba <i class="fa fa-question"></i></button>
						</li>
						{else if $MODULE eq 'Vendors'}		
							<li>
								<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;" class="btn addButton btn-primary" onclick="window.open('/faq/faq.php#Vendors' , '_blank');">Pagalba <i class="fa fa-question"></i></button>
							</li>
					{else}		
						<li>
							<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;" class="btn addButton btn-primary" onclick="window.open('/faq/faq.php' , '_blank');">Pagalba <i class="fa fa-question"></i></button>
						</li>
					{/if}
						{foreach item=BASIC_ACTION from=$MODULE_BASIC_ACTIONS}			

							{if $BASIC_ACTION->getLabel() == 'LBL_IMPORT'}

							{if $MODULE eq 'SalesOrder'}
								<li>
								<button style="padding: 6px 12px; margin-right: 10px; margin-top: 4px;"	class="btn addButton btn-default module-buttons" data-toggle="modal" data-target="#importFromExel">
									<div class="fa fa-download" aria-hidden="true"></div>&nbsp;&nbsp;Importuoti
								</button>
							</li>
							{else}
								<li>							
									<button id="{$MODULE}_basicAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($BASIC_ACTION->getLabel())}" type="button" class="btn addButton btn-default module-buttons" 
											{if stripos($BASIC_ACTION->getUrl(), 'javascript:')===0}  
												onclick='{$BASIC_ACTION->getUrl()|substr:strlen("javascript:")};'
											{else}
												onclick="Vtiger_Import_Js.triggerImportAction('{$BASIC_ACTION->getUrl()}')"
											{/if}>
										<div class="fa {$BASIC_ACTION->getIcon()}" aria-hidden="true"></div>&nbsp;&nbsp;
										{vtranslate($BASIC_ACTION->getLabel(), $MODULE)}
									</button>
								</li>
								{/if}

							{else}
								<li>
									{* NOTE itoma  Pardavejam nerodos sukurti saskaita mygtuko *}
								{if $ROLE_ID == 'H24' && $BASIC_ACTION->getLabel() == 'LBL_ADD_RECORD' && $MODULE eq "Invoice"}	
								{else}						
								
									<button id="{$MODULE}_listView_basicAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($BASIC_ACTION->getLabel())}" type="button" class="btn addButton btn-default module-buttons" 
											{if stripos($BASIC_ACTION->getUrl(), 'javascript:')===0}  
												onclick='{$BASIC_ACTION->getUrl()|substr:strlen("javascript:")}'
											{else} 
												onclick='window.location.href = "{$BASIC_ACTION->getUrl()}&app={$SELECTED_MENU_CATEGORY}{if $MODULE eq "Invoice"}&newRecord=true{/if}"'
											{/if}>
										<div class="fa {$BASIC_ACTION->getIcon()}" aria-hidden="true"></div>&nbsp;&nbsp;
										{vtranslate($BASIC_ACTION->getLabel(), $MODULE)}
									</button>
									{/if}

								</li>								
							{/if}
						{/foreach}


{if (($SHOW_EXPORT eq 'on') || ($SHOW_EXPORT eq 1)) || $ROLE eq 'H2'}
				{if $MODULE eq "PurchaseOrder"}
						<li>
							<div class="btn-group">
								<button type="button" class="btn addButton btn-default module-buttons dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<div class="fa fa-download" aria-hidden="true"></div>&nbsp;&nbsp;Exportuoti&nbsp;&nbsp;<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" id="export_purchase" style="display:block; margin-left: 10px; margin-top: 5px;" onclick="window.location.href = 'vtlib/Vtiger/exel/purchase_order_invoices.php?search_params'">Saskaitas (Excel)</a>
									<a class="dropdown-item" id="export_detail_purchase" style="display:block; margin-left: 10px; margin-top: 5px; margin-bottom: 5px;" onclick="window.location.href = 'vtlib/Vtiger/exel/purchase_order_detail_invoices.php?search_params'">Detalias eilutes (Excel)</a>			
							</div>						
						</li>
					{elseif $MODULE eq "Invoice"}
						<li>
							<div class="btn-group">
								<button type="button" class="btn addButton btn-default module-buttons dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<div class="fa fa-download" aria-hidden="true"></div>&nbsp;&nbsp;Exportuoti&nbsp;&nbsp;<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" id="export_invoice" style="display:block; margin-left: 10px; margin-top: 5px;" onclick="window.location.href = 'vtlib/Vtiger/exel/sales_order_invoices.php?search_params'">Saskaitas (Excel)</a>
									<a class="dropdown-item" id="export_detail_invoice" style="display:block; margin-left: 10px; margin-top: 5px; margin-bottom: 5px;" onclick="window.location.href = 'vtlib/Vtiger/exel/sales_order_detail_invoices.php?search_params'">Detalias eilutes (Excel)</a>			
							</div>						
						</li>
				{elseif $MODULE eq 'SalesOrder'}
							<li>
								<button type="button" class="btn addButton btn-default module-buttons" onclick="window.location.href='index.php?module=Arriving&view=Edit'"><div class="fa fa-plus" aria-hidden="true"></div>&nbsp;&nbsp;Pridėti atvykimą</button>
							</li>

							<li>
							<div class="btn-group exportSalesLi">
								<button type="button" class="btn addButton btn-default module-buttons dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<div class="fa fa-download" aria-hidden="true"></div>&nbsp;&nbsp;Exportuoti&nbsp;&nbsp;<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" id="export_sales" style="display:block; margin-left: 10px; margin-top: 5px;" onclick="window.location.href = 'vtlib/Vtiger/exel/sales_order_export.php?search_params'">Užsakymus (CSV)</a>
									<a class="dropdown-item" id="export_detail_sales" style="display:block; margin-left: 10px; margin-top: 5px; margin-bottom: 5px;" onclick="window.location.href = 'vtlib/Vtiger/exel/sales_order_detail_export.php?search_params'">Detalias eilutes (Excel)</a>			
							</div>						
						</li>
				{/if}
{/if}
						{if $MODULE_SETTING_ACTIONS|@count gt 0}
							<li>
								<div class="settingsIcon">
									<button type="button" class="btn btn-default module-buttons dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="{vtranslate('LBL_SETTINGS', $MODULE)}">
										<span class="fa fa-wrench" aria-hidden="true"></span>&nbsp;{vtranslate('LBL_CUSTOMIZE', 'Reports')}&nbsp; <span class="caret"></span>
									</button>
									<ul class="detailViewSetting dropdown-menu">
										{foreach item=SETTING from=$MODULE_SETTING_ACTIONS}
											<li id="{$MODULE_NAME}_listview_advancedAction_{$SETTING->getLabel()}"><a href={$SETTING->getUrl()}>{vtranslate($SETTING->getLabel(), $MODULE_NAME ,vtranslate($MODULE_NAME, $MODULE_NAME))}</a></li>
										{/foreach}
									</ul>
								</div>
							</li>
						{/if}
					</ul>
				</div>
			</div>
			{/if}
		</div>

	{if $MODULE eq 'SalesOrder'}
			{assign var=user_id value=$USER_ID}

	<!-- Modal -->
	<div class="modal fade" id="importFromExel" tabindex="-1" role="dialog" aria-labelledby="importFromExelTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Užsakymų importas</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<form action="vtlib/Vtiger/exel/import.php" method="POST" enctype="multipart/form-data" id="importExel">
						<div class="form-group">
							<label>Pasirinkite excel failą</label>
							<input type="file" name="fileToUpload" id="fileToUpload">
							<input type="hidden" name="user_id" value="{$user_id->id}">
						</div>
					</form>



				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="showLoader();">Įkelti</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Atšaukti</button>
				</div>
			</div>
		</div>
	</div>


	<script>
		function showLoader() {
			$('#messageBar').removeClass('hide');
			$('#messageBar').html(
				`<div style="text-align:center;position:fixed;top:50%;left:40%;"><img src="layouts/v7/skins/images/loading.gif"></div>`
			);
			document.getElementById('importExel').submit();
			$('#importFromExel').modal('hide');
		}
	</script>

{/if}


		{if $FIELDS_INFO neq null}
			<script type="text/javascript">
				var uimeta = (function () {
					var fieldInfo = {$FIELDS_INFO};
					return {
						field: {
							get: function (name, property) {
								if (name && property === undefined) {
									return fieldInfo[name];
								}
								if (name && property) {
									return fieldInfo[name][property]
								}
							},
							isMandatory: function (name) {
								if (fieldInfo[name]) {
									return fieldInfo[name].mandatory;
								}
								return false;
							},
							getType: function (name) {
								if (fieldInfo[name]) {
									return fieldInfo[name].type
								}
								return false;
							}
						},
					};
				})();
			</script>
		{/if}
	</div>     
{/strip}
