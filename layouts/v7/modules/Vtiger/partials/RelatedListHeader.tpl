{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
{assign var=MEDIAN value=$MEDIAN}
{assign var=AUTO_INVOICING_HISTORY value=$AUTO_INVOICING_HISTORY}
{assign var=AUTO_INVOICING_EMPLOYEES value=$AUTO_INVOICING_EMPLOYEES}
	
<script src="layouts/v7/modules/Automaticinvoicing/resources/related.js"></script>

	<div class="relatedHeader">
{*	{if $MODULE eq 'Accounts' && $RELATED_MODULE_NAME eq 'Invoice'}*}
{*		Mokėjimų vidurkis (Mediana): {$MEDIAN}  [ {$STATISTICS} ] Klausimas: ar šita vieta tik kliento sąskaitų sąrašo, ar čia visur rodys.*}
{*	{/if}*}
		{include file="partials/RelatedListHeaderStatistics.tpl"|vtemplate_path:$RELATED_MODULE_NAME }
		<div class="btn-toolbar row">
			<div class="col-lg-6 col-md-6 col-sm-6 btn-toolbar">
				 {foreach item=RELATED_LINK from=$RELATED_LIST_LINKS['LISTVIEWBASIC']}
					<div class="btn-group">
						{assign var=DROPDOWNS value=$RELATED_LINK->get('linkdropdowns')}
						{if count($DROPDOWNS) gt 0}
							<a class="btn dropdown-toggle" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200" data-close-others="false" style="width:20px;height:18px;">
								<img title="{$RELATED_LINK->getLabel()}" alt="{$RELATED_LINK->getLabel()}" src="{vimage_path("{$RELATED_LINK->getIcon()}")}">
							</a>
							<ul class="dropdown-menu">
								{foreach item=DROPDOWN from=$DROPDOWNS}
									<li><a id="{$RELATED_MODULE_NAME}_relatedlistView_add_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($DROPDOWN['label'])}" class="{$RELATED_LINK->get('linkclass')}" href='javascript:void(0)' data-documentType="{$DROPDOWN['type']}" data-url="{$DROPDOWN['url']}" data-name="{$RELATED_MODULE_NAME}" data-firsttime="{$DROPDOWN['firsttime']}"><i class="icon-plus"></i>&nbsp;{vtranslate($DROPDOWN['label'], $RELATED_MODULE_NAME)}</a></li>
								{/foreach}
							</ul>
						{else}
							{assign var=IS_SELECT_BUTTON value={$RELATED_LINK->get('_selectRelation')}}
							{* setting button module attribute to Events or Calendar based on link label *}
							{assign var=LINK_LABEL value={$RELATED_LINK->get('linklabel')}}
							{if $RELATED_LINK->get('_linklabel') === '_add_event'}
								{assign var=RELATED_MODULE_NAME value='Events'}
							{elseif $RELATED_LINK->get('_linklabel') === '_add_task'}
								{assign var=RELATED_MODULE_NAME value='Calendar'}
							{/if}

								{assign var=rule value=false}
							 {if $RELATED_LINK->get('linkclass') eq 'customModal'}
							 	{assign var=rule value=true}
							 {/if}							
											

							{if $IS_SELECT_BUTTON || $IS_CREATE_PERMITTED}
								<button type="button" module="{$RELATED_MODULE_NAME}" class="btn btn-default{if $IS_SELECT_BUTTON eq true} selectRelation{else} addButton" {if !$rule}name="addButton"{/if}{/if} {if $rule}data-toggle="modal" data-target="{$RELATED_LINK->get('modalId')}"{/if} {if $rule}onclick="clearInputs(event);"{/if}
									{if $IS_SELECT_BUTTON eq true} data-moduleName="{$RELATED_LINK->get('_module')->get('name')}" {/if}
									{if ($RELATED_LINK->isPageLoadLink())}
										{if $RELATION_FIELD} data-name="{$RELATION_FIELD->getName()}" {/if}
										data-url="{$RELATED_LINK->getUrl()}{if $SELECTED_MENU_CATEGORY}&app={$SELECTED_MENU_CATEGORY}{/if}"
									{/if}
									>{if $IS_SELECT_BUTTON eq false}<i class="fa fa-plus"></i>&nbsp;{/if}&nbsp;{$RELATED_LINK->getLabel()}</button>
							{/if}
						{/if}
					</div>
				{/foreach}
				&nbsp;
			</div>
			{assign var=CLASS_VIEW_ACTION value='relatedViewActions'}
			{assign var=CLASS_VIEW_PAGING_INPUT value='relatedViewPagingInput'}
			{assign var=CLASS_VIEW_PAGING_INPUT_SUBMIT value='relatedViewPagingInputSubmit'}
			{assign var=CLASS_VIEW_BASIC_ACTION value='relatedViewBasicAction'}
			{assign var=PAGING_MODEL value=$PAGING}
			{assign var=RECORD_COUNT value=$RELATED_RECORDS|@count}
			{assign var=PAGE_NUMBER value=$PAGING->get('page')}
			{include file="Pagination.tpl"|vtemplate_path:$MODULE SHOWPAGEJUMP=true}
		</div>
	</div>	


	<!-- Modal -->
<div class="modal fade" id="load_address" tabindex="-1" role="dialog" aria-labelledby="load_addressTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pridėti pasikrovimo adresą</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<table class="table" id="loadTable">
				 <tbody id="loadBody"> 			
						<tr id="load_copy" class="except" style="display: none;">
								<td class="listViewEntryValue">
									<select class="inputElement except">
										{foreach item=HIS from=$AUTO_INVOICING_HISTORY['load']}
												<option value="{$HIS['address']}">{$HIS['street']}, {$HIS['city']}</option>
										{/foreach}
									</select>
								</td>
								<td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
							</tr>		
						<tr>
							<td class="listViewEntryValue">
								<select class="inputElement">
									{foreach item=HIS from=$AUTO_INVOICING_HISTORY['load']}
											<option value="{$HIS['address']}">{$HIS['street']}, {$HIS['city']}</option>
									{/foreach}
								</select>
							</td>
							<td></td>
						</tr>
				</tbody>
			</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="saveFields('loadTable','load');">Įrašyti</button>
        <button type="button" class="btn btn-primary" onclick="cloneSelect('load_copy','loadBody');">Pridėti pasirinkimą</button>
				<button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="unload_address" tabindex="-1" role="dialog" aria-labelledby="unload_addressTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pridėti išsikrovimo adresą</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<table class="table" id="unloadTable">
				<tbody id="unloadBody"> 
					<tr id="unload_copy" class="except" style="display: none;">
									<td class="listViewEntryValue">
										<select class="inputElement except">
											{foreach item=HIS from=$AUTO_INVOICING_HISTORY['unload']}
													<<option value="{$HIS['address']}">{$HIS['street']}, {$HIS['city']}</option>
											{/foreach}
										</select>
									</td>
									<td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
					</tr>		
					<tr>
						<td class="listViewEntryValue">
							<select class="inputElement">
								 {foreach item=HIS from=$AUTO_INVOICING_HISTORY['unload']}
										<<option value="{$HIS['address']}">{$HIS['street']}, {$HIS['city']}</option>
								{/foreach}
							</select>
						</td>
						<td></i></td>
					</tr>
				</tbody>
			</table>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-primary" onclick="saveFields('unloadTable','unload');">Įrašyti</button>
				 <button type="button" class="btn btn-primary" onclick="cloneSelect('unload_copy','unloadBody');">Pridėti pasirinkimą</button>
				<button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="employee" tabindex="-1" role="dialog" aria-labelledby="employeeTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pridėti darbuotoją</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table" id="employeeTable">
				<tbody id="employeeBody">  
					<tr id="employee_copy" class="except" style="display: none;">
						<td class="listViewEntryValue">
							<select class="inputElement except">
								{foreach item=EMPLOYEE from=$AUTO_INVOICING_EMPLOYEES}
										<option value="{$EMPLOYEE}">{$EMPLOYEE}</option>
								{/foreach}
							</select>
						</td>
						<td><i class="fa fa-trash" style="cursor: pointer;" onclick="deleteField(event);"></i></td>
					</tr>
					<tr>
						<td class="listViewEntryValue">
							<select class="inputElement">
								 {foreach item=EMPLOYEE from=$AUTO_INVOICING_EMPLOYEES}
										<option value="{$EMPLOYEE}">{$EMPLOYEE}</option>
								{/foreach}
							</select>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-primary" onclick="saveFieldsEmployee();">Įrašyti</button>
				 <button type="button" class="btn btn-primary" onclick="cloneSelect('employee_copy','employeeBody');">Pridėti pasirinkimą</button>
			   <button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>
{if $MODULE eq 'Accounts' && $RELATED_MODULE_NAME eq 'Autoinvoicingrules'}
	<script>
		function clearInputs(event){
			let modalId = event.target.dataset.target;
			let modal = document.querySelector(modalId);
			let trs = modal.querySelectorAll('.modal-body tr:not(.except)');
			for(let i = 1; i < trs.length; i++){
				trs[i].remove();
			}
		}

		function cloneSelect(which,where){
			let itm = document.getElementById(which);
			let cln = itm.cloneNode(true);
			cln.style = '';
			cln.id = '';
			cln.children[0].children[0].classList.remove('except');
			cln.classList.remove('except');
			document.getElementById(where).appendChild(cln);
		}

		function deleteField(e) {
			e.target.parentElement.parentElement.remove();
		}
	</script>
{/if}


{/strip}