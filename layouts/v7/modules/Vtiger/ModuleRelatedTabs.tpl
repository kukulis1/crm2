{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}

	{assign var=SEND_INVOICE_COUNT value=$SEND_INVOICE_COUNT}
	{assign var=INVOICE_CONFIRMATION value=$INVOICE_CONFIRMATION}
	{assign var=INVOICE_INFO value=$INVOICE_INFO}

	<div class='related-tabs row'>
		<ul class="nav nav-tabs">
			{foreach item=RELATED_LINK from=$DETAILVIEW_LINKS['DETAILVIEWTAB']}
				{assign var=RELATEDLINK_URL value=$RELATED_LINK->getUrl()}
				{assign var=RELATEDLINK_LABEL value=$RELATED_LINK->getLabel()}
				{assign var=RELATED_TAB_LABEL value={vtranslate('SINGLE_'|cat:$MODULE_NAME, $MODULE_NAME)}|cat:" "|cat:$RELATEDLINK_LABEL}
				<li class="tab-item {if $RELATED_TAB_LABEL==$SELECTED_TAB_LABEL}active{/if}" data-url="{$RELATEDLINK_URL}&tab_label={$RELATED_TAB_LABEL}&app={$SELECTED_MENU_CATEGORY}" data-label-key="{$RELATEDLINK_LABEL}" data-link-key="{$RELATED_LINK->get('linkKey')}" >
					<a href="{$RELATEDLINK_URL}&tab_label={$RELATEDLINK_LABEL}&app={$SELECTED_MENU_CATEGORY}" class="textOverflowEllipsis">
						<span class="tab-label"><strong>{vtranslate($RELATEDLINK_LABEL,{$MODULE_NAME})}</strong></span>
					</a>
				</li>
			{/foreach}

			{assign var=RELATEDTABS value=$DETAILVIEW_LINKS['DETAILVIEWRELATED']}
			{assign var=COUNT value=$RELATEDTABS|@count}

			{assign var=LIMIT value = 10}
			{if $COUNT gt 10}
				{assign var=COUNT1 value = $LIMIT}
			{else}
				{assign var=COUNT1 value=$COUNT}
			{/if}

			{for $i = 0 to $COUNT1-1}
				{assign var=RELATED_LINK value=$RELATEDTABS[$i]}
				{assign var=RELATEDMODULENAME value=$RELATED_LINK->getRelatedModuleName()}
				{assign var=DELIVERY_NUMS value=$RELATED_LINK->getDeliveryPlansNum()}
				{assign var=RELATEDFIELDNAME value=$RELATED_LINK->get('linkFieldName')}
				{assign var="DETAILVIEWRELATEDLINKLBL" value= vtranslate($RELATED_LINK->getLabel(),$RELATEDMODULENAME)}
				<li class="tab-item {if (trim($RELATED_LINK->getLabel())== trim($SELECTED_TAB_LABEL)) && ($RELATED_LINK->getId() == $SELECTED_RELATION_ID)}active{/if}" data-url="{$RELATED_LINK->getUrl()}&tab_label={$RELATED_LINK->getLabel()}&app={$SELECTED_MENU_CATEGORY}" data-label-key="{$RELATED_LINK->getLabel()}" data-labs="DeliveryPlan"
					data-module="{$RELATEDMODULENAME}" data-relation-id="{$RELATED_LINK->getId()}" {if $RELATEDMODULENAME eq "ModComments"} title {else} title="{$DETAILVIEWRELATEDLINKLBL}"{/if} {if $RELATEDFIELDNAME}data-relatedfield ="{$RELATEDFIELDNAME}"{/if}>
					<a href="index.php?{$RELATED_LINK->getUrl()}&tab_label={$RELATED_LINK->getLabel()}&app={$SELECTED_MENU_CATEGORY}" class="textOverflowEllipsis" displaylabel="{$DETAILVIEWRELATEDLINKLBL}" recordsCount="" >
						{if $RELATEDMODULENAME eq "ModComments"}
							<i class="fa fa-comment"></i> &nbsp;<span class="tab-label"><strong>{$DETAILVIEWRELATEDLINKLBL}</strong></span>
						{else}
							<span class="tab-icon">
								{assign var=RELATED_MODULE_MODEL value=Vtiger_Module_Model::getInstance($RELATEDMODULENAME)}  
								{$RELATED_MODULE_MODEL->getModuleIcon()}
							</span>
						{/if}
						{* ITOMA *}
						{if $RELATEDMODULENAME eq 'Deliveryplan' && $DELIVERY_NUMS}&nbsp;<span style="background: none repeat scroll 0 0 #8e44ad;border-radius: 2px;	color: white;	display: inline-block;	font-size: 10px;min-height: 19px;min-width: 19px;padding: 2px;text-align: center;vertical-align: baseline;margin-top: 2px;">{$DELIVERY_NUMS}</span>{/if}
						&nbsp;<span class="numberCircle hide">0</span>
					</a>
				</li>
				{if ($RELATED_LINK->getId() == {$smarty.request.relationId})}
					{assign var=MORE_TAB_ACTIVE value='true'}
				{/if}
			{/for}
			{if $MORE_TAB_ACTIVE neq 'true'}
				{for $i = 0 to $COUNT-1}
					{assign var=RELATED_LINK value=$RELATEDTABS[$i]}
					{if ($RELATED_LINK->getId() == {$smarty.request.relationId})}
						{assign var=RELATEDMODULENAME value=$RELATED_LINK->getRelatedModuleName()}
						{assign var=RELATEDFIELDNAME value=$RELATED_LINK->get('linkFieldName')}
						{assign var="DETAILVIEWRELATEDLINKLBL" value= vtranslate($RELATED_LINK->getLabel(),$RELATEDMODULENAME)}
						<li class="more-tab moreTabElement active"  data-url="{$RELATED_LINK->getUrl()}&tab_label={$RELATED_LINK->getLabel()}&app={$SELECTED_MENU_CATEGORY}" data-label-key="{$RELATED_LINK->getLabel()}"
							data-module="{$RELATEDMODULENAME}" data-relation-id="{$RELATED_LINK->getId()}" {if $RELATEDMODULENAME eq "ModComments"} title {else} title="{$DETAILVIEWRELATEDLINKLBL}"{/if} {if $RELATEDFIELDNAME}data-relatedfield ="{$RELATEDFIELDNAME}"{/if}>
							<a href="index.php?{$RELATED_LINK->getUrl()}&tab_label={$RELATED_LINK->getLabel()}&app={$SELECTED_MENU_CATEGORY}" class="textOverflowEllipsis" displaylabel="{$DETAILVIEWRELATEDLINKLBL}" recordsCount="" >
								{if $RELATEDMODULENAME eq "ModComments"}
									<i class="fa fa-comment"></i> &nbsp;<span class="tab-label"><strong>{$DETAILVIEWRELATEDLINKLBL}</strong></span>
								{else}  
									<span class="tab-icon">
										{assign var=RELATED_MODULE_MODEL value=Vtiger_Module_Model::getInstance($RELATEDMODULENAME)}  
										{$RELATED_MODULE_MODEL->getModuleIcon()}
									</span>
								{/if}
								&nbsp;<span class="numberCircle hide">0</span>
							</a>
						</li>
						{break}
					{/if}
				{/for}
			{/if}
			{if $COUNT gt $LIMIT}
				<li class="dropdown related-tab-more-element">
					<a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
						<span class="tab-label"> 
							<strong>{vtranslate("LBL_MORE",$MODULE_NAME)}</strong> &nbsp; <b class="fa fa-caret-down"></b>
						</span>
					</a>
					<ul class="dropdown-menu pull-right" id="relatedmenuList">
						{for $j = $COUNT1 to $COUNT-1}
							{assign var=RELATED_LINK value=$RELATEDTABS[$j]}
							{assign var=RELATEDMODULENAME value=$RELATED_LINK->getRelatedModuleName()}
							{assign var=RELATEDFIELDNAME value=$RELATED_LINK->get('linkFieldName')}
							{assign var="DETAILVIEWRELATEDLINKLBL" value= vtranslate($RELATED_LINK->getLabel(),$RELATEDMODULENAME)}
							<li class="more-tab {if (trim($RELATED_LINK->getLabel())== trim($SELECTED_TAB_LABEL)) && ($RELATED_LINK->getId() == $SELECTED_RELATION_ID)}active{/if}" data-url="{$RELATED_LINK->getUrl()}&tab_label={$RELATED_LINK->getLabel()}&app={$SELECTED_MENU_CATEGORY}" data-label-key="{$RELATED_LINK->getLabel()}"
								data-module="{$RELATEDMODULENAME}" title="" data-relation-id="{$RELATED_LINK->getId()}" {if $RELATEDFIELDNAME}data-relatedfield ="{$RELATEDFIELDNAME}"{/if}>
								<a href="index.php?{$RELATED_LINK->getUrl()}&tab_label={$RELATED_LINK->getLabel()}&app={$SELECTED_MENU_CATEGORY}" displaylabel="{$DETAILVIEWRELATEDLINKLBL}" recordsCount="">
									{if $RELATEDMODULENAME eq "ModComments"}
										<i class="fa fa-comment"></i> &nbsp;<span class="tab-label">{$DETAILVIEWRELATEDLINKLBL}</span>
									{else}  
										{assign var=RELATED_MODULE_MODEL value=Vtiger_Module_Model::getInstance($RELATEDMODULENAME)}  
										<span class="tab-icon textOverflowEllipsis">
											{$RELATED_MODULE_MODEL->getModuleIcon()}
											<span class="content"> &nbsp;{$DETAILVIEWRELATEDLINKLBL}</span>
										</span>
									{/if}
									&nbsp;<span class="numberCircle hide">0</span>
								</a>
							</li>
						{/for}
					</ul>
				</li>
			{/if}

		{if $MODULE_NAME eq 'Invoice'}
			{if $SEND_INVOICE_COUNT > 0}
				<li class="tab-item" data-except="true" data-toggle="popover" title="Saskaitos info" data-placement="top" data-content="<ul><li>Išsiuntė: {$INVOICE_INFO['employee']}</li><li>Laikas: {$INVOICE_INFO['send_date']}</li>
				{if $INVOICE_INFO['email_addreses']}	<li style='word-break: break-all;'>Išsiųsta į: {$INVOICE_INFO['email_addreses']}</li>{/if}
				<li>Nuoroda: <a href='javascript:void(0);' onclick='openInvoiceUrl(`{$INVOICE_INFO["url"]}`);'>{vtranslate('LBL_VIEW_FILE','Documents')}</a> <i title='Kopijuoti nuorodą' class='fa fa-copy' style='cursor:pointer;' onclick='copyUrlToClipboard(event);' data-url='{$INVOICE_INFO['url']}'></i></li><li>Nuoroda galioja iki: {$INVOICE_INFO['expired']}</li></ul>">
					<a href="#" class="textOverflowEllipsis" title="Klientui išsiųsta sąskaita">
					<i style="font-size: 25px; color: blue;" class="fa fa-file"></i>&nbsp;<span style="margin-left: -10px;" class="numberCircle ">{$SEND_INVOICE_COUNT}</span></a>	
				</li>				
		{/if}	
			{if $INVOICE_CONFIRMATION >= 1}
				<li class="tab-item" data-except="true" data-toggle="popover" title="Kliento info" data-placement="top" data-content="<ul><li>Atsisiuntė: {$INVOICE_INFO['receive_date']}</li></ul>">
					<a href="#" class="textOverflowEllipsis" title="Klientas parsisiuntė sąskaitą">
					<i style="font-size: 25px; color: #7fdf17;" class="fa fa-check-circle"></i>&nbsp;<span style="margin-left: -10px;background: none repeat scroll 0 0 #7fdf17;border: 1px solid white;" class="numberCircle">{$INVOICE_CONFIRMATION}</span></a>	
				</li>
			{/if}			
				<li class="tab-item {if $SELECTED_TAB_LABEL eq 'Eiluciu istorija'}active{/if}" data-url="index.php?module=Invoice&view=Detail&record={$INVOICE_INFO['invoiceid']}&mode=showLinesHistory&tab_label=Eiluciu istorija">
				<a href="#" class="textOverflowEllipsis" title="Sąskaitos detaliu eilučiu istorija">
					<i style="font-size: 25px; color: #7fdf17;" class="fa fa-history"></i></a>	
				</li>
		{/if}
		</ul>
	</div>
	{strip}
{if $MODULE_NAME eq 'Invoice'}
	<style>
		.popover-content{
			padding-top: 10px;
			padding-left: 0;
		}
	</style>
	<script>
	$(function () {
		 $('[data-toggle="popover"]').popover({
			container: 'body',
			html: 'true'
		})
	});

	</script>

		{/if}