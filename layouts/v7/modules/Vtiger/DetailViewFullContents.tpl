{*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************}
{* modules/Vtiger/views/Detail.php *}

{* START YOUR IMPLEMENTATION FROM BELOW. Use {debug} for information *}
{strip}
{if $MODULE_NAME eq 'Accounts'}
	<link rel="stylesheet" href="layouts/v7/modules/Accounts/resources/style.css" type="text/css" />
{assign var="STATUS" value=$STANDING_ORDER_STATUS}

    <div class="block block_Standing sales orders" data-block="Standing sales orders" data-blockid="918"><div>
        <h4 class="textOverflowEllipsis maxWidth50" style="display:inline-flex">&nbsp;Nuolatiniai pardavimo užsakymai</h4>
        {* <label class="switch">
             <input type="checkbox" id="standingOrdersRule" {if $STATUS eq 1}checked{/if}>
            <span class="slider round"></span> <small style="position: absolute;margin-top: 2px;margin-left: 45px;">Įjungti/Išjungti</small>
        </label>  *}
        <button class="btn btn-default addTemplate" data-toggle="modal" data-target="#orderFormModal" style="float:right;margin-top:11px;"><i class="fa fa-plus"></i>&nbsp;&nbsp;Pridėti šabloną</button></div>
        <hr>
        <div class="blockData">
            {include file=vtemplate_path("partials/SalesOrderTemplate.tpl","Accounts") FIELD_MODEL=$FIELD_MODEL USER_MODEL=$USER_MODEL MODULE="Accounts" RECORD=$RECORD}
        </div>
    </div>
    <br>
{/if}

{if $MODULE_NAME eq 'Vendors'}
	<link rel="stylesheet" href="layouts/v7/modules/Vendors/resources/style.css" type="text/css" />
{assign var="STATUS" value=$STANDING_ORDER_STATUS}

    <div class="block"><div>
        <h4 class="textOverflowEllipsis maxWidth50" style="display:inline-flex"><img class="cursorPointer alignMiddle blockToggle  hide " src="layouts/v7/skins/images/arrowRight.png" data-mode="hide">
        <img class="cursorPointer alignMiddle blockToggle " src="layouts/v7/skins/images/arrowdown.png" data-mode="show">&nbsp;Pirkimo užsakymu šablonai</h4>
        <label class="switch">
             <input type="checkbox" id="purchaseOrderRule" {if $STATUS eq 1}checked{/if}>
            <span class="slider round"></span> <small style="position: absolute;margin-top: 2px;margin-left: 45px;">Įjungti/Išjungti</small>
        </label> 
        <button class="btn btn-default addTemplate" data-toggle="modal" data-target="#orderFormModal" style="float:right;margin-top:11px;"><i class="fa fa-plus"></i>&nbsp;&nbsp;Pridėti šabloną</button></div>
        <hr>
        <div class="blockData">
            {include file=vtemplate_path("partials/PurchaseOrderTemplate.tpl","Vendors") FIELD_MODEL=$FIELD_MODEL USER_MODEL=$USER_MODEL MODULE="Vendors" RECORD=$RECORD}
        </div>
    </div>
    <br>
{/if}

    <form id="detailView" method="POST">
        {include file='DetailViewBlockView.tpl'|@vtemplate_path:$MODULE_NAME RECORD_STRUCTURE=$RECORD_STRUCTURE MODULE_NAME=$MODULE_NAME}
    </form>
{/strip}
