{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	{assign var="FIELD_INFO" value=$FIELD_MODEL->getFieldInfo()}
	{assign var="SPECIAL_VALIDATOR" value=$FIELD_MODEL->getValidator()}

	{if $MODULE eq 'Accounts'}
				{$COLUMNS=['cf_1218' => 'cf_1216', 'cf_1222' => 'cf_1220', 'cf_1226' => 'cf_1224', 'cf_1230' => 'cf_1228', 'cf_1234' => 'cf_1232','cf_1238' => 'cf_1236', 'cf_1242' => 'cf_1240', 'cf_1246' => 'cf_1244', 'cf_1250' => 'cf_1248', 'cf_1254' => 'cf_1252','cf_1400' => 'cf_1398', 'cf_1404' => 'cf_1402', 'cf_1408' => 'cf_1406', 'cf_1412' => 'cf_1410', 'cf_1416' => 'cf_1414', 'cf_1420' => 'cf_1418', 'cf_1424' => 'cf_1422', 'cf_1428' => 'cf_1426', 'cf_1432' => 'cf_1430', 'cf_1436' => 'cf_1434']}										
	{/if}

	{if (!$FIELD_NAME)}
		{assign var="FIELD_NAME" value=$FIELD_MODEL->getFieldName()}
	{/if}
	<input id="{$MODULE}_editView_fieldName_{$FIELD_NAME}" type="{if $FIELD_NAME eq 'subject' and ($MODULE eq 'Invoice' or $MODULE eq 'PurchaseOrder')}hidden{else}text{/if}" data-fieldname="{$FIELD_NAME}" {if $MODULE eq 'Accounts'}data-column="{$FIELD_NAME}" data-type-column="{$COLUMNS[$FIELD_NAME]}"{/if} data-fieldtype="string" class="inputElement {if $FIELD_MODEL->isNameField()}nameField{/if}" name="{$FIELD_NAME}" value="{$FIELD_MODEL->get('fieldvalue')}"
		{if $FIELD_MODEL->get('uitype') eq '3' || $FIELD_MODEL->get('uitype') eq '4'|| $FIELD_MODEL->isReadOnly()}
			{if $FIELD_MODEL->get('uitype') neq '106'}
				readonly
			{else if $FIELD_MODEL->get('uitype') eq '106' && $MODE eq 'edit'}
				readonly
			{/if}
		{/if}
		{if !empty($SPECIAL_VALIDATOR)}data-validator="{Zend_Json::encode($SPECIAL_VALIDATOR)}"{/if}
		{if $FIELD_INFO["mandatory"] eq true} data-rule-required="true" {/if}
		{if count($FIELD_INFO['validator'])}
			data-specific-rules='{ZEND_JSON::encode($FIELD_INFO["validator"])}'
		{/if}

			{if $MODULE eq 'Accounts' && $FIELD_MODEL->getFieldName() eq 'bill_country'}  		
				minlength="3"  maxlength="3"
			{/if}  

						{if $MODULE eq 'Accounts' && $FIELD_MODEL->getFieldName() eq 'ship_country'}  		
				minlength="3"  maxlength="3"
			{/if}  

		   />
	
{/strip}
