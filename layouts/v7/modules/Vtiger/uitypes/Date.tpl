{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
{strip}

{assign var="FIELD_INFO" value=$FIELD_MODEL->getFieldInfo()}
{assign var="SPECIAL_VALIDATOR" value=$FIELD_MODEL->getValidator()}
{assign var="dateFormat" value=$USER_MODEL->get('date_format')}


{if (!$FIELD_NAME)}
  {assign var="FIELD_NAME" value=$FIELD_MODEL->getFieldName()}
{/if}

{if $MODULE eq 'SalesOrder'}

 {$DATE_FIELD_NAME = ($FIELD_NAME == 'load_date_from') ? 'load_date_to' : 'unload_date_to'}

  <div class="input-group inputElement" style="margin-bottom: 3px">
  <input id="{$MODULE}_editView_fieldName_{$FIELD_NAME}" type="text" data-calendar-type="range" class="dateField form-control" data-fieldname="{$FIELD_NAME}" data-fieldtype="date" name="{$FIELD_NAME}" data-date-format="{$dateFormat}"
      value="{$FIELD_MODEL->getEditViewDisplayValue($FIELD_MODEL->get('fieldvalue'))}{if $FIELD_MODEL->getUnLoadDate($RECORD_ID,$DATE_FIELD_NAME)},{/if}{$FIELD_MODEL->getUnLoadDate($RECORD_ID,$DATE_FIELD_NAME)}" data-field-type="date"/>
  <span class="input-group-addon"><i class="fa fa-calendar "></i></span>
  </div>

  {$TIME_FIELD_NAME = ($FIELD_NAME == 'load_date_from') ? 'load_time_from' : 'unload_time_from'}
  {$TIME_FIELD_NAME2 = ($FIELD_NAME == 'load_date_from') ? 'load_time_to' : 'unload_time_to'} 
  
  {assign var="TIME_FROM_VALUE" value=$FIELD_MODEL->getOrderTime($RECORD_ID, $TIME_FIELD_NAME)}

  <div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;margin-left: 10px;">
		<input style="width:84px !important;" id="{$MODULE}_editView_fieldName_{$TIME_FIELD_NAME}" type="text" data-format="24" class="timepicker-default form-control" value="{$TIME_FROM_VALUE}" name="{$TIME_FIELD_NAME}"/>
		<span class="input-group-addon" style="width: 30px;">
			<i class="fa fa-clock-o"></i>
		</span>
	</div>

   {assign var="TIME_TO_VALUE" value=$FIELD_MODEL->getOrderTime($RECORD_ID, $TIME_FIELD_NAME2)}

  <div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;margin-left: 10px;">
		<input style="width:84px !important;" id="{$MODULE}_editView_fieldName_{$TIME_FIELD_NAME2}" type="text" data-format="24" class="timepicker-default form-control" value="{$TIME_TO_VALUE}" name="{$TIME_FIELD_NAME2}"/>
		<span class="input-group-addon" style="width: 30px;">
			<i class="fa fa-clock-o"></i>
		</span>
	</div>

{else}

<div class="input-group inputElement" style="margin-bottom: 3px">
<input id="{$MODULE}_editView_fieldName_{$FIELD_NAME}" type="text" class="dateField form-control {if $IGNOREUIREGISTRATION}ignore-ui-registration{/if}" data-fieldname="{$FIELD_NAME}" data-fieldtype="date" name="{$FIELD_NAME}" data-date-format="{$dateFormat}"
    value="{$FIELD_MODEL->getEditViewDisplayValue($FIELD_MODEL->get('fieldvalue'))}" {if !empty($SPECIAL_VALIDATOR)}data-validator='{Zend_Json::encode($SPECIAL_VALIDATOR)}'{/if}
    {if $MODE eq 'edit' && $FIELD_NAME eq 'due_date'} data-user-changed-time="true" {/if}
    {if $FIELD_INFO["mandatory"] eq true} data-rule-required="true" {/if}
    {if count($FIELD_INFO['validator'])}
        data-specific-rules='{ZEND_JSON::encode($FIELD_INFO["validator"])}'
    {/if}  data-rule-date="true" />
<span class="input-group-addon"><i class="fa fa-calendar "></i></span>
</div>

{/if}




{if ($MODULE eq 'Invoice' || $MODULE eq 'PurchaseOrder') && $FIELD_NAME eq 'duedate'}
<input type="hidden" id="duedate">
{/if}
{/strip}
