$(document).ready(function () {
  order_time();

  $('.execute_type').on('change', function () {
    let parent = $(this).parent().parent();
    if ($(this).val() == 1) {
      parent.find('#weekDays').removeClass('hide');
      parent.find('#monthDays').addClass('hide');
    } else if ($(this).val() == 2) {
      parent.find('#weekDays').addClass('hide');
      parent.find('#monthDays').removeClass('hide');
    } else {
      parent.find('#weekDays').addClass('hide');
      parent.find('#monthDays').addClass('hide');
    }
  });

  $('.deleteRow').on('click', function (e) {
    let body = $(this).parent().parent().parent();
    $(this).parent().parent().remove();
    let tr = body.find('.lineItemRow');

    tr.each(function (i) {
      $(this).attr('id', `row${i + 1}`);
      $(this).attr("data-row-num", i + 1);
      $(this).find('.cargo_wgt').attr('id', `cargo_wgt${i + 1}`).attr('name', `cargo_wgt${i + 1}`);
      $(this).find('.cargo_length').attr('id', `cargo_length${i + 1}`).attr('name', `cargo_length${i + 1}`);
      $(this).find('.cargo_width').attr('id', `cargo_width${i + 1}`).attr('name', `cargo_width${i + 1}`);
      $(this).find('.cargo_height').attr('id', `cargo_height${i + 1}`).attr('name', `cargo_height${i + 1}`);
      $(this).find('.measure').attr('id', `measure${i + 1}`).attr('name', `measure${i + 1}`);
      $(this).find('.qty').attr('id', `qty${i + 1}`).attr('name', `qty${i + 1}`);
    });
  });

  $('.saveTemplateBtn').on('click', function () {
    let count = $(this).parent().parent().find('.lineItemRow').length;
    $(this).parent().find('#totalProductCount').val(count);
    if (templateValidate()) {
      document.getElementById('templateForm').submit();
    }
  });

  $('.addProduct').on('click', function () {
    let num = $(this).parent().parent().parent().find('.lineItemRow').length + 1;
    let appendTo = $(this).parent().parent().parent().find('#lineItemTab tbody');
    let copyElement = $(this).parent().parent().parent().find('.lineItemCloneCopy').clone(true, true);
    copyElement.removeClass('hide lineItemCloneCopy');
    copyElement.addClass('lineItemRow');
    copyElement.attr('id', `row${num}`);
    copyElement.attr("data-row-num", num);
    copyElement.find('#cargo_wgt0').attr('id', `cargo_wgt${num}`).attr('name', `cargo_wgt${num}`);
    copyElement.find('#cargo_length0').attr('id', `cargo_length${num}`).attr('name', `cargo_length${num}`);
    copyElement.find('#cargo_width0').attr('id', `cargo_width${num}`).attr('name', `cargo_width${num}`);
    copyElement.find('#cargo_height0').attr('id', `cargo_height${num}`).attr('name', `cargo_height${num}`);
    copyElement.find('#measure0').attr('id', `measure${num}`).attr('name', `measure${num}`);
    copyElement.find('#qty0').attr('id', `qty${num}`).attr('name', `qty${num}`);
    copyElement.appendTo(appendTo);
  });

  $('.lineItemInputBox').keyup(function (e) {
    let number = $(this).val();
    $(this).parent().parent().parent().parent().find('#grandTotal').html(number);
    $(this).parent().parent().parent().parent().find('[name="total"]').val(number);
  });

  $('.editBtn').on('click', function () {
    let record = $(this).data('record');
    loadPriceCalculator(`#editOrderFormModal_${record}`, 'edit');
  });

  $('.qty').on('keyup', function () {
    let val = $(this).val();
    let id = $(this).data('num');
    $(`#qty${id}`).val(val);
  });

  $('#standingOrdersRule').on('click', function () {
    let status;
    if ($(this).is(":checked")) {
      status = 1;
    } else {
      status = 0;
    }

    let accountid = $('[name="record_id"]').val();

    $.ajax({
      type: "POST",
      url: "modules/Accounts/ajax/enable.php",
      data: { accountid: accountid, status: status },
      success: function (response) {
        console.log(response);
      }
    });


  });
});


function editTemplate(event, id) {
  let count = $(event).parent().parent().find('.lineItemRow').length;
  $(event).parent().find('#totalProductCount').val(count);
  if (eachTemplateValidate(id)) {
    document.getElementById(`templateForm${id}`).submit();
  }
}

function deleteRecord(id, event) {
  if (confirm('Ar tikrai norite ištrinti šabloną?')) {
    $.ajax({
      type: "POST",
      url: "modules/Accounts/ajax/deleteTemplate.php",
      data: { id: id },
      success: function (response) {
        if (response == 'success') {
          $(event.target).parent().parent().parent().parent().remove();
        } else {
          console.log(response);
        }
      }
    });
  }
}

function eachTemplateValidate(id) {
  let errors = new Array();
  let n = 1;
  $(`#templateForm${id} [data-require="true"]`).each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(n);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    n++;
  });

  let m = 1;
  $(`#templateForm${id} .lineItemRow [data-rule-required=true]`).each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(m);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    m++;
  });

  if (errors.length == 0) {
    return true;
  } else {
    return false;
  }
}

function templateValidate() {
  let errors = new Array();
  let n = 1;
  $('#templateForm [data-require="true"]').each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(n);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    n++;
  });

  let m = 1;
  $('#templateForm .lineItemRow [data-rule-required=true]').each(function () {
    if ($(this).val() == '') {
      $(this).addClass('alert-danger');
      errors.push(m);
    } else {
      $(this).removeClass('alert-danger');
    }

    $(this).on('change', function () {
      if ($(this).val() != '') {
        $(this).removeClass('alert-danger');
      }
    });
    m++;
  });

  if (errors.length == 0) {
    return true;
  } else {
    return false;
  }
}

function order_time() {
  var load_time_from = $("#SalesOrder_editView_fieldName_load_time_from").val();
  var load_time_to = $("#SalesOrder_editView_fieldName_load_time_to").val();
  var unload_time_from = $("#SalesOrder_editView_fieldName_unload_time_from").val();
  var unload_time_to = $("#SalesOrder_editView_fieldName_unload_time_to").val();

  if (load_time_from.replace(/\s/g, "") == "") {
    $("#SalesOrder_editView_fieldName_load_time_from").val('08:00');
  }

  if (load_time_to.replace(/\s/g, "") == "") {
    $("#SalesOrder_editView_fieldName_load_time_to").val('17:00');
  }

  if (unload_time_from.replace(/\s/g, "") == "") {
    $("#SalesOrder_editView_fieldName_unload_time_from").val('08:00');
  }

  if (unload_time_to.replace(/\s/g, "") == "") {
    $("#SalesOrder_editView_fieldName_unload_time_to").val('17:00');
  }
}


var isString = function (e) {
  if($('#SalesOrder_editView_fieldName_bill_country').val() != 'POL' || $('#SalesOrder_editView_fieldName_ship_country').val() != 'POL'){
    if (isNaN($(e).val())) {
      $(e).val('');
    }
  }
}

var isInt = function (e) {
  if (!isNaN($(e).val())) {
    $(e).val('');
  }
}