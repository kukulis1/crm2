{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

	<div>Įrašai: {$VISES['filter']}</div>

{if $VISES['user'] eq 1 OR $VISES['user'] eq 5}

	{if $VISES['content'] neq false}
		<div class="row entry clearfix" style="padding: 2px 20px 2px 14px;">    
			<div class="col-lg-1 pull-left"><b>Saskaitos nr.</b></div>
			<div class="col-lg-1 pull-left"><b>Sask. data</b></div>
			<div class="col-lg-1 pull-left"><b>Mok. data</b></div>
			<div class="col-lg-1 pull-left"><b>Tiekėjas</b></div>
			<div class="col-lg-1 pull-left"><b>Suma</b></div>
			<div class="col-lg-2 pull-left"><b>Atsakingas</b></div>
			<div class="col-lg-2 pull-left"><b>Sukūrė</b></div>
			<div class="col-lg-1 pull-left"><b title="Kada vizuota atsakingo asmens">Vizuota</b></div>
			<div class="col-lg-1 pull-left"><b>Veiksmas</b></div>
		</div>
	<div style='padding:10px;'>
		{foreach key=$index item=ORDER from=$VISES['content']} 
			<div class="row entry clearfix" style="padding: 10px 3px 6px;border-bottom: 1px solid #ddd;">    	
				<div class="col-lg-1 pull-left"><a target="_blank" href="index.php?module=PurchaseOrder&view=Detail&record={$ORDER['purchaseorderid']}&app=INVENTORY">{$ORDER['invoice_num']}</a></div>   
				<div class="col-lg-1 pull-left">{$ORDER['invoicedate']}</div>   
				<div class="col-lg-1 pull-left">{$ORDER['duedate']}</div>   
				<div class="col-lg-1 pull-left">{$ORDER['vendorname']}</div>   
				<div class="col-lg-1 pull-left">{$ORDER['total']}</div>   
				<div class="col-lg-2 pull-left">{$ORDER['user']}</div>  
				<div class="col-lg-2 pull-left">{if !$ORDER['creator']}{$ORDER['user']}{else}{$ORDER['creator']}{/if}</div>  
				<div class="col-lg-1 pull-left">{$ORDER['createdtime']}</div>    
				<div class="col-lg-1 pull-left">
					<button type="button" data-poid="{$ORDER['purchaseorderid']}" class="btn btn-success btn-sm" onclick="viseOrder(event);">Vizuoti</button>
				</div>    
			</div>
    {/foreach}
 </div>
			{else}
		<span class="noDataMsg">{vtranslate('LBL_NO_VISEPURCHASES', $MODULE_NAME)}</span>
{/if}

{else}
	<span class="noDataMsg">{vtranslate('LBL_NO_RIGHTS', $MODULE_NAME)}</span>
{/if}