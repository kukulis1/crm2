{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

	<div>Įrašai: {$VISES['filter']}</div>

{if $VISES['role'] eq 'H2' OR $VISES['role'] eq 'H11'}
	{if $VISES['content'] neq false}
		<div class="row entry clearfix" style="padding: 2px 20px 2px 14px;">    
			<div class="col-lg-2 pull-left"><b>Saskaitos nr.</b></div>
			<div class="col-lg-2 pull-left"><b>Tipas</b></div>
			<div class="col-lg-2 pull-left"><b>Įmonė</b></div>
			<div class="col-lg-1 pull-left"><b>Suma</b></div>
			<div class="col-lg-2 pull-left"><b>Sukūrė</b></div>
			<div class="col-lg-2 pull-left"><b>Sukurta</b></div>
			<div class="col-lg-1 pull-left"></div>
		</div>
	<div style='padding:10px;'>
		{foreach key=$index item=ORDER from=$VISES['content']} 
			<div class="row entry clearfix" style="padding: 10px 3px 6px;border-bottom: 1px solid #ddd;{if $ORDER['ok'] eq 0}font-weight: bold;{/if}">    	
				<div class="col-lg-2 pull-left"><a target="_blank" href="index.php?module={if $ORDER['setype'] == 'SalesOrder'}SalesOrder{else}PurchaseOrder{/if}&view=Detail&record={$ORDER['id']}&app=INVENTORY">{$ORDER['numb']}</a></div>   
				<div class="col-lg-2 pull-left">{if $ORDER['setype'] == 'SalesOrder'}Užsakymas{else}Pirkimas{/if}</div> 
				<div class="col-lg-2 pull-left">{$ORDER['name']}</div>   
				<div class="col-lg-1 pull-left">{$ORDER['total']}</div>   
				<div class="col-lg-2 pull-left">{if !$ORDER['creator']}{$ORDER['user']}{else}{$ORDER['creator']}{/if}</div>  
				<div class="col-lg-2 pull-left">{$ORDER['createdtime']}</div>     
				<div class="col-lg-1 pull-left">{if $ORDER['ok'] eq 0}<button class="btn btn-sm btn-success" style="padding: 0 6px;" onclick="setOK(event,{$ORDER['id']})">OK</button>{/if}</div>     
			</div>
    {/foreach}
 </div>
			{else}
		<span class="noDataMsg">{vtranslate('LBL_NO_CANCEL_PURCHASES', $MODULE_NAME)}</span>
{/if}

{else}
	<span class="noDataMsg">{vtranslate('LBL_NO_RIGHTS', $MODULE_NAME)}</span>
{/if}