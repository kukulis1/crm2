/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Inventory_List_Js("PurchaseOrder_List_Js",{},{});

function mergePurchaseInvoices(){
  let body = $('#listview-table tbody tr');

  let invoiceidArray = new Array();
  let check = false;

  for (let i = 0; i < body.length; i++) {
    check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
    if(check) {
      invoiceidArray.push($('#listview-table tbody tr').eq(i).data('id'));
    }
  }  

  let purchaseorderid = invoiceidArray.join(',');

  $.ajax({
    type: "POST",
    url: "modules/PurchaseOrder/ajax/mergePurchaseInvoice.php",
    data: {purchaseorderid:purchaseorderid},
    dataType: "JSON",
    success: function (response) {
     if(response.status == 'success'){
       window.location.href = 'index.php?module=PurchaseOrder&view=List&viewname=25';
     }else{
       alert('Įvyko klaida praneškite puslapio administratoriui '+response.status);
     }
    }
  });

}