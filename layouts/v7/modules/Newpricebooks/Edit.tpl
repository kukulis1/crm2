<link rel="stylesheet" href="layouts/v7/modules/Newpricebooks/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Newpricebooks/resources/main.js"></script>




<div id="table-content" class="table-container">
<form method="POST" id="pricebook_form">
<div>
  <div style="float:left;margin-left: 5%;">    
    <input id="pricebook_name" class="inputElement" style="width: 400px; margin-top: 20px;" name="pricebook_name" placeholder="Kainoraščio pavadinimas" autocomplete="off" required>
    <input type="hidden" id="accountid" name="accountid">
<small style="display: block;color: orangered;margin-bottom: 10px;">Irašius kliento pavadinimą ir pasirinkus jį iš sąrašo, kainynas automatiškai bus priskirtas tam klientui. Jei įrašysite tik kainyno pavadinima kainynas bus niekam nepriskirtas, bet jį galėsite priskirti per kliento kortelę</small>
  </div>


  <div style="float:right; margin-right: 10%;">
    <h5>Kainyno zonos</h5>
    <select class="inputElement" name="zone_num" id="zone_num">
    {for $i = 1 to 20}
      <option {if $zones_num eq $i}selected{/if}>{$i}</option>
    {/for}  
    </select>
  </div>
</div>
<table id="listview-table" class="table listview-table" style="border-top: 1px solid #ddd;">
<thead>
 <tr>
  <th><h4>Krovinio kiekis</h4></th>
  <th><h4>Kainos,EUR</h4></th>
</tr>
</thead>
 <tr>
  <td>
    <table id="listview-table" class="table listview-table" style="border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;" >
      <thead>
      <tr>
        <td></th>
        <th>Kg</th>
        <th>M3</th>
        <th>M2</th>
        <th>M</th>
        <th title="Krovos darbų kaina">K/D</th>
        <th>Pll</th>
      </tr>
      </thead>
    <tbody id="dimensions-body">

      <tr>
          <td style="width: 30px;"></td>
          <td><input class="inputElement" name="max_weight1" style="width: 70px;" autocomplete="off"></td> 
          <td><input class="inputElement" name="max_volume1" style="width: 70px;" autocomplete="off"></td> 
          <td><input class="inputElement" name="max_square1" style="width: 70px;" autocomplete="off"></td>   
          <td><input class="inputElement" name="max_meter1" style="width: 70px;" autocomplete="off"></td>   
          <td><input class="inputElement" name="stevedoring1" style="width: 70px;" autocomplete="off"></td>   
          <td><input class="inputElement" name="pll1" style="width: 70px;" autocomplete="off"></td>   
      </tr>   

</tbody>
</table>  

</td>
<td>
  <table id="listview-table" class="table listview-table" style="border-bottom: 1px solid #ddd;">
        <thead id="zone-thead">
        <tr>
          <th>Zona 1</th>
        </tr>
        </thead>
      <tbody id="zones-body">
    <tr>    
      <td><input class="inputElement" name="product-1-1" data-zona="1"  style="width: 70px;" autocomplete="off"></td>
    </tr> 

  </tbody>
  </table>  
</td>
</tbody>
</table>  
 <div class='modal-overlay-footer clearfix' style="border-left: unset !important;">
    <div class="row clearfix">
        <div class='textAlignCenter col-lg-12 col-md-12 col-sm-12'>
         <button class="btn btn-success saveButton" type="submit" form="pricebook_form">{vtranslate('LBL_SAVE', $MODULE)}</button> &nbsp;&nbsp;
            <a class='cancelLink' href="javascript:history.back()">{vtranslate('LBL_CANCEL', $MODULE)}</a>
        </div>
    </div>
</div>
 
  <button type="button" class="btn btn-primary btn-group" id="add_field_btn">Pridėti eilutę</button>
    <input id="num_rows" type="hidden" name="num_rows" value="7">
    <input id="lines" type="hidden" name="lines" value="1">
</form>
<div id="wait" style="display:none;position:absolute;top:25%;left:43%;padding:2px;"><img src="/resources/loading.gif"></div>
<div style="height: 20px;"></div>

<div class="modal fade" id="selectCitiesModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pasirinkite kryptis</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
         <div id="wait2" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
         Į abi puses <input id="bothSides" type="checkbox" class="inputElement" title="Pažymėjus kryptys galios į abi puses" onclick="updateBothSide();"><br><i class="fa fa-arrow-left" style="color: grey;"></i><i class="fa fa-arrow-right" style="color: grey;"></i>
        <table class="table" id="table_begin">
          <thead>
            <tr>
              <td>Tipas</td> 
              <td>Iš kur</td>
              <td>Į kur</td>
              <td width="10px;">P/K</td>
              <td width="10px;">Adr.</td>
              <td width="10px;"></td>
            </tr>
          </thead>
          <tbody class="modal-table">
            </tbody>
          </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
            <button type="button" class="btn btn-primary" data-add-field="modal">Pridėti laukelį</button>
            <button type="button" class="btn btn-success" data-save="modal">Išsaugoti</button>
          </div>
        </div>
      </div>
    </div>

<div id="client_zones_place"></div>


<div style="height: 100px;"></div>
</div>

