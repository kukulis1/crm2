let upSide = document.querySelector('#appnav');

let record = new URLSearchParams(window.location.search);

// Hide unnecessary buttons
if (upSide.querySelector('.settingsIcon')) {
	upSide.querySelector('.settingsIcon').style.display = 'none';
}

if (upSide.querySelector('#Newpricebooks_basicAction_LBL_IMPORT')) {
	upSide.querySelector('#Newpricebooks_basicAction_LBL_IMPORT').style.display = 'none';
}



let rec = record.get('record');

if (record.get('view') == 'PriceBook') {
	let navbar = document.querySelector('#appnav .nav');
	let li = document.createElement('li');
	let btn = document.createElement('button');
	btn.setAttribute('class', 'btn addButton btn-default module-buttons');
	btn.setAttribute('onclick', `window.location.href = "index.php?module=Newpricebooks&view=EditPricebook&record=${rec}"`);
	btn.innerHTML = 'Redaguoti kainyną';
	li.append(btn);
	navbar.append(li);

	let li3 = document.createElement('li');
	let btn3 = document.createElement('button');
	btn3.setAttribute('class', 'btn addButton btn-default module-buttons');
	btn3.setAttribute('onclick', `window.location.href = "index.php?module=Newpricebooks&view=copyPricebook&record=${rec}"`);
	btn3.innerHTML = 'Kopijuoti kainyną';
	li3.append(btn3);
	navbar.append(li3);

	let li4 = document.createElement('li');
	let btn4 = document.createElement('button');
	btn4.setAttribute('class', 'btn addButton btn-primary');
	btn4.setAttribute('style', 'padding: 6px 12px; margin-left: 5px; margin-top: 3px;');
	btn4.setAttribute('onclick', "window.open('/faq/faq.php#Newpricebooks' , '_blank');");
	btn4.innerHTML = 'Pagalba <i class="fa fa-question"></i>';
	li4.append(btn4);

	let div = document.createElement('div');
	div.setAttribute('class', 'btn-group');

	let li2 = document.createElement('li');
	let btn2 = document.createElement('button');
	btn2.setAttribute('class', 'btn addButton btn-default module-buttons');
	btn2.setAttribute('data-toggle', 'modal');
	btn2.setAttribute('data-target', '#sendPriceBook');
	btn2.innerHTML = 'Siųsti kainyną';

	let li5 = document.createElement('li');
	let btn5 = document.createElement('button');
	btn5.setAttribute('class', 'btn addButton btn-default module-buttons');
	btn5.setAttribute('onclick', `window.location.href="index.php?module=Newpricebooks&view=Pdf&record=${rec}"`);
	btn5.innerHTML = 'Eksportuoti kainyną';
	li5.append(btn5);

	let li6 = document.createElement('li');
	let btn6 = document.createElement('button');
	btn6.setAttribute('class', 'btn addButton btn-default module-buttons');
	btn6.setAttribute('onclick', `reCalculate(${rec})`);
	btn6.innerHTML = 'Perskaičiuoti užsakymų kainas';
	li6.append(btn6);


	navbar.append(li5);
	navbar.append(li6);
	li2.append(btn2);
	navbar.append(li2);
	// navbar.append(li4);


} else if (record.get('view') == 'List') {
	let navbar = document.querySelector('.module-breadcrumb');
	let input = document.createElement('input');
	let btn = document.createElement('button');
	btn.setAttribute('class', 'btn btn-success');
	btn.setAttribute('style', "margin-left: 10px;");
	btn.setAttribute('onclick', "searchPriceBook()");
	btn.innerHTML = 'Ieškoti';

	input.setAttribute('class', 'inputElement');
	input.setAttribute('id', 'pricebook_search');
	input.setAttribute('placeholder', 'Kainyno paieška');
	input.setAttribute('style', "margin-top: 6px; width: 300px; padding: 6px 10px;");
	navbar.append(input);
	navbar.append(btn);


	$('#pricebook_search').val(record.get('search'));

	$('#pricebook_search').keydown(function (e) {
		var key = e.which;
		if (key == 13 || key == 9) {
			searchFilter();
		}
	});
	$(this).change(function () {
		searchFilter();
	});


}


jQuery(document).ready(function () {
	createTableElements();
	$('#collapseOne').collapse();

	$('#collapseTwo').on('show.bs.collapse', function () {
		$('#collapseOne').collapse('hide');
		$('#collapseBtnOne').removeClass('selected');
		$('#collapseBtnTwo').addClass('selected');
	});

	$('#collapseOne').on('show.bs.collapse', function () {
		$('#collapseTwo').collapse('hide');
		$('#collapseBtnTwo').removeClass('selected');
		$('#collapseBtnOne').addClass('selected');
	});

	if (record.get('view') == 'EditPricebook' && record.get('success') != null) {
		let form = $('#edit_form').serialize();
		let pricebookid = record.get('record');

		$.ajax({
			type: "POST",
			url: "modules/Newpricebooks/ajax/updateProducts.php",
			data: { form: form },
			beforeSend: function () {
				$('#wait').show();
				$('#edit_form').css('opacity', '0.2');
			},
			success: function (result) {
				setTimeout(() => {
					$('#wait').hide();
					window.location.href = `?module=Newpricebooks&view=PriceBook&record=${pricebookid}`;
				}, 1000);
			}
		});

	}

	// document.getElementById('submit_new_address').addEventListener('click', saveNewAddress);


	if (record.get('view') == 'CopyPricebook') {
		$('#copy_pricebook').val(1);
		$('#edit_form').submit();
	}

});


if (record.get('view') != 'List' && record.get('view') != 'Edit') {
	jQuery('.app-trigger, .app-icon, .app-navigator').on('click', function (e) {
		e.stopPropagation();
		if (checkHide()) {
			toggleAppMenu('show');
		} else {
			toggleAppMenu('hide');
		}
	});

	jQuery('body').on('click', function (e) {
		if (!checkHide()) {
			toggleAppMenu('hide');
		}
	});

}


function reCalculate(rec){
	if(confirm('Ar tikrai norite perskaičiuoti visus užsakymus kuriems nėra išrašyta sąskaita?')){
		$('#wait').show();
		$('#table-content').css('opacity', '0.2');
		window.location.href=`index.php?module=Newpricebooks&view=Calculate&record=${rec}`;
	}
}

function checkHide() {
	return document.querySelector('.app-menu.hide');
}

function toggleAppMenu(type) {
	var appMenu = jQuery('.app-menu');
	var appNav = jQuery('.app-nav');
	appMenu.appendTo('#page');
	appMenu.css({
		'top': appNav.offset().top + appNav.height(),
		'left': 0
	});
	if (typeof type === 'undefined') {
		type = appMenu.is(':hidden') ? 'show' : 'hide';
	}
	if (type == 'show') {
		document.querySelector('.app-menu').classList.remove('hide');
		appMenu.show(200, function () { });
	} else {
		document.querySelector('.app-menu').classList.add('hide');
		appMenu.hide(200, function () { });
	}
};


jQuery('.app-modules-dropdown-container').hover(function (e) {
	var dropdownContainer = jQuery(e.currentTarget);
	jQuery('.dropdown').removeClass('open');
	if (dropdownContainer.length) {
		if (dropdownContainer.hasClass('dropdown-compact')) {
			dropdownContainer.find('.app-modules-dropdown').css('top', dropdownContainer.position().top - 8);
		} else {
			dropdownContainer.find('.app-modules-dropdown').css('top', '');
		}
		dropdownContainer.addClass('open').find('.app-item').addClass('active-app-item');
	}
}, function (e) {
	var dropdownContainer = jQuery(e.currentTarget);
	dropdownContainer.find('.app-item').removeClass('active-app-item');
	setTimeout(function () {
		if (dropdownContainer.find('.app-modules-dropdown').length && !dropdownContainer.find('.app-modules-dropdown').is(':hover') && !dropdownContainer.is(':hover')) {
			dropdownContainer.removeClass('open');
		}
	}, 500);

});

jQuery('.app-item').on('click', function () {
	var url = jQuery(this).data('defaultUrl');
	if (url) {
		window.location.href = url;
	}
});



function addNumRows() {
	setTimeout(() => {
		let numRows = document.querySelectorAll('#table-content input:not([type=hidden])').length + 1;
		$('#num_rows').val(numRows);
		$('#lines').val(jQuery('#dimensions-body tr').unbind().length);
	}, 100);
}

function changeCountZonesValue() {
	let num = $('#dimensions-body tr').length;
	$('[name="count_zones_value"]').val(num);
}


// Suskaiciuoja kiek is viso yra zonu dom'e
function checkZonos() {
	return document.querySelectorAll('#zones-body tr')[0].getElementsByTagName('td').length;
}


// Suskaiciuoja kiek is viso yra zonu dom'e ir prie gauto skaiciaus viena prideda
function checkZonosLine2() {
	return document.querySelectorAll('#zones-body tr')[0].getElementsByTagName('td').length + 1;
}


// Prideda zonas (td) Sukuria head ,td ir iterpia i dom'a
function addZoneFields(zoneThead, zoneBody, zones, zonos) {
	for (let i = zonos + 1; i <= zones; i++) {
		let zline = parseInt(checkZonosLine2());
		let theadTh = document.createElement('th');
		theadTh.innerHTML = `Zona ${i}`;
		zoneThead.append(theadTh);
		let ztd = document.createElement('td');
		let zinput = document.createElement('input');
		zinput.setAttribute('class', 'inputElement');
		zinput.setAttribute('name', `product-${zline}-1`);
		zinput.setAttribute('data-zona', `${zline}`);
		zinput.setAttribute('style', 'width: 70px;');
		zinput.setAttribute('autocomplete', 'off');
		ztd.append(zinput);
		zoneBody.append(ztd);
	}

	addLines(zones, zonos);
}


function removeZoneFields(zones, zonos) {
	for (let i = zonos - 1; i >= zones; i--) {
		let e = parseInt(i) + 1;
		document.querySelectorAll('#zone-thead tr th')[i].remove();
		$(`[data-zona="${e}"]`).parent().remove();
	}
}



// Sukuria naujas eilutes
function addLines(zones, zonos) {

	let zline = jQuery('#zones-body tr').unbind().length;
	let zoneBody;

	for (let e = zonos; e < zones; e++) {
		if (zline > 1) {
			for (let k = 1; k < zline; k++) {
				zoneBody = document.querySelectorAll('#zones-body tr')[k];
				let zotd = document.createElement('td');
				let zoinput = document.createElement('input');
				zoinput.setAttribute('class', 'inputElement');
				zoinput.setAttribute('name', `product-${e + 1}-${k + 1}`);
				zoinput.setAttribute('data-zona', `${e + 1}`);
				zoinput.setAttribute('style', 'width: 70px;');
				zoinput.setAttribute('autocomplete', 'off');
				zotd.append(zoinput);
				zoneBody.append(zotd);
			}
		}
		addNumRows();
	}
}


function addRowAndLines() {

	let i = jQuery('#dimensions-body tr').unbind().length + 1;
	let dimBody = document.getElementById('dimensions-body');
	let tr = document.createElement('tr');
	let td = document.createElement('td');
	td.setAttribute('style', 'width: 30px;');
	tr.setAttribute('data-line', `${i}`);
	let td1 = document.createElement('td');
	let td2 = document.createElement('td');
	let td3 = document.createElement('td');
	let td4 = document.createElement('td');
	let td5 = document.createElement('td');
	let td6 = document.createElement('td');

	let button = document.createElement('button');
	button.type = 'button';
	button.setAttribute('class', 'btn delete_field_btn');
	button.setAttribute('data-line', `${i}`);
	button.setAttribute('style', 'background: transparent;outline: 0; padding: 0;');
	button.setAttribute('onclick', `deleteField(${i});`);
	button.innerHTML = '<i class="fa fa-trash" style="cursor: pointer;"></i>';

	let input = document.createElement('input');
	input.type = 'text';
	input.setAttribute('class', 'inputElement');
	input.setAttribute('name', `max_weight${i}`);
	input.setAttribute('style', 'width: 70px;');
	input.setAttribute('autocomplete', 'off');

	let input_2 = document.createElement('input');
	input_2.setAttribute('title', 'Pažymėjus taikoma min reikšmė bus 0.001');
	input_2.type = 'checkbox';
	input_2.setAttribute('name', `min_kg${i}`);
	input_2.style = 'margin-left: 5px;';


	let input2 = document.createElement('input');
	input2.type = 'text';
	input2.setAttribute('class', 'inputElement');
	input2.setAttribute('name', `max_volume${i}`);
	input2.setAttribute('style', 'width: 70px;');
	input2.setAttribute('autocomplete', 'off');

	let input2_2 = document.createElement('input');
	input2_2.setAttribute('title', 'Pažymėjus taikoma min reikšmė bus 0.001');
	input2_2.type = 'checkbox';
	input2_2.setAttribute('name', `min_m3${i}`);
	input2_2.style = 'margin-left: 5px;';


	let input3 = document.createElement('input');
	input3.type = 'text';
	input3.setAttribute('class', 'inputElement');
	input3.setAttribute('name', `max_square${i}`);
	input3.setAttribute('style', 'width: 70px;');
	input3.setAttribute('autocomplete', 'off');

	let input3_2 = document.createElement('input');
	input3_2.setAttribute('title', 'Pažymėjus taikoma min reikšmė bus 0.001');
	input3_2.type = 'checkbox';
	input3_2.setAttribute('name', `min_m2${i}`);
	input3_2.style = 'margin-left: 5px;';

	let input6 = document.createElement('input');
	input6.type = 'text';
	input6.setAttribute('class', 'inputElement');
	input6.setAttribute('name', `max_meter${i}`);
	input6.setAttribute('style', 'width: 70px;');
	input6.setAttribute('autocomplete', 'off');

	let input6_2 = document.createElement('input');
	input6_2.setAttribute('title', 'Pažymėjus taikoma min reikšmė bus 0.001');
	input6_2.type = 'checkbox';
	input6_2.setAttribute('name', `min_m${i}`);
	input6_2.style = 'margin-left: 5px;';

	let input4 = document.createElement('input');
	input4.type = 'text';
	input4.setAttribute('class', 'inputElement');
	input4.setAttribute('name', `stevedoring${i}`);
	input4.setAttribute('style', 'width: 70px;');
	input4.setAttribute('autocomplete', 'off');

	let input5 = document.createElement('input');
	input5.type = 'text';
	input5.setAttribute('class', 'inputElement');
	input5.setAttribute('name', `pll${i}`);
	input5.setAttribute('style', 'width: 70px;');
	input5.setAttribute('autocomplete', 'off');

	td.append(button);
	td1.append(input);
	td1.append(input_2);

	td2.append(input2);
	td2.append(input2_2);

	td3.append(input3);
	td3.append(input3_2);

	td6.append(input6);
	td6.append(input6_2);

	td4.append(input4);
	td5.append(input5);

	tr.append(td);
	tr.append(td1);
	tr.append(td2);
	tr.append(td3);
	tr.append(td6);
	tr.append(td4);
	tr.append(td5);
	dimBody.append(tr);

	let zotr;
	let zonos = checkZonos();
	let zline = jQuery('#zones-body tr').unbind().length;
	let zoneBody;


	for (let e = zline; e < i; e++) {
		zotr = document.createElement('tr');
		zotr.setAttribute('data-line', `${e + 1}`);
		document.querySelector('#zones-body').append(zotr);
		let m = e - 1;
		zoneBody = document.querySelectorAll('#zones-body tr')[m];
		let zotd = document.createElement('td');
		let zoinput = document.createElement('input');
		zoinput.type = 'text';
		zoinput.setAttribute('class', 'inputElement');
		zoinput.setAttribute('name', `product-1-${i}`);
		zoinput.setAttribute('data-zona', `1`);
		zoinput.setAttribute('style', 'width: 70px;');
		zoinput.setAttribute('autocomplete', 'off');
		zotd.append(zoinput);
		zotr.append(zotd);
	}


	for (let e = 1; e < zonos; e++) {

		if (zonos > 1) {
			zoneBody = document.querySelectorAll('#zones-body tr')[zline];
			let zotd = document.createElement('td');
			let zoinput = document.createElement('input');
			zoinput.type = 'text';
			zoinput.setAttribute('class', 'inputElement');
			zoinput.setAttribute('name', `product-${e + 1}-${i}`);
			zoinput.setAttribute('data-zona', `${e + 1}`);
			zoinput.setAttribute('style', 'width: 70px;');
			zoinput.setAttribute('autocomplete', 'off');
			zotd.append(zoinput);
			zotr.append(zotd);
			zoneBody.append(zotd);
		}
		addNumRows();
	}
}


function createTableElements() {

	$('#zone_num').on('change', function (e) {
		let zones = e.target.value;
		let zoneThead = document.querySelector('#zone-thead tr');
		let zoneBody = document.querySelector('#zones-body tr');
		let zonos = checkZonos();
		let pricebookid = record.get('record');

		if (zones < zonos) {

			if (record.get('view') == 'EditPricebook') {
				let checkInput = $(`[data-zona="${zonos}"]`);
				if (checkInput.val() != '') {
					if (confirm("Ar tikrai norite nustatyti žemesnį zonų skaičių? Joje esančios kainos bus ištrintos") == true) {
						let zline = document.querySelectorAll('#zones-body tr').length;
						let productIds = new Array();

						for (let i = 0; i < zline; i++) {
							let hiddenInputs = $(`[data-hiddenid="${zonos}"]`)[i];
							if (hiddenInputs !== undefined) {
								productIds.push(hiddenInputs.value);
							}
						}
						if (productIds != '') {
							$.ajax({
								type: "POST",
								url: "modules/Newpricebooks/ajax/removeProducts.php",
								data: { productIds: productIds, zones: zones, pricebookid: pricebookid },
								success: function (result) {
									if (result == 'Success') {
										removeZoneFields(zones, zonos);
									}
								}
							});
						}
					} else {
						$('#zone_num').val(zonos);
					}
				} else {
					removeZoneFields(zones, zonos);
				}
			} else {
				removeZoneFields(zones, zonos);
			}
		} else {
			addZoneFields(zoneThead, zoneBody, zones, zonos);
		}
	});


	$('#add_field_btn').on('click', function () {
		addRowAndLines();
		addNumRows();
		setTimeout(() => {
			changeCountZonesValue();
		}, 500);
	});
}

function deleteField(line) {
	let zline = document.querySelectorAll('#zones-body tr')[0].getElementsByTagName('td').length;
	let productIds = new Array();

	for (let i = 0; i < zline; i++) {
		let hiddenInputs = $(`[data-line="${line}"]`).find('input:hidden')[i];
		if (hiddenInputs !== undefined && hiddenInputs.value != '') {
			productIds.push(hiddenInputs.value);
		}
	}

	if (productIds != '') {
		if (confirm("Ar tikrai norite ištrinti?") == true) {
			$.ajax({
				type: "POST",
				url: "modules/Newpricebooks/ajax/removeProducts.php",
				data: { productIds: productIds },
				success: function (result) {
					if (result == 'Success') {
						$(`[data-line="${line}"]`).remove();
					}
				}
			});
		}
	} else {
		$(`[data-line="${line}"]`).remove();
	}

	setTimeout(() => {
		changeCountZonesValue();
		addNumRows();
	}, 500);
}


function searchPriceBook() { searchFilter(); }


function searchFilter() {
	let pricebook = $('#pricebook_search').val();
	return window.location.href = `index.php?module=Newpricebooks&view=List&search=${pricebook}`;
}

function action(e) {
	let orderId = $(e.target).parent().data('orderid');
	return window.location.href = `index.php?module=Newpricebooks&view=PriceBook&record=${orderId}`;
}

function openTypeSelector(id) {

	let pencil = document.querySelector(`#type${id}`).parentElement.getElementsByClassName('action')[0];

	pencil.style.display = 'none';

	$(`#type${id}`).hide();
	$(`#hidden_options${id}`).show();

	$('.input-group-addon-cancel').on('click', function () {
		$(`#type${id}`).show();
		$(`#hidden_options${id}`).hide();
		pencil.style.display = 'block';
	});

	$(`.inlineAjaxSave${id}`).on('click', function () {
		$(`#hidden_options${id}`).hide();
		pencil.style.display = 'block';
		let type = $(`#select_type${id}`).val();
		$(`#type${id}`).html(type);
		$(`#type${id}`).show();
		saveZoneTypes(id, type);
	});
}

function saveZoneTypes(id, type) {

	let column = $(`#type${id}`).data('column');
	let accountid = $('#accountid').val();

	$.ajax({
		type: "POST",
		url: "/pricebookAjax/saveZoneTypes.php",
		data: { type: type, column: column, accountid: accountid },
		dataType: 'JSON',
		beforeSend: function () {
			$('#wait').show();
			$('#zones_table').css('opacity', '0.2');
		},
		success: function (result) {
			setTimeout(() => {
				$('#wait').hide();
				$('#zones_table').css('opacity', '1');
			}, 2000);
		}
	});
}

async function locationsSelectors(id) {
	let input = $(`#zone${id}`).html();
	let col = $(`#zone${id}`).data('column');
	$('.modal-body').data('zoneColumn', col)
	let accountid = $('#accountid').val();
	let column = $(`#zone${id}`).data('typecolumn');

	let whoZone = function () {
		let ats;
		$.ajax({
			'async': false,
			'type': "POST",
			'global': false,
			'url': "modules/Newpricebooks/ajax/getType.php",
			'data': { accountid: accountid, column: column },
			'success': function (data) {
				ats = data;
			}
		});
		return ats;
	}();

	if (whoZone == '') whoZone = 'Miestas - Miestas';

	let start = true;

	let list = input.split(":");
	list = list.slice(1, list.length - 1);


	let inputs = 0;
	if (input != '') {
		inputs = list.length;
		// inputs = list.length - 1;
	}


	var responce = new Promise(function (resolve, reject) {
		$.ajax({
			type: "POST",
			url: "/price-algorithm/popup.php",
			dataType: "JSON",
			data: { whoZone: whoZone, inputs: inputs, start: start, col: col },
			beforeSend: function () {
				$("#selectCitiesModal").modal('show');
				$('#wait2').show();
				$('#table_begin').css('opacity', '0.2');
			},
			success: function (result) {
				if (result == null) {
					alert('Pirma pasirinkite kainos tipą');
					return;
				} else {
					$('#table_begin').css('opacity', '1');
					$('#wait2').hide();
					$('.modal-table').html(result.res);
					if (input != '') {
						appendLocation(whoZone, list);
					}
					addOneMoreField(id);
					saves(id);
				}
			},
			error: function (err) {
				reject(err);
			}
		});
	});
}

function loadCheckPostCode(event) {

	let city = event.target.parentElement.getElementsByTagName('select')[0].value;
	let post = event.target.value;

	var responce = new Promise(function (resolve, reject) {
		$.ajax({
			type: "POST",
			url: "/price-algorithm/checkCities.php",
			data: { city: city, post: post },
			success: function (result) {
				if (!result) {
					$(event.target).addClass('alert alert-danger');
				} else {
					$(event.target).removeClass('alert alert-danger');
				}
			},
			error: function (err) {
				reject(err);
			}
		});
	});
}


function directPostCodesCheckBox(event, id) {

	// let id2 = parseInt(id) + 1;
	let tds = event.target.parentElement.parentElement.getElementsByTagName('td');
	let ids = event.target.parentElement.parentElement.getElementsByTagName('td')[0].getElementsByTagName('select')[0].dataset.nr
	let ids2 = event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByTagName('select')[0].dataset.nr

	let input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('class', 'inputElement post');
	input.setAttribute('style', 'margin-top: 10px;');
	input.setAttribute('id', `post_code${ids}`);
	input.setAttribute('data-num', `${id + 1}`);
	input.setAttribute('onkeyup', `loadCheckPostCode(event);`);
	input.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
	let td = tds[2];

	let input2 = document.createElement('input');
	input2.setAttribute('type', 'text');
	input2.setAttribute('class', 'inputElement post');
	input2.setAttribute('style', 'margin-top: 10px;');
	input2.setAttribute('id', `post_code${ids2}`);
	input2.setAttribute('data-num', `${id + 1}`);
	input2.setAttribute('onkeyup', `loadCheckPostCode(event);`);
	input2.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
	let td2 = tds[1];

	if ($(event.target).is(':checked')) {
		td.append(input);
		td2.append(input2);
	} else {
		event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByClassName('post')[0].remove();
		event.target.parentElement.parentElement.getElementsByTagName('td')[2].getElementsByClassName('post')[0].remove();
	}

}

function directAddressCheckBox(event, id) {
	let tds = event.target.parentElement.parentElement.getElementsByTagName('td');
	let ids = event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByTagName('select')[0].dataset.nr
	let ids2 = event.target.parentElement.parentElement.getElementsByTagName('td')[2].getElementsByTagName('select')[0].dataset.nr
	
	let input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('class', 'inputElement adr');
	input.setAttribute('autocomplete', `off`);
	input.setAttribute('style', 'margin-top: 10px;');
	input.setAttribute('id', `address${ids}`);
	input.setAttribute('data-num', `${id + 1}`);
	input.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');

	let postInput = document.createElement('input');
	postInput.setAttribute('class', 'adr_code');
	postInput.setAttribute('type', 'hidden');
	postInput.setAttribute('data-num', `${id + 1}`);
	postInput.setAttribute('id', `address_post${ids}`);
	let td = tds[1];

	let input2 = document.createElement('input');
	input2.setAttribute('type', 'text');
	input2.setAttribute('class', 'inputElement adr');
	input2.setAttribute('autocomplete', `off`);
	input2.setAttribute('style', 'margin-top: 10px;');
	input2.setAttribute('id', `address${ids2}`);
	input2.setAttribute('data-num', `${id + 1}`);
	input2.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');

	let postInput2 = document.createElement('input');
	postInput2.setAttribute('class', 'adr_code');
	postInput2.setAttribute('type', 'hidden');
	postInput2.setAttribute('data-num', `${id + 1}`);
	postInput2.setAttribute('id', `address_post${ids2}`);

	let td2 = tds[2];

	if ($(event.target).is(':checked')) {
		td.append(input);
		td.append(postInput);
		getSuggestionsAddress(ids, 'from');

		td2.append(input2);
		td2.append(postInput2);
		getSuggestionsAddress(ids2, 'to');

		let td3 = tds[3];
		let span = document.createElement('span');
		span.setAttribute('class', 'fa fa-plus-circle  cursorPointer add_new_addresses');
		span.setAttribute('title', 'Pridėti adresą');
		span.setAttribute('data-from', ids);
		span.setAttribute('data-to', ids2);
		span.setAttribute('style', 'font-size: 20px;margin-top: 25px;');
		span.setAttribute('data-toggle', 'modal');
		span.setAttribute('data-target', '#addNewAddress');
		td3.append(span);
		setTimeout(() => {
			document.querySelector('.add_new_addresses').addEventListener('click', setInputIdsInModal);
		}, 100);
	} else {
		event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByClassName('adr')[0].remove();
		event.target.parentElement.parentElement.getElementsByTagName('td')[2].getElementsByClassName('adr')[0].remove();
		event.target.parentElement.parentElement.getElementsByTagName('td')[1].getElementsByClassName('adr_code')[0].remove();
		event.target.parentElement.parentElement.getElementsByTagName('td')[2].getElementsByClassName('adr_code')[0].remove();
		event.target.parentElement.parentElement.getElementsByTagName('td')[3].getElementsByClassName('add_new_addresses')[0].remove();
	}

}

function getSuggestionsAddress(id, which) {
	let accountid = $('#accountid').val();
	$.ajax({
		type: "POST",
		url: "modules/Newpricebooks/ajax/getAddressHistory.php",
		data: { accountid: accountid, which: which },
		dataType: 'JSON',
		success: function (result) {		
			autoCompleteAddress(document.getElementById(`address${id}`), result, which, id);
		}
	});
}


function setInputIdsInModal(e) {
	let from = e.target.dataset.from;
	let to = e.target.dataset.to;
	setTimeout(() => {
		$('#fromInput').val(to);
		$('#toInput').val(from);
		document.getElementById('submit_new_address').addEventListener('click', saveNewAddress);
	}, 200);
}

function saveNewAddress() {

	let fromInputs = document.querySelectorAll('#new_address input');
	let toInputs = document.querySelectorAll('#new_address2 input');
	let fromArray = new Array();
	let toArray = new Array();
	for (let i = 0, len = fromInputs.length; i < len; i++) {
		let name = fromInputs[i].name;
		if (fromInputs[i].value == '') {
			document.getElementById(`req-${name}`).classList.remove('hide');
		} else {
			document.getElementById(`req-${name}`).classList.add('hide');
			fromArray.push(fromInputs[i].value);
		}
	}

	for (let i = 0, len = toInputs.length; i < len; i++) {
		let name = toInputs[i].name;
		if (toInputs[i].value == '') {
			document.getElementById(`req-${name}`).classList.remove('hide');
		} else {
			document.getElementById(`req-${name}`).classList.add('hide');
			toArray.push(toInputs[i].value);
		}
	}

	if (fromArray.length == 4 && toArray.length == 4) {
		saveAddress(fromArray, 'from');
		saveAddress(toArray, 'to');
	}

}

function saveAddress(info, type) {
	let accountid = $('#accountid').val();
	let company = info[0];
	let city = info[1];
	let address = info[2];
	let code = info[3];

	let from = $('#fromInput').val();
	let to = $('#toInput').val();

	let line = company + " " + address + " " + city + " " + code;

	$.ajax({
		type: "POST",
		url: "modules/Newpricebooks/ajax/saveAddress.php",
		data: { accountid: accountid, type: type, company: company, city: city, address: address, code: code },
		beforeSend: function () {
			$('#wait3').show();
			$('#zones_table').css('opacity', '0.2');
		},
		success: function (result) {
			setTimeout(() => {
				$('#wait3').hide();
				$('#zones_table').css('opacity', '1');
				$('.dismiss').click();
				if (type == 'from') {
					$('#address' + from).val(line);
					$('#address_post' + from).val(code);
					document.getElementById('address' + from).classList.add('selected2');
				} else {
					$('#address' + to).val(line);
					$('#address_post' + to).val(code);
					document.getElementById('address' + to).classList.add('selected2');
				}
			}, 1000);
		}
	});

}


function addOneMoreField(id) {
	let count = 0;
	$('[data-add-field="modal"]').unbind().click(function (e) {
		count++;
		let inputs = count;
		let whoZone = 'Miestas - Miestas';
		let col = $(`#zone${id}`).data('column');
		let ids = $('.modal-table tr').length;

		$.ajax({
			type: "POST",
			url: "/price-algorithm/popup.php",
			data: { whoZone: whoZone, inputs: inputs, col: col, ids: ids },
			dataType: "JSON",
			success: function (result) {
				if (result != '') {
					$('.modal-table').append(result.res);
				}
			}
		});
		e.preventDefault();
	});
}

function changeFieldType(event) {
	let inputs = 1;
	let col = $('.modal-body').data('zoneColumn');
	let parent = event.target.parentElement.parentElement;
	let allTd = parent.querySelectorAll('td');
	let whoZone = event.target.value;
	let theParent = $(event.target).parent().parent();
	let ids = $('.modal-table tr').length;

	$.ajax({
		type: "POST",
		url: "/price-algorithm/popup.php",
		data: { whoZone: whoZone, inputs: inputs, col: col, ids: ids, change: 1 },
		dataType: "JSON",
		success: function (result) {
			if (result != '') {
				for (let n = 0; n < allTd.length; n++) {
					allTd[n].remove();
				}
				theParent.append(result.res);
			}
		}
	});
}

function validateForm(){	

	let inputAdr = $(".modal-table .adr");	
	let status = true;	

	$.each(inputAdr, function (index, input) {	
		if($(input).val() == ''){	
			$(input).addClass('alert-danger');	
			$(input).attr('placeholder', 'Būtina užpildyti šį lauką');	
			status = false;
		}
	});	

	let postCode = $(".modal-table .post");

	$.each(postCode, function (index, input) {	
		if($(input).val() == ''){	
			$(input).addClass('alert-danger');
			$(input).attr('placeholder', 'Būtina užpildyti šį lauką');		
			status = false;
		}
	});

	return status;
}


function saves(e) {
	$('[data-save="modal"]').unbind().click(function () {
		
		if(!validateForm()){		
			return;
		}
	
		$("#wait").css("display", "block");
		$('#table-content').css('opacity', '0.2');
		let numb;
		let inp;
		let selected = [];
		let values;
		const rows = document.querySelectorAll(".modal-table tr");

		for (let g = 0; g < rows.length; g++) {
			if ($(rows[g].querySelector('.type')).hasClass('selectas')) {
				let nodeList = rows[g].querySelectorAll('select:not(.except)');
				let group = '';
				$(nodeList).each(function () {
					group += $(this).val() + ' ';
				});

				group = group.slice(0, -1);
				selected.push(group);
			} else if ($(rows[g].querySelector('.type')).hasClass('inputas')) {
				selected.push(rows[g].querySelector('input[type="number"]').value);
			}
		}

		const selectedType = $(".modal-table .except option:selected");
		const inputs = $(".modal-table .post");
		const inputAdr = $(".modal-table .adr");
		const inputAdrCode = $(".modal-table .adr_code");
		let line = '';
		let adr = '';
		let adrCode = '';

		for (let i = 0; inputs.length > i; i++) {
			inp = inputs[i].value;
			numb = inputs[i].dataset.num;
			if (i % 2 == 0) {
				line += (i > 1 ? '/' : '') + numb + "=" + inp;
			} else {
				line += ";" + inp;
			}
		}

		for (let i = 0; inputAdr.length > i; i++) {
			inp = inputAdr[i].value;
			numb = inputAdr[i].dataset.num;
			if (i % 2 == 0) {
				adr += (i > 1 ? '/' : '') + numb + "=" + inp;
			} else {
				adr += ";" + inp;
			}
		}

		for (let i = 0; inputAdrCode.length > i; i++) {
			inp = inputAdrCode[i].value;
			numb = inputAdrCode[i].dataset.num;
			if (i % 2 == 0) {
				adrCode += (i > 1 ? '/' : '') + numb + "=" + inp;
			} else {
				adrCode += ";" + inp;
			}
		}


		let valuesType = Array.from(selectedType).map(el => el.innerHTML);
		valuesType = valuesType.filter(function (type) {
			return type != "Pasirinkite";
		});



		values = selected.filter(function (zone) {
			return zone != "Pasirinkite";
		});

		let list = values.join(",");
		let listType = valuesType.join(",");


		let parts = list.split(',');
		let locationList = '';

		for (var i = 0; i < parts.length; i++) {
			if (valuesType[i] != 'Atstumas') {
				if(parts[i] != ''){
					locationList += ':';
					locationList += `${parts[i]}`;
					// locationList += ':';
				}		
			} else {			
				if(parts[i] != ''){
					locationList += ':';
					locationList += `${parts[i]}`;					
				}
			}
		}

		if(locationList == ''){ locationList = ' ' }else{ locationList += ":";}

		insertValues(e, locationList);
		$("#selectCitiesModal").modal('hide');
		saveToDB(e, locationList, listType, line, adr, adrCode);
	});
	$('[data-dismiss="modal"]').click(function () { $("div.input-save-wrap .inlineAjaxCancel").click(); });

}

function insertValues(e, locationList) {
	$(`#zone${e}`).html(locationList);
	setTimeout(function () {
		$("#wait").css("display", "none");
		$('#zones_table').css('opacity', '1');
	}, 2000);
}


function saveToDB(id, list, listType, line, adr, adrCode) {

	let column = $(`#zone${id}`).data('column');
	let columnType = $(`#zone${id}`).data('typecolumn');
	let accountid = $('#accountid').val();

	$.ajax({
		type: "POST",
		url: "/pricebookAjax/saveZones.php",
		data: { column: column, list: list, listType: listType, columnType: columnType, accountid: accountid, line: line, adr: adr, adrCode: adrCode },
		dataType: "JSON",
		success: function (result) {
			;
			setTimeout(() => {
				$('#wait').hide();
				$('#table-content').css('opacity', '1');
			}, 2000);
		}
	});
}

function distanceHandler(id) {

	let spanValue = $(`#zone${id}`).html();
	let inp = document.createElement('input');
	inp.setAttribute('id', `number_inp${id}`);
	inp.setAttribute('class', 'inputElement form-control');
	inp.setAttribute('type', 'number');
	inp.value = spanValue;
	let span = document.getElementById(`zone${id}`);
	span.style.display = 'none';
	let parent = span.parentElement;
	parent.append(inp);

	let saveDiv = document.createElement('div');
	saveDiv.setAttribute('class', 'input-save-wrap');
	saveDiv.setAttribute('id', `save-wrap${id}`);

	let saveSpan = document.createElement('span');
	saveSpan.setAttribute('class', `pointerCursorOnHover input-group-addon input-group-addon-save inlineAjaxSave${id}`);

	let cancelSpan = document.createElement('span');
	cancelSpan.setAttribute('class', `pointerCursorOnHover input-group-addon input-group-addon-cancel inlineAjaxCancel`);

	let saveI = document.createElement('i');
	saveI.setAttribute('class', 'fa fa-check');

	let cancelI = document.createElement('i');
	cancelI.setAttribute('class', 'fa fa-close');


	saveSpan.append(saveI);
	cancelSpan.append(cancelI);
	saveDiv.append(saveSpan);
	saveDiv.append(cancelSpan);
	parent.append(saveDiv);

	distanceSave(id);


}

function distanceSave(id) {
	let numberInput = document.getElementById(`number_inp${id}`);
	let wrap = document.getElementById(`save-wrap${id}`);

	$('.input-group-addon-cancel').on('click', function () {
		$(`#zone${id}`).show();
		numberInput.remove();
		wrap.remove();
	});

	$(`.inlineAjaxSave${id}`).on('click', function () {
		$('#wait').show();
		saveToDB(id, numberInput.value);
		$(`#zone${id}`).show();
		$(`#zone${id}`).html(numberInput.value);
		numberInput.remove();
		wrap.remove();
	});
}

function appendLocation(whoZones, list) {
	const except = new Array('Kazlų Rūda', 'Kazlų Rūdos r.', 'Kudirkos Naumiestis', 'Naujoji Akmenė', 'Baltoji Vokė', 'Vilniaus apskr.', 'Kauno apskr.', 'Alytaus apskr.', 'Utenos apskr.', 'Panevėžio apskr.', 'Marijampolės apskr.',
		'Tauragės apskr.', 'Šiaulių apskr.', 'Telšių apskr.', 'Klaipėdos apskr.');
	const latvia = '(LV)';

	let whoZone = whoZones.split(",");

	for (let i = 0; i < list.length; i++) {
		if (whoZone.length < list.length) whoZone.push(whoZone[0]);
		if (whoZone[i] == 'Atstumas') {
			$(`#number_inp${i}`).val(list[i]);
		} else {
			let check = list[i].split(' ').slice(0, 2).join(' ');
			let checkr = list[i].split(' ').slice(0, 3).join(' ');
			
			if (whoZone[i] == "Geozona - Miestas" || whoZone[i] == "Rajonas - Miestas" || whoZone[i] == "Rajonas - Rajonas" || whoZone[i] == "Apskritis - Miestas" || whoZone[i] == "Apskritis - Rajonas" || whoZone[i] == "Apskritis - Geozona") {
				if (except.includes(checkr)) {
					listFirst = list[i].split(' ').slice(0, 3).join(' ');				
				} else {
					listFirst = list[i].split(' ').slice(0, 2).join(' ');				
				}
			} else {
				if (except.includes(check)) {
					listFirst = list[i].split(' ').slice(0, 2).join(' ');
				} else if (checkr.includes(latvia)) {
					let t = checkr.split(" ");
					t = t.indexOf(latvia) + 1;
					if (t == 2) {
						listFirst = list[i].split(' ').slice(0, 2).join(' ');
					} else {
						listFirst = list[i].split(' ').shift();
					}
				}else if(whoZone[i] == "Rajonas - Apskritis"){
					listFirst =  list[i].split(' ').slice(0, 2).join(' ');;
				} else {
					listFirst = list[i].split(' ').shift();
				}
			}

			let check2 = listSecond = list[i].split(' ').slice(-2).join(' ');
			let checkr2 = listSecond = list[i].split(' ').slice(-3).join(' ');

			if (whoZone[i] == "Apskritis - Rajonas" || whoZone[i] == "LTU - Rajonas" || whoZone[i] == "Miestas - Geozona" || whoZone[i] == "Miestas - Rajonas" || whoZone[i] == "Rajonas - Rajonas" ) {
				if (except.includes(checkr2)) {
					listSecond = list[i].split(' ').slice(-3).join(' ');
				} else {
					listSecond = list[i].split(' ').slice(-2).join(' ');
				}
			} else {
				if (except.includes(check2)) {
					listSecond = list[i].split(' ').slice(-2).join(' ');
				} else if (checkr.includes(latvia)) {
					let t = checkr.split(" ");
					t = t.indexOf(latvia) + 1;
					if (t == 2) {
						listSecond = list[i].split(' ').slice(-1).join(' ');
					} else {
						listSecond = list[i].split(' ').slice(1).join(' ');
					}
				} else {
					listSecond = list[i].split(' ').pop();
				}
			}

			let fieldFirst = document.querySelectorAll(".modal-table tr")[i].querySelectorAll('.selectas')[0];
			let fieldSecond = document.querySelectorAll(".modal-table tr")[i].querySelectorAll('.selectas')[1];

			const option = document.createElement('option');
			option.setAttribute('selected', 'selected');
			option.innerHTML = listFirst;
			fieldFirst.append(option);

			const option2 = document.createElement('option');
			option2.setAttribute('selected', 'selected');
			option2.innerHTML = listSecond;
			fieldSecond.append(option2);
		}
	}
	checkBothSide();
	postCodeInputs();
}

function checkBothSide() {
	let accountid = $('#accountid').val();
	let column = $('[data-col]').data('col');
	let type = 'check';

	var responce = new Promise(function (resolve, reject) {
		$.ajax({
			type: "POST",
			url: "/price-algorithm/bothSide.php",
			data: { accountid: accountid, column: column, type: type },
			success: function (result) {
				if (result == 1) {
					document.querySelector('.modal-body #bothSides').checked = true;
				} else {
					document.querySelector('.modal-body #bothSides').checked = false;
				}
			},
			error: function (err) {
				reject(err);
			}
		});
	});
}

function updateBothSide() {
	let accountid = $('#accountid').val();
	let column = $('[data-col]').data('col');
	let type = 'update';
	let update = '';

	if ($('#bothSides').is(':checked')) {
		update = 1;
	} else {
		update = 0;
	}

	var responce = new Promise(function (resolve, reject) {
		$.ajax({
			type: "POST",
			url: "/price-algorithm/bothSide.php",
			data: { accountid: accountid, column: column, type: type, update: update },
			success: function (result) {
			},
			error: function (err) {
				reject(err);
			}
		});
	});
}


function postCodeInputs() {
	let accountid = $('#accountid').val();
	let column = $('[data-col]').data('col');

	var responce = new Promise(function (resolve, reject) {
		let type = 'post';
		$.ajax({
			type: "POST",
			url: "/price-algorithm/directions.php",
			data: { accountid: accountid, column: column, type: type },
			success: function (result) {
				if (result != '') {
					let splitSlash = result.split("/");
					let rowArr = new Array();
					let codes = new Array();
					let code = new Array();
					let num = new Array();

					for (let i = 0; splitSlash.length > i; i++) {
						rowArr.push(splitSlash[i].split("="));
					}

					for (let e = 0; rowArr.length > e; e++) {
						let nr = rowArr[e][0] - 1;
						document.querySelectorAll('[data-col]')[nr].setAttribute('checked', 'checked');
						codes.push(rowArr[e][1].split(";"));
						rowlen = rowArr.length;
						if (rowlen == 1) rowlen = rowlen + 1;
						for (let f = 0; rowlen > f; f++) {
							if (codes[e][f] != undefined) {
								code.push(codes[e][f]);
								num.push(rowArr[e]);
							}
						}
					}
					createInputs(num, code);
				}
			},
			error: function (err) {
				reject(err);
			}
		});
	});

	var responce2 = new Promise(function (resolve, reject) {
		let type = 'adr';
		$.ajax({
			type: "POST",
			url: "/price-algorithm/directions.php",
			dataType: "JSON",
			data: { accountid: accountid, column: column, type: type },
			success: function (result) {
				if (result[0] != '' && result[0] != null) {
					let splitSlash = result[1].split("/");
					let rowArr = new Array();
					let codes = new Array();
					let code = new Array();
					let num = new Array();

					let splitSlash2 = result[0].split("/");
					let rowArr2 = new Array();
					let codes2 = new Array();
					let code2 = new Array();
					let num2 = new Array();

					for (let i = 0; splitSlash.length > i; i++) {
						rowArr.push(splitSlash[i].split("="));
						rowArr2.push(splitSlash2[i].split("="));
					}

					for (let e = 0; rowArr.length > e; e++) {
						let nr = rowArr[e][0] - 1;
						document.querySelectorAll('[data-column]')[nr].setAttribute('checked', 'checked');
						codes.push(rowArr[e][1].split(";"));
						codes2.push(rowArr2[e][1].split(";"));
						rowlen = rowArr.length;
						if (rowlen == 1) rowlen = rowlen + 1;
						for (let f = 0; rowlen > f; f++) {
							if (codes[e][f] != undefined) {
								code.push(codes[e][f]);
								num.push(rowArr[e]);
								code2.push(codes2[e][f]);
								num2.push(rowArr2[e]);
							}
						}
					}
					createInputs2(num, code, code2);
				}
			},
			error: function (err) {
				reject(err);
			}
		});
	});

}

function createInputs(row, codes) {

	for (let id = 0; codes.length > id; id++) {

		ids = row[id][0] - 1;

		if (id % 2 == 0) {
			let input = document.createElement('input');
			input.setAttribute('type', 'text');
			input.setAttribute('class', 'inputElement post');
			input.setAttribute('data-num', `${row[id][0]}`);
			input.setAttribute('style', 'margin-top: 10px;');
			input.setAttribute('id', `post_code${id}`);
			input.setAttribute('onkeyup', `loadCheckPostCode(event);`);
			input.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
			input.value = codes[id];
			let td = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[1];
			td.append(input);
		} else {
			let input2 = document.createElement('input');
			input2.setAttribute('type', 'text');
			input2.setAttribute('class', 'inputElement post ');
			input2.setAttribute('data-num', `${row[id][0]}`);
			input2.setAttribute('style', 'margin-top: 10px;');
			input2.setAttribute('id', `post_code${id}`);
			input2.setAttribute('onkeyup', `loadCheckPostCode(event);`);
			input2.setAttribute('placeholder', 'Pašto kodus atskirti kableliu');
			input2.value = codes[id];
			let td2 = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[2];
			td2.append(input2);
		}
	}

}


function createInputs2(row, adr, code) {
	for (let id = 0; code.length > id; id++) {
		ids = row[id][0] - 1;

		if (id % 2 == 0) {
			let input = document.createElement('input');
			input.setAttribute('type', 'text');
			input.setAttribute('class', `inputElement adr`);
			input.setAttribute('autocomplete', `off`);
			input.setAttribute('data-num', `${row[id][0]}`);
			input.setAttribute('style', 'margin-top: 10px;');
			input.setAttribute('id', `address${id}`);
			input.setAttribute('required', '');
			input.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');
			input.value = adr[id];

			let postInput = document.createElement('input');
			postInput.setAttribute('class', 'adr_code');
			postInput.setAttribute('type', 'hidden');
			postInput.setAttribute('data-num', `${row[id][0]}`);
			postInput.setAttribute('id', `address_post${id}`);
			postInput.value = code[id];

			let td = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[1];
			td.append(input);
			td.append(postInput);
			getSuggestionsAddress(id, 'from');
		} else {
			let input2 = document.createElement('input');
			input2.setAttribute('type', 'text');
			input2.setAttribute('class', `inputElement adr`);
			input2.setAttribute('autocomplete', `off`);
			input2.setAttribute('data-num', `${row[id][0]}`);
			input2.setAttribute('style', 'margin-top: 10px;');
			input2.setAttribute('id', `address${id}`);
			input2.setAttribute('required', '');
			input2.setAttribute('placeholder', 'Įveskite adresą arba pasirinkite iš sarašo');
			input2.value = adr[id];

			let postInput2 = document.createElement('input');
			postInput2.setAttribute('class', 'adr_code');
			postInput2.setAttribute('type', 'hidden');
			postInput2.setAttribute('data-num', `${row[id][0]}`);
			postInput2.setAttribute('id', `address_post${id}`);
			postInput2.value = code[id];

			let td2 = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[2];
			td2.append(input2);
			td2.append(postInput2);
			getSuggestionsAddress(id, 'to');

			let td3 = document.querySelectorAll('[data-append]')[ids].getElementsByTagName('td')[3];
			let span = document.createElement('span');
			span.setAttribute('class', 'fa fa-plus-circle  cursorPointer add_new_addresses');
			span.setAttribute('title', 'Pridėti adresą');
			span.setAttribute('style', 'font-size: 20px;margin-top: 25px;');
			span.setAttribute('data-toggle', 'modal');
			span.setAttribute('data-target', '#addNewAddress');
			td3.append(span);
		}
	}

}




// create new Pricebook

let Newpricebooks = new URLSearchParams(window.location.search);

if (Newpricebooks.get('module') == 'Newpricebooks' && (Newpricebooks.get('view') == 'Edit' || Newpricebooks.get('view') == 'EditPricebook')) {

	$(document).ready(function () {
		getSuggestionsPricebook();
	});

}


if (Newpricebooks.get('module') == 'Newpricebooks' && Newpricebooks.get('view') == 'EditPricebook') {
	$(document).ready(function () {
		let navbar = document.querySelector('#appnav .nav');
		let li = document.createElement('li');
		let btn = document.createElement('button');

		btn.setAttribute('class', 'btn addButton btn-default module-buttons');
		btn.setAttribute('id', 'copy_pricebook_btn');
		btn.setAttribute('type', 'submit');
		btn.setAttribute('form', 'edit_form');
		btn.innerHTML = 'Kopijuoti kainyną';
		li.append(btn);
		navbar.append(li);

		$('#copy_pricebook_btn').on('click', function () {
			$('#copy_pricebook').val(1);
		});
	});
}

if (Newpricebooks.get('module') == 'Newpricebooks' && Newpricebooks.get('view') == 'copyPricebook') {
	$(document).ready(function () {
		$('#copy_pricebook').val(1);
		$('.saveButton').click();
	});
}


function sendPriceBook() {
	if (document.querySelector('[name="emailAddress"]').value) {
		document.getElementById('sendEmail').submit();
		$('[name="emailAddress"]').removeClass('alert alert-danger');
	} else {
		$('[name="emailAddress"]').addClass('alert alert-danger');
	}
}


function getSuggestionsPricebook() {
	$.ajax({
		type: "POST",
		url: "/pricebookAjax/accountsSuggestions.php",
		data: { 1: 1 },
		dataType: 'JSON',
		success: function (result) {
			autocompletePricebook(document.getElementById(`pricebook_name`), result);
		}
	});
}


function autocompletePricebook(inp, arr) {
	/*the autocomplete function takes two arguments,
	the text field element and an array of possible autocompleted values:*/
	var currentFocus;
	let accountname = arr.accountname;
	let accountid = arr.accountid;

	/*execute a function when someone writes in the text field:*/
	inp.addEventListener("input", function (e) {
		$('#pricebook_name').removeClass('selected2');
		if (inp.value == '') {
			$('#client_block').remove();
			$('#accountid').val('');
		}

		var a, b, i, val = this.value;
		/*close any already open lists of autocompleted values*/
		closeAllLists();

		if (val.length >= 3) {

			if (!val) { return false; }
			currentFocus = -1;
			/*create a DIV element that will contain the items (values):*/
			a = document.createElement("DIV");
			a.setAttribute("id", this.id + "autocomplete-list");

			a.setAttribute("class", "autocomplete-items-employee");

			/*append the DIV element as a child of the autocomplete container:*/
			this.parentNode.appendChild(a);
			/*for each item in the array...*/
			for (i = 0; i < accountname.length; i++) {
				/*check if the item starts with the same letters as the text field value:*/
				if (((accountname[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
					// if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
					/*create a DIV element for each matching element:*/
					b = document.createElement("DIV");
					b.setAttribute('class', 'listas');
					/*make the matching letters bold:*/
					b.innerHTML = accountname[i].substr(0, val.length);
					b.innerHTML += accountname[i].substr(val.length);
					/*insert a input field that will hold the current array item's value:*/
					b.innerHTML += "<input type='hidden' value='" + accountname[i] + "'>";
					/*execute a function when someone clicks on the item value (DIV element):*/
					b.addEventListener("click", function (e) {
						/*insert the value for the autocomplete text field:*/
						inp.value = this.getElementsByTagName("input")[0].value;
						/*close the list of autocompleted values,
						(or any other open lists of autocompleted values:*/
						closeAllLists();
					});

					accid = document.createElement("input");
					accid.setAttribute('class', 'accid');
					accid.setAttribute('type', 'hidden');
					accid.setAttribute('value', `${accountid[i]}`);

					a.appendChild(b);
					b.appendChild(accid);
					let accidClass = document.querySelectorAll('.accid');
					let listas = document.querySelectorAll('.listas');

					for (let i = 0; i < listas.length; i++) {
						listas[i].onclick = () => {
							// inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
							inp.classList.add('selected2');
							if (accountid !== undefined) {
								$('#accountid').val(accidClass[i].value);
								loadClientCardZones(accidClass[i].value);
							}
						}
					}

				}
				inp.onchange = () => {
					if (!inp.classList.contains('selected2')) {
						// inp.value = ''; jei nieko nepasirinkama is saraso istustina laukeli
					}
				}
			}
		}
	});




	/*execute a function presses a key on the keyboard:*/
	inp.addEventListener("keydown", function (e) {
		var x = document.getElementById(this.id + "autocomplete-list");
		if (x) x = x.getElementsByTagName("div");
		if (e.keyCode == 40) {
			/*If the arrow DOWN key is pressed,
			increase the currentFocus variable:*/
			currentFocus++;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 38) { //up
			/*If the arrow UP key is pressed,
			decrease the currentFocus variable:*/
			currentFocus--;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 13) {
			/*If the ENTER key is pressed, prevent the form from being submitted,*/
			e.preventDefault();
			if (currentFocus > -1) {
				/*and simulate a click on the "active" item:*/
				if (x) x[currentFocus].click();
			}
		}
	});
	function addActive(x) {
		/*a function to classify an item as "active":*/
		if (!x) return false;
		/*start by removing the "active" class on all items:*/
		removeActive(x);
		if (currentFocus >= x.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (x.length - 1);
		/*add class "autocomplete-active":*/
		x[currentFocus].classList.add("autocomplete-active");
	}
	function removeActive(x) {
		/*a function to remove the "active" class from all autocomplete items:*/
		for (var i = 0; i < x.length; i++) {
			x[i].classList.remove("autocomplete-active");
		}
	}
	function closeAllLists(elmnt) {
		/*close all autocomplete lists in the document,
		except the one passed as an argument:*/
		var x = document.getElementsByClassName("autocomplete-items-employee");
		for (var i = 0; i < x.length; i++) {
			if (elmnt != x[i] && elmnt != inp) {
				x[i].parentNode.removeChild(x[i]);
			}
		}
	}
	/*execute a function when someone clicks in the document:*/
	document.addEventListener("click", function (e) {
		closeAllLists(e.target);

	});
}

function autoCompleteAddress(inp, arr, which, number) {
	/*the autocomplete function takes two arguments,
 the text field element and an array of possible autocompleted values:*/
	var currentFocus;
	let company;
	let postCode;

	let except = document.getElementsByTagName('input');


	if (which == 'from') company = arr.load_company; else if (which == 'to') company = arr.unload_company;
	if (which == 'from') postCode = arr.bill_code; else if (which == 'to') postCode = arr.ship_code;

	/*execute a function when someone writes in the text field:*/
	inp.addEventListener("input", function (e) {

		if (which == 'from') {
			$(`#address${number}`).removeClass('selected2');
		} else if (which == 'to') {
			$(`#address${number}`).removeClass('selected2');
		}


		var a, b, i, val = this.value;
		/*close any already open lists of autocompleted values*/
		closeAllLists();
		if (!val) { return false; }
		currentFocus = -1;
		/*create a DIV element that will contain the items (values):*/
		a = document.createElement("DIV");
		a.setAttribute("id", this.id + "autocomplete-list");
		a.setAttribute("class", "autocomplete-items-employee");

		/*append the DIV element as a child of the autocomplete container:*/
		this.parentNode.appendChild(a);
		/*for each item in the array...*/
		for (i = 0; i < company.length; i++) {
			/*check if the item starts with the same letters as the text field value:*/
			if (((company[i].toLowerCase()).indexOf(val.toLowerCase())) > -1) {
				// if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
				/*create a DIV element for each matching element:*/
				b = document.createElement("DIV");
				b.setAttribute('class', 'listas');
				/*make the matching letters bold:*/
				b.innerHTML = company[i].substr(0, val.length);
				b.innerHTML += company[i].substr(val.length);
				/*insert a input field that will hold the current array item's value:*/
				b.innerHTML += "<input type='hidden' value='" + company[i] + "'>";

				/*execute a function when someone clicks on the item value (DIV element):*/
				b.addEventListener("click", function (e) {
					/*insert the value for the autocomplete text field:*/
					inp.value = this.getElementsByTagName("input")[0].value;
					/*close the list of autocompleted values,
					(or any other open lists of autocompleted values:*/
					closeAllLists();
				});

				code = document.createElement("input");
				code.setAttribute('class', 'code');
				code.setAttribute('type', 'hidden');
				code.setAttribute('value', `${postCode[i]}`);

				a.appendChild(b);
				b.appendChild(code);

				let listas = document.querySelectorAll('.listas');
				let codeClass = document.querySelectorAll('.code');


				for (let i = 0; i < listas.length; i++) {
					listas[i].onclick = () => {
						// inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
						inp.classList.add('selected');
						if (which == 'from') {
							$(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
						} else {
							$(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
						}

					}
				}

			}
			inp.onchange = () => {
				if (!inp.classList.contains('selected')) {
					inp.value = ''; // jei nieko nepasirinkama is saraso istustina laukeli
				}
			}
		}
	});


	inp.addEventListener("click", function (e) {


		var a, b, i, val = this.value;
		/*close any already open lists of autocompleted values*/
		closeAllLists();
		// if (!val) { return false;}
		currentFocus = -1;
		/*create a DIV element that will contain the items (values):*/
		a = document.createElement("DIV");
		a.setAttribute("id", this.id + "autocomplete-list");
		a.setAttribute("class", "autocomplete-items-employee");

		/*append the DIV element as a child of the autocomplete container:*/
		this.parentNode.appendChild(a);

		/*for each item in the array...*/
		for (i = 0; i < company.length; i++) {
			/*check if the item starts with the same letters as the text field value:*/

			// if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
			/*create a DIV element for each matching element:*/
			b = document.createElement("DIV");
			b.setAttribute('class', 'listas');
			/*make the matching letters bold:*/
			b.innerHTML = company[i];
			//  b.innerHTML += company[i];
			/*insert a input field that will hold the current array item's value:*/
			b.innerHTML += "<input type='hidden' value='" + company[i] + "'>";

			/*execute a function when someone clicks on the item value (DIV element):*/
			b.addEventListener("click", function (e) {
				/*insert the value for the autocomplete text field:*/
				inp.value = this.getElementsByTagName("input")[0].value;
				/*close the list of autocompleted values,
				(or any other open lists of autocompleted values:*/
				closeAllLists();
			});

			code = document.createElement("input");
			code.setAttribute('class', 'code');
			code.setAttribute('type', 'hidden');
			code.setAttribute('value', `${postCode[i]}`);

			a.appendChild(b);
			b.appendChild(code);

			let listas = document.querySelectorAll('.listas');
			let codeClass = document.querySelectorAll('.code');


			for (let i = 0; i < listas.length; i++) {
				listas[i].onclick = () => {
					// inp.readOnly = true; jei pasirenkamas siulomas irasas inputas pasidaro read only
					inp.classList.add('selected2');
					if (which == 'from') {
						$(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
					} else {
						$(`#address_post${number}`).val((codeClass[i].value != 'null' ? codeClass[i].value : ''));
					}

				}
			}


			inp.onchange = () => {
				if (!inp.classList.contains('selected')) {
					inp.value = ''; // jei nieko nepasirinkama is saraso istustina laukeli
				}
			}
		}
	});



	/*execute a function presses a key on the keyboard:*/
	inp.addEventListener("keydown", function (e) {

		var x = document.getElementById(this.id + "autocomplete-list");
		if (x) x = x.getElementsByTagName("div");
		if (e.keyCode == 40) {
			/*If the arrow DOWN key is pressed,
			increase the currentFocus variable:*/
			currentFocus++;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 38) { //up
			/*If the arrow UP key is pressed,
			decrease the currentFocus variable:*/
			currentFocus--;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 13) {
			/*If the ENTER key is pressed, prevent the form from being submitted,*/
			e.preventDefault();
			if (currentFocus > -1) {
				/*and simulate a click on the "active" item:*/
				if (x) x[currentFocus].click();
			}
		}
	});



	function addActive(x) {
		/*a function to classify an item as "active":*/
		if (!x) return false;
		/*start by removing the "active" class on all items:*/
		removeActive(x);
		if (currentFocus >= x.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (x.length - 1);
		/*add class "autocomplete-active":*/
		x[currentFocus].classList.add("autocomplete-active");
	}
	function removeActive(x) {
		/*a function to remove the "active" class from all autocomplete items:*/
		for (var i = 0; i < x.length; i++) {
			x[i].classList.remove("autocomplete-active");
		}
	}
	function closeAllLists(elmnt) {
		/*close all autocomplete lists in the document,
		except the one passed as an argument:*/
		var x = document.getElementsByClassName("autocomplete-items-employee");

		for (var i = 0; i < x.length; i++) {
			if (elmnt != x[i] && elmnt != inp) {
				x[i].parentNode.removeChild(x[i]);
			}
		}
	}
	/*execute a function when someone clicks in the document:*/
	document.addEventListener("click", function (e) {
		closeAllLists(e.target);

	}, false);

	for (var i = 0; i < except.length; i++) {
		except[i].addEventListener("click", function (ev) {
			ev.stopPropagation();
		}, false);
	}
}


function loadClientCardZones(accountid) {
	let zonesPlaces = document.getElementById('client_zones_place');

	$.ajax({
		type: "POST",
		url: "/pricebookAjax/getClientCartInfo.php",
		data: { accountid: accountid },
		dataType: 'JSON',
		success: function (result) {
			let html = '';
			if (result != null) {
				data = result.zone_types;
				html += `
				<div class="container block" id="client_block">    
      	<h4 class="textOverflowEllipsis maxWidth50">Kliento ${result.client_zones.accountname} kortelės zonos</h4>  
  			<hr>

				<table class="table detailview-table no-border" id="zones_table">
				<tbody>	
				<tr>		
				<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 1</span></td>	
				<td class="fieldValue" >
					<span id="zone1" data-typecolumn="cf_1216" data-column="cf_1218">${(result.client_zones.cf_1218 != null ? result.client_zones.cf_1218 : '')}</span>
				<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(1);" class="editAction fa fa-pencil"></a></span>
				</td>			
			
				<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 2</span></td>
	
				<td class="fieldValue" >
					<span id="zone2" data-typecolumn="cf_1220" data-column="cf_1222">${(result.client_zones.cf_1222 != null ? result.client_zones.cf_1222 : '')}</span>
				<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(2);" class="editAction fa fa-pencil" id="pencil-2"></a></span>
				</td>
			</tr>

			<tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 3</span></td>
      <td class="fieldValue" >
        <span id="zone3" data-typecolumn="cf_1224" data-column="cf_1226">${(result.client_zones.cf_1226 != null ? result.client_zones.cf_1226 : '')}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(3);" class="editAction fa fa-pencil" id="pencil-3"></a></span>
      </td>
		<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 4</span></td>
		<td class="fieldValue" >
			<span id="zone4" data-typecolumn="cf_1230" data-column="cf_1230">${(result.client_zones.cf_1230 != null ? result.client_zones.cf_1230 : '')}</span>
		<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(4);" class="editAction fa fa-pencil" id="pencil-4"></a></span>
		</td>
	</tr>

	<tr>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 5</span></td>
	<td class="fieldValue" >
		<span id="zone5" data-typecolumn="cf_1232" data-column="cf_1234">${(result.client_zones.cf_1234 != null ? result.client_zones.cf_1234 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(5);" class="editAction fa fa-pencil" id="pencil-5"></a></span>
	</td>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 6</span></td>

	<td class="fieldValue" >
		<span id="zone6" data-typecolumn="cf_1236" data-column="cf_1238">${(result.client_zones.cf_1238 != null ? result.client_zones.cf_1238 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(6);" class="editAction fa fa-pencil" id="pencil-6"></a></span>
	</td>
</tr>

 <tr>	
	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 7</span></td>
	<td class="fieldValue" >
		<span id="zone7" data-typecolumn="cf_1240" data-column="cf_1242">${(result.client_zones.cf_1242 != null ? result.client_zones.cf_1242 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(7);" class="editAction fa fa-pencil" id="pencil-7"></a></span>
	</td>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 8</span></td>
	<td class="fieldValue" >
		<span id="zone8" data-typecolumn="cf_1244" data-column="cf_1246">${(result.client_zones.cf_1246 != null ? result.client_zones.cf_1246 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(8);" class="editAction fa fa-pencil" id="pencil-8"></a></span>
	</td>
</tr>

 <tr>
	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 9</span></td>

	<td class="fieldValue" >
		<span id="zone9" data-typecolumn="cf_1248" data-column="cf_1250">${(result.client_zones.cf_1250 != null ? result.client_zones.cf_1250 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(9);" class="editAction fa fa-pencil" id="pencil-9"></a></span>
	</td>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 10</span></td>

	<td class="fieldValue" >
		<span id="zone10" data-typecolumn="cf_1252" data-column="cf_1254">${(result.client_zones.cf_1254 != null ? result.client_zones.cf_1254 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(10);" class="editAction fa fa-pencil" id="pencil-10"></a></span>
	</td>
</tr>

 <tr>
	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 11</span></td>

	<td class="fieldValue" >
		<span id="zone11" data-typecolumn="cf_1398" data-column="cf_1400">${(result.client_zones.cf_1400 != null ? result.client_zones.cf_1400 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(11);" class="editAction fa fa-pencil" id="pencil-11"></a></span>
	</td>
	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 12</span></td>

	<td class="fieldValue" >
		<span id="zone12" data-typecolumn="cf_1402" data-column="cf_1404">${(result.client_zones.cf_1404 != null ? result.client_zones.cf_1404 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(12);" class="editAction fa fa-pencil" id="pencil-12"></a></span>
	</td>
</tr>

 <tr>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 13</span></td>

	<td class="fieldValue" >
		<span id="zone13" data-typecolumn="cf_1406" data-column="cf_1408">${(result.client_zones.cf_1408 != null ? result.client_zones.cf_1408 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(13);" class="editAction fa fa-pencil" id="pencil-13"></a></span>
	</td>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 14</span></td>

	<td class="fieldValue" >
		<span id="zone14" data-typecolumn="cf_1410" data-column="cf_1412">${(result.client_zones.cf_1412 != null ? result.client_zones.cf_1412 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(14);" class="editAction fa fa-pencil" id="pencil-14"></a></span>
	</td>
</tr>

 <tr>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 15</span></td>

	<td class="fieldValue" >
		<span id="zone15" data-typecolumn="cf_1414" data-column="cf_1416">${(result.client_zones.cf_1416 != null ? result.client_zones.cf_1416 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(15);" class="editAction fa fa-pencil" id="pencil-15"></a></span>
	</td>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 16</span></td>
	<td class="fieldValue" >
		<span id="zone16" data-typecolumn="cf_1418" data-column="cf_1420">${(result.client_zones.cf_1420 != null ? result.client_zones.cf_1420 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(16);" class="editAction fa fa-pencil" id="pencil-16"></a></span>
	</td>
</tr>

 <tr>	
	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 17</span></td>
	<td class="fieldValue" >
		<span id="zone17" data-typecolumn="cf_1422" data-column="cf_1424">${(result.client_zones.cf_1424 != null ? result.client_zones.cf_1424 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(17);" class="editAction fa fa-pencil" id="pencil-17"></a></span>
	</td>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 18</span></td>
	<td class="fieldValue" >
		<span id="zone18" data-typecolumn="cf_1426" data-column="cf_1428">${(result.client_zones.cf_1428 != null ? result.client_zones.cf_1428 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(18);" class="editAction fa fa-pencil" id="pencil-18"></a></span>
	</td>
</tr>

 <tr>
	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 19</span></td>

	<td class="fieldValue" >
		<span id="zone19" data-typecolumn="cf_1430" data-column="cf_1432">${(result.client_zones.cf_1432 != null ? result.client_zones.cf_1432 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(19);" class="editAction fa fa-pencil" id="pencil-19"></a></span>
	</td>

	<td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 20</span></td>

	<td class="fieldValue" >
		<span id="zone20" data-typecolumn="cf_1434" data-column="cf_1436">${(result.client_zones.cf_1436 != null ? result.client_zones.cf_1436 : '')}</span>
	<span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(20);" class="editAction fa fa-pencil" id="pencil-20"></a></span>
	</td>
</tr>
	</tbody>
	</table> 
	</div>`;

				zonesPlaces.innerHTML = html;

			}
		}
	});


}


