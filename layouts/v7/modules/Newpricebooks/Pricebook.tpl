<link rel="stylesheet" href="layouts/v7/modules/Newpricebooks/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Newpricebooks/resources/main.js?u=1"></script>


{assign var=products value=$products}
{assign var=prices value=$prices}
{assign var=pricebook_name value=$pricebook_name}
{assign var=zones_num value=$zones_num}
{assign var=clientZones value=$clientZones}
{assign var=zone_types value=$zone_types}
{assign var=accountid value=$accountInfo['accountid']}
{assign var=accountname value=$accountInfo['accountname']}
{assign var=pricebookid value=$pricebookid}
{assign var=pricebookClients value=$pricebookClients}
{assign var=accountnameGET value=$accountname}

{foreach from=$prices item=price} 
  {$bigger_zone[] = $price['count']}
{/foreach}

{$count_zones_value = max($bigger_zone)}

{if $smarty.cookies.pricebookid eq $pricebookid}
  {if !empty($smarty.cookies.orders)}
    <div class="alert alert-success" id="alert_message" style="margin-top: 10px; width: 50%;justify-content: center;display: flex;margin-left: auto;margin-right: auto;">
      {$smarty.cookies.orders}
    </div>
  {/if}
{/if}

<div id="table-content" class="table-container">
<div>
  <div style="float:left;margin-left: 5%;"><h3>{$pricebook_name}</h3></div>

  {* <div style="float:right; margin-right: 10%;">
    <h5>Kainyno zonos</h5>
    <form method="POST">
    <select class="inputElement disabled" name='zone-count' onchange='this.form.submit()' disabled>
    {for $i = 1 to 20}
      <option {if $zones_num eq $i}selected{/if}>{$i}</option>
    {/for}  
    </select>
    </form>
  </div> *}
</div>


<div id="accordion">
 {if $accountid}
     <button id="collapseBtnOne" class="btn selected" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="margin-top: 40px;margin-left: 70px;">Kainynas</button> 
    <button id="collapseBtnTwo" class="btn collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="margin-top: 40px;">{if $accountid neq 10965}Kliento kortelės zonos{else}BAZINISLT zonos{/if}</button>
 
{if $accountid neq 10965}
  <form method="POST" style="display: inline;">
    <select class="inputElement" name='change_client' onchange='this.form.submit()' style="width: 200px; position: absolute; margin-top: 40px; margin-left: 3px;">
   {foreach from=$pricebookClients item=clients} 
      <option {if $accountnameGET eq $clients['accountname']}selected{/if} value="{$clients['accountname']}">{$clients['accountname']}</option>
    {/foreach}  
    </select>
  </form>
  {/if}
 {/if}
  <div class="card">
    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">


<table id="listview-table" class="table listview-table" style="border-top: 1px solid #ddd;">
<thead>
 <tr>
  <th><h4>Krovinio kiekis</h4></th>
  <th><h4>Kainos,EUR</h4></th>
</tr>
</thead>
 <tr>
  <td>
    <table id="listview-table" class="table listview-table" style="border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;" >
      <thead>
      <tr>
        <th>Kg</th>
        <th>M3</th>
        <th>M2</th>
        <th>M</th>
        <th title="Krovos darbų kaina">K/D</th>
        <th>Pll</th>
      </tr>
      </thead>
    <tbody>
  {foreach from=$products item=product}  
      <tr>
          <td title="min: {$product['min_weight']}">{$product['max_weight']}</td> 
          <td title="min: {$product['min_volume']}">{$product['max_volume']}</td> 
          <td title="min: {$product['min_square']}">{$product['max_square']}</td>   
          <td>{$product['max_meter']}</td>   
          <td>{$product['stevedoring']}</td>   
          <td>{$product['pll']}</td>   
      </tr>  
  {/foreach}   

</tbody>
</table>  

</td>
<td>
<table id="listview-table" class="table listview-table" style="border-bottom: 1px solid #ddd;">
      <thead>
      <tr>
     {for $len=1 to $zones_num}
        <th>Zona {$len}</th>
     {/for}
      </tr>
      </thead>
    <tbody>

{for $len=0 to $count_zones_value-2}
  <tr>     
    {for $c=1 to $zones_num}
      {$zona= "zona`$c`"}
      {$zone = "zone`$c`"}

      {foreach from=$prices item=zona}   
        {if $zona[$zone]}
          <td> {$zona[$zone][$len]}</td>
        {/if}
      {/foreach}
    {/for}
   </tr> 

{/for}

</tbody>
</table>  
</td>
</tbody>
</table> 

</div>
    </div>
  </div>
<div id="wait" style="display:none;position:absolute;top:25%;left:43%;padding:2px;"><img src="/resources/loading.gif"></div>

<div class="modal fade" id="selectCitiesModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pasirinkite kryptis</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center" data-zone-column="">
        <div id="wait2" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
        Į abi puses <input id="bothSides" type="checkbox" class="inputElement" title="Pažymėjus kryptys galios į abi puses" onclick="updateBothSide();"><br><i class="fa fa-arrow-left" style="color: grey;"></i><i class="fa fa-arrow-right" style="color: grey;"></i>
        <table class="table" id="table_begin">
          <thead>
            <tr>            
              <td>Tipas</td>
              <td>Iš kur</td>
              <td>Į kur</td>
              <td width="10px;">P/K</td>
              <td width="10px;">Adr.</td>
              <td width="10px;"></td>
            </tr>
          </thead>
          <tbody class="modal-table">
            </tbody>
          </table>
          </div>
    <div class="modal-footer">
 <div style="display:inline-block; float: left;">
    <span style="display: flex;">P/K&nbsp; -&nbsp; Pašto kodai</span>
    <span style="display: flex;">Adr.&nbsp; -&nbsp; Konkretus adresas</span>
    <span style="display: flex;"><i class="fa fa-arrow-left" style="color: grey;"></i><i class="fa fa-arrow-right" style="color: grey;"></i>&nbsp;&nbsp; Galioja į abi puses</span>
</div>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      <button type="button" class="btn btn-primary" data-add-field="modal">Pridėti laukelį</button>
      <button type="button" class="btn btn-success" data-save="modal">Išsaugoti</button>
    </div>
        </div>
      </div>
    </div>


<div class="modal fade" id="addNewAddress" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true" style="top: 30px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5  id="ModalLabel">Naujų adresų sukūrimas</h5>
      </div>
      <div class="modal-body">
       <div id="wait3" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
 <label>Pakrovimas</label>
 <input type="hidden" id="fromInput">
 <input type="hidden" id="toInput">
<table id="new_address">
<tr>
<td>
  <div class="form-group">
    <label>Įmonė</label>
    <input type="text" class="form-control" name="title"> 
		<div id="req-title" class="alert alert-danger hide">Įveskite įmonę</div>  	
  </div>
</td>
<td>
  <div class="form-group">
    <label>Miestas:</label>
    <input type="text" class="form-control" name="city">
		<div id="req-city" class="alert alert-danger hide">Įveskite miestą</div>  
  </div>
</td>
<td>
  <div class="form-group">
    <label>Adresas:</label>
    <input type="text" class="form-control" name="address">
		<div id="req-address" class="alert alert-danger hide">Įveskite adresą</div>  
  </div>
</td>

<td>
  <div class="form-group">
    <label>Pašto kodas:</label>
    <input type="text" class="form-control" name="code">
		<div id="req-code" class="alert alert-danger hide">Įveskite pašto kodą</div>  
  </div>
</td>

</tr>
</table>

 <label>Iškrovimas</label>
 <table id="new_address2">
<tr>
<td>
  <div class="form-group">
    <label>Įmonė</label>
    <input type="text" class="form-control" name="title2"> 
		<div id="req-title2" class="alert alert-danger hide">Įveskite įmonę</div>  
  </div>
</td>

<td>
  <div class="form-group">
    <label>Miestas:</label>
    <input type="text" class="form-control" name="city2">
		<div id="req-city2" class="alert alert-danger hide">Įveskite miestą</div>  
  </div>
</td>

<td>
  <div class="form-group">
    <label>Adresas:</label>
    <input type="text" class="form-control" name="address2">
		<div id="req-address2" class="alert alert-danger hide">Įveskite adresą</div>  
  </div>
</td>


<td>
  <div class="form-group">
    <label>Pašto kodas:</label>
    <input type="text" class="form-control" name="code2">
		<div id="req-code2" class="alert alert-danger hide">Įveskite pašto kodą</div>  
  </div>
</td>
</tr>
</table>
      </div>
      <div class="modal-footer">
				<button type="button" id="submit_new_address" class="btn btn-primary">Išsaugoti</button>
        <button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="sendPriceBook" tabindex="-1" role="dialog" aria-labelledby="sendPriceBookLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 35%;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendPriceBookLabel">Kainyno siuntimas klientui el. paštu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="sendEmail">
         <div class="form-group">
          <input name="emailAddress" type="email" class="form-control" placeholder="Kliento el. pašto adresas" required>
            </div>
          <div class="form-group">
            <textarea class="form-control" rows="5" name="message" placeholder="Žinutė"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Atšaukti</button>
        <button type="button" class="btn btn-primary" onclick="sendPriceBook()">Siųsti kainyną <i class="fa fa-paper-plane"></i></button>
      </div>
    </div>
  </div>
</div>


<input type="hidden" id="accountid" value="{$accountid}">
<div class="card">
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
 
  <div class="container block">    
      <h4 class="textOverflowEllipsis maxWidth50">{if $accountid != '10965'}Kliento{/if} {$accountname} {if $accountid != '10965'}kortelės{/if} zonos</h4>  
  <hr>

    <table class="table detailview-table no-border" id="zones_table">
      <tbody>
    {foreach from=$clientZones item=client_zones} 
      <tr>    

      
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 1</span></td>

      <td class="fieldValue" >
        <span id="zone1" data-typecolumn="cf_1216" data-column="cf_1218">{$client_zones['cf_1218']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(1);" class="editAction fa fa-pencil"></a></span>
      </td>
     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 2</span></td>

      <td class="fieldValue" >
        <span id="zone2" data-typecolumn="cf_1220" data-column="cf_1222">{$client_zones['cf_1222']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(2);" class="editAction fa fa-pencil" id="pencil-2"></a></span>
      </td>
    </tr>

    <tr>
      
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 3</span></td>

      <td class="fieldValue" >
        <span id="zone3" data-typecolumn="cf_1224" data-column="cf_1226">{$client_zones['cf_1226']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(3);" class="editAction fa fa-pencil" id="pencil-3"></a></span>
      </td>
    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 4</span></td>

      <td class="fieldValue" >
        <span id="zone4" data-typecolumn="cf_1228" data-column="cf_1230">{$client_zones['cf_1230']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(4);" class="editAction fa fa-pencil" id="pencil-4"></a></span>
      </td>
    </tr>

      <tr>
     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 5</span></td>

      <td class="fieldValue" >
        <span id="zone5" data-typecolumn="cf_1232" data-column="cf_1234">{$client_zones['cf_1234']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(5);" class="editAction fa fa-pencil" id="pencil-5"></a></span>
      </td>   
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 6</span></td>

      <td class="fieldValue" >
        <span id="zone6" data-typecolumn="cf_1236" data-column="cf_1238">{$client_zones['cf_1238']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(6);" class="editAction fa fa-pencil" id="pencil-6"></a></span>
      </td>
    </tr>

     <tr>
     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 7</span></td>

      <td class="fieldValue" >
        <span id="zone7" data-typecolumn="cf_1240" data-column="cf_1242">{$client_zones['cf_1242']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(7);" class="editAction fa fa-pencil" id="pencil-7"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 8</span></td>

      <td class="fieldValue" >
        <span id="zone8" data-typecolumn="cf_1244" data-column="cf_1246">{$client_zones['cf_1246']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(8);" class="editAction fa fa-pencil" id="pencil-8"></a></span>
      </td>
    </tr>

     <tr>      
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 9</span></td>

      <td class="fieldValue" >
        <span id="zone9" data-typecolumn="cf_1248" data-column="cf_1250">{$client_zones['cf_1250']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(9);" class="editAction fa fa-pencil" id="pencil-9"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 10</span></td>

      <td class="fieldValue" >
        <span id="zone10" data-typecolumn="cf_1252" data-column="cf_1254">{$client_zones['cf_1254']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(10);" class="editAction fa fa-pencil" id="pencil-10"></a></span>
      </td>
    </tr>

     <tr>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 11</span></td>

      <td class="fieldValue" >
        <span id="zone11" data-typecolumn="cf_1398" data-column="cf_1400">{$client_zones['cf_1400']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(11);" class="editAction fa fa-pencil" id="pencil-11"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 12</span></td>

      <td class="fieldValue" >
        <span id="zone12" data-typecolumn="cf_1402" data-column="cf_1404">{$client_zones['cf_1404']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(12);" class="editAction fa fa-pencil" id="pencil-12"></a></span>
      </td>
    </tr>

     <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 13</span></td>

      <td class="fieldValue" >
        <span id="zone13" data-typecolumn="cf_1406" data-column="cf_1408">{$client_zones['cf_1408']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(13);" class="editAction fa fa-pencil" id="pencil-13"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 14</span></td>

      <td class="fieldValue" >
        <span id="zone14" data-typecolumn="cf_1410" data-column="cf_1412">{$client_zones['cf_1412']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(14);" class="editAction fa fa-pencil" id="pencil-14"></a></span>
      </td>
    </tr>

     <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 15</span></td>

      <td class="fieldValue" >
        <span id="zone15" data-typecolumn="cf_1414" data-column="cf_1416">{$client_zones['cf_1416']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(15);" class="editAction fa fa-pencil" id="pencil-15"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 16</span></td>

      <td class="fieldValue" >
        <span id="zone16" data-typecolumn="cf_1418" data-column="cf_1420">{$client_zones['cf_1420']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(16);" class="editAction fa fa-pencil" id="pencil-16"></a></span>
      </td>
    </tr>

     <tr>
          <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 17</span></td>

      <td class="fieldValue" >
        <span id="zone17" data-typecolumn="cf_1422" data-column="cf_1424">{$client_zones['cf_1424']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(17);" class="editAction fa fa-pencil" id="pencil-17"></a></span>
      </td>
    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 18</span></td>

      <td class="fieldValue" >
        <span id="zone18" data-typecolumn="cf_1426" data-column="cf_1428">{$client_zones['cf_1428']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(18);" class="editAction fa fa-pencil" id="pencil-18"></a></span>
      </td>
    </tr>

     <tr>   
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 19</span></td>

      <td class="fieldValue" >
        <span id="zone19" data-typecolumn="cf_1430" data-column="cf_1432">{$client_zones['cf_1432']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(19);" class="editAction fa fa-pencil" id="pencil-19"></a></span>
      </td>   
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 20</span></td>

      <td class="fieldValue" >
        <span id="zone20" data-typecolumn="cf_1434" data-column="cf_1436">{$client_zones['cf_1436']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(20);" class="editAction fa fa-pencil" id="pencil-20"></a></span>
      </td>
    </tr>
  
  {/foreach}  

    </tbody>
    </table> 
     

      </div>
    </div>
  </div>


</div>



</div>

