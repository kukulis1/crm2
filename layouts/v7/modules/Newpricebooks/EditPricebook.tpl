<link rel="stylesheet" href="layouts/v7/modules/Newpricebooks/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Newpricebooks/resources/main.js?u=1"></script>


{assign var=products value=$products}
{assign var=prices value=$prices}
{assign var=pricebook_name value=$pricebook_name}
{assign var=zones_num value=$zones_num}
{assign var=zones_num value=$zones_num}
{assign var=post value=$post}
{assign var=clientZones value=$clientZones}
{assign var=zone_types value=$zone_types}
{assign var=accountid value=$accountInfo['accountid']}
{assign var=accountname value=$accountInfo['accountname']}
{assign var=accountname value=$accountInfo['accountname']}
{assign var=pricebookClients value=$pricebookClients}
{assign var=accountnameGET value=$accountname}
{assign var=zonesFromPriceBook value=$zonesFromPriceBook}

{foreach from=$prices item=price} 
  {$bigger_zone[] = $price['count']}
{/foreach}

{$count_zones_value = max($bigger_zone)}


<form method="POST" id="edit_form">
<div id="table-content" class="table-container">
<div id="wait" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>
  {* <div style="float:left;margin-left: 5%;"><h3>{$pricebook_name}</h3> </div> *}
  <div style="float:left;margin-left: 5%;">   
    <input id="pricebook_name" class="inputElement" style="width: 400px; margin-top: 20px;" name="pricebook_name" value="{$pricebook_name}" autocomplete="off" required {if $accountid eq 10965}readonly{/if}>
    {if $zonesFromPriceBook}    
      <input type="checkbox" name="copyZonesFromPriceBook"> Kopijuoti zonas iš kainyno priskiriamam klientui
    {/if}
    {* <input type="hidden" id="accountid" name="accountid"> *}
  </div>

  <div style="float:right; margin-right: 10%;">
    <h5>Kainyno zonos</h5>
    <select class="inputElement" name='zone-count' id="zone_num">
    {for $i = 1 to 20}
      <option {if $zones_num eq $i}selected{/if}>{$i}</option>
    {/for}  
    </select>
<input id="copy_pricebook" type="hidden" name="copy_pricebook" value="">
</div>
</div>


<input type="hidden" name="accountid" id="accountid" value="{$accountid}">

<table id="listview-table" class="table listview-table" style="border-top: 1px solid #ddd;">
  <input type="hidden" name="pricebook_form" value="1">
  <input type="hidden" name="count_zones_value" value="{$count_zones_value-1}">
<thead>
 <tr>
  <th><h4>Krovinio kiekis</h4></th>
  <th><h4>Kainos,EUR</h4></th>
</tr>
</thead>
 <tr>
  <td>
    <table id="listview-table" class="table listview-table" style="border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;" >
      <thead>
      <tr>
        <th></th>
        <th>Kg</th>
        <th>M3</th>
        <th>M2</th>
        <th>M</th>
        <th title="Krovos darbų kaina">K/D</th>
        <th>Pll</th>
      </tr>
      </thead>
    <tbody id="dimensions-body">
  {foreach from=$products key=key item=product}  
      <tr data-line="{$key+1}">
          <td style="width: 30px;">
            <button onclick="deleteField({$key+1});" type="button" class="btn delete_field_btn" style="background: transparent;outline: 0; padding: 0;"> <i class="fa fa-trash" style="cursor: pointer;"></i></button>       
          </td>
          <td>
            <input class="inputElement" name="max_weight{$key+1}" value="{$product['max_weight']}" style="width: 70px;" autocomplete="off"> 
            <input {if $product['kg_check']}checked{/if} title="Pažymėjus taikoma min reikšmė bus 0.001" type="checkbox" name="min_kg{$key+1}"> 
          </td> 
         
          <td>
            <input class="inputElement" name="max_volume{$key+1}" value="{$product['max_volume']}" style="width: 70px;" autocomplete="off"> 
            <input {if $product['m3_check']}checked{/if} title="Pažymėjus taikoma min reikšmė bus 0.001" type="checkbox" name="min_m3{$key+1}">
          </td> 
          
          <td>
            <input class="inputElement" name="max_square{$key+1}" value="{$product['max_square']}" style="width: 70px;" autocomplete="off"> 
            <input {if $product['m2_check']}checked{/if} title="Pažymėjus taikoma min reikšmė bus 0.001" type="checkbox" name="min_m2{$key+1}">
          </td>  

          <td>
            <input class="inputElement" name="max_meter{$key+1}" value="{$product['max_meter']}" style="width: 70px;" autocomplete="off"> 
            <input {if $product['m_check']}checked{/if} title="Pažymėjus taikoma min reikšmė bus 0.001" type="checkbox" name="min_m{$key+1}">
          </td>    
          
          <td><input class="inputElement" name="stevedoring{$key+1}" value="{$product['stevedoring']}" style="width: 70px;" autocomplete="off"></td>   
          <td><input class="inputElement" name="pll{$key+1}" value="{$product['pll']}" style="width: 70px;" autocomplete="off"></td>   
      </tr>  
  {/foreach}   

</tbody>
</table>  

</td>
<td>
  <table id="listview-table" class="table listview-table" style="border-bottom: 1px solid #ddd;">
        <thead id="zone-thead">
        <tr>
      {for $len=1 to $zones_num}
          <th>Zona {$len}</th>
      {/for}
        </tr>
        </thead>
      <tbody id="zones-body">

  {for $len=0 to $count_zones_value-2}
    <tr data-line="{$len+1}">     
      {for $c=1 to $zones_num} 
        {$zona = "zona`$c`"}
        {$zone = "zone`$c`"}
        {$proid = "proid`$c`"}

        {foreach from=$prices item=zona}   
          {if $zona[$zone]}
          {$proname = "product-`$c`-`$len+1`"}
            <td><input class="inputElement" data-zona="{$c}"  name="product-{$c}-{$len+1}" value="{if $zona[$zone][$len]}{$zona[$zone][$len]}{else}{$post[$proname]}{/if}" style="width: 70px;" autocomplete="off">
               <input type="hidden" data-hiddenid="{$c}" name="productid-{$c}-{$len+1}" value="{$zona[$zone][$proid][$len]}"> 
            </td>
          {/if}
        {/foreach}

      {/for}
    </tr> 

  {/for}

  </tbody>
  </table>  
</td>
</tbody>
</table>  

<div id="client_zones_place"></div>

 <div class='modal-overlay-footer clearfix' style="border-left: unset !important;">
    <div class="row clearfix">
        <div class='textAlignCenter col-lg-12 col-md-12 col-sm-12 '>
         <button class="btn btn-success saveButton" type="submit">{vtranslate('LBL_SAVE', $MODULE)}</button> &nbsp;&nbsp;
            <a class='cancelLink' href="javascript:history.back()">{vtranslate('LBL_CANCEL', $MODULE)}</a>
        </div>
    </div>
</div>
 
  <button type="button" class="btn btn-primary btn-group" id="add_field_btn">Pridėti eilutę</button>
    {* <input id="add_field" type="hidden" name="add_field" value="1" disabled> *}
    {* <input id="delete_field" type="hidden" name="delete_field" disabled> *}

</form>


<div id="wait" style="display:none;position:absolute;top:25%;left:43%;padding:2px;"><img src="/resources/loading.gif"></div>
<div class="modal fade" id="selectCitiesModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pasirinkite kryptis</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
        <div id="wait2" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
         Į abi puses <input id="bothSides" type="checkbox" class="inputElement" title="Pažymėjus kryptys galios į abi puses" onclick="updateBothSide();"><br><i class="fa fa-arrow-left" style="color: grey;"></i><i class="fa fa-arrow-right" style="color: grey;"></i>
        <table class="table" id="table_begin">
          <thead>
            <tr>   
              <td>Tipas</td>          
              <td>Iš kur</td>
              <td>Į kur</td>
              <td width="10px;">P/K</td>
              <td width="10px;">Adr.</td>
              <td width="10px;"></td>
            </tr>
          </thead>
          <tbody class="modal-table">
            </tbody>
          </table>
          </div>
          <div class="modal-footer">
            <div style="display:inline-block; float: left;">
          <span style="display: flex;">P/K&nbsp; -&nbsp; Pašto kodai</span>
          <span style="display: flex;">Adr.&nbsp; -&nbsp; Konkretus adresas</span>
          <span style="display: flex;"><i class="fa fa-arrow-left" style="color: grey;"></i><i class="fa fa-arrow-right" style="color: grey;"></i>&nbsp;&nbsp; Galioja į abi puses</span>
      </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
            <button type="button" class="btn btn-primary" data-add-field="modal">Pridėti laukelį</button>
            <button type="button" class="btn btn-success" data-save="modal">Išsaugoti</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="addNewAddress" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true" style="top: 30px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5  id="ModalLabel">Naujų adresų sukūrimas</h5>
      </div>
      <div class="modal-body">
       <div id="wait3" style="display:none;position:absolute;top:50%;left:30%;padding:2px;"><img src="/resources/loading.gif"></div>
 <label>Pakrovimas</label>
 <input type="hidden" id="fromInput">
 <input type="hidden" id="toInput">
<table id="new_address">
<tr>
<td>
  <div class="form-group">
    <label>Įmonė</label>
    <input type="text" class="form-control" name="title"> 
		<div id="req-title" class="alert alert-danger hide">Įveskite įmonę</div>  	
  </div>
</td>
<td>
  <div class="form-group">
    <label>Miestas:</label>
    <input type="text" class="form-control" name="city">
		<div id="req-city" class="alert alert-danger hide">Įveskite miestą</div>  
  </div>
</td>
<td>
  <div class="form-group">
    <label>Adresas:</label>
    <input type="text" class="form-control" name="address">
		<div id="req-address" class="alert alert-danger hide">Įveskite adresą</div>  
  </div>
</td>

<td>
  <div class="form-group">
    <label>Pašto kodas:</label>
    <input type="text" class="form-control" name="code">
		<div id="req-code" class="alert alert-danger hide">Įveskite pašto kodą</div>  
  </div>
</td>

</tr>
</table>

 <label>Iškrovimas</label>
 <table id="new_address2">
<tr>
<td>
  <div class="form-group">
    <label>Įmonė</label>
    <input type="text" class="form-control" name="title2"> 
		<div id="req-title2" class="alert alert-danger hide">Įveskite įmonę</div>  
  </div>
</td>

<td>
  <div class="form-group">
    <label>Miestas:</label>
    <input type="text" class="form-control" name="city2">
		<div id="req-city2" class="alert alert-danger hide">Įveskite miestą</div>  
  </div>
</td>

<td>
  <div class="form-group">
    <label>Adresas:</label>
    <input type="text" class="form-control" name="address2">
		<div id="req-address2" class="alert alert-danger hide">Įveskite adresą</div>  
  </div>
</td>


<td>
  <div class="form-group">
    <label>Pašto kodas:</label>
    <input type="text" class="form-control" name="code2">
		<div id="req-code2" class="alert alert-danger hide">Įveskite pašto kodą</div>  
  </div>
</td>
</tr>
</table>
      </div>
      <div class="modal-footer">
				<button type="button" id="submit_new_address" class="btn btn-primary">Išsaugoti</button>
        <button type="button" class="btn btn-secondary dismiss" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>


{if $accountname}


  <div class="container block" id="client_block">    
      <h4 class="textOverflowEllipsis maxWidth50" style="display: inline;">{if $accountid neq 10965}Kliento{/if} {$accountname} {if $accountid neq 10965}kortelės{/if} zonos</h4> 
 {if $accountid neq 10965}      
  <form method="POST" style="display: inline;">
    <select class="inputElement" name='change_client' onchange='this.form.submit()' style="width: 200px; margin-top: 10px; margin-left: 3px;">
   {foreach from=$pricebookClients item=clients} 
      <option {if $accountnameGET eq $clients['accountname']}selected{/if} value="{$clients['accountname']}">{$clients['accountname']}</option>
    {/foreach}  
    </select>
  </form>
  {/if}
  <hr>

    <table class="table detailview-table no-border" id="zones_table">
      <tbody>
    {foreach from=$clientZones item=client_zones} 
      <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 1</span></td>

      <td class="fieldValue" >
        <span id="zone1" data-typecolumn="cf_1216"  data-column="cf_1218">{$client_zones['cf_1218']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(1);" class="editAction fa fa-pencil"></a></span>
      </td>      
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 2</span></td>

      <td class="fieldValue" >
        <span id="zone2" data-typecolumn="cf_1220"  data-column="cf_1222">{$client_zones['cf_1222']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(2);" class="editAction fa fa-pencil" id="pencil-2"></a></span>
      </td>
    </tr>

    <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 3</span></td>

      <td class="fieldValue" >
        <span id="zone3" data-typecolumn="cf_1224"  data-column="cf_1226">{$client_zones['cf_1226']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(3);" class="editAction fa fa-pencil" id="pencil-3"></a></span>
      </td>   
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 4</span></td>

      <td class="fieldValue" >
        <span id="zone4" data-typecolumn="cf_1228"  data-column="cf_1230">{$client_zones['cf_1230']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(4);" class="editAction fa fa-pencil" id="pencil-4"></a></span>
      </td>
    </tr>
      <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 5</span></td>

      <td class="fieldValue" >
        <span id="zone5" data-typecolumn="cf_1232" data-column="cf_1234">{$client_zones['cf_1234']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(5);" class="editAction fa fa-pencil" id="pencil-5"></a></span>
      </td>
    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 6</span></td>

      <td class="fieldValue" >
        <span id="zone6" data-typecolumn="cf_1236" data-column="cf_1238">{$client_zones['cf_1238']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(6);" class="editAction fa fa-pencil" id="pencil-6"></a></span>
      </td>
    </tr>

     <tr>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 7</span></td>

      <td class="fieldValue" >
        <span id="zone7" data-typecolumn="cf_1240" data-column="cf_1242">{$client_zones['cf_1242']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(7);" class="editAction fa fa-pencil" id="pencil-7"></a></span>
      </td>
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 8</span></td>

      <td class="fieldValue" >
        <span id="zone8" data-typecolumn="cf_1244" data-column="cf_1246">{$client_zones['cf_1246']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(8);" class="editAction fa fa-pencil" id="pencil-8"></a></span>
      </td>
    </tr>

     <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 9</span></td>

      <td class="fieldValue" >
        <span id="zone9" data-typecolumn="cf_1248" data-column="cf_1250">{$client_zones['cf_1250']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(9);" class="editAction fa fa-pencil" id="pencil-9"></a></span>
      </td>   
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 10</span></td>

      <td class="fieldValue" >
        <span id="zone10" data-typecolumn="cf_1252"  data-column="cf_1254">{$client_zones['cf_1254']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(10);" class="editAction fa fa-pencil" id="pencil-10"></a></span>
      </td>
    </tr>

     <tr>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 11</span></td>

      <td class="fieldValue" >
        <span id="zone11"  data-typecolumn="cf_1398" data-column="cf_1400">{$client_zones['cf_1400']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(11);" class="editAction fa fa-pencil" id="pencil-11"></a></span>
      </td>   
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 12</span></td>

      <td class="fieldValue" >
        <span id="zone12" data-typecolumn="cf_1402" data-column="cf_1404">{$client_zones['cf_1404']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(12);" class="editAction fa fa-pencil" id="pencil-12"></a></span>
      </td>
    </tr>

     <tr>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 13</span></td>

      <td class="fieldValue" >
        <span id="zone13" data-typecolumn="cf_1406"  data-column="cf_1408">{$client_zones['cf_1408']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(13);" class="editAction fa fa-pencil" id="pencil-13"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 14</span></td>

      <td class="fieldValue" >
        <span id="zone14" data-typecolumn="cf_1410"  data-column="cf_1412">{$client_zones['cf_1412']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(14);" class="editAction fa fa-pencil" id="pencil-14"></a></span>
      </td>
    </tr>

     <tr>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 15</span></td>

      <td class="fieldValue" >
        <span id="zone15" data-typecolumn="cf_1414"  data-column="cf_1416">{$client_zones['cf_1416']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(15);" class="editAction fa fa-pencil" id="pencil-15"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 16</span></td>

      <td class="fieldValue" >
        <span id="zone16" data-typecolumn="cf_1418"  data-column="cf_1420">{$client_zones['cf_1420']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(16);" class="editAction fa fa-pencil" id="pencil-16"></a></span>
      </td>
    </tr>

     <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 17</span></td>

      <td class="fieldValue" >
        <span id="zone17" data-typecolumn="cf_1422"  data-column="cf_1424">{$client_zones['cf_1424']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(17);" class="editAction fa fa-pencil" id="pencil-17"></a></span>
      </td>   
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 18</span></td>

      <td class="fieldValue" >
        <span id="zone18" data-typecolumn="cf_1426"  data-column="cf_1428">{$client_zones['cf_1428']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(18);" class="editAction fa fa-pencil" id="pencil-18"></a></span>
      </td>
    </tr>

     <tr>     
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 19</span></td>

      <td class="fieldValue" >
        <span id="zone19" data-typecolumn="cf_1430"  data-column="cf_1432">{$client_zones['cf_1432']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(19);" class="editAction fa fa-pencil" id="pencil-19"></a></span>
      </td>    
      <td class="fieldLabel textOverflowEllipsis"><span class="muted">Kainos Zona 20</span></td>

      <td class="fieldValue" >
        <span id="zone20" data-typecolumn="cf_1434"  data-column="cf_1436">{$client_zones['cf_1436']}</span>
      <span class="action pull-right"><a href="javascript:" onclick="locationsSelectors(20);" class="editAction fa fa-pencil" id="pencil-20"></a></span>
      </td>
    </tr>
  
  {/foreach}  

    </tbody>
    </table> 
     

      </div>
{/if}



<div style="height: 100px;"></div>



</div>

