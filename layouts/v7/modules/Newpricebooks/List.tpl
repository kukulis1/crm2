<link rel="stylesheet" href="layouts/v7/modules/Newpricebooks/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Newpricebooks/resources/main.js?u=3"></script>

{assign var=pricebooks value=$pricebooks}
<div id="table-content" class="table-container">
	<table id="listview-table" class="table listview-table">
				<thead>
					<tr class="listViewContentHeader">
<th>Pavadinimas</th>
<th>Aktyvus</th>
<th>Sukūrimo laikas</th>
<th>Pakeitimo laikas</th>
</tr>
</thead>
<tbody>
{$count = 1}      
  {foreach from=$pricebooks item=pricebook}  
  <tr onclick="action(event);" style="cursor: pointer;" data-orderid="{$pricebook['pricebookid']}">
      <input type="hidden" class="order_id" value="{$pricebook['pricebookid']}">
      <td style="padding: 10px; width: 20%;"  class="listViewEntryValue"><a href="index.php?module=Newpricebooks&view=PriceBook&record={$pricebook['pricebookid']}"> {$pricebook['bookname']}</a></td>     
      <td style="padding: 10px;" class="listViewEntryValue">{if $pricebook['active'] eq 1}Taip{else}Ne{/if}</td> 
      <td style="padding: 10px;" class="listViewEntryValue">{$pricebook['createdtime']}</td> 
      <td style="padding: 10px;" class="listViewEntryValue">{$pricebook['modifiedtime']}</td> 
  </tr>  
   {assign var=count value=$count+1}        
  {/foreach}   
</tbody>
</table>  
       {if $count <= 1}<div id="no_records" style="text-align: center;"><h5>Nieko nerasta</h5></div>{/if} 
</div>

