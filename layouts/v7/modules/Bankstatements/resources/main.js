$(document).ready(function(){

  if(document.getElementById('alert_message') != null){
    const name = $('#alert_message').data('import');
    removeAlert(5000,name);
  }
  
  $('[name="submit"]').on('click', function(){
    $('#wait').show();
    $('#table-content').css('opacity', '0.2');
  });

});

function removeAlert(sec,name){
  eraseCookie(name);
  setTimeout(() => {
    $('#alert_message').hide('slow');  
  }, sec);
}


function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=0'
}

// $('#monthPicker').MonthPicker({
//   Button: false
// });

function openModal(event){
  if($(event.target).data('invoiceid') != undefined){
    getInvoiceInfo('invoiceid',event);
  }else if($(event.target).data('purchaseorderid') != undefined){
    getInvoiceInfo('purchaseorderid',event);
  }else if($(event.target).data('overdue') != undefined){
    getOverdueInfo(event);
  }  
}

function getOverdueInfo(event){
  let html = '';
  const body = $('#ordersTbody');
  body.html('');
  // $('#total_debt').html('');
  let date = $(event.target).data('overdue');
  $('#wait9').show();
  $.post("modules/Bankstatements/ajax/getOverduePayments.php", {date:date},
  function (res, textStatus, jqXHR) {
     if(res.status == 'success'){   
       let total_sum_arr = new Array();   
      res.data.forEach(element => {
        html += `<tr>
                    <td><a target="_blank" href="index.php?module=PurchaseOrder&view=Detail&record=${element.purchaseorderid}">${element.purchaseorder_no}</a></td>  
                    <td>${element.total}</td>  
                    <td>${(element.vendorname == null ? '---' : element.vendorname)}</td>  
                    <td>${element.duedate}</td>  
                 </tr>`; 
                 total_sum_arr.push(element.total);   
      });
      let total_sum = total_sum_arr.map(Number).reduce(function(a,b){return a+b});
      console.log(total_sum);
      // $('#total_debt').html(`Viso: ${total_sum.toFixed(2)}`);
      body.html(html);
      $('#wait9').hide();
     }else{
       alert('Įvyko klaida');
     }
  },
  "JSON"
);

}

function getInvoiceInfo(type,event){
  let html = '';
  let id = $(event.target).data(type);
  const body = $('#ordersTbody');
  let module = (type == 'invoiceid' ? 'Invoice' : 'PurchaseOrder');
  $('#wait9').show();
  $.post("modules/Bankstatements/ajax/getInvoiceInfo.php", {id:id,module:module},
    function (res, textStatus, jqXHR) {
       if(res.status == 'success'){
        res.data.forEach(element => {
          html += `<tr>
                      <td><a target="_blank" href="index.php?module=${module}&view=Detail&record=${element.id}">${element.no}</a></td>  
                      <td>${element.total}</td>  
                      <td>${element.name}</td>  
                      <td>${element.duedate}</td>  
                   </tr>`;    
        });
        body.html(html);
        $('#wait9').hide();
       }else{
         alert('Įvyko klaida');
       }
    },
    "JSON"
  );

}