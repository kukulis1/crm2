{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<link rel="stylesheet" href="layouts/v7/modules/Bankstatements/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Bankstatements/resources/main.js"></script> 

<script>
$('#monthPicker').MonthPicker({
   Button: false
 });
</script>


<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="invoiceModalTitle" aria-hidden="true">
	<div id="wait9" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1050;"><img src="/resources/loading.gif"></div>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="invoiceModalLongTitle">Sąskaitos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
					<thead>
					<th>Saskaitos Nr.</th>
					<th>Suma</th>
					<th>Klientas</th>					
					<th>Mokėjimo data</th>
					</thead>
					<tbody id="ordersTbody"></tbody>
        </table>
        <span id="total_debt"></span>
      </div>      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
        {* <button type="button" class="btn btn-primary">Save changes</button> *}
      </div>
    </div>
  </div>
</div>

{if $REPORTS['role'] eq 'H2' OR $REPORTS['role'] eq 'H11' OR $REPORTS['role'] eq 'H13'}

  {if $REPORTS['filter']}<div>{$REPORTS['filter']}</div>{/if}

   <table id='table' style='margin-top:10px;' data-super-report="true">
    <thead>
      <tr class='listViewContentHeader'>      
        <th style="width:10%">Data</th>       
				{foreach item=value from=$REPORTS['period2']}           
         	<th>{$value}</th>
        {/foreach}      
    </tr>
    </thead>
		<tbody onclick="openModal(event);">   
      <tr>
        <td class='listViewEntryValue'>Bankas</td> 
        {foreach item=value from=$REPORTS['period']}         
          <td class='listViewEntryValue'>{$REPORTS['data']['turnover'][$value]}</td>    
        {/foreach}   
      <tr>   
      <tr>
        <td class='listViewEntryValue'>Skolos</td> 
      	{foreach item=value from=$REPORTS['period']}            
           <td class='listViewEntryValue'>{round($REPORTS['data']['total_debt'][$value],2)}</td>       
        {/foreach}   
      </tr> 
      <tr> 
        <td class='listViewEntryValue'>Praterminuotos skolos</td>       
       	{foreach item=value from=$REPORTS['period']}  
           <td class='listViewEntryValue'>{$REPORTS['data']['total_overdue_debt'][$value]}</td>    
        {/foreach}   
      </tr> 
      <tr>
        <td class='listViewEntryValue'>Įplaukos</td> 
        {foreach item=value from=$REPORTS['period']}            
           <td class='listViewEntryValue'>{$REPORTS['data']['revenue'][$value]}</td>      
        {/foreach}   
      <tr> 
       <tr>
        <td class='listViewEntryValue'>Įplaukų planas</td> 
        {foreach item=value from=$REPORTS['period']} 
           <td data-toggle="modal" data-target="#invoiceModal" class='listViewEntryValue' data-invoiceid="{$REPORTS['data']['revenue_plan']['invoiceid'][$value]}">{$REPORTS['data']['revenue_plan']['SUM'][$value]}</td>      
        {/foreach}   
      <tr> 
      <tr>
        <td class='listViewEntryValue'>Mokėjimai</td>   
        {foreach item=value from=$REPORTS['period']}            
           <td class='listViewEntryValue'>{$REPORTS['data']['payments'][$value]}</td>      
        {/foreach}  
      <tr> 
      <tr>
        <td class='listViewEntryValue'>Mokėjimų planas</td>   
        {foreach item=value from=$REPORTS['period']}            
           <td data-toggle="modal" data-target="#invoiceModal" class='listViewEntryValue' data-purchaseorderid="{$REPORTS['data']['payments_plan']['purchaseorderid'][$value]}">{$REPORTS['data']['payments_plan']['SUM'][$value]}</td>      
        {/foreach}  
      <tr> 
      <tr>
        <td class='listViewEntryValue'>Praterminuoti Parnaso mokėjimai</td>   
        {foreach item=value from=$REPORTS['period']}            
           <td data-toggle="modal" data-target="#invoiceModal" class='listViewEntryValue' data-overdue="{$value}" >{$REPORTS['data']['total_debt_payments'][$value]}</td>      
        {/foreach}  
      <tr>
    </tbody>
  </table>
{else}
	<span class="noDataMsg">{vtranslate('LBL_NO_RIGHTS', $MODULE_NAME)}</span>
{/if}
