{strip}

{include file="partials/RelatedListHeader.tpl"|vtemplate_path:$RELATED_MODULE_NAME}
{assign var=DOCUMENTS_LIST value=$DOCUMENTS_LIST}
{assign var=CHECKED_DOCUMENTS_LIST value=$CHECKED_DOCUMENTS_LIST}
{assign var=EMPLOYEID value=$EMPLOYEID}

		<div class="relatedContents col-lg-12 col-md-12 col-sm-12 table-container">
			<div class="bottomscroll-div">
				<table id="listview-table" class="table listview-table">
					<thead>
						<tr class="listViewHeaders">
              <th class="inline-search-btn" style="width:130px !important;"></th>
              <th class="nowrap">#</th>
              <th class="nowrap">Pavadinimas</th>
              <th class="nowrap">Pažymėjo</th>
              <th class="nowrap">Pažymėjimo data</th>
            </tr>  
	        </thead>
          <tbody>
          {foreach from=$DOCUMENTS_LIST item=LIST}    
            <tr class="listViewEntries">
              <td class="related-list-actions"></td>
              <td><input onclick="initCheckDokument({$LIST['documentarycheckid']},{$EMPLOYEID},event)" type="checkbox" class="inputElement" {if !empty($CHECKED_DOCUMENTS_LIST[$LIST['documentarycheckid']])}checked disabled{/if}></td>
              <td class="relatedListEntryValues"><span class="fieldValue"><a target="_blank" href="index.php?module=Documentarycheck&view=Detail&record={$LIST['documentarycheckid']}&mode=showDetailViewByMode&requestMode=full">{$LIST['document_name']}</a></span></td>              
              <td class="relatedListEntryValues"><span class="fieldValue ownername">{$CHECKED_DOCUMENTS_LIST[$LIST['documentarycheckid']]['owner']}</span></td>   
              <td class="relatedListEntryValues"><span class="fieldValue checkeddate">{$CHECKED_DOCUMENTS_LIST[$LIST['documentarycheckid']]['createddate']}</span></td>           
            </tr>
          {/foreach}
          </tbody>
        </table>
			</div>
		</div> 
{/strip}