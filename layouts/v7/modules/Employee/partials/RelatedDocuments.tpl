{strip}

{include file="partials/RelatedListHeader.tpl"|vtemplate_path:$RELATED_MODULE_NAME}
{assign var=EMPLOYEE_FILES value=$EMPLOYEE_FILES}

		<div class="relatedContents col-lg-12 col-md-12 col-sm-12 table-container">
			<div class="bottomscroll-div">
				<table id="listview-table" class="table listview-table">
					<thead>
						<tr class="listViewHeaders">
              <th class="inline-search-btn" style="width:130px !important;"></th>
              <th class="nowrap">Pavadinimas</th>
              <th class="nowrap">Sukūrė</th>
              <th class="nowrap">Sukūrimo laikas</th>
              <th class="nowrap">Veiksmas</th>
            </tr>  
	        </thead>
          <tbody>
          {foreach from=$EMPLOYEE_FILES item=FILES}    
            <tr class="listViewEntries">
              <td class="related-list-actions"></td>
              <td>
                <span class="fieldValue"><a target="_blank" href="index.php?module=Documentstorage&view=Detail&record={$FILES['documentstorageid']}&mode=showDetailViewByMode&requestMode=full">{$FILES['filename']}</a></span>
              </td>
              <td class="relatedListEntryValues"><span class="fieldValue">{$FILES['owner']}</span></td>
              <td class="relatedListEntryValues"><span class="fieldValue">{$FILES['createdtime']}</span></td>
              <td>
              <span class="fieldValue">
                <a style="margin-right: 10px;" href="index.php?module=Documentstorage&view=DownloadFile&file_id={$FILES['documentstorageid']}"><i title="Parsisiųsti failą" class="fa fa-download alignMiddle" style="font-size: 20px;"></i></a>
                <a class="value" href="javascript:void(0);"><i title="Ištrinti failą" class="fa fa-trash alignMiddle" style="font-size: 20px;" onclick="deleteFile(event,{$FILES['documentstorageid']})"></i></a>
              </span>
              </td>
            </tr>
          {/foreach}
          </tbody>
        </table>
			</div>
		</div> 
{/strip}