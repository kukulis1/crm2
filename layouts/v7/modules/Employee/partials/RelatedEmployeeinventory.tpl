{strip}

{include file="partials/RelatedListHeader.tpl"|vtemplate_path:$RELATED_MODULE_NAME}
{assign var=INVENTORY_LIST value=$INVENTORY_LIST}
{assign var=ASSIGNED_INVENTORY value=$GET_ASSIGNED_INVENTORY}
{assign var=EMPLOYEID value=$EMPLOYEID}

<style>
  .hiddenInputBlock {
    display: inline;
  }

  .hiddenQtyInput {
    width: 60px;
    margin: 0 5px;
  }
</style>

		<div class="relatedContents col-lg-12 col-md-12 col-sm-12 table-container">
			<div class="bottomscroll-div">
				<table id="listview-table" class="table listview-table">

          {* thead generuojamas dinamiskai pagal header zyma lauku administravime *}
        	<thead>
						<tr class="listViewHeaders">
              <th class="inline-search-btn" style="width:130px !important;"></th>
              <th class="nowrap">Daiktas</th>
              <th class="nowrap">Kodas</th>
              <th class="nowrap">Vnt.</th>
              <th class="nowrap">Išdavimo data</th>
              <th class="nowrap">Grąžinimo data</th>
              <th class="nowrap">Išdavęs asmuo</th>
              <th class="nowrap">Vertė</th>  
            </tr>  
	        </thead>
          <tbody>
          {foreach from=$INVENTORY_LIST item=LIST}    
            <tr class="listViewEntries">
              <td class="related-list-actions"></td>
              <td class="related-list-actions"><span class="fieldValue">{$LIST['employeeinventory_tks_item']}</span></td>              
              <td class="relatedListEntryValues"><span class="fieldValue">{$LIST['employeeinventory_tks_code']}</span></td>              
              <td class="relatedListEntryValues" style="width: 240px;">
                <span class="fieldValue">
                 <a href="#" onclick="openQtyPicker(this, {$EMPLOYEID}, {$LIST['employeeinventoryid']});" class="editAction fa fa-pencil" title="Pridėti kiekį"></a> 
                 <div class="hiddenInputBlock" style="display:none;">
                  <input class="hiddenQtyInput" type="text"> 
                  <button type="button" class="btn btn-success btn-sm btnOk" style="padding:1px 4px;">
                    <i class="fa fa-check"></i>
                  </button>
                  <button type="button" class="btn btn-danger btn-sm btnCancel" style="padding:1px 5px;">x</button>
                </div>
                 &nbsp;
                  <span class="qty">
                    {if $ASSIGNED_INVENTORY[$LIST['employeeinventoryid']]['qty']}
                      {$ASSIGNED_INVENTORY[$LIST['employeeinventoryid']]['qty']}                    
                    {/if}
                  </span>
                </span>
              </td>              
              <td class="relatedListEntryValues">
                <span class="fieldValue">
                <a href="#" onclick="openDatePicker(this, {$EMPLOYEID}, {$LIST['employeeinventoryid']}, 'date_of_issue');" class="editAction fa fa-calendar" style="font-size:16px;" title="Pridėti priskytimo data"></a> &nbsp;
                  <span class="date">
                    {if $ASSIGNED_INVENTORY[$LIST['employeeinventoryid']]['date_of_issue'] > 0}
                      {$ASSIGNED_INVENTORY[$LIST['employeeinventoryid']]['date_of_issue']}  &nbsp;
                      <i onclick="deleteInventory(this, {$EMPLOYEID}, {$LIST['employeeinventoryid']}, 'date_of_issue');" style="color: #ec6262; cursor: pointer;" title="Ištrinti" class="fa fa-times"></i>         
                    {/if}
                  </span>
                </span>
              </td>   
              <td class="relatedListEntryValues">
                <span class="fieldValue">
                 <a href="#" onclick="openDatePicker(this, {$EMPLOYEID}, {$LIST['employeeinventoryid']}, 'date_of_return');" class="editAction fa fa-calendar" style="font-size:16px;" title="Pridėti priskytimo data"></a> &nbsp;
                  <span class="date">
                    {if $ASSIGNED_INVENTORY[$LIST['employeeinventoryid']]['date_of_return'] > 0}
                      {$ASSIGNED_INVENTORY[$LIST['employeeinventoryid']]['date_of_return']}  &nbsp;
                      <i onclick="deleteInventory(this, {$EMPLOYEID}, {$LIST['employeeinventoryid']}, 'date_of_return');" style="color: #ec6262; cursor: pointer;" title="Ištrinti" class="fa fa-times"></i>         
                    {/if}
                   </span>
                </span>
              </td>           
              <td class="relatedListEntryValues"><span class="fieldValue owner">{$ASSIGNED_INVENTORY[$LIST['employeeinventoryid']]['owner']}</span></td>           
              <td class="relatedListEntryValues"><span class="fieldValue">{$LIST['value']}</span></td>           
            </tr>
          {/foreach}
          </tbody>
        </table>
			</div>
		</div> 
{/strip}