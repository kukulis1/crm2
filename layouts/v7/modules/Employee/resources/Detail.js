Vtiger_Detail_Js("Employee_Detail_Js",{},{

    registerAjaxPreSaveEvents: function (container) {
        var thisInstance = this;
        app.event.on(Vtiger_Detail_Js.PreAjaxSaveEvent, function (e) {
            if (!thisInstance.checkForPortalUser(container)) {
                e.preventDefault();
            }
        });
    },
    /**
     * Function to check for Portal User
     */
    checkForPortalUser: function (form) {
        var element = jQuery('[name="portal"]', form);
        var response = element.is(':checked');
        var primaryEmailField = jQuery('[name="email"]');
        var primaryEmailValue = primaryEmailField.val();
        if (response) {
            if (primaryEmailField.length == 0) {
                app.helper.showErrorNotification({message: app.vtranslate('JS_PRIMARY_EMAIL_FIELD_DOES_NOT_EXISTS')});
                return false;
            }
            if (primaryEmailValue == "") {
                app.helper.showErrorNotification({message: app.vtranslate('JS_PLEASE_ENTER_PRIMARY_EMAIL_VALUE_TO_ENABLE_PORTAL_USER')});
                return false;
            }
        }
        return true;
    },
    /**
     * Function which will register all the events
     */

    registerEvents: function () {
        var form = this.getForm();
        this._super();
        this.registerAjaxPreSaveEvents(form);
        var moduleName = app.getModuleName();
        jQuery(document).ready(function () {
            if(moduleName == 'Employee'){
                var firstnameTd = jQuery("#Employee_detailView_fieldValue_firstname");
                firstnameTd.find('.action').remove();
                var firstnameTd = jQuery("#Employee_detailView_fieldValue_lastname");
                firstnameTd.find('.action').remove();
                var emailTd = jQuery("#Employee_detailView_fieldValue_email");
                emailTd.find('.action').remove();
                jQuery("#Employee_detailView_fieldValue_candidate_id").hide();
                jQuery("#Employee_detailView_fieldLabel_candidate_id").hide();
            }
        });
    }
});


function deleteFile(event,id){
    if(confirm("Ar tikrai norite ištrinti failą?")){
      deleteFileProcess(event,id);
    }
  }
  
  function deleteFileProcess(event,recordid){
    $.ajax({
      type: "POST",
      url: "modules/Documentstorage/actions/removeFileFromList.php",
      data: {recordid:recordid},
      success: function (response) {
        if(response = 'success'){
          $(event.target).parent().parent().parent().parent().remove();
        }else{
          console.log(response);
        }
      }
    });
  }
  

  function initCheckDokument(recordid,employeid,event){
    if(confirm('Ar tikrai norite pažymėti?')){
      let userid = _USERMETA.id;
      $.ajax({
        type: "POST",
        url: "modules/Documentarycheck/ajax/checkDocumentToUser.php",
        data: {recordid:recordid,userid:userid,employeid:employeid},
        dataType: "JSON",
        success: function (response) {
          if(response.status == 'success'){
            $(event.target).attr('disabled','disabled');
            $(event.target).parent().parent().find('.ownername').html(response.name);
            $(event.target).parent().parent().find('.checkeddate').html(response.date);
          }else{
            alert('Ups! Įrašyti nepavyko, praneškite puslapio administratoriui');
          }
        }
      });
    }
  }

  function openDatePicker(event,employeid,employeeinventoryid,column){ 
    let userid = _USERMETA.id;   
    $(event).datepicker('show');
    $(event).datepicker({
    }).on('changeDate', function(selectedDate){
      $('.datepicker').hide();
      const offset = selectedDate.date.getTimezoneOffset();
      let date =  new Date(selectedDate.date - (offset*60*1000));
      let stringDate = date.toISOString().split('T')[0];
      setInventory(event,userid,employeid,stringDate,employeeinventoryid,column);
    });
  }

  function openQtyPicker(event,employeid,employeeinventoryid){
    $(event).parent().find('.hiddenInputBlock').show();
    let userid = _USERMETA.id;   
    $(event).parent().find('.btnOk').on('click', function(){
      let qty = $(event).parent().find('.hiddenQtyInput').val();         
      $(event).parent().find('.hiddenInputBlock').hide();
      setQty(event,userid,employeid,qty,employeeinventoryid);
    });
    
    $(event).parent().find('.btnCancel').on('click', function(){
      $(event).parent().find('.hiddenInputBlock').hide();
    });
  }

  function setQty(event,userid,employeid,qty,employeeinventoryid){
    $.ajax({
      type: "POST",
      url: "modules/Employeeinventory/ajax/assignQty.php",
      data: {userid:userid,employeid:employeid,qty:qty,employeeinventoryid:employeeinventoryid},
      dataType: "JSON",
      beforeSend: function(){
        app.helper.showProgress();
      },
      success: function (response) {        
        setTimeout(() => {
          app.helper.hideProgress();
          $(event).parent().find('.hiddenInputBlock').hide();
          if(response.status == 'success'){
            $(event).parent().find('.qty').html(qty);                 
          }else{
            alert('Įvyko klaida!');
          }
        }, 1000);
      }
    });
  }


  function setInventory(event,userid,employeid,stringDate,employeeinventoryid,column){
    $.ajax({
      type: "POST",
      url: "modules/Employeeinventory/ajax/assignInventory.php",
      data: {userid:userid,employeid:employeid,stringDate:stringDate,employeeinventoryid:employeeinventoryid,column:column},
      dataType: "JSON",
      beforeSend: function(){
        app.helper.showProgress();
      },
      success: function (response) {        
        setTimeout(() => {
          app.helper.hideProgress();
          if(response.status == 'success'){
            $(event).parent().find('.date').html(stringDate);
            $(event).parent().parent().parent().find('.owner').html(response.name);         
          }else{
            alert('Įvyko klaida!');
          }
        }, 1000);
      }
    });
  }

  function deleteInventory(event,employeid,employeeinventoryid,column){
    if(confirm('Ar tikrai norite panaikinti data?')){
      $.ajax({
        type: "POST",
        url: "modules/Employeeinventory/ajax/removeDate.php",
        data: {employeid:employeid,column:column,employeeinventoryid:employeeinventoryid},
        dataType: "JSON",
        beforeSend: function(){
          app.helper.showProgress();
        },
        success: function (response) {        
          setTimeout(() => {
            app.helper.hideProgress();
            if(response.status == 'success'){
              $(event).parent().parent().find('.date').html('');       
            }else{
              alert('Įvyko klaida!');
            }
          }, 1000);
        }
      }); 
    }
  }