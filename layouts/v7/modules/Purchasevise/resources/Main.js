
$(document).ready(function () {
  viseBtn();
  DebtsFieldsCheker();
});

$(document).ajaxComplete(function () {
  viseBtn();
  DebtsFieldsCheker();
});


function DebtsFieldsCheker() {
  $('.listViewEntriesCheckBox').click(function () {
    if ($(this).is(':checked')) {
      viseBtn();
    } else {
      viseBtn();
    }
  });
}


function checkAllBox(){
  if ($('.listViewEntriesMainCheckBox').is(':checked')) {
    setTimeout(() => {     
      viseBtn();
    }, 500);    
  }else{
    setTimeout(() => {     
      viseBtn();
    }, 100); 
  }
}

function viseBtn() {
  let userid = _USERMETA.id;

  let body = $('#listview-table tbody tr');
  let purchaseid = '';
  let check = false;

  for (let i = 0; i < body.length; i++) {
    check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
    if (check) {
      purchaseid += $('#listview-table tbody tr').eq(i).data('id') + ",";
    }
  }

  purchaseid = purchaseid.slice(0, -1); 

  if(purchaseid != ''){
    document.querySelector('#viseBtn').removeAttribute('disabled');
    setViseToPurchaseOrder(userid, purchaseid);
  }else{
    document.querySelector('#viseBtn').setAttribute('disabled','disabled');
  }
}


function setViseToPurchaseOrder(userid, purchaseid){
  $('#viseBtn').on('click', ()=>{
    purchases = purchaseid.split(',');
      if(purchases.length != ''){
        $.ajax({
          type: "POST",
          url: 'modules/Purchasevise/ajax/visePurchaseOrder.php',
          data: { userid: userid, purchaseid: purchaseid },
          beforeSend: function () {
            $('#wait2').show();
            $('#real_search').css('opacity', '0.2');
          },
          success: function (res) { 
            if (res == 'true') {
              setTimeout(() => {
                $('#wait2').hide();
                $('#real_search').css('opacity', '1');  
                purchases.forEach(inv => {
                  $(`[data-id="${inv}"]`).remove();
                });
              }, 2000);
            } else {
              $('#wait2').hide();
              $('#real_search').css('opacity', '1');  
              alert('Įvyko klaida, praneškite puslapio administratoriui.');
            }
          }      
        });
      }
  });


}
