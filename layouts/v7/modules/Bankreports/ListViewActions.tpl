{*<!--
/*+***********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************/
-->*}

{strip}
    {assign var=IBAN value=$BANKIBAN}
    {assign var=LISTVIEW_MASSACTIONS_1 value=array()}
    <div id="listview-actions" class="listview-actions-container">
        {foreach item=LIST_MASSACTION from=$LISTVIEW_MASSACTIONS name=massActions}
            {if $LIST_MASSACTION->getLabel() eq 'LBL_EDIT'}
                {assign var=editAction value=$LIST_MASSACTION}
            {else if $LIST_MASSACTION->getLabel() eq 'LBL_DELETE'}
                {assign var=deleteAction value=$LIST_MASSACTION}
            {else if $LIST_MASSACTION->getLabel() eq 'LBL_ADD_COMMENT'}
                {assign var=commentAction value=$LIST_MASSACTION}
            {else}
                {$a = array_push($LISTVIEW_MASSACTIONS_1, $LIST_MASSACTION)}
                {* $a is added as its print the index of the array, need to find a way around it *}
            {/if}
        {/foreach}
  
        <div class = "row">
            <div class=" col-md-9">
            <div class="btn-group listViewActionsContainer" style="display: inline-flex;">
                    
            </div>
            </div>
            <div class="col-md-3">
                {assign var=RECORD_COUNT value=$LISTVIEW_ENTRIES['count']}
                {assign var=TOTAL_RECORD_COUNT value=$LISTVIEW_ENTRIES['totalOrders']}
                {assign var=FROM_NUMBER value=$LISTVIEW_ENTRIES['fromNumber']}
                {assign var=TO_NUMBER value=$LISTVIEW_ENTRIES['toNumber']}
                {assign var=IS_PREV_PAGE_EXISTS value=$LISTVIEW_ENTRIES['isPrevPageExists']}
                {assign var=IS_NEXT_PAGE_EXISTS value=$LISTVIEW_ENTRIES['isNextPageExists']}
                {include file="Pagination.tpl"|vtemplate_path:$MODULE SHOWPAGEJUMP=true}
            </div>
        </div>	
     </div>     
     <div class="alert alert-danger hide" id="alert_message" style="margin-top: 10px; width: 50%;"></div>
{/strip}
