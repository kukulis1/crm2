{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Vtiger/views/List.php *}

{* START YOUR IMPLEMENTATION FROM BELOW. Use {debug} for information *}
		{include file="PicklistColorMap.tpl"|vtemplate_path:$MODULE}
		<div class="col-sm-12 col-xs-12 ">
			{if $MODULE neq 'EmailTemplates' && $SEARCH_MODE_RESULTS neq true}
			{assign var=LEFTPANELHIDE value=$CURRENT_USER_MODEL->get('leftpanelhide')}

			<div class="essentials-toggle" title="{vtranslate('LBL_LEFT_PANEL_SHOW_HIDE', 'Vtiger')}">
				<span
					class="essentials-toggle-marker fa {if $LEFTPANELHIDE eq '1'}fa-chevron-right{else}fa-chevron-left{/if} cursorPointer"></span>
			</div>
			{/if}

			{assign var=LISTVIEW_ENTRIES_COUNT value=$LISTVIEW_ENTRIES['totalOrders'] }
	
			{if !empty($PICKIST_DEPENDENCY_DATASOURCE)}
			<input type="hidden" name="picklistDependency"
				value='{Vtiger_Util_Helper::toSafeHTML($PICKIST_DEPENDENCY_DATASOURCE)}' />
			{/if}
			{if !$SEARCH_MODE_RESULTS}		
			{include file="ListViewActions.tpl"|vtemplate_path:$MODULE}
			{/if}


{$DEBIT_TRANSACTONS_SUM_ARR = array()}


	{foreach item=LISTVIEW_HEADER from=$LISTVIEW_HEADERS}		
		{foreach item=LISTVIEW_ENTRY from=$LISTVIEW_ENTRIES['listResult2'] name=listview}			
			{if $LISTVIEW_HEADER->get('name') eq 'bankreports_tks_bankamount'}				 
					{$DEBIT_TRANSACTONS_SUM_ARR[] = $LISTVIEW_ENTRY['bank_total']}	
			{/if}		 
		{/foreach}	
{/foreach}	


{$DEBIT_TRANSACTONS_SUM = array_sum($DEBIT_TRANSACTONS_SUM_ARR)}

<div class="row">
<div class="col-sm-9"></div>
<div class="col-sm-3"><span> Debetas: {$DEBIT_TRANSACTONS_SUM} €</span></div>
</div>



			
	
			<div id="table-content" class="table-container">
				<form name='list' id='listedit' action='' onsubmit="return false;">		
					<table id="listview-table"
						class="table {if $LISTVIEW_ENTRIES_COUNT eq '0'}listview-table-norecords {/if} listview-table ">
						<thead>
							<tr class="listViewContentHeader">
								<th>
									{* {if !$SEARCH_MODE_RESULTS}
									<div class="table-actions">
										<div class="dropdown" style="float:left;">
										<span class="input dropdown-toggle" data-toggle="dropdown"
												title="{vtranslate('LBL_CLICK_HERE_TO_SELECT_ALL_RECORDS',$MODULE)}">
												<input class="listViewEntriesMainCheckBox" type="checkbox" onclick="checkAllBox();">
											</span>
											<span class="input dropdown-toggle" data-toggle="dropdown"
												title="{vtranslate('LBL_CLICK_HERE_TO_SELECT_ALL_RECORDS',$MODULE)}">												
											</span>
										</div>
										{if $MODULE_MODEL->isFilterColumnEnabled()}
										<div id="listColumnFilterContainer">			
											<div	class="listColumnFilter {if $CURRENT_CV_MODEL and !($CURRENT_CV_MODEL->isCvEditable())}disabled{/if}"
												{if $CURRENT_CV_MODEL->isCvEditable()}
												title="{vtranslate('LBL_CLICK_HERE_TO_MANAGE_LIST_COLUMNS',$MODULE)}"
												{else}
												{if $CURRENT_CV_MODEL->get('viewname') eq 'All' and !$CURRENT_USER_MODEL->isAdminUser()}
												title="{vtranslate('LBL_SHARED_LIST_NON_ADMIN_MESSAGE',$MODULE)}"
												{elseif !$CURRENT_CV_MODEL->isMine()}
												{assign var=CURRENT_CV_USER_ID value=$CURRENT_CV_MODEL->get('userid')}
												{if !Vtiger_Functions::isUserExist($CURRENT_CV_USER_ID)}
												{assign var=CURRENT_CV_USER_ID value=Users::getActiveAdminId()}
												{/if}
												title="{vtranslate('LBL_SHARED_LIST_OWNER_MESSAGE',$MODULE,
												getUserFullName($CURRENT_CV_USER_ID))}"
												{/if}
												{/if}
												{if $MODULE eq 'Documents'}style="width: 10%;"{/if}
												data-toggle="tooltip" data-placement="bottom" data-container="body">
												<i class="fa fa-th-large"></i>
											</div>								
										</div>
										{/if}
									</div>
									{elseif $SEARCH_MODE_RESULTS}
									{vtranslate('LBL_ACTIONS',$MODULE)}
									{/if} *}
								</th>
					
								{foreach item=LISTVIEW_HEADER from=$LISTVIEW_HEADERS}
									{if $SEARCH_MODE_RESULTS || ($LISTVIEW_HEADER->getFieldDataType() eq 'multipicklist')}
									{assign var=NO_SORTING value=1}
									{else}
									{assign var=NO_SORTING value=0}
									{/if}
									<th {if $COLUMN_NAME eq $LISTVIEW_HEADER->get('name')} nowrap="nowrap" {/if}>
										<a href="#" class="noSorting"	data-columnname="{$LISTVIEW_HEADER->get('name')}" data-field-id='{$LISTVIEW_HEADER->getId()}'>
									
										&nbsp;{vtranslate($LISTVIEW_HEADER->get('label'), $LISTVIEW_HEADER->getModuleName())}&nbsp;
									</a>
									{if $COLUMN_NAME eq $LISTVIEW_HEADER->get('name')}
									<a href="#" class="removeSorting"><i class="fa fa-remove"></i></a>
									{/if}
								</th>
								{/foreach}
							</tr>

							{if $MODULE_MODEL->isQuickSearchEnabled() && !$SEARCH_MODE_RESULTS}
							<tr class="searchRow">
								<th class="inline-search-btn">
									<div class="table-actions">
										<button class="btn btn-success btn-sm"
											data-trigger="listSearch">{vtranslate("LBL_SEARCH",$MODULE)}</button>
									</div>
								</th>
							
								{foreach item=LISTVIEW_HEADER from=$LISTVIEW_HEADERS}
								<th>
									{assign var=FIELD_UI_TYPE_MODEL value=$LISTVIEW_HEADER->getUITypeModel()}
									{include file=vtemplate_path($FIELD_UI_TYPE_MODEL->getListSearchTemplateName(),$MODULE) FIELD_MODEL=
									$LISTVIEW_HEADER SEARCH_INFO=$SEARCH_DETAILS[$LISTVIEW_HEADER->getName()]
									USER_MODEL=$CURRENT_USER_MODEL}
									<input type="hidden" class="operatorValue"
										value="{$SEARCH_DETAILS[$LISTVIEW_HEADER->getName()]['comparator']}">
								</th>
								{/foreach}
							</tr>
							{/if}
						</thead>
						<tbody class="overflow-y">				

							{foreach item=LISTVIEW_ENTRY from=$LISTVIEW_ENTRIES['listResult'] name=listview}						
								
							<tr class="listViewEntries" data-id="{$LISTVIEW_ENTRY['purchaseorderid']}"> 															
								<td	class="listViewRecordActions">
								{* <div class="table-actions">
									<span class="input" >
										<input type="checkbox" value="{$LISTVIEW_ENTRY['purchaseorderid']}" class="listViewEntriesCheckBox"/>
									</span>
								</div> *}
								</td>
								{* start *}
								{foreach item=LISTVIEW_HEADER from=$LISTVIEW_HEADERS}

								{assign var=LISTVIEW_HEADERNAME value=$LISTVIEW_HEADER->get('name')}	
								
								<td class="listViewEntryValue" style="cursor:default;">	
										<span class="fieldValue">
									{if $LISTVIEW_HEADERNAME eq 'bankreports_tks_payer'}									
										<span class="value">{$LISTVIEW_ENTRY['payer']}</span>	

									{else if  $LISTVIEW_HEADERNAME eq 'cf_2159'}
											<span class="value">{$LISTVIEW_ENTRY['bankstatements_tks_bank']}</span>

									{else if  $LISTVIEW_HEADERNAME eq 'bankreports_tks_date'}
										<span class="value">{$LISTVIEW_ENTRY['date']}</span>

									{else if  $LISTVIEW_HEADERNAME eq 'bankreports_tks_purpose'}	
										<span class="value">{$LISTVIEW_ENTRY['purpose']}</span>										
									
									{else if  $LISTVIEW_HEADERNAME eq 'bankreports_tks_crmamount'}
										<span class="value">{$LISTVIEW_ENTRY['crm_total']}</span>

									{else if  $LISTVIEW_HEADERNAME eq 'bankreports_tks_bankamount'} 
										<span class="value">{$LISTVIEW_ENTRY['bank_total']}</span>

									{else if  $LISTVIEW_HEADERNAME eq 'bankreports_tks_difference'} 
										<span class="value">{$LISTVIEW_ENTRY['diff']}</span>	
									{/if}
										</span>						
								</td>					
								{/foreach}

								{* end *}
							</tr>
							{/foreach}




							{if $LISTVIEW_ENTRIES_COUNT eq '0'}
							<tr class="emptyRecordsDiv">
								{assign var=COLSPAN_WIDTH value={count($LISTVIEW_HEADERS)}+1}
								<td colspan="{$COLSPAN_WIDTH}">
									<div class="emptyRecordsContent">
										{assign var=SINGLE_MODULE value="SINGLE_$MODULE"}
										{vtranslate('LBL_RECORDS_IN_MODEL')} {vtranslate($MODULE, $MODULE)} {vtranslate('LBL_FOUND')}.
										
									</div>
								</td>
							</tr>
							{/if}
						</tbody>
					</table>
				</form>

			</div>
			<div id="scroller_wrapper" class="bottom-fixed-scroll">
				<div id="scroller" class="scroller-div"></div>
			</div>
		</div>