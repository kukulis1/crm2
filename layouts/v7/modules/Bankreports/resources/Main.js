$(document).ready(function () {
  let url = new URLSearchParams(window.location.search); 
  totalRecords();
  pageUp(url);
  nextPageButton(url);
  previousPageButton(url);
  exportButton()
});

$(document).ajaxComplete(function () {
  exportButton();
  pageUp(url);
  nextPageButton(url);
  previousPageButton(url);
  totalRecords();  
});

function exportButton(){
  $('#exportPaymentBtn').unbind().on('click', function () {
    exportPaymentBtn();
    showErrors('errors');
    removeExportedInvoices();
  });
}

function nextPageButton(url){
  $('#NextPageButton').unbind().on('click', function(){
    goToPage(+1,url);
  });
}

function previousPageButton(url){
  $('#PreviousPageButton').unbind().on('click', function(){
    goToPage(-1,url);
  });
}

function totalRecords(){
  $('.totalNumberOfRecords3').on('click', function () {
    $('.showTotalCountIcon3').addClass('hide');
  
    setTimeout(function () {
      $('#countShow').removeClass('hide');
    }, 1000);
  });
}

function pageUp(url){
  $('#PageJump').on('click', function(){
    if($(this).find('[aria-expanded="true"]')){
      $.ajax({
        type: "POST",
        url: "modules/Bankreports/ajax/calculatePages.php",
        data: {search:url.get('search_params')},
        dataType: 'JSON',
        success: function (res) {    
            $('.totalPageCount').html(res.orders);
        }
      }); 
    }    
  });
}

function goToPage(num,url){ 
  let module = url.get('module');
  let parent = (url.get('parent') != null ? url.get('parent') : '');
  let page = (url.get('page') != null ? parseInt(url.get('page'))+num: num+1);
  let view = url.get('view');
  let search_params = (url.get('search_params') != null ? url.get('search_params') :'');
  window.location.href = `index.php?module=${module}&parent=${parent}&page=${page}&view=${view}&search_params=${search_params}`; 
}
