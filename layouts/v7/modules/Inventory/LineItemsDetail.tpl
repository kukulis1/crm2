{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}
{* modules/Inventory/views/Detail.php *}

{assign var=ITEM_DETAILS_BLOCK value=$BLOCK_LIST['LBL_ITEM_DETAILS']}
{assign var=LINEITEM_FIELDS value=$ITEM_DETAILS_BLOCK->getFields()}

{assign var=COL_SPAN1 value=0}
{assign var=COL_SPAN2 value=0}
{assign var=COL_SPAN3 value=2}
{assign var=IMAGE_VIEWABLE value=false}
{assign var=PRODUCT_VIEWABLE value=false}
{assign var=QUANTITY_VIEWABLE value=false}
{assign var=PURCHASE_COST_VIEWABLE value=false}
{assign var=LIST_PRICE_VIEWABLE value=false}
{assign var=MARGIN_VIEWABLE value=false}
{assign var=COMMENT_VIEWABLE value=false}
{assign var=ITEM_DISCOUNT_AMOUNT_VIEWABLE value=false}
{assign var=ITEM_DISCOUNT_PERCENT_VIEWABLE value=false}
{assign var=SH_PERCENT_VIEWABLE value=false}
{assign var=DISCOUNT_AMOUNT_VIEWABLE value=false}
{assign var=DISCOUNT_PERCENT_VIEWABLE value=false}

{if $LINEITEM_FIELDS['image']}
    {assign var=IMAGE_VIEWABLE value=$LINEITEM_FIELDS['image']->isViewable()}
{if $IMAGE_VIEWABLE}{assign var=COL_SPAN1 value=($COL_SPAN1)+1}{/if}
{/if}
{if $LINEITEM_FIELDS['productid']}
    {assign var=PRODUCT_VIEWABLE value=$LINEITEM_FIELDS['productid']->isViewable()}
{if $PRODUCT_VIEWABLE}{assign var=COL_SPAN1 value=($COL_SPAN1)+1}{/if}
{/if}
{if $LINEITEM_FIELDS['quantity']}
    {assign var=QUANTITY_VIEWABLE value=$LINEITEM_FIELDS['quantity']->isViewable()}
{if $QUANTITY_VIEWABLE}{assign var=COL_SPAN1 value=($COL_SPAN1)+1}{/if}
{/if}
{if $LINEITEM_FIELDS['purchase_cost']}
    {assign var=PURCHASE_COST_VIEWABLE value=$LINEITEM_FIELDS['purchase_cost']->isViewable()}
{if $PURCHASE_COST_VIEWABLE}{assign var=COL_SPAN2 value=($COL_SPAN2)+1}{/if}
{/if}
{if $LINEITEM_FIELDS['listprice']}
    {assign var=LIST_PRICE_VIEWABLE value=$LINEITEM_FIELDS['listprice']->isViewable()}
{if $LIST_PRICE_VIEWABLE}{assign var=COL_SPAN2 value=($COL_SPAN2)+1}{/if}
{/if}
{if $LINEITEM_FIELDS['margin']}
    {assign var=MARGIN_VIEWABLE value=$LINEITEM_FIELDS['margin']->isViewable()}
{if $MARGIN_VIEWABLE}{assign var=COL_SPAN3 value=($COL_SPAN3)+1}{/if}
{/if}
{if $LINEITEM_FIELDS['comment']}
    {assign var=COMMENT_VIEWABLE value=$LINEITEM_FIELDS['comment']->isViewable()}
{/if}
{if $LINEITEM_FIELDS['discount_amount']}
    {assign var=ITEM_DISCOUNT_AMOUNT_VIEWABLE value=$LINEITEM_FIELDS['discount_amount']->isViewable()}
{/if}
{if $LINEITEM_FIELDS['discount_percent']}
    {assign var=ITEM_DISCOUNT_PERCENT_VIEWABLE value=$LINEITEM_FIELDS['discount_percent']->isViewable()}
{/if}
{if $LINEITEM_FIELDS['hdnS_H_Percent']}
    {assign var=SH_PERCENT_VIEWABLE value=$LINEITEM_FIELDS['hdnS_H_Percent']->isViewable()}
{/if}
{if $LINEITEM_FIELDS['hdnDiscountAmount']}
    {assign var=DISCOUNT_AMOUNT_VIEWABLE value=$LINEITEM_FIELDS['hdnDiscountAmount']->isViewable()}
{/if}
{if $LINEITEM_FIELDS['hdnDiscountPercent']}
    {assign var=DISCOUNT_PERCENT_VIEWABLE value=$LINEITEM_FIELDS['hdnDiscountPercent']->isViewable()}
{/if}

{if $INDEPENDENT}
	{assign var=independent value=$INDEPENDENT}
{/if}


{if $DEBT}
	{assign var=debt value=$DEBT}
{/if}


{if $PAID_SUM}
	{assign var=paid_sum value=$PAID_SUM}
{/if}

{if $TOTAL_WITH_VAT}
    {assign var=total_with_vat value=$TOTAL_WITH_VAT}
{/if}

{if $MODULE == 'Invoice' || $MODULE == 'PurchaseOrder'}
    {assign var=ANCHOR_SEQUENCE value=$anchor_sequence}
{/if}

<input type="hidden" class="isCustomFieldExists" value="false">

{assign var=FINAL_DETAILS value=$RELATED_PRODUCTS.1.final_details}

{if $MODULE eq 'SalesOrder' || $MODULE eq 'Invoice'}
    {assign var=MEASURE value=$TARE}
    {assign var=services value=$services}
    {assign var=hdnProductId value=$hdnProductId}
    {assign var=margin value=$margin}
{/if}



{if $MODULE neq 'SalesOrder'} 
<div class="details block">
    <div class="lineItemTableDiv">
    {if $independent eq '121391' || $MODULE eq 'PurchaseOrder'}
        <table class="table table-bordered lineItemsTable" style = "margin-top:15px" {if $MODULE eq 'Invoice'}data-invoice="unnecessary"{/if}>
            <thead>
            <th colspan="{$COL_SPAN1}" class="lineItemBlockHeader">
                {assign var=REGION_LABEL value=vtranslate('LBL_ITEM_DETAILS', $MODULE_NAME)}
                {if $RECORD->get('region_id') && $LINEITEM_FIELDS['region_id'] && $LINEITEM_FIELDS['region_id']->isViewable()}
                    {assign var=TAX_REGION_MODEL value=Inventory_TaxRegion_Model::getRegionModel($RECORD->get('region_id'))}
                    {if $TAX_REGION_MODEL}
                        {assign var=REGION_LABEL value="{vtranslate($LINEITEM_FIELDS['region_id']->get('label'), $MODULE_NAME)} : {$TAX_REGION_MODEL->getName()}"}
                    {/if}
                {/if}
                {$REGION_LABEL}
            </th>
            {if $MODULE eq 'PurchaseOrder'}
                <th colspan="{$COL_SPAN1}" class="lineItemBlockHeader"></th>
            {/if}
             {if $MODULE eq 'Invoice'}
              <th colspan="{$COL_SPAN1}" class="lineItemBlockHeader"></th>
             {/if}
            <th colspan="{$COL_SPAN2}" class="lineItemBlockHeader">
                {assign var=CURRENCY_INFO value=$RECORD->getCurrencyInfo()}
                {vtranslate('LBL_CURRENCY', $MODULE_NAME)} : {vtranslate($CURRENCY_INFO['currency_name'],$MODULE_NAME)}({$CURRENCY_INFO['currency_symbol']})  
            </th>
            <th colspan="{$COL_SPAN3}" class="lineItemBlockHeader">
                {vtranslate('LBL_TAX_MODE', $MODULE_NAME)} : {vtranslate($FINAL_DETAILS.taxtype, $MODULE_NAME)}
            </th>
            </thead>
            <tbody>
                <tr>
                    {if $IMAGE_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong>{vtranslate({$LINEITEM_FIELDS['image']->get('label')},$MODULE)}</strong>
                        </td>
                    {/if}

                   
                    {if $PRODUCT_VIEWABLE}
                        <td class="lineItemFieldName"> 
                        <!--Logtime-->    
                        {if $MODULE neq 'PurchaseOrder' && $independent neq '121391'} 
                           <strong>Užsakytas krovinys: SVORIS ILGISxPLOTISxAUKŠTIS</strong> 
                        {else}
                            <span class="redColor">*</span><strong>{vtranslate({$LINEITEM_FIELDS['productid']->get('label')},$MODULE_NAME)}</strong> 
                        {/if}                                 
                        </td>
                    {/if}

                    
                    {if $MODULE eq 'Invoice' && $independent neq '121391'}
                        <td class="lineItemFieldName">
                            <strong>Tara</strong>
                        </td>
                    {/if}

                    {if $MODULE eq 'PurchaseOrder' || $independent eq '121391'}
                        {if $MODULE eq 'PurchaseOrder' }
                            <td class="lineItemFieldName">
                                <strong>Data</strong>
                            </td>
                        {/if}
                        <td class="lineItemFieldName">
                            <strong>Kaštų centras</strong>
                        </td>
                    {/if}

                    {if $QUANTITY_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong>{vtranslate({$LINEITEM_FIELDS['quantity']->get('label')},$MODULE_NAME)}</strong>
                            {if $MODULE eq 'Invoice'}
                        	    <strong style="padding-left: 50px;">{vtranslate('LBL_MEASURE',$MODULE)}</strong>
                             {/if}
                        </td>
                    {/if}
                    {if $PURCHASE_COST_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong>{vtranslate({$LINEITEM_FIELDS['purchase_cost']->get('label')},$MODULE_NAME)}</strong>
                        </td>
                    {/if}    

                    {if $LIST_PRICE_VIEWABLE}
                        <td style="white-space: nowrap;">                        
                            <strong>{vtranslate({$LINEITEM_FIELDS['listprice']->get('label')},$MODULE_NAME)}</strong>                         
                        </td>
                    {/if}
        

                    <td class="lineItemFieldName">
                     {if $MODULE eq 'PurchaseOrder'}
                            <strong>Suma be PVM</strong>
                         {else}
                        <strong class="pull-right">{vtranslate('LBL_TOTAL',$MODULE_NAME)}</strong>
                        {/if}
                    </td>
                    {if $MARGIN_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong class="pull-right">{vtranslate({$LINEITEM_FIELDS['margin']->get('label')},$MODULE_NAME)}</strong>
                        </td>
                    {/if}
                    <td class="lineItemFieldName">
                        <strong class="pull-right">{vtranslate('LBL_NET_PRICE',$MODULE_NAME)}</strong>
                    </td>
                </tr>
        
                {foreach key=INDEX item=LINE_ITEM_DETAIL from=$RELATED_PRODUCTS}
                    <tr {if ($MODULE_NAME eq 'Invoice' || $MODULE_NAME eq 'PurchaseOrder') && in_array($LINE_ITEM_DETAIL["sequence_no{$INDEX}"], explode(',',$ANCHOR_SEQUENCE))} style="background: #ffff00;" {/if}>
                        {if $IMAGE_VIEWABLE}
                            <td style="text-align:center;">
                                <img src='{$LINE_ITEM_DETAIL["productImage$INDEX"]}' height="42" width="42">
                            </td>
                        {/if}               
                        

                        {if $PRODUCT_VIEWABLE}
                            <td>
                                <div>
                                    {if $LINE_ITEM_DETAIL["productDeleted$INDEX"]}
                                        {$LINE_ITEM_DETAIL["productName$INDEX"]}
                                    {else}
                                    {if $MODULE eq 'PurchaseOrder'}
                                     <h5><a class="fieldValue" href="index.php?module=Itemservice&view=Detail&record={$LINE_ITEM_DETAIL["itemserviceid$INDEX"]}" target="_blank">{$LINE_ITEM_DETAIL["purchase$INDEX"]}</a></h5>
                                     {{$LINE_ITEM_DETAIL["comment$INDEX"]}}
                                    {else}
                                        <h5><a class="fieldValue" href="index.php?module={$LINE_ITEM_DETAIL["entityType$INDEX"]}&view=Detail&record={$LINE_ITEM_DETAIL["hdnProductId$INDEX"]}" target="_blank">{$LINE_ITEM_DETAIL["productName$INDEX"]}</a></h5>
                                     {/if}   
                                        {/if}
                                </div>
                                {if $LINE_ITEM_DETAIL["productDeleted$INDEX"]}
                                    <div class="redColor deletedItem">
                                        {if empty($LINE_ITEM_DETAIL["productName$INDEX"])}
                                            {vtranslate('LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM',$MODULE)}
                                        {else}
                                            {vtranslate('LBL_THIS',$MODULE)} {$LINE_ITEM_DETAIL["entityType$INDEX"]} {vtranslate('LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM',$MODULE)}
                                        {/if}
                                    </div>
                                {/if}
                                <div>
                                    {$LINE_ITEM_DETAIL["subprod_names$INDEX"]}
                                </div>
                                {if $COMMENT_VIEWABLE && !empty($LINE_ITEM_DETAIL["productName$INDEX"])}
                                    <div>
                                        {decode_html($LINE_ITEM_DETAIL["comment$INDEX"])|nl2br}
                                    </div>
                                {/if}
                            </td>
                             {if $MODULE eq 'PurchaseOrder' || $independent eq '121391'}
                                {if $independent neq '121391'}
                                <td>{$LINE_ITEM_DETAIL["purchase_date$INDEX"]}</td>
                                {/if}
                             
                             <td style="width: 300px;">
                                <div>Kaštas: {$LINE_ITEM_DETAIL["costname$INDEX"]}</div>
                                <div>Darbuotojas: {$LINE_ITEM_DETAIL["employee$INDEX"]}</div>
                                <div>Objektas: {$LINE_ITEM_DETAIL["object$INDEX"]}</div>
                            </td>
                             {/if}
                        {/if}

                          {if $MODULE eq 'Invoice' && $independent neq '121391'}
                            <td>
                            {$LINE_ITEM_DETAIL["measure$INDEX"]}
                            {* {foreach item=cargo from=$MEASURE}
                                {if $LINE_ITEM_DETAIL["measure$INDEX"] eq $cargo['id'] || $LINE_ITEM_DETAIL["measure$INDEX"] eq $cargo['code']}                      
                                    {$cargo['code']}
                                {/if}
                             {/foreach} *}
                             </td>
                          {/if}

                        {if $QUANTITY_VIEWABLE}
                            <td>
                                <span>{$LINE_ITEM_DETAIL["qty$INDEX"]}<span>
                                {if $MODULE eq 'Invoice'}
                                 <span style="padding-left: 80px;">{$LINE_ITEM_DETAIL["cargo_measure_invoice$INDEX"]}</span>
                                 {/if}
                            </td>
                        {/if}

                        {if $PURCHASE_COST_VIEWABLE}
                            <td>
                                {$LINE_ITEM_DETAIL["purchaseCost$INDEX"]}
                            </td>
                        {/if}

                        {if $LIST_PRICE_VIEWABLE}
                            <td style="white-space: nowrap;">
                                <div>
                                    {$LINE_ITEM_DETAIL["listPrice$INDEX"]}
                                </div>
                                {if $MODULE_NAME eq 'SalesOrder' && $MODULE_NAME eq 'PurchaseOrder'}
                                {if $ITEM_DISCOUNT_AMOUNT_VIEWABLE || $ITEM_DISCOUNT_PERCENT_VIEWABLE}
                                    <div>
                                        {assign var=DISCOUNT_INFO value="{if $LINE_ITEM_DETAIL["discount_type$INDEX"] == 'amount'} {vtranslate('LBL_DIRECT_AMOUNT_DISCOUNT',$MODULE_NAME)} = {$LINE_ITEM_DETAIL["discountTotal$INDEX"]}
									{elseif $LINE_ITEM_DETAIL["discount_type$INDEX"] == 'percentage'} {$LINE_ITEM_DETAIL["discount_percent$INDEX"]} % {vtranslate('LBL_OF',$MODULE_NAME)} {$LINE_ITEM_DETAIL["productTotal$INDEX"]} = {$LINE_ITEM_DETAIL["discountTotal$INDEX"]}
									{/if}"}
                                        (-)&nbsp; <strong><a href="javascript:void(0)" class="individualDiscount inventoryLineItemDetails" tabindex="0" role="tooltip" id ="example" data-toggle="popover" data-trigger="focus" title="{vtranslate('LBL_DISCOUNT',$MODULE_NAME)}" data-content="{$DISCOUNT_INFO}">{vtranslate('LBL_DISCOUNT',$MODULE_NAME)}</a> : </strong>
                                    </div>
                                {/if}
                                <div>
                                    <strong>{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} :</strong>
                                </div>
                                {if $FINAL_DETAILS.taxtype neq 'group'}
                                    <div class="individualTaxContainer">
                                        {assign var=INDIVIDUAL_TAX_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT', $MODULE_NAME)} = {$LINE_ITEM_DETAIL["totalAfterDiscount$INDEX"]}<br /><br />{foreach item=tax_details from=$LINE_ITEM_DETAIL['taxes']}{if $LINEITEM_FIELDS["{$tax_details['taxname']}"]}{$tax_details['taxlabel']} : \t{$tax_details['percentage']}%  {vtranslate('LBL_OF',$MODULE_NAME)}  {if $tax_details['method'] eq 'Compound'}({/if}{$LINE_ITEM_DETAIL["totalAfterDiscount$INDEX"]}{if $tax_details['method'] eq 'Compound'}{foreach item=COMPOUND_TAX_ID from=$tax_details['compoundon']}{if $FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']} + {$FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']}{/if}{/foreach}){/if} = {$tax_details['amount']}<br />{/if}{/foreach}<br /><br />{vtranslate('LBL_TOTAL_TAX_AMOUNT',$MODULE_NAME)} = {$LINE_ITEM_DETAIL["taxTotal$INDEX"]}"}
                                        (+)&nbsp;<strong><a href="javascript:void(0)" class="individualTax inventoryLineItemDetails" tabindex="0" role="tooltip" id="example" title ="{vtranslate('LBL_TAX',$MODULE_NAME)}" data-trigger ="focus" data-toggle ="popover" data-content="{$INDIVIDUAL_TAX_INFO}">{vtranslate('LBL_TAX',$MODULE_NAME)} </a> : </strong>
                                    </div>
                                {/if}
                            </td>
                            {/if} 
                        {/if} 

                        <td>
                            <div align = "right">{$LINE_ITEM_DETAIL["productTotal$INDEX"]}</div>
                            {if $ITEM_DISCOUNT_AMOUNT_VIEWABLE || $ITEM_DISCOUNT_PERCENT_VIEWABLE}
                                <div align = "right">{$LINE_ITEM_DETAIL["discountTotal$INDEX"]}</div>           
                            {/if}
                            <div align = "right">{$LINE_ITEM_DETAIL["totalAfterDiscount$INDEX"]}</div>
                            {if $FINAL_DETAILS.taxtype neq 'group'}
                                <div align = "right">{$LINE_ITEM_DETAIL["taxTotal$INDEX"]}</div>
                            {/if}
                        </td>
                        {if $MARGIN_VIEWABLE}
                            <td>
                            <div align = "right">{$LINE_ITEM_DETAIL["margin$INDEX"]}</div>
                            </td>
							{/if}
                        <td>
                            <div align = "right">{$LINE_ITEM_DETAIL["netPrice$INDEX"]}</div>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
{/if}


{if $MODULE eq 'Invoice' && $independent neq '121391'}

	<table class="table table-bordered" id="lineItemTab">					
					<tr>					
					<th style="text-align:center;">Užsakymo Nr.</th>
					<th>Pakrovimas/Paslauga</th>
					<th>Iškrovimas</th>
					<th>Pastaba</th>
					<th>Kaštų centras</th>
					<th style="text-align:center;">Krovinys</th>
					<th style="text-align:center;">Svoris, kg.</th>
					<th style="text-align:center;">Tūris, m3</th>	
					<th>Kainų sarašas</th>	
					<th><span class="pull-right">Suma</span></th>	
					<th><span class="pull-right">Tarpinė kaina</span></th>	
					</tr>				
					<tr id="row0" class="hide lineItemCloneCopy" data-row-num="0">
						{include file="partials/InvoiceCargoTableDetail.tpl"|@vtemplate_path:'Inventory' row_no=0 data=[] IGNORE_UI_REGISTRATION=true}
					</tr> 
					{foreach key=row_no item=data from=$RELATED_PRODUCTS}
						<tr id="row{$row_no}" data-row-num="{$row_no}" class="lineItemRow" {if $ANCHOR_SEQUENCE eq $data["sequence_no{$row_no}"]} style="background: #ffff00;" {/if}>
							{include file="partials/InvoiceCargoTableDetail.tpl"|@vtemplate_path:'Inventory' row_no=$row_no data=$data}
						</tr>
					{/foreach}
					{if count($RELATED_PRODUCTS) eq 0}
						<tr id="row1" class="lineItemRow" data-row-num="1">
							{include file="partials/InvoiceCargoTableDetail.tpl"|@vtemplate_path:'Inventory' row_no=1 data=[] IGNORE_UI_REGISTRATION=false}
						</tr>
					{/if}					
					</table>
{/if}
    </div>
    
    <table class="table table-bordered lineItemsTable">
                       
        <tr>
            <td width="83%">
                <div class="pull-right">
                    <strong>{vtranslate('LBL_ITEMS_TOTAL',$MODULE_NAME)}</strong>
                </div>
            </td>
            <td>
                <span class="pull-right">
                    <strong>{$FINAL_DETAILS["hdnSubTotal"]}</strong>
                </span>
            </td>
        </tr>
    
        {* {if $DISCOUNT_AMOUNT_VIEWABLE || $DISCOUNT_PERCENT_VIEWABLE}
            <tr>
                <td width="83%">
                    <div align="right">
                        {assign var=FINAL_DISCOUNT_INFO value="{vtranslate('LBL_FINAL_DISCOUNT_AMOUNT',$MODULE_NAME)} = {if $DISCOUNT_PERCENT_VIEWABLE && $FINAL_DETAILS['discount_type_final'] == 'percentage'} {$FINAL_DETAILS['discount_percentage_final']}	% {vtranslate('LBL_OF',$MODULE_NAME)} {$FINAL_DETAILS['hdnSubTotal']} = {/if}{$FINAL_DETAILS['discountTotal_final']}"}
                        (-)&nbsp;<strong><a class="inventoryLineItemDetails" href="javascript:void(0)" id="finalDiscount" tabindex="0" role="tooltip" data-trigger ="focus" data-placement="left" data-toggle = "popover" title= "{vtranslate('LBL_OVERALL_DISCOUNT',$MODULE_NAME)}" data-content="{$FINAL_DISCOUNT_INFO}">{vtranslate('LBL_OVERALL_DISCOUNT',$MODULE_NAME)}</a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS['discountTotal_final']}
                    </div>

                </td>
            </tr>
        {/if} *}
        {if $SH_PERCENT_VIEWABLE}
            <tr>
                <td width="83%">
                    <div align="right">
                        {assign var=CHARGES_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['totalAfterDiscount']}<br /><br />{foreach key=CHARGE_ID item=CHARGE_INFO from=$SELECTED_CHARGES_AND_ITS_TAXES} {if $CHARGE_INFO['deleted']}({strtoupper(vtranslate('LBL_DELETED',$MODULE_NAME))}){/if} {$CHARGE_INFO['name']} {if $CHARGE_INFO['percent']}: {$CHARGE_INFO['percent']}% {vtranslate('LBL_OF',$MODULE_NAME)} {$FINAL_DETAILS['totalAfterDiscount']}{/if} = {$CHARGE_INFO['amount']}<br />{/foreach}<br /><h5>{vtranslate('LBL_CHARGES_TOTAL',$MODULE_NAME)} = {$FINAL_DETAILS['shipping_handling_charge']}</h5>"}
                        (+)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" href="javascript:void(0)" id="example" data-trigger="focus" data-placement ="left"  data-toggle="popover" title={vtranslate('LBL_CHARGES',$MODULE_NAME)} data-content="{$CHARGES_INFO}">{vtranslate('LBL_CHARGES',$MODULE_NAME)}</a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS["shipping_handling_charge"]}
                    </div>
                </td>
            </tr>
        {/if}
        <tr>
            <td width="83%">
                <div align="right">
                    <strong>{vtranslate('LBL_PRE_TAX_TOTAL', $MODULE_NAME)} </strong>
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS["preTaxTotal"]}
                </div>
            </td>
        </tr>

        {if $MODULE_NAME neq 'Invoice' and $MODULE_NAME neq 'PurchaseOrder'}
         {if $FINAL_DETAILS.taxtype eq 'group'}
            <tr>
                <td width="83%">
                    <div align="right">
                        {assign var=GROUP_TAX_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['totalAfterDiscount']}<br /><br />{foreach item=tax_details from=$FINAL_DETAILS['taxes']}{$tax_details['taxlabel']} : \t{$tax_details['percentage']}% {vtranslate('LBL_OF',$MODULE_NAME)} {if $tax_details['method'] eq 'Compound'}({/if}{$FINAL_DETAILS['totalAfterDiscount']}{if $tax_details['method'] eq 'Compound'}{foreach item=COMPOUND_TAX_ID from=$tax_details['compoundon']}{if $FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']} + {$FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']}{/if}{/foreach}){/if} = {$tax_details['amount']}<br />{/foreach}<br />{vtranslate('LBL_TOTAL_TAX_AMOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['tax_totalamount']}"}
                        (+)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" href="javascript:void(0)" id="finalTax" data-trigger ="focus" data-placement ="left" title = "{vtranslate('LBL_TAX',$MODULE_NAME)}" data-toggle ="popover" data-content="{$GROUP_TAX_INFO}">{vtranslate('LBL_TAX',$MODULE_NAME)}</a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS['tax_totalamount']}
                    </div>
                </td>
            </tr>
        {/if}
        {if $SH_PERCENT_VIEWABLE}
            <tr>
                <td width="83%">
                    <div align="right">
                        {assign var=CHARGES_TAX_INFO value="{vtranslate('LBL_CHARGES_TOTAL',$MODULE_NAME)} = {$FINAL_DETAILS["shipping_handling_charge"]}<br /><br />{foreach key=CHARGE_ID item=CHARGE_INFO from=$SELECTED_CHARGES_AND_ITS_TAXES}{if $CHARGE_INFO['taxes']}{if $CHARGE_INFO['deleted']}({strtoupper(vtranslate('LBL_DELETED',$MODULE_NAME))}){/if} {$CHARGE_INFO['name']}<br />{foreach item=CHARGE_TAX_INFO from=$CHARGE_INFO['taxes']}&emsp;{$CHARGE_TAX_INFO['name']}: &emsp;{$CHARGE_TAX_INFO['percent']}% {vtranslate('LBL_OF',$MODULE_NAME)} {if $CHARGE_TAX_INFO['method'] eq 'Compound'}({/if}{$CHARGE_INFO['amount']} {if $CHARGE_TAX_INFO['method'] eq 'Compound'}{foreach item=COMPOUND_TAX_ID from=$CHARGE_TAX_INFO['compoundon']}{if $CHARGE_INFO['taxes'][$COMPOUND_TAX_ID]['name']} + {$CHARGE_INFO['taxes'][$COMPOUND_TAX_ID]['name']}{/if}{/foreach}){/if} = {$CHARGE_TAX_INFO['amount']}<br />{/foreach}<br />{/if}{/foreach}\r\n{vtranslate('LBL_TOTAL_TAX_AMOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['shtax_totalamount']}"}
                        (+)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" title = "{vtranslate('LBL_TAXES_ON_CHARGES',$MODULE_NAME)}" data-trigger ="focus" data-placement ="left" data-toggle="popover"  href="javascript:void(0)" id="taxesOnChargesList" data-content="{$CHARGES_TAX_INFO}">
                                {vtranslate('LBL_TAXES_ON_CHARGES',$MODULE_NAME)} </a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS["shtax_totalamount"]}
                    </div>
                </td>
            </tr>
        {/if} 
         <tr>
            <td width="83%">
                <div align="right">
                    {assign var=DEDUCTED_TAXES_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} = {$FINAL_DETAILS["totalAfterDiscount"]}<br /><br />{foreach key=DEDUCTED_TAX_ID item=DEDUCTED_TAX_INFO from=$FINAL_DETAILS['deductTaxes']}{if $DEDUCTED_TAX_INFO['selected'] eq true}{$DEDUCTED_TAX_INFO['taxlabel']}: \t{$DEDUCTED_TAX_INFO['percentage']}%  = {$DEDUCTED_TAX_INFO['amount']}\r\n{/if}{/foreach}\r\n\r\n{vtranslate('LBL_DEDUCTED_TAXES_TOTAL',$MODULE_NAME)} = {$FINAL_DETAILS['deductTaxesTotalAmount']}"}
                    (-)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" href="javascript:void(0)" id="deductedTaxesList" data-trigger="focus" data-toggle="popover" title = "{vtranslate('LBL_DEDUCTED_TAXES',$MODULE_NAME)}" data-placement ="left" data-content="{$DEDUCTED_TAXES_INFO}">
                            {vtranslate('LBL_DEDUCTED_TAXES',$MODULE_NAME)} </a></strong>
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS['deductTaxesTotalAmount']}
                </div>
            </td>
        </tr> 

        {/if}
        <tr>
            <td width="83%">
                <div align="right">
                    <strong>{vtranslate('LBL_ADJUSTMENT',$MODULE_NAME)}</strong>
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS["adjustment"]}
                </div>
            </td>
        </tr>
        <tr>
            <td width="83%">
                <div align="right">
                    <strong>{vtranslate('LBL_GRAND_TOTAL',$MODULE_NAME)}</strong>
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS["grandTotal"]}
                </div>
            </td>
        </tr>
        {if $MODULE_NAME eq 'Invoice' or $MODULE_NAME eq 'PurchaseOrder'}
            <tr>
                <td width="83%">
                    {if $MODULE_NAME eq 'Invoice'}
                        <div align="right">
                            <strong>{vtranslate('LBL_RECEIVED',$MODULE_NAME)}</strong>
                        </div>
                    {else}
                        <div align="right">
                            <strong>{vtranslate('LBL_PAID',$MODULE_NAME)}</strong>
                        </div>
                    {/if}
                </td>
                <td>
                    {if $MODULE_NAME eq 'Invoice'}
                        <div align="right">
                            {* {if $RECORD->getDisplayValue('received')}
                                {$RECORD->getDisplayValue('received')}
                            {else}
                                0
                            {/if} *}

                            {if $paid_sum}
                               {$paid_sum}
                            {else}
                                0
                            {/if}                             
                        </div>
                    {else}
                        <div align="right">
                            {if $RECORD->getDisplayValue('paid')}
                                {$RECORD->getDisplayValue('paid')}
                            {else}
                                0
                            {/if}
                        </div>
                    {/if}
                </td>
            </tr>
            <tr>
                <td width="83%">
                    <div align="right">
                        <strong>{vtranslate('LBL_BALANCE',$MODULE_NAME)}</strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {* {if $RECORD->getDisplayValue('balance')}
                            {$RECORD->getDisplayValue('balance')}
                        {else}0
                        {/if} *}
                        {if $debt} {$debt} {else} {$total_with_vat} {/if}
                    </div>
                </td>
            </tr>
        {/if}
    </table>
</div>
{else}
<div class="details block">
    <div class="lineItemTableDiv">
        <table class="table table-bordered lineItemsTable" style = "margin-top:15px">
            <thead>
            <th style="display:none;" colspan="{$COL_SPAN1}" class="lineItemBlockHeader">
                {assign var=REGION_LABEL value=vtranslate('LBL_ITEM_DETAILS', $MODULE_NAME)}
                {if $RECORD->get('region_id') && $LINEITEM_FIELDS['region_id'] && $LINEITEM_FIELDS['region_id']->isViewable()}
                    {assign var=TAX_REGION_MODEL value=Inventory_TaxRegion_Model::getRegionModel($RECORD->get('region_id'))}
                    {if $TAX_REGION_MODEL}
                        {assign var=REGION_LABEL value="{vtranslate($LINEITEM_FIELDS['region_id']->get('label'), $MODULE_NAME)} : {$TAX_REGION_MODEL->getName()}"}
                    {/if}
                {/if}
                {$REGION_LABEL}
            </th>
            <th style="display:none;" colspan="{$COL_SPAN2}" class="lineItemBlockHeader">
                {assign var=CURRENCY_INFO value=$RECORD->getCurrencyInfo()}
                {vtranslate('LBL_CURRENCY', $MODULE_NAME)} : {vtranslate($CURRENCY_INFO['currency_name'],$MODULE_NAME)}({$CURRENCY_INFO['currency_symbol']})
            </th>
            <th style="display:none;" colspan="{$COL_SPAN3}" class="lineItemBlockHeader">
                {vtranslate('LBL_TAX_MODE', $MODULE_NAME)} : {vtranslate($FINAL_DETAILS.taxtype, $MODULE_NAME)}
            </th>
            </thead>
            <tbody>
                <tr>
                    {if $IMAGE_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong>{vtranslate({$LINEITEM_FIELDS['image']->get('label')},$MODULE)}</strong>
                        </td>
                    {/if}
                    {if $PRODUCT_VIEWABLE}
                        <td class="lineItemFieldName"> 
                        <!--Logtime-->    
                        {if $MODULE neq 'PurchaseOrder'} 
                            <strong>Užsakytas krovinys: SVORIS ILGISxPLOTISxAUKŠTIS</strong> 
                        {else}
                            <span class="redColor">*</span><strong>{vtranslate({$LINEITEM_FIELDS['productid']->get('label')},$MODULE_NAME)}</strong> 
                        {/if}                                 
                        </td>
                    {/if}
                    {* ITOMA *}
                       {if $MODULE eq 'SalesOrder'}                   
                         <td class="lineItemFieldName">
                            <strong>Tara/Kaina</strong>
                        </td>
                       {/if}

                    {if $QUANTITY_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong>{vtranslate({$LINEITEM_FIELDS['quantity']->get('label')},$MODULE_NAME)}</strong>
                        </td>
                    {/if}
{* 
                     {if $MODULE eq 'SalesOrder'} 
                       <td class="lineItemFieldName">
                            <strong>Suma</strong>
                        </td>     
                    {/if} *}
                    {if $PURCHASE_COST_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong>{vtranslate({$LINEITEM_FIELDS['purchase_cost']->get('label')},$MODULE_NAME)}</strong>
                        </td>
                    {/if}
                    {if $LIST_PRICE_VIEWABLE}
                        <td style="display:none; white-space: nowrap;">
                            <strong>{vtranslate({$LINEITEM_FIELDS['listprice']->get('label')},$MODULE_NAME)}</strong>
                        </td>
                    {/if}
                    <td style="display:none;" class="lineItemFieldName">
                        <strong class="pull-right">{vtranslate('LBL_TOTAL',$MODULE_NAME)}</strong>
                    </td>
                    {if $MARGIN_VIEWABLE}
                        <td class="lineItemFieldName">
                            <strong class="pull-right">{vtranslate({$LINEITEM_FIELDS['margin']->get('label')},$MODULE_NAME)}</strong>
                        </td>
                    {/if}
                    <td style="display:none;" class="lineItemFieldName">
                        <strong class="pull-right">{vtranslate('LBL_NET_PRICE',$MODULE_NAME)}</strong>
                    </td>
                </tr>
                {foreach key=INDEX item=LINE_ITEM_DETAIL from=$RELATED_PRODUCTS} 
                 {assign var="load_id" value=$LINE_ITEM_DETAIL["load_id$INDEX"] }
                    <tr>
                        {if $IMAGE_VIEWABLE}
                            <td style="text-align:center;">
                                <img src='{$LINE_ITEM_DETAIL["productImage$INDEX"]}' height="42" width="42">
                            </td>
                        {/if}

                        {if $PRODUCT_VIEWABLE}
                            <td>
                                <div>
                                    {if $LINE_ITEM_DETAIL["productDeleted$INDEX"]}
                                        {$LINE_ITEM_DETAIL["productName$INDEX"]}
                                    {else}
                                    {if {$LINE_ITEM_DETAIL["entityType$INDEX"]} eq 'Products'}
                                         {* {$packages = explode('|##|',$LINE_ITEM_DETAIL["packages$INDEX"][$load_id])} *}
                                        <table style="width: 100%;">
                                        <tr>
                                        <td>
                                        <h5 style="display:inline-flex"><a class="fieldValue" href="index.php?module={$LINE_ITEM_DETAIL["entityType$INDEX"]}&view=Detail&record={$LINE_ITEM_DETAIL["hdnProductId$INDEX"]}" target="_blank">{$LINE_ITEM_DETAIL["productName$INDEX"]}</a>&nbsp;&nbsp; 
                                         {if !empty($PACKAGES[$load_id])}
                                         <i class="fa fa-plus" style="cursor:pointer" title="Rodyti paketus" onclick="showPackages(event)"></i>
                                         {/if}

                                         <i class="fa fa-exclamation-circle {if !$LINE_ITEM_DETAIL["meters$INDEX"]}hide{/if}" title="Kaina skaičiuojama pagal metrus" style="font-size: 20px;color:#fc631a;"></i></h5> 
                                            <ul class="packages hide" style="padding-left: 0px;">
                                                {foreach from=$PACKAGES[$load_id] item=pack}
                                                    <li style="list-style:none;">{$pack}</li>
                                                {/foreach}
                                            </ul>
                                            </td>
                                            <td>
                                          <div style="display: inline-flex; float:right;">
                                            <div>
                                            <div>{if $LINE_ITEM_DETAIL["flags$INDEX"][$load_id]["minifest"]}<i style="color: #8cce45;" class="fa fa-check"></i>{else}<i style="color: #ec6262; cursor: default;" class="fa fa-times"></i>{/if} Krovinio važtaraštis</div>
                                            <div>{if $LINE_ITEM_DETAIL["flags$INDEX"][$load_id]["cmr"]}<i style="color: #8cce45;" class="fa fa-check"></i>{else}<i style="color: #ec6262; cursor: default;" class="fa fa-times"></i>{/if} CMR</div>
                                            <div>{if $LINE_ITEM_DETAIL["flags$INDEX"][$load_id]["invoice"]}<i style="color: #8cce45;" class="fa fa-check"></i>{else}<i style="color: #ec6262; cursor: default;" class="fa fa-times"></i>{/if} Sąskaita</div>
                                            </div>
                                        </div>
                                        </td>
                                        </tr>
                                        </table>
                                    {else}                               
                                     <h5><a class="fieldValue" href="index.php?module={$LINE_ITEM_DETAIL["entityType$INDEX"]}&view=Detail&record={$LINE_ITEM_DETAIL["hdnProductId$INDEX"]}" target="_blank">{$LINE_ITEM_DETAIL["service$INDEX"]}</a></h5>            
                                   
                                    {/if}   
                                        {/if}
                                </div>
                                {if $LINE_ITEM_DETAIL["productDeleted$INDEX"]}
                                    <div class="redColor deletedItem">
                                        {if empty($LINE_ITEM_DETAIL["productName$INDEX"])}
                                            {vtranslate('LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM',$MODULE)}
                                        {else}
                                            {vtranslate('LBL_THIS',$MODULE)} {$LINE_ITEM_DETAIL["entityType$INDEX"]} {vtranslate('LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM',$MODULE)}
                                        {/if}
                                    </div>
                                {/if}
                                <div>
                                    {$LINE_ITEM_DETAIL["subprod_names$INDEX"]}
                                </div>
                                {if $COMMENT_VIEWABLE && !empty($LINE_ITEM_DETAIL["productName$INDEX"])}
                                    <div>
                                        {decode_html($LINE_ITEM_DETAIL["comment$INDEX"])|nl2br}
                                    </div>
                                {/if}
                            </td>
                        {/if}
                        {* ITOMA *}
                            {if $MODULE eq 'SalesOrder'}                 
                            <td>
                            {if $LINE_ITEM_DETAIL["hdnProductId$INDEX"] neq '36641'}
                             {$LINE_ITEM_DETAIL["measure$INDEX"]}  
                             {else}
                                  {$LINE_ITEM_DETAIL["margin$INDEX"]}                          
                            {/if}
                            </td>
                          {/if}

                        {if $QUANTITY_VIEWABLE}
                            <td>
                                {$LINE_ITEM_DETAIL["qty$INDEX"]}
                            </td>
                        {/if}

                        {* {if $MODULE eq 'SalesOrder'} 
                            <td>
                                {$LINE_ITEM_DETAIL["margin$INDEX"]}
                            </td>
                        {/if} *}
                        {if $PURCHASE_COST_VIEWABLE}
                            <td>
                                {$LINE_ITEM_DETAIL["purchaseCost$INDEX"]}
                            </td>
                        {/if}

                        {if $LIST_PRICE_VIEWABLE}
                            <td style="display:none; white-space: nowrap;">
                                <div>
                                    {$LINE_ITEM_DETAIL["listPrice$INDEX"]}
                                </div>
                                {if $ITEM_DISCOUNT_AMOUNT_VIEWABLE || $ITEM_DISCOUNT_PERCENT_VIEWABLE}
                                    <div>
                                        {assign var=DISCOUNT_INFO value="{if $LINE_ITEM_DETAIL["discount_type$INDEX"] == 'amount'} {vtranslate('LBL_DIRECT_AMOUNT_DISCOUNT',$MODULE_NAME)} = {$LINE_ITEM_DETAIL["discountTotal$INDEX"]}
									{elseif $LINE_ITEM_DETAIL["discount_type$INDEX"] == 'percentage'} {$LINE_ITEM_DETAIL["discount_percent$INDEX"]} % {vtranslate('LBL_OF',$MODULE_NAME)} {$LINE_ITEM_DETAIL["productTotal$INDEX"]} = {$LINE_ITEM_DETAIL["discountTotal$INDEX"]}
									{/if}"}
                                        (-)&nbsp; <strong><a href="javascript:void(0)" class="individualDiscount inventoryLineItemDetails" tabindex="0" role="tooltip" id ="example" data-toggle="popover" data-trigger="focus" title="{vtranslate('LBL_DISCOUNT',$MODULE_NAME)}" data-content="{$DISCOUNT_INFO}">{vtranslate('LBL_DISCOUNT',$MODULE_NAME)}</a> : </strong>
                                    </div>
                                {/if}
                                <div>
                                    <strong>{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} :</strong>
                                </div>
                                {if $FINAL_DETAILS.taxtype neq 'group'}
                                    <div class="individualTaxContainer">
                                        {assign var=INDIVIDUAL_TAX_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT', $MODULE_NAME)} = {$LINE_ITEM_DETAIL["totalAfterDiscount$INDEX"]}<br /><br />{foreach item=tax_details from=$LINE_ITEM_DETAIL['taxes']}{if $LINEITEM_FIELDS["{$tax_details['taxname']}"]}{$tax_details['taxlabel']} : \t{$tax_details['percentage']}%  {vtranslate('LBL_OF',$MODULE_NAME)}  {if $tax_details['method'] eq 'Compound'}({/if}{$LINE_ITEM_DETAIL["totalAfterDiscount$INDEX"]}{if $tax_details['method'] eq 'Compound'}{foreach item=COMPOUND_TAX_ID from=$tax_details['compoundon']}{if $FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']} + {$FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']}{/if}{/foreach}){/if} = {$tax_details['amount']}<br />{/if}{/foreach}<br /><br />{vtranslate('LBL_TOTAL_TAX_AMOUNT',$MODULE_NAME)} = {$LINE_ITEM_DETAIL["taxTotal$INDEX"]}"}
                                        (+)&nbsp;<strong><a href="javascript:void(0)" class="individualTax inventoryLineItemDetails" tabindex="0" role="tooltip" id="example" title ="{vtranslate('LBL_TAX',$MODULE_NAME)}" data-trigger ="focus" data-toggle ="popover" data-content="{$INDIVIDUAL_TAX_INFO}">{vtranslate('LBL_TAX',$MODULE_NAME)} </a> : </strong>
                                    </div>
                                {/if}
                            </td>
                        {/if}

                        <td style="display:none;">
                            <div align = "right">{$LINE_ITEM_DETAIL["productTotal$INDEX"]}</div>
                            {if $ITEM_DISCOUNT_AMOUNT_VIEWABLE || $ITEM_DISCOUNT_PERCENT_VIEWABLE}
                                <div align = "right">{$LINE_ITEM_DETAIL["discountTotal$INDEX"]}</div>           
                            {/if}
                            <div align = "right">{$LINE_ITEM_DETAIL["totalAfterDiscount$INDEX"]}</div>
                            {if $FINAL_DETAILS.taxtype neq 'group'}
                                <div align = "right">{$LINE_ITEM_DETAIL["taxTotal$INDEX"]}</div>
                            {/if}
                        </td>
                        {if $MARGIN_VIEWABLE}
                            <td><div align = "right">{$LINE_ITEM_DETAIL["margin$INDEX"]}</div></td>
							{/if}
                        <td style="display:none;">
                            <div align = "right">{$LINE_ITEM_DETAIL["netPrice$INDEX"]}</div>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
    <table class="table table-bordered lineItemsTable">
         <tr>
            <td style="display:none;" width="83%">
                <div class="pull-right">
                    <strong>{vtranslate('LBL_ITEMS_TOTAL',$MODULE_NAME)}</strong>
                </div>
            </td>
            <td style="display:none;">
                <span class="pull-right">
                    <strong>{$FINAL_DETAILS["hdnSubTotal"]}</strong>
                </span>
            </td>
        </tr>
        {if $DISCOUNT_AMOUNT_VIEWABLE || $DISCOUNT_PERCENT_VIEWABLE}
            <tr style="display:none;">
                <td width="83%">
                    <div align="right">
                        {assign var=FINAL_DISCOUNT_INFO value="{vtranslate('LBL_FINAL_DISCOUNT_AMOUNT',$MODULE_NAME)} = {if $DISCOUNT_PERCENT_VIEWABLE && $FINAL_DETAILS['discount_type_final'] == 'percentage'} {$FINAL_DETAILS['discount_percentage_final']}	% {vtranslate('LBL_OF',$MODULE_NAME)} {$FINAL_DETAILS['hdnSubTotal']} = {/if}{$FINAL_DETAILS['discountTotal_final']}"}
                        (-)&nbsp;<strong><a class="inventoryLineItemDetails" href="javascript:void(0)" id="finalDiscount" tabindex="0" role="tooltip" data-trigger ="focus" data-placement="left" data-toggle = "popover" title= "{vtranslate('LBL_OVERALL_DISCOUNT',$MODULE_NAME)}" data-content="{$FINAL_DISCOUNT_INFO}">{vtranslate('LBL_OVERALL_DISCOUNT',$MODULE_NAME)}</a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS['discountTotal_final']}
                    </div>

                </td>
            </tr>
        {/if}
        {if $SH_PERCENT_VIEWABLE}
            <tr style="display:none;">
                <td width="83%">
                    <div align="right">
                        {assign var=CHARGES_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['totalAfterDiscount']}<br /><br />{foreach key=CHARGE_ID item=CHARGE_INFO from=$SELECTED_CHARGES_AND_ITS_TAXES} {if $CHARGE_INFO['deleted']}({strtoupper(vtranslate('LBL_DELETED',$MODULE_NAME))}){/if} {$CHARGE_INFO['name']} {if $CHARGE_INFO['percent']}: {$CHARGE_INFO['percent']}% {vtranslate('LBL_OF',$MODULE_NAME)} {$FINAL_DETAILS['totalAfterDiscount']}{/if} = {$CHARGE_INFO['amount']}<br />{/foreach}<br /><h5>{vtranslate('LBL_CHARGES_TOTAL',$MODULE_NAME)} = {$FINAL_DETAILS['shipping_handling_charge']}</h5>"}
                        (+)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" href="javascript:void(0)" id="example" data-trigger="focus" data-placement ="left"  data-toggle="popover" title={vtranslate('LBL_CHARGES',$MODULE_NAME)} data-content="{$CHARGES_INFO}">{vtranslate('LBL_CHARGES',$MODULE_NAME)}</a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS["shipping_handling_charge"]}
                    </div>
                </td>
            </tr>
        {/if}
        <tr style="display:none;">
            <td width="83%">
                <div align="right">               
                    <strong>{vtranslate('LBL_PRE_TAX_TOTAL', $MODULE_NAME)} </strong>               
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS["preTaxTotal"]}
                </div>
            </td>
        </tr>
        {if $FINAL_DETAILS.taxtype eq 'group'}
            <tr style="display:none;">
                <td width="83%">
                    <div align="right">
                        {assign var=GROUP_TAX_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['totalAfterDiscount']}<br /><br />{foreach item=tax_details from=$FINAL_DETAILS['taxes']}{$tax_details['taxlabel']} : \t{$tax_details['percentage']}% {vtranslate('LBL_OF',$MODULE_NAME)} {if $tax_details['method'] eq 'Compound'}({/if}{$FINAL_DETAILS['totalAfterDiscount']}{if $tax_details['method'] eq 'Compound'}{foreach item=COMPOUND_TAX_ID from=$tax_details['compoundon']}{if $FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']} + {$FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']}{/if}{/foreach}){/if} = {$tax_details['amount']}<br />{/foreach}<br />{vtranslate('LBL_TOTAL_TAX_AMOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['tax_totalamount']}"}
                        (+)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" href="javascript:void(0)" id="finalTax" data-trigger ="focus" data-placement ="left" title = "{vtranslate('LBL_TAX',$MODULE_NAME)}" data-toggle ="popover" data-content="{$GROUP_TAX_INFO}">{vtranslate('LBL_TAX',$MODULE_NAME)}</a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS['tax_totalamount']}
                    </div>
                </td>
            </tr>
        {/if}
        {if $SH_PERCENT_VIEWABLE}
            <tr style="display:none;">
                <td width="83%">
                    <div align="right">
                        {assign var=CHARGES_TAX_INFO value="{vtranslate('LBL_CHARGES_TOTAL',$MODULE_NAME)} = {$FINAL_DETAILS["shipping_handling_charge"]}<br /><br />{foreach key=CHARGE_ID item=CHARGE_INFO from=$SELECTED_CHARGES_AND_ITS_TAXES}{if $CHARGE_INFO['taxes']}{if $CHARGE_INFO['deleted']}({strtoupper(vtranslate('LBL_DELETED',$MODULE_NAME))}){/if} {$CHARGE_INFO['name']}<br />{foreach item=CHARGE_TAX_INFO from=$CHARGE_INFO['taxes']}&emsp;{$CHARGE_TAX_INFO['name']}: &emsp;{$CHARGE_TAX_INFO['percent']}% {vtranslate('LBL_OF',$MODULE_NAME)} {if $CHARGE_TAX_INFO['method'] eq 'Compound'}({/if}{$CHARGE_INFO['amount']} {if $CHARGE_TAX_INFO['method'] eq 'Compound'}{foreach item=COMPOUND_TAX_ID from=$CHARGE_TAX_INFO['compoundon']}{if $CHARGE_INFO['taxes'][$COMPOUND_TAX_ID]['name']} + {$CHARGE_INFO['taxes'][$COMPOUND_TAX_ID]['name']}{/if}{/foreach}){/if} = {$CHARGE_TAX_INFO['amount']}<br />{/foreach}<br />{/if}{/foreach}\r\n{vtranslate('LBL_TOTAL_TAX_AMOUNT',$MODULE_NAME)} = {$FINAL_DETAILS['shtax_totalamount']}"}
                        (+)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" title = "{vtranslate('LBL_TAXES_ON_CHARGES',$MODULE_NAME)}" data-trigger ="focus" data-placement ="left" data-toggle="popover"  href="javascript:void(0)" id="taxesOnChargesList" data-content="{$CHARGES_TAX_INFO}">
                                {vtranslate('LBL_TAXES_ON_CHARGES',$MODULE_NAME)} </a></strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {$FINAL_DETAILS["shtax_totalamount"]}
                    </div>
                </td>
            </tr>
        {/if}
        <tr style="display:none;">
            <td width="83%">
                <div align="right">
                    {assign var=DEDUCTED_TAXES_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} = {$FINAL_DETAILS["totalAfterDiscount"]}<br /><br />{foreach key=DEDUCTED_TAX_ID item=DEDUCTED_TAX_INFO from=$FINAL_DETAILS['deductTaxes']}{if $DEDUCTED_TAX_INFO['selected'] eq true}{$DEDUCTED_TAX_INFO['taxlabel']}: \t{$DEDUCTED_TAX_INFO['percentage']}%  = {$DEDUCTED_TAX_INFO['amount']}\r\n{/if}{/foreach}\r\n\r\n{vtranslate('LBL_DEDUCTED_TAXES_TOTAL',$MODULE_NAME)} = {$FINAL_DETAILS['deductTaxesTotalAmount']}"}
                    (-)&nbsp;<strong><a class="inventoryLineItemDetails" tabindex="0" role="tooltip" href="javascript:void(0)" id="deductedTaxesList" data-trigger="focus" data-toggle="popover" title = "{vtranslate('LBL_DEDUCTED_TAXES',$MODULE_NAME)}" data-placement ="left" data-content="{$DEDUCTED_TAXES_INFO}">
                            {vtranslate('LBL_DEDUCTED_TAXES',$MODULE_NAME)} </a></strong>
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS['deductTaxesTotalAmount']}
                </div>
            </td>
        </tr>
        <tr style="display:none;">
            <td width="83%">
                <div align="right">
                    <strong>{vtranslate('LBL_ADJUSTMENT',$MODULE_NAME)}</strong>
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS["adjustment"]}
                </div>
            </td>
        </tr>
        <tr>
            <td width="83%">
                <div align="right">
                 {if $MODULE_NAME eq 'Invoice'} 
                  <strong>{vtranslate('LBL_GRAND_TOTAL_INVOICE', $MODULE_NAME)}</strong>
                  {elseif $MODULE_NAME eq 'SalesOrder'}  
                    <strong>{vtranslate('LBL_PAY_GRAND_TOTAL',$MODULE_NAME)}</strong>
                {else}
                  <strong>{vtranslate('LBL_GRAND_TOTAL',$MODULE_NAME)}</strong>
                {/if}
                </div>
            </td>
            <td>
                <div align="right">
                    {$FINAL_DETAILS["grandTotal"]}
                </div>
            </td>
        </tr>
        {if $MODULE_NAME eq 'Invoice' or $MODULE_NAME eq 'PurchaseOrder'}
            <tr>
                <td width="83%">
                    {if $MODULE_NAME eq 'Invoice'}
                        <div align="right">
                            <strong>{vtranslate('LBL_RECEIVED',$MODULE_NAME)}</strong>
                        </div>
                    {else}
                        <div align="right">
                            <strong>{vtranslate('LBL_PAID',$MODULE_NAME)}</strong>
                        </div>
                    {/if}
                </td>
                <td>
                    {if $MODULE_NAME eq 'Invoice'}
                        <div align="right">
                            {if $RECORD->getDisplayValue('received')} 
                                {$RECORD->getDisplayValue('received')}
                            {else}
                                0
                            {/if}
                        </div>
                    {else}
                        <div align="right">
                            {if $RECORD->getDisplayValue('paid')}
                                {$RECORD->getDisplayValue('paid')}
                            {else}
                                0
                            {/if}
                        </div>
                    {/if}
                </td>
            </tr>
            <tr>
                <td width="83%">
                    <div align="right">
                        <strong>{vtranslate('LBL_BALANCE',$MODULE_NAME)}</strong>
                    </div>
                </td>
                <td>
                    <div align="right">
                        {if $RECORD->getDisplayValue('balance')}
                            {$RECORD->getDisplayValue('balance')}
                        {else}0
                        {/if}
                    </div>
                </td>
            </tr>
        {/if}
    </table> 
</div>
{/if}    
