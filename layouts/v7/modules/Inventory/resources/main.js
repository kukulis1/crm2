let isPage = new URLSearchParams(window.location.search);


if(isPage.get('module') == 'Invoice' && isPage.get('view') == 'Edit' && isPage.get('create') == null){
  setTimeout(()=>{
    var so_popup = document.querySelectorAll('.so_popup');
    for(let i = 1; i < so_popup.length; i++){   
      var so_btn =  document.getElementById(`so_popup${i}`);  
      so_btn.dataset.target = so_btn.dataset.target+i;
      so_btn.dataset.num = i;    
    } 
  },500);
}


// function giveRowNumber(){
//   var so_popup = document.querySelectorAll('.so_popup');
//   console.log('move');
//   for(let i = 1; i < so_popup.length; i++){   
//     var quantity = document.getElementById(`cargo_measure${i}`)
//     var measure = document.getElementById(`measure${i}`);
//     var pll = document.getElementById(`pll${i}`);
//     var cargo_measure_invoice = document.getElementById(`cargo_measure_invoice${i}`);
//     var cargo_wgt = document.getElementById(`cargo_wgt${i}`);
//     var qty = document.getElementById(`qty${i}`);  

//     quantity.name = `quantity${i}`;
//     measure.name = `measure${i}`;
//     pll.name = `pll${i}`;
//     cargo_measure_invoice.name = `cargo_measure_invoice${i}`;
//     cargo_wgt.name = `cargo_wgt${i}`;
//     qty.name = `qty${i}`;    
//   }

// }


if((isPage.get('module') == 'Invoice' && isPage.get('view') != 'Detail') && (isPage.get("orderid") != null || isPage.get("userid") != null || isPage.get("record") != null) ){

function popupSalesOrder(source){  
  let id = source.target.dataset.num;
  let order = document.getElementById(`order_id${id}`).value;
  $.ajax({ 
    type: "POST",   
    url: "invoices/fill_popup.php",
    data: {order:order}, 
    dataType: "JSON", 
    success: function(result){              
         insertSalesOrderValues(result,id);         
      }            
  }); 
  $(`#salesorder${id}`).draggable({
    handle: ".modal-body"
  });
  
  $('.modal-dialog').css({
    top: 0
  });

  $('.modal-dialog').draggable({
    handle: ".modal-body"
  }); 

}

function insertSalesOrderValues(orders,i){  
      if(orders.info.total == null) orders.info.total = 0;  
      if(orders.info.type == null)  orders.info.type = '';
      if(orders.info.status == null)  orders.info.status = '';
      if(orders.info.salesorderid == null)  orders.info.salesorderid = '';
      if(orders.info.accountname == null)  orders.info.accountname = '';
      if(orders.info.load_date_from == null)  orders.info.load_date_from = '';
      if(orders.info.unload_date_to == null)  orders.info.unload_date_to = '';
      if(orders.info.load_company == null)  orders.info.load_company = '';
      if(orders.info.unload_company == null)  orders.info.unload_company = '';
      if(orders.info.sender_code == null)  orders.info.sender_code = '';
      if(orders.info.receiver_code == null)  orders.info.receiver_code = '';
      if(orders.info.sender_contact == null)  orders.info.sender_contact = '';
      if(orders.info.receiver_contact == null)  orders.info.receiver_contact = '';
      if(orders.info.bill_street == null)  orders.info.bill_street = '';
      if(orders.info.ship_street == null)  orders.info.ship_street = '';
      if(orders.info.ship_city == null)  orders.info.ship_city = '';
      if(orders.info.bill_city == null)  orders.info.bill_city = '';
      if(orders.info.ship_code == null)  orders.info.ship_code = '';
      if(orders.info.bill_code == null)  orders.info.bill_code = '';
      if(orders.info.bill_country == null)  orders.info.bill_country = '';
      if(orders.info.ship_country == null)  orders.info.ship_country = '';
      if(orders.info.salesorder_no == null)  orders.info.salesorder_no = '';
      if(orders.info.source == null)  orders.info.source = '';
      if(orders.info.modifiedtime == null)  orders.info.modifiedtime = '';
      if(orders.info.createdtime == null)  orders.info.createdtime = '';
      if(orders.info.termo == null)  orders.info.termo = '';
      if(orders.info.stevedoring == null)  orders.info.stevedoring = '';
      if(orders.info.km == null)  orders.info.km = '';
      if(orders.info.working_time == null)  orders.info.working_time = '';
      if(orders.info.shipment_code == null)  orders.info.shipment_code = '';
      if(orders.info.pricebook == null)  orders.info.pricebook = '';
      if(orders.info.ordered == null)  orders.info.ordered = '';
      if(orders.info.ordered_m3 == null)  orders.info.ordered_m3 = '';
      if(orders.info.revised == null)  orders.info.revised = '';
      if(orders.info.revised_m3 == null)  orders.info.revised_m3 = '';
      if(orders.info.agreed_price == null)  orders.info.agreed_price = '';
      if(orders.info.agreed_price_desc == null)  orders.info.agreed_price_desc = '';
      if(orders.info.pricebook_price == null)  orders.info.pricebook_price = '';
      if(orders.info.note == null)  orders.info.note = '';
      if(orders.info.cargo_measure == null)  orders.info.cargo_measure = '';
      if(orders.info.quantity == null)  orders.info.quantity = '';
      if(orders.info.total == null)  orders.info.total = '';

      if(orders.info.termo == 0) orders.info.termo = 'Ne'; else orders.info.termo = 'Taip';
      if(orders.info.stevedoring == 0) orders.info.stevedoring = 'Ne'; else orders.info.stevedoring = 'Taip';

     


      document.getElementById(`type${i}`).innerHTML = orders.info.type;
      document.getElementById(`status${i}`).innerHTML = orders.info.status;
      document.getElementById(`salesorderid${i}`).innerHTML = orders.info.salesorderid;
      document.getElementById(`client${i}`).innerHTML = orders.info.accountname;
      document.getElementById(`load_date${i}`).innerHTML = orders.info.load_date_from;
      document.getElementById(`unload_date${i}`).innerHTML = orders.info.unload_date_to;
      document.getElementById(`sender${i}`).innerHTML = orders.info.load_company;
      document.getElementById(`receiver${i}`).innerHTML = orders.info.unload_company;
      document.getElementById(`sender_code${i}`).innerHTML = orders.info.sender_code;
      document.getElementById(`receiver_code${i}`).innerHTML = orders.info.receiver_code;
      document.getElementById(`sender_contact${i}`).innerHTML = orders.info.sender_contact;
      document.getElementById(`receiver_contact${i}`).innerHTML = orders.info.receiver_contact;
      document.getElementById(`load_address${i}`).innerHTML = orders.info.bill_street;
      document.getElementById(`unload_address${i}`).innerHTML = orders.info.ship_street;
      document.getElementById(`load_city${i}`).innerHTML = orders.info.bill_city;
      document.getElementById(`unload_city${i}`).innerHTML = orders.info.ship_city;
      document.getElementById(`load_code${i}`).innerHTML = orders.info.bill_code;
      document.getElementById(`unload_code${i}`).innerHTML = orders.info.ship_code;
      document.getElementById(`load_country${i}`).innerHTML = orders.info.bill_country;
      document.getElementById(`unload_country${i}`).innerHTML = orders.info.ship_country;
      document.getElementById(`order_no${i}`).innerHTML = orders.info.salesorder_no;
      document.getElementById(`source${i}`).innerHTML = orders.info.source;
      document.getElementById(`updated_time${i}`).innerHTML = orders.info.modifiedtime;
      document.getElementById(`created_time${i}`).innerHTML = orders.info.createdtime;
      document.getElementById(`termo${i}`).innerHTML = orders.info.termo;
      document.getElementById(`stevedoring${i}`).innerHTML = orders.info.stevedoring;
      document.getElementById(`km${i}`).innerHTML = orders.info.km;
      document.getElementById(`working_time${i}`).innerHTML = orders.info.working_time;
      document.getElementById(`shipment${i}`).innerHTML = orders.info.shipment_code;
      document.getElementById(`pricebook${i}`).innerHTML = orders.info.pricebook;
      document.getElementById(`ordered${i}`).innerHTML = orders.info.ordered;
      document.getElementById(`ordered_m3${i}`).innerHTML = orders.info.ordered_m3;
      document.getElementById(`revised${i}`).innerHTML = orders.info.revised;
      document.getElementById(`revised_m3${i}`).innerHTML = orders.info.revised_m3;
      document.getElementById(`agreed_price${i}`).innerHTML = orders.info.agreed_price;
      document.getElementById(`agreed_price_desc${i}`).innerHTML = orders.info.agreed_price_desc;
      document.getElementById(`pricebook_price${i}`).innerHTML = orders.info.pricebook_price;
      document.getElementById(`note${i}`).innerHTML = orders.info.note;    
      document.getElementById(`grandTotalPop${i}`).innerHTML = parseFloat(orders.info.total).toFixed(2);

      let dim = '';
      let table = document.getElementById(`dimensions${i}`);

       orders.dimensions.forEach(item => {
        if(item.cargo_wgt == null) item.cargo_wgt = 0;
        if(item.cargo_length == null) item.cargo_length = 0;
        if(item.cargo_width == null) item.cargo_width = 0;
        if(item.cargo_height == null) item.cargo_height = 0;
        if(item.description == null) item.description = '';
        if(item.cargo_measure == null) item.cargo_measure = '';
        if(item.quantity == null) item.quantity = '';

        let product;
        if(item.cargo_wgt != 'Services'){
          product = `${item.cargo_wgt} ${item.cargo_length}x${item.cargo_width}x${item.cargo_height}`;
        }else{
          product = item.service_name;
        }
         dim += `
         <tr>
         <td><div class="dimens"><h5>${product}</h5></div>
         <div class="dimens"><p>${item.description}</p></div>
         </td>
         <td>${item.cargo_measure}</td>
         <td>${parseInt(item.quantity)}</td>
         </tr>
         `;   
 
       });  
       
       table.innerHTML = dim;

}


}

function getTaresArray(id){
  return document.getElementById(`cargo_measure_invoice${id}`).value;
}

function getTares(e,option){
  let id = e.target.dataset.measure;
  let cargo_measure = getTaresArray(id);
 
  if (cargo_measure.indexOf(',') > -1){  
    cargo_measure = cargo_measure.split(",");
  }else{
    cargo_measure = new Array(cargo_measure);
  }
  
  let html ='';
  html = `<table id="tare_table${id}">`;
  let num = 1;
  cargo_measure.forEach((item)=>{ 
    measure = item.split(" ");
      qty = (measure != '' ? measure[0] : '');
      tare = (measure != '' ? measure[1] : '');     

    html += `<tr><td><input data-qty="${num}" class="smallInputBox inputElement qty_input" value="${qty}" style="${(num > 1) ? 'margin-top: 5px;' :''} width: 40px; margin-right: 5px;">
    <select data-select="${num}" class="inputElement tare_input" style="${(num > 1) ? 'margin-top: 5px;' :''} width: 150px;">`;
    html += `<option value="${tare}">${tare}</option>`;
    html += option;
    html += `</select><button type="button" data-remove="${num}" class="close" style="margin-left: 7px;" onclick="removeField(${num});"><span  class="fa fa-close"></span></button></td></tr>`;
    num++;
  });
  html += '</table>';       
  html += `<button type="button" class="btn btn-success btn-sm" style="margin-top: 15px;" onclick="savePop();">Saugoti</button>
           <button type="button" class="btn btn-danger btn-sm" style="margin-top: 15px;" onclick="closepop();">Atšaukti</button>
           <button type="button" class="btn btn-secondary btn-sm" style="margin-top: 15px;" onclick="addField();">Pridėti laukelį</button>`;


  closepop = function(){
    $(`[data-toggle="popover${id}"]`).popover('hide');
  }

  savePop = function(){
    let tares = document.querySelectorAll('.tare_input');
    let qtys = document.querySelectorAll('.qty_input');
    let invoice_input = document.getElementById(`cargo_measure_invoice${id}`);
    let invoice_show = document.getElementById(`measure${id}`);
    let taresArr = new Array();
    let taresArr2 = new Array();

    for(let i = 0; i < tares.length; i++){
      cargo = qtys[i].value+" "+tares[i].value;
      cargo2 = qtys[i].value+" "+tares[i].value+"<br>";
      taresArr.push(cargo);
      taresArr2.push(cargo2);
    }
    invoice_input.value = taresArr;
    taresArr2 = taresArr2.toString();
    taresArr2 = taresArr2.replace(/,/g, '');
    invoice_show.innerHTML = taresArr2;
    $(`[data-toggle="popover${id}"]`).popover('destroy');
  }

  addField = function(){
    let select = document.createElement('select');
    let input = document.createElement('input');
    let btn = document.createElement('button');
    let span = document.createElement('span');
    let last = document.querySelectorAll(`#tare_table${id} td`).length;
    let table = document.querySelectorAll(`#tare_table${id} td`)[last-1];
    select.setAttribute('class', 'inputElement tare_input');
    select.setAttribute('style', 'margin-top: 5px; width: 150px;');
    select.setAttribute('data-select', `${num}`);
    input.setAttribute('class','smallInputBox inputElement qty_input');
    input.setAttribute('value','1');
    input.setAttribute('style', 'width: 40px; margin-top: 5px; margin-right: 8px;');
    input.setAttribute('data-qty', `${num}`);
    btn.setAttribute('type', 'button');
    btn.setAttribute('data-remove', `${num}`);
    btn.setAttribute('class', 'close');
    btn.setAttribute('style', 'margin-left: 7px;');
    btn.setAttribute('onclick', `removeField(${num});`);
    span.setAttribute('class', 'fa fa-close');
    btn.append(span);
    select.innerHTML = option;
    table.append(input,select,btn);
  }

  removeField = function(i){
    document.querySelector(`[data-qty='${i}']`).remove();
    document.querySelector(`[data-select='${i}']`).remove();
    document.querySelector(`[data-remove='${i}']`).remove();
  }
     
 $(`[data-toggle="popover${id}"]`).popover({
 html:true,
 content: function(){
   return html;
 }});  
 

}

function tareSelector(e){
  const url = 'https://uzsakymai.parnasas.lt/export/crm/tare_types.php';
  const user = '123';
  const password = 'raktas';
  const formdata = new FormData();  

  formdata.append('user',user);
  formdata.append('password',password);  
  let measures = [];
  let option = '';
 fetch(url,{ method: 'POST',body: formdata})   
    .then((resp) => resp.json())         
    .then(function(data){ 
      measures.push(data.measures); 
      data.measures.forEach(measure => {
        option += `<option value="${measure.code}">${measure.code}</option>`;
      });
      getTares(e,option);
  }); 
  
}

if( isPage.get('module') == 'Invoice' && isPage.get('view') == 'Edit' && isPage.get("record") != null ) {

  let orderId = isPage.get("record");
  if(orderId != null) orderId = orderId.replace(/'/g,'');

    $.ajax({ 
      type: "POST",
      url: "invoices/invoice_filler_edit.php",
      data: {orderId:orderId}, 
      dataType : 'JSON',
      success: function(result){   
        if(result != 'empty'){
          document.getElementById('duedate').value = result;        
        }                     
        }            
    }); 
  
}