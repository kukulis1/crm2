{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
{strip}
{if !empty($PICKIST_DEPENDENCY_DATASOURCE)}
    <input type="hidden" name="picklistDependency" value='{Vtiger_Util_Helper::toSafeHTML($PICKIST_DEPENDENCY_DATASOURCE)}' />
{/if}
<div name='editContent'>
	{if $DUPLICATE_RECORDS}
		<div class="fieldBlockContainer duplicationMessageContainer">
			<div class="duplicationMessageHeader"><b>{vtranslate('LBL_DUPLICATES_DETECTED', $MODULE)}</b></div>
			<div>{getDuplicatesPreventionMessage($MODULE, $DUPLICATE_RECORDS)}</div>
		</div>
	{/if}

{if $MODULE eq 'PurchaseOrder'}
    <div id="fileBox" name='editContent' style="display:none;">
        <div class="fieldBlockContainer">
        <h4 class="fieldBlockHeader">Sąskaitos dokumentas</h4>
        <hr>
            <div class="row">
                <div id="fileinfo" class="col-md-6">
                    <input type="hidden" name="mailid">
                    <input type="hidden" name="userid">
                    <input type="hidden" name="filecount">
                </div>
            <div class="col-md-6 search_in_mail" style="display:none;">                
                <input type="text" class="inputElement" id="search_document" placeholder="Ieškoti failų gautuose laiškuose" >
                <small style="display: block;">Minimum 3 simboliai</small>
                <div class="search_content" style="display:none;margin-top: 10px;border: 1px solid #ddd;padding-top: 15px;"></div>  
                <div class="empty-results" style="display: none;"><p>Nieko nerasta</p></div>            
            </div>
            </div>
        </div>
    </div>

    <style>
    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: .25rem;
    }
    .card-body {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
    }
    .card-hover {
       cursor: pointer;
       margin-bottom: 10px;
    }
    .card-hover:hover {
        border: 1px solid gold;
        background: aliceblue;
    }
    ul#menu li {
        display:inline;
        padding-left: 10px;
        margin-right: 15px;
    }
    </style>

<div class="modal fade" id="purchaseTemplates" tabindex="-1" role="dialog" aria-labelledby="purchaseTemplatesTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 700px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pirkimo saskaitu šablonai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="popup_body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
      </div>
    </div>
  </div>
</div>

{/if}    
    {foreach key=BLOCK_LABEL item=BLOCK_FIELDS from=$RECORD_STRUCTURE name=blockIterator}
        {if $BLOCK_LABEL eq 'LBL_ITEM_DETAILS'}{continue}{/if}
         {if $BLOCK_FIELDS|@count gt 0}
     {if $BLOCK_LABEL neq 'LBL_ADDRESS_INFORMATION' && $BLOCK_LABEL neq 'LBL_TERMS_INFORMATION'}
         
             <div class='fieldBlockContainer' data-block="{$BLOCK_LABEL}">
                     <h4 class='fieldBlockHeader' {if $MODULE eq 'SalesOrder' && $BLOCK_LABEL eq 'LBL_SO_INFORMATION'}style="display:inline;margin-right: 200px;"{/if}>{vtranslate($BLOCK_LABEL, $MODULE)}</h4>
                    {if $MODULE eq 'SalesOrder' && $BLOCK_LABEL eq 'LBL_SO_INFORMATION'}
                        <span id="last_shipment_code" style="margin-right: 20px"></span>
                        <input name="assign_to_last_order" id="assign_to_last_order" type="checkbox" class="hide" style="margin-right: 5px"> <span id="last_shipment_code_text" class="hide"> Priskirti prie paskutinio užsakymo</span>
                    {/if}
                 <hr>
                 <table class="table table-borderless {if $BLOCK_LABEL eq 'LBL_ADDRESS_INFORMATION'} addressBlock{/if}">
                     {if ($BLOCK_LABEL eq 'LBL_ADDRESS_INFORMATION') and ($MODULE neq 'PurchaseOrder') }
                        <tr>
                            <td class="fieldLabel " name="copyHeader1">
                                <label  name="togglingHeader">{vtranslate('LBL_BILLING_ADDRESS_FROM', $MODULE)}</label>
                            </td>
                            <td class="fieldValue" name="copyAddress1">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="copyAddressFromRight" class="accountAddress" data-copy-address="billing" checked="checked">
                                        &nbsp;{vtranslate('SINGLE_Accounts', $MODULE)}
                                    </label>
                                </div>
                                <div class="radio">
                                    <label> 
                                        {if $MODULE eq 'Quotes'}
                                            <input type="radio" name="copyAddressFromRight" class="contactAddress" data-copy-address="billing" checked="checked">
                                            &nbsp;{vtranslate('Related To', $MODULE)}
                                        {else}
                                            <input type="radio" name="copyAddressFromRight" class="contactAddress" data-copy-address="billing" checked="checked">
                                            &nbsp;{vtranslate('SINGLE_Contacts', $MODULE)}
                                        {/if}
                                    </label>
                                </div>
                                <div class="radio" name="togglingAddressContainerRight">
                                    <label>
                                        <input type="radio" name="copyAddressFromRight" class="shippingAddress" data-target="shipping" checked="checked">
                                        &nbsp;{vtranslate('Shipping Address', $MODULE)}
                                    </label>
                                </div>
                                <div class="radio hide" name="togglingAddressContainerLeft">
                                    <label>
                                        <input type="radio" name="copyAddressFromRight"  class="billingAddress" data-target="billing" checked="checked">
                                        &nbsp;{vtranslate('Billing Address', $MODULE)}
                                    </label>
                                </div>
                            </td>
                            <td class="fieldLabel" name="copyHeader2">
                                <label  name="togglingHeader">{vtranslate('LBL_SHIPPING_ADDRESS_FROM', $MODULE)}</label>
                            </td>
                            <td class="fieldValue" name="copyAddress2">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="copyAddressFromLeft" class="accountAddress" data-copy-address="shipping" checked="checked">
                                        &nbsp;{vtranslate('SINGLE_Accounts', $MODULE)}
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        {if $MODULE eq 'Quotes'}
                                            <input type="radio" name="copyAddressFromLeft" class="contactAddress" data-copy-address="shipping" checked="checked">
                                            &nbsp;{vtranslate('Related To', $MODULE)}
                                        {else}
                                            <input type="radio" name="copyAddressFromLeft" class="contactAddress" data-copy-address="shipping" checked="checked">
                                            &nbsp;{vtranslate('SINGLE_Contacts', $MODULE)}	
                                        {/if}
                                    </label>
                                </div>
                                <div class="radio" name="togglingAddressContainerLeft">
                                    <label>
                                        <input type="radio" name="copyAddressFromLeft" class="billingAddress" data-target="billing" checked="checked">
                                        &nbsp;{vtranslate('Billing Address', $MODULE)}
                                    </label>
                                </div>
                                <div class="radio hide" name="togglingAddressContainerRight">
                                    <label>
                                        <input type="radio" name="copyAddressFromLeft" class="shippingAddress" data-target="shipping" checked="checked">
                                        &nbsp;{vtranslate('Shipping Address', $MODULE)}
                                    </label>
                                </div>
                            </td>
                        </tr>
                    {/if}
                     <tr>
                     {assign var=COUNTER value=0}

                     {foreach key=FIELD_NAME item=FIELD_MODEL from=$BLOCK_FIELDS name=blockfields}                   
                         {assign var="isReferenceField" value=$FIELD_MODEL->getFieldDataType()}
                         {assign var="refrenceList" value=$FIELD_MODEL->getReferenceList()}
                         {assign var="refrenceListCount" value=count($refrenceList)}
                         {if $FIELD_MODEL->isEditable() eq true && !($FIELD_NAME === 'cf_1661' && $MODULE === 'SalesOrder' && !empty($RECORD_ID))}
                             {if $FIELD_MODEL->get('uitype') eq "19"}
                                 {if $COUNTER eq '1'}
                                     <td></td><td></td></tr><tr>
                                     {assign var=COUNTER value=0}
                                 {/if}
                             {/if}
                             {if $COUNTER eq 2}
                                 </tr><tr>
                                 {assign var=COUNTER value=1}
                             {else}
                                 {assign var=COUNTER value=$COUNTER+1}
                             {/if}
                             {* ITOMA *}
                         
                            {if $FIELD_NAME eq 'subject' and ($MODULE eq 'Invoice' or $MODULE eq 'PurchaseOrder')}
                            {elseif $FIELD_NAME eq 'cf_1365' && empty($GET['record'])}
                            	<td class="fieldLabel alignMiddle"></td>
                            {else}
                            {* {if $FIELD_NAME neq 'cf_1365' &&  empty($GET['record'])} *}
                            {if $FIELD_NAME neq 'cf_1612' &&  $FIELD_NAME neq 'cf_1616' }
                             <td class="fieldLabel alignMiddle" {if $FIELD_MODEL->get('label') eq 'Sales Order' and $MODULE eq 'Invoice'}id="sales_order_title" {/if}>
                           
                             {if $FIELD_MODEL->isMandatory() eq true} <span class="redColor">*</span> {/if}
                             {if $isReferenceField eq "reference"}
                                 {if $refrenceListCount > 1}
                                     {assign var="REFERENCED_MODULE_ID" value=$FIELD_MODEL->get('fieldvalue')}
                                     {assign var="REFERENCED_MODULE_STRUCTURE" value=$FIELD_MODEL->getUITypeModel()->getReferenceModule($REFERENCED_MODULE_ID)}
                                     {if !empty($REFERENCED_MODULE_STRUCTURE)}
                                        {assign var="REFERENCED_MODULE_NAME" value=$REFERENCED_MODULE_STRUCTURE->get('name')}
                                     {/if}                                       
                                     <select style="width: 140px;" class="select2 referenceModulesList">
                                        {foreach key=index item=value from=$refrenceList}
                                            <option value="{$value}" {if $value eq $REFERENCED_MODULE_NAME} selected {/if}>{vtranslate($value, $value)}</option>
                                        {/foreach}
                                    </select>
                                 {else}                                 
                                     {vtranslate($FIELD_MODEL->get('label'), $MODULE)}
                                 {/if}
                             {else}
                                 {vtranslate($FIELD_MODEL->get('label'), $MODULE)}
                             {/if}
                             {/if}
                             &nbsp;&nbsp;
                         </td>
                       {/if}
                  
                       {* ITOMA *}    
                         {if  $FIELD_NAME neq 'cf_1616' && !($FIELD_NAME === 'cf_1661' && $MODULE === 'SalesOrder' && !empty($RECORD_ID))}                                      
                         <td class="fieldValue" {if $FIELD_MODEL->getFieldDataType() eq 'boolean'} style="width:25%" {/if}  {if $FIELD_MODEL->get('label') eq 'Sales Order' and $MODULE eq 'Invoice'}id="sales_order_input" {/if}
                         {if $FIELD_MODEL->get('uitype') eq '19'} colspan="3" {assign var=COUNTER value=$COUNTER+1} {/if}>  
                                                 
                             {if $FIELD_MODEL->getFieldDataType() eq 'image' || $FIELD_MODEL->getFieldDataType() eq 'file'}
                                 <div class='col-lg-4 col-md-4 redColor'>
                                     {vtranslate('LBL_NOTE_EXISTING_ATTACHMENTS_WILL_BE_REPLACED', $MODULE)}
                                 </div>
                             {/if}                  
                             {include file=vtemplate_path($FIELD_MODEL->getUITypeModel()->getTemplateName(),$MODULE)}                             
                         </td>
                           {/if}   
                         {/if}
                   
                     {/foreach}

                     {*If their are odd number of fields in edit then border top is missing so adding the check*}
                     {if $COUNTER is odd}
                         <td></td>
                         <td></td>
                     {/if}
                     </tr>
                 </table>
             </div>
             {/if}
         {/if}
     {/foreach}
 </div>

<input type="hidden" id="globalMetersValue">
<input type="hidden" id="accMetersValue">
<div id="sumPrices" style="display:none;">
</div>

{if $INDEPENDENT}
<input type="hidden" name="INDEPENDENT" value="1">
{/if}

{* {if $MODULE eq 'SalesOrder'}
    <div class='fieldBlockContainer' data-block="NOTE_FOR_INVOICE">
                     <h4 class='fieldBlockHeader'>{vtranslate('NOTE_FOR_INVOICE', $MODULE)}</h4>
                 <hr>
    <table class="table table-borderless">
    <tr>  
    <td class="fieldLabel alignMiddle">{vtranslate('NOTE', $MODULE)}</td>
            <td class="fieldValue" colspan="3">                                     
                <textarea rows="3" class="inputElement textAreaElement col-lg-12 " name="note"></textarea>                         
            </td>
    </tr>
    </table>
    </div>
{/if} *}

{include file="partials/LineItemsEdit.tpl"|@vtemplate_path:'Inventory'}
