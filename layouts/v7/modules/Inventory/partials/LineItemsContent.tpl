{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
	{assign var="deleted" value="deleted"|cat:$row_no}
	{assign var="image" value="productImage"|cat:$row_no}
	{assign var="purchaseCost" value="purchaseCost"|cat:$row_no}
	{assign var="margin" value="margin"|cat:$row_no}
    {assign var="hdnProductId" value="hdnProductId"|cat:$row_no}
    {assign var="productName" value="productName"|cat:$row_no}

	    {assign var="cargo_wgt" value="cargo_wgt"|cat:$row_no}
	    {assign var="cargo_length" value="cargo_length"|cat:$row_no}
	    {assign var="cargo_width" value="cargo_width"|cat:$row_no}
	    {assign var="cargo_height" value="cargo_height"|cat:$row_no}

    {assign var="purchase" value="purchase"|cat:$row_no}
	  {assign var="invoice_num" value="invoice_num"|cat:$row_no}
    {assign var="cargo_measure" value="cargo_measure"|cat:$row_no}
    {assign var="comment" value="comment"|cat:$row_no}
    {assign var="productDescription" value="productDescription"|cat:$row_no}
    {assign var="qtyInStock" value="qtyInStock"|cat:$row_no}
    {assign var="qty" value="qty"|cat:$row_no}
    {assign var="cargo_measure_invoice" value="cargo_measure_invoice"|cat:$row_no}
    {assign var="listPrice" value="listPrice"|cat:$row_no}
    {assign var="productTotal" value="productTotal"|cat:$row_no}
    {assign var="subproduct_ids" value="subproduct_ids"|cat:$row_no}
    {assign var="subprod_names" value="subprod_names"|cat:$row_no}
	  {assign var="subprod_qty_list" value="subprod_qty_list"|cat:$row_no}
    {assign var="entityIdentifier" value="entityType"|cat:$row_no}
    {assign var="entityType" value=$data.$entityIdentifier}

    {assign var="discount_type" value="discount_type"|cat:$row_no}
    {assign var="discount_percent" value="discount_percent"|cat:$row_no}
    {assign var="checked_discount_percent" value="checked_discount_percent"|cat:$row_no}
    {assign var="style_discount_percent" value="style_discount_percent"|cat:$row_no}
    {assign var="discount_amount" value="discount_amount"|cat:$row_no}
    {assign var="checked_discount_amount" value="checked_discount_amount"|cat:$row_no}
    {assign var="style_discount_amount" value="style_discount_amount"|cat:$row_no}
    {assign var="checked_discount_zero" value="checked_discount_zero"|cat:$row_no}
    {* itoma *}
    {assign var="load_id" value="load_id"|cat:$row_no}
    {assign var="meters" value="meters"|cat:$row_no}
    {assign var="order_id" value=$order_id}
    {assign var="productnameInv" value="productnameInv"|cat:$row_no}
    {assign var="independentInvoice" value="independentInvoice"|cat:$row_no}
   						

    {assign var="discountTotal" value="discountTotal"|cat:$row_no}
    {assign var="totalAfterDiscount" value="totalAfterDiscount"|cat:$row_no}
    {assign var="taxTotal" value="taxTotal"|cat:$row_no}
    {assign var="netPrice" value="netPrice"|cat:$row_no}
    {assign var="FINAL" value=$RELATED_PRODUCTS.1.final_details}

	{assign var="productDeleted" value="productDeleted"|cat:$row_no}
	{assign var="productId" value=$data[$hdnProductId]}
		{assign var="tare" value=$TARE}
		{assign var="measure_list" value=$measure_list}
		{assign var="services" value=$services}
   {assign var="service_id" value="service_id"|cat:$row_no}

	 		{assign var="cost_center" value=$cost_center}
	    {assign var="costid" value="costid"|cat:$row_no}

	    {assign var="employee" value="employee"|cat:$row_no}
	    {assign var="object" value="object"|cat:$row_no}
	    {assign var="purchase_date" value="purchase_date"|cat:$row_no}
		{assign var="GET" value=$GET}
			
	{assign var="listPriceValues" value=Products_Record_Model::getListPriceValues($productId)}
	{if $MODULE eq 'PurchaseOrder'}
		{assign var="listPriceValues" value=array()}
		{assign var="purchaseCost" value="{if $data.$purchaseCost}{((float)$data.$purchaseCost) / ((float)$data.$qty * {$RECORD_CURRENCY_RATE})}{else}0{/if}"}
	  {assign var="region" value="region"|cat:$row_no}

		
		{foreach item=currency_details from=$CURRENCIES}
			{append var='listPriceValues' value=$currency_details.conversionrate * $purchaseCost index=$currency_details.currency_id}
		{/foreach}
	{/if}


 {if $MODULE neq 'PurchaseOrder' && $data['independent'] neq '121391'}
	<td style="text-align:center;">
		<i class="fa fa-trash deleteRow cursorPointer" title="{vtranslate('LBL_DELETE',$MODULE)}"></i>
		&nbsp;<a><img src="{vimage_path('drag.png')}" border="0" title="{vtranslate('LBL_DRAG',$MODULE)}"/></a>&nbsp;
		<input type="hidden" class="rowNumber" value="{$row_no}" />
		<input type="hidden" id="load_id{$row_no}" name="load_id{$row_no}"  value="{$data.$load_id}" />
		<input type="hidden" name="external_order_id" value="{$data['order_id']}" />
		<input type="hidden" data-come-from="{$row_no}" id="come_from{$row_no}" value="{if $data.$productnameInv eq 'Metrika'}1{else}0{/if}" />
	</td>
	{else}
	<td style="text-align:center;"><i class="fa fa-trash deleteRow cursorPointer" title="{vtranslate('LBL_DELETE',$MODULE)}"></i>
	</td>
	{/if}
	{if $IMAGE_EDITABLE}
		<td class='lineItemImage' style="text-align:center;">
			<img src='{$data.$image}' height="42" width="42">
		</td>
	{/if}
	{if $PRODUCT_EDITABLE}
		<td>
			<!-- Product Re-Ordering Feature Code Addition Starts -->
			<input type="hidden" name="hidtax_row_no{$row_no}" id="hidtax_row_no{$row_no}" value="{$tax_row_no}"/>
			<!-- Product Re-Ordering Feature Code Addition ends -->
			<div class="itemNameDiv form-inline">
				<div class="row">
					<div class="col-lg-10">
						<div class="input-group2 {if $MODULE neq 'SalesOrder'}input-group{/if}" style="{if $MODULE eq 'PurchaseOrder' || $data['independent'] eq '121391' || $newrecord}width:550px;{else}width: 100%;margin-left: -70px;{/if}">
					
                  
       {if $MODULE neq 'PurchaseOrder' && $data['independent'] neq '121391' && !$newrecord}																										{* ITOMA *}																			


	<input type="hidden" id="{$productName}" name="{$productName}" value="1" class="productName form-control" autoComplete="off">	
	<input type="hidden" id="skipMeters{$row_no}" value="0">	

	<input type="text" id="{$cargo_wgt}" name="{$cargo_wgt}" value="{if $data.$cargo_wgt}{$data.$cargo_wgt}{else}{if empty($GET['record'])}{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 60px;margin-right: 10px;" autoComplete="off" data-rule-required=true>

	<input type="text" id="{$cargo_length}" name="{$cargo_length}" value="{if $data.$cargo_length}{$data.$cargo_length}{else}{if empty($GET['record'])}0.8{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 40px;margin-right: 10px;" autoComplete="off" data-rule-required=true>
	<input type="text" id="{$cargo_width}" name="{$cargo_width}" value="{if $data.$cargo_width}{$data.$cargo_width}{else}{if empty($GET['record'])}1.2{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 40px;margin-right: 10px;" autoComplete="off" data-rule-required=true>
	<input type="text" id="{$cargo_height}" name="{$cargo_height}" value="{if $data.$cargo_height}{$data.$cargo_height}{else}{if empty($GET['record'])}{else}0{/if}{/if}" class="productName form-control" onkeypress="return isNumber(event)" style="width: 40px;" autoComplete="off" data-rule-required=true>
  <i id="meter_price{$row_no}" class="fa fa-exclamation-circle {if $data.$meters}{else}hide{/if}" title="Kaina skaičiuojama pagal metrus" style="font-size: 20px; position: absolute;top: 5px; margin-left: 10px;color:#fc631a;"></i>
	<input id="meters{$row_no}" type="hidden" name="meters{$row_no}">

        {else}
						<input type="text" id="{$productName}" name="{$productName}" value="{if $MODULE eq 'PurchaseOrder'}{$data.$purchase}{elseif $data['independent'] eq '121391'}{$data.$independentInvoice}{/if}" class="productName form-control" autocomplete="off" aria-required="true" data-rule-required="true" placeholder="{vtranslate('LBL_TYPE_SEARCH',$MODULE)}">
         {/if}    
								   {* {if !empty($data.$productName)} disabled="disabled" {/if} *}
									 								
						{if !$data.$productDeleted}
							{if $MODULE neq 'SalesOrder'}
								<span class="input-group-addon cursorPointer clearLineItem" title="{vtranslate('LBL_CLEAR',$MODULE)}">
									<i class="fa fa-times-circle"></i>
								</span> 
								{/if}
								{if $MODULE eq 'PurchaseOrder' || $data['independent'] eq '121391' || $newrecord}  
							<span id="addNewItemServiceBtn{$row_no}" class="fa fa-plus-circle  cursorPointer add_new_entity" title="Pridėti Prekę/Paslaugą"
							data-toggle="modal" data-target="#addNewItemService" style="font-size: 20px;position:absolute;top: 5px;right: 35px;"></span>   
							{/if}   
							                                                               
						{/if}	

							  {if $MODULE neq 'PurchaseOrder'}				
					<select id="service_select{$row_no}" name="service_select{$row_no}" class="inputElement tare" value="" style="display: none;">	
								<option value="0">{vtranslate('LBL_SELECT_OPTION_21_VAT', $MODULE)}</option>	    	
							{foreach item=service from=$services}	
							{if {$service['id']} neq '999'}
								<option {if $service['id'] eq $data.$service_id}selected{/if} value="{$service['id']}" >{$service['name']}</option>			
								{/if}
							{/foreach}
					</select>						
						<input type="hidden" id="service_hide_id{$row_no}" name="service_hide_id{$row_no}" 	value="{$data.$service_id}"/>
						{/if}						
	<input type="hidden" id="{$hdnProductId}" name="{$hdnProductId}" 	value="{if $MODULE eq 'PurchaseOrder'}36641{elseif $data['independent'] eq '121391'}121391{elseif empty($data.$hdnProductId)}14244{else}{$data.$hdnProductId}{/if}" class="selectedModuleId"/>
							{* ITOMA *}
							<input type="hidden" id="lineItemType{$row_no}" name="lineItemType{$row_no}" value="{if $data['independent'] eq '121391'}Item{else}Products{/if}" class="lineItemType"/>
							{* <input type="hidden" id="lineItemType{$row_no}" name="lineItemType{$row_no}" value="{$entityType}" class="lineItemType"/> *}
							<div class="col-lg-2">
								<!--Logtime-->         
                                                                {if $MODULE neq 'PurchaseOrder'} 
                                                                    {if $row_no eq 0}
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ServicesPopup" title="{vtranslate('Services',$MODULE)}" data-module-name="Services" data-field-name="serviceid"></span>
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ProductsPopup" title="{vtranslate('Products',$MODULE)}" data-module-name="Products" data-field-name="productid"></span>
                                                                    {elseif $entityType eq '' and $PRODUCT_ACTIVE eq 'true'}
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ProductsPopup" title="{vtranslate('Products',$MODULE)}" data-module-name="Products" data-field-name="productid"></span>
                                                                    {elseif $entityType eq '' and $SERVICE_ACTIVE eq 'true'}
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ServicesPopup" title="{vtranslate('Services',$MODULE)}" data-module-name="Services" data-field-name="serviceid"></span>
                                                                    {else}
                                                                            {if ($entityType eq 'Services') and (!$data.$productDeleted)}
                                                                                    <span class="lineItemPopup cursorPointer" data-popup="ServicesPopup" title="{vtranslate('Services',$MODULE)}" data-module-name="Services" data-field-name="serviceid"></span>
                                                                            {elseif (!$data.$productDeleted)}
                                                                                    <span class="lineItemPopup cursorPointer" data-popup="ProductsPopup" title="{vtranslate('Products',$MODULE)}" data-module-name="Products" data-field-name="productid"></span>
                                                                            {/if}
                                                                    {/if}
                                                                {else}
																																{* hide products select button *}
                                                                    {* {if $row_no eq 0}
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ServicesPopup" title="{vtranslate('Services',$MODULE)}" data-module-name="Services" data-field-name="serviceid">{Vtiger_Module_Model::getModuleIconPath('Services')}</span>
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ProductsPopup" title="{vtranslate('Products',$MODULE)}" data-module-name="Products" data-field-name="productid">{Vtiger_Module_Model::getModuleIconPath('Products')}</span>
                                                                    {elseif $entityType eq '' and $PRODUCT_ACTIVE eq 'true'}
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ProductsPopup" title="{vtranslate('Products',$MODULE)}" data-module-name="Products" data-field-name="productid">{Vtiger_Module_Model::getModuleIconPath('Products')}</span>
                                                                    {elseif $entityType eq '' and $SERVICE_ACTIVE eq 'true'}
                                                                            <span class="lineItemPopup cursorPointer" data-popup="ServicesPopup" title="{vtranslate('Services',$MODULE)}" data-module-name="Services" data-field-name="serviceid">{Vtiger_Module_Model::getModuleIconPath('Services')}</span>
                                                                    {else}
                                                                            {if ($entityType eq 'Services') and (!$data.$productDeleted)}
                                                                                    <span class="lineItemPopup cursorPointer" data-popup="ServicesPopup" title="{vtranslate('Services',$MODULE)}" data-module-name="Services" data-field-name="serviceid">{Vtiger_Module_Model::getModuleIconPath('Services')}</span>
                                                                            {elseif (!$data.$productDeleted)}
                                                                                    <span class="lineItemPopup cursorPointer" data-popup="ProductsPopup" title="{vtranslate('Products',$MODULE)}" data-module-name="Products" data-field-name="productid">{Vtiger_Module_Model::getModuleIconPath('Products')}</span>
                                                                            {/if}
                                                                    {/if}    *}
                                                                {/if}    
							</div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" value="{$data.$subproduct_ids}" id="{$subproduct_ids}" name="{$subproduct_ids}" class="subProductIds" />
			<div id="{$subprod_names}" name="{$subprod_names}" class="subInformation">
				<span class="subProductsContainer">

				</span>
			</div>
	
			{if $data.$productDeleted}
				<div class="row-fluid deletedItem redColor">
					{if empty($data.$productName)}
						{vtranslate('LBL_THIS_LINE_ITEM_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_THIS_LINE_ITEM',$MODULE)}
					{else}
						{vtranslate('LBL_THIS',$MODULE)} {$entityType} {vtranslate('LBL_IS_DELETED_FROM_THE_SYSTEM_PLEASE_REMOVE_OR_REPLACE_THIS_ITEM',$MODULE)}
					{/if}
				</div>
			{else}
                                <!--Logtime-->
																
				{if $COMMENT_EDITABLE}
					<div><br><textarea {if $MODULE eq 'PurchaseOrder' || $data['independent'] eq '121391'}style="max-width: 480px;"{/if} id="{$comment}" name="{$comment}" class="lineItemCommentBox form-control tare" {if $MODULE eq 'PurchaseOrder' || $data['independent'] eq '121391'}placeholder="Pastaba"{/if}>{decode_html($data.$comment)}</textarea></div>
				{/if}
			{/if}
		</td>
	{/if}
	{if $MODULE eq 'SalesOrder'}
	{assign var="load_id" value=$data.$load_id}
	<td>
	{if $data['service_id'|cat:$row_no] neq '12'}
		<div id="services_checkbox{$row_no}">
			<div> 
			<input type="checkbox" id="minifest{$row_no}" name="minifest{$row_no}" class="inputElement" {if $data['flags'|cat:$row_no][$load_id]["minifest"]}checked{/if} {if $GET['record'] && $data['flags'|cat:$row_no][$load_id]["minifest"]}disabled{/if}> Krovinio važtaraštis
			</div>
			<div> 
			<input type="checkbox" id="cmr{$row_no}" name="cmr{$row_no}" class="inputElement" {if $data['flags'|cat:$row_no][$load_id]["cmr"]}checked{/if} {if $GET['record'] && $data['flags'|cat:$row_no][$load_id]["cmr"]}disabled{/if}>
				CMR
			</div>
			<div> 
			<input type="checkbox" id="invoice{$row_no}" name="invoice{$row_no}" class="inputElement" {if $data['flags'|cat:$row_no][$load_id]["invoice"]}checked{/if} {if $GET['record'] && $data['flags'|cat:$row_no][$load_id]["invoice"]}disabled{/if}> Sąskaita
			</div>
		</div>
		{/if}
	</td>
	<td>
	<div id="service_div{$row_no}" style="display: none;">Paslaugos kaina: <input id="service_price{$row_no}" onkeypress="return isNumber(event);" class="smallInputBox inputElement tare" type="text" value="{$data.$margin}" style="display: block;"></div>
		<select id="measure{$row_no}" name="measure{$row_no}" class="inputElement tare">	
				<option value="1">pll</option>	
		  {$i = 0}      	
			{foreach key=tara_key item=TARA_INFO from=$tare}	
				<option value="{$TARA_INFO['id']}" {if $data['measure'] eq $TARA_INFO['id'] || $data['measure'] eq $TARA_INFO['code']}selected{/if}>{$TARA_INFO['code']}</option>			
			{/foreach}
		</select>
	</td>
	{/if}


 {if $MODULE eq 'PurchaseOrder' || $data['independent'] eq '121391' || $newrecord}
	{if $MODULE neq 'Invoice'}
		<td style="width: 240px;">
		<div class="input-group inputElement">
		<input name="purchase_date{$row_no}" id="purchase_date{$row_no}" type="text" class="form-control" data-fieldtype="date" data-date-format="yyyy-mm-dd" data-rule-date="true" value="{if !empty($data.$purchase_date)}{$data.$purchase_date}{/if}" autoComplete="off" aria-required="true">
		<span class="input-group-addon"><i class="fa fa-calendar "></i></span></div>
		</td>
	{/if}
 <td style="width: 220px;">
			<select class="inputElement" id="costcenter{$row_no}" name="costcenter{$row_no}" style="display: block;">
			{foreach  item=COST_LIST from=$cost_center}	
				<option value="{$COST_LIST['costid']}" {if $data.$costid eq $COST_LIST['costid']}selected{/if}>{$COST_LIST['costname']}</option>
			{/foreach}
			</select>
			<div style="display: block; margin-top: 5px;">
			<input type="text" id="object{$row_no}" name="object{$row_no}" placeholder="Objektas" class="inputElement" autoComplete="off" value="{if !empty($data.$object)}{$data.$object}{/if}">
			</div>
			<div style="display: block; margin-top: 5px;">
			<input type="text" id="employee{$row_no}" name="employee{$row_no}" placeholder="Darbuotojas" class="inputElement" autoComplete="off" value="{if !empty($data.$employee)}{$data.$employee}{/if}">		
			</div>	
 </td>
 {/if}
	<td {if $MODULE eq 'PurchaseOrder' || $MODULE eq 'Invoice'}style="width: 220px;"{/if}>
		<input {if $MODULE eq 'SalesOrder'}oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1').replace(/\./g,'');"{/if} id="{$qty}" name="{$qty}" type="text" class="qty smallInputBox inputElement" data-num="{$row_no}"
			   data-rule-required=true  value="{if !empty($data.$qty)}{$data.$qty}{else}1{/if}"
			   {if $QUANTITY_EDITABLE eq false} disabled=disabled {/if} {if $MODULE eq 'Invoice'}style="width: 50px;"{/if}/>
			{if $MODULE eq 'Invoice'}
				<select class="inputElement" id="cargo_measure_invoice{$row_no}" name="cargo_measure_invoice{$row_no}" style="width: 125px; margin-left: 10px; margin-top: 1px;">
					{foreach  item=MEASURE_LIST from=$measure_list}	
					<option value="{$MEASURE_LIST}"  {if $data.$cargo_measure_invoice eq $MEASURE_LIST}selected{/if}>{$MEASURE_LIST}</option>
					{/foreach}
					
				</select>
			{/if}
				 {* data-rule-positive=true panaikinta apsauga ivesti tik teigiama kieki *} 
				 {* data-rule-greater_than_zero=true panaikinta apsauga ivesti didesni kieki uz nuli *}

		{if $PURCHASE_COST_EDITABLE eq false and $MODULE neq 'PurchaseOrder'}
			<input id="{$purchaseCost}" type="hidden" value="{if ((float)$data.$purchaseCost)}{((float)$data.$purchaseCost) / ((float)$data.$qty)}{else}0{/if}" />
            <span style="display:none" class="purchaseCost">0</span>
			<input name="{$purchaseCost}" type="hidden" value="{if $data.$purchaseCost}{$data.$purchaseCost}{else}0{/if}" />
		{/if}
		{if $MARGIN_EDITABLE eq false}
			<input type="hidden" name="{$margin}" value="{if $data.$margin}{$data.$margin}{else}0{/if}"></span>
			<span class="margin pull-right" style="display:none">{if $data.$margin}{$data.$margin}{else}0 {/if} </span>
		{/if}
		{if $MODULE neq 'PurchaseOrder'}
			{* <br>
			<span class="stockAlert redColor {if $data.$qty <= $data.$qtyInStock}hide{/if}" >
				{vtranslate('LBL_STOCK_NOT_ENOUGH',$MODULE)}
				<br>
				{vtranslate('LBL_MAX_QTY_SELECT',$MODULE)}&nbsp;<span class="maxQuantity">{$data.$qtyInStock}</span>
			</span> *}
		{/if}

		
	</td>

	{if $PURCHASE_COST_EDITABLE}
		<td>
			<input id="{$purchaseCost}" type="hidden" value="{if $data.$purchaseCost}{((float)$data.$purchaseCost) / ((float)$data.$qty)}{else}0{/if}" />
			<input name="{$purchaseCost}" type="hidden" value="{if $data.$purchaseCost}{$data.$purchaseCost}{else}0{/if}" />
			<span class="pull-right purchaseCost">{if $data.$purchaseCost}{$data.$purchaseCost}{else}0 {/if} </span>
		</td>
	{/if}

	{if $LIST_PRICE_EDITABLE}
            <!--Logtime-->
            {if $MODULE neq 'SalesOrder'}     
		<td {if $MODULE eq 'PurchaseOrder'}style="width: 220px;"{/if}>
            {else}
		<td style="display:none;">
            {/if}    
			<div>
                            <!--Logtime-->
   {if $MODULE neq 'SalesOrder'} 
				{* <input id="{$listPrice}" name="{$listPrice}" value="{if !empty($data.$listPrice)}{$data.$listPrice}{else}0{/if}" type="text" *}
				<input id="fix-price{$row_no}" name="fix-price{$row_no}" value="{$data.$listPrice}" class="smallInputBox inputElement fix-price"  type="text" 
		{else}
				<input type="hidden" id="{$listPrice}" name="{$listPrice}" value="{if !empty($data.$listPrice)}{$data.$listPrice}{else}0{/if}" type="text"   
		{/if}              
				data-rule-required=true  class="listPrice smallInputBox inputElement" data-is-price-changed="{if $RECORD_ID && $row_no neq 0}true{else}false{/if}" list-info='{if isset($data.$listPrice)}{Zend_Json::encode($listPriceValues)}{/if}' data-base-currency-id="{getProductBaseCurrency($productId, {$entityType})}" />
				&nbsp;

				{* data-rule-positive=true  inpute neleidzia neigiamu skaiciu (-10) *}
				{assign var=PRICEBOOK_MODULE_MODEL value=Vtiger_Module_Model::getInstance('PriceBooks')}
				<!--Logtime-->
                                {if $MODULE neq 'PurchaseOrder'} 
                                    {if $PRICEBOOK_MODULE_MODEL->isPermitted('DetailView') && $MODULE != 'PurchaseOrder'}
                                            <span class="priceBookPopup cursorPointer" data-popup="Popup"  title="{vtranslate('PriceBooks', $MODULE)}" data-module-name="PriceBooks" style="float:left"></span>
                                    {/if}
                                {else}
                                    {if $PRICEBOOK_MODULE_MODEL->isPermitted('DetailView') && $MODULE != 'PurchaseOrder'}
                                            <span class="priceBookPopup cursorPointer" data-popup="Popup"  title="{vtranslate('PriceBooks', $MODULE)}" data-module-name="PriceBooks" style="float:left">{Vtiger_Module_Model::getModuleIconPath('PriceBooks')}</span>
                                    {/if}    
                                {/if}        
			</div>
			<div style="clear:both"></div>
                        
                        <!--Logtime-->       
                        {if $MODULE neq 'SalesOrder'}   
                        
                            {if $ITEM_DISCOUNT_AMOUNT_EDITABLE || $ITEM_DISCOUNT_PERCENT_EDITABLE}
                                    <div>
                                            <span>(-)&nbsp;
                                                    <strong><a href="javascript:void(0)" class="individualDiscount">{vtranslate('LBL_DISCOUNT',$MODULE)}
                                                                    <span class="itemDiscount">
                                                                            {if $ITEM_DISCOUNT_PERCENT_EDITABLE && $data.$discount_type eq 'percentage'}
                                                                                    ({$data.$discount_percent}%)
                                                                            {else if $ITEM_DISCOUNT_AMOUNT_EDITABLE && $data.$discount_type eq 'amount'}
                                                                                    ({$data.$discount_amount})
                                                                            {else}
                                                                                    (0)
                                                                            {/if}
                                                                    </span>
                                                            </a> : </strong>
                                            </span>
                                    </div>                                                               

                                        <div class="discountUI validCheck hide" id="discount_div{$row_no}">
                                                {assign var="DISCOUNT_TYPE" value="zero"}
                                                {if !empty($data.$discount_type)}
                                                        {assign var="DISCOUNT_TYPE" value=$data.$discount_type}
                                                {/if}
                                                <input type="hidden" id="discount_type{$row_no}" name="discount_type{$row_no}" value="{$DISCOUNT_TYPE}" class="discount_type" />
                                                <p class="popover_title hide">
                                                        {vtranslate('LBL_SET_DISCOUNT_FOR',$MODULE)} : <span class="variable">{$data.$productTotal}</span>
                                                </p>
                                                <table width="100%" border="0" cellpadding="5" cellspacing="0" class="table table-nobordered popupTable">
                                                        <!-- TODO : discount price and amount are hide by default we need to check id they are already selected if so we should not hide them  -->
                                                        <tr>
                                                                <td>
                                                                        <input type="radio" name="discount{$row_no}" {$data.$checked_discount_zero} {if empty($data.$discount_type)}checked{/if} class="discounts" data-discount-type="zero" />
                                                                        &nbsp;
                                                                        {vtranslate('LBL_ZERO_DISCOUNT',$MODULE)}
                                                                </td>
                                                                <td>
                                                                        <!-- Make the discount value as zero -->
                                                                        <input type="hidden" class="discountVal" value="0" />
                                                                </td>
                                                        </tr>
                                                        {if $ITEM_DISCOUNT_PERCENT_EDITABLE}
                                                                <tr>
                                                                        <td>
                                                                                <input type="radio" name="discount{$row_no}" {$data.$checked_discount_percent} class="discounts" data-discount-type="percentage" />
                                                                                &nbsp; %
                                                                                {vtranslate('LBL_OF_PRICE',$MODULE)}
                                                                        </td>
                                                                        <td>
                                                                                <span class="pull-right">&nbsp;%</span>
                                                                                <input type="text" data-rule-positive=true data-rule-inventory_percentage=true id="discount_percentage{$row_no}" name="discount_percentage{$row_no}" value="{$data.$discount_percent}" class="discount_percentage span1 pull-right discountVal {if empty($data.$checked_discount_percent)}hide{/if}" />
                                                                        </td>
                                                                </tr>
                                                        {/if}
                                                        {if $ITEM_DISCOUNT_AMOUNT_EDITABLE}
                                                                <tr>
                                                                        <td class="LineItemDirectPriceReduction">
                                                                                <input type="radio" name="discount{$row_no}" {$data.$checked_discount_amount} class="discounts" data-discount-type="amount" />
                                                                                &nbsp;
                                                                                {vtranslate('LBL_DIRECT_PRICE_REDUCTION',$MODULE)}
                                                                        </td>
                                                                        <td>
                                                                                <input type="text" data-rule-positive=true id="discount_amount{$row_no}" name="discount_amount{$row_no}" value="{$data.$discount_amount}" class="span1 pull-right discount_amount discountVal {if empty($data.$checked_discount_amount)}hide{/if}"/>
                                                                        </td>
                                                                </tr>
                                                        {/if}
                                                </table>
                                        </div>
                                    {/if} 


                                    <div style="width:150px;">
                                            <strong>{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE)} :</strong>
                                    </div>
			{/if}

			<div class="individualTaxContainer {if $IS_GROUP_TAX_TYPE}hide{/if}">
				(+)&nbsp;<strong><a href="javascript:void(0)" class="individualTax">{vtranslate('LBL_TAX',$MODULE)} </a> : </strong>
			</div>
			<span class="taxDivContainer">
				<div class="taxUI hide" id="tax_div{$row_no}">
					<p class="popover_title hide">
						{vtranslate('LBL_SET_TAX_FOR',$MODULE)} : <span class="variable">{$data.$totalAfterDiscount}</span>
					</p>
					{if count($data.taxes) > 0}
						<div class="individualTaxDiv">
							<!-- we will form the table with all taxes -->
							<table width="100%" border="0" cellpadding="5" cellspacing="0" class="table table-nobordered popupTable" id="tax_table{$row_no}">
								{foreach key=tax_row_no item=tax_data from=$data.taxes}
									{assign var="taxname" value=$tax_data.taxname|cat:"_percentage"|cat:$row_no}
									{assign var="tax_id_name" value="hidden_tax"|cat:$tax_row_no+1|cat:"_percentage"|cat:$row_no}
									{assign var="taxlabel" value=$tax_data.taxlabel|cat:"_percentage"|cat:$row_no}
									{assign var="popup_tax_rowname" value="popup_tax_row"|cat:$row_no}
									<tr>
										<td>&nbsp;&nbsp;{$tax_data.taxlabel}</td>
										<td style="text-align: right;">
											<input type="text" data-rule-positive=true data-rule-inventory_percentage=true  name="{$taxname}" id="{$taxname}" value="{$tax_data.percentage}" data-compound-on="{if $tax_data.method eq 'Compound'}{Vtiger_Util_Helper::toSafeHTML(Zend_Json::encode($tax_data.compoundon))}{/if}" data-regions-list="{Vtiger_Util_Helper::toSafeHTML(Zend_Json::encode($tax_data.regionsList))}" class="span1 taxPercentage" />&nbsp;%
										</td>
										<td style="text-align: right; padding-right: 10px;">
											<input type="text" name="{$popup_tax_rowname}" class="cursorPointer span1 taxTotal taxTotal{$tax_data.taxid}" value="{$tax_data.amount}" readonly />
										</td>
									</tr>
								{/foreach}
							</table>
						</div>
					{/if}
				</div>
			</span>


		</td>
	{/if}

{if $MODULE eq 'PurchaseOrder'}   
	     <td>
					{if $LINEITEM_FIELDS['region_id'] && $LINEITEM_FIELDS['region_id']->isEditable()}														
									<select class="inputElement region_id" id="region{$row_no}" name="region{$row_no}" style="width: 120px;">
										<option value="0" data-info="{Vtiger_Util_Helper::toSafeHTML(Zend_Json::encode($DEFAULT_TAX_REGION_INFO))}">{vtranslate('LBL_SELECT_OPTION_21_VAT', $MODULE)}</option>
										{foreach key=TAX_REGION_ID item=TAX_REGION_INFO from=$TAX_REGIONS}
											<option {if $data.$region == $TAX_REGION_ID}selected{/if} value="{$TAX_REGION_ID}" data-info='{Vtiger_Util_Helper::toSafeHTML(Zend_Json::encode($TAX_REGION_INFO))}'>{$TAX_REGION_INFO['name']}</option>
										{/foreach}
									</select>
									<input type="hidden" id="prevRegionId{$row_no}" value="{$RECORD->get('region_id')}" />						
							{/if}
				</td>

				<td>
						<input type="hidden" id="vatpriceInp{$row_no}" name="vatpriceInp{$row_no}" value="{if $data.$vat}{$data.$vat}{else}0{/if}"></span>
						<span id="vatprice{$row_no}" class="vatprice pull-right">{if $data.$vat}{$data.$vat}{else}0{/if}</span>
				</td>
  {/if}      
        <!--Logtime-->  
{if $MODULE neq 'SalesOrder'}           
	<td>
		<div id="productTotal{$row_no}" align="right" class="productTotal">{if $data.$productTotal}{$data.$productTotal}{else}0{/if}</div>
	{if $MODULE neq 'PurchaseOrder'}    
		{if $ITEM_DISCOUNT_AMOUNT_EDITABLE || $ITEM_DISCOUNT_PERCENT_EDITABLE}
			<div id="discountTotal{$row_no}" align="right" class="discountTotal">{if $data.$discountTotal}{$data.$discountTotal}{else}0{/if}</div>
			<div id="totalAfterDiscount{$row_no}" align="right" class="totalAfterDiscount">{if $data.$totalAfterDiscount}{$data.$totalAfterDiscount}{else}0{/if}</div>
		{/if}
 {/if}
		<div id="taxTotal{$row_no}" align="right" class="productTaxTotal {if $IS_GROUP_TAX_TYPE}hide{/if}">{if $data.$taxTotal}{$data.$taxTotal}{else}0{/if}</div>
	</td>

	{if $MARGIN_EDITABLE && $PURCHASE_COST_EDITABLE}
		<td>
			<input type="hidden" name="{$margin}" value="{if $data.$margin}{$data.$margin}{else}0{/if}"></span>
		<span class="margin pull-right">{if $data.$margin}{$data.$margin}{else}0{/if}</span>
	</td>
	{/if}

	<td>
		<span id="netPrice{$row_no}" class="pull-right netPrice">{if $data.$netPrice}{$data.$netPrice}{else}0{/if}</span>
		 <input type="hidden" class="netPriceValue" id="netPrice{$row_no}" name="{$netPrice}" value="{if $data.$netPrice}{$data.$netPrice}{else}0{/if}">
	</td>
{/if}  
            <td style="display:none;">
		<div id="productTotal{$row_no}" align="right" class="productTotal">{if $data.$productTotal}{$data.$productTotal}{else}0{/if}</div>
		{if $ITEM_DISCOUNT_AMOUNT_EDITABLE || $ITEM_DISCOUNT_PERCENT_EDITABLE}
			<div id="discountTotal{$row_no}" align="right" class="discountTotal">{if $data.$discountTotal}{$data.$discountTotal}{else}0{/if}</div>
			<div id="totalAfterDiscount{$row_no}" align="right" class="totalAfterDiscount">{if $data.$totalAfterDiscount}{$data.$totalAfterDiscount}{else}0{/if}</div>
		{/if}

		<div id="taxTotal{$row_no}" align="right" class="productTaxTotal {if $IS_GROUP_TAX_TYPE}hide{/if}">{if $data.$taxTotal}{$data.$taxTotal}{else}0{/if}</div>
            </td>

            {if $MARGIN_EDITABLE && $PURCHASE_COST_EDITABLE}
		<td style="display:none;">
			<input type="hidden" name="{$margin}" value="{if $data.$margin}{$data.$margin}{else}0{/if}"></span>
		<span class="margin pull-right">{if $data.$margin}{$data.$margin}{else}0{/if}</span>
            </td>
            {/if}

            <td style="display:none;">
                    <span id="netPriceHidden{$row_no}" class="pull-right netPriceHidden">{if $data.$netPrice}{$data.$netPrice}{else}0{/if}</span>
            </td> 


{/strip}
