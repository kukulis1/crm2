{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}

{strip}
	{assign var="deleted" value="deleted"|cat:$row_no}
	{assign var="image" value="productImage"|cat:$row_no}
	{assign var="purchaseCost" value="purchaseCost"|cat:$row_no}
	{assign var="margin" value="margin"|cat:$row_no}
    {assign var="hdnProductId" value="hdnProductId"|cat:$row_no}
    {assign var="productName" value="productName"|cat:$row_no}
    {assign var="comment" value="comment"|cat:$row_no}
    {assign var="productDescription" value="productDescription"|cat:$row_no}
    {assign var="qtyInStock" value="qtyInStock"|cat:$row_no}
    {assign var="qty" value="qty"|cat:$row_no}
    {assign var="listPrice" value="listPrice"|cat:$row_no}
    {assign var="productTotal" value="productTotal"|cat:$row_no}

    {assign var="salesorderid" value="salesorderid"|cat:$row_no}
    {assign var="shipment_code" value="shipment_code"|cat:$row_no}
    {assign var="bill_country" value="bill_country"|cat:$row_no}
    {assign var="ship_country" value="ship_country"|cat:$row_no}
    {assign var="customer_order_code" value="customer_order_code"|cat:$row_no}

    {assign var="bill_ads" value="bill_ads"|cat:$row_no}
    {assign var="ship_ads" value="ship_ads"|cat:$row_no}


    {assign var="note" value="note"|cat:$row_no}
    {assign var="cargo_wgt" value="cargo_wgt"|cat:$row_no}
    {assign var="volume" value="volume"|cat:$row_no}
    {assign var="measures" value="measures"|cat:$row_no}
    {assign var="cargo_measure_for_invoice" value="cargo_measure_for_invoice"|cat:$row_no}
    {assign var="service_type" value="service_type"|cat:$row_no}

    {assign var="subproduct_ids" value="subproduct_ids"|cat:$row_no}
    {assign var="subprod_names" value="subprod_names"|cat:$row_no}
	{assign var="subprod_qty_list" value="subprod_qty_list"|cat:$row_no}
    {assign var="entityIdentifier" value="entityType"|cat:$row_no}
    {assign var="entityType" value=$data.$entityIdentifier}

    {assign var="discount_type" value="discount_type"|cat:$row_no}
    {assign var="discount_percent" value="discount_percent"|cat:$row_no}
    {assign var="checked_discount_percent" value="checked_discount_percent"|cat:$row_no}
    {assign var="style_discount_percent" value="style_discount_percent"|cat:$row_no}
    {assign var="discount_amount" value="discount_amount"|cat:$row_no}
    {assign var="checked_discount_amount" value="checked_discount_amount"|cat:$row_no}
    {assign var="style_discount_amount" value="style_discount_amount"|cat:$row_no}
    {assign var="checked_discount_zero" value="checked_discount_zero"|cat:$row_no}

    {assign var="discountTotal" value="discountTotal"|cat:$row_no}
    {assign var="totalAfterDiscount" value="totalAfterDiscount"|cat:$row_no}
    {assign var="taxTotal" value="taxTotal"|cat:$row_no}
    {assign var="netPrice" value="netPrice"|cat:$row_no}
    {assign var="cost_center" value="cost_center"|cat:$row_no}
    {assign var="employee" value="employee"|cat:$row_no}
    {assign var="object" value="object"|cat:$row_no}
    {assign var="FINAL" value=$RELATED_PRODUCTS.1.final_details}

	{assign var="productDeleted" value="productDeleted"|cat:$row_no}
	{assign var="productId" value=$data[$hdnProductId]}
	{assign var="listPriceValues" value=Products_Record_Model::getListPriceValues($productId)}

  <td style="text-align:center;">
    <div id="order_number{$row_no}"> 
        {if $data.$hdnProductId != 36641}
            {if $data.$shipment_code}
                {if $data.$salesorderid}
                    <a target="_blank" href="/index.php?module=SalesOrder&view=Detail&record={$data.$salesorderid}">{$data.$shipment_code}</a>{/if}
                {if $data.$customer_order_code} <br> {$data.$customer_order_code}{/if}
            {else}
                ---
            {/if}
            
            {if strtoupper($data.$bill_country) eq 'LTU' && strtoupper($data.$ship_country) eq 'LTU'}{else}<span><i class="fa fa-flag-checkered" title="Užsakymas ne lietuvos teritorijoje" style="color: crimson;"></i></span>{/if}
        {else}
            Paslauga
        {/if}     
    </div>  
  </td>
  <td> {if $data.$hdnProductId != 36641}{$data.$bill_ads}{else}{$data.$service_type}{/if}</td>
  <td>{$data.$ship_ads}</td>
  <td {if !$data.$note}style="text-align:center;"{/if}>{if $data.$note}{$data.$note}{else}---{/if}</td>
  <td>
  <div>{$data.$cost_center}</div>
  <div>{if $data.$object}{$data.$object}{/if}</div>  
  <div>{if $data.$employee} {$data.$employee}{/if}</div> 
 
  </td>

  <td style="text-align:center;">{if $data.$hdnProductId != 36641}{$data.$cargo_measure_for_invoice}{/if}</td>
  <td style="text-align:center;">{if $data.$hdnProductId != 36641}{$data.$cargo_wgt}{/if}</td>
  <td style="text-align:center;">{if $data.$hdnProductId != 36641}{$data.$volume}{/if}</td>


                        {if $LIST_PRICE_VIEWABLE}
                            <td style="white-space: nowrap;">
                                <div>
                                    {$data["listPrice$row_no"]}
                                </div>
                                {if $ITEM_DISCOUNT_AMOUNT_VIEWABLE || $ITEM_DISCOUNT_PERCENT_VIEWABLE}
                                    <div>
                                        {assign var=DISCOUNT_INFO value="{if $data["discount_type$row_no"] == 'amount'} {vtranslate('LBL_DIRECT_AMOUNT_DISCOUNT',$MODULE_NAME)} = {$data["discountTotal$row_no"]}
									{elseif $data["discount_type$row_no"] == 'percentage'} {$data["discount_percent$row_no"]} % {vtranslate('LBL_OF',$MODULE_NAME)} {$data["productTotal$row_no"]} = {$data["discountTotal$row_no"]}
									{/if}"}
                                        (-)&nbsp; <strong><a href="javascript:void(0)" class="individualDiscount inventoryLineItemDetails" tabindex="0" role="tooltip" id ="example" data-toggle="popover" data-trigger="focus" title="{vtranslate('LBL_DISCOUNT',$MODULE_NAME)}" data-content="{$DISCOUNT_INFO}">{vtranslate('LBL_DISCOUNT',$MODULE_NAME)}</a> : </strong>
                                    </div>
                                {/if}
                                <div>
                                    <strong>{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE_NAME)} :</strong>
                                </div>
                                {if $FINAL_DETAILS.taxtype neq 'group'}
                                    <div class="individualTaxContainer">
                                        {assign var=INDIVIDUAL_TAX_INFO value="{vtranslate('LBL_TOTAL_AFTER_DISCOUNT', $MODULE_NAME)} = {$data["totalAfterDiscount$row_no"]}<br /><br />{foreach item=tax_details from=$data['taxes']}{if $LINEITEM_FIELDS["{$tax_details['taxname']}"]}{$tax_details['taxlabel']} : \t{$tax_details['percentage']}%  {vtranslate('LBL_OF',$MODULE_NAME)}  {if $tax_details['method'] eq 'Compound'}({/if}{$data["totalAfterDiscount$row_no"]}{if $tax_details['method'] eq 'Compound'}{foreach item=COMPOUND_TAX_ID from=$tax_details['compoundon']}{if $FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']} + {$FINAL_DETAILS['taxes'][$COMPOUND_TAX_ID]['taxlabel']}{/if}{/foreach}){/if} = {$tax_details['amount']}<br />{/if}{/foreach}<br /><br />{vtranslate('LBL_TOTAL_TAX_AMOUNT',$MODULE_NAME)} = {$data["taxTotal$row_no"]}"}
                                        (+)&nbsp;<strong><a href="javascript:void(0)" class="individualTax inventoryLineItemDetails" tabindex="0" role="tooltip" id="example" title ="{vtranslate('LBL_TAX',$MODULE_NAME)}" data-trigger ="focus" data-toggle ="popover" data-content="{$INDIVIDUAL_TAX_INFO}">{vtranslate('LBL_TAX',$MODULE_NAME)} </a> : </strong>
                                    </div>
                                {/if}
                            </td>
                        {/if}
    
           
	<td><div id="productTotal{$row_no}" align="right" class="productTotal">{if $data.$productTotal}{$data.$productTotal}{else}0{/if}</div>
		{* {if $ITEM_DISCOUNT_AMOUNT_EDITABLE || $ITEM_DISCOUNT_PERCENT_EDITABLE} *}
			<div id="discountTotal{$row_no}" align="right" class="discountTotal">{if $data.$discountTotal}{$data.$discountTotal}{else}0{/if}</div>
			<div id="totalAfterDiscount{$row_no}" align="right" class="totalAfterDiscount">{if $data.$totalAfterDiscount}{$data.$totalAfterDiscount}{else}0{/if}</div>
		{* {/if} *}

		<div id="taxTotal{$row_no}" align="right" class="productTaxTotal {if $IS_GROUP_TAX_TYPE}hide{/if}">{if $data.$taxTotal}{$data.$taxTotal}{else}0{/if}</div>
	</td>

	{if $MARGIN_EDITABLE && $PURCHASE_COST_EDITABLE}
		<td>
			<input type="hidden" name="{$margin}" value="{if $data.$margin}{$data.$margin}{else}0{/if}"></span>
		<span class="margin pull-right">{if $data.$margin}{$data.$margin}{else}0{/if}</span>
	</td>
	{/if}

	<td>
		<span id="netPrice{$row_no}" class="pull-right netPrice">{if $data.$netPrice}{$data.$netPrice}{else}0{/if}</span>
	</td>


{/strip}
