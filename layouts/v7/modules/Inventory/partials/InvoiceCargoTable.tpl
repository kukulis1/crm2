{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*************************************************************************************}


{strip}
{assign var="deleted" value="deleted"|cat:$row_no}
{assign var="image" value="productImage"|cat:$row_no}
{assign var="purchaseCost" value="purchaseCost"|cat:$row_no}
{assign var="margin" value="margin"|cat:$row_no}
{assign var="hdnProductId" value="hdnProductId"|cat:$row_no}
{assign var="productName" value="productName"|cat:$row_no}
{assign var="comment" value="comment"|cat:$row_no}
{assign var="productDescription" value="productDescription"|cat:$row_no}
{assign var="qtyInStock" value="qtyInStock"|cat:$row_no}
{assign var="qty" value="qty"|cat:$row_no}
{assign var="listPrice" value="listPrice"|cat:$row_no}
{assign var="productTotal" value="productTotal"|cat:$row_no}
{assign var="subproduct_ids" value="subproduct_ids"|cat:$row_no}
{assign var="subprod_names" value="subprod_names"|cat:$row_no}
{assign var="subprod_qty_list" value="subprod_qty_list"|cat:$row_no}
{assign var="entityIdentifier" value="entityType"|cat:$row_no}
{assign var="entityType" value=$data.$entityIdentifier}

{assign var="discount_type" value="discount_type"|cat:$row_no}
{assign var="discount_percent" value="discount_percent"|cat:$row_no}
{assign var="checked_discount_percent" value="checked_discount_percent"|cat:$row_no}
{assign var="style_discount_percent" value="style_discount_percent"|cat:$row_no}
{assign var="discount_amount" value="discount_amount"|cat:$row_no}
{assign var="checked_discount_amount" value="checked_discount_amount"|cat:$row_no}
{assign var="style_discount_amount" value="style_discount_amount"|cat:$row_no}
{assign var="checked_discount_zero" value="checked_discount_zero"|cat:$row_no}

{assign var="discountTotal" value="discountTotal"|cat:$row_no}
{assign var="totalAfterDiscount" value="totalAfterDiscount"|cat:$row_no}
{assign var="taxTotal" value="taxTotal"|cat:$row_no}
{assign var="netPrice" value="netPrice"|cat:$row_no}
{assign var="FINAL" value=$RELATED_PRODUCTS.1.final_details}

{assign var="productDeleted" value="productDeleted"|cat:$row_no}
{assign var="productId" value=$data[$hdnProductId]}
{assign var="listPriceValues" value=Products_Record_Model::getListPriceValues($productId)}

{* itoma *}
{assign var="shipment_code" value="shipment_code"|cat:$row_no}
{assign var="bill_ads" value="bill_ads"|cat:$row_no}
{assign var="ship_ads" value="ship_ads"|cat:$row_no}
{assign var="note" value="note"|cat:$row_no}
{assign var="cargo_wgt" value="cargo_wgt"|cat:$row_no}
{assign var="volume" value="volume"|cat:$row_no}
{assign var="measures" value="measures"|cat:$row_no}
{assign var="salesorderid" value="salesorderid"|cat:$row_no}
{assign var="storage" value="storage"|cat:$row_no}
{assign var="cargo_measure_invoice" value="cargo_measure_invoice"|cat:$row_no}
{assign var="quantity" value="quantity"|cat:$row_no}
{assign var="cargo_measure_for_invoice" value="cargo_measure_for_invoice"|cat:$row_no}
{assign var="carg" value="carg"|cat:$row_no}

{assign var="cargo_measure" value="cargo_measure"|cat:$row_no}
{assign var="service_id" value="service_id"|cat:$row_no}
{assign var="productnameDimensions" value="productnameDimensions"|cat:$row_no}
{assign var="service_names" value="service_names"|cat:$row_no}
{assign var="invoice_type" value="invoice_type"|cat:$row_no}
{assign var="cost_center" value=$cost_center}
{assign var="employee" value="employee"|cat:$row_no}
{assign var="object" value="object"|cat:$row_no}
{assign var="costid" value="costid"|cat:$row_no}
<td style="text-align:center;">
        <input checked type="checkbox" name="list" id="listCheckBox{$row_no}"  class="listCheckBox" onclick="disableUncheckFields(event);">
        <input type="hidden" id="services_count{$row_no}" name="services_count{$row_no}" value="0">
        {* <i class="fa fa-trash deleteRow cursorPointer" title="{vtranslate('LBL_DELETE',$MODULE)}" style="display: block;"></i> *}
</td>


<td style="text-align:center; width: 6%;">
        <div class="so_popup" id="order_number{$row_no}">
                {if $entityType eq 'Services'} <span id="so_popup{$row_no}">Paslauga</span><input type="hidden"
                        class="info_popup" data-num data-target> {else}
                {if !empty($data.$invoice_type)}<i class="fa fa-history" title="Yra išankstinė saskaita"
                        style="font-size: 15px;"></i>{/if}
                <button onclick="popupSalesOrder(event);" type="button" id="so_popup{$row_no}"
                        class="btn btn-sm btn-secondary info_popup" data-toggle="modal" data-num
                        data-target="#salesorder" style="background: transparent;">{if empty($data.$shipment_code)}
                        Krovinys{else}{$data.$shipment_code}{/if}{/if}</button>
        </div>
        <input type="hidden" id="{$productName}" name="{$productName}"
                value="{if $entityType eq 'Services'}Services{else}{$data.$productnameDimensions}{/if}"
                class="productName" />
        <input type="hidden" id="{$hdnProductId}" name="{$hdnProductId}" value="{$data.$hdnProductId}"
                class="selectedModuleId" />
        <input type="hidden" id="lineItemType{$row_no}" name="lineItemType{$row_no}" value="Products"
                class="lineItemType" />
        <input type="hidden" id="order_id{$row_no}" name="order_id{$row_no}" value="{$data.$salesorderid}"
                class="order_id" />
        <input type="hidden" id="order_id_check{$row_no}" name="order_id_check{$row_no}" value="{$data.$salesorderid}"
                class="order_id_check" />
         <input type="hidden" id="order_checked{$row_no}" name="order_checked{$row_no}" value="1"
                class="order_checked" />
                
        {if $entityType neq 'Services'}
        <button type="button" class="btn btn-sm btn-secondary addServiceAfterCargo" data-module-name="Services" data-storage="{$data.$storage}"
                data-module-name-type="addServices" style="margin-top: 10px;">Pridėti paslaugą</button>
        {/if}

        <div class="modal fade my_modal" id="salesorder{$row_no}" tabindex="-1" role="dialog"
                aria-labelledby="salesorderLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog2" role="document" style="width:1000px;">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <h5 class="modal-title" id="salesorder">Pardavimo užsakymo nr. <span
                                                        id="salesorderid{$row_no}"></span> informacija</h5>
                                        <button type="button" class="close_btn" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                        </button>
                                </div>
                                <div class="modal-body" style="cursor: move;">
                                        {include file="partials/popupcontent.tpl"|@vtemplate_path:'Inventory'
                                        IGNORE_UI_REGISTRATION=false}
                                </div>
                        </div>
                </div>
        </div>
</td>
<td style="width: 20%">
        {if $entityType neq 'Services'}
        <textarea class="inputElement textAreaElement bill_address" name="bill_address{$row_no}" rows="3" cols="50"
                id="bill_address{$row_no}">{$data.$bill_ads}</textarea>
        <select id="service_select{$row_no}" class="inputElement service_select" aria-invalid="false"
                name="service_select{$row_no}" style="width: 100%; display: none;">
        </select>
        {else}
        <select class="inputElement service_select" aria-invalid="false" name="service_select{$row_no}"
                style="width: 100%;">
                {foreach item=service_name from=$data.$service_names}
                <option {if $data.$service_id eq $service_name['id']}selected{/if} value="{$service_name['id']}">
                        {$service_name['name']}</option>
                {{/foreach}}
        </select>
        <input type="hidden" class="bill_address" name="bill_address{$row_no}" id="bill_address{$row_no}">
        {/if}
</td>

<td style="width: 20%"> {if $entityType neq 'Services'} <textarea class="inputElement textAreaElement ship_address"
                name="ship_address{$row_no}" rows="3" cols="50"
                id="ship_address{$row_no}">{$data.$ship_ads}</textarea>{else} <input type="hidden" class="ship_address"
                name="ship_address{$row_no}" id="ship_address{$row_no}"> {/if}</td>

<td style="width: 15%"> <textarea class="inputElement textAreaElement note_field" name="note_field{$row_no}" rows="3"
                cols="50" id="note_field{$row_no}">{$data.$note}</textarea></td>

<td style="width: 220px;"> 
        <select class="inputElement costcenter" id="costcenter{$row_no}" name="costcenter{$row_no}"
                style="display: block;">
                {* <option value="97380">Transporto paslaugos</option> *}
                {foreach item=COST_LIST from=$cost_center}
                <option value="{$COST_LIST['costid']}" {if $data.$costid eq
                        $COST_LIST['costid']}selected{/if}>{$COST_LIST['costname']} </option> {/foreach} </select> 
                        <div style="display: block; margin-top: 5px;">
                        <input type="text" id="object{$row_no}" name="object{$row_no}" placeholder="Objektas"
                                class="inputElement object" autoComplete="off"
                                value="{if !empty($data.$object)}{$data.$object}{/if}">
                        </div>
                        <div style="display: block; margin-top: 5px;">
                                <input type="text" id="employee{$row_no}" name="employee{$row_no}"
                                        placeholder="Darbuotojas" class="inputElement employee" autoComplete="off"
                                        value="{if !empty($data.$employee)}{$data.$employee}{/if}">
                        </div>
                       
</td>



<td style="text-align:center;">
        <div id="measure{$row_no}" data-toggle="popover{$row_no}" data-measure="{$row_no}" style="cursor:pointer;"
                title="Taros pakeitimas" onclick="tareSelector(event);">
                {if $data.$hdnProductId != 36641}{if
                $data.$cargo_measure_for_invoice}{$data.$cargo_measure_for_invoice}{else}Pridėti krovinį{/if}{/if}</div>
        <input type="hidden" class="cargo_measure_invoice" name="cargo_measure_invoice{$row_no}"
                id="cargo_measure_invoice{$row_no}" value="{$data.$cargo_measure_invoice}">

</td>
<td style="text-align:center;">{if $data.$hdnProductId != 36641}<input class="smallInputBox inputElement cargo_wgt"
                type="text" name="cargo_wgt{$row_no}" id="cargo_wgt{$row_no}" value="{$data.$cargo_wgt}"
                style="width: 55px;">{else} <input type="hidden" class="cargo_wgt" type="text" name="cargo_wgt{$row_no}"
                id="cargo_wgt{$row_no}"> {/if}</td>
<td style="text-align:center;">{if $data.$hdnProductId != 36641}<input class="smallInputBox inputElement cargo_volume"
                type="text" name="cargo_volume{$row_no}" id="cargo_volume{$row_no}" value="{$data.$volume}"
                style="width: 55px;">{else} <input type="hidden" class="cargo_volume" type="text"
                name="cargo_volume{$row_no}" id="cargo_volume{$row_no}"> {/if}</td>

{if $LIST_PRICE_EDITABLE}
<td>
        <div>

                <input id="fix-price{$row_no}" name="fix-price{$row_no}" data-id="{$row_no}" type="text"
                        value="{$data.$listPrice}" class="smallInputBox inputElement fix-price" onchange="checkServiceType(event)"/>
                &nbsp;
                {assign var=PRICEBOOK_MODULE_MODEL value=Vtiger_Module_Model::getInstance('PriceBooks')}

                {if $MODULE neq 'PurchaseOrder'}
                {if $PRICEBOOK_MODULE_MODEL->isPermitted('DetailView') && $MODULE != 'PurchaseOrder'}
                <span class="priceBookPopup cursorPointer" data-popup="Popup"
                        title="{vtranslate('PriceBooks', $MODULE)}" data-module-name="PriceBooks"
                        style="float:left"></span>
                {/if}
                {else}
                {if $PRICEBOOK_MODULE_MODEL->isPermitted('DetailView') && $MODULE != 'PurchaseOrder'}
                <span class="priceBookPopup cursorPointer" data-popup="Popup"
                        title="{vtranslate('PriceBooks', $MODULE)}" data-module-name="PriceBooks"
                        style="float:left">{Vtiger_Module_Model::getModuleIconPath('PriceBooks')}</span>
                {/if}
                {/if}
        </div>
        <div style="clear:both"></div>


        {if $MODULE neq 'SalesOrder'}

        {if $ITEM_DISCOUNT_AMOUNT_EDITABLE || $ITEM_DISCOUNT_PERCENT_EDITABLE}
        <div>
                <span>(-)&nbsp;
                        <strong><a href="javascript:void(0)"
                                        class="individualDiscount">{vtranslate('LBL_DISCOUNT',$MODULE)}
                                        <span class="itemDiscount">
                                                {if $ITEM_DISCOUNT_PERCENT_EDITABLE && $data.$discount_type eq
                                                'percentage'}
                                                ({$data.$discount_percent}%)
                                                {else if $ITEM_DISCOUNT_AMOUNT_EDITABLE && $data.$discount_type eq
                                                'amount'}
                                                ({$data.$discount_amount})
                                                {else}
                                                (0)
                                                {/if}
                                        </span>
                                </a> : </strong>
                </span>
        </div>

        <div class="discountUI validCheck hide" id="discount_div{$row_no}">
                {assign var="DISCOUNT_TYPE" value="zero"}
                {if !empty($data.$discount_type)}
                {assign var="DISCOUNT_TYPE" value=$data.$discount_type}
                {/if}
                <input type="hidden" id="discount_type{$row_no}" name="discount_type{$row_no}" value="{$DISCOUNT_TYPE}"
                        class="discount_type" />
                <p class="popover_title hide">
                        {vtranslate('LBL_SET_DISCOUNT_FOR',$MODULE)} : <span
                                class="variable">{$data.$productTotal}</span>
                </p>
                <table width="100%" border="0" cellpadding="5" cellspacing="0"
                        class="table table-nobordered popupTable">
                        <!-- TODO : discount price and amount are hide by default we need to check id they are already selected if so we should not hide them  -->
                        <tr>
                                <td>
                                        <input type="radio" name="discount{$row_no}" {$data.$checked_discount_zero} {if
                                                empty($data.$discount_type)}checked{/if} class="discounts"
                                                data-discount-type="zero" />
                                        &nbsp;
                                        {vtranslate('LBL_ZERO_DISCOUNT',$MODULE)}
                                </td>
                                <td>
                                        <!-- Make the discount value as zero -->
                                        <input type="hidden" class="discountVal" value="0" />
                                </td>
                        </tr>
                        {if $ITEM_DISCOUNT_PERCENT_EDITABLE}
                        <tr>
                                <td>
                                        <input type="radio" name="discount{$row_no}" {$data.$checked_discount_percent}
                                                class="discounts" data-discount-type="percentage" />
                                        &nbsp; %
                                        {vtranslate('LBL_OF_PRICE',$MODULE)}
                                </td>
                                <td>
                                        <span class="pull-right">&nbsp;%</span>
                                        <input type="text" data-rule-positive=true data-rule-inventory_percentage=true
                                                id="discount_percentage{$row_no}" name="discount_percentage{$row_no}"
                                                value="{$data.$discount_percent}"
                                                class="discount_percentage span1 pull-right discountVal {if empty($data.$checked_discount_percent)}hide{/if}" />
                                </td>
                        </tr>
                        {/if}
                        {if $ITEM_DISCOUNT_AMOUNT_EDITABLE}
                        <tr>
                                <td class="LineItemDirectPriceReduction">
                                        <input type="radio" name="discount{$row_no}" {$data.$checked_discount_amount}
                                                class="discounts" data-discount-type="amount" />
                                        &nbsp;
                                        {vtranslate('LBL_DIRECT_PRICE_REDUCTION',$MODULE)}
                                </td>
                                <td>
                                        <input type="text" data-rule-positive=true id="discount_amount{$row_no}"
                                                name="discount_amount{$row_no}" value="{$data.$discount_amount}"
                                                class="span1 pull-right discount_amount discountVal {if empty($data.$checked_discount_amount)}hide{/if}" />
                                </td>
                        </tr>
                        {/if}
                </table>
        </div>
        {/if}

        <div style="width:150px;">
                <strong>{vtranslate('LBL_TOTAL_AFTER_DISCOUNT',$MODULE)} :</strong>
        </div>
        {/if}

        <div class="individualTaxContainer {if $IS_GROUP_TAX_TYPE}hide{/if}">
                (+)&nbsp;<strong><a href="javascript:void(0)" class="individualTax">{vtranslate('LBL_TAX',$MODULE)} </a>
                        : </strong>
        </div>
        <span class="taxDivContainer">
                <div class="taxUI hide" id="tax_div{$row_no}">
                        <p class="popover_title hide">
                                {vtranslate('LBL_SET_TAX_FOR',$MODULE)} : <span
                                        class="variable">{$data.$totalAfterDiscount}</span>
                        </p>
                        {if count($data.taxes) > 0}
                        <div class="individualTaxDiv">
                                <!-- we will form the table with all taxes -->
                                <table width="100%" border="0" cellpadding="5" cellspacing="0"
                                        class="table table-nobordered popupTable" id="tax_table{$row_no}">
                                        {foreach key=tax_row_no item=tax_data from=$data.taxes}
                                        {assign var="taxname" value=$tax_data.taxname|cat:"_percentage"|cat:$row_no}
                                        {assign var="tax_id_name"
                                        value="hidden_tax"|cat:$tax_row_no+1|cat:"_percentage"|cat:$row_no}
                                        {assign var="taxlabel" value=$tax_data.taxlabel|cat:"_percentage"|cat:$row_no}
                                        {assign var="popup_tax_rowname" value="popup_tax_row"|cat:$row_no}
                                        <tr>
                                                <td>&nbsp;&nbsp;{$tax_data.taxlabel}</td>
                                                <td style="text-align: right;">
                                                        <input type="text" data-rule-positive=true
                                                                data-rule-inventory_percentage=true name="{$taxname}"
                                                                id="{$taxname}" value="{$tax_data.percentage}"
                                                                data-compound-on="{if $tax_data.method eq 'Compound'}{Vtiger_Util_Helper::toSafeHTML(Zend_Json::encode($tax_data.compoundon))}{/if}"
                                                                data-regions-list="{Vtiger_Util_Helper::toSafeHTML(Zend_Json::encode($tax_data.regionsList))}"
                                                                class="span1 taxPercentage" />&nbsp;%
                                                </td>
                                                <td style="text-align: right; padding-right: 10px;">
                                                        <input type="text" name="{$popup_tax_rowname}"
                                                                class="cursorPointer span1 taxTotal taxTotal{$tax_data.taxid}"
                                                                value="{$tax_data.amount}" readonly />
                                                </td>
                                        </tr>
                                        {/foreach}
                                </table>
                        </div>
                        {/if}
                </div>
        </span>
</td>
{/if}


<td>
        <div id="productTotal{$row_no}" align="right" class="productTotal">{if
                $data.$productTotal}{$data.$productTotal}{else}0{/if}</div>
        {if $ITEM_DISCOUNT_AMOUNT_EDITABLE || $ITEM_DISCOUNT_PERCENT_EDITABLE}
        <div id="discountTotal{$row_no}" align="right" class="discountTotal">{if
                $data.$discountTotal}{$data.$discountTotal}{else}0{/if}</div>
        <div id="totalAfterDiscount{$row_no}" align="right" class="totalAfterDiscount">{if
                $data.$totalAfterDiscount}{$data.$totalAfterDiscount}{else}0{/if}</div>
        {/if}

        <div id="taxTotal{$row_no}" align="right" class="productTaxTotal {if $IS_GROUP_TAX_TYPE}hide{/if}">{if
                $data.$taxTotal}{$data.$taxTotal}{else}0{/if}</div>
</td>

{if $MARGIN_EDITABLE && $PURCHASE_COST_EDITABLE}
<td>
        <input type="hidden" name="{$margin}" value="{if $data.$margin}{$data.$margin}{else}0{/if}"></span>
        <span class="margin pull-right">{if $data.$margin}{$data.$margin}{else}0{/if}</span>
</td>
{/if}

<td>
        <span id="netPrice{$row_no}" class="pull-right netPrice">{if $data.$netPrice}{$data.$netPrice}{else}0{/if}</span>
        <input type="hidden" class="netPriceValue" id="netPrice{$row_no}" name="{$netPrice}" value="{if $data.$netPrice}{$data.$netPrice}{else}0{/if}">
</td>


{/strip}