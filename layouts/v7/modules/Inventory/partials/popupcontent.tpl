
<div class="block">
  <div>
    <h4 class=" maxWidth50">Pardavimo užsakymo informacija</h4>
  </div>
  <hr>
  <div class="detailViewContainer">
    <table class="table detailview-table no-border">
      <tbody>
        <tr>
          <td class="fieldLabel " id="SalesOrder_detailView_fieldLabel_cf_855">
          <span class="muted">Užsakymo tipas</span>
          </td>
          <td class="fieldValue" id="SalesOrder_detailView_fieldValue_cf_855">
              <span id="type{$row_no}" style="background-color: #ffffff; line-height:15px; color: black;">
               
              </span>
            </span>
            </td>
          <td class="fieldLabel  " id="SalesOrder_detailView_fieldLabel_sostatus">
          <span  class="muted">Būsena</span>
          </td>

          <td class="fieldValue">
              <span id="status{$row_no}" class="muted" style="background-color: #ffffff; line-height:15px; color: black;">              
              </span>            
          </td>
        </tr>
        <tr>
          <td class="fieldLabel  " id="SalesOrder_detailView_fieldLabel_account_id">
          <span class="muted">Kliento pavadinimas</span>
          </td>

          <td class="fieldValue">
            <span id="client{$row_no}"></span>            
           </td>

          <td class="fieldLabel">
            <span class="muted">Priskirta</span>
          </td>
          <td class="fieldValue">
          <span id="assigned{$row_no}">
           
            </span>
          </td>
        </tr>
        <tr>
          <td class="fieldLabel">
          <span class="muted">Pakrovimo data nuo</span>
          </td>
          <td class="fieldValue">
          <span id="load_date{$row_no}">            
            </span>
          </td>
          <td class="fieldLabel ">
          <span class="muted">Iškrovimo data nuo</span>
          </td>
          <td class="fieldValue">
          <span id="unload_date{$row_no}">
             
            </span></td>
        </tr>
        <tr>
          <td class="fieldLabel ">
          <span class="muted">Siuntėjas</span>
          </td>
          <td class="fieldValue">
          <span id="sender{$row_no}">
            
            </span>
            </td>
          <td class="fieldLabel ">
          <span class="muted">Gavėjas</span>
          </td>
          <td class="fieldValue">
          <span id="receiver{$row_no}">
              
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel">
          <span class="muted">Siuntėjo kodas</span>
          </td>
          <td class="fieldValue">
          <span id="sender_code{$row_no}">
            
            </span>
          </td>
          <td class="fieldLabel">
          <span class="muted">Gavėjo kodas</span>
          </td>
          <td class="fieldValue">
           <span id="receiver_code{$row_no}">
              
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel">
          <span class="muted">Siuntėjo kontaktas</span>
          </td>
          <td class="fieldValue">
          <span id="sender_contact{$row_no}"></span>          
          </td>
          <td class="fieldLabel">
          <span class="muted">Gavėjo kontaktas</span>
          </td>
          <td class="fieldValue">
          <span id="receiver_contact{$row_no}"></span>
          </td>
        </tr>
        <tr>
          <td class="fieldLabel">
          <span class="muted">Pakrovimo adresas</span>
          </td>
          <td class="fieldValue">
          <span id="load_address{$row_no}">
            
            </span>
          </td>
          <td class="fieldLabel"><span class="muted">Iškrovimo adresas</span>
          </td>
          <td class="fieldValue">
          <span id="unload_address{$row_no}">
             
            </span>
          </td>
        </tr>
        <tr>
          <td class="fieldLabel">
          <span class="muted">Pakrovimo miestas</span>
          </td>
          <td class="fieldValue">
          <span id="load_city{$row_no}">
             
            </span>
          </td>
          <td class="fieldLabel">
          <span  class="muted">Iškrovimo miestas</span>
          </td>
          <td class="fieldValue">
          <span id="unload_city{$row_no}">
            
            </span>
          </td>
        </tr>
        <tr>
          <td class="fieldLabel">
          <span class="muted">Pakrovimo pašto kodas</span>
          </td>
          <td class="fieldValue">
          <span id="load_code{$row_no}">
             
            </span>
          </td>
          <td class="fieldLabel">
          <span class="muted">Iškrovimo pašto kodas</span>
          </td>
          <td class="fieldValue">
          <span id="unload_code{$row_no}">
             
            </span>
          </td>
        </tr>
        <tr>
          <td class="fieldLabel">
          <span class="muted">Pakrovimo šalis</span>
          </td>
          <td class="fieldValue">
          <span id="load_country{$row_no}">
              
            </span>
          </td>
          <td class="fieldLabel">
          <span  class="muted">Iškrovimo šalis</span>
          </td>
          <td class="fieldValue">
          <span id="unload_country{$row_no}">
            
            </span>
          </td>
        </tr>
              
        <tr>
          <td class="fieldLabel"><span
              class="muted">Pardavimo užsakymo Nr.</span></td>
          <td class="fieldValue">
          <span id="order_no{$row_no}">
             
            </span></td>
          <td class="fieldLabel"><span
              class="muted">Source</span>
              </td>
          <td class="fieldValue">
          <span id="source{$row_no}">
             
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel"><span
              class="muted">Pakeitimo laikas:</span>
              </td>
          <td class="fieldValue">
          <span id="updated_time{$row_no}">
             
            </span>
            </td>
          <td class="fieldLabel" ><span
              class="muted">Sukūrimo laikas</span>
              </td>
          <td class="fieldValue">
          <span id="created_time{$row_no}">
             
            </span>
            </td>
        </tr>           
        <tr>
          <td class="fieldLabel" style="width:8%">
          <span class="muted">Pastabos</span>
          </td>
          <td class="fieldValue">
          <span id="notes{$row_no}">
            </span>
            </td>
          <td class="fieldLabel" i><span
              class="muted">Krovos darbai</span>
              </td>
          <td class="fieldValue">
          <span id="stevedoring{$row_no}">
             
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel"><span
              class="muted">Termo režimas</span>
              </td>
          <td class="fieldValue">
          <span id="termo{$row_no}">
            
            </span>
            </td>
          <td class="fieldLabel"><span
              class="muted">Kilometražas</span>
              </td>
          <td class="fieldValue">
          <span id="km{$row_no}">
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel" id="SalesOrder_detailView_fieldLabel_cf_932"><span
              class="muted">Sugaištas laikas</span>
          </td>
          <td class="fieldValue">
          <span id="working_time{$row_no}">
            </span>
            </td>
          <td class="fieldLabel"><span
              class="muted">Siuntos Nr.</span>
          </td>
          <td class="fieldValue">
          <span id="shipment{$row_no}">
            
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel"><span
              class="muted">Kainoraštis</span>
              </td>
          <td class="fieldValue">
          <span id="pricebook{$row_no}">
             
            </span>
          </td>
          <td class="fieldLabel"><span
              class="muted">Užsakytas, kg ir matmenys</span>
              </td>
          <td class="fieldValue">
          <span id="ordered{$row_no}">
             
            </span>
          </td>
        </tr>
        <tr>
          <td class="fieldLabel"><span
              class="muted">Užsakytas, m3</span>
              </td>
          <td class="fieldValue">
          <span id="ordered_m3{$row_no}">
            
            </span>
            </td>
          <td class="fieldLabel"><span
              class="muted">Faktas, kg ir matmenys</span></td>
          <td class="fieldValue">
          <span id="revised{$row_no}">
              
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel"><span
              class="muted">Faktas, m3</span>
              </td>
          <td class="fieldValue">
          <span id="revised_m3{$row_no}">
              
            </span>
            </td>
          <td class="fieldLabel"><span
              class="muted">Sutarta kaina, Eur</span>
              </td>
          <td class="fieldValue">
          <span id="agreed_price{$row_no}">
            </span>
            </td>
        </tr>
        <tr>
          <td class="fieldLabel"><span
              class="muted">Kainyno kaina, Eur</span>
              </td>
          <td class="fieldValue">
          <span id="pricebook_price{$row_no}">
              
            </span>
            </td>
          <td class="fieldLabel"><span
              class="muted">Sutartos kainos aprašymas</span>
              </td>
          <td class="fieldValue">
          <span id="agreed_price_desc{$row_no}" >
            </span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
  <hr>
  <div class="blockData">
    <table class="table detailview-table no-border">
      <tbody>
        <tr>
          <td class="fieldLabel"><span
              class="muted">Pastaba</span>
              </td>
          <td class="fieldValue">
          <span id="note{$row_no}">
            </span>
            </td>
          <td class="fieldLabel "></td>
          <td class=""></td>
        </tr>
      </tbody>
    </table>
  </div>
</div><br>

<div class="details block">
  <div class="lineItemTableDiv" style="overflow-x: unset;">
    <table class="table table-bordered lineItemsTable" style="margin-top:15px">
    <thead>
     <tr>
          <td class="lineItemFieldName">
            <strong>Krovinys: SVORIS ILGISxPLOTISxAUKŠTIS</strong>
          </td>
          <td class="lineItemFieldName">
            <strong>Tara</strong>
            </td>
          <td class="lineItemFieldName">
            <strong>Kiekis</strong>
          </td>
        </tr>      
    </thead>
      <tbody id="dimensions{$row_no}">       
      </tbody>
    </table>
  </div>
  <table class="table table-bordered lineItemsTable">
    <tbody>
      <tr>
        <td width="83%">
          <div align="right" class="paid_price">
            <strong>Mokama kaina</strong>
          </div>
        </td>
        <td>
          <div id="grandTotalPop{$row_no}" align="right"></div>
        </td>
      </tr>
    </tbody>
  </table>
</div>