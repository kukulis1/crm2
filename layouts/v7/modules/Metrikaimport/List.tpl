<link rel="stylesheet" href="layouts/v7/modules/Metrikaimport/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Metrikaimport/resources/main2.js"></script>

{assign var=ORDERS value=$orders}


<div id="table-content" class="table-container">
    <form name='list' id='submit-invoice' action='' onsubmit="return false;">
    	<table class="table listview-table">
      <tr>
        <td></td>
        <td><h4>Pasirinkite datos rėžius ARBA surašykite užsakymo numerius</h4></td>
        <td></td>
      </tr>
      </table>
			<table class="table listview-table">
				<thead>
					<tr>
            <th style="width: 30% !important;"></th>
            <th>Nuo</th>
             <th></th>
            <th>Iki</th>  
            <th style="width: 30% !important;"></th>         
          </tr>	
				</thead>     
				<tbody>    
          <tr>
              <td style="width: 30%;"></td>
              <td><input type="text" id="from" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></td>
               <td></td>
              <td><input type="text" id="to" class="listSearchContributor inputElement search_date search_input" data-fieldtype="date" data-date-format="yyyy-mm-dd" value="" autocomplete="off"></td>   
              <td style="width: 30%;"></td>
          </tr>	        
				</tbody>
			</table> 
      	<table class="table listview-table">
          <tr>
            <td>  
            <div id="wait" style="display:none;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>
            <div id="progres"></div>
          
            <div id="import_done" style="display:none;padding:2px;z-index: 1001;">
                 <div style="border:1px solid black; background: white; padding: 50px 200px;"><h3>Užsakymai įkelti</h3></div>
            </div>

           <span><h4>Užsakymo numeris pvz. 134-6192-2</h4> </span>
              <textarea id="shipment_code" cols="123" rows="10" placeholder="Užsakymo numerius rašykite atskirtus kableliu pvz. 134-6192-1, 134-6192-2, 134-6192-3 ..."></textarea>
            </td>
          </tr>           
     	</table> 
       <table class="table listview-table">
        <tr>
          <td style="width: 30%;"></td>
          <td>
            <button type="button" class="btn btn-success btn-lg btn-block" onclick="importOrders();">Įkelti užsakymus</button>
          </td>
          <td style="width: 30%;"></td>
        </tr>
       </table>

  
       
</form>



</div>

