/* ********************************************************************************
 * The content of this file is subject to the Notifications ("License");
 * You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is VTExperts.com
 * Portions created by VTExperts.com. are Copyright(C) VTExperts.com.
 * All Rights Reserved.
 * ****************************************************************************** */

/** @class NotificationsJS */

Vtiger.Class("NotificationsJS", {}, {
    addHeaderIcon: function () {
        var thisInstance = this;
        var vtversion = thisInstance.getVtigerVersion();

        var headerLinksBig = jQuery('#menubar_quickCreate').closest('li');
        var headerIcon = '<li>' +
            '<div id="notificationContainer" class="dropdown" ' + ((vtversion != '7.3' && vtversion != '7.3.0') ? 'style="margin-top: 15px;"' : '') + ' class="">' +
            '<a href="#" id="headerNotification" class="dropdown-toggle" >' +
            '<span class="vicon vicon-bell" aria-hidden="true" title="Notifications"></span>' +
            '<strong class="notification_count">?</strong></a>' +
            '<div id="headerNotificationList" class="dropdown-menu" role="menu">' +
            '</div></li>';
        headerLinksBig.before(headerIcon);

        jQuery('#headerNotification .vicon-bell').on('click', function (e) {
            jQuery('#notificationContainer').toggleClass('open');
        });
        jQuery('body').on('click', function (e) {
            if (!jQuery('#notificationContainer').is(e.target) && jQuery('#notificationContainer').has(e.target).length === 0 && jQuery('.open').has(e.target).length === 0) {
                jQuery('#notificationContainer').removeClass('open');
            }
        });

        thisInstance.refresh();

        // Redirect link
        jQuery('#headerNotificationList').on('click', '.notification_link .notification_full_name', function (event) {
            var currentTarget = jQuery(event.currentTarget);
            var notificationLink = currentTarget.closest('.notification_link');
            window.open(notificationLink.data('href'), '_blank');
            window.location.href = notificationLink.data('href');
        });

        //  jQuery('#headerNotificationListForPopUp').on('click', '.notification_link .notification_full_name', function (event) {
        //     var currentTarget = jQuery(event.currentTarget);
        //     var notificationLink = currentTarget.closest('.notification_link');
        //     window.open(notificationLink.data('href'), '_blank');            
        // });

        jQuery('#headerNotification').on('click', function () {
            jQuery('#headerNotificationListForPopUp').css('display', 'none');
        });

    },
    refreshNotifications: function () {
        setInterval(() => {
            this.refresh();
        }, 30000);
    },
    refresh: function () {
        var instance = new NotificationsJS();
        $('#headerNotificationListForPopUp').remove();
        var notificationList = jQuery('#headerNotificationList');
        var headerNotificationListForPopUp = `<div id="headerNotificationListForPopUp" style="display:none;"></div>`;
        jQuery('#notificationContainer').append(headerNotificationListForPopUp);
        let user_id = _USERMETA.id;

        //// Init total notice
        var params = {
            'module': 'Notifications',
            'action': 'ActionAjax',
            'mode': 'getNotifications',
            'user_id': user_id
        };
        $.ajax({
            type: "POST",
            url: "modules/Notification/ajax/getNotifications.php",
            data: params,
            dataType: "JSON",
            success: function (response) {
                if (response.success == true) {
                    // console.log(response.result.query);
                    notificationList.empty();

                    var count = response.result.count;
                    if(count > 99){
                    	jQuery('.notification_count').css('height','25px');
                    	jQuery('.notification_count').css('width','25px');
                    	jQuery('.notification_count').css('padding-top','3px');
                    }
                    jQuery('.notification_count').html(count);

                    if (count == 0) {
                        // Empty list
                        notificationList.remove();
                        return;
                    }

                    var items = response.result.items;
                    var popupitems = response.result.popupitems;
                    var item = null;
                    var popupitem = null;
                    var listItem = '';
                    var listItemPopUp = '';
                    var itemLength = items.length;
                    var limitDivide = itemLength - 2;

                    // for (var k in items) {
                    for (var i = 0; i < itemLength; i++) {
                        item = items[i];

                        var divider = '';
                        if (i >= 0 && i <= limitDivide) {
                            divider = '<div class="divider">&nbsp;</div>';
                        }

                        jQuery('#headerNotificationList').append(`
                            <li style="margin: 0.5rem;">
                                <div class="notification-container" data-follow="${item.follow}" data-id="${item.id}">
                                    <i class="icon icon-ok" onclick="return clickToOk(this);" title="Supratau">
                                        <img src="layouts/v7/modules/Notification/resources/img/icon-ok.png" alt="Supratau">
                                    </i>
                                    <div class="notification_detail" onclick="window.open('${item.link}', '_blank');" style="cursor: pointer;">
                                        <span class="notification_full_name" title="${item.full_name}">${item.full_name}&nbsp;</span>
                                        <span class="notification_description" title="${item.accountname}">${item.accountname}&nbsp;</span>
                                        <span class="notification_description" title="${item.description}">${item.description}&nbsp;</span>
                                        <span class="notification_createdtime" title="${item.createdtime}">${item.createdtime}&nbsp;</span>
                                    </div>
                                     <i class="icon icon-pp" onclick="return clickToPP(this);" title="Priminti vėliau">
                                        <img src="layouts/v7/modules/Notification/resources/img/icon-pp.png" alt="Priminti vėliau">
                                    </i>
                                    <div class="clearfix"></div>
                                </div>
                                ${divider} 
                            </li>
                        `);

                    }
                             
                    if (popupitems != undefined) {
                        var limitDivide2 = popupitems.length - 2;
                        for (var e = 0; e < popupitems.length; e++) {
                            popupitem = popupitems[e];
                            var divider2 = '';
                            if (e >= 0 && e <= limitDivide2) {
                                divider2 = '<div class="divider2">&nbsp;</div>';
                            }

                            jQuery('#headerNotificationListForPopUp').append(`
                                <li style="margin: 0.5rem;">
                                    <div class="notification-container" data-follow="${popupitem.follow}" data-id="${popupitem.id}">           
                                        <i class="icon icon-ok" onclick="return clickToOk(this);" title="Supratau"> 
                                            <img src="layouts/v7/modules/Notification/resources/img/icon-ok.png" alt="Supratau">
                                        </i>          
                                        <div class="notification_detail" onclick="window.open('${popupitem.link}', '_blank');" style="cursor: pointer;">               
                                            <span class="notification_full_name" title="${popupitem.full_name}">${popupitem.full_name}&nbsp;</span>              
                                            <span class="notification_description" title="${popupitem.accountname}">${popupitem.accountname}&nbsp;</span>
                                            <span class="notification_description" title="${popupitem.description}">${popupitem.description}&nbsp;</span>              
                                            <span class="notification_createdtime" title="${popupitem.createdtime}">${popupitem.createdtime}&nbsp;</span>           
                                        </div>           
                                        <i class="icon icon-pp" onclick="return clickToPP(this);" title="Priminti vėliau">
                                            <img src="layouts/v7/modules/Notification/resources/img/icon-pp.png" alt="Priminti vėliau">
                                        </i>          
                                        <div class="clearfix"></div>       
                                    </div>   
                                    ${divider2}
                                </li>
                            `);


                        }

                        // jQuery('#notificationContainer').removeClass('open');
                        if (!$('#notificationContainer').hasClass('open')) {
                            setTimeout(() => {
                                jQuery('#headerNotificationListForPopUp').fadeIn('fast', 'linear');
                                // instance.removePopUp(5000);
                            }, 500);
                        }
                    }
                } else {
                    jQuery('.notification_count').html(0);
                    notificationList.remove();
                }
            }
        });
    },
    removePopUp: function (time) {
        setTimeout(() => {
            $('#headerNotificationListForPopUp').fadeOut("slow", "swing", function () {
                $('#headerNotificationListForPopUp').remove();
            });
        }, time);
    },
    /**
     * Fn - updateTotalCounter
     *
     * @param notificationLink
     * @param notificationList
     * @param notificationCounter
     */
    updateTotalCounter: function (notificationLink, notificationList, notificationCounter) {
        notificationLink.closest('li').addClass('hide');
        // Remove last divide
        notificationList.find('li:not(.hide)').filter(':last').find('.divider').remove();

        // Update counter
        var currentTotal = notificationCounter.text();
        currentTotal = (currentTotal) ? parseInt(currentTotal) : 0;
        var total = currentTotal - 1;
        total = (total > 0) ? total : 0;
        notificationCounter.text(total);

        if (total == 0) {
            // Remove empty list
            notificationList.remove();
        }
    },
    getVtigerVersion: function () {
        var version = '';
        var scripts = document.getElementsByTagName("script")
        for (var i = 0; i < scripts.length; ++i) {
            var src = scripts[i].src;
            if (src.indexOf('.js?v=') > -1) {
                var versionTmp = src.split('js?v=');
                version = versionTmp[1];
                break;
            } else if (src.indexOf('.js?&v=') > -1) {
                var versionTmp = src.split('.js?&v=');
                version = versionTmp[1];
                break;
            }
        }
        return version;
    },
    /**
     * Fn - registerEvents
     */
    registerEvents: function () {
        var thisInstance = this;
        thisInstance.addHeaderIcon();
        thisInstance.refreshNotifications();
    }
});

//On Page Load
jQuery(document).ready(function () {
    setTimeout(function () {
        var instance = new NotificationsJS();
        instance.registerEvents();
    }, 2000);
});

function clickToOk(btnOK) {
    var notificationContainer = jQuery('#headerNotification');
    var notificationList = jQuery('#headerNotificationList');
    var notificationCounter = notificationContainer.find('.notification_count');
    var currentTarget = jQuery(btnOK);
    var notificationLink = currentTarget.closest('.notification-container');
    var id = notificationLink.data('id');
    var follow = notificationLink.data('follow');
    let user_id = _USERMETA.id;

    // Mark notification read
    var params = {
        'module': 'Notifications',
        'action': 'ActionAjax',
        'mode': 'markNotificationRead',
        'record': id,
        'user_id': user_id,
        'follow': follow
    };
    var instance = new NotificationsJS();

    $.ajax({
        type: "POST",
        url: "modules/Notification/ajax/setSeenNotifications.php",
        data: params,
        dataType: "JSON",
        success: function (response) {
            if (response.success == true) {
                instance.updateTotalCounter(notificationLink, notificationList, notificationCounter);
                app.helper.showSuccessNotification({ message: 'Pranešimas pažymėtas kaip matytas' }, { offset: { y: 450 } });
            } else {
                app.helper.showErrorNotification({ "message": response.error.message });
            }
        }
    });

    return false;
}
function clickToPP(btnPP) {
    var notificationContainer = jQuery('#headerNotification');
    var notificationList = jQuery('#headerNotificationList');
    var notificationCounter = notificationContainer.find('.notification_count');
    var currentTarget = jQuery(btnPP);
    var notificationLink = currentTarget.closest('.notification-container');
    var instance = new NotificationsJS();
    var id = notificationLink.data('id');
    var follow = notificationLink.data('follow');
    let user_id = _USERMETA.id;

    // Mark notification snoose
    var params = {
        'module': 'Notifications',
        'action': 'ActionAjax',
        'mode': 'markNotificationSnoose',
        'record': id,
        'user_id': user_id,
        'follow': follow
    };

    $.ajax({
        type: "POST",
        url: "modules/Notification/ajax/setSnooseNotifications.php",
        data: params,
        dataType: "JSON",
        success: function (response) {
            if (response.success == true) {
                instance.updateTotalCounter(notificationLink, notificationList, notificationCounter);
                app.helper.showSuccessNotification({ message: 'Pranešimas bus primintas po 15 min.' }, { offset: { y: 450 } });
            } else {
                app.helper.showErrorNotification({ "message": response.error.message });
            }
        }
    });
    return false;
}