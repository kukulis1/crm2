$(document).ready(function () {
  let url = new URLSearchParams(window.location.search);
  FieldsCheker(); 
  totalRecords();
  pageUp(url);
  nextPageButton(url);
  previousPageButton(url);
  exportButton();
});

$(document).ajaxComplete(function () {
  exportButton();
  FieldsCheker();
  pageUp(url);
  nextPageButton(url);
  previousPageButton(url);
  totalRecords();  
});

function exportButton(){
  $('#exportPaymentBtn').unbind().on('click', function () {
    exportPaymentBtn();
    showErrors('errors');
    removeExportedInvoices();
  });
}

function nextPageButton(url){
  $('#NextPageButton').unbind().on('click', function(){
    goToPage(+1,url);
  });
}

function previousPageButton(url){
  $('#PreviousPageButton').unbind().on('click', function(){
    goToPage(-1,url);
  });
}

function totalRecords(){
  $('.totalNumberOfRecords3').on('click', function () {
    $('.showTotalCountIcon3').addClass('hide');
  
    setTimeout(function () {
      $('#countShow').removeClass('hide');
    }, 1000);
  });
}

function pageUp(url){
  $('#PageJump').on('click', function(){
    if($(this).find('[aria-expanded="true"]')){
      $.ajax({
        type: "POST",
        url: "modules/Exportpurchaseinvoices/ajax/calculatePages.php",
        data: {search:url.get('search_params')},
        success: function (number) {             
            $('.totalPageCount').html(number);
        }
      }); 
    }    
  });
}

function goToPage(num,url){ 
  let module = url.get('module');
  let parent = url.get('parent');
  let page = parseInt(url.get('page'))+num;
  let view = url.get('view');
  let search_params = url.get('search_params');
  window.location.href = `index.php?module=${module}&parent=${parent}&page=${page}&view=${view}&search_params=${search_params}`; 
}

function checkAllBox() { 
  if ($('.listViewEntriesMainCheckBox').is(':checked')) {
    setTimeout(() => {
      $('#exportPaymentBtn').attr('disabled',false);
    }, 100);
  }else{
    $('#exportPaymentBtn').attr('disabled',true);
  }  
}

function FieldsCheker() {
  $('.listViewEntriesCheckBox').unbind().click(function () {    
    setTimeout(() => {
      $('#exportPaymentBtn').attr('disabled',false);
    }, 100);
  });
}

function removeAlert(sec) {
  setTimeout(() => {
    document.getElementById('alert_message').remove();
  }, sec);
}

function removeExportedInvoices() {
  let body = $('#listview-table tbody tr');
  let check = false;
  let invoiceid = new Array();
  for (let i = 0; i < body.length; i++) {
    check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
    if (check) {
      invoiceid.push($('#listview-table tbody tr').eq(i).data('id'));
    }
  }
  for (let e = 0; e < invoiceid.length; e++) {
    $(`[data-id=${invoiceid[e]}]`).remove();
  }
}

function showErrors(name) {
  let myTimer = setInterval(() => {
    let cookie = getCookie(name);
    if (cookie) {
      cookie = cookie.replace(/\%2F/g, "/");
      cookie = cookie.replace(/\%2C/g, ",");
      cookies = cookie.split(',');

      if (document.getElementById('alert_message') != null && cookies.length > 0) {
        clearInterval(myTimer);
        let message = document.getElementById('alert_message');
        let html = '<p style="margin-bottom: 20px;">Neįkeltos saskaitos, kurios neturi sumos arba agnum kodo</p><ul>';
        cookies.forEach(err => {
          html += `<li>${err}</li>`;
        });
        html += '</ul>';
        message.classList.remove('hide');
        message.innerHTML = html;
        eraseCookie(name);
      }
    }
  }, 2000);

}

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=0'
}


function exportPaymentBtn() {
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let iban = $('#iban :selected').text();
  let parameters = '';
  let format = document.getElementById('format').value;
  let showExported = document.querySelector('#showExported').checked; 
  let userid = _USERMETA.id;

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      parameters += `${form_key}=${form_value}:`;
    }
  }

  let body = $('#listview-table tbody tr');

  let invoiceid = '';
  let check = false;

  for (let i = 0; i < body.length; i++) {
    check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
    if (check) {
      invoiceid += $('#listview-table tbody tr').eq(i).data('id') + ",";
    }
  }

  parameters = parameters.slice(0, -1);
  invoiceid = invoiceid.slice(0, -1);

  if (invoiceid != '') {
    window.location.href = `vtlib/Vtiger/xml/exportPurchasePayment.php?parameters=${parameters}&iban=${iban}&showExported=${showExported}&format=${format}&userid=${userid}${(invoiceid != '') ? '&purchaseid=' + invoiceid : ''}`;
  } else {
    document.querySelector('#exportPaymentBtn').setAttribute('disabled', 'disabled');
  }
}

function filterPurchaseOrdersForExport() {
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let form_array = new Array();
  let html = '';
  let fake_search = document.getElementById('fake_search');
  let showExported = document.querySelector('#showExported').checked;
  // $('#messageBar').removeClass('hide');
  $('#wait3').show();

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      form_array.push({ [form_key]: form_value });
    }
  }


  document.querySelector('#table-content').style.height = '100%';

  $.ajax({
    type: "POST",
    url: '/purchase/filterPurchaseOrdersForExport.php',
    data: { form_array: form_array, showExported: showExported },
    dataType: "JSON",
    success: function (response) {
      $('#real_search').addClass('hide');
      response.forEach(res => {
        html += `
        <tr style="${(res.exported == '1') ? 'background: #fafad2;' : ''}${(res.hasiban == '0') ? 'background: #8dac76;' : ''}" class="listViewEntries" data-id="${res.purchaseorderid}"
        data-recordurl="index.php?module=PurchaseOrder&view=Detail&record=${res.purchaseorderid}&app=SALES&independent=true"
        id="Unpaidinvoices_listView_row_1">
        <td class="listViewRecordActions">
          <!--LIST VIEW RECORD ACTIONS-->
          <div class="table-actions">
            <span class="input">
              <input ${(res.hasiban == '0') ? 'disabled' : ''} type="checkbox" value="${res.purchaseorderid}" class="listViewEntriesCheckBox" /></span><span>            
            </span>           
          </div>
        </td>    

        <td class="listViewEntryValue" data-vendorid="${res.vendorid}" data-name="vendor_id" title="${res.vendor_id}" data-rawvalue="${res.vendorid}"
        data-field-type="reference">
        <span class="fieldValue">
          <span class="value">
            <a class="js-reference-display-value" href="?module=Vendors&view=Detail&record=${res.vendorid}" title="Klientai">${res.vendor_id}</a>
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>

      <td class="listViewEntryValue" data-name="purchaseorder_no" title="${res.invoice_nr}" data-rawvalue="${res.invoice_nr}"
      data-field-type="string">
      <span class="fieldValue">
        <span class="value">
         ${res.invoice_nr}
        </span>
      </span>
    </td>

    <td class="listViewEntryValue" data-name="invoicedate" title="${res.invoicedate}" data-rawvalue="${res.invoicedate}"
    data-field-type="date">
    <span class="fieldValue">
      <span class="value">
        ${res.invoicedate}
      </span>
    </span>
    <span class="hide edit"> </span>
  </td>

      <td class="listViewEntryValue" data-name="hdnGrandTotal" title="" data-rawvalue="" data-field-type="currency">
      <span class="fieldValue">
        <span class="value">
          ${res.hdnGrandTotal}
        </span>
      </span>
    </td>  

       <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${res.debt}</span>
        </span>
      </td>
      <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${res.pay_date}</span>
        </span>
      </td>
        <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${res.payed}</span>
          </span>
        </td>

        <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${res.pay_type}</span>
        </span>
      </td>
      </tr>
        `;
      });

      fake_search.innerHTML = html;
      setTimeout(() => {
        fake_search.classList.remove('hide');
        // $('#messageBar').addClass('hide');
        $('#wait3').hide();
      }, 1500);
    }
  });

}
