<script src="layouts/v7/modules/Outgoing/resources/main.js"></script> 
{assign var=SHIPMENT_INFO value=$SHIPMENT_INFO}

  <div class="container">      
      
      <table id="listview-table" class="table listview-table" style="margin-top: 40px;margin-bottom: 20px;">             
        <tbody>  
        <thead>
          <tr>
            <th><label>Tara</label></th>
            <th><label>Svoris</label></th>
            <th><label>Ilgis</label></th>
            <th><label>Plotis</label></th>
            <th><label>Aukštis</label></th>
          </tr>
        </thead>       
         {foreach from=$SHIPMENT_INFO item=load} 
          <tr class="listViewEntries">
            <td>{$load['revised_quantity']} {$load['tare_type_name']}</td>
            <td>{$load['revised_weight_kg']}</td>        
            <td>{$load['revised_length_m']}</td>
            <td>{$load['revised_width_m']}</td>
            <td>{$load['revised_height_m']}</td>
          </tr>
        {/foreach}      
        </tbody>   
      </table>
  </div>