// total records show number
$('.totalNumberOfRecords2').on('click', function () {
  $('.showTotalCountIcon2').addClass('hide');

  setTimeout(function () {
    $('#count_show').removeClass('hide');
  }, 1000);
});

$('[data-columnname="(vendor_id ; (Vendors) assigned_user_id)"]').html('<i class="fa fa-sort customsort"></i> Vadybininkas');
$(document).ready(function () {

  $(function () {
    $('.pay_day').datepicker({
      dateFormat: "yy-mm-dd",
      autoclose: true
    });
  });
  purchaseDebtsFieldsCheker();

});

$(document).ajaxComplete(function () {
  $(function () {
    $('.pay_day').datepicker({
      dateFormat: "yy-mm-dd",
      autoclose: true
    });
  });
  purchaseDebtsFieldsCheker();
});


function purchaseDebtsFieldsCheker() {
  $('.listViewEntriesCheckBox').click(function () {
    if ($(this).is(':checked')) {
      insertPurchaseCheckedId();
    } else {
      insertPurchaseCheckedId();
    }
  });
}

function insertPurchaseCheckedId() {
  let body = $('#listview-table tbody tr');
  let purchaseid = new Array();

  for (let i = 0; i < body.length; i++) {
    let check = $('#listview-table tbody tr').eq(i).children().find('input:checkbox').is(':checked');
    if (check) {
      purchaseid.push($('#listview-table tbody tr').eq(i).data('id'));
    }
  }

  if (purchaseid.length > 0) {
    $('#pay_debt_btn').removeClass('btn-secondary');
    $('#pay_debt_btn').addClass('btn-success');
    $('#pay_debt_btn').attr('disabled', false);
    $('#selectedInvoices').val(purchaseid);
  } else {
    $('#pay_debt_btn').removeClass('btn-success');
    $('#pay_debt_btn').addClass('btn-secondary');
    $('#pay_debt_btn').attr('disabled', true);
    $('#selectedInvoices').val('');
  }

}


function addPaidPurchaseDebtList() {
  let purchaseid = $('#selectedInvoices').val();
  getSelectedPurchaseOrdersInfo(purchaseid);
}




function getSelectedPurchaseOrdersInfo(purchaseid) {
  let date = $('#pay_day').val();
  let payMethod = $('#pay_method').val();
  let modalBody = document.querySelector('.modal-table');
  let modalSummary = document.querySelector('.modal-summary');
  let tableElements = '';
  let totalPrice;
  let numbers = new Array();

  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0');
  let yyyy = today.getFullYear();
  today = `${yyyy}-${mm}-${dd}`;


  if (date == '') date = today;

  $.ajax({
    type: "POST",
    url: '/purchase/getSelectedOrdersInfo.php',
    data: { purchaseid: purchaseid },
    dataType: 'json',
    success: function (res) {
      if (res != null) {
        res.forEach(item => {
          tableElements += `
            <tr class="listViewContentHeader">
              <td class="listViewEntryValue">${item.subject}</td>               
              <td class="listViewEntryValue">${ (item.vendorname ? item.vendorname : '--')}</td>              
              <td class="listViewEntryValue">${item.total}</td>              
            </tr>`;
          numbers.push(item.total);
          totalPrice = numbers.map(Number).reduce((a, b) => { return a + b; });
        });
      }

      modalBody.innerHTML = tableElements;
      modalSummary.innerHTML = `
          <tr class="listViewContentHeader">
            <td class="listViewEntryValue">${date}</td>          
            <td class="listViewEntryValue">${payMethod}</td>          
            <td class="listViewEntryValue">${totalPrice.toFixed(2)}</td>
          </tr>`;
    }
  });

}




function paySelectedPurchaseDebts() {
  let purchaseid = $('#selectedInvoices').val();
  let payMethod = $('#pay_method').val();
  let date = $('#pay_day').val();
  purchases = purchaseid.split(',');

  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0');
  let yyyy = today.getFullYear();
  today = `${yyyy}-${mm}-${dd}`;


  if (date == '') date = today;

  $.ajax({
    type: "POST",
    url: '/purchase/paySelectedDebts.php',
    data: { purchaseid: purchaseid, payMethod: payMethod, date: date },
    beforeSend: function () {
      $('#wait2').show();
      $('#table_begin').css('opacity', '0.2');
      $('#table_begin2').css('opacity', '0.2');
    },
    success: function (res) {
      if (res == 'true') {
        setTimeout(() => {
          $('#wait2').hide();
          $('#table_begin').css('opacity', '1');
          $('#table_begin2').css('opacity', '1');
          $('[data-dismiss="modal"]').click();

          $('#pay_debt_btn').removeClass('btn-success');
          $('#pay_debt_btn').addClass('btn-secondary');
          $('#pay_debt_btn').attr('disabled', true);
          $('#selectedInvoices').val('');

          purchases.forEach(inv => {
            $(`[data-id="${inv}"]`).remove();
          });
        }, 2000);
      } else {
        alert('Įvyko klaida, praneškite puslapio administratoriui.');
      }

    }

  });
}


function searchByPurchaseDebtOrPayed() {
  let form_length = $('.searchRow input:not([type="hidden"])').length;
  let form_array = new Array();
  let html = '';
  let fake_search = document.getElementById('fake_search');
  $('#messageBar').removeClass('hide');
  $('#wait3').show();

  for (let i = 0; i < form_length; i++) {
    let form_key = $('.searchRow input:not([type="hidden"])')[i].name;
    let form_value = $('.searchRow input:not([type="hidden"])')[i].value;
    if (form_value != '') {
      form_array.push({ [form_key]: form_value });
    }
  }


  $.ajax({
    type: "POST",
    url: '/purchase/filter_unpaid_invoices.php',
    data: { form_array: form_array },
    dataType: "JSON",
    success: function (response) {
      response.forEach(res => {
        html += `
        <tr class="listViewEntries" data-id="${res.purchaseorderid}"
        data-recordurl="index.php?module=PurchaseOrder&view=Detail&record=${res.purchaseorderid}&app=SALES&independent=true"
        id="Unpaidinvoices_listView_row_1">
        <td class="listViewRecordActions">
          <!--LIST VIEW RECORD ACTIONS-->
          <div class="table-actions">
            <span class="input">
              <input type="checkbox" value="${res.purchaseorderid}" class="listViewEntriesCheckBox" /></span><span>
              <a class="quickView fa fa-eye icon action" data-app="SALES" title="Quick View"></a></span><span><a
                class="markStar fa icon action fa-star-o" title=" Click to follow"></a></span><span
              class="more dropdown action"><span href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i
                  class="fa fa-ellipsis-v icon"></i></span>
              <ul class="dropdown-menu">
                <li>
                  <a data-id="${res.purchaseorderid}"
                    href="index.php?module=PurchaseOrder&view=Detail&record=${res.purchaseorderid}&mode=showDetailViewByMode&requestMode=full&tab_label=Sąskaita Išsami informacija&app=SALES">Išsami
                    informacija</a>
                </li>
                <li>
                  <a data-id="${res.purchaseorderid}" href="javascript:void(0);"
                    data-url="index.php?module=PurchaseOrder&view=Edit&record=${res.purchaseorderid}&app=SALES" name="editlink">Redaguoti</a>
                </li>
                <li>
                  <a data-id="${res.purchaseorderid}" href="javascript:void(0);" class="deleteRecordButton">Pašalinti</a>
                </li>
              </ul>
            </span>
            <div class="btn-group inline-save hide">
              <button class="button btn-success btn-small save" type="button" name="save">
                <i class="fa fa-check"></i></button><button class="button btn-danger btn-small cancel" type="button"
                name="Cancel">
                <i class="fa fa-close"></i>
              </button>
            </div>
          </div>
        </td>
      
        <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${res.debt}</span>
          </span>
        </td>
        <td class="listViewEntryValue">
          <span class="fieldValue">
            <span class="picklist-color picklist-453-Created">${res.payed}</span>
          </span>
        </td>

        <td class="listViewEntryValue">
        <span class="fieldValue">
          <span class="picklist-color picklist-453-Created">${res.late_payment}</span>
        </span>
      </td>    

      <td class="listViewEntryValue" data-name="createdtime" title="${res.createdtime}" data-rawvalue="${res.createdtime}"
      data-field-type="date">
      <span class="fieldValue">
        <span class="value">
          ${res.createdtime}
        </span>
      </span>
      <span class="hide edit"> </span>
    </td>


        <td class="listViewEntryValue" data-vendorid="${res.vendorid}" data-name="vendor_id" title="${res.vendor_id}" data-rawvalue="${res.vendorid}"
        data-field-type="reference">
        <span class="fieldValue">
          <span class="value">
            <a class="js-reference-display-value" href="?module=Vendors&view=Detail&record=${res.vendorid}" title="Klientai">${res.vendor_id}</a>
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>


      <td class="listViewEntryValue" data-name="hdnGrandTotal" title="" data-rawvalue="" data-field-type="currency">
      <span class="fieldValue">
        <span class="value">
          ${res.hdnGrandTotal}
        </span>
      </span>
    </td>  

    <td class="listViewEntryValue" data-name="assigned_user_id" title="${res.owner}" data-rawvalue="${res.smownerid}" data-field-type="owner">
    <span class="fieldValue">
      <span class="value">
       ${res.owner}
      </span>
    </span>
    <span class="hide edit"> </span>
  </td>

  <td class="listViewEntryValue" data-name="invoicestatus" title="Created" data-rawvalue="Created"
  data-field-type="picklist">
  <span class="fieldValue">
    <span class="value">
      <span class="picklist-color picklist-453-Created"> Naujas </span>
    </span>
  </span>
  <span class="hide edit"> </span>
</td>
            
      <td class="listViewEntryValue" data-name="purchaseorder_no" title="${res.invoice_nr}" data-rawvalue="${res.invoice_nr}"
      data-field-type="string">
      <span class="fieldValue">
        <span class="value">
         ${res.invoice_nr}
        </span>
      </span>
    </td>


     <td class="listViewEntryValue" data-name="invoicedate" title="${res.invoicedate}" data-rawvalue="${res.invoicedate}"
          data-field-type="date">
          <span class="fieldValue">
            <span class="value">
              ${res.invoicedate}
            </span>
          </span>
          <span class="hide edit"> </span>
        </td>
        
        <td class="listViewEntryValue" data-name="duedate" title="${res.duedate}" data-rawvalue="${res.duedate}"
        data-field-type="date">
        <span class="fieldValue">
          <span class="value">
            ${res.duedate}
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>   


      <td class="listViewEntryValue" data-name="purchaseorder_no" title="${res.purchaseorder_no}" data-rawvalue="${res.purchaseorder_no}"
      data-field-type="string">
      <span class="fieldValue">
        <span class="value">
          <a href="index.php?module=PurchaseOrder&view=Detail&record=${res.purchaseorderid}&app=SALES">${res.purchaseorder_no}</a>
        </span>
      </span>
    </td>


        <td class="listViewEntryValue" data-name="assigned_user_id2" title="${res.owner_name}" data-rawvalue="${res.smownerid2}" data-field-type="owner">
        <span class="fieldValue">
          <span class="value">
           ${res.owner_name}
          </span>
        </span>
        <span class="hide edit"> </span>
      </td>

            
      <td class="listViewEntryValue" data-name="balance" title="${res.balance}" data-rawvalue="${res.balance}" data-field-type="currency">
      <span class="fieldValue">
        <span class="value">
          ${res.balance}
        </span>
      </span>
    </td>

      </tr>
        `;
      });

      fake_search.innerHTML = html;
      setTimeout(() => {
        fake_search.classList.remove('hide');
        $('#messageBar').addClass('hide');
        $('#wait3').hide();
        purchaseDebtsFieldsCheker();
      }, 1500);
    }
  });

}
