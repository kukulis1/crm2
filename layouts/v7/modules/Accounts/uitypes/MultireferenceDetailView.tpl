{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
{strip}
	{assign var=RELATED_PRICEBOOKS value=$RELATED_PRICEBOOKS}
    {foreach item=CONTACT_INFO from=$RELATED_PRICEBOOKS}
        <a target="_blank" href='index.php?module=Newpricebooks&view=PriceBook&record={$CONTACT_INFO['id']}' title='{vtranslate("Newpricebooks", "Newpricebooks")}'> {$CONTACT_INFO['name']}</a>
        <br>
    {/foreach}
{/strip}