/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Vtiger_Edit_Js("Accounts_Edit_Js", {

}, {

	//This will store the editview form
	editViewForm: false,

	//Address field mapping within module
	addressFieldsMappingInModule: {
		'bill_street': 'ship_street',
		'bill_pobox': 'ship_pobox',
		'bill_city': 'ship_city',
		'bill_state': 'ship_state',
		'bill_code': 'ship_code',
		'bill_country': 'ship_country'
	},

	// mapping address fields of MemberOf field in the module              
	memberOfAddressFieldsMapping: {
		'bill_street': 'bill_street',
		'bill_pobox': 'bill_pobox',
		'bill_city': 'bill_city',
		'bill_state': 'bill_state',
		'bill_code': 'bill_code',
		'bill_country': 'bill_country',
		'ship_street': 'ship_street',
		'ship_pobox': 'ship_pobox',
		'ship_city': 'ship_city',
		'ship_state': 'ship_state',
		'ship_code': 'ship_code',
		'ship_country': 'ship_country'
	},
	/**
	 * Function to swap array
	 * @param Array that need to be swapped
	 */
	swapObject: function (objectToSwap) {
		var swappedArray = {};
		var newKey, newValue;
		for (var key in objectToSwap) {
			newKey = objectToSwap[key];
			newValue = key;
			swappedArray[newKey] = newValue;
		}
		return swappedArray;
	},

	/**
	 * Function to copy address between fields
	 * @param strings which accepts value as either odd or even
	 */
	copyAddress: function (swapMode, container) {
		var thisInstance = this;
		var addressMapping = this.addressFieldsMappingInModule;
		if (swapMode == "false") {
			for (var key in addressMapping) {
				var fromElement = container.find('[name="' + key + '"]');
				var toElement = container.find('[name="' + addressMapping[key] + '"]');
				toElement.val(fromElement.val());
			}
		} else if (swapMode) {
			var swappedArray = thisInstance.swapObject(addressMapping);
			for (var key in swappedArray) {
				var fromElement = container.find('[name="' + key + '"]');
				var toElement = container.find('[name="' + swappedArray[key] + '"]');
				toElement.val(fromElement.val());
			}
		}
	},

	/**
	 * Function to register event for copying address between two fileds
	 */
	registerEventForCopyingAddress: function (container) {
		var thisInstance = this;
		var swapMode;
		jQuery('[name="copyAddress"]').on('click', function (e) {
			var element = jQuery(e.currentTarget);
			var target = element.data('target');
			if (target == "billing") {
				swapMode = "false";
			} else if (target == "shipping") {
				swapMode = "true";
			}
			thisInstance.copyAddress(swapMode, container);
		})
	},

	/**
	 * Function which will copy the address details - without Confirmation
	 */
	copyAddressDetails: function (data, container) {
		var thisInstance = this;
		thisInstance.getRecordDetails(data).then(
			function (data) {
				var response = data['result'];
				thisInstance.mapAddressDetails(thisInstance.memberOfAddressFieldsMapping, response['data'], container);
			},
			function (error, err) {

			});
	},

	/**
	 * Function which will map the address details of the selected record
	 */
	mapAddressDetails: function (addressDetails, result, container) {
		for (var key in addressDetails) {
			// While Quick Creat we don't have address fields, we should  add
			if (container.find('[name="' + key + '"]').length == 0) {
				container.append("<input type='hidden' name='" + key + "'>");
			}
			container.find('[name="' + key + '"]').val(result[addressDetails[key]]);
			container.find('[name="' + key + '"]').trigger('change');
			container.find('[name="' + addressDetails[key] + '"]').val(result[addressDetails[key]]);
			container.find('[name="' + addressDetails[key] + '"]').trigger('change');
		}
	},

	registerRelatedContactSpecificEvents: function (form) {
		var thisInstance = this;
		if (typeof form == "undefined") {
			form = this.getForm();
		}

		form.find('[name="pricebook"]').on(Vtiger_Edit_Js.preReferencePopUpOpenEvent, function (e) {
			var parentIdElement = form.find('[name="parent_id"]');
			if (parentIdElement.length <= 0) {
				parentIdElement = form.find('[name="pricebook"]');
			}

		})
		//If module is not accounts then we dont have to register events
		if (!this.isAccounts(form)) {
			return;
		}
		this.getRelatedContactElement(form).select2({
			minimumInputLength: 3,
			ajax: {
				'url': 'index.php?module=PriceBooks&action=BasicAjax&search_module=PriceBooks',
				'dataType': 'json',
				'data': function (term, page) {
					var data = {};
					data['search_value'] = term;
					var parentIdElement = form.find('[name="parent_id"]');
					if (parentIdElement.length > 0 && parentIdElement.val().length > 0) {
						var closestContainer = parentIdElement.closest('td');
						data['parent_id'] = parentIdElement.val();
						data['parent_module'] = closestContainer.find('[name="popupReferenceModule"]').val();
					}
					return data;
				},
				'results': function (data) {
					data.results = data.result;
					for (var index in data.results) {

						var resultData = data.result[index];
						resultData.text = resultData.label;
					}
					return data
				},
				transport: function (params) {
					return jQuery.ajax(params);
				}
			},
			multiple: true,
			//To Make the menu come up in the case of quick create
			dropdownCss: { 'z-index': '10001' }
		});

		//To add multiple selected contact from popup
		form.find('[name="pricebook"]').on(Vtiger_Edit_Js.refrenceMultiSelectionEvent, function (e, result) {
			thisInstance.addNewContactToRelatedList(result, form);
		});

		$('#pricebook_display').on('change', function () {
			let value = $('#pricebook_display').val();
			$('[name="pricebook"]').val(value);
		});

		this.fillRelatedContacts(form);
	},

	registerAccountsDublicatePrevencion: function (form) {
		let thisInstance = this;
		//If module is not accounts then we dont have to register events
		if (!this.isAccounts(form)) {
			return;
		}
		$(document).on('change', '#Accounts_editView_fieldName_accountname', function () {
			if ($('[data-fieldname="accounttype"]').val() == 'Juridinis') {
				thisInstance.checkDublikate($(this).val(), this,'name');
			} else {
				$(this).removeClass('alert alert-danger');
				$(this).parent().find('p').remove();
			}
		});
		$(document).on('change', '#Accounts_editView_fieldName_legal_entity_code', function () {
			if ($('[data-fieldname="accounttype"]').val() == 'Juridinis') {
				if($(this).val() != ''){
					thisInstance.checkDublikate($(this).val(), this,'code');
				}
			} else {
				$(this).removeClass('alert alert-danger');
				$(this).parent().find('p').remove();
			}
		});
		$(document).on('change', '#Accounts_editView_fieldName_legal_vat_code', function () {

			if ($('[data-fieldname="accounttype"]').val() == 'Juridinis') {
				if($(this).val() != '' && !$('input#Accounts_editView_fieldName_cf_2669').is(':checked')){
					thisInstance.checkDublikate($(this).val(), this,'code');
				}else{
					$('.saveButton').attr('disabled',false);
					$(this).removeAttr('required')
					$(this).removeClass('alert alert-danger');
					$(this).parent().find('p').remove();
				}
			} else {
				$(this).removeClass('alert alert-danger');
				$(this).parent().find('p').remove();
			}
		});

		$('input#Accounts_editView_fieldName_cf_2669').on('click', function(){
			if($(this).is('checked')){
				$('.saveButton').attr('disabled',false);
				$('#Accounts_editView_fieldName_legal_vat_code').val('');
				$('#Accounts_editView_fieldName_legal_vat_code').attr('required', false)
				$('#Accounts_editView_fieldName_legal_vat_code').removeClass('alert alert-danger');
				$('#Accounts_editView_fieldName_legal_vat_code').parent().find('p').remove();
			}else{
				$('#Accounts_editView_fieldName_legal_vat_code').attr('required',true);
			}
		});
	},

	registerAccountsSwithType: function (form) {
		let thisInstance = this;
		//If module is not accounts then we dont have to register events
		if (!this.isAccounts(form)) {
			return;
		}
		$('#Accounts_editView_fieldName_legal_entity_code').attr('required', 'true');
		$('#Accounts_editView_fieldName_legal_vat_code').attr('required', 'true');

		$(document).on('change', '[data-fieldname="accounttype"]', function () {
			if ($('[data-fieldname="accounttype"]').val() == 'Fizinis') {
				$('#Accounts_editView_fieldName_legal_entity_code').removeAttr('required');
				$('#Accounts_editView_fieldName_legal_vat_code').removeAttr('required');
			} else {
				$('#Accounts_editView_fieldName_legal_entity_code').attr('required', 'true');
				$('#Accounts_editView_fieldName_legal_vat_code').attr('required', 'true');
			}
		});

		$(document).ready(function(){
			if($('[data-fieldname="accounttype"]').val()  == 'Fizinis'){
				$('#Accounts_editView_fieldName_legal_entity_code').removeAttr('required');
				$('#Accounts_editView_fieldName_legal_vat_code').removeAttr('required');	
			}

			if(!$('input#Accounts_editView_fieldName_cf_2669').is('checked')){
				$('#Accounts_editView_fieldName_legal_vat_code').attr('required', false)
			}

		});

	},

	checkDublikate: function (code, event,place) {
		let error_text = (place == 'code' ? 'Klientas su tokiu kodu jau yra!' : 'Klientas su tokiu vardu jau yra!');
		let accountid = $('[name="record"]').val();
		$.ajax({
			url: "modules/Accounts/ajax/dublicate.php",
			type: "POST",
			data: { code: code, accountid:accountid},
			success: function (data) {
				if(data){
					$(event).addClass('alert alert-danger');
					$(event).parent().find('p').remove();
					$(event).parent().append(`<p style='color: red;'>${error_text}</p>`);
					$('.saveButton').attr('disabled',true);
				} else {
					$(event).removeClass('alert alert-danger');
					$(event).parent().find('p').remove();
					$('.saveButton').attr('disabled',false);
				}
			}
		});
	},

	addNewContactToRelatedList: function (newContactInfo, form) {
		if (form.length <= 0) {
			form = this.getForm();
		}
		var resultentData = new Array();

		var element = jQuery('#pricebook_display', form);
		var selectContainer = jQuery(element.data('select2').container, form);
		var choices = selectContainer.find('.select2-search-choice');
		choices.each(function (index, element) {
			resultentData.push(jQuery(element).data('select2-data'));
		});
		var select2FormatedResult = newContactInfo.data;
		for (var i = 0; i < select2FormatedResult.length; i++) {
			var recordResult = select2FormatedResult[i];
			recordResult.text = recordResult.name;
			resultentData.push(recordResult);
		}
		element.select2('data', resultentData);
		if (form.find('.quickCreateContent').length > 0) {
			form.find('[name="relatedContactInfo"]').data('value', resultentData);
			var relatedContactElement = this.getRelatedContactElement(form);
			if (relatedContactElement.length > 0) {
				jQuery('<input type="hidden" name="contactidlist" /> ').appendTo(form).val(relatedContactElement.val().split(',').join(';'));
				form.find('[name="contact_id"]').attr('name', '');
			}
		}
		let value = $('#pricebook_display').val();
		$('[name="pricebook"]').val(value);
	},


	relatedContactElement: false,

	recurringEditConfirmation: false,

	getRelatedContactElement: function (form) {
		if (typeof form == "undefined") {
			form = this.getForm();
		}
		this.relatedContactElement = jQuery('#pricebook_display', form);
		return this.relatedContactElement;
	},

	fillRelatedContacts: function (form) {
		if (typeof form == "undefined") {
			form = this.getForm();
		}
		var relatedContactValue = form.find('[name="relatedContactInfo"]').data('value');
		for (var contactId in relatedContactValue) {
			var info = relatedContactValue[contactId];
			info.text = info.name;
			relatedContactValue[contactId] = info;
		}
		this.getRelatedContactElement(form).select2('data', relatedContactValue);
	},

	isAccounts: function (form) {
		if (typeof form === 'undefined') {
			form = this.getForm();
		}
		var moduleName = form.find('[name="module"]').val();
		if (moduleName === 'Accounts') {
			return true;
		}
		return false;
	},

	/**
	 * Function which will register basic events which will be used in quick create as well
	 *
	 */
	registerBasicEvents: function (container) {
		this._super(container);
		this.registerEventForCopyingAddress(container);
		this.registerRelatedContactSpecificEvents(container);
		this.registerAccountsDublicatePrevencion(container);
		this.registerAccountsSwithType(container);
	}
});

