$('#datepicker-year').datepicker({
  format: "yyyy"+"-01-01",
  weekStart: 1,
  orientation: "top",
  language: "{{ app.request.locale }}",
  keyboardNavigation: false,
  viewMode: "years",
  minViewMode: "years"
});