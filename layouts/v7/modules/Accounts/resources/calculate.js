$('[data-target="#orderFormModal"]').on('click', function () {
  loadPriceCalculator('#orderFormModal', 'new');
  $(window).keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});


function loadPriceCalculator(selector, type) {
  let parent = $(selector);

  if (type == 'new') {
    parent.find(`#cargo_length1`).val(0.8);
    parent.find(`#cargo_width1`).val(1.2);
    parent.find(`#cargo_height1`).val(1);
  }

  const totalHidden = parent.find('#grandTotal').html();

  const blockPrice = parent.find("[name='editContent']");

  if (totalHidden !== '') {
    setTimeout(function () {
      parent.find('#grandTotal').html(totalHidden);
    }, 200);
  }


  const divas = document.createElement("div");
  divas.setAttribute('id', 'wait');
  divas.setAttribute('style', 'display:none;position:absolute;top: 70%;left:50%;padding:2px;z-index: 1001;');
  const img = document.createElement('img');
  img.setAttribute('src', '/resources/loading.gif');
  divas.append(img);
  blockPrice.append(divas);

  const alert = document.createElement("div");
  alert.setAttribute("id", "alert");
  alert.setAttribute("display", "block");
  alert.setAttribute("style", "color:red;margin-left: 90px;margin-top: 10px;");

  function getPrice(i) {

    let rows = document.querySelectorAll(`${selector} #lineItemTab .lineItemRow:not(.except)`);
    let cargoKgArray = new Array();
    let cargoKgArray2 = new Array();
    let values = new Array();
    let values_m2 = new Array();
    let cargoVolume = new Array();
    let cargoVolume2 = new Array();
    let cargoSquare;
    let cargoSquare2 = new Array();
    let cargoTemp = new Array();
    let cargoTemp2 = new Array();
    let cargoKg;
    let dimensions;
    let dimensions_m2;
    let weight;
    let length;
    let width;
    let height;
    let measure;
    let measures = new Array();

    for (let e = 0, len = rows.length; e < len; e++) {
      c = rows[e].dataset.rowNum;
      weight = parent.find('#cargo_wgt' + c).val();
      length = parent.find('#cargo_length' + c).val();
      width = parent.find('#cargo_width' + c).val();
      height = parent.find('#cargo_height' + c).val();

      measure = $('#measure' + c).val();

      dimensions = `${weight} ${length}x${width}x${height}`;
      dimensions_m2 = length * width;

      values.push(dimensions);
      values_m2.push(dimensions_m2);
      measures.push(Number(measure));

      values = values.join();
      values = values.split(" ").join();
      values = values.split(",");

    }


    measures = measures.map(function (item) { return item == 2 || item == 3 ? 1 : item; });

    const found = measures.includes(1);
    let allEqual = false;

    if (found) {
      allEqual = measures.every((val, i, arr) => val === arr[0]);
    }

    let qantity = new Array();

    for (j = 0; j <= values.length; j = j + 2) {
      if (values[j] !== undefined) {
        cargoKgArray.push(Number(values[j]));
        cargoTemp.push(values[j + 1]);
      }
    }


    for (let m = 0; m < rows.length; m++) {
      b = rows[m].dataset.rowNum;
      qantity[m] = parseInt(document.getElementById(`qty${b}`).value);
      cargoTemp2[m] = cargoTemp[m];

      cargoTemp2[m] = cargoTemp2[m].replace(/X/g, 'x');
      cargoTemp2[m] = cargoTemp2[m].replace(/,/g, '.');
      cargoTemp2[m] = cargoTemp2[m].split("x");

      cargoTemp2[m] = cargoTemp2[m].map(Number).reduce(function (a, b) { return a * b; });
      cargoVolume[m] = cargoTemp2[m];


      cargoVolume2[m] = parseFloat(cargoVolume[m]) * parseFloat(qantity[m]);
      cargoSquare2[m] = parseFloat(values_m2[m]) * parseFloat(qantity[m]);
      cargoKgArray2[m] = parseFloat(cargoKgArray[m]);
    }


    cargoKg = cargoKgArray2.map(Number).reduce((a, b) => a + b, 0);
    cargoVolume = cargoVolume2.map(Number).reduce(function (a, b) { return a + b; });
    cargoSquare = cargoSquare2.map(Number).reduce(function (a, b) { return a + b; });

    cargoVolume = parseFloat(cargoVolume).toFixed(3);

    let pll = 0;

    if (allEqual) {
      pll = qantity.map(Number).reduce((a, b) => a + b, 0);
    }

    console.log(cargoKg + " kg");
    console.log(cargoVolume + " m3");
    console.log(cargoSquare + " m2");
    console.log(pll + " pll");

    rows = undefined;


    let fromPost = parent.find('[name="bill_code"]').val();
    let toPost = parent.find('[name="ship_code"]').val();

    let from_country = parent.find('[name="bill_country"]').val();
    let to_country = parent.find('[name="ship_country"]').val();
    let distance = 0;

    let accId = $('[name="record_id"]').val();


    const appendAlert = parent.find(`#row${i} .input-group2`);
    if (fromPost == '' || toPost == '') {
      appendAlert.append(alert);
      parent.find("#alert").html("Įveskite pašto kodą");
      parent.find("#wait").css("display", "none");
      parent.find('#lineItemTab').css('opacity', '1');
    } else if (cargoKg == '') {
      appendAlert.append(alert);
      parent.find("#alert").html("Įveskite matmenis");
      parent.find("#wait").css("display", "none");
      parent.find('#lineItemTab').css('opacity', '1');
    } else {
      $("#alert").remove();
      $.ajax({
        type: "POST",
        url: "price-algorithm/index.php",
        data: { fromPost: fromPost, accId: accId, toPost: toPost, cargoKg: cargoKg, cargoVolume: cargoVolume, distance: distance, cargoSquare: cargoSquare, from_country: from_country, to_country: to_country, pll: pll },
        dataType: "JSON",
        success: function (result) {
          console.log(result);
          if (result.price !== 0) {
            $(document).ajaxComplete(function () {
              setTimeout(function () {
                parent.find("#wait").css("display", "none");
                parent.find('#lineItemTab').css('opacity', '1');
              }, 500);
            });
            parent.find('#pricebookprice').html(parseFloat(result.price).toFixed(2));
            parent.find('[name="total"]').val(parseFloat(result.price).toFixed(2));
            parent.find('[name="pricebookprice"]').val(parseFloat(result.price).toFixed(2));

            let grand = parent.find("#grandTotal");
            grand.html(parseFloat(result.price).toFixed(2));
            postTrig = '';

          } else {
            $(document).ajaxComplete(function () {
              setTimeout(function () {
                parent.find("#wait").css("display", "none");
                parent.find('#lineItemTab').css('opacity', '1');
              }, 1000);
            });
            appendAlert.append(alert);
            parent.find("#grandTotal").html(parseFloat(result.price).toFixed(2));

            parent.find('.input-group2').click(function () {
              alert.remove();
            });

            parent.find("#alert").html(result.combination);

          }
        }
      });
    }
  }


  parent.find('#lineItemTab').click(function () {
    alert.remove();
  });


  var input = parent.find(".enter_trig");
  for (let trigId = 0; trigId < input.length; trigId++) {
    input[trigId].addEventListener("keyup", function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        trigger();
      }
    });
  }

  parent.find('#lineItemTab').change(function () {
    let line = $(this);
    let id = $(this).data('rowNum');
    if (line.find(`#cargo_wgt${id}`).val() && line.find(`#cargo_length${id}`).val() && line.find(`#cargo_width${id}`).val() && line.find(`#cargo_height${id}`).val()) {
      trigger();
    }
  });

  parent.find('#lineItemTab').keyup(function (e) {
    if (e.keyCode == 13) {
      trigger();
    }
  });

  function trigger() {
    parent.find("#wait").css("display", "block");
    parent.find('#lineItemTab').css('opacity', '0.2');
    let product = parent.find('.lineItemRow');
    let pro;
    for (let i = 0, len = product.length; i <= len - 1; i++) {
      pro = document.querySelectorAll('.lineItemRow')[i].dataset.rowNum;
    }
    getPrice(pro);
  }
}



function isNumber(evt) {
  let charCode = (event.which) ? event.which : event.keyCode;
  evt.target.value = evt.target.value.replace(/,/g, '.');


  if (charCode != 46 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  } else {
    //if dot sign entered more than once then don't allow to enter dot sign again. 46 is the code for dot sign
    let parts = evt.srcElement.value.split('.');

    if (parts.length > 1 && (charCode == 46 || charCode == 44)) {
      return false;
    }
    return true;
  }
}
