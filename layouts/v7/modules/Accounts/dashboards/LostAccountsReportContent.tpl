{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<link rel="stylesheet" href="layouts/v7/modules/Accounts/resources/dashboard.css" type="text/css" />
<script src="layouts/v7/modules/Accounts/resources/dashboard.js"></script>


{if $REPORTS['role'] eq 'H2' OR $REPORTS['role'] eq 'H11' OR $REPORTS['role'] eq 'H23' OR $REPORTS['role'] eq 'H13'}

{if $REPORTS['filter']}<div>{$REPORTS['filter']}</div>{/if}
<div style="display: inline;">
  <div style="display: inline;">
    Filtruoti pagal atsakinga:  <input class="inputElement widgetFilter reloadOnChange" name="owner2" style="width: 200px;" value="{$REPORTS['owner_name']}">
  </div>
  <div style="display: inline;margin-left: 15px;">
    <span style="margin-right: 5px;">Užsakymo tipas:</span>  
    <input type="radio" class="inputElement widgetFilter" name="order_type2" value="Visi" {if !$REPORTS['order_type'] || $REPORTS['order_type'] eq 'Visi'}checked{/if}>
    <label style="margin-right: 15px;">Visi</label>
    <input type="radio" class="inputElement widgetFilter" name="order_type2" value="Transporto užsakymas" {if $REPORTS['order_type'] eq 'Transporto užsakymas'}checked{/if}>
    <label style="margin-right: 15px;">Transporto</label>
    <input type="radio" class="inputElement widgetFilter" name="order_type2" value="Perkraustymo užsakymas" {if $REPORTS['order_type'] eq 'Perkraustymo užsakymas'}checked{/if}>
    <label>Perkraustymo</label>
  </div>
</div>

{* <pre>
    {$REPORTS['statistic_all']|@print_R}
</pre> *}

{if $REPORTS['clients'] neq false}

<table id='table' style='margin-top:10px;'>
    <thead>
      <tr class='listViewContentHeader'>
        <th width="10" style="padding-top: 12px;">#</th>
        <th style="width:10%">Klientas</th>
        <th style="width:10%">Atsakingas</th>             
				{foreach item=value from=$REPORTS['period']}  
         	<th>{$REPORTS['month_locale'][$value['month']]}</th>
        {/foreach}      
    </tr>
    </thead>
		<tbody>   
    {$count = 1} 
    {foreach item=client from=$REPORTS['clients']}    
      <tr>
        <td class='listViewEntryValue'>{$count}</td>    
        <td class='listViewEntryValue'><a target="_blank" href="/index.php?module=Accounts&view=Detail&record={$REPORTS['accountid'][$client]}">{$client}</a></td>         
        <td class='listViewEntryValue'>{$REPORTS['owner'][$client]}</td>    
        {foreach item=month from=$REPORTS['period']}    
            {$date = $month['date']}
          <td class='listViewEntryValue'>
         <a type="button" data-toggle="modal" data-target="#ordersModalLostClients" data-salesorderid="{$REPORTS['salesorderid'][$client][$date]}" onclick="showSalesOrdersLostClients(event);"> {if !empty($REPORTS['statistic_all'][$date][$client])}{$REPORTS['statistic_all'][$date][$client]}{else}0{/if}</a></td>                 
        {/foreach}    
      </tr>
			</tr>
      {assign var=count value=$count+1}
    {/foreach}

			</tbody>
</table>
      {else if $REPORT['no-records']} 
	      <span class="noDataMsg" style="font-size: 30px;">Nėra įrašų</span>
			{else}
		<span class="noDataMsg" style="font-size: 30px;margin-top:15px;display:block;">Užsakymų nėra</span> 
 {/if}

{else}
	<span class="noDataMsg">{vtranslate('LBL_NO_RIGHTS', $MODULE_NAME)}</span>
{/if}
