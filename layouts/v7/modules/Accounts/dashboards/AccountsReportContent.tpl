{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

<style>
	table {
		border-collapse: collapse;
	}

	th, td {
		text-align: left;
		padding: 8px;
	}

	#list:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>


	{if $REPORTS neq false}

	<div>Užsakymai: {$REPORTS['filter']}</div>
			<table style="width: 100%;">
				<tr>
				<th></th>
				{foreach key=$index item=DAY from=$REPORTS['days']}					 
					<th title="{$DAY['full']}">{$DAY['short']}</td>				
				{/foreach}		
				</tr>
			{foreach key=$index item=ACCOUNTNAME from=$REPORTS['accountname']}		
				<tr id="list">
					<td>{$ACCOUNTNAME}</td>	
					{foreach key=$index item=DAY from=$REPORTS['orders'][$ACCOUNTNAME]}		
						<td>{$DAY}</td>						
					{/foreach}
				</tr>								
			{/foreach}	
			</table>
	{else}
		<span class="noDataMsg">		
				{vtranslate('LBL_NO_SALESORDERS', $MODULE_NAME)}			
		</span>
{/if}