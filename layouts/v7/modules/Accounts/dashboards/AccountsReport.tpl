{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}



<div class="dashboardWidgetHeader clearfix">
    <div class="title">
        <div class="dashboardTitle" title="{vtranslate($WIDGET->getTitle(), $MODULE_NAME)}"><b>&nbsp;&nbsp;{vtranslate($WIDGET->getTitle())}</b></div>
    </div>

</div>
 	{if $REPORTS neq false}
		<div class="row entry clearfix clients" style="padding: 5px 5px;">
    	<div class="col-lg-8 pull-left" style="font-size: 14px;"><b>Klientas</b></div>   
    	<div class="col-lg-4 pull-right muted" style="font-size: 14px;"><b>Užsakymai</b></div>
		</div>
    {/if}
<div class="dashboardWidgetContent scrollContainer" style="padding:5px; padding-bottom: 20px;">
 <div style='padding:5px;'>	 
{include file="dashboards/AccountsReportContent.tpl"|@vtemplate_path:$MODULE_NAME}
</div>

</div>

<div class="widgeticons dashBoardWidgetFooter">
    <div class="filterContainer boxSizingBorderBox">
     <div class="row" style="margin-bottom: 10px;">
            <div class="col-sm-12">
                <div class="col-lg-4">
                    <span><strong>{vtranslate('LBL_SHOW', $MODULE_NAME)}</strong></span>
                </div>
                <div class="col-lg-7">                     
                        <label class="radio-group cursorPointer">
                            <input type="radio" name="accountReportType" class="widgetFilter reloadOnChange cursorPointer" value="without" /> 
                            <span>{vtranslate('LBL_WITHOUT_ORDERS', $MODULE_NAME)}</span>
                        </label><br>
                        <label class="radio-group cursorPointer">
                            <input type="radio" name="accountReportType" class="widgetFilter reloadOnChange cursorPointer" value="with" checked /> {vtranslate('LBL_WITH_ORDERS', $MODULE_NAME)}
                        </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <span class="col-lg-5">
                        <span>
                            <strong>{vtranslate('LBL_SELECT_DATE_FROM', $MODULE_NAME)}</strong>
                        </span>
                </span>
                <span class="col-lg-7">
                    <div class="input-daterange input-group dateRange widgetFilter" id="datepicker" name="modifiedtime">
                        <input type="text" class="input-sm form-control" autocomplete="off" name="start" style="height:30px;"/>
                        {* <span class="input-group-addon">iki</span> *}
                        <input type="hidden" class="input-sm form-control" autocomplete="off" name="end" style="height:30px;"/> 
                    </div>
                </span>
            </div>
        </div>
    </div>
    <div class="footerIcons pull-right">
        {include file="dashboards/DashboardFooterIcons.tpl"|@vtemplate_path:$MODULE_NAME SETTING_EXIST=true}
    </div>
</div>