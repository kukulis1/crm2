{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

<script>
$('[data-toggle="popover"]').popover({ trigger: "hover" });
</script>

<div class="modal fade" id="ordersModalLostClients" tabindex="-1" role="dialog" aria-labelledby="ordersModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ordersModalLabelLostClients">Užsakymai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
					<thead>
					<th>Siuntos Nr.</th>
					<th>Suma</th>
					<th>Klientas</th>
					<th>Sukūrimo laikas</th>
					</thead>
					<tbody id="ordersTbodyLostClients"></tbody>
        </table>
      </div>
      <div class="modal-footer">
       <div style="float:left"><label>Viso: </label> <span class="viso_span2"></span></div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
        {* <button type="button" class="btn btn-primary">Save changes</button> *}
      </div>
    </div>
  </div>
</div>

<div class="dashboardWidgetHeader clearfix">
    <div class="title">
        <div class="dashboardTitle" title="{vtranslate($WIDGET->getTitle(), $MODULE_NAME)}"><b>&nbsp;&nbsp;{vtranslate($WIDGET->getTitle())}</b> 
        <i style="font-size: 20px;cursor: context-menu;" class="fa fa-question-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="Yra pasiimamas rėžis nuo metu pradžios iki galo jei praeiti metai, jei dabartiniai tada iki praeito mėnesio, nes esamo mėnesio negalim vertinti. Tada yra tikrinima koks skirtumas tarp sausio ir vasario, vasario ir kovo, kovo ir balandzio ir t.t., jei pusė iš rėžyje esančiu mėnesiu skirtumas buna daugiau nei 20% užsakymu tada ta klienta rodom ataskaitoje."></i></div>
    </div>
</div>
<div class="dashboardWidgetContent scrollContainer" style="padding-top:15px;">

{include file="dashboards/LostAccountsReportContent.tpl"|@vtemplate_path:$MODULE_NAME}
</div>
<div class="widgeticons dashBoardWidgetFooter">
    <div class="filterContainer boxSizingBorderBox">
        <div class="row">
            <div class="col-sm-12">
                <span class="col-lg-5">
                        <span>
                            <strong>{vtranslate('LBL_SELECT_DATE_FROM', $MODULE_NAME)}</strong>
                        </span>
                </span>
                <span class="col-lg-7">
                    <div class="input-daterange input-group dateRange widgetFilter" id="datepicker" name="modifiedtime">
                        <input type="text" class="input-sm form-control" id="datepicker-year" autocomplete="off" name="start" style="height:30px;"/>
                        {* <span class="input-group-addon">iki</span> *}
                        <input type="hidden" class="input-sm form-control" autocomplete="off" name="end" style="height:30px;"/> 
                    </div>
                </span>
            </div>
        </div>
    </div>
    <div class="footerIcons pull-right">
        {include file="dashboards/DashboardFooterIcons.tpl"|@vtemplate_path:$MODULE_NAME SETTING_EXIST=true}
    </div>
</div>
