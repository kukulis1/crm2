{strip}
	<script src="layouts/v7/lib/jquery/fullcalendar/lib/jquery-ui.min.js"></script>
	<script src="layouts/v7/modules/Accounts/resources/standingSalesOrders.js"></script>
	<script type="text/javascript" src="{vresource_url('layouts/v7/modules/Accounts/resources/calculate.js')}"></script>

			{assign var="TEMPLATES" value=$GET_TEMPLATES}
			{$weekDays = ['Monday' => 'Pirmadienį', 'Tuesday' => 'Antradienį', 'Wednesday' => 'Trečiadienį', 'Thursday' => 'Ketvirtadienį', 'Friday' => 'Penktadienį', 'Saturday' => 'Šestadienį', 'Sunday' => 'Sekmadienį']}

				{if !$TEMPLATES}
						<div style="display: flex;justify-content: center;border: 1px solid #ddd;margin-bottom: 30px;"><h5>Šablonų nėra</h5></div>
				{else}
					<table class="table detailview-table">
						<tr class="listViewContentHeader">
							<th style="width: 80px;">#</th>
							<th>Užsakymo tipas</th>
							<th>Tipas</th>
							<th>Kartojimas</th>
							<th>Vykdymas nuo</th>
							<th>Vykdymas iki</th>
							<th>Užsakymo data</th>
							<th>Pasikrovimas</th>
							<th>Išsikrovimas</th>
							<th>Siuntėjas</th>
							<th>Pakrovimo adresas</th>
							<th>Pakrovimo miestas</th>
							<th>Pakr. p/k</th>						
							<th>Gavėjas</th>
							<th>Iškrovimo adresas</th>
							<th>Iškrovimo miestas</th>
							<th>Iškr. p/k</th>
							<th>Sukūrė</th>
							<th>Sukūrimo laikas</th>
						</tr>
						{foreach from=$TEMPLATES item=item key=key}
							{$execute_days = explode(",",$item['execute'])}						
							<tr>
								<td class="listViewEntryValue" style="padding:8px 0 0;">
									<span class="actionImages">&nbsp;&nbsp;&nbsp;
										<a><i data-toggle="modal" data-record="{$item['record']}" data-target="#editOrderFormModal_{$item['record']}" title="Redaguoti" class="fa fa-pencil editBtn"></i></a> &nbsp;&nbsp;
										<a onclick="deleteRecord({$item['record']},event);"><i title="Pašalinti" class="fa fa-trash"></i></a>
									</span>
									  {* <label class="switch" style="width: 30px;height: 15px;margin-top: 0px;margin-left: 10px;">
											<input data-record="{$item['record']}" type="checkbox" class="eachStandingOrdersRule" {if $item['enabled'] eq 1}checked{/if}>
											<span class="slider2 round"></span>
									</label>  *}
								</td>
								<td class="listViewEntryValue">{$item['order_type']}</td>	
								<td class="listViewEntryValue">{if $item['execute_type'] eq 1}Kiekviena savaite{else if $item['execute_type'] eq 2}Mėnesio diena{else}Kiekviena diena{/if}</td>
								<td class="listViewEntryValue">
									{if $item['execute_type'] eq 1}<ul style="margin: 0;list-style: none;padding: 0;">{foreach from=$execute_days item=ex_day}<li>{$weekDays[$ex_day]}</li>{/foreach}</ul>{else}<span style="word-break: break-all;">{$item['execute']}</span>{/if}
								</td>
								<td class="listViewEntryValue">{$item['create_from']}</td>
								<td class="listViewEntryValue">{$item['create_untill']}</td>
								<td class="listViewEntryValue"><ul style="margin: 0;list-style: none;padding: 0;">{foreach from=$item['execution_date'][$item['record']] item=ex_date}<li>{$ex_date['actual_date']}</li>{/foreach}</ul></td>		
								<td class="listViewEntryValue"><ul style="margin: 0;list-style: none;padding: 0;">{foreach from=$item['execution_date'][$item['record']] item=ex_date}<li>{$ex_date['load_date']}</li>{/foreach}</ul></td>	
								<td class="listViewEntryValue"><ul style="margin: 0;list-style: none;padding: 0;">{foreach from=$item['execution_date'][$item['record']] item=ex_date}<li>{$ex_date['unload_date']}</li>{/foreach}</ul></td>						
								<td class="listViewEntryValue">{$item['load_company']}</td>
								<td class="listViewEntryValue">{$item['bill_street']}</td>
								<td class="listViewEntryValue">{$item['bill_city']}</td>
								<td class="listViewEntryValue">{$item['bill_code']}</td>							
								<td class="listViewEntryValue">{$item['unload_company']}</td>
								<td class="listViewEntryValue">{$item['ship_street']}</td>
								<td class="listViewEntryValue">{$item['ship_city']}</td>
								<td class="listViewEntryValue">{$item['ship_code']}</td>								
								<td class="listViewEntryValue">{$item['creator']}</td>								
								<td class="listViewEntryValue">{$item['createdtime']}</td>								
							</tr>
						{/foreach}
					</table>



	{foreach from=$TEMPLATES item=item key=key}
			<form id="templateForm{$item['record']}" method="POST">
					<div class="modal fade" id="editOrderFormModal_{$item['record']}" tabindex="-1" role="dialog" aria-labelledby="editOrderFormModalTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document" style="width: 1100px;">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="editOrderFormModalTitle">Pardavimo užsakymo forma</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">	               
								<input type="hidden" id="edit" name="edit" value="1">				
								<input type="hidden" id="template_id" name="template_id" value="{$item['record']}">				
									<table class="table detailview-table no-border">
                  <tr>
                   	<td class="fieldLabel alignMiddle">Intervalo tipas</td>
                    <td class="fieldValue">
                      <select style="width:240px;" class="inputElement execute_type" name="execute_type">
                        <option value="1" {if $item['execute_type'] eq 1}selected{/if}>Savaitės diena</option>
                        <option value="2" {if $item['execute_type'] eq 2}selected{/if}>Mėnesio diena</option>
                        <option value="3" {if $item['execute_type'] eq 3}selected{/if}>Kiekviena diena</option>
                      </select>
                    </td>

                    <td class="fieldLabel alignMiddle">Kartoti kiekvieną</td>
                    <td class="fieldValue">
											<div class="input-group inputElement" style="min-width: 100px;width: 100px;border:0;">									
											
											<select style="width:100px;" id="weekDays" class="inputElement{if $item['execute_type'] eq '2' OR $item['execute_type'] eq '3'} hide{/if}" name="execute[]" multiple>
											{foreach from=$weekDays item=row key=key}
												<option value="{$key}" {if in_array($key,explode(',',$item['execute']))}selected{/if}>{$row}</option>
											{/foreach}								
											</select>			

											<select style="width:100px;" id="monthDays" class="inputElement{if $item['execute_type'] eq '1' OR $item['execute_type'] eq '3'} hide{/if}" name="execute2[]" multiple>
														{for $day=1 to 31} 
																<option value="{$day}" {if in_array($day,explode(',',$item['execute']))}selected{/if}>{$day}</option>
														{/for}
											</select>              
											</div>
											<div class="input-group inputElement time" style="margin-left: 10px;width:84px !important; min-width:84px !important;height: 27px;">
													<input style="width:84px !important;height: 27px;"  type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="{$item['execute_time']}" name="execute_time" autocomplete="off">
													<span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
                    </td>
                  </tr>

										<tr>
											<td class="fieldLabel alignMiddle"><span class="redColor">*</span> Užsakymo tipas</td>
											<td class="fieldValue">
												<select class="inputElement" name="cf_855" style="width:240px;">
													<option value="Transporto užsakymas" {if $item['order_type'] eq 'Transporto užsakymas'}selected{/if}>Transporto užsakymas</option>
													<option value="Perkraustymo užsakymas" {if $item['order_type'] eq 'Perkraustymo užsakymas'}selected{/if}>Perkraustymo užsakymas</option>
													<option value="Sandėliavimo užsakymas" {if $item['order_type'] eq 'Sandėliavimo užsakymas'}selected{/if}>Sandėliavimo užsakymas</option>
												</select>
											</td>
											<td class="fieldLabel alignMiddle">Sukurti nuo:</td>
											<td>
											<div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;">
											<input id="create_from" type="text" data-calendar-type="date" class="dateField form-control" data-fieldtype="date" name="create_from" data-date-format="yyyy-mm-dd" value="{$item['create_from']}" data-field-type="date" aria-required="true" aria-invalid="false"><span class="input-group-addon"><i class="fa fa-calendar "></i></span></div>											
											</td>										
										</tr>

										<tr>
											<td class="fieldLabel alignMiddle"></td>
											<td class="fieldValue"></td>
											<td class="fieldLabel alignMiddle">Sukurti iki:</td>
											<td>
											<div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;">
											<input id="create_untill" type="text" data-calendar-type="date" class="dateField form-control" data-fieldtype="date" name="create_untill" data-date-format="yyyy-mm-dd" value="{$item['create_untill']}" data-field-type="date" aria-required="true" aria-invalid="false"><span class="input-group-addon"><i class="fa fa-calendar "></i></span></div>											
											</td>										
										</tr>


										<tr>
										</tr>
										<tr>
											<td class="fieldLabel alignMiddle">Pakrovimo atidėjimas ir laikas nuo iki&nbsp;&nbsp;</td>

											<td class="fieldValue">											
												<small style="color: green;">Pakrovimo data bus kartojimo data plius atidėjimas</small>
												<div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;border:0">
														<select style="width:240px;" class="inputElement" name="load_delay">
															{for $day=0 to 15} 
																	<option value="{$day}" {if $item['load_delay'] eq $day}selected{/if}>{$day}</option>
															{/for}
														</select>   
												</div>
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;">
													<input style="width:84px !important;" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="{$item['load_time_from']}" name="load_time_from" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
													
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;margin-left:10px;">
													<input style="width:84px !important;" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="{$item['load_time_to']}" name="load_time_to" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
													</div>
											</td>	

											<td class="fieldLabel alignMiddle">Iškrovimo atidėjimas ir laikas nuo iki&nbsp;&nbsp;</td>

											<td class="fieldValue">											
												<small style="color: green;">Iškrovimo data bus kartojimo data plius atidėjimas</small>
												<div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;border:0">
														<select style="width:240px;" class="inputElement" name="unload_delay">
															{for $day=1 to 15} 
																	<option value="{$day}" {if $item['unload_delay'] eq $day}selected{/if}>{$day}</option>
															{/for}
														</select>   
												</div>
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;">
													<input style="width:84px !important;" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="{$item['unload_time_from']}" name="unload_time_from" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;margin-left: 10px;">
													<input style="width:84px !important;" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="{$item['unload_time_to']}" name="unload_time_to" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
											</td>
											</tr>

											<tr>
												<td class="fieldLabel alignMiddle">Siuntėjas&nbsp;&nbsp;</td>
												<td class="fieldValue">	
													<input style="width: 240px;" type="text" class="inputElement" name="load_company" value="{$item['load_company']}" maxlength="50" autocomplete="off">
												</td>
												<td class="fieldLabel alignMiddle">Gavėjas&nbsp;&nbsp;</td>
												<td class="fieldValue">
														<input style="width: 240px;" type="text" class="inputElement" name="unload_company" value="{$item['unload_company']}" autocomplete="off">
												</td>
											</tr>  

									<tr>
										<td class="fieldLabel alignMiddle">Siuntėjo kontaktas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="load_contact" value="{$item['load_contact']}" maxlength="50">
										</td>
										<td class="fieldLabel alignMiddle">Gavėjo kontaktas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" class="inputElement" name="unload_contact" value="{$item['unload_contact']}" maxlength="50">
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle">Siuntėjo telefonas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="cf_1564" value="{$item['load_phone']}" maxlength="50">
										</td>
										<td class="fieldLabel alignMiddle">Gavėjo telefonas&nbsp;&nbsp;</td><td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="cf_1566" value="{$item['unload_phone']}" maxlength="50">
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakrovimo adresas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="bill_street" value="{$item['bill_street']}" maxlength="255" data-require="true"  autocomplete="off" >
										</td>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Iškrovimo adresas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;"  type="text" class="inputElement" name="ship_street" value="{$item['ship_street']}" maxlength="255" data-require="true" >
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakrovimo miestas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement " name="bill_city" value="{$item['bill_city']}" maxlength="50" data-require="true">
										</td>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Iškrovimo miestas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement " name="ship_city" value="{$item['ship_city']}" maxlength="50" data-require="true">
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakr. p/k&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement enter_trig" name="bill_code" value="{$item['bill_code']}" minlength="4" maxlength="6" onpaste="isString(this);" onkeypress="isString(this);" onkeyup="isString(this);" data-require="true">
										</td>
										<td class="fieldLabel alignMiddle"><span class="redColor">*</span> Iškr. p/k&nbsp;&nbsp;</td>
										<td class="fieldValue">
												<input style="width: 240px;" type="text" class="inputElement enter_trig" name="ship_code" value="{$item['ship_code']}" minlength="4" maxlength="6" onpaste="isString(this);" onkeypress="isString(this,);" onkeyup="isString(this);" data-require="true">
										</td>
									</tr>

                  <tr>
                    <td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakrovimo šalis (LTU, LVA, EST, POL)&nbsp;&nbsp;</td>
                    <td class="fieldValue">
                      <input style="width: 240px;" type="text" class="inputElement " name="bill_country" value="{$item['bill_country']}" minlength="3" maxlength="3" onpaste="isInt(this);" onkeypress="isInt(this);" onkeyup="isInt(this);" data-require="true" autocomplete="off">
                    </td>
                    <td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Iškrovimo šalis (LTU, LVA, EST, POL)&nbsp;&nbsp;</td><td class="fieldValue">
                      <input style="width: 240px;" type="text" class="inputElement " name="ship_country" value="{$item['ship_country']}" minlength="3" maxlength="3" onpaste="isInt(this);" onkeypress="isInt(this);" onkeyup="isInt(this);" data-require="true">
                   </td>									
                  </tr>

									<tr>
									 <td class="fieldLabel alignMiddle">Neeksportuoti į metriką&nbsp;&nbsp;</td>
									 <td class="fieldLabel alignMiddle"><input type="checkbox" class="inputElement" name="metrika" value="1" {if $item['metrika'] eq 1}checked{/if}></td>

									 <td class="fieldLabel alignMiddle">Be sąskaitos	&nbsp;&nbsp;</td>
									 <td class="fieldLabel alignMiddle"><input type="checkbox" class="inputElement" name="withoutinvoice" value="1" {if $item['withoutinvoice'] eq 1}checked{/if}></td>
									</tr>

									<tr>
									 <td class="fieldLabel alignMiddle">Klientas atsisakė paslaugos	&nbsp;&nbsp;</td>
									 <td class="fieldLabel alignMiddle"><input type="checkbox" class="inputElement" name="dismisservice" value="1" {if $item['dismisservice'] eq 1}checked{/if}></td>

									 <td class="fieldLabel alignMiddle"></td>
									 <td class="fieldLabel alignMiddle"></td>
									</tr>

									</table>
	<div name='editContent' style="margin-top: 50px;">
		<div class='fieldBlockContainer'>
			<table class="table table-bordered" id="lineItemTab">	
				<tr>
					<th>Įrankiai</th>
					<th>
						<strong>
							<span style="margin-right: 20px;"></span>SVORIS
							<span style="margin-right: 10px;"></span> ILGIS 
							<span style="margin-right: 10px;"></span>PLOTIS
							<span style="margin-right: 10px;"></span>AUKŠTIS
						</strong>
					</th>
					<th>Tara</th>
					<th>Kiekis</th>	
					</tr>								
					<tr id="row0" class="hide lineItemCloneCopy" data-row-num="0">
						{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Accounts' row_no=0 data=[] IGNORE_UI_REGISTRATION=true}
					</tr>					
					{foreach key=row_no item=data from=$item['loads']}					
						<tr id="row{$row_no}" data-row-num="{$row_no}" class="lineItemRow">						
							{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Accounts' row_no=$row_no data=$data}
						</tr>						
					{/foreach}
					{if count($item['loads']) eq 0}
						<tr id="row1" class="lineItemRow" data-row-num="1">					
							{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Accounts' row_no=1 data=[] IGNORE_UI_REGISTRATION=false}
						</tr>									
					{/if}		
				</table>
		</div>
	</div>

  	<div class="btn-toolbar">     					                                   
						<span class="btn-group">
							<button type="button" class="btn btn-default addProduct" data-module-name="Products">
								<i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Pridėti krovinį</strong>
							</button>
						</span>	                              
					</div>	
					<br>

          	<div class="fieldBlockContainer">
			<table class="table table-bordered blockContainer lineItemTable" id="lineItemResult">
				<tr valign="top">				
						<td width="83%">
							<div class="pull-right">
								<strong>{vtranslate('LBL_PRICEBOOK_PRICE',$MODULE)}&nbsp;&nbsp;</strong>
							</div>
						</td>
						<td>
							<span id="pricebookprice" class="pull-right pricebookprice">{if $item['pricebook_price']}{$item['pricebook_price']}{else}0{/if}</span>
							<input type="hidden" name="pricebookprice" value="{if $item['total']}{$item['total']}{else}0{/if}">
						</td>				
				</tr>

				<tr>			
					<td>
						<div class="pull-right">
							<strong>{vtranslate('LBL_AGREED_PRICE',$MODULE)}&nbsp;&nbsp;</strong>
						</div>	
					</td>
					<td>
						<span class="pull-right">
							<input type="text" id="agreedprice" name="agreedprice" class="form-control lineItemInputBox" value="{if $item['agreed_price']}{$item['agreed_price']}{else}0{/if}">
						</span>
					</td>						
				</tr>
				<tr valign="top">
					<td width="83%">					
						<span class="pull-right"><strong>{vtranslate('LBL_GRAND_TOTAL',$MODULE)}</strong></span>			
					</td>			
				
					<td>
						<span id="grandTotal" name="grandTotal" class="pull-right grandTotal">{if $item['price']}{$item['price']}{else}0{/if}</span>
						<input type="hidden" name="total" value="{if $item['price']}{$item['price']}{else}0{/if}">
					</td>
				</tr>
			
			</table>
		</div> 
							</div>
							<div class="modal-footer">
								<input type="hidden" id="createorders" name="createorders" value="0">
								<input type="hidden" id="totalProductCount" name="totalProductCount" value="{count($item['loads'])}">		
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
								<button type="button" class="btn btn-primary saveTemplateBtn" onclick="editTemplate(this,{$item['record']});">Redaguoti</button>
							</div>
						</div>
					</div>
				</div>
				</form>
	{/foreach}

{/if}



<form id="templateForm" method="POST">
				<div class="modal fade" id="orderFormModal" tabindex="-1" role="dialog" aria-labelledby="orderFormModalTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document" style="width: 1100px;">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="orderFormModalTitle">Pardavimo užsakymo forma</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div> 
							<div class="modal-body">               
								<input type="hidden" id="edit" name="edit" value="0">				
								<input type="hidden" id="template_id" name="template_id" value="0">				
									<table class="table detailview-table no-border">
                  <tr>
                   	<td class="fieldLabel alignMiddle">Intervalo tipas</td>
                    <td class="fieldValue">
                      <select style="width:240px;" class="inputElement execute_type" name="execute_type">
                        <option value="1">Savaitės diena</option>
                        <option value="2">Mėnesio diena</option>
                        <option value="3">Kiekviena diena</option>
                      </select>
                    </td>

                    <td class="fieldLabel alignMiddle">Kartoti kiekvieną</td>
                    <td class="fieldValue">
											<div class="input-group inputElement" style="min-width: 100px;width: 100px;border:0;">

											{$weekDays = ['Monday' => 'Pirmadienį', 'Tuesday' => 'Antradienį', 'Wednesday' => 'Trečiadienį', 'Thursday' => 'Ketvirtadienį', 'Friday' => 'Penktadienį', 'Saturday' => 'Šestadienį', 'Sunday' => 'Sekmadienį']}
											
											<select style="width:100px;" id="weekDays" class="inputElement" name="execute[]" multiple>
											{foreach from=$weekDays item=item key=key}
												<option value="{$key}">{$item}</option>
											{/foreach}								
											</select>			

											<select style="width:100px;" id="monthDays" class="inputElement hide" name="execute2[]" multiple>
														{for $day=1 to 31} 
																<option value="{$day}">{$day}</option>
														{/for}
											</select>              
											</div>
											<div class="input-group inputElement time" style="margin-left: 10px;width:84px !important; min-width:84px !important;height: 27px;">
													<input style="width:84px !important;height: 27px;" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="08:00" name="execute_time" autocomplete="off">
													<span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
                    </td>
                  </tr>


									<tr>
											<td class="fieldLabel alignMiddle"><span class="redColor">*</span> Užsakymo tipas</td>
											<td class="fieldValue">
												<select class="inputElement" name="cf_855" style="width:240px;">
													<option value="Transporto užsakymas" {if $item['order_type'] eq 'Transporto užsakymas'}selected{/if}>Transporto užsakymas</option>
													<option value="Perkraustymo užsakymas" {if $item['order_type'] eq 'Perkraustymo užsakymas'}selected{/if}>Perkraustymo užsakymas</option>
													<option value="Sandėliavimo užsakymas" {if $item['order_type'] eq 'Sandėliavimo užsakymas'}selected{/if}>Sandėliavimo užsakymas</option>
												</select>
											</td>
											<td class="fieldLabel alignMiddle">Sukurti nuo:</td>
											<td><div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;">
											<input id="create_from" type="text" data-calendar-type="date" class="dateField form-control" data-fieldtype="date" name="create_from" data-date-format="yyyy-mm-dd" value="" data-field-type="date" aria-required="true" aria-invalid="false"><span class="input-group-addon"><i class="fa fa-calendar "></i></span>
										</div></td>														
										</tr>

										<tr>
											<td class="fieldLabel alignMiddle"></td>
											<td class="fieldValue"></td>
											<td class="fieldLabel alignMiddle">Sukurti iki:</td>
											<td><div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;">
											<input id="create_untill" type="text" data-calendar-type="date" class="dateField form-control" data-fieldtype="date" name="create_untill" data-date-format="yyyy-mm-dd" value="" data-field-type="date" aria-required="true" aria-invalid="false"><span class="input-group-addon"><i class="fa fa-calendar "></i></span>
										</div></td>												
										</tr>										
										<tr>
											<td class="fieldLabel alignMiddle">Pakrovimo atidėjimas ir laikas nuo iki&nbsp;&nbsp;</td>

											<td class="fieldValue">
												<small style="color: green;">Pakrovimo data bus kartojimo data plius atidėjimas</small>
												<div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;border:0">
														<select style="width:240px;" class="inputElement" name="load_delay">
															{for $day=0 to 15} 
																	<option value="{$day}">{$day}</option>
															{/for}
														</select>   
												</div>
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;">
													<input style="width:84px !important;" id="SalesOrder_editView_fieldName_load_time_from" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="" name="load_time_from" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
													
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;margin-left:10px;">
													<input style="width:84px !important;" id="SalesOrder_editView_fieldName_load_time_to" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="" name="load_time_to" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
													</div>
											</td>	

											<td class="fieldLabel alignMiddle">Iškrovimo atidėjimas ir laikas nuo iki&nbsp;&nbsp;</td>

											<td class="fieldValue">
												<small style="color: green;">Iškrovimo data bus kartojimo data plius atidėjimas</small>
												<div class="input-group inputElement" style="margin-bottom: 3px;min-width: 100px;width: 240px;border:0">
														<select style="width:240px;" class="inputElement" name="unload_delay">
															{for $day=1 to 15} 
																	<option value="{$day}">{$day}</option>
															{/for}
														</select>   
												</div>
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;">
													<input style="width:84px !important;" id="SalesOrder_editView_fieldName_unload_time_from" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="" name="unload_time_from" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
												<div class="input-group inputElement time" style="width:84px !important; min-width:84px !important;margin-left: 10px;">
													<input style="width:84px !important;" id="SalesOrder_editView_fieldName_unload_time_to" type="text" data-format="24" class="timepicker-default form-control ui-timepicker-input" value="" name="unload_time_to" autocomplete="off"><span class="input-group-addon" style="width: 30px;"><i class="fa fa-clock-o"></i></span>
												</div>
											</td>
											</tr>

											<tr>
												<td class="fieldLabel alignMiddle">Siuntėjas&nbsp;&nbsp;</td>
												<td class="fieldValue">	
													<input style="width: 240px;" type="text" class="inputElement" name="load_company" value="" maxlength="50" autocomplete="off">
												</td>
												<td class="fieldLabel alignMiddle">Gavėjas&nbsp;&nbsp;</td>
												<td class="fieldValue">
														<input style="width: 240px;" type="text" class="inputElement" name="unload_company" value="" autocomplete="off">
												</td>
											</tr>  

									<tr>
										<td class="fieldLabel alignMiddle">Siuntėjo kontaktas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="load_contact" value="" maxlength="50">
										</td>
										<td class="fieldLabel alignMiddle">Gavėjo kontaktas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="unload_contact" value="" maxlength="50">
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle">Siuntėjo telefonas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="cf_1564" value="" maxlength="50">
										</td>
										<td class="fieldLabel alignMiddle">Gavėjo telefonas&nbsp;&nbsp;</td><td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="cf_1566" value="" maxlength="50">
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakrovimo adresas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="bill_street" value="" maxlength="255" data-require="true"  autocomplete="off" >
										</td>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Iškrovimo adresas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement" name="ship_street" value="" maxlength="255" data-require="true" autocomplete="off">
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakrovimo miestas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement " name="bill_city" value="" maxlength="50" data-require="true">
										</td>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Iškrovimo miestas&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement " name="ship_city" value="" maxlength="50" data-require="true">
										</td>
									</tr>

									<tr>
										<td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakr. p/k&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement enter_trig" name="bill_code" value="" minlength="4" maxlength="6" onpaste="isString(this);" onkeypress="isString(this);" onkeyup="isString(this);" data-require="true">
										</td>
										<td class="fieldLabel alignMiddle"><span class="redColor">*</span> Iškr. p/k&nbsp;&nbsp;</td>
										<td class="fieldValue">
											<input style="width: 240px;" type="text" class="inputElement enter_trig" name="ship_code" value="" minlength="4" maxlength="6" onpaste="isString(this);" onkeypress="isString(this);" onkeyup="isString(this);" data-require="true">
										</td>
									</tr>

                  <tr>
                    <td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Pakrovimo šalis (LTU, LVA, EST, POL)&nbsp;&nbsp;</td>
                    <td class="fieldValue">
                      <input style="width: 240px;" type="text" class="inputElement " name="bill_country" value="LTU" minlength="3" maxlength="3" onpaste="isInt(this);" onkeypress="isInt(this);" onkeyup="isInt(this);" data-require="true" autocomplete="off">
                    </td>
                    <td class="fieldLabel alignMiddle"> <span class="redColor">*</span> Iškrovimo šalis (LTU, LVA, EST, POL)&nbsp;&nbsp;</td><td class="fieldValue">
                      <input style="width: 240px;" type="text" class="inputElement " name="ship_country" value="LTU" minlength="3" maxlength="3" onpaste="isInt(this);" onkeypress="isInt(this);" onkeyup="isInt(this);" data-require="true">
                   </td>
                  </tr>

									<tr>
									 <td class="fieldLabel alignMiddle">Neeksportuoti į metriką&nbsp;&nbsp;</td>
									 <td class="fieldLabel alignMiddle"><input type="checkbox" class="inputElement" name="metrika" value="1"></td>

									 <td class="fieldLabel alignMiddle">Be sąskaitos	&nbsp;&nbsp;</td>
									 <td class="fieldLabel alignMiddle"><input type="checkbox" class="inputElement" name="withoutinvoice" value="1"></td>
									</tr>

									<tr>
									 <td class="fieldLabel alignMiddle">Klientas atsisakė paslaugos	&nbsp;&nbsp;</td>
									 <td class="fieldLabel alignMiddle"><input type="checkbox" class="inputElement" name="dismisservice" value="1"></td>

									 <td class="fieldLabel alignMiddle"></td>
									 <td class="fieldLabel alignMiddle"></td>
									</tr>
									</table>
                  
	<div name='editContent' style="margin-top: 50px;">
		<div class='fieldBlockContainer'>
			<table class="table table-bordered" id="lineItemTab">	
				<tr>
					<th>Įrankiai</th>
					<th>
						<strong>
							<span style="margin-right: 20px;"></span>SVORIS
							<span style="margin-right: 10px;"></span> ILGIS 
							<span style="margin-right: 10px;"></span>PLOTIS
							<span style="margin-right: 10px;"></span>AUKŠTIS
						</strong>
					</th>
					<th>Tara</th>
					<th>Kiekis</th>	
					</tr>								
					<tr id="row0" class="hide lineItemCloneCopy" data-row-num="0">
						{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Accounts' row_no=0 data=[] IGNORE_UI_REGISTRATION=true}
					</tr>					
					{foreach key=row_no item=data from=$RELATED_PRODUCTS}
						<tr id="row{$row_no}" data-row-num="{$row_no}" class="lineItemRow" {if $data["entityType$row_no"] eq 'Products'}data-quantity-in-stock={$data["qtyInStock$row_no"]}{/if}>
							{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Accounts' row_no=$row_no data=$data}
						</tr>						
					{/foreach}
					{if count($RELATED_PRODUCTS) eq 0}
						<tr id="row1" class="lineItemRow" data-row-num="1">					
							{include file="partials/LineItemsContent.tpl"|@vtemplate_path:'Accounts' row_no=1 data=[] IGNORE_UI_REGISTRATION=false}
						</tr>									
					{/if}		
				</table>
		</div>
	</div>

  	<div class="btn-toolbar">     					                                   
						<span class="btn-group">
							<button type="button" class="btn btn-default addProduct" id="addProduct" data-module-name="Products" {if $MODULE eq 'Invoice'}disabled{/if}>
								<i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Pridėti krovinį</strong>
							</button>
						</span>	                              
					</div>	
					<br>

          	<div class="fieldBlockContainer">
			<table class="table table-bordered blockContainer lineItemTable" id="lineItemResult">
				<tr valign="top">				
						<td width="83%">
							<div class="pull-right">
								<strong>{vtranslate('LBL_PRICEBOOK_PRICE',$MODULE)}&nbsp;&nbsp;</strong>
							</div>
						</td>
						<td>
							<span id="pricebookprice" class="pull-right pricebookprice">{if $FINAL.pricebook_price}{$FINAL.pricebook_price}{else}0{/if}</span>
							<input type="hidden" name="pricebookprice" value="{if $FINAL.total}{$FINAL.total}{else}0{/if}">
						</td>				
				</tr>

				<tr>			
					<td>
						<div class="pull-right">
							<strong>{vtranslate('LBL_AGREED_PRICE',$MODULE)}&nbsp;&nbsp;</strong>
						</div>	
					</td>
					<td>
						<span class="pull-right">
							<input type="text" id="agreedprice" name="agreedprice" class="form-control lineItemInputBox" value="{if $FINAL.agreed_price}{$FINAL.agreed_price}{else}0{/if}">
						</span>
					</td>						
				</tr>
				<tr valign="top">
					<td width="83%">					
						<span class="pull-right"><strong>{vtranslate('LBL_GRAND_TOTAL',$MODULE)}</strong></span>			
					</td>			
				
					<td>
						<span id="grandTotal" name="grandTotal" class="pull-right grandTotal">{if $FINAL.total}{$FINAL.total}{else}0{/if}</span>
						<input type="hidden" name="total" value="{if $FINAL.total}{$FINAL.total}{else}0{/if}">
					</td>
				</tr>			
			</table>
		</div> 
							</div>
							<div class="modal-footer">
							<input type="hidden" id="createorders" name="createorders" value="0">
							 	<input type="hidden" id="totalProductCount" name="totalProductCount">		
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
								<button type="button" class="btn btn-primary saveTemplateBtn">Sukurti</button>
							</div>
						</div>
					</div>
				</div>
</form>

{/strip}