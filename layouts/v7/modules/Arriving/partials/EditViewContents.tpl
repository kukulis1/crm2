{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
********************************************************************************/
-->*}
{strip}
	{assign var=MEASURES value=$MEASURES}
	{assign var=WAREHOUSES value=$WAREHOUSES}
	{$MAIN_TARES = array(1,2,3,13,14)}

	<style>
		.table-label {
			border: 1px solid #cccccc;
			padding: 9px;
			width: 100%;
			margin-bottom: 10px;
		}

		.col-gaps {
			margin: 0 10px;
		}

		.tr-border {
			border: 1px solid #ddd;
			border-bottom: 2px solid #0017ff;
		}

		.goods-tr {
			border: 1px solid #d1cccc;
    	background: #eee;
		}

		
	</style>

	<div name='editContent'>
		{if $DUPLICATE_RECORDS}
			<div class="fieldBlockContainer duplicationMessageContainer">
				<div class="duplicationMessageHeader"><b>{vtranslate('LBL_DUPLICATES_DETECTED', $MODULE)}</b></div>
				<div>{getDuplicatesPreventionMessage($MODULE, $DUPLICATE_RECORDS)}</div>
			</div>
		{/if}

		<div class='fieldBlockContainer' data-block="{$BLOCK_LABEL}">
		<h4 class='fieldBlockHeader'>Pridėti naują</h4>
		<hr>

			<table class="table table-borderless">
				<tr>
					<td class="fieldLabel alignMiddle">Manifesto nr</td>
					<td class="fieldValue"><input type="text" class="inputElement" name="MANIFEST_NO" autoComplete="off" required>
						<input type="checkbox" name="generateManifest" class="inputElement" style="margin-left: 10px;"> 
						<span style="color: #2c3b49;opacity: 0.8;padding-left: 5px;">Sugeneruoti automatiškai</span></td>					
					<td class="fieldLabel alignMiddle">Manifesto data</td>
					<td class="fieldValue"><input type="text" class="inputElement dateField" data-date-format="yyyy-mm-dd" name="MANIFEST_DATE" autoComplete="off" required></td>
			
			</tr>
			</table>
		<div id="wait" style="display:none;position:absolute;top: 50%;left:45%;padding:2px;z-index: 1001;"><img src="/resources/loading.gif"></div>	
		<label>Prekių savininkas</label> 
			<table class="table table-borderless">
				<tr>
					<td class="fieldLabel alignMiddle">Kliento pavadinimas <span class="redColor">*</span></td>
					<td class="fieldValue">
						<div class="referencefield-wrapper">
							<input name="popupReferenceModule" type="hidden" value="Accounts">
							<div class="input-group">
								<input name="account_id" type="hidden" value="" class="sourceField">
								<input name="account_id_display" data-fieldname="account_id" data-fieldtype="reference" type="text" class="marginLeftZero autoComplete inputElement ui-autocomplete-input" value="" placeholder="Rašykite, norėdami pasinaudoti paieška" data-rule-required="true" data-rule-reference_required="true" autocomplete="off" aria-required="true" aria-invalid="true" data-hasqtip="12" aria-describedby="qtip-12"><a href="#" class="clearReferenceSelection hide"> x </a>
								 <span class="input-group-addon relatedPopup cursorPointer" title="{vtranslate('LBL_SELECT', $MODULE)}">
                <i id="{$MODULE}_editView_fieldName_{$FIELD_NAME}_select" class="fa fa-search"></i></span>
            </span>
							</div>
							<input id="customer_id" type="hidden" value="">						
						</div>
					</td>	
					<td class="fieldLabel alignMiddle">Parnaso transportas</td>
					<td class="fieldValue"><input type="checkbox" name="REQUIRED_TRANSPORT"></td>		
				</tr>	
			</table>

		<div id="load_section" style="display:none;">
		<table class="table table-borderless">	
				<tr>
					<td class="fieldLabel alignMiddle" style="opacity: 1;padding-left: 0px;"><span style="font-weight: 700;">Pakrovimas</span></td>
					<td class="fieldValue"><input type="text" class="inputElement dateField" data-date-format="yyyy-mm-dd" name="LOAD_DATE" autoComplete="off"></td>
					<td></td>
					<td></td>
			</tr>
				<tr>
					<td class="fieldLabel alignMiddle">Siuntėjas</td>					
					<td class="fieldValue"><input type="text" class="inputElement" name="SENDER_NAME"></td>	

					<td class="fieldLabel alignMiddle">Šalis</td>
					<td class="fieldValue">
						<select name="SENDER_COUNTRY" class="inputElement">
							<option value="LTU">LTU</option>
							<option value="LVA">LVA</option>
							<option value="EST">EST</option>
						</select>					
					</td>
				</tr>

			<tr>
				<td class="fieldLabel alignMiddle">Adresas</td>
				<td class="fieldValue"><input type="text" class="inputElement" name="SENDER_ADDRESS"></td>

				<td class="fieldLabel alignMiddle">Miestas/Pašto kodas</td>
				<td class="fieldValue"><input type="text" class="inputElement" name="SENDER_CITY" style="width: 130px;"> <input type="text" class="inputElement" name="SENDER_POST_CODE" style="width: 70px;"></td>
			</tr>	
			
			<tr>
			<td class="fieldLabel alignMiddle">Asmuo</td>
			<td class="fieldValue"><input type="text" class="inputElement" name="SENDER_CONTACT"></td>

			<td class="fieldLabel alignMiddle">Telefonas</td>
			<td class="fieldValue"><input type="text" class="inputElement" name="SENDER_PHONE"></td>
		</tr>	
			</table>
		</div>

			<label>Krovinys ir kiti duomenys</label> 
			<table class="table table-borderless">
				<tr>
					<td class="fieldLabel alignMiddle">Transporto nr.</td>
					<td class="fieldValue"><input type="text" class="inputElement" name="TRANSPORT_NO"></td>

					<td class="fieldLabel alignMiddle">Vairuotojas</td>
					<td class="fieldValue"><input type="text" class="inputElement" name="DRIVER"></td>			
				</tr>	

				<tr>
					<td class="fieldLabel alignMiddle">Pastabos</td>
					<td class="fieldValue"><textarea class="inputElement" name="NOTE"></textarea></td>

					<td class="fieldLabel alignMiddle">Vežėjas</td>
					<td class="fieldValue"><input type="text" class="inputElement" name="TRANSPORT"></td>
				</tr>	
			</table>
			<table class="table table-borderless">
				<tr>			
					<td class="fieldLabel alignMiddle">Sandėlis <span class="redColor">*</span></td>
					<td class="fieldValue">
						<select class="inputElement" name="WAREHOUSE_NME">
						{foreach from=$WAREHOUSES item=WAREHOUSE}				
							<option value="{$WAREHOUSE['METRIKA_ID']}">{$WAREHOUSE['WAREHOUSE_NME']}</option>
						{/foreach}
						</select>
						<input type="hidden" name="WAREHOUSE_TEXT" value="Vilnius, Metalo g. 2">
					</td>
					<td class="fieldLabel alignMiddle">Iškrovimas</td>
					<td class="fieldValue"><input type="text" class="inputElement dateField" data-date-format="yyyy-mm-dd" name="UNLOAD_DATE" autoComplete="off" required></td>
				</tr>
				<tr>
					<td class="fieldLabel alignMiddle">Krovinio numeris</td>
					<td class="fieldValue"><input type="text" class="inputElement" name="CARGO_NO" autoComplete="off" required></td>

					<td></td>
					<td></td>
				</tr>
			</table>

<label class="table-label">Prekės informacija iš kortelės 
<span style="font-weight:100;float:right;">Prekių kiekis taikomas:&nbsp;
 	<input type="radio" name="qty_type" value="1" checked>
  <label for="html">Viskam</label>
  <input type="radio" name="qty_type" value="2">
  <label for="css">Paletei</label>
</span> 
</label> 
	


<div style="display:none;">
	<select class="inputElement" id="hiddenTares">
		{foreach from=$MEASURES item=MESS}			
				<option value="{$MESS['id']}">{$MESS['code']}</option>		
		{/foreach}
	</select>
</div>


<input type="hidden" id="tares_json" value='{json_encode($MEASURES)}'>
<input type="hidden" id="goods_json" value="">
<input type="hidden" id="rests_json" value="">

<div class="modal fade" id="goodsModal" tabindex="-1" role="dialog" aria-labelledby="goodsModalLabel" aria-hidden="true">
	<input type="hidden" id="is_editing" value="0">
	<input type="hidden" id="whish_editing">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width: 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="goodsModalLabel">Prekės</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<span id="alert" style="color:red;"></span>
       <table class="table table-borderless" style="margin-bottom:0;">
				<thead>
					<tr>		
						<th>Tara</th>
						<th style="width:50px;">Kiekis</th>
						<th>Barkodas</th>		
						<th>Svoris</th>					
						<th>Ilgis</th>					
						<th>Plotis</th>					
						<th>Aukštis</th>				
					</tr>		
				</thead>		
	
				<tbody id="good_body">
					<tr class="lineItem">
						<td>							
							<select class="inputElement" id="Packing1" onchange="initTareSelector(event);" style="width: 100%;">																
								{foreach from=$MEASURES item=MESS}
									{if in_array($MESS['id'],$MAIN_TARES)}
										<option value="{$MESS['id']}">{$MESS['code']}</option>
									{/if}
								{/foreach}
							</select>
						</td>
						<td><input type="text" class="inputElement" id="qty1" value="1" style="width:50px;" onkeypress="return isNumber(event);" onpaste="return isNumber(event);" onkeyup="return isNumber(event);"></td>
						<td><input type="text" class="inputElement" id="barcode1"></td>
						<td><input type="text" class="inputElement" id="weight" style="width: 70px;" onkeypress="return isNumber(event);" onpaste="return isNumber(event);" onkeyup="return isNumber(event);"></td>
						<td><input type="text" class="inputElement" id="length" style="width: 50px;" onkeypress="return isNumber(event);" onpaste="return isNumber(event);" onkeyup="return isNumber(event);" value="0.8"></td>
						<td><input type="text" class="inputElement" id="width" style="width: 50px;" onkeypress="return isNumber(event);" onpaste="return isNumber(event);" onkeyup="return isNumber(event);" value="1.2"></td>
						<td><input type="text" class="inputElement" id="height" style="width: 50px;" onkeypress="return isNumber(event);" onpaste="return isNumber(event);" onkeyup="return isNumber(event);"></td>
					</tr>					
				</tbody>	
			</table>

			  <table class="table table-borderless hide" id="goods_table" style="width: 80%;margin-left:80px;">
					<thead>
						<tr>
							<th style="width: 60%;">Prekė</th>
							<th>Kiekis</th>
							<th>Kodas</th>
							<th>Svoris</th>										
							<th></th>										
						</tr>						
					</thead>
					<tbody id="goods-tbody"></tbody>	
				</table>
      </div>
      <div class="modal-footer" style="text-align:unset;">
						<div class="row">
							<div class="col-sm-6">							
								<button type="button" class="btn btn-default addTare" onclick="initAddTare();">Pridėti tarą</button>
								<button type="button" class="btn btn-success addGood" onclick="initGetGoods();">Pridėti prekę</button>
							</div>
							<div class="col-sm-6">
								<button type="button" class="btn btn-secondary" onclick="deleteLine();">Ištrinti paskutinę eilutę</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="clearModal();">Atšaukti</button>
								<button type="button" class="btn btn-primary save_btn" onclick="saveGoods();">Išsaugoti</button>
							</div>
					</div>
      </div>
    </div>
  </div>
</div>
	<table class="table table-borderless" id="goods_content">
		<thead>
			<tr>		
				<th>Tara/Prekė</th>
				<th>Kiekis</th> 
				<th>Barkodas</th>
				<th>Matmenys</th>
				<th></th>
			</tr>		
		</thead>	
	</table>
			<input type="hidden" name="groups_count" value="1">
			<button type="button" class="btn btn-primary" onclick="openGoodsModal();">Komplektuoti prekes</button>
			{* <button type="button" class="btn btn-warning deleteRow" onclick="deleteRow();" disabled>Ištrinti paskutinę eilutę</button> *}
		</div>
	</div>

	<div style="height:200px;"></div>
{/strip}
