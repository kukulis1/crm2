Mokėjimų vidurkis (Mediana): {$MEDIAN}
<div>Bendros pajamos: <span style="font-weight: bold" id="relatedListHeaderStatisticsTotalIncome"></span></div>
<div>Vidutinės pajamos per mėnesį: <span style="font-weight: bold" id="relatedListHeaderStatisticsAverageIncome"></span></div>
<div>Užsakymų kiekis viso: <span style="font-weight: bold" id="relatedListHeaderStatisticsOrdersCount"></span></div>
<div>Užsakymų vidurkis per mėnesį: <span style="font-weight: bold" id="relatedListHeaderStatisticsAverageOrdersCount"></span></div>
{*<div>This month income: <span style="font-weight: bold" id="relatedListHeaderStatistics_ThisMonthIncome"></span></div>*}
{*<div>Previous month income: <span style="font-weight: bold" id="relatedListHeaderStatistics_PreviousMonthIncome"></span></div>*}
{*<div>This month orders count: <span style="font-weight: bold" id="relatedListHeaderStatistics_ThisMonthOrdersCount"></span></div>*}
{*<div>Previous month orders count: <span style="font-weight: bold" id="relatedListHeaderStatistics_PreviousMontOrdersCount"></span></div>*}
<div>Pajamų pokytis per paskutinį mėnesį: <span style="font-weight: bold" id="relatedListHeaderStatistics_IncomeChange"></span></div>
<div>Užsakymų kiekio pokytis per paskutinį mėnesį: <span style="font-weight: bold" id="relatedListHeaderStatistics_OrdersCountChange"></span></div>


{*record = {$smarty.get.record}*}

<script type="text/javascript">
    jQuery(document).ready(function () {
        $.ajax({
            type: "GET",
            url: 'modules/Accounts/ajax/getaccountinvoicestatistics.php',
            data: { 'accountid': {$smarty.get.record} },
            dataType: 'JSON',
            success: function (res) {
                jQuery('#relatedListHeaderStatisticsTotalIncome').text( res.data.total_income);
                jQuery('#relatedListHeaderStatisticsAverageIncome').text( res.data.average_income);
                jQuery('#relatedListHeaderStatisticsOrdersCount').text( res.data.orders_count);
                jQuery('#relatedListHeaderStatisticsAverageOrdersCount').text( res.data.average_orders_count);

                // jQuery('#relatedListHeaderStatistics_ThisMonthIncome').text( res.data.this_month_income);
                // jQuery('#relatedListHeaderStatistics_PreviousMonthIncome').text( res.data.previous_month_income);
                // jQuery('#relatedListHeaderStatistics_ThisMonthOrdersCount').text( res.data.this_month_orders_count);
                // jQuery('#relatedListHeaderStatistics_PreviousMontOrdersCount').text( res.data.previous_month_orders_count);
                jQuery('#relatedListHeaderStatistics_IncomeChange').text( res.data.income_month_change);
                jQuery('#relatedListHeaderStatistics_OrdersCountChange').text( res.data.orders_count_month_change);
            }
        });
    });
</script>
