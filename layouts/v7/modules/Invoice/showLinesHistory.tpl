{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
{assign var=HISTORY value=$HISTORY}
{assign var=HISTORY_TAGS value=$HISTORY_TAGS}
{assign var=HISTORY_EDIT_TAGS value=$HISTORY_EDIT_TAGS}

	<div class="summaryWidgetContainer">
		<div class="widget_header clearfix">
			{* <h4 class="display-inline-block pull-left">Eilučių istorija</h4> *}
		</div>
		<div class="widget_contents">

{if count($HISTORY['edit']) neq '0'}
 {foreach item=HIS from=$HISTORY['edit']} 
    <h4>Atnaujinta {$HIS['time']}</h4>
    <table class="table table-bordered">
    <tbody>    
      <tr>		  
        <th>#</th>  
        <th>Pakrovimas/Paslauga</th>
        <th>Iškrovimas</th>
        <th>Pastaba</th>
        <th>Kaštų centras</th>
        <th style="text-align:center;">Krovinys</th>
        <th style="text-align:center;">Svoris, kg.</th>
        <th style="text-align:center;">Tūris, m3</th>		
        <th><span class="pull-right">Suma</span></th>	  	
        </tr>     
        {foreach item=line key=index from=$HIS['data']}       
          <tr class="lineItemRow">  
            <td>{$index+1}</td>
            <td style="{if $HISTORY_TAGS[$HIS['time']][$index]['bill_address'] OR 
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['bill_address'] OR
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['service']}background:yellow;{/if}">
            {if $line['type'] eq '14244'} {$line['bill_address']}{else}{$line['service']}{/if}
            </td>

            <td style="{if $HISTORY_TAGS[$HIS['time']][$index]['ship_address'] OR 
             $HISTORY_EDIT_TAGS[$HIS['time']][$index]['ship_address'] }background:yellow;{/if}">{$line['ship_address']}</td>

            <td style="{if $HISTORY_TAGS[$HIS['time']][$index]['note'] OR 
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['note']}background:yellow;{/if}">
              {if !empty($line['note'])} {$line['note']}{else}---{/if}</td>

            <td style="{if $HISTORY_TAGS[$HIS['time']][$index]['costcenter'] OR 
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['costcenter']}background:yellow;{/if}">{$line['costcenter']}</td>

            <td style="text-align:center;{if $HISTORY_TAGS[$HIS['time']][$index]['cargo_measure'] OR 
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['cargo_measure']}background:yellow;{/if}">{$line['cargo_measure']}</td>

            <td style="text-align:center;{if $HISTORY_TAGS[$HIS['time']][$index]['cargo_wgt'] OR 
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['cargo_wgt']}background:yellow;{/if}">{$line['cargo_wgt']}</td>

            <td style="text-align:center;{if $HISTORY_TAGS[$HIS['time']][$index]['cargo_volume'] OR 
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['cargo_volume']}background:yellow;{/if}">{$line['cargo_volume']}</td>

            <td style="white-space: nowrap;{if $HISTORY_TAGS[$HIS['time']][$index]['price'] OR 
            $HISTORY_EDIT_TAGS[$HIS['time']][$index]['price']}background:yellow;{/if}">{$line['price']}</td>
          </tr>
        {/foreach}  
    </tbody>
    </table>
  {/foreach}
{/if}


{if count($HISTORY['new']) neq '0'}
 {foreach item=HIS from=$HISTORY['new']} 
    <h4>Sukurta {$HIS['time']}</h4>
    <table class="table table-bordered">
    <tbody>    
      <tr>		    
        <th>#</th>
        <th>Pakrovimas/Paslauga</th>
        <th>Iškrovimas</th>
        <th>Pastaba</th>
        <th>Kaštų centras</th>
        <th style="text-align:center;">Krovinys</th>
        <th style="text-align:center;">Svoris, kg.</th>
        <th style="text-align:center;">Tūris, m3</th>		
        <th><span class="pull-right">Suma</span></th>	  	
        </tr>     
        {foreach item=line key=key from=$HIS['data']}       
          <tr class="lineItemRow">  
            <td>{$key+1}</td>
            <td>{if $line['type'] eq '14244'} {$line['bill_address']}{else}{$line['service']}{/if}</td>
            <td>{$line['ship_address']}</td>
            <td style="text-align:center;">{if !empty($line['note'])} {$line['note']}{else}---{/if}</td>
            <td>{$line['costcenter']}</td>
            <td style="text-align:center;">{$line['cargo_measure']}</td>
            <td style="text-align:center;">{$line['cargo_wgt']}</td>
            <td style="text-align:center;">{$line['cargo_volume']}</td>
            <td style="white-space: nowrap;">{$line['price']}</td>
          </tr>
        {/foreach}  
    </tbody>
    </table>
  {/foreach}

{else}
  <div class="summaryWidgetContainer noContent">
    <p class="textAlignCenter">Istorijos nėra</p>
  </div>
{/if}

	</div>
</div>
{/strip}