{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
<link rel="stylesheet" href="layouts/v7/modules/Claims/resources/style.css" type="text/css" />
<script src="layouts/v7/modules/Claims/resources/main.js"></script>
<form id="detailView" method="POST" enctype="multipart/form-data">
    {include file='DetailViewBlockView.tpl'|@vtemplate_path:$MODULE_NAME RECORD_STRUCTURE=$RECORD_STRUCTURE MODULE_NAME=$MODULE_NAME}
    {include file='LineComments.tpl'|@vtemplate_path:$MODULE_NAME}
    {include file='LineEmailMessages.tpl'|@vtemplate_path:$MODULE_NAME}
</form>
