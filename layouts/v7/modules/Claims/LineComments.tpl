{assign var=COMMENTS value=$GET_COMMENTS}


<div class="editViewContents">
<div class="fieldBlockContainer">
<h4 class="fieldBlockHeader">Komentarai</h4>
<hr>  

  <textarea rows="3" class="inputElement" name="comment" style="margin: 0px; width: 100%; height: 70px;" placeholder="Rašyti komentarą"></textarea>
  <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="document.getElementById('detailView').submit();">Rašyti</button>


<hr style="margin-top: 20px;margin-bottom: 20px;"> 

   {foreach item=COMMENT from=$COMMENTS}
       <div class="container" style="background-color: transparent; color: #555;"> 
        <p>Parašė: {$COMMENT['first_name']} {$COMMENT['last_name']}</p>
        <span class="time-right">{$COMMENT['timestamp']}</span>
        <span class="time-left">{$COMMENT['comment']}</span>
      </div>
   {/foreach}
      
    </div>
 </div>
