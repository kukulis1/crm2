{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}



<div class="dashboardWidgetHeader clearfix">
    <div class="title">
        <div class="dashboardTitle" title="{vtranslate($WIDGET->getTitle(), $MODULE_NAME)}"><b>&nbsp;&nbsp;{vtranslate($WIDGET->getTitle())}</b></div>
    </div>
</div>

<p>Viso pretenzijų: {$CLAIMS['count']}  Priimta: {$CLAIMS['accepted']}  Atmesta: {$CLAIMS['rejected']}</p>
<p><b>Skyrius: <span style="margin-left: 5px;">Pretenzijų kiekis</span>  / <span style="margin-left: 5px;">Viso suma</span> / <span style="margin-left: 5px;">Viso patvirtinta</span></b></p>
{foreach  item=item from=$CLAIMS['section']} 
    <p style="display:inline-flex; margin-right: 10px;">{$item['section']}: <span title="Viso pretenzijų">{$item['count']}</span> / <span title="Viso suma" style="margin-left: 5px;">{$item['total']}€</span> / <span title="Viso patvirtinta" style="margin-left: 5px;">{$item['payout']}€</span></p>
{/foreach}
<p>Viso pretenzijų suma: {$CLAIMS['SUM']} &nbsp;&nbsp;&nbsp; Priimta mokėjimui suma: {$CLAIMS['payout']}  iš jų moka draudimas: {$CLAIMS['insurance']}</p>
<h6><b>Pretenzijų autoriai</b></h6>
<div style="height:200px;overflow:auto;">
    {foreach from=$CLAIMS['couser'] key=name item=SUM} 
    <p><b>Sukėlė</b>: {if {$SUM['employid']} > 0}<a target="_blank" href="/index.php?module=Employee&relatedModule=Claims&view=Detail&record={$SUM['employid']}&mode=showRelatedList&relationId=229&tab_label=Sukeltos%20pretenzijos&app=HRM">{$name}</a>{else}{$name}{/if} <b>Kiekis</b>: {$SUM['qty']} <b>Suma</b>: {$SUM['total']} <b>Patvirtinta suma</b>: {$SUM['payout']}</p>
    {/foreach}
</div>

<div class="dashboardWidgetContent scrollContainer" style="padding-top:15px;">
{include file="dashboards/ClaimsContent.tpl"|@vtemplate_path:$MODULE_NAME}
</div>
<div class="widgeticons dashBoardWidgetFooter">
    <div class="footerIcons pull-right">
        {include file="dashboards/DashboardFooterIcons.tpl"|@vtemplate_path:$MODULE_NAME SETTING_EXIST=false}
    </div>
</div>