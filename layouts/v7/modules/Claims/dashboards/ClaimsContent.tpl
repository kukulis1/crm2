{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}


	{if $CLAIMS['content'] neq false}
		<div class="row entry clearfix" style="padding: 2px 20px 2px 14px;">    
			<div class="col-lg-2 pull-left"><b>Sukurta</b></div>
			<div class="col-lg-2 pull-left"><b>Statusas</b></div>
			<div class="col-lg-2 pull-left"><b>Klientas</b></div>
			<div class="col-lg-2 pull-left"><b>Skyrius</b></div>
			<div class="col-lg-1 pull-left"><b>Pretenzijos suma</b></div>
			<div class="col-lg-1 pull-left"><b>Sutarta suma</b></div>
			<div class="col-lg-2 pull-left"><b>Uždaryta</b></div>
		</div>
	<div style='padding:10px;'>
		{foreach key=$index item=CLAIM from=$CLAIMS['content']} 
			<div class="row entry clearfix" style="padding: 10px 3px 6px;border-bottom: 1px solid #ddd;">    	
				<div class="col-lg-2 pull-left"><a target="_blank" href="index.php?module=Claims&view=Detail&record={$CLAIM['claimsid']}">{$CLAIM['createdtime']}</a></div>
				<div class="col-lg-2 pull-left">{$CLAIM['status']}</div>      
				<div class="col-lg-2 pull-left">{if $CLAIM['accountname']}{$CLAIM['accountname']}{else}---{/if}</div>   
				<div class="col-lg-2 pull-left">{$CLAIM['section']}</div>   
				<div class="col-lg-1 pull-left">{$CLAIM['claims_tks_claim_amount']}</div>   
				<div class="col-lg-1 pull-left">{$CLAIM['payout']}</div>   
				<div class="col-lg-2 pull-left">{if $CLAIM['status'] eq 'Uždaryta'}{$CLAIM['end_claim']}{else}---{/if}</div>  
			</div>
    {/foreach}
 </div>
			{else}
		<span class="noDataMsg">{vtranslate('LBL_NO_REMOVED_RECORD', $MODULE_NAME)}</span>
{/if}

