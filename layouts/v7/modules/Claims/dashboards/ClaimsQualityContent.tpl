{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

	{if in_array($CLAIMS['role'],array('H2','H57'))}
		{if $CLAIMS['content'] neq false}
			<div class="row entry clearfix" style="padding: 2px 20px 2px 14px;">    
				<div class="col-lg-2 pull-left"><b>Sukūrimo data</b></div>
				<div class="col-lg-2 pull-left"><b>Uždarytų</b></div>
				<div class="col-lg-3 pull-left"><b>Sukelėjai</b></div>
				<div class="col-lg-3 pull-left"><b>Siuntėjai</b></div>
				<div class="col-lg-2 pull-left"><b>Prašoma/sutarta suma</b></div>
			</div>
		<div style='padding:10px;'>
			{foreach key=$index item=CLAIM key=date from=$CLAIMS['content']} 
				<div class="row entry clearfix" style="padding: 10px 3px 6px;border-bottom: 1px solid #ddd;">  	
					<div class="col-lg-2 pull-left">{$date}</div>      
					<div class="col-lg-2 pull-left">{$CLAIM['closed']}</div>      
					<div class="col-lg-3 pull-left">{$CLAIM['cousers']}</div>   
					<div class="col-lg-3 pull-left">{$CLAIM['senders']}</div>   
					<div class="col-lg-2 pull-left">{$CLAIM['amounts']}</div>   
				</div>
			{/foreach}
	</div>
				{else}
			<span class="noDataMsg">Nėra įrašų</span>
	{/if}

{else}
	<span class="noDataMsg">Neturite teisių matyti ši moduli</span>
{/if}