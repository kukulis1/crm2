{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is: vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

	{if $CLAIMS_VISE['content'] neq false}
		<div class="row entry clearfix" style="padding: 2px 20px 2px 14px;">    
			<div class="col-lg-1 pull-left"><b>Pretenzijos nr.</b></div>
			<div class="col-lg-1 pull-left"><b>Sukurta</b></div>
			<div class="col-lg-1 pull-left"><b>Trukmė (d.)</b></div>
			<div class="col-lg-1 pull-left"><b>Klientas</b></div>
			<div class="col-lg-1 pull-left"><b>Skyrius</b></div>
			<div class="col-lg-1 pull-left"><b>Statusas</b></div>
			<div class="col-lg-1 pull-left"><b>Pretenzijos komentaras</b></div>	
			<div class="col-lg-1 pull-left"><b>Užsakymo nr.</b></div>
			<div class="col-lg-1 pull-left"><b>Autorius</b></div>
			<div class="col-lg-1 pull-left"><b>Pretenzijos/Sutarta suma</b></div>
			<div class="col-lg-1 pull-left"><b>Draudimas moka</b></div>
			{if $CLAIMS_VISE['user'] eq '5' || $CLAIMS_VISE['user'] eq '1'}
			<div class="col-lg-1 pull-left"><b>Vizuoti</b></div>
			{/if}
		</div>
	<div style='padding:10px;'>
		{foreach key=$index item=CLAIM from=$CLAIMS_VISE['content']} 
			{$couser = str_replace('|##|',',',$CLAIM['causer'])}
			{if $CLAIMS_VISE['user'] eq $CLAIMS_VISE['managers_role'][$CLAIM['section']] || in_array($CLAIMS_VISE['user'], array(1,5))}
				<div class="row entry clearfix" style="padding: 10px 3px 6px;border-bottom: 1px solid #ddd;">   
					<div class="col-lg-1 pull-left"><a target="_blank" href="index.php?module=Claims&view=Detail&record={$CLAIM['claimsid']}">{$CLAIM['claimsno']}</a></div> 	
					<div class="col-lg-1 pull-left">{$CLAIM['createdtime']}</div>
					<div class="col-lg-1 pull-left">{$CLAIM['claim_exist']}</div>      
					<div class="col-lg-1 pull-left">{if $CLAIM['accountname']}{$CLAIM['accountname']}{else}---{/if}</div>   
					<div class="col-lg-1 pull-left">{if $CLAIM['section']}{$CLAIM['section']}{else}---{/if}</div>   
					<div class="col-lg-1 pull-left">{if $CLAIM['status']}{$CLAIM['status']}{else}---{/if}</div>   
					<div class="col-lg-1 pull-left">{if $CLAIM['claim_decision']}{$CLAIM['claim_decision']}{else}---{/if}</div> 
					<div class="col-lg-1 pull-left">{$CLAIM['shipment_code']}</div>   
					<div class="col-lg-1 pull-left">{$couser}</div>   
					<div class="col-lg-1 pull-left">{round($CLAIM['claims_tks_claim_amount'],2)} / {round($CLAIM['payout'],2)}</div>  
					<div class="col-lg-1 pull-left">{round($CLAIM['insurance_price'],2)}</div>  
					{if $CLAIMS_VISE['user'] eq '5' || $CLAIMS_VISE['user'] eq '1'}
						<div class="col-lg-1 pull-left" {if $CLAIM['confirm'] eq '1'}style="margin: 5px 0px;"{/if}>
						{if $CLAIM['confirm'] eq '0'}	
							<button type="button" 
											data-claim_id="{$CLAIM['claimsid']}" 
											class="btn btn-success btn-sm" 									
											onclick="viseClaimCEO(event);">Vizuoti
							</button>
						{else}
						<span>Atmesta</span>
						{/if}
						</div> 
					{/if} 			
				</div>				
					{/if}		
    {/foreach}
 </div>
			{else}
		<span class="noDataMsg">{vtranslate('LBL_NO_VISE_CLAIMS', $MODULE_NAME)}</span>
{/if}

