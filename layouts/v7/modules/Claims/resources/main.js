let pageId = new URLSearchParams(window.location.search);

$(document).ready(function () {


  $(document).ajaxComplete(function ( event, xhr, settings) {
    if (pageId.get('view') == 'Detail' && settings.url == 'index.php?module=Claims&recordId=822084&action=RelatedRecordsAjax&mode=getRelatedRecordsCount') {
      loadStatusButton();
    }
  });

  $('body').on('change', '#Claims_detailView_fieldValue_cf_20023', function () {
    let accountid = $('[name="cf_20023"]').val();
    let recordid = $('[name="record_id"]').val();
    $('.input-save-wrap span').on('click', function () {
      $.ajax({
        url: "modules/Claims/ajax/setEmailAddress.php",
        type: "POST",
        data: { accountid: accountid, recordid: recordid },
        success: function (data) {
          if (data != '') {
            document.querySelector('#Claims_detailView_fieldValue_cf_1839 span').innerHTML = `<a class="emailField cursorPointer" href="mailto:${data}">${data}</a>`;
          }
        }
      });
    });
  });

  $('#Claims_detailView_fieldValue_claims_tks_order_number .editAction').on('click', function(){
    setTimeout(() => {
      $('.input-save-wrap span').on('click', function () {
          let  salesorderid = document.querySelector('[name="claims_tks_order_number"]').value;     
          let claim_id = document.querySelector('[name="record_id"]').value;
          if (salesorderid != '') {
            updateClaimInfo2(salesorderid, claim_id, 'Detail', 'shipment_code');
          }       
      });
    }, 500);
  });


  $(document).on('blur', '[name="claims_tks_order_number"]', function (e) {
    setTimeout(() => {
      loadOrderInfo(e, 'Detail');
    }, 500);
  });

  $('#Claims_editView_fieldName_claims_tks_order_number_select').on('click', function () {
    $(document).ajaxComplete(function () {
      $('.popupEntriesTableContainer  tbody tr').on('click', function () {
        let claim_id = '';
        setTimeout(() => {
          let salesorderid = $('[name="claims_tks_order_number"]').val();
          updateClaimInfoFields(salesorderid, claim_id, 'Edit','salesorderid');
        }, 1000);
      });
    });
  });

  $(document).on('blur', '[name="claims_tks_order_number_display"]', function (e) {
    setTimeout(() => {
      loadOrderInfo(e, 'Edit');
    }, 100);
  });


  $(document).on('change', '[name="cf_1837"]', function (e) {
    if (e.target.value == 'Uždaryta') {
      if (pageId.get('view') == 'Edit') {
        calculateClaimTime('Edit');
      } else {
        $('.input-save-wrap span').on('click', function () {
          calculateClaimTime('Detail');
        });
      }
    }
  });

  $(document).on('click', '[name="cf_1856"]', function (e) {
    let which = e.target.checked;
    $('.input-save-wrap span').on('click', function () {
      unlockStatus(which);
    });
  });

  $(document).on('click', '#Claims_editView_fieldName_cf_1856', function (e) {
    unlockStatus(e.target.checked);
  });

  $('#Claims_editView_fieldName_claimsno').on('change', function () {
    let number = $(this).val();
    let claimid = $('[name="record"').val();
    $.ajax({
      url: "modules/Claims/ajax/checkClaimNumber.php",
      type: "POST",
      data: { number: number, claimid: claimid },
      success: function (data) {
        if (data) {
          $('#info').show();
        } else {
          $('#info').hide();
        }
      }
    });
  });
});


function isNumber(evt) {  
  let charCode = (evt.which) ? evt.which : evt.keyCode;
  evt.target.value = evt.target.value.replace(/,/g, '.');


  if (charCode != 46 && charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  } else {
    //if dot sign entered more than once then don't allow to enter dot sign again. 46 is the code for dot sign
    let parts = evt.srcElement.value.split('.');

    if (parts.length > 1 && (charCode == 46 || charCode == 44)) {
      return false;
    }
    return true;
  }
}

function loadStatusButton() {
  let claimid = $('[name="record_id"').val();
  $.ajax({
    url: "modules/Claims/ajax/checkVise.php",
    type: "POST",
    data: { claimid: claimid },
    success: function (data) {
      unlockStatus(data);
    }
  });
}


function unlockStatus(bool) {
  let status = document.querySelector('.status_select');
  if (document.querySelector('.status_select') != null) {
    if (bool) status.style = ''; else status.style = 'display: none;';
  }
}


function calculateClaimTime(place) {
  let claim_id = document.querySelector(`[name="${(place == 'Detail') ? 'record_id' : 'record'}"]`).value;
  sendRequestForClaimTime(claim_id, place);
}


function sendRequestForClaimTime(claim, place) {
  $.ajax({
    url: "modules/Claims/ajax/calculateClaimTime.php",
    type: "POST",
    data: { claim: claim },
    success: function (data) {
      if (place == 'Detail') {
        $('#Claims_detailView_fieldValue_cf_1841 .value').html(data);
      } else {
        $('#Claims_editView_fieldName_cf_1841').val(data);
      }
    }
  });
}

function loadOrderInfo(event, place) {
  let salesorderid;
  let claim_id;
  if (place == 'Edit') {
    claim_id = document.querySelector('[name="record"]').value;
    salesorderid = event.target.parentElement.querySelector('.sourceField').value;
    if (salesorderid != '') {
      updateClaimInfoFields(salesorderid, claim_id, place,'salesorderid');
    }
  } else {  
    claim_id = document.querySelector('[name="record_id"]').value;
    salesorderid = event.target.parentElement.parentElement.querySelector('#claims_tks_order_number_hidden_id').value;

    $('.input-save-wrap span').on('click', function () {
      if (salesorderid != '') {
        updateClaimInfo(salesorderid, claim_id, place, 'salesorderid');
      }
    });
  }
}

function updateClaimInfo2(salesorderid, claim_id, place, type) {
  $.ajax({
    url: "modules/Claims/ajax/getSalesOrderInfo.php",
    type: "POST",
    data: { salesorderid: salesorderid, claim_id: claim_id, place: place, type:type },
    dataType: 'JSON',
    success: function (data) {
      if (data.order_date != null) {
        $('#Claims_detailView_fieldValue_claims_tks_order_date [data-field-type="date"]').html(data.order_date);
        $('#Claims_detailView_fieldValue_claims_tks_load_date [data-field-type="date"]').html(data.load_date_to);
        $('#Claims_detailView_fieldValue_claims_tks_load_place .value').html(data.load_place);
        $('#Claims_detailView_fieldValue_claims_tks_unload_date [data-field-type="date"]').html(data.unload_date_to);
        $('#Claims_detailView_fieldValue_claims_tks_unload_place .value').html(data.unload_place);
        $('#Claims_detailView_fieldValue_claims_tks_cargo .value').html(data.cargo);
        $('#Claims_detailView_fieldValue_claims_tks_trip .value').html(data.route);
        $('#Claims_detailView_fieldValue_claims_tks_stevedore .value').html(data.loaders);
        $('#Claims_detailView_fieldValue_claims_tks_driver .value').html(data.driver);
        $('#Claims_detailView_fieldValue_claims_tks_order_type .value').html(data.type);
        $('#Claims_detailView_fieldValue_cf_1837 .value').html('Sprendžiama');
        $('#Claims_detailView_fieldValue_cf_20023 .value').html(`<a href="index.php?module=Accounts&view=Detail&record=${data.accountid}" title="Klientas:${data.accountname}" data-original-title="Klientai">${data.accountname}</a>`);
      }
    }
  });
}

function updateClaimInfo(salesorderid, claim_id, place, type) {
  $.ajax({
    url: "modules/Claims/ajax/getSalesOrderInfo.php",
    type: "POST",
    data: { salesorderid: salesorderid, claim_id: claim_id, place: place, type:type },
    dataType: 'JSON',
    success: function (data) {
      if (data.order_date != null) {
        $('#Claims_detailView_fieldValue_claims_tks_order_date [data-field-type="date"]').html(data.order_date);
        $('#Claims_detailView_fieldValue_claims_tks_load_date [data-field-type="date"]').html(data.load_date_to);
        $('#Claims_detailView_fieldValue_claims_tks_load_place .value').html(data.load_place);
        $('#Claims_detailView_fieldValue_claims_tks_unload_date [data-field-type="date"]').html(data.unload_date_to);
        $('#Claims_detailView_fieldValue_claims_tks_unload_place .value').html(data.unload_place);
        $('#Claims_detailView_fieldValue_claims_tks_cargo .value').html(data.cargo);
        $('#Claims_detailView_fieldValue_claims_tks_trip .value').html(data.route);
        $('#Claims_detailView_fieldValue_claims_tks_stevedore .value').html(data.loaders);
        $('#Claims_detailView_fieldValue_claims_tks_driver .value').html(data.driver);
        $('#Claims_detailView_fieldValue_claims_tks_order_type .value').html(data.type);
        $('#Claims_detailView_fieldValue_cf_1837 .value').html('Sprendžiama');
        $('#Claims_detailView_fieldValue_cf_20023 .value').html(`<a href="index.php?module=Accounts&view=Detail&record=${data.accountid}" title="Klientas:${data.accountname}" data-original-title="Klientai">${data.accountname}</a>`);
      }
    }
  });
}

function updateClaimInfoFields(salesorderid, claim_id, place,type) {
  $.ajax({
    url: "modules/Claims/ajax/getSalesOrderInfo.php",
    type: "POST",
    data: { salesorderid: salesorderid, claim_id: claim_id, place: place,type:type },
    dataType: 'JSON',
    success: function (data) {
      if (data != '') {
        $('#Claims_editView_fieldName_claims_tks_order_date').val(data.order_date);
        $('#Claims_editView_fieldName_claims_tks_load_date').val(data.load_date_to);
        $('#Claims_editView_fieldName_claims_tks_load_place').val(data.load_place);
        $('#Claims_editView_fieldName_claims_tks_unload_date').val(data.unload_date_to);
        $('#Claims_editView_fieldName_claims_tks_unload_place').val(data.unload_place);
        $('#Claims_editView_fieldName_claims_tks_cargo').val(data.cargo);
        $('#Claims_editView_fieldName_claims_tks_trip').val(data.route);
        $('#Claims_editView_fieldName_claims_tks_stevedore').val(data.loaders);
        $('#Claims_editView_fieldName_claims_tks_driver').val(data.driver);
        $('[name="claims_tks_order_type"]').val(data.type);
        $('#select2-chosen-6').html(data.type);
        $('[name="cf_1837"]').val('Sprendžiama');
        $('#select2-chosen-10').html('Sprendžiama');
        $('[name="cf_20023"]').val(data.accountid);
        $('#cf_20023_display').val(data.accountname);
        $('#cf_20023_display').attr("disabled", true);
      }
    }
  });
}


function checkEmailExist() {
  let emailField = document.querySelector('#Claims_detailView_fieldValue_cf_1839 span a').innerHTML;
  let answer = document.querySelector('[name="answer"]');

  if (answer.value == '') {
    answer.className = 'inputElement alert alert-danger';
  } else {
    answer.className = 'inputElement';
  }

  if (emailField != '') {
    document.getElementById('detailView').submit();
  } else {
    $('#Claims_detailView_fieldValue_cf_1839 span a').click();
    setTimeout(() => {
      document.querySelector('[name="cf_1839"]').className = 'inputElement form-control alert alert-danger';
    }, 100);
  }
}
