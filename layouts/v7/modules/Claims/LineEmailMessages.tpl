{assign var=MAILS value=$GET_CLAIM_MAILS}


<div class="editViewContents">
<div class="fieldBlockContainer">
<h4 class="fieldBlockHeader">Laiškai</h4>
<hr>  
  <input type="text" name="cc" class="inputElement" style="width: 100%; margin-bottom: 10px;" placeholder="Cc - Galima vesti kelis adresus atskiriant ;">
  <textarea rows="3" class="inputElement" name="answer" style="margin: 0px; width: 100%; height: 143px;" placeholder="Rašyti atsakymą"></textarea>
  <input type="file" name="attachments[]" class="inputElement" style="margin-top: 10px; width: 100%;" multiple>
  <button type="button" class="btn btn-secondary btn-lg btn-block" style="margin-top: 10px;" onclick="checkEmailExist();">Siųsti</button>
<hr style="margin-top: 20px;margin-bottom: 20px;"> 

   {foreach item=MAIL from=$MAILS}
    {* {assign var=ATTACHMENT value=explode('|##|', $MAIL['attachment'])} *}
       <div class="container {if $MAIL['cc']}cc-bg{/if} {if $MAIL['type'] eq '1'}darker{/if}">
        <span class="time-left">Nuo: {$MAIL['email']}</span><br>
        <span class="time-left">Tema: {$MAIL['subject']}</span>        
        <span class="time-right">
        {if $MAIL['attachment']}
         {assign var=ATTACHMENTS value=$MODULE_MODEL->getAttachments(explode(',', $MAIL['attachment']))}
    

      {$ATTACHMENTS}
          {* {foreach from=$ATTACHMENT item=item}
            {$number = explode(',', $item)}
            <a name="downloadfile" href="index.php?module=Documents&action=DownloadFile&record={$number[0]}&fileid={$number[1]}"><i class="fa fa-paperclip initFancyBox" style="font-size: 17px;"></i></a>
          {/foreach}       *}
        {/if}{$MAIL['createdate']}</span><br>
        <hr>
         <p>{nl2br($MAIL['body'])}</p>
      </div>
   {/foreach}      
    </div>
 </div>
<script src="layouts/v7/modules/Claims/resources/fancy.js"></script>
 {* onclick="event.stopImmediatePropagation();" *}
