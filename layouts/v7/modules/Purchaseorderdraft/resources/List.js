/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Vtiger_List_Js("Purchaseorderdraft_List_Js",{},{});


function loadInputChecker(){

    let vendorid = new Array();
    let check = false;
    $('#listview-table tbody tr').each(function(){
      check = $(this).find('input:checkbox').is(':checked');       
      if(check) {   
        if($(this).find('[data-name="purchaseorderdraft_tks_vendor"]').data('rawvalue')){
          vendorid.push($(this).find('[data-name="purchaseorderdraft_tks_vendor"]').data('rawvalue'));
        }
      }            
    });
    
    const allEqual = arr => arr.every(v => v === arr[0]);
  
  
    if(allEqual(vendorid)){
      $('#exportBtn').attr('disabled',false); 
    }else if(vendorid.length == 0){
      $('#exportBtn').attr('disabled',true);
    }else{
      $('#exportBtn').attr('disabled',true); 
    }

}

function WritePurchaseOrderInvoice(){
  let invoiceidArray = new Array();
  let check = false;
  let vendorid = new Array();

  $('#listview-table tbody tr').each(function(){
    check = $(this).find('input:checkbox').is(':checked');  
    if(check) {
      invoiceidArray.push($(this).data('id'));    
      if($(this).find('[data-name="purchaseorderdraft_tks_vendor"]').data('rawvalue')){
        vendorid.push($(this).find('[data-name="purchaseorderdraft_tks_vendor"]').data('rawvalue'));
      } 
    }
  });  

  let purchaseorderid = invoiceidArray.join(',');


  let vendor = vendorid.filter((v, i, a) => a.indexOf(v) === i);

  window.location.href = `index.php?module=PurchaseOrder&view=Edit&purchaseorderid=${purchaseorderid}&vendor_id=${vendor}`;
}

function invoiceExist(checkbox, recordId){
  let status = 1;
  if(!$(checkbox).is(':checked')){
    status = 0;
  }

  $.ajax({
    type: "POST",
    url: "modules/Purchaseorderdraft/ajax/invoice_exist.php",
    data: {status:status, recordId:recordId},
    dataType: "JSON",
    success: function (response) {
      console.log(response);
    }
  });

}


