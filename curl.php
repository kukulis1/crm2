<?php

require_once "config.inc.php";




  $adb = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $adb ->set_charset("utf8");

header('Content-Type: application/json; charset=utf-8');


    
   
            $order_id = 43633;  
            
            $order_sent = 'Sent';  
            $order_not_sent = 'Not Sent'; 
            
            $result_data = array('order' => array()); 
            
            $sql = "SELECT
                                s.salesorderid as order_id,
                                s.sostatus AS order_status,                        
                                e.createdtime AS order_date,
                                a.accountname AS customer_name,                
                                a.account_no AS customer_code,
                                a.customer_id,
                                s.external_order_id,             
                                s.ivaz_no,
                                s.source,                                
                                e.description AS notes,
                                CONCAT(s.load_date_from, ' ', s.load_time_from) AS load_date_from,
                                CONCAT(s.load_date_to, ' ', s.load_time_to) AS load_date_to, 
                                CONCAT(s.unload_date_from, ' ', s.unload_time_from) AS unload_date_from,
                                CONCAT(s.unload_date_to, ' ', s.unload_time_to) AS unload_date_to, 
                                l.load_company,
                                l.bill_street AS load_address,
                                l.bill_city AS load_municipality,
                                l.bill_state AS load_region,
                                l.bill_code AS load_zipcode,
                                l.bill_country AS load_country,
                                h.unload_company,
                                h.ship_street AS unload_address,
                                h.ship_city AS unload_municipality,
                                h.ship_state AS unload_region,
                                h.ship_code AS unload_zipcode,
                                h.ship_country AS unload_country,
                                a.legal_vat_code AS unload_vat_code,
                                i.productid,                                
                                i.quantity AS cargo_qty,
                                i.cargo_measure,
                                i.cargo_wgt,
                                i.cargo_length,
                                i.cargo_width,
                                i.cargo_height,
                                i.comment AS cargo_notes,
                                r.serviceid
                                    FROM vtiger_salesorder s
                                    JOIN vtiger_crmentity e ON e.crmid = s.salesorderid
                                    JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
                                    JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid                            
                                    JOIN vtiger_users u ON u.id = e.smownerid 
                                    JOIN vtiger_account a ON a.accountid = s.accountid 
                                    JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
                                    LEFT JOIN vtiger_products p ON p.productid = i.productid
                                    LEFT JOIN vtiger_service r ON r.serviceid = i.productid                                    
                                    WHERE e.deleted = 0 AND s.salesorderid = $order_id ";

            $result = $adb->query($sql);

            $orders = array();

        while($row = mysqli_fetch_array($result)) {


                    
            $date = date("Y-m-d H:i:s");
            $order_id = intval($row['order_id']);

            $orders[$order_id]['customer_id'] = intval($row['customer_id']);

            if(empty($row['external_order_id'])) {
                $orders[$order_id]['crm_id'] = intval($row['order_id']);  
                // $orders[$order_id]['order_id'] = ''; 
            } else {
                $orders[$order_id]['crm_id'] = intval($row['order_id']);  
                $orders[$order_id]['order_id'] = $row['external_order_id']; 
            }       

            $orders[$order_id]['ivaz_no'] = $row['ivaz_no'];  
            $orders[$order_id]['notes'] = $row['notes'];
            $orders[$order_id]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'])); 

            if(!empty($row['load_date_to'])){       
                $orders[$order_id]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to']));   
            } else {
                $orders[$order_id]['load_date_to'] = '';      
            }  

            if(!empty($row['customer_id'])) {
                if(empty($row['load_company'])){       
                    $orders[$order_id]['load_company'] = $row['customer_name'];          
                } else {
                    $orders[$order_id]['load_company'] = $row['load_company'];
                }        
            } else {
                    $orders[$order_id]['load_company'] = $row['customer_name']; 
            }        

            $orders[$order_id]['load_address'] = $row['load_address'];
            $orders[$order_id]['load_settlement'] = '';
            $orders[$order_id]['load_municipality'] = $row['load_municipality'];   
            $orders[$order_id]['load_region'] = $row['load_region'];                      
            $orders[$order_id]['load_zipcode'] = $row['load_zipcode'];
            
                //if($row['load_zipcode'] == 'LTU') {
                //$orders[$order_id]['load_zipcode'] = ''; 
                //}
            
            $orders[$order_id]['load_country'] = $row['load_country'];  
            $orders[$order_id]['load_person'] = '';  
            $orders[$order_id]['load_phone'] = '';  

            if(empty($row['load_company'])){       
                $orders[$order_id]['load_company_code'] = '';            
            } else {
                $orders[$order_id]['load_company_code'] = '';
            }    

            $orders[$order_id]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from']));

            if(!empty($row['unload_date_to'])){       
                $orders[$order_id]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to']));  
            } else {
                $orders[$order_id]['unload_date_to'] = '';      
            }               

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_company'] = $row['customer_name'];          
            } else {
                $orders[$order_id]['unload_company'] = $row['unload_company'];
            }

            $orders[$order_id]['unload_address'] = $row['unload_address']; 
            $orders[$order_id]['unload_settlement'] = '';        
            $orders[$order_id]['unload_municipality'] = $row['unload_municipality'];         
            $orders[$order_id]['unload_region'] = $row['unload_region'];                      
            $orders[$order_id]['unload_zipcode'] = $row['unload_zipcode']; 
            
                //if($row['unload_zipcode'] == 'LTU') {
                //$orders[$order_id]['unload_zipcode'] = ''; 
                //}            
            
            $orders[$order_id]['unload_country'] = $row['unload_country']; 
            $orders[$order_id]['unload_person'] = '';  
            $orders[$order_id]['unload_phone'] = ''; 

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_company_code'] = '';            
            } else {
                $orders[$order_id]['unload_company_code'] = '';
            }

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_vat_code'] = '';            
            } else {
                $orders[$order_id]['unload_vat_code'] = '';
            }

            $orders_items = array();

            $orders_items['cargo']['cargo_qty'] = ($row['cargo_qty'] < 1 ) ? intval(1.000) : intval($row['cargo_qty']);
    
            // if($row['cargo_measure'] == 'pll') {
                $orders_items['cargo']['cargo_measure'] = 1;
            // }     

            $orders_items['cargo']['cargo_wgt'] = sprintf('%0.2f', $row['cargo_wgt']);
            $orders_items['cargo']['cargo_length'] = sprintf('%0.2f', $row['cargo_length']);       
            $orders_items['cargo']['cargo_width'] = sprintf('%0.2f', $row['cargo_width']);      
            $orders_items['cargo']['cargo_height'] = sprintf('%0.2f', $row['cargo_height']);   
            $orders_items['cargo']['cargo_ldm'] = sprintf('%0.2f', $row['cargo_ldm']);
            $orders_items['cargo']['cargo_facilities'] = '';        
            $orders_items['cargo']['cargo_termo'] = 0;  
            $orders_items['cargo']['cargo_return_documents'] = intval($row['cargo_return_documents']);          
            $orders_items['cargo']['cargo_notes'] = $row['cargo_notes'];

            if($row['source'] == 'CRM') {
                $orders_items['cargo']['update_type'] = 1;   
            }              
            
            if($row['serviceid'] == NULL) {  
                $orders[$order_id]['cargos'][] = $orders_items;
            }
            
            $orders_indexed = array_values($orders);
            $result_data['order'] = $orders_indexed;    

        }       

         
        
            $res = json_encode($result_data, JSON_UNESCAPED_UNICODE);     

          
            // echo "<pre>";
            print_R($res);
            // echo "</pre>";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders.php");
            // curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders.php");  

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders' => $res));
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $result = curl_exec($ch);
            curl_error($ch);
            curl_close($ch);    



            // $this->orders_log($result, 'insert_value.log');  

            $data = json_decode($result);


            print_R($result);
               die();     
                $state = $data[0]['status'];


                     
                
                if($state == 'ERROR') {          
                    $logfile = 'orders_error_out.log';
                    // $this->orders_log($result_data, $logfile);  
                    // $this->orders_log($data, $logfile);
                    
                $order_id = $data[0]['crm_id'];
                $errors = serialize($data[0]['errors']);
                    
                            // //Atnaujiname uzsakymo busena           
                            // $sql = 'UPDATE vtiger_salesorder SET sostatus = ?, status = ?, errors = ? WHERE salesorderid = ?';
                            // $result = $adb->pquery($sql, array($order_not_sent, $state, $errors, $order_id));

                            // //Atnaujiname uzsakymo redagavimo data    
                            // $sql = 'UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?';
                            // $result = $adb->pquery($sql, array($date, $order_id));                     
                    
                } else {    
                    $logfile = 'orders_good_out.log';
                    // $this->orders_log($result_data, $logfile);  
                    // $this->orders_log($data, $logfile);  
                    
                $order_id = $data[0]['order']['crm_id'];
                $external_order_id = $data[0]['order']['order_id'];
                $ivaz_no = $data[0]['order']['ivaz_no'];
                $barcode = $data[0]['order']['barcode'];
                $direction = $data[0]['order']['direction'];     
                $errors = '';
                
                            //Atnaujiname uzsakymo busena           
                            // $sql = 'UPDATE vtiger_salesorder SET sostatus = ?, ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ? WHERE salesorderid = ?';
                            // $result = $adb->query($sql, array($order_sent, $ivaz_no, $barcode, $direction, $state, $errors, $external_order_id, $order_id));

                            // //Atnaujiname uzsakymo redagavimo data    
                            // $sql = 'UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?';
                            // $result = $adb->query($sql, array($date, $order_id));
      
              }
            
          

