<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}


$history = $conn->query("SELECT DISTINCT vtiger_inventoryproductrel.cargo_wgt as purchase 
                          FROM vtiger_crmentity 
                          LEFT JOIN vtiger_inventoryproductrel on vtiger_inventoryproductrel.id=vtiger_crmentity.crmid
                          WHERE setype = 'PurchaseOrder'");

$all_history = array();
foreach($history as $all_records){
  if(!empty($all_records['purchase']))
  $all_history[] = $all_records['purchase'];
}


echo  json_encode($all_history);