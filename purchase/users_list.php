<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}


$user_list = $conn->query("SELECT CONCAT(`first_name`, ' ', `last_name`) AS `name` FROM vtiger_users");

$user_names = Array();
foreach($user_list AS $users){
  $user_names[] = $users['name'];
}

echo json_encode($user_names);