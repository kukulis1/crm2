<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

$id = $_POST['id'];
$edited = $_POST['edited'];

$query = mysqli_fetch_array($conn->query("SELECT CASE WHEN message_no THEN message_no ELSE id END AS email_id, 
                                          GROUP_CONCAT(m.filename SEPARATOR '|##|') as filename, GROUP_CONCAT(original_file_name SEPARATOR '|##|') AS original_file_name, f.filename AS new_file, f.path AS new_path, CONCAT(comments,' ',body) AS body
                                          FROM app_invoice_mails m
                                          LEFT JOIN vtiger_invoiceemails_files f ON CASE WHEN m.message_no THEN f.email_id=m.message_no ELSE m.id=f.email_id END 
                                          LEFT JOIN vtiger_invoiceemails_comments c ON c.email_id=m.message_no 
                                          WHERE m.message_no = $id OR m.id = $id"));

if($edited == 0){
  $filename = array();
  $filepath = array();
  $ext = array();
  $fullfilename = array();
  $url = array();
  $original_file_name = array();

  $getFileName = explode('|##|', $query['filename']);
  $getOriginal_file_name = explode('|##|', $query['original_file_name']);

  foreach($getFileName as $file){
    $filename[] = pathinfo($file, PATHINFO_FILENAME);
  }

  foreach($getFileName as $file){
    $filepath[] = pathinfo($file, PATHINFO_DIRNAME);
  }

  foreach($getFileName as $file){
    $ext[] = pathinfo($file, PATHINFO_EXTENSION);
  }

  for($i = 0; $i < count($filename); $i++){
    $fullfilename[] = $filename[$i].'.'.$ext[$i];
  }

  for($i = 0; $i < count($filename); $i++){
    $url[] = $filepath[$i]."/".$fullfilename[$i];
  }

  foreach($getOriginal_file_name as $OrgFile){
    $original_file_name[] = $OrgFile;
  }

  $result = array(
    'mail' => array(
      'mailid' => $query['email_id'], 
      'body' => $query['body']), 
    'file' => array(
      'filename' => $filename,
      'fullfilename' => $fullfilename,
      'url' => $url,
      'filepath' => $filepath,
      'original_file_name' => $original_file_name)
    
    
  );

}else{
  $result = array(
    'mail' => array(
      'mailid' => $query['email_id'], 
      'body' => $query['body']), 
    'file' => array(
      'filename' => array($query['new_file']),
      'fullfilename' => array($query['new_file']),
      'url' => array($query['new_path'].$query['new_file']),
      'filepath' => array($query['new_path']),
      'original_file_name' => array($query['new_file']))       
  );
}

echo json_encode($result);