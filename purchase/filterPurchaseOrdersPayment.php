<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

$data = (array)$_POST['form_array'];
$data = call_user_func_array('array_merge', $data);

$keys = array();
$values = array();

foreach($data as $key => $row){
  if(!is_numeric($key))  $keys[] = $key;
  if($row != '') $values[] = $row;
}


$query_string = "SELECT exported,
                    CASE 
                      WHEN payment_tks_suma IS NULL
                      THEN FORMAT(total,2)
                      ELSE FORMAT((total - SUM(DISTINCT vtiger_payment.payment_tks_suma)),2)
                    END AS debt, 
                    payment_tks_suma,vendorname AS vendor_id,
                    CASE 
                      WHEN payment_tks_suma IS NULL
                      THEN 0
                      ELSE FORMAT(SUM(DISTINCT vtiger_payment.payment_tks_suma),2)
                    END AS payed, 
                  FORMAT(SUM(DISTINCT vtiger_payment.payment_tks_suma),2) AS paid_sum, total,
                    FORMAT(total, 2) AS total_with_vat, vtiger_purchaseorder.purchaseorder_no, vtiger_purchaseorder.postatus, FORMAT(vtiger_purchaseorder.total,2) as hdnGrandTotal, vtiger_purchaseorder.vendorid, cf_1345 as invoicedate, vtiger_purchaseorder.duedate, FORMAT(vtiger_purchaseorder.subtotal,2) as hdnSubTotal, FORMAT(vtiger_purchaseorder.balance,2) as balance, vtiger_crmentity.smownerid, acc_owner.smownerid as smownerid2, vtiger_purchaseorder.purchaseorderid, vtiger_crmentity_user_field.starred, vtiger_crmentity.createdtime,vtiger_purchaseordercf.cf_1355 as invoice_nr,payment_tks_mokėjimodata AS pay_date,payment_tks_tipas AS pay_type
                FROM vtiger_purchaseorder 
                LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid 
                LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid = vtiger_purchaseorder.purchaseorderid 
                LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 
                LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_purchaseorder.vendorid
                LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_payment.paymentid  
                INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid 
                LEFT JOIN vtiger_crmentity as acc_owner ON acc_owner.crmid = vtiger_vendor.vendorid               
                LEFT JOIN vtiger_groups ON vtiger_crmentity.smownerid = vtiger_groups.groupid 
                LEFT JOIN vtiger_crmentity_user_field ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity_user_field.recordid AND vtiger_crmentity_user_field.userid=1 
                WHERE vtiger_crmentity.deleted=0 AND vtiger_purchaseorder.purchaseorderid > 0 AND ( de.deleted = 0 OR de.deleted IS NULL) 
                    GROUP BY vtiger_purchaseorder.purchaseorderid	
                    HAVING paid_sum != 0 AND (debt <= total OR payment_tks_suma IS NULL) AND ";



for($i=0; $i<COUNT($keys); $i++){
  if($keys[$i] == 'duedate' || $keys[$i] == 'cf_1345' || $keys[$i] == 'pay_date'){
    $period = explode(",",$values[$i]);
    $query_string .=  "$keys[$i] BETWEEN '$period[0]' AND '$period[1]' ".($i+1 < COUNT($keys) ? 'AND ' :'');
  }else{
    $query_string .=  "$keys[$i] LIKE '$values[$i]%' ".($i+1 < COUNT($keys) ? 'AND ' :'');
  }
}

$query_string .= " ORDER BY vtiger_crmentity.createdtime DESC";


$query = $conn->query($query_string);


$result = array();
while($row = $query->fetch_assoc()) {
  $result[] = $row;      
}  

echo json_encode($result);


}else{
  http_response_code(404);
}