<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}


$history = $conn->query("SELECT DISTINCT itemservice_tks_name as purchase 
                          FROM vtiger_itemservice       
                          LEFT JOIN vtiger_crmentity e ON e.crmid=itemserviceid                   
                          WHERE deleted = 0");

$all_history = array();
foreach($history as $all_records){
  if(!empty($all_records['purchase']))
  $all_history[] = $all_records['purchase'];
}


echo  json_encode($all_history);