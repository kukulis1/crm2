<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}


$object = $conn->query("SELECT DISTINCT vtiger_inventoryproductrel.cargo_measure as `object` 
                          FROM vtiger_crmentity 
                          LEFT JOIN vtiger_inventoryproductrel on vtiger_inventoryproductrel.id=vtiger_crmentity.crmid
                          WHERE setype = 'PurchaseOrder'");

$all_objects = array();
foreach($object as $all_records){
  if(!empty($all_records['object']))
  $all_objects[] = $all_records['object'];
}


echo  json_encode($all_objects);