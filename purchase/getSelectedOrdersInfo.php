<?php
error_reporting(0);
require_once "../config.inc.php";


if($_POST){

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $purchaseid = $_POST['purchaseid'];

  $info = $conn->query("SELECT cf_1355 as subject, vendorname, 
                          CASE 
                            WHEN vtiger_payment.payment_tks_suma THEN ROUND((total - SUM(DISTINCT vtiger_payment.payment_tks_suma)),2)
                            ELSE ROUND(total,2)
                          END AS total
                          FROM vtiger_purchaseorder 
                          LEFT JOIN vtiger_vendor on vtiger_purchaseorder.vendorid=vtiger_vendor.vendorid 
                        LEFT JOIN vtiger_purchaseordercf on vtiger_purchaseordercf.purchaseorderid=vtiger_purchaseorder.purchaseorderid 
                          LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid 
                          LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 
                          LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_payment.paymentid
                          WHERE vtiger_purchaseorder.purchaseorderid IN ($purchaseid)
                          GROUP BY vtiger_purchaseorder.purchaseorderid 
                          ORDER BY vtiger_purchaseorder.purchaseorderid DESC");
                          

  $purchase_info = array();
  while($row = $info->fetch_assoc()){ 
    $purchase_info[] = $row;          
  }


  echo json_encode($purchase_info);

}else{
  http_response_code(404);
}
  
