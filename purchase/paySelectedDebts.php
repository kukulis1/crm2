<?php
error_reporting(0);
require_once "../config.inc.php";


if($_POST){

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $purchaseid = $_POST['purchaseid'];
  $date = $_POST['date'];
  $method = $_POST['payMethod'];
  $date_now = date("Y-m-d H:i:s");


  $info = $conn->query("SELECT vtiger_purchaseorder.purchaseorderid,
                          CASE 
                            WHEN vtiger_payment.payment_tks_suma THEN ROUND((total - SUM(vtiger_payment.payment_tks_suma)),2)
                            ELSE ROUND(total,2)
                          END AS total
                        FROM vtiger_purchaseorder 
                        LEFT JOIN vtiger_vendor on vtiger_purchaseorder.vendorid=vtiger_vendor.vendorid 
                        LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid 
                        LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 
                        LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_payment.paymentid
                        WHERE vtiger_purchaseorder.purchaseorderid IN ($purchaseid)
                        GROUP BY vtiger_purchaseorder.purchaseorderid	 
                        ORDER BY vtiger_purchaseorder.purchaseorderid DESC");
                            
  
  while($row = $info->fetch_assoc()){ 
    $entity_id = last_entity_record($conn);
    insert_entity($conn,'Payment', $entity_id, 1, NULL, '', 'CRM', $date_now);
    insert_entityrel($conn, $row['purchaseorderid'], 'PurchaseOrder',  $entity_id, 'Payment');
    insertDebt($conn,$entity_id,$date,$row['total'],$method);
    $conn->query("INSERT INTO vtiger_paymentcf (paymentid) VALUES ('$entity_id')");
  }


  if($entity_id){
    echo 'true';
  }else{
    echo 'false';
  }

}else{
  http_response_code(404);
}
  

function insert_entity($conn,$setype, $entity_id, $user_id, $description, $label, $source, $date)
{

  $sth = $conn->query("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) 
                       VALUES ('$entity_id', '$user_id', '$user_id', '$setype', '$description', '$source', '$label', '$date', '$date')");

  $sth2 = $conn->query("UPDATE vtiger_crmentity_seq SET id = $entity_id");
}

function insert_entityrel($conn, $entity_id, $module, $relcrmid, $relmodule)
{

  $sth = $conn->query("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) 
                       VALUES ('$entity_id', '$module', '$relcrmid', '$relmodule')");
}

function insertDebt($conn, $entity_id,$date,$total,$method)
{
  $sth = $conn->query("INSERT INTO vtiger_payment (paymentid, payment_tks_mokėjimodata, payment_tks_suma, payment_tks_tipas) 
                           VALUES ('$entity_id', '$date', '$total', '$method')");
}

function last_entity_record($conn)
{

  $result = $conn->query("SELECT id FROM vtiger_crmentity_seq");
  $productid = $result->fetch_assoc();
  $crmid = $productid['id'] + 1;    
  
  return $crmid;
}