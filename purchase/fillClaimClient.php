<?php

require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
}else{
  http_response_code(404);
}

$claimid = $_POST['claimid'];

$query = $conn->query("SELECT vendorname, vendorid, ROUND(cf_1845,2) as amount
                        FROM vtiger_claims c
                        LEFT JOIN vtiger_account a ON a.accountid=c.cf_20023
                        LEFT JOIN vtiger_claimscf cf ON c.claimsid=cf.claimsid
                        LEFT JOIN vtiger_vendor v ON v.vendorname LIKE a.accountname
                        WHERE c.claimsid = $claimid");

$result = array();

foreach($query AS $row){
  $result = $row;
}

echo json_encode($result);