<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $invoiceid = $_POST['invoiceid'];
  $type = $_POST['type'];
  $cost = $_POST['cost'];

  $storage_main_cost = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT cost) AS cost FROM app_opex_invoice_cost_list WHERE name IN ('storage_profit_vilnius','storage_profit_kaunas','storage_profit_klaipeda')"));
  $storage_main_cost = implode(',',array_unique(explode(',', $storage_main_cost['cost'])));

  $storage_costs = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT cost) AS cost FROM app_opex_invoice_free_cost_list"));
  $storage_costs = implode(',',array_unique(explode(',', $storage_costs['cost'])));
  $storage_string = mysqli_fetch_assoc($conn->query("SELECT DISTINCT GROUP_CONCAT(DISTINCT \"'\",itemservice_tks_name,\"'\") AS itemservice_name 
                                                      FROM vtiger_itemservice i 
                                                      LEFT JOIN vtiger_crmentity e ON e.crmid=i.itemserviceid                                  
                                                      WHERE e.deleted = 0 AND itemserviceid IN ($storage_costs)"));

  $storage_string = $storage_string['itemservice_name']; 


  $query = "SELECT IF(productid = 121391, i.listprice * quantity, i.listprice) AS listprice, ((((IF(productid = 121391, i.listprice * quantity, i.listprice)) * IF(inv.region_id > 0, value, 21)) /100) + IF(productid = 121391, i.listprice * quantity, i.listprice)) AS listprice_vat, a.accountname, inv.invoice_no, inv.invoiceid,invoicedate,
                        CASE 
                          WHEN productid = 36641 THEN  s.name
                          ELSE cargo_wgt
                        END AS service_name
                        FROM vtiger_inventoryproductrel i
                        LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
                        LEFT JOIN vtiger_account a ON a.accountid=inv.accountid
                        LEFT JOIN vtiger_crmentity e ON e.crmid=i.id  
                        LEFT JOIN app_services_type s ON s.id=i.service   
                        LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id             
                        WHERE i.id IN ($invoiceid) AND e.deleted = 0 AND e.setype = 'Invoice' AND
                         CASE WHEN productid = 36641 THEN  service IN ($cost)  
                              WHEN productid = 121391 THEN (cargo_wgt IN ($storage_string) OR service IN ($storage_main_cost))
                         END  ";                      

                        $query .= " AND costcenter = $type";

  $get_list = $conn->query($query);


  $list = array();

  foreach($get_list as $row){
    $list[] = $row;
  }


  echo  json_encode($list);

}else{
  http_response_code(404);
}