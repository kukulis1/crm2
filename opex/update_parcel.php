<?php

require_once "../config.inc.php";

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");




  $first_day_of_month = '2021-03-01';
  $today = '2021-03-31';
  $date = date("Y-m-d H:i:s");



  $transport_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(salesorderid) AS transport_orders
  FROM vtiger_salesorder s
  LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
  WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));

$transport_parcel_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(s.salesorderid) AS parcel_orders
  FROM vtiger_salesorder s
  JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
  LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
  WHERE (DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today') AND setype = 'SalesOrder' AND cf_855 = 'Perkraustymo užsakymas' AND deleted = 0")); 

$transport_orders_pll_places = mysqli_fetch_assoc($conn->query("SELECT DISTINCT SUM(i.pll) AS pll_places
  FROM vtiger_salesorder s
  LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
  LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
  WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today'  AND i.pll != 0 "));

$transport_total_weight = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(cargo_wgt),2) AS total_weight
  FROM vtiger_salesorder s
  LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
  LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
  WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));

$transport_cancel_orders = mysqli_fetch_assoc($conn->query("SELECT SUM(s.total) AS total
  FROM vtiger_salesorder s
  LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
  LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
  WHERE e.deleted =0 AND setype = 'SalesOrder' AND scf.cf_1614 = 1 AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));    

echo '<pre>';
print_R($transport_orders);
print_R($transport_parcel_orders);
print_R($transport_orders_pll_places);
print_R($transport_total_weight);
print_R($transport_cancel_orders);


$get_months_id = mysqli_fetch_assoc($conn->query("SELECT * FROM app_opex_report WHERE years = '".$transport_orders['years']."' AND  month_num = '".$transport_orders['month_num']."'"));

$id = $get_months_id['id'];


echo "<br>";
echo $id;
echo '</pre>';



$check_month = mysqli_fetch_assoc($conn->query("SELECT `tid` FROM app_opex_transport_orders WHERE `tid` = $id"));

if(!$check_month){    
$conn->query("INSERT INTO app_opex_transport_orders  (tid,
                      total_orders,     
                      total_parcel_orders,                                                      
                      total_cancel_orders,
                      total_orders_pll_places,                                                      
                      total_weight,                                                       
                      update_date) 
                      VALUES ('$id',
                              '".$transport_orders['transport_orders']."',  
                              '".$transport_parcel_orders['parcel_orders']."',  
                              '".$transport_cancel_orders['total']."',
                              '".$transport_orders_pll_places['pll_places']."',
                              '".$transport_total_weight['total_weight']."',
                              '$date')");
}else{
$conn->query("UPDATE app_opex_transport_orders SET total_orders =  '".$transport_orders['transport_orders']."', 
                 total_parcel_orders = '".$transport_parcel_orders['parcel_orders']."',  
                 total_cancel_orders =  '".$transport_cancel_orders['total']."',
                 total_orders_pll_places =  '".$transport_orders_pll_places['pll_places']."',
                 total_weight = '".$transport_total_weight['total_weight']."', 
                 update_date = '$date' 
                 WHERE tid = $id"); 
}
