<?php
require_once "../config.inc.php";

error_reporting(0);
ini_set('display_errors', 0);


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
  $year = ($_GET['year'] ?: date('Y'));
  $n = ($_GET['month'] ?: date('m'));

 
  $get_months_id = $conn->query("SELECT * FROM app_opex_report");
  $month_id = [];
  $month_id2 = [];
  foreach ($get_months_id as $item) {
    $month_id2[$item['years']][abs($item['month_num'])] = $item['id'];
    $month_id[$item['years']][$item['month_num']] = $item['id'];
  }

  $id = $month_id[$year][$n];

  $conn->query("DELETE FROM app_opex_invoice_details WHERE did = $id");

  $conn->query("DELETE FROM app_opex_invoice_other_details WHERE did = $id");

  $conn->query("TRUNCATE app_opex_invoice_other_details_temp");

  $conn->query("TRUNCATE app_opex_invoice_details_temp");

  $conn->query("DELETE FROM app_opex_invoice_cost_to_month WHERE `year` = $year AND `month` = $n");

  $get_invoice_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_invoice_cost_list");

  $mon = $n;

  // if($n != 10 AND $n != 11 AND $n != 12){
  //   $mon = "0$n";
  // }  

  $from = $year."-$mon-01";
  $to = $year."-$mon-".date('t',strtotime($year.'-'.$mon));
  $date = $year."-$mon-01 23:59:59";




  $stats_query2 = "SELECT listprice AS other_total, DATE_FORMAT(invoicedate, '%Y') as years ,DATE_FORMAT(invoicedate, '%m') as month_num,
  inv.invoiceid, service, sequence_no, costcenter_tks_cost, IF(vtiger_taxregions.value IS NULL, 21, vtiger_taxregions.value) as vatprocent,
  v.accountname, inv.invoice_no, IF(productid = 121391,itemservice_tks_name,se.name) as service_name, IF(productid = 121391,itemserviceid,i.service) AS service_key,
  CASE ";  
        foreach($get_invoice_cost_centers_with_costs as $cost){
          $cost_center = explode(",", $cost['cost_center']);
          $costs = explode(",", $cost['cost']);
          for($e=0; $e < count($cost_center); $e++){
            if(!empty($cost_center[$e])){
              for($i=0; $i < count($costs); $i++){     
                if(!empty($costs[$i])){       
                  $stats_query2 .= "\n WHEN costcenter = $cost_center[$e] AND ".(strlen($costs[$i]) <= 3 ? 'service': 'itemserviceid')." = $costs[$i] THEN IF(productid = 121391, listprice * quantity, listprice)";
                }
              }
            }
          }
        }
  $stats_query2 .= " END AS total
       FROM vtiger_inventoryproductrel i
       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
       LEFT JOIN vtiger_invoice inv on inv.invoiceid=i.id
       LEFT JOIN vtiger_invoicecf icf on inv.invoiceid=icf.invoiceid
       LEFT JOIN vtiger_account v ON v.accountid=inv.accountid
       LEFT JOIN app_services_type se ON se.id=i.service 
       LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.costcenter
       LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id    
       WHERE e.deleted = 0  AND e.setype = 'Invoice'  AND invoicedate BETWEEN '$from' AND '$to' AND costcenter IN (103914,103916,103917,103918,103919,103920,103921) AND DATE_FORMAT(invoicedate, '%m') != '' AND cf_1277 != 'Išankstinė'
       GROUP BY  i.id,sequence_no                                         
       ORDER BY i.id DESC";

  $stats2 = $conn->query($stats_query2);



  $rows_array = [];
  $rows2_array = [];
  foreach($stats2 as $row){ 
    $vat_procent = $row['vatprocent'];
    $vat = $row['total'] * $vat_procent/100;
    $vat2 = $row['other_total'] * $vat_procent/100;
    $total = $row['total'] + $vat;   
    $other_total = $row['other_total'] + $vat2;
    $cost_center = $row['costcenter_tks_cost'];
    $id = $month_id[$row['years']][$row['month_num']];
     
    if(empty($row['total']) AND $row['service_key'] != 1){     
      $rows2_array[] =  ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'invoiceid' => $row['invoiceid'],'service_key' => $row['service_key'], 'cost_center' => $cost_center,'total' => $other_total,'total_without_vat' => $row['other_total'],'service_name' => $row['service_name'],'sequence_no' => $row['sequence_no'],'accountname' => $row['accountname'],'invoice_no' => $row['invoice_no']];
    }else{
      $rows_array[] = ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'invoiceid' => $row['invoiceid'],'service_key' => $row['service_key'], 'cost_center' => $cost_center,'total' => $total,'total_without_vat' => $row['total'],'service_name' => $row['service_name'],'sequence_no' => $row['sequence_no']];     
    }
  }


    $array = array_chunk($rows_array,100);
    $array2 = array_chunk($rows2_array,100);


    for($i=0;$i<count($array);$i++){
      $values = '';
      $values2 = '';
      foreach($array[$i] AS $row){   
        $values .= "('".$row['id']."','".$row['years']."','".$row['month_num']."','".$row['invoiceid']."','".$row['service_key']."','".$row['cost_center']."','$date'),";

        $values2 .= "('".$row['id']."','".$row['service_name']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total']."','".$row['invoiceid']."','".$row['sequence_no']."','$date'),";
      }

      $values = rtrim($values,',');
      $values2 = rtrim($values2,',');
  
      $conn->query("INSERT INTO app_opex_invoice_cost_to_month (id,years, month, invoiceid, service_key, cost_center, update_date) VALUES $values");

      $conn->query("INSERT INTO app_opex_invoice_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,sequence_no,update_date) VALUES $values2");
    }

    for($i=0;$i<count($array2);$i++){
      $values = '';
      $values2 = '';
      foreach($array2[$i] AS $row){  
        $values .= "('".$row['id']."','".$row['service_name']."','".$row['accountname']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total']."','".$row['invoiceid']."','".$row['invoice_no']."','".$row['sequence_no']."','$date'),";
      }
      $values = rtrim($values,',');

      $conn->query("INSERT INTO app_opex_invoice_other_details_temp (tempid,service_name,accountname,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,invoice_no,sequence_no,update_date) VALUES $values");
    } 

  $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, total_without_vat , total_with_vat, invoiceid,sequence_no
                                 FROM app_opex_invoice_details_temp
                                 LEFT JOIN app_opex_report re on re.id=tempid                                       
                                 GROUP BY month_num,invoiceid,sequence_no");


  $group_query = [];
                        

  foreach($grouped_stats as $result){  
    $group_query[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'], 'service_name' => $result['service_name'], 'service_key' => $result['service_key'], 'cost_center' => $result['cost_center'],'invoiceid' => $result['invoiceid'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat']];
  }

  $grouped_array = array_chunk($group_query,100);


  for($i=0;$i<count($grouped_array);$i++){
    $values = '';
    foreach($grouped_array[$i] AS $row){ 
      $values .= "('".$row['tempid']."','".$row['sequence_no']."','".$row['service_name']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total_with_vat']."','".$row['invoiceid']."','$date'),";
    }
    $values = rtrim($values,',');
    
    $conn->query("INSERT INTO app_opex_invoice_details (did,sequence_no,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date)  VALUES $values");
  }


  // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,accountname,invoice_no,service_key,cost_center, total_without_vat , total_with_vat, invoiceid, sequence_no
                                    FROM app_opex_invoice_other_details_temp
                                    LEFT JOIN app_opex_report re on re.id=tempid                                       
                                    GROUP BY month_num,invoiceid,sequence_no");
                                    
                                    

  $group2_query = [];


  foreach($grouped_stats2 as $result){  
    $group2_query[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'], 'service_name' => $result['service_name'],'accountname' => $result['accountname'],'invoice_no' => $result['invoice_no'], 'service_key' => $result['service_key'], 'cost_center' => $result['cost_center'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat'],'invoiceid' => $result['invoiceid']];
  }


  $grouped2_array = array_chunk($group2_query,100);


  for($i=0;$i<count($grouped2_array);$i++){
    $values = '';
    foreach($grouped2_array[$i] AS $row){ 
      $values .= "('".$row['tempid']."','".$row['sequence_no']."','".$row['service_name']."','".$row['accountname']."','".$row['invoice_no']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total_with_vat']."','".$row['invoiceid']."','$date'),";
    }
    $values = rtrim($values,',');

    $conn->query("INSERT INTO app_opex_invoice_other_details (did,sequence_no,service_name,accountname,invoice_no,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date) VALUES $values");
  }       

  // Nepakliuve i sarasa kastu centrai pabaiga


$vat = $_GET['vat'];
if(empty($_REQUEST['cron'])){
  header("location:/index.php?module=Opex&view=List&year=$year&vat=$vat");
}