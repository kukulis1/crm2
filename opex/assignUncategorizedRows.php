<?php
require_once "../config.inc.php";


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  if($_GET['cron']){
    $setDate = date('Y-m-d',strtotime("-1 days"));  
  }else{
    $setDate = date('Y-m-d');  
  }

  $date = date("Y-m-d H:i:s");   
  $year = ($_GET['year'] ?: date('Y'));

  $checkPurchaseServices = $conn->query("SELECT cost, c.costcenter_tks_cost AS cost_center_name 
                                          FROM app_opex_cost_list l
                                          LEFT JOIN vtiger_costcenter c ON c.costcenterid=l.cost_center
                                          WHERE DATE_FORMAT(last_update,'%Y-%m-%d') = '$setDate'");


  foreach ($checkPurchaseServices as $val) {      
    $unsigned_costs = $conn->query("SELECT * FROM app_opex_other_details WHERE cost_center = '".$val['cost_center_name']."' AND service_key IN  (".$val['cost'].")");
    foreach ($unsigned_costs as $row) {
      $conn->query("INSERT INTO app_opex_details (did, service_name, service_key, cost_center, total_without_vat, total_with_vat, purchaseorderid, update_date) VALUES ('".$row['did']."','".$row['service_name']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total_with_vat']."','".$row['purchaseorderid']."','$date')");
      $conn->query("DELETE FROM app_opex_other_details WHERE cost_center = '".$row['cost_center']."' AND service_key = '".$row['service_key']."'");
    }
  }

  $checkSalesOrderServices = $conn->query("SELECT cost, c.costcenter_tks_cost AS cost_center_name 
                                      FROM app_opex_invoice_cost_list l
                                      LEFT JOIN vtiger_costcenter c ON c.costcenterid=l.cost_center
                                      WHERE DATE_FORMAT(last_update,'%Y-%m-%d') = '$setDate'");

  foreach ($checkSalesOrderServices as $item) {     
      $unsigned_costs = $conn->query("SELECT * FROM app_opex_invoice_other_details WHERE cost_center = '".$item['cost_center_name']."' AND service_key IN  (".$item['cost'].")");
      foreach ($unsigned_costs as $rows) {
        $conn->query("INSERT INTO app_opex_invoice_details (did, service_name, service_key, cost_center, total_without_vat, total_with_vat, invoiceid, update_date) VALUES ('".$rows['did']."','".$rows['service_name']."','".$rows['service_key']."','".$rows['cost_center']."','".$rows['total_without_vat']."','".$rows['total_with_vat']."','".$rows['invoiceid']."','$date')");
        $conn->query("DELETE FROM app_opex_invoice_other_details WHERE cost_center = '".$rows['cost_center']."' AND service_key = '".$rows['service_key']."'");
      }
  }   


  $moveToUnsigned = $conn->query("SELECT service_key,costcenterid,purchaseorderid FROM app_opex_move_to_unsigned");
  foreach ($moveToUnsigned as $unsigned) {
    $check = mysqli_num_rows($conn->query("SELECT * FROM app_opex_cost_list  WHERE cost_center = ".$unsigned['costcenterid']." AND find_in_set(".$unsigned['service_key'].", cost)"));
    if(!$check){     
      $orders = $conn->query("SELECT u.did ,cargo_wgt AS service_name, it.itemserviceid AS service_key, c.costcenter_tks_cost AS cost_center, vendorname,purchaseorder_no, SUM(listprice*quantity) AS total_without_vat,
                                SUM(listprice*quantity) + SUM((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) AS total_with_vat,p.purchaseorderid
                                FROM vtiger_purchaseorder p 
                                JOIN vtiger_purchaseordercf pc ON pc.purchaseorderid=p.purchaseorderid
                                JOIN vtiger_inventoryproductrel i ON i.id=p.purchaseorderid
                                JOIN vtiger_vendor v ON v.vendorid=p.vendorid
                                JOIN vtiger_itemservice it ON it.itemservice_tks_name=cargo_wgt                              
                                JOIN vtiger_costcenter c ON c.costcenterid=i.cargo_width
                                JOIN app_opex_move_to_unsigned u ON u.purchaseorderid=p.purchaseorderid
                                LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=p.region_id)       
                                WHERE p.purchaseorderid IN (".$unsigned['purchaseorderid'].") AND cargo_width = ".$unsigned['costcenterid']." AND itemserviceid = ".$unsigned['service_key']."
                                GROUP BY p.purchaseorderid");
         foreach ($orders AS $value) {          
           $conn->query("INSERT INTO app_opex_other_details (did, service_name, vendorname, purchaseorder_no, service_key, cost_center, total_without_vat, total_with_vat, purchaseorderid, create_date) 
           VALUES ('".$value['did']."','".$value['service_name']."','".$value['vendorname']."','".$value['purchaseorder_no']."','".$value['service_key']."','".$value['cost_center']."','".$value['total_without_vat']."','".$value['total_with_vat']."','".$value['purchaseorderid']."','$date')");
            $conn->query("DELETE FROM app_opex_details WHERE did = ".$value['did']." AND service_key = ".$value['service_key']." AND cost_center = '".$value['cost_center']."'");
         }                              
    }
  }
  $conn->query("TRUNCATE app_opex_move_to_unsigned");

  $moveToUnsigned2 = $conn->query("SELECT service_key,costcenterid,invoiceid FROM app_opex_move_invoice_to_unsigned");
  foreach ($moveToUnsigned2 as $unsigned) {
    $check = mysqli_num_rows($conn->query("SELECT * FROM app_opex_invoice_cost_list  WHERE cost_center = ".$unsigned['costcenterid']." AND find_in_set(".$unsigned['service_key'].", cost)"));
    if(!$check){          
      $orders = $conn->query("SELECT u.did ,s.name AS service_name, i.service AS service_key, c.costcenter_tks_cost AS cost_center, accountname,invoice_no, SUM(listprice*quantity) AS total_without_vat, SUM(listprice*quantity) + SUM((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) AS total_with_vat,inv.invoiceid
                                        FROM vtiger_invoice inv 
                                        JOIN vtiger_inventoryproductrel i ON i.id=inv.invoiceid
                                        JOIN vtiger_account a ON a.accountid=inv.accountid
                                        JOIN app_services_type s ON s.id=i.service
                                        JOIN vtiger_costcenter c ON c.costcenterid=i.costcenter  
                                        JOIN app_opex_move_invoice_to_unsigned u ON u.invoiceid=inv.invoiceid
                                        LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id       
                                        WHERE inv.invoiceid in (".$unsigned['invoiceid'].") AND costcenter = ".$unsigned['costcenterid']." AND service = ".$unsigned['service_key']."
                                        GROUP BY inv.invoiceid");
         foreach ($orders AS $value) {  
           $conn->query("INSERT INTO app_opex_invoice_other_details (did, service_name, accountname, invoice_no, service_key, cost_center, total_without_vat, total_with_vat, invoiceid, create_date) 
           VALUES ('".$value['did']."','".$value['service_name']."','".$value['accountname']."','".$value['invoice_no']."','".$value['service_key']."','".$value['cost_center']."','".$value['total_without_vat']."','".$value['total_with_vat']."','".$value['invoiceid']."','$date')");
            $conn->query("DELETE FROM app_opex_invoice_details WHERE did = ".$value['did']." AND service_key = ".$value['service_key']." AND cost_center = '".$value['cost_center']."'");
         }                              
    }
  }
  $conn->query("TRUNCATE app_opex_move_invoice_to_unsigned");

  if(!$_GET['cron']){
    $vat = $_GET['vat'];
    header("location:/index.php?module=Opex&view=List&year=$year&vat=$vat");
  }
  
  