<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $invoiceid = $_POST['invoiceid'];
  $type = $_POST['type'];
  $service = $_POST['service'];


  $query = "SELECT IF(productid = 121391,i.listprice * quantity,i.listprice) AS listprice, ((((IF(productid = 121391,(i.listprice * quantity),i.listprice)) * IF(inv.region_id > 0, value, 21)) /100) + IF(productid = 121391,(i.listprice * quantity),i.listprice)) AS listprice_vat, a.accountname, inv.invoice_no, inv.invoiceid,invoicedate, IF(productid = 121391,itemservice_tks_name,s.name) as service_name, sequence_no
                        FROM vtiger_inventoryproductrel i                        
                        LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
                        LEFT JOIN vtiger_account a ON a.accountid=inv.accountid
                        LEFT JOIN vtiger_crmentity e ON e.crmid=i.id  
                        LEFT JOIN app_services_type s ON s.id=i.service   
                        LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
                        LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id             
                        WHERE i.id IN ($invoiceid) AND e.deleted = 0 AND e.setype = 'Invoice' AND IF(productid = 121391,itemserviceid, service) IN ($service)  AND costcenter IN ($type)";

  $get_list = $conn->query($query);


  $list = array();

  foreach($get_list as $row){
    $list[] = $row;
  }


  echo  json_encode($list);

}else{
  http_response_code(404);
}