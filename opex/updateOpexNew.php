<?php
require_once "../config.inc.php";

error_reporting(1);


  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
  $year = ($_GET['year'] ?: date('Y'));

  if($year == date('Y')){
    $how_much_month = date("m");
  }else{
    $how_much_month = 12;
  }


$detail_num = $conn->query("SELECT id, month_num FROM app_opex_report WHERE years = $year");

foreach($detail_num AS $result) {
  // $first = $conn->query("DELETE FROM app_opex_report WHERE years = $year AND month_num = $mon");

  $second = $conn->query("DELETE FROM app_opex_details WHERE did = '".$result['id']."'");

  $third = $conn->query("DELETE FROM app_opex_transport_orders WHERE tid = '".$result['id']."'");

  $fourt = $conn->query("DELETE FROM app_storage_service WHERE `year` = $year AND `month` = '".$result['month_num']."'");

  $fifth = $conn->query("DELETE FROM app_stevedoring_service WHERE `year` = $year AND `month` = '".$result['month_num']."'");

  $six = $conn->query("DELETE FROM app_storage_independent_service WHERE `year` = $year AND `month` = '".$result['month_num']."'");

  $seven = $conn->query("TRUNCATE app_opex_details_temp");

  $eight = $conn->query("DELETE FROM app_opex_other_details WHERE did = '".$result['id']."'");

  $nine = $conn->query("DELETE FROM app_opex_cost_to_month WHERE id = '".$result['id']."'");

  $ten = $conn->query("DELETE FROM app_opex_other_cost_to_month WHERE id = '".$result['id']."'");

  $eleven = $conn->query("TRUNCATE app_opex_other_details_temp");

  $twelve = $conn->query("DELETE FROM app_opex_invoice_profit_without_list WHERE `year` = $year AND `month` = '".$result['month_num']."'");

  $thirteen = $conn->query("DELETE FROM app_forwarding_service WHERE `year` = $year AND `month` = '".$result['month_num']."'");
}


for($n=1; $n <= $how_much_month; $n++){  


  if($n == 1){

    $month = 'January';
    $month_n = '01'; 

  }elseif($n == 2){

    $month = 'February';
    $month_n = '02';

  }elseif($n == 3){

    $month = 'March';
    $month_n = '03';

  }elseif($n == 4){

    $month = 'April';
    $month_n = '04';

  }elseif($n == 5){

    $month = 'May';
    $month_n = '05';

  }elseif($n == 6){

    $month = 'June';
    $month_n = '06';

  }elseif($n == 7){

    $month = 'July';
    $month_n = '07';

  }elseif($n == 8){

    $month = 'August';
    $month_n = '08';  

  }elseif($n == 9){

    $month = 'September';
    $month_n = '09';

  }elseif($n == 10){

    $month = 'October';
    $month_n = '10';   

  }elseif($n == 11){
    $month = 'November';
    $month_n = '11'; 

  }elseif($n == 12){

    $month = 'December';
    $month_n = '12';   
  }
 

  $last_update_time = mysqli_fetch_assoc($conn->query("SELECT last_update FROM app_opex_report ORDER BY id DESC LIMIT 1"));
  $get_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_cost_list");

  $stevedoring_cost = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT cost) AS cost FROM app_opex_invoice_cost_list WHERE name IN ('stevedoring_profit_klaipeda','stevedoring_profit_vilnius','stevedoring_profit_kaunas')"));
  $stevedoring_cost = implode(',',array_unique(explode(',', $stevedoring_cost['cost'])));

  $storage_main_cost = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT cost) AS cost FROM app_opex_invoice_cost_list WHERE name IN ('storage_profit_vilnius','storage_profit_kaunas','storage_profit_klaipeda')"));
  $storage_main_cost = implode(',',array_unique(explode(',', $storage_main_cost['cost'])));

  $storage_invoice_costs = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT cost) AS cost, GROUP_CONCAT(DISTINCT cost_center) AS cost_center FROM app_opex_invoice_free_cost_list"));
  $storage_costs = implode(',',array_unique(explode(',', $storage_invoice_costs['cost'])));
  $storage_costs_center = implode(',',array_unique(explode(',', $storage_invoice_costs['cost_center'])));
  $storage_string = mysqli_fetch_assoc($conn->query("SELECT DISTINCT GROUP_CONCAT(DISTINCT \"'\",itemservice_tks_name,\"'\") AS itemservice_name 
                                                      FROM vtiger_itemservice i 
                                                      LEFT JOIN vtiger_crmentity e ON e.crmid=i.itemserviceid                                  
                                                      WHERE e.deleted = 0 AND itemserviceid IN ($storage_costs)"));

  $storage_string = $storage_string['itemservice_name'];

  $common_invoice_cost = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT cost) AS cost, GROUP_CONCAT(DISTINCT cost_center) AS cost_center FROM app_opex_invoice_cost_list"));
  $common_costs = implode(',',array_unique(explode(',', $common_invoice_cost['cost'])));
  $common_costs_center = implode(',',array_unique(explode(',', $common_invoice_cost['cost_center'])));

  $forward_cost = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT cost) AS cost, GROUP_CONCAT(DISTINCT cost_center) AS cost_center FROM app_opex_invoice_cost_list WHERE name = 'forwarding_profit'"));
  $forward_costs = implode(',',array_unique(explode(',', $forward_cost['cost'])));
  $forward_costs_center = implode(',',array_unique(explode(',', $forward_cost['cost_center'])));


  $mon = $n;

  if($n != 10 AND $n != 11 AND $n != 12){
    $mon = "0$n";
  }  

  $from = $year."-$mon-01";
  $to = $year."-$mon-31";
  $date = $year."-$mon-01 23:59:59";


  $except_costs = '';

  $stats_query = "SELECT (listprice*quantity) AS other_total, DATE_FORMAT(cf_1345, '%Y') as years ,DATE_FORMAT(cf_1345, '%m') as month_num, po.purchaseorderid, cargo_wgt as service, sequence_no, itemserviceid, costcenter_tks_cost,vtiger_taxregions.value as vatprocent, v.vendorname, po.purchaseorder_no, 
  CASE ";
        foreach($get_cost_centers_with_costs as $cost){
          $cost_center = $cost['cost_center'];
          $costs = explode(",", $cost['cost']);         
          if($cost['name'] == 'other_klaipeda')  $except_costs = $costs;
          for($i =0; $i < count($costs); $i++){
          $stats_query .= " WHEN cargo_width = $cost_center AND itemserviceid = $costs[$i] THEN (listprice*quantity)";
          }
        }
      $stats_query .="  END AS total
      FROM vtiger_inventoryproductrel i
      LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
      LEFT JOIN vtiger_purchaseorder po on po.purchaseorderid=i.id
      LEFT JOIN vtiger_purchaseordercf pocf on po.purchaseorderid=pocf.purchaseorderid
      LEFT JOIN vtiger_vendor v ON v.vendorid=po.vendorid
      LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
      LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.cargo_width
      LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=po.region_id)       
      WHERE e.deleted = 0  AND e.setype = 'PurchaseOrder'  AND (e.createdtime BETWEEN '$from' AND '$to' OR DATE_FORMAT(e.modifiedtime, 'Y-m-d') BETWEEN '$from' AND '$to') AND itemserviceid IS NOT NULL 
      AND cargo_width IN (103914,103916,103917,103918,103919,103920,103921)
      GROUP BY  id,sequence_no                                         
      ORDER BY i.id DESC";


  $stats = $conn->query($stats_query);

  $first_day_of_month = $year."-$mon-01";


//   $transport_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(salesorderid) AS transport_orders
//                                       FROM vtiger_salesorder s
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
//                                       WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to' "));

//   $transport_orders_pll_places = mysqli_fetch_assoc($conn->query("SELECT DISTINCT SUM(i.pll) AS pll_places
//                                       FROM vtiger_salesorder s
//                                       LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
//                                       WHERE e.deleted =0 AND setype = 'SalesOrder' AND e.createdtime BETWEEN '$first_day_of_month'  AND '$to' AND i.pll != 0 "));

//   $transport_total_weight = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(cargo_wgt),2) AS total_weight
//                                       FROM vtiger_salesorder s
//                                       LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
//                                       WHERE e.deleted =0 AND setype = 'SalesOrder' AND e.createdtime BETWEEN '$first_day_of_month' AND '$to' "));

//   $transport_movement_orders = mysqli_fetch_assoc($conn->query("SELECT COUNT(inv.salesorderid) AS transport_movement_orders, REPLACE(ROUND(SUM(inv.total),2),'.',',') as total
//                                       FROM vtiger_invoice inv                  
//                                       JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid 
//                                       JOIN vtiger_invoicecf i ON i.invoiceid=inv.invoiceid
//                                       WHERE deleted = 0 AND setype = 'Invoice' AND cf_1486 = 'Perkraustymas' AND (invoicedate BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to')")); 
                                      
//   $movement_storage_profit = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(listprice),2) AS total
//                                       FROM vtiger_inventoryproductrel i
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id                                 
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND cargo_wgt = 'Perkraustymo sandėliavimo pajamos' AND (createdtime BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to')"));

//   $pack_material_profit = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(listprice),2) AS total 
//                                       FROM vtiger_inventoryproductrel i
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id                                  
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND cargo_wgt = 'Pakavimo medžiagų pardavimas' AND (createdtime BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to') "));
                                      
//   $transport_cancel_orders = mysqli_fetch_assoc($conn->query("SELECT SUM(s.total) AS total
//                                       FROM vtiger_salesorder s
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
//                                       LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
//                                       WHERE e.deleted =0 AND setype = 'SalesOrder' AND scf.cf_1614 = 1 AND (e.createdtime BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to') "));

//   $storage_price_arr = $conn->query("SELECT invoiceid,id, i.productid, SUM(i.listprice) AS listprice, i.costcenter,i.service as service_key, co.costcenter_tks_cost AS cost, co.costcenterid, DATE_FORMAT(invoicedate, '%m') as month,DATE_FORMAT(invoicedate, '%Y') as year,CASE WHEN inv.region_id = 0 THEN 21 ELSE vtiger_taxregions.value END AS vatprocent
//                                       FROM vtiger_inventoryproductrel i 
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
//                                       LEFT JOIN vtiger_costcenter co ON co.costcenterid=i.costcenter
//                                       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id     
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND i.productid = 36641 AND service IN ($storage_main_cost) AND (invoicedate BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to')  AND costcenterid != '' 
//                                       GROUP BY id,service,costcenter_tks_cost,DATE_FORMAT(inv.invoicedate, '%m')");

//   $storage_independent = $conn->query("SELECT invoiceid,ROUND(SUM(listprice * quantity),2) AS total,costcenter_tks_cost, i.service as service_key, DATE_FORMAT(invoicedate, '%m') as month,DATE_FORMAT(invoicedate, '%Y') as year,CASE WHEN inv.region_id = 0 THEN 21 ELSE vtiger_taxregions.value END AS vatprocent
//                                       FROM vtiger_inventoryproductrel i
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id                                   
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
//                                       LEFT JOIN vtiger_costcenter co ON co.costcenterid=i.costcenter
//                                       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id     
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND i.productid = 121391 AND cargo_wgt IN ($storage_string) AND (invoicedate BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d')  BETWEEN '$first_day_of_month' AND '$to') AND costcenter_tks_cost != '' 
//                                       GROUP BY id,service,costcenter_tks_cost,DATE_FORMAT(inv.invoicedate, '%m')");                                    

//   $stevedoring_price_arr = $conn->query("SELECT invoiceid, id, i.productid, SUM(i.listprice) AS listprice, i.service as service_key, co.costcenter_tks_cost AS cost, co.costcenterid, DATE_FORMAT(invoicedate, '%m') as month,DATE_FORMAT(invoicedate, '%Y') as year,CASE WHEN inv.region_id = 0 THEN 21 ELSE vtiger_taxregions.value END AS vatprocent
//                                       FROM vtiger_inventoryproductrel i 
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
//                                       LEFT JOIN vtiger_costcenter co ON co.costcenterid=i.costcenter
//                                       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id     
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND i.productid = 36641 AND service IN ($stevedoring_cost) AND (invoicedate BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to')  AND costcenterid != '' 
//                                       GROUP BY id,service,costcenter_tks_cost,DATE_FORMAT(inv.invoicedate, '%m')");

//   $profit_without_list = $conn->query("SELECT invoiceid, SUM(i.listprice) AS listprice,accountname, invoice_no, i.service as service_key,ty.name, co.costcenter_tks_cost AS cost, co.costcenterid, DATE_FORMAT(invoicedate, '%m') as month,DATE_FORMAT(invoicedate, '%Y') as year,CASE WHEN inv.region_id = 0 THEN 21 ELSE vtiger_taxregions.value END AS vatprocent
//                                       FROM vtiger_inventoryproductrel i 
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
//                                       LEFT JOIN vtiger_account a ON a.accountid=inv.accountid
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
//                                       LEFT JOIN vtiger_costcenter co ON co.costcenterid=i.costcenter
//                                       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id    
//                                       LEFT JOIN app_services_type ty ON ty.id=service
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND i.productid = 36641 AND (invoicedate BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to')  AND costcenterid != '' AND IF(costcenter IN ($common_costs_center),service NOT IN ($common_costs), 1+1)
//                                       GROUP BY i.id,service,costcenter_tks_cost,DATE_FORMAT(inv.invoicedate, '%m')");    
                                      
                                      
//   $profit_without_list_independent = $conn->query("SELECT invoiceid,ROUND(SUM(listprice * quantity),2) AS listprice,costcenter_tks_cost as cost,accountname,costcenterid, invoice_no, i.service as service_key,cargo_wgt as name, DATE_FORMAT(invoicedate, '%m') as month,DATE_FORMAT(invoicedate, '%Y') as year,CASE WHEN inv.region_id = 0 THEN 21 ELSE vtiger_taxregions.value END AS vatprocent
//                                       FROM vtiger_inventoryproductrel i
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id  
//                                       LEFT JOIN vtiger_account a ON a.accountid=inv.accountid                                 
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
//                                       LEFT JOIN vtiger_costcenter co ON co.costcenterid=i.costcenter
//                                       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id     								
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND i.productid = 121391 AND (invoicedate BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d')  BETWEEN '$first_day_of_month' AND '$to') 
//                                       AND costcenter_tks_cost != '' AND IF(costcenter IN ($storage_costs_center),cargo_wgt NOT IN ($storage_string), 1+1)
//                                       GROUP BY i.id,service,costcenter_tks_cost,DATE_FORMAT(inv.invoicedate, '%m')");  

// $profit_forward_list = $conn->query("SELECT invoiceid, id, i.productid, SUM(i.listprice) AS listprice, i.service as service_key, co.costcenter_tks_cost AS cost, co.costcenterid, DATE_FORMAT(invoicedate, '%m') as month,DATE_FORMAT(invoicedate, '%Y') as year,CASE WHEN inv.region_id = 0 THEN 21 ELSE vtiger_taxregions.value END AS vatprocent
//                                       FROM vtiger_inventoryproductrel i 
//                                       LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
//                                       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
//                                       LEFT JOIN vtiger_costcenter co ON co.costcenterid=i.costcenter
//                                       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id     
//                                       WHERE e.deleted = 0 AND e.setype = 'Invoice' AND i.productid = 36641 AND service IN ($forward_costs) AND (invoicedate BETWEEN '$first_day_of_month' AND '$to' OR DATE_FORMAT(e.modifiedtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to')  AND costcenterid IN  ($forward_costs_center) 
//                                       GROUP BY id,service,costcenter_tks_cost,DATE_FORMAT(inv.invoicedate, '%m')");           
                   

                                      
$check_last = mysqli_fetch_assoc($conn->query("SELECT * FROM app_opex_report ORDER BY id DESC LIMIT 1"));


if(($check_last['years'] < $year ? true : $check_last['years'] == $year && $check_last['month_num'] < $month_n )){ 
  $conn->query("INSERT INTO app_opex_report (`years`,`months`,`month_num`,`last_update`) VALUES ('$year','$month','$month_n','$date')");
}else{
  $conn->query("UPDATE app_opex_report SET `last_update` = '$date' WHERE month_num = $month_n");
}


  foreach($stats as $row){ 
    if(!empty($row['total'])){  
       $conn->query("DELETE FROM app_opex_details_temp WHERE purchaseorderid = '".$row['purchaseorderid']."'");      
    }else{
      $conn->query("DELETE FROM app_opex_other_details_temp WHERE purchaseorderid = '".$row['purchaseorderid']."'");      
    }
  }

  foreach($stats as $row){ 
    $vat_procent = $row['vatprocent'];
    if(empty($vat_procent)) $vat_procent = 21;
    $vat = $row['total'] * $vat_procent/100;
    $vat2 = $row['other_total'] * $vat_procent/100;
    $total = $row['total'] + $vat;

    $other_total = $row['other_total'] + $vat2;

    $cost_center = $row['costcenter_tks_cost'];
    if( in_array($row['itemserviceid'], $except_costs)  AND $row['costcenter_tks_cost'] == 'KLAIPĖDOS SANDĖLIS') $cost_center = $row['costcenter_tks_cost']."2"; 

    $get_id = mysqli_fetch_assoc($conn->query("SELECT id FROM app_opex_report WHERE years = '".$row['years']."' AND month_num = '".$row['month_num']."'"));
    $id = $get_id['id'];

    if(!empty($row['total'])){      
      $conn->query("INSERT INTO app_opex_cost_to_month (id,years, month, purchaseorderid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['purchaseorderid']."','".$row['itemserviceid']."','$cost_center','$date')");

       $conn->query("INSERT INTO app_opex_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,sequence_no,update_date) VALUES ('$id', '".$row['service']."','".$row['itemserviceid']."','$cost_center','".$row['total']."','$total','".$row['purchaseorderid']."','".$row['sequence_no']."','$date')");
    }else{
      $conn->query("INSERT INTO app_opex_other_cost_to_month (id,years, month, purchaseorderid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['purchaseorderid']."','".$row['itemserviceid']."','$cost_center','$date')");

      $conn->query("INSERT INTO app_opex_other_details_temp (tempid,service_name,vendorname,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,purchaseorder_no,sequence_no,update_date) VALUES ('$id', '".$row['service']."','".$row['vendorname']."','".$row['itemserviceid']."','$cost_center','".$row['other_total']."','$other_total','".$row['purchaseorderid']."','".$row['purchaseorder_no']."','".$row['sequence_no']."','$date')");
    }

  

    $check_dublicate = $conn->query("SELECT * FROM app_opex_cost_to_month WHERE purchaseorderid = '".$row['purchaseorderid']."' AND service_key = '".$row['itemserviceid']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate);

    if($num_rows >= 2){

      $orders_array = array();

      foreach($check_dublicate AS $rez){
        $orders_array[] = $rez;
      }

      $first_num = $orders_array[0];
      $second_num = $orders_array[1];
     

      if(strtotime($first_num['update_date']) < strtotime($second_num['update_date'])){
        $delete_order_id = $first_num['id'];
        $delete_order_service_key = $first_num['service_key'];
        $delete_order_cost_center = $first_num['cost_center'];
        $delete_order_purchaseorderid = $first_num['purchaseorderid'];
      }else{
        $delete_order_id = $second_num['id'];
        $delete_order_service_key = $second_num['service_key'];
        $delete_order_cost_center = $second_num['cost_center'];
        $delete_order_purchaseorderid = $second_num['purchaseorderid'];
      }

      $conn->query("DELETE FROM app_opex_details WHERE did = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND purchaseorderid = $delete_order_purchaseorderid ");

      $conn->query("DELETE FROM app_opex_cost_to_month WHERE id = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND purchaseorderid = $delete_order_purchaseorderid");     
    }


    // Nepakliuve i sarasa kastu centrai

    $check_dublicate2 = $conn->query("SELECT * FROM app_opex_other_cost_to_month WHERE purchaseorderid = '".$row['purchaseorderid']."' AND service_key = '".$row['itemserviceid']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate2);

    if($num_rows >= 2){

      $orders_array2 = array();

      foreach($check_dublicate2 AS $rez){
        $orders_array2[] = $rez;
      }

      $first_num2 = $orders_array2[0];
      $second_num2 = $orders_array2[1];
     

      if(strtotime($first_num2['update_date']) < strtotime($second_num2['update_date'])){
        $delete_order_id2 = $first_num2['id'];
        $delete_order_service_key2 = $first_num2['service_key'];
        $delete_order_cost_center2 = $first_num2['cost_center'];
        $delete_order_purchaseorderid2 = $first_num2['purchaseorderid'];
      }else{
        $delete_order_id2 = $second_num2['id'];
        $delete_order_service_key2 = $second_num2['service_key'];
        $delete_order_cost_center2 = $second_num2['cost_center'];
        $delete_order_purchaseorderid2 = $second_num2['purchaseorderid'];
      }

      $conn->query("DELETE FROM app_opex_other_details WHERE did = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND purchaseorderid = $delete_order_purchaseorderid2 ");

      $conn->query("DELETE FROM app_opex_other_cost_to_month WHERE id = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND purchaseorderid = $delete_order_purchaseorderid2");     
    }

     // Nepakliuve i sarasa kastu centrai pabaiga


}

  $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, GROUP_CONCAT(DISTINCT purchaseorderid) AS purchaseorderid
                            FROM app_opex_details_temp
                            LEFT JOIN app_opex_report re on re.id=tempid                                       
                            GROUP BY service_key,cost_center, month_num");


    foreach($grouped_stats as $result){   
      $check_cost = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' "));

      if(empty($check_cost['service_name'])){
        $conn->query("INSERT INTO app_opex_details (did,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['purchaseorderid']."','$date')");
      }else{
        $conn->query("UPDATE app_opex_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', purchaseorderid = '".$result['purchaseorderid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
      }    
    }

  // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,vendorname,purchaseorder_no,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, GROUP_CONCAT(DISTINCT purchaseorderid) AS purchaseorderid
                                    FROM app_opex_other_details_temp
                                    LEFT JOIN app_opex_report re on re.id=tempid                                       
                                    GROUP BY service_key,cost_center, month_num");


foreach($grouped_stats2 as $result){   
  $check_cost2 = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_other_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' "));

  if(empty($check_cost2['service_name'])){
    $conn->query("INSERT INTO app_opex_other_details (did,service_name,vendorname,purchaseorder_no,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['vendorname']."','".$result['purchaseorder_no']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['purchaseorderid']."','$date')");
  }else{
    $conn->query("UPDATE app_opex_other_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', purchaseorderid = '".$result['purchaseorderid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
  }    
}
// Nepakliuve i sarasa kastu centrai pabaiga


    $get_month_id = mysqli_fetch_assoc($conn->query("SELECT id FROM app_opex_report WHERE years = '".$transport_orders['years']."' AND month_num = '".$transport_orders['month_num']."' "));

    $check_month = mysqli_fetch_assoc($conn->query("SELECT `tid` FROM app_opex_transport_orders WHERE `tid` = '".$get_month_id['id']."'"));



//     $month_id =  (int)$get_month_id['id'];


// if(!$check_month){    
//     $conn->query("INSERT INTO app_opex_transport_orders  (tid,
//                                                           total_orders,
//                                                           total_parcel_orders,
//                                                           total_parcel_sum,
//                                                           total_cancel_orders,
//                                                           total_orders_pll_places,                                                      
//                                                           total_weight,
//                                                           total_movement_storage_profit,
//                                                           total_pack_material_profit,
//                                                           update_date) 
//                                                           VALUES ('$month_id',
//                                                                   '".$transport_orders['transport_orders']."',
//                                                                   '".$transport_movement_orders['transport_movement_orders']."',
//                                                                   '".$transport_movement_orders['total']."',
//                                                                   '".$transport_cancel_orders['total']."',
//                                                                   '".$transport_orders_pll_places['pll_places']."',
//                                                                   '".$transport_total_weight['total_weight']."',
//                                                                   '".$movement_storage_profit['total']."',
//                                                                   '".$pack_material_profit['total']."',
//                                                                   '$date')");
// }else{
//   $conn->query("UPDATE app_opex_transport_orders SET total_orders =  '".$transport_orders['transport_orders']."',
//                                                      total_parcel_orders =  '".$transport_movement_orders['transport_movement_orders']."',
//                                                      total_parcel_sum =  '".$transport_movement_orders['total']."',
//                                                      total_cancel_orders =  '".$transport_cancel_orders['total']."',
//                                                      total_orders_pll_places =  '".$transport_orders_pll_places['pll_places']."',
//                                                      total_weight = '".$transport_total_weight['total_weight']."',
//                                                      total_movement_storage_profit = '".$movement_storage_profit['total']."',
//                                                      total_pack_material_profit =  '".$pack_material_profit['total']."',
//                                                      update_date = '$date' WHERE tid = $month_id"); 
// }






  // foreach($stevedoring_price_arr as $row){      
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];  
  //   $conn->query("DELETE FROM app_stevedoring_service WHERE year = $year_num AND `month` = $month_num AND invoiceid = '".$row['invoiceid']."'");  
  // }

  // foreach($stevedoring_price_arr as $row){
  //   $vat_procent = $row['vatprocent'];
  //   $vat = $row['listprice'] * $vat_procent/100;
  //   $total = $row['listprice'] + $vat;
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];   
  //   $conn->query("INSERT INTO app_stevedoring_service (`year`,`month`,`cost_center`,`service_key`,`listprice`,`invoiceid`,`update_date`) VALUES($year_num,'$month_num','".$row['cost']."','".$row['service_key']."', '$total','".$row['id']."', '$date')");
  // }


  // foreach($storage_price_arr as $row){      
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];  
  //   $conn->query("DELETE FROM app_storage_service WHERE year = $year_num AND `month` = $month_num AND invoiceid = '".$row['invoiceid']."'"); 
  // } 

  // foreach($storage_price_arr as $row){ 
  //   $vat_procent = $row['vatprocent'];
  //   $vat = $row['listprice'] * $vat_procent/100;
  //   $total = $row['listprice'] + $vat;  
  //   $month_num = (int)$row['month'];    
  //   $year_num = $row['year'];    
  //   $conn->query("INSERT INTO app_storage_service (`year`,`month`,`cost_center`,`service_key`,`listprice`, `invoiceid`,`update_date`) VALUES($year_num,'$month_num','".$row['cost']."','".$row['service_key']."', '$total','".$row['id']."', '$date')");
  // }

  // foreach($storage_independent as $row){      
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];  
  //   $conn->query("DELETE FROM app_storage_independent_service WHERE year = $year_num AND `month` = $month_num AND invoiceid = '".$row['invoiceid']."'"); 
  // }

  // foreach($storage_independent as $row){
  //   $month_num = (int)$row['month'];
  //   $vat_procent = $row['vatprocent'];
  //   $vat = $row['total'] * $vat_procent/100;
  //   $total = $row['total'] + $vat;
  //   $year_num = $row['year'];   
  //   $conn->query("INSERT INTO app_storage_independent_service (`year`,`month`,`cost_center`,`service_key`,`listprice`,`invoiceid`,`update_date`) VALUES ($year_num,'$month_num','".$row['costcenter_tks_cost']."','".$row['service_key']."', '$total','".$row['invoiceid']."', '$date')"); 
  // }


  // foreach($profit_without_list as $row){      
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];  
  //   $conn->query("DELETE FROM app_opex_invoice_profit_without_list WHERE year = $year_num AND `month` = $month_num AND invoiceid = '".$row['invoiceid']."'");  
  // }

  // foreach($profit_without_list as $row){
  //   $vat_procent = $row['vatprocent'];
  //   $vat = $row['listprice'] * $vat_procent/100;
  //   $total = $row['listprice'] + $vat;
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];   
  //   $conn->query("INSERT INTO app_opex_invoice_profit_without_list (`invoiceid`,`invoice_no`,`accountname`, `total_with_vat`,`total_with_out_vat`, `service_key`, `name`, `cost`, `costcenterid`, `month`, `year`, `vatprocent`) VALUES ('".$row['invoiceid']."','".$row['invoice_no']."','".$row['accountname']."','$total','".$row['listprice']."','".$row['service_key']."','".$row['name']."','".$row['cost']."','".$row['costcenterid']."','$month_num','$year_num','".$row['vatprocent']."')");
  // }

  // foreach($profit_without_list_independent as $row){      
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];  
  //   $conn->query("DELETE FROM app_opex_invoice_profit_without_list WHERE year = $year_num AND `month` = $month_num AND invoiceid = '".$row['invoiceid']."'"); 
  // }

  // foreach($profit_without_list_independent as $row){
  //   $month_num = (int)$row['month'];
  //   $vat_procent = $row['vatprocent'];
  //   $vat = $row['listprice'] * $vat_procent/100;
  //   $total = $row['listprice'] + $vat;
  //   $year_num = $row['year'];   
  //   $conn->query("INSERT INTO app_opex_invoice_profit_without_list (`invoiceid`,`invoice_no`,`accountname`, `total_with_vat`,`total_with_out_vat`, `service_key`, `name`, `cost`, `costcenterid`, `month`, `year`, `vatprocent`) VALUES ('".$row['invoiceid']."','".$row['invoice_no']."','".$row['accountname']."','$total','".$row['listprice']."','".$row['service_key']."','".$row['name']."','".$row['cost']."','".$row['costcenterid']."','$month_num','$year_num','".$row['vatprocent']."')");
  // }


  // foreach($profit_forward_list as $row){      
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];  
  //   $conn->query("DELETE FROM app_forwarding_service WHERE year = $year_num AND `month` = $month_num AND invoiceid = '".$row['invoiceid']."'");  
  // }

  // foreach($profit_forward_list as $row){
  //   $vat_procent = $row['vatprocent'];
  //   $vat = $row['listprice'] * $vat_procent/100;
  //   $total = $row['listprice'] + $vat;
  //   $month_num = (int)$row['month'];
  //   $year_num = $row['year'];   
  //   $conn->query("INSERT INTO app_forwarding_service (`year`,`month`,`cost_center`,`service_key`,`listprice`,`invoiceid`,`update_date`) VALUES($year_num,'$month_num','".$row['cost']."','".$row['service_key']."', '$total','".$row['id']."', '$date')");
  // }

}


header("location:/index.php?module=Opex&view=List&year=$year");