<?php
require_once "../config.inc.php";

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
  error_reporting(1);

  $last_update_time = mysqli_fetch_assoc($conn->query("SELECT last_update FROM app_opex_report ORDER BY id DESC LIMIT 1"));
  $get_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_cost_list");
  $get_invoice_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_invoice_cost_list");

  $from = $last_update_time['last_update'];
  $to = date("Y-m-d H:i:s");
  $except_costs = array();

  $stats_query ="SELECT (listprice*quantity) AS other_total, DATE_FORMAT(cf_1345, '%Y') as years ,DATE_FORMAT(cf_1345, '%m') as month_num, po.purchaseorderid, cargo_wgt as service, sequence_no, itemserviceid, costcenter_tks_cost,vtiger_taxregions.value as vatprocent, v.vendorname, po.purchaseorder_no,
                          CASE";
          foreach($get_cost_centers_with_costs as $cost){
            $cost_center = $cost['cost_center'];
            $costs = explode(",", $cost['cost']);         
            if($cost['name'] == 'other_klaipeda')  $except_costs = $costs;
            for($i =0; $i < count($costs); $i++){
              if(!empty($cost_center) && !empty($costs[$i])){
               $stats_query .= "\n WHEN cargo_width = $cost_center AND itemserviceid = $costs[$i] THEN (listprice*quantity)";
              }
            }
          }
      $stats_query .=" END AS total
                        FROM vtiger_inventoryproductrel i
                        LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
                        LEFT JOIN vtiger_purchaseorder po on po.purchaseorderid=i.id
                        LEFT JOIN vtiger_purchaseordercf pocf on po.purchaseorderid=pocf.purchaseorderid
                        LEFT JOIN vtiger_vendor v ON v.vendorid=po.vendorid
                        LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
                        LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.cargo_width
                        LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=po.region_id)       
                        WHERE e.deleted = 0  AND e.setype = 'PurchaseOrder'  AND (e.createdtime BETWEEN '$from' AND '$to' OR e.modifiedtime BETWEEN '$from' AND '$to') AND itemserviceid IS NOT NULL 
                        AND cargo_width IN (103914,103916,103917,103918,103919,103920,103921) AND cf_1926 != 1
                        GROUP BY  id,sequence_no                                         
                        ORDER BY i.id DESC";


  $stats = $conn->query($stats_query);

  $year = date("Y");   
  $month = strftime('%B');
  $month_n = date("m");
  $date = date("Y-m-d H:i:s");


  $check_last = mysqli_fetch_assoc($conn->query("SELECT * FROM app_opex_report ORDER BY id DESC LIMIT 1"));

  if(($check_last['years'] < $year ? true : $check_last['month_num'] < $month_n )){ 
    $conn->query("INSERT INTO app_opex_report (`years`,`months`,`month_num`,`last_update`) VALUES ('$year','$month','$month_n','$date')");
  }else{
    $conn->query("UPDATE app_opex_report SET `last_update` = '$date' WHERE month_num = $month_n");
  }

  foreach($stats as $row){ 
    if(!empty($row['total'])){  
       $conn->query("DELETE FROM app_opex_details_temp WHERE purchaseorderid = '".$row['purchaseorderid']."'");      
    }else{
      $conn->query("DELETE FROM app_opex_other_details_temp WHERE purchaseorderid = '".$row['purchaseorderid']."'");      
    }
  }

  foreach($stats as $row){ 
    $vat_procent = $row['vatprocent'];
    if(empty($vat_procent)) $vat_procent = 21;
    $vat = $row['total'] * $vat_procent/100;
    $vat2 = $row['other_total'] * $vat_procent/100;
    $total = $row['total'] + $vat;

    $other_total = $row['other_total'] + $vat2;

    $cost_center = $row['costcenter_tks_cost'];
    if(in_array($row['itemserviceid'], $except_costs) AND $row['costcenter_tks_cost'] == 'KLAIPĖDOS SANDĖLIS') $cost_center = $row['costcenter_tks_cost']."2"; 

    $get_id = mysqli_fetch_assoc($conn->query("SELECT id FROM app_opex_report WHERE years = '".$row['years']."' AND month_num = '".$row['month_num']."'"));
    $id = $get_id['id'];

    if(!empty($row['total'])){      
      $conn->query("INSERT INTO app_opex_cost_to_month (id,years, month, purchaseorderid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['purchaseorderid']."','".$row['itemserviceid']."','$cost_center','$date')");

       $conn->query("INSERT INTO app_opex_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,sequence_no,update_date) VALUES ('$id', '".$row['service']."','".$row['itemserviceid']."','$cost_center','".$row['total']."','$total','".$row['purchaseorderid']."','".$row['sequence_no']."','$date')");
    }else{
      $conn->query("INSERT INTO app_opex_other_cost_to_month (id,years, month, purchaseorderid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['purchaseorderid']."','".$row['itemserviceid']."','$cost_center','$date')");

      $conn->query("INSERT INTO app_opex_other_details_temp (tempid,service_name,vendorname,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,purchaseorder_no,sequence_no,update_date) VALUES ('$id', '".$row['service']."','".$row['vendorname']."','".$row['itemserviceid']."','$cost_center','".$row['other_total']."','$other_total','".$row['purchaseorderid']."','".$row['purchaseorder_no']."','".$row['sequence_no']."','$date')");
    }

  

    $check_dublicate = $conn->query("SELECT * FROM app_opex_cost_to_month WHERE purchaseorderid = '".$row['purchaseorderid']."' AND service_key = '".$row['itemserviceid']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate);

    if($num_rows >= 2){

      $orders_array = array();

      foreach($check_dublicate AS $rez){
        $orders_array[] = $rez;
      }

      $first_num = $orders_array[0];
      $second_num = $orders_array[1];
     

      if(strtotime($first_num['update_date']) < strtotime($second_num['update_date'])){
        $delete_order_id = $first_num['id'];
        $delete_order_service_key = $first_num['service_key'];
        $delete_order_cost_center = $first_num['cost_center'];
        $delete_order_purchaseorderid = $first_num['purchaseorderid'];
      }else{
        $delete_order_id = $second_num['id'];
        $delete_order_service_key = $second_num['service_key'];
        $delete_order_cost_center = $second_num['cost_center'];
        $delete_order_purchaseorderid = $second_num['purchaseorderid'];
      }

      $conn->query("DELETE FROM app_opex_details WHERE did = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND purchaseorderid = $delete_order_purchaseorderid ");

      $conn->query("DELETE FROM app_opex_cost_to_month WHERE id = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND purchaseorderid = $delete_order_purchaseorderid");     
    }


    // Nepakliuve i sarasa kastu centrai

    $check_dublicate2 = $conn->query("SELECT * FROM app_opex_other_cost_to_month WHERE purchaseorderid = '".$row['purchaseorderid']."' AND service_key = '".$row['itemserviceid']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate2);

    if($num_rows >= 2){

      $orders_array2 = array();

      foreach($check_dublicate2 AS $rez){
        $orders_array2[] = $rez;
      }

      $first_num2 = $orders_array2[0];
      $second_num2 = $orders_array2[1];
     

      if(strtotime($first_num2['update_date']) < strtotime($second_num2['update_date'])){
        $delete_order_id2 = $first_num2['id'];
        $delete_order_service_key2 = $first_num2['service_key'];
        $delete_order_cost_center2 = $first_num2['cost_center'];
        $delete_order_purchaseorderid2 = $first_num2['purchaseorderid'];
      }else{
        $delete_order_id2 = $second_num2['id'];
        $delete_order_service_key2 = $second_num2['service_key'];
        $delete_order_cost_center2 = $second_num2['cost_center'];
        $delete_order_purchaseorderid2 = $second_num2['purchaseorderid'];
      }

      $conn->query("DELETE FROM app_opex_other_details WHERE did = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND purchaseorderid = $delete_order_purchaseorderid2 ");

      $conn->query("DELETE FROM app_opex_other_cost_to_month WHERE id = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND purchaseorderid = $delete_order_purchaseorderid2");     
    }

     // Nepakliuve i sarasa kastu centrai pabaiga


  }

  $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, GROUP_CONCAT(DISTINCT purchaseorderid) AS purchaseorderid
                            FROM app_opex_details_temp
                            LEFT JOIN app_opex_report re on re.id=tempid                                       
                            GROUP BY service_key,cost_center, month_num,tempid");


    foreach($grouped_stats as $result){   
      $check_cost = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' "));  
      if(empty($check_cost['service_name'])){
        $conn->query("INSERT INTO app_opex_details (did,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['purchaseorderid']."','$date')");
      }else{
        $conn->query("UPDATE app_opex_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', purchaseorderid = '".$result['purchaseorderid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
      }    
    }

   // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,vendorname,purchaseorder_no,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, purchaseorderid
   FROM app_opex_other_details_temp
   LEFT JOIN app_opex_report re on re.id=tempid                                       
   GROUP BY service_key,cost_center, month_num,purchaseorderid");


  foreach($grouped_stats2 as $result){   
    $check_cost2 = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_other_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' AND purchaseorderid = ".$result['purchaseorderid']."'"));

    if(empty($check_cost2['service_name'])){
    $conn->query("INSERT INTO app_opex_other_details (did,service_name,vendorname,purchaseorder_no,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['vendorname']."','".$result['purchaseorder_no']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['purchaseorderid']."','$date')");
    }else{
    $conn->query("UPDATE app_opex_other_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', purchaseorderid = '".$result['purchaseorderid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
    }    
  }

  // Nepakliuve i sarasa kastu centrai pabaiga

    $first_day_of_month = date("Y-m-d", strtotime("first day of this month"));
    $today = date("Y-m-d");

    $transport_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(salesorderid) AS transport_orders
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));

    $transport_orders_pll_places = mysqli_fetch_assoc($conn->query("SELECT DISTINCT SUM(i.pll) AS pll_places
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today'  AND i.pll != 0 "));

    $transport_total_weight = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(cargo_wgt),2) AS total_weight
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND  AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));

    $transport_cancel_orders = mysqli_fetch_assoc($conn->query("SELECT SUM(s.total) AS total
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND scf.cf_1614 = 1 AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));    


    $get_month_id = mysqli_fetch_assoc($conn->query("SELECT id FROM app_opex_report WHERE years = '".$transport_orders['years']."' AND month_num = '".$transport_orders['month_num']."' "));

    $check_month = mysqli_fetch_assoc($conn->query("SELECT `tid` FROM app_opex_transport_orders WHERE `tid` = '".$get_month_id['id']."'"));
    $check_month2 = mysqli_fetch_assoc($conn->query("SELECT `sid` FROM app_storage_independent_service WHERE `sid` = '".$get_month_id['id']."'"));


    $month_id =  (int)$get_month_id['id'];



    if(!$check_month){    
      $conn->query("INSERT INTO app_opex_transport_orders  (tid,
                                                            total_orders,                                                          
                                                            total_cancel_orders,
                                                            total_orders_pll_places,                                                      
                                                            total_weight,                                                       
                                                            update_date) 
                                                            VALUES ('$month_id',
                                                                    '".$transport_orders['transport_orders']."',                                                                   
                                                                    '".$transport_cancel_orders['total']."',
                                                                    '".$transport_orders_pll_places['pll_places']."',
                                                                    '".$transport_total_weight['total_weight']."',                                                                   
                                                                    '$date')");
  }else{
    $conn->query("UPDATE app_opex_transport_orders SET total_orders =  '".$transport_orders['transport_orders']."',                                                    
                                                      total_cancel_orders =  '".$transport_cancel_orders['total']."',
                                                      total_orders_pll_places =  '".$transport_orders_pll_places['pll_places']."',
                                                      total_weight = '".$transport_total_weight['total_weight']."',                                                     
                                                      update_date = '$date' 
                                                      WHERE tid = $month_id"); 
  }



  $stats_query2 = "SELECT listprice AS other_total, DATE_FORMAT(invoicedate, '%Y') as years ,DATE_FORMAT(invoicedate, '%m') as month_num,
  inv.invoiceid, service, sequence_no, costcenter_tks_cost, IF(vtiger_taxregions.value IS NULL, 21, vtiger_taxregions.value) as vatprocent,
  v.accountname, inv.invoice_no, IF(productid = 121391,itemservice_tks_name,se.name) as service_name, IF(productid = 121391,itemserviceid,i.service) AS service_key,
  CASE ";  
        foreach($get_invoice_cost_centers_with_costs as $cost){
          $cost_center = explode(",", $cost['cost_center']);
          $costs = explode(",", $cost['cost']);
          for($e=0; $e < count($cost_center); $e++){
            if(!empty($cost_center[$e])){
              for($i=0; $i < count($costs); $i++){     
                if(!empty($costs[$i])){       
                  $stats_query2 .= "\n WHEN costcenter = $cost_center[$e] AND ".(strlen($costs[$i]) <= 3 ? 'service': 'itemserviceid')." = $costs[$i] THEN IF(productid = 121391, listprice * quantity, listprice)";
                }
              }
            }
          }
        }
  $stats_query2 .= " END AS total
       FROM vtiger_inventoryproductrel i
       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
       LEFT JOIN vtiger_invoice inv on inv.invoiceid=i.id
       LEFT JOIN vtiger_invoicecf icf on inv.invoiceid=icf.invoiceid
       LEFT JOIN vtiger_account v ON v.accountid=inv.accountid
       LEFT JOIN app_services_type se ON se.id=i.service 
       LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.costcenter
       LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id    
       WHERE e.deleted = 0  AND e.setype = 'Invoice'  AND (e.createdtime BETWEEN '$from' AND '$to' OR e.modifiedtime BETWEEN '$from' AND '$to')  
       AND costcenter IN (103914,103916,103917,103918,103919,103920,103921) AND DATE_FORMAT(invoicedate, '%m') != ''
       GROUP BY  i.id,sequence_no                                         
       ORDER BY i.id DESC";


  $stats2 = $conn->query($stats_query2);

  foreach($stats2 as $row){ 
    if(!empty($row['total'])){  
       $conn->query("DELETE FROM app_opex_invoice_details_temp WHERE invoiceid = '".$row['invoiceid']."'");      
    }else{
       $conn->query("DELETE FROM app_opex_invoice_other_details_temp WHERE invoiceid = '".$row['invoiceid']."'");      
    }
  }


  foreach($stats2 as $row){ 

    $vat_procent = $row['vatprocent'];
    $vat = $row['total'] * $vat_procent/100;
    $vat2 = $row['other_total'] * $vat_procent/100;
    $total = $row['total'] + $vat;

    $other_total = $row['other_total'] + $vat2;

    $cost_center = $row['costcenter_tks_cost'];

    $get_id = mysqli_fetch_assoc($conn->query("SELECT id FROM app_opex_report WHERE years = '".$row['years']."' AND month_num = '".$row['month_num']."'"));
    $id = $get_id['id'];

    if(!empty($row['total'])){      
      $conn->query("INSERT INTO app_opex_invoice_cost_to_month (id,years, month, invoiceid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['invoiceid']."','".$row['service_key']."','$cost_center','$date')");

       $conn->query("INSERT INTO app_opex_invoice_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,sequence_no,update_date) VALUES ('$id', '".$row['service_name']."','".$row['service_key']."','$cost_center','".$row['total']."','$total','".$row['invoiceid']."','".$row['sequence_no']."','$date')");
    }else{
      $conn->query("INSERT INTO app_opex_invoice_other_cost_to_month (id,years, month, invoiceid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['invoiceid']."','".$row['service_key']."','$cost_center','$date')");

      $conn->query("INSERT INTO app_opex_invoice_other_details_temp (tempid,service_name,accountname,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,invoice_no,sequence_no,update_date) VALUES ('$id', '".$row['service_name']."','".$row['accountname']."','".$row['service_key']."','$cost_center','".$row['other_total']."','$other_total','".$row['invoiceid']."','".$row['invoice_no']."','".$row['sequence_no']."','$date')");
    }

    $check_dublicate = $conn->query("SELECT * FROM app_opex_invoice_cost_to_month WHERE invoiceid = '".$row['invoiceid']."' AND service_key = '".$row['service_key']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate);

    if($num_rows >= 2){

      $orders_array = array();

      foreach($check_dublicate AS $rez){
        $orders_array[] = $rez;
      }

      $first_num = $orders_array[0];
      $second_num = $orders_array[1];
     

      if(strtotime($first_num['update_date']) < strtotime($second_num['update_date'])){
        $delete_order_id = $first_num['id'];
        $delete_order_service_key = $first_num['service_key'];
        $delete_order_cost_center = $first_num['cost_center'];
        $delete_order_invoiceid = $first_num['invoiceid'];
      }else{
        $delete_order_id = $second_num['id'];
        $delete_order_service_key = $second_num['service_key'];
        $delete_order_cost_center = $second_num['cost_center'];
        $delete_order_invoiceid = $second_num['invoiceid'];
      }

      $conn->query("DELETE FROM app_opex_invoice_details WHERE did = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND invoiceid = $delete_order_invoiceid ");

      $conn->query("DELETE FROM app_opex_invoice_cost_to_month WHERE id = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND invoiceid = $delete_order_invoiceid");     
    }
    
    // Nepakliuve i sarasa kastu centrai

    $check_dublicate2 = $conn->query("SELECT * FROM app_opex_invoice_other_cost_to_month WHERE invoiceid = '".$row['invoiceid']."' AND service_key = '".$row['service_key']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate2);

    if($num_rows >= 2){

      $orders_array2 = array();

      foreach($check_dublicate2 AS $rez){
        $orders_array2[] = $rez;
      }

      $first_num2 = $orders_array2[0];
      $second_num2 = $orders_array2[1];
     

      if(strtotime($first_num2['update_date']) < strtotime($second_num2['update_date'])){
        $delete_order_id2 = $first_num2['id'];
        $delete_order_service_key2 = $first_num2['service_key'];
        $delete_order_cost_center2 = $first_num2['cost_center'];
        $delete_order_invoiceid2 = $first_num2['invoiceid'];
      }else{
        $delete_order_id2 = $second_num2['id'];
        $delete_order_service_key2 = $second_num2['service_key'];
        $delete_order_cost_center2 = $second_num2['cost_center'];
        $delete_order_invoiceid2 = $second_num2['invoiceid'];
      }

      $conn->query("DELETE FROM app_opex_invoice_other_details WHERE did = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND invoiceid = $delete_order_invoiceid2 ");

      $conn->query("DELETE FROM app_opex_invoice_other_cost_to_month WHERE id = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND invoiceid = $delete_order_invoiceid2");     
    }

     // Nepakliuve i sarasa kastu centrai pabaiga

  }

  $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, GROUP_CONCAT(DISTINCT invoiceid) AS invoiceid
                                  FROM app_opex_invoice_details_temp
                                  LEFT JOIN app_opex_report re on re.id=tempid                                       
                                  GROUP BY service_key,cost_center, month_num");


  foreach($grouped_stats as $result){   
      $check_cost = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_invoice_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' "));

      if(empty($check_cost['service_name'])){
          $conn->query("INSERT INTO app_opex_invoice_details (did,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['invoiceid']."','$date')");
      }else{
          $conn->query("UPDATE app_opex_invoice_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', invoiceid = '".$result['invoiceid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
      }    
  }

  // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,accountname,invoice_no,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, GROUP_CONCAT(DISTINCT invoiceid) AS invoiceid
                                    FROM app_opex_invoice_other_details_temp
                                    LEFT JOIN app_opex_report re on re.id=tempid                                       
                                    GROUP BY service_key,cost_center, month_num");


  foreach($grouped_stats2 as $result){   
    $check_cost2 = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_invoice_other_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' "));

    if(empty($check_cost2['service_name'])){
      $conn->query("INSERT INTO app_opex_invoice_other_details (did,service_name,accountname,invoice_no,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['accountname']."','".$result['invoice_no']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['invoiceid']."','$date')");
    }else{
      $conn->query("UPDATE app_opex_invoice_other_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', invoiceid = '".$result['invoiceid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
    }    
  }
 // Nepakliuve i sarasa kastu centrai pabaiga


header("location:/index.php?module=Opex&view=List&year=$year");