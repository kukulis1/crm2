<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $purchaseorderid = $_POST['purchaseorderid'];
  $service_key = $_POST['service_key'];
  $type = $_POST['type'];

// $purchaseid = 17705882;
// $service_key = '103973,114453';
// $type = 103919;

  $query = "SELECT SUM(i.listprice*i.quantity) AS listprice, ((SUM(i.listprice*quantity)) * IF(r.value IS NULL,21, r.value) /100)  + SUM(i.listprice*i.quantity) AS listprice_vat,
  i.cargo_wgt ,v.vendorname, p.purchaseorder_no,p.purchaseorderid,cf_1345 as invoicedate, i.sequence_no
                           FROM vtiger_inventoryproductrel i
                           LEFT JOIN vtiger_purchaseorder p ON p.purchaseorderid=i.id
                           LEFT JOIN vtiger_purchaseordercf pf ON pf.purchaseorderid=p.purchaseorderid
                           LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid
                           LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
                           LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
                           LEFT JOIN vtiger_taxregions r ON IF(i.tax2 > 0, r.regionid=i.tax2 ,r.regionid=p.region_id)                  
                           WHERE id IN ($purchaseorderid) AND e.deleted = 0 AND e.setype = 'PurchaseOrder' AND itemserviceid IN ($service_key) AND cargo_width = $type 
                           GROUP BY i.id,i.sequence_no";

  $get_list = $conn->query($query);
  $list = array();
  $sums = array();
  $list2 = array();

  foreach($get_list as $row){
    $list[$row['purchaseorder_no']] = array('vendorname' => $row['vendorname'], 
                                            'cargo_wgt' => $row['cargo_wgt'], 
                                            'purchaseorder_no' => $row['purchaseorder_no'],  
                                            'invoicedate' => $row['invoicedate'],
                                            'purchaseorderid' => $row['purchaseorderid']
                                          );

    $sums[$row['purchaseorder_no']][] = array('listprice' => $row['listprice'],
                                              'listprice_vat' => $row['listprice_vat'],                                             
                                              'sequence_no' => $row['sequence_no']
                                             );
  }

  $list2 = [];
  $i = 0;
  foreach($list as $li){    
    $list2[$i] = array('vendorname' => $li['vendorname'], 'cargo_wgt' =>  $li['cargo_wgt'],'purchaseorder_no' =>  $li['purchaseorder_no'],'purchaseorderid' =>  $li['purchaseorderid'],'invoicedate' =>  $li['invoicedate'], 'sequence_no' => implode(',',$li['sequence_no']));
   
    foreach($sums[$li['purchaseorder_no']] as $amount){
      $list2[$i]['listprice'][] = $amount['listprice'];
      $list2[$i]['listprice_vat'][] = $amount['listprice_vat'];
      $list2[$i]['sequence_no'][] = $amount['sequence_no'];
    } 
    $i++;
  }

  echo  json_encode($list2);

}else{
  http_response_code(404);
}