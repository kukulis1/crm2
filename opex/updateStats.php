<?php
require_once "../config.inc.php";

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
  error_reporting(0);

  $last_update_time = mysqli_fetch_assoc($conn->query("SELECT last_update FROM app_opex_report ORDER BY id DESC LIMIT 1"));
  $get_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_cost_list");
  $get_invoice_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_invoice_cost_list");

  $from = $last_update_time['last_update'];
  $to = date("Y-m-d H:i:s");
  $except_costs = array();

  $get_months_id = $conn->query('SELECT * FROM app_opex_report');
  $month_id = [];
  foreach ($get_months_id as $item) {
    $month_id[$item['years']][$item['month_num']] = $item['id'];
  }


  $stats_query ="SELECT (listprice*quantity) + ((listprice*quantity) * IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) AS other_total_vat, (listprice*quantity) AS other_total, ((listprice*quantity) * IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) AS vat, DATE_FORMAT(cf_1345, '%Y') as years ,DATE_FORMAT(cf_1345, '%m') as month_num, po.purchaseorderid, cargo_wgt as service, sequence_no, itemserviceid, costcenter_tks_cost,vtiger_taxregions.value as vatprocent, v.vendorname, po.purchaseorder_no, 
                          CASE";
          foreach($get_cost_centers_with_costs as $cost){
            $cost_center = $cost['cost_center'];
            $costs = explode(",", $cost['cost']);         
            if($cost['name'] == 'other_klaipeda')  $except_costs = $costs;
            for($i =0; $i < count($costs); $i++){
              if(!empty($cost_center) && !empty($costs[$i])){
               $stats_query .= "\n WHEN cargo_width = $cost_center AND itemserviceid = $costs[$i] THEN (listprice*quantity)";
              }
            }
          }
      $stats_query .=" END AS total,
      CASE ";
      foreach($get_cost_centers_with_costs as $cost){
        $cost_center = $cost['cost_center'];
        $costs = explode(",", $cost['cost']);         
        if($cost['name'] == 'other_klaipeda')  $except_costs = $costs;
        for($i =0; $i < count($costs); $i++){
          if(!empty($cost_center) && !empty($costs[$i])){
            $stats_query .= "\n WHEN cargo_width = $cost_center AND itemserviceid = $costs[$i] THEN (listprice*quantity) + ((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) ";
          }
        }
      }
    $stats_query .="  END AS total_vat
                        FROM vtiger_inventoryproductrel i
                        LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
                        LEFT JOIN vtiger_purchaseorder po on po.purchaseorderid=i.id
                        LEFT JOIN vtiger_purchaseordercf pocf on po.purchaseorderid=pocf.purchaseorderid
                        LEFT JOIN vtiger_vendor v ON v.vendorid=po.vendorid
                        LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
                        LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.cargo_width
                        LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=po.region_id)       
                        WHERE e.deleted = 0  AND e.setype = 'PurchaseOrder'  AND  e.modifiedtime BETWEEN '$from' AND '$to'                                                 
                        AND itemserviceid IS NOT NULL 
                        AND cargo_width IN (103914,103916,103917,103918,103919,103920,103921) AND cf_1926 != 1 AND cf_1343 != 'Išankstinė'
                        GROUP BY  id,sequence_no                                         
                        ORDER BY i.id DESC";
              


  $stats = $conn->query($stats_query);
  $year = date("Y");   
  $month = strftime('%B');
  $month_n = date("m");
  $date = date("Y-m-d H:i:s");


  $conn->query("TRUNCATE app_opex_details_temp");      
  $conn->query("TRUNCATE app_opex_other_details_temp");  

  $purchase_array = [];                           
  $purchase2_array = [];    

  foreach($stats as $row){ 
    $cost_center = $row['costcenter_tks_cost'];
    if( in_array($row['itemserviceid'], $except_costs)  AND $row['costcenter_tks_cost'] == 'KLAIPĖDOS SANDĖLIS') $cost_center = $row['costcenter_tks_cost']."2"; 

    $id = $month_id[$row['years']][$row['month_num']]; 

    if(!empty($row['total'])){     
      $purchase_array[] = ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'purchaseorderid' => $row['purchaseorderid'],'itemserviceid' => $row['itemserviceid'], 'cost_center' => $cost_center,'total' => $row['total'],'total_vat' => $row['total_vat'],'service' => $row['service'],'sequence_no' => $row['sequence_no']];  
    }else{
      $purchase2_array[] =  ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'purchaseorderid' => $row['purchaseorderid'],'itemserviceid' => $row['itemserviceid'], 'cost_center' => $cost_center,'total' => $row['other_total_vat'],'total_without_vat' => $row['other_total'],'service' => $row['service'],'sequence_no' => $row['sequence_no'],'vendorname' => $row['vendorname'], 'purchaseorder_no' => $row['purchaseorder_no']];
    } 
  }

  $purchase_array = array_chunk($purchase_array,100);
  $purchase2_array = array_chunk($purchase2_array,100);

  for($i=0;$i<count($purchase_array);$i++){
    $values = '';
    $values2 = '';
    foreach($purchase_array[$i] AS $item){   
      $values .= "('".$item['id']."','".$item['service']."','".$item['itemserviceid']."','".$item['cost_center']."','".$item['total']."','".$item['total_vat']."','".$item['purchaseorderid']."','".$item['sequence_no']."','$date'),";
    }
    $values = rtrim($values,',');

    $conn->query("INSERT INTO app_opex_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,sequence_no,update_date) VALUES $values");
  }

  for($i=0;$i<count($purchase2_array);$i++){
    $values = '';
    $values2 = '';
    foreach($purchase2_array[$i] AS $item){   
      $values .= "('".$item['id']."','".$item['service']."','".$item['vendorname']."','".$item['itemserviceid']."','".$item['cost_center']."','".$item['total_without_vat']."','".$item['total']."','".$item['purchaseorderid']."','".$item['purchaseorder_no']."','".$item['sequence_no']."','$date'),";
    }
    $values = rtrim($values,',');

    $conn->query("INSERT INTO app_opex_other_details_temp (tempid,service_name,vendorname,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,purchaseorder_no,sequence_no,update_date) VALUES $values");
  }

  $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, total_without_vat , total_with_vat, purchaseorderid,sequence_no
                                 FROM app_opex_details_temp
                                 LEFT JOIN app_opex_report re on re.id=tempid                                       
                                 GROUP BY month_num,purchaseorderid,sequence_no");

  // Pasiruosiam trinti atnaujintus irasus
  $updated_records_arr = [];
  foreach($grouped_stats as $result){  
    $updated_records_arr[] = ['purchaseorderid' => $result['purchaseorderid'],'sequence_no' => $result['sequence_no']];
  }

  $updated_records_arr = array_chunk($updated_records_arr,50);

  for($i=0;$i<count($updated_records_arr);$i++){
    $delete = '';   
    foreach($updated_records_arr[$i] AS $item){  
      $delete .= "('".$item['purchaseorderid']."','".$item['sequence_no']."'),";
    }
    $delete = rtrim($delete,',');    
    $conn->query("DELETE FROM app_opex_details WHERE (purchaseorderid,sequence_no) IN ($delete)");
  }


  foreach($grouped_stats as $result){
    $purchase_group_array[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'], 'service_name' => $result['service_name'],'service_key' => $result['service_key'],'cost_center' => $result['cost_center'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat'], 'purchaseorderid' => $result['purchaseorderid']];
  }

  $purchase_group_array = array_chunk($purchase_group_array,100);

  for($i=0;$i<count($purchase_group_array);$i++){
    $values = '';   
    foreach($purchase_group_array[$i] AS $item){
      $values .= "('".$item['tempid']."','".$item['sequence_no']."','".$item['service_name']."','".$item['service_key']."','".$item['cost_center']."','".$item['total_without_vat']."','".$item['total_with_vat']."','".$item['purchaseorderid']."','$date'),"; 
    }
    $values = rtrim($values,',');    
    $conn->query("INSERT INTO app_opex_details (did,sequence_no,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES $values");
  } 

   // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,vendorname,purchaseorder_no,service_key,cost_center, total_without_vat ,total_with_vat, purchaseorderid,sequence_no
   FROM app_opex_other_details_temp
   LEFT JOIN app_opex_report re on re.id=tempid                                       
   GROUP BY month_num,purchaseorderid,sequence_no");


  // Pasiruosiam trinti atnaujintus irasus
  $updated_records2_arr = [];
  foreach($grouped_stats2 as $result){  
    $updated_records2_arr[] = ['purchaseorderid' => $result['purchaseorderid'],'sequence_no' => $result['sequence_no']];
  }

  $updated_records2_arr = array_chunk($updated_records2_arr,50);

  for($i=0;$i<count($updated_records2_arr);$i++){
    $delete = '';   
    foreach($updated_records2_arr[$i] AS $item){  
      $delete .= "('".$item['purchaseorderid']."','".$item['sequence_no']."'),";
    }
    $delete = rtrim($delete,',');    
    $conn->query("DELETE FROM app_opex_other_details WHERE (purchaseorderid,sequence_no) IN ($delete)");
  }

  $purchase2_group_array = [];
  foreach($grouped_stats2 as $result){  
    $purchase2_group_array[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'], 'service_name' => $result['service_name'],'vendorname' => $result['vendorname'],'purchaseorder_no' => $result['purchaseorder_no'],'service_key' => $result['service_key'],'cost_center' => $result['cost_center'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat'], 'purchaseorderid' => $result['purchaseorderid']];
  }

  $purchase2_group_array = array_chunk($purchase2_group_array,100);

  for($i=0;$i<count($purchase2_group_array);$i++){
    $values = '';   
    foreach($purchase2_group_array[$i] AS $item){
      $values .= "('".$item['tempid']."','".$item['sequence_no']."','".$item['service_name']."','".$item['vendorname']."','".$item['purchaseorder_no']."','".$item['service_key']."','".$item['cost_center']."','".$item['total_without_vat']."','".$item['total_with_vat']."','".$item['purchaseorderid']."','$date'),";
    }
    $values = rtrim($values,',');
        
    $conn->query("INSERT INTO app_opex_other_details (did,sequence_no,service_name,vendorname,purchaseorder_no,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES $values");
  }   


  // Nepakliuve i sarasa kastu centrai pabaiga

    $first_day_of_month = date("Y-m-d", strtotime("first day of this month"));
    $today = date("Y-m-d");

    $transport_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(salesorderid) AS transport_orders
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));

    $transport_parcel_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(s.salesorderid) AS parcel_orders
                                        FROM vtiger_salesorder s
                                        JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE (DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today') AND setype = 'SalesOrder' AND cf_855 = 'Perkraustymo užsakymas' AND deleted = 0")); 

    $transport_orders_pll_places = mysqli_fetch_assoc($conn->query("SELECT DISTINCT SUM(i.pll) AS pll_places
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today'  AND i.pll != 0 "));

    $transport_total_weight = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(cargo_wgt),2) AS total_weight
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));

    $transport_cancel_orders = mysqli_fetch_assoc($conn->query("SELECT SUM(s.total) AS total
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND scf.cf_1614 = 1 AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$today' "));    



    $id = $month_id[$transport_orders['years']][$transport_orders['month_num']];

    $check_month = mysqli_fetch_assoc($conn->query("SELECT `tid` FROM app_opex_transport_orders WHERE `tid` = $id"));
    $check_month2 = mysqli_fetch_assoc($conn->query("SELECT `sid` FROM app_storage_independent_service WHERE `sid` = $id"));

    if(!$check_month){    
      $conn->query("INSERT INTO app_opex_transport_orders  (tid,
                                                            total_orders,     
                                                            total_parcel_orders,                                                      
                                                            total_cancel_orders,
                                                            total_orders_pll_places,                                                      
                                                            total_weight,                                                       
                                                            update_date) 
                                                            VALUES ('$id',
                                                                    '".$transport_orders['transport_orders']."',  
                                                                    '".$transport_parcel_orders['parcel_orders']."',  
                                                                    '".$transport_cancel_orders['total']."',
                                                                    '".$transport_orders_pll_places['pll_places']."',
                                                                    '".$transport_total_weight['total_weight']."',
                                                                    '$date')");
  }else{
    $conn->query("UPDATE app_opex_transport_orders SET total_orders =  '".$transport_orders['transport_orders']."', 
                                                       total_parcel_orders = '".$transport_parcel_orders['parcel_orders']."',  
                                                       total_cancel_orders =  '".$transport_cancel_orders['total']."',
                                                       total_orders_pll_places =  '".$transport_orders_pll_places['pll_places']."',
                                                       total_weight = '".$transport_total_weight['total_weight']."', 
                                                       update_date = '$date' 
                                                       WHERE tid = $id"); 
  }



  $stats_query2 = "SELECT listprice AS other_total, DATE_FORMAT(invoicedate, '%Y') as years ,DATE_FORMAT(invoicedate, '%m') as month_num,
  inv.invoiceid, service, sequence_no, costcenter_tks_cost, IF(vtiger_taxregions.value IS NULL, 21, vtiger_taxregions.value) as vatprocent,
  v.accountname, inv.invoice_no, IF(productid = 121391,itemservice_tks_name,se.name) as service_name, IF(productid = 121391,itemserviceid,i.service) AS service_key,
  CASE ";  
        foreach($get_invoice_cost_centers_with_costs as $cost){
          $cost_center = explode(",", $cost['cost_center']);
          $costs = explode(",", $cost['cost']);
          for($e=0; $e < count($cost_center); $e++){
            if(!empty($cost_center[$e])){
              for($i=0; $i < count($costs); $i++){     
                if(!empty($costs[$i])){       
                  $stats_query2 .= "\n WHEN costcenter = $cost_center[$e] AND ".(strlen($costs[$i]) <= 3 ? 'service': 'itemserviceid')." = $costs[$i] THEN IF(productid = 121391, listprice * quantity, listprice)";
                }
              }
            }
          }
        }
  $stats_query2 .= " END AS total
       FROM vtiger_inventoryproductrel i
       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
       LEFT JOIN vtiger_invoice inv on inv.invoiceid=i.id
       LEFT JOIN vtiger_invoicecf icf on inv.invoiceid=icf.invoiceid
       LEFT JOIN vtiger_account v ON v.accountid=inv.accountid
       LEFT JOIN app_services_type se ON se.id=i.service 
       LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.costcenter
       LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id    
       WHERE e.deleted = 0  AND e.setype = 'Invoice'  AND e.modifiedtime BETWEEN '$from' AND '$to'     
       AND costcenter IN (103914,103916,103917,103918,103919,103920,103921) AND DATE_FORMAT(invoicedate, '%m') != '' AND cf_1277 != 'Išankstinė'
       GROUP BY  i.id,sequence_no                                         
       ORDER BY i.id DESC";

 
  $stats2 = $conn->query($stats_query2);
  $check_last = mysqli_fetch_assoc($conn->query("SELECT * FROM app_opex_report ORDER BY id DESC LIMIT 1"));

  if(($check_last['years'] < $year ? true : $check_last['month_num'] < $month_n )){ 
    $conn->query("INSERT INTO app_opex_report (`years`,`months`,`month_num`,`last_update`) VALUES ('$year','$month','$month_n','$date')");
  }else{
    $conn->query("UPDATE app_opex_report SET `last_update` = '$date' WHERE years = '$year' AND month_num = '$month_n'");
  }

  $conn->query("TRUNCATE app_opex_invoice_details_temp");      
  $conn->query("TRUNCATE app_opex_invoice_other_details_temp");

  $rows_array = [];
  $rows2_array = [];
  foreach($stats2 as $row){ 
    $vat_procent = $row['vatprocent'];
    $vat = $row['total'] * $vat_procent/100;
    $vat2 = $row['other_total'] * $vat_procent/100;
    $total = $row['total'] + $vat;   
    $other_total = $row['other_total'] + $vat2;
    $cost_center = $row['costcenter_tks_cost'];
    $id = $month_id[$row['years']][$row['month_num']];
     
    if(empty($row['total']) AND $row['service_key'] != 1){     
      $rows2_array[] =  ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'invoiceid' => $row['invoiceid'],'service_key' => $row['service_key'], 'cost_center' => $cost_center,'total' => $other_total,'total_without_vat' => $row['other_total'],'service_name' => $row['service_name'],'sequence_no' => $row['sequence_no'],'accountname' => $row['accountname'],'invoice_no' => $row['invoice_no']];
    }else{
      $rows_array[] = ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'invoiceid' => $row['invoiceid'],'service_key' => $row['service_key'], 'cost_center' => $cost_center,'total' => $total,'total_without_vat' => $row['total'],'service_name' => $row['service_name'],'sequence_no' => $row['sequence_no']];     
    }
  }


    $array = array_chunk($rows_array,100);
    $array2 = array_chunk($rows2_array,100);


    for($i=0;$i<count($array);$i++){
      $values = '';
      foreach($array[$i] AS $row){   
        $values .= "('".$row['id']."','".$row['service_name']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total']."','".$row['invoiceid']."','".$row['sequence_no']."','$date'),";
      }

      $values = rtrim($values,',');
  
      $conn->query("INSERT INTO app_opex_invoice_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,sequence_no,update_date) VALUES $values");
    }

    for($i=0;$i<count($array2);$i++){
      $values = '';
      foreach($array2[$i] AS $row){  
        $values .= "('".$row['id']."','".$row['service_name']."','".$row['accountname']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total']."','".$row['invoiceid']."','".$row['invoice_no']."','".$row['sequence_no']."','$date'),";
      }
      $values = rtrim($values,',');
      
      $conn->query("INSERT INTO app_opex_invoice_other_details_temp (tempid,service_name,accountname,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,invoice_no,sequence_no,update_date) VALUES $values");
    } 


    $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, total_without_vat , total_with_vat, invoiceid,sequence_no
                                 FROM app_opex_invoice_details_temp
                                 LEFT JOIN app_opex_report re on re.id=tempid                                       
                                 GROUP BY month_num,invoiceid,sequence_no");


    $delete_arr = [];                        

    foreach($grouped_stats as $result){  
      $delete_arr[] = ['invoiceid' => $result['invoiceid'],'sequence_no' => $result['sequence_no']];
    }
  
    $delete_arr = array_chunk($delete_arr,50);

    for($i=0;$i<count($delete_arr);$i++){
      $delete = '';   
      foreach($delete_arr[$i] AS $item){  
        $delete .= "('".$item['invoiceid']."','".$item['sequence_no']."'),";
      }
      $delete = rtrim($delete,',');    
      $conn->query("DELETE FROM app_opex_invoice_details WHERE (invoiceid,sequence_no) IN ($delete)");
    } 

    $group_query = [];                        

    foreach($grouped_stats as $result){  
      $group_query[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'], 'service_name' => $result['service_name'], 'service_key' => $result['service_key'], 'cost_center' => $result['cost_center'],'invoiceid' => $result['invoiceid'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat']];
    }
  
    $grouped_array = array_chunk($group_query,100);
  
  
    for($i=0;$i<count($grouped_array);$i++){
      $values = '';
      foreach($grouped_array[$i] AS $row){ 
        $values .= "('".$row['tempid']."','".$row['sequence_no']."','".$row['service_name']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total_with_vat']."','".$row['invoiceid']."','$date'),";
      }
      $values = rtrim($values,',');
      
      $conn->query("INSERT INTO app_opex_invoice_details (did,sequence_no,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date)  VALUES $values");
    }


  // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,accountname,invoice_no,service_key,cost_center, total_without_vat , total_with_vat, invoiceid, sequence_no                     FROM app_opex_invoice_other_details_temp
                                  LEFT JOIN app_opex_report re on re.id=tempid                                       
                                  GROUP BY month_num,invoiceid,sequence_no");


  $delete2_arr = [];                        

  foreach($grouped_stats2 as $result){  
    $delete2_arr[] = ['invoiceid' => $result['invoiceid'],'sequence_no' => $result['sequence_no']];
  }

  $delete2_arr = array_chunk($delete2_arr,50);

  for($i=0;$i<count($delete2_arr);$i++){
    $delete = '';   
    foreach($delete2_arr[$i] AS $item){  
      $delete .= "('".$item['invoiceid']."','".$item['sequence_no']."'),";
    }
    $delete = rtrim($delete,',');    
    $conn->query("DELETE FROM app_opex_invoice_other_details WHERE (invoiceid,sequence_no) IN ($delete)");
  } 
 
  $group2_query = [];

  foreach($grouped_stats2 as $result){  
    $group2_query[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'],'invoiceid' => $result['invoiceid'], 'service_name' => $result['service_name'],'accountname' => $result['accountname'],'invoice_no' => $result['invoice_no'], 'service_key' => $result['service_key'], 'cost_center' => $result['cost_center'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat']];
  }

  $grouped2_array = array_chunk($group2_query,100);


  for($i=0;$i<count($grouped2_array);$i++){
    $values = '';
    foreach($grouped2_array[$i] AS $row){ 
      $values .= "('".$row['tempid']."','".$row['sequence_no']."','".$row['service_name']."','".$row['accountname']."','".$row['invoice_no']."','".$row['service_key']."','".$row['cost_center']."','".$row['total_without_vat']."','".$row['total_with_vat']."','".$row['invoiceid']."','$date'),";
    }
    $values = rtrim($values,',');  
    $conn->query("INSERT INTO app_opex_invoice_other_details (did,sequence_no,service_name,accountname,invoice_no,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date) VALUES $values");
  }       

 // Nepakliuve i sarasa kastu centrai pabaiga
 

 if(!$_GET['cron']){
  $year = $_GET['year'];
  $vat = $_GET['vat'];
  header("location:/index.php?module=Opex&view=List&year=$year&vat=$vat");
 }