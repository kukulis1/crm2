<?php
require_once "../config.inc.php";

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $last_update_time = mysqli_fetch_assoc($conn->query("SELECT last_update FROM app_opex_report ORDER BY id DESC LIMIT 1"));
  $get_invoice_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_invoice_cost_list");

  $from = $last_update_time['last_update'];
  $to = date("Y-m-d H:i:s");
  $except_costs = array();

  

  $year = date("Y");   
  $month = strftime('%B');
  $month_n = date("m");
  $date = date("Y-m-d H:i:s");


  $check_last = mysqli_fetch_assoc($conn->query("SELECT * FROM app_opex_report ORDER BY id DESC LIMIT 1"));

  if(($check_last['years'] < $year ? true : $check_last['month_num'] < $month_n )){ 
    $conn->query("INSERT INTO app_opex_report (`years`,`months`,`month_num`,`last_update`) VALUES ('$year','$month','$month_n','$date')");
  }else{
    $conn->query("UPDATE app_opex_report SET `last_update` = '$date' WHERE month_num = $month_n");
  }



  $stats_query2 = "SELECT listprice AS other_total, DATE_FORMAT(invoicedate, '%Y') as years ,DATE_FORMAT(invoicedate, '%m') as month_num,
  inv.invoiceid, service, sequence_no, costcenter_tks_cost, IF(vtiger_taxregions.value IS NULL, 21, vtiger_taxregions.value) as vatprocent,
  v.accountname, inv.invoice_no, se.name as service_name, i.service AS service_key,
  CASE ";  
        foreach($get_invoice_cost_centers_with_costs as $cost){
          $cost_center = explode(",", $cost['cost_center']);
          $costs = explode(",", $cost['cost']);
          for($e=0; $e < count($cost_center); $e++){
            for($i=0; $i < count($costs); $i++){            
              $stats_query2 .= "\n WHEN costcenter = $cost_center[$e] AND service = $costs[$i] THEN listprice";
            }
          }
        }
  $stats_query2 .= " END AS total
       FROM vtiger_inventoryproductrel i
       LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
       LEFT JOIN vtiger_invoice inv on inv.invoiceid=i.id
       LEFT JOIN vtiger_invoicecf icf on inv.invoiceid=icf.invoiceid
       LEFT JOIN vtiger_account v ON v.accountid=inv.accountid
       LEFT JOIN app_services_type se ON se.id=i.service 
       LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.costcenter
       LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id    
       WHERE e.deleted = 0  AND e.setype = 'Invoice'  AND (e.createdtime BETWEEN '$from' AND '$to' OR e.modifiedtime BETWEEN '$from' AND '$to')  
       AND costcenter IN (103914,103916,103917,103918,103919,103920,103921) AND DATE_FORMAT(invoicedate, '%m') != ''
       GROUP BY  i.id,sequence_no                                         
       ORDER BY i.id DESC";


  $stats2 = $conn->query($stats_query2);

  foreach($stats2 as $row){ 
    if(!empty($row['total'])){  
       $conn->query("DELETE FROM app_opex_invoice_details_temp WHERE invoiceid = '".$row['invoiceid']."'");      
    }else{
       $conn->query("DELETE FROM app_opex_invoice_other_details_temp WHERE invoiceid = '".$row['invoiceid']."'");      
    }
  }


  foreach($stats2 as $row){ 

    $vat_procent = $row['vatprocent'];
    $vat = $row['total'] * $vat_procent/100;
    $vat2 = $row['other_total'] * $vat_procent/100;
    $total = $row['total'] + $vat;

    $other_total = $row['other_total'] + $vat2;

    $cost_center = $row['costcenter_tks_cost'];

    $get_id = mysqli_fetch_assoc($conn->query("SELECT id FROM app_opex_report WHERE years = '".$row['years']."' AND month_num = '".$row['month_num']."'"));
    $id = $get_id['id'];

    if(!empty($row['total'])){      
      $conn->query("INSERT INTO app_opex_invoice_cost_to_month (id,years, month, invoiceid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['invoiceid']."','".$row['service_key']."','$cost_center','$date')");

       $conn->query("INSERT INTO app_opex_invoice_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,sequence_no,update_date) VALUES ('$id', '".$row['service_name']."','".$row['service_key']."','$cost_center','".$row['total']."','$total','".$row['invoiceid']."','".$row['sequence_no']."','$date')");
    }else{
      $conn->query("INSERT INTO app_opex_invoice_other_cost_to_month (id,years, month, invoiceid, service_key, cost_center, update_date) VALUES ('$id', '".$row['years']."','".$row['month_num']."','".$row['invoiceid']."','".$row['service_key']."','$cost_center','$date')");

      $conn->query("INSERT INTO app_opex_invoice_other_details_temp (tempid,service_name,accountname,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,invoice_no,sequence_no,update_date) VALUES ('$id', '".$row['service_name']."','".$row['accountname']."','".$row['service_key']."','$cost_center','".$row['other_total']."','$other_total','".$row['invoiceid']."','".$row['invoice_no']."','".$row['sequence_no']."','$date')");
    }

    $check_dublicate = $conn->query("SELECT * FROM app_opex_invoice_cost_to_month WHERE invoiceid = '".$row['invoiceid']."' AND service_key = '".$row['service_key']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate);

    if($num_rows >= 2){

      $orders_array = array();

      foreach($check_dublicate AS $rez){
        $orders_array[] = $rez;
      }

      $first_num = $orders_array[0];
      $second_num = $orders_array[1];
     

      if(strtotime($first_num['update_date']) < strtotime($second_num['update_date'])){
        $delete_order_id = $first_num['id'];
        $delete_order_service_key = $first_num['service_key'];
        $delete_order_cost_center = $first_num['cost_center'];
        $delete_order_invoiceid = $first_num['invoiceid'];
      }else{
        $delete_order_id = $second_num['id'];
        $delete_order_service_key = $second_num['service_key'];
        $delete_order_cost_center = $second_num['cost_center'];
        $delete_order_invoiceid = $second_num['invoiceid'];
      }

      $conn->query("DELETE FROM app_opex_invoice_details WHERE did = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND invoiceid = $delete_order_invoiceid ");

      $conn->query("DELETE FROM app_opex_invoice_cost_to_month WHERE id = $delete_order_id AND service_key =  $delete_order_service_key AND cost_center = '$delete_order_cost_center' AND invoiceid = $delete_order_invoiceid");     
    }
    
    // Nepakliuve i sarasa kastu centrai

    $check_dublicate2 = $conn->query("SELECT * FROM app_opex_invoice_other_cost_to_month WHERE invoiceid = '".$row['invoiceid']."' AND service_key = '".$row['service_key']."' AND cost_center = '$cost_center' ");

    $num_rows = mysqli_num_rows($check_dublicate2);

    if($num_rows >= 2){

      $orders_array2 = array();

      foreach($check_dublicate2 AS $rez){
        $orders_array2[] = $rez;
      }

      $first_num2 = $orders_array2[0];
      $second_num2 = $orders_array2[1];
     

      if(strtotime($first_num2['update_date']) < strtotime($second_num2['update_date'])){
        $delete_order_id2 = $first_num2['id'];
        $delete_order_service_key2 = $first_num2['service_key'];
        $delete_order_cost_center2 = $first_num2['cost_center'];
        $delete_order_invoiceid2 = $first_num2['invoiceid'];
      }else{
        $delete_order_id2 = $second_num2['id'];
        $delete_order_service_key2 = $second_num2['service_key'];
        $delete_order_cost_center2 = $second_num2['cost_center'];
        $delete_order_invoiceid2 = $second_num2['invoiceid'];
      }

      $conn->query("DELETE FROM app_opex_invoice_other_details WHERE did = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND invoiceid = $delete_order_invoiceid2 ");

      $conn->query("DELETE FROM app_opex_invoice_other_cost_to_month WHERE id = $delete_order_id2 AND service_key =  $delete_order_service_key2 AND cost_center = '$delete_order_cost_center2' AND invoiceid = $delete_order_invoiceid2");     
    }

     // Nepakliuve i sarasa kastu centrai pabaiga

  }

  $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, GROUP_CONCAT(DISTINCT invoiceid) AS invoiceid
                                  FROM app_opex_invoice_details_temp
                                  LEFT JOIN app_opex_report re on re.id=tempid                                       
                                  GROUP BY service_key,cost_center, month_num");


  foreach($grouped_stats as $result){   
      $check_cost = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_invoice_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' "));

      if(empty($check_cost['service_name'])){
          $conn->query("INSERT INTO app_opex_invoice_details (did,service_name,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['invoiceid']."','$date')");
      }else{
          $conn->query("UPDATE app_opex_invoice_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', invoiceid = '".$result['invoiceid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
      }    
  }

  // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,accountname,invoice_no,service_key,cost_center, SUM(total_without_vat) AS total_without_vat ,SUM(total_with_vat) AS total_with_vat, GROUP_CONCAT(DISTINCT invoiceid) AS invoiceid
                                    FROM app_opex_invoice_other_details_temp
                                    LEFT JOIN app_opex_report re on re.id=tempid                                       
                                    GROUP BY service_key,cost_center, month_num");


  foreach($grouped_stats2 as $result){   
    $check_cost2 = mysqli_fetch_assoc($conn->query("SELECT service_name FROM app_opex_invoice_other_details WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."' "));

    if(empty($check_cost2['service_name'])){
      $conn->query("INSERT INTO app_opex_invoice_other_details (did,service_name,accountname,invoice_no,service_key,cost_center,total_without_vat,total_with_vat,invoiceid,update_date) VALUES ('".$result['tempid']."','".$result['service_name']."','".$result['accountname']."','".$result['invoice_no']."','".$result['service_key']."','".$result['cost_center']."','".$result['total_without_vat']."','".$result['total_with_vat']."','".$result['invoiceid']."','$date')");
    }else{
      $conn->query("UPDATE app_opex_invoice_other_details SET total_without_vat = '".$result['total_without_vat']."', total_with_vat = '".$result['total_with_vat']."', invoiceid = '".$result['invoiceid']."', update_date = '$date' WHERE did = '".$result['tempid']."' AND service_key = '".$result['service_key']."' AND cost_center = '".$result['cost_center']."'");
    }    
  }
  // Nepakliuve i sarasa kastu centrai pabaiga



header("location:/index.php?module=Opex&view=List&year=$year");