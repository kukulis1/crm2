<?php


function getGroupServices($conn,$storage_ids, $type)
{


  $storage_arr = array();
  foreach($storage_ids AS $row){
    $inv_id = $row['id'];
    $storage_info = $conn->query("SELECT id, i.productid, i.listprice, i.costcenter, i.service, co.costcenter_tks_cost, co.costcenterid, DATE_FORMAT(invoicedate, '%m') as month,vtiger_taxregions.value as vatprocent, sequence_no,productid
                                      FROM vtiger_inventoryproductrel i
                                      LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
                                      LEFT JOIN vtiger_costcenter co ON co.costcenterid=i.costcenter
                                      LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
                                      LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.invoiceid     
                                      WHERE i.id = $inv_id ");
                                      
    foreach($storage_info AS $row2){
        $storage_arr[$inv_id][] =  array('id' => $row2['id'],
                                          'month' => $row2['month'],
                                          'service' => $row2['service'],
                                          'productid' => $row2['productid'],
                                          'cost' => $row2['costcenter_tks_cost'],
                                          'cost_id' => $row2['costcenterid'],                                        
                                          'price' => $row2['listprice'],
                                          'sequence_no' => $row2['sequence_no'],
                                          'vatprocent' => $row2['vatprocent']
                                        );
    }    
  }



  $storage_price_arr = array();


  foreach($storage_arr AS $row){ 
    $arr[] = $row;
  }

  for($k= 0; $k < count($arr); $k++){


    for($i= 0; $i < count($arr[$k]); $i++){
      if(empty($arr[$k][$i]['cost'])){
        $e = $i -1;
        $cost = $arr[$k][$e]['cost'];
        $cost_id = $arr[$k][$e]['cost_id'];
        $arr[$k][$i]['cost'] = $cost;
        $arr[$k][$i]['cost_id'] = $cost_id;
      }
    }
    for($i= 0; $i < count($arr[$k]); $i++){
      if($arr[$k][$i]['productid'] == 36641){
        array_push($storage_price_arr,$arr[$k][$i]);
      }
    }
  }
  

  return $storage_price_arr;  
}