<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $costName = $_POST['costName'];
  $numbersArr = $_POST['numbersArr'];
  $costs = implode(",", $numbersArr);
  $date = date("Y-m-d H:i:s");

  $update = $conn->query("UPDATE app_opex_invoice_cost_list SET cost = '$costs', last_update = '$date' WHERE name = '$costName'");

  if($update){
    echo json_encode('success');
  }else{
    echo json_encode('Fail');
  }

}else{
  http_response_code(404);
}