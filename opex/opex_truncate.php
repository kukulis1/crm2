<?php
require_once "../config.inc.php";

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $first = $conn->query("TRUNCATE app_opex_report");
  if($first) echo "1. app_opex_report complete<br>";

  $second = $conn->query("TRUNCATE app_opex_details");
  if($second) echo "2. app_opex_details complete<br>";

  $third = $conn->query("TRUNCATE app_opex_transport_orders");
  if($third) echo "3. app_opex_transport_orders complete<br>";

  $fourt = $conn->query("TRUNCATE app_storage_service");
  if($fourt) echo "4. app_storage_service complete<br>";

  $fifth = $conn->query("TRUNCATE app_stevedoring_service");
  if($fifth) echo "5. app_stevedoring_service complete<br>";

  $six = $conn->query("TRUNCATE app_storage_independent_service");
  if($six) echo "6. app_storage_independent_service complete<br>";

  $seven = $conn->query("TRUNCATE app_opex_details_temp");
  if($seven) echo "7. app_opex_details_temp complete<br>";

  $eight = $conn->query("TRUNCATE app_opex_other_details");
  if($eight) echo "8. app_opex_other_details complete<br>";

  $nine = $conn->query("TRUNCATE app_opex_cost_to_month");
  if($nine) echo "9. app_opex_cost_to_month complete<br>";

  $ten = $conn->query("TRUNCATE app_opex_other_cost_to_month");
  if($ten) echo "10. app_opex_other_cost_to_month complete<br>";

  $eleven = $conn->query("TRUNCATE app_opex_other_details_temp");
  if($eleven) echo "11. app_opex_other_details_temp complete<br>";

  