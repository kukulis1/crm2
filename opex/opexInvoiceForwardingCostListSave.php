<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $costName = $_POST['costName'];
  $title = $_POST['title'];
  $numbersArr = $_POST['numbersArr'];
  $costCenter = implode(",", $numbersArr);  
  $date = date("Y-m-d H:i:s");

  $numbersArr2 = $_POST['numbersArr2'];
  $costs = implode(",", $numbersArr2);

  $check_exist = $conn->query("SELECT name FROM app_opex_invoice_cost_list WHERE name = '$costName'");
  $check_exist = mysqli_num_rows($check_exist);


  for($i =0; $i < COUNT($numbersArr2); $i++){
    $check_dublicates = $conn->query("SELECT * FROM app_opex_invoice_cost_list WHERE cost_center =  '$costCenter' AND name != '$costName' AND cost LIKE '$numbersArr2[$i]%'");
    $dublicates = mysqli_num_rows($check_dublicates);

    if($dublicates){    
      $check_exist2 = mysqli_fetch_assoc($conn->query("SELECT title FROM app_opex_invoice_cost_list WHERE cost LIKE '%$numbersArr2[$i]%'"));
      echo json_encode(array('line' => $i, 'exist' => $check_exist2['title']));     
      die();
    }
  }


  if(!$check_exist){
    $update = $conn->query("INSERT INTO app_opex_invoice_cost_list (name, title, cost_center, cost) VALUES ('$costName','$title','$costCenter','$costs')");
  }else{
    $update = $conn->query("UPDATE app_opex_invoice_cost_list SET cost_center = '$costCenter', cost = '$costs', last_update = '$date' WHERE name = '$costName'"); 
  }

  if($update){
    echo json_encode('success');
  }else{
    echo json_encode('Fail');
  }

}else{
  http_response_code(404);
}