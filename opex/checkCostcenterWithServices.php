<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $name = trim($_POST['name']);
  $type = $_POST['type'];

  if($type == 'All'){
    $query = $conn->query("SELECT `cost`,`cost_center` FROM app_opex_invoice_cost_list WHERE `name` = '$name'");
    $check_exist = mysqli_num_rows($query);
    if($check_exist){
      $get_list = mysqli_fetch_assoc($query);
    }else{
      $get_list[] = null;
    }
  }

  $costs_list = array();
  $result = array();


  $costs = $conn->query("SELECT id, name FROM app_services_type"); 
  
  $costs2 = $conn->query("SELECT `itemserviceid` AS id ,`itemservice_tks_name` AS name 
                            FROM vtiger_itemservice i 
                            LEFT JOIN vtiger_crmentity e ON e.crmid=i.itemserviceid
                            WHERE e.deleted = 0 ");
  
    foreach($costs as $row){
      $costs_list[] = $row; 
    } 

    foreach($costs2 as $row){
      $costs_list[] = $row; 
    } 

    if($type == 'All'){
      $result = array(
        'list' => $get_list,
        'costs' => $costs_list,       
      );
    }
    
  if($type == 'All'){
   echo json_encode($result);
  }else{
    echo json_encode($costs_list);
  }

}else{
  http_response_code(404);
}