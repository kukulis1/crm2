<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $name = trim($_POST['name']);
  $type = $_POST['type'];

  if($type == 'All'){
    $query = "SELECT `cost` FROM app_opex_invoice_free_cost_list WHERE `name` = '$name'";
    $get_list = mysqli_fetch_assoc($conn->query($query));

    $query2 = "SELECT `cost` FROM app_opex_invoice_cost_list WHERE `name` = '$name'";
    $get_list2 = mysqli_fetch_assoc($conn->query($query2));
  }

  $costs_list = array();
  $costs_list2 = array();
  $result = array();

  $costs = $conn->query("SELECT `itemserviceid`,`itemservice_tks_name` AS itemservice_name 
                          FROM vtiger_itemservice i 
                          LEFT JOIN vtiger_crmentity e ON e.crmid=i.itemserviceid
                          WHERE e.deleted = 0 ");

  $costs2 = $conn->query("SELECT id, name FROM app_services_type");                        
  
    foreach($costs as $row){
      $costs_list[] = $row; 
    }

    foreach($costs2 as $row){
      $costs_list2[] = $row; 
    }

    if($type == 'All'){
      $result = array(
        'list' => $get_list,
        'costs' => $costs_list,
        'list2' => $get_list2,
        'costs2' => $costs_list2
      );
    }
    
  if($type == 'All'){
   echo json_encode($result);
  }else{
    echo json_encode($costs_list);
  }

}else{
  http_response_code(404);
}