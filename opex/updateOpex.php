<?php
require_once "../config.inc.php";


ini_set('display_errors', 0);


$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");
$year = ($_GET['year'] ?: date('Y'));
$n = ($_GET['month'] ?: date('m'));
if(!empty($_REQUEST['cron'])){
    $year = date('Y', strtotime($_REQUEST['cron']));
    $n = date('m', strtotime($_REQUEST['cron']));
}


 
  $get_months_id = $conn->query("SELECT * FROM app_opex_report");
  $month_id = [];
  $month_id2 = [];
  foreach ($get_months_id as $item) {
    $month_id2[$item['years']][abs($item['month_num'])] = $item['id'];
    $month_id[$item['years']][$item['month_num']] = $item['id'];
  }

  $id = $month_id[$year][$n];

  $conn->query("DELETE FROM app_opex_details WHERE did = $id");

  $conn->query("DELETE FROM app_opex_transport_orders WHERE tid = $id");

  $conn->query("TRUNCATE app_opex_details_temp");

  $conn->query("DELETE FROM app_opex_other_details WHERE did = $id");

  $conn->query("DELETE FROM app_opex_cost_to_month WHERE id = $id");

  $conn->query("DELETE FROM app_opex_other_cost_to_month WHERE id = $id");

  $conn->query("TRUNCATE app_opex_other_details_temp");


  $get_cost_centers_with_costs = $conn->query("SELECT cost_center,cost,name FROM app_opex_cost_list");

  $mon = $n;

  // if($n != 10 AND $n != 11 AND $n != 12){
  //   $mon = "0$n";
  // }  

  $from = $year."-$mon-01";
  $to = $year."-$mon-".date('t',strtotime($year.'-'.$mon));
  $date = $year."-$mon-01 23:59:59";
  $except_costs = '';

  $stats_query = "SELECT (listprice*quantity) + ((listprice*quantity) * IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) AS other_total_vat, (listprice*quantity) AS other_total, ((listprice*quantity) * IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) AS vat, DATE_FORMAT(cf_1345, '%Y') as years ,DATE_FORMAT(cf_1345, '%m') as month_num, po.purchaseorderid, cargo_wgt as service, sequence_no, itemserviceid, costcenter_tks_cost,vtiger_taxregions.value as vatprocent, v.vendorname, po.purchaseorder_no, 
  CASE ";
        foreach($get_cost_centers_with_costs as $cost){
          $cost_center = $cost['cost_center'];
          $costs = explode(",", $cost['cost']);         
          if($cost['name'] == 'other_klaipeda')  $except_costs = $costs;
          for($i =0; $i < count($costs); $i++){
            if(!empty($cost_center) && !empty($costs[$i])){
              $stats_query .= "\n WHEN cargo_width = $cost_center AND itemserviceid = $costs[$i] THEN (listprice*quantity)";
            }
          }
        }
      $stats_query .="  END AS total,
      CASE ";
      foreach($get_cost_centers_with_costs as $cost){
        $cost_center = $cost['cost_center'];
        $costs = explode(",", $cost['cost']);         
        if($cost['name'] == 'other_klaipeda')  $except_costs = $costs;
        for($i =0; $i < count($costs); $i++){
          if(!empty($cost_center) && !empty($costs[$i])){
            $stats_query .= "\n WHEN cargo_width = $cost_center AND itemserviceid = $costs[$i] THEN (listprice*quantity) + ((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100) ";
          }
        }
      }
    $stats_query .="  END AS total_vat
      FROM vtiger_inventoryproductrel i
      LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
      LEFT JOIN vtiger_purchaseorder po on po.purchaseorderid=i.id
      LEFT JOIN vtiger_purchaseordercf pocf on po.purchaseorderid=pocf.purchaseorderid
      LEFT JOIN vtiger_vendor v ON v.vendorid=po.vendorid
      LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
      LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=i.cargo_width
      LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=po.region_id)       
      WHERE e.deleted = 0  AND e.setype = 'PurchaseOrder' AND cf_1345 BETWEEN '$from' AND '$to' AND itemserviceid IS NOT NULL 
      AND cargo_width IN (103914,103916,103917,103918,103919,103920,103921) AND cf_1926 != 1 AND cf_1343 != 'Išankstinė'
      GROUP BY  id,sequence_no                                         
      ORDER BY i.id DESC";

  $stats = $conn->query($stats_query);
  $purchase_array = [];                           
  $purchase2_array = [];    

  foreach($stats as $row){ 
    $cost_center = $row['costcenter_tks_cost'];
    if( in_array($row['itemserviceid'], $except_costs)  AND $row['costcenter_tks_cost'] == 'KLAIPĖDOS SANDĖLIS') $cost_center = $row['costcenter_tks_cost']."2"; 

    $id = $month_id[$row['years']][$row['month_num']]; 

    if(!empty($row['total'])){     
      $purchase_array[] = ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'purchaseorderid' => $row['purchaseorderid'],'itemserviceid' => $row['itemserviceid'], 'cost_center' => $cost_center,'total' => $row['total'],'total_vat' => $row['total_vat'],'service' => $row['service'],'sequence_no' => $row['sequence_no']];  
    }else{
      $purchase2_array[] =  ['id' => $id,'years' => $row['years'],'month_num' => $row['month_num'],'purchaseorderid' => $row['purchaseorderid'],'itemserviceid' => $row['itemserviceid'], 'cost_center' => $cost_center,'total' => $row['other_total_vat'],'total_without_vat' => $row['other_total'],'service' => $row['service'],'sequence_no' => $row['sequence_no'],'vendorname' => $row['vendorname'], 'purchaseorder_no' => $row['purchaseorder_no']];
    } 
  }

  $purchase_array = array_chunk($purchase_array,100);
  $purchase2_array = array_chunk($purchase2_array,100);


  for($i=0;$i<count($purchase_array);$i++){
    $values = '';
    $values2 = '';
    foreach($purchase_array[$i] AS $item){  
      $values .= "('".$item['id']."','".$item['service']."','".$item['itemserviceid']."','".$item['cost_center']."','".$item['total']."','".$item['total_vat']."','".$item['purchaseorderid']."','".$item['sequence_no']."','$date'),";
    }
    $values = rtrim($values,',');

    $conn->query("INSERT INTO app_opex_details_temp (tempid,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,sequence_no,update_date) VALUES $values");
  }


  for($i=0;$i<count($purchase2_array);$i++){
    $values = '';
    $values2 = '';
    foreach($purchase2_array[$i] AS $item){   
      $values .= "('".$item['id']."','".$item['service']."','".$item['vendorname']."','".$item['itemserviceid']."','".$item['cost_center']."','".$item['total_without_vat']."','".$item['total']."','".$item['purchaseorderid']."','".$item['purchaseorder_no']."','".$item['sequence_no']."','$date'),";
    }
    $values = rtrim($values,',');
    $conn->query("INSERT INTO app_opex_other_details_temp (tempid,service_name,vendorname,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,purchaseorder_no,sequence_no,update_date) VALUES $values");
  } 


  $grouped_stats = $conn->query("SELECT tempid, service_name,service_key,cost_center, total_without_vat, total_with_vat, purchaseorderid,sequence_no
                                 FROM app_opex_details_temp
                                 LEFT JOIN app_opex_report re on re.id=tempid                                       
                                 GROUP BY month_num,purchaseorderid,sequence_no");

  $purchase_group_array = [];

  foreach($grouped_stats as $result){
    $purchase_group_array[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'], 'service_name' => $result['service_name'],'service_key' => $result['service_key'],'cost_center' => $result['cost_center'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat'], 'purchaseorderid' => $result['purchaseorderid']];
  }

  $purchase_group_array = array_chunk($purchase_group_array,100);


  for($i=0;$i<count($purchase_group_array);$i++){
    $values = '';   
    foreach($purchase_group_array[$i] AS $item){
      $values .= "('".$item['tempid']."','".$item['sequence_no']."','".$item['service_name']."','".$item['service_key']."','".$item['cost_center']."','".$item['total_without_vat']."','".$item['total_with_vat']."','".$item['purchaseorderid']."','$date'),"; 
    }
    $values = rtrim($values,',');    
    $conn->query("INSERT INTO app_opex_details (did,sequence_no,service_name,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES $values");
  } 




  //  // Nepakliuve i sarasa kastu centrai
  $grouped_stats2 = $conn->query("SELECT tempid, service_name,vendorname,purchaseorder_no,service_key,cost_center, total_without_vat ,total_with_vat, purchaseorderid,sequence_no
                                  FROM app_opex_other_details_temp
                                  LEFT JOIN app_opex_report re on re.id=tempid                                       
                                  GROUP BY month_num,purchaseorderid,sequence_no");

  
  $purchase2_group_array = [];
  foreach($grouped_stats2 as $result){  
    $purchase2_group_array[] = ['tempid' => $result['tempid'],'sequence_no' => $result['sequence_no'], 'service_name' => $result['service_name'],'vendorname' => $result['vendorname'],'purchaseorder_no' => $result['purchaseorder_no'],'service_key' => $result['service_key'],'cost_center' => $result['cost_center'], 'total_without_vat' => $result['total_without_vat'], 'total_with_vat' => $result['total_with_vat'], 'purchaseorderid' => $result['purchaseorderid']];
  }



  $purchase2_group_array = array_chunk($purchase2_group_array,100);

  for($i=0;$i<count($purchase2_group_array);$i++){
    $values = '';   
    foreach($purchase2_group_array[$i] AS $item){
      $values .= "('".$item['tempid']."','".$item['sequence_no']."','".$item['service_name']."','".$item['vendorname']."','".$item['purchaseorder_no']."','".$item['service_key']."','".$item['cost_center']."','".$item['total_without_vat']."','".$item['total_with_vat']."','".$item['purchaseorderid']."','$date'),";
    }
    $values = rtrim($values,',');
    
    $conn->query("INSERT INTO app_opex_other_details (did,sequence_no,service_name,vendorname,purchaseorder_no,service_key,cost_center,total_without_vat,total_with_vat,purchaseorderid,update_date) VALUES $values");
  }      
  
  //  Nepakliuve i sarasa kastu centrai pabaiga


    $first_day_of_month = $year."-$mon-01";

    $transport_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(salesorderid) AS transport_orders
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE (DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to') AND e.deleted = 0 AND setype = 'SalesOrder' "));
    $transport_parcel_orders = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(e.createdtime, '%Y') AS years, DATE_FORMAT(e.createdtime, '%m') AS month_num,  COUNT(s.salesorderid) AS parcel_orders
                                        FROM vtiger_salesorder s
                                        JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE (DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to') AND setype = 'SalesOrder' AND cf_855 = 'Perkraustymo užsakymas' AND deleted = 0"));                                    

    $transport_orders_pll_places = mysqli_fetch_assoc($conn->query("SELECT DISTINCT SUM(i.pll) AS pll_places
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND  DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month'  AND '$to' AND i.pll != 0 "));

    $transport_total_weight = mysqli_fetch_assoc($conn->query("SELECT ROUND(SUM(cargo_wgt),2) AS total_weight
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND  DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to' "));

    $transport_cancel_orders = mysqli_fetch_assoc($conn->query("SELECT SUM(s.total) AS total
                                        FROM vtiger_salesorder s
                                        LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                        LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
                                        WHERE e.deleted =0 AND setype = 'SalesOrder' AND scf.cf_1614 = 1 AND  DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN '$first_day_of_month' AND '$to' "));                                    

    $check_month = mysqli_fetch_assoc($conn->query("SELECT `tid` FROM app_opex_transport_orders WHERE `tid` = $id"));


  if(!$check_month){    
      $conn->query("INSERT INTO app_opex_transport_orders  (tid,
                                                            total_orders,    
                                                            total_parcel_orders,                                                    
                                                            total_cancel_orders,
                                                            total_orders_pll_places,                                                      
                                                            total_weight,                                                       
                                                            update_date) 
                                                            VALUES ('$id',
                                                                    '".$transport_orders['transport_orders']."',   
                                                                    '".$transport_parcel_orders['parcel_orders']."',           
                                                                     '".$transport_cancel_orders['total']."',
                                                                    '".$transport_orders_pll_places['pll_places']."',
                                                                    '".$transport_total_weight['total_weight']."',  
                                                                    '$date')");
  }else{
    $conn->query("UPDATE app_opex_transport_orders SET total_orders =  '".$transport_orders['transport_orders']."',  
                                                      total_parcel_orders = '".$transport_parcel_orders['parcel_orders']."',         
                                                      total_cancel_orders =  '".$transport_cancel_orders['total']."',
                                                      total_orders_pll_places =  '".$transport_orders_pll_places['pll_places']."',
                                                      total_weight = '".$transport_total_weight['total_weight']."',
                                                      update_date = '$date' 
                                                      WHERE tid = $id"); 
  }
  $vat = $_GET['vat'];
  header("location:/opex/updateOpexInvoice.php?year=$year&month=$n&vat=$vat");  