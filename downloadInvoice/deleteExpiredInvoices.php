<?php

require_once "../config.inc.php";
error_reporting(E_ALL);

$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn ->set_charset("utf8");

$check = $conn->query("SELECT id,invoice_no FROM vtiger_send_invoice_for_client WHERE expired < NOW() AND deleted = 0");

foreach ($check as $del) {
  $invoice = $del['invoice_no'];
  $id = $del['id'];
  unlink("../storage/invoices/$invoice");
  $conn->query("UPDATE vtiger_send_invoice_for_client SET deleted = 1 WHERE id = $id");
}
