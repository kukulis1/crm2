<?php

require($_SERVER['DOCUMENT_ROOT'].'/config.inc.php');

$log_file = fopen(__DIR__ . '/logs/downloads.log', 'a+');
fwrite($log_file, '[' . date('Y-m-d H:i:s') . ']: ' . json_encode([
    'address' => !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '',
    'invoice' => !empty($_GET['invoice']) ? $_GET['invoice'] : '',
    'secret' => !empty($_GET['secret']) ? $_GET['secret'] : '',
    'user-agent' => !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
    'from_self_service' => !empty($_GET['from_self_service']) ? true : false,
], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) . "\n\n");
fclose($log_file);

require_once "../config.inc.php";
$conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
$conn->set_charset("utf8");
$invoice = $_GET['invoice'];
$secret = $_GET['secret'];

if (is_numeric($invoice) && !empty($secret)) {
    $result = $conn->query("SELECT invoice_no,receive_date,expired FROM vtiger_send_invoice_for_client WHERE invoiceid = $invoice AND `hash` = '$secret'");
    $num_rows = mysqli_num_rows($result);
    $data =  mysqli_fetch_assoc($result);
    $date = date("Y-m-d H:i:s");

    if(!empty($data['expired'])){
        if (strtotime($date) < strtotime($data['expired']) && $num_rows) {
            if ($data['receive_date']) {
                $conn->query("UPDATE vtiger_send_invoice_for_client SET `confirm` = `confirm`+1 WHERE invoiceid = $invoice");
            } else {
                $conn->query("UPDATE vtiger_send_invoice_for_client SET `confirm` = `confirm`+1, `receive_date` = '$date' WHERE invoiceid = $invoice");
            }
            $url = $_SERVER['DOCUMENT_ROOT']."/storage/invoices/{$data['invoice_no']}";

            if(!empty($_GET['from_self_service'])){
                // NOTE New method for sending info for self-service
                echo json_encode([
                    'status' => 200,
                    'file_name' => $data['invoice_no'],
                    'download_url' => "{$site_URL}storage/invoices/{$data['invoice_no']}",
                    'content_size' => $length   = sprintf("%u", filesize($url)),
                ]);
                exit;
            } else{
                // NOTE Kept old download method for old urls

                $basename = basename($url);
                $length   = sprintf("%u", filesize($url));
        
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $basename . '"');
                header('Content-Transfer-Encoding: binary');
                header('Connection: Keep-Alive');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . $length);

                ob_end_flush();   // <--- instead of ob_clean()
                set_time_limit(0);
                readfile($url);
                exit;
            }
        } else {
            fallbackReturn($_GET['from_self_service'], 403);
        }
    }else{
        fallbackReturn($_GET['from_self_service'], 100);
    }    
} else {
    fallbackReturn($_GET['from_self_service'], 404);
}

function fallbackReturn($is_self_service, $err_code){
    if(!empty($is_self_service)){
        echo json_encode([
            'status' => $err_code,
        ]);
        exit;
    } else{
        header("Location: https://uzsakymai.parnasas.lt");
    }
}