<?php

require($_SERVER['DOCUMENT_ROOT'].'/config.inc.php');

$db = new PDO("mysql:host=" . $dbconfig['db_server'] . ";dbname=" . $dbconfig['db_name'], $dbconfig['db_username'], $dbconfig['db_password']);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
$db->exec("set names utf8");

$filename = '';

if (is_numeric($_REQUEST['invoiceid'])){

    $check_invoice = $db->prepare("SELECT invoice_no, vtiger_account.accountid , invoice_no
                                   FROM vtiger_invoice 
                                   JOIN vtiger_account ON vtiger_account.accountid=vtiger_invoice.accountid
                                   WHERE vtiger_invoice.invoiceid = :invoiceid");

    $check_invoice->execute([
        ':invoiceid' => $_REQUEST['invoiceid']        
    ]); 

    $result = $check_invoice->fetch();

    if($result->invoice_no){
        // Get token
        $get_token = MakeGetRequest("$site_URL/webservice.php?operation=getchallenge&username=admin");
        $get_token = json_decode($get_token, true);
        $token = $get_token['result']['token'];
        $access_key = 'KMj4v7M31SVsFIUw';

        // Login
        $params = [
            'operation' => 'login',
            'username' => 'admin',
            'accessKey' => md5($token.$access_key)
        ];

        $get_sesion =  MakePostRequest("$site_URL/webservice.php", $params);
        $get_sesion = json_decode($get_sesion, true);
        $sessionId = $get_sesion['result']['sessionName'];


        $params = [
            'operation' => 'generate_invoice',
            'sessionName' => $sessionId,  
            'invoiceid' => $_REQUEST['invoiceid'], 
            'accountid' => $result->accountid, 
            'invoice_no' => $result->invoice_no 
        ];

        $response =  MakePostRequest("$site_URL/webservice.php", $params);   
        $filename = str_replace("/", "", $result->invoice_no).'.pdf';		
      
    }

    
	header('Content-Description: File Transfer');
	header("Content-Disposition: attachment; filename=$filename");
	header('Content-Type: application/pdf;charset=UTF-8;');
	echo $response;	

}else{
    header('Content-Description: File Transfer');
	header("Content-Disposition: attachment; filename=$filename");
	header('Content-Type: application/pdf;charset=UTF-8;');
}

function MakeGetRequest($url){
    $ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$data = curl_exec($ch);
	curl_close($ch);
    return $data;
}

function MakePostRequest($url, $params){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}


