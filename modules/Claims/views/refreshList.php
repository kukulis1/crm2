<?php

include "modules/Claims/models/ListModel.php";

use modules\Claims\models\ListModel as ListModel;

class Claims_refreshList_View extends Vtiger_Index_View {

  function __construct(){
        $this->ListModel = new ListModel;  
  }

	public function process(Vtiger_Request $request) {    
      $this->ListModel->saveClaimMails();   
  }       
}
