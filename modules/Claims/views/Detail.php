<?php

  include "modules/Claims/models/ListModel.php";

  use modules\Claims\models\ListModel as ListModel;
  use Claims_Module_Model;

  class Claims_Detail_View extends Vtiger_Detail_View {


	function showModuleDetailView(Vtiger_Request $request) {
		$this->showLineItemDetails($request);
		return parent::showModuleDetailView($request);
	}

	function showModuleSummaryView($request) {
    $this->showLineItemDetails($request);
		return parent::showModuleSummaryView($request);
  }

  function showLineItemDetails(Vtiger_Request $request) {
    global $current_user;
    $moduleName = $request->getModule();

    $this->ListModel = new ListModel;  
    $recordid = $request->get('record'); 

    $this->ListModel->setSeen($recordid,$current_user->id);
    
    if($_POST['answer']){
      $this->ListModel->sendAnswerMail($_POST,$recordid); 
    }

    if($_POST['comment']){
      $this->ListModel->claimComment($_POST,$recordid,$current_user->id); 
    }

    if($_POST['merge']){
      $this->ListModel->mergeClaims($recordid,$_POST); 
    }
    
    $getClaimMail = $this->ListModel->getClaimMail($recordid); 	
    $getComments = $this->ListModel->getComments($recordid); 	
    $viewer = $this->getViewer($request);
    $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		$viewer->assign('MODULE_MODEL', $moduleModel);
    $viewer->assign('GET_CLAIM_MAILS', $getClaimMail);  
    $viewer->assign('GET_COMMENTS', $getComments);  
  }
  

}
