<?php
error_reporting(E_ALL);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");  

  $claim_id = $_POST['claim'];
  $today = date("Y-m-d");
  $conn->query("UPDATE vtiger_claims SET end_claim = '$today' WHERE claimsid = $claim_id");
  $query = mysqli_fetch_assoc($conn->query("SELECT DATE_FORMAT(createdtime,'%Y-%m-%d') as claim_date 
                                              FROM vtiger_claims
                                              LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_claims.claimsid
                                              WHERE claimsid = $claim_id"));
  $claim_date = $query['claim_date'];
  $days = number_of_working_days($claim_date, $today);
  $conn->query("UPDATE vtiger_claimscf SET cf_1841 = $days WHERE claimsid = $claim_id"); 
  echo $days;
}else{
  http_response_code(404);
}  

function number_of_working_days($from, $to) {
  $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
  $holidayDays = [date("Y").'-01-01', date("Y").'-02-16',	date("Y").'-03-11',	date("Y-m-d",easter_date() +86400), date("Y").'-05-01',	date("Y").'-06-24',	date("Y").'-07-06',	date("Y").'-08-15',	date("Y").'-11-01',	date("Y").'-12-24',	date("Y").'-12-25',	date("Y").'-12-26']; # variable and fixed holidays

  $from = new DateTime($from);
  $from->modify('+1 day');
  $to = new DateTime($to);
  $to->modify('+1 day');
  $interval = new DateInterval('P1D');
  $periods = new DatePeriod($from, $interval, $to);  

  $days = 0;
  foreach ($periods as $period) {
      if (!in_array($period->format('N'), $workingDays)) continue;
      if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
      if (in_array($period->format('*-m-d'), $holidayDays)) continue;
      $days++;
  }
  return $days;
}