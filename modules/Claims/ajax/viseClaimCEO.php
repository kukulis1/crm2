<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $claimid = $_POST['claimid'];


  $get_claim_info = mysqli_fetch_assoc($conn->query("SELECT cf_2657 AS action,cf_2659 AS amount, claimsno, cf_20023 AS accountid  
                                                      FROM vtiger_claims  
                                                      JOIN vtiger_claimscf ON vtiger_claimscf.claimsid=vtiger_claims.claimsid
                                                      WHERE vtiger_claimscf.claimsid = $claimid"));

  $action = $get_claim_info['action'];
  $claimno = $get_claim_info['claimsno'];
  $amount = $get_claim_info['amount'];
  $accountid = $get_claim_info['accountid'];
  $userid = $_POST['userid'];
  $date = date("Y-m-d H:i:s");


  if($action == 'Taikoma nuolaida'){
      $entity_id = last_entity_record($conn);

      $conn->query("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype,  label, source, createdtime, modifiedtime) 
                    VALUES ($entity_id, $userid, $userid, 'Customerdiscount','$claimno','CRM', '$date', '$date')");

      $conn->query("INSERT INTO vtiger_customerdiscount (customerdiscountid, customerdiscount_tks_claim_no, customerdiscount_tks_amount) VALUES ($entity_id,'$claimno',$amount)");    

      $conn->query("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES ($entity_id,'Customerdiscount',$accountid,'Accounts')");
                
      $result = $conn->query("INSERT INTO vtiger_customerdiscountcf (customerdiscountid) VALUES ($entity_id)");              
    
  }


  $check = $conn->query("SELECT * FROM vtiger_claims_vise WHERE claim_id = $claimid");

  $check2 = mysqli_num_rows($check);

  if(!$check2) $insert = $conn->query("INSERT INTO vtiger_claims_vise (claim_id,ceo) VALUES ($claimid,1)");

  if($insert){
    echo 'true';
  }else{
    echo 'false';  
  }
    
}else{
  http_response_code(404);
}
  

function last_entity_record($conn)
{    
  $conn->query('LOCK TABLES vtiger_crmentity_seq READ'); 
  $get_crmid = mysqli_fetch_assoc($conn->query("SELECT id FROM `vtiger_crmentity_seq`"));       
  // $conn->query('UNLOCK TABLES');   
  $new_seq = $get_crmid ['id'] + 1;  
  $conn->query('LOCK TABLES vtiger_crmentity_seq WRITE');   
  $conn->query("UPDATE `vtiger_crmentity_seq` SET id = $new_seq");
  $conn->query('UNLOCK TABLES');  

  return $new_seq;
}