<?php

error_reporting(0);
require_once "../../../config.inc.php";
global $site_URL;

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $action = $_POST['action'];
  $claimno = $_POST['claimno'];
  $claimid = $_POST['claimid'];
  $amount = $_POST['amount'];
  $accountid = $_POST['accountid'];
  $userid = $_POST['userid'];
  $date = date("Y-m-d H:i:s");

  if($action == 'Taikoma nuolaida'){

    $check_discount = $conn->query("SELECT r.crmid 
        FROM vtiger_crmentityrel r
        JOIN vtiger_customerdiscount  ON customerdiscountid=r.crmid
        INNER JOIN vtiger_crmentity e ON e.crmid=r.crmid
        WHERE deleted = 0 AND r.module = 'Customerdiscount' AND (relcrmid = $accountid AND relmodule = 'Accounts') AND customerdiscount_tks_claim_no = '$claimno'");

    if(mysqli_num_rows($check_discount)){
      $discount = mysqli_fetch_assoc($check_discount);
      $result = $conn->query("UPDATE vtiger_customerdiscount SET customerdiscount_tks_amount = $amount WHERE customerdiscountid = ".$discount['crmid']);
      $conn->query("UPDATE vtiger_crmentity SET modifiedtime = '$date' WHERE crmid = ".$discount['crmid']);
      if($result){
        echo json_encode(['status' => 'success']);
      }else{
        echo json_encode(['status' => 'Fail']);
      }
    }else{
      $entity_id = last_entity_record($conn);

      $conn->query("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype,  label, source, createdtime, modifiedtime) 
                    VALUES ($entity_id, $userid, $userid, 'Customerdiscount','$claimno','CRM', '$date', '$date')");

      $conn->query("INSERT INTO vtiger_customerdiscount (customerdiscountid, customerdiscount_tks_claim_no, customerdiscount_tks_amount) VALUES ($entity_id,'$claimno',$amount)");    

      $conn->query("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES ($entity_id,'Customerdiscount',$accountid,'Accounts')");
                
      $result = $conn->query("INSERT INTO vtiger_customerdiscountcf (customerdiscountid) VALUES ($entity_id)");              

      if($result){
        echo json_encode(['status' => 'success','action' => $action, 'claimno' => $claimno, 'amount' => $amount]); 
      }else{
        echo json_encode(['status' => 'Fail']);
      }
    }

  }else{
    $get_vendor_id = mysqli_fetch_assoc($conn->query("SELECT vendorid FROM vtiger_vendor WHERE accountid = $accountid"));
    $vendorid = $get_vendor_id['vendorid'];
    $region = ($action == 'Parnasas perka su PVM' ? 0 : 1);
    $text = "Pagal pretenzija NR. $claimno";

    $url = $site_URL."/index.php?module=PurchaseOrder&view=Edit&vendor_id=$vendorid&region=$region&netTotal=$amount&description=$text";

    $conn->query("UPDATE vtiger_claimscf SET cf_2661 = '$url' WHERE claimsid = $claimid");
    echo json_encode(['status' => 'success','url' => $url]);
  }
    
}else{
  http_response_code(404);
}
  

function last_entity_record($conn)
{    
  $conn->query('LOCK TABLES vtiger_crmentity_seq READ'); 
  $get_crmid = mysqli_fetch_assoc($conn->query("SELECT id FROM `vtiger_crmentity_seq`"));       
  // $conn->query('UNLOCK TABLES');   
  $new_seq = $get_crmid ['id'] + 1;  
  $conn->query('LOCK TABLES vtiger_crmentity_seq WRITE');   
  $conn->query("UPDATE `vtiger_crmentity_seq` SET id = $new_seq");
  $conn->query('UNLOCK TABLES');  

  return $new_seq;
}