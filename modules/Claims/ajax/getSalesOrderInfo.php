<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
  
  $salesorderid = $_POST['salesorderid'];
  $claim_id = $_POST['claim_id'];
  $place = $_POST['place'];
  $type = $_POST['type'];


  $query_string = "SELECT s.accountid, a.accountname, DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS order_date , cf_855 AS type, load_date_to,unload_date_to, 
                              CONCAT(bill_street,' , ',bill_city,' , ',bill_code) load_place,
                              CONCAT(ship_street,' , ',ship_city,' , ',ship_code) unload_place,
                              GROUP_CONCAT(ROUND(quantity,0),' ', code,' ' ,cargo_wgt,' ',cargo_length,'x',cargo_width,'x',cargo_height) AS cargo, cf_1562 AS driver, cf_1558 AS route,REPLACE(cf_1687,'|##|',',') AS loaders
                                          FROM vtiger_salesorder s
                                          LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                          LEFT JOIN vtiger_account a ON a.accountid=s.accountid
                                          LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
                                          LEFT JOIN vtiger_sobillads bill ON bill.sobilladdressid=s.salesorderid
                                          LEFT JOIN vtiger_soshipads ship ON ship.soshipaddressid=s.salesorderid
                                          LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                                          LEFT JOIN app_measures ON CASE WHEN i.cargo_measure REGEXP '^[0-9]+$' THEN app_measures.id=i.cargo_measure  ELSE app_measures.code=i.cargo_measure	END
                                          WHERE "; 
                                          if($type == 'salesorderid'){
                                           $query_string .="  s.salesorderid = $salesorderid ";
                                          }elseif($type == 'shipment_code'){
                                            if(is_numeric($salesorderid)){
                                              $query_string .="  s.external_order_id = $salesorderid ";
                                            }else{
                                              $query_string .="  s.shipment_code = '$salesorderid' ";
                                            }
                                          }
                                        $query_string .="  GROUP BY s.salesorderid";

  $query = mysqli_fetch_assoc($conn->query($query_string));

  if($place == 'Detail'){
    $query2 = $conn->query("UPDATE vtiger_claims SET 
                                                    claims_tks_order_date = '".$query['order_date']."',  
                                                    claims_tks_order_type = '".$query['type']."',
                                                    claims_tks_load_date = '".$query['load_date_to']."',
                                                    claims_tks_load_place = '".$query['load_place']."',
                                                    claims_tks_unload_date = '".$query['unload_date_to']."',
                                                    claims_tks_unload_place = '".$query['unload_place']."',
                                                    claims_tks_cargo = '".$query['cargo']."',
                                                    claims_tks_trip = '".$query['route']."',
                                                    claims_tks_driver = '".$query['driver']."',
                                                    claims_tks_stevedore = '".$query['loaders']."',
                                                    cf_20023 = '".$query['accountid']."'
                              WHERE claimsid = $claim_id");

    $query3 = $conn->query("UPDATE vtiger_claimscf SET cf_1837 = 'Sprendžiama' WHERE claimsid = $claim_id");
  }

  echo json_encode($query);

}else{
  http_response_code(404);
}
  

