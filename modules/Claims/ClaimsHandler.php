<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */
include_once 'include/Webservices/Revise.php';
include_once 'include/Webservices/Retrieve.php';

class ClaimsHandler extends VTEventHandler {

    function handleEvent($eventName, $entityData) {

        $moduleName = $entityData->getModuleName();

        // Validate the event target
        if ($moduleName != 'Claims') {
            return;
        } 

        //Get Current User Information
        global $current_user, $currentModule, $site_URL;

  
        if ($eventName == 'vtiger.entity.aftersave') {
            $db = PearDatabase::getInstance();
            
            if ($currentModule != 'Claims')
                return;

            $claimid = $entityData->getId();           
            $action = $entityData->get('cf_2657');
            $amount = $entityData->get('cf_2659');                
            $claimno = $entityData->get('claimsno');
            $accountid = $entityData->get('cf_20023');
            $userid = $current_user->id;
            $vise = $entityData->get('cf_2443');
            $date = date("Y-m-d H:i:s");

            if(!empty($accountid)){
                if($action == 'Taikoma nuolaida'){   
                    if(!$vise){
                        $amount = ($amount ?:0);

                        $check_discount_query = "SELECT r.crmid ,customerdiscount_tks_amount
                        FROM vtiger_crmentityrel r
                        JOIN vtiger_customerdiscount  ON customerdiscountid=r.crmid
                        INNER JOIN vtiger_crmentity e ON e.crmid=r.crmid
                        WHERE deleted = 0 AND r.module = 'Customerdiscount' AND (relcrmid = ? AND relmodule = 'Accounts') AND customerdiscount_tks_claim_no = ?";

                        $check_discount = $db->pquery($check_discount_query,array($accountid,$claimno));   
                        $discount_sum = $db->query_result($check_discount,0,'customerdiscount_tks_amount');
                        $num_rows = $db->num_rows($check_discount);                   

                        if($num_rows > 0){
                            if($amount != $discount_sum){
                                $discount_id = $db->query_result($check_discount,0,'crmid');
                                $db->pquery("UPDATE vtiger_customerdiscount SET customerdiscount_tks_amount = ? WHERE customerdiscountid = ?",array($amount,$discount_id));
                                $db->pquery("UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?",array($date,$discount_id));         
                            }
                        }else{
                            $db->query('LOCK TABLES vtiger_crmentity_seq READ'); 
                            $get_crmid_query = $db->query("SELECT id FROM `vtiger_crmentity_seq`");  
                            $get_crmid = $db->query_result($get_crmid_query,0,'id');     
                            
                            $new_seq = $get_crmid + 1;  
                            $db->query('LOCK TABLES vtiger_crmentity_seq WRITE');   
                            $db->pquery("UPDATE `vtiger_crmentity_seq` SET id = ?",array($new_seq));
                            $db->query('UNLOCK TABLES');                      

                            $db->pquery("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype,  label, source, createdtime, modifiedtime) 
                                        VALUES (?,?,?,?,?,?,?,?)",array($new_seq, $userid, $userid, 'Customerdiscount',$claimno,'CRM', $date, $date));

                            $db->pquery("INSERT INTO vtiger_customerdiscount (customerdiscountid, customerdiscount_tks_claim_no, customerdiscount_tks_amount) VALUES (?,?,?)",array($new_seq,$claimno,$amount));    

                            $db->pquery("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES (?,?,?,?)",array($new_seq,'Customerdiscount',$accountid,'Accounts'));
                                        
                            $db->pquery("INSERT INTO vtiger_customerdiscountcf (customerdiscountid) VALUES (?)",array($new_seq));
                        }   
                    }        
                }else{
                    $get_vendor_id = $db->pquery("SELECT vendorid FROM vtiger_vendor WHERE accountid = ?",array($accountid));
                    $vendorid = $db->query_result($get_vendor_id,0,'vendorid');
                    $region = ($action == 'Parnasas perka su PVM' ? 0 : 1);
                    $text = "Pagal pretenzija NR. $claimno";
                    $url = $site_URL."/index.php?module=PurchaseOrder&view=Edit&vendor_id=$vendorid&region=$region&netTotal=$amount&description=$text";
                    $db->pquery("UPDATE vtiger_claimscf SET cf_2661 = ? WHERE claimsid = ?",array($url,$claimid)); 
                        
                }
            }
        }

    }

}

?>