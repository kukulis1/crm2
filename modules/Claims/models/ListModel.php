<?php
namespace modules\Claims\models;

require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';

require_once 'vtlib/Vtiger/Email/vendor/autoload.php';

use PhpImap\Exceptions\ConnectionException;
use PhpImap\Mailbox;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


class ListModel {

  protected $db;
  protected $mailHost;
  protected $mailUsernameQuality;
  protected $mailPasswordQuality;

  public function __construct()
  {
    global $adb;
    $this->db = $adb; 
  }

  public function mergeClaims($recordid,$post){
    $merge_claims = $post['merge_claims'];

    $sql = "UPDATE vtiger_claim_emails SET claimid = ? WHERE claimid = ?";
    $sql2 = "UPDATE vtiger_claim_comments SET claim_id = ? WHERE claim_id = ?";
    $sql3 = "UPDATE vtiger_crmentity SET deleted = 1 WHERE crmid = ?";

    $sql4 = "UPDATE vtiger_attachments SET related_id = ? WHERE related_id = ?";
    $sql5 = "UPDATE vtiger_senotesrel SET crmid = ? WHERE crmid = ?";

    foreach($merge_claims AS $merge){      
      $this->db->pquery($sql,array($recordid,$merge));
      $this->db->pquery($sql2,array($recordid,$merge));
      $this->db->pquery($sql3,array($merge));
      $this->db->pquery($sql4,array($recordid,$merge));
      $this->db->pquery($sql5,array($recordid,$merge));
    }
    
    header("Location:/index.php?module=Claims&view=Detail&record=$recordid&mode=showDetailViewByMode&requestMode=full");
  }

  public function claimComment($post,$recordid,$user)
  {
    $sql = "INSERT INTO vtiger_claim_comments (claim_id, comment, user) VALUES (?,?,?)";
    $this->db->pquery($sql,array($recordid,$post['comment'],$user));
    $this->db->pquery("UPDATE vtiger_crmentity SET modifiedtime = ?, modifiedby = ? WHERE crmid = ?",array(date("Y-m-d H:i:s"),$user,$recordid));
    header("Location:/index.php?module=Claims&view=Detail&record=$recordid&mode=showDetailViewByMode&requestMode=full");
  }

  public function autoAnswerToClaim($client_email,$subject,$hash)
  {
    global $mailHost, $mailUsernameQuality,$mailPasswordQuality;
    $this->mailHost = $mailHost;
    $this->mailUsername = $mailUsernameQuality;
    $this->mailPassword = $mailPasswordQuality;  

    $answer = 'Jūsų pretenzija gauta, susisieksime 5d.d. laikotarpyje.';

    $mail = new PHPMailer(true);

    try {
        //Server settings
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      
        $mail->isSMTP();                                            
        $mail->Host       = $this->mailHost;                    
        $mail->SMTPAuth   = true;                                   
        $mail->Username   = $this->mailUsername;                    
        $mail->Password   = $this->mailPassword;                               
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;        
        $mail->Port       = 465; 
        $mail->SMTPOptions = array('ssl' => array(
                                   'verify_peer' => false,
                                   'verify_peer_name' => false,
                                   'allow_self_signed' => true
                                    )
                              );
        $mail->setLanguage('lt', '../vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
        $mail->CharSet = 'UTF-8';
        // $mail->SMTPDebug = false;  

        //Recipients
        $mail->setFrom($this->mailUsername, 'Kokybe');
        $mail->addAddress($client_email);         
       

        // Content
        $mail->isHTML(true); 
        $mail->Subject = "RE:$subject $hash";
        $mail->Body    = $answer;
        $mail->send();  

    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
  }

  public function sendAnswerMail($post,$recordid)
  {
    global $mailHost, $mailUsernameQuality,$mailPasswordQuality;
    global $current_user;
    $this->mailHost = $mailHost;
    $this->mailUsername = $mailUsernameQuality;
    $this->mailPassword = $mailPasswordQuality;    
    
    $year = date("Y");
    $month = date("F");
    $dayofweek = "week".$this->weekOfMonth(strtotime(date("Y-m-d")));        
    $time = time();
    $target_dir = "storage/$year/$month/$dayofweek/"; 


    if($_FILES['attachments']['name'][0]){
      if (!file_exists("storage/$year/$month/$dayofweek")) {
        mkdir("storage/$year/$month/$dayofweek", 0777, true);
      }   
    
      $files_arr = [];
      $files_to_insert = [];
      $count=0; 
      foreach ($_FILES['attachments']['name'] as $filename) 
      {  
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $oldName= pathinfo(basename($filename), PATHINFO_FILENAME);  
        $newName =  $this->lt_chars(str_replace(' ', '',$oldName)).'_'.$time.'.'.$ext;        
        $files_arr[] = $target_dir.$newName;
        $files_to_insert[$oldName] = $newName;
        move_uploaded_file($_FILES['attachments']['tmp_name'][$count],$target_dir.$newName);  

        $count++;         
      }
    }

    $cc = explode(';',$post['cc']);

    $sql = "SELECT email,hash,subject,body FROM vtiger_claim_emails WHERE claimid = ? AND type = 1 ORDER BY id DESC LIMIT 1";
    $sql2 = "SELECT cf_1839 FROM vtiger_claimscf WHERE claimsid = ?";
    $sql3 = "SELECT CONCAT(first_name,' ',last_name) AS employee, rolename
                      FROM crm.vtiger_users u
                      LEFT JOIN vtiger_user2role ur ON ur.userid=u.id
                      LEFT JOIN vtiger_role r ON r.roleid=ur.roleid
                      WHERE id = ?";

    $result = $this->db->pquery($sql,array($recordid));
    $result2 = $this->db->pquery($sql2,array($recordid));
    $result3 = $this->db->pquery($sql3,array($current_user->id));
    $client_email = $this->db->query_result($result2,0,'cf_1839');
    $bodyText = $this->db->query_result($result,0,'body');
    $employee = $this->db->query_result($result3,0,'employee');
    $rolename = $this->db->query_result($result3,0,'rolename');

    $signatare_phones = $this->db->pquery("SELECT * FROM vtiger_claims_email_signatare_phones ORDER BY id DESC LIMIT 1",[]);
    $t_phone = $this->db->query_result($signatare_phones, 0, 't_phone');
    $f_phone = $this->db->query_result($signatare_phones, 0, 'f_phone');
    $m_phone = $this->db->query_result($signatare_phones, 0, 'm_phone');
    $email = $this->db->query_result($signatare_phones, 0, 'email');

   $signatare = "
    <br><br>
    <img src='https://crm.parnasas.lt//layouts/v7/modules/Claims/resources/parnasas-signatare.jpg'><br>
    <table border='0' cellpadding='0' cellspacing='0' style='color:#808080; font-size:12px;'>      
      <tr style='margin-top:10px;'>
        <td>Geros dienos Jums linki |  Best Regards</td>
      </tr>
      </table>
      <table border='0' cellpadding='0' cellspacing='0' style='color:#808080; font-size:12px;'>     
      <tr>
        <td>$employee</td>
      </tr>
      <tr>
        <td>$rolename </td>
      </tr>
      </table>
      <table border='0' cellpadding='0' cellspacing='0' style='color:#808080; font-size:12px;margin-top:10px;'>  
      <tr>
        <td style='width: 120px;'><span style='color:#000080;font-size:13px;font-weight:bold;'>T</span> $t_phone &nbsp;&nbsp;|</td>
        <td style='width: 120px;'><span style='color:#000080;font-size:13px;font-weight:bold;'>F</span> $f_phone &nbsp;&nbsp;|</td>
        <td style='width: 120px;'><span style='color:#000080;font-size:13px;font-weight:bold;'>M</span> $m_phone</td>
      </tr>
      </table>
      <table border='0' cellpadding='0' cellspacing='0' style='color:#808080; font-size:12px;'>  
      <tr>
          <td style='width: 90px;'>JSC Parnasas &nbsp;|</td>
          <td style='width: 80px;'>Metalo g. 2  &nbsp;|</td>
          <td style='width: 90px;'>Vilnius 02190 &nbsp;&nbsp;|</td>
          <td style='width: 90px;'>Lithuania</td>    
      </tr>
      </table>
      <table border='0' cellpadding='0' cellspacing='0' style='color:#808080; font-size:12px;'>  
      <tr>
        <td style='width: 110px;'><a href='www.parnasas.lt'>www.parnasas.lt</a> &nbsp;|</td>
        <td style='width: 130px;'><a href='www.perkraustymas.lt'>www.perkraustymas.lt</a>  &nbsp;|</td>  
        <td style='width: 120px;'><a email='$email'>&nbsp;&nbsp;$email</td>
      </tr>
    </table>";
    $subject2 = $this->db->query_result($result,0,'subject');
    $hash = $this->db->query_result($result,0,'hash');
    $pattern = "/[[][a-z]{3}[:][_][A-Z]{5}[_]\d{10}[.]\d{1,9}[:][a-z]{3}[]]/";   

    if(preg_match($pattern, $subject2)){
      $subject = $subject2;
    }else{  
      $subject = $subject2." ".$hash;
    }

    $sql2 = "INSERT INTO vtiger_claim_emails (claimid, type, subject, email, cc, body) VALUES (?,?,?,?,?,?)";
    

    // $body = "<hr style='margin-top:10px;'>
    // <blockquote style='margin-left: 0;'>
    //   <b>From:</b> [mailto:$cEmail] <br>
    //   <b>To:</b> '$this->mailUsername'<br>
    //   <b>Subject:</b> $subject2<br><br>
    //   $bodyText     
    // </blockquote>";

    $myfile = fopen($_SERVER['DOCUMENT_ROOT']."/uploads/quote_$recordid.txt", "w");
    fwrite($myfile, $bodyText);
    fclose($myfile);


    $answer = nl2br($post['answer']);
    // $answer .= nl2br($body); // quote siunciamas atskirai txt faile

    $warning = '<br><br> <small>Norėdami užtikrinti kokybę ir sklandų problemos sprendimą, prašome nekeisti laiško temos (subject).</small>';

    $answer = nl2br($post['answer']);
    // $answer .= nl2br($body.$warning); // quote siunciamas atskirai txt faile
    $answer .= nl2br($warning);

    $answer .= $signatare;

    $mail = new PHPMailer(true);

    try {
        //Server settings
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      
        $mail->isSMTP();                                            
        $mail->Host       = $this->mailHost;                    
        $mail->SMTPAuth   = true;                                   
        $mail->Username   = $this->mailUsername;                    
        $mail->Password   = $this->mailPassword;                               
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;        
        $mail->Port       = 465; 
        $mail->SMTPOptions = array('ssl' => array(
                                   'verify_peer' => false,
                                   'verify_peer_name' => false,
                                   'allow_self_signed' => true
                                    )
                              );
        $mail->setLanguage('lt', '../vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
        $mail->CharSet = 'UTF-8';
        // $mail->SMTPDebug = false;  

        //Recipients
        $mail->setFrom($this->mailUsername, 'Kokybe');
        $mail->addAddress($client_email); 
         
        if(!empty($cc[0])){
         foreach ($cc as $cc_email) {
           if(!empty($cc_email)){
             $mail->addCC(trim($cc_email));
           }
         }
        }

        if(count($files_arr) > 0){
          foreach($files_arr AS $target_file){
            $mail->addAttachment($target_file);
          }
        }
        
   

        $mail->addAttachment($_SERVER['DOCUMENT_ROOT']."/uploads/quote_$recordid.txt");
        
        $subject = str_replace("RE:", "",$subject);

        // Content
        $mail->isHTML(true); 
        $mail->Subject = "RE:$subject";
        $mail->Body    = $answer;
        $mail->send();

        $cc_string = implode(',',$cc);

        $this->db->pquery($sql2,array($recordid,0,"RE:$subject", $this->mailUsername,$cc_string,$post['answer']));
        $letter_id = $this->db->getLastInsertID();

        if(count($files_to_insert) > 0){
          foreach($files_to_insert AS $fileName => $newFileName){
            $this->insertDocument($this->db,$recordid,26,$target_dir,$fileName, $newFileName,$letter_id);    
          }
        }

        header("Location:/index.php?module=Claims&view=Detail&record=$recordid&mode=showDetailViewByMode&requestMode=full");
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
  }

  public function getClaimMail($recordid)
  {
    $sql = "SELECT e.*, GROUP_CONCAT(attachmentsid) AS attachment,
     CASE WHEN cf_1839 != email && email != 'kokybe@parnasas.lt' THEN true ELSE false END as cc
               FROM vtiger_claim_emails e
               LEFT JOIN vtiger_claimscf cf ON cf.claimsid=e.claimid               
               LEFT JOIN vtiger_senotesrel s ON s.crmid=claimid
               LEFT JOIN vtiger_notes n ON n.notesid=s.notesid
			         LEFT JOIN vtiger_seattachmentsrel r ON r.crmid=n.notesid    
               WHERE claimid = ?
               GROUP BY e.id
               ORDER BY id DESC";
    $result = $this->db->pquery($sql, array($recordid));
    return $result;
  }

  public function getComments($recordid)
  {    
    $sql = "SELECT vtiger_claim_comments.*, vtiger_users.first_name, vtiger_users.last_name 
                     FROM vtiger_claim_comments 
                     LEFT JOIN vtiger_users ON vtiger_users.id=vtiger_claim_comments.user
                     WHERE claim_id = ? ORDER BY id DESC";
    $result = $this->db->pquery($sql, array($recordid));
    return $result;
  }

  public function generateHash($mail_id){
    $date = time();

    $string = "[ref:_CLAIM_";
    $string .= $date.".".$mail_id;
    $string .= ":ref]";

    return $string;
  }


  public function saveClaimMails()
  {

    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    global $mailHostQuality, $mailUsernameQuality,$mailPasswordQuality;
    $this->mailHost = $mailHostQuality;
    $this->mailUsername = $mailUsernameQuality;
    $this->mailPassword = $mailPasswordQuality;  
    $date = date("Y-m-d H:i:s");
    $setype = 'Claims';
 

    $sql = "INSERT INTO vtiger_claims (claimsid,claims_tks_source,claims_tks_client) VALUES(?,?,?)";
    $sql1 = "INSERT INTO vtiger_claimscf (claimsid,cf_1837,cf_1839) VALUES(?,?,?)";
    $sql2 = "INSERT INTO vtiger_claim_emails (claimid,mailid,hash,type,subject,email,body) VALUES(?,?,?,?,?,?,?)";
    
    $sql3 = "SELECT c.claimsid FROM vtiger_claims c                     
                LEFT JOIN vtiger_claim_emails em ON em.claimid=c.claimsid
                LEFT JOIN vtiger_crmentity e ON e.crmid=c.claimsid
                WHERE e.deleted = 0 AND ((em.mailid = ?  OR em.email = ?) AND em.type = 1) OR em.cc = ? AND em.type = 0
                ORDER BY claimsid DESC LIMIT 1";

    $sql4 = "SELECT claimid FROM vtiger_claim_emails WHERE type = 1 AND mailid = ? LIMIT 1";
    $sql9 = "SELECT claimid,hash FROM vtiger_claim_emails WHERE type = 1 AND hash = ? LIMIT 1";

    $sql5 = "SELECT claimid FROM vtiger_claim_emails WHERE type = 1 AND email = ? OR cc = ? ORDER BY claimid DESC LIMIT 1";

    $sql6 = "SELECT cf_1837 FROM vtiger_claimscf WHERE claimsid = ?";  
    
    $sql7 = "UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?";

    $sql8 = "UPDATE vtiger_claims_seen SET seen = ?, seen_time = ?, reviewed_person = ? WHERE claim_id = ?";
    
    $mailbox = new Mailbox($this->mailHost,$this->mailUsername,$this->mailPassword);  
    $last_check = date("Ymd");


    try {
      $mail_ids = $mailbox->searchMailbox('SINCE "'.$last_check.'"');
      foreach ($mail_ids AS $mail_id) {  

        $email = $mailbox->getMail(
            $mail_id, 
            true 
        );

        $pattern = "/[[][a-z]{3}[:][_][A-Z]{5}[_]\d{10}[.]\d{1,9}[:][a-z]{3}[]]/";   
      

        // echo '<pre>';
        // echo "Emailai";
        // echo $email->subject."<br>";
        // echo $email->fromAddress."<br>";
        // echo $email->textPlain."<br>";
        // echo "***********************************************************";
        // echo "<br><br>";
        // echo '</pre>';
       

        $matches = array(); 
        preg_match($pattern, $email->subject, $matches);          
        $subject_hash = $matches[0]; 

        $check_result = $this->db->pquery($sql3,array($mail_id,$email->fromAddress,$email->fromAddress));
        $check_email = $this->db->pquery($sql4,array($mail_id));
        $check_hash = $this->db->pquery($sql9,array($subject_hash));

        // $getId = $this->db->pquery($sql5,array($email->fromAddress,$email->fromAddress));

        $num_rows =  $this->db->num_rows($check_result);
        $num_rows2 =  $this->db->num_rows($check_email);     
        $num_rows3 =  $this->db->num_rows($check_hash);     

        // if(strpos($email->textPlain, "From")){
        //   $body = substr($email->textPlain, 0, strpos($email->textPlain, "From"));
        // }else{
          $body = $this->shorter($email->textPlain,5000);
        // }
    
 
        $emailSubDomain = substr($email->fromAddress, strpos($email->fromAddress, "@") + 1);  
        

        if(!$num_rows2){
          if(!$num_rows3){       
            $hash = $this->generateHash($mail_id);
            $entity_id = $this->last_entity_record($this->db);
             $sender_email = '';
            if($emailSubDomain != 'parnasas.lt'){
              $sender_email = $email->fromAddress;
            }
            $this->insert_entity($this->db, $setype, $entity_id, 26, NULL, $email->fromName,'Email', $date);
            $this->db->pquery($sql,array($entity_id ,'Klientas',$email->fromName));
            $this->db->pquery($sql1,array($entity_id ,'Naujas',$sender_email));

            $this->db->pquery($sql2,array($entity_id,$mail_id,$hash,1 ,$email->subject,$email->fromAddress,$body));
            $letter_id = $this->db->getLastInsertID();
            $this->setClaimNumber($entity_id);
            $this->addAttachment($this->db,$email,$entity_id,$letter_id);
             if($emailSubDomain != 'parnasas.lt'){
                $this->autoAnswerToClaim($email->fromAddress,$email->subject,$hash);
              }
          }else{      
            $claim_id = $this->db->query_result($check_hash,0,'claimid');
            $hash = $this->db->query_result($check_hash,0,'hash');
            $this->db->pquery($sql2,array($claim_id,$mail_id,$hash,1 ,$email->subject,$email->fromAddress,$body));
            $letter_id = $this->db->getLastInsertID();
            $this->update_entity($this->db,$claim_id,$date);    
            $this->addAttachment($this->db,$email,$claim_id,$letter_id);       
            $this->db->pquery($sql8,array(0,'','',$claim_id));
          }
        }        
         

    }

    } catch (ConnectionException $ex) {
        die('IMAP connection failed: '.$ex->getMessage());
    } catch (Exception $ex) {
        die('An error occured: '.$ex->getMessage());
    } 
    
 
    $mailbox->disconnect();   
  
    header("Location:/index.php?module=Claims&view=List");
  }

  public function shorter($text, $chars_limit)
  {
      // Check if length is larger than the character limit
      if (strlen($text) > $chars_limit)
      {
          // If so, cut the string at the character limit
          $new_text = substr($text, 0, $chars_limit);
          // Trim off white space
          $new_text = trim($new_text);
          // Add at end of text ...
          return $new_text . "...";
      }
      // If not just return the text as is
      else
      {
      return $text;
      }
  }

  public function setClaimNumber($recordId){
		$date = date('Y-m-d');

		$prefix = 'CLAIM-';		

		$query = "SELECT claimsno FROM vtiger_claims
															LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_claims.claimsid 							 
															WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$date'  AND vtiger_crmentity.deleted = 0  AND claimsno != ''
															ORDER BY vtiger_claims.claimsid  
															DESC LIMIT 1";
				
		$check_cur =  $this->db->pquery($query, array());
	
		$curid =  $this->db->query_result($check_cur, 0, 'claimsno');

		if(!empty($curid)){
			$curid = substr($curid, -3);
		}else{
			$req_no = '001';
		}	

		$todayDate = date('ymd');

		if(!empty($curid)){
			$strip = strlen($curid) - strlen($curid + 1);
			if ($strip < 0)
				$strip = 0;
			$temp = str_repeat("0", $strip);
			$req_no.= $temp . ($curid + 1);
		}
	
		$prev_inv_no = $prefix.$todayDate."-".$req_no;

	  $this->db->pquery("UPDATE vtiger_claims SET claimsno = ? WHERE claimsid=?", array($prev_inv_no, $recordId));
    $this->db->pquery("UPDATE vtiger_crmentity SET label = ? WHERE crmid = ?", array($prev_inv_no, $recordId));
    
    $this->db->pquery("INSERT INTO  vtiger_claims_seen (claim_id, seen, seen_time, reviewed_person) VALUES (?,?,?,?)", array($recordId,0,'',''));

  }
  
public function setSeen($recordId, $userId){
  $sql = "SELECT seen FROM vtiger_claims_seen WHERE claim_id = ? AND seen = ?";
  $check_result = $this->db->pquery($sql,array($recordId, 0));  
  $num_rows =  $this->db->num_rows($check_result);

  if($num_rows){
    $sql2 = "UPDATE vtiger_claims_seen SET seen = ?, seen_time = ?, reviewed_person = ? WHERE claim_id = ? ";
    $date = date('Y-m-d H:i:s');
    $this->db->pquery($sql2, array(1,$date,$userId,$recordId));

    $result = $this->db->pquery("SELECT CONCAT(first_name,' ',last_name) as employee FROM vtiger_users WHERE id = ?",array($userId));
    $employee =  $this->db->query_result($result, 0, 'employee');
    $id = $this->last_modtracker_record2();    
    $this->db->pquery("INSERT INTO vtiger_modtracker_basic (id, crmid, module, whodid, changedon,status) VALUES (?,?,?,?,?,?)", array($id,$recordId,'Claims',26, $date,0));
    $this->db->pquery("INSERT INTO vtiger_modtracker_detail (id, fieldname, prevalue, postvalue) VALUES (?,?,?,?)", array($id,'cf_1924', '', $employee));   
    $this->db->pquery("UPDATE vtiger_claimscf SET cf_1924 = ? WHERE claimsid = ?", array($employee,$recordId));
  }
}

public function last_modtracker_record2()
{
  $result = $this->db->pquery('SELECT id FROM vtiger_modtracker_basic_seq',array());
  $curid =  $this->db->query_result($result, 0, 'id');
  $crmid = $curid + 1; 
  $this->db->pquery("UPDATE vtiger_modtracker_basic_seq SET id = ?", array($crmid));   
  return $crmid;
}

 public function addAttachment($db,$email,$id,$letter_id){
    if ($email->hasAttachments()) {                 
      $attachments = $email->getAttachments();
      $year = date("Y");
      $month = date("F");
      $dayofweek = "week".$this->weekOfMonth(strtotime(date("Y-m-d"))); 

      foreach ($attachments AS $attachment) {         
        
        if (!file_exists("storage/$year/$month/$dayofweek")) {
          mkdir("storage/$year/$month/$dayofweek", 0777, true);
        } 
          $ext = strtolower(pathinfo($attachment->name, PATHINFO_EXTENSION)); 
          $oldName = $this->lt_chars(str_replace(' ', '',pathinfo($attachment->name, PATHINFO_FILENAME)));                             
            if(empty($ext)) $ext = strtolower($oldName);
            $newFileName = $oldName.'_'.time().'.'.$ext;    
           
            if(in_array($ext,array('jpg','png','rar','zip','gif','xls','xlsx','pdf','docx','doc'))){            
              $filename = $oldName.'_'.time();              
              $attachment->setFilePath("storage/$year/$month/$dayofweek/$newFileName");            
              $patch = "storage/$year/$month/$dayofweek/";         

              if ($attachment->saveToDisk()) {   
                $this->insertDocument($db,$id,26,$patch,$filename, $newFileName,$letter_id);                            
              } else {
                echo "ERROR, could not save!<br>";
              }     
            }                 
      }  
               
    }
 }

  public function insertDocument($db,$recordid,$userid,$patch,$filename,$filefullname,$letter_id)
  {
    global $root_directory;
    $date_now = date('Y-m-d H:s:i');
    $entity_id = $this->last_entity_record($db);   
    $this->insert_entity($db,'Documents', $entity_id, $userid, NULL, '', 'CRM', $date_now);
    $this->insert_senotesrel($db, $entity_id, $recordid);
    $this->insertNotes($db,$entity_id,$filename,$filefullname,1,'I',1);    
    $this->insert_NoteCf($db, $entity_id);
    
    $entity_id2 = $this->last_entity_record($db);  
    $db->query("INSERT INTO vtiger_claims_letters_attachments (letter_id,attachment_id) VALUES ($letter_id,$entity_id2)");
    if(renameFile($patch,$filefullname,$entity_id2)){
      $this->insert_entity($db,'Documents Attachment', $entity_id2, $userid, NULL, '', 'CRM', $date_now);
      $this->insertAttachments($db, $entity_id2,$filefullname,$patch,$recordid);
      $this->insertSeAttachmentsrel($db, $entity_id,$entity_id2);	
    }
  
  }

  public function last_entity_record($adb)
  {
    $sql = "SELECT id FROM `vtiger_crmentity_seq`";
    $sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
    $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
    $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
    $lock3 = 'UNLOCK TABLES';

    $this->db->pquery($lock, array()); 
      $get_id = $this->db->pquery($sql, array()); 
      $seq = $this->db->query_result($get_id,0,'id');    
    $this->db->pquery($lock3, array());    

    $new_seq = $seq + 1;   

    $this->db->pquery($lock2, array()); 
     $this->db->pquery($sql2, array($new_seq)); 
    $this->db->pquery($lock3, array()); 

    return $new_seq;
  }

  public function insert_entity($adb, $setype, $entity_id, $user_id, $description, $label,$source, $date)
  {
    $sth = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, source, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
    $adb->pquery($sth, array($entity_id, $user_id, $user_id, $setype, $description, $label,$source,$date, $date));
  }

  public function update_entity($adb, $entity_id, $date)
  {
    $sth = 'UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?';
    $adb->pquery($sth,array($date,$entity_id));
  }
  

  public function insert_senotesrel($conn, $entity_id, $moduleId)
  {
    $sth = $conn->query("INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES ('$moduleId', '$entity_id')");
  }

  public function insertAttachments($conn, $entity_id,$title,$filename,$recordid)
  {
    $sth = $conn->query("INSERT INTO vtiger_attachments (attachmentsid, name, path,related_id)  VALUES ('$entity_id', '$title', '$filename','$recordid')");
  }

  public function insertSeAttachmentsrel($conn, $entity_id,$moduleId)
  {
    $sth = $conn->query("INSERT INTO vtiger_seattachmentsrel (crmid, attachmentsid)  VALUES ('$entity_id', '$moduleId')");
  }

  public function insertNotes($conn, $entity_id,$title,$filename,$folderid,$locationType,$filestatus)
  {
    $sth = $conn->query("INSERT INTO vtiger_notes (notesid, title, filename, folderid, filelocationtype, filestatus) 
                            VALUES ('$entity_id', '$title', '$filename', '$folderid','$locationType','$filestatus')");
  }

  public function insert_NoteCf($conn, $entity_id)
  {
    $sth = $conn->query("INSERT INTO vtiger_notescf (notesid) VALUES ('$entity_id')");
  }

  public function lt_chars($text) {   
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

    return $text; 
  }

  public function weekOfMonth($date) 
  {  
  $firstOfMonth = strtotime(date("Y-m-01", $date));
  return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
  }

  function renameFile($location,$filename,$entity_id)
  { 
    global $root_directory;
    $oldName = $root_directory.'/'.$location.$filename;
    $newName = $root_directory.'/'.$location.$entity_id.'_'.$filename;
    if(rename("$oldName","$newName")){
      return true;
    }
  
  }


}