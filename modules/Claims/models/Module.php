<?php



class Claims_Module_Model extends Vtiger_Module_Model {

  public function isSummaryViewSupported() {
    return false;
  }

  public function getAttachments($array){  
    $db = PearDatabase::getInstance();

    $sql = "SELECT n.notesid, r.attachmentsid,filename, path
            FROM vtiger_notes n
            LEFT JOIN vtiger_senotesrel s ON s.notesid=n.notesid
            LEFT JOIN vtiger_seattachmentsrel r ON r.crmid=n.notesid
            LEFT JOIN vtiger_attachments a ON a.attachmentsid=r.attachmentsid
            WHERE a.attachmentsid = ?";

    $html = '';
    $allowFormats = array('jpg','jpeg','png','gif');	
		$url = sprintf("%s://%s%s",isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',$_SERVER['SERVER_NAME'],'');

    foreach($array AS $attachmentid){
      $result = $db->pquery($sql,array($attachmentid)); 
      $noteid = $db->query_result($result,0,'notesid');
      $path = $db->query_result($result,0,'path');
      $fileName = $db->query_result($result,0,'filename');
     
      $ext = pathinfo($path.'/'.$fileName, PATHINFO_EXTENSION);
      if( in_array(strtolower($ext), $allowFormats)){
        $html .= '<a data-fancybox href="'.$url.'/'.$path.$attachmentid.'_'.$fileName.'"><i class="fa fa-paperclip initFancyBox" style="font-size: 17px;"></i></a>';
      }else{
        $html .= '<a name="downloadfile" href="index.php?module=Documents&action=DownloadFile&record='.$noteid.'&fileid='.$attachmentid.'" onclick="event.stopImmediatePropagation();"><i class="fa fa-paperclip initFancyBox" style="font-size: 17px;"></i></a>';
      }
    }


    // $attachments        <a name="downloadfile" href="index.php?module=Documents&action=DownloadFile&record={$number[0]}&fileid={$number[1]}"><i class="fa fa-paperclip initFancyBox" style="font-size: 17px;"></i></a>



    return $html;
  }

  public function checkAreSelectedWhoIsResponsible($recordID){  
    $db = PearDatabase::getInstance();

    $sql = "SELECT IF(cf_2604 = 'Darbuotojas', 1,0) AS response FROM vtiger_claimscf WHERE claimsid = ?";
    $result = $db->pquery($sql,array($recordID));
    return $db->query_result($result,0,'response');
  }

  public function checkAreSelectedaActionAfertVise($recordID){  
    $db = PearDatabase::getInstance();

    // $sql = "SELECT IF(cf_2657 != '' AND cf_2657 IS NOT NULL, 1,0) AS response FROM vtiger_claimscf WHERE claimsid = ?";
    // $result = $db->pquery($sql,array($recordID));
    // return $db->query_result($result,0,'response');
    return true;
  }

  public function checkAreActionIsPurchase($recordID){  
    $db = PearDatabase::getInstance();

    // $sql = "SELECT IF((cf_2661 != '' AND cf_2661 IS NOT NULL) AND cf_2657 != 'Taikoma nuolaida', 1,0) AS response FROM vtiger_claimscf WHERE claimsid = ?";
    // $result = $db->pquery($sql,array($recordID));
    // return $db->query_result($result,0,'response');
    return true;
  }

  public function checkAreClaimIsRejected($recordID){  
    $db = PearDatabase::getInstance();
    $sql = "SELECT cf_1856 FROM vtiger_claimscf WHERE claimsid = ?";
    $result = $db->pquery($sql,array($recordID));
    return $db->query_result($result,0,'cf_1856');
  }

  public function checkClaimStatus($recordID){  
    $db = PearDatabase::getInstance();
    $sql = "SELECT cf_1837 FROM vtiger_claimscf WHERE claimsid = ?";
    $result = $db->pquery($sql,array($recordID));
    return $db->query_result($result,0,'cf_1837');
  }


  public function getClaims(){
    $db = PearDatabase::getInstance();
 
      $sql = "SELECT c.claimsid, createdtime, cf_1837 as status, accountname, ROUND(claims_tks_claim_amount) AS total, ROUND(REPLACE(claims_tks_claim_amount,',','.'),2) AS claims_tks_claim_amount , end_claim, cf.cf_1854 as section, cf_1854id,cf_1856, 
    IF(cf_1845 > 0, REPLACE(ROUND(cf_1845,2),',','.'),0) AS payout, REPLACE(ROUND(cf_2129,2),',','.') AS insurance ,cf_2062 AS causer
                FROM vtiger_claims c
                LEFT JOIN vtiger_claimscf cf ON c.claimsid=cf.claimsid
                LEFT JOIN vtiger_crmentity e ON e.crmid=c.claimsid
                LEFT JOIN vtiger_account a ON a.accountid=c.cf_20023
                LEFT JOIN vtiger_cf_1854 ON vtiger_cf_1854.cf_1854=cf.cf_1854
                WHERE deleted = 0 
                GROUP BY claimsid
                ORDER BY createdtime DESC";


      $sql_get_all_emploees = "SELECT CONCAT(TRIM(firstname),' ',TRIM(lastname)) AS employee,employid FROM vtiger_hrm_employee INNER JOIN vtiger_crmentity ON crmid=employid WHERE deleted = 0";
      $all_employees_result = $db->pquery($sql_get_all_emploees, array());

      $all_employees = array();



      foreach ($all_employees_result as  $val) {
        $all_employees[html_entity_decode(trim($val['employee']))] = $val['employid'];
      }

          
    $result = $db->pquery($sql,array());   		
    $noOfRows = $db->num_rows($result);

    $claims = array();
    $main_sum = array();
    $payout = array();
    $insurance = array();
    $accepted = array();
    $rejected = array();
    $sections_arr = array();

    for($i=0; $i<$noOfRows; $i++) {
      $row = $db->query_result_rowdata($result, $i);
      $claims['sections'][$row['section']]['count'][] = 1;	
      $claims['sections'][$row['section']]['total'] += $row['total'];
      $claims['sections'][$row['section']]['payout'] += $row['payout'];
      $claims['content'][] = $row;
      if(!empty($row['causer'])){
        $name =  explode("|##|",$row['causer']);
        for($e=0; $e < count($name); $e++){
          $trimed_name = html_entity_decode(trim($name[$e]));    
          $claims['couser'][$trimed_name]['total'] += $row['total'];
          $claims['couser'][$trimed_name]['payout'] += $row['payout'];
          $claims['couser'][$trimed_name]['qty'] += 1;
          $claims['couser'][$trimed_name]['employid'] = (!empty($all_employees[$trimed_name]) ? $all_employees[$trimed_name] : 0);
        }
      }

      if(!empty($row['claims_tks_claim_amount']) && $row['claims_tks_claim_amount'] > 0){		
        $main_sum[] = $row['claims_tks_claim_amount'];
      }
      $payout[] = $row['payout'];
      $insurance[] = $row['insurance'];
      if($row['cf_1856'] == 1){		
        $rejected[] = 1;
      }else{
        $accepted[] = 1;
      }
    }   

  
    $claims['count'] = $noOfRows;
    $claims['SUM'] = array_sum($main_sum);
    $claims['payout'] = array_sum($payout);
    $claims['insurance'] = array_sum($insurance);
    $claims['accepted'] = COUNT($accepted);
    $claims['rejected'] = COUNT($rejected);

    foreach($claims['sections'] AS $key => $row){
      $section = ($key ?: 'Nepriskirta');
      $sections_arr[$section] = array('section' => $section, 'count' => array_sum($row['count']), 'total' => $row['total'], 'payout' => $row['payout']);
    }

    $claims['section'] = $sections_arr;

    return $claims;
  }

  public function getClaimVise(){
    $db = PearDatabase::getInstance();
    global $current_user;

    $query = "SELECT id, SUBSTRING_INDEX(`rolename`, ' ', 1) as role 
                FROM vtiger_users u
                LEFT JOIN vtiger_user2role ur ON ur.userid=u.id
                LEFT JOIN vtiger_role r ON r.roleid=ur.roleid
                WHERE (rolename LIKE '%vadovas%' OR rolename LIKE '%direktorius') AND id != 59";

    $managers_result = $db->pquery($query,array());
    $managers = array();

    foreach($managers_result as $row){
      $managers['id'][] = $row['id'];
      $managers['role'][$row['role']] = $row['id'];
    }


    $sql = "SELECT c.claimsid,createdtime, DATEDIFF(CURDATE(), createdtime) AS claim_exist, accountname,shipment_code,claims_tks_claim_amount , cf_2062 AS causer,ceo, cf_1854 as section, cf_1856 AS confirm,claimsno, cf.cf_1854 as section,cf_1837 as status,cf_1843 AS claim_decision, cf_2129 AS insurance_price,cf_1845 AS payout
                FROM vtiger_claims c
                LEFT JOIN vtiger_claimscf cf ON c.claimsid=cf.claimsid
                LEFT JOIN vtiger_crmentity e ON e.crmid=c.claimsid
                LEFT JOIN vtiger_claims_vise v ON c.claimsid=v.claim_id
                LEFT JOIN vtiger_account a ON a.accountid=c.cf_20023
                LEFT JOIN vtiger_salesorder s ON s.salesorderid=c.claims_tks_order_number
                LEFT JOIN vtiger_users u ON u.id=e.smcreatorid
                WHERE e.deleted = 0 AND cf_2443 = 1 AND IF(cf_1856 = 0, (ceo = 0 OR claim_id IS NULL), (cf_1856 = 0 OR claim_id IS NULL))
                GROUP BY claimsid
                ORDER BY createdtime DESC";


    $result = array();

    $rez = $db->pquery($sql,array());
    $noOfRows = $db->num_rows($rez);

    foreach($rez as $row){
      $result['content'][] = $row;     
    }

    $result['count'] = $noOfRows;
    $result['user'] = $current_user->id;
    $result['managers'] = $managers['id'];
    $result['managers_role'] = $managers['role'];

    return $result;   
  }

  public function getClaimsQuality($dateFilter=''){
    $db = PearDatabase::getInstance();
    $month_back = date('Y-m-d', strtotime('-30 days'));
		$today = date("Y-m-d");
    global $current_user;

    if(!empty($dateFilter)) {			
			$params[] = $dateFilter['start'];
      $params[] = $dateFilter['end'];     
		}else{     
      $params[] = $month_back; 
      $params[] = $today; 
    }


    $get_closed_claims_query = "SELECT DATE_FORMAT(createdtime,'%Y-%m-%d') AS created, COUNT(cf_1837) AS closed 
                       FROM vtiger_claimscf INNER JOIN vtiger_crmentity ON crmid=claimsid 
                       WHERE (DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN ? AND ?) AND cf_1837 = 'Uždaryta' AND setype = 'Claims' AND deleted = 0 
                       GROUP BY created";

    $get_closed_claims = $db->pquery($get_closed_claims_query,array($params[0],$params[1]));    
    $closed_array = [];

    foreach ($get_closed_claims as $row) {
      $closed_array[$row['created']] = $row['closed'];
    }

    $get_claims_couser_query = "SELECT DATE_FORMAT(createdtime,'%Y-%m-%d') AS created, GROUP_CONCAT(cf_2062 SEPARATOR ' | ') AS senders 
                          FROM vtiger_claimscf 
                          INNER JOIN vtiger_crmentity ON crmid=claimsid                       
                          WHERE (DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN ? AND ?) AND cf_2062 IS NOT NULL AND setype = 'Claims' AND deleted = 0 
                          GROUP BY created";

    $get_claims_couser = $db->pquery($get_claims_couser_query,array($params[0],$params[1]));  
    $claims_couser_array = [];

    foreach ($get_claims_couser as $row) {
      $claims_couser_array[$row['created']] = $row['senders'];
    }


    $get_claims_sender_query = "SELECT DATE_FORMAT(createdtime,'%Y-%m-%d') AS created, GROUP_CONCAT(accountname SEPARATOR ' | ') AS senders 
                          FROM vtiger_claims INNER JOIN vtiger_crmentity ON crmid=claimsid 
                          JOIN vtiger_account ON accountid=vtiger_claims.cf_20023
                          WHERE (DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN ? AND ?)  AND setype = 'Claims' AND deleted = 0 
                          GROUP BY created";

    $get_claims_sender = $db->pquery($get_claims_sender_query,array($params[0],$params[1]));  
    $sender_array = [];

    foreach ($get_claims_sender as $row) {
      $sender_array[$row['created']] = $row['senders'];
    }


    $get_claim_amounts_query = "SELECT DATE_FORMAT(createdtime,'%Y-%m-%d') AS created, GROUP_CONCAT(CONCAT(ROUND(claims_tks_claim_amount,2),' / ',ROUND(cf_1845,2)) SEPARATOR ' | ') AS amount 
                          FROM vtiger_claims
                          LEFT JOIN vtiger_claimscf ON vtiger_claimscf.claimsid=vtiger_claims.claimsid
                          INNER JOIN vtiger_crmentity ON crmid=vtiger_claims.claimsid                       
                          WHERE (DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN ? AND ?) AND (claims_tks_claim_amount IS NOT NULL AND cf_1845 IS NOT NULL) AND setype = 'Claims' AND deleted = 0 
                          GROUP BY created";

    $get_claim_amounts = $db->pquery($get_claim_amounts_query,array($params[0],$params[1]));  
    $claim_amounts_array = [];

    foreach ($get_claim_amounts as $row) {
      $claim_amounts_array[$row['created']] = $row['amount'];
    }    
    
    
    
    $final_array = [];
    $period = $this->getDatesFromRange($params[0], $params[1]); 

		foreach ($period as $value) {	
      $date = $value->format('Y-m-d');	
      if(!empty($closed_array[$date]) || !empty($claims_couser_array[$date]) || !empty($sender_array[$date]) || !empty($claim_amounts_array[$date])){
        $final_array[$date] = ['closed' => ($closed_array[$date] ?: '--'),
                               'cousers' => ($claims_couser_array[$date] ?: '--'),
                               'senders' => ($sender_array[$date] ?: '--'),
                               'amounts' => ($claim_amounts_array[$date] ?: '--')
                              ];
      }
    }  

    
 
    return array('content' => $final_array, 'role' => $current_user->roleid);     
  }

  function getDatesFromRange($start, $end) { 
    $array = array();  
    $interval = new DateInterval('P1D');  
    $realEnd = new DateTime($end); 
    $realEnd->add($interval);   
    $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
    foreach($period as $date) {                  
        $array[] = $date;  
    }  
    return $period; 
	} 

  static function getSignatareInfo(){
    $db = PearDatabase::getInstance();
    $signatare_phones = $db->pquery("SELECT * FROM vtiger_claims_email_signatare_phones ORDER BY id DESC LIMIT 1",[]);
    $t_phone = $db->query_result($signatare_phones, 0, 't_phone');
    $f_phone = $db->query_result($signatare_phones, 0, 'f_phone');
    $m_phone = $db->query_result($signatare_phones, 0, 'm_phone');
    $email = $db->query_result($signatare_phones, 0, 'email');

    return [
      't_phone' => $t_phone,
      'f_phone' => $f_phone,
      'm_phone' => $m_phone,
      'email' => $email,
    ];
  }

  static function insertSignatareInfo($req){
    $db = PearDatabase::getInstance();
    global $current_user; 

    $params = [
      $req->get('t_phone'), 
      $req->get('f_phone'), 
      $req->get('m_phone'), 
      $req->get('email'), 
      $current_user->id, 
      date("Y-m-d H:i:s")
    ];

    $db->pquery("INSERT INTO vtiger_claims_email_signatare_phones (t_phone, f_phone, m_phone, email, owner_id, created_date) VALUES (?,?,?,?,?,?)", $params);    
  }

}