<?php


Class Arriving_Edit_View extends Vtiger_Edit_View {

	public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$record = $request->get('record');

		$actionName = 'CreateView';
		if ($record && !$request->get('isDuplicate')) {
			$actionName = 'EditView';
		}

		if(!Users_Privileges_Model::isPermitted($moduleName, $actionName, $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if ($record) {
			$recordEntityName = getSalesEntityType($record);
			if ($recordEntityName !== $moduleName) {
				throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
			}
		}
	}

	public function process(Vtiger_Request $request) {
		$viewer = $this->getViewer($request); 
		$record = $request->get('record');		
		$moduleName = $request->getModule(); 
		$measures = $this->getMeasures();
		$warehouses = $this->getWarehouses();
		$viewer->assign('MEASURES', $measures);
		$viewer->assign('WAREHOUSES', $warehouses);
		$viewer->view('EditView.tpl', $moduleName);
	}

	public function getMeasures(){
		$db = PearDatabase::getInstance();
		$sql = "SELECT * FROM app_measures ORDER BY id";
		$result = $db->pquery($sql,array());

		$measures = array();
		foreach($result AS $row){
			$measures[] = $row;
		}

		return $measures;
	}

	public function getWarehouses(){
		$select = 'WAREHOUSE_NME,METRIKA_ID,WAREHOUSE_ID';
		$table = 'W_WAREHOUSES'; 
		$where = 'IS_ACTIVE = 1';

		$headers = array(
			'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
			);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => $select, 'table' => $table, 'where' => $where));				
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);

		return $data;
	}

}