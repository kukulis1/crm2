<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Arriving_Record_Model extends Vtiger_Record_Model {

	function getMoreInfoFromStorage($recordId){				
		// $select = "municipality,street_address,post_code,phone,shipment_code,customer_name,tare_type_name,unload_date,plan_pkg,sender_name,consignee_name,consignee_country,consignee_address,consignee_city,consignee_post_code,consignee_contact,consignee_phone,m_load_id, customer_address,customer_city,customer_post_code,sender_name,sender_address,sender_city,sender_post_code,sender_country,sender_contact,sender_phone,plan_pkg,plan_wgt,tare_type_name";
		// $table = 'storage_arriving 
		// 					LEFT JOIN tare_type ON tare_type.tare_type_id=storage_arriving.tare_type_id
		// 					LEFT JOIN location ON location.location_id=storage_arriving.warehouse_id ';

		$select = '*';
		$table = 'STORAGE_UNORDERED_View'; 
		$where = "SHIPMENT_ID=$recordId";

		$select2 = '*';
		$table2 = 'STORAGE_UNORDERED_GOODS_View';		
		$where2 = "SHIPMENT_ID=$recordId";


		$headers = array(
			'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
			);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => $select, 'table' => $table, 'where' => $where));				
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);
	
		$shipment_info = $data[0];

		$ch2 = curl_init();
		curl_setopt($ch2, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
		curl_setopt($ch2, CURLOPT_POST, 1);
		curl_setopt($ch2, CURLOPT_POSTFIELDS, array('select' => $select2, 'table' => $table2, 'where' => $where2));				
		curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch2, CURLOPT_HEADER, 0);
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch2, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 1);
		$result2 = curl_exec($ch2);
		curl_close($ch2);
		$goods = json_decode($result2, true);

		return array('shipment' => $shipment_info, 'goods' => $goods);
	}


}
