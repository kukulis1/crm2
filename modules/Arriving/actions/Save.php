<?php

/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
class Arriving_Save_Action extends Vtiger_Action_Controller {	


	public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$record = $request->get('record');

		$actionName = ($record) ? 'EditView' : 'CreateView';
		if(!Users_Privileges_Model::isPermitted($moduleName, $actionName, $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if(!Users_Privileges_Model::isPermitted($moduleName, 'Save', $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if ($record) {
			$recordEntityName = getSalesEntityType($record);
			if ($recordEntityName !== $moduleName) {
				throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
			}
		}
	}

	public function process(Vtiger_Request $request) {
		$db = PearDatabase::getInstance();

		$sql = "SELECT accountname,customer_id,phone, bill_country,bill_city,bill_code,bill_street 
						FROM vtiger_account a
						JOIN vtiger_accountbillads b ON b.accountaddressid=a.accountid
				 		WHERE a.accountid = ?";

			$grand_package_array = array();
			$package_array = array();
			$goods_array = array();
		
			$groups_count = $_POST['groups_count'];
		  $qty_type = $_POST['qty_type'];

			$cargo = array();
			$cargo_weight = array();
			$length = array();
			$width = array();
			$height = array();

			
			for ($i=1; $i <= $groups_count; $i++) { 
				$group_tares_count = $_POST["group_tares$i"];
				$group_goods_count = $_POST["group_goods$i"]; 
			
				$package_array = array();
				$goods_array = array();
				for ($g=1; $g <= $group_goods_count; $g++) { 
					$qty = ($qty_type == 2 ?  $_POST['group_'.$i.'goods_qty'.$g]/$_POST['group_'.$i.'qty1'] : $_POST['group_'.$i.'goods_qty'.$g]);
					$goods_array[] = array('GoodId' => (int)$_POST['group_'.$i.'good'.$g],'Qty' => (int)$qty);				
				}

				$cargo[$i] = array('tare' => (int)$_POST['group_'.$i.'package1'], 'qty' => (int)$_POST['group_'.$i.'qty1']);
				$cargo_weight[$i] =  $_POST['group_'.$i.'weight'];
				$length[$i] = $_POST['group_'.$i.'length'];
				$width[$i] = $_POST['group_'.$i.'width'];
				$height[$i] = $_POST['group_'.$i.'height'];	

		
				for ($p=$group_tares_count; $p > 0; $p--) {   	
					$package_info_array = array();  
					$package_info_array['TareTypeId'] = (int)$_POST['group_'.$i.'package'.$p];
					$package_info_array['Qty'] = (int)$_POST['group_'.$i.'qty'.$p];	

					if($p == 1){
						$package_info_array['PackingWeightKg'] = (float)$_POST['group_'.$i.'weight'];
						$package_info_array['PackingLenghtM'] = (float)$_POST['group_'.$i.'length'];
						$package_info_array['PackingWidthM'] = (float)$_POST['group_'.$i.'width'];
						$package_info_array['PackingHeightM'] = (float)$_POST['group_'.$i.'height'];
					}

					$package_info_array['PackingCode'] = "".$_POST['group_'.$i.'barcode'.$p]."";
					$package_info_array['Goods'] = ($p == $group_tares_count ? $goods_array : array());					
		
					if(!empty($package_array)){
						$package_info_array =  array_merge($package_info_array,$package_array);
					}
					$package_array = array();
					$package_array["Packing$p"] = $package_info_array;    
				}
				$grand_package_array[] = $package_array;
			}

			$result = $db->pquery($sql,array($_POST['account_id']));
			$account_id = $db->query_result($result,0,'customer_id');

		if($_POST['REQUIRED_TRANSPORT'] == 'on'){
			$sender_name = $_POST['SENDER_NAME'];
			$sender_address = $_POST['SENDER_ADDRESS'];
			$sender_city = $_POST['SENDER_CITY'];
			$sender_post_code = $_POST['SENDER_POST_CODE'];
			$sender_country = $_POST['SENDER_COUNTRY'];
			$sender_phone = $_POST['SENDER_PHONE'];		
			$load_date = $_POST['LOAD_DATE'];
		}else{ 
			$sender_name = ($db->query_result($result,0,'accountname') ?: '');
			$sender_address = ($db->query_result($result,0,'bill_street') ?: '');
			$sender_city = ($db->query_result($result,0,'bill_city') ?: '');
			$sender_post_code = ($db->query_result($result,0,'bill_code') ?: '');
			$sender_country = ($db->query_result($result,0,'bill_country') ?: '');
			$sender_phone = ($db->query_result($result,0,'phone') ?: '');
			$load_date = date("Y-m-d");
		}

		if($_POST['generateManifest'] == 'on'){
			$manifest_number = time();
		}else{
			$manifest_number = $_POST['MANIFEST_NO'];
		}

		$required = ($_POST['REQUIRED_TRANSPORT'] == 'on' ? 1:0);
		
		if($required){
			$shipment_code = $this->createSalesOrders($load_date,$_POST['UNLOAD_DATE'],$_POST['account_id'],$cargo,$cargo_weight,$sender_name,$sender_address,$sender_city,$sender_post_code,$sender_country,$sender_phone,$_POST['WAREHOUSE_TEXT'],$length,$width,$height);			
		}else{
			$shipment_code = $_POST['CARGO_NO'];
		}

		if(empty($shipment_code)) $shipment_code = '';

		

		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'charset: utf-8';		

		$content = array ( 
			array (
				'CustomerId' => (int)$account_id,
				'ManifestNumber' => "$manifest_number",
				'ManifestDate' => $_POST['MANIFEST_DATE'],
				'WarehouseId' => (int)$_POST['WAREHOUSE_NME'],
				'ArrivingDate' => $_POST['UNLOAD_DATE'],
				'Comments' => $_POST['NOTE'],
				'VehicleNum' => $_POST['TRANSPORT_NO'],
				'DriverNme' => $_POST['DRIVER'],
				'CarrierNme' => $_POST['TRANSPORT'],
				'TransportRequired' => (int)$required,
				'ArrivingCargos' => 
				array (	 
					array (
						'CargoNumber' => $shipment_code,
						'SenderName' => $sender_name,
						'SenderAddress' => $sender_address,
						'SenderCity' => $sender_city,
						'SenderPostCode' => $sender_post_code,
						'SenderCountry' => $sender_country,
						'SenderPhone' => $sender_phone,
						'ArrivingCargoGoods' => $grand_package_array
					),
				),
			),
		);

		$content = json_encode($content);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/DemoWarehouseApi/WarehouseApi/crmCreateArriving"); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);      
		curl_setopt($ch, CURLOPT_USERPWD, "crmApiUser1:crmApiUser1*");         
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);	
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);

	
		header("Location:index.php?module=SalesOrder&view=List");
		
	}
	

	public function createSalesOrders($load_date,$unload_date,$accountid,$cargo,$cargo_weight,$sender_name,$sender_address,$sender_city,$sender_post_code,$sender_country,$sender_phone,$warehouse,$length,$width,$height){
			$db = PearDatabase::getInstance();		
		
			global $current_user;		
			$date = date("Y-m-d H:i:s");   
			$receiver = explode(',',$warehouse);  

			$stmt = "INSERT INTO vtiger_salesorder (salesorderid,subject,total,subtotal,accountid,sostatus,load_date_from,load_time_from,load_date_to,load_time_to,unload_date_from,unload_time_from,unload_date_to,unload_time_to,source,import_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				

			$stmt2 = "INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,margin,cargo_measure,cargo_wgt,cargo_length,cargo_width,cargo_height,m3,pll,source,service) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			$stmt3 = "INSERT INTO vtiger_sobillads (sobilladdressid,bill_city,bill_code,bill_country,bill_street,load_company,load_contact,load_phone) VALUES (?,?,?,?,?,?,?,?)";
			$stmt4 = "INSERT INTO vtiger_soshipads (soshipaddressid,ship_city,ship_code,ship_country,ship_street,unload_company,unload_contact,unload_phone) VALUES (?,?,?,?,?,?,?,?)";
			$stmt5 = "INSERT INTO vtiger_salesordercf (salesorderid,cf_855, cf_1297,cf_1374, cf_1661,cf_1663, cf_1564,cf_1566) VALUES (?,?,?,?,?,?,?,?)";


			if($receiver[0] == 'Vilnius'){
				$ship_code = '02190';
			}elseif($receiver[0] == 'Kaunas'){
				$ship_code = '51119';
			}elseif($receiver[0] == 'Klaipėda'){
				$ship_code = 94101;
			}elseif($receiver[0] == 'Šiauliai'){
				$ship_code = 78147;
			}elseif($receiver[0] == 'Panevėžys'){
				$ship_code = 35115;
			}elseif($receiver[0] == 'SBA Trans'){
				$ship_code = 1082;
			}


			$last_entity_record = $this->getEntityId(); 
			$insert_entity = $this->insertEntity('SalesOrder', $last_entity_record, $current_user->id, NULL,'CRM', '', $date);

			$m3_arr = array();
			$m2_arr = array();

			for($i = 1; $i <= count($cargo_weight);$i++){
				$qty = $cargo[$i]['qty'];
				$tare = $cargo[$i]['tare'];
				$pll_arr[] = (in_array($tare,array(1,2,3,13)) ? $qty : 0);
				// $weight_arr[] = array_sum($cargo_weight[$i]);

				$m3_arr[] = $length[$i]*$width[$i]*$height[$i];
				$m2_arr[] = $length[$i]*$width[$i];
			}


			$m3 = array_sum($m3_arr);
			$m2 = array_sum($m2_arr);

			$total_pll = array_sum($pll_arr);
			$total_weight = array_sum($cargo_weight);		


			include $_SERVER['DOCUMENT_ROOT']."/config.inc.php";
			include $_SERVER['DOCUMENT_ROOT']."/v1/external_data/getPriceFromPriceBook.php";			
			
			$host = $dbconfig['db_server'];
			$dbname = $dbconfig['db_name'];
			$user = $dbconfig['db_username'];
			$pass = $dbconfig['db_password'];

			$db_host = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
			$db_host->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$db_host->exec("set names utf8");  

			$price = getPriceFromPriceBook($db_host, $sender_post_code,$ship_code,$accountid,$total_weight,$m3,$m2,$total_pll,0,'LTU','LTU','CLIENT'); 

			$db->pquery($stmt,array($last_entity_record,'Užsakymas',$price['price'],$price['price'],$accountid, 'Sent',$load_date,'08:00:00',$load_date,'17:00:00',$unload_date,'08:00:00',$unload_date,'17:00:00','CRM',$date));
			
			$db->pquery($stmt3,array($last_entity_record,$sender_city,$sender_post_code,$sender_country,$sender_address,$sender_name,'',$sender_phone));

			$db->pquery($stmt4,array($last_entity_record, $receiver[0],$ship_code,'LTU',$receiver[1],'Parnasas','',''));

			$db->pquery($stmt5,array($last_entity_record,'Sandėliavimo užsakymas',$price['combination'],$price['price'],$current_user->id,$current_user->id,'',''));
			
			$e = 1;
			for($i = 1; $i <= count($cargo_weight);$i++){		
				$tare = $cargo[$i]['tare'];
				$qty = $cargo[$i]['qty'];
				$pll = (in_array($tare,array(1,2,3,13)) ? $qty : 0);

				$db->pquery($stmt2,array($last_entity_record, 
																								14244,
																								$e,
																								$cargo[$i]['qty'],
																								$price['price'],																			                                            
																								$cargo[$i]['tare'],
																								($cargo_weight[$i] ?: 0),
																								($length[$i] ?: 0),
																								($width[$i] ?: 0),
																								($height[$i] ?: 0),
																								($m3 ?: 0),
																								$pll,
																								'CRM', 																		
																								2));
				$e++;
			}

		return	$this->exportSalesOrderToMetrika($last_entity_record);

	}

	function getEntityId(){
		$db = PearDatabase::getInstance();
    $sql = "SELECT id FROM `vtiger_crmentity_seq`";
    $sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
    $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
    $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
    $lock3 = 'UNLOCK TABLES';

    $db->pquery($lock, array()); 
      $get_id = $db->pquery($sql, array()); 
      $seq = $db->query_result($get_id,0,'id');    
    $db->pquery($lock3, array());    

    $new_seq = $seq + 1;   

    $db->pquery($lock2, array()); 
     $db->pquery($sql2, array($new_seq)); 
    $db->pquery($lock3, array()); 

    return $new_seq;
  }

	function insertEntity($setype, $entity_id, $user_id, $description,$source, $label, $date){
		$db = PearDatabase::getInstance();
    $sql = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)';
    $db->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description,$source, $label, $date, $date)); 
  }
	

	public function exportSalesOrderToMetrika($salesorderid){
			$adb = PearDatabase::getInstance();
			$order_sent = 'Sent';  
			$order_not_sent = 'Not Sent'; 
		
			$result_data = array('order' => array()); 
			
			$sql = "SELECT
													s.salesorderid as order_id,
													s.total as finish_price,
													sc.cf_855 as order_type,
													sc.cf_1374 as price,
													sc.cf_1376 as agreed_price,
													sc.cf_1564 as load_phone,
													sc.cf_1566 as unload_phone,                                
													s.sostatus AS order_status,   
													s.status,                     
													e.createdtime AS order_date,
													a.accountname AS customer_name,                
													a.account_no AS customer_code,
													a.customer_id,
													s.external_order_id,             
													s.ivaz_no,
													s.source,                                
													e.description AS notes,
													CONCAT(s.load_date_from, ' ', s.load_time_from) AS load_date_from,
													CONCAT(s.load_date_to, ' ', s.load_time_to) AS load_date_to, 
													CONCAT(s.unload_date_from, ' ', s.unload_time_from) AS unload_date_from,
													CONCAT(s.unload_date_to, ' ', s.unload_time_to) AS unload_date_to,                                
													l.load_company,
													l.bill_street AS load_address,
													l.bill_city AS load_municipality,
													l.bill_state AS load_region,
													l.bill_code AS load_zipcode,
													l.bill_country AS load_country,
													l.load_contact,
													h.unload_company,
													h.ship_street AS unload_address,
													h.ship_city AS unload_municipality,
													h.ship_state AS unload_region,
													h.ship_code AS unload_zipcode,
													h.ship_country AS unload_country,
													h.unload_contact,
													a.legal_vat_code AS unload_vat_code,
													i.productid,  
													i.external_load_id,                              
													i.quantity AS cargo_qty,
													i.cargo_measure,
													i.cargo_wgt,
													i.cargo_length,
													i.cargo_width,
													i.cargo_height,
													i.comment AS cargo_notes,
													r.serviceid,
													i.service,
													i.margin
															FROM vtiger_salesorder s
															JOIN vtiger_salesordercf sc ON sc.salesorderid = s.salesorderid
															JOIN vtiger_crmentity e ON e.crmid = s.salesorderid
															JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
															JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid                            
															JOIN vtiger_users u ON u.id = e.smownerid 
															JOIN vtiger_account a ON a.accountid = s.accountid 
															JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
															LEFT JOIN vtiger_products p ON p.productid = i.productid
															LEFT JOIN vtiger_service r ON r.serviceid = i.productid                                    
															WHERE e.deleted = 0 AND s.salesorderid = ?";

			$result = $adb->pquery($sql, array($salesorderid));

			$orders = array();
			
	while($row = $adb->fetch_array($result)) {
		$date = date("Y-m-d H:i:s");
		$salesorderid = intval($row['order_id']);

		$orders[$salesorderid]['customer_id'] = intval($row['customer_id']);

		if(empty($row['external_order_id'])) {
				$orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
				// $orders[$salesorderid]['order_id'] = ''; 
		} else {
				$orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
				$orders[$salesorderid]['order_id'] = $row['external_order_id']; 
		} 
		
		// if($row['order_type'] == 'Transporto užsakymas'){
				$shipment_type = 'parcel';
		// }else{
		// 		$shipment_type = 'movement';
		// }     
		

		$orders[$salesorderid]['ivaz_no'] = $row['ivaz_no'];  
		$orders[$salesorderid]['shipment_type'] = $shipment_type;
		$orders[$salesorderid]['notes'] = iconv(mb_detect_encoding($row['notes']), "UTF-8", $row['notes']);
		$orders[$salesorderid]['price'] = (float)$row['price'];
		$orders[$salesorderid]['price_agreed'] = (float)$row['agreed_price'];
		$orders[$salesorderid]['finish_price'] = (float)$row['finish_price'];
		
		$orders[$salesorderid]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'])); 

		if(!empty($row['load_date_to'])){       
				$orders[$salesorderid]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to']));   
		} else {
				$orders[$salesorderid]['load_date_to'] = '';      
		}  

		$load_company = preg_replace('/[^A-Za-z0-9 ]/', '', $row['load_company']);
		$load_company = str_replace("quot", "", $load_company);   


		if(!empty($row['customer_id'])) {
				if(empty($row['load_company'])){       
						$orders[$salesorderid]['load_company'] = $row['customer_name'];          
				} else {
						$orders[$salesorderid]['load_company'] = $load_company;
				}        
		} else {
						$orders[$salesorderid]['load_company'] = $row['customer_name']; 
		} 
		
		$load_address = str_replace('&Scaron;', 'Š', $row['load_address']);
		$load_address = str_replace('&scaron;', 'š', $row['load_address']);


		$orders[$salesorderid]['load_address'] = $load_address;
		$orders[$salesorderid]['load_settlement'] = '';
		// $orders[$salesorderid]['load_municipality'] = 'Vilnius';   
		$orders[$salesorderid]['load_municipality'] = $row['load_municipality'];   
		$orders[$salesorderid]['load_region'] = $row['load_region'];                      
		$orders[$salesorderid]['load_zipcode'] = $row['load_zipcode'];
		
				//if($row['load_zipcode'] == 'LTU') {
				//$orders[$salesorderid]['load_zipcode'] = ''; 
				//}
		
		$orders[$salesorderid]['load_country'] = 'LTU';  
		// $orders[$salesorderid]['load_country'] = $row['load_country'];  
		$orders[$salesorderid]['load_person'] = $row['load_contact'];  


		$orders[$salesorderid]['load_phone'] = $row['load_phone'];


		if(empty($row['load_company'])){       
				$orders[$salesorderid]['load_company_code'] = '';            
		} else {
				$orders[$salesorderid]['load_company_code'] = '';
		}    

		$orders[$salesorderid]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from']));

		if(!empty($row['unload_date_to'])){       
				$orders[$salesorderid]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to']));  
		} else {
				$orders[$salesorderid]['unload_date_to'] = '';      
		} 


		$unload_company = preg_replace('/[^A-Za-z0-9 ]/', '', $row['unload_company']); 

		$unload_company = str_replace("quot", "", $unload_company);         

		if(empty($row['unload_company'])){       
				$orders[$salesorderid]['unload_company'] = $row['customer_name'];          
		} else {
				$orders[$salesorderid]['unload_company'] = $unload_company;
		}

		$unload_address = str_replace('&Scaron;', 'Š', $row['unload_address']);
		$unload_address = str_replace('&scaron;', 'š', $row['unload_address']);

		$orders[$salesorderid]['unload_address'] = $unload_address; 
		$orders[$salesorderid]['unload_settlement'] = '';        
		$orders[$salesorderid]['unload_municipality'] = 'Kaunas';         
		// $orders[$salesorderid]['unload_municipality'] = $row['unload_municipality'];         
		$orders[$salesorderid]['unload_region'] = $row['unload_region'];                      
		$orders[$salesorderid]['unload_zipcode'] = $row['unload_zipcode']; 
		
				//if($row['unload_zipcode'] == 'LTU') {
				//$orders[$salesorderid]['unload_zipcode'] = ''; 
				//}            
		
		// $orders[$salesorderid]['unload_country'] = $row['unload_country']; 
		$orders[$salesorderid]['unload_country'] = 'LTU'; 
		$orders[$salesorderid]['unload_person'] = $row['unload_contact'];  ;  
		$orders[$salesorderid]['unload_phone'] = $row['unload_phone'];

		if(empty($row['unload_company'])){       
				$orders[$salesorderid]['unload_company_code'] = '';            
		} else {
				$orders[$salesorderid]['unload_company_code'] = '';
		}

		if(empty($row['unload_company'])){       
				$orders[$salesorderid]['unload_vat_code'] = '';            
		} else {
				$orders[$salesorderid]['unload_vat_code'] = '';
		}

		$orders_items = array(); 
		$orders_services = array(); 
		$delete_service = array(); 


		if($row['productid'] == 36641){
				$orders_services['service_type_id'] = $row['service'];
				$orders_services['quantity_agreed'] = $row['cargo_qty'];
				$orders_services['price_agreed'] = $row['margin'];

				$orders_services['remarks'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

		}

		

				$orders_items['cargo']['load_id'] = $row['external_load_id'];
				$orders_items['cargo']['update_type'] = 2;

		if($row['productid'] != 36641){
				$orders_items['cargo']['cargo_qty'] = ($row['cargo_qty'] < 1 ) ? intval(1.000) : intval($row['cargo_qty']);
				if($row['cargo_measure'] == 0){
					$orders_items['cargo']['cargo_measure'] = 1;
				}else{
					$orders_items['cargo']['cargo_measure'] = $row['cargo_measure'];
			}


				if($row['cargo_measure'] == null){
						$orders_items['cargo']['cargo_measure'] = 1;
				}

				if(($row['cargo_measure'] == 1 OR $row['cargo_measure'] == 2 OR $row['cargo_measure'] == 3) OR ($row['cargo_measure'] == 'pll' OR $row['cargo_measure'] == 'RUS-pll' OR $row['cargo_measure'] == 'FIN-pll')){
						$orders_items['cargo']['volume_unit2'] = intval($row['cargo_qty']);
				}else{
						$orders_items['cargo']['volume_unit2'] = 0;
				}
			
				// itoma
				$orders_items['cargo']['cargo_wgt'] = (float)str_replace(",",".", $row['cargo_wgt']);
				$orders_items['cargo']['cargo_length'] = (float)str_replace(",",".", $row['cargo_length']);      
				$orders_items['cargo']['cargo_width'] = (float)str_replace(",",".", $row['cargo_width']);      
				$orders_items['cargo']['cargo_height'] = (float)str_replace(",",".", $row['cargo_height']);   

				$orders_items['cargo']['cargo_ldm'] = sprintf('%0.2f', $row['cargo_ldm']);
				$orders_items['cargo']['cargo_facilities'] = '';        
				$orders_items['cargo']['cargo_termo'] = 0;  
				$orders_items['cargo']['cargo_return_documents'] = intval($row['cargo_return_documents']);
			
				$orders_items['cargo']['cargo_notes'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

				if($row['source'] == 'CRM') {
						if($row['status'] == 'ERROR' OR $row['status'] == ''){ 
								$orders_items['cargo']['update_type'] = 1;
								$insertType = 1; 
						}else{				
								$orders_items['cargo']['update_type'] = 1;  
								$insertType = 1;  								
						}
				}  
		}    

		
			if($row['serviceid'] == NULL) {  
					if($row['productid'] != 36641){    
							$orders[$salesorderid]['cargos'][] = $orders_items;        
					}

					if($row['productid'] == 36641){  
							$orders[$salesorderid]['additional_services'][] = $orders_services;  
					}          
			}    
														
				$orders_indexed = array_values($orders);

				$result_data['order'] = $orders_indexed;  
	}
						
		$res = json_encode($result_data, JSON_UNESCAPED_UNICODE); 

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders.php");
		// curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders.php");           
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders' => $res));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);         

		$data = json_decode($result, true);

		$state = $data[0];
								
			if($state == 'ERROR') {    
					$order_id = $data[0]['crm_id'];
					$errors = $data[1]['errors'][0]['error_message'];     
				
					//Atnaujiname uzsakymo busena           
					$sql = 'UPDATE vtiger_salesorder SET sostatus = ?, status = ?, errors = ? WHERE salesorderid = ?';
					$result = $adb->pquery($sql, array($order_not_sent, $state, $errors, $salesorderid));

					//Atnaujiname uzsakymo redagavimo data    
					$sql = 'UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?';
					$result = $adb->pquery($sql, array($date, $salesorderid));                     
					
			} else {    					
				$order_id = $data[0]['order']['crm_id'];
				$external_order_id = $data[0]['order']['order_id'];   
				$shipment_code = $data[0]['order']['shipment_code'];
				$ivaz_no = $data[0]['order']['ivaz_no'];
				$barcode = $data[0]['order']['barcode'];
				$direction = $data[0]['order']['direction']; 
				$status =  $data[0]['status']; 
				$errors = '';


									//Atnaujiname uzsakymo busena           
					if($insertType == 1){
							$sql = 'UPDATE vtiger_salesorder SET sostatus = ?, ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?';
							$result = $adb->pquery($sql, array($order_sent, $ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
					}else{
						$sql = 'UPDATE vtiger_salesorder SET ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?';
												$result = $adb->pquery($sql, array($ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
					}
						$sql2 = 'UPDATE vtiger_inventoryproductrel SET external_order_id = ?, external_load_id = ? WHERE id = ? AND sequence_no = ?';
												
						$seq = 1;
						foreach($data[0]['order']['loads'] as $load_id){                  
								$result = $adb->pquery($sql2, array($external_order_id, $load_id, $order_id, $seq ));
								$seq++;
					}								


					//Atnaujiname uzsakymo redagavimo data    
					$sql = 'UPDATE vtiger_crmentity SET label = ?, modifiedtime = ? WHERE crmid = ?';
					$result = $adb->pquery($sql, array($shipment_code, $date, $order_id));


			}

			return $shipment_code;

	} 
}
