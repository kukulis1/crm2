<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   
error_reporting(0);
if($_POST){

  try {
    $dbh->beginTransaction();  

    $sth = $dbh->prepare("SELECT bill_city,bill_street,bill_code,bill_country,customer_id
                          FROM vtiger_accountbillads
                          JOIN vtiger_account ON vtiger_account.accountid=vtiger_accountbillads.accountaddressid
                          WHERE accountaddressid = ?");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array($_POST['accountid'])); 
    $customer_info = $sth->fetch();  
    $customer_id = $customer_info['customer_id'];

    $select = "S_GOODS.ITEM_ID,
              S_GOODS.ITEM_NME,ITEM_CODE,
              S_GOODS.ITEM_WEIGHT,	
              S_GOODS.I_QTY_IN_PLL,
              MEASURE,
              W_PALLETS.PALLET";

    $from = "S_GOODS 
            JOIN VVS_MEASURES ON MEASURE_ID=S_GOODS.ITEM_MEASURE_ID
            JOIN W_PALLETS ON W_PALLETS.PALLET_ID=S_GOODS.I_PALLET_TYPE";

    $where = "ITEM_OWNER_ID = $customer_id AND S_GOODS.IS_ACTIVE = 1 ORDER BY ITEM_NME";


    $select2 =  "I_TOT_QTY,ITEM_ID,ITEM_NME,ITEM_CODE,I_PLL_QTY";

    $from2 = "STORAGE_RESTS_View";

    $where2 = "ITEM_OWNER_ID = $customer_id";



    $headers = array(
      'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => $select, 'table' => $from, 'where' => $where));				
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);  
    

    $rest_array = array();

    if($_POST['which'] == 'outgoing'){
      $ch2 = curl_init();
      curl_setopt($ch2, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");
      curl_setopt($ch2, CURLOPT_POST, 1);
      curl_setopt($ch2, CURLOPT_POSTFIELDS, array('select' => $select2, 'table' => $from2, 'where' => $where2));				
      curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch2, CURLOPT_HEADER, 0);
      curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch2, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 1);
      $result2 = curl_exec($ch2);
      curl_close($ch2); 
      
      $rest = array();   

      foreach (json_decode($result,true) as $row) {
        $rest[$row['ITEM_ID']] = $row;
      }

      foreach (json_decode($result2,true) as $row) {
        $rest_array[] = array('ITEM_ID' => $rest[$row['ITEM_ID']]['ITEM_ID'],
                              'ITEM_NME' => $row['ITEM_NME'],
                              'ITEM_CODE' => $row['ITEM_CODE'],                       
                              'ITEM_WEIGHT' => $rest[$row['ITEM_ID']]['ITEM_WEIGHT'],                       
                              'I_PLL_QTY' => $row['I_PLL_QTY'] ,
                              'I_QTY' => $row['I_TOT_QTY']                                
                              );
      }
    }

    echo json_encode(array('status' => 'success','data' => $customer_info,'goods' => $result, 'rests' => json_encode($rest_array)));

    $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'fail'));
  }

}else{
  http_response_code(404);
} 