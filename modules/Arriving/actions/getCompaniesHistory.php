<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

try {
  $dbh->beginTransaction();  

  $final = array();

  if($_REQUEST["type"] == 'get_load_companies_list') {
    $account_id = $_REQUEST["account_id"];
    $load_company = $_REQUEST["company"];
    
    $sth = $dbh->prepare("SELECT DISTINCT load_company,sobilladdressid
                    FROM vtiger_salesorder
                    LEFT JOIN vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid                     
                    WHERE vtiger_salesorder.accountid = ? AND load_company LIKE '%$load_company%'
                    GROUP BY load_company
                    ORDER BY vtiger_salesorder.salesorderid DESC");
                    
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array($account_id));       
    
    foreach($sth AS $res){                
      $temp['label'] = htmlspecialchars_decode(html_entity_decode($res["load_company"],  ENT_NOQUOTES, 'UTF-8'));
      $temp['value'] = $res['load_company'];                            
      $temp['code'] = $res['sobilladdressid'];                            
      $final[] = $temp;
    }   


  } elseif($_REQUEST["type"] == 'get_load_company_info') {

    $load_company_code = $_REQUEST["company_code"];     
        
    $sth = $dbh->prepare("SELECT DISTINCT load_company, bill_code,bill_city,bill_street, load_contact AS load_contact,bill_country,load_phone
                            FROM vtiger_sobillads 
                            WHERE sobilladdressid = ?");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array($load_company_code)); 

    foreach($sth AS $res){ 
      $temp['contact'] = htmlspecialchars_decode(html_entity_decode($res["load_contact"],  ENT_NOQUOTES, 'UTF-8'));  
      $temp['street'] = htmlspecialchars_decode(html_entity_decode($res["bill_street"],  ENT_NOQUOTES, 'UTF-8'));
      $temp['city'] = htmlspecialchars_decode(html_entity_decode($res["bill_city"],  ENT_NOQUOTES, 'UTF-8'));  
      $temp['post_code'] = $res['bill_code']; 
      $temp['country'] = $res['bill_country'];                           
      $temp['phone'] = $res['load_phone'];                           
      $final[] = $temp;
    }   


  }else if($_REQUEST["type"] == 'get_unload_companies_list') {
    $account_id = $_REQUEST["account_id"];
    $load_company = $_REQUEST["company"];
    
    $sth = $dbh->prepare("SELECT DISTINCT unload_company,soshipaddressid
                    FROM vtiger_salesorder
                    LEFT JOIN vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid                     
                    WHERE vtiger_salesorder.accountid = ? AND unload_company LIKE '%$load_company%'
                    GROUP BY unload_company
                    ORDER BY vtiger_salesorder.salesorderid DESC");
                    
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array($account_id));       
    
    foreach($sth AS $res){                
      $temp['label'] = htmlspecialchars_decode(html_entity_decode($res["unload_company"],  ENT_NOQUOTES, 'UTF-8'));
      $temp['value'] = $res['unload_company'];                            
      $temp['code'] = $res['soshipaddressid'];                            
      $final[] = $temp;
    }  
    
  } elseif($_REQUEST["type"] == 'get_unload_company_info') {

    $load_company_code = $_REQUEST["company_code"];     
        
    $sth = $dbh->prepare("SELECT DISTINCT unload_company, ship_code,ship_city,ship_street, unload_contact AS load_contact,ship_country,unload_phone
                            FROM vtiger_soshipads 
                            WHERE soshipaddressid = ?");

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute(array($load_company_code)); 

    foreach($sth AS $res){ 
      $temp['contact'] = htmlspecialchars_decode(html_entity_decode($res["unload_contact"],  ENT_NOQUOTES, 'UTF-8'));  
      $temp['street'] = htmlspecialchars_decode(html_entity_decode($res["ship_street"],  ENT_NOQUOTES, 'UTF-8'));
      $temp['city'] = htmlspecialchars_decode(html_entity_decode($res["ship_city"],  ENT_NOQUOTES, 'UTF-8'));  
      $temp['post_code'] = $res['ship_code']; 
      $temp['country'] = $res['ship_country'];                           
      $temp['phone'] = $res['unload_phone'];                           
      $final[] = $temp;
    }   

  }


  echo json_encode($final);

  $dbh->commit();
} catch (PDOException $e) {
  $dbh->rollBack();    
  echo json_encode(array('status' => 'fail'));
}