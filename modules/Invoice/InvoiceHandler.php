<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */
include_once 'include/Webservices/Revise.php';
include_once 'include/Webservices/Retrieve.php';

class InvoiceHandler extends VTEventHandler {

    function handleEvent($eventName, $entityData) {

        $moduleName = $entityData->getModuleName();

        // Validate the event target
        if ($moduleName != 'Invoice') {
            return;
        }

        //Get Current User Information
        global $current_user, $currentModule;

        /**
         * Adjust the balance amount against total & received amount
         * NOTE: beforesave the total amount will not be populated in event data.
         */
        if ($eventName == 'vtiger.entity.aftersave') {
            // Trigger from other module (due to indirect save) need to be ignored - to avoid inconsistency.
            if ($currentModule != 'Invoice')
                return;
            $entityDelta = new VTEntityDelta();
            $db = PearDatabase::getInstance();

            // NOTE ITOMA
            $update_query = "UPDATE vtiger_invoice SET ";
            $update_values = [];
            
            $getOldType = $entityDelta->getOldValue($entityData->getModuleName(), $entityData->getId(), 'cf_1277');
            $newType = $entityDelta->getCurrentValue($entityData->getModuleName(), $entityData->getId(), 'cf_1277');

            if ($getOldType != $newType && $getOldType != '') {
                $new_invoice_no = $this->getNewModuleSeqNumber($newType);
                $db->pquery("UPDATE vtiger_crmentity SET label = ? WHERE crmid = ?", array($new_invoice_no, $entityData->getId()));

                $update_query .= 'subject = ?, invoice_no = ?, ';
                $update_values[] = $new_invoice_no;
                $update_values[] = $new_invoice_no;
            }


            $oldCurrency = $entityDelta->getOldValue($entityData->getModuleName(), $entityData->getId(), 'currency_id');
            $newCurrency = $entityDelta->getCurrentValue($entityData->getModuleName(), $entityData->getId(), 'currency_id');
            $oldConversionRate = $entityDelta->getOldValue($entityData->getModuleName(), $entityData->getId(), 'conversion_rate');
           
            $wsid = vtws_getWebserviceEntityId('Invoice', $entityData->getId());
            $wsrecord = vtws_retrieve($wsid,$current_user);
            if ($oldCurrency != $newCurrency && $oldCurrency != '') {
              if($oldConversionRate != ''){ 
                $wsrecord['received'] = floatval(($wsrecord['received']/$oldConversionRate) * $wsrecord['conversion_rate']);
              }  
            }
            $wsrecord['balance'] = floatval($wsrecord['hdnGrandTotal'] - $wsrecord['received']);
            if ($wsrecord['balance'] == 0) {
                $wsrecord['invoicestatus'] = 'Paid';
            }

            $update_query .= 'balance = ?,received = ? WHERE invoiceid=?';
            $update_values[] = $wsrecord['balance'];
            $update_values[] = $wsrecord['received'];
            $update_values[] = $entityData->getId();
          
            $db->pquery($update_query,  $update_values);


            if($_SERVER['REMOTE_ADDR'] != '127.0.0.1'){

				$recordId = $entityData->getId();

				$salesorders_from_invoice = $db->pquery("SELECT vtiger_inventoryproductrel.order_id AS shipment_crm_id, vtiger_salesorder.external_order_id AS shipment_id, SUM(vtiger_inventoryproductrel.listprice) as invoice_price
				FROM vtiger_inventoryproductrel  
				LEFT JOIN vtiger_salesorder ON vtiger_salesorder.salesorderid=vtiger_inventoryproductrel.order_id 
				WHERE vtiger_inventoryproductrel.id = ? AND vtiger_inventoryproductrel.order_id > 0
				GROUP BY vtiger_inventoryproductrel.order_id", [$recordId]); 
				
				$get_additional_services = $db->pquery('SELECT order_id, amount FROM vtiger_inventoryproduct_splited_main_services WHERE invoiceid = ?', [$recordId]);
				
				$additional_services = $db->pluck($get_additional_services, 'amount', 'order_id');
				
				$price_to_send_in_metrika = [];
		
				foreach($salesorders_from_invoice AS $order_info){             
					
					$price_to_send_in_metrika[$order_info['shipment_id']] = [
						'shipment_crm_id' => $order_info['shipment_crm_id'],
						'shipment_id' => $order_info['shipment_id'],
						'invoice_price' => round($order_info['invoice_price'] + ($additional_services[$order_info['shipment_crm_id']] ?? 0), 2),
						'status' => null
					];
					
				}				
		
				$prices_json = json_encode($price_to_send_in_metrika);    
		
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/shipment_crm_price_and_status.php");           
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'shipment_data' => $prices_json));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				curl_exec($ch);
				curl_close($ch);
          	}
        }
    }

    function getNewModuleSeqNumber($type){
        $db = PearDatabase::getInstance();   
        $today = date('Y-m-d');
      
        $check_cur = $db->pquery("SELECT invoice_no FROM vtiger_invoice 
                                  LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid 
                                  LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                  WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = ? AND vtiger_invoicecf.cf_1277 = ? AND vtiger_crmentity.deleted = 0 
                                  ORDER BY vtiger_invoice.invoiceid  
                                  DESC LIMIT 1", array($today, $type));
      
        $curid = $db->query_result($check_cur, 0, 'invoice_no');
       
        if($type == 'Debetinė')	$prefix = 'PT';
        elseif($type == 'Kreditinė')  $prefix = 'KR'; 
        elseif($type == 'Išankstinė') $prefix = 'IS';

        if(!empty($curid)){
          $curid = substr($curid, -3);
        }else{
          $req_no = '001';
        }
      

        $todayDate = date('y/m/d');
        if(!empty($curid)){
          $strip = strlen($curid) - strlen($curid + 1);
          if ($strip < 0) $strip = 0;
          $temp = str_repeat("0", $strip);
          $req_no.= $temp . ($curid + 1);
        }
      
        $prev_inv_no = $prefix.$todayDate."/".$req_no;
        return $prev_inv_no;
    }

}

?>
