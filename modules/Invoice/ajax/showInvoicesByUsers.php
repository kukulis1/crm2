<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $invoiceid = $_POST['invoiceid'];

  $invoice = $conn->query("SELECT DISTINCT i.invoice_no, i.invoiceid, FORMAT(i.total,2) AS total, a.accountname, e.createdtime , invoicedate
                                      FROM vtiger_invoice i                                   
                                      LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid
                                      LEFT JOIN vtiger_account a ON a.accountid=i.accountid
                                      WHERE e.deleted = 0 AND e.setype = 'Invoice' AND i.invoiceid IN ($invoiceid) ");

  $invoice_array = array();

  foreach($invoice as $row){
    $invoice_array[] = $row;
  }

  echo json_encode($invoice_array);

    
}else{
  http_response_code(404);
}
  

