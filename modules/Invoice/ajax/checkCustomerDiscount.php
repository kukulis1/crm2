<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $accountid = $_POST['accountid'];

  $result = mysqli_fetch_assoc($conn->query("SELECT COUNT(customerdiscountid) AS num, ROUND(SUM(customerdiscount_tks_amount) - SUM(customerdiscount_tks_used),2) AS remaining_amount, GROUP_CONCAT(d.customerdiscountid) AS customerdiscountid,GROUP_CONCAT(customerdiscount_tks_claim_no) AS claim_no
                          FROM vtiger_crmentityrel r
                          LEFT JOIN vtiger_customerdiscount d ON d.customerdiscountid=r.crmid
                          INNER JOIN vtiger_crmentity e ON e.crmid=r.crmid
                          WHERE (r.relmodule = 'Accounts' AND r.module = 'Customerdiscount') AND r.relcrmid = $accountid  AND customerdiscount_tks_amount > customerdiscount_tks_used
                          ORDER BY e.createdtime ASC LIMIT 1"));                      

  echo json_encode(array('data' => $result));
    
}else{
  http_response_code(404);
}
  

