<?php


/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

include_once 'include/InventoryPDFController.php';

class Vtiger_InvoicePDFController extends Vtiger_InventoryPDFController{
	function buildHeaderModelTitle() {
		global $adb, $_REQUEST;
		$singularModuleNameKey = 'SINGLE_'.$this->moduleName;
		$translatedSingularModuleLabel = getTranslatedString($singularModuleNameKey, $this->moduleName);
		if($translatedSingularModuleLabel == $singularModuleNameKey) {
			$translatedSingularModuleLabel = getTranslatedString($this->moduleName, $this->moduleName);
		}
		$invoice_no = $this->focusColumnValue('invoice_no');

		if(empty($invoice_no)){
			$invoice_no = $_REQUEST['invoice_no'];
		}

		return sprintf("%s %s", '', $invoice_no);
	}

	// ITOMA
	function buildHeaderModelColumnCenter() {
		global $adb, $_REQUEST;
		$invoiceid = $_REQUEST['record'];
		$accountid = $this->focusColumnValue('account_id');

		if(empty($accountid)){
			$accountid = $_REQUEST['accountid'];
		}

		if(empty($invoiceid)){
			$invoiceid = $_REQUEST['invoiceid'];
		}

		$info = $adb->pquery("SELECT * FROM vtiger_account WHERE accountid = ?", array($accountid));

		$address = $adb->pquery("SELECT bill_city, bill_code, bill_street FROM vtiger_accountbillads WHERE accountaddressid = ?", array($accountid));
		
		$result2 = $adb->pquery("SELECT DATE_FORMAT(vtiger_invoice.invoicedate, '%Y-%m-%d') AS invoicedate ,vtiger_invoicecf.cf_1277 as invoice_type, IF(cf_2114 = 'LT', 'lt_lt', 'en_us') AS lang
																				FROM vtiger_invoice 
																				LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid
																				LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
																				WHERE vtiger_invoice.invoiceid = ?", array($invoiceid));

																		

		$info_row = $adb->fetch_array($info);
		$address_row = $adb->fetch_array($address);
		$resultrow2 = $adb->fetch_array($result2);
		$invoice_lang = $resultrow2['lang'];

		$customer_name =	getTranslatedString('LBL_CUSTOMER_NAME','Invoice',$invoice_lang);
		$vat_code =	getTranslatedString('LBL_VAT_CODE','Invoice',$invoice_lang);
		$bill_address =	getTranslatedString('LBL_BILL_ADDRESS','Invoice',$invoice_lang);
		$legal_entity_code_word =	getTranslatedString('LBL_LEGAL_ENTITY_CODE','Invoice',$invoice_lang);

		$customer_information = array();
		$customer_information2 = array();
		$customerName = $this->resolveReferenceLabel($accountid, 'Accounts');
		if(!empty($customerName)){
		$customerName1 = decode_html(' '.$customer_name);
		$customerName = decode_html(' '.$customerName);
		}


		if(!empty($info_row['legal_vat_code'])){
			$legal_vat_code1 = decode_html($vat_code);
			$legal_vat_code = $info_row['legal_vat_code'];
		}
		if(!empty($address_row['bill_street'])){
			$customer_address1 = decode_html($bill_address);
			$customer_address =  decode_html($address_row['bill_street']);
		}
		if(!empty($info_row['legal_entity_code'])){
			$legal_entity_code1 = decode_html($legal_entity_code_word);
			$legal_entity_code = $info_row['legal_entity_code'];
		}


		$customer_information2[] = $customerName1."\n";
		$customer_information[] = $customerName ."\n";
		if(!empty($legal_entity_code)){
			$customer_information2[] = $legal_entity_code1."\n";
			$customer_information[] = $legal_entity_code ."\n";
		}
		if(!empty($legal_vat_code)){
			$customer_information2[] = $legal_vat_code1."\n";
			$customer_information[] = $legal_vat_code ."\n";
		}
		if(!empty($customer_address)){
			$customer_information2[] = $customer_address1."\n";
			$customer_information[] = $customer_address.", ". decode_html($address_row['bill_city']).", ".$address_row['bill_code'] ."\n";
		}

		$modelColumnCenter = array(
			'crm_entity' => $resultrow2,
			'customer_information' => decode_html($this->joinValues($customer_information, ' ')),
			'customer_information2' => decode_html($this->joinValues($customer_information2, ' ')),
		
		);
		return $modelColumnCenter;
	}

	function buildHeaderModelColumnRight() {
		$issueDateLabel = getTranslatedString('Issued Date', $this->moduleName);
		$validDateLabel = getTranslatedString('Due Date', $this->moduleName);
		$billingAddressLabel = getTranslatedString('Billing Address', $this->moduleName);
		$shippingAddressLabel = getTranslatedString('Shipping Address', $this->moduleName);

		$modelColumnRight = array(
				'dates' => array(
					$issueDateLabel  => $this->formatDate(date("Y-m-d")),
					$validDateLabel => $this->formatDate($this->focusColumnValue('duedate')),
				),
				$billingAddressLabel  => $this->buildHeaderBillingAddress(),
				$shippingAddressLabel => $this->buildHeaderShippingAddress()
			);
		return $modelColumnRight;
	}

	// ITOMA
	function getWatermarkContent() {
		// return $this->focusColumnValue('invoicestatus');
	}
}
?>
