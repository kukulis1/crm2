<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Accounts_CheckPriceBook_Action extends Vtiger_Action_Controller {

	function checkPermission(Vtiger_Request $request) {
		return;
	}

	public function process(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$record = $request->get('record');

		if ($record) {
			$recordModel = Vtiger_Record_Model::getInstanceById($record, $moduleName);
		} else {
			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
		}	

		$pricebooks = $recordModel->get('pricebook');

		$sql = "SELECT  GROUP_CONCAT(accountid) AS accountid,accountname FROM vtiger_account
                                           LEFT JOIN vtiger_crmentity on vtiger_crmentity.crmid=accountid
                                           WHERE deleted =0 AND FIND_IN_SET('$pricebook', pricebook)";   

		if($pricebooks != '') {
			$pricebooks = explode(',', $pricebooks);
			$status = true;
			foreach($pricebooks AS $pricebook){
				if(!in_array($pricebook, [10965, 0])){
					if(!empty($pricebook)){
						$status = false;
						break;
					}
				}
			}
		
		} else {
			$status = true;
		}

		if (!$status) {
			$result = array('success'=> false, 'message'=> vtranslate('LBL_HAS_PRICEBOOK', $moduleName));
		} else {
			$result = array('success'=> true);
		}
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}
}
