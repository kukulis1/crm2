<?php

error_reporting(0);

require_once __DIR__ . "/../../../include/database/DatabaseWrapper.php";
require_once __DIR__ . "/../../../include/services/InvoicesStatisticsCalculator.php";

try {
    $invoiceStatisticsCalculator = new InvoicesStatisticsCalculator(DatabaseWrapper::getInstance());
    $now = new DateTime('now');
    $accountId = $_GET['accountid'];
    $invoiceStatisticsCalculator->calculateInvoiceStatistics($now, $accountId);


    $data = [
        'total_income' => sprintf("%0.2f", $invoiceStatisticsCalculator->getTotalIncome()),
        'average_income' => sprintf("%0.2f", $invoiceStatisticsCalculator->getAverageIncomePerMonth()),
        'orders_count' => $invoiceStatisticsCalculator->getOrdersCount(),
        'average_orders_count' => sprintf("%0.2f", $invoiceStatisticsCalculator->getAverageOrdersCount()),
        'this_month_income' => sprintf("%0.2f", $invoiceStatisticsCalculator->getThisMonthIncome()),
        'previous_month_income' => sprintf("%0.2f", $invoiceStatisticsCalculator->getPreviousMonthIncome()),
        'income_month_change' => sprintf("%0.2f", $invoiceStatisticsCalculator->getIncomeMonthChange()),
        'this_month_orders_count' => $invoiceStatisticsCalculator->getThisMonthOrdersCount(),
        'previous_month_orders_count' => $invoiceStatisticsCalculator->getPreviousMonthOrdersCount(),
        'orders_count_month_change' => $invoiceStatisticsCalculator->getOrdersCountMonthChange(),
    ];

    echo json_encode(['data' => $data]);
} catch (Throwable $e) {
    $error = [
        'message' => $e->getMessage()
    ];

    echo json_encode(['error' => $error]);
}
