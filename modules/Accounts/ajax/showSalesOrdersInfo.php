<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $salesorderid = $_POST['salesorderid'];

  $salesorder = $conn->query("SELECT DISTINCT s.shipment_code, s.salesorderid, FORMAT(s.total,2) AS total, ROUND(s.total,2) AS total_rounded, a.accountname, e.createdtime , cf_1376 as price_agreed
                                      FROM vtiger_salesorder s
                                      LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
                                      LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                      LEFT JOIN vtiger_account a ON s.accountid=a.accountid
                                      WHERE e.deleted = 0 AND e.setype = 'SalesOrder' AND s.salesorderid IN ($salesorderid)");

  $salesorders_array = array();

  foreach($salesorder as $row){
    $salesorders_array[] = $row;
  }

  echo json_encode($salesorders_array);

    
}else{
  http_response_code(404);
}
  

