<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ************************************************************************************/

require_once(__DIR__.'/../../../Helpers/Helper.php');

class Accounts_Module_Model extends Vtiger_Module_Model {

	/**
	 * Function to get the Quick Links for the module
	 * @param <Array> $linkParams
	 * @return <Array> List of Vtiger_Link_Model instances
	 */
	public function getSideBarLinks($linkParams) {
		$parentQuickLinks = parent::getSideBarLinks($linkParams);

		$quickLink = array(
			'linktype' => 'SIDEBARLINK',
			'linklabel' => 'LBL_DASHBOARD',
			'linkurl' => $this->getDashBoardUrl(),
			'linkicon' => '',
		);

		//Check profile permissions for Dashboards
		$moduleModel = Vtiger_Module_Model::getInstance('Dashboard');
		$userPrivilegesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$permission = $userPrivilegesModel->hasModulePermission($moduleModel->getId());
		if($permission) {
			$parentQuickLinks['SIDEBARLINK'][] = Vtiger_Link_Model::getInstanceFromValues($quickLink);
		}
		
		return $parentQuickLinks;
	}

	/**
	 * Function to get list view query for popup window
	 * @param <String> $sourceModule Parent module
	 * @param <String> $field parent fieldname
	 * @param <Integer> $record parent id
	 * @param <String> $listQuery
	 * @return <String> Listview Query
	 */
	public function getQueryByModuleField($sourceModule, $field, $record, $listQuery) {
		if (($sourceModule == 'Accounts' && $field == 'account_id' && $record)
				|| in_array($sourceModule, array('Campaigns', 'Products', 'Services', 'Emails'))) {

			if ($sourceModule === 'Campaigns') {
				$condition = " vtiger_account.accountid NOT IN (SELECT accountid FROM vtiger_campaignaccountrel WHERE campaignid = '$record')";
			} elseif ($sourceModule === 'Products') {
				$condition = " vtiger_account.accountid NOT IN (SELECT crmid FROM vtiger_seproductsrel WHERE productid = '$record')";
			} elseif ($sourceModule === 'Services') {
				$condition = " vtiger_account.accountid NOT IN (SELECT relcrmid FROM vtiger_crmentityrel WHERE crmid = '$record' UNION SELECT crmid FROM vtiger_crmentityrel WHERE relcrmid = '$record') ";
			} elseif ($sourceModule === 'Emails') {
				$condition = ' vtiger_account.emailoptout = 0';
			} else {
				$condition = " vtiger_account.accountid != '$record'";
			}

			$position = stripos($listQuery, 'where');
			if($position) {
				$split = preg_split('/where/i', $listQuery);
				$overRideQuery = $split[0] . ' WHERE ' . $split[1] . ' AND ' . $condition;
			} else {
				$overRideQuery = $listQuery. ' WHERE ' . $condition;
			}
			return $overRideQuery;
		}
	}

	/**
	 * Function to get relation query for particular module with function name
	 * @param <record> $recordId
	 * @param <String> $functionName
	 * @param Vtiger_Module_Model $relatedModule
	 * @return <String>
	 */
	public function getRelationQuery($recordId, $functionName, $relatedModule, $relationId) {
		if ($functionName === 'get_activities') {
			$focus = CRMEntity::getInstance($this->getName());
			$focus->id = $recordId;
			$entityIds = $focus->getRelatedContactsIds();
			$entityIds = implode(',', $entityIds);

			$userNameSql = getSqlForNameInDisplayFormat(array('first_name' => 'vtiger_users.first_name', 'last_name' => 'vtiger_users.last_name'), 'Users');

			$query = "SELECT CASE WHEN (vtiger_users.user_name not like '') THEN $userNameSql ELSE vtiger_groups.groupname END AS user_name,
						vtiger_crmentity.*, vtiger_activity.activitytype, vtiger_activity.subject, vtiger_activity.date_start, vtiger_activity.time_start,
						vtiger_activity.recurringtype, vtiger_activity.due_date, vtiger_activity.time_end, vtiger_activity.visibility, vtiger_seactivityrel.crmid AS parent_id,
						CASE WHEN (vtiger_activity.activitytype = 'Task') THEN (vtiger_activity.status) ELSE (vtiger_activity.eventstatus) END AS status
						FROM vtiger_activity
						INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_activity.activityid
						LEFT JOIN vtiger_seactivityrel ON vtiger_seactivityrel.activityid = vtiger_activity.activityid
						LEFT JOIN vtiger_cntactivityrel ON vtiger_cntactivityrel.activityid = vtiger_activity.activityid
						LEFT JOIN vtiger_users ON vtiger_users.id = vtiger_crmentity.smownerid
						LEFT JOIN vtiger_groups ON vtiger_groups.groupid = vtiger_crmentity.smownerid
							WHERE vtiger_crmentity.deleted = 0 AND vtiger_activity.activitytype <> 'Emails'
								AND (vtiger_seactivityrel.crmid = ".$recordId;
			if($entityIds) {
				$query .= " OR vtiger_cntactivityrel.contactid IN (".$entityIds."))";
			} else {
				$query .= ")";
			}

			$relatedModuleName = $relatedModule->getName();
			$query .= $this->getSpecificRelationQuery($relatedModuleName);
			$nonAdminQuery = $this->getNonAdminAccessControlQueryForRelation($relatedModuleName);
			if ($nonAdminQuery) {
				$query = appendFromClauseToQuery($query, $nonAdminQuery);

				if(trim($nonAdminQuery)) {
					$relModuleFocus = CRMEntity::getInstance($relatedModuleName);
					$condition = $relModuleFocus->buildWhereClauseConditionForCalendar();
					if($condition) {
						$query .= ' AND '.$condition;
					}
				}
			}

			// There could be more than one contact for an activity.
			$query .= ' GROUP BY vtiger_activity.activityid';
		} else {
			$query = parent::getRelationQuery($recordId, $functionName, $relatedModule, $relationId);
		}

		return $query;
	}

	/**
	 * Function returns the Calendar Events for the module
	 * @param <String> $mode - upcoming/overdue mode
	 * @param <Vtiger_Paging_Model> $pagingModel - $pagingModel
	 * @param <String> $user - all/userid
	 * @param <String> $recordId - record id
	 * @return <Array>
	 */
	function getCalendarActivities($mode, $pagingModel, $user, $recordId = false) {
		$currentUser = Users_Record_Model::getCurrentUserModel();
		$db = PearDatabase::getInstance();

		if (!$user) {
			$user = $currentUser->getId();
		}

		$nowInUserFormat = Vtiger_Datetime_UIType::getDisplayDateTimeValue(date('Y-m-d H:i:s'));
		$nowInDBFormat = Vtiger_Datetime_UIType::getDBDateTimeValue($nowInUserFormat);
		list($currentDate, $currentTime) = explode(' ', $nowInDBFormat);

		$focus = CRMEntity::getInstance($this->getName());
		$focus->id = $recordId;
		$entityIds = $focus->getRelatedContactsIds();
		$entityIds = implode(',', $entityIds);

		$query = "SELECT DISTINCT vtiger_crmentity.crmid, (CASE WHEN (crmentity2.crmid not like '') THEN crmentity2.crmid ELSE crmentity3.crmid END) AS parent_id, 
					(CASE WHEN (crmentity2.setype not like '') then crmentity2.setype ELSE crmentity3.setype END) AS crmentity2module, vtiger_crmentity.smownerid, vtiger_crmentity.setype, vtiger_activity.* FROM vtiger_activity
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_activity.activityid
					LEFT JOIN vtiger_seactivityrel ON vtiger_seactivityrel.activityid = vtiger_activity.activityid
					LEFT JOIN vtiger_cntactivityrel ON vtiger_cntactivityrel.activityid = vtiger_activity.activityid
					LEFT JOIN vtiger_crmentity as crmentity2 on (vtiger_seactivityrel.crmid = crmentity2.crmid AND vtiger_seactivityrel.crmid IS NOT NULL AND crmentity2.deleted = 0)
					LEFT JOIN vtiger_crmentity as crmentity3 on (vtiger_cntactivityrel.contactid = crmentity3.crmid AND vtiger_cntactivityrel.contactid IS NOT NULL AND crmentity3.deleted = 0)
					LEFT JOIN vtiger_groups ON vtiger_groups.groupid = vtiger_crmentity.smownerid";

		$query .= Users_Privileges_Model::getNonAdminAccessControlQuery('Calendar');

		$query .= " WHERE vtiger_crmentity.deleted=0
					AND (vtiger_activity.activitytype NOT IN ('Emails'))
					AND (vtiger_activity.status is NULL OR vtiger_activity.status NOT IN ('Completed', 'Deferred', 'Cancelled'))
					AND (vtiger_activity.eventstatus is NULL OR vtiger_activity.eventstatus NOT IN ('Held', 'Cancelled'))";

		if(!$currentUser->isAdminUser()) {
			$moduleFocus = CRMEntity::getInstance('Calendar');
			$condition = $moduleFocus->buildWhereClauseConditionForCalendar();
			if($condition) {
				$query .= ' AND '.$condition;
			}
		}

		if ($mode === 'upcoming') {
			$query .= " AND CASE WHEN vtiger_activity.activitytype='Task' THEN due_date >= '$currentDate' ELSE CONCAT(due_date,' ',time_end) >= '$nowInDBFormat' END";
		} elseif ($mode === 'overdue') {
			$query .= " AND CASE WHEN vtiger_activity.activitytype='Task' THEN due_date < '$currentDate' ELSE CONCAT(due_date,' ',time_end) < '$nowInDBFormat' END";
		}

		$params = array();

		if ($recordId) {
			$query .= " AND (vtiger_seactivityrel.crmid = ?";
			array_push($params, $recordId);
			if ($entityIds) {
				$query .= " OR vtiger_cntactivityrel.contactid IN (" . $entityIds . "))";
			} else {
				$query .= ")";
			}
		}

		if ($user != 'all' && $user != '') {
			$query .= " AND vtiger_crmentity.smownerid = ?";
			array_push($params, $user);
		}

		$query .= " ORDER BY date_start, time_start LIMIT " . $pagingModel->getStartIndex() . ", " . ($pagingModel->getPageLimit() + 1);

		$result = $db->pquery($query, $params);
		$numOfRows = $db->num_rows($result);

		$groupsIds = Vtiger_Util_Helper::getGroupsIdsForUsers($currentUser->getId());
		$activities = array();
		$recordsToUnset = array();
		for ($i = 0; $i < $numOfRows; $i++) {
			$newRow = $db->query_result_rowdata($result, $i);
			$model = Vtiger_Record_Model::getCleanInstance('Calendar');
			$ownerId = $newRow['smownerid'];
			$currentUser = Users_Record_Model::getCurrentUserModel();
			$visibleFields = array('activitytype', 'date_start', 'time_start', 'due_date', 'time_end', 'assigned_user_id', 'visibility', 'smownerid', 'crmid');
			$visibility = true;
			if (in_array($ownerId, $groupsIds)) {
				$visibility = false;
			} else if ($ownerId == $currentUser->getId()) {
				$visibility = false;
			}
			if (!$currentUser->isAdminUser() && $newRow['activitytype'] != 'Task' && $newRow['visibility'] == 'Private' && $ownerId && $visibility) {
				foreach ($newRow as $data => $value) {
					if (in_array($data, $visibleFields) != -1) {
						unset($newRow[$data]);
					}
				}
				$newRow['subject'] = vtranslate('Busy', 'Events') . '*';
			}
			if ($newRow['activitytype'] == 'Task') {
				unset($newRow['visibility']);

				$due_date = $newRow["due_date"];
				$dayEndTime = "23:59:59";
				$EndDateTime = Vtiger_Datetime_UIType::getDBDateTimeValue($due_date . " " . $dayEndTime);
				$dueDateTimeInDbFormat = explode(' ', $EndDateTime);
				$dueTimeInDbFormat = $dueDateTimeInDbFormat[1];
				$newRow['time_end'] = $dueTimeInDbFormat;
			}

			if ($newRow['crmentity2module'] == 'Contacts') {
				$newRow['contact_id'] = $newRow['parent_id'];
				unset($newRow['parent_id']);
			}
			$model->setData($newRow);
			$model->setId($newRow['crmid']);
			$activities[$newRow['crmid']] = $model;
			if (!$currentUser->isAdminUser() && $newRow['activitytype'] == 'Task' && isToDoPermittedBySharing($newRow['crmid']) == 'no') {
				$recordsToUnset[] = $newRow['crmid'];
			}
		}

		$pagingModel->calculatePageRange($activities);
		if ($numOfRows > $pagingModel->getPageLimit()) {
			array_pop($activities);
			$pagingModel->set('nextPageExists', true);
		} else {
			$pagingModel->set('nextPageExists', false);
		}
		//after setting paging model, unsetting the records which has no permissions
		foreach ($recordsToUnset as $record) {
			unset($activities[$record]);
		}
		return $activities;
	}

	function getDatesFromRange($start, $end) { 
		$array = array();  
		$interval = new DateInterval('P1D');  
		$realEnd = new DateTime($end); 
		$realEnd->add($interval);   
		$period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
		foreach($period as $date) {                  
			$array[] = $date;  
		}  
		return $period; 
	} 

	function getAccountsReport($type='', $dateFilter='') {
    	$db = PearDatabase::getInstance();
		$last_week = date('Y-m-d', strtotime('-13 days'));
		$today = date("Y-m-d");
			
		if(!$type) $type = 'with';

		$sql = "SELECT DISTINCT a.accountid, a.accountname, DATE_FORMAT(e.createdtime, '%d') as day, DATE_FORMAT(e.createdtime, '%Y-%m-%d') as week_day, COUNT(DISTINCT s.salesorderid) as orders_num 
									FROM vtiger_account a
									LEFT JOIN vtiger_salesorder s ON s.accountid=a.accountid
									LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
									WHERE e.deleted = 0 AND setype = 'SalesOrder' ";			
		$params = array();		

		//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";
			$params[] = $dateFilter['start'];
			$params[] = date('Y-m-d',  strtotime("+13 day", strtotime($dateFilter['start'])));
			$today = date('Y-m-d',  strtotime("+13 day", strtotime($dateFilter['start'])));
			$last_week = date('Y-m-d', strtotime($dateFilter['start']));
		}else{
			$sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";
			$params[] = $last_week; 
			$params[] = $today; 
		}

		$sql .= " GROUP BY a.accountid  ORDER BY  orders_num DESC ";	
			
		$result = $db->pquery($sql,$params);   		
		$noOfRows = $db->num_rows($result);

		$accountsSalesOrders = array();
		$accountid_array = array();

		$lt_week_days_full = array('Monday' => 'Pirmadienis', 'Tuesday' => 'Antradienis', 'Wednesday' => 'Trečiadienis', 'Thursday' => 'Ketvirtadienis', 'Friday' => 'Penktadienis', 'Saturday' => 'Šeštadienis', 'Sunday' => 'Sekmadienis');

		$lt_week_days = array('Monday' => 'Pr.', 'Tuesday' => 'An.', 'Wednesday' => 'Tr.', 'Thursday' => 'Kt.', 'Friday' => 'Pn.', 'Saturday' => 'Št.', 'Sunday' => 'Sk.');

		$accounts = array();	
		$total_array = array();

		foreach($result as $row){
			$accounts[$row['accountname']] = $row['accountname'];
		}	
		
		$period = $this->getDatesFromRange($params[0], $params[1]); 

		foreach ($period as $key => $value) {				
			$order_date = $value->format('Y-m-d');	
			$order_week_day = $value->format('l');	
			$sql2 = "SELECT DISTINCT a.accountid, a.accountname, DATE_FORMAT(e.createdtime, '%d') as day, DATE_FORMAT(e.createdtime, '%Y-%m-%d') as week_day, COUNT(DISTINCT s.salesorderid) as orders_num 
									FROM vtiger_account a
									LEFT JOIN vtiger_salesorder s ON s.accountid=a.accountid
									LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
									WHERE e.deleted = 0 AND setype = 'SalesOrder' AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') = ? 
									GROUP BY a.accountid  ORDER BY orders_num DESC ";

				$result2 = $db->pquery($sql2,array($order_date));			
				$noOfRows2 = $db->num_rows($result2);
			if($order_week_day != 'Saturday' && $order_week_day != 'Sunday'){
				$accountsSalesOrders['days'][] = array('short' => $lt_week_days[date('l', strtotime($order_week_day))], 'full' =>  $lt_week_days_full[date('l', strtotime($order_week_day))]);	
		
				if($noOfRows2){
					for($i=0; $i<$noOfRows2; $i++) {
					
						if($type == 'with'){								
							$row = $db->query_result_rowdata($result2, $i);
							$weekDay = $lt_week_days[date('l', strtotime($row['week_day']))];								   
							$total_array[$weekDay][$row['accountname']] = $row['orders_num']; 
							
							foreach($accounts as $account){				        
								$accountsSalesOrders['orders'][$account][$key] = ($total_array[$weekDay][$account]) ?: 0; 
							}
							$accountsSalesOrders['accountname'] = $accounts;		
						}elseif($type == 'without'){
							$accountid_array[] = $db->query_result($result,$i,'accountid');		
						}				
					}

				}else{
					if($type == 'with'){		
						foreach($accounts as $account){				        
							$accountsSalesOrders['orders'][$account][$key] = 0; 
						}
					}elseif($type == 'without'){
						$accountid_array[] = $db->query_result($result,0,'accountid');		
					}		
				}

			}
			
		} 

		if($type == 'without'){
			$accountid = implode(",", $accountid_array);		
			$sql2 = "SELECT DISTINCT a.accountname, 0 as orders_num FROM vtiger_account a
											LEFT JOIN vtiger_crmentity e ON e.crmid=a.accountid
											WHERE e.setype =0 AND a.accountid NOT IN ($accountid)
											ORDER BY a.accountid DESC LIMIT 50";
			$result3 = $db->pquery($sql2,array());
			
			foreach($result3 as $row){
				$accounts2[$row['accountname']] = $row['accountname'];
			}
			$accountsSalesOrders['accountname'] = $accounts2;			
		}

		$accountsSalesOrders['filter'] = 	"Nuo: ".$last_week." Iki: ".$today; 		

		return $accountsSalesOrders;
	}

	function getAccountsProfitReport($dateFilter='', $owner_name='',$order_type='',$new_filter='') {
		$db = PearDatabase::getInstance();
	
		if(empty($dateFilter)) {
			$date_from = date("Y")."-01";
			$date_to = date("Y-m");
		}else{
			$date_from =	date("Y-m",strtotime($dateFilter['start']));
			$date_to = (date("Y",strtotime($dateFilter['start'])) == date("Y") ? date("Y-m") : date("Y",strtotime($dateFilter['start']))."-12");
		}

		$show_filter = "Pasirinkti metai: ".(empty($dateFilter) ? date("Y") : date("Y",strtotime($dateFilter['start']))); 
		global $current_user;
		$role = $current_user->roleid;	

		$period = $this->getMonthRange($date_from,$date_to);
		
		$month_locale = array('Jan' => 'Sausis', 'Feb' => 'Vasaris', 'Mar' => 'Kovas', 'Apr' => 'Balandis', 'May' => 'Gegužė', 'Jun' => 'Birželis', 'Jul' => 'Liepa', 'Aug' => 'Rugpjūtis', 'Sep' => 'Rugsėjis', 'Oct' => 'Spalis', 'Nov' => 'Lapkritis', 'Dec' => 'Gruodis');

		$sql = "SELECT COUNT(s.salesorderid) AS orders, GROUP_CONCAT(s.salesorderid) AS salesorderid, SUM(s.total) AS total, accountname,DATE_FORMAT(se.createdtime, '%Y-%m') AS month, CONCAT(first_name,' ',last_name) AS owner,a.accountid, DATE_FORMAT(se.createdtime, '%b') AS short_month, DATE_FORMAT(e.createdtime, '%Y-%m') AS user_created ";
		if(!empty($new_filter)){
			$sql .= " ,IF(DATE_FORMAT(e.createdtime, '%b') = '$new_filter', 1,0) as created_month,
			IF(DATE_FORMAT(e.createdtime, '%b') = '$new_filter', SUM(s.total),0) as total_from_new ";
		}
		$sql .=" FROM vtiger_account a							
			INNER JOIN vtiger_crmentity e ON e.crmid=a.accountid
			LEFT JOIN vtiger_salesorder s ON s.accountid=a.accountid
			LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
			INNER JOIN vtiger_crmentity se ON se.crmid=s.salesorderid
			LEFT JOIN vtiger_accountscf acf ON acf.accountid=a.accountid
			LEFT JOIN vtiger_users u ON u.id=acf.cf_1655
			WHERE e.deleted = 0 AND se.deleted = 0 "; 			
		$params = array();		

		//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m') BETWEEN ? AND ? AND DATE_FORMAT(se.createdtime, '%Y-%m') BETWEEN ? AND ? ";
			$params[] = $date_from;
			$params[] = $date_to;    
			$params[] = $date_from;
			$params[] = $date_to;   
		}else{
			$sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m') BETWEEN ? AND ? AND DATE_FORMAT(se.createdtime, '%Y-%m') BETWEEN ? AND ? ";
			$params[] = $date_from; 
			$params[] = $date_to;  
			$params[] = $date_from;
			$params[] = $date_to;  
    	}

		if(!empty($owner_name)){
			$sql .= " AND CONCAT(first_name,' ',last_name) LIKE '%$owner_name%' ";
		}		
		
		if(!empty($order_type) && $order_type != 'Visi'){
			$sql .= " AND cf_855 = ? ";
			$params[] = $order_type;
		}	

		$sql .= " GROUP BY a.accountid,DATE_FORMAT(se.createdtime, '%Y-%m') ";

    
		if(!empty($new_filter)){
			$sql .= " ORDER BY created_month DESC,COUNT(s.salesorderid) DESC ";	
		}else{
			$sql .= " ORDER BY COUNT(s.salesorderid) DESC ";	
		}

		$db->query("SET SESSION GROUP_CONCAT = 1000000");      

    $result = $db->pquery($sql,$params); 
		       
		$statistic = array();
		$thisMonth = array();
		$profit = array();
		$month_profit = array();
		$client_month_profit = array();
		$clients = array();
		$owner = array();
		$salesorderid = array();

		foreach($result AS $row){
			$statistic[$row['month']][$row['accountname']] = $row['orders'];
			$thisMonth[$row['accountname']][$row['user_created']] = 1;
			$profit[$row['accountname']] += $row['total'];

			if(!empty($new_filter)){
				$month_profit[$row['short_month']] += $row['total_from_new'];
			}else{
				$month_profit[$row['short_month']] += $row['total'];
			}

			$client_month_profit[$row['month']][$row['accountname']] += $row['total'];
			$clients[$row['accountname']] = $row['accountname'];
			$owner[$row['accountname']] = $row['owner'];
			$accountid[$row['accountname']] = $row['accountid'];
			$salesorderid[$row['accountname']][$row['month']] = $row['salesorderid'];
		}	

		return array('period' => $period, 'month_locale' => $month_locale, 'statistic' => $statistic,'thisMonth' => $thisMonth, 'profit' => $profit,'month_profit' => $month_profit,'client_month_profit' => $client_month_profit, 'clients' => $clients,'owner' => $owner,'salesorderid' => $salesorderid, 'filter' => $show_filter,'role' => $role,'owner_name' => $owner_name,'order_type' => $order_type,'new_filter' => $new_filter,'accountid' => $accountid);
	}

	function getLostAccountsReport($dateFilter='', $owner_name='',$order_type='') {
		$db = PearDatabase::getInstance();
	
		if(empty($dateFilter)) {
			$date_from = date("Y")."-01";
			$date_to = date("Y-m");
		}else{
			$date_from =	date("Y-m",strtotime($dateFilter['start']));
			$date_to = (date("Y",strtotime($dateFilter['start'])) == date("Y") ? date("Y-m") : date("Y",strtotime($dateFilter['start']))."-12");
		}

		$show_filter = "Pasirinkti metai: ".(empty($dateFilter) ? date("Y") : date("Y",strtotime($dateFilter['start']))); 
		global $current_user;
		$role = $current_user->roleid;	

		$period = $this->getMonthRange($date_from,$date_to);
		
		$month_locale = array('Jan' => 'Sausis', 'Feb' => 'Vasaris', 'Mar' => 'Kovas', 'Apr' => 'Balandis', 'May' => 'Gegužė', 'Jun' => 'Birželis', 'Jul' => 'Liepa', 'Aug' => 'Rugpjūtis', 'Sep' => 'Rugsėjis', 'Oct' => 'Spalis', 'Nov' => 'Lapkritis', 'Dec' => 'Gruodis');

		$customers_q = "SELECT accountname,DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS created, a.accountid FROM vtiger_account a INNER JOIN vtiger_crmentity e ON e.crmid=a.accountid WHERE deleted = 0";
		// $customers = $db->pquery($customers_q,array()); 
		$db->pquery("SET SESSION group_concat_max_len = 1000000",array());

		$sql = "SELECT COUNT(s.salesorderid) AS orders, SUM(total) AS total, accountname,DATE_FORMAT(se.createdtime, '%Y-%m') AS month,CONCAT(first_name,' ',last_name) AS owner,a.accountid, GROUP_CONCAT(s.salesorderid) AS salesorderids
							FROM vtiger_account a
							INNER JOIN vtiger_crmentity e ON e.crmid=a.accountid
							LEFT JOIN vtiger_salesorder s ON s.accountid=a.accountid
							LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid					
							INNER JOIN vtiger_crmentity se ON se.crmid=s.salesorderid
							LEFT JOIN vtiger_accountscf acf ON acf.accountid=a.accountid
							LEFT JOIN vtiger_users u ON u.id=acf.cf_1655
							WHERE e.deleted = 0 AND se.deleted = 0 "; 			
		$params = array();		

		//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$sql .= " AND DATE_FORMAT(se.createdtime, '%Y-%m') BETWEEN ? AND ? ";
			$params[] = $date_from;
			$params[] = $date_to;      
		}else{
			$sql .= " AND DATE_FORMAT(se.createdtime, '%Y-%m') BETWEEN ? AND ? ";
			$params[] = $date_from; 
			$params[] = $date_to; 
		}

		if(!empty($owner_name)){
			$sql .= " AND CONCAT(first_name,' ',last_name) LIKE '%$owner_name%' ";
		}		

		if(!empty($order_type) && $order_type != 'Visi'){
			$sql .= " AND cf_855 = ? ";
			$params[] = $order_type;
		}	
							

    $sql .= " GROUP BY a.accountid,DATE_FORMAT(se.createdtime, '%Y-%m') HAVING orders >= 5	ORDER BY DATE_FORMAT(e.createdtime, '%Y-%m') ASC";

    $result = $db->pquery($sql,$params); 
		       
		$statistic = array();
		$salesorderid = array();
		$clients = array();
		$client_date = array();
		$statistic_all = array();


		foreach($result AS $row){ 
			$statistic[$row['accountname']][$row['month']] = $row['orders'];
			$salesorderid[$row['accountname']][$row['month']] = $row['salesorderids']; 
			$owner[$row['accountname']] = $row['owner']; 			
		}
	

		$DOWN = array();
		$orders = array();
		foreach ($statistic as $key => $value) {
			ksort($value);
			$orders_num = array();

			foreach ($value as $row) {
				$orders_num[] = $row;
			}
			
			
			for($i = 0; $i < COUNT($orders_num);$i++){
				$count = count($value)-1;
				$e = $i+1;         
				if($count >=  $e){ 
					if($orders_num[$e] < $orders_num[$i]*0.8){
						$DOWN[$key][] = 1;         
					}
				}     
			}      
		
			if(COUNT($orders_num)/2 <= COUNT($DOWN[$key])){
				$orders[$key] = $value;
			}

		}


		foreach($result AS $customer){ 	
			$client_date[$customer['accountname']] = $customer['created'];
			foreach($period AS $value){ 
				$date = $value['date'];
				if($orders[$customer['accountname']][$date] >= 5){
					$clients[$customer['accountname']] = $customer['accountname'];       
					$accountid[$customer['accountname']] = $customer['accountid'];     
					$statistic_all[$date][$customer['accountname']] = $orders[$customer['accountname']][$date];							
				}
			}
		
		}		
	

		return array('period' => $period, 'month_locale' => $month_locale, 'statistic_all' => $statistic_all, 'client_date' => $client_date,'owner' => $owner, 'salesorderid' => $salesorderid,'clients' => $clients, 'filter' => $show_filter,'role' => $role,'order_type' => $order_type,'accountid' => $accountid,'owner_name' => $owner_name);
	}

	function getMonthRange($date1, $date2){
		$start    = (new DateTime($date1))->modify('first day of this month');
		$end      = (new DateTime($date2))->modify('first day of next month');
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);
		$periodas = array();
		foreach ($period as $dt) {
			$periodas[] = array('month' => $dt->format("M"), 'date' => $dt->format('Y-m'));
		}    
		return $periodas; 
	} 


	function getUsersReport($dateFilter='') {
		$db = PearDatabase::getInstance();
		$today = date("Y-m-d");

		global $current_user;

		$salesorders_query = "SELECT u.id, CONCAT(u.first_name,' ',u.last_name) AS employee, COUNT(DISTINCT e.crmid) AS number, GROUP_CONCAT(e.crmid) AS salesorderid, ROUND(SUM(total),2) AS total
							FROM vtiger_users u							
							JOIN vtiger_crmentity e ON e.smownerid=u.id
							JOIN vtiger_salesorder s ON s.salesorderid=e.crmid
							WHERE e.deleted = 0 AND e.setype = 'SalesOrder' AND e.source = 'crm'";

		$purchase_query = "SELECT u.id, CONCAT(u.first_name,' ',u.last_name) AS employee, COUNT(DISTINCT e.crmid) AS number,GROUP_CONCAT(e.crmid) AS purchaseorderid
							FROM vtiger_users u
							LEFT JOIN vtiger_crmentity e ON e.smownerid=u.id
							WHERE e.deleted = 0 AND u.id = 41 AND e.setype = 'PurchaseOrder' ";

		$employee_invoices_count_query = "SELECT u.id, CONCAT(u.first_name,' ',u.last_name) AS employee, ROUND(SUM(total), 2) AS total, COUNT(DISTINCT vtiger_crmentity.crmid) AS number,GROUP_CONCAT(vtiger_crmentity.crmid) AS invoiceid
							FROM vtiger_users u                            
							LEFT JOIN vtiger_crmentity ON vtiger_crmentity.smownerid=u.id
							LEFT JOIN vtiger_invoice ON vtiger_invoice.invoiceid=vtiger_crmentity.crmid
							WHERE vtiger_crmentity.deleted = 0  AND vtiger_crmentity.setype = 'Invoice' ";
							
		$employee_price_corrections_query = "SELECT u.id, CONCAT(u.first_name,' ',u.last_name) AS employee, COUNT(DISTINCT crmid) AS corrections,GROUP_CONCAT(crmid) AS salesorderid,roleid
							FROM vtiger_users u
							LEFT JOIN vtiger_modtracker_basic b ON u.id=b.whodid
							LEFT JOIN vtiger_modtracker_detail d ON b.id=d.id	
							INNER JOIN vtiger_user2role ON u.id = vtiger_user2role.userid 
							WHERE b.module = 'SalesOrder' AND d.fieldname = 'hdnGrandTotal' AND d.prevalue IS NOT NULL AND roleid IN ('H12','H25','H26') ";	

		$employee_price_corrections2_query = "SELECT u.id, CONCAT(u.first_name,' ',u.last_name) AS employee, COUNT(DISTINCT crmid) AS corrections,GROUP_CONCAT(crmid) AS salesorderid,roleid
							FROM vtiger_users u
							LEFT JOIN vtiger_modtracker_basic b ON u.id=b.whodid
							LEFT JOIN vtiger_modtracker_detail d ON b.id=d.id	
							INNER JOIN vtiger_user2role ON u.id = vtiger_user2role.userid 
							WHERE b.module = 'SalesOrder' AND d.fieldname = 'hdnGrandTotal' AND d.prevalue IS NOT NULL AND roleid NOT IN ('H12','H25','H26') ";						
		$params = array();
		$dates = [];

		//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$salesorders_query .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";
			$purchase_query .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";
			$employee_invoices_count_query .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";
			$employee_price_corrections_query .= " AND DATE_FORMAT(b.changedon, '%Y-%m-%d') BETWEEN ? AND ? ";
			$employee_price_corrections2_query .= " AND DATE_FORMAT(b.changedon, '%Y-%m-%d') BETWEEN ? AND ? ";
			$params[] = $dateFilter['start'];
			$params[] = $dateFilter['end'];

			$dates['from'] = $dateFilter['start'];
			$dates['to'] = $dateFilter['end'];

			$show_filter = 	"Nuo: ".$dateFilter['start']." Iki: ".$dateFilter['end'];
		}else{
			$salesorders_query .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') =  ? ";
			$purchase_query .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') =  ? ";
			$employee_invoices_count_query .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') =  ? ";
			$employee_price_corrections_query .= " AND DATE_FORMAT(b.changedon, '%Y-%m-%d') =  ? ";
			$employee_price_corrections2_query .= " AND DATE_FORMAT(b.changedon, '%Y-%m-%d') =  ? ";
			$params[] = $today;	
			$show_filter = $today." dienos";

			$dates['from'] = $today;
			$dates['to'] = $today;
		}	

		$salesorders_query .= " GROUP BY u.id";			
		$purchase_query .= " GROUP BY u.id";			
		$employee_invoices_count_query .= " GROUP BY u.id ORDER BY employee DESC";			
		$employee_price_corrections_query .= " GROUP BY u.id ORDER BY employee DESC";			
		$employee_price_corrections2_query .= " GROUP BY u.id ORDER BY employee DESC";			

		$db->query("SET SESSION group_concat_max_len = 1000000");
		$salesorders_result = $db->pquery($salesorders_query,$params);   		
		$purchase_result = $db->pquery($purchase_query,$params);   		
		$employee_invoices_count_result = $db->pquery($employee_invoices_count_query,$params);   		
		$employee_price_corrections_result = $db->pquery($employee_price_corrections_query,$params);   		
		$employee_price_corrections2_result = $db->pquery($employee_price_corrections2_query,$params);   		
		
		$salesorders_no_of_rows = $db->num_rows($salesorders_result);
		$purchase_no_of_rows = $db->num_rows($purchase_result);		
		$employee_price_corrections_no_of_rows = $db->num_rows($employee_price_corrections_result);
		$employee_price_corrections2_no_of_rows = $db->num_rows($employee_price_corrections2_result);
		
		
		$usersReportArray = [];
		$user_info = [];
		$user_info2 = [];		
			
		if($dates['from'] == $today && $dates['to'] == $today){
			$invoice_plan =  $db->pquery("SELECT total_invoices FROM vtiger_saskaitos_plan ORDER BY id DESC LIMIT 1", []); 
			$plan_details['count'] = $db->query_result($invoice_plan, 0, 'total_invoices');
		}else{
			$plan_details = \Helpers\InvoicePlanHelper::getInvoicesPlanForDashboard($db, $dates);
		}
		
		$usersReportArray['invoice']['plan'] = $plan_details['count'];	
	

		foreach($employee_invoices_count_result ?? [] as $row){           
			$user_info[$row['employee']] = array('id' => $row['id'],'name' => $row['employee']);              
		} 

		foreach($employee_price_corrections_result ?? [] as $row){   
			$user_info[$row['employee']] = array('id' => $row['id'],'name' => $row['employee']);	                
		} 

		foreach($employee_price_corrections2 ?? [] as $row){  			     
			$user_info2[$row['employee']] = array('id' => $row['id'],'name' => $row['employee']);                
		} 


		$usersReportArray['role'] = $current_user->roleid;
		$usersReportArray['admin'] = $current_user->is_admin;
	
		for($i=0; $i<$salesorders_no_of_rows; $i++) {
			$row = $db->query_result_rowdata($salesorders_result, $i);		
			$usersReportArray['sales'][] = $row;	
		} 

		for($i=0; $i<$purchase_no_of_rows; $i++) {
			$row2 = $db->query_result_rowdata($purchase_result, $i);		
			$usersReportArray['purchase'][] = $row2;	
		} 

	
		foreach($user_info ?? [] as $user){  				
			$usersReportArray['invoice']['users'][] = $user;	
		}

		$usersReportArray['invoice']['total_invoices'] = 0;
		$usersReportArray['invoice']['total_invoices_sum'] = 0;

		foreach($user_info ?? [] as $user){  	
			foreach ($employee_invoices_count_result ?? [] as $employee_invoices) {
				if($user['id'] == $employee_invoices['id']){	

					$usersReportArray['invoice'][$user['name']]['num'] = [
						'num' => $employee_invoices['number'],
						'sum' => $employee_invoices['total'], 
						'invoiceid' => $employee_invoices['invoiceid']
					];	

					$usersReportArray['invoice']['total_invoices'] += $employee_invoices['number'];
					$usersReportArray['invoice']['total_invoices_sum'] += $employee_invoices['total'];
				}			
			}
		}

		foreach($user_info ?? [] as $user){  		
			for($i=0; $i<$employee_price_corrections_no_of_rows; $i++) {
				$row4 = $db->query_result_rowdata($employee_price_corrections_result, $i);	
				
				if($user['id'] == $row4['id']){		
					$usersReportArray['invoice'][$user['name']]['corr'] = array('corr' => $row4['corrections'], 'salesorderid' => $row4['salesorderid']);
				}
			}
		}

		foreach($user_info2 ?? [] as $user){  		
			for($i=0; $i<$employee_price_corrections2_no_of_rows; $i++) {
				$row5 = $db->query_result_rowdata($employee_price_corrections2_result, $i);					
				if(!empty($row5['prevalue'])){				
					if($user['id'] == $row4['id']){		
						$usersReportArray['managers_corr'][$user['name']] = array('corr' => $row5['corrections'], 'salesorderid' => $row5['salesorderid']);	
					}
				}				
			}
		}

		$usersReportArray['filter'] = $show_filter;   

		return $usersReportArray;
	}
}
