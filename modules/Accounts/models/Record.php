<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Accounts_Record_Model extends Vtiger_Record_Model {

	/**
	 * Function returns the details of Accounts Hierarchy
	 * @return <Array>
	 */
	function getAccountHierarchy() {
		$focus = CRMEntity::getInstance($this->getModuleName());
		$hierarchy = $focus->getAccountHierarchy($this->getId());
		$i=0;
		foreach($hierarchy['entries'] as $accountId => $accountInfo) {
			preg_match('/<a href="+/', $accountInfo[0], $matches);
			if($matches != null) {
				preg_match('/[.\s]+/', $accountInfo[0], $dashes);
				preg_match("/<a(.*)>(.*)<\/a>/i",$accountInfo[0], $name);

				$recordModel = Vtiger_Record_Model::getCleanInstance('Accounts');
				$recordModel->setId($accountId);
				$hierarchy['entries'][$accountId][0] = $dashes[0]."<a href=".$recordModel->getDetailViewUrl().">".$name[2]."</a>";
			}
		}
		return $hierarchy;
	}

	/**
	 * Function returns the url for create event
	 * @return <String>
	 */
	function getCreateEventUrl() {
		$calendarModuleModel = Vtiger_Module_Model::getInstance('Calendar');
		return $calendarModuleModel->getCreateEventRecordUrl().'&parent_id='.$this->getId();
	}

	/**
	 * Function returns the url for create todo
	 * @retun <String>
	 */
	function getCreateTaskUrl() {
		$calendarModuleModel = Vtiger_Module_Model::getInstance('Calendar');
		return $calendarModuleModel->getCreateTaskRecordUrl().'&parent_id='.$this->getId();
	}

	/**
	 * Function to check duplicate exists or not
	 * @return <boolean>
	 */
	public function checkDuplicate() {
		$db = PearDatabase::getInstance();

		$query = "SELECT 1 FROM vtiger_crmentity WHERE setype = ? AND label = ? AND deleted = 0";
                $params = array($this->getModule()->getName(), decode_html($this->getName())); 

		$record = $this->getId();
		if ($record) {
			$query .= " AND crmid != ?";
			array_push($params, $record);
		}

		$result = $db->pquery($query, $params);
		if ($db->num_rows($result)) {
			return true;
		}
		return false;
	}

	/**
	 * Function to get List of Fields which are related from Accounts to Inventory Record.
	 * @return <array>
	 */
	public function getInventoryMappingFields() {
		return array(
				//Billing Address Fields
				array('parentField'=>'bill_city', 'inventoryField'=>'bill_city', 'defaultValue'=>''),
				array('parentField'=>'bill_street', 'inventoryField'=>'bill_street', 'defaultValue'=>''),
				array('parentField'=>'bill_state', 'inventoryField'=>'bill_state', 'defaultValue'=>''),
				array('parentField'=>'bill_code', 'inventoryField'=>'bill_code', 'defaultValue'=>''),
				array('parentField'=>'bill_country', 'inventoryField'=>'bill_country', 'defaultValue'=>''),
				array('parentField'=>'bill_pobox', 'inventoryField'=>'bill_pobox', 'defaultValue'=>''),

				//Shipping Address Fields
				array('parentField'=>'ship_city', 'inventoryField'=>'ship_city', 'defaultValue'=>''),
				array('parentField'=>'ship_street', 'inventoryField'=>'ship_street', 'defaultValue'=>''),
				array('parentField'=>'ship_state', 'inventoryField'=>'ship_state', 'defaultValue'=>''),
				array('parentField'=>'ship_code', 'inventoryField'=>'ship_code', 'defaultValue'=>''),
				array('parentField'=>'ship_country', 'inventoryField'=>'ship_country', 'defaultValue'=>''),
				array('parentField'=>'ship_pobox', 'inventoryField'=>'ship_pobox', 'defaultValue'=>'')
		);
	}

	function getRoadTaxFee(){
		$db = PearDatabase::getInstance();
		$result = $db->pquery("SELECT value FROM app_other_settings WHERE title = 'LBL_ROAD_TAX_FEE'");
		return $db->query_result($result,0, 'value');
	}

	function saveTemplate($req){
		$recordid = $req->get('record');
		$db = PearDatabase::getInstance();
		global $current_user; 	

		$query = "INSERT INTO app_standing_salesorders (account_id, enabled, execute_type, execute,execute_time, order_type,create_from,create_untill, load_delay, load_time_from, load_time_to, unload_delay, unload_time_from, unload_time_to, load_company, unload_company, load_contact, unload_contact,load_phone,unload_phone, bill_street, ship_street, bill_city, ship_city, bill_code, ship_code, bill_country, ship_country,pricebook_price,agreed_price,price,created_person, metrika, withoutinvoice, dismisservice) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		$query2 = "INSERT INTO app_standing_salesorders_cargo (id, sequence_no, quantity, comment, cargo_wgt, cargo_length, cargo_width, cargo_height, measure) VALUES (?,?,?,?,?,?,?,?,?)";

		if($req->get('execute_type') == 1){
			$execute = implode(",",$req->get('execute'));
		}elseif($req->get('execute_type') == 2){
			$execute = implode(",",$req->get('execute2'));
		}else{
			$execute = 0;
		}


		$execute = array($recordid,1,$req->get('execute_type'),$execute,$req->get('execute_time'),$req->get('cf_855'),$req->get('create_from'),$req->get('create_untill'),$req->get('load_delay'),$req->get('load_time_from'),$req->get('load_time_to'),$req->get('unload_delay'),$req->get('unload_time_from'),$req->get('unload_time_to'),$req->get('load_company'),$req->get('unload_company'),$req->get('load_contact'),$req->get('unload_contact'),$req->get('cf_1564'),$req->get('cf_1566'),$req->get('bill_street'),$req->get('ship_street'),$req->get('bill_city'),$req->get('ship_city'),$req->get('bill_code'),$req->get('ship_code'),$req->get('bill_country'),$req->get('ship_country'),$req->get('pricebookprice'),$req->get('agreedprice'),$req->get('total'),$current_user->id, $req->get('metrika'),$req->get('withoutinvoice'),$req->get('dismisservice'));

		$db->pquery($query,$execute);

		$totalProductCount = $req->get('totalProductCount');

	 	$last_id = $db->getLastInsertID();

		for($i = 1; $i <= $totalProductCount;$i++){
			$execute2 = array($last_id,$i,$req->get('qty'.$i),$req->get('comment'.$i),$req->get('cargo_wgt'.$i),$req->get('cargo_length'.$i),$req->get('cargo_width'.$i),$req->get('cargo_height'.$i),$req->get('measure'.$i));
			$db->pquery($query2,$execute2);
		}

		if($req->get('createorders') == 1){
			$this->executeCreateSalesOrders($db, $last_id,$recordid);
		}else{
			$referer = $_SERVER['PHP_SELF']."?module=Accounts&view=Detail&record=$recordid";
			return header("Location: $referer");
		}

	}

	function editTemplate($req){
		$recordid = $req->get('record');
		$last_id = $req->get('template_id');
		$db = PearDatabase::getInstance();	
		

		$query = "UPDATE app_standing_salesorders SET execute_type = ?, execute = ?,execute_time = ?, order_type = ?,create_from = ?,create_untill = ?, load_delay = ?, load_time_from = ?, load_time_to = ?, unload_delay = ?, unload_time_from = ?, unload_time_to = ?, load_company = ?, unload_company = ?, load_contact = ?, unload_contact = ?,load_phone = ?,unload_phone = ?, bill_street = ?, ship_street = ?, bill_city = ?, ship_city = ?, bill_code = ?, ship_code = ?, bill_country = ?, ship_country = ?,pricebook_price = ?,agreed_price = ?, price = ?, metrika = ?, withoutinvoice = ?, dismisservice = ? WHERE account_id = ? AND id = ?";

		$query2 = "INSERT INTO app_standing_salesorders_cargo (id, sequence_no, quantity, comment, cargo_wgt, cargo_length, cargo_width, cargo_height, measure) VALUES (?,?,?,?,?,?,?,?,?)";

		$query3 = "DELETE FROM app_standing_salesorders_cargo WHERE id = ?";

		if($req->get('execute_type') == 1){
			$execute = implode(",",$req->get('execute'));
		}else	if($req->get('execute_type') == 2){
			$execute = implode(",",$req->get('execute2'));
		}else{
			$execute = 0;
		}

		$execute = array($req->get('execute_type'),$execute,$req->get('execute_time'),$req->get('cf_855'),$req->get('create_from'),$req->get('create_untill'),$req->get('load_delay'),$req->get('load_time_from'),$req->get('load_time_to'),$req->get('unload_delay'),$req->get('unload_time_from'),$req->get('unload_time_to'),$req->get('load_company'),$req->get('unload_company'),$req->get('load_contact'),$req->get('unload_contact'),$req->get('cf_1564'),$req->get('cf_1566'),$req->get('bill_street'),$req->get('ship_street'),$req->get('bill_city'),$req->get('ship_city'),$req->get('bill_code'),$req->get('ship_code'),$req->get('bill_country'),$req->get('ship_country'),$req->get('pricebookprice'),$req->get('agreedprice'),$req->get('total'), $req->get('metrika'), $req->get('withoutinvoice'), $req->get('dismisservice'), $recordid, $last_id);

		$db->pquery($query,$execute);

		$db->pquery($query3,array($last_id));

		$totalProductCount = $req->get('totalProductCount');

	 
		for($i = 1; $i <= $totalProductCount;$i++){
			$execute2 = array($last_id,$i,$req->get('qty'.$i),$req->get('comment'.$i),$req->get('cargo_wgt'.$i),$req->get('cargo_length'.$i),$req->get('cargo_width'.$i),$req->get('cargo_height'.$i),$req->get('measure'.$i));
			$db->pquery($query2,$execute2);
		}

		if($req->get('createorders') == 1){
			$this->executeCreateSalesOrders($db, $last_id,$recordid);
		}else{
			$referer = $_SERVER['PHP_SELF']."?module=Accounts&view=Detail&record=$recordid";
			return header("Location: $referer");
		}

	}

	function getTemplate(){
		$db = PearDatabase::getInstance();
		$record = $this->getId();

		$query = "SELECT s.*, CONCAT(u.first_name,' ',u.last_name) AS creator 
											 FROM app_standing_salesorders s
											 LEFT JOIN vtiger_users u ON u.id=s.created_person
											 WHERE account_id = ? ORDER BY s.id DESC";		

		$query2 = "SELECT * FROM app_standing_salesorders_cargo WHERE id = ? ORDER BY id DESC";

		$result = $db->pquery($query,array($record));
		$templates = array();		
		$startDate = strtotime(date('Y-m-d'));
	
			foreach($result AS $row){			
				$execution_date = '';					
				$endDate = strtotime($row['create_untill']);

				if($row['execute_type'] == 1){
					$exe = explode(",",$row['execute']);				
					foreach($exe AS $execute){
						for($i = strtotime($execute, $startDate); $i <= $endDate; $i = strtotime('+1 week', $i)){
							$order_date =  date('Y-m-d', $i);
							if($order_date !=  date("Y-m-d", strtotime(date("Y-m-d",easter_date(date('Y',strtotime($order_date)))). "+ 1 day"))	&& $order_date != date("Y")."-06-24" && $order_date != date("Y")."-03-11"){
								$execute_days[$row['id']][] = array('load_date' => date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days")), 'unload_date' => date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days")),'actual_date' => $order_date );	
							}
						}
					}
				}elseif($row['execute_type'] == 2){
					$exe = explode(",",$row['execute']);
					foreach($exe AS $execute){
						for ($currentDate = $startDate; $currentDate <= $endDate; $currentDate += (86400)) {
							if(date('d', $currentDate) == $execute) { 
								$order_date =  date('Y-m-d', $currentDate);
								$execute_days[$row['id']][] = array('load_date' => date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days")), 'unload_date' => date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days")),'actual_date' => $order_date );					
							}
						}
					}
				}elseif($row['execute_type'] == 3){
					for($i = $startDate; $i <= $endDate; $i = strtotime('+1 day', $i)){				
						$order_date = date('Y-m-d', $i);
						if(date('D', strtotime($order_date)) != 'Sat' && date('D', strtotime($order_date)) != 'Sun'){
							if($order_date !=  date("Y-m-d", strtotime(date("Y-m-d",easter_date(date('Y',strtotime($order_date)))). "+ 1 day"))	&& $order_date != date("Y")."-06-24" && $order_date != date("Y")."-03-11"){
								$execute_days[$row['id']][] = array('load_date' => date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days")), 'unload_date' => date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days")),'actual_date' => $order_date );
							}
						}		
					}
				}


			$load_date = date("Y-m-d", strtotime($execution_date. "+". $row['load_delay']."days"));
			$unload_date = date("Y-m-d", strtotime($execution_date. "+". $row['unload_delay']."days"));

			$result2 = $db->pquery($query2,array($row['id']));
			$loads = array();
			$n = 1;
			foreach($result2 AS $load){
				$loads[$n] = array('cargo_wgt'.$n => $load['cargo_wgt'],
												 'cargo_length'.$n => $load['cargo_length'],
												 'cargo_width'.$n => $load['cargo_width'],
												 'cargo_height'.$n => $load['cargo_height'],
												 'measure'.$n => $load['measure'],
												 'qty'.$n => $load['quantity'],
												 'comment'.$n => $load['comment']);
				$n++;
			}
			sort($execute_days[$row['id']]);
			$templates[] = array('record' => $row['id'],
													 'enabled' => $row['enabled'],
													 'order_type' => $row['order_type'],
													 'create_untill' => $row['create_untill'],
													 'create_from' => $row['create_from'],
													 'execute_type' => $row['execute_type'],
													 'execute' => ($row['execute_type'] == 3 ? 'Kasdien' : $row['execute']),
													 'execution_date' => $execute_days,
													//  'execution_date' => $execute_days." ".substr($row['execute_time'],0,-3),
													 'execute_time' => substr($row['execute_time'],0,-3),
													 'create_untill' => $row['create_untill'],
													 'load_date' => $load_date,
													 'load_company' => $row['load_company'],
													 'load_contact' => $row['load_contact'],
													 'load_phone' => $row['load_phone'],
													 'bill_street' => $row['bill_street'],
													 'bill_city' => $row['bill_city'],
													 'bill_code' => $row['bill_code'],
													 'bill_country' => $row['bill_country'],
													 'unload_date' => $unload_date,
													 'unload_company' => $row['unload_company'],
													 'unload_contact' => $row['unload_contact'],
													 'unload_phone' => $row['unload_phone'],
													 'ship_street' => $row['ship_street'],
													 'ship_city' => $row['ship_city'],
													 'ship_code' => $row['ship_code'],
													 'ship_country' => $row['ship_country'],
													 'load_delay' => $row['load_delay'],
													 'load_time_from' => substr($row['load_time_from'],0,-3),
													 'load_time_to' => substr($row['load_time_to'],0,-3),
													 'unload_delay' => $row['unload_delay'],
													 'unload_time_from' => substr($row['unload_time_from'],0,-3),
													 'unload_time_to' => substr($row['unload_time_to'],0,-3),												
													 'order_type' => $row['order_type'],
													 'pricebook_price' => $row['pricebook_price'],
													 'agreed_price' => $row['agreed_price'],
													 'price' => $row['price'],
													 'creator' => $row['creator'],
													 'createdtime' => $row['createdtime'],
													 'metrika' => $row['metrika'],
													 'withoutinvoice' => $row['withoutinvoice'],
													 'dismisservice' => $row['dismisservice'],
													 'loads' => $loads);
		}
		// die;
		return $templates;
	}

	function standingOrdersStatus($record){
		$db = PearDatabase::getInstance();	
		$query = "SELECT cf_2002 FROM vtiger_accountscf WHERE accountid = ?";
		$result = $db->pquery($query,array($record));
	  $status =	$db->query_result($result,0,'cf_2002');

		return $status;
	}

	function getMeasure() {
		$db = PearDatabase::getInstance();					
		$query = "SELECT * FROM app_measures";
		$result = $db->query($query);
		$measures = array();
		foreach($result AS $row){
			$measures[] = $row;
		}	
		return $measures;
	}

	
	function getProducts($id) {
		$db = PearDatabase::getInstance();
		
		$query = "SELECT *,app_measures.code 
											 FROM vtiger_leads_cargo 
											 LEFT JOIN app_measures ON app_measures.id=vtiger_leads_cargo.measure
							WHERE vtiger_leads_cargo.id = ?";		
		$result = $db->pquery($query,array($id));

		$rowCount = $db->num_rows($result);
		$relatedProducts = array();
	
		for($i = 1; $i <= $rowCount;$i++){
			$relatedProducts[$i]['cargo_wgt'.$i] = $db->query_result($result,$i-1,'cargo_wgt');
			$relatedProducts[$i]['cargo_length'.$i] = $db->query_result($result,$i-1,'cargo_length');
			$relatedProducts[$i]['cargo_width'.$i] = $db->query_result($result,$i-1,'cargo_width');
			$relatedProducts[$i]['cargo_height'.$i] = $db->query_result($result,$i-1,'cargo_height');
			$relatedProducts[$i]['measure'.$i] = $db->query_result($result,$i-1,'measure');
			$relatedProducts[$i]['measure_code'.$i] = $db->query_result($result,$i-1,'code');
			$relatedProducts[$i]['qty'.$i] = $db->query_result($result,$i-1,'quantity');
			$relatedProducts[$i]['comment'.$i] = $db->query_result($result,$i-1,'comment');
			$relatedProducts[$i]['productName'.$i] = $db->query_result($result,$i-1,'cargo_wgt')." ".$db->query_result($result,$i-1,'cargo_length')."x".$db->query_result($result,$i-1,'cargo_width')."x".$db->query_result($result,$i-1,'cargo_height') ;
		}



		return $relatedProducts;
	}

	function executeCreateSalesOrders($db, $id,$recordid){

		$sth = $db->pquery('SELECT execute_type,create_from,create_untill,execute,load_delay,unload_delay FROM app_standing_salesorders WHERE id = ?',array($id));

		// $startDate = strtotime(date('Y-m-d'));	
				
		$execute_days = array();

		foreach($sth AS $row){
			$startDate = strtotime($row['create_from']);
			$endDate = strtotime($row['create_untill']);
			if($row['execute_type'] == 1){
				$exe = explode(",",$row['execute']);				
				foreach($exe AS $execute){
					for($i = strtotime($execute, $startDate); $i <= $endDate; $i = strtotime('+1 week', $i)){
						$order_date =  date('Y-m-d', $i);
						if($order_date !=  date("Y-m-d", strtotime(date("Y-m-d",easter_date(date('Y',strtotime($order_date)))). "+ 1 day"))	&& $order_date != date("Y")."-06-24" && $order_date != date("Y")."-03-11"){
							$execute_days[] = array('load_date' => date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days")), 'unload_date' => date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days")),'actual_date' => $order_date );	
						}
					}
				}
			}elseif($row['execute_type'] == 2){
				$exe = explode(",",$row['execute']);				
				foreach($exe AS $execute){
					for ($currentDate = $startDate; $currentDate <= $endDate; $currentDate += (86400)) {
						if(date('d', $currentDate) == $execute) { 
							$order_date =  date('Y-m-d', $currentDate);
							$execute_days[] = array('load_date' => date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days")), 'unload_date' => date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days")),'actual_date' => $order_date );					
						}
					}
				}
			}elseif($row['execute_type'] == 3){
				for($i = $startDate; $i <= $endDate; $i = strtotime('+1 day', $i)){				
					$order_date =  date('Y-m-d', $i);
					if(date('D', strtotime($order_date)) != 'Sat' && date('D', strtotime($order_date)) != 'Sun'){
						if($order_date !=  date("Y-m-d", strtotime(date("Y-m-d",easter_date(date('Y',strtotime($order_date)))). "+ 1 day"))	&& $order_date != date("Y")."-06-24" && $order_date != date("Y")."-03-11"){
							$execute_days[] = array('load_date' => date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days")), 'unload_date' => date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days")),'actual_date' => $order_date );	
						}
					}		
					// $execute_days[] = array('load_date' => date("Y-m-d", strtotime($order_date. "+". $row['load_delay']."days")), 'unload_date' => date("Y-m-d", strtotime($order_date. "+". $row['unload_delay']."days")),'actual_date' => $order_date );			
				}
			}
		}	


		sort($execute_days);
		foreach($execute_days AS $days){
			$this->createSalesOrders($db, $id, $days);
		}

		$referer = $_SERVER['PHP_SELF']."?module=Accounts&view=Detail&record=$recordid";
		return header("Location: $referer");
	}


	function createSalesOrders($db, $id, $order_dates){

			global $current_user;
		
			$date = $order_dates['actual_date']." ".date("H:i:s");

			$sth = $db->pquery('SELECT * FROM app_standing_salesorders WHERE id = ?',array($id));			
			$metrika = $db->query_result($sth, 0, 'metrika');

			$sth2 = $db->pquery('SELECT * FROM app_standing_salesorders_cargo WHERE id = ?',array($id));	

			$user_id = $current_user->id;         
			$stmt = "INSERT INTO vtiger_salesorder (salesorderid,subject,total,subtotal,accountid,sostatus,load_date_from,load_time_from,load_date_to,load_time_to,unload_date_from,unload_time_from,unload_date_to,unload_time_to,source,import_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";				
			
			$stmt2 = "INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,margin,comment,description,cargo_measure,cargo_wgt,cargo_length,cargo_width,cargo_height,pll,source,ordered_weight,ordered_length,ordered_width,ordered_height,revised_weight,revised_length,revised_width,revised_height,m3,service) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			$stmt3 = "INSERT INTO vtiger_sobillads (sobilladdressid,bill_city,bill_code,bill_country,bill_street,load_company,load_contact,load_phone) VALUES (?,?,?,?,?,?,?,?)";
			$stmt4 = "INSERT INTO vtiger_soshipads (soshipaddressid,ship_city,ship_code,ship_country,ship_street,unload_company,unload_contact,unload_phone) VALUES (?,?,?,?,?,?,?,?)";
			$stmt5 = "INSERT INTO vtiger_salesordercf (salesorderid,cf_855,cf_1374, cf_1661,cf_1663, cf_1564,cf_1566, cf_1614, cf_1468, cf_1936) VALUES (?,?,?,?,?,?,?,?,?,?)";
	
			$last_entity_record = $this->getEntityId($db); 
			$insert_entity = $this->insertEntity($db,'SalesOrder', $last_entity_record, $user_id, NULL,'CRM', '', $date);

		 	foreach($sth AS $row){
				$load_date = $order_dates['load_date'];
				$unload_date = $order_dates['unload_date'];
				$db->pquery($stmt,array($last_entity_record,'Užsakymas',$row['price'],$row['price'],$row['account_id'], 'Sent',$load_date,$row['load_time_from'],$load_date,$row['load_time_to'],$unload_date,$row['unload_time_from'],$unload_date,$row['unload_time_to'],'CRM',$date));
				$db->pquery($stmt3,array($last_entity_record,$row['bill_city'],$row['bill_code'],$row['bill_country'],$row['bill_street'],$row['load_company'],$row['load_contact'],$row['load_phone']));
				$db->pquery($stmt4,array($last_entity_record, $row['ship_city'],$row['ship_code'],$row['ship_country'],$row['ship_street'],$row['unload_company'],$row['unload_contact'],$row['unload_phone']));
				$db->pquery($stmt5,array($last_entity_record,$row['order_type'],$row['pricebook_price'],$user_id,$user_id,$row['load_phone'],$row['unload_phone'],$row['dismisservice'],$row['withoutinvoice'],$row['metrika']));
			}

			$i = 1;
			foreach($sth2 AS $row){
				$m3 = round(($row['cargo_length'] * $row['cargo_width'] * $row['cargo_height']) * $row['quantity'],2);
				$pll = ($row['measure'] == 1 ?  $row['quantity'] : 0) ;

				$db->pquery($stmt2,array($last_entity_record, 
																								14244,
																								$i,
																								$row['quantity'],
																								$row['price'],
																								$row['comment'],
																								$row['comment'],                                                  
																								$row['measure'],
																								$row['cargo_wgt'],
																								$row['cargo_length'],
																								$row['cargo_width'],
																								$row['cargo_height'],
																								$pll,
																								'CRM',
																								$row['cargo_wgt'],
																								$row['cargo_length'],
																								$row['cargo_width'],
																								$row['cargo_height'],
																								$row['cargo_wgt'],
																								$row['cargo_length'],
																								$row['cargo_width'],
																								$row['cargo_height'],
																								$m3,
																								2));
				$i++;
			}
			 if($metrika != 1){
			 	$this->exportSalesOrderToMetrika($db,$last_entity_record);
			 }
	}

	function exportSalesOrderToMetrika($adb,$salesorderid){

		$order_sent = 'Sent';  
		$order_not_sent = 'Not Sent'; 
	
		$result_data = array('order' => array()); 
		$stmt = $adb->pquery("SELECT
																						s.salesorderid as order_id,
																						s.total as finish_price,
																						sc.cf_855 as order_type,
																						sc.cf_1374 as price,
																						sc.cf_1376 as agreed_price,
																						sc.cf_1564 as load_phone,
																						sc.cf_1566 as unload_phone,                                
																						s.sostatus AS order_status,   
																						s.status,                     
																						e.createdtime AS order_date,
																						a.accountname AS customer_name,                
																						a.account_no AS customer_code,
																						a.customer_id,
																						s.external_order_id,             
																						s.ivaz_no,
																						s.source,                                
																						e.description AS notes,
																						CONCAT(s.load_date_from, ' ', s.load_time_from) AS load_date_from,
																						CONCAT(s.load_date_to, ' ', s.load_time_to) AS load_date_to, 
																						CONCAT(s.unload_date_from, ' ', s.unload_time_from) AS unload_date_from,
																						CONCAT(s.unload_date_to, ' ', s.unload_time_to) AS unload_date_to,                                
																						l.load_company,
																						l.bill_street AS load_address,
																						l.bill_city AS load_municipality,
																						l.bill_state AS load_region,
																						l.bill_code AS load_zipcode,
																						l.bill_country AS load_country,
																						l.load_contact,
																						h.unload_company,
																						h.ship_street AS unload_address,
																						h.ship_city AS unload_municipality,
																						h.ship_state AS unload_region,
																						h.ship_code AS unload_zipcode,
																						h.ship_country AS unload_country,
																						h.unload_contact,
																						a.legal_vat_code AS unload_vat_code,
																						i.productid,  
																						i.external_load_id,                              
																						i.quantity AS cargo_qty,
																						i.cargo_measure,
																						i.cargo_wgt,
																						i.cargo_length,
																						i.cargo_width,
																						i.cargo_height,
																						i.comment AS cargo_notes,
																						r.serviceid,
																						i.service,
																						i.margin,
																						cf_1936 AS metrika
																										FROM vtiger_salesorder s
																										JOIN vtiger_salesordercf sc ON sc.salesorderid = s.salesorderid
																										JOIN vtiger_crmentity e ON e.crmid = s.salesorderid
																										JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
																										JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid                            
																										JOIN vtiger_users u ON u.id = e.smownerid 
																										JOIN vtiger_account a ON a.accountid = s.accountid 
																										JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
																										LEFT JOIN vtiger_products p ON p.productid = i.productid
																										LEFT JOIN vtiger_service r ON r.serviceid = i.productid                                    
																										WHERE e.deleted = 0 AND s.salesorderid = ?",array($salesorderid));
	
	
		$orders = array();
	
	foreach($stmt AS $row){
	
		$date = date("Y-m-d H:i:s");
		$salesorderid = intval($row['order_id']);
	
		$orders[$salesorderid]['customer_id'] = intval($row['customer_id']);
	
		if(empty($row['external_order_id'])) {
					$orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
					// $orders[$salesorderid]['order_id'] = ''; 
		} else {
					$orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
					$orders[$salesorderid]['order_id'] = $row['external_order_id']; 
		} 
	
		if($row['order_type'] == 'Transporto užsakymas'){
					$shipment_type = 'parcel';
		}else{
					$shipment_type = 'movement';
		}     
	
	
		$orders[$salesorderid]['ivaz_no'] = $row['ivaz_no'];  
		$orders[$salesorderid]['shipment_type'] = $shipment_type;
		$orders[$salesorderid]['notes'] = iconv(mb_detect_encoding($row['notes']), "UTF-8", $row['notes']);
		$orders[$salesorderid]['price'] = (float)$row['price'];
		$orders[$salesorderid]['price_agreed'] = (float)$row['agreed_price'];
		$orders[$salesorderid]['finish_price'] = (float)$row['finish_price'];
	
		$orders[$salesorderid]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'])); 
	
		if(!empty($row['load_date_to'])){       
					$orders[$salesorderid]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to']));   
		} else {
					$orders[$salesorderid]['load_date_to'] = '';      
		}  
	
		$load_company = preg_replace('/[^A-Za-z0-9 ]/', '', $row['load_company']);
		$load_company = str_replace("quot", "", $load_company);   
	
	
		if(!empty($row['customer_id'])) {
					if(empty($row['load_company'])){       
									$orders[$salesorderid]['load_company'] = $row['customer_name'];          
					} else {
									$orders[$salesorderid]['load_company'] = $load_company;
					}        
		} else {
									$orders[$salesorderid]['load_company'] = $row['customer_name']; 
		} 
	
		$load_address = str_replace('&Scaron;', 'Š', $row['load_address']);
		$load_address = str_replace('&scaron;', 'š', $row['load_address']);
	
	
		$orders[$salesorderid]['load_address'] = $load_address;
		$orders[$salesorderid]['load_settlement'] = '';
		$orders[$salesorderid]['load_municipality'] = ($row['load_municipality'] ?: "");   
		$orders[$salesorderid]['load_region'] = $row['load_region'];                      
		$orders[$salesorderid]['load_zipcode'] = $row['load_zipcode'];
	
					//if($row['load_zipcode'] == 'LTU') {
					//$orders[$salesorderid]['load_zipcode'] = ''; 
					//}
	
		$orders[$salesorderid]['load_country'] = 'LTU';  
		// $orders[$salesorderid]['load_country'] = $row['load_country'];  
		$orders[$salesorderid]['load_person'] = $row['load_contact'];  
	
	
		$orders[$salesorderid]['load_phone'] = $row['load_phone'];
	
	
		if(empty($row['load_company'])){       
					$orders[$salesorderid]['load_company_code'] = '';            
		} else {
					$orders[$salesorderid]['load_company_code'] = '';
		}    
	
		$orders[$salesorderid]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from']));
	
		if(!empty($row['unload_date_to'])){       
					$orders[$salesorderid]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to']));  
		} else {
					$orders[$salesorderid]['unload_date_to'] = '';      
		} 
	
	
	
		$unload_company = str_replace("quot", "", $row['unload_company']);   
	
		$unload_company = str_replace('&Scaron;', 'Š', $unload_company);
		$unload_company = str_replace('&scaron;', 'š', $unload_company);
	
		if(empty($row['unload_company'])){       
					$orders[$salesorderid]['unload_company'] = $row['customer_name'];          
		} else {
					$orders[$salesorderid]['unload_company'] = $unload_company;
		}
	
		$unload_address = str_replace('&Scaron;', 'Š', $row['unload_address']);
		$unload_address = str_replace('&scaron;', 'š', $row['unload_address']);
	
		$unload_address = str_replace('Scaron;', 'Š', $unload_address);
		$unload_address = str_replace('scaron;', 'š', $unload_address);
	
		$orders[$salesorderid]['unload_address'] = $unload_address; 
		$orders[$salesorderid]['unload_settlement'] = '';      
		$orders[$salesorderid]['unload_municipality'] = ($row['unload_municipality'] ?: "");        
		$orders[$salesorderid]['unload_region'] = $row['unload_region'];                      
		$orders[$salesorderid]['unload_zipcode'] = $row['unload_zipcode']; 
	
					//if($row['unload_zipcode'] == 'LTU') {
					//$orders[$salesorderid]['unload_zipcode'] = ''; 
					//}            
	
		// $orders[$salesorderid]['unload_country'] = $row['unload_country']; 
		$orders[$salesorderid]['unload_country'] = 'LTU'; 
		$orders[$salesorderid]['unload_person'] = $row['unload_contact'];  ;  
		$orders[$salesorderid]['unload_phone'] = $row['unload_phone'];
	
		if(empty($row['unload_company'])){       
					$orders[$salesorderid]['unload_company_code'] = '';            
		} else {
					$orders[$salesorderid]['unload_company_code'] = '';
		}
	
		if(empty($row['unload_company'])){       
					$orders[$salesorderid]['unload_vat_code'] = '';            
		} else {
					$orders[$salesorderid]['unload_vat_code'] = '';
		}
	
		$orders_items = array(); 
		$orders_services = array(); 
		$delete_service = array(); 
	
	
		if($row['productid'] == 36641){
					$orders_services['service_type_id'] = $row['service'];
					$orders_services['quantity_agreed'] = $row['cargo_qty'];
					$orders_services['price_agreed'] = $row['margin'];
	
					$orders_services['remarks'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);
	
		}
	
	
	
					$orders_items['cargo']['load_id'] = $row['external_load_id'];
					$orders_items['cargo']['update_type'] = 2;
	
		if($row['productid'] != 36641){
					$orders_items['cargo']['cargo_qty'] = ($row['cargo_qty'] < 1 ) ? intval(1.000) : intval($row['cargo_qty']);
					if($row['cargo_measure'] == 0){
							$orders_items['cargo']['cargo_measure'] = 1;
					}else{
							$orders_items['cargo']['cargo_measure'] = $row['cargo_measure'];
			}
	
	
					if($row['cargo_measure'] == null){
									$orders_items['cargo']['cargo_measure'] = 1;
					}
	
					if(($row['cargo_measure'] == 1 OR $row['cargo_measure'] == 2 OR $row['cargo_measure'] == 3) OR ($row['cargo_measure'] == 'pll' OR $row['cargo_measure'] == 'RUS-pll' OR $row['cargo_measure'] == 'FIN-pll')){
									$orders_items['cargo']['volume_unit2'] = intval($row['cargo_qty']);
					}else if($row['cargo_measure'] == 14 OR $row['cargo_measure'] == '1/2 pll'){
                    				$orders_items['cargo']['volume_unit2'] = (float)$row['cargo_qty']/2;
                    }elseif($row['cargo_measure'] == 7 OR $row['cargo_measure'] == 'nestd.'){
								$orders_items['cargo']['volume_unit2'] =(float)ceil( (($row['cargo_length'] * $row['cargo_width']) * $row['cargo_qty'])/0.96);
					}else{
								$orders_items['cargo']['volume_unit2'] = 0;
					}
			
					// itoma
					$orders_items['cargo']['cargo_wgt'] = (float)str_replace(",",".", $row['cargo_wgt']);
					$orders_items['cargo']['cargo_length'] = (float)str_replace(",",".", $row['cargo_length']);      
					$orders_items['cargo']['cargo_width'] = (float)str_replace(",",".", $row['cargo_width']);      
					$orders_items['cargo']['cargo_height'] = (float)str_replace(",",".", $row['cargo_height']);   
	
					$orders_items['cargo']['cargo_ldm'] = sprintf('%0.2f', $row['cargo_ldm']);
					$orders_items['cargo']['cargo_facilities'] = '';        
					$orders_items['cargo']['cargo_termo'] = 0;  
					$orders_items['cargo']['cargo_return_documents'] = intval($row['cargo_return_documents']);
			
					$orders_items['cargo']['cargo_notes'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);
	
					if($row['source'] == 'CRM') {
									if($row['status'] == 'ERROR' OR $row['status'] == ''){ 
													$orders_items['cargo']['update_type'] = 1;
													$insertType = 1; 
									}else{				
													$orders_items['cargo']['update_type'] = 1;  
													$insertType = 1;  								
									}
					}  
		}    
	
	
			if($row['serviceid'] == NULL) {  
							if($row['productid'] != 36641){    
											$orders[$salesorderid]['cargos'][] = $orders_items;        
							}
	
							if($row['productid'] == 36641){  
											$orders[$salesorderid]['additional_services'][] = $orders_services;  
							}          
			}    
																									
					$orders_indexed = array_values($orders);
	
					$result_data['order'] = $orders_indexed;  
	}
									
		$res = json_encode($result_data, JSON_UNESCAPED_UNICODE);   
	
	
		$ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders.php");
	  	curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders.php");           
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders' => $res));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);         
	
		$data = json_decode($result, true);
	
	 
		$state = $data[0];
				
		if($state == 'ERROR') {    
						$order_id = $data[0]['crm_id'];
						$errors = $data[1]['errors'][0]['error_message'];     
				
						//Atnaujiname uzsakymo busena           
						$stmt2 = $adb->pquery('UPDATE vtiger_salesorder SET sostatus = ?, status = ?, errors = ? WHERE salesorderid = ?', array($order_not_sent, $state, $errors, $salesorderid));
	
						//Atnaujiname uzsakymo redagavimo data    
						$stmt3 = $adb->pquery('UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?', array($date, $salesorderid));
                   
						
		} else {    					
				$order_id = $data[0]['order']['crm_id'];
				$external_order_id = $data[0]['order']['order_id'];   
				$shipment_code = $data[0]['order']['shipment_code'];
				$ivaz_no = $data[0]['order']['ivaz_no'];
				$barcode = $data[0]['order']['barcode'];
				$direction = $data[0]['order']['direction']; 
				$status =  $data[0]['status']; 
				$errors = '';
	
	
														//Atnaujiname uzsakymo busena           
						if($insertType == 1){
										$stmt = $adb->pquery('UPDATE vtiger_salesorder SET sostatus = ?, ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?',array($order_sent, $ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
				
						}else{
								$stmt = $adb->pquery('UPDATE vtiger_salesorder SET ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?',array($ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));							
						}
								$stmt2 = 'UPDATE vtiger_inventoryproductrel SET external_order_id = ?, external_load_id = ? WHERE id = ? AND sequence_no = ?';
																				
								$seq = 1;
								foreach($data[0]['order']['loads'] as $load_id){                  
									$adb->pquery($stmt2,array($external_order_id, $load_id, $order_id, $seq ));                     
									$seq++;
								}								
	
	
						//Atnaujiname uzsakymo redagavimo data    
						$stmt3 = $adb->pquery('UPDATE vtiger_crmentity SET label = ?, modifiedtime = ? WHERE crmid = ?',array($shipment_code, $date, $order_id));
		}
	}


	function getEntityId($db){
    $sql = "SELECT id FROM `vtiger_crmentity_seq`";
    $sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
    $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
    $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
    $lock3 = 'UNLOCK TABLES';

    $db->pquery($lock, array()); 
      $get_id = $db->pquery($sql, array()); 
      $seq = $db->query_result($get_id,0,'id');    
    $db->pquery($lock3, array());    

    $new_seq = $seq + 1;   

    $db->pquery($lock2, array()); 
     $db->pquery($sql2, array($new_seq)); 
    $db->pquery($lock3, array()); 

    return $new_seq;
  }


	function insertEntity($db,$setype, $entity_id, $user_id, $description,$source, $label, $date){
    $sql = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)';
    $db->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description,$source, $label, $date, $date)); 
  }


}
