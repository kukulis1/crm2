<?php

class Accounts_Field_Model extends Calendar_Field_Model {

  public function getFieldDataType() {
    if($this->getName() == 'pricebook' ) {
    return 'multireference';
    }
    return parent::getFieldDataType();
    
    }

}
