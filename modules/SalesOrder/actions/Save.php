﻿<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/


class SalesOrder_Save_Action extends Inventory_Save_Action {
    
public function saveRecord($request) {
        $recordModel = $this->getRecordModelFromRequest($request);
		if($request->get('imgDeleted')) {
			$imageIds = $request->get('imageid');
			foreach($imageIds as $imageId) {
				$status = $recordModel->deleteImage($imageId);
			}
        }

        // itoma
        $check_salesorder = $this->checkSalesOrderHasInvoice();


        if(!$check_salesorder){
		 $recordModel->save();               
         $mode = $recordModel->get('mode');		
          $this->updateOrderTime($recordModel); 
          $this->checkPriceAgreeed($recordModel);           
            if($request->get('cf_1936') == 'on'){                 
                $this->generateInternalShipmentCode($recordModel);                      
            }else{             	                          
          
                if($_SERVER['REMOTE_ADDR'] != '127.0.0.1'){              
                    $this->SaveOrderMetrika($recordModel);     
                }           
                
                if($mode != 'edit'){
                    $this->addOrderCreateService($recordModel); 
                }   
            }            
            $this->addLoadersToStevedoring($recordModel); 
            $this->updateRevisedDimensions($recordModel); 

            if(!empty($request->get('cf_2674'))){
                $this->createInternalClaim($recordModel);
            }
           

        }else{
            header('Location: /');
        }     
        
        
		if($request->get('relationOperation')) {
			$parentModuleName = $request->get('sourceModule');
			$parentModuleModel = Vtiger_Module_Model::getInstance($parentModuleName);
			$parentRecordId = $request->get('sourceRecord');
			$relatedModule = $recordModel->getModule();
			$relatedRecordId = $recordModel->getId();
			if($relatedModule->getName() == 'Events'){
				$relatedModule = Vtiger_Module_Model::getInstance('Calendar');
			}

			$relationModel = Vtiger_Relation_Model::getInstance($parentModuleModel, $relatedModule);
			$relationModel->addRelation($parentRecordId, $relatedRecordId);
		}
		$this->savedRecordId = $recordModel->getId();
		return $recordModel;
}

public function orders_log($log_variable, $logfile) {       
      
    $log_json = '--------------------------------' . date("Y-m-d H:i:s") . '--------------------------------' . "\n" . json_encode($log_variable) . "\n" . "\n";
    
    file_put_contents('/home/parnasas/public_html/v1/external_data' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, $log_json, FILE_APPEND);
	
    if (filesize('/home/parnasas/public_html/v1/external_data' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile) > 1024 * 1024 * 10) {
        copy('/home/parnasas/public_html/v1/external_data' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile, 
             '/home/parnasas/public_html/v1/external_data' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('Y-m-d_H-i-s') . '_' . $logfile);
        unlink('/home/parnasas/public_html/v1/external_data' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $logfile);
    }
}


public function createInternalClaim($model){   
    $recordId = $model->getId(); 

    if(!$this->checkAreSalesOrderHaveClaim($recordId)){
        $accountid = $model->get('account_id'); 
        $recordModel = Vtiger_Record_Model::getCleanInstance('Claims');
        $recordModel->set('mode', '');
        $recordModel->set('cf_1837', 'Naujas');   
        $recordModel->set('claims_tks_source', 'Vidinė');  
        $recordModel->set('claims_tks_order_number', $recordId);  
        $recordModel->set('cf_20023', $accountid);  
        $recordModel->save();

        $newClaimId = $recordModel->getId();
        $this->setClaimNumber($newClaimId);
    }
}

public function checkAreSalesOrderHaveClaim($recordId){
    $adb = PearDatabase::getInstance();
    $check_claims = $adb->pquery("SELECT * FROM vtiger_claims WHERE claims_tks_order_number = ?", array($recordId));
    
    if($adb->num_rows($check_claims)){
        return true;
    }
    return false;
}

public function setClaimNumber($recordId){
    $adb = PearDatabase::getInstance();
    $date = date('Y-m-d');
    $prefix = 'CLAIM-';	
    $query = "SELECT claimsno 
              FROM vtiger_claims
              LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_claims.claimsid 							 
              WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$date'  AND vtiger_crmentity.deleted = 0  AND claimsno != ''
              ORDER BY vtiger_claims.claimsid  
              DESC LIMIT 1";
            
    $check_cur =  $adb->pquery($query, array());
    $curid =  $adb->query_result($check_cur, 0, 'claimsno');

    if(!empty($curid)){
        $curid = substr($curid, -3);
    }else{
        $req_no = '001';
    }	

    $todayDate = date('ymd');

    if(!empty($curid)){
        $strip = strlen($curid) - strlen($curid + 1);
        if ($strip < 0)
            $strip = 0;
        $temp = str_repeat("0", $strip);
        $req_no.= $temp . ($curid + 1);
    }

    $prev_inv_no = $prefix.$todayDate."-".$req_no;

    $adb->pquery("UPDATE vtiger_claims SET claimsno = ? WHERE claimsid=?", array($prev_inv_no, $recordId));
    $adb->pquery("UPDATE vtiger_crmentity SET label = ? WHERE crmid = ?", array($prev_inv_no, $recordId));
    $adb->pquery("INSERT INTO  vtiger_claims_seen (claim_id, seen, seen_time, reviewed_person) VALUES (?,?,?,?)", array($recordId,0,'',''));
}

public function checkSalesOrderHasInvoice(){
    global $_REQUEST;
    $adb = PearDatabase::getInstance();
    $order_id = $_REQUEST['record']; 
    
    if(!empty($order_id)){
    $sql = "SELECT * FROM vtiger_salesorder
                     LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
                     WHERE  vtiger_invoice_salesorders_list.salesorderid = ".$order_id; 

    $result = $adb->pquery($sql, array());

        $response = $adb->fetch_array($result);
    }else{      
        $response = false;
    }
    return $response;
}

public function generateInternalShipmentCode($recordModel){
    $adb = PearDatabase::getInstance();
    $record_id = $recordModel->getId();  

    $get_salesorder_info = $adb->pquery("SELECT s.accountid,cf_855 AS type, customer_id, status, shipment_code
                                         FROM vtiger_salesorder s 
                                         LEFT JOIN vtiger_salesordercf cf ON s.salesorderid=cf.salesorderid 
                                         LEFT JOIN vtiger_account a ON a.accountid=s.accountid
                                         WHERE s.salesorderid = ?", array($record_id));

    $accountid = $adb->query_result($get_salesorder_info, 0, 'accountid');
    $type = $adb->query_result($get_salesorder_info, 0, 'type');
    $customer_id = $adb->query_result($get_salesorder_info, 0, 'customer_id');

    $status = $adb->query_result($get_salesorder_info, 0, 'status');
    $old_shipment_code = $adb->query_result($get_salesorder_info, 0, 'shipment_code');

    if($status == 'ERROR' || empty($old_shipment_code)){
        if($type == 'Perkraustymo užsakymas'){
            $select = 'p_shipment_code_seq';       
            $begin = "P";

        }else if($type == 'Transporto užsakymas'){
            $select = 't_shipment_code_seq';       
            $begin = "T";
            
        }else if($type == 'Sandėliavimo užsakymas'){
            $select = 's_shipment_code_seq';       
            $begin = "S";

        }else{
            return false;
        }

        $get_shipment_code = $adb->pquery("SELECT $select FROM vtiger_internal_shipment_code WHERE accountid = ?", array($accountid));
        $shipment_code = $adb->query_result($get_shipment_code, 0, $select); 
        $num_rows = $adb->num_rows($get_shipment_code);  

        if(!empty($shipment_code)){

            $get_number = '';
            $move = true;

            for($e = 0; $e < strlen($shipment_code); $e++){   
    
                if($shipment_code[$e] == 0 && $move){
                    continue;
                }
                
                $move = false;
                $get_number .= (string)$shipment_code[$e];
            }

            $shipment_code = $get_number;

            $temp = $shipment_code+1;
            $len = strlen($temp);
            $suffix = '';
            for($i = 0; $i < strlen($shipment_code)-$len; $i++){
                $suffix .= '0';
            }
            $req_no = $begin.'-'.$customer_id.'-'.$suffix.$temp;
            $adb->pquery("UPDATE vtiger_internal_shipment_code SET $select = ? WHERE accountid = ?",array($suffix.$temp, $accountid));       

        }else{       
            $req_no = $begin.'-'.$customer_id.'-00001'; 
        
            if($num_rows > 0){
                $adb->pquery("UPDATE vtiger_internal_shipment_code SET $select = ? WHERE accountid = ?",array('00001', $accountid));  
            }else{
            $adb->pquery("INSERT INTO vtiger_internal_shipment_code (accountid, $select) VALUES (?,?)",array($accountid, '00001'));
            }      
        }

        $adb->pquery("UPDATE vtiger_salesorder SET sostatus = ?, shipment_code = ?, external_order_id = ? WHERE salesorderid = ?", array('Created',$req_no, 1, $record_id));
        $adb->pquery("UPDATE vtiger_crmentity SET label = ?  WHERE crmid = ?", array($req_no, $record_id));   
    }
}

public function updateOrderTime($recordModel){
    global $_REQUEST; 
    $adb = PearDatabase::getInstance();
    $record_id = $recordModel->getId();  

    $load_date_comma = explode(",", $_REQUEST['load_date_from']);
    $unload_date_comma = explode(",", $_REQUEST['unload_date_from']);
           
    $load_date_from = $load_date_comma[0];
    $load_date_to = $load_date_comma[1];

    $unload_date_from = $unload_date_comma[0];
    $unload_date_to = $unload_date_comma[1];
    
    $load_time_from = $_REQUEST['load_time_from'];
    $load_time_to = $_REQUEST['load_time_to'];

    $unload_time_from = $_REQUEST['unload_time_from'];
    $unload_time_to = $_REQUEST['unload_time_to'];

    $sql = "UPDATE vtiger_salesorder SET  load_date_from = ?, load_date_to = ?, load_time_from = ?, load_time_to = ?, unload_date_from = ?, unload_date_to = ?, unload_time_from = ?, unload_time_to = ? WHERE salesorderid = ?";
    $adb->pquery($sql, array($load_date_from,$load_date_to,$load_time_from,$load_time_to,$unload_date_from,$unload_date_to,$unload_time_from,$unload_time_to,$record_id));
 }

  public function checkPriceAgreeed($recordModel){
    global $_REQUEST;
    $db = PearDatabase::getInstance();
    if($_REQUEST['cf_1376'] > 0){
        $db->pquery("UPDATE vtiger_salesorder SET total = ? WHERE salesorderid = ?", array($_REQUEST['cf_1376'], $recordModel->getId()));
    }    
 }

 public function addOrderCreateService($recordModel){
    $adb = PearDatabase::getInstance();
    $record_id = $recordModel->getId();  

    $get_client_id_query = "SELECT accountid FROM vtiger_salesorder WHERE salesorderid = ?";
    $result = $adb->pquery($get_client_id_query, array($record_id));
    $accountid = $adb->query_result($result,0,'accountid');

    $check_customer_info_query = "SELECT cf_2548,cf_2550 FROM vtiger_accountscf WHERE accountid = ?";
    $result2 = $adb->pquery($check_customer_info_query, array($accountid));
    $service_status = $adb->query_result($result2,0,'cf_2548');
    $service_price = $adb->query_result($result2,0,'cf_2550');

    $check_sequence_no_query = "SELECT id FROM vtiger_inventoryproductrel WHERE id = ?";
    $result3 = $adb->pquery($check_sequence_no_query, array($record_id));
    $num_rows = $adb->num_rows($result3);

    $insert_service_query = "INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,margin,service,inventory_type) VALUES (?,?,?,?,?,?,?)";

    if($service_status){ 
        $price = ($service_price > 0 ?:2);
        $adb->pquery($insert_service_query, array($record_id,36641,$num_rows+1,1,$price,32,'order_entry'));
    }
}


public function addLoadersToStevedoring($recordModel){
    $adb = PearDatabase::getInstance();
    $record_id = $recordModel->getId();  
    $sql = "SELECT cf_1687,salesorderid
                            FROM vtiger_salesordercf							
                            WHERE salesorderid = ?";  

    $sql2 = "UPDATE vtiger_stevedoring SET stevedoring_tks_namelastname = ? WHERE stevedoringno = ?";		

    $result = $adb->pquery($sql, array($record_id));
    while($row = $adb->fetch_array($result)) {
        $adb->pquery($sql2, array($row['cf_1687'],$row['salesorderid']));
    }
}


public function updateRevisedDimensions($recordModel){
    $adb = PearDatabase::getInstance();
    $record_id = $recordModel->getId();

    $select_inventory = 'SELECT id,sequence_no,quantity,cargo_measure,cargo_wgt,cargo_length,cargo_width,cargo_height,pll FROM vtiger_inventoryproductrel where id = ? AND productid = 14244';

    $update_inventory = 'UPDATE vtiger_inventoryproductrel SET ordered_weight = ?,
                                                               ordered_length = ?,
                                                               ordered_width = ?,
                                                               ordered_height = ?,
                                                               revised_measure = ?,
                                                               revised_quantity = ?,
                                                               revised_weight = ?,
                                                               revised_length = ?,
                                                               revised_width = ?,
                                                               revised_height = ?,
                                                               inventory_type = ?
                                                              WHERE id = ? AND sequence_no = ? AND productid = 14244';

    $result = $adb->pquery($select_inventory, array($record_id));

    while($row = $adb->fetch_array($result)) {
        $cargo_weight = ($row['cargo_wgt'] == 0 ? 1 : $row['cargo_wgt']);

        $adb->pquery($update_inventory, array($cargo_weight,
                                              $row['cargo_length'],
                                              $row['cargo_width'],
                                              $row['cargo_height'],
                                              $row['cargo_measure'],
                                              $row['quantity'],
                                              $cargo_weight,
                                              $row['cargo_length'],
                                              $row['cargo_width'],
                                              $row['cargo_height'],
                                              'transport',
                                              $row['id'],
                                              $row['sequence_no']                                             
                                            ));
    }

}
 
public function SaveOrderMetrika($recordModel){        
    global $_REQUEST;  

	    $adb = PearDatabase::getInstance();
            $order_id = $recordModel->getId();  
            $salesorderid =  $recordModel->getId();
            $mode = $recordModel->get('mode');
            $order_sent = 'Sent';  
            $order_not_sent = 'Not Sent'; 
            
            $result_data = array('order' => array()); 
            
            $sql = "SELECT
                                s.salesorderid as order_id,
                                s.total as finish_price,
                                sc.cf_855 as order_type,
                                sc.cf_1374 as price,
                                sc.cf_1376 as agreed_price,
                                sc.cf_1564 as load_phone,
                                sc.cf_1566 as unload_phone,                                
                                s.sostatus AS order_status,   
                                s.status,                     
                                e.createdtime AS order_date,
                                a.accountname AS customer_name,                
                                a.account_no AS customer_code,
                                a.customer_id,
                                s.external_order_id,             
                                s.ivaz_no,
                                s.source,                                
                                e.description AS notes,
                                CONCAT(s.load_date_from, ' ', s.load_time_from) AS load_date_from,
                                CONCAT(s.load_date_to, ' ', s.load_time_to) AS load_date_to, 
                                CONCAT(s.unload_date_from, ' ', s.unload_time_from) AS unload_date_from,
                                CONCAT(s.unload_date_to, ' ', s.unload_time_to) AS unload_date_to,                                
                                l.load_company,
                                l.bill_street AS load_address,
                                l.bill_city AS load_municipality,
                                l.bill_state AS load_region,
                                l.bill_code AS load_zipcode,
                                l.bill_country AS load_country,
                                l.load_contact,
                                h.unload_company,
                                h.ship_street AS unload_address,
                                h.ship_city AS unload_municipality,
                                h.ship_state AS unload_region,
                                h.ship_code AS unload_zipcode,
                                h.ship_country AS unload_country,
                                h.unload_contact,
                                a.legal_vat_code AS unload_vat_code,
                                i.productid,  
                                i.external_load_id,                              
                                i.quantity AS cargo_qty,
                                i.cargo_measure,
                                i.cargo_wgt,
                                i.cargo_length,
                                i.cargo_width,
                                i.cargo_height,
                                i.comment AS cargo_notes,
                                r.serviceid,
                                i.service,
                                i.margin
                                    FROM vtiger_salesorder s
                                    JOIN vtiger_salesordercf sc ON sc.salesorderid = s.salesorderid
                                    JOIN vtiger_crmentity e ON e.crmid = s.salesorderid
                                    JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
                                    JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid                            
                                    JOIN vtiger_users u ON u.id = e.smownerid 
                                    JOIN vtiger_account a ON a.accountid = s.accountid 
                                    JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
                                    LEFT JOIN vtiger_products p ON p.productid = i.productid
                                    LEFT JOIN vtiger_service r ON r.serviceid = i.productid                                    
                                    WHERE e.deleted = 0 AND s.salesorderid = ? 
                                    ";

            $result = $adb->pquery($sql, array($order_id));

            $orders = array();
            
        while($row = $adb->fetch_array($result)) {


                    
            $date = date("Y-m-d H:i:s");
            $order_id = intval($row['order_id']);
		
            $orders[$order_id]['customer_id'] = intval($row['customer_id']);

            if(empty($row['external_order_id'])) {
                $orders[$order_id]['crm_id'] = intval($row['order_id']);  
                // $orders[$order_id]['order_id'] = ''; 
            } else {
                $orders[$order_id]['crm_id'] = intval($row['order_id']);  
                $orders[$order_id]['order_id'] = $row['external_order_id']; 
            } 
            
            if($row['order_type'] == 'Transporto užsakymas'){
                $shipment_type = 'parcel';
            }else{
                $shipment_type = 'movement';
            }     
            

            $orders[$order_id]['ivaz_no'] = $row['ivaz_no'];  
            $orders[$order_id]['shipment_type'] = $shipment_type;
            $orders[$order_id]['notes'] = iconv(mb_detect_encoding($row['notes']), "UTF-8", $row['notes']);
            $orders[$order_id]['price'] = (float)$row['price'];
            $orders[$order_id]['price_agreed'] = (float)$row['agreed_price'];
            $orders[$order_id]['finish_price'] = (float)$row['finish_price'];
            
            $orders[$order_id]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'])); 

            if(!empty($row['load_date_to'])){       
                $orders[$order_id]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to']));   
            } else {
                $orders[$order_id]['load_date_to'] = '';      
            }  

            $load_company = preg_replace('/[^A-Ža-ž0-9 ]/', '', $row['load_company']);
            $load_company = str_replace("quot", "", $load_company);     

            if(!empty($row['customer_id'])) {
                if(empty($row['load_company'])){       
                    $orders[$order_id]['load_company'] = $row['customer_name'];          
                } else {
                    $orders[$order_id]['load_company'] = $load_company;
                }        
            } else {
                    $orders[$order_id]['load_company'] = $row['customer_name']; 
            } 
            
            $load_address = str_replace('&Scaron;', 'Š', $row['load_address']);
            $load_address = str_replace('&scaron;', 'š', $row['load_address']);


            $orders[$order_id]['load_address'] = $load_address;
            $orders[$order_id]['load_settlement'] = '';
            $orders[$order_id]['load_municipality'] = $row['load_municipality'];   
            $orders[$order_id]['load_region'] = $row['load_region'];                      
            $orders[$order_id]['load_zipcode'] = $row['load_zipcode'];
            
                //if($row['load_zipcode'] == 'LTU') {
                //$orders[$order_id]['load_zipcode'] = ''; 
                //}
            
            $orders[$order_id]['load_country'] = $row['load_country'];  
            $orders[$order_id]['load_person'] = $row['load_contact'];  


            $orders[$order_id]['load_phone'] = $row['load_phone'];
  

            if(empty($row['load_company'])){       
                $orders[$order_id]['load_company_code'] = '';            
            } else {
                $orders[$order_id]['load_company_code'] = '';
            }    

            $orders[$order_id]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from']));

            if(!empty($row['unload_date_to'])){       
                $orders[$order_id]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to']));  
            } else {
                $orders[$order_id]['unload_date_to'] = '';      
            } 

        
             $unload_company = preg_replace('/[^A-Ža-ž0-9 ]/', '', $row['unload_company']); 

             $unload_company = str_replace("quot", "", $unload_company);         

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_company'] = $row['customer_name'];          
            } else {
                $orders[$order_id]['unload_company'] = $unload_company;
            }

            $unload_address = str_replace('&Scaron;', 'Š', $row['unload_address']);
            $unload_address = str_replace('&scaron;', 'š', $row['unload_address']);

            $orders[$order_id]['unload_address'] = $unload_address; 
            $orders[$order_id]['unload_settlement'] = '';        
            $orders[$order_id]['unload_municipality'] = $row['unload_municipality'];         
            $orders[$order_id]['unload_region'] = $row['unload_region'];                      
            $orders[$order_id]['unload_zipcode'] = $row['unload_zipcode']; 
            
                //if($row['unload_zipcode'] == 'LTU') {
                //$orders[$order_id]['unload_zipcode'] = ''; 
                //}            
            
            $orders[$order_id]['unload_country'] = $row['unload_country']; 
            $orders[$order_id]['unload_person'] = $row['unload_contact'];  ;  
            $orders[$order_id]['unload_phone'] = $row['unload_phone'];

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_company_code'] = '';            
            } else {
                $orders[$order_id]['unload_company_code'] = '';
            }

            if(empty($row['unload_company'])){       
                $orders[$order_id]['unload_vat_code'] = '';            
            } else {
                $orders[$order_id]['unload_vat_code'] = '';
            }

            $orders_items = array(); 
            // $orders_services = array(); 
            // $delete_service = array(); 
          

            // if($row['productid'] == 36641){
            //     $orders_services['service_type_id'] = $row['service'];
            //     $orders_services['quantity_agreed'] = $row['cargo_qty'];
            //     $orders_services['price_agreed'] = $row['margin'];

            //     $orders_services['remarks'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

            //     if($mode == 'edit'){
            //         $service_action = 0;
            //         for($i=1; $i <= $_REQUEST['totalProductCount']; $i++){
            //            $hide =  $_REQUEST['service_hide_id'.$i];
            //            $selected =  $_REQUEST['service_select'.$i];                        
            //         }
            //         if($selected != $hide) $service_action = $hide;

            //         if($service_action != 0){
            //             $delete_service['service_type_id'] = $service_action;
            //             $delete_service['delete_it'] = 1;
            //         }
            //     }
            // }

            
         
            $orders_items['cargo']['load_id'] = $row['external_load_id'];
            

            if($row['productid'] != 36641){
                $orders_items['cargo']['cargo_qty'] = ($row['cargo_qty'] < 1 ) ? intval(1.000) : intval($row['cargo_qty']);
                if($row['cargo_measure'] == 0){
                	$orders_items['cargo']['cargo_measure'] = 1;
                }else{
                	$orders_items['cargo']['cargo_measure'] = $row['cargo_measure'];
            	}


                if($row['cargo_measure'] == null){
                    $orders_items['cargo']['cargo_measure'] = 1;
                }

                if(($row['cargo_measure'] == 1 OR $row['cargo_measure'] == 2 OR $row['cargo_measure'] == 3 OR $row['cargo_measure'] == 13) OR ($row['cargo_measure'] == 'pll' OR $row['cargo_measure'] == 'RUS-pll' OR $row['cargo_measure'] == 'FIN-pll' OR $row['cargo_measure'] == 'kont.')){
                    $orders_items['cargo']['volume_unit2'] = intval($row['cargo_qty']);
                }else if($row['cargo_measure'] == 14 OR $row['cargo_measure'] == '1/2 pll'){
                    $orders_items['cargo']['volume_unit2'] = (float)$row['cargo_qty']/2;
                }elseif($row['cargo_measure'] == 7 OR $row['cargo_measure'] == 'nestd.'){
                    $orders_items['cargo']['volume_unit2'] = (float)ceil( (($row['cargo_length'] * $row['cargo_width']) * $row['cargo_qty'])/0.96);
                }else{
                    $orders_items['cargo']['volume_unit2'] = 0;
                }               

                
                // itoma
                $orders_items['cargo']['cargo_wgt'] = (float)str_replace(",",".", $row['cargo_wgt']);
                $orders_items['cargo']['cargo_length'] = (float)str_replace(",",".", $row['cargo_length']);      
                $orders_items['cargo']['cargo_width'] = (float)str_replace(",",".", $row['cargo_width']);      
                $orders_items['cargo']['cargo_height'] = (float)str_replace(",",".", $row['cargo_height']);   

                $orders_items['cargo']['cargo_ldm'] = sprintf('%0.2f', $row['cargo_ldm']);
                $orders_items['cargo']['cargo_facilities'] = '';        
                $orders_items['cargo']['cargo_termo'] = 0;  
                $orders_items['cargo']['cargo_return_documents'] = intval($row['cargo_return_documents']);
              
                $orders_items['cargo']['cargo_notes'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

                $orders_items['cargo']['update_type'] = 2;

                if($row['source'] == 'CRM' || $row['source'] == 'Excel') {
                    if($row['status'] == 'ERROR' OR $row['status'] == ''){ 
                        $orders_items['cargo']['update_type'] = 1;
                        $insertType = 1; 
                    }else{
                        if($mode == 'edit'){                        	 
                             $orders_items['cargo']['update_type'] = 2;   
                             $insertType = 2; 
                        }else{
                            $orders_items['cargo']['update_type'] = 1;  
                            $insertType = 1;  
                        }
                    }
                }  
            }    
      
            
            if($row['serviceid'] == NULL) {  
                if($row['productid'] == 14244){    
                    $orders[$order_id]['cargos'][] = $orders_items;        
                }

                // if($row['productid'] == 36641){  
                //     $orders[$order_id]['additional_services'][] = $orders_services;  
                //     if($mode == 'edit'){ 
                //         if($service_action != 0){
                //             $orders[$order_id]['additional_services'][] = $delete_service;  
                //         }
                //     } 
                // }          
            }    
                    
            
            $orders_indexed = array_values($orders);

            $result_data['order'] = $orders_indexed;    

        }             

            $res = json_encode($result_data, JSON_UNESCAPED_UNICODE);  


            if($_REQUEST['assign_to_last_order']){              
                $assign = 1;
            }else{              
                $assign = 0;
            } 
        //  if($order_id == 16506259){
        //   echo '<pre>';
        //   print_R($res);
        //   echo '</pre>';
        //     echo "insert type ". $insertType;
        //   die;
        // }


          
         
        $ch = curl_init(); 
        // curl_setopt($ch, CURLOPT_URL, "https://demo-uzsakymai.parnasas.lt/import/crm/orders.php");
        curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders.php");           
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'assign' => $assign,  'orders' => $res));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);     

        $data = json_decode($result, true);

        $state = $data[0];
        
        if($state == 'ERROR') {          
            $logfile = 'orders_error_out.log';
            $this->orders_log($result_data, $logfile);  
            $this->orders_log($data, $logfile);
            
            $order_id = $data[0]['crm_id'];
            $errors = $data[1]['errors'][0]['error_message'];     
            
                    //Atnaujiname uzsakymo busena           
                    $sql = 'UPDATE vtiger_salesorder SET sostatus = ?, status = ?, errors = ? WHERE salesorderid = ?';
                    $result = $adb->pquery($sql, array($order_not_sent, $state, $errors, $salesorderid));

                    //Atnaujiname uzsakymo redagavimo data    
                    $sql = 'UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?';
                    $result = $adb->pquery($sql, array($date, $salesorderid));                     
            
        } else {    
            $logfile = 'orders_good_out.log';
            $this->orders_log($result_data, $logfile);  
            $this->orders_log($data, $logfile);  
            
            $order_id = $data[0]['order']['crm_id'];
            $external_order_id = $data[0]['order']['order_id'];   
            $shipment_code = $data[0]['order']['shipment_code'];
            $ivaz_no = $data[0]['order']['ivaz_no'];
            $barcode = $data[0]['order']['barcode'];
            $direction = $data[0]['order']['direction']; 
            $status =  $data[0]['status']; 
            $errors = '';


            //Atnaujiname uzsakymo busena           
            if($insertType == 1){
                $sql = 'UPDATE vtiger_salesorder SET sostatus = ?, ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?';
                $result = $adb->pquery($sql, array($order_sent, $ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
            }else{
                $sql = 'UPDATE vtiger_salesorder SET ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?';
                $result = $adb->pquery($sql, array($ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
            }

            $sql2 = 'UPDATE vtiger_inventoryproductrel SET external_order_id = ?, external_load_id = ?, productid = 14244 WHERE id = ? AND sequence_no = ?';
                        
            $seq = 1;
            foreach($data[0]['order']['loads'] as $load_id){                  
                $result = $adb->pquery($sql2, array($external_order_id, $load_id, $order_id, $seq ));
                $this->insertFlags($external_order_id, $load_id, $order_id,$seq);
                $seq++;
            }      


            //Atnaujiname uzsakymo redagavimo data    
            $sql = 'UPDATE vtiger_crmentity SET label = ?, modifiedtime = ? WHERE crmid = ?';
            $result = $adb->pquery($sql, array($shipment_code, $date, $order_id));
        }
            
    }

    public function insertFlags($external_order_id, $load_id, $order_id,$i){
        $adb = PearDatabase::getInstance();
        global $_REQUEST; 
        
        $minifest = ($_REQUEST['minifest'.$i] ? 1 : 0);
        $cmr = ($_REQUEST['cmr'.$i] ? 1: 0);
        $invoice = ($_REQUEST['invoice'.$i] ? 1 : 0);

        $query = 'SELECT flag_id FROM vtiger_inventoryproductrel_flags WHERE external_order_id = ? AND external_load_id = ?';
        $result = $adb->pquery($query, array($external_order_id, $load_id));

        if($result->rowCount()){
            $sql = 'UPDATE vtiger_inventoryproductrel_flags SET minifest = ?, cmr = ?, invoice = ? WHERE external_order_id = ? AND external_load_id = ?';
            $adb->pquery($sql,array($minifest,$cmr,$invoice,$external_order_id,$load_id));
        }else{    
            $sql = 'INSERT INTO vtiger_inventoryproductrel_flags (flag_id,external_order_id,external_load_id,minifest,cmr,invoice,import_date) VALUES (?,?,?,?,?,?,?)';
            $adb->pquery($sql,array($order_id,$external_order_id,$load_id,$minifest,$cmr,$invoice,date("Y-m-d H:i:s")));
        }     
        
        $this->sentFlags($external_order_id, $load_id); 

        
    }

    public function sentFlags($external_order_id, $load_id){
        $adb = PearDatabase::getInstance();
        $query = 'SELECT minifest,cmr,invoice FROM vtiger_inventoryproductrel_flags WHERE external_order_id = ? AND external_load_id = ?';
        $result = $adb->pquery($query,array($external_order_id, $load_id)); 
        $order = array();               

       $minifest = $adb->query_result($result,0,'minifest');
       $cmr = $adb->query_result($result,0,'cmr');
       $invoice = $adb->query_result($result,0,'invoice');
      
            if($minifest > 0){
                $order['load_flags'][] = array('load_id' => $load_id, 'key' => 'flg_minifest','value' => $minifest);
            }
            if($cmr > 0){
                $order['load_flags'][] = array('load_id' => $load_id, 'key' => 'flg_cmr', 'value' => $cmr);
            }
            if($invoice > 0){
                $order['load_flags'][] = array('load_id' => $load_id, 'key' => 'flg_invoice','value' => $invoice);
            }
            $res = json_encode($order);
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/load_flags.php");                  
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'load_flags' => $res));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $responce = curl_exec($ch);
            curl_close($ch);
            
        
    }

}
