<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $salesorderid = $_POST['salesorderid'];
  $salesorder = $conn->query("SELECT DISTINCT s.shipment_code, s.salesorderid, FORMAT(s.total,2) AS total, a.accountname, e.createdtime , cf_1376 as price_agreed, cf_1378 as priceagreed_desc,cf_1374
                                      FROM vtiger_salesorder s
                                      LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
                                      LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                                      LEFT JOIN vtiger_account a ON s.accountid=a.accountid
                                      WHERE e.deleted = 0 AND e.setype = 'SalesOrder' AND s.salesorderid IN ($salesorderid)");


  $changes = $conn->query("SELECT b.crmid, d.prevalue, d.postvalue,d.fieldname
                              FROM vtiger_crmentity e                                                                                             
                              LEFT JOIN vtiger_modtracker_basic b ON e.modifiedby=b.whodid
                              LEFT JOIN vtiger_modtracker_detail d ON b.id=d.id	
                              WHERE e.deleted = 0 AND e.setype = 'SalesOrder' AND e.crmid IN ($salesorderid) AND b.module = 'SalesOrder' AND b.crmid IN ($salesorderid) AND (d.fieldname = 'hdnGrandTotal' OR d.fieldname = 'cf_1297')");

  $salesorders_array = array();
  $changes_arr = array();

  foreach($changes as $row2){
    if($row2['fieldname'] == 'hdnGrandTotal'){
      $changes_arr[$row2['crmid']]['price'] = ROUND($row2['prevalue'],2);
    }elseif($row2['fieldname'] == 'cf_1297'){
      $changes_arr[$row2['crmid']]['pricebook'] = $row2['postvalue'];
    }
  }

  foreach($salesorder as $row){
    $salesorders_array[] = array('salesorderid' => $row['salesorderid'],
                                                    'shipment_code' => $row['shipment_code'],
                                                    'total' => $row['total'],
                                                    'accountname' => $row['accountname'],
                                                    'price_agreed' => $row['price_agreed'],
                                                    'priceagreed_desc' => $row['priceagreed_desc'],
                                                    'pricebook' => $changes_arr[$row['salesorderid']]['pricebook'],
                                                    'old_total' => ($changes_arr[$row['salesorderid']]['price'] ?: $row['cf_1374']),
                                                    'createdtime' => $row['createdtime']
    );
  }

  echo json_encode($salesorders_array);    
}else{
  http_response_code(404);
}
  

