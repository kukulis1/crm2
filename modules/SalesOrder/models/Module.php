<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class SalesOrder_Module_Model extends Inventory_Module_Model{

  public function getSalesOrderReport($dateFilter='') {
    $db = PearDatabase::getInstance();
    $today = date("Y-m-d");

		$sql = "SELECT DISTINCT s.sostatus, COUNT(DISTINCT s.salesorderid) as orders_num, GROUP_CONCAT(DISTINCT salesorderid) AS salesorderid
                      FROM vtiger_salesorder s	
                      LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
                      WHERE e.deleted = 0 AND setype = 'SalesOrder' ";

		$currentUser = Users_Record_Model::getCurrentUserModel();
    $params = array();

		//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";
			$params[] = $dateFilter['start'];
      $params[] = $dateFilter['end'];
      $show_filter = 	"Nuo: ".$dateFilter['start']." Iki: ".$dateFilter['end'];
		}else{
      $sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') =  ? ";
      $params[] = $today;
      $show_filter = $today." dienos";
    }

    $sql2 = $sql;

    $sql .= ' GROUP BY s.sostatus ORDER BY orders_num DESC';	

      
    $result = $db->pquery($sql,$params);   
    $result2 = $db->pquery($sql2,$params);   
		
		$noOfRows = $db->num_rows($result);

		$salesOrders = array();
		for($i=0; $i<$noOfRows; $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$salesOrders['orders'][] = $row;			
    }   
    $salesOrders['filter'] = $show_filter;
    $salesOrders['count'] = $db->query_result($result2,0,'orders_num');


		return $salesOrders;
  }

  
	function getDatesFromRange($start, $end) { 
    $array = array();  
    $interval = new DateInterval('P1D');  
    $realEnd = new DateTime($end); 
    $realEnd->add($interval);   
    $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
    foreach($period as $date) {                  
        $array[] = $date;  
    }  
    return $period; 
	} 
  
  public function getSalesOrderGraphic($dateFilter='') {
    $db = PearDatabase::getInstance();
    $last_week = date('Y-m-d', strtotime('-13 days'));
    $today = date("Y-m-d");

    $sql = "SELECT  DATE_FORMAT(e.createdtime, '%Y-%m-%d') as week_day, SUM(i.pll) as pll_orders_num ,COUNT(DISTINCT s.salesorderid) as orders_num 
                      FROM vtiger_salesorder s	
                      LEFT JOIN vtiger_inventoryproductrel i ON i.id=s.salesorderid
                      LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
                      WHERE e.deleted = 0 AND setype = 'SalesOrder' ";

		$currentUser = Users_Record_Model::getCurrentUserModel();
    $params = array();

    

		//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";
			$params[] = $dateFilter['start'];
      $params[] = date('Y-m-d',  strtotime("+13 day", strtotime($dateFilter['start'])));
      $today = date('Y-m-d',  strtotime("+13 day", strtotime($dateFilter['start'])));
      $last_week = $dateFilter['start'];
		}else{     
      $sql .= " AND DATE_FORMAT(e.createdtime, '%Y-%m-%d') BETWEEN ? AND ? ";         
      $params[] = $last_week; 
      $params[] = $today; 
    }

    $sql .= " GROUP BY DATE_FORMAT(e.createdtime, '%Y-%m-%d')  
              ORDER BY DATE_FORMAT(e.createdtime, '%Y-%m-%d') ";	
    
    $lt_week_days = array('Monday' => 'Pirmadienis', 'Tuesday' => 'Antradienis', 'Wednesday' => 'Trečiadienis', 'Thursday' => 'Ketvirtadienis', 'Friday' => 'Penktadienis', 'Saturday' => 'Šeštadienis', 'Sunday' => 'Sekmadienis');

    $salesOrders = array();

    $result = $db->pquery($sql,array($params[0],$params[1]));  

    $orders = [];
    foreach ($result as $value) {
      $orders[$value['week_day']] = $value;
    }

    		
    $period = $this->getDatesFromRange($params[0], $params[1]);         

		foreach ($period as $key => $value) {	 
      $order_date = $value->format('Y-m-d');
      $order_week_day = $value->format('l');

      if($order_week_day != 'Saturday' && $order_week_day != 'Sunday'){
        $salesOrders['orders'][] = ($orders[$order_date]['orders_num'] ?:0);       
        $salesOrders['days'][] =  "'".$lt_week_days[$order_week_day]."'";        
        $salesOrders['pll_orders'][] = ($orders[$order_date]['pll_orders_num'] ?:0); 
      }   
    }

    $salesOrders['filter'] = 	"Nuo: ".$last_week." Iki: ".$today; 		

    return $salesOrders;
	}

}
?>
