<?php


class SettingPositionHandler extends VTEventHandler {

  function handleEvent($eventName, $entityData)
  {
    global $adb, $current_user;

        if($eventName == 'vtiger.entity.aftersave') {
            $moduleName = $entityData->getModuleName();

            if($moduleName != 'SettingPosition'){
                return;
            }

            if($moduleName == 'SettingPosition' && $_REQUEST['module']=='SettingPosition'){
              $recordId = $entityData->getId();  

              $get_role = $adb->pquery("SELECT p1.name AS role, p2.name AS report_to
                                          FROM vtiger_hrm_position p1
                                          LEFT JOIN vtiger_hrm_position p2 ON p2.id=p1.cf_2435
                                          WHERE p1.id = ?",array($recordId));

              $role = $adb->query_result($get_role,0,'role');
              $reports_to = $adb->query_result($get_role,0,'report_to');

              $check_reporter_roles = $adb->pquery("SELECT roleid, parentrole,depth FROM vtiger_role WHERE rolename = ?", array($reports_to));
              $roleid = $adb->query_result($check_reporter_roles,0,'roleid');
              $parent = $adb->query_result($check_reporter_roles,0,'parentrole');
              $depth = $adb->query_result($check_reporter_roles,0,'depth');
              $depth = $depth+1;


              $get_roles_seq = $adb->pquery("SELECT id FROM vtiger_role_seq",array());
              $seq = $adb->query_result($get_roles_seq,0,'id');

              $new_role = "H$seq";
              $parentrole = $parent."::".$new_role;
         
              $insert_new_role = $adb->pquery("INSERT INTO vtiger_role (roleid, rolename, parentrole, depth, allowassignedrecordsto) VALUES (?,?,?,?,?)", array($new_role,$role,$parentrole,$depth,1));

              $picklist2RoleSQL = "INSERT INTO vtiger_role2picklist SELECT '".$new_role."',picklistvalueid,picklistid,sortid
              FROM vtiger_role2picklist WHERE roleid = ?";
              $adb->pquery($picklist2RoleSQL, array($roleid));

              $profile = $role."+Profilis";

              $adb->pquery("INSERT INTO vtiger_role2profile (roleid,profileid) VALUES (?,?)", array($new_role,$seq));
              $adb->pquery("INSERT INTO vtiger_profile2globalpermissions (profileid, globalactionid, globalactionpermission) VALUES (?,?,?)",array($seq,1,1));
              $adb->pquery("INSERT INTO vtiger_profile2globalpermissions (profileid, globalactionid, globalactionpermission) VALUES (?,?,?)",array($seq,2,1));
              $adb->pquery("INSERT INTO vtiger_profile (profileid, profilename,  directly_related_to_role) values (?,?,?)", array($seq,$profile,1));

              if($insert_new_role){
                $adb->pquery("UPDATE vtiger_role_seq SET id = ?", array($seq+1));
              }

            }
        }
  }

}