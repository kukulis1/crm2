<?php

class Employeeinventory_Module_Model extends Vtiger_Module_Model {

  static function getInventoryList($recordID){
    $db = PearDatabase::getInstance();

    $sql = "SELECT vtiger_employeeinventory.employeeinventoryid, employeeinventory_tks_item, employeeinventory_tks_code, cf_2372 AS value
            FROM vtiger_employeeinventory
            LEFT JOIN vtiger_employeeinventorycf ON vtiger_employeeinventorycf.employeeinventoryid=vtiger_employeeinventory.employeeinventoryid
            INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_employeeinventory.employeeinventoryid
            WHERE deleted = 0
            ORDER BY vtiger_employeeinventory.employeeinventoryid DESC";

    $result = $db->pquery($sql,array());

    $get_individual_inventory = $db->pquery("SELECT * FROM vtiger_employee_individual_inventory WHERE user_id = ?", array($recordID));

    $list = array();

    foreach ($result as $value) {
      $list[] = $value;
    }

    foreach ($get_individual_inventory as $value) {
      $list[] = $value;
    }


    return $list;
  }
  
  static function getAssignedInventory($recordID){
    $db = PearDatabase::getInstance();

    $sql = "SELECT assigned_item, qty, CONCAT(first_name,' ',last_name) AS owner, date_of_issue,date_of_return
            FROM vtiger_employeeinventory_assign 
            LEFT JOIN vtiger_users u ON u.id=assigned_user
            WHERE user_id = ?";
    $result = $db->pquery($sql,array($recordID));

    $list = array();

    foreach ($result as $val) {
      $list[$val['assigned_item']] = $val;
    }
    return $list;
  }

}