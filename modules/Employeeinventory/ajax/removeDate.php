<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();         
        $column = $_POST['column'];        

        $update_inventory = $dbh->prepare("UPDATE vtiger_employeeinventory_assign SET $column = ? WHERE `user_id` = ? AND assigned_item = ?");       
        $update_inventory->execute(['', $_POST['employeid'], $_POST['employeeinventoryid']]);      

        echo json_encode(array('status' => 'success'));
        
      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'Fail'));
  }
}else{
  http_response_code(404);
} 