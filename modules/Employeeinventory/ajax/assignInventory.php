<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();  
        $date = date("Y-m-d H:i:s");

        $column = $_POST['column'];

        $check_exist = $dbh->prepare("SELECT id FROM vtiger_employeeinventory_assign WHERE `user_id` = ? AND assigned_item = ?");
        $check_exist->execute([$_POST['employeid'], $_POST['employeeinventoryid']]);
        

        $insert_inventory = $dbh->prepare("INSERT INTO vtiger_employeeinventory_assign (`user_id`, assigned_user, assigned_item, $column, assigned_date) VALUES (:user_id, :assigned_user, :assigned_item, :{$column}, :assigned_date)");   

        $update_inventory = $dbh->prepare("UPDATE vtiger_employeeinventory_assign SET $column = ? WHERE `user_id` = ? AND assigned_item = ?");
        
        if(!$check_exist->rowCount()){        
          $insert_inventory->execute([':user_id' => $_POST['employeid'],
                                      ':assigned_user' => $_POST['userid'],
                                      ':assigned_item' => $_POST['employeeinventoryid'],
                                      ":{$column}" => $_POST['stringDate'],                                   
                                      ':assigned_date' => $date
                                    ]
                        );
        }else{
          $update_inventory->execute([$_POST['stringDate'], $_POST['employeid'], $_POST['employeeinventoryid'] ]);
        }              

        if($insert_inventory || $update_inventory){
          $getName = $dbh->prepare("SELECT CONCAT(first_name,' ',last_name) AS name FROM vtiger_users WHERE id = ?");
          $getName->setFetchMode(PDO::FETCH_ASSOC);
          $getName->execute(array($_POST['userid']));         
          echo json_encode(array('status' => 'success','name' => $getName->fetch()['name']));
        }else{
          echo json_encode(array('status' => 'Fail'));
        }

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'Fail'));
  }
}else{
  http_response_code(404);
} 