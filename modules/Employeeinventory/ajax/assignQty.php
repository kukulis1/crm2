<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();  
        $date = date("Y-m-d H:i:s");      

        $check_exist = $dbh->prepare("SELECT id FROM vtiger_employeeinventory_assign WHERE `user_id` = ? AND assigned_item = ?");
        $check_exist->execute([$_POST['employeid'], $_POST['employeeinventoryid']]);
        

        $insert_inventory = $dbh->prepare("INSERT INTO vtiger_employeeinventory_assign (`user_id`, assigned_user, assigned_item,qty, assigned_date) VALUES (:user_id, :assigned_user, :assigned_item, :qty, :assigned_date)");   

        $update_inventory = $dbh->prepare("UPDATE vtiger_employeeinventory_assign SET qty = ? WHERE `user_id` = ? AND assigned_item = ?");
        
        if(!$check_exist->rowCount()){        
          $insert_inventory->execute([':user_id' => $_POST['employeid'],
                                      ':assigned_user' => $_POST['userid'],
                                      ':assigned_item' => $_POST['employeeinventoryid'],
                                      ':qty' => $_POST['qty'],                                                                    
                                      ':assigned_date' => $date
                                    ]
                        );
        }else{
          $update_inventory->execute([$_POST['qty'], $_POST['employeid'], $_POST['employeeinventoryid'] ]);
        }              

        if($insert_inventory || $update_inventory){              
          echo json_encode(array('status' => 'success'));
        }else{
          echo json_encode(array('status' => 'Fail'));
        }

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'Fail'));
  }
}else{
  http_response_code(404);
} 