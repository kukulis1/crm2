<?php


class DismissalHandler extends VTEventHandler {

  function handleEvent($eventName, $entityData)
  {
    global $adb, $current_user;

        if($eventName == 'vtiger.entity.aftersave') {
            $moduleName = $entityData->getModuleName();

            if($moduleName != 'Dismissal'){
                return;
            }

            if($moduleName == 'Dismissal' && $_REQUEST['module']=='Dismissal'){
              $recordId = $entityData->getId();              
              $employee = $adb->pquery("SELECT cf_20023 FROM vtiger_dismissal WHERE dismissalid = ?",array($recordId));
              $employeeid = $adb->query_result($employee,0,'cf_20023');
              $adb->pquery("UPDATE vtiger_hrm_employcf SET cf_2370 = ? WHERE employid = ?",array('Nebedirba',$employeeid));
              $user = $adb->pquery("SELECT user_id FROM vtiger_hrm_employee WHERE employid = ?", array($employeeid));
              $userid = $adb->query_result($user,0,'user_id');
              $adb->pquery("UPDATE vtiger_users SET status = ? WHERE id = ?",array('Inactive',$userid));
            }
        }
  }

}