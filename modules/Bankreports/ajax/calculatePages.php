<?php

require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");  

  $params = $_POST['search'];
  $search_params = str_replace('"','',substr($params,3,-3));
	$search_params = explode("],[", $search_params);

  $query = "SELECT p.purchaseorderid, bankstatements_tks_payerreceiv AS payer,bankstatements_tks_date AS date, bankstatements_tks_purpose AS purpose, ROUND(p.total,2) AS crm_total, bankstatements_tks_amount AS bank_total, (total - bankstatements_tks_amount) AS diff
		FROM vtiger_bankstatements b
		LEFT JOIN vtiger_bankstatements_end_to_end_id en ON en.recordid=b.bankstatementsid
		LEFT JOIN vtiger_purchaseorder_export_payments pe ON pe.uniqueid=en.code	 
		LEFT JOIN vtiger_purchaseorder p ON p.purchaseorderid=pe.purchaseorderid
		JOIN vtiger_crmentity e ON e.crmid=b.bankstatementsid
		WHERE e.deleted=0 AND IF(p.purchaseorderid IS NOT NULL, (total - bankstatements_tks_amount) > 0, bankstatementsid > 0)  "; 
    
  $query2 = "SELECT p2.purchaseorderid, vendorname AS payer, DATE_FORMAT(export_date,'%Y-%m-%d') AS date, '-' AS purpose, ROUND(p2.total,2) AS crm_total, '-' AS bank_total, '-' AS diff
  FROM vtiger_purchaseorder p2
  JOIN vtiger_vendor v ON v.vendorid=p2.vendorid
  JOIN vtiger_purchaseorder_export_payments ep ON ep.purchaseorderid=p2.purchaseorderid
  LEFT JOIN vtiger_bankstatements_end_to_end_id et ON et.code=ep.uniqueid 
  INNER JOIN vtiger_crmentity e ON e.crmid=p2.purchaseorderid
  WHERE e.deleted = 0 AND et.recordid IS NULL ";	


  $where = '';
  $where2 = '';
  $having = '';
  $having2 = '';
			
  if(!empty($search_params[0])){
    for($i = 0; $i < count($search_params); $i++){
      $param = explode(",", $search_params[$i]);
      if($param[0] == 'bankreports_tks_payer'){
        $column = 'bankstatements_tks_payerreceiv';
        $column2 = 'vendorname';
      }elseif($param[0] == 'bankreports_tks_date'){
        $column = 'bankstatements_tks_date';
        $column2 = "DATE_FORMAT(export_date,'%Y-%m-%d')";
      }elseif($param[0] == 'bankreports_tks_purpose'){
        $column = 'bankstatements_tks_purpose';
        $column2 = 'compound_taxes_info';
      }elseif($param[0] == 'bankreports_tks_crmamount'){
        $column = 'total';
        $column2 = 'total';
      }elseif($param[0] == 'bankreports_tks_bankamount'){
        $column = 'bankstatements_tks_amount';
        $column2 = 'compound_taxes_info';								
      }


      if($param[0] != 'bankreports_tks_difference'){
        if($param[0] == 'bankreports_tks_date'){
          $where .= " $column BETWEEN '$param[2]' AND '$param[3]' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";									
          $where2 .= " $column2 BETWEEN '$param[2]' AND '$param[3]' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";									
        }else{
          $where .= " $column LIKE '%".strtolower($param[2])."%' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";
          $where2 .= " $column2 LIKE '%".strtolower($param[2])."%' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";
        }	
      }
      
      if($param[0] == 'bankreports_tks_difference'){
        $having .=  "HAVING diff LIKE '%".strtolower($param[2])."%'";
        $having2 .=  " HAVING diff = $param[2]";
      }


    }
  }

  if(!empty($where)) $query .= ' AND ';
  if(!empty($where2)) $query2 .= ' AND ';


  $query .= $where;
	$query2 .= $where2;
 
	$queryForCount .= "$query GROUP BY b.bankstatementsid $having UNION $query2 GROUP BY p2.purchaseorderid $having2";
  $result = $conn->query($queryForCount);

  $num_rows = mysqli_num_rows($result);


  echo json_encode(array('orders' => ceil(($num_rows)/$list_max_entries_per_page)));
}else{
  http_response_code(404);
} 