<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

/**
 * Vtiger ListView Model Class
 */
class Bankreports_ListView_Model extends Vtiger_Base_Model {

	/**
	 * Function to get the Module Model
	 * @return Vtiger_Module_Model instance
	 */
	public function getModule() {
		return $this->get('module');
	}


	/**
	 * Function to get the Quick Links for the List view of the module
	 * @param <Array> $linkParams
	 * @return <Array> List of Vtiger_Link_Model instances
	 */
	public function getSideBarLinks($linkParams) {
		$linkTypes = array('SIDEBARLINK', 'SIDEBARWIDGET');
		$moduleLinks = $this->getModule()->getSideBarLinks($linkParams);

		$listLinkTypes = array('LISTVIEWSIDEBARLINK', 'LISTVIEWSIDEBARWIDGET');
		$listLinks = Vtiger_Link_Model::getAllByType($this->getModule()->getId(), $listLinkTypes);

		if($listLinks['LISTVIEWSIDEBARLINK']) {
			foreach($listLinks['LISTVIEWSIDEBARLINK'] as $link) {
				$moduleLinks['SIDEBARLINK'][] = $link;
			}
		}

		if($listLinks['LISTVIEWSIDEBARWIDGET']) {
			foreach($listLinks['LISTVIEWSIDEBARWIDGET'] as $link) {
				$moduleLinks['SIDEBARWIDGET'][] = $link;
			}
		}

		return $moduleLinks;
	}

	/**
	 * Function to get the list of listview links for the module
	 * @param <Array> $linkParams
	 * @return <Array> - Associate array of Link Type to List of Vtiger_Link_Model instances
	 */
	public function getListViewLinks($linkParams) {
		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$moduleModel = $this->getModule();

		$linkTypes = array('LISTVIEWBASIC', 'LISTVIEW', 'LISTVIEWSETTING');
		$links = Vtiger_Link_Model::getAllByType($moduleModel->getId(), $linkTypes, $linkParams);

		$basicLinks = $this->getBasicLinks();

		foreach($basicLinks as $basicLink) {
			$links['LISTVIEWBASIC'][] = Vtiger_Link_Model::getInstanceFromValues($basicLink);
		}

		$advancedLinks = $this->getAdvancedLinks();

		foreach($advancedLinks as $advancedLink) {
			$links['LISTVIEW'][] = Vtiger_Link_Model::getInstanceFromValues($advancedLink);
		}

		if($currentUserModel->isAdminUser()) {

			$settingsLinks = $this->getSettingLinks();
			foreach($settingsLinks as $settingsLink) {
				$links['LISTVIEWSETTING'][] = Vtiger_Link_Model::getInstanceFromValues($settingsLink);
			}
		}

		return $links;
	}

	/**
	 * Function to get the list of Mass actions for the module
	 * @param <Array> $linkParams
	 * @return <Array> - Associative array of Link type to List of  Vtiger_Link_Model instances for Mass Actions
	 */
	public function getListViewMassActions($linkParams) {
		$currentUserModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleModel = $this->getModule();

		$linkTypes = array('LISTVIEWMASSACTION');
		$links = Vtiger_Link_Model::getAllByType($moduleModel->getId(), $linkTypes, $linkParams);


		$massActionLinks = array();
		if($currentUserModel->hasModuleActionPermission($moduleModel->getId(), 'EditView')) {
			$massActionLinks[] = array(
				'linktype' => 'LISTVIEWMASSACTION',
				'linklabel' => 'LBL_EDIT',
				'linkurl' => 'javascript:Vtiger_List_Js.triggerMassEdit("index.php?module='.$moduleModel->get('name').'&view=MassActionAjax&mode=showMassEditForm");',
				'linkicon' => ''
			);
		}
		if($currentUserModel->hasModuleActionPermission($moduleModel->getId(), 'Delete')) {
			$massActionLinks[] = array(
				'linktype' => 'LISTVIEWMASSACTION',
				'linklabel' => 'LBL_DELETE',
				'linkurl' => 'javascript:Vtiger_List_Js.massDeleteRecords("index.php?module='.$moduleModel->get('name').'&action=MassDelete");',
				'linkicon' => ''
			);
		}

		$modCommentsModel = Vtiger_Module_Model::getInstance('ModComments');
		if($moduleModel->isCommentEnabled() && $modCommentsModel->isPermitted('CreateView')) {
			$massActionLinks[] = array(
				'linktype' => 'LISTVIEWMASSACTION',
				'linklabel' => 'LBL_ADD_COMMENT',
				'linkurl' => 'index.php?module='.$moduleModel->get('name').'&view=MassActionAjax&mode=showAddCommentForm',
				'linkicon' => ''
			);
		}

		foreach($massActionLinks as $massActionLink) {
			$links['LISTVIEWMASSACTION'][] = Vtiger_Link_Model::getInstanceFromValues($massActionLink);
		}

		return $links;
	}

	/**
	 * Function to get the list view header
	 * @return <Array> - List of Vtiger_Field_Model instances
	 */
	public function getListViewHeaders() {
		$listViewContoller = $this->get('listview_controller');
		$module = $this->getModule();
		$headerFieldModels = array();
		$headerFields = $listViewContoller->getListViewHeaderFields();

		foreach($headerFields as $fieldName => $webserviceField) {
			if($webserviceField && !in_array($webserviceField->getPresence(), array(0,2))) continue;
			if($webserviceField && $webserviceField->parentReferenceField && !in_array($webserviceField->parentReferenceField->getPresence(), array(0,2))){
				continue;
			}
		
			if($webserviceField->getDisplayType() == '6') continue;
			// check if the field is reference field
			preg_match('/(\w+) ; \((\w+)\) (\w+)/', $fieldName, $matches);
			if(count($matches) > 0) {
				list($full, $referenceParentField, $referenceModule, $referenceFieldName) = $matches;
				$referenceModuleModel = Vtiger_Module_Model::getInstance($referenceModule);
				$referenceFieldModel = Vtiger_Field_Model::getInstance($referenceFieldName, $referenceModuleModel);
				$referenceFieldModel->set('webserviceField', $webserviceField);
				// added tp use in list view to see the title, for reference field rawdata key is different than the actual field
				// eg: in rawdata its account_idcf_2342 (raw column name used in querygenerator), actual field name (account_id ;(Accounts) cf_2342)
				// When generating the title we use rawdata and from field model we have no way to find querygenrator raw column name.

				$referenceFieldModel->set('listViewRawFieldName', $referenceParentField.$referenceFieldName);

				// this is added for picklist colorizer (picklistColorMap.tpl), for fetching picklist colors we need the actual field name of the picklist
				$referenceFieldModel->set('_name', $referenceFieldName);
				$headerFieldModels[$fieldName] = $referenceFieldModel->set('name', $fieldName); // resetting the fieldname as we use it to fetch the value from that name
				$matches=null;
			} else {
				$fieldInstance = Vtiger_Field_Model::getInstance($fieldName,$module);
				$fieldInstance->set('listViewRawFieldName', $fieldInstance->get('column'));
				$headerFieldModels[$fieldName] = $fieldInstance;
			}
		}

		return $headerFieldModels;
	}

	/**
	 * Function to get the list view entries
	 * @param Vtiger_Paging_Model $pagingModel
	 * @return <Array> - Associative array of record id mapped to Vtiger_Record_Model instance.
	 */


	//    list
	public function getListViewEntries($pagingModel) {

		global $list_max_entries_per_page; 

		$page = (isset($_REQUEST['page'])) ? (int)$_REQUEST['page'] : 1;  
		$startAt = $list_max_entries_per_page * ($page - 1);

		$db = PearDatabase::getInstance();
		$results = array();
		$search_params = str_replace('"','',substr($_REQUEST['search_params'],3,-3));
		$search_params = explode("],[", $search_params);


		$query = "SELECT p.purchaseorderid,bankstatements_tks_bank, bankstatements_tks_payerreceiv AS payer,bankstatements_tks_date AS date, bankstatements_tks_purpose AS purpose, ROUND(p.total,2) AS crm_total, bankstatements_tks_amount AS bank_total, (total - bankstatements_tks_amount) AS diff
		FROM vtiger_bankstatements b
		LEFT JOIN vtiger_bankstatements_end_to_end_id en ON en.recordid=b.bankstatementsid
		LEFT JOIN vtiger_purchaseorder_export_payments pe ON pe.uniqueid=en.code	 
		LEFT JOIN vtiger_purchaseorder p ON p.purchaseorderid=pe.purchaseorderid
		LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid
		JOIN vtiger_crmentity e ON e.crmid=b.bankstatementsid
		WHERE e.deleted=0 "; 

// AND IF(p.purchaseorderid IS NOT NULL, (total - bankstatements_tks_amount) > 0, bankstatementsid > 0)
    
		// $query2 = "SELECT p2.purchaseorderid,bankstatements_tks_bank AS bankstatements_tks_bank,  vendorname AS payer, DATE_FORMAT(export_date,'%Y-%m-%d') AS date, '-' AS purpose, ROUND(p2.total,2) AS crm_total, bankstatements_tks_amount AS bank_total, (total - bankstatements_tks_amount) AS diff
		// FROM vtiger_purchaseorder p2
		// JOIN vtiger_vendor v ON v.vendorid=p2.vendorid
		// JOIN vtiger_purchaseorder_export_payments ep ON ep.purchaseorderid=p2.purchaseorderid
		// LEFT JOIN vtiger_bankstatements_end_to_end_id et ON et.code=ep.uniqueid 
		// JOIN vtiger_bankstatements b2 ON et.recordid=b2.bankstatementsid
		// INNER JOIN vtiger_crmentity e ON e.crmid=p2.purchaseorderid
		// WHERE e.deleted = 0  ";	


							
				$where = '';
				$having = '';
				$having2 = '';
			
				if(!empty($search_params[0])){
					for($i = 0; $i < count($search_params); $i++){
						$param = explode(",", $search_params[$i]);
						if($param[0] == 'bankreports_tks_payer'){
							$column = 'bankstatements_tks_payerreceiv';
							$column2 = 'vendorname';
						}elseif($param[0] == 'bankreports_tks_date'){
							$column = 'bankstatements_tks_date';
							$column2 = "DATE_FORMAT(export_date,'%Y-%m-%d')";
						}elseif($param[0] == 'bankreports_tks_purpose'){
							$column = 'bankstatements_tks_purpose';
							$column2 = 'compound_taxes_info';
						}elseif($param[0] == 'bankreports_tks_crmamount'){
							$column = 'total';
							$column2 = 'total';
						}elseif($param[0] == 'bankreports_tks_bankamount'){
							$column = 'bankstatements_tks_amount';
							$column2 = 'compound_taxes_info';
						}elseif($param[0] == 'cf_2159'){
							$column = 'bankstatements_tks_bank';
							$column2 = 'compound_taxes_info';															
						}

						if($param[0] != 'bankreports_tks_difference'){
							if($param[0] == 'bankreports_tks_date'){
								$where .= " $column BETWEEN '$param[2]' AND '$param[3]' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";									
								$where2 .= " $column2 BETWEEN '$param[2]' AND '$param[3]' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";									
							}else{
								$where .= " $column LIKE '%".strtolower($param[2])."%' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";
								$where2 .= " $column2 LIKE '%".strtolower($param[2])."%' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";
							}	
						}
						
						if($param[0] == 'bankreports_tks_difference'){
							$having .=  "HAVING diff LIKE '%".strtolower($param[2])."%'";
							$having2 .=  " HAVING diff = $param[2]";
						}

					}
				}
				

			if(!empty($where)) $query .= ' AND ';
			if(!empty($where2)) $query2 .= ' AND ';

			$query .= $where;
			$query2 .= $where2;
	
	
			// $queryForCount .= "$query GROUP BY b.bankstatementsid $having UNION $query2 GROUP BY p2.purchaseorderid $having2";
			$queryForCount .= "$query GROUP BY b.bankstatementsid $having ";

			$listResultCount = $db->pquery($queryForCount, array());
		
			$totalOrdersCount =	$db->num_rows($listResultCount);

			$order_by .= " ORDER BY `date` DESC LIMIT $startAt, $list_max_entries_per_page";


			// $FINAL_QUERY = 	"$query GROUP BY b.bankstatementsid $having UNION $query2 GROUP BY p2.purchaseorderid $having2 $order_by";					
			$FINAL_QUERY = 	"$query GROUP BY b.bankstatementsid $having  $order_by";					

			$listResult = $db->pquery($FINAL_QUERY, array());
			$ordersCount = $db->num_rows($listResult);


			$from_number = 	(!$startAt ? 1 : $startAt+1);
			$to_number = ($totalOrdersCount < 100 ? $totalOrdersCount : ($startAt+$list_max_entries_per_page > $totalOrdersCount ? $totalOrdersCount : $startAt+$list_max_entries_per_page)); 

			$results['fromNumber'] = $from_number;
			$results['toNumber'] = $to_number;
			$results['isPrevPageExists'] = ($page > 1 ? 1 : 0);
			$results['isNextPageExists'] = ($page < ceil($totalOrdersCount/$list_max_entries_per_page) ? 1 : 0);

			$results['totalOrders'] = $totalOrdersCount;
			$results['count'] = $ordersCount;
			$results['listResult'] = $listResult;
			$results['listResult2'] = $listResultCount;

		return $results;
	}

	
	public function getBankIban(){
		$db = PearDatabase::getInstance();
		$ibans = array();

		$listQuery = "SELECT bankaccountnum, bankaccountnum2 FROM vtiger_organizationdetails";
		$listResult = $db->pquery($listQuery, array());
		$rez = $db->fetch_array($listResult);

		$bankaccountnum = explode(";", $rez['bankaccountnum']);
		$bankaccountnum2 = explode(";", $rez['bankaccountnum2']);

		if(!empty($bankaccountnum[0])) $ibans[] = $bankaccountnum[0];
		if(!empty($bankaccountnum2[0]))	$ibans[] = $bankaccountnum2[0];

		return $ibans;
	} 

	/**
	 * Function to get the list view entries
	 * @param Vtiger_Paging_Model $pagingModel
	 * @return <Array> - Associative array of record id mapped to Vtiger_Record_Model instance.
	 */
	public function getListViewCount() {
		$db = PearDatabase::getInstance();

		$queryGenerator = $this->get('query_generator');


		$searchParams = $this->get('search_params');
		if(empty($searchParams)) {
			$searchParams = array();
		}

		// for Documents folders we should filter with folder id as well
		$folderKey = $this->get('folder_id');
		$folderValue = $this->get('folder_value');
		if(!empty($folderValue)) {
			$queryGenerator->addCondition($folderKey,$folderValue,'e');
		}

		$glue = "";
		if(count($queryGenerator->getWhereFields()) > 0 && (count($searchParams)) > 0) {
			$glue = QueryGenerator::$AND;
		}
		$queryGenerator->parseAdvFilterList($searchParams, $glue);

		$searchKey = $this->get('search_key');
		$searchValue = $this->get('search_value');
		$operator = $this->get('operator');
		if(!empty($searchKey)) {
			$queryGenerator->addUserSearchConditions(array('search_field' => $searchKey, 'search_text' => $searchValue, 'operator' => $operator));
		}
		$moduleName = $this->getModule()->get('name');
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$listQuery = $this->getQuery();
		
		

		$sourceModule = $this->get('src_module');
		if(!empty($sourceModule)) {
			$moduleModel = $this->getModule();
			if(method_exists($moduleModel, 'getQueryByModuleField')) {
				$overrideQuery = $moduleModel->getQueryByModuleField($sourceModule, $this->get('src_field'), $this->get('src_record'), $listQuery);
				if(!empty($overrideQuery)) {
					$listQuery = $overrideQuery;
				}
			}
		}
		$position = stripos($listQuery, ' from ');
		if ($position) {
			$split = preg_split('/ from /i', $listQuery);
			$splitCount = count($split);
			// If records is related to two records then we'll get duplicates. Then count will be wrong
			$meta = $queryGenerator->getMeta($this->getModule()->getName());
			$columnIndex = $meta->getObectIndexColumn();
			$baseTable = $meta->getEntityBaseTable();
			$listQuery = "SELECT count(distinct($baseTable.$columnIndex)) AS count ";
			for ($i=1; $i<$splitCount; $i++) {
				$listQuery = $listQuery. ' FROM ' .$split[$i];
			}
		}

		if($this->getModule()->get('name') == 'Calendar'){
			$listQuery .= ' AND activitytype <> "Emails"';
		}

		$listResult = $db->pquery($listQuery, array());
		return $db->query_result($listResult, 0, 'count');
	}

	function getQuery() {
		$queryGenerator = $this->get('query_generator');
		$listQuery = $queryGenerator->getQuery();
		return $listQuery;
	}
	/**
	 * Static Function to get the Instance of Vtiger ListView model for a given module and custom view
	 * @param <String> $moduleName - Module Name
	 * @param <Number> $viewId - Custom View Id
	 * @return Vtiger_ListView_Model instance
	 */
	public static function getInstance($moduleName, $viewId='0', $listHeaders = array()) {
		$db = PearDatabase::getInstance();
		$currentUser = vglobal('current_user');

		$modelClassName = Vtiger_Loader::getComponentClassName('Model', 'ListView', $moduleName);
		$instance = new $modelClassName();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		$queryGenerator = new EnhancedQueryGenerator($moduleModel->get('name'), $currentUser);
		$customView = new CustomView();
		if (!empty($viewId) && $viewId != "0") {
			$queryGenerator->initForCustomViewById($viewId);

			//Used to set the viewid into the session which will be used to load the same filter when you refresh the page
			$viewId = $customView->getViewId($moduleName);
		} else {
			$viewId = $customView->getViewId($moduleName);
			if(!empty($viewId) && $viewId != 0) {
				$queryGenerator->initForDefaultCustomView();
			} else {
				$entityInstance = CRMEntity::getInstance($moduleName);
				$listFields = $entityInstance->list_fields_name;
				$listFields[] = 'id';
				$queryGenerator->setFields($listFields);
			}
		}

		$fieldsList = $queryGenerator->getFields();
		if(!empty($listHeaders) && is_array($listHeaders) && count($listHeaders) > 0) {
			$fieldsList = $listHeaders;
			$fieldsList[] = 'id';
		}
		//to show starred field in list view
		$fieldsList[] = 'starred';
		$queryGenerator->setFields($fieldsList);

		$moduleSpecificControllerPath = 'modules/'.$moduleName.'/controllers/ListViewController.php';
		if(file_exists($moduleSpecificControllerPath)) {
			include_once $moduleSpecificControllerPath;
			$moduleSpecificControllerClassName = $moduleName.'ListViewController';
			$controller = new $moduleSpecificControllerClassName($db, $currentUser, $queryGenerator);
		} else {
			$controller = new ListViewController($db, $currentUser, $queryGenerator);
		}

		return $instance->set('module', $moduleModel)->set('query_generator', $queryGenerator)->set('listview_controller', $controller);
	}

	/**
	 * Function to create clean instance
	 * @param type $moduleName -- module for which list view model has to be created
	 * @return type -- List view model  
	 */
	public static function getCleanInstance($moduleName) {
		$modelClassName = Vtiger_Loader::getComponentClassName('Model', 'ListView', $moduleName);
		$instance = new $modelClassName();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		return $instance->set('module', $moduleModel);
	}

	/**
	 * Static Function to get the Instance of Vtiger ListView model for a given module and custom view
	 * @param <String> $value - Module Name
	 * @param <Number> $viewId - Custom View Id
	 * @return Vtiger_ListView_Model instance
	 */
	public static function getInstanceForPopup($value) {
		$db = PearDatabase::getInstance();
		$currentUser = vglobal('current_user');

		$modelClassName = Vtiger_Loader::getComponentClassName('Model', 'ListView', $value);
		$instance = new $modelClassName();
		$moduleModel = Vtiger_Module_Model::getInstance($value);

		$queryGenerator = new EnhancedQueryGenerator($moduleModel->get('name'), $currentUser);

		$listFields = $moduleModel->getPopupViewFieldsList();

		$listFields[] = 'id';
		$queryGenerator->setFields($listFields);

		$controller = new ListViewController($db, $currentUser, $queryGenerator);

		return $instance->set('module', $moduleModel)->set('query_generator', $queryGenerator)->set('listview_controller', $controller);
	}

	/*
	 * Function to give advance links of a module
	 *	@RETURN array of advanced links
	 */
	public function getAdvancedLinks(){
		$moduleModel = $this->getModule();
		$createPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'CreateView');
		$advancedLinks = array();
		$importPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'Import');
		if($importPermission && $createPermission) {
			$advancedLinks[] = array(
							'linktype' => 'LISTVIEW',
							'linklabel' => 'LBL_IMPORT',
							'linkurl' => $moduleModel->getImportUrl(),
							'linkicon' => ''
			);
		}

		$duplicatePermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'DuplicatesHandling');
		$editPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'EditView');
		if($duplicatePermission && $editPermission) {
			$advancedLinks[] = array(
				'linktype' => 'LISTVIEWMASSACTION',
				'linklabel' => 'LBL_FIND_DUPLICATES',
				'linkurl' => 'Javascript:Vtiger_List_Js.showDuplicateSearchForm("index.php?module='.$moduleModel->getName().
								'&view=MassActionAjax&mode=showDuplicatesSearchForm")',
				'linkicon' => ''
			);
		}

		$exportPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'Export');
		if($exportPermission) {
			$advancedLinks[] = array(
					'linktype' => 'LISTVIEW',
					'linklabel' => 'LBL_EXPORT',
					'linkurl' => 'javascript:Vtiger_List_Js.triggerExportAction("'.$this->getModule()->getExportUrl().'")',
					'linkicon' => ''
				);
		}

		return $advancedLinks;
	}

	/*
	 * Function to get Setting links
	 * @return array of setting links
	 */
	public function getSettingLinks() {
		return $this->getModule()->getSettingLinks();
	}

	/*
	 * Function to get Basic links
	 * @return array of Basic links
	 */
	public function getBasicLinks(){
		$basicLinks = array();
		$moduleModel = $this->getModule();
		$createPermission = Users_Privileges_Model::isPermitted($moduleModel->getName(), 'CreateView');
		if($createPermission) {
			$basicLinks[] = array(
					'linktype' => 'LISTVIEWBASIC',
					'linklabel' => 'LBL_ADD_RECORD',
					'linkurl' => $moduleModel->getCreateRecordUrl(),
					'linkicon' => ''
			);
		}
		return $basicLinks;
	}

	public function extendPopupFields($fieldsList) {
		$moduleModel = $this->get('module');
		$queryGenerator = $this->get('query_generator');

		$listFields = $moduleModel->getPopupViewFieldsList();

		$listFields[] = 'id';
		$listFields = array_merge($listFields, $fieldsList);
		$queryGenerator->setFields($listFields);
		$this->get('query_generator', $queryGenerator);
	}

	public function getSortParamsSession($key) {
		return $_SESSION[$key];
			}

	public function setSortParamsSession($key, $params) {
		$_SESSION[$key] = $params;
	}

	public function deleteParamsSession($key, $params) {
		if(!is_array($params)) {
			$params = array($params);
		}
		foreach($params as $param) {
			$_SESSION[$key][$param] = '';
		}
	}

	public function isImportEnabled() {
		$linkParams = array('MODULE'=>$this->getModule()->getName(), 'ACTION'=>'LIST');
		$listViewLinks = $this->getListViewLinks($linkParams);
		$listViewActions = $listViewLinks['LISTVIEW'];
		if (is_array($listViewActions)) {
			foreach($listViewActions as $linkAction) {
				if($linkAction->getLabel() == 'LBL_IMPORT'){
					return true;
				}
			}
		}
		return false;
	}
}
