<?php

include "modules/Clientsemails/models/ListModel.php";

use modules\Clientsemails\models\ListModel as ListModel;

class Clientsemails_refreshList_View extends Vtiger_Index_View {

  function __construct(){
        $this->ListModel = new ListModel;  
  }

	public function process(Vtiger_Request $request) {    
      $this->ListModel->saveMails();   
  }       
}
