<?php

  include "modules/Clientsemails/models/ListModel.php";

  use modules\Clientsemails\models\ListModel as ListModel;

  class Clientsemails_Detail_View extends Vtiger_Detail_View {


	function showModuleDetailView(Vtiger_Request $request) {
		$this->showLineItemDetails($request);
		return parent::showModuleDetailView($request);
	}

  function showLineItemDetails(Vtiger_Request $request) {

    global $current_user;

    $this->ListModel = new ListModel;  
    $recordid = $request->get('record'); 

    if(!$_POST['answer']){
      $this->ListModel->setSeen($recordid,$current_user->id);
    }
    
    $getClaimMail = $this->ListModel->getMail($recordid); 	
    $viewer = $this->getViewer($request);
    $viewer->assign('GET_CLAIM_MAILS', $getClaimMail); 

  }
  

}
