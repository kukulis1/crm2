<?php
namespace modules\Clientsemails\models;

require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';

require_once 'vtlib/Vtiger/Email/vendor/autoload.php';

use PhpImap\Exceptions\ConnectionException;
use PhpImap\Mailbox;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


class ListModel {

  protected $db;
  protected $mailHost;
  protected $mailUsername;
  protected $mailPassword;

  public function __construct()
  {
    global $adb;
    $this->db = $adb; 
  }

  public function getMail($recordid)
  {
    $sql = "SELECT e.*, GROUP_CONCAT(n.notesid,',',attachmentsid SEPARATOR '|##|') AS attachment,
     CASE WHEN cf_1839 != email && email != 'kokybe@parnasas.lt' THEN true ELSE false END as cc
               FROM vtiger_clientsemails_email e
               LEFT JOIN vtiger_claimscf cf ON cf.claimsid=e.client_email_id               
               LEFT JOIN vtiger_senotesrel s ON s.crmid=client_email_id
               LEFT JOIN vtiger_notes n ON n.notesid=s.notesid
			         LEFT JOIN vtiger_seattachmentsrel r ON r.crmid=n.notesid    
               WHERE client_email_id = ?
               GROUP BY e.id
               ORDER BY id DESC";
    $result = $this->db->pquery($sql, array($recordid));
    return $result;
  }

  public function generateHash($mail_id)
  {
    $date = time();
    $string = "[ref:_CLIENT_EMAIL_";
    $string .= $date.".".$mail_id;
    $string .= ":ref]";
    return $string;
  }


  public function saveMails()
  { 
    global $mailHostQuality, $mailUsername,$mailPassword;
    $this->mailHost = $mailHostQuality;
    $this->mailUsername = $mailUsername;
    $this->mailPassword = $mailPassword;  
    $date = date("Y-m-d H:i:s");
    $setype = 'Clientsemails';

    $sql = "INSERT INTO vtiger_clientsemails (clientsemailsid,clientsemails_tks_sendername,clientsemails_tks_email) VALUES(?,?,?)";
    $sql1 = "INSERT INTO vtiger_clientsemailscf (clientsemailsid) VALUES(?)";
    $sql2 = "INSERT INTO vtiger_clientsemails_email (client_email_id,mailid,hash,type,subject,email,body) VALUES(?,?,?,?,?,?,?)";
    
    $sql4 = "SELECT client_email_id FROM vtiger_clientsemails_email WHERE type = 1 AND mailid = ? LIMIT 1";    

    $sql5 = "SELECT client_email_id FROM vtiger_clientsemails_email WHERE type = 1 AND email = ? OR cc = ? ORDER BY client_email_id DESC LIMIT 1";

    $sql6 = "INSERT INTO vtiger_clientsemails_seen (mail_id, seen, seen_time, reviewed_person) VALUES (?,?,?,?)";
    
    // $sql7 = "UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?";

    $sql8 = "UPDATE vtiger_clientsemails_seen SET seen = ?, seen_time = ?, reviewed_person = ? WHERE mail_id = ?";

    $sql9 = "SELECT client_email_id,hash FROM vtiger_clientsemails_email WHERE type = 1 AND hash = ? LIMIT 1";
    
    $mailbox = new Mailbox($this->mailHost,$this->mailUsername,$this->mailPassword);  
    $last_check = date("Ymd");
    // $last_check = '20210312';

    try {
      $mail_ids = $mailbox->searchMailbox('SINCE "'.$last_check.'"');

      foreach ($mail_ids AS $mail_id) {  

        $email = $mailbox->getMail(
            $mail_id, 
            true 
        ); 
        $pattern = "/[[][a-z]{3}[:][_][A-Z]{6}[_][A-Z]{5}[_]\d{10}[.]\d{1,9}[:][a-z]{3}[]]/";   

        $matches = array(); 
        preg_match($pattern, $email->subject, $matches);          
        $subject_hash = $matches[0]; 
    
        $check_email = $this->db->pquery($sql4,array($mail_id));
        $check_hash = $this->db->pquery($sql9,array($subject_hash));
  
        $getId = $this->db->pquery($sql5,array($email->fromAddress,$email->fromAddress));

        $num_rows2 =  $this->db->num_rows($check_email);     
        $num_rows3 =  $this->db->num_rows($check_hash);     

        if(strpos($email->textPlain, "From")){
          $body = substr($email->textPlain, 0, strpos($email->textPlain, "From"));
        }else{
          $body = $email->textPlain;
        }

        if(!$num_rows2){
          if(!$num_rows3){       
            $hash = $this->generateHash($mail_id);           
            $entity_id = $this->last_entity_record($this->db);
            $this->insert_entity($this->db, $setype, $entity_id, 26, NULL, $email->fromName,'Email', $date);          
            $this->db->pquery($sql,array($entity_id,$email->fromName,$email->fromAddress));  
            $this->db->pquery($sql1,array($entity_id));
            $this->db->pquery($sql2,array($entity_id,$mail_id,$hash,1 ,$email->subject,$email->fromAddress,$body));
            $this->db->pquery($sql6,array($entity_id,0,'',''));   
            $letter_id = $this->db->getLastInsertID();
            $this->addAttachment($this->db,$email,$entity_id,$letter_id);                    
          }else{      
            $client_mail_id = $this->db->query_result($check_hash,0,'client_email_id');
            $hash = $this->db->query_result($check_hash,0,'hash');
            $this->db->pquery($sql2,array($client_mail_id,$mail_id,$hash,1 ,$email->subject,$email->fromAddress,$body));
            $this->db->pquery($sql8,array(0,'','',$client_mail_id));
            $letter_id = $this->db->getLastInsertID();
            $this->addAttachment($this->db,$email,$client_mail_id,$letter_id);              
          }
        }     
      }    
    } catch (ConnectionException $ex) {
        die('IMAP connection failed: '.$ex->getMessage());
    } catch (Exception $ex) {
        die('An error occured: '.$ex->getMessage());
    } 
 
    $mailbox->disconnect();  
    
    header("Location:/index.php?module=Clientsemails&view=List");
  }
  
public function setSeen($recordId, $userId){ 
  $sql = "SELECT seen FROM vtiger_clientsemails_seen WHERE mail_id = ? AND seen = ?";
  $check_result = $this->db->pquery($sql,array($recordId, 0));  
  $num_rows =  $this->db->num_rows($check_result);

  if($num_rows){
    $sql2 = "UPDATE vtiger_clientsemails_seen SET seen = ?, seen_time = ?, reviewed_person = ? WHERE mail_id = ? ";
    $date = date('Y-m-d H:i:s');
    $this->db->pquery($sql2, array(1,$date,$userId,$recordId));

    $result = $this->db->pquery("SELECT CONCAT(first_name,' ',last_name) as employee FROM vtiger_users WHERE id = ?",array($userId));
    $employee =  $this->db->query_result($result, 0, 'employee');
    $id = $this->last_modtracker_record2();    
    $this->db->pquery("INSERT INTO vtiger_modtracker_basic (id, crmid, module, whodid, changedon,status) VALUES (?,?,?,?,?,?)", array($id,$recordId,'Clientsemails',26, $date,0));

    // kas perziurejo
    $this->db->pquery("UPDATE vtiger_clientsemailscf SET cf_1988 = ? WHERE clientsemailsid = ?", array($employee,$recordId));
    $this->db->pquery("INSERT INTO vtiger_modtracker_detail (id, fieldname, prevalue, postvalue) VALUES (?,?,?,?)", array($id,'cf_1988', '', $employee));   
   
  }
}

public function last_modtracker_record2()
{
  $result = $this->db->pquery('SELECT id FROM vtiger_modtracker_basic_seq',array());
  $curid =  $this->db->query_result($result, 0, 'id');
  $crmid = $curid + 1; 
  $this->db->pquery("UPDATE vtiger_modtracker_basic_seq SET id = ?", array($crmid));   
  return $crmid;
}

 public function addAttachment($db,$email,$id,$letter_id){
    if ($email->hasAttachments()) {                 
      $attachments = $email->getAttachments();
      $year = date("Y");
      $month = date("F");
      $dayofweek = "week".$this->weekOfMonth(strtotime(date("Y-m-d"))); 

      foreach ($attachments AS $attachment) {     
        
        if($attachment->name != 'jpeg'){        
          if (!file_exists("storage/$year/$month/$dayofweek")) {
            mkdir("storage/$year/$month/$dayofweek", 0777, true);
          } 
            $ext = pathinfo($attachment->name, PATHINFO_EXTENSION); 
            $oldName = $this->lt_chars(str_replace(' ', '',pathinfo($attachment->name, PATHINFO_FILENAME)));                             
              if(empty($ext)) $ext = $oldName;
              $newFileName = $oldName.'_'.time().'.'.$ext;          
              $filename = $oldName.'_'.time();          
              $attachment->setFilePath("storage/$year/$month/$dayofweek/$newFileName");            
              $patch = "storage/$year/$month/$dayofweek/";        
              
              if ($attachment->saveToDisk()) {              
                $this->insertDocument($db,$id,26,$patch,$filename, $newFileName,$letter_id);                            
              } else {
                echo "ERROR, could not save!<br>";
              } 
        }                   
      }  
               
    }
 }

  public function insertDocument($db,$recordid,$userid,$patch,$filename,$filefullname,$letter_id)
  {
    global $root_directory;
    $date_now = date('Y-m-d H:s:i');
    $entity_id = $this->last_entity_record($db);   
    $this->insert_entity($db,'Documents', $entity_id, $userid, NULL, '', 'CRM', $date_now);
    $this->insert_senotesrel($db, $entity_id, $recordid);
    $this->insertNotes($db,$entity_id,$filename,$filefullname,1,'I',1);    
    $this->insert_NoteCf($db, $entity_id);
    
    $entity_id2 = $this->last_entity_record($db);  
    $db->query("INSERT INTO vtiger_claims_letters_attachments (letter_id,attachment_id) VALUES ($letter_id,$entity_id2)");
    if(renameFile($patch,$filefullname,$entity_id2)){
      $this->insert_entity($db,'Documents Attachment', $entity_id2, $userid, NULL, '', 'CRM', $date_now);
      $this->insertAttachments($db, $entity_id2,$filefullname,$patch,$recordid);
      $this->insertSeAttachmentsrel($db, $entity_id,$entity_id2);	
    }
  
  }

  public function last_entity_record($adb)
  {
      $sql = "SELECT id FROM `vtiger_crmentity_seq`";
      $sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
      $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
      $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
      $lock3 = 'UNLOCK TABLES';
  
      $this->db->pquery($lock, array()); 
        $get_id = $this->db->pquery($sql, array()); 
        $seq = $this->db->query_result($get_id,0,'id');    
      $this->db->pquery($lock3, array());    
  
      $new_seq = $seq + 1;   
  
      $this->db->pquery($lock2, array()); 
       $this->db->pquery($sql2, array($new_seq)); 
      $this->db->pquery($lock3, array()); 
  
      return $new_seq;
  }

  public function insert_entity($adb, $setype, $entity_id, $user_id, $description, $label,$source, $date)
  {
    $sth = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, source, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
    $adb->pquery($sth, array($entity_id, $user_id, $user_id, $setype, $description, $label,$source,$date, $date));
  }

  public function insert_senotesrel($conn, $entity_id, $moduleId)
  {
    $sth = $conn->query("INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES ('$moduleId', '$entity_id')");
  }

  public function insertAttachments($conn, $entity_id,$title,$filename,$recordid)
  {
    $sth = $conn->query("INSERT INTO vtiger_attachments (attachmentsid, name, path,related_id)  VALUES ('$entity_id', '$title', '$filename','$recordid')");
  }

  public function insertSeAttachmentsrel($conn, $entity_id,$moduleId)
  {
    $sth = $conn->query("INSERT INTO vtiger_seattachmentsrel (crmid, attachmentsid)  VALUES ('$entity_id', '$moduleId')");
  }

  public function insertNotes($conn, $entity_id,$title,$filename,$folderid,$locationType,$filestatus)
  {
    $sth = $conn->query("INSERT INTO vtiger_notes (notesid, title, filename, folderid, filelocationtype, filestatus) 
                            VALUES ('$entity_id', '$title', '$filename', '$folderid','$locationType','$filestatus')");
  }

  public function insert_NoteCf($conn, $entity_id)
  {
    $sth = $conn->query("INSERT INTO vtiger_notescf (notesid) VALUES ('$entity_id')");
  }

  public function lt_chars($text) {   
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

    return $text; 
  }

  public function weekOfMonth($date) 
  {  
  $firstOfMonth = strtotime(date("Y-m-01", $date));
  return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
  }

  function renameFile($location,$filename,$entity_id)
  { 
    global $root_directory;
    $oldName = $root_directory.'/'.$location.$filename;
    $newName = $root_directory.'/'.$location.$entity_id.'_'.$filename;
    if(rename("$oldName","$newName")){
      return true;
    }
  
  }


}