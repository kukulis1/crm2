<?php

include "modules/Salesordersdocuments/models/ordersModel.php";


use modules\Salesordersdocuments\models\ordersModel as ordersModel;

class Salesordersdocuments_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->ordersModel = new ordersModel;  
       }

	public function process(Vtiger_Request $request) {
              global $site_URL;
              $moduleName = $request->getModule();
              $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
              $params = [];
              
              if(isset($_GET['filter_by'])){
                     $params = $_GET;  
                     $url = $site_URL."/index.php".$this->getPageUrl($params);
              }else{
                     $url = $site_URL."/index.php?module=Salesordersdocuments&view=List&app=SALES";
              }     
              
              
              $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1; 
              $perPage = 100;   
              $startAt = $perPage * ($page - 1);
              $orders = $this->ordersModel->getOrders($perPage,$startAt, $params);   
              $total = $this->ordersModel->getAllOrders($params); 
              
              $from_number = (!$startAt ? 1 : $startAt+1);
              $to_number = ($total < 100 ? $total : ($startAt+$perPage > $total ? $total : $startAt+$perPage)); 
              
              $pagination = $this->get_pagination_links($page, ceil($total/$perPage), $url); 
              
              $viewer = $this->getViewer($request);  
              $viewer->assign('MODULE_MODEL', $moduleModel);                
              $viewer->assign('orders', $orders['orders']);
              $viewer->assign('transit', $orders['transit']);
              $viewer->assign('params', $params);
              $viewer->assign('revised_measures', $orders['revised_measures']);
              $viewer->assign('documents', $orders['documents']);
              $viewer->assign('all_records', $total);
              $viewer->assign('FROM_NUMBER', $from_number);
              $viewer->assign('TO_NUMBER', $to_number);
              $viewer->assign('pagination', $pagination);
              $viewer->view('List.tpl', $request->getModule()); 
       }
       
       public function get_pagination_links($current_page, $total_pages, $url){
        $links = "";
        $links .= '<div class="pagination">';    
              if ($total_pages >= 1 && $current_page <= $total_pages) {
                     $links .= "<a ".($current_page == 1 ? 'class=active' : '')." href=\"{$url}&page=1\">1</a>";
                     $i = max(2, $current_page - 5);
                     if ($i > 2)
                     $links .= "<div>...</div>";
                     for (; $i < min($current_page + 6, $total_pages); $i++) {
                     $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$i}\">{$i}</a>";
                     }
                     if ($i != $total_pages)
                     $links .= "<div>...</div>";
                     $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$total_pages}\">{$total_pages}</a>";
              }
              $links .= '</div>';
              return $links;
       }

       public function getPageUrl($params){
              $url = '';
              foreach($params AS $key => $value){
                     if(!empty($value) && $key != 'page'){
                            $separator = ($key == 'module' ? '?' : '&');                           
                            $url .= "{$separator}{$key}={$value}";   
                                               
                     }
              }        

              return $url;
       }


}
