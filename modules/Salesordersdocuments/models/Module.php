<?php

class Salesordersdocuments_Module_Model extends Vtiger_Module_Model {


	public static function getPostCodeInfo(array $post_code, array $city){
		$db = PearDatabase::getInstance();

		$bill_city = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', html_entity_decode(trim($city[0]))));		
		$ship_city = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', html_entity_decode(trim($city[1]))));		

		$result = $db->pquery("SELECT DISTINCT city, zone_customer FROM crm_post_codes WHERE post_code = ?", [$post_code[0]]);
		
		$result2 = $db->pquery("SELECT DISTINCT city, zone_customer FROM crm_post_codes WHERE post_code = ?", [$post_code[1]]);

		$cities = [];

		foreach ($result as $row) {
			$get_city = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', html_entity_decode(trim($row['city']))));
			$zone_customer = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', html_entity_decode(trim($row['zone_customer']))));
		 
			if(!empty($get_city)){
				if(!strpos($get_city, ' k.'))	$cities[trim($row['city'])] = $get_city;
			}		
		
			if(!empty($zone_customer)) {
				if(!strpos($zone_customer, ' k.')) $cities[trim($row['zone_customer'])] = $zone_customer;
			}

		}	
		

		$cities2 = [];

		foreach ($result2 as $row) {
			$get_city = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', html_entity_decode(trim($row['city']))));
			$zone_customer = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', html_entity_decode(trim($row['zone_customer']))));		
		
			if(!empty($get_city)){
				if(!strpos($get_city, ' k.'))	$cities2[trim($row['city'])] = $get_city;
			}		
		
			if(!empty($zone_customer)) {
				if(!strpos($zone_customer, ' k.')) $cities2[trim($row['zone_customer'])] = $zone_customer;
			}
		}	
		
		$status = (in_array($bill_city, $cities) ? 0 : 1);		
		$status2 = (in_array($ship_city, $cities2) ? 0 : 1);

		$show_cities = 'Neegzistuojantis pašto kodas';
		if(count($cities) > 0){
			$show_cities =	implode(',', array_keys($cities));
		}

		$show_cities2 = 'Neegzistuojantis pašto kodas';
		if(count($cities2) > 0){
			$show_cities2 =	implode(',', array_keys($cities2));
		}
		
		
	 return ['bill' => ['status' => $status, 'city' => $city[0], 'location' => $show_cities],
					 'ship' => ['status' => $status2, 'city' => $city[1], 'location' => $show_cities2]];
	}
}