<?php

namespace modules\Salesordersdocuments\models;

require_once(__DIR__.'/../../../Helpers/Helper.php');

class ordersModel {

  public function __construct(){
    global $adb;
    $this->db = $adb;
  }


  public function getOrders($perPage,$startAt, $params){
  
    $sql = "SELECT vtiger_salesorder.preinvoice, vtiger_salesorder.salesorder_no, IF(cf_1376 > 0,cf_1376, REPLACE(ROUND(vtiger_salesorder.total,2),'.',',')) as totalprice, vtiger_salesorder.salesorderid,vtiger_salesorder.load_date_from ,vtiger_salesorder.unload_date_from, vtiger_salesorder.sostatus,vtiger_salesorder.shipment_code, vtiger_salesorder.accountid,
    DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') AS createdtime, vtiger_soshipads.unload_company, vtiger_soshipads.ship_city ,vtiger_soshipads.ship_code,bill_country,ship_country ,vtiger_soshipads.ship_street, vtiger_sobillads.load_company, vtiger_sobillads.bill_city ,vtiger_sobillads.bill_code ,vtiger_sobillads.bill_street, vtiger_inventoryproductrel.margin , vtiger_accountscf.cf_1279, vtiger_accountscf.cf_1281,  vtiger_accountscf.cf_2752 AS taxable_dims_change_permisions, vtiger_salesordercf.cf_1376 as agreed_price,
    SUM(FORMAT(vtiger_inventoryproductrel.quantity,0)) as quantity,
    SUM(FORMAT(vtiger_inventoryproductrel.revised_quantity,0)) as revised_quantity,
      SUM(vtiger_inventoryproductrel.ordered_weight) as ordered_weight, 
      SUM(vtiger_inventoryproductrel.revised_weight) as revised_weight, 
      ROUND(SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity),2)  as ordered_volume,
      ROUND(SUM(vtiger_inventoryproductrel.m3),2)  as revised_volume,
      CASE WHEN inventory_type = '' THEN (SELECT GROUP_CONCAT(packages_tks_tare,' ',packages_tks_kg,' ',packages_tks_length,'x',packages_tks_width,'x',packages_tks_height SEPARATOR '#:#') FROM vtiger_packages INNER JOIN vtiger_crmentity on crmid=packagesid WHERE vtiger_packages.cf_20023=vtiger_salesorder.salesorderid AND deleted = 0 )  END AS order_cargos,
      ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(revised_quantity) ELSE SUM(quantity) END,2) AS taxable_quantity,
      ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(revised_weight) ELSE SUM(ordered_weight) END,2) AS taxable_weight,
      ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(m3) ELSE SUM((ordered_length * ordered_width * ordered_height) * quantity) END,2)  AS taxable_volume,
      
      CASE WHEN SUM(vtiger_inventoryproductrel.ordered_weight) < SUM(vtiger_inventoryproductrel.revised_weight) AND cf_1376 > 0
        THEN SUM(vtiger_inventoryproductrel.revised_weight) > 
          (SUM(vtiger_inventoryproductrel.ordered_weight) + (SUM(vtiger_inventoryproductrel.ordered_weight) * 0.10))
          WHEN SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity) < SUM(vtiger_inventoryproductrel.m3) AND cf_1376 > 0
          THEN SUM(vtiger_inventoryproductrel.m3) > 
          (SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity) + 
          (SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity) * 0.10))
        ELSE 0
      END AS warning, 
  
  
      CASE WHEN cargo_measure IN (1,7) OR cargo_measure IN ('pll','nestd.') THEN ROUND(SUM((ordered_length * ordered_width) * quantity) /0.96 ,2) ELSE 0 END AS ordered_pll,
      CASE 
          WHEN revised_measure IN (1,7) 
              OR revised_measure IN ('pll','nestd.') 
              THEN (
                  SELECT SUM(pll_rel.pll)
                  FROM vtiger_inventoryproductrel as pll_rel
                  WHERE pll_rel.id = vtiger_salesorder.salesorderid
              )
          ELSE 0 
      END AS revised_pll,
  
      CASE 
          WHEN app_taxable_dimensions.type = 'revised' THEN 
              CASE 
                  WHEN revised_measure IN (1,7) 
                      OR revised_measure IN ('pll','nestd.') 
                      THEN (
                          SELECT SUM(pll_rel.pll)
                          FROM vtiger_inventoryproductrel as pll_rel
                          WHERE pll_rel.id = vtiger_salesorder.salesorderid
                      )
                  ELSE 0 
              END
          ELSE 
              CASE WHEN cargo_measure IN (1,7) OR cargo_measure IN ('pll','nestd.') THEN ROUND(SUM((ordered_length * ordered_width) * quantity) /0.96 ,2) ELSE 0 
      END end AS taxable_pll, 
      CASE WHEN app_taxable_dimensions.type = 'fail' OR app_taxable_dimensions.type IS NULL THEN 0 ELSE 1 END AS show_taxable,
      CASE WHEN vtiger_salesordercf.cf_855 = 'Transporto užsakymas' THEN 'T' ELSE 'P' END AS type, 
      IF(vtiger_salesorder_updated.status IS NOT NULL, IF(vtiger_salesorder_updated.status = 0, 0,1), 1) as update_status,
    SUM(vtiger_inventoryproductrel.cargo_wgt) as cargo_wgt, SUM(vtiger_inventoryproductrel.cargo_length) as cargo_length,SUM( vtiger_inventoryproductrel.cargo_width) as cargo_width, 
    SUM(vtiger_inventoryproductrel.cargo_height) as cargo_height, vtiger_account.accountname,vtiger_users.user_name,vtiger_users.first_name,vtiger_users.last_name, (vtiger_inventoryproductrel.cargo_length * vtiger_inventoryproductrel.cargo_height * vtiger_inventoryproductrel.cargo_width) as cargo_volume,
    SUM((vtiger_inventoryproductrel.cargo_length * vtiger_inventoryproductrel.cargo_width * vtiger_inventoryproductrel.cargo_height) * vtiger_inventoryproductrel.quantity ) AS volume,
    vtiger_salesordercf.cf_1297 as pricebook,cf_1558 AS route, vtiger_salesordercf.cf_930 as termo, cf_1562 AS driver, cf_1560 AS transport, SUM(vtiger_inventoryproductrel.meters) AS meters, cf_2718 AS pricebook_details,
      (SELECT CONCAT(`city`,'/ ',IF(`city` != `zone_customer`, CONCAT(`zone_customer`,'/'),''),' ', `state`, '/ ', `zone_base`) FROM crm_post_codes WHERE post_code = vtiger_sobillads.bill_code LIMIT 1) AS `location_from`,
      (SELECT CONCAT(`city`,'/ ',IF(`city` != `zone_customer`, CONCAT(`zone_customer`,'/'),''),' ', `state`, '/ ', `zone_base`) FROM crm_post_codes WHERE post_code = vtiger_soshipads.ship_code LIMIT 1) AS `location_to`,
      (SELECT max(minifest) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as minifest,
      (SELECT max(cmr) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as cmr,
      (SELECT max(invoice) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as invoice ";
          if(!empty($params['signatares'])){
            $sql .= " ,(SELECT filename  FROM `vtiger_senotesrel` 
                    LEFT JOIN `vtiger_notes` as pod_notes ON pod_notes.notesid=vtiger_senotesrel.notesid  AND pod_notes.folderid = 2                       
                    WHERE  vtiger_senotesrel.crmid = vtiger_salesorder.salesorderid LIMIT 1) AS filename ";
          }

                  $sql .= " FROM vtiger_salesorder                                      
                            INNER JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid
                            LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                            LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                            LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid                                    
                            LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid                                    
                            LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid     
                            LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid  
                            LEFT JOIN `app_taxable_dimensions` ON app_taxable_dimensions.salesorderid=vtiger_salesorder.salesorderid   
                            LEFT JOIN `vtiger_salesorder_updated` ON vtiger_salesorder_updated.salesorderid=vtiger_salesorder.salesorderid
                            LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid AND productid = 14244
                            WHERE vtiger_salesordercf.cf_1456 = 0 AND cf_1614 = 0 AND (vtiger_salesorder.shipment_code IS NOT NULL AND vtiger_salesorder.shipment_code != '') AND 
                                  cf_1468 = 0 AND
                                  vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder'";
                            if(!empty($params)){
                              $sql .= $this->getWhere($params);
                            }else{
                              $sql .= " GROUP BY vtiger_salesorder.salesorderid ";
                            }
                            $sql .= " ORDER BY vtiger_salesorder.shipment_code ASC 
                            LIMIT $startAt, $perPage";                   
                          
                            
    $query2 = "SELECT GROUP_CONCAT(DISTINCT pod_notes.filename SEPARATOR '|##|' ) AS pod_url,
                  CASE WHEN pod_notes.notecontent 
                    THEN GROUP_CONCAT(DISTINCT pod_notes.notecontent SEPARATOR '|##|' ) 
                    ELSE GROUP_CONCAT(DISTINCT pod_notes.title SEPARATOR '|##|' ) 
                  END  AS pod_title,
                  GROUP_CONCAT(DISTINCT pod_notescf.cf_1852 SEPARATOR '|##|') as pod_date,   
                  GROUP_CONCAT(DISTINCT doc_notes.filename SEPARATOR '|##|' ) AS doc_url,                     
                  CASE WHEN doc_notes.notecontent 
                    THEN GROUP_CONCAT(DISTINCT doc_notes.notecontent SEPARATOR '|##|' ) 
                    ELSE GROUP_CONCAT(DISTINCT doc_notes.title SEPARATOR '|##|' ) 
                  END  AS doc_title,
                  GROUP_CONCAT(DISTINCT doc_notescf.cf_1852 SEPARATOR '|##|') as doc_date
                  FROM `vtiger_senotesrel` 
                  LEFT JOIN `vtiger_notes` as pod_notes ON pod_notes.notesid=vtiger_senotesrel.notesid  AND pod_notes.folderid = 2      
                  LEFT JOIN `vtiger_notes` as doc_notes ON doc_notes.notesid=vtiger_senotesrel.notesid  AND doc_notes.folderid = 3 
                  LEFT JOIN vtiger_notescf as pod_notescf ON pod_notescf.notesid=pod_notes.notesid  AND pod_notes.folderid = 2
                  LEFT JOIN vtiger_notescf as doc_notescf ON doc_notescf.notesid=doc_notes.notesid  AND doc_notes.folderid = 3
                  WHERE  vtiger_senotesrel.crmid = ?";                        
                
    $this->db->query("SET SESSION group_concat_max_len = 1000000");                               
    $records = $this->db->query($sql);

    $result = array();

    foreach($records AS $row){
        $documents = $this->db->pquery($query2, array($row['salesorderid']));    
        $documents = $this->db->pquery($query2, array($row['salesorderid']));    
        $documents = $this->db->pquery($query2, array($row['salesorderid']));    
        $result['orders'][] = $row;
        
        $required_revised_measures[] = $row['salesorderid'];
        $result['transit'][$row['salesorderid']] = (strtoupper($row['bill_country']) == 'LTU' && strtoupper($row['ship_country']) == 'LTU' ? 1 : 0);

        $result['documents']['doc_url'][$row['salesorderid']] =  $this->db->query_result($documents,0,'doc_url');
        $result['documents']['doc_title'][$row['salesorderid']] = $this->db->query_result($documents,0,'doc_title');
        $result['documents']['doc_date'][$row['salesorderid']] = $this->db->query_result($documents,0,'doc_date');
        $result['documents']['pod_url'][$row['salesorderid']] =  $this->db->query_result($documents,0,'pod_url');
        $result['documents']['pod_title'][$row['salesorderid']] = $this->db->query_result($documents,0,'pod_title');
        $result['documents']['pod_date'][$row['salesorderid']] = $this->db->query_result($documents,0,'pod_date');
    }

    $result['revised_measures'] = \Helpers\CargosHelper::getAllRevisedMeasures($this->db, $required_revised_measures);


    return $result;
 }
 

 public function getWhere($params){

   $date_from_to = explode(',', $params['date_from_to']);
   $filterBy = $params['filter_by'];
   $orderDate = $params['order_date'];
   $orderNumber = $params['shipment_code'];
   $customer = $params['customer'];
   $pricebook = $params['pricebook'];
   $type = $params['type'];
   $loadDate = $params['load_date'];
   $loadAddress = $params['load_address'];
   $unloadDate = $params['unload_date'];
   $unloadAddress = $params['unload_address'];
   $signatare = $params['signatares'];
   $document = $params['documents'];
   $route = $params['route'];
   $driver = $params['drivers'];
   $transport = $params['transport'];
   $status = $params['status'];
   $manager = $params['manager'];
    
    $query = '';
    if(!empty($status)){
      $query .= " AND  vtiger_salesorder.sostatus LIKE '$status%' ";
    }     
                              
    if(!empty($date_from_to[0]) && !empty($date_from_to[1])){
      if($filterBy == 'order_date'){                                                                                       
        $query .= " AND  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$date_from_to[0]' AND '$date_from_to[1]' ";
      }elseif($filterBy == 'load_date'){
        $query .= " AND  vtiger_salesorder.load_date_from BETWEEN '$date_from_to[0]' AND '$date_from_to[1]' ";
      }elseif($filterBy == 'unload_date'){
        $query .= " AND  vtiger_salesorder.unload_date_from BETWEEN '$date_from_to[0]' AND '$date_from_to[1]' ";
      }
    }
    if(!empty($orderDate)){
      $query .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') =  '$orderDate' ";
    }                                         
    if(!empty($orderNumber)){
      $query .= " AND vtiger_salesorder.shipment_code LIKE '%$orderNumber%' ";
    }
    if(!empty($customer)){
      $query .= " AND vtiger_account.accountname LIKE '$customer%' ";
    }
    if(!empty($pricebook)){
      $query .= " AND vtiger_salesordercf.cf_1297 LIKE '$pricebook%' ";
    }
    if(!empty($type)){
      if($type == 'T') $type = 'Transporto užsakymas'; elseif($type == 'P') $type = 'Perkraustymo užsakymas';
      $query .= " AND vtiger_salesordercf.cf_855 LIKE '$type%' ";
    }
    if(!empty($loadDate)){
      $query .= " AND vtiger_salesorder.load_date_from = '$loadDate' ";
    }
    if(!empty($loadAddress)){
      $query .= " AND  CONCAT(vtiger_sobillads.load_company,' ', vtiger_sobillads.bill_street,' ', vtiger_sobillads.bill_city,' ',vtiger_sobillads.bill_code) LIKE '%$loadAddress%' ";
    }
    if(!empty($unloadDate)){
      $query .= " AND  vtiger_salesorder.unload_date_from = '$unloadDate' ";
    }
    if(!empty($unloadAddress)){  
      $query .= " AND CONCAT(vtiger_soshipads.unload_company,' ', vtiger_soshipads.ship_street,' ', vtiger_soshipads.ship_city,' ',vtiger_soshipads.ship_code) LIKE '%$unloadAddress%' ";
    }                                                             
    if(!empty($route)){  
      $query .= " AND vtiger_salesordercf.cf_1558 LIKE '$route%' ";
    }
    if(!empty($driver)){  
      $query .= " AND vtiger_salesordercf.cf_1560 LIKE '$driver%' ";
    }
    if(!empty($transport)){  
      $query .= " AND vtiger_salesordercf.cf_1562 LIKE '$transport%' ";
    }
    if(!empty($manager)){
      $query .= " AND  vtiger_users.user_name LIKE '$manager%' OR vtiger_users.first_name LIKE'$manager%' OR vtiger_users.last_name  LIKE '$manager%' OR CONCAT(vtiger_users.first_name,vtiger_users.last_name) LIKE '%$manager%'";
    }   

    $query .= " GROUP BY vtiger_salesorder.salesorderid"; 

    
    if(!empty($document) || !empty($signatare) ){
      $query .= " HAVING ";
    }  
    if($document == 'true'){  
      $query .= " (minifest IS NOT NULL AND minifest != 0) OR (cmr IS NOT NULL AND cmr != 0) OR (invoice IS NOT NULL AND invoice != 0) AND ";
    }else if($document == 'false'){
      $query .= "(minifest IS NULL OR minifest = 0) OR (cmr IS  NULL OR cmr = 0) OR (invoice IS NULL OR invoice = 0) AND ";
    } 
    if($signatare == 'true'){  
      $query .= " filename IS NOT NULL ";
    }else if($signatare == 'false'){
      $query .= " filename IS NULL ";
    }  

    $query = rtrim($query, 'AND ');
  
    return $query;
  }


 public function getAllOrders($params){
  $sql = "SELECT vtiger_salesorder.salesorderid ";
  if(!empty($params['signatares'])){
    $sql .= " ,(SELECT filename  FROM `vtiger_senotesrel` LEFT JOIN `vtiger_notes` as pod_notes ON pod_notes.notesid=vtiger_senotesrel.notesid  
                AND pod_notes.folderid = 2 WHERE  vtiger_senotesrel.crmid = vtiger_salesorder.salesorderid LIMIT 1) AS filename ";
  }
  if(!empty($params['documents'])){
    $sql .= " ,(SELECT max(minifest) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as minifest,
    (SELECT max(cmr) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as cmr,
    (SELECT max(invoice) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as invoice ";
  }
  $sql .= " FROM vtiger_salesorder         
          LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid                              
          LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid   
          LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid   
          LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
          LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid                                                        
          INNER JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid    
          LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid  
          JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid AND productid = 14244 
          WHERE vtiger_salesordercf.cf_1456 = 0 AND cf_1468 = 0 AND cf_1614 = 0 AND (vtiger_salesorder.shipment_code IS NOT NULL AND vtiger_salesorder.shipment_code != '') AND  vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' ";
          if(!empty($params)){
            $sql .= $this->getWhere($params);
          }else{
            $sql .= " GROUP BY vtiger_salesorder.salesorderid ";
          } 


    $records = $this->db->query($sql);
    $allRecords = $records->rowCount();    

    return $allRecords;
 }

}
