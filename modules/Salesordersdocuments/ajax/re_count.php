<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
if($_POST){

    include $_SERVER['DOCUMENT_ROOT'].'/v1/external_data/ws.config.php';
    global $config;

    require_once $_SERVER['DOCUMENT_ROOT'].'/v1/external_data/getPriceFromPriceBook.php';


    try {

        require($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

        $salesorderid = $_POST['salesorderid'];       

        $sth = $dbh->prepare("SELECT accountname, vtiger_accountscf.cf_918 as termo_multiplier, cf.cf_930 as termo, GROUP_CONCAT(CONCAT(l.ordered_weight,' ',l.ordered_length,'x',l.ordered_width,'x',ordered_height)) AS ordered, 
                                    GROUP_CONCAT(CONCAT(l.revised_weight,' ',l.revised_length,'x',l.revised_width,'x',revised_height)) AS revised, 
                                    GROUP_CONCAT(l.quantity) AS quantity,
                                    GROUP_CONCAT(l.revised_quantity) AS revised_quantity,
                                    GROUP_CONCAT(l.external_load_id) AS external_load_id, 
                                    ROUND(cf_928,2) AS shipment_km,
                                    ROUND(SUM(l.m3),2) AS m3, a.accountid, sa.salesorderid, bill_code,ship_code,
                                    CASE WHEN cargo_measure = 'pll' THEN SUM(l.quantity) ELSE 0 END AS  pll,
                                    CASE WHEN revised_measure = 'pll' THEN FLOOR(SUM(l.revised_quantity)) ELSE 0 END AS revised_pll,

                                    CASE WHEN cargo_measure IN (1,7) OR cargo_measure IN ('pll','nestd.') THEN ROUND(SUM((ordered_length * ordered_width) * quantity) /0.96 ,2) ELSE 0 END AS ordered_pll,
                                    CASE WHEN revised_measure IN (1,7) OR revised_measure IN ('pll','nestd.') THEN ROUND(SUM((revised_length * revised_width) * revised_quantity) /0.96 ,2) ELSE 0 END AS revised_pll2,

                                     sa.external_order_id,bill_country,ship_country
                                            FROM vtiger_salesorder sa
                                            LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=sa.salesorderid
                                            LEFT JOIN vtiger_account a ON a.accountid = sa.accountid 
                                            LEFT JOIN vtiger_accountscf ON  vtiger_accountscf.accountid = sa.accountid
                                            LEFT JOIN vtiger_crmentity e ON e.crmid = sa.salesorderid  AND e.setype = 'SalesOrder'     
                                            LEFT JOIN vtiger_inventoryproductrel l ON l.id = sa.salesorderid  AND productid = 14244  
                                            LEFT JOIN vtiger_sobillads b ON b.sobilladdressid=sa.salesorderid   
                                            LEFT JOIN vtiger_soshipads s ON s.soshipaddressid=sa.salesorderid   
                                            WHERE  e.deleted = 0 AND sa.salesorderid = ? 
                                            GROUP BY sa.salesorderid");

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($salesorderid));

        $sth2 = $dbh->prepare('UPDATE vtiger_salesorder SET      
                                                            total = ?,
                                                            subtotal = ?,                                                         
                                                            pre_tax_total = ?                                                     
                                                WHERE salesorderid = ?');                                            
                                    

        $sth4 = $dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ?  WHERE id = ? AND productid = 14244'); 
        $sth3 = $dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = ?, cf_1374 = ?,  cf_2718 = ? WHERE   salesorderid = ? '); 
        $sth5 = $dbh->prepare('UPDATE vtiger_crmentity  SET modifiedtime = ? WHERE crmid = ? '); 
        
        $sth9 = $dbh->prepare('INSERT INTO app_taxable_dimensions (salesorderid,type) VALUES (?,?)');
        $st10 = $dbh->prepare('SELECT salesorderid FROM app_taxable_dimensions WHERE salesorderid = ?');
        $sth11 = $dbh->prepare('UPDATE app_taxable_dimensions SET type = ? WHERE salesorderid = ?');
        $sth12 = $dbh->prepare('UPDATE vtiger_salesorder_updated SET status = ?, update_time = ? WHERE salesorderid = ?');

        $insert_log = $dbh->prepare('INSERT INTO vtiger_salesorder_dimensions_log (salesorderid, amount, pricebook, kg, m3, m2, pll, kg_revised, m3_revised, m2_revised, pll_revised, which, importdate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)');

        $row = $sth->fetch();  
        
        $accountid = $row['accountid'];
        $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
        $row['bill_code'] = preg_replace("/[^0-9]/", "",$row['bill_code']);
        $row['ship_code'] = preg_replace("/[^0-9]/", "",$row['ship_code']);
        $pll = $row['pll'];
        $ordered_pll = $row['ordered_pll'];
        $revised_pll = $row['revised_pll'];
        $revised_pll2 = $row['revised_pll2'];
        $date = date("Y-m-d");
        $date2 = date("Y-m-d H:i:s");       
        $quantity = explode(",", $row['quantity']);
        $quantity_revised = explode(",", $row['quantity_revised']);       
        $external_load_id = explode(",", $row['external_load_id']);    
        $weight_ordered = array();
        $dim_ordered = array();
        $dim_ordered2 = array();
        $square_ordered = array();
        $square_ordered_temp = array();
        $weight_revised = array();
        $dim_revised = array();
        $dim_revised2 = array();
        $square_revised = array();
        $square_revised_temp = array();

        $ordered = explode(",",$row['ordered']); 
   
        for($i = 0; $i < count($ordered); $i++){
           $exploded_arr = explode(' ',$ordered[$i]);
           $weight_ordered[] = $exploded_arr[0];         
           $dim_ordered[] = array_product(explode('x', $exploded_arr[1])) * $quantity[$i];     
           $dim_ordered2[] = explode('x', $exploded_arr[1]); 
           $square_ordered_temp[] = explode('x', $exploded_arr[1]); 
           unset($square_ordered_temp[$i][2]);           
           $square_ordered[] = array_product($square_ordered_temp[$i]) * $quantity[$i];
        }

        $revised = explode(",",$row['revised']);
        
        for($i = 0; $i < count($revised); $i++){
           $exploded_arr2 = explode(' ',$revised[$i]);
           $weight_revised[] = $exploded_arr2[0];       
           $dim_revised2[] = explode('x', $exploded_arr2[1]);
           $square_revised_temp[] = explode('x', $exploded_arr2[1]); 
           unset($square_revised_temp[$i][2]);
           $square_revised[] = array_product($square_revised_temp[$i]) * $quantity_revised[$i];         
        }


        $ordered_cargo_wgt = $exploded_arr[0];
        $revised_cargo_wgt = $exploded_arr2[0];

        $ordered_dim = explode('x', $exploded_arr[1]);
        $revised_dim = explode('x', $exploded_arr2[1]);

        $ordered_w = array_sum($weight_ordered);
        $ordered_m3 = ROUND(array_sum($dim_ordered),2);
        $ordered_m2 = array_sum($square_ordered);

        $revised_w = array_sum($weight_revised);       
        $revised_m3 = $row['m3'];
        $revised_m2 = array_sum($square_revised);  

        $quantity = array_sum($quantity);
   
        $getPrice = array();
        $getPrice_revised = array();
        $getPrice_ordered = array();
        

        if(!empty($row['bill_code']) AND !empty($row['ship_code']) AND (!empty($ordered_w) OR !empty($revised_w)) AND ($row['bill_country'] != "EST" AND $row['ship_country'] != "EST") ){



            if($ordered_w == $revised_w && $ordered_m3 == $revised_m3 && $ordered_m2 == $revised_m2 && $pll == $revised_pll){
                $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'], 'CLIENT'); 
                                  
                $detail_message = getPriceDetail($getPrice_revised, $getPrice_revised);
                
                if($getPrice_revised['price'] == 0){
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS');
                }

                $getPrice['price'] = $getPrice_revised['price'];
                $getPrice['pricebook'] =  $getPrice_revised['combination'];
                $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                $getPrice['type'] = 'fail';
                if($getPrice_revised['price'] > 0){
                    $getPrice['type'] = 'revised';
                   
                }
                $getPrice['salesorderid'] = $row['salesorderid'];
                $getPrice['dim'] = "$quantity/$revised_w/$revised_m3";  
            }else{

                $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'CLIENT'); 
                       
                if($getPrice_ordered['price'] > 0){
                 $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'],'CLIENT'); 
                }else{
                    $getPrice_revised['price'] = 0;
                    $getPrice_revised['combination'] = '';
                }

                $detail_message = getPriceDetail($getPrice_ordered, $getPrice_revised);
             

                if($getPrice_ordered['price'] == 0 || $getPrice_revised['price'] == 0){
                    $getPrice_ordered = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS');
                    $getPrice_revised = getPriceFromPriceBook($dbh, $row['bill_code'],$row['ship_code'],$accountid,$revised_w,$revised_m3, $revised_m2,$revised_pll,$distance,$row['bill_country'],$row['ship_country'],'BAZINIS'); 
                }

                if($getPrice_ordered['price'] > $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_ordered['price'];
                    $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                    $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];
                    $getPrice['type'] = 'fail';
                    if($getPrice_ordered['price'] > 0){
                        $getPrice['type'] = 'ordered';
                    }
                    $getPrice['salesorderid'] = $row['salesorderid'];
                    $getPrice['dim'] = "$quantity/$ordered_w/$ordered_m3/$ordered_pll";            

                }elseif($getPrice_ordered['price'] < $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_revised['price'];
                    $getPrice['pricebook'] =  $getPrice_revised['combination'];
                    $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                    $getPrice['type'] = 'fail';
                    if($getPrice_revised['price'] > 0){
                        $getPrice['type'] = 'revised';
                    }
                    $getPrice['salesorderid'] = $row['salesorderid'];
                    $getPrice['dim'] = "$quantity/$revised_w/$revised_m3/$revised_pll2";         
                }elseif($getPrice_ordered['price'] == $getPrice_revised['price']){                   
                        $getPrice['price'] = $getPrice_revised['price'];
                        $getPrice['pricebook'] =  $getPrice_revised['combination'];
                        $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                        $getPrice['type'] = 'fail';
                        if($getPrice_revised['price'] > 0){
                            $getPrice['type'] = 'revised';
                        }
                        $getPrice['salesorderid'] = $row['salesorderid'];
                        $getPrice['dim'] = "$quantity/$revised_w/$revised_m3/$revised_pll2";         
                }

            }
   
        }else{ 
            $getPrice = array('price' => 0);

            if(empty($row['bill_code']) AND empty( $row['ship_code']) AND !empty($ordered_w) AND !empty($revised_w)){
                $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
            }elseif(empty($row['bill_code']) AND empty($row['ship_code'])){    
                $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
            }elseif(empty($row['bill_code'])){
                $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
            }elseif(empty($row['ship_code'])){
                $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
            }elseif(empty($ordered_w) AND empty($revised_w)){
                    $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
            }elseif($row['bill_country'] == "EST" OR $row['ship_country'] == "EST"){
                $getPrice = array('pricebook' => 'Estija','price' => 0);
            }  
            
            $getPrice['type'] = '';
        } 

        $price = $getPrice['price'];     

        if(!empty($row['termo_multiplier']) && !empty($row['termo'])){
            $price = ceil( ((float)$price) * (((float)$row['termo_multiplier'])/100+1)*10000 )/10000;
        }

        $sth2->execute(array(
            $price,
            $price,                                                     
            $price,                                      
            $row['salesorderid']
        ));   
    

        $sth3->execute(array(                       
            $getPrice['pricebook'],  
            $price,              
            ($detail_message ?: 'Nepavyko nustatyti'),          
            $row['salesorderid']
        ));    
    
                    
        $sth4->execute(array(                                                                                          
            $price,                                                                                                  
            $row['salesorderid']
        ));

        $sth5->execute(array(                                                                                          
            $date2,                                                                        
            $row['salesorderid']
        ));  
        
        $st10->execute(array($row['salesorderid']));
    

        if(!$st10->rowCount()){
            $sth9->execute(array($row['salesorderid'],$getPrice['type']));        
        }else{
            $sth11->execute(array($getPrice['type'],$row['salesorderid']));
        }

        $sth12->execute(array(  
            1,                                                                                        
            $date2,                                                                        
            $row['salesorderid']
        ));


        $insert_log->execute(array($row['salesorderid'],
                    $price,
                    $getPrice['pricebook'],
                    $ordered_w,
                    $ordered_m3,
                    $ordered_m2,
                    $pll,
                    $revised_w,
                    $revised_m3,
                    $revised_m2,
                    $revised_pll,
                    'UPDATE recount',
                    $date2
        ));

        sendPriceToMetrika($row['external_order_id'], $price, 0, $price); 

        echo json_encode(array('status' => 'success','price' => str_replace('.',',',number_format($price,2)),'pricebook' => $getPrice['pricebook'],'taxable' => $getPrice['dim'], 'dimensions' => 'ordered: '. $ordered_w.' / '.$ordered_m3.' / '.$ordered_m2.' revised: '.$revised_w.' / '.$revised_m3.' / '.$revised_m2));


    } catch (PDOException $e) {
        $dbh->rollBack();    
        echo json_encode(array('status' => 'fail'));
    }

}else{
    http_response_code(404);
} 
             

function sendPriceToMetrika($external_order_id, $price,$price_agreed,$finish_price){
	$order = array();
	$order['order_id'] = $external_order_id;
	$order['price'] = $price;
	$order['price_agreed'] = $price_agreed;
	$order['finish_price'] = $finish_price;
	$orders[] = $order;
	$orders_indexed = array_values($orders);
	$result_data['orders_price'] = $orders_indexed;   
	$res = json_encode($result_data); 

    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/orders_price.php");           
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders_price' => $res));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);	
	$result = curl_exec($ch);
	curl_close($ch);

}


function getPriceDetail($ord, $rev){

    $message_ordered = '';
    $message_ordered_apm = '';

    $message_revised = '';  
    $message_revised_apm = ''; 

    if($ord['count_weight']['count'] > 0 && in_array($ord['price'], $ord['count_weight']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal kg, ';

    }else if($ord['count_weight']['count'] > 0 && !in_array($ord['price'], $ord['count_weight']['price'])){
        $message_ordered .= 'Pagal kg kainos nerasta, ';

    }else if($ord['count_weight']['count'] == 0 && !isset($ord['price']['count_weight']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno kg rėžio, ';               
    }             

    if($ord['count_m3']['count'] > 0 && in_array($ord['price'], $ord['count_m3']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal m3, ';

    }else if($ord['count_m3']['count'] > 0 && !in_array($ord['price'], $ord['count_m3']['price'])){
        $message_ordered .= 'Pagal m3 kainos nerasta, ';

    }else if($ord['count_m3']['count'] == 0 && !isset($ord['price']['count_m3']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno m3 rėžio, ';             
    }

    if($ord['count_m2']['count'] > 0 && in_array($ord['price'], $ord['count_m2']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal m2, ';

    }else if($ord['count_m2']['count'] > 0 && !in_array($ord['price'], $ord['count_m2']['price'])){
        $message_ordered .= 'Pagal m2 kainos nerasta, ';

    }else if($ord['count_m2']['count'] == 0 && !isset($ord['price']['count_m2']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno m2 rėžio, ';             
    }
    
    if($ord['count_pll']['count'] > 0 && in_array($ord['price'], $ord['count_pll']['price'])){
        $message_ordered_apm .= 'Kaina nustatyta pagal pll, ';

    }else if($ord['count_pll']['count'] > 0 && !in_array($ord['price'], $ord['count_pll']['price'])){
        $message_ordered .= 'Pagal pll kainos nerasta, ';

    }else if($ord['count_pll']['count'] == 0 && !isset($ord['price']['count_pll']['price'][0])){
        $message_ordered .= 'Neatitiko nei vieno pll rėžio, ';             
    }

    // Revised

    if($rev['count_weight']['count'] > 0 && in_array($rev['price'], $rev['count_weight']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal kg, ';

    }else if($rev['count_weight']['count'] > 0 && !in_array($rev['price'], $rev['count_weight']['price'])){
        $message_revised .= 'Pagal kg kainos nerasta, ';

    }else if($rev['count_weight']['count'] == 0 && !isset($rev['price']['count_weight']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno kg rėžio, ';               
    }             

    if($rev['count_m3']['count'] > 0 && in_array($rev['price'], $rev['count_m3']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal m3, ';

    }else if($rev['count_m3']['count'] > 0 && !in_array($rev['price'], $rev['count_m3']['price'])){
        $message_revised .= 'Pagal m3 kainos nerasta, ';

    }else if($rev['count_m3']['count'] == 0 && !isset($rev['price']['count_m3']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno m3 rėžio, ';             
    }

    if($rev['count_m2']['count'] > 0 && in_array($rev['price'], $rev['count_m2']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal m2, ';

    }else if($rev['count_m2']['count'] > 0 && !in_array($rev['price'], $rev['count_m2']['price'])){
        $message_revised .= 'Pagal m2 kainos nerasta, ';

    }else if($rev['count_m2']['count'] == 0 && !isset($rev['price']['count_m2']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno m2 rėžio, ';             
    }
    
    if($rev['count_pll']['count'] > 0 && in_array($rev['price'], $rev['count_pll']['price'])){
        $message_revised_apm .= 'Kaina nustatyta pagal pll, ';

    }else if($rev['count_pll']['count'] > 0 && !in_array($rev['price'], $rev['count_pll']['price'])){
        $message_revised .= 'Pagal pll kainos nerasta, ';

    }else if($rev['count_pll']['count'] == 0 && !isset($rev['price']['count_pll']['price'][0])){
        $message_revised .= 'Neatitiko nei vieno pll rėžio, ';             
    } 

    $why_bazinis = '';
    
    if($ord['price'] == 0){
        $why_bazinis .= 'Pereita į bazinį nes, pagal užsakytus duomenis nerasta kaina';
    }else if($rev['price'] == 0){
        $why_bazinis .= 'Pereita į bazinį nes, pagal faktinius duomenis nerasta kaina';
    }                     
    
    if(!empty($message_ordered_apm)){
        $message_ordered = rtrim($message_ordered_apm,', '); 
    }else{
        $message_ordered = rtrim($message_ordered,', ');             

    }
 
    
    $message_revised = rtrim($message_revised,', ');

    if(!empty($message_revised_apm)){
        $message_revised = rtrim($message_revised_apm,', '); 
    }else{
        $message_revised = rtrim($message_revised,', ');             

    }
    
    
    return "Užsakyti: $message_ordered -  ".$ord['combination']." / Faktas: $message_revised - ".$rev['combination']." / $why_bazinis";
}