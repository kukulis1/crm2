<?php
// error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../../../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $orderId = $_POST['orderId'];
  $userId = $_POST['userId'];
  $type = $_POST['type'];

  if($type == 'one'){
    if(!empty($orderId)){
      $result = setDocsReceive($conn, $orderId, $userId);

      if($result){
        echo json_encode(['status' => 'success']);
      }
      
    }else{
      echo json_encode(['status' => 'fail']);
    }

  }else{
    foreach($orderId as $salesorderid) {
      setDocsReceive($conn, $salesorderid, $userId);
    }
    echo json_encode(['status' => 'success']);
  }


}else{
  http_response_code(404);
}


function last_modtracker_record($conn)
{

  $result = $conn->query('SELECT id FROM vtiger_modtracker_basic_seq');
  $productid1 = $result->fetch_assoc();
  $crmid = $productid1['id'] + 1;    
  $conn->query("UPDATE vtiger_modtracker_basic_seq SET id = $crmid");  
  return $crmid;
}

function setDocsReceive($conn, $orderId, $userId){
  $list = mysqli_num_rows($conn->query("SELECT salesorderid FROM vtiger_salesordercf WHERE salesorderid = $orderId AND cf_1456 = 0"));
  $query = "UPDATE vtiger_salesordercf SET cf_1456 = 1 WHERE salesorderid =  $orderId";  
  $result = $conn->query($query);    

  if($list){
    $id = last_modtracker_record($conn);    
    $date = date("Y-m-d H:i:s"); 
    $query2 = "INSERT INTO vtiger_modtracker_basic (id, crmid, module, whodid, changedon,status) VALUES ('$id','$orderId','SalesOrder','$userId', '$date',0)";
    $query3 = "INSERT INTO vtiger_modtracker_detail (id, fieldname, prevalue, postvalue) VALUES ('$id','cf_1456', 0,1)"; 
    $conn->query($query2);
    $conn->query($query3);
  }

  return $result;
}