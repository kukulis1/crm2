<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "../../../config.inc.php";
require_once(__DIR__.'/../../../Helpers/Helper.php');

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

    $between = $_POST['between'];
    $date_from_to = explode(",",$between);   

    $filterBy = trim($_POST['filterBy']);
    $orderDate = trim($_POST['orderDate']);
    $orderNumber = trim($_POST['orderNumber']);
    $customer = trim($_POST['customer']);
    $pricebook = trim($_POST['pricebook']);
    $type = trim($_POST['type']);
    $loadDate = trim($_POST['loadDate']);
    $loadAddress = trim($_POST['loadAddress']);
    $unloadDate = trim($_POST['unloadDate']);
    $unloadAddress = trim($_POST['unloadAddress']);
    $signatare = trim($_POST['signatare']);
    $document = trim($_POST['documents']);
    $route = trim($_POST['route']);
    $driver = trim($_POST['driver']);
    $transport = trim($_POST['transport']);
    $status = trim($_POST['status']);
    $manager = trim($_POST['manager']);

    if(!empty($between) || !empty($orderDate) || !empty($orderNumber) ||!empty($customer) ||!empty($pricebook) ||!empty($loadDate) ||!empty($loadAddress) ||!empty($unloadDate) ||!empty($unloadAddress) || !empty($signatare) || !empty($document) ||!empty($route) ||!empty($driver) ||!empty($transport) ||!empty($status) ||!empty($manager) ||!empty($type)){

  $query = "SELECT  vtiger_salesorder.salesorder_no, IF(cf_1376 > 0,cf_1376, REPLACE(ROUND(vtiger_salesorder.total,2),'.',',')) as totalprice, vtiger_salesorder.salesorderid,vtiger_salesorder.load_date_from ,vtiger_salesorder.unload_date_from, vtiger_salesorder.sostatus,vtiger_salesorder.shipment_code, vtiger_salesorder.accountid,
  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') AS createdtime, vtiger_soshipads.unload_company, vtiger_soshipads.ship_city ,vtiger_soshipads.ship_code,bill_country,ship_country ,vtiger_soshipads.ship_street, vtiger_sobillads.load_company, vtiger_sobillads.bill_city ,vtiger_sobillads.bill_code ,vtiger_sobillads.bill_street, vtiger_inventoryproductrel.margin , vtiger_accountscf.cf_1279, vtiger_accountscf.cf_1281, vtiger_salesordercf.cf_1376 as agreed_price,
  SUM(FORMAT(vtiger_inventoryproductrel.quantity,0)) as quantity,
  SUM(FORMAT(vtiger_inventoryproductrel.revised_quantity,0)) as revised_quantity,
    SUM(vtiger_inventoryproductrel.ordered_weight) as ordered_weight, 
    SUM(vtiger_inventoryproductrel.revised_weight) as revised_weight, 
    ROUND(SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity),2)  as ordered_volume,
    ROUND(SUM(vtiger_inventoryproductrel.m3),2)  as revised_volume,
    CASE WHEN inventory_type = '' THEN (SELECT GROUP_CONCAT(packages_tks_tare,' ',packages_tks_kg,' ',packages_tks_length,'x',packages_tks_width,'x',packages_tks_height SEPARATOR '#:#') FROM vtiger_packages INNER JOIN vtiger_crmentity on crmid=packagesid WHERE vtiger_packages.cf_20023=vtiger_salesorder.salesorderid AND deleted = 0 )  END AS order_cargos,
    ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(revised_quantity) ELSE SUM(quantity) END,2) AS taxable_quantity,
    ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(revised_weight) ELSE SUM(ordered_weight) END,2) AS taxable_weight,
    ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(m3) ELSE SUM((ordered_length * ordered_width * ordered_height) * quantity) END,2)  AS taxable_volume,
    
    CASE WHEN SUM(vtiger_inventoryproductrel.ordered_weight) < SUM(vtiger_inventoryproductrel.revised_weight) AND cf_1376 > 0
      THEN SUM(vtiger_inventoryproductrel.revised_weight) > 
        (SUM(vtiger_inventoryproductrel.ordered_weight) + (SUM(vtiger_inventoryproductrel.ordered_weight) * 0.10))
        WHEN SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity) < SUM(vtiger_inventoryproductrel.m3) AND cf_1376 > 0
        THEN SUM(vtiger_inventoryproductrel.m3) > 
        (SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity) + 
        (SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity) * 0.10))
      ELSE 0
    END AS warning, 


    CASE WHEN cargo_measure IN (1,7) OR cargo_measure IN ('pll','nestd.') THEN ROUND(SUM((ordered_length * ordered_width) * quantity) /0.96 ,2) ELSE 0 END AS ordered_pll,
    CASE 
        WHEN revised_measure IN (1,7) 
            OR revised_measure IN ('pll','nestd.') 
            THEN (
                SELECT SUM(pll_rel.pll)
                FROM vtiger_inventoryproductrel as pll_rel
                WHERE pll_rel.id = vtiger_salesorder.salesorderid
            )
        ELSE 0 
    END AS revised_pll,

    CASE 
        WHEN app_taxable_dimensions.type = 'revised' THEN 
            CASE 
                WHEN revised_measure IN (1,7) 
                    OR revised_measure IN ('pll','nestd.') 
                    THEN (
                        SELECT SUM(pll_rel.pll)
                        FROM vtiger_inventoryproductrel as pll_rel
                        WHERE pll_rel.id = vtiger_salesorder.salesorderid
                    )
                ELSE 0 
            END
        ELSE 
            CASE WHEN cargo_measure IN (1,7) OR cargo_measure IN ('pll','nestd.') THEN ROUND(SUM((ordered_length * ordered_width) * quantity) /0.96 ,2) ELSE 0 
    END end AS taxable_pll, 
    CASE WHEN app_taxable_dimensions.type = 'fail' OR app_taxable_dimensions.type IS NULL THEN 0 ELSE 1 END AS show_taxable,
    CASE WHEN vtiger_salesordercf.cf_855 = 'Transporto užsakymas' THEN 'T' ELSE 'P' END AS type, 
    IF(vtiger_salesorder_updated.status IS NOT NULL, IF(vtiger_salesorder_updated.status = 0, 0,1), 1) as update_status,
  SUM(vtiger_inventoryproductrel.cargo_wgt) as cargo_wgt, SUM(vtiger_inventoryproductrel.cargo_length) as cargo_length,SUM( vtiger_inventoryproductrel.cargo_width) as cargo_width, 
  SUM(vtiger_inventoryproductrel.cargo_height) as cargo_height, vtiger_account.accountname,vtiger_users.user_name,vtiger_users.first_name,vtiger_users.last_name, (vtiger_inventoryproductrel.cargo_length * vtiger_inventoryproductrel.cargo_height * vtiger_inventoryproductrel.cargo_width) as cargo_volume,
  SUM((vtiger_inventoryproductrel.cargo_length * vtiger_inventoryproductrel.cargo_width * vtiger_inventoryproductrel.cargo_height) * vtiger_inventoryproductrel.quantity ) AS volume,
  vtiger_salesordercf.cf_1297 as pricebook,cf_1558 AS route, vtiger_salesordercf.cf_930 as termo, cf_1562 AS driver, cf_1560 AS transport, SUM(vtiger_inventoryproductrel.meters) AS meters, cf_2718 AS pricebook_details,
    (SELECT CONCAT(`city`,'/ ',IF(`city` != `zone_customer`, CONCAT(`zone_customer`,'/'),''),' ', `state`, '/ ', `zone_base`) FROM crm_post_codes WHERE post_code = vtiger_sobillads.bill_code LIMIT 1) AS `location_from`,
    (SELECT CONCAT(`city`,'/ ',IF(`city` != `zone_customer`, CONCAT(`zone_customer`,'/'),''),' ', `state`, '/ ', `zone_base`) FROM crm_post_codes WHERE post_code = vtiger_soshipads.ship_code LIMIT 1) AS `location_to`,
    (SELECT max(minifest) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as minifest,
    (SELECT max(cmr) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as cmr,
    (SELECT max(invoice) FROM vtiger_inventoryproductrel_flags WHERE flag_id = vtiger_salesorder.salesorderid LIMIT 1) as invoice ";
    if(!empty($signatare)){
      $query .= " ,(SELECT filename  FROM `vtiger_senotesrel` 
              LEFT JOIN `vtiger_notes` as pod_notes ON pod_notes.notesid=vtiger_senotesrel.notesid  AND pod_notes.folderid = 2                       
              WHERE  vtiger_senotesrel.crmid = vtiger_salesorder.salesorderid LIMIT 1) AS filename ";
    }
                      $query .= " FROM vtiger_salesorder                                      
                                  INNER JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid
                                  LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                                  LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                                  LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid                                    
                                  LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid                                    
                                  LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid AND productid = 14244
                                  LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid     
                                  LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid  
                                  LEFT JOIN `app_taxable_dimensions` ON app_taxable_dimensions.salesorderid=vtiger_salesorder.salesorderid   
                                  LEFT JOIN `vtiger_salesorder_updated` ON vtiger_salesorder_updated.salesorderid=vtiger_salesorder.salesorderid                                         
                                  WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456 = 0 AND cf_1614 = 0 AND  vtiger_inventoryproductrel.productid != 36641 AND (vtiger_salesorder.shipment_code IS NOT NULL AND vtiger_salesorder.shipment_code != '') ";

                                  $query .= getWhere($date_from_to, $filterBy, $orderDate, $orderNumber, $customer, $pricebook, $type, $loadDate, $loadAddress, $unloadDate, $unloadAddress,  $signatare, $document, $route, $driver, $transport, $status, $manager);
                                             

                                  $query .= " ORDER BY vtiger_salesorder.external_order_id ASC ";  
                                  $query .= " LIMIT 500";                                  
    

      $conn->query("SET SESSION group_concat_max_len = 1000000");
      $order_dimensions = $conn->query($query);
      if(!$order_dimensions){
        echo json_encode('empty');
      }

      $required_cargo_measures = [];
      $result = array();
      while($row = $order_dimensions->fetch_assoc()) {
        $documents = mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(DISTINCT pod_notes.filename SEPARATOR '|##|' ) AS pod_url,
                                                              CASE WHEN pod_notes.notecontent 
                                                                THEN GROUP_CONCAT(DISTINCT pod_notes.notecontent SEPARATOR '|##|' ) 
                                                                ELSE GROUP_CONCAT(DISTINCT pod_notes.title SEPARATOR '|##|' ) 
                                                              END  AS pod_title,
                                                              GROUP_CONCAT(DISTINCT pod_notescf.cf_1852 SEPARATOR '|##|') as pod_date,   
                                                              GROUP_CONCAT(DISTINCT doc_notes.filename SEPARATOR '|##|' ) AS doc_url,                     
                                                              CASE WHEN doc_notes.notecontent 
                                                                THEN GROUP_CONCAT(DISTINCT doc_notes.notecontent SEPARATOR '|##|' ) 
                                                                ELSE GROUP_CONCAT(DISTINCT doc_notes.title SEPARATOR '|##|' ) 
                                                              END  AS doc_title,
                                                              GROUP_CONCAT(DISTINCT doc_notescf.cf_1852 SEPARATOR '|##|') as doc_date
                                          FROM `vtiger_senotesrel` 
                                          LEFT JOIN `vtiger_notes` as pod_notes ON pod_notes.notesid=vtiger_senotesrel.notesid  AND pod_notes.folderid = 2      
                                          LEFT JOIN `vtiger_notes` as doc_notes ON doc_notes.notesid=vtiger_senotesrel.notesid  AND doc_notes.folderid = 3 
                                          LEFT JOIN vtiger_notescf as pod_notescf ON pod_notescf.notesid=pod_notes.notesid  AND pod_notes.folderid = 2
                                          LEFT JOIN vtiger_notescf as doc_notescf ON doc_notescf.notesid=doc_notes.notesid  AND doc_notes.folderid = 3
                                          WHERE  vtiger_senotesrel.crmid = '".$row['salesorderid']."'"));     

        $altered_row = $row;
        foreach(['ordered_weight', 'quantity', 'ordered_volume', 'ordered_pll', 'revised_weight', 'revised_quantity', 'revised_volume', 'revised_pll', 
            'taxable_weight', 'taxable_quantity', 'taxable_volume', 'taxable_pll', 'volume'] as $item){
            $altered_row[$item] = (float)round($row[$item], 2);
        }

        $result['orders'][] = $altered_row;     
        
 
        $result['check_transit'][$row['salesorderid']] = (strtoupper($row['bill_country']) == 'LTU' && strtoupper($row['ship_country']) == 'LTU' ? 1 : 0);

        $required_revised_measures[] = $row['salesorderid'];

        $result['doc_url'][$row['salesorderid']] = $documents['doc_url'];
        $result['doc_title'][$row['salesorderid']] = $documents['doc_title'];
        $result['doc_date'][$row['salesorderid']] = $documents['doc_date'];

        $result['pod_url'][$row['salesorderid']] = $documents['pod_url'];
        $result['pod_title'][$row['salesorderid']] = $documents['pod_title'];
        $result['pod_date'][$row['salesorderid']] = $documents['pod_date'];
      }   


      $result['filtered_count'] = countFiltered($conn, $date_from_to, $filterBy, $orderDate, $orderNumber, $customer, $pricebook, $type, $loadDate, $loadAddress, $unloadDate, $unloadAddress,  $signatare, $document, $route, $driver, $transport, $status, $manager); 

      if(!empty($result)){
          $result['revised_measures'] = \Helpers\CargosHelper::getAllRevisedMeasures($conn, $required_revised_measures);
      }


      if(!empty($result)){
        echo json_encode($result);
      }else{
        echo json_encode('no_results');
      }
  
  }else{
    echo json_encode('empty');
  }

}else{
  http_response_code(404);
}

function countFiltered($conn, $date_from_to, $filterBy, $orderDate, $orderNumber, $customer, $pricebook, $type, $loadDate, $loadAddress, $unloadDate, $unloadAddress,  $signatare, $document, $route, $driver, $transport, $status, $manager){

  $query = "SELECT vtiger_salesorder.salesorderid 
            FROM vtiger_salesorder         
            LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid                              
            LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid   
            LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid   
            LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
            LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid                                                        
            INNER JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid    
            LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid                           
            WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456 = 0 AND cf_1614 = 0 AND (vtiger_salesorder.shipment_code IS NOT NULL AND vtiger_salesorder.shipment_code != '') ";
  $query .= getWhere($date_from_to, $filterBy, $orderDate, $orderNumber, $customer, $pricebook, $type, $loadDate, $loadAddress, $unloadDate, $unloadAddress,  $signatare, $document, $route, $driver, $transport, $status, $manager);
                                    
  return mysqli_num_rows($conn->query($query));     
}

function getWhere($date_from_to, $filterBy, $orderDate, $orderNumber, $customer, $pricebook, $type, $loadDate, $loadAddress, $unloadDate, $unloadAddress,  $signatare, $document, $route, $driver, $transport, $status, $manager){
  
  $query = '';
  if(!empty($status)){
    $query .= " AND  vtiger_salesorder.sostatus LIKE '$status%' ";
  }     
                            
  if(!empty($date_from_to[0]) && !empty($date_from_to[1])){
    if($filterBy == 'order_date'){                                                                                       
      $query .= " AND  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$date_from_to[0]' AND '$date_from_to[1]' ";
    }elseif($filterBy == 'load_date'){
      $query .= " AND  vtiger_salesorder.load_date_from BETWEEN '$date_from_to[0]' AND '$date_from_to[1]' ";
    }elseif($filterBy == 'unload_date'){
      $query .= " AND  vtiger_salesorder.unload_date_from BETWEEN '$date_from_to[0]' AND '$date_from_to[1]' ";
    }
  }
  if(!empty($orderDate)){
    $query .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') =  '$orderDate' ";
  }                                         
  if(!empty($orderNumber)){
    $query .= " AND vtiger_salesorder.shipment_code LIKE '%$orderNumber%' ";
  }
  if(!empty($customer)){
    $query .= " AND vtiger_account.accountname LIKE '$customer%' ";
  }
  if(!empty($pricebook)){
    $query .= " AND vtiger_salesordercf.cf_1297 LIKE '$pricebook%' ";
  }
  if(!empty($type)){
    if($type == 'T') $type = 'Transporto užsakymas'; elseif($type == 'P') $type = 'Perkraustymo užsakymas';
    $query .= " AND vtiger_salesordercf.cf_855 LIKE '$type%' ";
  }
  if(!empty($loadDate)){
    $query .= " AND vtiger_salesorder.load_date_from = '$loadDate' ";
  }
  if(!empty($loadAddress)){
    $query .= " AND  CONCAT(vtiger_sobillads.load_company,' ', vtiger_sobillads.bill_street,' ', vtiger_sobillads.bill_city,' ',vtiger_sobillads.bill_code) LIKE '%$loadAddress%' ";
  }
  if(!empty($unloadDate)){
    $query .= " AND  vtiger_salesorder.unload_date_from = '$unloadDate' ";
  }
  if(!empty($unloadAddress)){  
    $query .= " AND CONCAT(vtiger_soshipads.unload_company,' ', vtiger_soshipads.ship_street,' ', vtiger_soshipads.ship_city,' ',vtiger_soshipads.ship_code) LIKE '%$unloadAddress%' ";
  }                                                             
  if(!empty($route)){  
    $query .= " AND vtiger_salesordercf.cf_1558 LIKE '$route%' ";
  }
  if(!empty($driver)){  
    $query .= " AND vtiger_salesordercf.cf_1560 LIKE '$driver%' ";
  }
  if(!empty($transport)){  
    $query .= " AND vtiger_salesordercf.cf_1562 LIKE '$transport%' ";
  }
  if(!empty($manager)){
    $query .= " AND  vtiger_users.user_name LIKE '$manager%' OR vtiger_users.first_name LIKE'$manager%' OR vtiger_users.last_name  LIKE '$manager%' OR CONCAT(vtiger_users.first_name,vtiger_users.last_name) LIKE '%$manager%'";
  }   

  $query .= " GROUP BY vtiger_salesorder.salesorderid"; 

  if(!empty($document) || !empty($signatare)){
    $query .= " HAVING 1+1 ";
  }  
  if($document == 'true'){  
    $query .= "  AND (minifest IS NOT NULL AND minifest != 0) OR (cmr IS NOT NULL AND cmr != 0) OR (invoice IS NOT NULL AND invoice != 0) ";
  }else if($document == 'false'){
    $query .= " AND (minifest IS NULL OR minifest = 0) OR (cmr IS  NULL OR cmr = 0) OR (invoice IS NULL OR invoice = 0) ";
  } 
  if($signatare == 'true'){  
    $query .= " AND filename IS NOT NULL ";
  }else if($signatare == 'false'){
    $query .= " AND filename IS NULL ";
  }  
 
  return $query;
}

