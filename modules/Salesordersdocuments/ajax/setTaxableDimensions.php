<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
if($_POST){

    include $_SERVER['DOCUMENT_ROOT'].'/v1/external_data/ws.config.php';
    include $_SERVER['DOCUMENT_ROOT'].'/v1/external_data/utils.php';    
    global $config;

    require_once $_SERVER['DOCUMENT_ROOT'].'/v1/external_data/getPriceFromPriceBook.php';


    try {
        require($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

        $salesorderid = $_POST['salesorderid'];       
        $type = $_POST['type'];    
        $userId = $_POST['userId']; 
        
        $type_LT = ($type == 'revised' ? 'Faktiniai' : 'Užsakyti');

        $check_exist = $dbh->prepare('SELECT salesorderid FROM app_taxable_dimensions WHERE salesorderid = ?');
        $insert = $dbh->prepare('INSERT INTO app_taxable_dimensions (salesorderid,type) VALUES (?,?)');        
        $update = $dbh->prepare('UPDATE app_taxable_dimensions SET type = ? WHERE salesorderid = ?');       
        $update_salesordercf = $dbh->prepare('UPDATE vtiger_salesordercf SET cf_2671 = ? WHERE salesorderid = ?');       

        $check_exist->setFetchMode(PDO::FETCH_ASSOC);
        $check_exist->execute(array($salesorderid));

        
        if(!$check_exist->rowCount()){
            $insert->execute(array($salesorderid,$type));   
            $update_salesordercf->execute(array($type_LT,$salesorderid));     
        }else{
            $update->execute(array($type,$salesorderid));
            $update_salesordercf->execute(array($type_LT,$salesorderid));   
        }   
        
          
        $date = date("Y-m-d H:i:s"); 

        $id = last_modtracker_record($dbh);
        insert_modtracker_basic($dbh,$id,$salesorderid,'SalesOrder',$userId,$date,0);
        insert_modtracker_detail($dbh, $id, 'cf_2671', '', $type_LT);
     

        echo json_encode(array('status' => 'success'));


    } catch (PDOException $e) {
        $dbh->rollBack();    
        echo json_encode(array('status' => 'fail'));
        // echo "Error!";
        // echo $e->getMessage();
    }

}else{
    http_response_code(404);
}             


