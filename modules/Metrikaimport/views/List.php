<?php

include "modules/Metrikaimport/models/metrikaOrders.php";

use modules\Metrikaimport\models\metrikaOrders as metrikaOrders;

class Metrikaimport_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->metrikaOrders = new metrikaOrders;  
       }

	public function process(Vtiger_Request $request) {               
                $viewer = $this->getViewer($request);         
                $viewer->view('List.tpl', $request->getModule()); 
       }
       

}
