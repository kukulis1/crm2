<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
require_once(__DIR__.'/../../../Helpers/Helper.php');

class Inventory_Detail_View extends Vtiger_Detail_View {
	function preProcess(Vtiger_Request $request,$display = true) {
		$viewer = $this->getViewer($request);
		$viewer->assign('NO_SUMMARY', true);
		parent::preProcess($request);
	}

	/**
	 * Function returns Inventory details
	 * @param Vtiger_Request $request
	 */
	function showModuleDetailView(Vtiger_Request $request) {
		$this->showLineItemDetails($request);
		return parent::showModuleDetailView($request);
	}

	/**
	 * Function returns Inventory details
	 * @param Vtiger_Request $request
	 * @return type
	 */
	function showDetailViewByMode(Vtiger_Request $request) {
		$requestMode = $request->get('requestMode');
		if($requestMode == 'full') {
			return $this->showModuleDetailView($request);
		}
		echo parent::showDetailViewByMode( $request);
	}

	function showModuleBasicView($request) {
		$requestMode = $request->get('requestMode');
		if($requestMode == 'full') {
			return $this->showModuleDetailView($request);
		}
		echo parent::showModuleBasicView($request);
	}

	function showModuleSummaryView($request) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();
		// itoma
		$db = PearDatabase::getInstance();

		if (!$this->record) {
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();
		$recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_SUMMARY);

		$moduleModel = $recordModel->getModule();
		$viewer = $this->getViewer($request);
		$viewer->assign('RECORD', $recordModel);
		$viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());

			// itoma
		// if ($moduleName == 'SalesOrder'){
		// 	$salesorder_check_invoice = $db->pquery("SELECT vtiger_invoice_salesorders_list.salesorderid FROM vtiger_salesorder 
		// 																					LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid 
		// 																					WHERE vtiger_invoice_salesorders_list.salesorderid = ?", array($recordId));																			
		// 	$response = $db->fetch_array($salesorder_check_invoice);
		// 	$viewer->assign('CHECK_SALESORDER_INVOICE', $response);
		// }else{
		// 	$response = '';

		// }

		$viewer->assign('CHECK_SALESORDER_INVOICE', $response);
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
		$viewer->assign('SUMMARY_RECORD_STRUCTURE', $recordStrucure->getStructure());
		$viewer->assign('RELATED_ACTIVITIES', $this->getActivities($request));

		return $viewer->view('ModuleSummaryView.tpl', $moduleName, true);
	}

	/**
	 * Function returns Inventory Line Items
	 * @param Vtiger_Request $request
	 */
	function showLineItemDetails(Vtiger_Request $request) {

		$record = $request->get('record');
		$moduleName = $request->getModule();
		$independent = $request->get('independent');
		$db = PearDatabase::getInstance();
		$recordModel = Inventory_Record_Model::getInstanceById($record);
		$relatedProducts = $recordModel->getProducts();
		//##Final details convertion started
		$finalDetails = $relatedProducts[1]['final_details'];

		//Final tax details convertion started
		$taxtype = $finalDetails['taxtype'];
		if ($taxtype == 'group') {
			$taxDetails = $finalDetails['taxes'];
			$taxCount = count($taxDetails);
			foreach ($taxDetails as $key => $taxInfo) {
				$taxDetails[$key]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($taxInfo['amount'], null, true);
			}
			$finalDetails['taxes'] = $taxDetails;
		}
		//Final tax details convertion ended

		//Deducted tax details convertion started
		$deductTaxes = $finalDetails['deductTaxes'];
		foreach ($deductTaxes as $taxId => $taxInfo) {
			$deductTaxes[$taxId]['taxAmount'] = Vtiger_Currency_UIType::transformDisplayValue($deductTaxes[$taxId]['taxAmount'], null, true);
		}
		$finalDetails['deductTaxes'] = $deductTaxes;
		//Deducted tax details convertion ended

		$currencyFieldsList = array('adjustment', 'grandTotal', 'hdnSubTotal', 'preTaxTotal', 'tax_totalamount',
									'shtax_totalamount', 'discountTotal_final', 'discount_amount_final', 'shipping_handling_charge', 'totalAfterDiscount', 'deductTaxesTotalAmount');
		foreach ($currencyFieldsList as $fieldName) {
			$finalDetails[$fieldName] = Vtiger_Currency_UIType::transformDisplayValue($finalDetails[$fieldName], null, true);
		}

		$relatedProducts[1]['final_details'] = $finalDetails;
		//##Final details convertion ended

		//##Product details convertion started
		$productsCount = count($relatedProducts);
		for ($i=1; $i<=$productsCount; $i++) {
			$product = $relatedProducts[$i];


			//Product tax details convertion started
			if ($taxtype == 'individual') {
				$taxDetails = $product['taxes'];
				$taxCount = count($taxDetails);
				for($j=0; $j<$taxCount; $j++) {
					$taxDetails[$j]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($taxDetails[$j]['amount'], null, true);
				}
				$product['taxes'] = $taxDetails;
			}
			//Product tax details convertion ended

			$currencyFieldsList = array('taxTotal', 'netPrice', 'listPrice', 'unitPrice', 'productTotal','purchaseCost','margin',
										'discountTotal', 'discount_amount', 'totalAfterDiscount');
			foreach ($currencyFieldsList as $fieldName) {
				$product[$fieldName.$i] = Vtiger_Currency_UIType::transformDisplayValue($product[$fieldName.$i], null, true);
			}
		
			$relatedProducts[$i] = $product;
		}
		//##Product details convertion ended

		$selectedChargesAndItsTaxes = $relatedProducts[1]['final_details']['chargesAndItsTaxes'];
		if (!$selectedChargesAndItsTaxes) {
			$selectedChargesAndItsTaxes = array();
		}

		$shippingTaxes = array();
		$allShippingTaxes = getAllTaxes('all', 'sh');
		foreach ($allShippingTaxes as $shTaxInfo) {
			$shippingTaxes[$shTaxInfo['taxid']] = $shTaxInfo;
		}

		$selectedTaxesList = array();
		foreach ($selectedChargesAndItsTaxes as $chargeId => $chargeInfo) {
			if ($chargeInfo['taxes']) {
				foreach ($chargeInfo['taxes'] as $taxId => $taxPercent) {
					$taxInfo = array();
					$amount = $calculatedOn = $chargeInfo['value'];

					if ($shippingTaxes[$taxId]['method'] === 'Compound') {
						$compoundTaxes = Zend_Json::decode(html_entity_decode($shippingTaxes[$taxId]['compoundon']));
						if (is_array($compoundTaxes)) {
							foreach ($compoundTaxes as $comTaxId) {
								$calculatedOn += ((float)$amount * (float)$chargeInfo['taxes'][$comTaxId]) / 100;
							}
							$taxInfo['method']		= 'Compound';
							$taxInfo['compoundon']	= $compoundTaxes;
						}
					}
					$calculatedAmount = ((float)$calculatedOn * (float)$taxPercent) / 100;

					$taxInfo['name']	= $shippingTaxes[$taxId]['taxlabel'];
					$taxInfo['percent']	= $taxPercent;
					$taxInfo['amount']	= Vtiger_Currency_UIType::transformDisplayValue($calculatedAmount, null, true);

					$selectedTaxesList[$chargeId][$taxId] = $taxInfo;
				}
			}
		}

		$selectedChargesList = Inventory_Charges_Model::getChargeModelsList(array_keys($selectedChargesAndItsTaxes));
		foreach ($selectedChargesList as $chargeId => $chargeModel) {
			$chargeInfo['name']		= $chargeModel->getName();
			$chargeInfo['amount']	= Vtiger_Currency_UIType::transformDisplayValue($selectedChargesAndItsTaxes[$chargeId]['value'], null, true);
			$chargeInfo['percent']	= $selectedChargesAndItsTaxes[$chargeId]['percent'];
			$chargeInfo['taxes']	= $selectedTaxesList[$chargeId];
			$chargeInfo['deleted']	= $chargeModel->get('deleted');

			$selectedChargesList[$chargeId] = $chargeInfo;
		}

		$viewer = $this->getViewer($request);
		// ITOMA
		if ($moduleName == 'SalesOrder' OR $moduleName == 'Invoice') {
			$orderBy = ($moduleName == 'SalesOrder' ? 'vtiger_inventoryproductrel.productid' : 'vtiger_inventoryproductrel.sequence_no');
			$db->pquery("SET SESSION group_concat_max_len = 1000000",array());
				$cargo_query = "SELECT vtiger_inventoryproductrel.external_load_id, `name`, app_measures.code as cargo_measure, vtiger_costcenter.costcenter_tks_cost as costcenter, vtiger_inventoryproductrel.employee,vtiger_inventoryproductrel.object ,vtiger_inventoryproductrel.productid, ROUND(vtiger_inventoryproductrel.margin,2) AS margin";

				$cargo_query .= " FROM vtiger_inventoryproductrel 
				LEFT JOIN app_services_type ON app_services_type.id=vtiger_inventoryproductrel.service
				LEFT JOIN vtiger_costcenter ON vtiger_costcenter.costcenterid=vtiger_inventoryproductrel.costcenter
				LEFT JOIN app_measures ON CASE WHEN vtiger_inventoryproductrel.cargo_measure REGEXP '^[0-9]+$' THEN app_measures.id=vtiger_inventoryproductrel.cargo_measure  ELSE app_measures.code=vtiger_inventoryproductrel.cargo_measure	END";
				if($moduleName == 'SalesOrder'){
					$cargo_query .= "	LEFT JOIN vtiger_packages p ON p.load_id=vtiger_inventoryproductrel.external_load_id";
				}
				$cargo_query .= "	WHERE vtiger_inventoryproductrel.id = ? AND vtiger_inventoryproductrel.inventory_type != 'FuelSurcharge' AND vtiger_inventoryproductrel.inventory_type != 'stevedoring' ";
				 if($moduleName == 'SalesOrder'){
					$cargo_query .= " GROUP BY vtiger_inventoryproductrel.external_load_id,vtiger_inventoryproductrel.productid ";
				}
				$cargo_query .= " ORDER BY $orderBy";



			$cargo = $db->pquery($cargo_query, array($record));	
			$docs = $db->pquery("SELECT f.external_load_id, minifest,cmr,invoice 
														FROM vtiger_inventoryproductrel_flags f
														JOIN vtiger_inventoryproductrel i ON i.external_load_id=f.external_load_id
														WHERE flag_id = ?", array($record));

			$flags= array();
			foreach($docs AS $row){
				$flags[$row['external_load_id']] = $row;
			}	
	

      $external_load_ids = [];
			$num_rows=$db->num_rows($cargo);
			for($i = 1; $i <= $num_rows; $i++){			
				$external_load_ids[] = $db->query_result($cargo, $i-1, 'external_load_id');
				$relatedProducts[$i]['measure'.$i] = $db->query_result($cargo, $i-1, "cargo_measure");
				$relatedProducts[$i]['service'.$i] = $db->query_result($cargo, $i-1, "name");
				$relatedProducts[$i]['employee'.$i] = $db->query_result($cargo, $i-1, "employee");
				$relatedProducts[$i]['object'.$i] = $db->query_result($cargo, $i-1, "object");
				$relatedProducts[$i]['margin'.$i] = $db->query_result($cargo, $i-1, "margin");
				$relatedProducts[$i]['flags'.$i] = $flags;
				$viewer->assign('INDEPENDENT', $db->query_result($cargo, $i-1, "productid"));
			}
            
            if($moduleName == 'SalesOrder'){
                $packages = \Helpers\CargosHelper::getAllRevisedMeasuresForLoads($db, array_filter($external_load_ids), $record);
                $viewer->assign('PACKAGES', $packages);
            }
    
			$viewer->assign('TARE', $this->getTare()); 

					
		}

		if ($moduleName == 'SalesOrder'){
	
			$docs = $db->pquery("SELECT f.external_load_id, minifest,cmr,invoice 
				FROM vtiger_inventoryproductrel_flags f
				JOIN vtiger_inventoryproductrel i ON i.external_load_id=f.external_load_id
				WHERE flag_id = ? AND f.external_load_id > 0", array($record));

			$num_rows=$db->num_rows($docs);
			for($i = 1; $i <= $num_rows; $i++){	
				$external_load_id = 	$db->query_result($docs, $i-1, "external_load_id");
				$relatedProducts[$i]['minifest'.$external_load_id] = $db->query_result($docs, $i-1, "minifest");
				$relatedProducts[$i]['cmr'.$external_load_id] = $db->query_result($docs, $i-1, "cmr");
				$relatedProducts[$i]['invoice'.$external_load_id] = $db->query_result($docs, $i-1, "invoice");
			}
		
		}

		if ($moduleName == 'SalesOrder'){
			$salesorder_check_invoice = $db->pquery("SELECT vtiger_invoice_salesorders_list.salesorderid FROM vtiger_salesorder 
																							LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid 
																							WHERE vtiger_invoice_salesorders_list.salesorderid = ?", array($record));
			$response = $db->fetch_array($salesorder_check_invoice);


			$external_order_id = $db->pquery("SELECT external_order_id, preinvoice, cf_1936 AS withoutinvoice 
																	      FROM vtiger_salesorder 
																			  JOIN vtiger_salesordercf ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
																				WHERE vtiger_salesorder.salesorderid = ?", array($record));		
			$external_order =$db->query_result($external_order_id, 0, "external_order_id");
			$withoutinvoice =$db->query_result($external_order_id, 0, "withoutinvoice");
			$preinvoice = $db->query_result($external_order_id, 0, "preinvoice");
	
		}else{
			$response = '';
			$preinvoice = '';
		}



		if ($moduleName == 'Invoice') {
			$debt_query = $db->pquery("SELECT ROUND(SUM(debts_tks_suma),2) AS paid_sum,
				CASE 
					WHEN vtiger_debts.debts_tks_suma THEN ROUND(( IF(vtiger_invoice.region_id = 5, subtotal,total) - SUM(vtiger_debts.debts_tks_suma)),2)
					ELSE ROUND(IF(vtiger_invoice.region_id = 5,subtotal,total),2)
				END AS debt				
			FROM vtiger_invoice 
			LEFT JOIN vtiger_account on vtiger_invoice.accountid=vtiger_account.accountid 
			LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
			LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
			LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_debts.debtsid
			WHERE vtiger_invoice.invoiceid = ? AND relmodule = 'Debts' AND de.setype = 'Debts'
			ORDER BY invoiceid",array($record));
			

			$invoice_query = $db->pquery("SELECT FORMAT(total, 2) AS total_with_vat FROM vtiger_invoice WHERE invoiceid = ?",array($record));

			$viewer->assign('PAID_SUM', $db->query_result($debt_query, 0, "paid_sum"));
			$viewer->assign('DEBT', $db->query_result($debt_query, 0, "debt"));
			$viewer->assign('TOTAL_WITH_VAT', $db->query_result($invoice_query, 0, "total_with_vat"));
	
			
		}

		if($moduleName == 'Invoice' || $moduleName == 'PurchaseOrder'){
			$viewer->assign('anchor_sequence', $request->get('anchor_sequence'));
		}



		$viewer->assign('CHECK_SALESORDER_INVOICE', $response);
		$viewer->assign('CHECK_SALESORDER_PREINVOICE', $preinvoice);
		$viewer->assign('external_order_id', $external_order);
		$viewer->assign('withoutinvoice', $withoutinvoice);
		$viewer->assign('salesorderid', $record);
		$viewer->assign('RELATED_PRODUCTS', $relatedProducts);
		$viewer->assign('SELECTED_CHARGES_AND_ITS_TAXES', $selectedChargesList);
		$viewer->assign('RECORD', $recordModel);
		$viewer->assign('MODULE_NAME',$moduleName);

		
		// $viewer->view('LineItemsDetail.tpl', 'Inventory');
}

	// ITOMA
	function getTare(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/tare_types.php");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch); 
		$data = json_decode($result,true);
		return $data['measures'];
	}

	public function getActivities(Vtiger_Request $request) {
		$moduleName = 'Calendar';
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		if ($currentUserPriviligesModel->hasModulePermission($moduleModel->getId())) {
			$moduleName = $request->getModule();
			$recordId = $request->get('record');

			$pageNumber = $request->get('page');
			if (empty($pageNumber)) {
				$pageNumber = 1;
			}
			$pagingModel = new Vtiger_Paging_Model();
			$pagingModel->set('page', $pageNumber);
			$pagingModel->set('limit', 10);

			if (!$this->record) {
				$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
			}
			$recordModel = $this->record->getRecord();
			$moduleModel = $recordModel->getModule();

			$relatedActivities = $moduleModel->getCalendarActivities('', $pagingModel, 'all', $recordId);

			$viewer = $this->getViewer($request);
			$viewer->assign('RECORD', $recordModel);
			$viewer->assign('MODULE_NAME', $moduleName);
			$viewer->assign('PAGING_MODEL', $pagingModel);
			$viewer->assign('PAGE_NUMBER', $pageNumber);
			$viewer->assign('ACTIVITIES', $relatedActivities);

			return $viewer->view('RelatedActivities.tpl', $moduleName, true);
		}
	}

	/**
	 * Function to get the list of Script models to be included
	 * @param Vtiger_Request $request
	 * @return <Array> - List of Vtiger_JsScript_Model instances
	 */
	function getOverlayHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getOverlayHeaderScripts($request);
		$moduleName = $request->getModule();
		$moduleDetailFile = 'modules.' . $moduleName . '.resources.Detail';
		unset($headerScriptInstances[$moduleDetailFile]);
		$jsFileNames = array(
			'modules.Inventory.resources.Detail',
		);
		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}

	function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);

		$moduleName = $request->getModule();

		//Added to remove the module specific js, as they depend on inventory files
		$modulePopUpFile = 'modules.'.$moduleName.'.resources.Popup';
		$moduleEditFile = 'modules.'.$moduleName.'.resources.Edit';
		$moduleDetailFile = 'modules.'.$moduleName.'.resources.Detail';
		unset($headerScriptInstances[$modulePopUpFile]);
		unset($headerScriptInstances[$moduleEditFile]);
		unset($headerScriptInstances[$moduleDetailFile]);

		$jsFileNames = array(
			'modules.Inventory.resources.Popup',
			'modules.Inventory.resources.Detail',
			'modules.Inventory.resources.Edit',
			"modules.$moduleName.resources.Detail",
		);
		$jsFileNames[] = $moduleEditFile;
		$jsFileNames[] = $modulePopUpFile;
		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}
}
