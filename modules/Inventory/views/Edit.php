<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Class Inventory_Edit_View extends Vtiger_Edit_View {

	public function process(Vtiger_Request $request) {
		$db = PearDatabase::getInstance();
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();	
		$record = $request->get('record');
		$newRecord = $request->get('newRecord');
		$sourceRecord = $request->get('sourceRecord');
		$sourceModule = $request->get('sourceModule');
		if(empty($sourceRecord) && empty($sourceModule)) {
			$sourceRecord = $request->get('returnrecord');
			$sourceModule = $request->get('returnmodule');
		}

		$viewer->assign('MODE', '');
		$viewer->assign('IS_DUPLICATE', false);

		if ($request->has('totalProductCount')) {
			if($record) {
				$recordModel = Vtiger_Record_Model::getInstanceById($record);
			} else {
				$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
			}
			$relatedProducts = $recordModel->convertRequestToProducts($request);
	
			$taxes = $relatedProducts[1]['final_details']['taxes'];
		} else if(!empty($record)  && $request->get('isDuplicate') == true) {
			$recordModel = Inventory_Record_Model::getInstanceById($record, $moduleName);
			$currencyInfo = $recordModel->getCurrencyInfo();
			$taxes = $recordModel->getProductTaxes();
			$relatedProducts = $recordModel->getProducts();

			//While Duplicating record, If the related record is deleted then we are removing related record info in record model
			$mandatoryFieldModels = $recordModel->getModule()->getMandatoryFieldModels();
			foreach ($mandatoryFieldModels as $fieldModel) {
				if ($fieldModel->isReferenceField()) {
					$fieldName = $fieldModel->get('name');
					if (Vtiger_Util_Helper::checkRecordExistance($recordModel->get($fieldName))) {
						$recordModel->set($fieldName, '');
					}
				}
			}
			$viewer->assign('IS_DUPLICATE', true);
		} elseif (!empty($record)) {
			$recordModel = Inventory_Record_Model::getInstanceById($record, $moduleName);
			$currencyInfo = $recordModel->getCurrencyInfo();
			$taxes = $recordModel->getProductTaxes();
			$relatedProducts = $recordModel->getProducts();
			$viewer->assign('RECORD_ID', $record);
			$viewer->assign('MODE', 'edit');
		} elseif (($request->get('salesorder_id') || $request->get('quote_id') || $request->get('invoice_id')) && ($moduleName == 'PurchaseOrder')) {
			if ($request->get('salesorder_id')) {
				$referenceId = $request->get('salesorder_id');
			} elseif ($request->get('invoice_id')) {
				$referenceId = $request->get('invoice_id');
			} else{
				$referenceId = $request->get('quote_id');
			}

			$parentRecordModel = Inventory_Record_Model::getInstanceById($referenceId);
			$currencyInfo = $parentRecordModel->getCurrencyInfo();

			$relatedProducts = $parentRecordModel->getProductsForPurchaseOrder();
			$taxes = $parentRecordModel->getProductTaxes();

			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
			$recordModel->setRecordFieldValues($parentRecordModel);
		} elseif ($request->get('salesorder_id') || $request->get('quote_id')) {
			if ($request->get('salesorder_id')) {
				$referenceId = $request->get('salesorder_id');
			} else {
				$referenceId = $request->get('quote_id');
			}

			$parentRecordModel = Inventory_Record_Model::getInstanceById($referenceId);
			$currencyInfo = $parentRecordModel->getCurrencyInfo();
			$taxes = $parentRecordModel->getProductTaxes();
			$relatedProducts = $parentRecordModel->getProducts();
			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
			$recordModel->setRecordFieldValues($parentRecordModel);
		} else {
			$taxes = Inventory_Module_Model::getAllProductTaxes();
			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);

			//The creation of Inventory record from action and Related list of product/service detailview the product/service details will calculated by following code
			if ($request->get('product_id') || $sourceModule === 'Products' || $request->get('productid')) {
				if($sourceRecord) {
					$productRecordModel = Products_Record_Model::getInstanceById($sourceRecord);
				} else if($request->get('product_id')) {
					$productRecordModel = Products_Record_Model::getInstanceById($request->get('product_id'));
				} else if($request->get('productid')) {
					$productRecordModel = Products_Record_Model::getInstanceById($request->get('productid'));
				}
				$relatedProducts = $productRecordModel->getDetailsForInventoryModule($recordModel);

		
			} elseif ($request->get('service_id') || $sourceModule === 'Services') {
				if($sourceRecord) {
					$serviceRecordModel = Services_Record_Model::getInstanceById($sourceRecord);
				} else {
					$serviceRecordModel = Services_Record_Model::getInstanceById($request->get('service_id'));
				}
				$relatedProducts = $serviceRecordModel->getDetailsForInventoryModule($recordModel);
			} elseif ($sourceRecord && in_array($sourceModule, array('Accounts', 'Contacts', 'Potentials', 'Vendors', 'PurchaseOrder'))) {
				$parentRecordModel = Vtiger_Record_Model::getInstanceById($sourceRecord, $sourceModule);
				$recordModel->setParentRecordData($parentRecordModel);
				if ($sourceModule !== 'PurchaseOrder') {
					$relatedProducts = $recordModel->getParentRecordRelatedLineItems($parentRecordModel);
				}
			} elseif ($sourceRecord && in_array($sourceModule, array('HelpDesk', 'Leads'))) {
				$parentRecordModel = Vtiger_Record_Model::getInstanceById($sourceRecord, $sourceModule);
				$relatedProducts = $recordModel->getParentRecordRelatedLineItems($parentRecordModel);
			}
		}


		$deductTaxes = $relatedProducts[1]['final_details']['deductTaxes'];
		if (!$deductTaxes) {
			$deductTaxes = Inventory_TaxRecord_Model::getDeductTaxesList();
		}

		$taxType = $relatedProducts[1]['final_details']['taxtype'];
		$moduleModel = $recordModel->getModule();
		$fieldList = $moduleModel->getFields();
		$requestFieldList = array_intersect_key($request->getAllPurified(), $fieldList);

		//get the inventory terms and conditions
		$inventoryRecordModel = Inventory_Record_Model::getCleanInstance($moduleName);
		$termsAndConditions = $inventoryRecordModel->getInventoryTermsAndConditions();

		foreach($requestFieldList as $fieldName=>$fieldValue) {
			$fieldModel = $fieldList[$fieldName];
			if($fieldModel->isEditable()) {
				$recordModel->set($fieldName, $fieldModel->getDBInsertValue($fieldValue));
			}
		}
		$recordStructureInstance = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_EDIT);

		$viewer->assign('VIEW_MODE', "fullForm");

		$isRelationOperation = $request->get('relationOperation');

		//if it is relation edit
		$viewer->assign('IS_RELATION_OPERATION', $isRelationOperation);
		if($isRelationOperation) {
			$viewer->assign('SOURCE_MODULE', $sourceModule);
			$viewer->assign('SOURCE_RECORD', $sourceRecord);
		}
		if(!empty($record)  && $request->get('isDuplicate') == true) {
			$viewer->assign('IS_DUPLICATE',true);
		} else {
			$viewer->assign('IS_DUPLICATE',false);
		}
		$currencies = Inventory_Module_Model::getAllCurrencies();
		$picklistDependencyDatasource = Vtiger_DependencyPicklist::getPicklistDependencyDatasource($moduleName);

		$recordStructure = $recordStructureInstance->getStructure();

		$viewer->assign('PICKIST_DEPENDENCY_DATASOURCE',Vtiger_Functions::jsonEncode($picklistDependencyDatasource));
		$viewer->assign('RECORD',$recordModel);
		$viewer->assign('RECORD_STRUCTURE_MODEL', $recordStructureInstance);
		$viewer->assign('RECORD_STRUCTURE', $recordStructure);
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('CURRENTDATE', date('Y-n-j'));
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());

		$taxRegions = $recordModel->getRegionsList();
		$defaultRegionInfo = $taxRegions[0];
		unset($taxRegions[0]);

		// ITOMA

		if ($moduleName == 'SalesOrder') {
			$cargo = $db->pquery("SELECT cargo_measure,service FROM vtiger_inventoryproductrel 
			WHERE id = ? AND vtiger_inventoryproductrel.inventory_type != 'FuelSurcharge' AND  vtiger_inventoryproductrel.inventory_type != 'stevedoring' ", array($record));
			$services = $db->pquery("SELECT * FROM app_services_type");
			$num_rows=$db->num_rows($cargo);

			$docs = $db->pquery("SELECT f.external_load_id, minifest,cmr,invoice 
														FROM vtiger_inventoryproductrel_flags f
														JOIN vtiger_inventoryproductrel i ON i.external_load_id=f.external_load_id
														WHERE flag_id = ?", array($record));

			$flags= array();
			foreach($docs AS $row){
				$flags[$row['external_load_id']] = $row;
			}	
		
			for($i = 1; $i <= $num_rows; $i++)	{
				$relatedProducts[$i]['measure'] =  $db->query_result($cargo, $i-1, "cargo_measure");
				$relatedProducts[$i]['service'.$i] =  $db->query_result($cargo, $i-1, "service");
				$relatedProducts[$i]['flags'.$i] = $flags;
			}

			$loaders_query = $db->pquery("SELECT DISTINCT CONCAT(firstname,' ',lastname) AS loader_name 
																		FROM vtiger_hrm_employee AS e 
																		LEFT JOIN vtiger_hrm_employcf ON vtiger_hrm_employcf.employid=e.employid
																		LEFT JOIN vtiger_hrm_position ON id=position 
																		INNER JOIN vtiger_crmentity AS c ON c.crmid = e.employid
																		WHERE deleted = 0 AND cf_2714 = 1 AND cf_2370 = 'Dirba'
																		GROUP BY e.employid");

			$loaders_array= array();

			foreach($loaders_query AS $row){
				$loaders_array[$row['loader_name']] = $row['loader_name'];
			}

			
			$viewer->assign('TARE', $this->getTare());  
			$viewer->assign('services', $services);
			$viewer->assign('GET', $_GET);   
			$viewer->assign('LOADERS', $loaders_array);   
		}

		if($moduleName == 'PurchaseOrder' OR $moduleName == 'Invoice'){			
			$cost_center_result = $db->pquery("SELECT vtiger_costcenter.costcenterid AS costid,costcenter_tks_cost AS costname,cf_1386 AS cost_code 
																						FROM vtiger_costcenter 
																						LEFT JOIN vtiger_costcentercf ON vtiger_costcentercf.costcenterid=vtiger_costcenter.costcenterid
																						LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_costcenter.costcenterid
																						WHERE deleted = 0
																						");
			$viewer->assign('cost_center', $cost_center_result);  
		}

		
		if ($moduleName == 'Invoice') {
			$debt_query = $db->pquery("SELECT DISTINCT FORMAT(SUM(vtiger_debts.debts_tks_suma),2) AS paid_sum, FORMAT((total - SUM(vtiger_debts.debts_tks_suma)),2) AS debt
																				FROM vtiger_debts 														
																				INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_debts.debtsid 														
																				INNER JOIN vtiger_crmentityrel ON (vtiger_crmentityrel.relcrmid = vtiger_crmentity.crmid OR vtiger_crmentityrel.crmid = vtiger_crmentity.crmid) 
																				INNER JOIN vtiger_invoice ON (vtiger_crmentityrel.relcrmid = vtiger_invoice.invoiceid OR vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid) 
																				WHERE vtiger_crmentity.deleted = 0 AND (vtiger_crmentityrel.crmid = ? OR vtiger_crmentityrel.relcrmid = ?)",array($record,$record));


			$invoice_query = $db->pquery("SELECT FORMAT(total, 2) AS total_with_vat FROM vtiger_invoice WHERE invoiceid = ?",array($record));
			$measure_list = $db->pquery("SELECT value FROM app_other_settings WHERE title = 'LBL_CARGO_MEASURE'");

			$viewer->assign('PAID_SUM', $db->query_result($debt_query, 0, "paid_sum"));
			$viewer->assign('DEBT', $db->query_result($invoice_query, 0, "debt"));
			$viewer->assign('TOTAL_WITH_VAT', $db->query_result($invoice_query, 0, "total_with_vat"));
			$viewer->assign('INDEPENDENT', $_GET['newRecord']);
		}

		if($newRecord && $moduleName == 'Invoice'){
			$subquery3 = "SELECT value FROM app_other_settings WHERE title = 'LBL_CARGO_MEASURE'";
			$result = $db->pquery($subquery3);
			$measure_list= explode(",",$db->query_result($result,0,'value'));
			$viewer->assign('measure_list', $measure_list);
		}

		if($request->get('account_id')){				
			$record = $request->get('account_id');

			$orderDate = $request->get('orderDate');
			$from = $request->get('from');
			$to = $request->get('to');
			$filterBy = $request->get('filterBy');
			$orderNumber = $request->get('orderNumber');
			$customer = $request->get('customer');
			$pricebook = $request->get('pricebook');
			$loadDate = $request->get('loadDate');
			$loadAddress = $request->get('loadAddress');
			$unloadDate = $request->get('unloadDate');
			$unloadAddress = $request->get('unloadAddress');
			$status = $request->get('status');
			$manager = $request->get('manager');
			$order_ids = $request->get('order_ids');
			$pre = $request->get('pre');

			$relatedProducts = array();
			$db->pquery("SET SESSION group_concat_max_len = 1000000", array());
			 $this->checkRestInTerminal($record,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids, $pre);

			// Patikrinam kokia taisykle del kuro priemokos nustatyta pas klienta
			$get_user_surcharges_rules = $db->pquery("SELECT cf_1568 AS fuel_surcharge_rule, cf_2722 AS road_tax_fee_setting FROM vtiger_accountscf WHERE accountid = ?",array($record));
			$user_fuel_surcharge_rule = $db->query_result($get_user_surcharges_rules, 0, "fuel_surcharge_rule");	
			$user_road_tax_setting = $db->query_result($get_user_surcharges_rules, 0, "road_tax_fee_setting");	

			if($user_fuel_surcharge_rule == 'Taikoma užsakymui'){
				$this->checkFuelSurchargeForOrder($record,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);		
			}				
			
			$this->getRoadTaxForOrder($record,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);		
			
			
			if($user_road_tax_setting){
				$this->getRoadTaxForOrderByUser($record,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);	
			}
			
			$services = $db->pquery("SELECT * FROM app_services_type");

			$salesorder_string = $this->getInvoiceSelectQuery();			
			
			$salesorder_string .= $this->getInvoiceFromQuery($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids,$pre);
										
			$salesorder_string .= "	GROUP BY vtiger_salesorder.salesorderid ,vtiger_inventoryproductrel.productid	, vtiger_inventoryproductrel.service, vtiger_inventoryproductrel.inventory_type
			ORDER BY vtiger_salesorder.external_order_id ASC,productid ASC LIMIT 500";	
			// vtiger_inventoryproductrel.service pakeista productid	


			$salesorder = $db->pquery($salesorder_string, array($record));
			$num_rows=$db->num_rows($salesorder);	

			for($i = 1; $i <= $num_rows; $i++)	{
				$salesorderid = $db->query_result($salesorder, $i-1, "salesorderid");
				$storage = $db->query_result($salesorder, $i-1, "storage");
				$cargo_wgt=$db->query_result($salesorder,$i-1,'cargo_wgt');
				$cargo_length=$db->query_result($salesorder,$i-1,'cargo_length');
				$cargo_width=$db->query_result($salesorder,$i-1,'cargo_width');
				$cargo_height=$db->query_result($salesorder,$i-1,'cargo_height');
				$relatedProducts[$i]['salesorderid'.$i] = $salesorderid;
				$relatedProducts[$i]['storage'.$i] = $storage;
				$relatedProducts[$i]['salesorder_id'.$i] = $salesorderid;
				$relatedProducts[$i]['shipment_code'.$i] =  $db->query_result($salesorder, $i-1, "shipment_code");
				$relatedProducts[$i]['bill_ads'.$i] =  $db->query_result($salesorder, $i-1, "bill_ads");
				$relatedProducts[$i]['ship_ads'.$i] =  $db->query_result($salesorder, $i-1, "ship_ads");
				$relatedProducts[$i]['note'.$i] =  $db->query_result($salesorder, $i-1, "note");	
				$relatedProducts[$i]['cargo_wgt'.$i] = $cargo_wgt;		
				$cargo_list = $db->query_result($salesorder, $i-1, "cargo");
				$cargo_array = explode(",", $cargo_list);
				$cargo_values = array();
				foreach($cargo_array as $str){
					$str = explode(" ",$str);
					$cargo_values[$str[1]] += $str[0];
				}
				$hidden_measures = [];
				foreach ($cargo_values as $code => $sum) {
					$relatedProducts[$i]['cargo_measure_for_invoice'.$i] .= "$sum $code <br />";	
					$hidden_measures[$i] .= "$sum $code,";
				}

				$productid = $db->query_result($salesorder, $i-1, "productid");

				$listprice = $db->query_result($salesorder, $i-1, "margin");
				$margin = $db->query_result($salesorder, $i-1, "margin");

				if($productid == 36641){
					$listprice = $listprice * ($db->query_result($salesorder, $i-1, "quantity") ?? 1);
					$margin = $margin * ($db->query_result($salesorder, $i-1, "quantity") ?? 1);
				}

				$relatedProducts[$i]['cargo_measure_invoice'.$i] .= rtrim($hidden_measures[$i], ',');
				$relatedProducts[$i]['volume'.$i] =  $db->query_result($salesorder, $i-1, "volume");
				$relatedProducts[$i]['listPrice'.$i] =  (float)$listprice;
				$relatedProducts[$i]['netPrice'.$i] =  (float)$margin;
				$relatedProducts[$i]['productTotal'.$i] =  (float)$margin;
				$relatedProducts[$i]['hdnProductId'.$i] =  (int)$productid;	
				$relatedProducts[$i]['entityType'.$i] =  $db->query_result($salesorder, $i-1, "entitytype");
				$relatedProducts[$i]['service_id'.$i] =  $db->query_result($salesorder, $i-1, "service_id");
				$relatedProducts[$i]['service_names'.$i] =  $services;	
				$product_name =  ($cargo_wgt != 'z' && $cargo_length != 'z' && $cargo_width != 'z' && $cargo_height != 'z'?  $cargo_wgt." ".(float)$cargo_length . 'x' . (float)$cargo_width . 'x' . (float)$cargo_height : 'Nenurodyti matmenys');				
				$relatedProducts[$i]['productnameDimensions'.$i] =  $product_name;	
				$relatedProducts[$i]['invoice_type'.$i] =  $db->query_result($salesorder, $i-1, "invoice_type");
				$relatedProducts[$i]['fail_remark'.$i] = $db->query_result($salesorder, $i-1, "fail_remark");																									
			}		

		
			if($user_fuel_surcharge_rule == 'Taikoma bendra'){		
			 // Paskaiciuojama kuro priemoka	
			 $fuel_surcharge =	$this->checkFuelSurcharge($record,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);			

			  // Jei paskaiciuota kuro priemoka nera 0, pridedam papildoma paslaugos eilute apacioje 
				if($fuel_surcharge){	
					$num_rows = $num_rows+1;
					$relatedProducts[$num_rows]['salesorderid'.$num_rows] = '';
					$relatedProducts[$num_rows]['storage'.$num_rows] = '';
					$relatedProducts[$num_rows]['salesorder_id'.$num_rows] = '';
					$relatedProducts[$num_rows]['shipment_code'.$num_rows] =  '';			
					$relatedProducts[$num_rows]['bill_ads'.$num_rows] =  '';
					$relatedProducts[$num_rows]['ship_ads'.$num_rows] =  '';
					$relatedProducts[$num_rows]['note'.$num_rows] =  '';	
					$relatedProducts[$num_rows]['costid'.$num_rows] =  103914;	
					$relatedProducts[$num_rows]['cargo_wgt'.$num_rows] = '';		
					$relatedProducts[$num_rows]['cargo_measure_for_invoice'.$num_rows] = '';
					$relatedProducts[$num_rows]['cargo_measure_invoice'.$num_rows] = '';
					$relatedProducts[$num_rows]['volume'.$num_rows] =  '';
					$relatedProducts[$num_rows]['service_names'.$num_rows] =  $services;	
					$relatedProducts[$num_rows]['listPrice'.$num_rows] =  (float)$fuel_surcharge;
					$relatedProducts[$num_rows]['netPrice'.$num_rows] =  (float)$fuel_surcharge;
					$relatedProducts[$num_rows]['productTotal'.$num_rows] =  (float)$fuel_surcharge;
					$relatedProducts[$num_rows]['hdnProductId'.$num_rows] =  (float)36641;	
					$relatedProducts[$num_rows]['entityType'.$num_rows] =  'Services';
					$relatedProducts[$num_rows]['service_id'.$num_rows] =  13;
					$relatedProducts[$num_rows]['productnameDimensions'.$num_rows] =  '';	
					$relatedProducts[$num_rows]['invoice_type'.$num_rows] =  '';
					$relatedProducts[$num_rows]['fail_remark'.$num_rows] = '';	
				}
			}

			// Gaunam papildomus mokescius

			$additional_taxes = $this->getAdditionalTaxes($record,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);
			
			// Atlyginimo mokestis
			if($additional_taxes['salary_tax']){	
				$num_rows = $num_rows+1;
				$relatedProducts[$num_rows]['salesorderid'.$num_rows] = '';
				$relatedProducts[$num_rows]['storage'.$num_rows] = '';
				$relatedProducts[$num_rows]['salesorder_id'.$num_rows] = '';
				$relatedProducts[$num_rows]['shipment_code'.$num_rows] =  '';			
				$relatedProducts[$num_rows]['bill_ads'.$num_rows] =  '';
				$relatedProducts[$num_rows]['ship_ads'.$num_rows] =  '';
				$relatedProducts[$num_rows]['note'.$num_rows] =  '';	
				$relatedProducts[$num_rows]['costid'.$num_rows] =  103914;	
				$relatedProducts[$num_rows]['cargo_wgt'.$num_rows] = '';		
				$relatedProducts[$num_rows]['cargo_measure_for_invoice'.$num_rows] = '';
				$relatedProducts[$num_rows]['cargo_measure_invoice'.$num_rows] = '';
				$relatedProducts[$num_rows]['volume'.$num_rows] =  '';
				$relatedProducts[$num_rows]['service_names'.$num_rows] =  $services;	
				$relatedProducts[$num_rows]['listPrice'.$num_rows] =  (float)$additional_taxes['salary_tax'];
				$relatedProducts[$num_rows]['netPrice'.$num_rows] =  (float)$additional_taxes['salary_tax'];
				$relatedProducts[$num_rows]['productTotal'.$num_rows] =  (float)$additional_taxes['salary_tax'];
				$relatedProducts[$num_rows]['hdnProductId'.$num_rows] =  (float)36641;	
				$relatedProducts[$num_rows]['entityType'.$num_rows] =  'Services';
				$relatedProducts[$num_rows]['service_id'.$num_rows] =  34;
				$relatedProducts[$num_rows]['productnameDimensions'.$num_rows] =  '';	
				$relatedProducts[$num_rows]['invoice_type'.$num_rows] =  '';
				$relatedProducts[$num_rows]['fail_remark'.$num_rows] = '';	
			}


			// Infliacijos mokestis
	
			if($additional_taxes['inflation_tax']){	
				$num_rows = $num_rows+1;
				$relatedProducts[$num_rows]['salesorderid'.$num_rows] = '';
				$relatedProducts[$num_rows]['storage'.$num_rows] = '';
				$relatedProducts[$num_rows]['salesorder_id'.$num_rows] = '';
				$relatedProducts[$num_rows]['shipment_code'.$num_rows] =  '';			
				$relatedProducts[$num_rows]['bill_ads'.$num_rows] =  '';
				$relatedProducts[$num_rows]['ship_ads'.$num_rows] =  '';
				$relatedProducts[$num_rows]['note'.$num_rows] =  '';	
				$relatedProducts[$num_rows]['costid'.$num_rows] =  103914;	
				$relatedProducts[$num_rows]['cargo_wgt'.$num_rows] = '';		
				$relatedProducts[$num_rows]['cargo_measure_for_invoice'.$num_rows] = '';
				$relatedProducts[$num_rows]['cargo_measure_invoice'.$num_rows] = '';
				$relatedProducts[$num_rows]['volume'.$num_rows] =  '';
				$relatedProducts[$num_rows]['service_names'.$num_rows] =  $services;	
				$relatedProducts[$num_rows]['listPrice'.$num_rows] =  (float)$additional_taxes['inflation_tax'];
				$relatedProducts[$num_rows]['netPrice'.$num_rows] =  (float)$additional_taxes['inflation_tax'];
				$relatedProducts[$num_rows]['productTotal'.$num_rows] =  (float)$additional_taxes['inflation_tax'];
				$relatedProducts[$num_rows]['hdnProductId'.$num_rows] =  (float)36641;	
				$relatedProducts[$num_rows]['entityType'.$num_rows] =  'Services';
				$relatedProducts[$num_rows]['service_id'.$num_rows] =  35;
				$relatedProducts[$num_rows]['productnameDimensions'.$num_rows] =  '';	
				$relatedProducts[$num_rows]['invoice_type'.$num_rows] =  '';
				$relatedProducts[$num_rows]['fail_remark'.$num_rows] = '';	
			}



		}elseif($request->get('invoices')){

			$invoices_ids = $request->get('invoices');

			$services = $db->pquery("SELECT * FROM app_services_type");

			$invoice_string = "SELECT *,		
															CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN 'Products' ELSE 'Services' END as entitytype,
															CASE WHEN vtiger_inventoryproductrel.productid = 36641 THEN vtiger_inventoryproductrel.service ELSE 0 END as service_id	 
																	FROM vtiger_invoice
																	LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.id=vtiger_invoice.invoiceid
																	LEFT JOIN vtiger_salesorder ON vtiger_salesorder.salesorderid=vtiger_inventoryproductrel.order_id
																	LEFT JOIN `app_measures` ON CASE WHEN  vtiger_inventoryproductrel.cargo_measure REGEXP '^[0-9]+$' 
																																		THEN app_measures.id=vtiger_inventoryproductrel.cargo_measure 
																																		ELSE app_measures.code =vtiger_inventoryproductrel.cargo_measure 
																															END
																	WHERE vtiger_invoice.invoiceid IN ($invoices_ids)	
																	GROUP BY vtiger_invoice.invoiceid, vtiger_inventoryproductrel.sequence_no													
																	ORDER BY vtiger_invoice.invoiceid DESC";

			$invoice = $db->pquery($invoice_string, array());
			$num_rows=$db->num_rows($invoice);	


			for($i = 1; $i <= $num_rows; $i++)	{
				$salesorderid = $db->query_result($invoice, $i-1, "salesorderid");
				$cargo_wgt=$db->query_result($invoice,$i-1,'cargo_wgt');
				$cargo_length=$db->query_result($invoice,$i-1,'cargo_length');
				$cargo_width=$db->query_result($invoice,$i-1,'cargo_width');
				$cargo_height=$db->query_result($invoice,$i-1,'cargo_height');
				$relatedProducts[$i]['salesorderid'.$i] = $salesorderid;
				$relatedProducts[$i]['salesorder_id'.$i] = $salesorderid;
				$relatedProducts[$i]['shipment_code'.$i] =  $db->query_result($invoice, $i-1, "shipment_code");
				$relatedProducts[$i]['bill_ads'.$i] =  $db->query_result($invoice, $i-1, "bill_ads");
				$relatedProducts[$i]['ship_ads'.$i] =  $db->query_result($invoice, $i-1, "ship_ads");
				$relatedProducts[$i]['note'.$i] =  $db->query_result($invoice, $i-1, "note");			
				$relatedProducts[$i]['cargo_wgt'.$i] = $cargo_wgt;
				$relatedProducts[$i]['cargo_measure_for_invoice'.$i] = $db->query_result($invoice, $i-1, "cargo_measure");
				$relatedProducts[$i]['cargo_measure_invoice'.$i] = $db->query_result($invoice, $i-1, "cargo_measure");
				$relatedProducts[$i]['volume'.$i] =  $db->query_result($invoice, $i-1, "m3");
				$relatedProducts[$i]['listPrice'.$i] =  (float)$db->query_result($invoice, $i-1, "listprice");
				$relatedProducts[$i]['netPrice'.$i] =  (float)$db->query_result($invoice, $i-1, "listprice");
				$relatedProducts[$i]['productTotal'.$i] =  (float)$db->query_result($invoice, $i-1, "listprice");
				$relatedProducts[$i]['hdnProductId'.$i] =  (float)$db->query_result($invoice, $i-1, "productid");	
				$relatedProducts[$i]['entityType'.$i] =  $db->query_result($invoice, $i-1, "entitytype");
				$relatedProducts[$i]['service_id'.$i] =  $db->query_result($invoice, $i-1, "service_id");
				$relatedProducts[$i]['service_names'.$i] =  $services;	
				$product_name =  ($cargo_wgt != 'z' && $cargo_length != 'z' && $cargo_width != 'z' && $cargo_height != 'z'?  $cargo_wgt." ".(float)$cargo_length . 'x' . (float)$cargo_width . 'x' . (float)$cargo_height : 'Nenurodyti matmenys');				
				$relatedProducts[$i]['productnameDimensions'.$i] =  $product_name;
			}


		}elseif($request->get('purchaseorderid')){

			$purchaseorderids = $request->get('purchaseorderid');

			$purchaseorder_string = "SELECT d.*
																FROM vtiger_purchaseorderdraft d
																INNER JOIN vtiger_crmentity e ON e.crmid=d.purchaseorderdraftid
																LEFT JOIN vtiger_vendorcf v ON v.vendorid=purchaseorderdraft_tks_vendor
																WHERE e.deleted = 0 AND purchaseorderdraftid IN ($purchaseorderids)
																GROUP BY purchaseorderdraftid";

					$purchaseorder = $db->pquery($purchaseorder_string, array());	
					$num_rows=$db->num_rows($purchaseorder);	

					for($i = 1; $i <= $num_rows; $i++)	{	
						// $note = $db->query_result($purchaseorder,$i-1,'purchaseorderdraft_tks_note');					
						

						$relatedProducts[$i]['purchase'.$i] = 'Transporto sąnaudos ES (paslaugų pirkimai iš 3-ųjų šalių)';					
					  $relatedProducts[$i]['comment'.$i] = $db->query_result($purchaseorder,$i-1,'purchaseorderdraft_tks_route_code');
					  $relatedProducts[$i]['purchase_date'.$i] = $db->query_result($purchaseorder,$i-1,'purchaseorderdraft_tks_routedate');
						$relatedProducts[$i]['object'.$i] = $db->query_result($purchaseorder,$i-1,'purchaseorderdraft_tks_registration_number');
						$relatedProducts[$i]['employee'.$i] = $db->query_result($purchaseorder,$i-1,'transaction_user');					
						$relatedProducts[$i]['listPrice'.$i] = $db->query_result($purchaseorder,$i-1,'purchaseorderdraft_tks_amount');					
						$relatedProducts[$i]['region'.$i] = 0;					
						$relatedProducts[$i]['productTotal'.$i] = $db->query_result($purchaseorder,$i-1,'purchaseorderdraft_tks_amount');					
						$relatedProducts[$i]['netPrice'.$i] = $db->query_result($purchaseorder,$i-1,'purchaseorderdraft_tks_amount');										
				
					}

		}

		$viewer->assign('TAX_REGIONS', $taxRegions);
		$viewer->assign('DEFAULT_TAX_REGION_INFO', $defaultRegionInfo);
		$viewer->assign('INVENTORY_CHARGES', Inventory_Charges_Model::getInventoryCharges());
		$viewer->assign('RELATED_PRODUCTS', $relatedProducts);
		$viewer->assign('DEDUCTED_TAXES', $deductTaxes);
		$viewer->assign('TAXES', $taxes);
		$viewer->assign('TAX_TYPE', $taxType);
		$viewer->assign('CURRENCINFO', $currencyInfo);
		$viewer->assign('CURRENCIES', $currencies);
		$viewer->assign('TERMSANDCONDITIONS', $termsAndConditions);
		if($newRecord){
			$viewer->assign('NEW_RECORD', $newRecord);
		}



		$productModuleModel = Vtiger_Module_Model::getInstance('Products');
		$viewer->assign('PRODUCT_ACTIVE', $productModuleModel->isActive());

		$serviceModuleModel = Vtiger_Module_Model::getInstance('Services');
		$viewer->assign('SERVICE_ACTIVE', $serviceModuleModel->isActive());

		// added to set the return values
		if ($request->get('returnview')) {
			$request->setViewerReturnValues($viewer);
		}

		if ($request->get('displayMode') == 'overlay') {
			$viewer->assign('SCRIPTS', $this->getOverlayHeaderScripts($request));
			echo $viewer->view('OverlayEditView.tpl', $moduleName);
		} else {
			$viewer->view('EditView.tpl', 'Inventory');
		}
	
	}

// ITOMA
	function getTare(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/tare_types.php");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch); 
		$data = json_decode($result,true);
		return $data['measures'];
	}

	function getInvoiceSelectQuery(){
		global $storageAddreses;	
		$salesorder_string = "SELECT vtiger_salesorder.salesorder_no, vtiger_salesorder.salesorderid ,vtiger_salesorder.total, vtiger_salesorder.shipment_code,  IF(vtiger_inventoryproductrel.productid = 0, 14244, IF(vtiger_inventoryproductrel.productid IS NULL, 14244,vtiger_inventoryproductrel.productid)) AS productid,
		CONCAT(IFNULL(vtiger_salesorder.load_date_from, ''),', ', IFNULL(vtiger_sobillads.load_company, ''),', ', IFNULL(vtiger_sobillads.bill_street, ''),', ', IFNULL(vtiger_sobillads.bill_city, ''),', ', IFNULL(vtiger_sobillads.bill_code, '')) AS bill_ads,
		CONCAT(IFNULL(vtiger_salesorder.unload_date_to, ''),', ', IFNULL(vtiger_soshipads.unload_company, ''),', ', IFNULL(vtiger_soshipads.ship_street, ''),', ', IFNULL(vtiger_soshipads.ship_city, ''),', ', IFNULL(vtiger_soshipads.ship_code, '')) AS ship_ads, ";

		$salesorder_string .= "CASE ";
		
		foreach($storageAddreses AS $key => $item):
			$explode = explode("|##|",$item);
			$salesorder_string .=	"WHEN (vtiger_sobillads.bill_street LIKE '%$explode[0]%' AND vtiger_sobillads.bill_city LIKE '$explode[1]%') OR (vtiger_soshipads.ship_street LIKE '%$explode[0]%' AND  vtiger_soshipads.ship_city LIKE '$explode[1]%') THEN '$key' ";		
		
		endforeach;
		$salesorder_string .=	"	ELSE '0' END AS storage, ";

		 $salesorder_string .= "	CASE WHEN  vtiger_accountscf.cf_1570
		 THEN vtiger_inventoryproductrel.comment
		 WHEN  vtiger_inventoryproductrel.productid = 36641 THEN vtiger_inventoryproductrel.comment
		 ELSE '' 
		END AS note,
		CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN 'Products' ELSE 'Services' END as entitytype,
		CASE WHEN vtiger_inventoryproductrel.productid = 36641 THEN vtiger_inventoryproductrel.service ELSE 0 END as service_id,
		CASE WHEN vtiger_inventoryproductrel.productid != 36641 
		THEN 
		IF(cf_1376 > 0,cf_1376, IF(SUM(vtiger_inventoryproductrel.margin) = vtiger_salesorder.total, IF(vtiger_inventoryproductrel.source = 'Metrika', FORMAT(SUM(vtiger_inventoryproductrel.margin)/count( vtiger_inventoryproductrel.id),2), 
		SUM(vtiger_inventoryproductrel.margin)), vtiger_salesorder.total))  
					WHEN vtiger_inventoryproductrel.productid = 36641 AND inventory_type != 'FuelSurcharge' THEN vtiger_inventoryproductrel.margin 
					WHEN vtiger_inventoryproductrel.productid = 36641 AND inventory_type = 'FuelSurcharge' THEN vtiger_inventoryproductrel.margin 
	END as margin,
	
		ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(revised_weight) ELSE SUM(ordered_weight) END,2) AS cargo_wgt,
		
		ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(m3) ELSE SUM((ordered_length * ordered_width * ordered_height) * quantity) END,2)  AS volume,
		CASE WHEN vtiger_inventoryproductrel.productid != 36641         
			THEN 
				CASE WHEN app_taxable_dimensions.type = 'revised'  
					THEN GROUP_CONCAT(FORMAT(vtiger_inventoryproductrel.revised_quantity,0),' ',revised_measure.code)  
          ELSE GROUP_CONCAT(FORMAT(vtiger_inventoryproductrel.quantity,0),' ',app_measures.code)  
				END            
      ELSE vtiger_inventoryproductrel.cargo_wgt 
		END as cargo,			

		CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_length) ELSE vtiger_inventoryproductrel.cargo_length END as cargo_length,
		CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_width)  ELSE vtiger_inventoryproductrel.cargo_width END AS cargo_width,
		CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_height) ELSE vtiger_inventoryproductrel.cargo_height END as cargo_height,

		CASE WHEN vtiger_inventoryproductrel.productid != 36641 
				 THEN  FORMAT(SUM(vtiger_inventoryproductrel.quantity),0)  ELSE  FORMAT(vtiger_inventoryproductrel.quantity,0) 
		END AS quantity,

		CASE WHEN app_taxable_dimensions.type = 'revised' 
			THEN 	
				CASE WHEN vtiger_inventoryproductrel.productid != 36641 
					THEN FORMAT(SUM(vtiger_inventoryproductrel.revised_quantity),0)
					ELSE FORMAT(vtiger_inventoryproductrel.revised_quantity,0) 
				END				
		ELSE  
				CASE WHEN vtiger_inventoryproductrel.productid != 36641 
				  THEN	FORMAT(SUM(vtiger_inventoryproductrel.quantity),0) 
					ELSE  FORMAT(vtiger_inventoryproductrel.quantity,0) 
				END
		END AS quantity,

		CASE 
			WHEN vtiger_salesorder.carrier LIKE '%Neteisingas adresas (pasikrovimas%' THEN 'Tuščias važiavimas. '
			WHEN vtiger_salesorder.carrier LIKE '%Neteisingas adresas (pristatymas%' THEN 'Pristatymas kitu adresu. '
			WHEN vtiger_salesorder.carrier LIKE '%Negalimas privažiavimas%' THEN 'Pristatymas kitu adresu. '
			WHEN vtiger_salesorder.carrier LIKE '%Krovinis netinkamai supakuotas transportavimui%' THEN 'Krovinis netinkamai supakuotas transportavimui - Papildomas paruošimas transportavimui. '
			WHEN vtiger_salesorder.carrier LIKE '%Krovinis neatitinka užsakymo%' THEN 'Tuščias važiavimas. '
			WHEN vtiger_salesorder.carrier LIKE '%Tuščias važiavimas%' THEN 'Tuščias važiavimas. '
			ELSE ''
		END AS fail_remark,
		vtiger_inventoryproductrel.service,(SELECT ic.cf_1277 FROM vtiger_invoice i,vtiger_invoicecf ic,vtiger_crmentity c WHERE i.invoiceid=ic.invoiceid AND c.crmid=i.invoiceid AND i.salesorderid=vtiger_salesorder.salesorderid AND ic.cf_1277='Išankstinė' AND c.deleted = 0 ORDER BY i.invoiceid LIMIT 1) as invoice_type ";

		return $salesorder_string;
	}

	function getInvoiceFromQuery($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids, $pre){
	  $salesorder_string ="	FROM vtiger_salesorder  
										LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid              
										LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
										LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid
										LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
										LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid
										LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid                    
										LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
										LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid 
										LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid 
										LEFT JOIN `app_taxable_dimensions` ON app_taxable_dimensions.salesorderid=vtiger_salesorder.salesorderid       
										LEFT JOIN `app_measures` ON CASE WHEN  (vtiger_inventoryproductrel.cargo_measure REGEXP '^[0-9]+$') THEN app_measures.id=vtiger_inventoryproductrel.cargo_measure ELSE TRIM(app_measures.code) = TRIM(vtiger_inventoryproductrel.cargo_measure) END
										LEFT JOIN `app_measures` revised_measure ON  TRIM(revised_measure.code)=TRIM(vtiger_inventoryproductrel.revised_measure)   
										WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesorder.accountid = ? ";
										if($filterBy != 'salesorderid'){
											$salesorder_string .="	AND vtiger_invoice_salesorders_list.salesorderid IS NULL ";
										}
										if(!$pre){
											$salesorder_string .=" AND vtiger_salesordercf.cf_1468 = 0 AND vtiger_salesordercf.cf_1456 = 1 ";
										}

								//	AND	CASE WHEN vtiger_salesordercf.cf_1456 THEN ";
										if(!empty($status)){
					              $salesorder_string .= " AND vtiger_salesorder.sostatus LIKE '$status%'  ";
					          }
										// $salesorder_string .= "	CASE 
										// 	WHEN  vtiger_accountscf.cf_1281 = 'Kas savaitę' THEN YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW()) AND WEEK(vtiger_crmentity.createdtime, 1) < WEEK(NOW(), 1) 
										// 	WHEN  vtiger_accountscf.cf_1281 = 'Du kartus per mėn.'                                             
										// 		THEN (MONTH(vtiger_crmentity.createdtime) = MONTH(NOW()) 
										// 		AND (DAYOFMONTH(NOW()) >= 15) AND DAYOFMONTH(vtiger_crmentity.createdtime) < 15) 
										// 		OR  (DAYOFMONTH(NOW()) = '".date('t')."' AND DAYOFMONTH(vtiger_crmentity.createdtime) > 15)
										// 		OR MONTH(NOW()) > MONTH(vtiger_crmentity.createdtime)
										// 	WHEN vtiger_accountscf.cf_1281 = 'Už visą mėn.' THEN MONTH(vtiger_crmentity.createdtime) < MONTH(NOW()) OR YEAR(NOW()) > YEAR(vtiger_crmentity.createdtime)
										// 	ELSE YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW()) 
										// 	END ";

										if($filterBy == 'salesorderid'){
											$salesorder_string .= " AND  vtiger_salesorder.salesorderid IN ($order_ids) ";
										}else{
											if(!empty($from) && !empty($to)){
												if($filterBy == 'order_date'){
													$salesorder_string .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$from' AND '$to' ";
												}elseif($filterBy == 'load_date'){
													$salesorder_string .= " AND vtiger_salesorder.load_date_from BETWEEN '$from' AND '$to' ";
												}elseif($filterBy == 'unload_date'){
													$salesorder_string .= " AND vtiger_salesorder.unload_date_to BETWEEN '$from' AND '$to' ";
												}
											}
										  if(!empty($orderDate)){
												$salesorder_string .= " AND  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') =  '$orderDate' ";
											 }  
											 if(!empty($orderNumber)){
												$salesorder_string .= " AND  vtiger_salesorder.shipment_code LIKE '%$orderNumber%' ";
											 }
											 if(!empty($customer)){
												$salesorder_string .= " AND  vtiger_account.accountname LIKE '$customer%' ";
											 } 	
											 if(!empty($pricebook)){
												$salesorder_string .= " AND  vtiger_salesordercf.cf_1297 LIKE '$pricebook%' ";
											 }										 
											 if(!empty($loadDate)){
												$salesorder_string .= " AND  vtiger_salesorder.load_date_from = '$loadDate' ";
											 }
											 if(!empty($loadAddress)){
												$salesorder_string .= " AND CONCAT(vtiger_sobillads.bill_street,' ', vtiger_sobillads.bill_city,' ',vtiger_sobillads.bill_code) LIKE '%$loadAddress%' ";
											 }
											 if(!empty($unloadDate)){
												$salesorder_string .= " AND  vtiger_salesorder.unload_date_from = '$unloadDate' ";
											 }
											 if(!empty($unloadAddress)){  
												$salesorder_string .= " AND CONCAT(vtiger_soshipads.ship_street,' ', vtiger_soshipads.ship_city,' ',vtiger_soshipads.ship_code) LIKE '%$unloadAddress%' ";
											 }
											 if(!empty($manager)){
												$salesorder_string .= " AND  vtiger_users.user_name LIKE '$manager%' OR vtiger_users.first_name LIKE'$manager%' OR vtiger_users.last_name  LIKE '$manager%' OR CONCAT(vtiger_users.first_name,vtiger_users.last_name) LIKE '%".$manager."%'";
											 }
										}
											 // $salesorder_string .= " ELSE '' END ";

			return $salesorder_string;								 
	}

	function getInvoiceFromQueryForAdditionalServises($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids){
	  $salesorder_string ="	FROM vtiger_salesorder  
										LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid              
										LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
										LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid                 
										LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid 	
										LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
										LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid 	
										LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid		
										WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesorder.accountid = ? AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_salesordercf.cf_1468 = 0 AND vtiger_salesordercf.cf_1456 = 1 ";
							
										if(!empty($status)){
					              $salesorder_string .= " AND vtiger_salesorder.sostatus LIKE '$status%'  ";
					          }
									
										if($filterBy == 'salesorderid'){
											$salesorder_string .= " AND  vtiger_salesorder.salesorderid IN ($order_ids) ";
										}else{
											if(!empty($from) && !empty($to)){
												if($filterBy == 'order_date'){
													$salesorder_string .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$from' AND '$to' ";
												}elseif($filterBy == 'load_date'){
													$salesorder_string .= " AND vtiger_salesorder.load_date_from BETWEEN '$from' AND '$to' ";
												}elseif($filterBy == 'unload_date'){
													$salesorder_string .= " AND vtiger_salesorder.unload_date_to BETWEEN '$from' AND '$to' ";
												}
											}
										  if(!empty($orderDate)){
												$salesorder_string .= " AND  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') =  '$orderDate' ";
											 }  
											 if(!empty($orderNumber)){
												$salesorder_string .= " AND  vtiger_salesorder.shipment_code LIKE '%$orderNumber%' ";
											 }
											 if(!empty($customer)){
												$salesorder_string .= " AND  vtiger_account.accountname LIKE '$customer%' ";
											 } 	
											 if(!empty($pricebook)){
												$salesorder_string .= " AND  vtiger_salesordercf.cf_1297 LIKE '$pricebook%' ";
											 }										 
											 if(!empty($loadDate)){
												$salesorder_string .= " AND  vtiger_salesorder.load_date_from = '$loadDate' ";
											 }
											 if(!empty($loadAddress)){
												$salesorder_string .= " AND CONCAT(vtiger_sobillads.bill_street,' ', vtiger_sobillads.bill_city,' ',vtiger_sobillads.bill_code) LIKE '%$loadAddress%' ";
											 }
											 if(!empty($unloadDate)){
												$salesorder_string .= " AND  vtiger_salesorder.unload_date_from = '$unloadDate' ";
											 }
											 if(!empty($unloadAddress)){  
												$salesorder_string .= " AND CONCAT(vtiger_soshipads.ship_street,' ', vtiger_soshipads.ship_city,' ',vtiger_soshipads.ship_code) LIKE '%$unloadAddress%' ";
											 }
											 if(!empty($manager)){
												$salesorder_string .= " AND  vtiger_users.user_name LIKE '$manager%' OR vtiger_users.first_name LIKE'$manager%' OR vtiger_users.last_name  LIKE '$manager%' OR CONCAT(vtiger_users.first_name,vtiger_users.last_name) LIKE '%".$manager."%'";
											 }
										}								

			return $salesorder_string;								 
	}

	function getRoadTaxForOrderByUser($user_id,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids){
		$db = PearDatabase::getInstance();

		// Gaunam uzsakymo svori
		$salesorder_string = "SELECT vtiger_salesorder.salesorderid, vtiger_salesorder.external_order_id, 
		(SELECT SUM(cargo_wgt) FROM vtiger_inventoryproductrel WHERE id =vtiger_salesorder.salesorderid AND productid = 14244) AS weight,
		(SELECT city FROM crm_post_codes WHERE post_code = bill_code LIMIT 1) as load_city,
    (SELECT city FROM crm_post_codes WHERE post_code = ship_code LIMIT 1) as unload_city  ";
		$salesorder_string .= $this->getInvoiceFromQueryForAdditionalServises($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);
		$salesorder_string .= " AND cf_2724 IS NULL GROUP BY vtiger_salesorder.salesorderid	ORDER BY vtiger_salesorder.salesorderid ASC LIMIT 500";

		$result = $db->pquery("SELECT value FROM app_other_settings WHERE title = 'LBL_ROAD_TAX_FEE'");
		$road_tax_fee = $db->query_result($result,0, 'value');		

		$insert_inventory = "INSERT INTO vtiger_inventoryproductrel (id,external_order_id,productid,margin,note,service,inventory_type, costcenter) VALUES (?,?,?,?,?,?,?,?)";

		$check_are_exist_services_sql = "SELECT id FROM vtiger_inventoryproductrel WHERE id IN ($order_ids) AND service IN (8, 14) AND inventory_type = 'RoadTax'";
		$check_are_exist_services = $db->pquery($check_are_exist_services_sql, array());

		$check_exist = [];
		foreach ($check_are_exist_services as $value) {
			$check_exist[$value['id']] = $value;
		}

		$orders = $db->pquery($salesorder_string, array($user_id));

		foreach($orders AS $row) {
			if(empty($check_exist[$row['salesorderid']])){

				if($row['load_city'] != $row['unload_city']){
					if($row['weight'] >= 1000){
						$tax = (ceil($row['weight']/1000) * 1000) / 1000 * $road_tax_fee;	
					}	else{
						$tax = $road_tax_fee;	
					}	
							
					$db->pquery($insert_inventory,array($row['salesorderid'], $row['external_order_id'], 36641, $tax, '', 14, 'RoadTax', 103914));
					
				}
			}
		}

	}

	function getRoadTaxForOrder($user_id,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids){
		$db = PearDatabase::getInstance();

		// Gaunam uzsakymus kuriems ranka irasytas keliu mokestis
		$salesorder_string = "SELECT cf_2724 AS road_tax, vtiger_salesorder.salesorderid, vtiger_salesorder.external_order_id ";
		$salesorder_string .= $this->getInvoiceFromQueryForAdditionalServises($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);
		$salesorder_string .= " AND cf_2724 IS NOT NULL GROUP BY vtiger_salesorder.salesorderid	ORDER BY vtiger_salesorder.salesorderid ASC LIMIT 500";
		
		$insert_inventory = "INSERT INTO vtiger_inventoryproductrel (id,external_order_id,productid,margin,note,service,inventory_type, costcenter) VALUES (?,?,?,?,?,?,?,?)";


		$check_are_exist_services_sql = "SELECT id FROM vtiger_inventoryproductrel WHERE id IN ($order_ids) AND service IN (8, 14) AND inventory_type = 'RoadTax'";
		$check_are_exist_services = $db->pquery($check_are_exist_services_sql, array());

		$check_exist = [];
		foreach ($check_are_exist_services as $value) {
			$check_exist[$value['id']] = $value;
		}

		$orders = $db->pquery($salesorder_string, array($user_id));

		foreach($orders AS $row) {
			if($row['road_tax'] > 0){
				if(empty($check_exist[$row['salesorderid']])){
					$db->pquery($insert_inventory,array($row['salesorderid'], $row['external_order_id'], 36641, $row['road_tax'], '', 14, 'RoadTax', 103914));
				}
			}
		}

	}

	function checkFuelSurchargeForOrder($user_id,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids){
		$db = PearDatabase::getInstance();
		// Gaunam uzsakymo kainas ir uzsakymu datas
		$salesorder_string = "SELECT ROUND(total,2) AS total, DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') AS order_date, vtiger_salesorder.salesorderid, vtiger_salesorder.external_order_id ";
		$salesorder_string .= $this->getInvoiceFromQueryForAdditionalServises($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);
		$salesorder_string .= " GROUP BY vtiger_salesorder.salesorderid 	ORDER BY vtiger_salesorder.salesorderid ASC LIMIT 500";

		$get_history = $db->pquery("SELECT fuelpricelist_tks_galiojimopra AS price_start, fuelpricelist_tks_galiojimopab AS price_end, ROUND(fuelpricelist_tks_kurokaina,2) AS price
																FROM vtiger_fuelpricelist
																INNER JOIN vtiger_crmentity ON crmid=fuelpricelistid
																WHERE setype = 'Fuelpricelist' AND deleted = 0");															

		// Gaunam kuro priemokos procenta pagal tai koks kuro priemokos procentas buvo taikomas uzsakymo diena
		$get_client_ranges = $db->pquery("SELECT clientfuelrange_tks_minkurokai AS min_price, clientfuelrange_tks_maxkurokai AS max_price, clientfuelrange_tks_priemoka AS surcharge
																			FROM vtiger_clientfuelrange  
																			LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_clientfuelrange.clientfuelrangeid   
																			LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid                 
																			WHERE fuel_entity.deleted=0 AND vtiger_crmentityrel.crmid = ? 
																			ORDER BY clientfuelrangeid DESC", array($user_id));
		$orders = $db->pquery($salesorder_string, array($user_id));	
		
		$insert_inventory = "INSERT INTO vtiger_inventoryproductrel (id,external_order_id,productid,margin,note,service,inventory_type, costcenter) VALUES (?,?,?,?,?,?,?,?)";

		// Suskaiciuojam sumine kuro priemoka nuo uzsakymo sumos

		$check_are_exist_services_sql = "SELECT id FROM vtiger_inventoryproductrel WHERE id IN ($order_ids) AND service IN (8, 13) AND inventory_type = 'FuelSurcharge'";
		$check_are_exist_services = $db->pquery($check_are_exist_services_sql, array());

		$check_exist = [];
		foreach ($check_are_exist_services as $value) {
			$check_exist[$value['id']] = $value;
		}

		foreach($orders AS $row) {
			if(empty($check_exist[$row['salesorderid']])){
				$surcharge = [];		
				$calc_surcharge = 0;	
				foreach ($get_history as $his){
					if( $row['order_date'] >= $his['price_start'] &&  $row['order_date'] <= $his['price_end'] ){
						foreach ($get_client_ranges as $ran) {
							if($ran['min_price'] <= $his['price'] &&  $his['price'] <= $ran['max_price']){
								$surcharge[] = $ran['surcharge'];
							}			
						}
						$sur = max($surcharge);
					}
				}

				$calc_surcharge = round(($row['total']/100) * $sur, 2);	
				if($calc_surcharge > 0){
					$db->pquery($insert_inventory,array($row['salesorderid'], $row['external_order_id'], 36641, $calc_surcharge, '', 13, 'FuelSurcharge', 103914));
				}		
			}		
		}
	}

	function checkFuelSurcharge($user_id,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids){
		$db = PearDatabase::getInstance();
		// Gaunam uzsakymo kainas ir uzsakymu datas
		$salesorder_string = "SELECT ROUND(total,2) AS total, DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') AS order_date ";
		$salesorder_string .= $this->getInvoiceFromQueryForAdditionalServises($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);
		$salesorder_string .= " GROUP BY vtiger_salesorder.salesorderid 	ORDER BY vtiger_salesorder.salesorderid ASC LIMIT 500";

		$get_history = $db->pquery("SELECT fuelpricelist_tks_galiojimopra AS price_start, fuelpricelist_tks_galiojimopab AS price_end, ROUND(fuelpricelist_tks_kurokaina,2) AS price
																FROM vtiger_fuelpricelist
																INNER JOIN vtiger_crmentity ON crmid=fuelpricelistid
																WHERE setype = 'Fuelpricelist' AND deleted = 0");															

		// Gaunam kuro priemokos procenta pagal tai koks kuro priemokos procentas buvo taikomas uzsakymo diena
		$get_client_ranges = $db->pquery("SELECT clientfuelrange_tks_minkurokai AS min_price, clientfuelrange_tks_maxkurokai AS max_price, clientfuelrange_tks_priemoka AS surcharge
																			FROM vtiger_clientfuelrange  
																			LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_clientfuelrange.clientfuelrangeid   
																			LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid                 
																			WHERE fuel_entity.deleted=0 AND vtiger_crmentityrel.crmid = ? 
																			ORDER BY clientfuelrangeid DESC", array($user_id));
	
	
		
		$orders = $db->pquery($salesorder_string, array($user_id));


		// Suskaiciuojam sumine kuro priemoka nuo uzsakymo sumos
		$total_surcharge = 0;
		$total = 0;
		foreach($orders AS $row) {

			$surcharge = [];			
			foreach ($get_history as $his){

				if( $row['order_date'] >= $his['price_start'] &&  $row['order_date'] <= $his['price_end'] ){		
					foreach ($get_client_ranges as $ran) {
						if($ran['min_price'] <= $his['price'] &&  $his['price'] <= $ran['max_price']){
							$surcharge[] = $ran['surcharge'];
						}			
					}		
					$sur = max($surcharge);
				}
			
			
			}


			$total_surcharge += ($row['total']/100) * $sur;	
			$total += $row['total'];		
		}

		if($total_surcharge > 0) 	$total_surcharge = round($total_surcharge,2);

		return $total_surcharge;	
	}

	function checkRestInTerminal($user_id,$orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids, $pre){
		$db = PearDatabase::getInstance();
		$external_order_id = array();
		$cargo_measure = array();
		$salesorder_string = "SELECT vtiger_salesorder.external_order_id, IF(cargo_measure = 1, 'pll',cargo_measure) AS cargo_measure,cf_2479 AS fee_limit ";
		$salesorder_string .= $this->getInvoiceFromQuery($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids, $pre);
		$salesorder_string .= " AND productid = 14244	GROUP BY vtiger_salesorder.salesorderid ,vtiger_inventoryproductrel.service,external_load_id	ORDER BY vtiger_salesorder.external_order_id ASC,productid ASC LIMIT 500";
		

		$salesorder = $db->pquery($salesorder_string, array($user_id));
		$num_rows=$db->num_rows($salesorder);
		
		$fee_limit = $db->query_result($salesorder,0,'fee_limit');

		for($i = 1; $i <= $num_rows; $i++)	{
			$external_id = $db->query_result($salesorder,$i-1,'external_order_id');
			$external_order_id[] = $external_id;
			$cargo_measure[$external_id][] = trim($db->query_result($salesorder,$i-1,'cargo_measure'));		
		}	

		$external_order_ids = implode(',',$external_order_id);


		$check_are_exist_services_sql = "SELECT * FROM vtiger_inventoryproductrel WHERE external_order_id IN ($external_order_ids) AND service = 31";
		$check_are_exist_services = $db->pquery($check_are_exist_services_sql, array());
		$are_services_exist = $db->num_rows($check_are_exist_services);	

		if(!$are_services_exist){
			$pricebook_sql = "SELECT storagepricebook_tks_storageop, l.fee,l.unit, IF(operation_id = 7, 'pll','mix') AS measure
												FROM vtiger_storagepricebook p
												JOIN vtiger_storagepricebook_lines l ON l.related=p.storagepricebookid										
												WHERE accountid = ? AND operation_id IN (7,8)";

			$user_pricebook = $db->pquery($pricebook_sql, array($user_id));
			$num_rows2=$db->num_rows($user_pricebook);
			
			if($num_rows2){
				$pricebook = $user_pricebook;
			}else{
				$pricebook = $db->pquery($pricebook_sql, array(''));
			}

			$pricebook_info = array();
			
			foreach($pricebook AS $info){
				if($info['measure'] == 'pll'){
					$pricebook_info['pll'] = $info['fee'];
				}else{
					$pricebook_info['mix'] = $info['fee'];
				}

			}

			$select = 'SHIPMENT_ID,WORK_DAYS_IN_TERMINAL,LOAD_DATE,UNLOAD_DATE';

			$table = 'SHIPMENTS_DURATION_IN_TERMINAL';

			$where = "SHIPMENT_ID IN ($external_order_ids) AND WORK_DAYS_IN_TERMINAL > 0";

			$headers = array(
				'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => $select, 'table' => $table, 'where' => $where));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			$result = curl_exec($ch);
			curl_close($ch);
			$rests = json_decode($result, true);


			$check_salesorder = "SELECT id,external_order_id,external_load_id FROM vtiger_inventoryproductrel WHERE external_order_id = ?";
			$insert_inventory = "INSERT INTO vtiger_inventoryproductrel (id,external_order_id,external_load_id,productid,margin,note,comment,description,service,inventory_type) VALUES (?,?,?,?,?,?,?,?,?,?)";


			foreach($rests AS $rest){						
				$match = array();

				$get_order_info = $db->pquery($check_salesorder, array($rest['SHIPMENT_ID']));
				
				$id =	$db->query_result($get_order_info,0,'id');	
				$external_order_id = $db->query_result($get_order_info,0,'external_order_id');	
				$external_load_id =	$db->query_result($get_order_info,0,'external_load_id');	

				for($i = 0; $i < count($cargo_measure[$rest['SHIPMENT_ID']]);$i++){
					if($cargo_measure[$rest['SHIPMENT_ID']][$i] == 'pll'){
						$match[] = 1;
					}
				}

				if(count($cargo_measure[$rest['SHIPMENT_ID']]) == count($match)){
					$price = $pricebook_info['pll'];						
				}else{
					$price = $pricebook_info['mix'];						
				}

				$calc_price = ($fee_limit ? (($price * $rest['WORK_DAYS_IN_TERMINAL']) < 5 ? 5 : $price * $rest['WORK_DAYS_IN_TERMINAL']) : $price * $rest['WORK_DAYS_IN_TERMINAL']);
				$note = $rest['UNLOAD_DATE']." - ".$rest['LOAD_DATE'];

				$db->pquery($insert_inventory,array($id,$external_order_id,$external_load_id,36641,$calc_price,$note,$note,$note,31,'rest_in_terminal'));
			}
		}
	}

	public function getAdditionalTaxes($user_id, $orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids){
		$db = PearDatabase::getInstance();

		$get_individual_procents = $db->pquery("SELECT cf_2746, cf_2748 FROM vtiger_accountscf WHERE accountid = ?", [$user_id]);
		$salary_procent = $db->query_result($get_individual_procents, 0, 'cf_2746');
		$inflation_procent = $db->query_result($get_individual_procents, 0, 'cf_2748');

		$get_main_surcharges = $db->pquery("SELECT title, value FROM app_other_settings WHERE title IN ('LBL_SALARY_TAX', 'LBL_INFLATION_TAX')", []);
		$main_surcharges = $db->pluck($get_main_surcharges, 'value', 'title');

		if(empty($salary_procent)){
			$salary_procent = $main_surcharges['LBL_SALARY_TAX'];
		}

		if(empty($inflation_procent)){
			$inflation_procent = $main_surcharges['LBL_INFLATION_TAX'];
		}

		$salesorder_string = "SELECT ROUND(total,2) AS total ";
		$salesorder_string .= $this->getInvoiceFromQueryForAdditionalServises($orderDate,$from,$to,$filterBy,$orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$order_ids);
		$salesorder_string .= " GROUP BY vtiger_salesorder.salesorderid ORDER BY vtiger_salesorder.salesorderid ASC LIMIT 500";

		$orders = $db->pquery($salesorder_string, array($user_id));
	
		$salary_tax = 0;
		$inflation_tax = 0;

		foreach($orders AS $row) {	
			if(!empty($salary_procent)){			
				$salary_tax += ($row['total']/100) * $salary_procent;	
			}
			
			if(!empty($inflation_procent)){	
				$inflation_tax += ($row['total']/100) * $inflation_procent;	
			}
		}		
	
		
		if($salary_tax > 0) {
			$salary_tax = round($salary_tax, 2);
		}

		if($inflation_tax > 0) {
			$inflation_tax = round($inflation_tax, 2);
		}

		return ['salary_tax' => $salary_tax, 'inflation_tax' => $inflation_tax];
	}

	/**
	 * Function to get the list of Script models to be included
	 * @param Vtiger_Request $request
	 * @return <Array> - List of Vtiger_JsScript_Model instances
	 */
	function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);

		$moduleName = $request->getModule();
		$modulePopUpFile = 'modules.'.$moduleName.'.resources.Popup';
		$moduleEditFile = 'modules.'.$moduleName.'.resources.Edit';
		unset($headerScriptInstances[$modulePopUpFile]);
		unset($headerScriptInstances[$moduleEditFile]);

		$jsFileNames = array(
				'modules.Inventory.resources.Edit',
				'modules.Inventory.resources.Popup',
				'modules.PriceBooks.resources.Popup',
		);
		$jsFileNames[] = $moduleEditFile;
		$jsFileNames[] = $modulePopUpFile;
		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}

	public function getOverlayHeaderScripts(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$modulePopUpFile = 'modules.'.$moduleName.'.resources.Popup';
		$moduleEditFile = 'modules.'.$moduleName.'.resources.Edit';

		$jsFileNames = array(
			'modules.Inventory.resources.Popup',
			'modules.PriceBooks.resources.Popup',
		);
		$jsFileNames[] = $moduleEditFile;
		$jsFileNames[] = $modulePopUpFile;
		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		return $jsScriptInstances;
	}

}
