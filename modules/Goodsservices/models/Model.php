<?php
namespace modules\GoodsServices\models;

class Model {

  public function __construct(){
    global $adb;
    $this->db = $adb;
  }


  public function servicesList()
  {
    $list = $this->db->query("SELECT * FROM app_services_type WHERE id != 30 ORDER BY id ASC");
    return $list;
  }

  public function getSingleService($record)
  {
    $service = $this->db->pquery("SELECT * FROM app_services_type WHERE id = ?", array($record));
    $id = $this->db->query_result($service,0,'id');
    $name = $this->db->query_result($service,0,'name');
    $code = $this->db->query_result($service,0,'code');
    $storage_code = $this->db->query_result($service,0,'storage_code');
    return array('name' => $name, 'code' => $code, 'storage_code' => $storage_code);
  }

  public function saveService($record, $post){
    $code = $post['code'];
    $code_id = $post['code_id'];
    $storage = $post['storage_code'];
    $sql = "UPDATE app_services_type SET code = ?, code_id = ?, storage_code = ? WHERE id = ?";
    $this->db->pquery($sql, array($code,$code_id,$storage,$record));

    $referer = $_SERVER['PHP_SELF']."?module=Goodsservices&view=List";
    header("Location: $referer");
  }
}
