<?php

include "modules/Goodsservices/models/Model.php";

use modules\GoodsServices\models\Model as Model;

class GoodsServices_Edit_View extends Vtiger_Index_View {

       function __construct(){
              $this->Model = new Model;  
       }

	public function process(Vtiger_Request $request) {
                $record = $_GET['record'];  

                if($_POST['editCode']){
                     $this->Model->saveService($record, $_POST);   
                }

                $service = $this->Model->getSingleService($record);               
                $viewer = $this->getViewer($request);   
                $viewer->assign('service', $service);      
                $viewer->view('Edit.tpl', $request->getModule()); 
  }
       

}


