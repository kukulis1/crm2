<?php

include "modules/Goodsservices/models/Model.php";

use modules\GoodsServices\models\Model as Model;

class GoodsServices_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->Model = new Model;  
       }

	public function process(Vtiger_Request $request) {
                $list = $this->Model->servicesList();               
                $viewer = $this->getViewer($request);   
                $viewer->assign('list', $list);      
                $viewer->view('List.tpl', $request->getModule()); 
  }
       

}


