<?php
namespace modules\Opex\models;

class exportExcel  {

  public function __construct()
  {
    global $adb;
    $this->db = $adb;
  }

  public function getCostNameById($cost_center){
    if($cost_center == 103921) $cost_name = 'ADMINISTRACIJA';                                    
    if($cost_center == 103920) $cost_name = 'KAUNO SANDĖLIS';                                    
    if($cost_center == 103919) $cost_name = 'KLAIPĖDOS SANDĖLIS';                                                                      
    if($cost_center == 103918) $cost_name = 'VILNIAUS SANDĖLIS';                                    
    if($cost_center == 103917) $cost_name = 'PERKRAUSTYMAS';                                    
    if($cost_center == 103916) $cost_name = 'SERVISAS';                                    
    if($cost_center == 103915) $cost_name = 'EKSPEDIJAVIMAS';                                    
    if($cost_center == 103914) $cost_name = 'TRANSPORTAS';  
    return $cost_name;
  }

  public function costListNumber($cost_name)
  {
    $sql = "SELECT cost,cost_center FROM app_opex_cost_list WHERE `name` = ?";
    $records = $this->db->pquery($sql, array($cost_name));
    $cost = $this->db->query_result($records,0,'cost');
    $cost_center = $this->db->query_result($records,0,'cost_center');     
    return array('cost' => $cost, 'costcenter' => $this->getCostNameById($cost_center));
  }

  public function costListInvoiceNumber($cost_name)
  {
    $sql = "SELECT cost_center,cost FROM app_opex_invoice_cost_list WHERE `name` = ?";
    $records = $this->db->pquery($sql, array($cost_name));
    $cost_center = $this->db->query_result($records,0,'cost_center');
    $cost = $this->db->query_result($records,0,'cost');
    return array('cost_center' => $cost_center,'cost' => $cost) ;
  }

  public function totalProfit($year,$type)
  {
    $year_months_num = $this->thisYearMonthsNum($year);
    // $sql = "SELECT REPLACE(ROUND(SUM(total),2),',','.') as total
    //             FROM vtiger_invoice inv           
    //             LEFT JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid                    
    //             WHERE deleted = 0 AND setype = 'Invoice' AND DATE_FORMAT(invoicedate,'%Y') = ? ";

    $sql = "SELECT REPLACE(ROUND(SUM(inv.total),2),',','.') as total
              FROM vtiger_invoice inv                  
              JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid 
              JOIN vtiger_invoicecf i ON i.invoiceid=inv.invoiceid
              WHERE deleted = 0 AND setype = 'Invoice'  AND cf_1486 = 'Transportas' AND DATE_FORMAT(invoicedate,'%Y') = ?";
    // $sql2 = "SELECT REPLACE(ROUND(SUM(inv.total),2),',','.') as total
    //           FROM vtiger_invoice inv                  
    //           JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid 
    //           JOIN vtiger_invoicecf i ON i.invoiceid=inv.invoiceid
    //           WHERE deleted = 0 AND setype = 'Invoice'  AND cf_1486 = 'PVM sąskaita-faktūra' AND DATE_FORMAT(invoicedate,'%Y') = ?";

           if($type == 'All'){         
                $records = $this->db->pquery($sql, array($year));
                // $records2 = $this->db->pquery($sql2, array($year));
                $total = $this->db->query_result($records,0,'total');
                // $total2 = $this->db->query_result($records2,0,'total');
                $result = $total;

           }else{
            $result = array();
            $sql .=" AND  DATE_FORMAT(invoicedate,'%m') = ? "; 
            // $sql2 .=" AND  DATE_FORMAT(invoicedate,'%m') = ? ";
              for($i = 1; $i < $year_months_num+1; $i++){
                $records = $this->db->pquery($sql, array($year, $i));
                // $records2 = $this->db->pquery($sql2, array($year, $i));
                $total = $this->db->query_result($records,0,'total');
                // $total2 = $this->db->query_result($records2,0,'total');
                $result[] = $total;
              }
            }



    return $result;
  }

  // public function totalProfit($type)
  // {

  //   $year = date("Y");
  //   $year_months_num = $this->thisYearMonthsNum();
  //   $sql = "SELECT ROUND(SUM(total),2) as total
  //               FROM vtiger_invoice inv           
  //               LEFT JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid                    
  //               WHERE deleted = 0 AND setype = 'Invoice' AND DATE_FORMAT(createdtime,'%Y') = ? ";

  //          if($type == 'All'){         
  //               $records = $this->db->pquery($sql, array($year));
  //               $total = $this->db->query_result($records,0,'total');
  //               $result = $total;

  //          }else{
  //           $result = array();
  //           $sql .=" AND  DATE_FORMAT(createdtime,'%m') = ? ";
  //             for($i = 1; $i < $year_months_num+1; $i++){
  //               $records = $this->db->pquery($sql, array($year, $i));
  //               $total = $this->db->query_result($records,0,'total');
  //               $result[] = $total;
  //             }
  //           }



  //   return $result;
  // }

  public function thisYearMonthsNum($year)
  {
    $sql = "SELECT months,month_num FROM app_opex_report WHERE years = ?";  
    $records = $this->db->pquery($sql, array($year));
    $num_rows =  $this->db->num_rows($records);
    return $num_rows;
  }

  
  public function thisYearMonths($year)
  {
    $sql = "SELECT months,month_num FROM app_opex_report WHERE years = ? ORDER BY month_num ASC"; 
    $records = $this->db->pquery($sql, array($year));

    return $records ;
  }

  public function costList($year,$column,$cost,$type)
  {

    if(is_array($cost)){
      $cost = implode(",",$cost);
    }

    if(!empty($cost)){
      $year_months_num = $this->thisYearMonthsNum($year);
      $sql = "SELECT service_key, ROUND(SUM($column),2) AS total_with_vat
                        FROM app_opex_details de
                        LEFT JOIN app_opex_report re ON re.id=de.did
                        WHERE re.years = ? AND service_key IN ($cost) AND cost_center = ? AND month_num = ?
                        GROUP BY month_num
                        ORDER BY did ASC";                               

      $result = array();
      for($i = 1; $i < $year_months_num+1; $i++){
        $records = $this->db->pquery($sql, array($year, $type, $i));
        $total = $this->db->query_result($records,0,'total_with_vat');     
        $result[] = $total;
      }
    }else{
      $result = [];
    }

    return $result;
  }



  public function costListInvoice($year,$column,$cost)
  {
      $costs = $cost['cost'];         
      $cost_center = $this->getCostNamesFromId($cost);

      $cost_centers_name = implode(",",  $cost_center['name']);


      $year_months_num = $this->thisYearMonthsNum($year);
      $sql = "SELECT service_key, REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat
                        FROM app_opex_invoice_details de
                        LEFT JOIN app_opex_report re ON re.id=de.did
                        WHERE re.years = ? AND service_key IN ($costs) AND cost_center IN ($cost_centers_name)  AND month_num = ?
                        GROUP BY month_num
                        ORDER BY did ASC";

    if(!empty($costs)){  
      $result = array();
      $this->db->pquery("SET SESSION group_concat_max_len = 1000000", array());
      for($i = 1; $i < $year_months_num+1; $i++){
        $records = $this->db->pquery($sql, array($year, $i));
        $total = $this->db->query_result($records,0,'total_with_vat');
        $result[] = $total;
      }
    }else{
      for($i = 1; $i < $year_months_num+1; $i++){
        $result[] = '';
      }    
    }

    return $result;
  }

  public function invoiceProfitWithOutList($year,$column)
  {
    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat
                  FROM app_opex_invoice_other_details de   
                  LEFT JOIN app_opex_report re ON re.id=de.did              
                  WHERE `years` = ? AND month_num = ?
                  GROUP BY `month_num`
                  ORDER BY invoiceid ASC";
                                  

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total = $this->db->query_result($records,0,'total_with_vat');
      $result[] = $total;
    }

    return $result;
  }



  public function otherCostList($year,$column)
  {
    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat  
                      FROM app_opex_other_details de
                      LEFT JOIN app_opex_report re ON re.id=de.did
                      WHERE re.years = ? AND  month_num = ?
                      GROUP BY month_num
                      ORDER BY did ASC";
                                  

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total = $this->db->query_result($records,0,'total_with_vat');   
      $result[] = $total;
    }

    return $result;
  }


  public function totalTransportOrders($year)
  {

    $year_months_num = $this->thisYearMonthsNum($year);   

    $sql = "SELECT total_orders, total_parcel_orders, total_orders_pll_places, REPLACE(total_weight,',','.') AS total_weight 
                  FROM app_opex_transport_orders tro
                  LEFT JOIN app_opex_report re ON re.id=tro.tid	 	
                  WHERE re.years = ? AND month_num = ?";  

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total_orders = $this->db->query_result($records,0,'total_orders');
      $total_parcel_orders = $this->db->query_result($records,0,'total_parcel_orders');
      $total_orders_pll_places = $this->db->query_result($records,0,'total_orders_pll_places');
      $total_weight = $this->db->query_result($records,0,'total_weight');
      $result[] = array( 
        'total_orders' => $total_orders,
        'total_parcel_orders' => $total_parcel_orders,
        'total_orders_pll_places' => $total_orders_pll_places,
        'total_weight' => $total_weight
      );
    }

    return $result;
  }

  public function storageService($year,$cost)
  {  
    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT ROUND(SUM(ss.listprice),2) AS listprice
                      FROM app_storage_service ss
                      LEFT JOIN app_opex_report re ON re.id=ss.sid                    
                      WHERE re.years = ? AND ss.cost_center = ?   AND month_num = ?
                      GROUP BY month_num
                      ORDER BY ss.sid ASC";
                      
    $sql2 = "SELECT ROUND(SUM(sis.listprice),2) AS listprice
                      FROM app_storage_independent_service sis
                      LEFT JOIN app_opex_report re ON re.id=sis.sid
                      WHERE re.years = ? AND sis.cost_center = ?   AND month_num = ?
                      GROUP BY month_num
                      ORDER BY sis.sid ASC";  
    
  
                      

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $cost,  $i));
      $records2 = $this->db->pquery($sql2, array($year, $cost,  $i));
      $total = $this->db->query_result($records,0,'listprice');   
      $total2 = $this->db->query_result($records2,0,'listprice');


      $result[] = $total+$total2;
    }

    return $result;
  }

  public function stevedoringService($year,$cost)
  {
    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT  ROUND(SUM(listprice),2) AS listprice
                      FROM app_stevedoring_service ss
                      LEFT JOIN app_opex_report re ON re.id=ss.sid
                      WHERE re.years = ? AND cost_center = ?  AND month_num = ?
                      GROUP BY month_num
                      ORDER BY sid ASC";    
                            

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $cost, $i));
      $total = $this->db->query_result($records,0,'listprice');     
      $result[] = $total;
    }

    return $result;  
  }


  public function movementServicesWithOtherServices($year)
  {
    $year_months_num = $this->thisYearMonthsNum($year);

    $sql = "SELECT REPLACE(ROUND(SUM(total_parcel_sum),2),',','.') AS total_with_vat,REPLACE(ROUND(SUM(total_cancel_orders),2),',','.') AS total_cancel_orders,total_movement_storage_profit,total_pack_material_profit
                      FROM app_opex_transport_orders ot
                      LEFT JOIN app_opex_report re ON re.id=ot.tid
                      WHERE re.years = ? AND month_num = ?
                      GROUP BY month_num";    

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total = $this->db->query_result($records,0,'total_with_vat');
      $total2 = $this->db->query_result($records,0,'total_cancel_orders');
      $total3 = $this->db->query_result($records,0,'total_movement_storage_profit');
      $total4 = $this->db->query_result($records,0,'total_pack_material_profit');
  
      $result[$i][] = $total;
      $result[$i][] = $total2;
      $result[$i][] = $total3;          
      $result[$i][] = $total4;          

      $total_sum[] = array_sum($result[$i]);
    }

    return $total_sum;
  }

  public function totalProfitMovement($year,$type)
  {
    $year_months_num = $this->thisYearMonthsNum($year);

    $sql = "SELECT REPLACE(ROUND(CASE WHEN inv.region_id = 0 
    THEN   (SUM( IF(productid = 121391, listprice*quantity,listprice))/100 * 21)  + SUM(IF(productid = 121391, listprice*quantity,listprice))
    ELSE   (SUM(IF(productid = 121391, listprice*quantity,listprice))/100 * vtiger_taxregions.value) +SUM(IF(productid = 121391, listprice*quantity,listprice))
    END,2),',','.') as total
        FROM vtiger_invoice inv                  
        JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid 
        JOIN vtiger_inventoryproductrel  ON vtiger_inventoryproductrel.id=inv.invoiceid
        JOIN vtiger_invoicecf i ON i.invoiceid=inv.invoiceid
        LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`inv`.`region_id` 
        WHERE deleted = 0 AND setype = 'Invoice' AND costcenter = 103917 AND DATE_FORMAT(invoicedate,'%Y') = ?";




           if($type == 'All'){         
                $records = $this->db->pquery($sql, array($year));
                $total = $this->db->query_result($records,0,'total');
                $result = $total;

           }else{
            $result = array();
            $sql .=" AND  DATE_FORMAT(invoicedate,'%m') = ? ";
              for($i = 1; $i < $year_months_num+1; $i++){
                $records = $this->db->pquery($sql, array($year, $i));
                $total = $this->db->query_result($records,0,'total');
                $result[] = $total;
              }
            }



    return $result;
  }

  public function getCostNamesFromId($cost){
    $costs = $cost['cost'];
    $cost_center = explode(",",$cost['cost_center']);

    $cost_center_arr = array();
    $cost_center_name_arr = array();


    if(in_array(103921, $cost_center)){
      $cost_center_arr[] = 103921;
      $cost_center_name_arr[] = "'ADMINISTRACIJA'";
    }

    if(in_array(103920, $cost_center)){
      $cost_center_arr[] = 103920;
      $cost_center_name_arr[] = "'KAUNO SANDĖLIS'";
    }

    if(in_array(103919, $cost_center)){
      $cost_center_arr[] = 103919;
      $cost_center_name_arr[] = "'KLAIPĖDOS SANDĖLIS'";
    }

    if(in_array(103918, $cost_center)){
      $cost_center_arr[] = 103918;
      $cost_center_name_arr[] = "'VILNIAUS SANDĖLIS'";
    }

    if(in_array(103917, $cost_center)){
      $cost_center_arr[] = 103917;
      $cost_center_name_arr[] = "'PERKRAUSTYMAS'";
    }

    if(in_array(103916, $cost_center)){
      $cost_center_arr[] = 103916;
      $cost_center_name_arr[] = "'SERVISAS'";
    }

    if(in_array(103915, $cost_center)){
      $cost_center_arr[] = 103915;
      $cost_center_name_arr[] = "'EKSPEDIJAVIMAS'";
    }

    if(in_array(103914, $cost_center)){
      $cost_center_arr[] = 103914;
      $cost_center_name_arr[] = "'TRANSPORTAS'";
    }

    return array('id' => $cost_center_arr, 'name' => $cost_center_name_arr);
  }

  public function getHandWritedSums($year){
    $sql = "SELECT title, REPLACE(total, ',', '.') as total,month FROM app_opex_hand_writed_rows WHERE year = ? ORDER BY month";
    $records = $this->db->pquery($sql, array($year)); 
    $sums_array = [];

    foreach ($records as $row) {
      $sums_array[$row['title']][$row['month']] = $row['total']; 
    }
    
    return $sums_array;
  }


}