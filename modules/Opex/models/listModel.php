<?php
namespace modules\Opex\models;

class listModel  {

  public function __construct()
  {
    global $adb;
    $this->db = $adb;
  }

  public function lastUpdate(){
    $sql = "SELECT last_update FROM app_opex_report ORDER BY last_update DESC LIMIT 1";
    $records = $this->db->pquery($sql, array());
    $last_update = $this->db->query_result($records,0,'last_update');
    return $last_update;
  }

  public function costListNumber($cost_name)
  {
    $sql = "SELECT cost_center,cost FROM app_opex_cost_list WHERE `name` = ?";
    $records = $this->db->pquery($sql, array($cost_name));
    $cost_center = $this->db->query_result($records,0,'cost_center');
    $cost = $this->db->query_result($records,0,'cost');
    $result = array('cost_center' => $cost_center,'cost' => $cost, 'cost_name' => "'$cost_name'");
    return $result;
  }

  public function costListInvoiceNumber($cost_name)
  {
    $sql = "SELECT cost_center,cost FROM app_opex_invoice_cost_list WHERE `name` = ?";
    $records = $this->db->pquery($sql, array($cost_name));
    $cost_center = $this->db->query_result($records,0,'cost_center');
    $cost = $this->db->query_result($records,0,'cost');
    return array('cost_center' => $cost_center,'cost' => $cost) ;
  }

  public function getCostNameById($cost_center){
    if($cost_center == 103921) $cost_name = 'ADMINISTRACIJA';                                    
    if($cost_center == 103920) $cost_name = 'KAUNO SANDĖLIS';                                    
    if($cost_center == 103919) $cost_name = 'KLAIPĖDOS SANDĖLIS';                                                                      
    if($cost_center == 103918) $cost_name = 'VILNIAUS SANDĖLIS';                                    
    if($cost_center == 103917) $cost_name = 'PERKRAUSTYMAS';                                    
    if($cost_center == 103916) $cost_name = 'SERVISAS';                                    
    if($cost_center == 103915) $cost_name = 'EKSPEDIJAVIMAS';                                    
    if($cost_center == 103914) $cost_name = 'TRANSPORTAS';  
    return $cost_name;
  }
  

  public function getCostNamesFromId($cost){
    $costs = $cost['cost'];
    $cost_center = explode(",",$cost['cost_center']);

    $cost_center_arr = array();
    $cost_center_name_arr = array();


    if(in_array(103921, $cost_center)){
      $cost_center_arr[] = 103921;
      $cost_center_name_arr[] = "'ADMINISTRACIJA'";
    }

    if(in_array(103920, $cost_center)){
      $cost_center_arr[] = 103920;
      $cost_center_name_arr[] = "'KAUNO SANDĖLIS'";
    }

    if(in_array(103919, $cost_center)){
      $cost_center_arr[] = 103919;
      $cost_center_name_arr[] = "'KLAIPĖDOS SANDĖLIS'";
    }

    if(in_array(103918, $cost_center)){
      $cost_center_arr[] = 103918;
      $cost_center_name_arr[] = "'VILNIAUS SANDĖLIS'";
    }

    if(in_array(103917, $cost_center)){
      $cost_center_arr[] = 103917;
      $cost_center_name_arr[] = "'PERKRAUSTYMAS'";
    }

    if(in_array(103916, $cost_center)){
      $cost_center_arr[] = 103916;
      $cost_center_name_arr[] = "'SERVISAS'";
    }

    if(in_array(103915, $cost_center)){
      $cost_center_arr[] = 103915;
      $cost_center_name_arr[] = "'EKSPEDIJAVIMAS'";
    }

    if(in_array(103914, $cost_center)){
      $cost_center_arr[] = 103914;
      $cost_center_name_arr[] = "'TRANSPORTAS'";
    }

    return array('id' => $cost_center_arr, 'name' => $cost_center_name_arr);
  }

  public function totalProfit($year,$vat,$type)
  {
    $year_months_num = $this->thisYearMonthsNum($year);

    $column = ($vat == 1 ? 'inv.total' : 'inv.subtotal');

    $sql = "SELECT REPLACE(ROUND(SUM($column),2),',','.') as total
              FROM vtiger_invoice inv                  
              JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid 
              JOIN vtiger_invoicecf i ON i.invoiceid=inv.invoiceid
              WHERE deleted = 0 AND setype = 'Invoice'  AND cf_1486 = 'Transportas' AND DATE_FORMAT(invoicedate,'%Y') = ?";
          
          if($type == 'All'){         
                $records = $this->db->pquery($sql, array($year));               
                $total = $this->db->query_result($records,0,'total');               
                $result = $total;

           }else{
            $result = array();
            $sql .=" AND  DATE_FORMAT(invoicedate,'%m') = ? ";            
              for($i = 1; $i < $year_months_num+1; $i++){
                $records = $this->db->pquery($sql, array($year, $i));               
                $total = $this->db->query_result($records,0,'total');         
                $result[] = $total;
              }
            }



    return $result;
  }

  public function totalProfitMovement($year,$type)
  {
    $year_months_num = $this->thisYearMonthsNum($year);

    $sql = "SELECT REPLACE(ROUND(CASE WHEN inv.region_id = 0 
                    THEN   (SUM( IF(productid = 121391, listprice*quantity,listprice))/100 * 21)  + SUM(IF(productid = 121391, listprice*quantity,listprice))
                    ELSE   (SUM(IF(productid = 121391, listprice*quantity,listprice))/100 * vtiger_taxregions.value) +SUM(IF(productid = 121391, listprice*quantity,listprice))
                    END,2),',','.') as total
              FROM vtiger_invoice inv                  
              JOIN vtiger_crmentity e ON e.crmid=inv.invoiceid 
              JOIN vtiger_inventoryproductrel  ON vtiger_inventoryproductrel.id=inv.invoiceid
              JOIN vtiger_invoicecf i ON i.invoiceid=inv.invoiceid
              LEFT JOIN `vtiger_taxregions` ON `vtiger_taxregions`.`regionid`=`inv`.`region_id` 
              WHERE deleted = 0 AND setype = 'Invoice' AND costcenter = 103917 AND DATE_FORMAT(invoicedate,'%Y') = ?";


           if($type == 'All'){         
                $records = $this->db->pquery($sql, array($year));
                $total = $this->db->query_result($records,0,'total');
                $result = $total;

           }else{
            $result = array();
            $sql .=" AND  DATE_FORMAT(invoicedate,'%m') = ? ";
              for($i = 1; $i < $year_months_num+1; $i++){
                $records = $this->db->pquery($sql, array($year, $i));
                $total = $this->db->query_result($records,0,'total');
                $result[] = $total;
              }
            }



    return $result;
  }


  public function thisYearMonths($year)
  {
    $sql = "SELECT months,month_num FROM app_opex_report WHERE years = ? ORDER BY month_num ASC"; 
    $records = $this->db->pquery($sql, array($year));

    return $records ;
  }

  public function thisYearMonthsNum($year)
  {
    $sql = "SELECT months,month_num FROM app_opex_report WHERE years = ?";  
    $records = $this->db->pquery($sql, array($year));
    $num_rows =  $this->db->num_rows($records);
    return $num_rows;
  }

  public function costList($year,$column,$cost)
  {
    $costs = $cost['cost'];
    $cost_center = $cost['cost_center'];

    if($cost['cost_name'] == "'other_klaipeda'"){
      $cost_name = 'KLAIPĖDOS SANDĖLIS2';
    }else{
      $cost_name = $this->getCostNameById($cost_center);      
    }

    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT service_key, REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat, ROUND(SUM($column),2) AS total_with_vat2, GROUP_CONCAT(DISTINCT purchaseorderid) AS purchaseorderid
                      FROM app_opex_details de
                      LEFT JOIN app_opex_report re ON re.id=de.did
                      WHERE re.years = ? AND service_key IN ($costs) AND cost_center = ? AND month_num = ?
                      GROUP BY month_num
                      ORDER BY did ASC";       

     $result = array();
    if(!empty($costs)){     
      $this->db->pquery("SET SESSION group_concat_max_len = 1000000", array());
      for($i = 1; $i < $year_months_num+1; $i++){
        $records = $this->db->pquery($sql, array($year, $cost_name, $i));
        $total = $this->db->query_result($records,0,'total_with_vat');
        $total2 = $this->db->query_result($records,0,'total_with_vat2');
        $purchaseorderid = $this->db->query_result($records,0,'purchaseorderid');
        $result[] = array('sum' => $total,'sum2' => $total2, 'purchaseorderid' => $purchaseorderid, 'service_key' => $costs, 'type' => $cost_center);
      }     
    }else{
      for($i = 1; $i < $year_months_num+1; $i++){
        $result[] = array('sum' => '');
      }
    }
    return $result;
  }

  public function costListSum($year,$cost)
  {

    $costs = $cost['cost'];
    $cost_center = $cost['cost_center'];

    if($cost['cost_name'] == "'other_klaipeda'"){
      $cost_name = 'KLAIPĖDOS SANDĖLIS2';
    }else{
      $cost_name = $this->getCostNameById($cost_center);      
    }

    $sql = "SELECT REPLACE(ROUND(SUM(total_with_vat),2),',','.') AS total_with_vat 
                      FROM app_opex_details de
                      LEFT JOIN app_opex_report re ON re.id=de.did
                      WHERE re.years = ? AND service_key IN ($costs) AND cost_center = ?
                      ORDER BY month_num";  
    if(!empty($costs)){
      $records = $this->db->pquery($sql, array($year,$cost_name));
      $total = $this->db->query_result($records,0,'total_with_vat');
    }else{
      $total = '';
    }
    return $total;
  }


  public function costListInvoice($year,$column,$cost)
  {
      $costs = $cost['cost'];         
      $cost_center = $this->getCostNamesFromId($cost);

      $costs = ltrim($costs,',');
      $costs = rtrim($costs,',');

      $cost_centers = implode(",",  $cost_center['id']);
      $cost_centers_name = implode(",",  $cost_center['name']);

      $year_months_num = $this->thisYearMonthsNum($year);
      $sql = "SELECT service_key, REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat,GROUP_CONCAT(DISTINCT invoiceid) AS invoiceid, ROUND(SUM($column),2) as total_with_vat_point
                        FROM app_opex_invoice_details de
                        LEFT JOIN app_opex_report re ON re.id=de.did
                        WHERE re.years = ? AND service_key IN ($costs) AND cost_center IN ($cost_centers_name)  AND month_num = ?
                        GROUP BY month_num
                        ORDER BY did ASC";

    if(!empty($costs)){  
      $result = array();
      $this->db->pquery("SET SESSION group_concat_max_len = 1000000", array());
      for($i = 1; $i < $year_months_num+1; $i++){
        $records = $this->db->pquery($sql, array($year, $i));
        $total = $this->db->query_result($records,0,'total_with_vat');
        $total2 = $this->db->query_result($records,0,'total_with_vat_point');
        $invoiceid = rtrim($this->db->query_result($records,0,'invoiceid'),',');
        $invoiceid = ltrim($invoiceid,',');
        $result[] = array('sum' => $total, 'sum2' => $total2, 'invoiceid' => $invoiceid, 'service_key' => $costs, 'type' => $cost_centers);
      }
    }else{
      for($i = 1; $i < $year_months_num+1; $i++){
        $result[] = array('sum' => '');
      }    
    }

    return $result;
  }


  public function otherCostList($year,$column)
  {
    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT did AS month_id, REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat
                      FROM app_opex_other_details de
                      LEFT JOIN app_opex_report re ON re.id=de.did
                      WHERE re.years = ? AND  month_num = ?
                      GROUP BY month_num
                      ORDER BY did ASC";
                                  

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total = $this->db->query_result($records,0,'total_with_vat');
      $month_id = $this->db->query_result($records,0,'month_id');
      $result[] = array('sum' => $total, 'month' => $month_id);
    }

    return $result;
  }

  public function otherCostListSum($year,$column)
  {
    $sql = "SELECT REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat 
                      FROM app_opex_other_details de
                      LEFT JOIN app_opex_report re ON re.id=de.did
                      WHERE re.years = ?
                      ORDER BY month_num";  

    $records = $this->db->pquery($sql, array($year));
    $total = $this->db->query_result($records,0,'total_with_vat');
    return $total;
  }

  public function invoiceProfitWithOutList($year,$column)
  {
    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat
                  FROM app_opex_invoice_other_details de   
                  LEFT JOIN app_opex_report re ON re.id=de.did              
                  WHERE `years` = ? AND month_num = ?
                  GROUP BY `month_num`
                  ORDER BY invoiceid ASC";
                                  

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total = $this->db->query_result($records,0,'total_with_vat');
      $result[] = array('sum' => $total, 'month' => $i);
    }

    return $result;
  }


  public function invoiceProfitWithOutListSum($year,$column)
  {
    $sql = "SELECT REPLACE(ROUND(SUM($column),2),',','.') AS total_with_vat 
                       FROM app_opex_invoice_other_details de   
                      LEFT JOIN app_opex_report re ON re.id=de.did              
                      WHERE `years` = ?                    
                      ORDER BY `month_num`";  

    $records = $this->db->pquery($sql, array($year));
    $total = $this->db->query_result($records,0,'total_with_vat');
    return $total;
  }


  public function totalTransportOrders($year)
  {

    $year_months_num = $this->thisYearMonthsNum($year);   

    $sql = "SELECT total_orders, total_parcel_orders, total_orders_pll_places, REPLACE(total_weight,',','.') AS total_weight 
                  FROM app_opex_transport_orders tro
                  LEFT JOIN app_opex_report re ON re.id=tro.tid	 	
                  WHERE re.years = ? AND month_num = ?";  

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total_orders = $this->db->query_result($records,0,'total_orders');
      $total_parcel_orders = $this->db->query_result($records,0,'total_parcel_orders');
      $total_orders_pll_places = $this->db->query_result($records,0,'total_orders_pll_places');
      $total_weight = $this->db->query_result($records,0,'total_weight');
      $result[] = array( 
        'total_orders' => $total_orders,
        'total_parcel_orders' => $total_parcel_orders,
        'total_orders_pll_places' => $total_orders_pll_places,
        'total_weight' => $total_weight
      );
    }

    return $result;
  }




  public function sumMonthProfit($year,$not_in)
  {
    if(is_array($not_in)){
      $not_in = implode(",",$not_in);
    }

    $type = 'TRANSPORTAS';

    $year_months_num = $this->thisYearMonthsNum($year);  

    $sql = "SELECT ROUND(SUM(total_with_vat),2) AS total
              FROM app_opex_details od
              LEFT JOIN app_opex_report re ON re.id= od.did	 	
              WHERE re.years =  ? AND month_num = ? AND cost_center = ? ";
    if(!empty($not_in))
    $sql .= " AND service_key NOT IN ($not_in) ";

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){             
      $records = $this->db->pquery($sql, array($year, $i, $type));
      $total = $this->db->query_result($records,0,'total');
      $result[] = $total;
    }

    return $result;
  }

  public function movementServicesWithOtherServices($year)
  {
    $year_months_num = $this->thisYearMonthsNum($year);

    $sql = "SELECT REPLACE(ROUND(SUM(total_parcel_sum),2),',','.') AS total_with_vat,REPLACE(ROUND(SUM(total_cancel_orders),2),',','.') AS total_cancel_orders,total_movement_storage_profit,total_pack_material_profit
                      FROM app_opex_transport_orders ot
                      LEFT JOIN app_opex_report re ON re.id=ot.tid
                      WHERE re.years = ? AND month_num = ?
                      GROUP BY month_num";    

    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $total = $this->db->query_result($records,0,'total_with_vat');
      $total2 = $this->db->query_result($records,0,'total_cancel_orders');
      $total3 = $this->db->query_result($records,0,'total_movement_storage_profit');
      $total4 = $this->db->query_result($records,0,'total_pack_material_profit');
  
      $result[$i][] = $total;
      $result[$i][] = $total2;
      $result[$i][] = $total3;          
      $result[$i][] = $total4;          

      $total_sum[] = array_sum($result[$i]);
    }

    return $total_sum;
  }



  public function movementServicesWithOtherServicesDirectVersion($year)
  {
    $year_months_num = $this->thisYearMonthsNum($year);
                      
    $sql = "SELECT ROUND(SUM(s.total),2) AS total
                      FROM vtiger_salesorder s
                      LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                      LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
                      WHERE e.deleted =0 AND setype = 'SalesOrder' AND scf.cf_855 = 'Perkraustymo užsakymas' AND DATE_FORMAT(e.createdtime, '%Y') = ? AND DATE_FORMAT(e.createdtime, '%m') = ?"; 
                      
    $sql2 = "SELECT ROUND(SUM(s.total),2) AS total_cancel_orders
                      FROM vtiger_salesorder s
                      LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid
                      LEFT JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
                      WHERE e.deleted =0 AND setype = 'SalesOrder' AND scf.cf_1614 = 1 AND DATE_FORMAT(e.createdtime, '%Y') = ? AND DATE_FORMAT(e.createdtime, '%m') = ?";   
                      
    $sql3 = "SELECT ROUND(SUM(listprice),2) AS total_movement_storage_profit
                      FROM vtiger_inventoryproductrel i
                      LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
                      LEFT JOIN vtiger_crmentity e ON e.crmid=i.id                                 
                      WHERE e.deleted = 0 AND e.setype = 'Invoice' AND cargo_wgt = 'Perkraustymo sandėliavimo pajamos' AND DATE_FORMAT(e.createdtime, '%Y') = ? AND DATE_FORMAT(e.createdtime, '%m') = ?"; 
                      
    $sql4 = "SELECT ROUND(SUM(listprice),2) AS total_pack_material_profit 
                      FROM vtiger_inventoryproductrel i
                      LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
                      LEFT JOIN vtiger_crmentity e ON e.crmid=i.id                                  
                      WHERE e.deleted = 0 AND e.setype = 'Invoice' AND cargo_wgt = 'Pakavimo medžiagų pardavimas' AND DATE_FORMAT(e.createdtime, '%Y') = ? AND DATE_FORMAT(e.createdtime, '%m') = ?";                  



    $result = array();
    for($i = 1; $i < $year_months_num+1; $i++){
      $records = $this->db->pquery($sql, array($year, $i));
      $records2 = $this->db->pquery($sql2, array($year, $i));
      $records3 = $this->db->pquery($sql3, array($year, $i));
      $records4 = $this->db->pquery($sql4, array($year, $i));

      $total = $this->db->query_result($records,0,'total');
      $total2 = $this->db->query_result($records2,0,'total_cancel_orders');
      $total3 = $this->db->query_result($records3,0,'total_movement_storage_profit');
      $total4 = $this->db->query_result($records4,0,'total_pack_material_profit');

      $result[$i][] = $total;       
      $result[$i][] = $total2;       
      $result[$i][] = $total3;       
      $result[$i][] = $total4;       

      $total_sum[] = array_sum($result[$i]);
    }



    return $total_sum;
  }



  public function sumAllPlacesAndServicesCost($year)
  {
    $year_months_num = $this->thisYearMonthsNum($year);

    $sql = "SELECT ROUND(SUM(total_with_vat),2) AS total_with_vat
                    FROM app_opex_details de
                    LEFT JOIN app_opex_report re ON re.id=de.did
                    WHERE re.years = ? AND service_key IN (103979,103959,103980,103953,103957,107804,103981,103952) AND cost_center = 'SERVISAS' AND month_num = ?
                    GROUP BY month_num";

    $sql2 = "SELECT ROUND(SUM(total_with_vat),2) AS total_with_vat
                    FROM app_opex_details de
                    LEFT JOIN app_opex_report re ON re.id=de.did
                    WHERE re.years = ? AND service_key IN (103979,103980,103972,114452,103959,110428,103981,108887) AND cost_center = 'VILNIAUS SANDĖLIS' AND month_num = ?
                    GROUP BY month_num"; 
                      
    $sql3 = "SELECT ROUND(SUM(total_with_vat),2) AS total_with_vat
                    FROM app_opex_details de
                    LEFT JOIN app_opex_report re ON re.id=de.did
                    WHERE re.years = ? AND service_key IN (103979,103952,103972,103959,108887,103981,103980) AND cost_center = 'KLAIPĖDOS SANDĖLIS' AND month_num = ?
                    GROUP BY month_num";
    
    $sql4 = "SELECT ROUND(SUM(total_with_vat),2) AS total_with_vat
                    FROM app_opex_details de
                    LEFT JOIN app_opex_report re ON re.id=de.did
                    WHERE re.years = ? AND service_key IN (103979,103980,114452,103952,103981,108887) AND cost_center = 'KAUNO SANDĖLIS' AND month_num = ?
                    GROUP BY month_num";

    $sql5 = "SELECT ROUND(SUM(total_with_vat),2) AS total_with_vat
                    FROM app_opex_details de
                    LEFT JOIN app_opex_report re ON re.id=de.did
                    WHERE re.years = ? AND service_key IN (103979,103955,103981,103980,103952) AND cost_center = 'ADMINISTRACIJA' AND month_num = ?
                    GROUP BY month_num";


      $result = array();
      $total_sum = array();
      for($i = 1; $i < $year_months_num+1; $i++){
        $records = $this->db->pquery($sql, array($year, $i));
        $records2 = $this->db->pquery($sql2, array($year, $i));
        $records3 = $this->db->pquery($sql3, array($year, $i));
        $records4 = $this->db->pquery($sql4, array($year, $i));
        $records5 = $this->db->pquery($sql5, array($year, $i));
        $total = $this->db->query_result($records,0,'total_with_vat');
        $total2 = $this->db->query_result($records2,0,'total_with_vat');
        $total3 = $this->db->query_result($records3,0,'total_with_vat');
        $total4 = $this->db->query_result($records4,0,'total_with_vat');
        $total5 = $this->db->query_result($records5,0,'total_with_vat');

        $result[$i][] = $total;
        $result[$i][] = $total2;
        $result[$i][] = $total3;
        $result[$i][] = $total4;
        $result[$i][] = $total5;

        $total_sum[] = array_sum($result[$i]);

      }

      return $total_sum;

  }

  public function sumReal($year,$column) 
  {
    $year_months_num = $this->thisYearMonthsNum($year);
    $sql = "SELECT ROUND(SUM($column),2) AS total
              FROM app_opex_details od
              LEFT JOIN app_opex_report re ON re.id= od.did	 	
              WHERE re.years = ? AND month_num = ? AND cost_center IN ('TRANSPORTAS','VILNIAUS SANDĖLIS','KLAIPĖDOS SANDĖLIS','KAUNO SANDĖLIS','ADMINISTRACIJA')  ";

    $sql2 = "SELECT ROUND(SUM($column),2) AS total_with_vat
              FROM app_opex_details de
              LEFT JOIN app_opex_report re ON re.id=de.did
              WHERE re.years = ? AND service_key IN (103979,103959,103980,103953,103957,107804,103981,103952) AND cost_center = 'SERVISAS' AND month_num = ?
              GROUP BY month_num";

    $sql3 = "SELECT ROUND(SUM($column),2) AS total_with_vat
              FROM app_opex_details de
              LEFT JOIN app_opex_report re ON re.id=de.did
              WHERE re.years = ? AND service_key IN (110428,103956) AND cost_center = 'PERKRAUSTYMAS' AND month_num = ?
              GROUP BY month_num";
     


      $result = array();
      for($i = 1; $i < $year_months_num+1; $i++){
        $records = $this->db->pquery($sql, array($year, $i));
        $records2 = $this->db->pquery($sql2, array($year, $i));
        $records3 = $this->db->pquery($sql3, array($year, $i));           
       
        $total = $this->db->query_result($records,0,'total');
        $total2 = $this->db->query_result($records2,0,'total_with_vat');
        $total3 = $this->db->query_result($records3,0,'total_with_vat');    

        $result[$i][] = $total;
        $result[$i][] = $total2;
        $result[$i][] = $total3;          

        $total_sum[] = array_sum($result[$i]);
      }

      return $total_sum;

  }


  public function costCenters()
  {
    $sql = "SELECT * FROM vtiger_costcenter co
            LEFT JOIN vtiger_crmentity e ON e.crmid=co.costcenterid
    WHERE deleted = 0";  

    $records = $this->db->pquery($sql, array());
    return $records ;
  }


  public function saveCost($post)
  {
    $date = date("Y-m-d H:i:s");
    $sql = "INSERT INTO app_opex_cost_center_list (name,cost_center, cost, last_update) VALUES (?,?,?,?)";
    $this->db->pquery($sql, array($post['name'],$post['cost_center'],$post['cost'],$date));    

    header("Location: index.php?module=Opex&view=Edit");
  }

  public function getDetalization()
  {

    $sql = "SELECT id,name, costcenter_tks_cost as costcenter, cost 
                      FROM app_opex_cost_list li
                      LEFT JOIN vtiger_costcenter co ON co.costcenterid=li.cost_center";

    $records = $this->db->pquery($sql, array());

    $result = array();

    foreach ($records as $row) {
      $cost = $row['cost'];
      $sql2 = "SELECT itemservice_tks_name AS cost, itemservice_tks_accountingcode as accountingcode FROM vtiger_itemservice WHERE itemserviceid IN ($cost)";
      $records2 = $this->db->pquery($sql2, array());  
      $result[$row['name']]['costcenter'] =$row['costcenter'];
      foreach($records2 as $res){
        $result[$row['name']]['cost'][] = "/".$res['accountingcode']."/ ".$res['cost'];
      }
    }

    return $result;
  }

  public function getInvoiceDetalization()
  {

    $sql = "SELECT id,name, costcenter_tks_cost as costcenter, cost
                      FROM app_opex_invoice_cost_list li
                      LEFT JOIN vtiger_costcenter co ON co.costcenterid=li.cost_center";

    $records = $this->db->pquery($sql, array());

    $result = array();

    foreach ($records as $row) {
      $cost = $row['cost'];
      $sql2 = "SELECT itemservice_tks_name AS cost, itemservice_tks_accountingcode as accountingcode FROM vtiger_itemservice WHERE itemserviceid IN ($cost)";
      $sql3 = "SELECT name AS cost FROM app_services_type s WHERE id IN ($cost)";
      $records2 = $this->db->pquery($sql2, array());  
      $records3 = $this->db->pquery($sql3, array());  
      $result[$row['name']]['costcenter'] = $row['costcenter'];
      foreach($records2 as $res){
        $result[$row['name']]['cost'][] = "/".$res['accountingcode']."/ ".$res['cost'];
      }

      foreach($records3 as $res){
        $result[$row['name']]['cost'][] = $res['cost'];
      }
    }

    return $result;
  }




  public function getPurchaseDetalizationByVendor($year,$vat,$cost)
  {  
      $costs = $cost['cost']; 
      if(!empty($costs)){     
      $cost_center = $cost['cost_center'];
      
      // $cost_name = $this->getCostNameById($cost_center);     
      
      if($cost['cost_name'] == "'other_klaipeda'"){
        $cost_name = 'KLAIPĖDOS SANDĖLIS2';
      }else{
        $cost_name = $this->getCostNameById($cost_center);      
      }
      


      $year_months_num = $this->thisYearMonthsNum($year);
      $sql = "SELECT GROUP_CONCAT(DISTINCT purchaseorderid) AS purchaseorderid 
                FROM app_opex_details  de
                LEFT JOIN app_opex_report re ON re.id=de.did
                WHERE re.years = ? AND service_key IN ($costs) AND cost_center = ? AND  month_num = ?";   
                
      $sql3 = "SELECT GROUP_CONCAT(DISTINCT purchaseorderid) AS purchaseorderid 
                FROM app_opex_details  de
                LEFT JOIN app_opex_report re ON re.id=de.did
                WHERE re.years = ? AND service_key IN ($costs) AND cost_center = ?"; 

      $records3 = $this->db->pquery($sql3, array($year, $cost_name)); 
      $purchaseorderid = $this->db->query_result($records3,0,'purchaseorderid');      
      
      $sql4 = "SELECT vendorname
                    FROM vtiger_purchaseorder p                    
                    LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid 
                    WHERE p.purchaseorderid IN ($purchaseorderid)
                    GROUP BY vendorname";

      $records4 = $this->db->pquery($sql4, array());   

      $vendors = array();
      $total = array();
      $total_array = array();

      foreach($records4 as $key => $row){           
          $vendors[$row['vendorname']] = $row['vendorname'];              
      } 

      if($vat == 1){
        $total_sum_column = "REPLACE(ROUND(SUM(((((i.listprice*quantity) * IF(vtiger_taxregions.value > 0, value, 21)) /100) + i.listprice *quantity)),2),',','.')";
      }else{
        $total_sum_column = "ROUND(SUM(i.listprice*quantity))";
      }

      $this->db->pquery("SET SESSION group_concat_max_len = 1000000", array());
        for($i = 1; $i < $year_months_num+1; $i++){
          $records = $this->db->pquery($sql, array($year, $cost_name, $i));
          $purchaseorderid = $this->db->query_result($records,0,'purchaseorderid');

          $sql2 = "SELECT $total_sum_column AS total, v.vendorname
                      FROM vtiger_inventoryproductrel i
                      LEFT JOIN vtiger_purchaseorder p ON p.purchaseorderid=i.id
                      LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid
                      LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
                      LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt
                      LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=p.region_id)  
                      WHERE id IN ($purchaseorderid) AND e.deleted = 0 AND e.setype = 'PurchaseOrder' AND itemserviceid IN ($costs) AND cargo_width = ?
                      GROUP BY v.vendorname";

          if($purchaseorderid){            
            $records2 = $this->db->pquery($sql2, array($cost_center));
          }else{
            $records2 = array('total' => '0');
          }        
      
    
          foreach($records2 as $row2){        
            $total_array[$i][$row2['vendorname']] = $row2['total'];  
          }       

          foreach($vendors as $vendor){             
            $total[$i][$vendor] = ($total_array[$i][$vendor]) ?: 0; 
          }

        }
      }

      $result = array('vendors' => $vendors, 'sum' => $total);

      return $result;
    
  }

  
  public function getInvoiceDetalizationByAccount($year,$vat,$cost)
  {  
                             
     
      $costs = $cost['cost'];     
      $costs = ltrim($costs,',');
      $costs = rtrim($costs,',');
      if(!empty($costs)){    
      $cost_center = $this->getCostNamesFromId($cost);

      $cost_centers = implode(",",  $cost_center['id']);
      $cost_centers_name = implode(",",  $cost_center['name']);



      $year_months_num = $this->thisYearMonthsNum($year);
      $sql = "SELECT GROUP_CONCAT(DISTINCT invoiceid) AS invoiceid 
                FROM app_opex_invoice_details  de
                LEFT JOIN app_opex_report re ON re.id=de.did
                WHERE re.years = ? AND service_key IN ($costs) AND cost_center IN ($cost_centers_name) AND  month_num = ?";   
                
      $sql3 = "SELECT GROUP_CONCAT(DISTINCT invoiceid) AS invoiceid 
                FROM app_opex_invoice_details  de
                LEFT JOIN app_opex_report re ON re.id=de.did
                WHERE re.years = ? AND service_key IN ($costs) AND cost_center IN ($cost_centers_name)"; 

      $records3 = $this->db->pquery($sql3, array($year)); 
      $invoiceid = rtrim($this->db->query_result($records3,0,'invoiceid'),',');      
      $invoiceid = ltrim($invoiceid,',');      
      
      $sql4 = "SELECT accountname
                    FROM vtiger_invoice inv                    
                    LEFT JOIN vtiger_account a ON a.accountid=inv.accountid 
                    WHERE inv.invoiceid IN ($invoiceid)
                    GROUP BY accountname";

      $records4 = $this->db->pquery($sql4, array());   

      $accounts = array();
      $total = array();
      $total_array = array();

      foreach($records4 as $key => $row){           
          $accounts[$row['accountname']] = $row['accountname'];              
      } 

      if($vat == 1){
        $sum_total = "REPLACE(ROUND(SUM(((((i.listprice*quantity) * IF(vtiger_taxregions.value > 0, value, 21)) /100) + i.listprice *quantity)),2),',','.')";
      }else{
        $sum_total = "REPLACE(ROUND(SUM(i.listprice*quantity),2),',','.')";
      }

      $this->db->pquery("SET SESSION group_concat_max_len = 1000000", array());
      for($i = 1; $i < $year_months_num+1; $i++){
        $records = $this->db->pquery($sql, array($year, $i));       
        $invoiceid = rtrim($this->db->query_result($records,0,'invoiceid'),',');      
        $invoiceid = ltrim($invoiceid,',');     


        $sql2 = "SELECT $sum_total AS total, a.accountname
                    FROM vtiger_inventoryproductrel i
                    LEFT JOIN vtiger_invoice inv ON inv.invoiceid=i.id
                    LEFT JOIN vtiger_account a ON a.accountid=inv.accountid
                    LEFT JOIN vtiger_crmentity e ON e.crmid=i.id
                    LEFT JOIN vtiger_itemservice its ON its.itemservice_tks_name=i.cargo_wgt 
                    LEFT JOIN app_services_type s ON s.id=i.service   
                    LEFT JOIN vtiger_taxregions ON vtiger_taxregions.regionid=inv.region_id
                    WHERE i.id IN ($invoiceid) AND e.deleted = 0 AND e.setype = 'Invoice' AND IF(productid = 121391,itemserviceid, service) IN ($costs) AND costcenter IN ($cost_centers)
                    GROUP BY a.accountname"; 

        if($invoiceid){            
          $records2 = $this->db->pquery($sql2, array());
        }else{
          $records2 = array('total' => '0');
        }        


        foreach($records2 as $row2){        
          $total_array[$i][$row2['accountname']] = $row2['total'];  
        }       

        foreach($accounts as $account){             
          $total[$i][$account] = ($total_array[$i][$account]) ?: 0; 
        }

      }

    }

        $result = array('accounts' => $accounts, 'sum' => $total);

    return $result;
  }

  public function getHandWritedSums($year){
    $sql = "SELECT title, REPLACE(total, ',', '.') as total,month FROM app_opex_hand_writed_rows WHERE year = ? ORDER BY month";
    $records = $this->db->pquery($sql, array($year)); 
    $sums_array = [];

    foreach ($records as $row) {
      $sums_array[$row['title']][$row['month']] = $row['total']; 
    }
    
    return $sums_array;
  }

}
