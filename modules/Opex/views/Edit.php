<?php

include "modules/Opex/models/listModel.php";

use modules\Opex\models\listModel as listModel;

class Opex_Edit_View extends Vtiger_Index_View {

  function __construct(){
    $this->listModel = new listModel;  
  }

  public function process(Vtiger_Request $request) 
  {

    $cost_center = $this->listModel->costCenters();

    if($_POST['cost_center']){                
      $this->listModel->saveCost($_POST);  
    }   

    $viewer = $this->getViewer($request);  
    $viewer->assign('cost_center', $cost_center);   
    $viewer->view('Edit.tpl', $request->getModule()); 
  }   

}