<?php

include "modules/Opex/models/listModel.php";

use modules\Opex\models\listModel as listModel;

class Opex_List_View extends Vtiger_Index_View {

  function __construct(){
    $this->listModel = new listModel;  
  }


  public function process(Vtiger_Request $request) 
  {
    $year = ($_GET['year'] ?: date('Y'));
    $vat = ($_GET['vat'] ?: 1);
    $column = ($vat == 1 ? 'total_with_vat' : 'total_without_vat');

    if($_POST['years']){
      $years = $_POST['years'];
      $referer = $_SERVER['PHP_SELF']."?module=Opex&view=List&year=$years&vat=$vat";
      header("Location: $referer");
    }

    if($_POST['vat']){
      $vat = $_POST['vat'];
      $referer = $_SERVER['PHP_SELF']."?module=Opex&view=List&year=$year&vat=$vat";
      header("Location: $referer");
    }

    $last_update = $this->listModel->lastUpdate();
    $total_profit =  $this->listModel->totalProfit($year,$vat,'All');
    // $month_profit =  $this->listModel->totalProfit($year,'Month');

    $getHandWritedSums = $this->listModel->getHandWritedSums($year);

    $transport_codes = $this->listModel->costListInvoiceNumber('transport');
    $month_profit =  $this->listModel->costListInvoice($year,$column,$transport_codes);
    $transport_profit_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$transport_codes);
 
    
    $servis_codes = $this->listModel->costListInvoiceNumber('servis');
    $servis_profit =  $this->listModel->costListInvoice($year,$column,$servis_codes);
    $servis_profit_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$servis_codes);

    $movement_codes = $this->listModel->costListInvoiceNumber('movement');
    $movement_services_with_other_services = $this->listModel->costListInvoice($year,$column, $movement_codes);
    $movement_services_with_other_services_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$movement_codes);

    $storage_codes = $this->listModel->costListInvoiceNumber('storage_rent');
    $storage_profit =  $this->listModel->costListInvoice($year,$column,$storage_codes);
    $storage_profit_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$storage_codes);

    $forwarding_codes = $this->listModel->costListInvoiceNumber('forwarding_profit');
    $forwarding = $this->listModel->costListInvoice($year,$column,$forwarding_codes);
    $forwarding_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$forwarding_codes);

    $stevedoring_vilnius_codes = $this->listModel->costListInvoiceNumber('stevedoring_profit_vilnius');
    $stevedoring_vilnius = $this->listModel->costListInvoice($year,$column,$stevedoring_vilnius_codes);
    $stevedoring_vilnius_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$stevedoring_vilnius_codes);

    $stevedoring_klaipeda_codes = $this->listModel->costListInvoiceNumber('stevedoring_profit_klaipeda');
    $stevedoring_klaipeda = $this->listModel->costListInvoice($year,$column,$stevedoring_klaipeda_codes);
    $stevedoring_klaipeda_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$stevedoring_klaipeda_codes);

    $stevedoring_kaunas_codes = $this->listModel->costListInvoiceNumber('stevedoring_profit_kaunas');
    $stevedoring_kaunas = $this->listModel->costListInvoice($year,$column,$stevedoring_kaunas_codes);
    $stevedoring_kaunas_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$stevedoring_kaunas_codes);


    $storage_vilnius_codes = $this->listModel->costListInvoiceNumber('storage_profit_vilnius');
    $storage_vilnius = $this->listModel->costListInvoice($year,$column,$storage_vilnius_codes);
    $storage_vilnius_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$storage_vilnius_codes);

    $other_profit_codes = $this->listModel->costListInvoiceNumber('other_profit');
    $other_profit = $this->listModel->costListInvoice($year,$column,$other_profit_codes);
    $other_profit_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$other_profit_codes);


    $storage_kaunas_codes = $this->listModel->costListInvoiceNumber('storage_profit_kaunas');
    $storage_kaunas =  $this->listModel->costListInvoice($year,$column,$storage_kaunas_codes);
    $storage_kaunas_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$storage_kaunas_codes);

    $storage_klaipeda_codes = $this->listModel->costListInvoiceNumber('storage_profit_klaipeda');
    $storage_klaipeda = $this->listModel->costListInvoice($year,$column,$storage_klaipeda_codes);
    $storage_klaipeda_detalization = $this->listModel->getInvoiceDetalizationByAccount($year,$vat,$storage_klaipeda_codes);
    
    $month =  $this->listModel->thisYearMonths($year);

    // Pop up
    $detalization =  $this->listModel->getDetalization();  

    $invoiceDetalization = $this->listModel->getInvoiceDetalization();


    $driver_bank_codes = $this->listModel->costListNumber('driver_bank');
    $driver_bank =  $this->listModel->costList($year,$column,$driver_bank_codes);

    $company_fees_codes = $this->listModel->costListNumber('company_fees');
    $company_fees =  $this->listModel->costList($year,$column,$company_fees_codes);

    $company_employees_fees_codes = $this->listModel->costListNumber('company_employees_fees');
    $company_employees_fees =  $this->listModel->costList($year,$column,$company_employees_fees_codes);

    $company_backup_driver_codes = $this->listModel->costListNumber('company_backup_driver');
    $company_backup_driver =  $this->listModel->costList($year,$column,$company_backup_driver_codes);


    $trv_bank_codes = $this->listModel->costListNumber('trv_bank');
    $trv_bank =  $this->listModel->costList($year,$column,$trv_bank_codes);

    $trv_company_fees_codes = $this->listModel->costListNumber('trv_company_fees');
    $trv_company_fees =  $this->listModel->costList($year,$column,$trv_company_fees_codes);

    $trv_company_employee_fees_codes = $this->listModel->costListNumber('trv_company_employee_fees');
    $trv_company_employee_fees =  $this->listModel->costList($year,$column,$trv_company_employee_fees_codes);

    $trv_company_backup_codes = $this->listModel->costListNumber('trv_company_backup');
    $trv_company_backup =  $this->listModel->costList($year,$column,$trv_company_backup_codes);

    $front_forwarding_bank_codes = $this->listModel->costListNumber('front_forwarding_bank');
    $front_forwarding_bank =  $this->listModel->costList($year,$column,$front_forwarding_bank_codes);

    $front_forwarding_company_fees_codes = $this->listModel->costListNumber('front_forwarding_company_fees');
    $front_forwarding_company_fees =  $this->listModel->costList($year,$column,$front_forwarding_company_fees_codes);

    $front_forwarding_company_employees_fees_codes = $this->listModel->costListNumber('front_forwarding_company_employees_fees');
    $front_forwarding_company_employees_fees =  $this->listModel->costList($year,$column,$front_forwarding_company_employees_fees_codes);

    $front_forwarding_company_backup_codes = $this->listModel->costListNumber('front_forwarding_company_backup');
    $front_forwarding_company_backup =  $this->listModel->costList($year,$column,$front_forwarding_company_backup_codes);
    
    $servis_bank_codes = $this->listModel->costListNumber('servis_bank');
    $servis_bank =  $this->listModel->costList($year,$column,$servis_bank_codes);

    $servis_fees_codes = $this->listModel->costListNumber('servis_fees');
    $servis_fees =  $this->listModel->costList($year,$column,$servis_fees_codes);

    $servis_employee_fees_codes = $this->listModel->costListNumber('servis_employee_fees');
    $servis_employee_fees =  $this->listModel->costList($year,$column,$servis_employee_fees_codes);

    $servis_backup_codes = $this->listModel->costListNumber('servis_backup');
    $servis_backup =  $this->listModel->costList($year,$column,$servis_backup_codes);

    $pm_bank_codes = $this->listModel->costListNumber('pm_bank');
    $pm_bank =  $this->listModel->costList($year,$column,$pm_bank_codes);

    $pm_company_fees_codes = $this->listModel->costListNumber('pm_company_fees');
    $pm_company_fees =  $this->listModel->costList($year,$column,$pm_company_fees_codes);

    $pm_employee_fees_codes = $this->listModel->costListNumber('pm_employee_fees');
    $pm_employee_fees =  $this->listModel->costList($year,$column,$pm_employee_fees_codes);

    $movement_backup_codes = $this->listModel->costListNumber('movement_backup');
    $movement_backup =  $this->listModel->costList($year,$column,$movement_backup_codes);

    $movement_material_storage_codes = $this->listModel->costListNumber('movement_material_storage');
    $movement_material_storage =  $this->listModel->costList($year,$column,$movement_material_storage_codes);

    $pkv_zp_codes = $this->listModel->costListNumber('pkv_zp');
    $pkv_zp =  $this->listModel->costList($year,$column,$pkv_zp_codes);

    $pkv_fees_codes = $this->listModel->costListNumber('pkv_fees');
    $pkv_fees =  $this->listModel->costList($year,$column,$pkv_fees_codes);

    $pkv_employee_fees_codes = $this->listModel->costListNumber('pkv_employee_fees');
    $pkv_employee_fees =  $this->listModel->costList($year,$column,$pkv_employee_fees_codes);


    $vilnius_bank_codes = $this->listModel->costListNumber('vilnius_bank');
    $vilnius_bank =  $this->listModel->costList($year,$column,$vilnius_bank_codes);

    $vilnius_fees_codes = $this->listModel->costListNumber('vilnius_fees');
    $vilnius_fees =  $this->listModel->costList($year,$column,$vilnius_fees_codes);

    $vilnius_employee_fees_codes = $this->listModel->costListNumber('vilnius_employee_fees');
    $vilnius_employee_fees =  $this->listModel->costList($year,$column,$vilnius_employee_fees_codes);

    $vilnius_backup_codes = $this->listModel->costListNumber('vilnius_backup');
    $vilnius_backup =  $this->listModel->costList($year,$column,$vilnius_backup_codes);
    
    $transport_dyzel_codes = $this->listModel->costListNumber('transport_dyzel');
    $transport_dyzel =  $this->listModel->costList($year,$column,$transport_dyzel_codes);
    $total_dyzel =  $this->listModel->costListSum($year,$transport_dyzel_codes);
    $transport_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_dyzel_codes);

    $transport_other_expense_codes = $this->listModel->costListNumber('transport_other_expense');
    $transport_other_expense = $this->listModel->costList($year,$column,$transport_other_expense_codes);
    $transport_other_expense_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_other_expense_codes);

    $transport_other_expense_codes2 = $this->listModel->costListNumber('transport_other_expense2');
    $transport_other_expense2 = $this->listModel->costList($year,$column,$transport_other_expense_codes2);
    $transport_other_expense_detalization2 = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_other_expense_codes2);


    $hiring_transport_forwarding_codes = $this->listModel->costListNumber('hiring_transport_forwarding');
    $hiring_transport_forwarding = $this->listModel->costList($year,$column,$hiring_transport_forwarding_codes);
    $total_hiring_transport_forwarding = $this->listModel->costListSum($year,$hiring_transport_forwarding_codes);
    $hiring_transport_forwarding_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$hiring_transport_forwarding_codes);


    $forwarding_self_transport_codes = $this->listModel->costListNumber('forwarding_self_transport');
    $forwarding_self_transport = $this->listModel->costList($year,$column,$forwarding_self_transport_codes);
    $total_forwarding_self_transport = $this->listModel->costListSum($year,$forwarding_self_transport_codes);
    
    $klaipeda_bank_codes = $this->listModel->costListNumber('klaipeda_bank');
    $klaipeda_bank = $this->listModel->costList($year,$column,$klaipeda_bank_codes);
    $klaipeda_bank_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$klaipeda_bank_codes);

    $klaipeda_fees_codes = $this->listModel->costListNumber('klaipeda_fees');
    $klaipeda_fees = $this->listModel->costList($year,$column,$klaipeda_fees_codes);

    $klaipeda_employee_fees_codes = $this->listModel->costListNumber('klaipeda_employee_fees');
    $klaipeda_employee_fees = $this->listModel->costList($year,$column,$klaipeda_employee_fees_codes);   
    
    $klaipeda_storage_backup_codes = $this->listModel->costListNumber('klaipeda_storage_backup');
    $klaipeda_storage_backup = $this->listModel->costList($year,$column,$klaipeda_storage_backup_codes);
    
    $kaunas_bank_codes = $this->listModel->costListNumber('kaunas_bank');
    $kaunas_bank = $this->listModel->costList($year,$column,$kaunas_bank_codes);
    
    $kaunas_fees_codes = $this->listModel->costListNumber('kaunas_fees');
    $kaunas_fees = $this->listModel->costList($year,$column,$kaunas_fees_codes);

    $kaunas_employee_fees_codes = $this->listModel->costListNumber('kaunas_employee_fees');
    $kaunas_employee_fees = $this->listModel->costList($year,$column,$kaunas_employee_fees_codes);

    $kaunas_storage_backup_codes = $this->listModel->costListNumber('kaunas_storage_backup');
    $kaunas_storage_backup = $this->listModel->costList($year,$column,$kaunas_storage_backup_codes);

    $admin_bank_codes = $this->listModel->costListNumber('admin_bank');
    $admin_bank = $this->listModel->costList($year,$column,$admin_bank_codes);
    
    $admin_fees_codes = $this->listModel->costListNumber('admin_fees');
    $admin_fees = $this->listModel->costList($year,$column,$admin_fees_codes);

    $admin_employee_fees_codes = $this->listModel->costListNumber('admin_employee_fees');
    $admin_employee_fees = $this->listModel->costList($year,$column,$admin_employee_fees_codes);

    
    $admin_storage_backup_codes = $this->listModel->costListNumber('admin_storage_backup');
    $admin_storage_backup = $this->listModel->costList($year,$column,$admin_storage_backup_codes);
    
    $office_water_codes = $this->listModel->costListNumber('office_water');
    $office_water = $this->listModel->costList($year,$column,$office_water_codes);
    
    $office_phone_codes = $this->listModel->costListNumber('office_phone');
    $office_phone = $this->listModel->costList($year,$column,$office_phone_codes);

    $cell_phones_codes = $this->listModel->costListNumber('cell_phones');
    $cell_phones = $this->listModel->costList($year,$column,$cell_phones_codes);
    $cell_phones_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$cell_phones_codes);
    
    $fees_codes = $this->listModel->costListNumber('fees');
    $fees = $this->listModel->costList($year,$column,$fees_codes);
    $fees_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$fees_codes);

    
    $post_fees_codes = $this->listModel->costListNumber('post_fees');
    $post_fees = $this->listModel->costList($year,$column,$post_fees_codes);
    $post_fees_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$post_fees_codes);

    $transport_remont_codes = $this->listModel->costListNumber('transport_remont');
    $transport_remont = $this->listModel->costList($year,$column,$transport_remont_codes);
    $total_remont = $this->listModel->costListSum($year,$transport_remont_codes);
    $transport_remont_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_remont_codes);


    $transport_parts_codes = $this->listModel->costListNumber('transport_parts');
    $transport_parts = $this->listModel->costList($year,$column,$transport_parts_codes);
    $total_parts =  $this->listModel->costListSum($year,$transport_parts_codes);
    $transport_parts_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_parts_codes);

    $transport_hired_codes = $this->listModel->costListNumber('transport_hired');
    $transport_hired = $this->listModel->costList($year,$column,$transport_hired_codes);
    $total_hired_transport =  $this->listModel->costListSum($year,$transport_hired_codes);  
    $transport_hired_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_hired_codes); 


    $transport_insurance_servis_codes = $this->listModel->costListNumber('transport_insurance_servis');
    $transport_insurance_servis = $this->listModel->costList($year,$column,$transport_insurance_servis_codes);
    $total_transport_insurance_servis = $this->listModel->costListSum($year,$transport_insurance_servis_codes);
    $transport_insurance_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_insurance_servis_codes); 


    $transport_road_tol_codes = $this->listModel->costListNumber('transport_road_tol');
    $transport_road_tol = $this->listModel->costList($year,$column,$transport_road_tol_codes);
    $total_transport_road_tol = $this->listModel->costListSum($year,$transport_road_tol_codes);
    $transport_road_tol_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_road_tol_codes); 
    

    $transport_square_rent_codes = $this->listModel->costListNumber('transport_square_rent');
    $transport_square_rent = $this->listModel->costList($year,$column,$transport_square_rent_codes); 
    $total_transport_square_rent = $this->listModel->costListSum($year,$transport_square_rent_codes); 
    $transport_square_rent_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_square_rent_codes); 


    $transport_other_taxes_codes = $this->listModel->costListNumber('transport_other_taxes');
    $transport_other_taxes = $this->listModel->costList($year,$column,$transport_other_taxes_codes);
    $total_transport_other_taxes = $this->listModel->costListSum($year,$transport_other_taxes_codes);
    $transport_other_taxes_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_other_taxes_codes); 


    $transport_bank_charges_codes = $this->listModel->costListNumber('transport_bank_charges');
    $transport_bank_charges = $this->listModel->costList($year,$column,$transport_bank_charges_codes);
    $total_bank_charges_transport = $this->listModel->costListSum($year,$transport_bank_charges_codes);
    $transport_bank_charges_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_bank_charges_codes); 

    $total_transport_orders = $this->listModel->totalTransportOrders($year);

    $sum_month_transport_profit = $this->listModel->sumMonthProfit($year,103956);

    $num_rows = $this->listModel->thisYearMonthsNum($year);


    $transport_ads_codes = $this->listModel->costListNumber('transport_ads');
    $transport_ads =  $this->listModel->costList($year,$column,$transport_ads_codes);
    $total_transport_ads =  $this->listModel->costListSum($year,$transport_ads_codes);
    $transport_ads_detalization = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$transport_ads_codes);     


    $utilities_servis_codes = $this->listModel->costListNumber('utilities_servis');
    $utilities_servis =  $this->listModel->costList($year,$column,$utilities_servis_codes);
    $total_utilities_servis =  $this->listModel->costListSum($year,$utilities_servis_codes);
    $utilities_detalization_servis = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$utilities_servis_codes); 


    $tools_servis_codes = $this->listModel->costListNumber('tools_servis');
    $tools_servis =  $this->listModel->costList($year,$column,$tools_servis_codes);
    $total_tools_servis =  $this->listModel->costListSum($year,$tools_servis_codes);
    $tools_detalization_servis = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$tools_servis_codes); 


    $other_servis_codes = $this->listModel->costListNumber('other_servis');
    $other_servis =  $this->listModel->costList($year,$column,$other_servis_codes);
    $total_other_servis =  $this->listModel->costListSum($year,$other_servis_codes);
    $other_detalization_servis = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$other_servis_codes); 


    $parts_servis_codes = $this->listModel->costListNumber('parts_servis');
    $parts_servis =  $this->listModel->costList($year,$column,$parts_servis_codes);    
    $total_parts_servis =  $this->listModel->costListSum($year,$parts_servis_codes);    
    $parts_detalization_servis = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$parts_servis_codes); 


    $place_rent_security_servis_codes = $this->listModel->costListNumber('place_rent_security_servis');
    $place_rent_security_servis =  $this->listModel->costList($year,$column,$place_rent_security_servis_codes);
    $total_place_rent_security_servis =  $this->listModel->costListSum($year,$place_rent_security_servis_codes);
    $place_rent_security_detalization_servis = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$place_rent_security_servis_codes); 

    $sum_month_servis_profit = $this->listModel->sumMonthProfit($year,103956);


    $hire_loader_movement_codes = $this->listModel->costListNumber('hire_loader_movement');
    $hire_loader_movement =  $this->listModel->costList($year,$column,$hire_loader_movement_codes);
    $total_hire_loader_movement =  $this->listModel->costListSum($year,$hire_loader_movement_codes);
    $hire_loader_detalization_movement = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$hire_loader_movement_codes); 

    $hire_transport_movement_codes = $this->listModel->costListNumber('hire_transport_movement');
    $hire_transport_movement =  $this->listModel->costList($year,$column,$hire_transport_movement_codes);
    $total_hire_transport_movement =  $this->listModel->costListSum($year,$hire_transport_movement_codes);
    $hire_transport_detalization_movement = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$hire_transport_movement_codes); 


    $ads_movement_codes = $this->listModel->costListNumber('ads_movement');
    $ads_movement =  $this->listModel->costList($year,$column,$ads_movement_codes);
    $total_ads_movement =  $this->listModel->costListSum($year,$ads_movement_codes);
    $ads_detalization_movement = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$ads_movement_codes); 


    $pack_material_movement_codes = $this->listModel->costListNumber('pack_material_movement');
    $pack_material_movement =  $this->listModel->costList($year,$column,$pack_material_movement_codes);
    $total_pack_material_movement =  $this->listModel->costListSum($year,$pack_material_movement_codes);
    $pack_material_detalization_movement = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$pack_material_movement_codes); 


    $other_movement_codes = $this->listModel->costListNumber('other_movement');
    $other_movement =  $this->listModel->costList($year,$column,$other_movement_codes);
    $total_other_movement =  $this->listModel->costListSum($year,$other_movement_codes);
    $other_detalization_movement = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$other_movement_codes); 

    $sum_month_servis_movement = $this->listModel->sumMonthProfit($year,'');
  

    $electricity_vilnius_codes = $this->listModel->costListNumber('electricity_vilnius');
    $electricity_vilnius =  $this->listModel->costList($year,$column,$electricity_vilnius_codes);
    $total_electricity_vilnius =  $this->listModel->costListSum($year,$electricity_vilnius_codes);
    $electricity_detalization_vilnius = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$electricity_vilnius_codes); 


    $place_repair_vilnius_codes = $this->listModel->costListNumber('place_repair_vilnius');
    $place_repair_vilnius =  $this->listModel->costList($year,$column,$place_repair_vilnius_codes);
    $total_place_repair_vilnius =  $this->listModel->costListSum($year,$place_repair_vilnius_codes);
    $place_repair_detalization_vilnius = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$place_repair_vilnius_codes); 


    $pack_material_vilnius_codes = $this->listModel->costListNumber('pack_material_vilnius');
    $pack_material_vilnius =  $this->listModel->costList($year,$column,$pack_material_vilnius_codes);
    $total_pack_material_vilnius =  $this->listModel->costListSum($year,$pack_material_vilnius_codes);
    $pack_material_detalization_vilnius = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$pack_material_vilnius_codes); 


    $security_vilnius_codes = $this->listModel->costListNumber('security_vilnius');
    $security_vilnius =  $this->listModel->costList($year,$column,$security_vilnius_codes);
    $total_security_vilnius =  $this->listModel->costListSum($year,$security_vilnius_codes);
    $security_detalization_vilnius = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$security_vilnius_codes); 


    $rent_vilnius_codes = $this->listModel->costListNumber('rent_vilnius');
    $rent_vilnius =  $this->listModel->costList($year,$column,$rent_vilnius_codes);
    $total_rent_vilnius =  $this->listModel->costListSum($year,$rent_vilnius_codes);
    $rent_detalization_vilnius = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$rent_vilnius_codes); 


    $technique_rent_vilnius_codes = $this->listModel->costListNumber('technique_rent_vilnius');
    $technique_rent_vilnius =  $this->listModel->costList($year,$column,$technique_rent_vilnius_codes);
    $total_technique_rent_vilnius =  $this->listModel->costListSum($year,$technique_rent_vilnius_codes);
    $technique_detalization_vilnius = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$technique_rent_vilnius_codes); 


    $technique_repair_parts_vilnius_codes = $this->listModel->costListNumber('technique_repair_parts_vilnius');
    $technique_repair_parts_vilnius =  $this->listModel->costList($year,$column,$technique_repair_parts_vilnius_codes);
    $total_technique_repair_parts_vilnius =  $this->listModel->costListSum($year,$technique_repair_parts_vilnius_codes);
    $technique_repair_parts_detalization_vilnius = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$technique_repair_parts_vilnius_codes); 

    $sum_month_cost_vilnius = $this->listModel->sumMonthProfit($year,'');      


    $hire_employees_klaipeda_codes = $this->listModel->costListNumber('hire_employees_klaipeda');
    $hire_employees_klaipeda =  $this->listModel->costList($year,$column,$hire_employees_klaipeda_codes);
    $total_hire_employees_klaipeda =  $this->listModel->costListSum($year,$hire_employees_klaipeda_codes);
    $hire_employees_detalization_klaipeda = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$hire_employees_klaipeda_codes); 


    $electricity_klaipeda_codes = $this->listModel->costListNumber('electricity_klaipeda');
    $electricity_klaipeda =  $this->listModel->costList($year,$column,$electricity_klaipeda_codes);
    $total_electricity_klaipeda =  $this->listModel->costListSum($year,$electricity_klaipeda_codes);
    $electricity_detalization_klaipeda = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$electricity_klaipeda_codes); 


    $place_repair_klaipeda_codes = $this->listModel->costListNumber('place_repair_klaipeda');
    $place_repair_klaipeda =  $this->listModel->costList($year,$column,$place_repair_klaipeda_codes);
    $total_place_repair_klaipeda =  $this->listModel->costListSum($year,$place_repair_klaipeda_codes);
    $place_repair_detalization_klaipeda = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$place_repair_klaipeda_codes); 


    $rent_klaipeda_codes = $this->listModel->costListNumber('rent_klaipeda');
    $rent_klaipeda =  $this->listModel->costList($year,$column,$rent_klaipeda_codes);
    $total_rent_klaipeda =  $this->listModel->costListSum($year,$rent_klaipeda_codes);
    $rent_detalization_klaipeda = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$rent_klaipeda_codes); 

    
    $technique_rent_klaipeda_codes = $this->listModel->costListNumber('technique_rent_klaipeda');
    $technique_rent_klaipeda =  $this->listModel->costList($year,$column,$technique_rent_klaipeda_codes);
    $total_technique_rent_klaipeda =  $this->listModel->costListSum($year,$technique_rent_klaipeda_codes);
    $technique_rent_detalization_klaipeda = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$technique_rent_klaipeda_codes); 


    $technique_repair_parts_klaipeda_codes = $this->listModel->costListNumber('technique_repair_parts_klaipeda');
    $technique_repair_parts_klaipeda =  $this->listModel->costList($year,$column,$technique_repair_parts_klaipeda_codes);
    $total_technique_repair_parts_klaipeda =  $this->listModel->costListSum($year,$technique_repair_parts_klaipeda_codes);
    $technique_repair_parts_detalization_klaipeda = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$technique_repair_parts_klaipeda_codes); 


    $other_klaipeda_codes = $this->listModel->costListNumber('other_klaipeda');
    $other_klaipeda =  $this->listModel->costList($year,$column,$other_klaipeda_codes);
    $total_other_klaipeda =  $this->listModel->costListSum($year,$other_klaipeda_codes);
    $other_detalization_klaipeda = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$other_klaipeda_codes); 

 
    $sum_month_cost_klaipeda = $this->listModel->sumMonthProfit($year,$other_klaipeda_codes);  


    $electricity_kaunas_codes = $this->listModel->costListNumber('electricity_kaunas');
    $electricity_kaunas =  $this->listModel->costList($year,$column,$electricity_kaunas_codes);
    $total_electricity_kaunas =  $this->listModel->costListSum($year,$electricity_kaunas_codes);
    $electricity_detalization_kaunas = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$electricity_kaunas_codes); 


    $place_repair_kaunas_codes = $this->listModel->costListNumber('place_repair_kaunas');
    $place_repair_kaunas =  $this->listModel->costList($year,$column,$place_repair_kaunas_codes);
    $total_place_repair_kaunas =  $this->listModel->costListSum($year,$place_repair_kaunas_codes);
    $place_repair_detalization_kaunas = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$place_repair_kaunas_codes); 


    $security_kaunas_codes = $this->listModel->costListNumber('security_kaunas');
    $security_kaunas =  $this->listModel->costList($year,$column,$security_kaunas_codes);
    $total_security_kaunas =  $this->listModel->costListSum($year,$security_kaunas_codes);
    $security_detalization_kaunas = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$security_kaunas_codes); 


    $rent_kaunas_codes = $this->listModel->costListNumber('rent_kaunas');
    $rent_kaunas =  $this->listModel->costList($year,$column,$rent_kaunas_codes);
    $total_rent_kaunas =  $this->listModel->costListSum($year,$rent_kaunas_codes);
    $rent_detalization_kaunas = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$rent_kaunas_codes); 


    $technique_rent_kaunas_codes = $this->listModel->costListNumber('technique_rent_kaunas');
    $technique_rent_kaunas =  $this->listModel->costList($year,$column,$technique_rent_kaunas_codes);
    $total_technique_rent_kaunas =  $this->listModel->costListSum($year,$technique_rent_kaunas_codes);
    $technique_rent_detalization_kaunas = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$technique_rent_kaunas_codes); 


    $technique_repair_parts_kaunas_codes = $this->listModel->costListNumber('technique_repair_parts_kaunas');
    $technique_repair_parts_kaunas =  $this->listModel->costList($year,$column,$technique_repair_parts_kaunas_codes);
    $total_technique_repair_parts_kaunas =  $this->listModel->costListSum($year,$technique_repair_parts_kaunas_codes);
    $technique_repair_parts_detalization_kaunas = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$technique_repair_parts_kaunas_codes); 


    $sum_month_cost_kaunas = $this->listModel->sumMonthProfit($year,'');
    

    $electricity_office_codes = $this->listModel->costListNumber('electricity_office');
    $electricity_office =  $this->listModel->costList($year,$column,$electricity_office_codes);
    $total_electricity_office =  $this->listModel->costListSum($year,$electricity_office_codes);
    $electricity_detalization_office = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$electricity_office_codes); 
    

    $connection_office_codes = $this->listModel->costListNumber('connection_office');
    $connection_office =  $this->listModel->costList($year,$column,$connection_office_codes);
    $total_connection_office =  $this->listModel->costListSum($year,$connection_office_codes);
    $connection_detalization_office = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$connection_office_codes); 


    $rent_repair_office_codes = $this->listModel->costListNumber('rent_repair_office');
    $rent_repair_office =  $this->listModel->costList($year,$column,$rent_repair_office_codes);
    $total_rent_repair_office =  $this->listModel->costListSum($year,$rent_repair_office_codes);
    $rent_repair_detalization_office = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$rent_repair_office_codes); 


    $security_office_codes = $this->listModel->costListNumber('security_office');
    $security_office =  $this->listModel->costList($year,$column,$security_office_codes);
    $total_security_office =  $this->listModel->costListSum($year,$security_office_codes);
    $security_detalization_office = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$security_office_codes); 


    $leasing_office_codes = $this->listModel->costListNumber('leasing_office');
    $leasing_office =  $this->listModel->costList($year,$column,$leasing_office_codes);
    $total_leasing_office =  $this->listModel->costListSum($year,$leasing_office_codes);
    $leasing_detalization_office = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$leasing_office_codes); 

    $sum_month_cost_office = $this->listModel->sumMonthProfit($year,array(103957,103959,103956,104021,114452,127650,103953,127649,114454,114455,114457,114456));

  
    $sum_all_places_and_services_cost = $this->listModel->sumAllPlacesAndServicesCost($year);


    $insurance_servis_admin_codes = $this->listModel->costListNumber('insurance_servis_admin');
    $insurance_servis_admin = $this->listModel->costList($year,$column,$insurance_servis_admin_codes);
    $total_insurance_servis_admin = $this->listModel->costListSum($year,$insurance_servis_admin_codes);
    $insurance_servis_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$insurance_servis_admin_codes); 


    $inventory_admin_codes = $this->listModel->costListNumber('inventory_admin');
    $inventory_admin = $this->listModel->costList($year,$column,$inventory_admin_codes);
    $total_inventory_admin = $this->listModel->costListSum($year,$inventory_admin_codes);
    $inventory_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$inventory_admin_codes); 


    $ads_admin_codes = $this->listModel->costListNumber('ads_admin');
    $ads_admin = $this->listModel->costList($year,$column,$ads_admin_codes);
    $total_ads_admin = $this->listModel->costListSum($year,$ads_admin_codes);
    $ads_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$ads_admin_codes); 


    $fuel_admin_codes = $this->listModel->costListNumber('fuel_admin');
    $fuel_admin = $this->listModel->costList($year,$column,$fuel_admin_codes);
    $total_fuel_admin = $this->listModel->costListSum($year,$fuel_admin_codes);
    $fuel_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$fuel_admin_codes); 


    $other_admin_codes = $this->listModel->costListNumber('other_admin');
    $other_admin = $this->listModel->costList($year,$column,$other_admin_codes);
    $total_other_admin = $this->listModel->costListSum($year,$other_admin_codes);
    $other_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$other_admin_codes); 


    $other2_admin_codes = $this->listModel->costListNumber('other2_admin');
    $other2_admin = $this->listModel->costList($year,$column,$other2_admin_codes);
    $total_other2_admin = $this->listModel->costListSum($year,$other2_admin_codes);
    $other2_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$other2_admin_codes);


    $other_fees_admin_codes = $this->listModel->costListNumber('other_fees_admin');
    $other_fees_admin = $this->listModel->costList($year,$column,$other_fees_admin_codes);
    $total_other_fees_admin = $this->listModel->costListSum($year,$other_fees_admin_codes);
    $other_fees_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$other_fees_admin_codes);


    $representation_admin_codes = $this->listModel->costListNumber('representation_admin');
    $representation_admin = $this->listModel->costList($year,$column,$representation_admin_codes);
    $total_representation_admin = $this->listModel->costListSum($year,$representation_admin_codes);
    $representation_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$representation_admin_codes);


    $it_admin_codes = $this->listModel->costListNumber('it_admin');
    $it_admin = $this->listModel->costList($year,$column,$it_admin_codes);
    $total_it_admin = $this->listModel->costListSum($year,$it_admin_codes);
    $it_detalization_admin = $this->listModel->getPurchaseDetalizationByVendor($year,$vat,$it_admin_codes);

    $sum_month_cost_office_other = $this->listModel->sumMonthProfit($year,array(103979,103955,103981,103980,103952,103950,110429,103984,103983));

    $total_real = $this->listModel->sumReal($year,$column);


    $other_cost = $this->listModel->othercostList($year,$column);
    $sum_other_cost = $this->listModel->otherCostListSum($year,$column);

    $invoice_profit_without_list = $this->listModel->invoiceProfitWithOutList($year,$column);
    $sum_invoice_profit_without_list = $this->listModel->invoiceProfitWithOutListSum($year,$column);

    $viewer = $this->getViewer($request); 
    $viewer->assign('last_update', $last_update);                 
    $viewer->assign('total_profit', $total_profit);   
    $viewer->assign('month_profit', $month_profit);
    $viewer->assign('getHandWritedSums', $getHandWritedSums);
    $viewer->assign('transport_profit_detalization', $transport_profit_detalization);

    $viewer->assign('driver_bank', $driver_bank);
    $viewer->assign('company_fees', $company_fees);
    $viewer->assign('company_employees_fees', $company_employees_fees);
    $viewer->assign('company_backup_driver', $company_backup_driver);

    $viewer->assign('trv_bank', $trv_bank);
    $viewer->assign('trv_company_fees', $trv_company_fees);
    $viewer->assign('trv_company_employee_fees', $trv_company_employee_fees);
    $viewer->assign('trv_company_backup', $trv_company_backup);

    $viewer->assign('front_forwarding_bank', $front_forwarding_bank);
    $viewer->assign('front_forwarding_company_fees', $front_forwarding_company_fees);
    $viewer->assign('front_forwarding_company_employees_fees', $front_forwarding_company_employees_fees);
    $viewer->assign('front_forwarding_company_backup', $front_forwarding_company_backup);

    $viewer->assign('servis_bank', $servis_bank);
    $viewer->assign('servis_fees', $servis_fees);
    $viewer->assign('servis_employee_fees', $servis_employee_fees);
    $viewer->assign('servis_backup', $servis_backup);

    $viewer->assign('pm_bank', $pm_bank);
    $viewer->assign('pm_company_fees', $pm_company_fees);
    $viewer->assign('pm_employee_fees', $pm_employee_fees);
    $viewer->assign('movement_backup', $movement_backup);


    $viewer->assign('movement_material_storage', $movement_material_storage);
    $viewer->assign('pkv_zp', $pkv_zp);
    $viewer->assign('pkv_fees', $pkv_fees);
    $viewer->assign('pkv_employee_fees', $pkv_employee_fees);

    $viewer->assign('vilnius_bank', $vilnius_bank);
    $viewer->assign('vilnius_fees', $vilnius_fees);
    $viewer->assign('vilnius_employee_fees', $vilnius_employee_fees);
    $viewer->assign('vilnius_backup', $vilnius_backup);

    
    $viewer->assign('servis_profit', $servis_profit);
    $viewer->assign('servis_profit_detalization', $servis_profit_detalization);
    $viewer->assign('storage_profit', $storage_profit);
    $viewer->assign('storage_profit_detalization', $storage_profit_detalization);

    $viewer->assign('month', $month);
    
    $viewer->assign('transport_dyzel', $transport_dyzel);
    $viewer->assign('transport_other_expense', $transport_other_expense);
    $viewer->assign('transport_other_expense_detalization', $transport_other_expense_detalization);
    $viewer->assign('transport_other_expense2', $transport_other_expense2);
    $viewer->assign('transport_other_expense_detalization2', $transport_other_expense_detalization2);
    $viewer->assign('total_dyzel', $total_dyzel);
    $viewer->assign('transport_remont', $transport_remont);
    $viewer->assign('total_remont', $total_remont);
    $viewer->assign('transport_parts', $transport_parts);
    $viewer->assign('total_parts', $total_parts);
    $viewer->assign('transport_hired', $transport_hired); 
    $viewer->assign('total_hired_transport', $total_hired_transport);
    $viewer->assign('transport_insurance_servis', $transport_insurance_servis); 
    $viewer->assign('total_transport_insurance_servis', $total_transport_insurance_servis); 
    $viewer->assign('transport_road_tol', $transport_road_tol); 
    $viewer->assign('total_transport_road_tol', $total_transport_road_tol); 
    $viewer->assign('transport_square_rent', $transport_square_rent); 
    $viewer->assign('total_transport_square_rent', $total_transport_square_rent); 
    $viewer->assign('transport_other_taxes', $transport_other_taxes); 
    $viewer->assign('total_transport_other_taxes', $total_transport_other_taxes); 
    $viewer->assign('transport_bank_charges', $transport_bank_charges); 
    $viewer->assign('total_bank_charges_transport', $total_bank_charges_transport); 
    $viewer->assign('total_transport_orders', $total_transport_orders);
    $viewer->assign('sum_month_transport_profit', $sum_month_transport_profit);
    $viewer->assign('transport_ads', $transport_ads);
    $viewer->assign('forwarding', $forwarding);
    $viewer->assign('forwarding_detalization', $forwarding_detalization);
    $viewer->assign('total_transport_ads', $total_transport_ads);
    $viewer->assign('utilities_servis', $utilities_servis);
    $viewer->assign('total_utilities_servis', $total_utilities_servis);    
    $viewer->assign('tools_servis', $tools_servis);
    $viewer->assign('total_tools_servis', $total_tools_servis);
    $viewer->assign('other_servis', $other_servis);
    $viewer->assign('total_other_servis', $total_other_servis);
    $viewer->assign('parts_servis', $parts_servis);
    $viewer->assign('total_parts_servis', $total_parts_servis);
    $viewer->assign('place_rent_security_servis', $place_rent_security_servis);
    $viewer->assign('total_place_rent_security_servis', $total_place_rent_security_servis);
    $viewer->assign('sum_month_servis_profit', $sum_month_servis_profit);
    $viewer->assign('hire_loader_movement', $hire_loader_movement);
    $viewer->assign('total_hire_loader_movement', $total_hire_loader_movement);
    $viewer->assign('hire_transport_movement', $hire_transport_movement);
    $viewer->assign('total_hire_transport_movement', $total_hire_transport_movement);
    $viewer->assign('ads_movement', $ads_movement);
    $viewer->assign('total_ads_movement', $total_ads_movement);
    $viewer->assign('pack_material_movement', $pack_material_movement);
    $viewer->assign('total_pack_material_movement', $total_pack_material_movement);
    $viewer->assign('other_movement', $other_movement);
    $viewer->assign('total_other_movement', $total_other_movement);
    $viewer->assign('sum_month_servis_movement', $sum_month_servis_movement);
    $viewer->assign('movement_services_with_other_services', $movement_services_with_other_services);
    $viewer->assign('movement_services_with_other_services_detalization', $movement_services_with_other_services_detalization);
    $viewer->assign('electricity_vilnius', $electricity_vilnius);
    $viewer->assign('total_electricity_vilnius', $total_electricity_vilnius);
    $viewer->assign('place_repair_vilnius', $place_repair_vilnius);
    $viewer->assign('total_place_repair_vilnius', $total_place_repair_vilnius);
    $viewer->assign('pack_material_vilnius', $pack_material_vilnius);
    $viewer->assign('total_pack_material_vilnius', $total_pack_material_vilnius);
    $viewer->assign('security_vilnius', $security_vilnius);
    $viewer->assign('total_security_vilnius', $total_security_vilnius);
    $viewer->assign('rent_vilnius', $rent_vilnius);
    $viewer->assign('total_rent_vilnius', $total_rent_vilnius);
    $viewer->assign('technique_rent_vilnius', $technique_rent_vilnius);
    $viewer->assign('total_technique_rent_vilnius', $total_technique_rent_vilnius);
    $viewer->assign('technique_repair_parts_vilnius', $technique_repair_parts_vilnius);
    $viewer->assign('total_technique_repair_parts_vilnius', $total_technique_repair_parts_vilnius);
    $viewer->assign('sum_month_cost_vilnius', $sum_month_cost_vilnius);
    $viewer->assign('storage_vilnius', $storage_vilnius); 
    $viewer->assign('storage_vilnius_detalization', $storage_vilnius_detalization); 
    $viewer->assign('other_profit', $other_profit); 
    $viewer->assign('other_profit_detalization', $other_profit_detalization);   
    $viewer->assign('stevedoring_vilnius', $stevedoring_vilnius);
    $viewer->assign('stevedoring_vilnius_detalization', $stevedoring_vilnius_detalization);
    $viewer->assign('hire_employees_klaipeda', $hire_employees_klaipeda);
    $viewer->assign('total_hire_employees_klaipeda', $total_hire_employees_klaipeda);
    $viewer->assign('electricity_klaipeda', $electricity_klaipeda);
    $viewer->assign('total_electricity_klaipeda', $total_electricity_klaipeda);
    $viewer->assign('place_repair_klaipeda', $place_repair_klaipeda);
    $viewer->assign('total_place_repair_klaipeda', $total_place_repair_klaipeda);
    $viewer->assign('rent_klaipeda', $rent_klaipeda);
    $viewer->assign('total_rent_klaipeda', $total_rent_klaipeda);
    $viewer->assign('technique_rent_klaipeda', $technique_rent_klaipeda);
    $viewer->assign('total_technique_rent_klaipeda', $total_technique_rent_klaipeda);
    $viewer->assign('technique_repair_parts_klaipeda', $technique_repair_parts_klaipeda);
    $viewer->assign('total_technique_repair_parts_klaipeda', $total_technique_repair_parts_klaipeda);
    $viewer->assign('other_klaipeda', $other_klaipeda);
    $viewer->assign('total_other_klaipeda', $total_other_klaipeda);
    $viewer->assign('sum_month_cost_klaipeda', $sum_month_cost_klaipeda);
    $viewer->assign('storage_klaipeda', $storage_klaipeda);
    $viewer->assign('storage_klaipeda_detalization', $storage_klaipeda_detalization);
    $viewer->assign('stevedoring_klaipeda', $stevedoring_klaipeda);
    $viewer->assign('stevedoring_klaipeda_detalization', $stevedoring_klaipeda_detalization);
    $viewer->assign('electricity_kaunas', $electricity_kaunas);
    $viewer->assign('total_electricity_kaunas', $total_electricity_kaunas);
    $viewer->assign('place_repair_kaunas', $place_repair_kaunas);
    $viewer->assign('total_place_repair_kaunas', $total_place_repair_kaunas);    
    $viewer->assign('security_kaunas', $security_kaunas);
    $viewer->assign('total_security_kaunas', $total_security_kaunas);
    $viewer->assign('rent_kaunas', $rent_kaunas);
    $viewer->assign('technique_rent_kaunas', $technique_rent_kaunas);
    $viewer->assign('total_technique_rent_kaunas', $total_technique_rent_kaunas);    
    $viewer->assign('technique_repair_parts_kaunas', $technique_repair_parts_kaunas);
    $viewer->assign('total_technique_repair_parts_kaunas', $total_technique_repair_parts_kaunas);
    $viewer->assign('sum_month_cost_kaunas', $sum_month_cost_kaunas);
    $viewer->assign('storage_kaunas', $storage_kaunas); 
    $viewer->assign('storage_kaunas_detalization', $storage_kaunas_detalization); 
    $viewer->assign('stevedoring_kaunas', $stevedoring_kaunas);
    $viewer->assign('stevedoring_kaunas_detalization', $stevedoring_kaunas_detalization);
    $viewer->assign('hiring_transport_forwarding', $hiring_transport_forwarding);
    $viewer->assign('total_hiring_transport_forwarding', $total_hiring_transport_forwarding);
    $viewer->assign('hiring_transport_forwarding_detalization', $hiring_transport_forwarding_detalization);
    $viewer->assign('forwarding_self_transport', $forwarding_self_transport);
    $viewer->assign('total_forwarding_self_transport', $total_forwarding_self_transport);
    $viewer->assign('klaipeda_bank', $klaipeda_bank);
    $viewer->assign('klaipeda_bank_detalization', $klaipeda_bank_detalization);
    $viewer->assign('klaipeda_fees', $klaipeda_fees);
    $viewer->assign('klaipeda_employee_fees', $klaipeda_employee_fees);
    $viewer->assign('kaunas_bank', $kaunas_bank);
    $viewer->assign('kaunas_fees', $kaunas_fees);
    $viewer->assign('kaunas_employee_fees', $kaunas_employee_fees);
    $viewer->assign('kaunas_storage_backup', $kaunas_storage_backup);
    $viewer->assign('admin_bank', $admin_bank);
    $viewer->assign('admin_fees', $admin_fees);
    $viewer->assign('admin_employee_fees', $admin_employee_fees);
    $viewer->assign('admin_storage_backup', $admin_storage_backup);
    $viewer->assign('office_water', $office_water);
    $viewer->assign('office_phone', $office_phone);
    $viewer->assign('cell_phones', $cell_phones);
    $viewer->assign('cell_phones_detalization_admin', $cell_phones_detalization_admin);
    $viewer->assign('fees', $fees);
    $viewer->assign('fees_detalization_admin', $fees_detalization_admin);
    $viewer->assign('post_fees', $post_fees);
    $viewer->assign('post_fees_detalization', $post_fees_detalization);
    $viewer->assign('klaipeda_storage_backup', $klaipeda_storage_backup);
    $viewer->assign('electricity_office', $electricity_office);
    $viewer->assign('total_electricity_office', $total_electricity_office);
    $viewer->assign('electricity_office', $electricity_office);
    $viewer->assign('total_electricity_office', $total_electricity_office);    
    $viewer->assign('connection_office', $connection_office);
    $viewer->assign('total_connection_office', $total_connection_office);
    $viewer->assign('rent_repair_office', $rent_repair_office);
    $viewer->assign('total_rent_repair_office', $total_rent_repair_office);
    $viewer->assign('security_office', $security_office);
    $viewer->assign('total_security_office', $total_security_office);
    $viewer->assign('leasing_office', $leasing_office);
    $viewer->assign('total_leasing_office', $total_leasing_office);
    $viewer->assign('sum_month_cost_office', $sum_month_cost_office);
    $viewer->assign('sum_all_places_and_services_cost', $sum_all_places_and_services_cost);
    $viewer->assign('insurance_servis_admin', $insurance_servis_admin);
    $viewer->assign('total_insurance_servis_admin', $total_insurance_servis_admin);
    $viewer->assign('inventory_admin', $inventory_admin);
    $viewer->assign('total_inventory_admin', $total_inventory_admin);
    $viewer->assign('ads_admin', $ads_admin);
    $viewer->assign('total_ads_admin', $total_ads_admin);
    $viewer->assign('fuel_admin', $fuel_admin);
    $viewer->assign('total_fuel_admin', $total_fuel_admin);
    $viewer->assign('other_admin', $other_admin);
    $viewer->assign('total_other_admin', $total_other_admin);
    $viewer->assign('other2_admin', $other2_admin);
    $viewer->assign('total_other2_admin', $total_other2_admin);
    $viewer->assign('other_fees_admin', $other_fees_admin);
    $viewer->assign('total_other_fees_admin', $total_other_fees_admin);
    $viewer->assign('representation_admin', $representation_admin);
    $viewer->assign('total_representation_admin', $total_representation_admin);
    $viewer->assign('it_admin', $it_admin);
    $viewer->assign('total_it_admin', $total_it_admin);
    $viewer->assign('sum_month_cost_office_other', $sum_month_cost_office_other);
    $viewer->assign('total_real', $total_real);
    $viewer->assign('other_cost', $other_cost);
    $viewer->assign('sum_other_cost', $sum_other_cost);
    $viewer->assign('invoice_profit_without_list', $invoice_profit_without_list);
    $viewer->assign('sum_invoice_profit_without_list', $sum_invoice_profit_without_list);
    $viewer->assign('detalization', $detalization);
    $viewer->assign('invoiceDetalization', $invoiceDetalization);
    $viewer->assign('transport_detalization', $transport_detalization);
    $viewer->assign('transport_remont_detalization', $transport_remont_detalization);
    $viewer->assign('transport_parts_detalization', $transport_parts_detalization);
    $viewer->assign('transport_hired_detalization', $transport_hired_detalization);
    $viewer->assign('transport_insurance_detalization', $transport_insurance_detalization);
    $viewer->assign('transport_road_tol_detalization', $transport_road_tol_detalization);
    $viewer->assign('transport_square_rent_detalization', $transport_square_rent_detalization);
    $viewer->assign('transport_other_taxes_detalization', $transport_other_taxes_detalization);
    $viewer->assign('transport_bank_charges_detalization', $transport_bank_charges_detalization);
    $viewer->assign('transport_ads_detalization', $transport_ads_detalization);
    $viewer->assign('utilities_detalization_servis', $utilities_detalization_servis);
    $viewer->assign('other_detalization_servis', $other_detalization_servis);
    $viewer->assign('parts_detalization_servis', $parts_detalization_servis);
    $viewer->assign('place_rent_security_detalization_servis', $place_rent_security_detalization_servis);
    $viewer->assign('hire_loader_detalization_movement', $hire_loader_detalization_movement);
    $viewer->assign('hire_transport_detalization_movement', $hire_transport_detalization_movement);
    $viewer->assign('ads_detalization_movement', $ads_detalization_movement);
    $viewer->assign('pack_material_detalization_movement', $pack_material_detalization_movement);
    $viewer->assign('other_detalization_movement', $other_detalization_movement);
    $viewer->assign('electricity_detalization_vilnius', $electricity_detalization_vilnius);
    $viewer->assign('place_repair_detalization_vilnius', $place_repair_detalization_vilnius);
    $viewer->assign('pack_material_detalization_vilnius', $pack_material_detalization_vilnius);
    $viewer->assign('security_detalization_vilnius', $security_detalization_vilnius);
    $viewer->assign('rent_detalization_vilnius', $rent_detalization_vilnius);
    $viewer->assign('technique_detalization_vilnius', $technique_detalization_vilnius);
    $viewer->assign('technique_repair_parts_detalization_vilnius', $technique_repair_parts_detalization_vilnius);
    $viewer->assign('hire_employees_detalization_klaipeda', $hire_employees_detalization_klaipeda);
    $viewer->assign('electricity_detalization_klaipeda', $electricity_detalization_klaipeda);
    $viewer->assign('place_repair_detalization_klaipeda', $place_repair_detalization_klaipeda);
    $viewer->assign('rent_detalization_klaipeda', $rent_detalization_klaipeda);
    $viewer->assign('technique_rent_detalization_klaipeda', $technique_rent_detalization_klaipeda);
    $viewer->assign('technique_repair_parts_detalization_klaipeda', $technique_repair_parts_detalization_klaipeda);
    $viewer->assign('other_detalization_klaipeda', $other_detalization_klaipeda);
    $viewer->assign('electricity_detalization_kaunas', $electricity_detalization_kaunas);
    $viewer->assign('place_repair_detalization_kaunas', $place_repair_detalization_kaunas);
    $viewer->assign('security_detalization_kaunas', $security_detalization_kaunas);
    $viewer->assign('rent_detalization_kaunas', $rent_detalization_kaunas);
    $viewer->assign('technique_rent_detalization_kaunas', $technique_rent_detalization_kaunas);
    $viewer->assign('technique_repair_parts_detalization_kaunas', $technique_repair_parts_detalization_kaunas);
    $viewer->assign('electricity_detalization_office', $electricity_detalization_office);
    $viewer->assign('connection_detalization_office', $connection_detalization_office);
    $viewer->assign('rent_repair_detalization_office', $rent_repair_detalization_office);
    $viewer->assign('security_detalization_office', $security_detalization_office);
    $viewer->assign('leasing_detalization_office', $leasing_detalization_office);
    $viewer->assign('insurance_servis_detalization_admin', $insurance_servis_detalization_admin);
    $viewer->assign('inventory_detalization_admin', $inventory_detalization_admin);
    $viewer->assign('ads_detalization_admin', $ads_detalization_admin);
    $viewer->assign('fuel_detalization_admin', $fuel_detalization_admin);
    $viewer->assign('other_detalization_admin', $other_detalization_admin);
    $viewer->assign('other2_detalization_admin', $other2_detalization_admin);
    $viewer->assign('other_fees_detalization_admin', $other_fees_detalization_admin);
    $viewer->assign('representation_detalization_admin', $representation_detalization_admin);
    $viewer->assign('it_detalization_admin', $it_detalization_admin);
    $viewer->assign('year', $year);
    $viewer->assign('vat', $vat);
    $viewer->assign('num_rows', $num_rows);
    $viewer->view('List.tpl', $request->getModule()); 
  }   
}

