<?php

require 'vtlib/Vtiger/exel/vendor/autoload.php';

include "modules/Opex/models/exportExcel.php";

use modules\Opex\models\exportExcel as exportExcel;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Opex_Excel_View extends Vtiger_Index_View {

  function __construct(){
    $this->listModel = new exportExcel;  
	}
	

	public function process(Vtiger_Request $request) {
			$year = ($_GET['year'] ?: date('Y'));
			$vat = ($_GET['vat'] ?: 1);
			$column = ($vat == 1 ? 'total_with_vat' : 'total_without_vat');

			$letters = array('C','D','E','F','G','H','I','J','K','L','M','N');

			$reader = IOFactory::createReader('Xlsx');
			ob_end_clean();
			$spreadsheet = $reader->load('layouts/v7/modules/Opex/resources/OPEX_LAST.xlsx');		

			$spreadsheet->getActiveSheet()->setTitle('Opex ataskaita');	

			$getHandWritedSums = $this->listModel->getHandWritedSums($year);

			$num_rows = $this->listModel->thisYearMonthsNum($year);			
			
			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'5', ($getHandWritedSums['driver_count'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'6', ($getHandWritedSums['car_mileage'][$i] ?: 0));	
			}


			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'7', ($getHandWritedSums['fuel_litres'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'8', ($getHandWritedSums['car_count'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'9', ($getHandWritedSums['driver_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'10', ($getHandWritedSums['company_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'11', ($getHandWritedSums['company_employees_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'12', ($getHandWritedSums['company_backup_driver'][$i] ?: 0));	
			}

			$transport_dyzel_codes = $this->listModel->costListNumber('transport_dyzel');
    	$transport_dyzel =  $this->listModel->costList($year,$column,array($transport_dyzel_codes['cost']),$transport_dyzel_codes['costcenter']);		

			$i = 0;
			foreach($transport_dyzel as $SUM){				
				$spreadsheet->getActiveSheet()->setCellValue($letters[$i].'14', (float)$SUM);				
				$i++;
			}			

			$transport_remont_codes = $this->listModel->costListNumber('transport_remont');
   		$transport_remont = $this->listModel->costList($year,$column,array($transport_remont_codes['cost']),$transport_remont_codes['costcenter']);
			$f = 0;
			foreach($transport_remont as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$f].'15', (float)$SUM);	
				$f++;
			}

			$transport_parts_codes = $this->listModel->costListNumber('transport_parts');
			$transport_parts = $this->listModel->costList($year,$column,array($transport_parts_codes['cost']),$transport_parts_codes['costcenter']);
			$e = 0;
			foreach($transport_parts as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$e].'16', (float)$SUM);	
				$e++;
			}

			$transport_hired_codes = $this->listModel->costListNumber('transport_hired');
    	$transport_hired = $this->listModel->costList($year,$column,array($transport_hired_codes['cost']),$transport_hired_codes['costcenter']);
			$g = 0;
			foreach($transport_hired as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$g].'17', (float)$SUM);	
				$g++;
			}

			$transport_insurance_servis_codes = $this->listModel->costListNumber('transport_insurance_servis');
			$transport_insurance_servis = $this->listModel->costList($year,$column,array($transport_insurance_servis_codes['cost']),$transport_insurance_servis_codes['costcenter']);
			$h = 0;
			foreach($transport_insurance_servis as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$h].'18', (float)$SUM);	
				$h++;
			}

			$transport_road_tol_codes = $this->listModel->costListNumber('transport_road_tol');
			$transport_road_tol = $this->listModel->costList($year,$column,array($transport_road_tol_codes['cost']),$transport_road_tol_codes['costcenter']);
			$j = 0;
			foreach($transport_road_tol as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$j].'19', (float)$SUM);	
				$j++;
			}

			$transport_square_rent_codes = $this->listModel->costListNumber('transport_square_rent');
			$transport_square_rent = $this->listModel->costList($year,$column,array($transport_square_rent_codes['cost']),$transport_square_rent_codes['costcenter']); 
			$k = 0;
			foreach($transport_square_rent as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$k].'20', (float)$SUM);	
				$k++;
			}

			$transport_other_taxes_codes = $this->listModel->costListNumber('transport_other_taxes');
			$transport_other_taxes = $this->listModel->costList($year,$column,array($transport_other_taxes_codes['cost']),$transport_other_taxes_codes['costcenter']);
			$l = 0;
			foreach($transport_other_taxes as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$l].'21', (float)$SUM);	
				$l++;
			}

			$transport_bank_charges_codes = $this->listModel->costListNumber('transport_bank_charges');
			$transport_bank_charges = $this->listModel->costList($year,$column,array($transport_bank_charges_codes['cost']),$transport_bank_charges_codes['costcenter']);
			$z = 0;
			foreach($transport_bank_charges as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$z].'22', (float)$SUM);	
				$z++;
			}

			$total_transport_orders = $this->listModel->totalTransportOrders($year);
			$x = 0;
			foreach($total_transport_orders as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$x].'23', (int)$SUM['total_orders']);	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$x].'24', (int)$SUM['total_orders_pll_places']);	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$x].'25', (int)$SUM['total_weight']);	
				$x++;
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'26', ($getHandWritedSums['transport_manager'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'27', ($getHandWritedSums['trv_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'28', ($getHandWritedSums['trv_company_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'29', ($getHandWritedSums['trv_company_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'30', ($getHandWritedSums['trv_company_backup'][$i] ?: 0));	
			}

			$transport_codes = $this->listModel->costListInvoiceNumber('transport');
      $month_profit =  $this->listModel->costListInvoice($year,$column,$transport_codes);
			$c = 0;
			foreach($month_profit as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$c].'33', (float)$SUM);	
				$c++;
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'35', ($getHandWritedSums['front_forwarding_employees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'36', ($getHandWritedSums['front_forwarding_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'37', ($getHandWritedSums['front_forwarding_company_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'38', ($getHandWritedSums['front_forwarding_company_employees_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'39', ($getHandWritedSums['front_forwarding_company_backup'][$i] ?: 0));	
			}


			$hiring_transport_forwarding_codes = $this->listModel->costListNumber('hiring_transport_forwarding');
			$hiring_transport_forwarding =  $this->listModel->costList($year,$column,array($hiring_transport_forwarding_codes['cost']),$hiring_transport_forwarding_codes['costcenter']);
			
			$v3 = 0;
			foreach($hiring_transport_forwarding as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$v3].'41', (float)$SUM);	
				$v3++;
			}

			$forwarding_self_transport_codes = $this->listModel->costListNumber('forwarding_self_transport');
			$forwarding_self_transport = $this->listModel->costList($year,$column,array($forwarding_self_transport_codes['cost']),$forwarding_self_transport_codes['costcenter']);

			$v4 = 0;
			foreach($forwarding_self_transport as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$v4].'42', (float)$SUM);	
				$v4++;
			}

			$transport_ads_codes = $this->listModel->costListNumber('transport_ads');
			$transport_ads =  $this->listModel->costList($year,$column,array($transport_ads_codes['cost']),$transport_ads_codes['costcenter']);
			$v = 0;
			foreach($transport_ads as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$v].'43', (float)$SUM);	
				$v++;
			}

			$transport_other_expense_codes = $this->listModel->costListNumber('transport_other_expense');
			$transport_other_expense = $this->listModel->costList($year,$column,array($transport_other_expense_codes['cost']),$transport_other_expense_codes['costcenter']);
			$v5 = 0;
			foreach($transport_other_expense as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$v5].'44', (float)$SUM);	
				$v5++;
			}

			$forwarding_codes = $this->listModel->costListInvoiceNumber('forwarding_profit');
			$forwarding = $this->listModel->costListInvoice($year,$column,$forwarding_codes);
			$v2 = 0;
			foreach($forwarding as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$v2].'46', (float)$SUM);	
				$v2++;
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'48', ($getHandWritedSums['servis_employees_count'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'49', ($getHandWritedSums['servis_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'50', ($getHandWritedSums['servis_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'51', ($getHandWritedSums['servis_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'52', ($getHandWritedSums['servis_backup'][$i] ?: 0));	
			}


			$utilities_servis_codes = $this->listModel->costListNumber('utilities_servis');
			$utilities_servis =  $this->listModel->costList($year,$column,array($utilities_servis_codes['cost']),$utilities_servis_codes['costcenter']);
			$b = 0;
			foreach($utilities_servis as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$b].'54', (float)$SUM);	
				$b++;
			}

			$tools_servis_codes = $this->listModel->costListNumber('tools_servis');
			$tools_servis =  $this->listModel->costList($year,$column,array($tools_servis_codes['cost']),$tools_servis_codes['costcenter']);
			$n = 0;
			foreach($tools_servis as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$n].'55', (float)$SUM);	
				$n++;
			}

			$other_servis_codes = $this->listModel->costListNumber('other_servis');
			$other_servis =  $this->listModel->costList($year,$column,array($other_servis_codes['cost']),$other_servis_codes['costcenter']);
			$m = 0;
			foreach($other_servis as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$m].'56', (float)$SUM);	
				$m++;
			}

	    $parts_servis_codes = $this->listModel->costListNumber('parts_servis');
      $parts_servis =  $this->listModel->costList($year,$column,array($parts_servis_codes['cost']),$parts_servis_codes['costcenter']);  
			$q = 0;
			foreach($parts_servis as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$q].'57', (float)$SUM);	
				$q++;
			}

			$place_rent_security_servis_codes = $this->listModel->costListNumber('place_rent_security_servis');
			$place_rent_security_servis =  $this->listModel->costList($year,$column,array($place_rent_security_servis_codes['cost']),$parts_servis_codes['costcenter']);
			$w = 0;
			foreach($place_rent_security_servis as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$w].'58', (float)$SUM);	
				$w++;
			}


			$servis_codes = $this->listModel->costListInvoiceNumber('servis');
			$servis_profit =  $this->listModel->costListInvoice($year,$column,$servis_codes);
			$w22 = 0;
			foreach($servis_profit as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$w22].'60', (float)$SUM);	
				$w22++;
			}

			$r = 0;
			foreach($total_transport_orders as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$r].'62', (int)$SUM['total_parcel_orders']);	
				$r++;
			}


			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'63', ($getHandWritedSums['parcel_masters_count'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'64', ($getHandWritedSums['pm_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'65', ($getHandWritedSums['pm_company_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'66', ($getHandWritedSums['pm_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'67', ($getHandWritedSums['movement_backup'][$i] ?: 0));	
			}


			$hire_loader_movement_codes = $this->listModel->costListNumber('hire_loader_movement');
			$hire_loader_movement =  $this->listModel->costList($year,$column,array($hire_loader_movement_codes['cost']),$hire_loader_movement_codes['costcenter']);
			$t = 0;
			foreach($hire_loader_movement as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$t].'69', (float)$SUM);	
				$t++;
			}

			$hire_transport_movement_codes = $this->listModel->costListNumber('hire_transport_movement');
			$hire_transport_movement =  $this->listModel->costList($year,$column,array($hire_transport_movement_codes['cost']),$hire_transport_movement_codes['costcenter']);
			$y = 0;
			foreach($hire_transport_movement as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$y].'70', (float)$SUM);	
				$y++;
			}

			$ads_movement_codes = $this->listModel->costListNumber('ads_movement');
			$ads_movement =  $this->listModel->costList($year,$column,array($ads_movement_codes['cost']),$ads_movement_codes['costcenter']);
			$u = 0;
			foreach($ads_movement as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$u].'71', (float)$SUM);	
				$u++;
			}

			$pack_material_movement_codes = $this->listModel->costListNumber('pack_material_movement');
			$pack_material_movement =  $this->listModel->costList($year,$column,array($pack_material_movement_codes['cost']),$pack_material_movement_codes['costcenter']);
			$o = 0;
			foreach($pack_material_movement as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$o].'72', (float)$SUM);	
				$o++;
			}

			$other_movement_codes = $this->listModel->costListNumber('other_movement');
			$other_movement =  $this->listModel->costList($year,$column,array($other_movement_codes['cost']),$other_movement_codes['costcenter']);
			$p = 0;
			foreach($other_movement as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$p].'73', (float)$SUM);	
				$p++;
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'75', ($getHandWritedSums['movement_manager_count'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'76', ($getHandWritedSums['pkv_zp'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'77', ($getHandWritedSums['pkv_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'78', ($getHandWritedSums['pkv_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'79', ($getHandWritedSums['movement_manager_count_backup'][$i] ?: 0));	
			}

			// $movement_services_with_other_services = $this->listModel->movementServicesWithOtherServices($year);
			$movement_codes = $this->listModel->costListInvoiceNumber('movement');
			$movement_services_with_other_services = $this->listModel->costListInvoice($year,$column, $movement_codes);
			$q1 = 0;
			foreach($movement_services_with_other_services as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$q1].'82', (float)$SUM);	
				$q1++;
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'84', ($getHandWritedSums['employee_count_vilnius_sand'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'85', ($getHandWritedSums['vilnius_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'86', ($getHandWritedSums['vilnius_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'87', ($getHandWritedSums['vilnius_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'88', ($getHandWritedSums['employee_count_vilnius_sand'][$i] ?: 0));	
			}


			$electricity_vilnius_codes = $this->listModel->costListNumber('electricity_vilnius');
			$electricity_vilnius =  $this->listModel->costList($year,$column,array($electricity_vilnius_codes['cost']),$electricity_vilnius_codes['costcenter']);
			$w1 = 0;
			foreach($electricity_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$w1].'90', (float)$SUM);	
				$w1++;
			}

			$place_repair_vilnius_codes = $this->listModel->costListNumber('place_repair_vilnius');
			$place_repair_vilnius =  $this->listModel->costList($year,$column,array($place_repair_vilnius_codes['cost']),$place_repair_vilnius_codes['costcenter']);
			$e1 = 0;
			foreach($place_repair_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$e1].'91', (float)$SUM);	
				$e1++;
			}

			$pack_material_vilnius_codes = $this->listModel->costListNumber('pack_material_vilnius');
			$pack_material_vilnius =  $this->listModel->costList($year,$column,array($pack_material_vilnius_codes['cost']),$pack_material_vilnius_codes['costcenter']);
			$r1 = 0;
			foreach($pack_material_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$r1].'92', (float)$SUM);	
				$r1++;
			}

			$security_vilnius_codes = $this->listModel->costListNumber('security_vilnius');
			$security_vilnius =  $this->listModel->costList($year,$column,array($security_vilnius_codes['cost']),$security_vilnius_codes['costcenter']);
			$t1 = 0;
			foreach($security_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$t1].'93', (float)$SUM);	
				$t1++;
			}

			$rent_vilnius_codes = $this->listModel->costListNumber('rent_vilnius');
			$rent_vilnius =  $this->listModel->costList($year,$column,array($rent_vilnius_codes['cost']),$rent_vilnius_codes['costcenter']);
			$y1 = 0;
			foreach($rent_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$y1].'94', (float)$SUM);	
				$y1++;
			}

			$technique_rent_vilnius_codes = $this->listModel->costListNumber('technique_rent_vilnius');
			$technique_rent_vilnius =  $this->listModel->costList($year,$column,array($technique_rent_vilnius_codes['cost']),$technique_rent_vilnius_codes['costcenter']);
			$u1 = 0;
			foreach($technique_rent_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$u1].'95', (float)$SUM);	
				$u1++;
			}

			$technique_repair_parts_vilnius_codes = $this->listModel->costListNumber('technique_repair_parts_vilnius');
			$technique_repair_parts_vilnius =  $this->listModel->costList($year,$column,array($technique_repair_parts_vilnius_codes['cost']),$technique_repair_parts_vilnius_codes['costcenter']);
			$i1 = 0;
			foreach($technique_repair_parts_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$i1].'96', (float)$SUM);	
				$i1++;
			}

			$storage_vilnius_codes = $this->listModel->costListInvoiceNumber('storage_profit_vilnius');
			$storage_vilnius = $this->listModel->costListInvoice($year,$column,$storage_vilnius_codes);		
			$o1 = 0;
			foreach($storage_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$o1].'99', (float)$SUM);	
				$o1++;
			}

			$stevedoring_vilnius_codes = $this->listModel->costListInvoiceNumber('stevedoring_profit_vilnius');
			$stevedoring_vilnius = $this->listModel->costListInvoice($year,$column,$stevedoring_vilnius_codes);
			$p1 = 0;
			foreach($stevedoring_vilnius as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$p1].'100', (float)$SUM);	
				$p1++;
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'103', ($getHandWritedSums['klaipeda_sand_employee'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'104', ($getHandWritedSums['klaipeda_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'105', ($getHandWritedSums['klaipeda_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'106', ($getHandWritedSums['klaipeda_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'107', ($getHandWritedSums['klaipeda_storage_backup'][$i] ?: 0));	
			}

			$hire_employees_klaipeda_codes = $this->listModel->costListNumber('hire_employees_klaipeda');
			$hire_employees_klaipeda =  $this->listModel->costList($year,$column,array($hire_employees_klaipeda_codes['cost']),$hire_employees_klaipeda_codes['costcenter']);
			$a1 = 0;
			foreach($hire_employees_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$a1].'109', (float)$SUM);	
				$a1++;
			}

			$electricity_klaipeda_codes = $this->listModel->costListNumber('electricity_klaipeda');
			$electricity_klaipeda =  $this->listModel->costList($year,$column,array($electricity_klaipeda_codes['cost']),$electricity_klaipeda_codes['costcenter']);
			$s1 = 0;
			foreach($electricity_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$s1].'110', (float)$SUM);	
				$s1++;
			}

			$place_repair_klaipeda_codes = $this->listModel->costListNumber('place_repair_klaipeda');
			$place_repair_klaipeda =  $this->listModel->costList($year,$column,array($place_repair_klaipeda_codes['cost']),$place_repair_klaipeda_codes['costcenter']);
			$d1 = 0;
			foreach($place_repair_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$d1].'111', (float)$SUM);	
				$d1++;
			}

			$rent_klaipeda_codes = $this->listModel->costListNumber('rent_klaipeda');
			$rent_klaipeda =  $this->listModel->costList($year,$column,array($rent_klaipeda_codes['cost']),$rent_klaipeda_codes['costcenter']);
			$f1 = 0;
			foreach($rent_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$f1].'112', (float)$SUM);	
				$f1++;
			}

			$technique_rent_klaipeda_codes = $this->listModel->costListNumber('technique_rent_klaipeda');
			$technique_rent_klaipeda =  $this->listModel->costList($year,$column,array($technique_rent_klaipeda_codes['cost']),$technique_rent_klaipeda_codes['costcenter']);
			$g1 = 0;
			foreach($technique_rent_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$g1].'113', (float)$SUM);	
				$g1++;
			}

			$technique_repair_parts_klaipeda_codes = $this->listModel->costListNumber('technique_repair_parts_klaipeda');
			$technique_repair_parts_klaipeda =  $this->listModel->costList($year,$column,array($technique_repair_parts_klaipeda_codes['cost']),$technique_repair_parts_klaipeda_codes['costcenter']);
			$h1 = 0;
			foreach($technique_repair_parts_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$h1].'114', (float)$SUM);	
				$h1++;
			}

			$other_klaipeda_codes = $this->listModel->costListNumber('other_klaipeda');
			$other_klaipeda =  $this->listModel->costList($year,$column,array($other_klaipeda_codes['cost']),'KLAIPĖDOS SANDĖLIS2');
			$j1 = 0;
			foreach($other_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$j1].'115', (float)$SUM);	
				$j1++;
			}

		
			$storage_klaipeda_codes = $this->listModel->costListInvoiceNumber('storage_profit_klaipeda');
			$storage_klaipeda = $this->listModel->costListInvoice($year,$column,$storage_klaipeda_codes);
			$k1 = 0;
			foreach($storage_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$k1].'118', (float)$SUM);	
				$k1++;
			}
		
			$stevedoring_klaipeda_codes = $this->listModel->costListInvoiceNumber('stevedoring_profit_klaipeda');
			$stevedoring_klaipeda = $this->listModel->costListInvoice($year,$column,$stevedoring_klaipeda_codes);
			$l1 = 0;
			foreach($stevedoring_klaipeda as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$l1].'119', (float)$SUM);	
				$l1++;
			}


			$storage_codes = $this->listModel->costListInvoiceNumber('storage_rent');
			$storage_profit =  $this->listModel->costListInvoice($year,$column,$storage_codes);
			$mm1 = 0;
			foreach($storage_profit as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$mm1].'120', (float)$SUM);	
				$mm1++;
			}


			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'123', ($getHandWritedSums['kaunas_employee_count'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'124', ($getHandWritedSums['kaunas_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'125', ($getHandWritedSums['kaunas_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'126', ($getHandWritedSums['kaunas_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'127', ($getHandWritedSums['kaunas_storage_backup'][$i] ?: 0));	
			}



	    $electricity_kaunas_codes = $this->listModel->costListNumber('electricity_kaunas');
    	$electricity_kaunas =  $this->listModel->costList($year,$column,array($electricity_kaunas_codes['cost']),$electricity_kaunas_codes['costcenter']);
			$z1 = 0;
			foreach($electricity_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$z1].'129', (float)$SUM);	
				$z1++;
			}

			$place_repair_kaunas_codes = $this->listModel->costListNumber('place_repair_kaunas');
			$place_repair_kaunas =  $this->listModel->costList($year,$column,array($place_repair_kaunas_codes['cost']),$place_repair_kaunas_codes['costcenter']);
			$x1 = 0;
			foreach($place_repair_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$x1].'130', (float)$SUM);	
				$x1++;
			}

			$security_kaunas_codes = $this->listModel->costListNumber('security_kaunas');
			$security_kaunas =  $this->listModel->costList($year,$column,array($security_kaunas_codes['cost']),$security_kaunas_codes['costcenter']);
			$c1 = 0;
			foreach($security_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$c1].'131', (float)$SUM);	
				$c1++;
			}

			$rent_kaunas_codes = $this->listModel->costListNumber('rent_kaunas');
			$rent_kaunas =  $this->listModel->costList($year,$column,array($rent_kaunas_codes['cost']),$rent_kaunas_codes['costcenter']);
			$v1 = 0;
			foreach($rent_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$v1].'132', (float)$SUM);	
				$v1++;
			}

			$technique_rent_kaunas_codes = $this->listModel->costListNumber('technique_rent_kaunas');
			$technique_rent_kaunas =  $this->listModel->costList($year,$column,array($technique_rent_kaunas_codes['cost']),$technique_rent_kaunas_codes['costcenter']);
			$b1 = 0;
			foreach($technique_rent_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$b1].'133', (float)$SUM);	
				$b1++;
			}

			$technique_repair_parts_kaunas_codes = $this->listModel->costListNumber('technique_repair_parts_kaunas');
			$technique_repair_parts_kaunas =  $this->listModel->costList($year,$column,array($technique_repair_parts_kaunas_codes['cost']),$technique_repair_parts_kaunas_codes['costcenter']);
			$n1 = 0;
			foreach($technique_repair_parts_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$n1].'134', (float)$SUM);	
				$n1++;
			}

			$storage_kaunas_codes = $this->listModel->costListInvoiceNumber('storage_profit_kaunas');
			$storage_kaunas =  $this->listModel->costListInvoice($year,$column,$storage_kaunas_codes);
			$m1 = 0;
			foreach($storage_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$m1].'137', (float)$SUM);	
				$m1++;
			}

			$stevedoring_kaunas_codes = $this->listModel->costListInvoiceNumber('stevedoring_profit_kaunas');
			$stevedoring_kaunas = $this->listModel->costListInvoice($year,$column,$stevedoring_kaunas_codes);
			$q2 = 0;
			foreach($stevedoring_kaunas as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$q2].'138', (float)$SUM);	
				$q2++;
			}


			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'141', ($getHandWritedSums['admin_count'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'142', ($getHandWritedSums['admin_bank'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'143', ($getHandWritedSums['admin_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'144', ($getHandWritedSums['admin_employee_fees'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'145', ($getHandWritedSums['admin_storage_backup'][$i] ?: 0));	
			}


			$electricity_office_codes = $this->listModel->costListNumber('electricity_office');
			$electricity_office =  $this->listModel->costList($year,$column,array($electricity_office_codes['cost']),$electricity_office_codes['costcenter']);
			$w2 = 0;
			foreach($electricity_office as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$w2].'148', (float)$SUM);	
				$w2++;
			}

			$connection_office_codes = $this->listModel->costListNumber('connection_office');
			$connection_office =  $this->listModel->costList($year,$column,array($connection_office_codes['cost']),$connection_office_codes['costcenter']);
			$e2 = 0;
			foreach($connection_office as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$e2].'151', (float)$SUM);	
				$e2++;
			}

			$rent_repair_office_codes = $this->listModel->costListNumber('rent_repair_office');
			$rent_repair_office =  $this->listModel->costList($year,$column,array($rent_repair_office_codes['cost']),$rent_repair_office_codes['costcenter']);
			$r2 = 0;
			foreach($rent_repair_office as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$r2].'152', (float)$SUM);	
				$r2++;
			}
			
			$security_office_codes = $this->listModel->costListNumber('security_office');
			$security_office =  $this->listModel->costList($year,$column,array($security_office_codes['cost']),$security_office_codes['costcenter']);
			$t2 = 0;
			foreach($security_office as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$t2].'153', (float)$SUM);	
				$t2++;
			}
		
			$leasing_office_codes = $this->listModel->costListNumber('leasing_office');
			$leasing_office =  $this->listModel->costList($year,$column,array($leasing_office_codes['cost']),$leasing_office_codes['costcenter']);
			$y2 = 0;
			foreach($leasing_office as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$y2].'154', (float)$SUM);	
				$y2++;
			}

			
			$insurance_servis_admin_codes = $this->listModel->costListNumber('insurance_servis_admin');
			$insurance_servis_admin = $this->listModel->costList($year,$column,array($insurance_servis_admin_codes['cost']),$insurance_servis_admin_codes['costcenter']);
			$u2 = 0;
			foreach($insurance_servis_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$u2].'159', (float)$SUM);	
				$u2++;
			}

			$inventory_admin_codes = $this->listModel->costListNumber('inventory_admin');
			$inventory_admin = $this->listModel->costList($year,$column,array($inventory_admin_codes['cost']),$inventory_admin_codes['costcenter']);
			$i2 = 0;
			foreach($inventory_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$i2].'160', (float)$SUM);	
				$i2++;
			}

			$ads_admin_codes = $this->listModel->costListNumber('ads_admin');
			$ads_admin = $this->listModel->costList($year,$column,array($ads_admin_codes['cost']),$ads_admin_codes['costcenter']);
			$o2 = 0;
			foreach($ads_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$o2].'161', (float)$SUM);	
				$o2++;
			}

			$fuel_admin_codes = $this->listModel->costListNumber('fuel_admin');
			$fuel_admin = $this->listModel->costList($year,$column,array($fuel_admin_codes['cost']),$fuel_admin_codes['costcenter']);
			$p2 = 0;
			foreach($fuel_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$p2].'163', (float)$SUM);	
				$p2++;
			}

			$other_admin_codes = $this->listModel->costListNumber('other_admin');
			$other_admin = $this->listModel->costList($year,$column,array($other_admin_codes['cost']),$other_admin_codes['costcenter']);
			$a2 = 0;
			foreach($other_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$a2].'164', (float)$SUM);	
				$a2++;
			}

	    $other2_admin_codes = $this->listModel->costListNumber('other2_admin');
    	$other2_admin = $this->listModel->costList($year,$column,array($other2_admin_codes['cost']),$other2_admin_codes['costcenter']);
			$s2 = 0;
			foreach($other2_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$s2].'165', (float)$SUM);	
				$s2++;
			}

			$other_fees_admin_codes = $this->listModel->costListNumber('other_fees_admin');
			$other_fees_admin = $this->listModel->costList($year,$column,$other_fees_admin_codes['cost'],$other_fees_admin_codes['costcenter']);
			$d2 = 0;
			foreach($other_fees_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$d2].'166', (float)$SUM);	
				$d2++;
			}

			$representation_admin_codes = $this->listModel->costListNumber('representation_admin');
			$representation_admin = $this->listModel->costList($year,$column,array($representation_admin_codes['cost']),$representation_admin_codes['costcenter']);
			$f2 = 0;
			foreach($representation_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$f2].'167', (float)$SUM);	
				$f2++;
			}

			$it_admin_codes = $this->listModel->costListNumber('it_admin');
			$it_admin = $this->listModel->costList($year,$column,array($it_admin_codes['cost']),$it_admin_codes['costcenter']);
			$g2 = 0;
			foreach($it_admin as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$g2].'168', (float)$SUM);	
				$g2++;
			}

			$transport_other_expense_codes2 = $this->listModel->costListNumber('transport_other_expense2');
			$transport_other_expense2 = $this->listModel->costList($year,$column,array($transport_other_expense_codes2['cost']),$transport_other_expense_codes2['costcenter']);
			$h2 = 0;
			foreach($transport_other_expense2 as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$h2].'169', (float)$SUM);	
				$h2++;
			}

			$other_profit_codes = $this->listModel->costListInvoiceNumber('other_profit');
			$other_profit = $this->listModel->costListInvoice($year,$column,$other_profit_codes);	
			$k2 = 0;
			foreach($other_profit as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$k2].'171', (float)$SUM);	
				$k2++;
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'177', ($getHandWritedSums['working_days_number'][$i] ?: 0));	
			}

			for($len = 0; $len < $num_rows; $len++){			
				$i = $len+1;	
				$spreadsheet->getActiveSheet()->setCellValue($letters[$len].'180', ($getHandWritedSums['total_vat'][$i] ?: 0));	
			}


			$other_cost = $this->listModel->othercostList($year,$column);
			$l2 = 0;
			foreach($other_cost as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$l2].'183', (float)$SUM);	
				$l2++;
			}

			$invoice_profit_without_list = $this->listModel->invoiceProfitWithOutList($year,$column);
			$m2 = 0;
			foreach($invoice_profit_without_list as $SUM){
				$spreadsheet->getActiveSheet()->setCellValue($letters[$m2].'184', (float)$SUM);	
				$m2++;
			}


			$temp_file = tempnam(sys_get_temp_dir(), 'Excel');
			// Save EXCEL
			$writer = new Xlsx($spreadsheet);
			$writer->save($temp_file);

			$document = file_get_contents($temp_file);
			unlink($temp_file);  // delete file tmp

			$subject = 'opex_excel';

			header("Content-Disposition: attachment; filename=$subject.xlsx");
			header("Content-Type: application/excel;  charset=UTF-8");
			echo $document;
			die;

	}


}