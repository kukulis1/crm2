<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);

class Bankstatements_Module_Model extends Vtiger_Module_Model {

    public function getDebtProfitReport($dateFilter=''){
        $db = PearDatabase::getInstance();
        global $current_user;

        $month_locale = array('Jan' => 'Sausis', 'Feb' => 'Vasaris', 'Mar' => 'Kovas', 'Apr' => 'Balandis', 'May' => 'Gegužė', 'Jun' => 'Birželis', 'Jul' => 'Liepa', 'Aug' => 'Rugpjūtis', 'Sep' => 'Rugsėjis', 'Oct' => 'Spalis', 'Nov' => 'Lapkritis', 'Dec' => 'Gruodis');
        if(empty($dateFilter)) {
            $month = date("M");	
        }else{
            $month = date("M",strtotime($dateFilter['start']));	
        }
       
        $month = $month_locale[$month];
        $show_filter = "Pasirinktas menuo: ".$month; 	

        if(empty($dateFilter)) {
            $date_from = date('Y-m-d', strtotime('first day of this month')) ;
            $date_to =  date("Y-m-d", strtotime("last day of this month"));  
        }else{
            $date_from = date('Y-m-01', strtotime($dateFilter['start']));
            $date_to = date('Y-m-t', strtotime($dateFilter['start']));  
        }   

        $bank_revenue_sql = "SELECT bankstatements_tks_amount, bankstatements_tks_date
                            FROM vtiger_bankstatements s
                            LEFT JOIN vtiger_bankstatementscf sc ON sc.bankstatementsid=s.bankstatementsid
                            INNER JOIN vtiger_crmentity e ON e.crmid=s.bankstatementsid
                            WHERE (bankstatements_tks_date BETWEEN ? AND ?) AND cf_2084 = 'CRDT' AND deleted = 0 ";

        $bank_revenue_execute = $db->pquery($bank_revenue_sql,array($date_from,$date_to)); 

        $bank_revenue = array();    
        foreach ($bank_revenue_execute as $amount) {
            $bank_revenue[$amount['bankstatements_tks_date']] += $amount['bankstatements_tks_amount'];
        }


        $bank_payments_sql = "SELECT bankstatements_tks_amount, bankstatements_tks_date
                              FROM vtiger_bankstatements s
                              LEFT JOIN vtiger_bankstatementscf sc ON sc.bankstatementsid=s.bankstatementsid
                              INNER JOIN vtiger_crmentity e ON e.crmid=s.bankstatementsid
                              WHERE (bankstatements_tks_date BETWEEN ? AND ?) AND cf_2084 = 'DBIT' AND deleted = 0 ";   
                            
        $bank_payment_execute = $db->pquery($bank_payments_sql,array($date_from,$date_to)); 
        $bank_payments = array();  
        
        foreach ($bank_payment_execute as $amount) {
            $bank_payments[$amount['bankstatements_tks_date']] += $amount['bankstatements_tks_amount'];
        }                   


        $clients_debt_sql = "SELECT total, DATE_ADD(vtiger_invoice.duedate, INTERVAL 1 DAY) AS duedate, vtiger_invoice.invoiceid
                            FROM vtiger_invoice 
                            LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                            LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                            INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND setype = 'Invoice'
                            WHERE (vtiger_invoice.duedate BETWEEN ? AND ?) AND debts_tks_suma IS NULL AND  vtiger_crmentity.deleted=0  
                            GROUP BY vtiger_invoice.invoiceid "; 

        $clients_debt_execute = $db->pquery($clients_debt_sql,array($date_from,$date_to)); 
        $client_debt = array();
        $client_debt2 = array();

        foreach ($clients_debt_execute as $amount) {
            $client_debt[$amount['duedate']]['total'][] = $amount['total'];
            $client_debt[$amount['duedate']]['invoiceid'][] = $amount['invoiceid'];
            $client_debt2[$amount['duedate']] += $amount['total'];     
        }                    
                            
                            
        $bank_revenue_plan_sql = "SELECT IF(i.region_id = 5,subtotal,total) AS total,duedate,i.invoiceid
                                  FROM vtiger_invoice i 
                                  INNER JOIN vtiger_crmentity e ON e.crmid=i.invoiceid AND setype = 'Invoice'
                                  WHERE (duedate BETWEEN ? AND ? ) AND deleted = 0  
                                  GROUP BY i.invoiceid";


        $bank_revenue_plan_execute = $db->pquery($bank_revenue_plan_sql,array($date_from,$date_to));

        $revenue_plan = array();

        foreach ($bank_revenue_plan_execute as $amount) {
            $revenue_plan[$amount['duedate']]['total'][] = $amount['total'];
            $revenue_plan[$amount['duedate']]['invoiceid'][] = $amount['invoiceid'];
        }



        $payments_plan_sql = "SELECT total, duedate, vtiger_purchaseorder.purchaseorderid
                              FROM vtiger_purchaseorder  
                              LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseorder.purchaseorderid=vtiger_purchaseordercf.purchaseorderid
                              INNER JOIN vtiger_crmentity e ON e.crmid=vtiger_purchaseorder.purchaseorderid AND setype = 'PurchaseOrder'
                              WHERE (duedate BETWEEN ? AND ?) AND deleted = 0 AND cf_1343 NOT IN ('Išankstinė', 'Kreditinė') "; 


        $payments_plan_execute = $db->pquery($payments_plan_sql,array($date_from,$date_to));

        $payments_plan = array();

        foreach ($payments_plan_execute as $amount) {
            $payments_plan[$amount['duedate']]['total'][] = $amount['total'];
            $payments_plan[$amount['duedate']]['purchaseorderid'][] = $amount['purchaseorderid'];
        }                                   
                            
        $overdue_payments_sql2 = "SELECT CASE 
                                        WHEN payment_tks_suma IS NULL
                                        THEN ROUND(total,2)
                                        ELSE ROUND((total - SUM(vtiger_payment.payment_tks_suma)),2)
                                    END AS total_debt, vtiger_purchaseorder.duedate, MAX(payment_tks_mokėjimodata) AS payment
                                FROM vtiger_purchaseorder 
                                LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseorder.purchaseorderid=vtiger_purchaseordercf.purchaseorderid
                                LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid AND relmodule = 'Payment'	
                                LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid  				 
                                INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid  AND setype = 'PurchaseOrder' 
                                WHERE vtiger_purchaseorder.duedate < ? AND cf_1343 NOT IN ('Išankstinė', 'Kreditinė') AND  vtiger_crmentity.deleted=0 
                                GROUP BY vtiger_purchaseorder.purchaseorderid   
                               
                                HAVING total_debt > 0 AND (vtiger_purchaseorder.duedate < payment OR (payment IS NULL AND vtiger_purchaseorder.duedate < CURRENT_DATE())) ";

        $overdue_payments_execute = $db->pquery($overdue_payments_sql2,array($date_from)); 
        $total_debt_payments = 0;

        foreach ($overdue_payments_execute as $amount) {
            $total_debt_payments += $amount['total_debt'];
        }  
    

        $parnasas_debts_sql = "SELECT CASE 
                                        WHEN payment_tks_suma IS NULL
                                        THEN ROUND(total,2)
                                        ELSE ROUND((total - SUM(vtiger_payment.payment_tks_suma)),2)
                                    END AS period_debt,  vtiger_purchaseorder.duedate, MAX(payment_tks_mokėjimodata) AS payment
                                FROM vtiger_purchaseorder 
                                LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseorder.purchaseorderid=vtiger_purchaseordercf.purchaseorderid
                                LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid AND relmodule = 'Payment'
                                LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 					 
                                INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid  AND setype = 'PurchaseOrder'
                                WHERE (vtiger_purchaseorder.duedate BETWEEN ? AND ?) AND cf_1343 NOT IN ('Išankstinė', 'Kreditinė') AND vtiger_crmentity.deleted=0
                                GROUP BY vtiger_purchaseorder.purchaseorderid	
                                HAVING period_debt > 0 AND (vtiger_purchaseorder.duedate < payment OR (payment IS NULL AND vtiger_purchaseorder.duedate < CURRENT_DATE())) "; 
                                                            
        $parnasas_debts_execute = $db->pquery($parnasas_debts_sql,array($date_from,$date_to));

        $parnasas_debt = array(); 
        
        foreach ($parnasas_debts_execute as $amount) {
            $parnasas_debt[$amount['duedate']] += $amount['period_debt'];
        }  
        
        
        $data = array();    
        
    
        $period = $this->getDatesFromRange($date_from, $date_to);  

        $period_arr = array();
        $period_arr2 = array();

        // NOTE Bank balance
        $bank_balance_sql = "SELECT SUM(balance) as balance, xml_file_date as date
                             FROM app_bank_balances
                             WHERE xml_file_date BETWEEN ? AND ?
                             GROUP BY xml_file_date";

        $bank_balance_sql_results = $db->pquery($bank_balance_sql, [$date_from, $date_to]); 
        $bank_balance_results = [];
        foreach($bank_balance_sql_results as $sql_result){
            $bank_balance_results[$sql_result['date']] = $sql_result['balance'];
        }

        // NOTE Total sum for invoices before period
        $total_sum_before_period_sql = "SELECT CASE 
                                                WHEN debts_tks_suma IS NULL
                                                THEN round(IF(vtiger_invoice.region_id = 5,subtotal,total),2)
                                                ELSE round((IF(vtiger_invoice.region_id = 5,subtotal,total) - SUM(vtiger_debts.debts_tks_suma)),2)
                                            END AS total_debt
                                        FROM vtiger_invoice
                                        JOIN vtiger_invoicecf 
                                            ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                        LEFT JOIN vtiger_crmentityrel 
                                            ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                        LEFT JOIN vtiger_debts 
                                            ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                                        INNER JOIN vtiger_crmentity 
                                            ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid  AND setype = 'Invoice'   
                                        WHERE vtiger_crmentity.deleted = 0 
                                            AND vtiger_invoice.invoicedate < ? AND vtiger_invoicecf.cf_1277 = 'Debetinė' 
                                        GROUP BY vtiger_invoice.invoiceid
                                        HAVING total_debt > 0";
                                            
        $total_sum_before_period_sql_results = $db->pquery($total_sum_before_period_sql, [$date_from]);
        $total_debt = 0;
        foreach($total_sum_before_period_sql_results as $debt){
            $total_debt += $debt['total_debt'];
        }
       

        // NOTE New debts in period
        $period_new_debts_sql = "SELECT CASE 
                                            WHEN debts_tks_suma IS NULL
                                            THEN round(IF(vtiger_invoice.region_id = 5,subtotal,total),2)
                                            ELSE round((IF(vtiger_invoice.region_id = 5,subtotal,total) - SUM(vtiger_debts.debts_tks_suma)),2)
                                        END AS period_debt, vtiger_invoice.invoicedate 
                                    FROM vtiger_invoice 
                                    JOIN vtiger_invoicecf 
                                        ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                    LEFT JOIN vtiger_crmentityrel 
                                        ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                    LEFT JOIN vtiger_debts 
                                        ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 	
                                    INNER JOIN vtiger_crmentity 
                                        ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   AND setype = 'Invoice' 
                                    WHERE vtiger_crmentity.deleted = 0 
                                        AND vtiger_invoice.invoicedate >= ?
                                        AND vtiger_invoice.invoicedate <= ? AND vtiger_invoicecf.cf_1277 = 'Debetinė'
                                        GROUP BY vtiger_invoice.invoiceid
                                        HAVING period_debt > 0 ";
        
        $period_new_debts_sql_results = $db->pquery($period_new_debts_sql, [$date_from, $date_to]);
        $period_new_debts = [];

        foreach($period_new_debts_sql_results as $sql_result){
            $period_new_debts[$sql_result['invoicedate']] += $sql_result['period_debt'];
        }



        // NOTE Total sum of overdue before period
        $total_sum_overdue_before_period_sql = "SELECT CASE 
                                                        WHEN debts_tks_suma IS NULL
                                                        THEN round(IF(vtiger_invoice.region_id = 5,subtotal,total),2)
                                                        ELSE round((IF(vtiger_invoice.region_id = 5,subtotal,total) - SUM(vtiger_debts.debts_tks_suma)),2)
                                                    END AS total_debt, vtiger_invoice.duedate, MAX(debts_tks_data) AS payment
                                                    FROM vtiger_invoice
                                                    JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                                    LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                                    LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                                                    INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND setype = 'Invoice' 
                                                    WHERE vtiger_invoice.duedate < ? AND vtiger_invoicecf.cf_1277 = 'Debetinė' AND vtiger_crmentity.deleted = 0
                                                    GROUP BY vtiger_invoice.invoiceid
                                                    HAVING total_debt > 0 AND (vtiger_invoice.duedate < payment OR (payment IS NULL AND vtiger_invoice.duedate < CURRENT_DATE())) ";

        $total_sum_overdue_before_period_sql_results = $db->pquery($total_sum_overdue_before_period_sql, [$date_from]);
        // $total_overdue_debt = $db->query_result($total_sum_overdue_before_period_sql_results, 0, 'total');
        $total_overdue_debt = 0;
        foreach($total_sum_overdue_before_period_sql_results as $sql_result){
            $total_overdue_debt += $sql_result['total_debt'];
        }    
        
        // NOTE New overdue debts in period
        $period_new_overdue_debts_sql = "SELECT CASE 
                                                    WHEN debts_tks_suma IS NULL
                                                    THEN round(IF(vtiger_invoice.region_id = 5,subtotal,total),2)
                                                    ELSE round((IF(vtiger_invoice.region_id = 5,subtotal,total) - SUM(vtiger_debts.debts_tks_suma)),2)
                                                END AS period_debt, vtiger_invoice.duedate, MAX(debts_tks_data) AS payment
                                            FROM vtiger_invoice 
                                            JOIN vtiger_invoicecf 
                                                            ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                            LEFT JOIN vtiger_crmentityrel 
                                                ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                            LEFT JOIN vtiger_debts 
                                                ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 		
                                            INNER JOIN vtiger_crmentity 
                                                ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND setype = 'Invoice' 
                                            WHERE (vtiger_invoice.duedate >= ? AND vtiger_invoice.duedate <= ?)  
                                            AND vtiger_invoicecf.cf_1277 = 'Debetinė' AND vtiger_crmentity.deleted = 0
                                            GROUP BY vtiger_invoice.invoiceid
                                            HAVING period_debt > 0 AND (vtiger_invoice.duedate < payment OR (payment IS NULL AND vtiger_invoice.duedate < CURRENT_DATE()))";

        $period_new_overdue_debts_sql_results = $db->pquery($period_new_overdue_debts_sql, [$date_from, $date_to]);
        $period_new_overdue_debts = [];
        foreach($period_new_overdue_debts_sql_results as $sql_result){
            $period_new_overdue_debts[$sql_result['duedate']] += $sql_result['period_debt'];
        }
        
        foreach ($period AS $value) {	 
            $order_date = $value->format('Y-m-d');   

            $order_week_day = $value->format('l');            
            if($order_week_day != 'Saturday' && $order_week_day != 'Sunday'){    
                $period_arr[] = $order_date;
                $period_arr2[] = $value->format('m.d'); 
            }

            $data['revenue'][$order_date] =  ($bank_revenue[$order_date] ?: 0);      
            $data['payments'][$order_date] =  ($bank_payments[$order_date] ?: 0);      
            $data['client_debt']['SUM'][$order_date] =  ($client_debt[$order_date]['total'] ? array_sum($client_debt[$order_date]['total']) : 0); 
            $data['client_debt']['invoiceid'][$order_date] =  ($client_debt[$order_date]['invoiceid'] ? implode(',',$client_debt[$order_date]['invoiceid']) : ''); 
            $data['turnover'][$order_date] = ($bank_balance_results[$order_date] ?: 0);
        
            $total_debt += ($period_new_debts[$order_date] ?: 0);          
            $data['total_debt'][$order_date] = round($total_debt,2);
            

            $total_overdue_debt += ($period_new_overdue_debts[$order_date] ?: 0);                
            $data['total_overdue_debt'][$order_date] = round($total_overdue_debt, 2);


            $total_debt_payments += ($parnasas_debt[$order_date] ?: 0);     
            $data['total_debt_payments'][$order_date] = $total_debt_payments;

            $data['revenue_plan']['SUM'][$order_date] = round(($revenue_plan[$order_date]['total'] ? array_sum($revenue_plan[$order_date]['total']) : 0),2); 
            $data['revenue_plan']['invoiceid'][$order_date] = ($revenue_plan[$order_date]['invoiceid'] ? implode(',',$revenue_plan[$order_date]['invoiceid']) : 0); 


            $data['payments_plan']['SUM'][$order_date] = ($payments_plan[$order_date]['total'] ? array_sum($payments_plan[$order_date]['total']) : 0); 
            $data['payments_plan']['purchaseorderid'][$order_date] = ($payments_plan[$order_date]['purchaseorderid'] ? implode(',',$payments_plan[$order_date]['purchaseorderid']) : 0); 
        }  
        

        return array('role' => $current_user->roleid, 'period' => $period_arr,'period2' => $period_arr2,'filter' => $show_filter,'data' => $data);
    }

    function getDatesFromRange($start, $end) { 
        $array = array();  
        $interval = new DateInterval('P1D');  
        $realEnd = new DateTime($end); 
        $realEnd->add($interval);   
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
        foreach($period as $date) {                  
            $array[] = $date;  
        }  
        return $period; 
	} 
}