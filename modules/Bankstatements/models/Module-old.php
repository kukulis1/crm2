<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);

class Bankstatements_Module_Model extends Vtiger_Module_Model {

    public function getDebtProfitReport($dateFilter=''){
        $db = PearDatabase::getInstance();
        global $current_user;

        $month_locale = array('Jan' => 'Sausis', 'Feb' => 'Vasaris', 'Mar' => 'Kovas', 'Apr' => 'Balandis', 'May' => 'Gegužė', 'Jun' => 'Birželis', 'Jul' => 'Liepa', 'Aug' => 'Rugpjūtis', 'Sep' => 'Rugsėjis', 'Oct' => 'Spalis', 'Nov' => 'Lapkritis', 'Dec' => 'Gruodis');
        if(empty($dateFilter)) {
            $month = date("M");	
        }else{
            $month = date("M",strtotime($dateFilter['start']));	
        }
       
        $month = $month_locale[$month];
        $show_filter = "Pasirinktas menuo: ".$month; 	

        if(empty($dateFilter)) {
            $date_from = date('Y-m-d', strtotime('first day of this month')) ;
            $date_to =  date("Y-m-d", strtotime("last day of this month"));  
        }else{
            $date_from = date('Y-m-01', strtotime($dateFilter['start']));
            $date_to = date('Y-m-t', strtotime($dateFilter['start']));  
        }
        // $date_from = '2021-08-09';
        // $date_to = '2021-08-13';

        $bank_revenue_sql = "SELECT bankstatements_tks_amount, bankstatements_tks_date
                            FROM vtiger_bankstatements s
                            LEFT JOIN vtiger_bankstatementscf sc ON sc.bankstatementsid=s.bankstatementsid
                            INNER JOIN vtiger_crmentity e ON e.crmid=s.bankstatementsid
                            WHERE (bankstatements_tks_date BETWEEN ? AND ?) AND cf_2084 = 'CRDT' AND deleted = 0 ";

        $bank_payments_sql = "SELECT bankstatements_tks_amount, bankstatements_tks_date
                            FROM vtiger_bankstatements s
                            LEFT JOIN vtiger_bankstatementscf sc ON sc.bankstatementsid=s.bankstatementsid
                            INNER JOIN vtiger_crmentity e ON e.crmid=s.bankstatementsid
                            WHERE (bankstatements_tks_date BETWEEN ? AND ?) AND cf_2084 = 'DBIT' AND deleted = 0 ";                    

        $clients_debt_sql = "SELECT total, DATE_ADD(vtiger_invoice.duedate, INTERVAL 1 DAY) AS duedate, vtiger_invoice.invoiceid
                            FROM vtiger_invoice 
                            LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                            LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                            INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND setype = 'Invoice'
                            WHERE (vtiger_invoice.duedate BETWEEN ? AND ?) AND debts_tks_suma IS NULL AND  vtiger_crmentity.deleted=0  
                            GROUP BY vtiger_invoice.invoiceid "; 

        $total_overdue_sql = "SELECT SUM(total) AS total
                            FROM vtiger_invoice 
                            LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                            LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                            INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND setype = 'Invoice'  
                            WHERE debts_tks_suma IS NULL AND  vtiger_crmentity.deleted=0  ";                       


                                                        
        $overdue_debts_sql = "SELECT CASE 
                                    WHEN debts_tks_suma IS NULL
                                    THEN total
                                    ELSE SUM(vtiger_debts.debts_tks_suma)
                                    END AS debt, total, DATE_ADD(vtiger_invoice.duedate, INTERVAL 1 DAY) AS duedate, MAX(debts_tks_data) AS pay_date
                            FROM vtiger_invoice 
                            LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
                            LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                            INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND setype = 'Invoice'  
                            WHERE duedate < pay_date  AND (pay_date BETWEEN ? AND ?) AND  vtiger_crmentity.deleted=0 
                            GROUP BY vtiger_invoice.invoiceid";   
        
        $overdue_debts_sql2 = "SELECT SUM(total) AS total
                                FROM vtiger_invoice LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
                                LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                                INNER JOIN vtiger_crmentity ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid AND setype = 'Invoice'
                                WHERE debts_tks_data IS NULL AND duedate < ?  AND vtiger_crmentity.deleted=0";



        $bank_revenue_plan_sql = "SELECT total,duedate,i.invoiceid
                                FROM vtiger_invoice i 
                                INNER JOIN vtiger_crmentity e ON e.crmid=i.invoiceid AND setype = 'Invoice'
                                WHERE (duedate BETWEEN ? AND ? ) AND deleted = 0  
                                GROUP BY i.invoiceid";


        $payments_plan_sql = "SELECT total, duedate ,purchaseorderid
                        FROM vtiger_purchaseorder p 
                        INNER JOIN vtiger_crmentity e ON e.crmid=p.purchaseorderid AND setype = 'PurchaseOrder'
                        WHERE (duedate BETWEEN ? AND ?) AND deleted = 0 ";


        $overdue_payments_sql = "SELECT CASE 
                                        WHEN payment_tks_suma IS NULL
                                        THEN ROUND(total,2)
                                        ELSE ROUND((total - SUM(vtiger_payment.payment_tks_suma)),2)
                                        END AS debt, total,payment_tks_mokėjimodata, vtiger_purchaseorder.duedate,payment_tks_mokėjimodata AS pay_date
                                    FROM vtiger_purchaseorder 
                                    LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid 
                                    LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 					 
                                    INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid   
                                    WHERE vtiger_crmentity.deleted=0 AND duedate < payment_tks_mokėjimodata  AND payment_tks_mokėjimodata BETWEEN ? AND ?
                                    GROUP BY vtiger_purchaseorder.purchaseorderid	
                                    HAVING  payment_tks_mokėjimodata IS NOT NULL ";  
                                    
                                    
        $parnasas_debts_sql = "SELECT CASE 
                                    WHEN payment_tks_suma IS NULL
                                    THEN ROUND(total,2)
                                    ELSE ROUND((total - SUM(vtiger_payment.payment_tks_suma)),2)
                                END AS debt, total,payment_tks_mokėjimodata,payment_tks_suma, vtiger_purchaseorder.duedate, MAX(payment_tks_mokėjimodata) AS pay_date
                            FROM vtiger_purchaseorder 
                            LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid AND relmodule = 'Payment'
                            LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 					 
                            INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid  AND setype = 'PurchaseOrder'
                            WHERE vtiger_crmentity.deleted=0 AND  duedate BETWEEN ? AND ?
                            GROUP BY vtiger_purchaseorder.purchaseorderid	
                            HAVING debt > 0 OR pay_date IS NULL ";     


        $overdue_peyments_sql2 = "SELECT total
                                  FROM vtiger_purchaseorder 
                                  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid AND relmodule = 'Payment'	
                                  LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid  				 
                                  INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid  AND setype = 'PurchaseOrder' 
                                  WHERE duedate < ?  AND vtiger_crmentity.deleted=0
                                  GROUP BY vtiger_purchaseorder.purchaseorderid  
                                  HAVING MAX(payment_tks_mokėjimodata) IS NULL
                                  ORDER BY purchaseorderid DESC";



        $data = array();  
        $period_arr = array();
        $bank_revenue = array();  
        $bank_payments = array();  
        $client_debt = array();
        $client_debt2 = array();
        $overdue_debts = array();
        $revenue_plan = array();
        $payments_plan = array();
        $overdue_payments = array();
        $parnasas_debt = array();

        $result = $db->pquery($bank_revenue_sql,array($date_from,$date_to)); 
        $result2 = $db->pquery($bank_payments_sql,array($date_from,$date_to)); 
        $result3 = $db->pquery($clients_debt_sql,array($date_from,$date_to)); 
        // $result4 = $db->pquery($overdue_debts_sql2,array($date_from));
        // $result5 = $db->pquery($overdue_debts_sql,array($date_from,$date_to));
        $result6 = $db->pquery($bank_revenue_plan_sql,array($date_from,$date_to));
        $result7 = $db->pquery($payments_plan_sql,array($date_from,$date_to));

        $result8 = $db->pquery($overdue_peyments_sql2,array($date_from));
        // $result9 = $db->pquery($overdue_payments_sql,array($date_from,$date_to));
        $result10 = $db->pquery($parnasas_debts_sql,array($date_from,$date_to));
        // $result11 = $db->pquery($total_overdue_sql,array());

        // $total = $db->query_result($result4,0,'total');        
        // $total_overdue = $db->query_result($result11,0,'total');

        $total_debt_payments = 0;

        foreach ($result8 as $amount) {
            $total_debt_payments += $amount['total'];
        }



        foreach ($result as $amount) {
            $bank_revenue[$amount['bankstatements_tks_date']] += $amount['bankstatements_tks_amount'];
        }

        foreach ($result2 as $amount) {
            $bank_payments[$amount['bankstatements_tks_date']] += $amount['bankstatements_tks_amount'];
        }    

        foreach ($result3 as $amount) {
            $client_debt[$amount['duedate']]['total'][] = $amount['total'];
            $client_debt[$amount['duedate']]['invoiceid'][] = $amount['invoiceid'];
            $client_debt2[$amount['duedate']] += $amount['total'];     
        }

        // foreach ($result5 as $amount) {
        //     $overdue_debts[$amount['pay_date']] += $amount['debt'];
        // }

        foreach ($result6 as $amount) {
            $revenue_plan[$amount['duedate']]['total'][] = $amount['total'];
            $revenue_plan[$amount['duedate']]['invoiceid'][] = $amount['invoiceid'];
        }

        foreach ($result7 as $amount) {
            $payments_plan[$amount['duedate']]['total'][] = $amount['total'];
            $payments_plan[$amount['duedate']]['purchaseorderid'][] = $amount['purchaseorderid'];
        }

        // foreach ($result9 as $amount) {
        //     $overdue_payments[$amount['pay_date']] += $amount['total'];
        // }

        foreach ($result10 as $amount) {
            $parnasas_debt[$amount['duedate']] += $amount['debt'];
        }
    
        $period = $this->getDatesFromRange($date_from, $date_to);  

        $period_arr = array();
        $period_arr2 = array();


        // $total_debt = $total;
        // $total_overdue_debt = $total_overdue;


        // NOTE Bank balance
        $bank_balance_sql = "   SELECT SUM(balance) as balance, xml_file_date as date
                                FROM app_bank_balances
                                WHERE xml_file_date BETWEEN ? AND ?
                                GROUP BY xml_file_date";
        $bank_balance_sql_results = $db->pquery($bank_balance_sql, [$date_from, $date_to]); 
        $bank_balance_results = [];
        foreach($bank_balance_sql_results as $sql_result){
            $bank_balance_results[$sql_result['date']] = $sql_result['balance'];
        }

        // NOTE Total sum for invoices before period
        $total_sum_before_period_sql = "SELECT SUM(vtiger_invoice.total) as total
                                        FROM vtiger_invoice
                                        JOIN vtiger_invoicecf 
                                            ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                        LEFT JOIN vtiger_crmentityrel 
                                            ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                        LEFT JOIN vtiger_debts 
                                            ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                                        INNER JOIN vtiger_crmentity 
                                            ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid  AND setype = 'Invoice'   
                                        WHERE vtiger_crmentity.deleted = 0 
                                            AND vtiger_invoice.duedate < ? AND cf_1277 = 'Debetinė' AND vtiger_debts.debts_tks_data IS NULL";
                                            
        $total_sum_before_period_sql_results = $db->pquery($total_sum_before_period_sql, [$date_from]);
        $total_debt = $db->query_result($total_sum_before_period_sql_results, 0, 'total');

        // NOTE Total paid sum before period
        // $total_paid_sum_before_period_sql = "   SELECT SUM(vtiger_debts.debts_tks_suma) as total
        //                                         FROM vtiger_invoice
        //                                         LEFT JOIN vtiger_crmentityrel 
        //                                             ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
        //                                         INNER JOIN vtiger_debts 
        //                                             ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
        //                                         INNER JOIN vtiger_crmentity 
        //                                             ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   
        //                                         WHERE vtiger_crmentity.deleted = 0 
        //                                             AND vtiger_debts.debts_tks_data < ?";
        // $total_paid_sum_before_period_sql_results = $db->pquery($total_paid_sum_before_period_sql, [$date_from]);
        // $total_paid_sum_before_period = $db->query_result($total_paid_sum_before_period_sql_results, 0, 'total');

        // NOTE New debts in period
        $period_new_debts_sql = "   SELECT vtiger_invoice.total, vtiger_invoice.duedate 
                                    FROM vtiger_invoice 
                                    JOIN vtiger_invoicecf 
                                        ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                    LEFT JOIN vtiger_crmentityrel 
                                        ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                    LEFT JOIN vtiger_debts 
                                        ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 	
                                    INNER JOIN vtiger_crmentity 
                                        ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   AND setype = 'Invoice' 
                                    WHERE vtiger_crmentity.deleted = 0 
                                        AND vtiger_invoice.duedate >= ?
                                        AND vtiger_invoice.duedate <= ? AND cf_1277 = 'Debetinė' AND vtiger_debts.debts_tks_data IS NULL ";
        $period_new_debts_sql_results = $db->pquery($period_new_debts_sql, [$date_from, $date_to]);
        $period_new_debts = [];
        foreach($period_new_debts_sql_results as $sql_result){
            $period_new_debts[$sql_result['duedate']] += $sql_result['total'];
        }

        // NOTE New paid debts in period
        // $period_new_paid_debts_sql = "  SELECT vtiger_debts.debts_tks_suma as total, vtiger_debts.debts_tks_data AS pay_date
        //                                 FROM vtiger_invoice
        //                                 JOIN vtiger_invoicecf 
        //                                     ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
        //                                 LEFT JOIN vtiger_crmentityrel 
        //                                     ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
        //                                 LEFT JOIN vtiger_debts 
        //                                     ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
        //                                 INNER JOIN vtiger_crmentity 
        //                                     ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   AND setype = 'Invoice' 
        //                                 WHERE vtiger_crmentity.deleted = 0 
        //                                     AND vtiger_debts.debts_tks_data >= ?
        //                                     AND vtiger_debts.debts_tks_data <= ? AND cf_1277 = 'Debetinė'";
        // $period_new_paid_debts_sql_results = $db->pquery($period_new_paid_debts_sql, [$date_from, $date_to]);
        // $period_new_paid_debts = [];
        // foreach($period_new_paid_debts_sql_results as $sql_result){
        //     $period_new_paid_debts[$sql_result['pay_date']] += $sql_result['total'];
        // }

        // NOTE Total sum of overdue before period
        $total_sum_overdue_before_period_sql = "SELECT SUM(vtiger_invoice.total) as total
                                                FROM vtiger_invoice
                                                JOIN vtiger_invoicecf 
                                                     ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                                LEFT JOIN vtiger_crmentityrel 
                                                    ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                                LEFT JOIN vtiger_debts 
                                                    ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                                                INNER JOIN vtiger_crmentity 
                                                    ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   AND setype = 'Invoice' 
                                                WHERE vtiger_crmentity.deleted = 0 
                                                    AND vtiger_invoice.duedate < ? AND cf_1277 = 'Debetinė' AND vtiger_debts.debts_tks_data IS NULL";
        $total_sum_overdue_before_period_sql_results = $db->pquery($total_sum_overdue_before_period_sql, [$date_from]);
        $total_overdue_debt = $db->query_result($total_sum_overdue_before_period_sql_results, 0, 'total');

        // NOTE Total sum of paid overdue before period
        // $total_sum_paid_overdue_before_period_sql = "   SELECT SUM(vtiger_debts.debts_tks_suma) as total
        //                                                 FROM vtiger_invoice
        //                                                 JOIN vtiger_invoicecf 
        //                                                     ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
        //                                                 LEFT JOIN vtiger_crmentityrel 
        //                                                     ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
        //                                                 INNER JOIN vtiger_debts 
        //                                                     ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
        //                                                 INNER JOIN vtiger_crmentity 
        //                                                     ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   
        //                                                 WHERE vtiger_crmentity.deleted = 0 
        //                                                     AND vtiger_invoice.duedate < ? AND cf_1277 = 'Debetinė'";
        // $total_sum_paid_overdue_before_period_sql_results = $db->pquery($total_sum_paid_overdue_before_period_sql, [$date_from]);
        // $total_sum_paid_overdue_before_period = $db->query_result($total_sum_paid_overdue_before_period_sql_results, 0, 'total');




        // NOTE New overdue debts in period
        $period_new_overdue_debts_sql = "   SELECT vtiger_invoice.total, vtiger_invoice.duedate AS date
                                            FROM vtiger_invoice 
                                            JOIN vtiger_invoicecf 
                                                            ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                            LEFT JOIN vtiger_crmentityrel 
                                                ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                            INNER JOIN vtiger_crmentity 
                                                ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   AND setype = 'Invoice' 
                                            WHERE vtiger_crmentity.deleted = 0 
                                                AND vtiger_invoice.duedate >= ?
                                                AND vtiger_invoice.duedate <= ? 
                                                AND cf_1277 = 'Debetinė' AND max(vtiger_debts.debts_tks_data) IS NULL";

        $period_new_overdue_debts_sql_results = $db->pquery($period_new_overdue_debts_sql, [$date_from, $date_to]);
        $period_new_overdue_debts = [];
        foreach($period_new_overdue_debts_sql_results as $sql_result){
            $period_new_overdue_debts[$sql_result['date']] += $sql_result['total'];
        }

        // NOTE New paid overdue debts in period
        $period_new_paid_overdue_debts_sql = "  SELECT vtiger_debts.debts_tks_suma as total, vtiger_debts.debts_tks_data AS date
                                                FROM vtiger_invoice
                                                JOIN vtiger_invoicecf 
                                                            ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                                LEFT JOIN vtiger_crmentityrel 
                                                    ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid AND relmodule = 'Debts'
                                                INNER JOIN vtiger_debts 
                                                    ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 					 
                                                INNER JOIN vtiger_crmentity 
                                                    ON vtiger_invoice.invoiceid = vtiger_crmentity.crmid   AND setype = 'Invoice' 
                                                WHERE vtiger_crmentity.deleted = 0 
                                                    AND vtiger_debts.debts_tks_data >= vtiger_invoice.duedate
                                                    AND vtiger_debts.debts_tks_data >=  ?
                                                    AND vtiger_debts.debts_tks_data <= ?";
        // $period_new_paid_overdue_debts_sql_results = $db->pquery($period_new_paid_overdue_debts_sql, [$date_from, $date_to]);
        // $period_new_paid_overdue_debts = [];
        // foreach($period_new_paid_overdue_debts_sql_results as $sql_result){
        //     $period_new_paid_overdue_debts[$sql_result['date']] += $sql_result['total'];
        // }

        // $total_debt = $total_sum_before_period - $total_paid_sum_before_period;
        // $total_overdue_debt = $total_sum_overdue_before_period - $total_sum_paid_overdue_before_period;
        
        foreach ($period as $key => $value) {	 
            $order_date = $value->format('Y-m-d');   

            $order_week_day = $value->format('l');            
            if($order_week_day != 'Saturday' && $order_week_day != 'Sunday'){    
                $period_arr[] = $order_date;
                $period_arr2[] = $value->format('m.d'); 

                $data['revenue'][$order_date] =  ($bank_revenue[$order_date] ?: 0);      
                $data['payments'][$order_date] =  ($bank_payments[$order_date] ?: 0);      
                $data['client_debt']['SUM'][$order_date] =  ($client_debt[$order_date]['total'] ? array_sum($client_debt[$order_date]['total']) : 0); 
                $data['client_debt']['invoiceid'][$order_date] =  ($client_debt[$order_date]['invoiceid'] ? implode(',',$client_debt[$order_date]['invoiceid']) : ''); 
                $data['turnover'][$order_date] = ($bank_balance_results[$order_date] ?: 0);
            
                $total_debt += ($period_new_debts[$order_date] ?: 0);
                // $total_debt -= ($period_new_paid_debts[$order_date] ?: 0);
                $data['total_debt'][$order_date] = round($total_debt,2);
                

                $total_overdue_debt += ($period_new_overdue_debts[$order_date] ?: 0);
                // $total_overdue_debt -= ($period_new_paid_overdue_debts[$order_date] ?: 0);
                $data['total_overdue_debt'][$order_date] = round($total_overdue_debt, 2);

                // $total_debt -= ($client_debt2[$order_date] ?: 0); 
                // $total_debt += ($overdue_debts[$order_date] ?: 0);        
                // $data['total_debt'][$order_date] = $total_debt;

                // $total_overdue_debt -= ($overdue_debts[$order_date] ?: 0);        
                // $data['total_overdue_debt'][$order_date] = $total_overdue_debt;

                $total_debt_payments += ($parnasas_debt[$order_date] ?: 0);
                // $total_debt_payments -= ($overdue_payments[$order_date] ?: 0);
                $data['total_debt_payments'][$order_date] = $total_debt_payments;

                $data['revenue_plan']['SUM'][$order_date] = ($revenue_plan[$order_date]['total'] ? array_sum($revenue_plan[$order_date]['total']) : 0); 
                $data['revenue_plan']['invoiceid'][$order_date] = ($revenue_plan[$order_date]['invoiceid'] ? implode(',',$revenue_plan[$order_date]['invoiceid']) : 0); 


                $data['payments_plan']['SUM'][$order_date] = ($payments_plan[$order_date]['total'] ? array_sum($payments_plan[$order_date]['total']) : 0); 
                $data['payments_plan']['purchaseorderid'][$order_date] = ($payments_plan[$order_date]['purchaseorderid'] ? implode(',',$payments_plan[$order_date]['purchaseorderid']) : 0); 
            }  
        }

        return array('role' => $current_user->roleid, 'period' => $period_arr,'period2' => $period_arr2,'filter' => $show_filter,'data' => $data);
    }

    function getDatesFromRange($start, $end) { 
        $array = array();  
        $interval = new DateInterval('P1D');  
        $realEnd = new DateTime($end); 
        $realEnd->add($interval);   
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
        foreach($period as $date) {                  
            $array[] = $date;  
        }  
        return $period; 
	} 
}