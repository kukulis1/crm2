<?php

namespace modules\Bankstatements\models;

use Exception;

class Bankstatements {
  
  public function __construct()
  {
    global $adb;
    $this->db = $adb;
  }

  public function importXml($post,$files){
      global $current_user;
      $target_dir = "uploads/";
      $target_file = $target_dir . basename($files["payments"]["name"]);
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $date = date("Y-m-d H:i:s");
      

      if(isset($post["submit"])) {

        if($imageFileType != "xml") {
          setcookie("import_errors", "Galima įkelti tik xml failus.", time()+3600, "/","", 0);    
          $uploadOk = 0;
        }

        if (move_uploaded_file($files["payments"]["tmp_name"], $target_file)) {  
          setcookie("import_success", "Failas sėkmingai importuotas ", time()+3600, "/","", 0);        
        } else {        
          setcookie("import_errors", "Įkeliant failą įvyko klaida ", time()+3600, "/","", 0);  
          $uploadOk = 0;          
        }     

      }

      $load_xml = basename($files["payments"]["name"]);

     
      if(!empty($load_xml) && $uploadOk){
 
        $loaded_xml = simplexml_load_file("uploads/".$load_xml) or die("Error: Cannot create object");
        $xml_inside = (!empty($loaded_xml->BkToCstmrStmt) ? $loaded_xml->BkToCstmrStmt : $loaded_xml->BkToCstmrAcctRpt);
        $msgid = $xml_inside->GrpHdr->MsgId; // xml file id
        $entry = (!empty($xml_inside->Rpt) ? $xml_inside->Rpt : $xml_inside->Stmt); 

        $rows = count($entry->Ntry);        
        
        $iban = $entry->Acct->Id->IBAN; // Owner iban
        $bankName = $entry->Acct->Svcr->FinInstnId->Nm; // Owner bank name

        $balance = 0;
        $balance_date = '';
        try{
            foreach($entry->Bal as $balance_info){
                if($balance_info->Tp->CdOrPrtry->Cd == 'CLBD'){
                    $balance = (float)$balance_info->Amt;
                    $balance_date = (string)$balance_info->Dt->Dt;
                }
            }
        } catch(Exception $e){}

        if(!empty($balance)){
            $stmt = "SELECT id FROM app_bank_balances WHERE xml_file_id = ?";
            $is_balance_imported = $this->db->pquery($stmt, [$msgid]);

            if(empty($this->db->num_rows($is_balance_imported))){
                $stmt = "INSERT INTO app_bank_balances (bank_name, xml_file_id, balance, xml_file_date, created_at) VALUES (?, ?, ?, ?, ?)";
                $this->db->pquery($stmt, [$bankName, $msgid, $balance, $balance_date, $date]);
            }
        }

        $check = "SELECT msgid FROM vtiger_bankstatements_xml_history WHERE msgid = ?";
        $check2 = "SELECT cf_2161 FROM vtiger_bankstatementscf WHERE cf_2161 = ?";


        $sql = "INSERT INTO vtiger_bankstatements (bankstatementsid, bankstatements_tks_bank, bankstatements_tks_iban, bankstatements_tks_payerreceiv, bankstatements_tks_date, bankstatements_tks_payerreceiviban, bankstatements_tks_transaction, bankstatements_tks_purpose, bankstatements_tks_currency, bankstatements_tks_amount) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $sql2 = "INSERT INTO vtiger_bankstatementscf (bankstatementsid,cf_2084,cf_2086,cf_2161) VALUES (?,?,?,?)";

        $sql3 = "INSERT INTO vtiger_bankstatements_xml_history (`msgid`,`filename`,`rows`) VALUES (?,?,?)";

        $sql4 = "INSERT INTO vtiger_bankstatements_end_to_end_id (recordid, code) VALUES (?,?)";

        $check_file_exist =  $this->db->pquery($check, array($msgid));         
        $check_msgid = $this->db->num_rows($check_file_exist);

        if(empty($msgid)){
          $check_msgid = true;
        }   


        if(!$check_msgid){
            $this->db->pquery($sql3, array($msgid,$load_xml,$rows));  

            // $loop = (!empty($xml_inside->Rpt->Ntry) ? $xml_inside->Rpt->Ntry : $xml_inside->Stmt->Ntry);       
            foreach($entry->Ntry AS $pay){  
              $entity_id = $this->getEntityId(); 
              $type = $pay->CdtDbtInd; // transaction type

              $amount = $pay->Amt; // transaction amount
              $currency = $pay->Amt['Ccy']; // transaction currency            
              $purpose = $pay->NtryDtls->TxDtls->RmtInf->Ustrd; // transaction from description
              $transaction = $pay->NtryDtls->TxDtls->Refs->InstrId; // transaction from id
              $document_no = $pay->NtryDtls->TxDtls->Refs->AcctSvcrRef; // document no
              $endToEndId = $pay->NtryDtls->TxDtls->Refs->EndToEndId; // source code

              if($type == 'CRDT'){ // transaction from client name
                $client = $pay->NtryDtls->TxDtls->RltdPties->Dbtr->Nm; 
              }else{
                $client = $pay->NtryDtls->TxDtls->RltdPties->Cdtr->Nm; 
              }

              if($type == 'CRDT'){ // transaction from bank
                $cBankName = $pay->NtryDtls->TxDtls->RltdAgts->DbtrAgt->FinInstnId->Nm;      
              }else{
                $cBankName = $pay->NtryDtls->TxDtls->RltdAgts->CdtrAgt->FinInstnId->Nm;   
              }            

              if($type == 'CRDT'){ // transaction from client name
                $cIban = $pay->NtryDtls->TxDtls->RltdPties->DbtrAcct->Id->IBAN; 
              }else{
                $cIban = $pay->NtryDtls->TxDtls->RltdPties->CdtrAcct->Id->IBAN; 
              }         
             
              $transaction_date = $pay->ValDt->Dt; // transaction date   

              $check_payment_exist =  $this->db->pquery($check2, array($document_no));         
              $check_document_no = $this->db->num_rows($check_payment_exist);
              if(!$check_document_no){
                if($cIban){ 
                  $this->insertEntity('Bankstatements', $entity_id, $current_user->id, NULL, $client, $date);
                  $this->db->pquery($sql, array($entity_id,$bankName,$iban,$client,$transaction_date,$cIban,$transaction,$purpose,$currency,$amount)); 
                  $this->db->pquery($sql2, array($entity_id,$type,$cBankName,$document_no));
                  if(!empty($endToEndId) && $endToEndId != 'NOTPROVIDED'){
                    $this->db->pquery($sql4, array($entity_id,$endToEndId));
                  }
                }
              }
            }  

        }else{
          setcookie("import_errors", "Xml failas su id $msgid jau yra įkeltas", time()+3600, "/","", 0);                  
        }    

      }

      $this->importXmlToTransfers($files);

      // Remove xml file
      // unlink("uploads/".$load_xml);
      // header("Location:/index.php?module=Bankstatements&view=List");
  }



  public function importXmlToTransfers($files)
  {
    
      $target_dir = "uploads/";
      $target_file = $target_dir . basename($files["payments"]["name"]); 
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $load_xml = basename($files["payments"]["name"]);

      if(!empty($load_xml)){

        $sql = "INSERT INTO app_xml_history (`msgid`,`filename`,`rows`) VALUES (?,?,?)";
        $sql2 = "SELECT id FROM app_xml_history ORDER BY id DESC LIMIT 1";
        $sql3 = "SELECT msgid FROM app_xml_history WHERE msgid = ?";
        $sql4 = "INSERT INTO app_import_payment_draft (payment_id,pay_date, transfer_num, amount, currency, rate, amount_eur, comment,iban,client_name) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $sql5 = "UPDATE app_xml_history SET inserted_rows = ? WHERE id = ?";


        $loaded_xml = simplexml_load_file("uploads/".$load_xml) or die("Error: Cannot create object");

        $entry = (!empty($loaded_xml->BkToCstmrAcctRpt) ? $loaded_xml->BkToCstmrAcctRpt : $loaded_xml->BkToCstmrStmt); 


        $msgid = $entry->GrpHdr->MsgId;
        $rows = (!empty($entry->Rpt) ? count($entry->Rpt->Rpt->Ntry) :  count($entry->Stmt->Ntry) );
        $entry2 = (!empty($entry->Rpt) ? $entry->Rpt->Rpt->Ntry :  $entry->Stmt->Ntry);

        
        $check_payment_exist =  $this->db->pquery($sql3, array($msgid));         
        $check_msgid = $this->db->num_rows($check_payment_exist);

        if(empty($msgid)){
          $check_msgid = true;
        }

        if(!$check_msgid){
          
          $insert_history = $this->db->pquery($sql, array($msgid,$load_xml,$rows));        

          $get_id = $this->db->pquery($sql2, array());
          $id = $this->db->query_result($get_id,0,'id');    
                       
          $i = 0;
          foreach($entry2 AS $pay) {

            $pay_date = (!empty($pay->BookgDt->DtTm) ? trim(explode("T",$pay->BookgDt->DtTm)[0]) : trim($pay->BookgDt->Dt));
            $transfer = (!empty($pay->AcctSvcrRef) ? $pay->AcctSvcrRef  : trim($pay->NtryDtls->TxDtls->Refs->AcctSvcrRef));
            $amount = trim($pay->Amt);
            $currency = trim($pay->Amt['Ccy']);
            $debit =trim($pay->CdtDbtInd);
            $iban = trim($pay->NtryDtls->TxDtls->RltdPties->DbtrAcct->Id->IBAN);
            $payer = trim($pay->NtryDtls->TxDtls->RltdPties->Dbtr->Nm);
            $comment = trim($pay->NtryDtls->TxDtls->RmtInf->Ustrd);

            if($debit == 'CRDT'){       
                $insert_payments = $this->db->pquery($sql4, array($id,$pay_date,$transfer,$amount,$currency,1,$amount,$comment,$iban,$payer));
              $i++;   
            }
          }

          $update_history = $this->db->pquery($sql5, array($i,$id));
          $this->setInvoiceNumbers($id);
        }else{
          // setcookie("import_errors", "Toks mokėjimas jau yra įkeltas ", time()+3600, "/","", 0);                  
        }

      }

      // Ištrina xml faila
      // unlink("uploads/".$load_xml);
      header("Location:/index.php?module=Bankstatements&view=List");
  }

  public function getEntityId(){
    $sql = "SELECT id FROM `vtiger_crmentity_seq`";
    $sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
    $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
    $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
    $lock3 = 'UNLOCK TABLES';

    $this->db->pquery($lock, array()); 
      $get_id = $this->db->pquery($sql, array()); 
      $seq = $this->db->query_result($get_id,0,'id');    
    $this->db->pquery($lock3, array());    

    $new_seq = $seq + 1;   

    $this->db->pquery($lock2, array()); 
     $this->db->pquery($sql2, array($new_seq)); 
    $this->db->pquery($lock3, array()); 

    return $new_seq;
  }

  public function insertEntity($setype, $entity_id, $user_id, $description, $label, $date){
    $sql = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
    $this->db->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date)); 
  }

}
