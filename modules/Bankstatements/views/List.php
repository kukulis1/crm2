<?php

include "modules/Bankstatements/models/listModel.php";

use modules\Bankstatements\models\Bankstatements as Bankstatements;

class Bankstatements_List_View extends Vtiger_List_View {

  function __construct(){
    $this->Bankstatements = new Bankstatements;  
  }

  public function uploadHandler($post,$files)
  {
    $this->Bankstatements->importXml($post,$files);
  }
}