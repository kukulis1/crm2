<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();          
        $date = $_POST['date'];
       
        $period_new_paid_overdue_debts_sql = $dbh->prepare("SELECT purchaseorder_no,purchaseorderid, ROUND(total,2) AS total,SUM(vtiger_payment.payment_tks_suma) AS payed_price, vtiger_purchaseorder.duedate, MAX(payment_tks_mokėjimodata) AS pay_date,vendorname                             
                                  FROM vtiger_purchaseorder 
                                  LEFT JOIN vtiger_vendor ON vtiger_vendor.vendorid=vtiger_purchaseorder.vendorid
                                  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_purchaseorder.purchaseorderid AND relmodule = 'Payment'
                                  LEFT JOIN vtiger_payment ON vtiger_payment.paymentid = vtiger_crmentityrel.relcrmid 					 
                                  INNER JOIN vtiger_crmentity ON vtiger_purchaseorder.purchaseorderid = vtiger_crmentity.crmid  AND setype = 'PurchaseOrder' 
                                  WHERE duedate <= ? AND vtiger_crmentity.deleted = 0  
                                  GROUP BY vtiger_purchaseorder.purchaseorderid	
                                  HAVING pay_date IS NULL");  

                                  // HAVING pay_date >= duedate OR pay_date IS NULL
          

        $period_new_paid_overdue_debts_sql->setFetchMode(PDO::FETCH_ASSOC);
        $period_new_paid_overdue_debts_sql->execute(array($date)); 

        $period_new_paid_overdue_debts = array();
        foreach($period_new_paid_overdue_debts_sql as $row){
          $period_new_paid_overdue_debts[] = $row;
        }       

       
        echo json_encode(array('status' => 'success','data' => $period_new_paid_overdue_debts));

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'fail'));
  }
}else{
  http_response_code(404);
} 