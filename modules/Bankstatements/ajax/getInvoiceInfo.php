<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();          
        $id = $_POST['id'];

        if($_POST['module'] == 'Invoice'){
          $sql = "SELECT invoiceid AS id,invoice_no AS no,ROUND(total,2) AS total,duedate,accountname AS name
                  FROM vtiger_invoice i
                  JOIN vtiger_account a ON i.accountid=a.accountid
                  WHERE invoiceid IN ($id)";
        }else{
          $sql = "SELECT purchaseorderid AS id,purchaseorder_no AS no,ROUND(total,2) AS total,duedate,vendorname AS name 
                  FROM vtiger_purchaseorder p
                  JOIN vtiger_vendor v ON v.vendorid=p.vendorid
                  WHERE purchaseorderid IN ($id)";
        }

        $sth = $dbh->prepare($sql);

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $documents = array();

        foreach($sth AS $row){ 
          $documents[] = $row;
        }
       
        echo json_encode(array('status' => 'success','data' => $documents));

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'fail'));
  }
}else{
  http_response_code(404);
} 