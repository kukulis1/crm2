<?php


class DemandHandler extends VTEventHandler {

  function handleEvent($eventName, $entityData)
  {
    global $adb, $current_user;

        if($eventName == 'vtiger.entity.aftersave') {
            $moduleName = $entityData->getModuleName();

            if($moduleName != 'Demand'){
                return;
            }

            if($moduleName == 'Demand' && $_REQUEST['module']=='Demand'){            
             
              $recordId = $_REQUEST['record'];
              $currentid = $_REQUEST['currentid'];
              $number_of_sources = $_REQUEST['number_of_sources'];

              $source_array = array();

                for ($i=1; $i <= $number_of_sources; $i++) { 
                  $source_array[$i][] = $_REQUEST["source".$i];
                  $source_array[$i][] = $_REQUEST["price".$i];
                }

                $source_string = '';              

                for ($e=1; $e <= COUNT($source_array); $e++) {                  
                  $source_string .= implode("|##|",$source_array[$e]).(COUNT($source_array) == $e ? '' : ',');
                }

              if($recordId){
                $recordId = $recordId;
              }else{
                $recordId = $currentid;
              }           

              $adb->pquery("UPDATE vtiger_demand SET demand_tks_searchsources = ? WHERE demandid = ?",array($source_string,$recordId));       
              
              
            }
        }
  }

}