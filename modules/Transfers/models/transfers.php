<?php
namespace modules\Transfers\models;

use Bankstatements;
use Exception;

class Transfers {
  
  public function __construct()
  {
    global $adb;
    $this->db = $adb;
  }

  public function coverInvoices($post, $recordid, $user_id){
    $date_now = date("Y-m-d H:i:s");
    $invoice_amount_array = [];  
    $transfer_invoice_array = [];
    $pay_date = [];

    $check_invoice_id = [];

    for ($i=1; $i <= $post['keys']; $i++) { 
      for ($e=1; $e <= $post['lens'.$i]; $e++) { 
        if($post["amount$i-$e"] > 0){
          $check_invoice_id["invoice_id$i-$e"] = $post["invoice_id$i-$e"];
          $transfer_invoice_array[$post["transfer_id$i"]][] = $post["invoice_id$i-$e"];
          $invoice_amount_array[$post["invoice_id$i-$e"]] = $post["amount$i-$e"];
          $pay_date[$post["invoice_id$i-$e"]] = $post["pay_date$i"];
        }
      }
    }

    $same_invoice = array();
    foreach(array_count_values($check_invoice_id) as $val => $c){
      if($c > 1) $same_invoice[] = $val;
    }

    if(count($same_invoice) > 0){
      setcookie('same_invoice', 'has', (time() + 86400), "/");
      $imploded_same_invoice = implode(',', $same_invoice);
      $invoice_no = $this->db->pquery("SELECT GROUP_CONCAT(invoice_no) as invoice_numbers  FROM vtiger_invoice WHERE invoiceid IN ($imploded_same_invoice)", []);

      setcookie('invoice_no', $this->db->query_result($invoice_no, 0, 'invoice_numbers'), (time() + 86400), "/");
      return;
    }

    $invoiceids = array_keys($invoice_amount_array);
    $invoiceid = implode(',', $invoiceids);

    $get_all_accountid = [];    

    $info = $this->db->pquery("SELECT vtiger_invoice.invoiceid, IF(ROUND(SUM(debts_tks_suma),2) IS NULL,0,ROUND(SUM(debts_tks_suma),2)) AS payed_debt,
                        ROUND(IF(vtiger_invoice.region_id = 5,subtotal,total),2) AS total, vtiger_invoice.accountid
                      FROM vtiger_invoice 
                      LEFT JOIN vtiger_account on vtiger_invoice.accountid=vtiger_account.accountid 
                      LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
                      LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
                      LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_debts.debtsid
                      WHERE vtiger_invoice.invoiceid IN ($invoiceid)
                      GROUP BY vtiger_invoice.invoiceid	 
                      ORDER BY invoiceid", array());

    foreach($info AS $row){        
      $get_all_accountid[] = $row['accountid'];
      $total = (!empty($invoice_amount_array[$row['invoiceid']]) ? $invoice_amount_array[$row['invoiceid']] : 0);
      $pay_day = (!empty($pay_date[$row['invoiceid']]) ? $pay_date[$row['invoiceid']] : date("Y-m-d"));

      if($row['payed_debt'] < $row['total'] && $total != 0){               

        $entity_id = $this->getEntityId();
        $this->insertEntity('Debts', $entity_id, $user_id, NULL, 'XML', $post['xml_name'], $date_now);   
        $this->db->pquery("INSERT INTO vtiger_debts (debtsid, debtsno, debts_tks_data, debts_tks_suma, debts_tks_tipas) 
        VALUES (?,?,?,?,?)", array($entity_id, $recordid, $pay_day, $total, 'Pavedimu'));

        $this->db->pquery("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) 
        VALUES (?,?,?,?)", array($row['invoiceid'], 'Invoice', $entity_id, 'Debts'));        

        $this->db->pquery("INSERT INTO vtiger_debtscf (debtsid,cf_2665) VALUES (?,?)", array($entity_id, $post['xml_name']));

      }              
    }


    foreach ($transfer_invoice_array AS $transfer => $invoiceid) {    
      $this->db->pquery("UPDATE app_import_payment_draft SET invoiceid = ? WHERE transfer_num = ?", array(implode(',', $invoiceid), $transfer));       
    }


    $transfer_numbers = "'".implode("','", array_keys($transfer_invoice_array))."'";
    $imploded_accountids = implode(',', $get_all_accountid);

    $get_draft_info = $this->db->query("SELECT iban,accountid, client_name FROM app_import_payment_draft WHERE transfer_num IN ($transfer_numbers)");
    $get_all_accounts =  $this->db->query("SELECT * FROM vtiger_account_iban WHERE accountid IN ($imploded_accountids)");
   
    $accounts_arr = [];
    foreach ($get_all_accounts as $acc) {
      $accounts_arr[$acc['accountid']][$acc['iban']] = $acc['client_name'];
    }


    foreach ($get_draft_info as $customer){
      if(empty($accounts_arr[$customer['accountid']][$customer['iban']]) && $customer['accountid'] > 1){
        $this->db->pquery("INSERT INTO vtiger_account_iban (iban,accountid, client_name) VALUES (?,?,?)", array($customer['iban'], $customer['accountid'], $customer['client_name']));      
      }
    }

    header("Location: /index.php?module=Transfers&view=Draft&record=$recordid");
  }
  
  public function getTodayImports()
  {
    $today = date("Y-m-d"); 
    $sql = "SELECT * FROM app_xml_history WHERE date_format(import_date, '%Y-%m-%d') = ? ORDER BY id DESC";
    $records = $this->db->pquery($sql, array($today));

    return $records;
  }

  public function getHistory($perPage,$startAt)
  {
    $sql = "SELECT * FROM app_xml_history ORDER BY id DESC LIMIT $startAt, $perPage";
    $records = $this->db->pquery($sql, array());

    return $records;
  }

  public function getAllPaymentst(){
    $sql = "SELECT * FROM app_xml_history";  
    $records = $this->db->query($sql);
    $allRecords = $records->rowCount();    
    return $allRecords;
   }
 


  public function getDraft($id)
  {    
    $sql = "SELECT * FROM app_import_payment_draft WHERE payment_id = ? ORDER BY payment_id DESC";
    $records = $this->db->pquery($sql, array($id));

    return $records;
  }

  public function getXmlName($id){

    if(isset($_COOKIE['same_invoice'])) {
      setcookie('same_invoice', '', -1);
      setcookie('invoice_no', -1);
    }

    $sql = "SELECT `filename` FROM app_xml_history WHERE id = ?";
    $records = $this->db->pquery($sql, array($id));

    return $this->db->query_result($records,0,'filename');    
  }

  public function getAccountIdByIban($iban)
  {
    $sql = "SELECT accountid FROM crm.vtiger_vendor v
                    LEFT JOIN vtiger_vendorcf vc ON vc.vendorid=v.vendorid
                    WHERE cf_1462 = ?";
     $records = $this->db->pquery($sql, array($iban));
     $accountid = $this->db->query_result($records,0,'accountid');     
     
     return $accountid;
  }

  public function getCustomerOrders($id)
  {
    $sql = "SELECT * FROM app_import_payment_draft d WHERE d.payment_id = ?";
    $saved_draft = "SELECT * FROM app_import_payment_saved_draft WHERE payment_id = ?";

    $records = $this->db->pquery($sql, array($id));
    $get_saved_drafts = $this->db->pquery($saved_draft, array($id));
    $num_rows = $this->db->num_rows($get_saved_drafts);

    $draft_amount = [];
    if($num_rows){
      foreach($get_saved_drafts AS $draft){
        $invoiceid = explode(',',$draft['invoiceid']);
        $amount = explode('|',$draft['amount']);
        for ($i=0; $i < count($invoiceid); $i++) { 
          if(!empty($invoiceid[$i])){
            $draft_amount[$invoiceid[$i]] = $amount[$i];     
          }   
        }
      }
    }


    $invoice_arr = array();


    foreach($records AS $row){
 
      $invoiceid = $row['invoiceid'];        
      $invoices_arr = explode(",",$row['invoiceid']);  
      $find_invoices = explode(",",$row['find_invoices']); 
      
  

      $sql2 = "SELECT invoiceid, invoice_no, invoicedate,ROUND(IF(vtiger_invoice.region_id = 5,subtotal,total),2) AS total
                                FROM vtiger_invoice 
                                LEFT JOIN vtiger_crmentity e ON e.crmid=invoiceid                                                  
                                WHERE e.deleted = 0 AND invoiceid IN ($invoiceid) AND accountid != 2356
                                GROUP BY invoiceid
                                ORDER BY invoice_no ASC";

      $sql3 = "SELECT vtiger_invoice.invoiceid,  SUM(ROUND(debts_tks_suma,2)) as debt 
				          FROM vtiger_invoice 
                  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid=vtiger_invoice.invoiceid
                  LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid AND vtiger_debts.debtsno = $id 
                  INNER JOIN vtiger_crmentity e ON e.crmid=vtiger_debts.debtsid               
                  WHERE e.deleted = 0
                  AND vtiger_invoice.invoiceid IN ($invoiceid) 
                  GROUP BY vtiger_invoice.invoiceid";

      $sql4 = "SELECT  vtiger_crmentityrel.crmid as invoiceid, SUM(ROUND(debts_tks_suma,2)) as debt 
                      FROM vtiger_crmentityrel 
                      LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid  AND vtiger_debts.debtsno = $id       
                      INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_debts.debtsid            
                      WHERE deleted = 0 AND source = 'XML' AND vtiger_crmentityrel.crmid IN ($invoiceid) AND debts_tks_suma IS NOT NULL"; 

      $sql5 = "SELECT vtiger_invoice.invoiceid,  SUM(ROUND(debts_tks_suma,2)) as debt 
                FROM vtiger_invoice 
                LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid=vtiger_invoice.invoiceid
                LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
                INNER JOIN vtiger_crmentity e ON e.crmid=vtiger_debts.debtsid               
                WHERE e.deleted = 0 AND vtiger_invoice.invoiceid IN ($invoiceid)
                GROUP BY vtiger_invoice.invoiceid";             




      $records2 = $this->db->pquery($sql2, array());
      $records3 = $this->db->pquery($sql3, array());
      $records4 = $this->db->pquery($sql4, array()); 
      $records5 = $this->db->pquery($sql5, array()); 

      $debts_array = [];

      if($this->db->num_rows($records3)){
        foreach($records3 AS $result){
          $debts_array[$result['invoiceid']] = ($result['debt'] ?: 0);
        }
      }

      $total_debts_array = [];
      $total_debts_array2 = [];
      if($this->db->num_rows($records5)){
        foreach($records5 AS $debts){
          $total_debts_array[$debts['invoiceid']] = ($debts['debt'] ?: 0);
          $total_debts_array2[$debts['invoiceid']] = ($debts['debt'] ?: 0);
        }
      }

      $invoice_no_arr = [];
      $total = [];
      $invoicedate = [];
      $debt = [];
      $debt2 = [];
      $hidden_debt = [];
      $amount_to_be_paid = [];

      $balance = $row['amount_eur'];

     if(!empty($invoiceid)){       

        $num_rows = $this->db->num_rows($records2);
        if($this->db->num_rows($records4)){
          $included = $this->db->query_result($records4,0,'debt');
        }
        $included = ($included ?:0);      

        $final_payed = $row['amount_eur'];
        $temp_amount = $row['amount_eur'];      
            

        foreach($records2 AS $info){    
          $invoice_no_arr[$info['invoiceid']] = $info['invoice_no'];
          $total[$info['invoiceid']] = $info['total'];           
          $invoicedate[$info['invoiceid']] = $info['invoicedate'];  
          $balance -= $debts_array[$info['invoiceid']];   

          // NOTE final suma gauna is issaugoto drafto, jei jo nera tada is saskaitos sumos
          $final = ($draft_amount[$info['invoiceid']] ?: $info['total']);          
          $must_be_pay = round($final - $total_debts_array[$info['invoiceid']],2);
          $must_be_pay2 = round($info['total'] - $total_debts_array2[$info['invoiceid']],2);
          

          $debt[$info['invoiceid']] = $must_be_pay;

          // Eilutem kurios automatiskai nerado saskaitos
          $debt2[$info['invoiceid']] = round($info['total'] - $total_debts_array[$info['invoiceid']],2);

          if($num_rows == 1){
            $be_paid = ($must_be_pay <= $row['amount_eur'] ? $must_be_pay : $row['amount_eur']);       
            $be_paid2 = ($must_be_pay2 <= $row['amount_eur'] ? $must_be_pay2 : $row['amount_eur']);       

            $amount_to_be_paid[$info['invoiceid']] = ['amount' => $be_paid, 'total' => $be_paid2];   
            $final_payed -= $be_paid;             
          }else if(count($invoices_arr) == count($find_invoices)){  
            if($must_be_pay <= $temp_amount){
              $amount_to_be_paid[$info['invoiceid']] = ['amount' => $must_be_pay, 'total' => $info['total']];  
              $final_payed -= $must_be_pay;  
              $temp_amount -= $must_be_pay;
            }else{
              $amount_to_be_paid[$info['invoiceid']] = ['amount' => $temp_amount, 'total' => $info['total']]; 
              // $amount_to_be_paid[$info['invoiceid']] = ['amount' => $temp_amount, 'total' => $temp_amount]; 
              $final_payed -= $temp_amount;  
              $temp_amount -= $temp_amount;
            }                        
            
          }else{     
            $amount_to_be_paid[$info['invoiceid']] = ['amount' => ($draft_amount[$info['invoiceid']] ?:0), 'total' => $debt2[$info['invoiceid']]];
              
            if(!empty($draft_amount[$info['invoiceid']])){
              $final_payed -= $draft_amount[$info['invoiceid']];
            }            
          }           
   
        
          
          if($total_debts_array[$info['invoiceid']] >= $info['total']){
            unset($debt[$info['invoiceid']]);  
          }

          if($total_debts_array[$info['invoiceid']] >= $info['total'] OR  $included >= $row['amount_eur']){  
            unset($invoice_no_arr[$info['invoiceid']]);
            unset($total[$info['invoiceid']]);
            unset($invoicedate[$info['invoiceid']]);    
            unset($amount_to_be_paid[$info['invoiceid']]); 
            
            if(empty($total_debts_array[$info['invoiceid']])){
              unset($debt[$info['invoiceid']]);  
            }
          }        
          
          
          $final_payed -= $debts_array[$info['invoiceid']];  
        }    
            
        
              
        $invoice_arr[] = array(
          'client_info' => ['client_name' => $row['client_name'], 'id' => $row['id'], 'amount_balance' => $balance,'hidden_debt' => $hidden_debt],
          'transfer_num' => $row['transfer_num'],
          'pay_date' => $row['pay_date'],
          'amount' => $row['amount'],
          'currency' => $row['currency'],
          'rate' => $row['rate'],
          'amount_eur' => $row['amount_eur'],
          'comment' => $row['comment'],
          'included' => $included,
          'invoices_info' => [              
              ['invoice_no' => $invoice_no_arr],     
              ['invoicedate' => $invoicedate],       
              ['total' => $total],      
              ['debt' => $debt],           
              ['amount_to_be_paid' => $amount_to_be_paid],
              ['payed' => ($final_payed < 0 ? 0 : round($final_payed,2))]
              
               
          ] 
        );    
      }else{     
        $invoice_arr[] = array(
          'client_info' => ['client_name' => $row['client_name'], 'id' => $row['id']],
          'transfer_num' => $row['transfer_num'],
          'pay_date' => $row['pay_date'],
          'amount' => $row['amount'],
          'currency' => $row['currency'],
          'rate' => $row['rate'],
          'amount_eur' => $row['amount_eur'],
          'comment' => $row['comment'],
          'included' => 0,
          'invoices_info' => [
              ['invoice_no' => array()],     
              ['invoicedate' => array()],       
              ['total' => array()],      
              ['debt' => array()],
              ['amount_to_be_paid' => array()],
              ['payed' => $row['amount_eur']]              
          ] 
        );    
      }
    }

    return $invoice_arr;
  }

  public function uploadXml($post,$files)
  {
    
      $target_dir = "uploads/";
      $target_file = $target_dir . basename($files["payment"]["name"]);
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

      if(isset($post["submit"])) {

        if($imageFileType != "xml") {
          setcookie("import_errors", "Galima įkelti tik xml failus.", time()+3600, "/","", 0);    
          $uploadOk = 0;
        }

        if (move_uploaded_file($files["payment"]["tmp_name"], $target_file)) {  
          setcookie("import_success", "Failas sėkmingai importuotas ", time()+3600, "/","", 0);        
        } else {        
          setcookie("import_errors", "Įkeliant failą įvyko klaida ", time()+3600, "/","", 0);  
          $uploadOk = 0;          
        }
      

      }

      $load_xml = basename($files["payment"]["name"]);



      if(!empty($load_xml) && $uploadOk){

        $sql = "INSERT INTO app_xml_history (`msgid`,`filename`,`rows`) VALUES (?,?,?)";
        $sql2 = "SELECT id FROM app_xml_history ORDER BY id DESC LIMIT 1";
        $sql3 = "SELECT msgid FROM app_xml_history WHERE msgid = ?";
        $sql4 = "INSERT INTO app_import_payment_draft (payment_id,pay_date, transfer_num, amount, currency, rate, amount_eur, comment,iban,client_name) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $sql5 = "UPDATE app_xml_history SET inserted_rows = ? WHERE id = ?";


        $loaded_xml = simplexml_load_file("uploads/".$load_xml) or die("Error: Cannot create object");

        $entry = (!empty($loaded_xml->BkToCstmrAcctRpt) ? $loaded_xml->BkToCstmrAcctRpt : $loaded_xml->BkToCstmrStmt); 
        $msgid = $entry->GrpHdr->MsgId;
        $rows = (!empty($entry->Rpt) ? (!empty($entry->Rpt->Rpt) ? count($entry->Rpt->Rpt->Ntry) : count($entry->Rpt->Ntry)) :  count($entry->Stmt->Ntry) );
        $entry2 = (!empty($entry->Rpt) ? (!empty($entry->Rpt->Rpt) ? $entry->Rpt->Rpt->Ntry : $entry->Rpt->Ntry) :  $entry->Stmt->Ntry);  
        
        $check_payment_exist =  $this->db->pquery($sql3, array($msgid));         
        $check_msgid = $this->db->num_rows($check_payment_exist);

        if(empty($msgid)){
          $check_msgid = true;
        }

        if(!$check_msgid){
          
          $this->db->pquery($sql, array($msgid,$load_xml,$rows));        

          $get_id = $this->db->pquery($sql2, array());
          $id = $this->db->query_result($get_id,0,'id');    
                       
          $i = 0;
          foreach($entry2 AS $pay) {
            $pay_date = (!empty($pay->BookgDt->DtTm) ? trim(explode("T",$pay->BookgDt->DtTm)[0]) : trim($pay->BookgDt->Dt));
            $transfer = (!empty($pay->AcctSvcrRef) ? $pay->AcctSvcrRef  : trim($pay->NtryDtls->TxDtls->Refs->AcctSvcrRef));
            $amount = trim($pay->Amt);
            $currency = trim($pay->Amt['Ccy']);
            $debit =trim($pay->CdtDbtInd);
            $iban = trim($pay->NtryDtls->TxDtls->RltdPties->DbtrAcct->Id->IBAN);
            $payer = trim($pay->NtryDtls->TxDtls->RltdPties->Dbtr->Nm);
            $comment = trim($pay->NtryDtls->TxDtls->RmtInf->Ustrd);

            if($debit == 'CRDT'){       
                $this->db->pquery($sql4, array($id,$pay_date,$transfer,$amount,$currency,1,$amount,$comment,$iban,$payer));
              $i++;   
            }
          }
        
          $this->db->pquery($sql5, array($i,$id));
          $this->setInvoiceNumbers($id);
        }else{
          setcookie("import_errors", "Toks mokėjimas jau yra įkeltas ", time()+3600, "/","", 0);                  
        }

      }


      $this->importXmlToBankStatements($files);


      // Ištrina xml faila
      // unlink("uploads/".$load_xml);
      // header("Location:/index.php?module=Transfers&view=List");

  }


  public function importXmlToBankStatements($files){
    global $current_user;
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($files["payment"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $date = date("Y-m-d H:i:s");
    

    $load_xml = basename($files["payment"]["name"]);

    if(!empty($load_xml)){

      $loaded_xml = simplexml_load_file("uploads/".$load_xml) or die("Error: Cannot create object");
      $xml_inside = (!empty($loaded_xml->BkToCstmrStmt) ? $loaded_xml->BkToCstmrStmt : $loaded_xml->BkToCstmrAcctRpt);
      $msgid = $xml_inside->GrpHdr->MsgId; // xml file id
      $entry = (!empty($xml_inside->Rpt) ? $xml_inside->Rpt : $xml_inside->Stmt); 


      $rows = count($entry->Ntry);        
      
      $iban = $entry->Acct->Id->IBAN; // Owner iban
      $bankName = $entry->Acct->Svcr->FinInstnId->Nm; // Owner bank name
    
      $balance = 0;
      $balance_date = '';
      try{
          foreach($entry->Bal as $balance_info){
              if($balance_info->Tp->CdOrPrtry->Cd == 'CLBD'){
                  $balance = (float)$balance_info->Amt;
                  $balance_date = (string)$balance_info->Dt->Dt;
              }
          }
      } catch(Exception $e){}

      if(!empty($balance)){
          $stmt = "SELECT id FROM app_bank_balances WHERE xml_file_id = ?";
          $is_balance_imported = $this->db->pquery($stmt, [$msgid]);

          if(empty($this->db->num_rows($is_balance_imported))){
              $stmt = "INSERT INTO app_bank_balances (bank_name, xml_file_id, balance, xml_file_date, created_at) VALUES (?, ?, ?, ?, ?)";
              $this->db->pquery($stmt, [$bankName, $msgid, $balance, $balance_date, $date]);
          }
      }

      $check = "SELECT msgid FROM vtiger_bankstatements_xml_history WHERE msgid = ?";
      $check2 = "SELECT cf_2161 FROM vtiger_bankstatementscf WHERE cf_2161 = ?";


      $sql = "INSERT INTO vtiger_bankstatements (bankstatementsid, bankstatements_tks_bank, bankstatements_tks_iban, bankstatements_tks_payerreceiv, bankstatements_tks_date, bankstatements_tks_payerreceiviban, bankstatements_tks_transaction, bankstatements_tks_purpose, bankstatements_tks_currency, bankstatements_tks_amount) VALUES (?,?,?,?,?,?,?,?,?,?)";
      $sql2 = "INSERT INTO vtiger_bankstatementscf (bankstatementsid,cf_2084,cf_2086,cf_2161) VALUES (?,?,?,?)";

      $sql3 = "INSERT INTO vtiger_bankstatements_xml_history (`msgid`,`filename`,`rows`) VALUES (?,?,?)";

      $sql4 = "INSERT INTO vtiger_bankstatements_end_to_end_id (recordid, code) VALUES (?,?)";

      $check_file_exist =  $this->db->pquery($check, array($msgid));         
      $check_msgid = $this->db->num_rows($check_file_exist);

      if(empty($msgid)){
        $check_msgid = true;
      }   


      if(!$check_msgid){
      
          $this->db->pquery($sql3, array($msgid,$load_xml,$rows));  

          // $loop = (!empty($xml_inside->Rpt->Ntry) ? $xml_inside->Rpt->Ntry : $xml_inside->Stmt->Ntry);       
          foreach($entry->Ntry AS $pay){  
            $entity_id = $this->getEntityId(); 
            $type = $pay->CdtDbtInd; // transaction type

            $amount = $pay->Amt; // transaction amount
            $currency = $pay->Amt['Ccy']; // transaction currency            
            $purpose = $pay->NtryDtls->TxDtls->RmtInf->Ustrd; // transaction from description
            $transaction = $pay->NtryDtls->TxDtls->Refs->InstrId; // transaction from id
            $document_no = $pay->NtryDtls->TxDtls->Refs->AcctSvcrRef; // document no
            $endToEndId = $pay->NtryDtls->TxDtls->Refs->EndToEndId; // source code

            if($type == 'CRDT'){ // transaction from client name
              $client = $pay->NtryDtls->TxDtls->RltdPties->Dbtr->Nm; 
            }else{
              $client = $pay->NtryDtls->TxDtls->RltdPties->Cdtr->Nm; 
            }

            if($type == 'CRDT'){ // transaction from bank
              $cBankName = $pay->NtryDtls->TxDtls->RltdAgts->DbtrAgt->FinInstnId->Nm;      
            }else{
              $cBankName = $pay->NtryDtls->TxDtls->RltdAgts->CdtrAgt->FinInstnId->Nm;   
            }            

            if($type == 'CRDT'){ // transaction from client name
              $cIban = $pay->NtryDtls->TxDtls->RltdPties->DbtrAcct->Id->IBAN; 
            }else{
              $cIban = $pay->NtryDtls->TxDtls->RltdPties->CdtrAcct->Id->IBAN; 
            }         
           
            $transaction_date = $pay->ValDt->Dt; // transaction date   

            $check_payment_exist =  $this->db->pquery($check2, array($document_no));         
            $check_document_no = $this->db->num_rows($check_payment_exist);
            if(!$check_document_no){
              if($cIban){ 
                $this->insertEntity('Bankstatements', $entity_id, $current_user->id, NULL,'XML', $client, $date);
                $this->db->pquery($sql, array($entity_id,$bankName,$iban,$client,$transaction_date,$cIban,$transaction,$purpose,$currency,$amount)); 
                $this->db->pquery($sql2, array($entity_id,$type,$cBankName,$document_no));
                if(!empty($endToEndId) && $endToEndId != 'NOTPROVIDED'){
                  $this->db->pquery($sql4, array($entity_id,$endToEndId));
                }
              }
            }
          }  

      }  

   
    }

    // Remove xml file
    header("Location:/index.php?module=Transfers&view=List");
}

  public function setInvoiceNumbers($id)
  {

    $addQuotes = function($code) {
      return "'$code'";
    };  

    $addQuotes2 = function($code) {
      $check = substr($code, 0,2);
      if($check == 'PT'){
        $one = substr($code, 0,4);
        $two = substr($code, 4,-5);
        $three = substr($code, 6,-3);
        $fourt = substr($code, 8);
        return "'$one/$two/$three/$fourt'";   
      }else{
        return "'$code'";
      } 
      
    }; 

    $sql = "SELECT * FROM app_import_payment_draft WHERE payment_id = ? ORDER BY payment_id DESC";
    $sql2 = "UPDATE app_import_payment_draft SET invoice_no = ? WHERE transfer_num = ?";    
    $sql4 = "UPDATE app_import_payment_draft SET invoiceid = ?, find_invoices = ? WHERE transfer_num = ?";
    $sql6 = "UPDATE app_import_payment_draft SET accountid = ? WHERE transfer_num = ?";    
    
 

    $records = $this->db->pquery($sql, array($id));
    $pattern = "/[A-Za-z]{2}\d{2}\/\\d{2}\/\\d{2}\/\\d{3}/";
    $pattern2 = "/[A-Za-z]{2}\d{2}\d{2}\d{2}\d{3}/";

    $matches = array();
    $matches2 = array();
    $numbers = array();
    $numbers2 = array();

    $get_all_accounts = $this->db->pquery("SELECT * FROM vtiger_account_iban", array());
   
    $accounts_arr = [];
    foreach ($get_all_accounts as $acc) {
      $accounts_arr[trim($acc['iban'])] = $acc['accountid'];
    }
        
    foreach($records AS $row){
      preg_match_all($pattern, $row['comment'], $matches);          
      preg_match_all($pattern2, $row['comment'], $matches2);          
      $numbers = array_merge(...$matches);
      $numbers = array_map($addQuotes, $numbers);

      $numbers2 = array_merge(...$matches2);
      $numbers2 = array_map($addQuotes2, $numbers2);

      $grand_number = array_merge($numbers,$numbers2);

      $code = implode(",", $grand_number);

      if(!empty($code)){      
        $this->db->pquery($sql2, array($code,$row['transfer_num']));   
      }

      $accountname = trim(str_replace("UAB", "", $row['client_name']));
      $accountname = trim(str_replace("SIA", "", $accountname));
      $accountname = trim(str_replace("'", "", $accountname));
      $accountname = trim(str_replace('"', "", $accountname));
      $accountname = trim(str_replace(',', "", $accountname));
      $get_account_id_by_name = "SELECT accountid,accountname FROM vtiger_account 
                                LEFT JOIN vtiger_crmentity e ON e.crmid=accountid
                                WHERE accountname LIKE '%$accountname%' AND deleted = 0";  
       
      $accountid = (!empty($accounts_arr[trim($row['iban'])]) ? $accounts_arr[trim($row['iban'])] : 0);

      if(!$accountid){
        $account_info = $this->db->pquery($get_account_id_by_name, array());  
        $account_num = $this->db->num_rows($account_info);
        if($account_num > 1){
          $count_equivalent = 0;
          foreach ($account_info as $item) {
            $accountname2 = trim(str_replace("UAB", "", $item['accountname']));
            $accountname2 = trim(str_replace("SIA", "", $accountname2));
            $accountname2 = trim(str_replace("'", "", $accountname2));
            $accountname2 = trim(str_replace('"', "", $accountname2));
            $accountname2 = trim(str_replace(',', "", $accountname2));   
            if($accountname2 == $accountname){
              $accountid = $item['accountid'];
              $count_equivalent++;
            }
          }

          if($count_equivalent > 1){
            foreach ($account_info as $item) {      
              $item['accountname'] = trim(str_replace(',', "", $item['accountname']));
              if($item['accountname'] == $row['client_name']){
                $accountid = $item['accountid']; 
                break;           
              }else{
                $accountid = 0;
              }            
            }
          }

        }else{
          $accountid = $this->db->query_result($account_info, 0, 'accountid'); 
        }

      }    

      if($accountid){
        $this->db->pquery($sql6, array($accountid,$row['transfer_num']));  
      }else{
        $accountid = 0;
      }
      
      
      if($code){           
        $sql3 = "SELECT vtiger_invoice.invoiceid, accountid
                                FROM vtiger_invoice 
                                JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                LEFT JOIN vtiger_crmentity e ON e.crmid=vtiger_invoice.invoiceid
                                WHERE invoice_no IN ($code) AND cf_1277 = 'Debetinė' AND e.deleted = 0
                                GROUP BY vtiger_invoice.invoiceid";

        $invoices = $this->db->pquery($sql3, array());  

        $invoice_id = [];   
        foreach($invoices AS $item){
          if(!empty($accountid)){ 
            if($accountid == $item['accountid']){
              $invoice_id[] = $item['invoiceid'];
            }else{            
              $this->getAllUserUnpaidInvoices($accountid,$row['transfer_num']);              
              break;
            }
          }else{
            $invoice_id[] = $item['invoiceid'];
          }
        }

        if(count($invoice_id) > 0){        
          $invoiceid = implode(',', $invoice_id);
    
          $this->db->pquery($sql4, array($invoiceid,$invoiceid, $row['transfer_num'])); 
        }else if(!empty($accountid)){
          $this->getAllUserUnpaidInvoices($accountid,$row['transfer_num']);
        }

      }else if($accountid){              
        $this->getAllUserUnpaidInvoices($accountid,$row['transfer_num']);
      } 
    }  

  }

  public function getAllUserUnpaidInvoices($accountid, $transfer_num){
    $getAllUnpaidInvoices = "SELECT debts_tks_suma,
                              CASE 
                                WHEN debts_tks_suma IS NULL
                                THEN FORMAT(IF(vtiger_invoice.region_id = 5,subtotal,total),2)
                                ELSE FORMAT((IF(vtiger_invoice.region_id = 5,subtotal,total) - SUM(d.debts_tks_suma)),2)
                              END AS debt, GROUP_CONCAT(vtiger_invoice.invoiceid) as invoiceid
                              FROM vtiger_invoice 
                              JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                              LEFT JOIN vtiger_crmentity e ON e.crmid=vtiger_invoice.invoiceid
                              LEFT JOIN vtiger_crmentityrel rel ON rel.crmid=e.crmid
                              LEFT JOIN vtiger_debts d ON d.debtsid=rel.relcrmid
                              WHERE e.deleted = 0 AND vtiger_invoice.accountid = ? AND vtiger_invoice.invoiceid AND cf_1277 = 'Debetinė'
                              GROUP BY vtiger_invoice.invoiceid
                              HAVING debt > 0 OR debts_tks_suma IS NULL";

    $update_draft = "UPDATE app_import_payment_draft SET invoiceid = ?, find_invoices = ? WHERE transfer_num = ?";

    $account_invoices = $this->db->pquery($getAllUnpaidInvoices, array($accountid)); 
    $all_invoices = [];
    foreach($account_invoices AS $inv){
      $all_invoices[] = $inv['invoiceid'];
    }

    $invoiceid = implode(',',$all_invoices);

    $this->db->pquery($update_draft, array($invoiceid, $invoiceid, $transfer_num)); 
  }


  public function getEntityId(){
    $sql = "SELECT id FROM `vtiger_crmentity_seq`";
    $sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
    $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
    $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
    $lock3 = 'UNLOCK TABLES';

    $this->db->pquery($lock, array()); 
      $get_id = $this->db->pquery($sql, array()); 
      $seq = $this->db->query_result($get_id,0,'id');    
    $this->db->pquery($lock3, array());    

    $new_seq = $seq + 1;   

    $this->db->pquery($lock2, array()); 
     $this->db->pquery($sql2, array($new_seq)); 
    $this->db->pquery($lock3, array()); 

    return $new_seq;
  }
  
  public function insertEntity($setype, $entity_id, $user_id, $description, $source, $label, $date){
    $sql = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, source, label, createdtime, modifiedtime) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
    $this->db->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description, $source, $label, $date, $date)); 
  }


}
