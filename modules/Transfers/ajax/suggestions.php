<?php

require_once "../../../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


$account = $conn->query("SELECT DISTINCT accountname, accountid 
                              FROM vtiger_account a
                              INNER JOIN vtiger_crmentity e ON e.crmid=a.accountid
                              WHERE setype = 'Accounts' AND e.deleted = 0");

                      
foreach($account AS $all_records){
  $accountid[] =  $all_records['accountid'];
  $accountname[] =  $all_records['accountname'];
}


$all_accounts = array(
  'client' => array(
    'accountid' => $accountid, 
    'accountname' => $accountname
  )
);

echo  json_encode($all_accounts);

}else{
  http_response_code(404);
}