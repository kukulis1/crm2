<?php
error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $invoiceid = $_POST['invoiceid'];
  $total = $_POST['total'];
  $date = $_POST['date'];
  $method = 'Pavedimu';
  $date_now = date("Y-m-d H:i:s");
  $transfers = $_POST['transfer'];
  $debtno = $_POST['debtno'];
  $xml_name = $_POST['xml_name'];
  $user_id = $_POST['user_id'];
  $invoiceid_arr = explode(",", $invoiceid);
  $total_arr = explode("|", $total);

  $get_transfer_all_invoices = [];
  $get_all_accountid = [];


  $info = $conn->query("SELECT vtiger_invoice.invoiceid, IF(ROUND(SUM(debts_tks_suma),2) IS NULL,0,ROUND(SUM(debts_tks_suma),2)) AS payed_debt,
          CASE 
            WHEN vtiger_debts.debts_tks_suma THEN ROUND((total - SUM(vtiger_debts.debts_tks_suma)),2)
            ELSE ROUND(total,2)
          END AS total, vtiger_invoice.accountid
        FROM vtiger_invoice 
        LEFT JOIN vtiger_account on vtiger_invoice.accountid=vtiger_account.accountid 
        LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
        LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
        LEFT JOIN vtiger_crmentity AS de ON de.crmid=vtiger_debts.debtsid
        WHERE vtiger_invoice.invoiceid IN ($invoiceid)
        GROUP BY vtiger_invoice.invoiceid	 
        ORDER BY invoiceid");

    $i=0;
    foreach($info AS $row){
        $invoice = $invoiceid_arr[$i];
        $total = $total_arr[$i]; 
        
        $get_transfer_all_invoices[$transfers[$i]][] = $invoice;
        $get_all_accountid[] = $row['accountid'];
        
        if($row['payed_debt'] < $row['total'] && $total != 0){        
          $entity_id = last_entity_record($conn);
          insert_entity($conn,'Debts', $entity_id, $user_id, NULL, '', 'XML', $date_now);
          insertDebt($conn,$entity_id,$date,$total,$method, $debtno);
          insert_entityrel($conn, $invoice, 'Invoice',  $entity_id, 'Debts');          
          $conn->query("INSERT INTO vtiger_debtscf (debtsid,cf_2665) VALUES ('$entity_id','$xml_name')");

        }else{
          $entity_id = true;
        }
        
        $i++;
    }

    foreach ($get_transfer_all_invoices AS $transfer => $invoiceid) {    
      $id = implode(',', $invoiceid);  
      $conn->query("UPDATE app_import_payment_draft SET invoiceid = '$id' WHERE transfer_num = '$transfer'");      
    }

    $transfer_numbers = "'".implode("','", array_keys($get_transfer_all_invoices))."'";
    $imploded_accountids = implode(',', $get_all_accountid);

    $get_draft_info = $conn->query("SELECT iban,accountid, client_name FROM app_import_payment_draft WHERE transfer_num IN ($transfer_numbers)");
    $get_all_accounts = $conn->query("SELECT * FROM vtiger_account_iban WHERE accountid IN ($imploded_accountids)");
   
    $accounts_arr = [];
    foreach ($get_all_accounts as $acc) {
      $accounts_arr[$acc['accountid']][$acc['iban']] = $acc['client_name'];
    }


    foreach ($get_draft_info as $customer){
      if(empty($accounts_arr[$customer['accountid']][$customer['iban']]) && $customer['accountid'] > 1){
        $conn->query("INSERT INTO vtiger_account_iban (iban,accountid, client_name) VALUES ('".$customer['iban']."', '".$customer['accountid']."', '".$customer['client_name']."')");
      }
    }

    if($entity_id){
      echo json_encode(['status' => 'success']);
    }else{
      echo json_encode(['status' => 'fail']);
    }


}else{
  http_response_code(404);
}
  

function insert_entity($conn,$setype, $entity_id, $user_id, $description, $label, $source, $date)
{

  $conn->query("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) 
                       VALUES ('$entity_id', '$user_id', '$user_id', '$setype', '$description', '$source', '$label', '$date', '$date')");
}

function insert_entityrel($conn, $invoiceid, $module, $entity_id, $relmodule)
{
  $conn->query("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) 
                       VALUES ('$invoiceid', '$module', '$entity_id', '$relmodule')");
}

function insertDebt($conn, $entity_id,$date,$total,$method,$debtsno)
{
  $conn->query("INSERT INTO vtiger_debts (debtsid,debtsno, debts_tks_data, debts_tks_suma, debts_tks_tipas)
                           VALUES ('$entity_id','$debtsno', '$date', '$total', '$method')");                        
}

function last_entity_record($conn)
{

  $result = $conn->query("SELECT id FROM vtiger_crmentity_seq");
  $productid = $result->fetch_assoc();
  $crmid = $productid['id'] + 1;   
  
  $conn->query("UPDATE vtiger_crmentity_seq SET id = $crmid");
  
  return $crmid;
}