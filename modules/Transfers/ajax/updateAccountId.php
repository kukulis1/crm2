<?php
error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $accountid = $_POST['accountid'];
  $accountname = $_POST['accountname'];
  $rowid = $_POST['rowid'];
    
  $update = $conn->query("UPDATE app_import_payment_draft SET accountid = $accountid, client_name =  '$accountname' WHERE id = $rowid");

  if($update){

    $sql = $conn->query("SELECT debts_tks_suma,
                    CASE 
                      WHEN debts_tks_suma IS NULL
                      THEN FORMAT(total,2)
                      ELSE FORMAT((total - SUM(d.debts_tks_suma)),2)
                    END AS debt, GROUP_CONCAT(i.invoiceid) as invoiceid
                    FROM vtiger_invoice i
                    JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=i.invoiceid
                    LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid
                    LEFT JOIN vtiger_crmentityrel rel ON rel.crmid=e.crmid
                    LEFT JOIN vtiger_debts d ON d.debtsid=rel.relcrmid
                    WHERE e.deleted = 0 AND i.accountid = $accountid AND i.invoiceid AND cf_1277 = 'Debetinė'
                    GROUP BY i.invoiceid
                    HAVING debt > 0 OR debts_tks_suma IS NULL
                   ");
  
    $invoices = [];
    foreach($sql AS $row){
      $invoices[] = $row['invoiceid'];
    }

    $invoiceid = implode(',',$invoices);


    $conn->query("UPDATE app_import_payment_draft SET invoiceid = '$invoiceid' WHERE id = $rowid");


    echo 'true';
  }else{
    echo 'false';
  }

}else{
  http_response_code(404);
}
  
