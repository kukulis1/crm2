<?php

include "modules/Transfers/models/transfers.php";

use modules\Transfers\models\Transfers as Transfers;

class Transfers_Draft_View extends Vtiger_Index_View {

       function __construct(){
              $this->transfers = new Transfers;  
       }

	public function process(Vtiger_Request $request) {   
              global $current_user;
              $recordid = $_GET['record']; 
              
              if($_POST['cover'] == 1){
                  $this->transfers->coverInvoices($_POST, $recordid, $current_user->id);  
              }              
                    
              $invoice_info = $this->transfers->getCustomerOrders($recordid);   
              $xml = $this->transfers->getXmlName($recordid);
              $viewer = $this->getViewer($request);    
              $viewer->assign('invoice_arr', $invoice_info);         
              $viewer->assign('record_id', $recordid);
              $viewer->assign('xml_name', $xml);         
              $viewer->assign('post', $_POST);         
              $viewer->view('Draft.tpl', $request->getModule()); 
       }
       

}
