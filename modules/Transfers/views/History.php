<?php

include "modules/Transfers/models/transfers.php";

use modules\Transfers\models\Transfers as Transfers;

class Transfers_History_View extends Vtiger_Index_View {

       function __construct(){
              $this->transfers = new Transfers;  
       }

	public function process(Vtiger_Request $request) {        
              global $site_URL;                    
              $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1; 
              $perPage = 50;   
              $startAt = $perPage * ($page - 1);
              $getHistory = $this->transfers->getHistory($perPage,$startAt);
             
              $total = $this->transfers->getAllPaymentst();  
              $url = $site_URL."/index.php?module=Transfers&view=History";
              $pagination = $this->get_pagination_links($page, ceil($total/$perPage), $url); 

              $viewer = $this->getViewer($request);    
              $viewer->assign('all_records', $total);
              $viewer->assign('pagination', $pagination);    
              $viewer->assign('getHistory', $getHistory); 
              $viewer->view('History.tpl', $request->getModule()); 
       }

       public function get_pagination_links($current_page, $total_pages, $url){
              $links = "";
              $links .= '<div class="pagination">';    
                    if ($total_pages >= 1 && $current_page <= $total_pages) {
                           $links .= "<a ".($current_page == 1 ? 'class=active' : '')." href=\"{$url}&page=1\">1</a>";
                           $i = max(2, $current_page - 5);
                           if ($i > 2)
                           $links .= "<div>...</div>";
                           for (; $i < min($current_page + 6, $total_pages); $i++) {
                           $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$i}\">{$i}</a>";
                           }
                           if ($i != $total_pages)
                           $links .= "<div>...</div>";
                           $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$total_pages}\">{$total_pages}</a>";
                    }
             $links .= '</div>';
             return $links;
             }
       

}
