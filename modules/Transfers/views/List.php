<?php

include "modules/Transfers/models/transfers.php";

use modules\Transfers\models\Transfers as Transfers;

class Transfers_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->transfers = new Transfers;  
       }

	public function process(Vtiger_Request $request) {          
                  
              if(isset($_POST["submit"])){                   
                 $this->transfers->uploadXml($_POST,$_FILES);
              }

                $getHistory = $this->transfers->getTodayImports();

                $viewer = $this->getViewer($request);        
                $viewer->assign('getHistory', $getHistory); 
                $viewer->view('List.tpl', $request->getModule()); 
       }
       

}
