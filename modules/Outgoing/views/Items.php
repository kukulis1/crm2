<?php

class Outgoing_Items_View extends Vtiger_Index_View {

	function __construct() {
		parent::__construct();
	}


  function process (Vtiger_Request $request) {	
    $recordId = $request->get('record');
		$moduleName = $request->getModule();
    $SHIPMENT_INFO = Outgoing_Record_Model::getMoreInfoFromStorage($recordId);
    $viewer = $this->getViewer ($request);
    $viewer->assign('SHIPMENT', $SHIPMENT_INFO);
    $viewName = $request->get('viewname');
    $viewer->view('Items.tpl', $moduleName);
  }



}