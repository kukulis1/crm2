<?php
require_once "../../../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $user_id = $_POST['user_id'];
  $results = array();
  $total_count = 0;

  $resposible = 'cf_1659'; // Atsakingas už užsakymus 1
  $manager = 'cf_1655'; // Kliento vadybininkas 2
  $debts = 'cf_1657'; // Atsakingas už skolas 3
  $creator = 'cf_1661'; // Sukures asmuo 4
  $creator2 = 'r.smcreatorid'; // Sukures asmuo


  // NOTE Kokybe dalis

  $get_permisions = mysqli_fetch_assoc($conn->query("SELECT * FROM vtiger_notification_permisions WHERE id = 1"));
  $get_responsibles = explode(",",$get_permisions['responsibles']);
  $get_roles = explode(",",$get_permisions['managers']);
  $get_persons = explode(",",$get_permisions['person']);

  $where = '';
  $CHECK = array();
  if(in_array(1,$get_responsibles)){
    $where .= " $resposible = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(2,$get_responsibles)){
    $where .= (count($CHECK) > 0 ? ' OR ' :''). " $manager = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(3,$get_responsibles)){
    $where .= (count($CHECK) > 0 ? ' OR ' :'')." $debts = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(4,$get_responsibles)){
    $where .= (count($CHECK) > 0 ? ' OR ' :'')." ($creator = $user_id OR $creator2 = $user_id)";
    $CHECK[] = 1;
  }


  $check_role = mysqli_fetch_assoc($conn->query("SELECT * FROM vtiger_user2role WHERE userid = $user_id"));
  $role = $check_role['roleid'];

  $query = "SELECT n.notificationid,r.setype,notificationno, notification_tks_relatedto, notification_tks_notes, IF(n.notification_tks_whodid = 26, 'Metrika',CONCAT(u.first_name,' ',u.last_name)) AS full_name,e.createdtime,  cf_2044 AS description, accountname
        FROM vtiger_notification n
        LEFT JOIN vtiger_notificationcf nc ON nc.notificationid=n.notificationid
        INNER JOIN vtiger_crmentity r ON r.crmid=n.notification_tks_relatedto
        INNER JOIN vtiger_crmentity e ON e.crmid=n.notificationid
        LEFT JOIN vtiger_users u ON u.id=n.notification_tks_whodid
        JOIN vtiger_salesorder s ON s.salesorderid=n.notification_tks_relatedto
        JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
        JOIN vtiger_account a ON a.accountid=s.accountid
        JOIN vtiger_accountscf acf ON acf.accountid=s.accountid";
        if(in_array($role,$get_roles)){
          $query .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id != 1"; 
        }else{
          $query .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id = $user_id"; 
        }
       $query .=" LEFT JOIN vtiger_notification_managers_seen ms ON ms.record_id=n.notificationid AND ms.user_id = $user_id
        WHERE e.deleted = 0 AND cf_2046 = 'kokybe'";
        if(in_array($role,$get_roles)){
         $query .= " AND IF(ms.id IS NOT NULL, (ms.seen = 0 AND IF(ms.snoose IS NULL, TRUE, TIMEDIFF(ms.snoose,now()) < 0) ),TRUE) AND IF(see.id IS NOT NULL, see.seen = 0, TRUE)";
        }else{
          $query .= " AND IF(see.id IS NOT NULL, (see.seen = 0 AND IF(see.snoose IS NULL, TRUE, TIMEDIFF(see.snoose,now()) < 0) ),TRUE) ";
        }     

        if(in_array($role,$get_roles)){}
        else{     
          if(in_array($user_id,$get_persons)){}
          else{    
            if(!empty($where)) $query .= " AND ";
            if(count($CHECK) > 0){         
              $query .= (count($CHECK) > 1 ? '(': ''). $where . (count($CHECK) > 1 ? ')': '');
            }else{
              $query .= ' AND n.notificationid = 0';
            }  
          }        
        }       
     

      $query .=  " ORDER BY e.createdtime DESC";


  $result = $conn->query($query);

  $total_count += mysqli_num_rows($result); 

  foreach($result AS $row){  
      $module = $row['setype'];
      $related_id = $row['notification_tks_relatedto'];
      $notificationno = $row['notificationno'];    
      $description = ($notificationno ? "Įrašo $notificationno " : '');  
      $description .= $row['notification_tks_notes']." ";
      $description .= translate($row['description'], $module);
      $results['result']['items'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description, 'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'follow' => 1, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);
 
        $results['result']['popupitems'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description,'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'follow' => 1, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);    
  }


  // NOTE Pretenziju dalis

  $get_permisions_claims = mysqli_fetch_assoc($conn->query("SELECT * FROM vtiger_notification_permisions WHERE id = 2"));
  $get_responsibles_claims = explode(",",$get_permisions_claims['responsibles']);
  $get_roles_claims = explode(",",$get_permisions_claims['managers']);
  $get_persons_claims = explode(",",$get_permisions_claims['person']);

  $where_claims = '';
  $CHECK = array();
  if(in_array(1,$get_responsibles_claims)){
    $where_claims .= " $resposible = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(2,$get_responsibles_claims)){
    $where_claims .= (count($CHECK) > 0 ? ' OR ' :''). " $manager = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(3,$get_responsibles_claims)){
    $where_claims .= (count($CHECK) > 0 ? ' OR ' :'')." $debts = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(4,$get_responsibles_claims)){
    $where_claims .= (count($CHECK) > 0 ? ' OR ' :'')." ($creator = $user_id OR $creator2 = $user_id)";
    $CHECK[] = 1;
  }


  $query_claims = "SELECT n.notificationid,r.setype,notificationno, notification_tks_relatedto, notification_tks_notes, IF(n.notification_tks_whodid = 26, 'Metrika', IF(n.notification_tks_whodid = 0,'EMAIL', CONCAT(u.first_name,' ',u.last_name))) AS full_name,e.createdtime,  cf_2044 AS description, IF(accountname IS NULL,'---',accountname) AS accountname ,cf_1924 AS review
        FROM vtiger_notification n
        LEFT JOIN vtiger_notificationcf nc ON nc.notificationid=n.notificationid
        INNER JOIN vtiger_crmentity r ON r.crmid=n.notification_tks_relatedto
        INNER JOIN vtiger_crmentity e ON e.crmid=n.notificationid
        LEFT JOIN vtiger_users u ON u.id=n.notification_tks_whodid
        LEFT JOIN vtiger_claims c ON c.claimsid=n.notification_tks_relatedto
        LEFT JOIN vtiger_claimscf cf ON cf.claimsid=c.claimsid
        LEFT JOIN vtiger_account a ON a.accountid=c.cf_20023 ";
        if(in_array($role,$get_roles_claims)){
          $query_claims .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id != 1"; 
        }else{
          $query_claims .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id = $user_id"; 
        }
       $query_claims .=" LEFT JOIN vtiger_notification_managers_seen ms ON ms.record_id=n.notificationid AND ms.user_id = $user_id
        WHERE e.deleted = 0 AND cf_2046 = 'pretenzijos' AND n.notification_tks_whodid != $user_id";
        if(in_array($role,$get_roles_claims)){
         $query_claims .= " AND IF(ms.id IS NOT NULL, (ms.seen = 0 AND IF(ms.snoose IS NULL, TRUE, TIMEDIFF(ms.snoose,now()) < 0) ),TRUE) AND IF(see.id IS NOT NULL, see.seen = 0, TRUE)";
        }else{
          $query_claims .= " AND IF(see.id IS NOT NULL, (see.seen = 0 AND IF(see.snoose IS NULL, TRUE, TIMEDIFF(see.snoose,now()) < 0) ),TRUE) ";
        }     

        if(in_array($role,$get_roles_claims)){}
        else{     
          if(in_array($user_id,$get_persons_claims)){}
          else{    
            if(!empty($where_claims)) $query_claims .= " AND ";
            if(count($CHECK) > 0){         
              $query_claims .= (count($CHECK) > 1 ? '(': ''). $where_claims . (count($CHECK) > 1 ? ')': '');
            }else{
              $query_claims .= ' AND n.notificationid = 0';
            }  
          }        
        }       
     

      $query_claims .=  " ORDER BY e.createdtime DESC";


  $result_claims = $conn->query($query_claims);

  $total_count += mysqli_num_rows($result_claims);

  foreach($result_claims AS $row){  
    $module = $row['setype'];
    $notificationno = $row['notificationno'];   
    $related_id = $row['notification_tks_relatedto'];
    $description = (empty($row['review']) ? $notificationno : "Pretenzijoje $notificationno").' '.$row['notification_tks_notes']." ";
    $description .= translate($row['description'], $module);
    $results['result']['items'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description, 'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'follow' => 2, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);

      $results['result']['popupitems'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description,'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'follow' => 2, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);    
  }


  // NOTE Pirmas pristatytas kliento uzsakymas

  $get_permisions_first_order = mysqli_fetch_assoc($conn->query("SELECT * FROM vtiger_notification_permisions WHERE id = 3"));
  $get_responsibles_first_order = explode(",",$get_permisions_first_order['responsibles']);
  $get_roles_first_order = explode(",",$get_permisions_first_order['managers']);
  $get_persons_first_order = explode(",",$get_permisions_first_order['person']);

  $where_first_order = '';
  $CHECK = array();
  if(in_array(1,$get_responsibles_first_order)){
    $where_first_order .= " $resposible = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(2,$get_responsibles_first_order)){
    $where_first_order .= (count($CHECK) > 0 ? ' OR ' :''). " $manager = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(3,$get_responsibles_first_order)){
    $where_first_order .= (count($CHECK) > 0 ? ' OR ' :'')." $debts = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(4,$get_responsibles_first_order)){
    $where_first_order .= (count($CHECK) > 0 ? ' OR ' :'')." ($creator = $user_id OR $creator2 = $user_id)";
    $CHECK[] = 1;
  }


  $query_first_order = "SELECT n.notificationid,r.setype,notificationno, notification_tks_relatedto, notification_tks_notes, IF(n.notification_tks_whodid = 26, 'Metrika', IF(n.notification_tks_whodid = 0,'EMAIL', CONCAT(u.first_name,' ',u.last_name))) AS full_name,e.createdtime,  cf_2044 AS description, IF(accountname IS NULL,'---',accountname) AS accountname ,cf_1924 AS review
        FROM vtiger_notification n
        LEFT JOIN vtiger_notificationcf nc ON nc.notificationid=n.notificationid
        LEFT JOIN vtiger_salesorder ON vtiger_salesorder.accountid=n.notification_tks_relatedto  
        INNER JOIN vtiger_crmentity r ON r.crmid=n.notification_tks_relatedto
        INNER JOIN vtiger_crmentity e ON e.crmid=n.notificationid
        INNER JOIN vtiger_crmentity o ON o.crmid=salesorderid 
        LEFT JOIN vtiger_users u ON u.id=n.notification_tks_whodid
        LEFT JOIN vtiger_claims c ON c.claimsid=n.notification_tks_relatedto
        LEFT JOIN vtiger_claimscf cf ON cf.claimsid=c.claimsid
        LEFT JOIN vtiger_account a ON a.accountid=c.cf_20023 ";
        if(in_array($role,$get_roles_first_order)){
          $query_first_order .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id != 1"; 
        }else{
          $query_first_order .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id = $user_id"; 
        }
       $query_first_order .=" LEFT JOIN vtiger_notification_managers_seen ms ON ms.record_id=n.notificationid AND ms.user_id = $user_id
        WHERE e.deleted = 0 AND cf_2046 = 'pirmas_uzsakymas' AND o.smcreatorid = $user_id";
        if(in_array($role,$get_roles_first_order)){
         $query_first_order .= " AND IF(ms.id IS NOT NULL, (ms.seen = 0 AND IF(ms.snoose IS NULL, TRUE, TIMEDIFF(ms.snoose,now()) < 0) ),TRUE) AND IF(see.id IS NOT NULL, see.seen = 0, TRUE)";
        }else{
          $query_first_order .= " AND IF(see.id IS NOT NULL, (see.seen = 0 AND IF(see.snoose IS NULL, TRUE, TIMEDIFF(see.snoose,now()) < 0) ),TRUE) ";
        }     

        if(in_array($role,$get_roles_first_order)){}
        else{     
          if(in_array($user_id,$get_persons_first_order)){}
          else{    
            if(!empty($where_first_order)) $query_first_order .= " AND ";
            if(count($CHECK) > 0){         
              $query_first_order .= (count($CHECK) > 1 ? '(': ''). $where_first_order . (count($CHECK) > 1 ? ')': '');
            }else{
              $query_first_order .= ' AND n.notificationid = 0';
            }  
          }        
        }       
     

      $query_first_order .=  " GROUP BY n.notification_tks_relatedto  ORDER BY e.createdtime DESC";


  $result_first_order = $conn->query($query_first_order);

  $total_count += mysqli_num_rows($result_first_order);

  foreach($result_first_order AS $row){  
    $module = $row['setype'];
    $notificationno = $row['notificationno'];   
    $related_id = $row['notification_tks_relatedto'];
    $description = $row['notification_tks_notes'];
    $results['result']['items'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description, 'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'follow' => 3, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);

    $results['result']['popupitems'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description,'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'follow' => 3, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);    
  }



 // NOTE Tiekeju sutartys

 $get_permisions_vendor_contracts = mysqli_fetch_assoc($conn->query("SELECT * FROM vtiger_notification_permisions WHERE id = 4"));
 $get_responsibles_contracts = explode(",",$get_permisions_vendor_contracts['responsibles']);
 $get_managers_contracts = explode(",",$get_permisions_vendor_contracts['managers']);
 $get_persons_contracts = explode(",",$get_permisions_vendor_contracts['person']);

 $where_first_order = '';
 $CHECK = array();
 if(in_array(1,$get_responsibles_contracts)){
   $where_first_order .= " $resposible = $user_id";
   $CHECK[] = 1;
 }
 if(in_array(2,$get_responsibles_contracts)){
   $where_first_order .= (count($CHECK) > 0 ? ' OR ' :''). " $manager = $user_id";
   $CHECK[] = 1;
 }
 if(in_array(3,$get_responsibles_contracts)){
   $where_first_order .= (count($CHECK) > 0 ? ' OR ' :'')." $debts = $user_id";
   $CHECK[] = 1;
 }
 if(in_array(4,$get_responsibles_contracts)){
   $where_first_order .= (count($CHECK) > 0 ? ' OR ' :'')." ($creator = $user_id OR $creator2 = $user_id)";
   $CHECK[] = 1;
 }


 $query_vendor_contracts = "SELECT n.notificationid,r.setype,notificationno, notification_tks_relatedto, notification_tks_notes, e.createdtime, cf_2044 AS description,  IF(vendorname IS NULL,'---',vendorname) AS vendorname 
       FROM vtiger_notification n
       LEFT JOIN vtiger_notificationcf nc ON nc.notificationid=n.notificationid       
       INNER JOIN vtiger_crmentity r ON r.crmid=n.notification_tks_relatedto
       INNER JOIN vtiger_crmentity e ON e.crmid=n.notificationid       
       LEFT JOIN vtiger_users u ON u.id=n.notification_tks_whodid
       LEFT JOIN vtiger_vendor v ON v.vendorid=n.notification_tks_relatedto
       INNER JOIN vtiger_crmentity o ON o.crmid=v.vendorid AND o.deleted = 0
       LEFT JOIN vtiger_senotesrel on vtiger_senotesrel.crmid=v.vendorid ";

       if(in_array($role,$get_managers_contracts)){
         $query_vendor_contracts .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id != 1"; 
       }else{
         $query_vendor_contracts .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id = $user_id"; 
       }

      $query_vendor_contracts .=" LEFT JOIN vtiger_notification_managers_seen ms ON ms.record_id=n.notificationid AND ms.user_id = $user_id
       WHERE e.deleted = 0 AND cf_2046 = 'tiekeju_sutartys' ";

       if(in_array($role,$get_managers_contracts)){
        $query_vendor_contracts .= " AND IF(ms.id IS NOT NULL, (ms.seen = 0 AND IF(ms.snoose IS NULL, TRUE, TIMEDIFF(ms.snoose,now()) < 0) ),TRUE) AND IF(see.id IS NOT NULL, see.seen = 0, TRUE)";
       }else{
         $query_vendor_contracts .= " AND IF(see.id IS NOT NULL, (see.seen = 0 AND IF(see.snoose IS NULL, TRUE, TIMEDIFF(see.snoose,now()) < 0) ),TRUE) ";
       }     

       if(in_array($role,$get_managers_contracts)){}
       else{     
         if(in_array($user_id,$get_persons_contracts)){}
         else{    
           if(!empty($where_first_order)) $query_vendor_contracts .= " AND ";
           if(count($CHECK) > 0){         
             $query_vendor_contracts .= (count($CHECK) > 1 ? '(': ''). $where_first_order . (count($CHECK) > 1 ? ')': '');
           }else{
             $query_vendor_contracts .= ' AND n.notificationid = 0';
           }  
         }        
       }       
    

     $query_vendor_contracts .=  " GROUP BY n.notificationid  ORDER BY e.createdtime DESC";


 $result_vendor_contracts = $conn->query($query_vendor_contracts);

 $total_count += mysqli_num_rows($result_vendor_contracts);

 foreach($result_vendor_contracts AS $row){  
   $module = $row['setype'];
   $notificationno = $row['notificationno'];   
   $related_id = $row['notification_tks_relatedto'];
   $description = $row['notification_tks_notes'];
   $results['result']['items'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description, 'accountname' => $row['vendorname'], 'full_name' => 'Parnasas user', 'follow' => 4, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);

   $results['result']['popupitems'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description,'accountname' => $row['vendorname'], 'full_name' => 'Parnasas user', 'follow' => 4, 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);    
 }




  $results['result']['count'] = $total_count;
  if($total_count > 0){

    $results['success'] = true;
  }else{
    $results['success'] = false;
  }

  $results['result']['query'] = $query_vendor_contracts;


  echo json_encode($results);

}else{
  http_response_code(404);
}

function translate($key, $moduleName) {  
  if($moduleName){
   require("../../../languages/lt_lt/$moduleName.php");
  }else{
    return $key;
  }
  $returnValue = '';
  if(!empty($languageStrings[$key])){
    $returnValue = $languageStrings[$key];
  }else{
    require("../../../languages/lt_lt/Vtiger.php");
    if(!empty($languageStrings[$key])){
      $returnValue = $languageStrings[$key];
    }else{
      $returnValue = $key;
    }
  }
  return $returnValue;
}

