<?php
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


  $user_id = $_POST['user_id'];

  $resposible = 'cf_1659'; // Atsakingas už užsakymus 1
  $manager = 'cf_1655'; // Kliento vadybininkas 2
  $debts = 'cf_1657'; // Atsakingas už skolas 3
  $creator = 'cf_1661'; // Sukures asmuo 4
  $creator2 = 'r.smcreatorid'; // Sukures asmuo

  $get_permisions = mysqli_fetch_assoc($conn->query("SELECT * FROM vtiger_notification_permisions WHERE id = 1"));
  $get_responsibles = explode(",",$get_permisions['responsibles']);
  $get_roles = explode(",",$get_permisions['managers']);
  $get_persons = explode(",",$get_permisions['person']);

  $where = '';
  $CHECK = array();
  if(in_array(1,$get_responsibles)){
    $where .= " $resposible = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(2,$get_responsibles)){
    $where .= (count($CHECK) > 0 ? ' OR ' :''). " $manager = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(3,$get_responsibles)){
    $where .= (count($CHECK) > 0 ? ' OR ' :'')." $debts = $user_id";
    $CHECK[] = 1;
  }
  if(in_array(4,$get_responsibles)){
    $where .= (count($CHECK) > 0 ? ' OR ' :'')." ($creator = $user_id OR $creator2 = $user_id)";
    $CHECK[] = 1;
  }


  $check_role = mysqli_fetch_assoc($conn->query("SELECT * FROM vtiger_user2role WHERE userid = $user_id"));
  $role = $check_role['roleid'];

  $query = "SELECT n.notificationid,r.setype,notificationno, notification_tks_relatedto, notification_tks_notes, IF(n.notification_tks_whodid = 26, 'Metrika',CONCAT(u.first_name,' ',u.last_name)) AS full_name,e.createdtime,  cf_2044 AS description, accountname
        FROM vtiger_notification n
        LEFT JOIN vtiger_notificationcf nc ON nc.notificationid=n.notificationid
        LEFT JOIN vtiger_crmentity r ON r.crmid=n.notification_tks_relatedto
        LEFT JOIN vtiger_crmentity e ON e.crmid=n.notificationid
        LEFT JOIN vtiger_users u ON u.id=n.notification_tks_whodid
        JOIN vtiger_salesorder s ON s.salesorderid=n.notification_tks_relatedto
        JOIN vtiger_salesordercf scf ON scf.salesorderid=s.salesorderid
        JOIN vtiger_account a ON a.accountid=s.accountid
        JOIN vtiger_accountscf acf ON acf.accountid=s.accountid";
        if(in_array($role,$get_roles)){
          $query .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid"; 
        }else{
          $query .=" LEFT JOIN app_see_notification see ON see.record_id=n.notificationid AND see.user_id = $user_id"; 
        }
       $query .=" LEFT JOIN vtiger_notification_managers_seen ms ON ms.record_id=n.notificationid AND ms.user_id = $user_id
        WHERE e.deleted = 0 ";
        if(in_array($role,$get_roles)){
         $query .= " AND IF(ms.id IS NOT NULL, (ms.seen = 0 AND IF(ms.snoose IS NULL, TRUE, TIMEDIFF(ms.snoose,now()) < 0) ),TRUE) AND IF(see.id IS NOT NULL, see.seen = 0, TRUE)";
        }else{
          $query .= " AND IF(see.id IS NOT NULL, (see.seen = 0 AND IF(see.snoose IS NULL, TRUE, TIMEDIFF(see.snoose,now()) < 0) ),TRUE) ";
        }     

        if(in_array($role,$get_roles)){}
        else{     
          if(in_array($user_id,$get_persons)){}
          else{    
            if(!empty($where)) $query .= " AND ";
            if(count($CHECK) > 0){         
              $query .= (count($CHECK) > 1 ? '(': ''). $where . (count($CHECK) > 1 ? ')': '');
            }else{
              $query .= ' AND n.notificationid = 0';
            }  
          }        
        }       
     

      $query .=  " ORDER BY e.createdtime DESC";


  $result = $conn->query($query);

  $count = mysqli_num_rows($result);

  $results = array();
  $results['result']['count'] = $count;
  if($count > 0){
    $results['success'] = true;
  }else{
    $results['success'] = false;
  }

  foreach($result AS $row){  
      $module = $row['setype'];
      $related_id = $row['notification_tks_relatedto'];
      $notificationno = $row['notificationno'];    
      $description = ($notificationno ? "Įrašo $notificationno " : '');  
      $description .= $row['notification_tks_notes']." ";
      $description .= translate($row['description'], $module);
      $results['result']['items'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description, 'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);
 
        $results['result']['popupitems'][] = array('link' => "/index.php?module=$module&view=Detail&record=$related_id", 'description' => $description,'accountname' => $row['accountname'], 'full_name' => $row['full_name'], 'rel_id' => $related_id, 'id' => $row['notificationid'], 'createdtime' => $row['createdtime']);
     
    
  }

  $results['result']['query'] = $query;

  echo json_encode($results);

}else{
  http_response_code(404);
}


function translate($key, $moduleName) {  
  if($moduleName){
   require("../../../languages/lt_lt/$moduleName.php");
  }else{
    return $key;
  }
  $returnValue = '';
  if(!empty($languageStrings[$key])){
    $returnValue = $languageStrings[$key];
  }else{
    require("../../../languages/lt_lt/Vtiger.php");
    if(!empty($languageStrings[$key])){
      $returnValue = $languageStrings[$key];
    }else{
      $returnValue = $key;
    }
  }
  return $returnValue;
}

