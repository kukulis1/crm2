<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */
include_once 'include/Webservices/Revise.php';
include_once 'include/Webservices/Retrieve.php';

class NotificationHandler extends VTEventHandler {

    function handleEvent($eventName, $entityData) {     

        $moduleName = $entityData->getModuleName();

        //Get Current User Information
        global $current_user, $currentModule;

        if(!$current_user->id) $current_user->id = 26;

        if ($moduleName != 'Notification') {
          return;
        }

        /**
         * Adjust the balance amount against total & paid amount
         * NOTE: beforesave the total amount will not be populated in event data.
         */
        if ($eventName == 'vtiger.entity.aftersave') {         
            $notificationId = $entityData->getId();
            $triggeredModuleId = $_REQUEST['record'];
            $db = PearDatabase::getInstance();            
            $query = "UPDATE vtiger_notification SET notification_tks_relatedto = ?, notification_tks_whodid = ? WHERE notificationid = ?";
            $db->pquery($query, array($triggeredModuleId,$current_user->id,$notificationId));
           
        }
    }

}

?>
