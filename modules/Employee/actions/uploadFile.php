<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');  

if($_POST){
  try {
    $dbh->beginTransaction();  

    $current_user = $_POST['userid']; 
    $recordid = $_POST['recordid']; 

    $date = date('Y-m-d H:i:s');
    $root = $_SERVER['DOCUMENT_ROOT'];

    $file_name = $_FILES['file']['name'];
    $file_size = $_FILES['file']['size'];
    $file_tmp = $_FILES['file']['tmp_name'];

    $ext=strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
    $file_name_without_ext = pathinfo($file_name, PATHINFO_FILENAME);

    $time = time();
    $newName = lt_chars(str_replace(' ', '',pathinfo($file_name, PATHINFO_FILENAME))); 
    $newName = $newName."_".$time;

    $year = date("Y");
    $month = date("F");
    $dayofweek = "week".weekOfMonth(strtotime(date("Y-m-d")));  

    if (!file_exists("$root/documents_storage/$year/$month/$dayofweek")) {
      mkdir("$root/documents_storage/$year/$month/$dayofweek", 0777, true);
    }

    $target_dir = "$root/documents_storage/$year/$month/$dayofweek/"; 
    $uploadfile = $target_dir . $newName.".".$ext;  

    $server_path = "$year/$month/$dayofweek";

    if (move_uploaded_file($file_tmp, $uploadfile)) {   
      $sql2 = $dbh->prepare("INSERT INTO vtiger_documentstorage (documentstorageid,folder_id,folder_type,file_type, filename, path, temp_filename, size, extension) VALUES (?,?,?,?,?,?,?,?,?)");

      $sql3 = $dbh->prepare("INSERT INTO vtiger_documentstorage_employee_rel (documentstorageid, employid) VALUES (?,?)");

      $entity_id = getEntityId($dbh);
      insertEntity($dbh,'Documentstorage', $entity_id, $current_user, NULL, $file_name_without_ext, $date);

      $sql2->execute(array($entity_id,1,1,0,$file_name_without_ext,$server_path,$newName,$file_size,$ext));
      
      $sql3->execute(array($entity_id,$recordid));

      echo 'success';
    } else {
      echo "Fail";
    }  
  
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'fail'));
  }
}
     
function lt_chars($text) {
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

    return $text; 
}

function weekOfMonth($date){  
  $firstOfMonth = strtotime(date("Y-m-01", $date));
  return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}


function getEntityId($dbh){  
  $sql = $dbh->prepare("SELECT id FROM `vtiger_crmentity_seq`");
  $sql2 = $dbh->prepare("UPDATE `vtiger_crmentity_seq` SET id = ?");
  $lock = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq READ'); 
  $lock2 = $dbh->prepare('LOCK TABLES vtiger_crmentity_seq WRITE');
  $lock3 = $dbh->prepare('UNLOCK TABLES');

  $lock->execute(array()); 
    $sql->execute(array()); 
    $seq = $sql->fetch()['id'];    
  $lock3->execute(array());    

  $new_seq = $seq + 1;   

  $lock2->execute(array()); 
  $sql2->execute(array($new_seq)); 
  $lock3->execute(array()); 

  return $new_seq;
}

function insertEntity($dbh,$setype, $entity_id, $user_id, $description, $label, $date){
  $sql = $dbh->prepare('INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
  $sql->execute(array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date)); 
}