<?php

include "modules/Newpricebooks/models/listModel.php";

use modules\Newpricebooks\models\listModel as listModel;

class Newpricebooks_EditPricebook_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

       function process(Vtiger_Request $request) {
              
                $pricebookid = $_GET['record'];            
                
               if($_POST['pricebook_form'] ){      
                    $this->listModel->saveProducts($pricebookid, $_POST);   
               } 

               if($_POST['copy_pricebook']){                
                     $this->listModel->copyPricebook($_POST);  
               }  

               if($_POST['change_client']){
                     $clientName = $_POST['change_client'];
                     $this->listModel->changeClientEdit($pricebookid,$clientName);   
               }
               $accountname = $_GET['accountname'];
               $clientZones = $this->listModel->getClientCartZones($pricebookid,$accountname);
               $getZoneTypes = $this->listModel->getZoneTypes();
               $accountInfo = $this->listModel->getClientInfoByPriceBook($pricebookid,$accountname);
               $zonesFromPriceBook = $this->listModel->checkPriceBookHasZones($pricebookid);

                $products = $this->listModel->getPricebook($pricebookid);   
                $prices = $this->listModel->getPricebookPrice($pricebookid);   
                $pricebook_name = $this->listModel->getPriceBookName($pricebookid);              
                $zones_num = $this->listModel->getPriceBookZoneNum($pricebookid);
                $pricebookClients = $this->listModel->getPriceBookClients($pricebookid);
                              
                $viewer = $this->getViewer($request);                  
                $viewer->assign('pricebook_name', $pricebook_name);
                $viewer->assign('products', $products);
                $viewer->assign('prices', $prices);
                $viewer->assign('zones_num', $zones_num); 
                $viewer->assign('clientZones', $clientZones);
                $viewer->assign('zone_types', $getZoneTypes);
                $viewer->assign('accountInfo', $accountInfo);   
                $viewer->assign('pricebookClients', $pricebookClients);
                $viewer->assign('accountname', $accountname);              
                $viewer->assign('zonesFromPriceBook', $zonesFromPriceBook);              
                $viewer->view('EditPricebook.tpl', $request->getModule()); 


       }

}
