<?php
error_reporting(1);
include "modules/Newpricebooks/models/listModel.php";
use modules\Newpricebooks\models\listModel as listModel;
class Newpricebooks_Calculate_View extends Vtiger_Index_View {

  function __construct(){
    $this->listmodel = new listmodel;  
  }

	public function process(Vtiger_Request $request) {
    $pricebookid = $_GET['record']; 
    $calculate = $this->listmodel->reCalculatePrice($pricebookid); 
  }  

}
