<?php

include "modules/Newpricebooks/models/listModel.php";

use modules\Newpricebooks\models\listModel as listModel;

class Newpricebooks_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

	public function process(Vtiger_Request $request) {            
         

                $pricebooks = $this->listModel->getPricebookList(); 
                if($_GET['search']){
                     $pricebooks = $this->listModel->searchPricebook($_GET['search']);       
                }            
                $viewer = $this->getViewer($request);                  
                $viewer->assign('pricebooks', $pricebooks);
                $viewer->view('List.tpl', $request->getModule()); 
       }

}
