<?php

include "modules/Newpricebooks/models/listModel.php";
include "modules/Newpricebooks/models/sendMail.php";

use modules\Newpricebooks\models\listModel as listModel;
use modules\Newpricebooks\models\sendMail as sendMail;

class Newpricebooks_PriceBook_View extends Vtiger_Index_View {

       function __construct(){
          $this->listModel = new listModel;                
       }

       function process(Vtiger_Request $request) {
             
             
                $pricebookid = $_GET['record'];

              //   if($_POST['zone-count']){
              //        $num = $_POST['zone-count'];
              //        $this->listModel->setZoneNum($pricebookid, $num);   
              //   }                

                if($_POST['emailAddress']){
                    $sendMail = new sendMail; 
                    $sendMail->sendPriceBooksToClientEmail($_POST['emailAddress'],$_POST['message'],$pricebookid);  
                }

                if($_POST['change_client']){
                     $clientName = $_POST['change_client'];
                     $this->listModel->changeClient($pricebookid,$clientName);   
                }

                $pricebookClients = $this->listModel->getPriceBookClients($pricebookid);
               
                if($_GET['accountname']){
                  $accountname = $_GET['accountname'];
                }else{
                  $accountname = $pricebookClients[0];
                  $accountname = $accountname['accountname'];
                }
                $clientZones = $this->listModel->getClientCartZones($pricebookid,$accountname);
                $getZoneTypes = $this->listModel->getZoneTypes();
                $accountInfo = $this->listModel->getClientInfoByPriceBook($pricebookid,$accountname); 

                $products = $this->listModel->getPricebook($pricebookid);   
                $prices = $this->listModel->getPricebookPrice($pricebookid);   
                $pricebook_name = $this->listModel->getPriceBookName($pricebookid);              
                $zones_num = $this->listModel->getPriceBookZoneNum($pricebookid);
              
                              
                $viewer = $this->getViewer($request);                  
                $viewer->assign('pricebook_name', $pricebook_name);
                $viewer->assign('products', $products);
                $viewer->assign('prices', $prices);
                $viewer->assign('zones_num', $zones_num);
                $viewer->assign('clientZones', $clientZones);
                $viewer->assign('zone_types', $getZoneTypes);
                $viewer->assign('accountInfo', $accountInfo);
                $viewer->assign('pricebookClients', $pricebookClients);
                $viewer->assign('accountname', $accountname);
                $viewer->assign('pricebookid', $pricebookid);
                $viewer->view('Pricebook.tpl', $request->getModule()); 

                if (isset($_COOKIE['orders'])) {
                  unset($_COOKIE['orders']); 
                  setcookie('orders', null, -1, '/');  
                  unset($_COOKIE['pricebookid']); 
                  setcookie('pricebookid', null, -1, '/');            
                }

       }

}
