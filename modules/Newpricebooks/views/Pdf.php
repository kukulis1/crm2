<?php


include "modules/Newpricebooks/models/listModel.php";

use modules\Newpricebooks\models\listModel as listModel;

// require_once "vtlib/Vtiger/HTMLTOPDF/vendor/autoload.php"; 
// use Dompdf\Dompdf;

require_once 'vtlib/Vtiger/vendor/paragonie/random_compat/lib/random.php';
require_once 'vtlib/Vtiger/vendor/autoload.php';
use Mpdf\Mpdf;

class Newpricebooks_Pdf_View extends Vtiger_Index_View {

  function __construct(){
    $this->listmodel = new listmodel;  
  }

	public function process(Vtiger_Request $request) {
    $pricebookid = $_GET['record'];    
    $type = $_GET['type'];
    $pricebook_name = $this->listmodel->getPriceBookName($pricebookid);                
    $html = $this->listmodel->generatePdf($pricebookid, $pricebook_name); 

    $zone_num = $this->listmodel->getPriceBookZoneNum($pricebookid);
    $type = ($zone_num <= 9 ? 'p' : 'l');  
    $stylesheet = file_get_contents('layouts/v7/modules/Newpricebooks/resources/pdf.css');

    $mpdf = new Mpdf([
      'mode' => 'utf-8',
      'orientation' => $type,
      'tempDir' => 'storage'
    ]);
    
 

    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
    ob_clean();
    $mpdf->Output($pricebook_name."_kainoraštis.pdf", "D");
  }  

}
