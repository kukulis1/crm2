<?php

include "modules/Newpricebooks/models/listModel.php";

use modules\Newpricebooks\models\listModel as listModel;

class Newpricebooks_Edit_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

	public function process(Vtiger_Request $request) {              

                if($_POST['num_rows']){                
                  $this->listModel->saveNewPricebook($_POST);  
                }                                
                $viewer = $this->getViewer($request); 
                $viewer->view('Edit.tpl', $request->getModule()); 
       }

}
