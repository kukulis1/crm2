<?php

require_once "../../../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

    $params = array();
    $post = parse_str($_POST['form'], $params);
    $zones_num = $params['zone-count'];

    $row_array = Array();

    for($i = 1; $i <= $params['count_zones_value']; $i++)
    {  
      for($e = 1; $e <= $zones_num; $e++)
      { 
          $row_array['row-'.$e."-".$i] = array(
                                              'kg' => substr(number_format($params['max_weight'.$i], 3, '.', ''),0,-1), 
                                              'kg_min' =>   $params['min_kg'.$i],
                                              'm3' => substr(number_format($params['max_volume'.$i], 3, '.', ''),0,-1), 
                                              'm3_min' =>   $params['min_m3'.$i],
                                              'm2' => substr(number_format($params['max_square'.$i], 3, '.', ''),0,-1), 
                                              'm2_min' =>   $params['min_m2'.$i],
                                              'm' => substr(number_format($params['max_meter'.$i], 3, '.', ''),0,-1), 
                                              'm_min' =>   $params['min_m'.$i],
                                              'price' => $params['product-'.$e."-".$i], 
                                              'stevedoring' =>  $params['stevedoring'.$i], 
                                              'pll' =>  $params['pll'.$i], 
                                              'productid' => $params['productid-'.$e."-".$i]
                                            );
      }
    }
       
    asort($row_array);

    for($i = 1; $i <= $params['count_zones_value']; $i++)
    {
      for($e = 1; $e <= $zones_num; $e++)
      {     
        
          $k = $i-1;
          $watch = true;
          if($k > 0){ 
            $watch = false;        
          }
          
          // Kilogramai
          $min_weight_kg = $row_array['row-'.$e."-".$k]['kg']."1";
          $max_weight_kg = $row_array['row-'.$e."-".$i]['kg'];
          $min_kg = $row_array['row-'.$e."-".$i]['kg_min'];
    
          if($max_weight_kg > 0){
            if($watch){
              $min_weight_kg = '0.001';
            }

          }else{
            $min_weight_kg = '0.000';
            $max_weight_kg = '0.000';
          }

          if(isset($min_kg)){
            $min_weight_kg = '0.001';
            $min_kg = 1;
          }
          
          //Kubai
          $min_volume_m3 = $row_array['row-'.$e."-".$k]['m3']."1";
          $max_volume_m3 = $row_array['row-'.$e."-".$i]['m3'];
          $min_m3 = $row_array['row-'.$e."-".$i]['m3_min'];

          if($max_volume_m3 > 0){

            if($watch){
              $min_volume_m3 = '0.001';
            }   
            
          }else{
            $min_volume_m3 = '0.000';
            $max_volume_m3 = '0.000';
          }
          
          if(isset($min_m3)){
            $min_volume_m3 = '0.001';
            $min_m3 = 1;
          }
          

          //Kvadratai
          $min_square_m2 = $row_array['row-'.$e."-".$k]['m2']."1";  
          $max_square_m2 = $row_array['row-'.$e."-".$i]['m2']; 
          $min_m2 = $row_array['row-'.$e."-".$i]['m2_min'];

          if($max_square_m2 > 0) {
            if($watch){
              $min_square_m2 = '0.001';
            }
            
          }else{
            $min_square_m2 = '0.000';
            $max_square_m2 = '0.000';
          }

          if(isset($min_m2)){
            $min_square_m2 = '0.001';
            $min_m2 = 1;
          }

          //Metrai
          $min_meter = $row_array['row-'.$e."-".$k]['m']."1";  
          $max_meter = $row_array['row-'.$e."-".$i]['m']; 
          $min_m = $row_array['row-'.$e."-".$i]['m_min'];

          if($max_meter > 0) {
            if($watch){
              $min_meter = '0.001';
            }
            
          }else{
            $min_meter = '0.000';
            $max_meter = '0.000';
          }

          if(isset($min_m)){
            $min_meter = '0.001';
            $min_m = 1;
          }
  

            $price_value = $row_array['row-'.$e."-".$i]['price'];
            $stevedoring = $row_array['row-'.$e."-".$i]['stevedoring'];
            $pll = $row_array['row-'.$e."-".$i]['pll'];
            $proid = $row_array['row-'.$e."-".$i]['productid'];

                 
            $conn->query("UPDATE vtiger_products SET min_weight_kg = $min_weight_kg , max_weight_kg = $max_weight_kg, min_volume_m3 = $min_volume_m3, max_volume_m3 = $max_volume_m3, min_square_m2 = $min_square_m2, max_square_m2 = $max_square_m2, pll_pcs = $pll  WHERE productid = $proid");

            $conn->query("UPDATE vtiger_pricebookproductrel SET listprice = $price_value WHERE productid = $proid");  
              
            $check_prod =$conn->query("SELECT * FROM vtiger_productcf WHERE productid = $proid");
            $check_prod =  mysqli_num_rows($check_prod);

        
            if($check_prod){
              $conn->query("UPDATE vtiger_productcf SET cf_954 = $stevedoring, cf_1637 = $min_meter, cf_1639 = $max_meter, m_check = $min_m WHERE productid = $proid");    
            }else{
              $conn->query("INSERT INTO vtiger_productcf (productid, cf_954,cf_1637,cf_1639, m_check) VALUES ('$proid','$stevedoring','$min_meter','$max_meter','$min_m')");    
            }     
        }
      }


    echo  'Success';

}else{
  http_response_code(404);
}
