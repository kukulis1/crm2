<?php
require_once "../../../config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");


$account_id = $_POST['accountid'];
$which = $_POST['which'];



if($which == 'from'){
  $history = $conn->query("SELECT DISTINCT CONCAT(load_company,', ',bill_street,', ',bill_city,', ',bill_code) AS address, bill_code
                            FROM vtiger_salesorder
                            LEFT JOIN vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid                     
                            WHERE vtiger_salesorder.accountid = $account_id AND bill_code != ''
                            GROUP BY load_company
                            ORDER BY vtiger_salesorder.salesorderid DESC");

  foreach($history AS $all_records){
    $company[] =  $all_records['address'];
    $code[] =  $all_records['bill_code'];
  }
}





if($which == 'to'){
  $history2 = $conn->query("SELECT DISTINCT CONCAT(unload_company,', ',ship_street,', ',ship_city,', ',ship_code) AS address, ship_code
                            FROM vtiger_salesorder
                            LEFT JOIN vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid                     
                            WHERE vtiger_salesorder.accountid = $account_id AND ship_code != ''
                            GROUP BY unload_company 
                            ORDER BY vtiger_salesorder.salesorderid DESC");   

  foreach($history2 AS $all_records2){
    $company[] = $all_records2['address'];
    $code[] = $all_records2['ship_code'];
  }
}


$other_history = $conn->query("SELECT DISTINCT CONCAT(company,', ',street,', ',city,', ',code) AS address, code AS code FROM crm_customers_address 
                                      WHERE accountid = $account_id AND type = '$which'");

$checkIfAddressExist = mysqli_num_rows($other_history);

  if($which == 'from'){
    if($checkIfAddressExist){
      foreach($other_history AS $all_records){
        $company2[] =  $all_records['address'];
        $code2[] =  $all_records['code'];
      }
      
      $company = array_merge($company,$company2);
      $code = array_merge($code,$code2);
    }

    $all_history = array(
                    'load_company' => $company, 
                    'bill_code' => $code 
                    );
  }

  if($which == 'to'){
    if($checkIfAddressExist){
      foreach($other_history AS $all_records){
        $company2[] =  $all_records['address'];
        $code2[] =  $all_records['code'];
      }
      $company = array_merge($company,$company2);
      $code = array_merge($code,$code2);
    }

    $all_history = array(
                    'unload_company' => $company, 
                    'ship_code' => $code 
                    );                
  }
  
// echo "<pre>";
// print_R($all_history);
// echo "</pre>";


echo  json_encode($all_history);

}else{
  http_response_code(404);
}