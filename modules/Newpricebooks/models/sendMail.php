<?php
namespace modules\Newpricebooks\models;


require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// require_once "vtlib/Vtiger/HTMLTOPDF/vendor/autoload.php"; 
require_once 'vtlib/Vtiger/vendor/paragonie/random_compat/lib/random.php';
require_once 'vtlib/Vtiger/vendor/autoload.php';
use Mpdf\Mpdf;


class sendMail extends listModel {

  public function __construct()
  {
    global $adb;
    $this->db = $adb;   
  }

  public function sendPriceBooksToClientEmail($to,$message,$pricebookid)
  {
    global $current_user, $mailHost, $mailUsername,$mailPassword; 	
    $userid = $current_user->id;

    $sql = "SELECT email1,email2 FROM vtiger_users WHERE id = ?";
    $result = $this->db->pquery($sql, array($userid));

    $employeeEmail =  ($this->db->query_result($result,0,'email1') ?: $this->db->query_result($result,0,'email2'));
    $pricebook_name = $this->getPriceBookName($pricebookid); 
    $zone_num = $this->getPriceBookZoneNum($pricebookid);

    $type = ($zone_num <= 9 ? 'p' : 'l');   
    
    $html = $this->generatePdf($pricebookid,$pricebook_name);

    $filename = $pricebook_name."_kainoraštis.pdf";
    $path = 'uploads';   

    $stylesheet = file_get_contents('layouts/v7/modules/Newpricebooks/resources/pdf.css');

    $mpdf = new Mpdf([
      'mode' => 'utf-8',
      'orientation' => $type,
      'tempDir' => 'storage'
    ]);
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
    ob_clean();   

    $file = $path . "/" . $filename;

    $mpdf->Output($file,'F'); 

    $subject = 'Parnasas '.$pricebook_name.' kainoraštis';    
        
    $mail = new PHPMailer(true);

    try {
      //Server settings
      // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
      $mail->isSMTP();                                            // Send using SMTP
      $mail->Host       = $mailHost;                    // Set the SMTP server to send through
      $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
      $mail->Username   = $mailUsername;                     // SMTP username
      $mail->Password   = $mailPassword;                   // SMTP password
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
      $mail->Port       = 465;    
      $mail->AllowEmpty = true;  
      $mail->SMTPOptions = array(
          'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
          )
        );                              // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
        $mail->setLanguage('lt', '../vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
        $mail->CharSet = 'UTF-8';
        // $mail->SMTPDebug = false;

        $from = ($employeeEmail == $to ? 'Parnasas' : $employeeEmail); 

      //Recipients
      $mail->setFrom($mailUsername, $from);
      $mail->addAddress($to, $to);     // Add a recipient 

      // Attachments
 
      $mail->addAttachment($file);   
      
      // Add attachments
      // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

      // Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = $subject;
      $mail->Body    = $message;
      $mail->AltBody = $message;
      $mail->send();

    

      unlink($file);

      $referer = $_SERVER['PHP_SELF']."?module=Newpricebooks&view=PriceBook&record=$pricebookid";
      return header("Location: $referer");

    } catch (Exception $e) {
      echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

  }

}