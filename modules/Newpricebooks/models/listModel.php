<?php
namespace modules\Newpricebooks\models;
use PDO;

class listModel {

  public function __construct()
  {
    global $adb;
    // kas čia ?
    require('v1/external_data/mysql_connection.php');   
    $this->dbh = $dbh;  
    $this->db = $adb;  
  }

  public function reCalculatePrice($pricebookid)
  {   
    include 'v1/external_data/getPriceFromPriceBook.php';    

    $accountid = $this->getClientInfoByPriceBook($pricebookid,'')['accountid'];
     $year = date('Y');  
     $orders = 0; 
     $sth = $this->dbh->prepare("SELECT GROUP_CONCAT(CONCAT(i.ordered_weight,' ',i.ordered_length,'x',i.ordered_width,'x',i.ordered_height)) AS ordered, 
                                      GROUP_CONCAT(CONCAT(i.revised_weight,' ',i.revised_length,'x',i.revised_width,'x',i.revised_height)) AS revised, 
                                      GROUP_CONCAT(DISTINCT i.quantity) AS quantity,
                                      GROUP_CONCAT(DISTINCT i.external_load_id) AS external_load_id, 
                                      ROUND(cf_928,2) AS shipment_km,
                                      ROUND(SUM(DISTINCT i.m3),2) AS m3, sa.accountid, sa.salesorderid, bill_code,ship_code,
                                      CASE WHEN cargo_measure = 'pll' THEN i.quantity ELSE 0 END AS  pll, sa.external_order_id,bill_country,ship_country
                                        FROM vtiger_salesorder sa
                                        JOIN vtiger_account a ON a.accountid=sa.accountid 
                                        JOIN vtiger_salesordercf cf ON cf.salesorderid=sa.salesorderid
                                        INNER JOIN vtiger_crmentity e ON e.crmid=sa.salesorderid
                                        JOIN vtiger_inventoryproductrel i ON i.id = sa.salesorderid   
                                        JOIN vtiger_sobillads b ON b.sobilladdressid=sa.salesorderid   
                                        JOIN vtiger_soshipads s ON s.soshipaddressid=sa.salesorderid   
                                        LEFT JOIN vtiger_invoice_salesorders_list l ON l.salesorderid=sa.salesorderid
                                        INNER JOIN vtiger_crmentity pe ON pe.crmid=a.pricebook
                                        LEFT JOIN app_recount_orders_from_pricebook re ON re.salesorderid=sa.salesorderid 
                                        WHERE e.deleted = 0 AND sa.accountid = ? AND l.salesorderid IS NULL AND date_format(e.createdtime,'%Y') = ?  AND IF(re.salesorderid IS NULL, sa.salesorderid  > 0, re.count_time < pe.modifiedtime)
                                        GROUP BY sa.salesorderid 
                                        ORDER BY sa.salesorderid DESC LIMIT 100"); 

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($accountid,$year));
        $orders = $sth->rowCount();

        $sth2 = $this->dbh->prepare('UPDATE vtiger_salesorder SET      
                                                        total = ?,
                                                        subtotal = ?,                                                         
                                                        pre_tax_total = ?                                                     
                                            WHERE salesorderid = ?');       

        $sth3 = $this->dbh->prepare('UPDATE vtiger_salesordercf  SET cf_1297 = ? WHERE   salesorderid = ? ');  
        $sth4 = $this->dbh->prepare('UPDATE vtiger_inventoryproductrel  SET margin = ?  WHERE id = ? AND productid = 14244');    

        $sth9 = $this->dbh->prepare('INSERT INTO app_taxable_dimensions (salesorderid,type) VALUES (?,?)');
        $st10 = $this->dbh->prepare('SELECT salesorderid FROM app_taxable_dimensions WHERE salesorderid = ?');
        $sth11 = $this->dbh->prepare('UPDATE app_taxable_dimensions SET type = ? WHERE salesorderid = ?');  
        $sth12 = $this->dbh->prepare('INSERT INTO app_recount_orders_from_pricebook (salesorderid, count_time) VALUES (?,?)');

        $sth12 = $this->dbh->prepare('INSERT INTO app_recount_orders_from_pricebook (salesorderid, count_time) VALUES (?,?)'); 
        $sth13 = $this->dbh->prepare("SELECT salesorderid FROM app_recount_orders_from_pricebook WHERE salesorderid = ?");   
        $sth14 = $this->dbh->prepare("UPDATE app_recount_orders_from_pricebook SET count_time = ? WHERE salesorderid = ?");             
      
        while ($row = $sth->fetch()) {               
          $id = $row['accountid'];
          $distance = (empty($row['shipment_km']) ? '0' : $row['shipment_km']);
          $row['load_post_code'] = preg_replace("/[^0-9]/", "",$row['load_post_code']);
          $row['unload_post_code'] = preg_replace("/[^0-9]/", "",$row['unload_post_code']);
          $pll = $row['pll'];
          $date = date("Y-m-d");
          $date2 = date("Y-m-d H:i:s");         
          $quantity = explode(",", $row['quantity']);
          $external_load_id = explode(",", $row['external_load_id']);    
          $weight_ordered = array();
          $dim_ordered = array();
          $dim_ordered2 = array();
          $square_ordered = array();
          $square_ordered_temp = array();
          $weight_revised = array();
          $dim_revised = array();
          $dim_revised2 = array();
          $square_revised = array();
          $square_revised_temp = array();

          $ordered = explode(",",$row['ordered']); 
    
          for($i = 0; $i < count($ordered); $i++){
            $exploded_arr = explode(' ',$ordered[$i]);
            $weight_ordered[] = $exploded_arr[0];         
            $dim_ordered[] = array_product(explode('x', $exploded_arr[1])) * $quantity[$i];     
            $dim_ordered2[] = explode('x', $exploded_arr[1]); 
            $square_ordered_temp[] = explode('x', $exploded_arr[1]); 
            unset($square_ordered_temp[$i][2]);           
            $square_ordered[] = array_product($square_ordered_temp[$i]) * $quantity[$i];
          }

          $revised = explode(",",$row['revised']);
          
          for($i = 0; $i < count($revised); $i++){
            $exploded_arr2 = explode(' ',$revised[$i]);
            $weight_revised[] = $exploded_arr2[0];        
            $dim_revised2[] = explode('x', $exploded_arr2[1]);
            $square_revised_temp[] = explode('x', $exploded_arr2[1]); 
            unset($square_revised_temp[$i][2]);
            $square_revised[] = array_product($square_revised_temp[$i]) * $quantity[$i];         
          }

  
          $ordered_cargo_wgt = $exploded_arr[0];
          $revised_cargo_wgt = $exploded_arr2[0];

          $ordered_dim = explode('x', $exploded_arr[1]);
          $revised_dim = explode('x', $exploded_arr2[1]);

          $ordered_w = array_sum($weight_ordered);
          $ordered_m3 = ROUND(array_sum($dim_ordered),2);
          $ordered_m2 = array_sum($square_ordered);

          $revised_w = array_sum($weight_revised); 
          $revised_m3 = $row['m3'];
          $revised_m2 = array_sum($square_revised);  


          $getPrice_ordered = array();
          $getPrice_revised = array();
    
          $getPrice = array();
          if(!empty($row['bill_code']) AND !empty($row['ship_code']) AND (!empty($ordered_w) OR !empty($revised_w)) AND ($row['bill_country'] != "EST" AND $row['ship_country'] != "EST") ){

            if($ordered_w == $revised_w && $ordered_m3 == $revised_m3){
                $getPrice_ordered = getPriceFromPriceBook($this->dbh, $row['bill_code'],$row['ship_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'], 'CLIENT');         
                
                $taxable = $getPrice_ordered;
                $getPrice['price'] = $getPrice_ordered['price'];
                $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];
                $getPrice['type'] = 'revised';              
            }else{
                $getPrice_ordered = getPriceFromPriceBook($this->dbh, $row['bill_code'],$row['ship_code'],$id,$ordered_w,$ordered_m3, $ordered_m2,$pll,$distance,$row['bill_country'],$row['ship_country'], 'CLIENT');  
                $getPrice_revised = getPriceFromPriceBook($this->dbh, $row['bill_code'],$row['ship_code'],$id,$revised_w,$revised_m3, $revised_m2,$pll,$distance,$row['bill_country'],$row['ship_country'], 'CLIENT');              
                
                if($getPrice_ordered['price'] > $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_ordered['price'];
                    $getPrice['pricebook'] =  $getPrice_ordered['combination'];
                    $getPrice['stevedoring'] = $getPrice_ordered['stevedoring'];
                    $getPrice['type'] = 'ordered';
                    $taxable = $getPrice_ordered;
                }elseif($getPrice_ordered['price'] < $getPrice_revised['price']){
                    $getPrice['price'] = $getPrice_revised['price'];
                    $getPrice['pricebook'] =  $getPrice_revised['combination'];
                    $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                    $getPrice['type'] = 'revised';
                    $taxable = $getPrice_revised;
                }elseif($getPrice_ordered['price'] == $getPrice_revised['price']){                   
                        $getPrice['price'] = $getPrice_revised['price'];
                        $getPrice['pricebook'] =  $getPrice_revised['combination'];
                        $getPrice['stevedoring'] = $getPrice_revised['stevedoring'];
                        $getPrice['type'] = 'revised';
                        $taxable = $getPrice_revised;                        
                }
            }    
          }else{ 
              $getPrice = array('price' => 0);

              if(empty($row['bill_code']) AND empty( $row['ship_code']) AND !empty($ordered_w) AND !empty($revised_w)){
                  $getPrice = array('pricebook' => 'Nėra pašto kodų ir svorio','price' => 0);
              }elseif(empty($row['bill_code']) AND empty($row['ship_code'])){    
                  $getPrice = array('pricebook' => 'Nėra pašto kodų','price' => 0);
              }elseif(empty($row['bill_code'])){
                  $getPrice = array('pricebook' => 'Nėra pakrovimo pašto kodo','price' => 0);
              }elseif(empty($row['ship_code'])){
                  $getPrice = array('pricebook' => 'Nėra iškrovimo pašto kodo','price' => 0);
              }elseif(empty($ordered_w) AND empty($revised_w)){
                      $getPrice = array('pricebook' => 'Nenurodytas svoris','price' => 0);
              }elseif($row['bill_code'] == "EST" OR $row['ship_code'] == "EST"){
                  $getPrice = array('pricebook' => 'Estija','price' => 0);
              }
            
          }  


          $price = $getPrice['price'];  
          
          $st10->execute(array($row['salesorderid']));
          $check_record = $st10->fetch();
          if(empty($check_record['salesorderid'])){
              $sth9->execute(array($row['salesorderid'],$getPrice['type']));
            
          }else{
              $sth11->execute(array($getPrice['type'],$row['salesorderid']));
          }

            
          $sth2->execute(array(
            $price,
            $price,                                                     
            $price,                                      
            $row['salesorderid']
          ));   
        

          $sth3->execute(array(                       
            $getPrice['pricebook'],                 
            $row['salesorderid']          
          ));   
        
            
          $sth4->execute(array(                                                                                          
                              $price,                                                                                                  
                              $row['salesorderid']
          ));

          $sth13->execute(array($row['salesorderid']));
          if($sth13->rowCount()){
            $sth14->execute(array($date2,$row['salesorderid']));
          }else{
            $sth12->execute(array($row['salesorderid'],$date2));
          }
  
        }

        if($orders == 0){          
          setcookie("orders", 'Visi užsakymai perskaičiuoti', time()+3600, "/","", 0);
          setcookie("pricebookid", $pricebookid, time()+3600, "/","", 0);
        }
    
   
    $referer = $_SERVER['PHP_SELF']."?module=Newpricebooks&view=PriceBook&record=$pricebookid";
    return header("Location: $referer");
  }  

  public function generatePdf($pricebookid,$pricebook_name)
  {   

    $products = $this->getPricebook($pricebookid);
    $products_arr = [];  
    $count_empty_rows = [];  

    foreach($products as $product){
      $products_arr[] = array('max_weight' => $product['max_weight'],
                              'max_volume' => $product['max_volume'],
                              'max_square' => $product['max_square'],
                              'max_meter' => $product['max_meter'],
                              'stevedoring' => $product['stevedoring'],
                              'pll' => $product['pll']
      );

      if(empty($product['max_weight']) || $product['max_weight'] == 0){
        $count_empty_rows['max_weight'][] = 1;
      }

      if(empty($product['max_volume']) || $product['max_volume'] == 0){
        $count_empty_rows['max_volume'][] = 1;
      }

      if(empty($product['max_square']) || $product['max_square'] == 0){
        $count_empty_rows['max_square'][] = 1;
      }

      if(empty($product['max_meter']) || $product['max_meter'] == 0){
        $count_empty_rows['max_meter'][] = 1;
      }

      if(empty($product['stevedoring']) || $product['stevedoring'] == 0){
        $count_empty_rows['stevedoring'][] = 1;
      }

      if(empty($product['pll']) || $product['pll'] == 0){
        $count_empty_rows['pll'][] = 1;
      }
    }




    $products = [];
    $total = count($products_arr);
    $total_max_weight = count($count_empty_rows['max_weight']);
    $total_max_volume = count($count_empty_rows['max_volume']);
    $total_max_square = count($count_empty_rows['max_square']);
    $total_max_meter = count($count_empty_rows['max_meter']);
    $total_stevedoring = count($count_empty_rows['stevedoring']);
    $total_pll = count($count_empty_rows['pll']);
  
    foreach($products_arr as $key => $product){
      if($total != $total_max_weight){
        $products[$key]['max_weight'] = $product['max_weight'];
      }
      if($total != $total_max_volume){
        $products[$key]['max_volume'] = $product['max_volume'];
      }
      if($total != $total_max_square){
        $products[$key]['max_square'] = $product['max_square'];
      }
      if($total != $total_max_meter){
        $products[$key]['max_meter'] = $product['max_meter'];
      }
      if($total != $total_stevedoring){
        $products[$key]['stevedoring'] = $product['stevedoring'];
      }
      if($total != $total_pll){
        $products[$key]['pll'] = $product['pll'];    
      }
    }  
    
    
    $prices = $this->getPricebookPrice($pricebookid);           
    $zones_num = $this->getPriceBookZoneNum($pricebookid);

    $bigger_zone = array();

    foreach($prices as $price){
     $bigger_zone[] = $price['count'];
    }

    $count_zones_value = max($bigger_zone); 
    $font_size = ($zones_num > 8 ? 'font-size: 10px;' : 'font-size:12px;');
    $column_num = ($zones_num <= 9 ? 3 : 2); 
    $html = ''; 
    $html  .= '<span style="margin: 0;font-size:18px;">'.$pricebook_name.' kainoraštis</span>';
    $html  .= "  <table id='listview-table' align='center' class='table listview-table'>
      <tr>
        <td>
          <table id='listview-table' class='table listview-table'>
            <thead class='head-content'>
            <tr>";
            if($total != $total_max_weight){
              $html .= "<th style='width: 20px !important;$font_size;'>Kg</th>";
            }
            if($total != $total_max_volume){
              $html .= "<th style='width: 20px !important;$font_size;'>M3</th>";
            }
            if($total != $total_max_square){
              $html .= "<th style='width: 20px !important;$font_size;'>M2</th>";
            }
            if($total != $total_max_meter){
              $html .= "<th style='width: 20px !important;$font_size;'>M</th>";
            }
            if($total != $total_stevedoring){
              $html .= "<th style='width: 20px !important;$font_size;'>K/D</th>";
            }
            if($total != $total_pll){
              $html .= "<th style='width: 20px !important;$font_size;'>Pll</th>";
            }
            $html .= "</tr>
            </thead>
          <tbody class='content content2'>";
        foreach($products as $product){
          $html  .= "<tr>";
          if($total != $total_max_weight){
            $html  .= "<td style='width: 20px !important;text-align: center;'>".number_format($product['max_weight'], 2, '.', '')."</td>";
          }
          if($total != $total_max_volume){
            $html  .= "<td style='width: 20px !important;text-align: center;'>".number_format($product['max_volume'],2)."</td>";
          }
          if($total != $total_max_square){
            $html  .= "<td style='width: 20px !important;text-align: center;'>".number_format($product['max_square'],2)."</td>";   
          }
          if($total != $total_max_meter){
            $html  .= "<td style='width: 20px !important;text-align: center;'>".number_format($product['max_meter'],2)."</td>";
          }
          if($total != $total_stevedoring){
            $html  .= "<td style='width: 20px !important;text-align: center;'>".number_format($product['stevedoring'],2)."</td>";   
          }
          if($total != $total_pll){
            $html  .= "<td style='width: 20px !important;text-align: center;'>".number_format($product['pll'],2)."</td>";
          }   
          $html  .= "</tr>";  
        }  
      $html  .= '
      </tbody>
      </table> 
      </td>
      <td>
      <table id="listview-table" class="table listview-table">
            <thead class="head-content">
            <tr>';         
          for($len=1; $len <= $zones_num; $len++):          
            $html  .= "<th style='width: 100px !important;$font_size;text-align: center;'>Zona $len</th>";
          endfor;

        $html  .= '    </tr>
            </thead>
          <tbody class="content">';             
      for($len=0; $len <= $count_zones_value-2; $len++){
        $html  .= '<tr>';   
          for($c=1; $c <= $zones_num; $c++){          
            $zone = "zone$c";

            foreach($prices AS $zona){   
              if($zona[$zone]):                
                $html .= "<td class=".($zones_num == $c ? 'last' :'')." style='width: 20px !important;text-align: center;'>".$zona[$zone][$len]."</td>";
              endif;
            }
          }
          $html  .= '</tr> ';

      }
      $html  .= '
      </tbody>
      </table>  
      </td>
      </tbody>
      </table> ';

      $acc = $this->getClientInfoByPriceBook($pricebookid,'');
      $accountname = $acc['accountname'];

      $zones = $this->getClientCartZones($pricebookid,$accountname);
      $zones_arr = array();
  
      $columns = Array('Zona 1' => '1218','Zona 2' => '1222','Zona 3' => '1226','Zona 4' => '1230','Zona 5' => '1234','Zona 6' => '1238','Zona 7' => '1242','Zona 8' => '1246','Zona 9' => '1250','Zona 10' => '1254','Zona 11' => '1400','Zona 12' => '1404','Zona 13' => '1408','Zona 14' => '1412','Zona 15' => '1416','Zona 16' => '1420','Zona 17' => '1424','Zona 18' => '1428','Zona 19' => '1432','Zona 20' => '1436');
  
      foreach($zones AS $zone){  
        foreach($columns AS $zona => $column){ 
          if(!empty($zone["cf_$column"])){
            $zones_arr[$zona] = $zone["cf_$column"];
          }
        }
      }   

      $html .= '<div class="container"><div class="row">';

      $i = 1;
      foreach ($zones_arr as $zona => $combination) {
        $comb = str_replace(':',':;',$combination);
        $comb = substr($comb,1,-1);
        $comb = str_replace(array(';',':',' '),array('','<br>','-'),$comb);        
        $html .= "<div class='col-xs-$column_num text-left'><h4>$zona</h4>$comb</div>";     
        $i++;  
      }

      $html .= '</div></div>';    
    return $html;
  }



  public function getPricebookList()
  {

    $sql = "SELECT * FROM vtiger_pricebook  
                     LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_pricebook`.`pricebookid`
                     WHERE `vtiger_crmentity`.`deleted` = 0 
                     GROUP BY `vtiger_pricebook`.`pricebookid`
                     ORDER BY `vtiger_pricebook`.`pricebookid` DESC";  

    $records = $this->db->pquery($sql, array());
    return $records ;
  }

  public function searchPricebook($pricebook)
  {

    $sql = "SELECT * FROM vtiger_pricebook  
                     LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_pricebook`.`pricebookid`
                     WHERE `vtiger_crmentity`.`deleted` = 0  AND `vtiger_pricebook`.`bookname` LIKE '%$pricebook%'
                     ORDER BY `vtiger_pricebook`.`pricebookid` DESC";  

    $records = $this->db->pquery($sql, array());
    return $records ;
  }

  public function getPricebook($pricebookid)
  {  

    $rez = $this->getProductsCount($pricebookid);
    foreach($rez as $count){
      $zona = array_keys($count, max($count));
    }
    $zona = str_replace("_", " ",$zona[1]);

  

    $sql = "SELECT DISTINCT min_weight_kg AS min_weight, max_weight_kg as max_weight, min_volume_m3 AS min_volume, max_volume_m3 as max_volume, min_square_m2 AS min_square ,max_square_m2 as max_square,vtiger_products.productid, ROUND(cf_954,2) as stevedoring, vtiger_products.pll_pcs AS pll, kg_check,m3_check, m2_check, cf_1639 as max_meter, m_check
                    FROM `vtiger_pricebook`
                    LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                    LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                    LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                    LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                    WHERE `vtiger_crmentity`.`deleted` = 0 AND consignee = '$zona' AND `vtiger_pricebook`.`pricebookid` = ?
                    GROUP BY consignee,vtiger_products.productid
                    ORDER BY max_weight_kg,max_volume_m3,max_square_m2,cf_1639,pll_pcs";   

    $result = $this->db->pquery($sql, array($pricebookid));    
        
    return $result;
  }

  public function getProductsCount($pricebookid)
  {   

    $price_zones = $this->getPriceBookZoneNum($pricebookid);
    $sql = "SELECT  ";
                    for($i = 1; $i <= $price_zones; $i++){   
                      $sql .=  " COUNT(CASE WHEN consignee = 'Zona $i' THEN `vtiger_products`.`productid` END) as Zona_$i ";
                      $sql .= ($i >= 1 && $i < $price_zones ? ',' : '');
                    }         

                     $sql .= " FROM `vtiger_pricebook`
                     LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                     LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                     LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                     LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                     WHERE `vtiger_crmentity`.`deleted` = 0  AND `vtiger_pricebook`.`pricebookid` = ? ";

  
                 
     return $this->db->pquery($sql, array($pricebookid));   
  }

  public function getPricebookPrice($pricebookid)
  {   

    $price_zones = $this->getPriceBookZoneNum($pricebookid);
    $sql = "SELECT DISTINCT ";
                    for($i = 1; $i <= $price_zones; $i++){   
                      $sql .=  " CASE WHEN consignee = 'Zona $i' THEN ROUND(listprice,2) END as zone$i ";
                      $sql .= ($i >= 1  ? ',' : '');
                    }

                    for($i = 1; $i <= $price_zones; $i++){   
                      $sql .= " CASE WHEN consignee = 'Zona $i' THEN `vtiger_products`.`productid` END as productid$i ";
                      $sql .= ($i < $price_zones ? ',' : '');
                    }

                     $sql .= " FROM `vtiger_pricebook`
                     LEFT JOIN `vtiger_pricebookproductrel` ON `vtiger_pricebookproductrel`.`pricebookid` = `vtiger_pricebook`.`pricebookid`
                     LEFT JOIN `vtiger_products` ON `vtiger_pricebookproductrel`.`productid` = `vtiger_products`.`productid`
                     LEFT JOIN `vtiger_productcf` ON `vtiger_products`.`productid` = `vtiger_productcf`.`productid`
                     LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_products`.`productid`
                     WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = ? 
                     GROUP BY consignee,vtiger_products.productid
                     ORDER BY max_weight_kg,max_volume_m3,max_square_m2,cf_1639,pll_pcs
                    ";

                 
     $result = $this->db->pquery($sql, array($pricebookid));   
     $zones = Array();


     $num_rows = $this->db->num_rows($result);


     for($i=1; $i<=$num_rows; $i++)
     { 
       $zona1 = $this->db->query_result($result,$i-1,'zone1');
       $zona2 = $this->db->query_result($result,$i-1,'zone2');
       $zona3 = $this->db->query_result($result,$i-1,'zone3');
       $zona4 = $this->db->query_result($result,$i-1,'zone4');
       $zona5 = $this->db->query_result($result,$i-1,'zone5');
       $zona6 = $this->db->query_result($result,$i-1,'zone6');
       $zona7 = $this->db->query_result($result,$i-1,'zone7');
       $zona8 = $this->db->query_result($result,$i-1,'zone8');
       $zona9 = $this->db->query_result($result,$i-1,'zone9');
       $zona10 = $this->db->query_result($result,$i-1,'zone10');
       $zona11 = $this->db->query_result($result,$i-1,'zone11');
       $zona12 = $this->db->query_result($result,$i-1,'zone12');
       $zona13 = $this->db->query_result($result,$i-1,'zone13');
       $zona14 = $this->db->query_result($result,$i-1,'zone14');
       $zona15 = $this->db->query_result($result,$i-1,'zone15');
       $zona16 = $this->db->query_result($result,$i-1,'zone16');
       $zona17 = $this->db->query_result($result,$i-1,'zone17');
       $zona18 = $this->db->query_result($result,$i-1,'zone18');
       $zona19 = $this->db->query_result($result,$i-1,'zone19');
       $zona20 = $this->db->query_result($result,$i-1,'zone20');

       $productid1 = $this->db->query_result($result,$i-1,'productid1');
       $productid2 = $this->db->query_result($result,$i-1,'productid2');
       $productid3 = $this->db->query_result($result,$i-1,'productid3');
       $productid4 = $this->db->query_result($result,$i-1,'productid4');
       $productid5 = $this->db->query_result($result,$i-1,'productid5');
       $productid6 = $this->db->query_result($result,$i-1,'productid6');
       $productid7 = $this->db->query_result($result,$i-1,'productid7');
       $productid8 = $this->db->query_result($result,$i-1,'productid8');
       $productid9 = $this->db->query_result($result,$i-1,'productid9');
       $productid10 = $this->db->query_result($result,$i-1,'productid10');
       $productid11 = $this->db->query_result($result,$i-1,'productid11');
       $productid12 = $this->db->query_result($result,$i-1,'productid12');
       $productid13 = $this->db->query_result($result,$i-1,'productid13');
       $productid14 = $this->db->query_result($result,$i-1,'productid14');
       $productid15 = $this->db->query_result($result,$i-1,'productid15');
       $productid16 = $this->db->query_result($result,$i-1,'productid16');
       $productid17 = $this->db->query_result($result,$i-1,'productid17');
       $productid18 = $this->db->query_result($result,$i-1,'productid18');
       $productid19 = $this->db->query_result($result,$i-1,'productid19');
       $productid20 = $this->db->query_result($result,$i-1,'productid20');

       if($zona1){       
        $zone1[] = $zona1;
        $proid1[] = $productid1;
       }
       if($zona2){  
        $zone2[] = $zona2;
        $proid2[] = $productid2;
       }
       if($zona3){  
        $zone3[] = $zona3;
        $proid3[] = $productid3;
       }
       if($zona4){  
        $zone4[] = $zona4;
        $proid4[] = $productid4;
       }
       if($zona5){  
        $zone5[] = $zona5;
        $proid5[] = $productid5;
       }
       if($zona6){  
        $zone6[] = $zona6;
        $proid6[] = $productid6;
       }
       if($zona7){  
        $zone7[] = $zona7;
        $proid7[] = $productid7;
       }
       if($zona8){  
        $zone8[] = $zona8;
        $proid8[] = $productid8;
       }
       if($zona9){  
        $zone9[] = $zona9;
        $proid9[] = $productid9;
       }
       if($zona10){  
        $zone10[] = $zona10;
        $proid10[] = $productid10;
       }
       if($zona11){  
        $zone11[] = $zona11;
        $proid11[] = $productid11;
       }
       if($zona12){  
        $zone12[] = $zona12;
        $proid12[] = $productid12;
       }
       if($zona13){  
        $zone13[] = $zona13;
        $proid13[] = $productid13;
       }
       if($zona14){  
        $zone14[] = $zona14;
        $proid14[] = $productid14;
       }
       if($zona15){  
        $zone15[] = $zona15;
        $proid15[] = $productid15;
       }
       if($zona16){  
        $zone16[] = $zona16;
        $proid16[] = $productid16;
       }
       if($zona17){  
        $zone17[] = $zona17;
        $proid17[] = $productid17;
       }
       if($zona18){  
        $zone18[] = $zona18;
        $proid18[] = $productid18;
       }
       if($zona19){  
        $zone19[] = $zona19;
        $proid19[] = $productid19;
       }
       if($zona20){  
        $zone20[] = $zona20;
        $proid20[] = $productid20;
       }

     }

     $zone1['proid1'] = $proid1; $zone2['proid2'] = $proid2;  
     $zone3['proid3'] = $proid3; $zone4['proid4'] = $proid4;
     $zone5['proid5'] = $proid5; $zone6['proid6'] = $proid6;
     $zone7['proid7'] = $proid7; $zone8['proid8'] = $proid8;
     $zone9['proid9'] = $proid9; $zone10['proid10'] = $proid10;
     $zone11['proid11'] = $proid11; $zone12['proid12'] = $proid12;
     $zone13['proid13'] = $proid13; $zone14['proid14'] = $proid14;
     $zone15['proid15'] = $proid15; $zone16['proid16'] = $proid16;
     $zone17['proid17'] = $proid17; $zone18['proid18'] = $proid18;
     $zone19['proid19'] = $proid19; $zone20['proid20'] = $proid20;
    


     $zones[] = array('zone1' => $zone1, 'count' => count($zone1));
     $zones[] = array('zone2' => $zone2, 'count' => count($zone2));
     $zones[] = array('zone3' => $zone3, 'count' => count($zone3));
     $zones[] = array('zone4' => $zone4, 'count' => count($zone4));
     $zones[] = array('zone5' => $zone5, 'count' => count($zone5));
     $zones[] = array('zone6' => $zone6, 'count' => count($zone6));
     $zones[] = array('zone7' => $zone7, 'count' => count($zone7));
     $zones[] = array('zone8' => $zone8, 'count' => count($zone8));
     $zones[] = array('zone9' => $zone9, 'count' => count($zone9));
     $zones[] = array('zone10' => $zone10, 'count' => count($zone10));
     $zones[] = array('zone11' => $zone11, 'count' => count($zone11));
     $zones[] = array('zone12' => $zone12, 'count' => count($zone12));
     $zones[] = array('zone13' => $zone13, 'count' => count($zone13));
     $zones[] = array('zone14' => $zone14, 'count' => count($zone14));
     $zones[] = array('zone15' => $zone15, 'count' => count($zone15));
     $zones[] = array('zone16' => $zone16, 'count' => count($zone16));
     $zones[] = array('zone17' => $zone17, 'count' => count($zone17));
     $zones[] = array('zone18' => $zone18, 'count' => count($zone18));
     $zones[] = array('zone19' => $zone19, 'count' => count($zone19));
     $zones[] = array('zone20' => $zone20, 'count' => count($zone20));

     return $zones;
  }

  public function getPriceBookName($pricebookid)
  {
    $sql = "SELECT bookname FROM vtiger_pricebook  
                            LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_pricebook`.`pricebookid`
                            WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = ?";

    $result = $this->db->pquery($sql, array($pricebookid));
    $name = $this->db->query_result($result,0,'bookname');
    
    return $name;                                    
  }

  public function getPriceBookZoneNum($pricebookid)
  {
    $sql = "SELECT zones FROM vtiger_pricebook  
                          LEFT JOIN `vtiger_crmentity` ON `vtiger_crmentity`.`crmid`=`vtiger_pricebook`.`pricebookid`
                          WHERE `vtiger_crmentity`.`deleted` = 0 AND `vtiger_pricebook`.`pricebookid` = ?";

    $result = $this->db->pquery($sql, array($pricebookid));
    $name = $this->db->query_result($result,0,'zones');

    return $name;                                    
  }

  public function setZoneNum($pricebookid,$num)
  {
    $sql = "UPDATE vtiger_pricebook SET zones = ? WHERE pricebookid = ?";
    $this->db->pquery($sql, array($num,$pricebookid));

    $referer = $_SERVER['HTTP_REFERER'];

    // return header("Location: $referer");
  }

  public function saveProducts($pricebookid, $post)
  {
    $zones_num = $post['zone-count'];
    $pricebookName = $post['pricebook_name'];
    $this->setZoneNum($pricebookid,$zones_num);
    $date = date("Y-m-d H:i:s");
    global $current_user;

    $sql = "UPDATE vtiger_products SET min_weight_kg =?, max_weight_kg = ?, min_volume_m3 = ?, max_volume_m3 = ?, min_square_m2 = ?, max_square_m2 =?,pll_pcs = ?  WHERE productid = ?";
    
    $sql2 = "UPDATE vtiger_pricebookproductrel SET listprice =? WHERE productid = ?";

    $sql22 = "UPDATE vtiger_productcf SET cf_954 = ?, cf_1637 = ?, cf_1639 = ?, m_check = ? WHERE productid = ?";

    $sql222 = "SELECT * FROM vtiger_productcf WHERE productid = ?"; 

    $sql3 = "INSERT INTO vtiger_products (productid,product_no,productname,productcategory,unit_price,discontinued,usageunit,qtyinstock,min_weight_kg,max_weight_kg,min_volume_m3,max_volume_m3,consignee,min_square_m2,max_square_m2,pll_pcs,pricebookid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 

    $sql4 = "INSERT INTO vtiger_productcf (productid, cf_954, cf_1637, cf_1639, m_check) VALUES (?,?,?,?,?)";

    $sql5 = "INSERT INTO vtiger_pricebookproductrel (pricebookid, productid,listprice,usedcurrency) VALUES (?,?,?,?)";

    $sql6 = "UPDATE vtiger_pricebook SET bookname = ? WHERE pricebookid = ?";

    $sql7 = "UPDATE vtiger_account SET pricebook = ? WHERE accountid = ?";

    $sql8 = "SELECT accountid FROM vtiger_account  WHERE pricebook = ?";

    $sql9 = "UPDATE vtiger_products SET kg_check = ?, m3_check = ?, m2_check = ? WHERE productid = ?";

    $sql10 = "UPDATE vtiger_crmentity SET modifiedby = ?, modifiedtime = ? WHERE crmid = ?";

    $result = $this->db->pquery($sql8, array($pricebookid));
    $accountid = $this->db->query_result($result,0,'accountid');



    $this->db->pquery($sql6, array($pricebookName,$pricebookid));
    if($post['accountid']){
      if(!empty($accountid)){
       $this->db->pquery($sql7, array(10965,$accountid));
      }
      $this->db->pquery($sql7, array($pricebookid,$post['accountid']));
    }else{
      $this->db->pquery("UPDATE vtiger_account SET pricebook = ? WHERE pricebook = ?",array('',$pricebookid));
    }

    if($post['copyZonesFromPriceBook']){
      $this->copyPriceBookZonesToClient($post['accountid'],$pricebookid);
    }
    
    $row_array = Array();

    for($i = 1; $i <= $post['count_zones_value']; $i++)
    {  
      for($e = 1; $e <= $zones_num; $e++)
      { 
          $row_array['row-'.$e."-".$i] = array(
                                              'kg' => substr (number_format($post['max_weight'.$i], 3, '.', '') ,0,-1),
                                              'kg_min' =>   $post['min_kg'.$i],
                                              'm3' => substr(number_format($post['max_volume'.$i], 3, '.', ''),0,-1), 
                                              'm3_min' =>   $post['min_m3'.$i],
                                              'm2' => substr(number_format($post['max_square'.$i], 3, '.', ''),0,-1), 
                                              'm2_min' =>   $post['min_m2'.$i],
                                              'm' => substr(number_format($post['max_meter'.$i], 3, '.', ''),0,-1), 
                                              'm_min' =>   $post['min_m'.$i],
                                              'price' => $post['product-'.$e."-".$i], 
                                              'stevedoring' =>  $post['stevedoring'.$i], 
                                              'pll' =>  $post['pll'.$i], 
                                              'productid' => $post['productid-'.$e."-".$i]
                                            );
      }
    }
       
    asort($row_array);

    for($i = 1; $i <= $post['count_zones_value']; $i++)
    {
      for($e = 1; $e <= $zones_num; $e++)
      {     
        
        $k = $i-1;
        $watch = true;
        if($k > 0){ 
          $watch = false;        
        }

      
          // Kilogramai
          $min_weight_kg = $row_array['row-'.$e."-".$k]['kg']."1";
          $max_weight_kg = $row_array['row-'.$e."-".$i]['kg'];
          $min_kg = $row_array['row-'.$e."-".$i]['kg_min']; 

          if($max_weight_kg > 0){

            if($min_weight_kg >=  $max_weight_kg){
              for($j = $post['count_zones_value']; $j >= 1; $j--){
                $min_weight_kg = $row_array['row-'.$e."-".$j]['kg']."1";        
                if($min_weight_kg <  $max_weight_kg){
                  break;
                }
              }
            }
      
            if($watch){
              $min_weight_kg = '0.001';
            }

          }else{
            $min_weight_kg = '0.000';
            $max_weight_kg = '0.000';
          }
          
          if(isset($min_kg)){
            $min_weight_kg = '0.001';
            $min_kg = 1;
          }
        
        // Kubai 
        $min_volume_m3 = $row_array['row-'.$e."-".$k]['m3']."1";
        $max_volume_m3 = $row_array['row-'.$e."-".$i]['m3'];
        $min_m3 = $row_array['row-'.$e."-".$i]['m3_min'];

        if($max_volume_m3 > 0){

          if($min_volume_m3 >=  $max_volume_m3){
            for($j = $post['count_zones_value']; $j >= 1; $j--){
              $min_volume_m3 = $row_array['row-'.$e."-".$j]['m3']."1";           
              if($min_volume_m3 <  $max_volume_m3){
                break;
              }
            }
          }
          if($watch){
            $min_volume_m3 = '0.001';
          }        
        }else{
          $min_volume_m3 = '0.000';
          $max_volume_m3 = '0.000';
        }

        if(isset($min_m3)){
          $min_volume_m3 = '0.001';
          $min_m3 = 1;
        }
      
        // Kvadratai
        $min_square_m2 = $row_array['row-'.$e."-".$k]['m2']."1";  
        $max_square_m2 = $row_array['row-'.$e."-".$i]['m2']; 
        $min_m2 = $row_array['row-'.$e."-".$i]['m2_min'];

        if($max_square_m2 > 0){  
          if($min_square_m2 >=  $max_square_m2){
            for($j = $post['count_zones_value']; $j >= 1; $j--){
              $min_square_m2 = $row_array['row-'.$e."-".$j]['m2']."1";
              if($min_square_m2 <  $max_square_m2){
                break;
              }
            }
          }
          if($watch){
            $min_square_m2 = '0.001';
          }   
        }else{
            $min_square_m2 = '0.000';
            $max_square_m2 = '0.000';
        }

        if(isset($min_m2)){
          $min_square_m2 = '0.001';
          $min_m2 = 1;
        }


        // Metrai
        $min_meter = $row_array['row-'.$e."-".$k]['m']."1";  
        $max_meter = $row_array['row-'.$e."-".$i]['m']; 
        $min_m = $row_array['row-'.$e."-".$i]['m_min'];


        if($max_meter > 0){  
          if($min_meter >=  $max_meter){
            for($j = $post['count_zones_value']; $j >= 1; $j--){
              $min_meter = $row_array['row-'.$e."-".$j]['m']."1";
              if($min_meter <  $max_meter){
                break;
              }
            }
          }
          if($watch){
            $min_meter = '0.001';
          }   
        }else{
            $min_meter = '0.000';
            $max_meter = '0.000';
        }

        if(isset($min_m)){
          $min_meter = '0.001';
          $min_m = 1;
        }
     

          $price_value = $row_array['row-'.$e."-".$i]['price'];
          $stevedoring = $row_array['row-'.$e."-".$i]['stevedoring'];
          $pll = $row_array['row-'.$e."-".$i]['pll'];
          $proid = $row_array['row-'.$e."-".$i]['productid'];


         if($proid){  
                  
            $this->db->pquery($sql, array($min_weight_kg,$max_weight_kg,$min_volume_m3,$max_volume_m3,$min_square_m2,$max_square_m2,$pll,$proid));
            $this->db->pquery($sql2, array($price_value,$proid));  


            $this->db->pquery($sql9, array($min_kg,$min_m3,$min_m2,$proid));  
            
            $check_prod = $this->db->pquery($sql222, array($proid));
            $check_prod =  $this->db->num_rows($check_prod);

        
            if($check_prod){
              $this->db->pquery($sql22, array($stevedoring,$min_meter,$max_meter,$min_m,$proid));    
            }else{
              $this->db->pquery($sql4, array($proid,$stevedoring,$min_meter,$max_meter,$min_m));    
            }    
            
            $this->db->pquery($sql10, array($current_user->id,$date, $pricebookid));
         }else{

          if($price_value){
            $last_entity_record = $this->last_entity_record();
            $date = date("Y-m-d H:i:s");
            $productname = $this->getPriceBookName($pricebookid);
            $productname =  $productname." Zona ".$e." eilutė ".$i;
            $cur_id = $this->getProNumber();
            $insert_entity = $this->insert_entity('Products', $last_entity_record, 1, NULL,'CRM',  $productname, $date);

            $this->db->pquery($sql3, array($last_entity_record,$cur_id, $productname,'Pervežimas',$price_value,1,0,999, $min_weight_kg,$max_weight_kg,$min_volume_m3,$max_volume_m3,'Zona '.$e,$min_square_m2,$max_square_m2,$pll,$pricebookid));

            $this->db->pquery($sql4, array($last_entity_record,$stevedoring,$min_meter,$max_meter,$min_m));

            $this->db->pquery($sql5, array($pricebookid, $last_entity_record, $price_value,1));

            $this->db->pquery($sql9, array($min_kg,$min_m3,$min_m2,$last_entity_record));  

          }
         }   
         
         
        }  
    }

    $referer = $_SERVER['PHP_SELF']."?module=Newpricebooks&view=EditPricebook&record=$pricebookid&success";
    return header("Location: $referer");
  }

  public function copyPriceBookZonesToClient($accountid,$pricebookid){
    $sql = "SELECT * FROM vtiger_pricebook_zones WHERE pricebookid = ?";

    $sql2 = "UPDATE vtiger_accountscf SET cf_1216 = ?, check_1218 = ?, cf_1218 = ?, cf_1220 = ?, check_1222 = ?, cf_1222 = ?, cf_1224 = ?, check_1226 = ?, cf_1226 = ?, cf_1228 = ?, check_1230 = ?, cf_1230 = ?, cf_1232 = ?, check_1234 = ?, cf_1234 = ?, cf_1236 = ?, check_1238 = ?, cf_1238 = ?, cf_1240 = ?, check_1242 = ?, cf_1242 = ?, cf_1244 = ?, check_1246 = ?, cf_1246 = ?, cf_1248 = ?, check_1250 = ?, cf_1250 = ?, cf_1252 = ?, check_1254 = ?, cf_1254 = ?, cf_1398 = ?, check_1400 = ?, cf_1400 = ?, cf_1402 = ?, check_1404 = ?, cf_1404 = ?, cf_1406 = ?, check_1408 = ?, cf_1408 = ?, cf_1410 = ?, check_1412 = ?, cf_1412 = ?, cf_1414 = ?, check_1416 = ?, cf_1416 = ?, cf_1418 = ?, check_1420 = ?, cf_1420 = ?, cf_1422 = ?, check_1424 = ?, cf_1424 = ?, cf_1426 = ?, check_1428 = ?, cf_1428 = ?, cf_1430 = ?, check_1432 = ?, cf_1432 = ?, cf_1434 = ?, check_1436 = ?, cf_1436 = ? WHERE accountid = ?";

    $result = $this->db->pquery($sql, array($pricebookid));

    foreach($result AS $row){    
      $this->db->pquery($sql2, array($row['cf_1216'], $row['check_1218'], $row['cf_1218'], $row['cf_1220'], $row['check_1222'], $row['cf_1222'], $row['cf_1224'], $row['check_1226'], $row['cf_1226'], $row['cf_1228'], $row['check_1230'], $row['cf_1230'], $row['cf_1232'], $row['check_1234'], $row['cf_1234'], $row['cf_1236'], $row['check_1238'], $row['cf_1238'], $row['cf_1240'], $row['check_1242'], $row['cf_1242'], $row['cf_1244'], $row['check_1246'], $row['cf_1246'], $row['cf_1248'], $row['check_1250'], $row['cf_1250'], $row['cf_1252'], $row['check_1254'], $row['cf_1254'], $row['cf_1398'], $row['check_1400'], $row['cf_1400'], $row['cf_1402'], $row['check_1404'], $row['cf_1404'], $row['cf_1406'], $row['check_1408'], $row['cf_1408'], $row['cf_1410'], $row['check_1412'], $row['cf_1412'], $row['cf_1414'], $row['check_1416'], $row['cf_1416'], $row['cf_1418'], $row['check_1420'], $row['cf_1420'], $row['cf_1422'], $row['check_1424'], $row['cf_1424'], $row['cf_1426'], $row['check_1428'], $row['cf_1428'], $row['cf_1430'], $row['check_1432'], $row['cf_1432'], $row['cf_1434'], $row['check_1436'], $row['cf_1436'],$accountid));
    }
  }


  public function saveNewPricebook($post)
  {
    global $current_user;

    $sql = "INSERT INTO vtiger_pricebook (pricebookid, pricebook_no, bookname, active, currency_id, zones) VALUES (?,?,?,?,?,?)"; 

    $sql2 = "INSERT INTO vtiger_products (productid,product_no,productname,productcategory,unit_price,discontinued,usageunit,qtyinstock,min_weight_kg,max_weight_kg,min_volume_m3,max_volume_m3,consignee,min_square_m2,max_square_m2, pll_pcs,pricebookid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 

    $sql3 = "INSERT INTO vtiger_pricebookproductrel (pricebookid, productid,listprice,usedcurrency) VALUES (?,?,?,?)";

    $sql4 = "INSERT INTO vtiger_productcf  (productid, cf_954,cf_1637,cf_1639) VALUES (?,?,?,?)";

    $sql5 = "UPDATE vtiger_account SET pricebook = ? WHERE accountid = ?";

    $pricebookName = $post['pricebook_name'];
    $zone_num = $post['zone_num'];
    $last_id = $this->last_entity_record();
    $pricebook_number = $this->getPriceBookNumber();
    $date = date("Y-m-d H:i:s");
    $this->db->pquery($sql, array($last_id,$pricebook_number,$pricebookName,1,1,$zone_num));
    if($post['accountid']){
      $this->db->pquery($sql5, array($last_id,$post['accountid']));
    }
    $insert_pricebook_entity = $this->insert_entity('PriceBooks', $last_id, $current_user->id, NULL,'CRM',  $pricebookName, $date);


    for($i = 1; $i <= $post['lines']; $i++)
    {
        for($e = 1; $e <= $zone_num; $e++)
        {

            $h = $i-1;


            $min_weight_kg = substr(number_format($post['max_weight'.$h], 3, '.', ''),0,-1)."1";
            $max_weight_kg = $post['max_weight'.$i];
          if(!empty($max_weight_kg) && $max_weight_kg > 0){
            if($h == 0){
              $min_weight_kg = '0.001';
            }           
          
          }else{
            $min_weight_kg = '0.000';
            $max_weight_kg = '0.000';
          }

          $min_volume_m3 = substr(number_format($post['max_volume'.$h], 3, '.', ''),0,-1)."1";
          $max_volume_m3 = $post['max_volume'.$i];

          if(!empty( $max_volume_m3) &&  $max_volume_m3 > 0){
            
            if($h == 0){
              $min_volume_m3 = '0.001';
            }         

          }else{
            $min_volume_m3 = '0.000';
            $max_volume_m3 = '0.000';
          }

          $min_square_m2 = substr(number_format($post['max_square'.$h], 3, '.', ''),0,-1)."1";
          $max_square_m2 = $post['max_square'.$i]; 

          if(!empty($max_square_m2) &&  $max_square_m2 > 0){         
            if($h == 0){
              $min_square_m2 = '0.001';
            }     
            
          }else{
            $min_square_m2 = '0.000';
            $max_square_m2 = '0.000';
          } 

          $min_meter = substr(number_format($post['max_meter'.$h], 3, '.', ''),0,-1)."1";
          $max_meter = $post['max_meter'.$i]; 

          if(!empty($max_meter) &&  $max_meter > 0){         
            if($h == 0){
              $min_meter = '0.001';
            }     
            
          }else{
            $min_meter = '0.000';
            $max_meter = '0.000';
          } 

          $stevedoring = $post['stevedoring'.$i];
          $pll = $post['pll'.$i];
          $price_value = $post['product-'.$e."-".$i];
         

            $last_entity_record = $this->last_entity_record();
            $date = date("Y-m-d H:i:s");
         
            $productname =  $pricebookName." Zona ".$e." eilutė ".$i;
            $cur_id = $this->getProNumber();
            $insert_entity = $this->insert_entity('Products', $last_entity_record, 1, NULL,'CRM',  $productname, $date);

            $this->db->pquery($sql2, array($last_entity_record,$cur_id, $productname,'Pervežimas',$price_value,1,0,999, $min_weight_kg,$max_weight_kg,$min_volume_m3,$max_volume_m3,'Zona '.$e,$min_square_m2,$max_square_m2,$pll,$last_id));
            $this->db->pquery($sql3, array($last_id, $last_entity_record, $price_value,1));                       
            $this->db->pquery($sql4, array($last_entity_record, $stevedoring,$min_meter, $max_meter));   
            
        }
    }


    $referer = $_SERVER['PHP_SELF']."?module=Newpricebooks&view=PriceBook&record=$last_id";
    return header("Location: $referer");
  }


  public function changeClient($pricebookid,$clientName)
  {
    $referer = $_SERVER['PHP_SELF']."?module=Newpricebooks&view=PriceBook&record=$pricebookid&accountname=$clientName";
    return header("Location: $referer");
  }

  public function changeClientEdit($pricebookid,$clientName)
  {
    $referer = $_SERVER['PHP_SELF']."?module=Newpricebooks&view=EditPricebook&record=$pricebookid&accountname=$clientName";
    return header("Location: $referer");
  }

  public function addField($pricebookid)
  {

    $sql = "INSERT INTO vtiger_products (productid,product_no,productname,productcategory,unit_price,discontinued,usageunit,qtyinstock,min_weight_kg,max_weight_kg,
    min_volume_m3,max_volume_m3,consignee,min_square_m2,max_square_m2,pricebookid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 

    $sql2 = "INSERT INTO vtiger_pricebookproductrel (pricebookid, productid,listprice,usedcurrency) VALUES (?,?,?,?)";

    $zones_num = $this->getPriceBookZoneNum($pricebookid);

    for($e = 1; $e <= $zones_num; $e++){

      $last_entity_record = $this->last_entity_record();
      $date = date("Y-m-d H:i:s");
      $productname = $this->getPriceBookName($pricebookid);
      $rand = rand(10,1000);
      $productname =  $productname." Zona ".$e." ".$rand;
      $cur_id = $this->getProNumber();
      $insert_entity = $this->insert_entity('Products', $last_entity_record, 1, NULL,'CRM',  $productname, $date);

      $this->db->pquery($sql, array($last_entity_record,$cur_id, $productname,'Pervežimas',$price_value,1,0,999, 0,0,0,0,'Zona '.$e,0,0,$pricebookid));

      $this->db->pquery($sql2, array($pricebookid, $last_entity_record, 0,1));
    }

    $referer = $_SERVER['HTTP_REFERER'];

    return header("Location: $referer");

  }


  public function insert_entity($setype, $entity_id, $user_id, $description, $label, $source, $date)
  {
  
    $sth = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) 
                              VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)';
    $sql_params = array($entity_id, $user_id, $user_id, $setype, $description, $label,$source, $date, $date);
    $this->db->pquery($sth, $sql_params);

    $sth2 = 'UPDATE vtiger_crmentity_seq SET id = ?';
    $this->db->pquery($sth2, array($entity_id));
  }

  public function last_entity_record()
  {

    $result = $this->db->pquery('SELECT id FROM vtiger_crmentity_seq');
    $productid1 = $this->db->query_result($result,0,'id');
    $crmid = $productid1 + 1;    
    
    return $crmid;
  }

  public function getProNumber()
  {
    $result = $this->db->pquery("SELECT prefix,cur_id FROM vtiger_modentity_num WHERE semodule = 'Products' AND active = 1");
    $prefix = $this->db->query_result($result,0,'prefix');
    $cur_id = $this->db->query_result($result,0,'cur_id');
    $new_cur_id = $cur_id+1;
    $update_cur_id = $this->db->pquery("UPDATE vtiger_modentity_num SET cur_id = $new_cur_id WHERE semodule = 'Products' AND active = 1");  
    return $prefix.$cur_id;
  }

  public function getPriceBookNumber()
  {
    $result = $this->db->pquery("SELECT prefix,cur_id FROM vtiger_modentity_num WHERE semodule = 'PriceBooks' AND active = 1");
    $prefix = $this->db->query_result($result,0,'prefix');
    $cur_id = $this->db->query_result($result,0,'cur_id');
    $new_cur_id = $cur_id+1;
    $update_cur_id = $this->db->pquery("UPDATE vtiger_modentity_num SET cur_id = $new_cur_id WHERE semodule = 'PriceBooks' AND active = 1");  
    return $prefix.$cur_id;
  }

  public function deleteProducts($pricebookid, $post)
  {

    $zones_num = $this->getPriceBookZoneNum($pricebookid);

    $sql = "DELETE FROM vtiger_products WHERE productid = ?";    
    $sql2 = "DELETE FROM vtiger_pricebookproductrel WHERE productid = ?";
    $sql3 = "DELETE FROM vtiger_productcf WHERE productid = ?";
    $sql4 = "DELETE FROM vtiger_crmentity WHERE crmid = ?";

        for($e = 1; $e <= $zones_num; $e++)
        {      
          $proid = $post['productid-'.$e."-".$post['delete_field']];               
          $this->db->pquery($sql, array($proid));
          $this->db->pquery($sql2, array($$proid)); 
          $this->db->pquery($sql3, array($proid));     
          $this->db->pquery($sql4, array($proid));   
        }

        $referer = $_SERVER['HTTP_REFERER'];
        return header("Location: $referer");
  }

  public function getClientInfoByPriceBook($pricebook,$accountname)
  {      
    if($pricebook != 10965){
      $sql = "SELECT  GROUP_CONCAT(accountid) AS accountid,accountname FROM vtiger_account
                                           LEFT JOIN vtiger_crmentity on vtiger_crmentity.crmid=accountid
                                           WHERE deleted =0 AND FIND_IN_SET('$pricebook', pricebook)";    
      if(isset($accountname)){
        $sql .= " AND accountname LIKE '$accountname%'";
      }                                     
     
    }else{
      $sql = "SELECT  accountid,accountname FROM vtiger_account
                                           LEFT JOIN vtiger_crmentity on vtiger_crmentity.crmid=accountid
                                           WHERE deleted =0 AND accountid = $pricebook";    
    }
      $result = $this->db->pquery($sql, array());
      $accountid = $this->db->query_result($result,0,'accountid');
      $accountname = $this->db->query_result($result,0,'accountname');

      if(empty($accountid)){
        $sql2 = "SELECT z.pricebookid AS accountid, p.bookname AS accountname 
                FROM vtiger_pricebook_zones z 
                JOIN vtiger_pricebook p ON p.pricebookid=z.pricebookid 
                WHERE z.pricebookid = $pricebook
                GROUP BY z.pricebookid ";

        $result2 = $this->db->pquery($sql2, array());
        $accountid = $this->db->query_result($result2,0,'accountid');
        $accountname = $this->db->query_result($result2,0,'accountname');      
      }

      return array('accountid' => $accountid, 'accountname' => $accountname);    
  }

  public function getPriceBookClients($pricebook)
  {
    if($pricebook == 10965){
      $accountid = 10965;
    }else{
      $accountid = $this->getClientInfoByPriceBook($pricebook,'');
      $accountid = $accountid['accountid'];    
    }
    if($accountid == $pricebook && $pricebook != 10965){
      $sql = "SELECT DISTINCT bookname AS accountname FROM vtiger_pricebook  WHERE pricebookid = $pricebook";
    }else{
      $sql = "SELECT accountname FROM vtiger_account  WHERE accountid IN ($accountid)";
    }
    $result = $this->db->pquery($sql, array()); 

    $clients = array();

    foreach($result AS $client){
      $clients[] = $client;
    }

    return $clients;
  }


  public function getClientCartZones($pricebook,$accountname)
  {

    if(empty($accountname)){
      $accountname = $this->getClientInfoByPriceBook($pricebook,'');
      $accountname = $accountname['accountname'];
    }

    if($pricebook == 10965){
      $accountid = 10965;
    }else{
      $accountid = $this->getClientInfoByPriceBook($pricebook,'');
      $accountid = $accountid['accountid'];         
    }

    if($accountid == $pricebook && $pricebook != 10965){
      $sql = "SELECT *,pricebookid AS accountid
              FROM vtiger_pricebook_zones             
              WHERE pricebookid = $pricebook";
    }else{
      $sql = "SELECT vtiger_accountscf.* 
              FROM vtiger_accountscf
              LEFT JOIN vtiger_account ON vtiger_account.accountid=vtiger_accountscf.accountid
              WHERE vtiger_accountscf.accountid IN ($accountid) AND accountname LIKE '$accountname%' ";
    }

    $result = $this->db->pquery($sql, array());
    return $result;
  }

  public function checkPriceBookHasZones($pricebookid){
    $sql = "SELECT * FROM vtiger_pricebook_zones WHERE pricebookid = ?";
    $result = $this->db->pquery($sql, array($pricebookid));
    return $this->db->num_rows($result);
  }

  public function getZoneTypes(){
    $sql = "SELECT cf_1216 as types FROM vtiger_cf_1216";
    $result = $this->db->pquery($sql, array());
    return $result;
  }

  public function copyPricebook($post)
  {
    $sql = "INSERT INTO vtiger_pricebook (pricebookid, pricebook_no, bookname, active, currency_id, zones) VALUES (?,?,?,?,?,?)"; 

    $sql2 = "INSERT INTO vtiger_products (productid,product_no,productname,productcategory,unit_price,discontinued,usageunit,qtyinstock,min_weight_kg,max_weight_kg,min_volume_m3,max_volume_m3,consignee,min_square_m2,max_square_m2,pll_pcs,pricebookid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; 

    $sql3 = "INSERT INTO vtiger_pricebookproductrel (pricebookid, productid,listprice,usedcurrency) VALUES (?,?,?,?)";

    $sql4 = "INSERT INTO vtiger_productcf  (productid, cf_954) VALUES (?,?)";

    $sql5 = "UPDATE vtiger_products SET kg_check = ?, m3_check = ?, m2_check = ? WHERE productid = ?";


    $pricebookName = $post['pricebook_name']."-Copy";
    $zones_num = $post['zone-count'];
    $accountid = $post['accountid'];
    $last_id = $this->last_entity_record();
    $pricebook_number = $this->getPriceBookNumber();
    $date = date("Y-m-d H:i:s");
    $this->db->pquery($sql, array($last_id,$pricebook_number,$pricebookName,1,1,$zones_num));
    $this->copyClientZones($accountid,$last_id);

    $insert_pricebook_entity = $this->insert_entity('PriceBooks', $last_id, 1, NULL,'CRM',  $pricebookName, $date);
   

    $row_array = Array();

    for($i = 1; $i <= $post['count_zones_value']; $i++)
    {  
      for($e = 1; $e <= $zones_num; $e++)
      { 
          $row_array['row-'.$e."-".$i] = array(
                                              'kg' => substr (number_format($post['max_weight'.$i], 3, '.', '') ,0,-1), 
                                              'kg_min' =>   $post['min_kg'.$i],
                                              'm3' => substr(number_format($post['max_volume'.$i], 3, '.', ''),0,-1), 
                                              'm3_min' =>   $post['min_m3'.$i],
                                              'm2' => substr(number_format($post['max_square'.$i], 3, '.', ''),0,-1), 
                                              'm2_min' =>   $post['min_m2'.$i],
                                              'price' => $post['product-'.$e."-".$i], 
                                              'stevedoring' =>  $post['stevedoring'.$i], 
                                              'pll' =>  $post['pll'.$i], 
                                              'productid' => $post['productid-'.$e."-".$i]
                                            );

      }
    }
       
    asort($row_array);

    for($i = 1; $i <= $post['count_zones_value']; $i++)
    {
      for($e = 1; $e <= $zones_num; $e++)
      {     
        
        $k = $i-1;
        $watch = true;
        if($k > 0){ 
          $watch = false;        
        }

      
          
          $min_weight_kg = $row_array['row-'.$e."-".$k]['kg']."1";
          $max_weight_kg = $row_array['row-'.$e."-".$i]['kg'];
          $min_kg = $row_array['row-'.$e."-".$i]['kg_min'];

        if($max_weight_kg > 0){

          if($min_weight_kg >=  $max_weight_kg){
            for($j = $post['count_zones_value']; $j >= 1; $j--){
              $min_weight_kg = $row_array['row-'.$e."-".$j]['kg']."1";        
              if($min_weight_kg <  $max_weight_kg){
                break;
              }
            }
          }
    
          if($watch){
            $min_weight_kg = '0.001';
          }

        }else{
          $min_weight_kg = '0.000';
          $max_weight_kg = '0.000';
        }


        if(isset($min_kg)){
          $min_weight_kg = '0.001';
          $min_kg = 1;
        }
        
         
        $min_volume_m3 = $row_array['row-'.$e."-".$k]['m3']."1";
        $max_volume_m3 = $row_array['row-'.$e."-".$i]['m3'];
        $min_m3 = $row_array['row-'.$e."-".$i]['m3_min'];

        if($max_volume_m3 > 0){

          if($min_volume_m3 >=  $max_volume_m3){
            for($j = $post['count_zones_value']; $j >= 1; $j--){
              $min_volume_m3 = $row_array['row-'.$e."-".$j]['m3']."1";           
              if($min_volume_m3 <  $max_volume_m3){
                break;
              }
            }
          }
          if($watch){
            $min_volume_m3 = '0.001';
          }        
        }else{
          $min_volume_m3 = '0.000';
          $max_volume_m3 = '0.000';
        }

        
        if(isset($min_m3)){
          $min_volume_m3 = '0.001';
          $min_m3 = 1;
        }
      
        $min_square_m2 = $row_array['row-'.$e."-".$k]['m2']."1";  
        $max_square_m2 = $row_array['row-'.$e."-".$i]['m2']; 
        $min_m2 = $row_array['row-'.$e."-".$i]['m2_min'];

        if($max_square_m2 > 0){  
          if($min_square_m2 >=  $max_square_m2){
            for($j = $post['count_zones_value']; $j >= 1; $j--){
              $min_square_m2 = $row_array['row-'.$e."-".$j]['kg']."1";
              if($min_square_m2 <  $max_square_m2){
                break;
              }
            }
          }
          if($watch){
            $min_square_m2 = '0.001';
          }   
        }else{
            $min_square_m2 = '0.000';
            $max_square_m2 = '0.000';
        }

        if(isset($min_m2)){
          $min_square_m2 = '0.001';
          $min_m2 = 1;
        }


        $stevedoring = $post['stevedoring'.$i];
        $pll = $post['pll'.$i];
        $price_value = $post['product-'.$e."-".$i];
       

          $last_entity_record = $this->last_entity_record();
          $date = date("Y-m-d H:i:s");
       
          $productname =  $pricebookName." Zona ".$e." eilutė ".$i;
          $cur_id = $this->getProNumber();
          $insert_entity = $this->insert_entity('Products', $last_entity_record, 1, NULL,'CRM',  $productname, $date);

          $this->db->pquery($sql2, array($last_entity_record,$cur_id, $productname,'Pervežimas',$price_value,1,0,999, $min_weight_kg,$max_weight_kg,$min_volume_m3,$max_volume_m3,'Zona '.$e,$min_square_m2,$max_square_m2,$pll,$last_id));
          $this->db->pquery($sql3, array($last_id, $last_entity_record, $price_value,1));                       
          $this->db->pquery($sql4, array($last_entity_record, $stevedoring));    
          $this->db->pquery($sql5, array($min_kg,$min_m3,$min_m2,$last_entity_record));  
         
        }  
    }    

    $referer = $_SERVER['PHP_SELF']."?module=Newpricebooks&view=PriceBook&record=$last_id";
    return header("Location: $referer");
  }

  public function copyClientZones($accountid,$pricebookid){
    $sql = "SELECT accountid, cf_1216, check_1218, cf_1218, cf_1220, check_1222, cf_1222, cf_1224, check_1226, cf_1226, cf_1228, check_1230, cf_1230, cf_1232, check_1234, cf_1234, cf_1236, check_1238, cf_1238, cf_1240, check_1242, cf_1242, cf_1244, check_1246, cf_1246, cf_1248, check_1250, cf_1250, cf_1252, check_1254, cf_1254, cf_1398, check_1400, cf_1400, cf_1402, check_1404, cf_1404, cf_1406, check_1408, cf_1408, cf_1410, check_1412, cf_1412, cf_1414, check_1416, cf_1416, cf_1418, check_1420, cf_1420, cf_1422, check_1424, cf_1424, cf_1426, check_1428, cf_1428, cf_1430, check_1432, cf_1432, cf_1434, check_1436, cf_1436 FROM vtiger_accountscf WHERE accountid = ?";

    $sql2 = "INSERT INTO vtiger_pricebook_zones (pricebookid, cf_1216, check_1218, cf_1218, cf_1220, check_1222, cf_1222, cf_1224, check_1226, cf_1226, cf_1228, check_1230, cf_1230, cf_1232, check_1234, cf_1234, cf_1236, check_1238, cf_1238, cf_1240, check_1242, cf_1242, cf_1244, check_1246, cf_1246, cf_1248, check_1250, cf_1250, cf_1252, check_1254, cf_1254, cf_1398, check_1400, cf_1400, cf_1402, check_1404, cf_1404, cf_1406, check_1408, cf_1408, cf_1410, check_1412, cf_1412, cf_1414, check_1416, cf_1416, cf_1418, check_1420, cf_1420, cf_1422, check_1424, cf_1424, cf_1426, check_1428, cf_1428, cf_1430, check_1432, cf_1432, cf_1434, check_1436, cf_1436) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    $sql3 = "SELECT pricebookid, cf_1216, check_1218, cf_1218, cf_1220, check_1222, cf_1222, cf_1224, check_1226, cf_1226, cf_1228, check_1230, cf_1230, cf_1232, check_1234, cf_1234, cf_1236, check_1238, cf_1238, cf_1240, check_1242, cf_1242, cf_1244, check_1246, cf_1246, cf_1248, check_1250, cf_1250, cf_1252, check_1254, cf_1254, cf_1398, check_1400, cf_1400, cf_1402, check_1404, cf_1404, cf_1406, check_1408, cf_1408, cf_1410, check_1412, cf_1412, cf_1414, check_1416, cf_1416, cf_1418, check_1420, cf_1420, cf_1422, check_1424, cf_1424, cf_1426, check_1428, cf_1428, cf_1430, check_1432, cf_1432, cf_1434, check_1436, cf_1436 FROM vtiger_pricebook_zones WHERE pricebookid = ?";

    $result = $this->db->pquery($sql, array($accountid));
    $num_rows = $this->db->num_rows($result);

    if(!$num_rows){
      $result = $this->db->pquery($sql3, array($accountid));
    }

    foreach($result AS $row){
      $this->db->pquery($sql2, array($pricebookid, $row['cf_1216'], $row['check_1218'], $row['cf_1218'], $row['cf_1220'], $row['check_1222'], $row['cf_1222'], $row['cf_1224'], $row['check_1226'], $row['cf_1226'], $row['cf_1228'], $row['check_1230'], $row['cf_1230'], $row['cf_1232'], $row['check_1234'], $row['cf_1234'], $row['cf_1236'], $row['check_1238'], $row['cf_1238'], $row['cf_1240'], $row['check_1242'], $row['cf_1242'], $row['cf_1244'], $row['check_1246'], $row['cf_1246'], $row['cf_1248'], $row['check_1250'], $row['cf_1250'], $row['cf_1252'], $row['check_1254'], $row['cf_1254'], $row['cf_1398'], $row['check_1400'], $row['cf_1400'], $row['cf_1402'], $row['check_1404'], $row['cf_1404'], $row['cf_1406'], $row['check_1408'], $row['cf_1408'], $row['cf_1410'], $row['check_1412'], $row['cf_1412'], $row['cf_1414'], $row['check_1416'], $row['cf_1416'], $row['cf_1418'], $row['check_1420'], $row['cf_1420'], $row['cf_1422'], $row['check_1424'], $row['cf_1424'], $row['cf_1426'], $row['check_1428'], $row['cf_1428'], $row['cf_1430'], $row['check_1432'], $row['cf_1432'], $row['cf_1434'], $row['check_1436'], $row['cf_1436']));
    }
  }


}
