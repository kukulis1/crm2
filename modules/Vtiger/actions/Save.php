<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_Save_Action extends Vtiger_Action_Controller {

	public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$record = $request->get('record');

		$actionName = ($record) ? 'EditView' : 'CreateView';
		if(!Users_Privileges_Model::isPermitted($moduleName, $actionName, $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if(!Users_Privileges_Model::isPermitted($moduleName, 'Save', $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if ($record) {
			$recordEntityName = getSalesEntityType($record);
			if ($recordEntityName !== $moduleName) {
				throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
			}
		}
	}
	
	public function validateRequest(Vtiger_Request $request) {
		return $request->validateWriteAccess();
	}

// itoma
	public function process(Vtiger_Request $request) {
		try {		
			$recordModel = $this->saveRecord($request);
			if ($request->get('returntab_label')){
				$loadUrl = 'index.php?'.$request->getReturnURL();
			} else if($request->get('relationOperation')) {
				$parentModuleName = $request->get('sourceModule');
				$parentRecordId = $request->get('sourceRecord');
				$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentRecordId, $parentModuleName);
				//TODO : Url should load the related list instead of detail view of record
				$loadUrl = $parentRecordModel->getDetailViewUrl();
			} else if ($request->get('returnToList')) {
				$loadUrl = $recordModel->getModule()->getListViewUrl();
			} else if ($request->get('returnmodule') && $request->get('returnview')) {
				$loadUrl = 'index.php?'.$request->getReturnURL();
			} else {
				$loadUrl = $recordModel->getDetailViewUrl();
			}
			//append App name to callback url
			//Special handling for vtiger7.
			$appName = $request->get('appName');
			if(strlen($appName) > 0){
				$loadUrl = $loadUrl.$appName;
			}
			header("Location: $loadUrl");
		} catch (DuplicateException $e) {
			$requestData = $request->getAll();
			$moduleName = $request->getModule();
			unset($requestData['action']);
			unset($requestData['__vtrftk']);

			if ($request->isAjax()) {
				$response = new Vtiger_Response();
				$response->setError($e->getMessage(), $e->getDuplicationMessage(), $e->getMessage());
				$response->emit();
			} else {
				$requestData['view'] = 'Edit';
				$requestData['duplicateRecords'] = $e->getDuplicateRecordIds();
				$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

				global $vtiger_current_version;
				$viewer = new Vtiger_Viewer();

				$viewer->assign('REQUEST_DATA', $requestData);
				$viewer->assign('REQUEST_URL', $moduleModel->getCreateRecordUrl().'&record='.$request->get('record'));
				$viewer->view('RedirectToEditView.tpl', 'Vtiger');
			}
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	/**
	 * Function to save record
	 * @param <Vtiger_Request> $request - values of the record
	 * @return <RecordModel> - record Model of saved record
	 */
	public function saveRecord($request) {

		$recordModel = $this->getRecordModelFromRequest($request);
		if($request->get('imgDeleted')) {
			$imageIds = $request->get('imageid');
			foreach($imageIds as $imageId) {
				$status = $recordModel->deleteImage($imageId);
			}
		}

		$recordModel->save();
		// itoma
		$mode = $recordModel->get('mode');		
		$module = $request->get('module');
		if($module == 'Accounts' AND $mode != 'edit'){	
			$this->editAddreses($recordModel);
			$this->exportAccountToMetrika($recordModel); 	
			$this->SaveAccountToVendors($recordModel); 			
		}elseif($module == 'Accounts' && $mode == 'edit'){
			$this->editAddreses($recordModel);
			$this->editAccountToMetrika($recordModel); 	
			$this->EditVendor($recordModel);
		}elseif($module == 'Vendors' AND $mode != 'edit'){
			$this->SaveVendorsToAccount($recordModel); 
		}elseif($module == 'Vendors'){
			$this->EditAccount($recordModel); 
			$this->UpdateIban($recordModel);
		}elseif($module == 'Stevedoring'){
			$this->addLoaders($recordModel); 
		}elseif($module == 'Users' && $mode == 'edit'){
			$this->updateEmployee($recordModel);	
		}elseif($module == 'Users'){
			$this->addEmployee($recordModel);
		}elseif($module == 'Claims' && $mode != 'edit'){
			$this->setClaimNumber($recordModel);
		}  
		
		
		$relatedRecordId = $recordModel->getId();
		if($request->get('relationOperation')) {
			$parentModuleName = $request->get('sourceModule');
			$parentModuleModel = Vtiger_Module_Model::getInstance($parentModuleName);
			$parentRecordId = $request->get('sourceRecord');
			$relatedModule = $recordModel->getModule();
			$relatedRecordId = $recordModel->getId();
			if($relatedModule->getName() == 'Events'){
				$relatedModule = Vtiger_Module_Model::getInstance('Calendar');
			}

			$relationModel = Vtiger_Relation_Model::getInstance($parentModuleModel, $relatedModule);
			$relationModel->addRelation($parentRecordId, $relatedRecordId);
		}
		$this->savedRecordId = $recordModel->getId();
		return $recordModel;
	
	}

	public function updateEmployee($recordModel){
		$db = PearDatabase::getInstance();
		$recordId = $recordModel->getId();

		$roles = array('CEO' => 'CEO',
										'Direktorius' => 'Director',
										'Vadybininkas' => 'Manager',
										'Administratorius' => 'Administrator',
										'Pardavimų direktorius' => 'Sales Director',
										'Pardavimų vadovas' => 'Sales head',
										'Pardavėjai' => 'Sellers',
										'Askaitos vadovas / vadove' => 'Accounting manager',
										'Vyr. apskaitininkė' => 'Chief accountant',
										'Apskaitininke' => 'Accountant',
										'Apskaitos asistente / Apskaita' => 'Accounting assistant',
										'Samdomas kainynų' => 'Hired at a price list',
										'Serviso vadovas' => 'Service manager',
										'Serviso darbuotojas' => 'Service worker',
										'Sandėlio vadovas' => 'Warehouse manager',
										'Sandėlio pamainos viršininkas' => 'Warehouse shift manager',
										'Sandėlio darbuotojas' => 'Warehouse worker',
										'Transporto vadovas' => 'Transport head',
										'Transporto vadybininkas' => 'Transport manager',
										'Transporto asistentas' => 'Transport assistant',
										'Vairuotojas' => 'Driver',
										'Biuro administratorė' => 'Office administrator',
										'Perkraustymo vadovas' => 'Moving head',
										'Perkraustymo vadyba' => 'Moving management');


		$sql = "SELECT vtiger_users.*,rolename FROM vtiger_users 
								LEFT JOIN vtiger_user2role ON vtiger_user2role.userid=vtiger_users.id
								LEFT JOIN vtiger_role ON vtiger_role.roleid=vtiger_user2role.roleid 
								WHERE id = ?";

		$sql2 = "UPDATE vtiger_employees SET employees_tks_name = ?, employees_tks_lastname = ?, employees_tks_email = ?, employees_tks_position = ?,  employees_tks_phone = ?, employees_tks_work = ? WHERE userid = ?";

		$result = $db->pquery($sql, array($recordId));

		foreach($result as $row){				
			$db->pquery($sql2 , array($row['first_name'], $row['last_name'], $row['email1'], $roles[$row['rolename']] , $row['phone_mobile'], $row['phone_work'], $row['id'] ));			
		}
	}


	public function addEmployee($recordModel){
		$db = PearDatabase::getInstance();
		$date_now = date("Y-m-d H:i:s");
		$recordId = $recordModel->getId();

		$roles = array('CEO' => 'CEO',
										'Direktorius' => 'Director',
										'Vadybininkas' => 'Manager',
										'Administratorius' => 'Administrator',
										'Pardavimų direktorius' => 'Sales Director',
										'Pardavimų vadovas' => 'Sales head',
										'Pardavėjai' => 'Sellers',
										'Askaitos vadovas / vadove' => 'Accounting manager',
										'Vyr. apskaitininkė' => 'Chief accountant',
										'Apskaitininke' => 'Accountant',
										'Apskaitos asistente / Apskaita' => 'Accounting assistant',
										'Samdomas kainynų' => 'Hired at a price list',
										'Serviso vadovas' => 'Service manager',
										'Serviso darbuotojas' => 'Service worker',
										'Sandėlio vadovas' => 'Warehouse manager',
										'Sandėlio pamainos viršininkas' => 'Warehouse shift manager',
										'Sandėlio darbuotojas' => 'Warehouse worker',
										'Transporto vadovas' => 'Transport head',
										'Transporto vadybininkas' => 'Transport manager',
										'Transporto asistentas' => 'Transport assistant',
										'Vairuotojas' => 'Driver',
										'Biuro administratorė' => 'Office administrator',
										'Perkraustymo vadovas' => 'Moving head',
										'Perkraustymo vadyba' => 'Moving management');

		$sql = "SELECT vtiger_users.*,rolename FROM vtiger_users 
							LEFT JOIN vtiger_user2role ON vtiger_user2role.userid=vtiger_users.id
							LEFT JOIN vtiger_role ON vtiger_role.roleid=vtiger_user2role.roleid
							WHERE vtiger_users.id = ?";

		$sql2 = "INSERT INTO vtiger_employees (employeesid, employees_tks_name, employees_tks_lastname, employees_tks_email, employees_tks_position,  employees_tks_phone, employees_tks_work, userid) 
		VALUES (?,?,?,?,?,?,?,?)";
		$sql3 = "INSERT INTO vtiger_employeescf (employeesid,cf_1739) VALUES (?,?)";

		$result = $db->pquery($sql, array($recordId));

		foreach($result as $row){		
			$entity_id = $this->last_entity_record($db);
			$this->insert_entity2($db,'Employees', $entity_id, 1, NULL, '', 'CRM', $date_now);  
			$db->pquery($sql2 , array($entity_id, $row['first_name'], $row['last_name'], $row['email1'], $roles[$row['rolename']] ,$row['phone_mobile'], $row['phone_work'],$row['id']));			
			$db->pquery($sql3, array($entity_id,1));
		}

	}


	public function editAddreses($recordModel){
		$adb = PearDatabase::getInstance();
		$accountid = $recordModel->getId();

		$get_address = $adb->pquery("SELECT * FROM vtiger_accountbillads WHERE accountaddressid = ?", array($accountid));
	
		$bill_city = $adb->query_result($get_address, 0, 'bill_city');
		$bill_street = $adb->query_result($get_address, 0, 'bill_street');
		$bill_code = $adb->query_result($get_address, 0, 'bill_code');	

		$adb->pquery("UPDATE vtiger_account SET municipality = ?, billing_address = ?, post_code = ? WHERE accountid = ?", array($bill_city,$bill_street,$bill_code,$accountid));
	}



	public function UpdateIban($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();
		$sql = "SELECT cf_1462 AS iban FROM vtiger_vendorcf WHERE vendorid = ?";
		$sql2 = "UPDATE vtiger_purchaseordercf SET cf_1610 = ? WHERE purchaseorderid = ?";
		$sql3 = "SELECT vtiger_purchaseorder.purchaseorderid FROM vtiger_purchaseorder 
								LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=vtiger_purchaseorder.purchaseorderid
								WHERE vtiger_purchaseordercf.cf_1610 = '' AND vtiger_purchaseorder.vendorid = ?";

		$result = $adb->pquery($sql, array($account_id));
		$result2 = $adb->pquery($sql3, array($account_id));
		$getIban = $adb->fetch_array($result);	

		if(!empty($getIban['iban'])){	
			while($row = $adb->fetch_array($result2)) {
				$adb->pquery($sql2, array($getIban['iban'], $row['purchaseorderid']));
			}
		}
	}

	public function exportAccountToMetrika($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();  
		$sql = "SELECT accountname,phone,contact,account_type,legal_entity_code,legal_vat_code,bill_street,bill_city,bill_code ,bill_country
								FROM vtiger_account
								LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=accountid
								WHERE accountid = ?";  
		$sql2 = "UPDATE vtiger_account SET account_no = ?, customer_id = ?, billing_address = ?, municipality = ?, post_code = ? WHERE accountid = ?";
		$sql3 = "UPDATE vtiger_accountscf SET cf_1576 = ? WHERE accountid = ?";

		$result = $adb->pquery($sql, array($account_id));

		$account = array();

		while($row = $adb->fetch_array($result)) {
			$account["customer_id"] = "";
			$account["company"] = $row["accountname"];
			$account["address"] = $row["bill_street"];
			$account["settlement"] = "";
			$account["municipality"] = $row["bill_city"];
			$account["region"] = "";
			$account["zipcode"] = $row["bill_code"];
			$account["country"] = $row["bill_country"];
			$account["person"] = mb_strimwidth($row["contact"], 0, 50, "");
			$account["phone"] = $row["phone"];
			$account["company_code"] = $row["legal_entity_code"];

	 }             

			$res = json_encode($account);  	


			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/customers.php?insert_new_customer=1");           
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'customers' => $res));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			$result = curl_exec($ch);
			curl_close($ch);	

			$data = json_decode($result, true);
			
			$state = $data[0];

                
			if($state != 'ERROR' && $state != 'ALREADY EXSIST') { 
				$data = json_decode($result, true);
				$customer_id = $data[0]['customer']['customer_id'];
				$customer_code = $data[0]['customer']['customer_code'];
				$address = $data[0]['customer']['address'];
				$municipality = $data[0]['customer']['municipality'];
				$zipcode = $data[0]['customer']['zipcode'];
				$adb->pquery($sql2, array($customer_code,$customer_id,$address,$municipality,$zipcode,$account_id));
				$adb->pquery($sql3, array($customer_id,$account_id));				
			}



	}


	public function exportAccountToMetrika2($account_id){
		$adb = PearDatabase::getInstance();
		$sql = "SELECT accountname,phone,contact,account_type,legal_entity_code,legal_vat_code,bill_street,bill_city,bill_code ,bill_country
								FROM vtiger_account
								LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=accountid
								WHERE accountid = ?";  
		$sql2 = "UPDATE vtiger_account SET account_no = ?, customer_id = ?, billing_address = ?, municipality = ?, post_code = ? WHERE accountid = ?";
		$sql3 = "UPDATE vtiger_accountscf SET cf_1576 = ? WHERE accountid = ?";

		$result = $adb->pquery($sql, array($account_id));

		$account = array();

		while($row = $adb->fetch_array($result)) {
			$account["customer_id"] = "";
			$account["company"] = $row["accountname"];
			$account["address"] = $row["bill_street"];
			$account["settlement"] = "";
			$account["municipality"] = $row["bill_city"];
			$account["region"] = "";
			$account["zipcode"] = $row["bill_code"];
			$account["country"] = $row["bill_country"];
			$account["person"] = mb_strimwidth($row["contact"], 0, 50, "");
			$account["phone"] = $row["phone"];
			$account["company_code"] = $row["legal_entity_code"];

	 }             

			$res = json_encode($account);  	


			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/customers.php?insert_new_customer=1");           
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'customers' => $res));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			$result = curl_exec($ch);
			curl_close($ch);	

			$data = json_decode($result, true);
			
			$state = $data[0];
                
			if($state != 'ERROR') { 
				$data = json_decode($result, true);
				$customer_id = $data[0]['customer']['customer_id'];
				$customer_code = $data[0]['customer']['customer_code'];
				$address = $data[0]['customer']['address'];
				$municipality = $data[0]['customer']['municipality'];
				$zipcode = $data[0]['customer']['zipcode'];
				$adb->pquery($sql2, array($customer_code,$customer_id,$address,$municipality,$zipcode,$account_id));
				$adb->pquery($sql3, array($customer_id,$account_id));
			}



	}

	public function addLoaders($recordModel){
		$adb = PearDatabase::getInstance();
		$record_id = $recordModel->getId();  
		$sql = "SELECT stevedoring_tks_namelastname, stevedoringno
								FROM vtiger_stevedoring							
								WHERE stevedoringid = ?";  

		$sql2 = "UPDATE vtiger_salesordercf SET cf_1687 = ? WHERE salesorderid = ?";		

		$result = $adb->pquery($sql, array($record_id));
		while($row = $adb->fetch_array($result)) {
			$adb->pquery($sql2, array($row['stevedoring_tks_namelastname'],$row['stevedoringno']));
		}
	}


	public function editAccountToMetrika($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();  
		$sql = "SELECT accountname,phone,contact,account_type,legal_entity_code,legal_vat_code,bill_street,bill_city,bill_code ,bill_country,customer_id,account_no
								FROM vtiger_account
								LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=accountid
								WHERE accountid = ?";  
		$sql2 = "UPDATE vtiger_account SET account_no = ?, customer_id = ?, billing_address = ?, municipality = ?, post_code = ? WHERE accountid = ?";
		$sql3 = "UPDATE vtiger_accountscf SET cf_1576 = ? WHERE accountid = ?";
		

		$result = $adb->pquery($sql, array($account_id));

		$customer_id = $adb->query_result($result, 0, 'customer_id');

		if(empty($customer_id)){
			$this->exportAccountToMetrika($recordModel);
		}else{		
			
			
			$account = array();
			while($row = $adb->fetch_array($result)) {
				$account["customer_id"] = $row["customer_id"];
				$account["company"] = $row["accountname"];
				$account["address"] = $row["bill_street"];
				$account["settlement"] = "";
				$account["municipality"] = $row["bill_city"];
				$account["region"] = "";
				$account["zipcode"] = $row["bill_code"];
				$account["country"] = $row["bill_country"];
				$account["person"] = mb_strimwidth($row["contact"], 0, 50, "");
				$account["phone"] = $row["phone"];
				$account["company_code"] = $row["legal_entity_code"];
				$insert = $adb->pquery($sql2, array($row["account_no"],$row["customer_id"],$row["bill_street"],$row["bill_city"],$row["bill_code"],$account_id));

			} 

			$res = json_encode($account); 



				if(is_numeric($account["customer_id"])){

						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/customers.php");           
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'customers' => $res));
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_TIMEOUT, 30);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
						$result = curl_exec($ch);
						curl_close($ch);	
						
						$data = json_decode($result, true);
						
						$state = $data[0];
						
						if($state['status'] == 'UPDATED') { 
							$data = json_decode($result, true);
							$customer_id = $data[0]['customer']['customer_id'];
							$customer_code = $data[0]['customer']['customer_code'];
							$address = $data[0]['customer']['address'];
							$municipality = $data[0]['customer']['municipality'];
							$zipcode = $data[0]['customer']['zipcode'];
							$insert = $adb->pquery($sql2, array($customer_code,$customer_id,$address,$municipality,$zipcode,$account_id));
							$insert2 = $adb->pquery($sql3, array($customer_id,$account_id));							
						}
			}

		}

	}

	public function SaveAccountToVendors($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();  
		$date = date("Y-m-d H:i:s");
		$sql = "SELECT accountname,phone,email1,account_type,legal_entity_code,legal_vat_code,billing_address,municipality,post_code FROM vtiger_account WHERE accountid = ?";  
		$sth5 = 'UPDATE vtiger_accountscf SET cf_2060 = ? WHERE accountid = ?'; 
		
		$result = $adb->pquery($sql, array($account_id));
		$accountname = $adb->query_result($result,0,'accountname');
		$phone = $adb->query_result($result,0,'phone');
		$email1 = $adb->query_result($result,0,'email1');
		$billing_address = $adb->query_result($result,0,'billing_address');
		$municipality = $adb->query_result($result,0,'municipality');
		$post_code = $adb->query_result($result,0,'post_code');

		$agnum_code = $this->clean($accountname);
		$adb->pquery($sth5,array($agnum_code,$account_id)); 


		$ven =  $this->last_ven_id($adb);
		$last_entity_record = $this->last_entity_id($adb);
		$insert_entity = $this->insert_entity($adb, 'Vendors', $last_entity_record, 1, NULL,  $accountname, $date);    

		$sql2 = "INSERT INTO vtiger_vendor (vendorid,vendor_no,vendorname,phone,email,street,city,postalcode,accountid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$result2 = $adb->pquery($sql2, array($last_entity_record,$ven,$accountname, $phone, $email1, $billing_address,$municipality,$post_code,$account_id));    


		$legal_entity_code = $adb->query_result($result,0,'legal_entity_code');
		$legal_vat_code = $adb->query_result($result,0,'legal_vat_code');
		$account_type = $adb->query_result($result,0,'account_type');

		$sql3 = "INSERT INTO vtiger_vendorcf (vendorid,cf_1458, cf_1460, cf_1466) VALUES (?,?,?,?)";
		$result = $adb->pquery($sql3, array($last_entity_record,$legal_entity_code,$legal_vat_code,$account_type));
	}

	public function clean($string) {          
	    $string = str_replace(array('"','UAB',',',' '), array('','','',''), $string); // Removes special chars.     
	    return $string; 
	}

	public function SaveVendorsToAccount($recordModel){
		$adb = PearDatabase::getInstance();
		$vendor_id = $recordModel->getId();  
		$date = date("Y-m-d H:i:s");
		$sql = "SELECT vendorname,phone,email,street,city,postalcode FROM vtiger_vendor WHERE vendorid = ?";  
		$sql2 = "SELECT cf_1458 as legal_entity_code,cf_1460 as legal_vat_code,cf_1466 as `type` FROM vtiger_vendorcf WHERE vendorid = ?";  
		$sth5 = 'INSERT INTO vtiger_accountscf (accountid,cf_2060) VALUES (?,?)'; 


		$result = $adb->pquery($sql, array($vendor_id));
		$result2 = $adb->pquery($sql2, array($vendor_id));		

		$vendorname = $adb->query_result($result,0,'vendorname');		
		$type = $adb->query_result($result2,0,'type');
		$legal_entity_code = $adb->query_result($result2,0,'legal_entity_code');
		$legal_vat_code = $adb->query_result($result2,0,'legal_vat_code');
		$phone = $adb->query_result($result,0,'phone');
		$email = $adb->query_result($result,0,'email');
		$street = $adb->query_result($result,0,'street');
		$city = $adb->query_result($result,0,'city');
		$postalcode = $adb->query_result($result,0,'postalcode');

		$agnum_code = $this->clean($vendorname);
		
		$last_entity_record = $this->last_entity_id($adb);
		$insert_entity = $this->insert_entity($adb, 'Accounts', $last_entity_record, 1, NULL,  $vendorname, $date);    

		$sql4 = "INSERT INTO vtiger_account (accountid,accountname,account_type,phone,email1,legal_entity_code,legal_vat_code,billing_address,municipality,post_code) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$adb->pquery($sql4, array($last_entity_record,$vendorname,$type, $phone, $email,$legal_entity_code,$legal_vat_code, $street,$city,$postalcode));		
		

		$sql3 = "UPDATE vtiger_vendor SET accountid = ? WHERE vendorid = ?";
		$result3 = $adb->pquery($sql3, array($last_entity_record,	$vendor_id));
		$adb->pquery($sth5,array($last_entity_record,$agnum_code)); 
		
		$this->exportAccountToMetrika2($last_entity_record);

	}

	public function EditVendor($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();  
	
		$sql = "SELECT accountname,phone,email1,billing_address,municipality,post_code FROM vtiger_account WHERE accountid = ?";  
		$sql2 = "UPDATE vtiger_vendor SET vendorname = ?, phone = ?, email = ?, street = ?, city = ?, postalcode = ? WHERE accountid = ?";
		
		$result = $adb->pquery($sql, array($account_id));

    while($row = $adb->fetch_array($result)){
			$result = $adb->pquery($sql2,array($row['accountname'], $row['phone'], $row['email1'], $row['billing_address'],$row['municipality'],$row['post_code'],$account_id));
			$this->updateEntity($adb,$account_id);
		}		
	}

	public function EditAccount($recordModel){
		$adb = PearDatabase::getInstance();
		$vendor_id = $recordModel->getId();  
	
		$sql = "SELECT vendorname,phone,email,street,city,postalcode, accountid FROM vtiger_vendor WHERE vendorid = ?";  
		$sql2 = "UPDATE vtiger_account SET accountname = ?, phone = ?, email1 = ?, billing_address = ?, municipality = ?, post_code = ? WHERE accountid = ?";
		
		$result = $adb->pquery($sql, array($vendor_id));

    while($row = $adb->fetch_array($result)){
			$result = $adb->pquery($sql2,array($row['vendorname'], $row['phone'], $row['email'], $row['street'],$row['city'],$row['postalcode'],$row['accountid']));
			$this->updateEntity($adb,$vendor_id);
		}		
	}

	public function updateEntity($adb,$account_id){
		$date = date("Y-m-d H:i:s");

		$sql = "SELECT vendorname,vendorid FROM vtiger_vendor WHERE accountid = ?";
		$result = $adb->pquery($sql, array($account_id));

		$sql2 = "UPDATE vtiger_crmentity SET modifiedtime = ?, label = ? WHERE crmid = ?";	

		while($row = $adb->fetch_array($result)){	
			$result = $adb->pquery($sql2, array($date,$row['vendorname'],$row['vendorid']));			
		}

	}

	public function last_ven_id($adb){
	$sql = "SELECT vendor_no FROM vtiger_vendor ORDER BY vendorid desc limit 1";
	$result = $adb->pquery($sql, array()); 

	while($row = $adb->fetch_array($result)){
		$num = substr($row['vendor_no'], 3);
	}

	$num = $num +1;
	$vel = 'VEN'.$num;
	return $vel;
	}	


	function last_entity_id($adb){
			$sql = "SELECT * FROM vtiger_crmentity_seq";   
			$result = $adb->pquery($sql, array());
			$crmid = $adb->query_result($result,0,'id');

			$crmid = $crmid+1;			

			// Update sequence values
			$sql2 = "UPDATE vtiger_crmentity_seq SET id = ?";
			$result = $adb->pquery($sql2,array($crmid));

			return $crmid;
	}

	function insert_entity($adb, $setype, $entity_id, $user_id, $description, $label, $date) {
			$date = date("Y-m-d H:i:s");  
			// Insert into vtiger_crmentity 
			$sql = "INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, createdtime, modifiedtime,source) 
			VALUES (?,?,?,?,?,?,?,?,?)";
			$result = $adb->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date, 'CRM'));	
	}

	public function insert_entity2($db,$setype, $entity_id, $user_id, $description, $label, $source, $date)
	{
		$sql = "INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) VALUES (?,?,?,?,?,?,?,?,?)";
		$db->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description, $source, $label, $date, $date));
	}

	public function last_entity_record($db)
	{
		$result = $db->pquery("SELECT id FROM vtiger_crmentity_seq");
		$productid = 	$db->query_result($result,0,'id');
		$crmid = $productid + 1; 		
		$db->pquery("UPDATE vtiger_crmentity_seq SET id = ?", array($crmid));
		return $crmid;
	}

	public function setClaimNumber($recordModel){
		$adb = PearDatabase::getInstance();
		$recordId = $recordModel->getId();
		$date = date('Y-m-d');

		$prefix = 'CLAIM-';		

		$query = "SELECT claimsno FROM vtiger_claims
															LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_claims.claimsid 							 
															WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$date'  AND vtiger_crmentity.deleted = 0  AND claimsno != ''
															ORDER BY vtiger_claims.claimsid  
															DESC LIMIT 1";
				
		$check_cur = $adb->pquery($query, array());
	
		$curid = $adb->query_result($check_cur, 0, 'claimsno');

		if(!empty($curid)){
			$curid = substr($curid, -3);
		}else{
			$req_no = '001';
		}	

		$todayDate = date('ymd');

		if(!empty($curid)){
			$strip = strlen($curid) - strlen($curid + 1);
			if ($strip < 0)
				$strip = 0;
			$temp = str_repeat("0", $strip);
			$req_no.= $temp . ($curid + 1);
		}
	
		$prev_inv_no = $prefix.$todayDate."-".$req_no;

	 $result = $adb->pquery("SELECT smcreatorid,createdtime FROM vtiger_crmentity WHERE crmid = ?", array($recordId));
	 $smcreatorid = $adb->query_result($result, 0, 'smcreatorid');
	 $createdtime = $adb->query_result($result, 0, 'createdtime');

	 $adb->pquery("UPDATE vtiger_claims SET claimsno = ? WHERE claimsid=?", array($prev_inv_no, $recordId));
	 $adb->pquery("UPDATE vtiger_crmentity SET label = ? WHERE crmid = ?", array($prev_inv_no, $recordId));
	 $adb->pquery("INSERT INTO  vtiger_claims_seen (claim_id, seen, seen_time, reviewed_person) VALUES (?,?,?,?)", array($recordId,1,$createdtime,$smcreatorid));

	}


	/**
	 * Function to get the record model based on the request parameters
	 * @param Vtiger_Request $request
	 * @return Vtiger_Record_Model or Module specific Record Model instance
	 */
	protected function getRecordModelFromRequest(Vtiger_Request $request) {

		$moduleName = $request->getModule();
		$recordId = $request->get('record');

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		if(!empty($recordId)) {
			$recordModel = Vtiger_Record_Model::getInstanceById($recordId, $moduleName);
			$recordModel->set('id', $recordId);
			$recordModel->set('mode', 'edit');
		} else {
			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
			$recordModel->set('mode', '');
		}

		$fieldModelList = $moduleModel->getFields();
		foreach ($fieldModelList as $fieldName => $fieldModel) {
			$fieldValue = $request->get($fieldName, null);
			$fieldDataType = $fieldModel->getFieldDataType();
			if($fieldDataType == 'time'){
				$fieldValue = Vtiger_Time_UIType::getTimeValueWithSeconds($fieldValue);
			}
			if($fieldValue !== null) {
				if(!is_array($fieldValue) && $fieldDataType != 'currency') {
					$fieldValue = trim($fieldValue);
				}
				$recordModel->set($fieldName, $fieldValue);
			}
		}
		return $recordModel;
	}
}
