<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_RelationAjax_Action extends Vtiger_Action_Controller {
	function __construct() {
		parent::__construct();
		$this->exposeMethod('addRelation');
		$this->exposeMethod('deleteRelation');
		$this->exposeMethod('getRelatedListPageCount');
		$this->exposeMethod('getRelatedRecordInfo');
	}

	function checkPermission(Vtiger_Request $request) { }

	function preProcess(Vtiger_Request $request) {
		return true;
	}

	function postProcess(Vtiger_Request $request) {
		return true;
	}

	function process(Vtiger_Request $request) {
		$mode = $request->get('mode');
		if(!empty($mode)) {
			$this->invokeExposedMethod($mode, $request);
			return;
		}
	}

	/*
	 * Function to add relation for specified source record id and related record id list
	 * @param <array> $request
	 *		keys					Content
	 *		src_module				source module name
	 *		src_record				source record id
	 *		related_module			related module name
	 *		related_record_list		json encoded of list of related record ids
	 */
	function addRelation($request) {
		$sourceModule = $request->getModule();
		$sourceRecordId = $request->get('src_record');

		$relatedModule = $request->get('related_module');
		$relatedRecordIdList = $request->get('related_record_list');

		$sourceModuleModel = Vtiger_Module_Model::getInstance($sourceModule);
		$relatedModuleModel = Vtiger_Module_Model::getInstance($relatedModule);
		$relationModel = Vtiger_Relation_Model::getInstance($sourceModuleModel, $relatedModuleModel);
		foreach($relatedRecordIdList as $relatedRecordId) {
			$relationModel->addRelation($sourceRecordId,$relatedRecordId);
		}

		$response = new Vtiger_Response();
		$response->setResult(true);
		$response->emit();
	}

	/**
	 * Function to delete the relation for specified source record id and related record id list
	 * @param <array> $request
	 *		keys					Content
	 *		src_module				source module name
	 *		src_record				source record id
	 *		related_module			related module name
	 *		related_record_list		json encoded of list of related record ids
	 */
	function deleteRelation($request) {
		$sourceModule = $request->getModule();
		$sourceRecordId = $request->get('src_record');

		$relatedModule = $request->get('related_module');
		$relatedRecordIdList = $request->get('related_record_list');
		$recurringEditMode = $request->get('recurringEditMode');
		$relatedRecordList = array();

		if($relatedModule == 'Debts'){
			$this->deleteInvoiceDebt($relatedRecordIdList);
		}

		if($relatedModule == 'Calendar' && !empty($recurringEditMode) && $recurringEditMode != 'current') {
			foreach($relatedRecordIdList as $relatedRecordId) {
				$recordModel = Vtiger_Record_Model::getCleanInstance($relatedModule);
				$recordModel->set('id', $relatedRecordId);
				$recurringRecordsList = $recordModel->getRecurringRecordsList();
				foreach($recurringRecordsList as $parent => $childs) {
					$parentRecurringId = $parent;
					$childRecords = $childs;
				}
				if($recurringEditMode == 'future') {
					$parentKey = array_keys($childRecords, $relatedRecordId);
					$childRecords = array_slice($childRecords, $parentKey[0]);
				}
				foreach($childRecords as $recordId) {
					$relatedRecordList[] = $recordId;
				}
				$relatedRecordIdList = array_slice($relatedRecordIdList, $relatedRecordId);
			}
		}

		foreach($relatedRecordList as $record) {
			$relatedRecordIdList[] = $record;
		}

		//Setting related module as current module to delete the relation
		vglobal('currentModule', $relatedModule);

		$sourceModuleModel = Vtiger_Module_Model::getInstance($sourceModule);
		$relatedModuleModel = Vtiger_Module_Model::getInstance($relatedModule);
		$relationModel = Vtiger_Relation_Model::getInstance($sourceModuleModel, $relatedModuleModel);
		foreach($relatedRecordIdList as $relatedRecordId) {
			$response = $relationModel->deleteRelation($sourceRecordId,$relatedRecordId);
		}

		$response = new Vtiger_Response();
		$response->setResult(true);
		$response->emit();
	}

	public function deleteInvoiceDebt($recordId){
		$adb = PearDatabase::getInstance();	

		$debt = $adb->pquery("SELECT vtiger_crmentityrel.crmid as invoiceid, ROUND(debts_tks_suma,2) as debt, cf_1277 AS invoice_type,invoice_no,cf_2667 AS invoice_to_no 
                                              FROM vtiger_crmentityrel 
                                              LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
																							LEFT JOIN vtiger_debtscf ON vtiger_debtscf.debtsid = vtiger_debts.debtsid
                                              LEFT JOIN vtiger_crmentity e ON e.crmid=vtiger_crmentityrel.crmid
                                              LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_crmentityrel.crmid 
                                              LEFT JOIN vtiger_invoice ON vtiger_invoice.invoiceid=vtiger_crmentityrel.crmid
                                              WHERE relcrmid = ? AND e.deleted=0",array($recordId));

			$invoiceid = $adb->query_result($debt,0,'invoiceid');
			$invoice_type = $adb->query_result($debt,0,'invoice_type');
			$debit_invoice_no = $adb->query_result($debt,0,'invoice_no');
			$covered_sum = $adb->query_result($debt,0,'debt');
			$invoice_to_no = $adb->query_result($debt,0,'invoice_to_no');

			if($invoice_type != 'Kreditinė'){
				$get_credit_invoice = $adb->pquery("SELECT invoiceid FROM vtiger_invoice WHERE invoice_no = ?", array($invoice_to_no));
				$credit_invoiceid = $adb->query_result($get_credit_invoice,0,'invoiceid');
				
				$get_payment_id = $adb->pquery("SELECT relcrmid
																													FROM vtiger_crmentityrel
																													INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=relcrmid
																													JOIN vtiger_debts ON vtiger_debts.debtsid=relcrmid
																													JOIN vtiger_debtscf ON vtiger_debtscf.debtsid=vtiger_debts.debtsid
																													WHERE vtiger_crmentityrel.crmid = ? AND relmodule = 'Debts' AND deleted = 0 AND cf_2667 = ? AND ROUND(ABS(debts_tks_suma),2) = ?",array($credit_invoiceid,$debit_invoice_no,$covered_sum));																								
																													
				$relcrmid = $adb->query_result($get_payment_id,0,'relcrmid');																								
			
				if(!empty($credit_invoiceid)){
					$adb->pquery("DELETE FROM vtiger_crmentity WHERE crmid = ?", array($relcrmid));
					$adb->pquery("DELETE FROM vtiger_debts WHERE debtsid = ?", array($relcrmid));
					$adb->pquery("DELETE FROM vtiger_crmentityrel WHERE relcrmid = ?", array($relcrmid));

					// Atnaujinti, ne trinti
					$adb->pquery("UPDATE vtiger_invoice_cover_credit SET `cover` = (`cover` + $covered_sum) WHERE invoiceid = ?", array($credit_invoiceid));
					// $adb->pquery("DELETE FROM vtiger_invoice_cover_credit WHERE invoiceid_from = ?", array($invoiceid));
				}
			}else if($invoice_type == 'Kreditinė'){
				$adb->pquery("DELETE FROM vtiger_invoice_cover_credit WHERE invoiceid = ?",array($invoiceid));
			}

		  $adb->pquery("DELETE FROM vtiger_crmentity WHERE crmid = ?",array($recordId));
		  $adb->pquery("DELETE FROM vtiger_debts WHERE debtsid = ?",array($recordId));
	}

	/**
	 * Function to get the page count for reltedlist
	 * @return total number of pages
	 */
	function getRelatedListPageCount(Vtiger_Request $request){
		$moduleName = $request->getModule();
		$relatedModuleName = $request->get('relatedModule');
		$parentId = $request->get('record');
		$label = $request->get('tab_label');
		$pagingModel = new Vtiger_Paging_Model();
		$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentId, $moduleName);
		$relationListView = Vtiger_RelationListView_Model::getInstance($parentRecordModel, $relatedModuleName, $label);
		$totalCount = $relationListView->getRelatedEntriesCount();
		$pageLimit = $pagingModel->getPageLimit();
		$pageCount = ceil((int) $totalCount / (int) $pageLimit);

		if($pageCount == 0){
			$pageCount = 1;
		}
		$result = array();
		$result['numberOfRecords'] = $totalCount;
		$result['page'] = $pageCount;
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}

	public function validateRequest(Vtiger_Request $request) {
		$request->validateWriteAccess();
	}

	function getRelatedRecordInfo($request) {
		try {
			return $this->getParentRecordInfo($request);
		} catch (Exception $e) {
			$response = new Vtiger_Response();
			$response->setError($e->getCode(), $e->getMessage());
			$response->emit();
		}
	}

	function getParentRecordInfo($request) {
		$moduleName = $request->get('module');
		$recordModel = Vtiger_Record_Model::getInstanceById($request->get('id'), $moduleName);
		$moduleModel = $recordModel->getModule();
		$autoFillData = $moduleModel->getAutoFillModuleAndField($moduleName);

		if($autoFillData) {
			foreach($autoFillData as $data) {
				$autoFillModule = $data['module'];
				$autoFillFieldName = $data['fieldname'];
				$autofillRecordId = $recordModel->get($autoFillFieldName);

				$autoFillNameArray = getEntityName($autoFillModule, $autofillRecordId);
				$autoFillName = $autoFillNameArray[$autofillRecordId];

				$resultData[] = array(	'id'		=> $request->get('id'), 
										'name'		=> decode_html($recordModel->getName()),
										'parent_id'	=> array(	'id' => $autofillRecordId,
																'name' => decode_html($autoFillName),
																'module' => $autoFillModule));
			}

			$result[$request->get('id')] = $resultData;

		} else {
			$resultData = array('id'	=> $request->get('id'), 
								'name'	=> decode_html($recordModel->getName()),
								'info'	=> $recordModel->getRawData());
			$result[$request->get('id')] = $resultData;
		}

		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}

}
