<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_SaveAjax_Action extends Vtiger_Save_Action {

	public function process(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$fieldToBeSaved = $request->get('field');
		$response = new Vtiger_Response();
		try {
			vglobal('VTIGER_TIMESTAMP_NO_CHANGE_MODE', $request->get('_timeStampNoChangeMode',false));
			$recordModel = $this->saveRecord($request);
			vglobal('VTIGER_TIMESTAMP_NO_CHANGE_MODE', false);

			$fieldModelList = $recordModel->getModule()->getFields();
			$result = array();
			$picklistColorMap = array();
			foreach ($fieldModelList as $fieldName => $fieldModel) {
				$recordFieldValue = $recordModel->get($fieldName);
				if(is_array($recordFieldValue) && $fieldModel->getFieldDataType() == 'multipicklist') {
					foreach ($recordFieldValue as $picklistValue) {
						$picklistColorMap[$picklistValue] = Settings_Picklist_Module_Model::getPicklistColorByValue($fieldName, $picklistValue);
					}
					$recordFieldValue = implode(' |##| ', $recordFieldValue);     
				}
				if($fieldModel->getFieldDataType() == 'picklist') {
					$picklistColorMap[$recordFieldValue] = Settings_Picklist_Module_Model::getPicklistColorByValue($fieldName, $recordFieldValue);
				}
				$fieldValue = $displayValue = Vtiger_Util_Helper::toSafeHTML($recordFieldValue);
				if ($fieldModel->getFieldDataType() !== 'currency' && $fieldModel->getFieldDataType() !== 'datetime' && $fieldModel->getFieldDataType() !== 'date' && $fieldModel->getFieldDataType() !== 'double') { 
					$displayValue = $fieldModel->getDisplayValue($fieldValue, $recordModel->getId()); 
				}
				if ($fieldModel->getFieldDataType() == 'currency') {
					$displayValue = Vtiger_Currency_UIType::transformDisplayValue($fieldValue);
				}
				if(!empty($picklistColorMap)) {
					$result[$fieldName] = array('value' => $fieldValue, 'display_value' => $displayValue, 'colormap' => $picklistColorMap);
				} else {
					$result[$fieldName] = array('value' => $fieldValue, 'display_value' => $displayValue);
				}
			}

			$mode = $recordModel->get('mode');
			if($moduleName == 'Accounts' &&  $mode != 'edit'){
				// $this->exportAccountToMetrika($recordModel); 	
			}elseif($moduleName == 'Accounts' &&  $mode == 'edit'){
				// $this->editAccountToMetrika($recordModel); 	
			}elseif($moduleName == 'Vendors' &&  $mode == 'edit'){
				// $this->UpdateIban($recordModel);
			}
			
			if($moduleName == 'Debts' &&  $mode != 'edit'){
				if($request->get('debts_tks_tipas') == 'Kreditinė'){
					if(!empty($request->get('cf_2667'))){
						$this->coverInvoice($request);
					}					
				}
			}

			if($moduleName == 'PurchaseOrder'){
				$this->updateVise($recordModel);
			}

			if($moduleName == 'Stevedoring'){
				$this->addLoaders($recordModel); 
			}elseif($moduleName == 'SalesOrder'){
				$this->addLoadersToStevedoring($recordModel); 
			}
			
			if($moduleName == 'Employeeinventory'){	
				$parentRecordId = $request->get('sourceRecord');		
				$this->insertEmployeeId($recordModel,$parentRecordId,'vtiger_employeeinventory','employeeinventoryid');
			}else	if($moduleName == 'Vacation'){	
				$parentRecordId = $request->get('sourceRecord');		
				$this->insertEmployeeId($recordModel,$parentRecordId,'vtiger_vacation','vacationid');
			}else	if($moduleName == 'Secondment'){	
				$parentRecordId = $request->get('sourceRecord');		
				$this->insertEmployeeId($recordModel,$parentRecordId,'vtiger_secondment','secondmentid');
			}  

			
			//Handling salutation type
			if ($request->get('field') === 'firstname' && in_array($request->getModule(), array('Contacts', 'Leads'))) {
				$salutationType = $recordModel->getDisplayValue('salutationtype');
				$firstNameDetails = $result['firstname'];
				$firstNameDetails['display_value'] = $salutationType. " " .$firstNameDetails['display_value'];
				if ($salutationType != '--None--') $result['firstname'] = $firstNameDetails;
			}

			// removed decode_html to eliminate XSS vulnerability
			$result['_recordLabel'] = decode_html($recordModel->getName());
			$result['_recordId'] = $recordModel->getId();
			$response->setEmitType(Vtiger_Response::$EMIT_JSON);
			$response->setResult($result);
		} catch (DuplicateException $e) {
			$response->setError($e->getMessage(), $e->getDuplicationMessage(), $e->getMessage());
		} catch (Exception $e) {
			$response->setError($e->getMessage());
		}
		$response->emit();
	}

	public function coverInvoice($request){
		$db = PearDatabase::getInstance();
		$record_id = $request->get('returnrecord');
		$invoice_no = $request->get('cf_2667');
		$payment_date = $request->get('debts_tks_data');
		$userid = $request->get('assigned_user_id');
		$amount = $request->get('debts_tks_suma')*-1;
		$date = date("Y-m-d H:i:s");

		$get_invoice = $db->pquery("SELECT invoiceid, total FROM vtiger_invoice WHERE invoice_no = ?",array($invoice_no));
		$invoiceid = $db->query_result($get_invoice,0,'invoiceid');
		$total = $db->query_result($get_invoice,0,'total');
		
		
		$check_record = $db->pquery("SELECT ABS(total) AS total, cover, ABS(cover) AS coverplus FROM vtiger_invoice_cover_credit WHERE invoiceid = ?",array($invoiceid));
		
		$num_rows = $db->num_rows($check_record);


		$get_from_invoice = $db->pquery("SELECT invoice_no FROM vtiger_invoice WHERE invoiceid = ?",array($record_id));
		$from_invoice_no = $db->query_result($get_from_invoice,0,'invoice_no');
		
		$entity_id = last_entity_record($db);

		$db->pquery("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) 
		VALUES (?,?,?,?,?,?,?,?,?)",array($entity_id, $userid, $userid, 'Debts', null, 'CRM', $invoice_no, $date, $date));

		$db->pquery("INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) 
		VALUES ($invoiceid, 'Invoice', $entity_id, 'Debts')");

		$db->pquery("INSERT INTO vtiger_debts (debtsid, debts_tks_data, debts_tks_suma, debts_tks_tipas) 
		VALUES ($entity_id, '$payment_date', $amount, 'Kreditinė')");

		$db->pquery("INSERT INTO vtiger_debtscf (debtsid,cf_2667) VALUES (?,?)",array($entity_id,$from_invoice_no));

		if($num_rows){
			$total_from_cover = $db->query_result($check_record,0,'total');
			$cover = $db->query_result($check_record,0,'cover');
			$cover_plus = $db->query_result($check_record,0,'coverplus');

			if($total_from_cover > $cover_plus){
				$new_cover = $cover + $amount;
				if(($new_cover * -1) > $total_from_cover){
					$new_cover = $total;
				}
				$db->pquery("UPDATE vtiger_invoice_cover_credit SET cover = ?, total = ? WHERE invoiceid = ?",array($new_cover,$total,$invoiceid));
			}

		}else{
			$db->pquery("INSERT INTO vtiger_invoice_cover_credit (invoiceid,invoiceid_from,total,cover) VALUES (?,?,?,?)",array($invoiceid,$record_id,$total,$amount));
		}
	}


	public function insertEmployeeId($recordModel,$parentRecordId, $table, $column){
		$adb = PearDatabase::getInstance();
		$record_id = $recordModel->getId();  
		$sql = "UPDATE $table SET cf_20023 = ? WHERE $column = ?";
		$adb->pquery($sql, array($parentRecordId,$record_id));
	}


	public function addLoaders($recordModel){
		$adb = PearDatabase::getInstance();
		$record_id = $recordModel->getId();  
		$sql = "SELECT stevedoring_tks_namelastname, stevedoringno
								FROM vtiger_stevedoring							
								WHERE stevedoringid = ?";  

		$sql2 = "UPDATE vtiger_salesordercf SET cf_1687 = ? WHERE salesorderid = ?";		

		$result = $adb->pquery($sql, array($record_id));
		while($row = $adb->fetch_array($result)) {
			$adb->pquery($sql2, array($row['stevedoring_tks_namelastname'],$row['stevedoringno']));
		}
	}

	public function addLoadersToStevedoring($recordModel){
    $adb = PearDatabase::getInstance();
    $record_id = $recordModel->getId();  
    $sql = "SELECT cf_1687,salesorderid
                            FROM vtiger_salesordercf							
                            WHERE salesorderid = ?";  

    $sql2 = "UPDATE vtiger_stevedoring SET stevedoring_tks_namelastname = ? WHERE stevedoringno = ?";		

    $result = $adb->pquery($sql, array($record_id));
    while($row = $adb->fetch_array($result)) {
        $adb->pquery($sql2, array($row['cf_1687'],$row['salesorderid']));
    }
}



	public function updateVise($recordModel){
		$adb = PearDatabase::getInstance();
		global $current_user;

		$purchaseorderid = $recordModel->getId();
		$sql = "SELECT cf_1673 FROM vtiger_purchaseordercf WHERE purchaseorderid = ?";
		$result = $adb->pquery($sql, array($purchaseorderid));
		$vise = $adb->query_result($result, 0, 'cf_1673');	

		if($vise){
			$this->insertVise($adb, $purchaseorderid,$current_user->id);
		}else{
			removeVise($adb, $purchaseorderid);
		}
	}

	public function insertVise($adb, $purchaseorderid,$user){
		$sql = "SELECT crmid FROM vtiger_crmentityrel WHERE crmid  = ? AND relmodule = 'Purchasevise'";
		$checkRelEntity = $adb->pquery($sql, array($purchaseorderid));
		$num_rows = $adb->num_rows($checkRelEntity);
	
		if(!$num_rows){
			$entity_id = last_entity_record($adb);
			$date = date("Y-m-d H:i:s");
			insert_entity2($adb,'Purchasevise', $entity_id, $user, NULL, '', 'CRM', $date);
			insert_entityrel($adb, $purchaseorderid, 'PurchaseOrder',  $entity_id, 'Purchasevise');
			createVise($adb,$entity_id,1);
			$sql2 = "INSERT INTO vtiger_purchasevisecf (purchaseviseid) VALUES (?)";
			$adb->pquery($sql2, array($entity_id));
	
		}	
	}

	function removeVise($adb, $purchaseorderid)
	{
		$sql = "SELECT relcrmid FROM vtiger_crmentityrel WHERE crmid  = ? AND relmodule = ?";
		$result = $adb->pquery($sql, array($purchaseorderid, 'Purchasevise'));
		$relcrmid = $adb->query_result($result, 0, 'relcrmid');	

		if($relcrmid){	
			$sql = "DELETE FROM vtiger_crmentity WHERE crmid = ? AND setype = ?";
			$adb->pquery($sql, array($relcrmid,'Purchasevise'));

			$sql2 = "DELETE FROM vtiger_purchasevise WHERE purchaseviseid = ?";
			$adb->pquery($sql2, array($relcrmid));

			$sql3 = "DELETE FROM vtiger_purchasevisecf WHERE purchaseviseid = ?";
			$adb->pquery($sql3, array($relcrmid));

			$sql4 = "DELETE FROM vtiger_crmentityrel WHERE relcrmid = ?";
			$adb->pquery($sql4, array($relcrmid));
		}
	}

	function last_entity_record($adb)
	{
		$result = $adb->pquery('SELECT id FROM vtiger_crmentity_seq');
		$crmid = $adb->query_result($result, 0, 'id');	
		$crmid = $crmid + 1;	
		$sql = "UPDATE vtiger_crmentity_seq SET id = ?";
		$adb->pquery($sql, array($crmid));
		return $crmid;
	}

	function insert_entity2($adb,$setype, $entity_id, $user_id, $description, $label, $source, $date)
	{
		$sth = "INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) VALUES (?,?,?,?,?,?,?,?,?)";
		$adb->pquery($sth, array($entity_id, $user_id, $user_id, $setype, $description, $source, $label, $date, $date));	
	}

	function insert_entityrel($adb, $entity_id, $module, $relcrmid, $relmodule)
	{
		$sth = 'INSERT INTO vtiger_crmentityrel (crmid, module, relcrmid, relmodule) VALUES (?,?,?,?)';
		$adb->pquery($sth, array($entity_id, $module, $relcrmid, $relmodule));
	}

	function createVise($adb, $entity_id,$vise)
	{
		$sth = 'INSERT INTO vtiger_purchasevise (purchaseviseid,purchasevise_tks_vise)  VALUES (?,?)';
		$adb->pquery($sth, array($entity_id, $vise));
	}


	public function UpdateIban($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();
		$sql = "SELECT cf_1462 AS iban FROM vtiger_vendorcf WHERE vendorid = ?";
		$sql2 = "UPDATE vtiger_purchaseordercf SET cf_1610 = ? WHERE purchaseorderid = ?";
		$sql3 = "SELECT vtiger_purchaseorder.purchaseorderid FROM vtiger_purchaseorder 
								LEFT JOIN vtiger_purchaseordercf ON vtiger_purchaseordercf.purchaseorderid=vtiger_purchaseorder.purchaseorderid
								WHERE vtiger_purchaseordercf.cf_1610 = '' AND vtiger_purchaseorder.vendorid = ?";

		$result = $adb->pquery($sql, array($account_id));
		$result2 = $adb->pquery($sql3, array($account_id));
		$getIban = $adb->fetch_array($result);	

		if(!empty($getIban['iban'])){	
			while($row = $adb->fetch_array($result2)) {
				$adb->pquery($sql2, array($getIban['iban'], $row['purchaseorderid']));
			}
		}


	}

	public function editAccountToMetrika($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();  
		$sql = "SELECT accountname,phone,contact,account_type,legal_entity_code,legal_vat_code,bill_street,bill_city,bill_code ,bill_country,customer_id,account_no
								FROM vtiger_account
								LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=accountid
								WHERE accountid = ?";  
		$sql2 = "UPDATE vtiger_account SET account_no = ?, customer_id = ?, billing_address = ?, municipality = ?, post_code = ? WHERE accountid = ?";
		$sql3 = "UPDATE vtiger_accountscf SET cf_1576 = ? WHERE accountid = ?";
		

		$result = $adb->pquery($sql, array($account_id));

		$account = array();

		$sql_test = "INSERT INTO app_MyGuests (lastname) VALUES (?)";



		while($row = $adb->fetch_array($result)) {
			$account["customer_id"] = $row["customer_id"];
			$account["company"] = $row["accountname"];
			$account["address"] = $row["bill_street"];
			$account["settlement"] = "";
			$account["municipality"] = $row["bill_city"];
			$account["region"] = "";
			$account["zipcode"] = $row["bill_code"];
			$account["country"] = $row["bill_country"];
			$account["person"] = $row["contact"];
			$account["phone"] = $row["phone"];
			$account["company_code"] = $row["legal_entity_code"];

		} 

		$res = json_encode($account); 



			if(is_numeric($account["customer_id"])){

					$res = json_encode($account);  
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/customers.php");           
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'customers' => $res));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
					$result = curl_exec($ch);
					curl_close($ch);	
					
					$data = json_decode($result, true);
					
					$state = $data[0];
					
					if($state['status'] == 'UPDATED') { 
						$data = json_decode($result, true);
						$customer_id = $data[0]['customer']['customer_id'];
						$customer_code = $data[0]['customer']['customer_code'];
						$address = $data[0]['customer']['address'];
						$municipality = $data[0]['customer']['municipality'];
						$zipcode = $data[0]['customer']['zipcode'];
						// $insert = $adb->pquery($sql2, array($customer_code,$customer_id,$address,$municipality,$zipcode,$account_id));
						// $insert2 = $adb->pquery($sql3, array($customer_id,$account_id));
						$insert3 = $adb->pquery($sql_test, array($customer_id));
					}
		}

	}



	public function exportAccountToMetrika($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();  
		$sql = "SELECT accountname,phone,contact,account_type,legal_entity_code,legal_vat_code,bill_street,bill_city,bill_code ,bill_country
								FROM vtiger_account
								LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=accountid
								WHERE accountid = ?";  
		$sql2 = "UPDATE vtiger_account SET account_no = ?, customer_id = ?, billing_address = ?, municipality = ?, post_code = ? WHERE accountid = ?";
		$sql3 = "UPDATE vtiger_accountscf SET cf_1576 = ? WHERE accountid = ?";
		
		$result = $adb->pquery($sql, array($account_id));
		$sql_test = "INSERT INTO app_MyGuests (email) VALUES ('?')";
		$account = array();

		while($row = $adb->fetch_array($result)) {
			$account["customer_id"] = "";
			$account["company"] = $row["accountname"];
			$account["address"] = $row["bill_street"];
			$account["settlement"] = "";
			$account["municipality"] = $row["bill_city"];
			$account["region"] = "";
			$account["zipcode"] = $row["bill_code"];
			$account["country"] = $row["bill_country"];
			$account["person"] = $row["contact"];
			$account["phone"] = $row["phone"];
			$account["company_code"] = $row["legal_entity_code"];

	}             

			$res = json_encode($account);  	

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/import/crm/customers.php?insert_new_customer=1");           
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'customers' => $res));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			$result = curl_exec($ch);
			curl_close($ch);	

			$data = json_decode($result, true);
			
			$state = $data[0];
                
			if($state['status'] != 'ERROR') { 
				$data = json_decode($result, true);
				$customer_id = $data[0]['customer']['customer_id'];
				$customer_code = $data[0]['customer']['customer_code'];
				$address = $data[0]['customer']['address'];
				$municipality = $data[0]['customer']['municipality'];
				$zipcode = $data[0]['customer']['zipcode'];
				$insert = $adb->pquery($sql2, array($customer_code,$customer_id,$address,$municipality,$zipcode,$account_id));
				$insert2 = $adb->pquery($sql3, array($customer_id,$account_id));
				$insert3 = $adb->pquery($sql_test, array($customer_id));
			}



	}



	/**
	 * Function to get the record model based on the request parameters
	 * @param Vtiger_Request $request
	 * @return Vtiger_Record_Model or Module specific Record Model instance
	 */
	public function getRecordModelFromRequest(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		if($moduleName == 'Calendar') {
			$moduleName = $request->get('calendarModule');
		}
		$recordId = $request->get('record');

		if(!empty($recordId)) {
			$recordModel = Vtiger_Record_Model::getInstanceById($recordId, $moduleName);
			$recordModel->set('id', $recordId);
			$recordModel->set('mode', 'edit');

			$fieldModelList = $recordModel->getModule()->getFields();
			foreach ($fieldModelList as $fieldName => $fieldModel) {
				//For not converting createdtime and modified time to user format
				$uiType = $fieldModel->get('uitype');
				if ($uiType == 70) {
					$fieldValue = $recordModel->get($fieldName);
				} else {
					$fieldValue = $fieldModel->getUITypeModel()->getUserRequestValue($recordModel->get($fieldName));
				}

				// To support Inline Edit in Vtiger7
				if($request->has($fieldName)){
					$fieldValue = $request->get($fieldName,null);
				}else if($fieldName === $request->get('field')){
					$fieldValue = $request->get('value');
				}

				$fieldDataType = $fieldModel->getFieldDataType();
				if ($fieldDataType == 'time') {
					$fieldValue = Vtiger_Time_UIType::getTimeValueWithSeconds($fieldValue);
				}
				if ($fieldValue !== null) {
					if (!is_array($fieldValue)) {
						$fieldValue = trim($fieldValue);
					}
					$recordModel->set($fieldName, $fieldValue);
				}
				$recordModel->set($fieldName, $fieldValue);
				if($fieldName === 'contact_id' && isRecordExists($fieldValue)) {
					$contactRecord = Vtiger_Record_Model::getInstanceById($fieldValue, 'Contacts');
					$recordModel->set("relatedContact",$contactRecord);
				}
			}
		} else {
			$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
			$recordModel->set('mode', '');

			$fieldModelList = $moduleModel->getFields();
			foreach ($fieldModelList as $fieldName => $fieldModel) {
				if ($request->has($fieldName)) {
					$fieldValue = $request->get($fieldName, null);
				} else {
					$fieldValue = $fieldModel->getDefaultFieldValue();
				}
				$fieldDataType = $fieldModel->getFieldDataType();
				if ($fieldDataType == 'time') {
					$fieldValue = Vtiger_Time_UIType::getTimeValueWithSeconds($fieldValue);
				}
				if ($fieldValue !== null) {
					if (!is_array($fieldValue)) {
						$fieldValue = trim($fieldValue);
					}
					$recordModel->set($fieldName, $fieldValue);
				}
			} 
		}

		return $recordModel;
	}
}
