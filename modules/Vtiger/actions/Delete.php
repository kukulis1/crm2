<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_Delete_Action extends Vtiger_Action_Controller {

	function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$record = $request->get('record');

		$currentUserPrivilegesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		if(!$currentUserPrivilegesModel->isPermitted($moduleName, 'Delete', $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if ($record) {
			$recordEntityName = getSalesEntityType($record);
			if ($recordEntityName !== $moduleName) {
				throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
			}
		}
	}

	public function process(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$recordId = $request->get('record');
		$ajaxDelete = $request->get('ajaxDelete');
		$recurringEditMode = $request->get('recurringEditMode');
		
		$recordModel = Vtiger_Record_Model::getInstanceById($recordId, $moduleName);
		$recordModel->set('recurringEditMode', $recurringEditMode);
		$moduleModel = $recordModel->getModule();				
		$recordModel->delete();	
		$cv = new CustomView();
		$cvId = $cv->getViewId($moduleName);
		deleteRecordFromDetailViewNavigationRecords($recordId, $cvId, $moduleName);
		$listViewUrl = $moduleModel->getListViewUrl();
		// itoma
		if($moduleName == 'Accounts'){
			$this->deleteVendor($recordModel);
		}elseif($moduleName == 'Invoice'){
			$this->returnInvoiceToWriteList($recordModel);
		}
		if($ajaxDelete) {		
			$response = new Vtiger_Response();
			$response->setResult($listViewUrl);
			return $response;
		} else {
			header("Location: $listViewUrl");
		}
	}

	public function deleteVendor($recordModel){
		$adb = PearDatabase::getInstance();
		$account_id = $recordModel->getId();
		$sql = "SELECT vendorid FROM vtiger_vendor WHERE accountid = ?";
		$sql2 = "UPDATE vtiger_crmentity SET deleted = 1 WHERE crmid = ?";
		$result = $adb->pquery($sql, array($account_id));		
	
		while($row = $adb->fetch_array($result)){
			$result = $adb->pquery($sql2, array($row['vendorid']));
		}
	}

	public function returnInvoiceToWriteList($recordModel){
		$adb = PearDatabase::getInstance();
		$invoice_id = $recordModel->getId();
		$sql = "DELETE FROM vtiger_invoice_salesorders_list WHERE invoiceid = ?";
		$adb->pquery($sql, array($invoice_id));	
		
		$sql2 = "DELETE FROM vtiger_invoice_auto_invoicing WHERE invoiceid = ?";
		$adb->pquery($sql2, array($invoice_id));		
	}

	public function validateRequest(Vtiger_Request $request) {
		$request->validateWriteAccess();
	}
}
