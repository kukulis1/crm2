<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_QuickCreateAjax_View extends Vtiger_IndexAjax_View {

	public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();

		if (!(Users_Privileges_Model::isPermitted($moduleName, 'CreateView'))) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED', $moduleName));
		}
	}

	public function process(Vtiger_Request $request) {
		$moduleName = $request->getModule();

		$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
		$moduleModel = $recordModel->getModule();
		
		$fieldList = $moduleModel->getFields();
		$requestFieldList = array_intersect_key($request->getAll(), $fieldList);

		foreach($requestFieldList as $fieldName => $fieldValue){
			$fieldModel = $fieldList[$fieldName];
			if($fieldModel->isEditable()) {
				$recordModel->set($fieldName, $fieldModel->getDBInsertValue($fieldValue));
			}
		}

		$fieldsInfo = array();
		foreach($fieldList as $name => $model){
			$fieldsInfo[$name] = $model->getFieldInfo();
		}

		if($_GET['record'] == 10965 AND $_GET['relatedModule'] == 'Products'){
			$fieldsInfo['consignee']['type'] = 'string';
		}


		$recordStructureInstance = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_QUICKCREATE);
		$picklistDependencyDatasource = Vtiger_DependencyPicklist::getPicklistDependencyDatasource($moduleName);

		$viewer = $this->getViewer($request);
		$viewer->assign('PICKIST_DEPENDENCY_DATASOURCE', Vtiger_Functions::jsonEncode($picklistDependencyDatasource));
		$viewer->assign('CURRENTDATE', date('Y-n-j'));
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('SINGLE_MODULE', 'SINGLE_'.$moduleName);
		$viewer->assign('MODULE_MODEL', $moduleModel);
		$viewer->assign('RECORD_STRUCTURE_MODEL', $recordStructureInstance);
		$viewer->assign('RECORD_STRUCTURE', $recordStructureInstance->getStructure());
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$viewer->assign('FIELDS_INFO', json_encode($fieldsInfo));
		$viewer->assign('SCRIPTS', $this->getHeaderScripts($request));

		if($moduleName == 'Debts'){
			$db = PearDatabase::getInstance();

			$customer_id = $request->get('relatedorganization');
			$sql = "SELECT invoice_no,
							ROUND(CASE 
								WHEN SUM(ABS(debts_tks_suma)) IS NULL
								THEN ABS(total)
								ELSE ABS(total) - SUM(ABS(vtiger_debts.debts_tks_suma))
							END,2) AS debt,SUM(ABS(debts_tks_suma)) AS debts_tks_suma
					FROM vtiger_invoice 
					LEFT JOIN vtiger_invoicecf on vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
					LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
					LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 	
					INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid				
					WHERE accountid = ? AND cf_1277 = 'Kreditinė' AND deleted = 0
					GROUP BY vtiger_invoice.invoiceid
					HAVING (debt > 0 OR debts_tks_suma IS NULL)";

			$get_credit_invoices = $db->pquery($sql,array($customer_id));		
			$credit_invoices = [];

			foreach ($get_credit_invoices as $row) {
				$credit_invoices[$row['invoice_no']] = ['debt' => $row['debt'], 'title' => $row['invoice_no']." (".$row['debt']." €)"];
			}

			// $get_all_types = $db->pquery("SELECT * FROM vtiger_debts_tks_tipas",array());

			// $invoice_types = [];
			// foreach ($get_all_types as $row) {
			// 	$invoice_types[$row['debts_tks_tipas']] = $row['debts_tks_tipas'];
			// }

			$record_id = $request->get('returnrecord');
			$get_invoice_type = $db->pquery("SELECT cf_1277 FROM vtiger_invoicecf WHERE invoiceid = ?",array($record_id));
			$invoice_type = $db->query_result($get_invoice_type,0,'cf_1277');

			// if($invoice_type == 'Kreditinė'){
			// 	unset($invoice_types['Kreditinė']);
			// }

			$viewer->assign('KREDITINES', $credit_invoices);
			// $viewer->assign('INVOICE_TYPES', $invoice_types);
			$viewer->assign('INVOICE_TYPE', $invoice_type);
		}

		$viewer->assign('MAX_UPLOAD_LIMIT_MB', Vtiger_Util_Helper::getMaxUploadSize());
		$viewer->assign('MAX_UPLOAD_LIMIT_BYTES', Vtiger_Util_Helper::getMaxUploadSizeInBytes());
		echo $viewer->view('QuickCreate.tpl',$moduleName,true);

	}
	
	
	public function getHeaderScripts(Vtiger_Request $request) {
		
		$moduleName = $request->getModule();
		
		$jsFileNames = array(
			"modules.$moduleName.resources.Edit"
		);

		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		return $jsScriptInstances;
	}
    
}