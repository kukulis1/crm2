<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/



class Vtiger_Detail_View extends Vtiger_Index_View {
	protected $record = false;
	protected $isAjaxEnabled = null;

	function __construct() {
		parent::__construct();
		$this->exposeMethod('showDetailViewByMode');
		$this->exposeMethod('showModuleDetailView');
		$this->exposeMethod('showModuleSummaryView');
		$this->exposeMethod('showModuleBasicView');
		$this->exposeMethod('showRecentActivities');
		$this->exposeMethod('showRecentComments');
		$this->exposeMethod('showRelatedList');
		$this->exposeMethod('showChildComments');
		$this->exposeMethod('getActivities');
		$this->exposeMethod('showRelatedRecords');
		$this->exposeMethod('showLinesHistory');
	}

	function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$recordId = $request->get('record');

		$recordPermission = Users_Privileges_Model::isPermitted($moduleName, 'DetailView', $recordId);
		if(!$recordPermission) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if ($recordId) {
			$recordEntityName = getSalesEntityType($recordId);
			if ($recordEntityName !== $moduleName) {
				// NOTE Protection from dublicates when save invoice
				if($moduleName == 'Invoice'){
					global $adb;
					$insert_history = "INSERT INTO app_check_dublicates (module, crmid, new_crmid,place, date) VALUES (?,?,?,?,?)";
					$get_seq_query = "SELECT id FROM `vtiger_crmentity_seq`";				
					$update_seq = "UPDATE `vtiger_crmentity_seq` SET id = ?";

					if(!empty($recordEntityName)){
						$getEntityName = $adb->pquery("SELECT setype FROM vtiger_crmentity WHERE crmid = ?",array($recordId));
						$recordEntityName = $adb->query_result($getEntityName,0,'setype');
					}
					
					$get_seq = $adb->pquery($get_seq_query, array());
					$last_entity_record = $adb->query_result($get_seq, 0, 'id');
					$adb->pquery($update_seq,array($last_entity_record));
					$adb->pquery($insert_history,array($recordEntityName,$recordId,$last_entity_record,'Save Invoice',date('Y-m-d H:i:s')));


					if($recordEntityName == 'Accounts'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb);
							$this->changeAccountId($recordId,$last_entity_record,$adb);
					}elseif($recordEntityName == 'Vendors'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb);
							$this->changeVendorId($recordId,$last_entity_record,$adb);
					}elseif($recordEntityName == 'SalesOrder'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb);
							$this->changeSalesOrderId($recordId,$last_entity_record,$adb);
							$this->changeInventoryProductRelId($recordId,$last_entity_record,$adb);
					}elseif($recordEntityName == 'Payment'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb);
							$this->changeCrmentityRelId($recordEntityName,$recordId,$last_entity_record,$adb);
							$this->changeDebtId($recordId,$last_entity_record,$adb);
					}elseif($recordEntityName == 'Documents'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb);            
							$this->changeDocumentsId($recordId,$last_entity_record,$adb);
					}elseif($recordEntityName == 'Documents Attachment'){
							// changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb);            
							// changeDocumentsAttachmentId($recordId,$last_entity_record,$adb);
					}elseif($recordEntityName == 'Purchaseorderdraft'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb);            
							$this->changePurchaseOrderDraftId($recordId,$last_entity_record,$adb);
					}elseif($recordEntityName == 'Routepoints'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb); 
							$this->changeCrmentityRelId($recordEntityName,$recordId,$last_entity_record,$adb);  
							$this->changeRoutePoint($recordId,$last_entity_record,$adb); 
						}elseif($recordEntityName == 'HelpDesk'){
							$this->changeCrmentityId($recordEntityName,$recordId,$last_entity_record,$adb); 
							$this->changeCrmentityRelId($recordEntityName,$recordId,$last_entity_record,$adb);  
							$this->changeHelpDesk($recordId,$last_entity_record,$adb); 
					}  
					

				}else{
					throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
				}
			}
		}
		return true;
	}

	function preProcess(Vtiger_Request $request, $display=true) {
		global $adb, $current_user;
		parent::preProcess($request, false);

		$recordId = $request->get('record');
		$moduleName = $request->getModule();
		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();
		$recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_DETAIL);
		$summaryInfo = array();
		// Take first block information as summary information
		$stucturedValues = $recordStrucure->getStructure();
		foreach($stucturedValues as $blockLabel=>$fieldList) {
			$summaryInfo[$blockLabel] = $fieldList;
			break;
		}

		$detailViewLinkParams = array('MODULE'=>$moduleName,'RECORD'=>$recordId);

		$detailViewLinks = $this->record->getDetailViewLinks($detailViewLinkParams);
		$navigationInfo = ListViewSession::getListViewNavigation($recordId);

		$viewer = $this->getViewer($request);
		$viewer->assign('RECORD', $recordModel);
		$viewer->assign('NAVIGATION', $navigationInfo);

		// ITOMA BEGIN
		if($moduleName == 'Invoice'){
			$sql = 'SELECT count,confirm, CONCAT(first_name," ",last_name) as employee,expired,url,receive_date,send_date ,email_addreses
																		FROM vtiger_send_invoice_for_client i 
																		LEFT JOIN vtiger_users u ON u.id=i.employee 
																		WHERE invoiceid = ?';
			$invoice_send_count_result = $adb->pquery($sql, array($recordId));
			$invoice_send_count = $adb->query_result($invoice_send_count_result, 0, 'count');
			$viewer->assign('SEND_INVOICE_COUNT', $invoice_send_count);

			// $sql2 = 'SELECT confirm FROM vtiger_send_invoice_for_client WHERE invoiceid = ? AND confirm = ?';
			// $invoice_confirmation_result = $adb->pquery($sql2, array($recordId, 1));

			$invoice_confirmation = $adb->query_result($invoice_send_count_result, 0, 'confirm');
			$employee = $adb->query_result($invoice_send_count_result, 0, 'employee');
			$expired = $adb->query_result($invoice_send_count_result, 0, 'expired');
			$url = $adb->query_result($invoice_send_count_result, 0, 'url');
			$receive_date = $adb->query_result($invoice_send_count_result, 0, 'receive_date');
			$email_addreses = $adb->query_result($invoice_send_count_result, 0, 'email_addreses');
			$send_date = $adb->query_result($invoice_send_count_result, 0, 'send_date');
			$invoice_info = array('employee' => $employee,'expired' => $expired,'url' => $url,'email_addreses' => $email_addreses,'receive_date' => $receive_date,'send_date' => $send_date, 'invoiceid' => $recordId);

			$viewer->assign('INVOICE_CONFIRMATION', $invoice_confirmation);
			$viewer->assign('INVOICE_INFO', $invoice_info);
			
			$sql3 = "SELECT * FROM vtiger_invoice_exported_agnum WHERE invoiceid = ?";			
			$exported_invoice = $adb->pquery($sql3, array($recordId));
			$agnum_exported = $adb->query_result($exported_invoice, 0, 'invoiceid');
			$viewer->assign('AGNUM_EXPORTED', $agnum_exported);

			$sql4 = "SELECT cf_1277 as type, to_invoiceid FROM vtiger_invoicecf LEFT JOIN vtiger_invoice_converted_from_preinvoice ON from_invoiceid=invoiceid WHERE invoiceid = ?";		
			$invoice_type = $adb->pquery($sql4, array($recordId));
			$type = $adb->query_result($invoice_type, 0, 'type');
			$converted = $adb->query_result($invoice_type, 0, 'to_invoiceid');
			$viewer->assign('INVOICE_TYPE', $type);
			$viewer->assign('CONVERTED', $converted);
		}

		if($moduleName == 'Accounts'){
			$query = "SELECT pricebook FROM vtiger_account WHERE accountid = ?";
			$pricebook = $adb->pquery($query, array($recordId));			
			$pricebook_id = $adb->query_result($pricebook,0,'pricebook');

			$pricebooks = [];
			if ($pricebook_id ) {
                $query2 = "SELECT pricebookid, bookname FROM vtiger_pricebook WHERE pricebookid IN ($pricebook_id)";
                $pricebooks = $adb->pquery($query2, array());
            }
			$existingRelatedContacts = array();
			foreach($pricebooks AS $row){
				$existingRelatedContacts[] = array('name' => $row['bookname'], 'id' => $row['pricebookid']);
			}
			$viewer->assign('RELATED_PRICEBOOKS', $existingRelatedContacts);
		}
		if($moduleName == 'Claims'){
			$query = "SELECT claim_id FROM vtiger_claims_vise WHERE claim_id = ? AND ceo = 1";
			$query2 = "SELECT cf_1856 FROM vtiger_claimscf WHERE claimsid = ? AND cf_1856 = 1";
			$claim_id = $adb->pquery($query, array($recordId));	
			$cf_1856 = $adb->pquery($query2, array($recordId));	
			$num_rows = $adb->num_rows($claim_id);
			$num_rows2 = $adb->num_rows($cf_1856);
			if($num_rows || $num_rows2)
				$viewer->assign('VISE', true);
			else
				$viewer->assign('VISE', false);
			
			$sql = "SELECT claimsno,c.claimsid FROM vtiger_claims c JOIN vtiger_claimscf cf ON cf.claimsid=c.claimsid INNER JOIN vtiger_crmentity e ON e.crmid=c.claimsid WHERE e.deleted = 0 AND claimsno != '' AND c.claimsid != ? GROUP BY claimsid ORDER BY c.claimsno  DESC";
			$records = array();
			$result = $adb->pquery($sql,array($recordId));  
	
			foreach($result AS $row){
				$records[] = $row;
			}
			$viewer->assign('GET_ACTIVE_CLAIMS', $records);  
		}

		if($moduleName == 'SalesOrder'){
			$sticker_email_template_sql = 'SELECT value 
								FROM app_other_settings 
								WHERE title = "LBL_SEND_STICKERS_TEXT"';
			$sticker_email_template_result = $adb->pquery($sticker_email_template_sql, array());
			$sticker_email_template = $adb->query_result($sticker_email_template_result, 0, 'value');
			$viewer->assign('STICKER_EMAIL_TEMPLATE', $sticker_email_template);

			$client_email_sql = 'SELECT email1 , vtiger_account.accountid
								FROM vtiger_account
								LEFT JOIN vtiger_salesorder ON vtiger_salesorder.accountid = vtiger_account.accountid
								WHERE vtiger_salesorder.salesorderid = ?';
			$client_email_result = $adb->pquery($client_email_sql, array($recordId));
			$client_email = $adb->query_result($client_email_result,0 , 'email1');
			$accountid = $adb->query_result($client_email_result,0 , 'accountid');

			$get_invoice_info = $adb->pquery("SELECT vtiger_invoice.invoiceid, invoice_no 
																				FROM vtiger_invoice_salesorders_list
																				JOIN vtiger_invoice ON vtiger_invoice.invoiceid=vtiger_invoice_salesorders_list.invoiceid
																				INNER JOIN vtiger_crmentity ON crmid=vtiger_invoice.invoiceid
																				WHERE vtiger_invoice_salesorders_list.salesorderid = ? AND deleted = 0 
																				ORDER BY vtiger_invoice.invoiceid DESC LIMIT 1", array($recordId));	
			$has_invoice = false;
			if($adb->num_rows($get_invoice_info)){	
				$ORDER_INVOICE_NO = $adb->query_result($get_invoice_info, 0, 'invoice_no');																											
				$ORDER_INVOICE_ID = $adb->query_result($get_invoice_info, 0, 'invoiceid');		
				$has_invoice = true;
			}else{
				$ORDER_INVOICE_NO = '';
				$ORDER_INVOICE_ID = 'Nėra sąskaitos';
			}											
	
			
			$viewer->assign('CHECK_SALESORDER_INVOICE', $has_invoice);
			$viewer->assign('SALESORDER_ID', $recordId);
			$viewer->assign('user_id', $accountid);
			$viewer->assign('roleid', $current_user->roleid);
			$viewer->assign('CLIENT_EMAIL', $client_email);
			$viewer->assign('ORDER_INVOICE_NO', $ORDER_INVOICE_NO);
			$viewer->assign('ORDER_INVOICE_ID', $ORDER_INVOICE_ID);
		
		}
		// ITOMA END

		//Intially make the prev and next records as null
		$prevRecordId = null;
		$nextRecordId = null;
		$found = false;
		if ($navigationInfo) {
			foreach($navigationInfo as $page=>$pageInfo) {
				foreach($pageInfo as $index=>$record) {
					//If record found then next record in the interation
					//will be next record
					if($found) {
						$nextRecordId = $record;
						break;
					}
					if($record == $recordId) {
						$found = true;
					}
					//If record not found then we are assiging previousRecordId
					//assuming next record will get matched
					if(!$found) {
						$prevRecordId = $record;
					}
				}
				//if record is found and next record is not calculated we need to perform iteration
				if($found && !empty($nextRecordId)) {
					break;
				}
			}
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if(!empty($prevRecordId)) {
			$viewer->assign('PREVIOUS_RECORD_URL', $moduleModel->getDetailViewUrl($prevRecordId));
		}
		if(!empty($nextRecordId)) {
			$viewer->assign('NEXT_RECORD_URL', $moduleModel->getDetailViewUrl($nextRecordId));
		}

		$viewer->assign('MODULE_MODEL', $this->record->getModule());
		$viewer->assign('DETAILVIEW_LINKS', $detailViewLinks);

		$viewer->assign('IS_EDITABLE', $this->record->getRecord()->isEditable($moduleName));
		$viewer->assign('IS_DELETABLE', $this->record->getRecord()->isDeletable($moduleName));

		$linkParams = array('MODULE'=>$moduleName, 'ACTION'=>$request->get('view'));
		$linkModels = $this->record->getSideBarLinks($linkParams);
		$viewer->assign('QUICK_LINKS', $linkModels);
		$viewer->assign('MODULE_NAME', $moduleName);

		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$viewer->assign('DEFAULT_RECORD_VIEW', $currentUserModel->get('default_record_view'));

		$picklistDependencyDatasource = Vtiger_DependencyPicklist::getPicklistDependencyDatasource($moduleName);
		$viewer->assign('PICKIST_DEPENDENCY_DATASOURCE', Vtiger_Functions::jsonEncode($picklistDependencyDatasource));

		$tagsList = Vtiger_Tag_Model::getAllAccessible($currentUserModel->getId(), $moduleName, $recordId);
		$allUserTags = Vtiger_Tag_Model::getAllUserTags($currentUserModel->getId());
		$viewer->assign('TAGS_LIST', $tagsList);
		$viewer->assign('ALL_USER_TAGS', $allUserTags);

		$appName = $request->get('app');
		if(!empty($appName)){
			$viewer->assign('SELECTED_MENU_CATEGORY',$appName);
		}

		$selectedTabLabel = $request->get('tab_label');
		$relationId = $request->get('relationId');

		if(empty($selectedTabLabel)) {
			if($currentUserModel->get('default_record_view') === 'Detail') {
				$selectedTabLabel = vtranslate('SINGLE_'.$moduleName, $moduleName).' '. vtranslate('LBL_DETAILS', $moduleName);
			} else{
				if($moduleModel->isSummaryViewSupported()) {
					$selectedTabLabel = vtranslate('SINGLE_'.$moduleName, $moduleName).' '. vtranslate('LBL_SUMMARY', $moduleName);
				} else {
					$selectedTabLabel = vtranslate('SINGLE_'.$moduleName, $moduleName).' '. vtranslate('LBL_DETAILS', $moduleName);
				}
			}
		}

		$viewer->assign('SELECTED_TAB_LABEL', $selectedTabLabel);
		$viewer->assign('SELECTED_RELATION_ID',$relationId);

		//Vtiger7 - TO show custom view name in Module Header
		$viewer->assign('CUSTOM_VIEWS', CustomView_Record_Model::getAllByGroup($moduleName));

		$viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
		if($display) {
			$this->preProcessDisplay($request);
		}
	}

	function preProcessTplName(Vtiger_Request $request) {
		return 'DetailViewPreProcess.tpl';
	}

	function process(Vtiger_Request $request) {
		$module = $request->getModule();
		if($_POST['emailAddress'] && $module == 'Invoice') {
			$this->sendEmailToClient($_POST,$request);
		}

		if($_POST['emailAddress'] && $module == 'SalesOrder') {
			$this->sendLabelsToClient($_POST,$request);
		}
		$mode = $request->getMode();
		if(!empty($mode)) {
			echo $this->invokeExposedMethod($mode, $request);
			return;
		}

		if($module == 'Invoice' && $_GET['generateToDebit'] == 'true'){
			$this->generateCreditInvoiceToDebit($request);
		}

		$currentUserModel = Users_Record_Model::getCurrentUserModel();

		if ($currentUserModel->get('default_record_view') === 'Summary') {
			echo $this->showModuleBasicView($request);
		} else {
			echo $this->showModuleDetailView($request);
		}
	}

	public function sendEmailToClient($post,$request){	
		include "vtlib/Vtiger/ShortLink/generateShortLink.php";
		require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
		require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
		require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';
		require_once 'vtlib/Vtiger/Email/vendor/autoload.php';


		global $adb;
		global $current_user;
		global $mailHost, $mailUsername,$mailPassword, $mailPort, $mailEncryption;
  
        $this->mailHost = $mailHost;
        $this->mailUsername = $mailUsername;
        $this->mailPassword = $mailPassword;    
        $this->mailPort = $mailPort;    
        $this->mailEncryption = $mailEncryption;    

		$recordId = $request->get('record');	

		$get_storage_time = $adb->pquery("SELECT value FROM app_other_settings WHERE title = 'LBL_INVOICE_PDF_SAVE_TIME'");
		$storage_days = $adb->query_result($get_storage_time, 0, 'value');

		$result = $adb->pquery('SELECT subject FROM vtiger_invoice WHERE invoiceid = ?', array($recordId));
		$subject = $adb->query_result($result, 0, 'subject');

		$subject_pure = $adb->query_result($result, 0, 'subject');
		$subject = str_replace('/','',$subject);
		$date_with_added_days = date('Y-m-d H:i:s', strtotime("+ $storage_days days"));

		$filename = $subject.".pdf";

		$subject = "Parnasas saskaita nr. $subject_pure";  
		$to = explode(",",$post['emailAddress']);  
		$hash = md5( rand(0,1000) ); 
		$message = $post['message'];


		$target_dir = "storage/invoices/"; 
		$files_arr = array();

		if(!empty($_FILES["files"]["name"][0])){
			for($i=0; $i < count($_FILES["files"]["name"]); $i++){
				$oldName = basename($_FILES["files"]["name"][$i]);
				$ext = pathinfo($_FILES["files"]["name"][$i], PATHINFO_EXTENSION); 
				$newName = $this->lt_chars(str_replace(' ', '',pathinfo($oldName, PATHINFO_FILENAME))).".".$ext; 
				$target_file = $target_dir . $newName;  
				$files_arr[] =$target_file;
				move_uploaded_file($_FILES["files"]["tmp_name"][$i], $target_file);
			}
		}

		$get_invoice_text = $adb->pquery('SELECT * FROM app_other_settings WHERE title = ?', array('LBL_SEND_INVOICE_TEXT'));
		$text = $adb->query_result($get_invoice_text, 0, 'value');	
		
		$check_exist = 'SELECT hash FROM vtiger_send_invoice_for_client WHERE invoiceid = ?';
		$sended_invoice = $adb->pquery($check_exist, array($recordId));	
		$num_rows = $adb->num_rows($sended_invoice);

		if($num_rows){
			$hash = $adb->query_result($sended_invoice,0,'hash');
		}

		// $getUrl = createShortLink("https://".$_SERVER["HTTP_HOST"]."/downloadInvoice/download.php?invoice=$recordId&secret=$hash");
		// $url = $getUrl['responce']['link'];
		// // $id = $getUrl['responce']['id'];
		// $status = $getUrl['status'];
        
        $url = "https://uzsakymai.parnasas.lt/download-invoice/index.php?";
        $url .= http_build_query([
            'invoice' => $recordId,
            'secret' => $hash
        ]);
		
		$text = str_replace('br', '<br>',$text);

		$confirmation_message = $text." <a href='$url'>$url</a> <br> Atsisiuntimo nuoroda galioja iki: $date_with_added_days";

		$get_employee_info = $adb->pquery("SELECT CONCAT(first_name,' ',last_name) AS employee, rolename,phone_mobile
								 FROM vtiger_users u
								 LEFT JOIN vtiger_user2role ur ON ur.userid=u.id
								 LEFT JOIN vtiger_role r ON r.roleid=ur.roleid
								 WHERE id = ?",array($current_user->id));   

		$employee = $adb->query_result($get_employee_info,0,'employee');
		$rolename = $adb->query_result($get_employee_info,0,'rolename');
		$phone_mobile = $adb->query_result($get_employee_info,0,'phone_mobile');

		$bodyText = "<html><body>
		$confirmation_message
		<br>
		$message
   		 <br><br>
			<img src='https://crm.parnasas.lt/resources/signatare.jpg'><br>
			Pagarbiai,<br>
			$employee<br>
			$rolename <br>   
			$phone_mobile
		</body></html>";


		$mail = new PHPMailer\PHPMailer\PHPMailer(true);

		try {
			//Server settings
			// $mail->SMTPDebug = PHPMailer\PHPMailer\SMTP::DEBUG_SERVER;
			$mail->isSMTP();
			$mail->Host       = $this->mailHost;
			$mail->SMTPAuth   = true;
			$mail->Username   = $this->mailUsername;
			$mail->Password   = $this->mailPassword;
			// $mail->SMTPSecure = isset($this->mailEncryption) ? $this->mailEncryption : 'tls';
			$mail->SMTPSecure = PHPMailer\PHPMailer\PHPMailer::ENCRYPTION_SMTPS;
			$mail->Port       = 465;
			$mail->SMTPOptions = array(
			    'ssl' => array(
			        'verify_peer' => false,
			        'verify_peer_name' => false,
			        'allow_self_signed' => true
			    )
			);
			$mail->setLanguage('lt', '../vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
			$mail->CharSet = 'UTF-8';
			// $mail->SMTPDebug = false;

			//Recipients
			$mail->setFrom($this->mailUsername, 'Parnasas');
			
			for ($i = 0; $i < count($to); $i++) {
					$mail->addAddress($to[$i]);
			}

			if($cc){
				$mail->addCC($cc);
			}
			
			if(!empty($_FILES["files"]["name"][0])){
				for($x=0; $x < count($files_arr); $x++){
					$mail->addAttachment($files_arr[$x]);   
				}		
			}

	
			if(count($post["ordersFiles"]) > 0){
				for($x=0; $x < count($post["ordersFiles"]); $x++){
					if(file_exists($post["ordersFiles"][$x])){
						$mail->addAttachment($post["ordersFiles"][$x]);   
					}
				}		
			}

			// Content
			$mail->isHTML(true); 
			$mail->Subject = "Parnasas saskaita nr. $subject_pure";
			// $mail->MsgHTML($bodyText);
			$mail->Body    = $bodyText;
			$mail->AltBody = $bodyText;
			$mail->send();				
			

			$insert_sended_invoice_info = 'INSERT INTO vtiger_send_invoice_for_client (invoiceid, `count`, `hash`,`send_date`, `status`,`invoice_no`,`employee`,`url`,`expired`,`email_addreses`) VALUES (?,?,?,?,?,?,?,?,?,?)';
			$update_sended_invoice_info = 'UPDATE vtiger_send_invoice_for_client SET `count` = `count`+?, `hash` = ?, `status` = ?, employee = ?, `url` = ?, `expired` = ?,email_addreses = ?  WHERE invoiceid = ?';
			$log_send_invoice = 'INSERT INTO vtiger_send_invoice_for_client_log (`invoiceid`,`email_addreses`,`employee`,`url`,`send_date`) VALUES (?,?,?,?,?)';

			

			$date = date("Y-m-d H:i:s");

			if(!$num_rows){
				$adb->pquery($insert_sended_invoice_info, array($recordId,1,$hash,$date,'success',$filename,$current_user->id,$url,$date_with_added_days,$post['emailAddress']));
			}else{
				$adb->pquery($update_sended_invoice_info, array(1,$hash,'success',$current_user->id,$url,$date_with_added_days,$post['emailAddress'],$recordId));
			}

			$adb->pquery($log_send_invoice, array($recordId,$post['emailAddress'],$current_user->id,$url,$date));


			if(!empty($_FILES["files"]["name"][0])){
				for($x=0; $x < count($files_arr); $x++){
					if(file_exists($files_arr[$x])){						
						unlink($files_arr[$x]);
					}
				}		
			}	

			$deleteFiles = explode(",",$post["deleteFiles"]);
			if(count($deleteFiles) > 0)	{		
				for($x=0; $x < count($deleteFiles); $x++){							
					if(file_exists($deleteFiles[$x])){		
						unlink($deleteFiles[$x]);
					}
				}	
			}		


			// NOTE saskaitos issiuntimo istorijos irasymas
			$get_last_modtracker_id = $adb->pquery('SELECT id FROM vtiger_modtracker_basic_seq');
			$last_modtracker_id = $adb->query_result($get_last_modtracker_id, 0, 'id') +1; 
			$adb->pquery("UPDATE vtiger_modtracker_basic_seq SET id = ?", array($last_modtracker_id)); 			
			$adb->pquery("INSERT INTO vtiger_modtracker_basic (id, crmid, module, whodid, changedon,status) VALUES (?,?,?,?,?,?)", array($last_modtracker_id, $recordId, 'Invoice', $current_user->id, $date, 0));	
			$adb->pquery("INSERT INTO vtiger_modtracker_detail (id, fieldname, prevalue, postvalue) VALUES (?,?,?,?)", array($last_modtracker_id, 'cf_2720', '', "$current_user->first_name $current_user->last_name išsiuntė sąskaitą į ".$post['emailAddress']));	
				
				
			$referer = $_SERVER['PHP_SELF']."?module=Invoice&view=Detail&record=$recordId"; 
			return header("Location: $referer");

		} catch (Exception $e) {	
				// echo "Message could not be sent. Mailer Error: {$e->getMessage()}";
				$error = $e->getMessage();
				$adb->pquery("INSERT INTO email_send_from_crm_fails (record, email_address, error, time) VALUES (?,?,?,?)", array($recordId, $post['emailAddress'], $error, date("Y-m-d H:i:s")));
				echo "<script>alert('Klaida!! Žinutė nebuvo išsiųsta! Prašome, bandykite dar kartą.   $error')</script>";
		}


	}


public function generateCreditInvoiceToDebit($req){
		global $adb;
		global $current_user;
		$date = date("Y-m-d H:i:s");
		$recordId = $req->get('record');

		$getInvoiceInfoSql = "SELECT i.invoiceid, salesorderid,subtotal,total,s_h_amount,i.accountid,invoicestatus,received,s_h_percent,region_id,current_date() AS invoicedate,DATE_ADD(current_date(), INTERVAL cf_1279 DAY) as duedate,cf_1486 AS template,cf_1516 AS payment_type,cf_2114 AS lang
														FROM vtiger_invoice i 
														JOIN vtiger_invoicecf c ON c.invoiceid=i.invoiceid													
														LEFT JOIN vtiger_accountscf a ON a.accountid=i.accountid 
														WHERE i.invoiceid = ?";

		$getInvoiceRowsSql = "SELECT id,productid,sequence_no,quantity,listprice, discount_percent,discount_amount,comment,description,incrementondel,cargo_measure,cargo_wgt,order_id,note,bill_ads,ship_ads,service,m3,costcenter,employee,object
													FROM vtiger_inventoryproductrel WHERE id = ?";

		$create_invoice_query = "INSERT INTO vtiger_invoice (invoiceid,subject,salesorderid,invoicedate,duedate,subtotal,total,s_h_amount,accountid,invoicestatus,invoice_no,received,s_h_percent,region_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";											
		$create_invoice_query2 = "INSERT INTO vtiger_invoicecf (invoiceid,cf_1277,cf_1486,cf_1516,cf_2114,cf_2437) VALUES (?,?,?,?,?,?)";			
		$create_invoice_entity = "INSERT INTO vtiger_crmentity (crmid,smcreatorid,smownerid,setype,createdtime,modifiedtime,source,deleted,label) VALUES (?,?,?,?,?,?,?,?,?)";			

		$create_invoice_rows_query = "INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,listprice, discount_percent,discount_amount,comment,description,incrementondel,cargo_measure,cargo_wgt,order_id,note,bill_ads,ship_ads,service,m3,costcenter,employee,object) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		$insert_to_invoice_salesorder = "INSERT INTO vtiger_invoice_salesorders_list (invoiceid, salesorderid) VALUES (?,?)";

		$check_list = $adb->pquery("SELECT to_invoiceid 
																FROM vtiger_invoicecf 
																LEFT JOIN vtiger_invoice_converted_from_preinvoice ON from_invoiceid=invoiceid 
																WHERE invoiceid = ?",array($recordId));

		$to_invoiceid = $adb->query_result($check_list,0,'to_invoiceid');
		if($to_invoiceid){
			header("Location:/index.php?module=Invoice&view=Detail&record=$to_invoiceid");	
		}	else{							 

			$result = $adb->pquery($getInvoiceInfoSql,array($recordId));   
			$result2 = $adb->pquery($getInvoiceRowsSql,array($recordId)); 
		

			$salesorderid = $adb->query_result($result,0,'salesorderid');
			$invoicedate = $adb->query_result($result,0,'invoicedate');
			$duedate = $adb->query_result($result,0,'duedate');
			$subtotal = $adb->query_result($result,0,'subtotal');
			$total = $adb->query_result($result,0,'total');
			$s_h_amount = $adb->query_result($result,0,'s_h_amount');
			$accountid = $adb->query_result($result,0,'accountid');
			$invoicestatus = $adb->query_result($result,0,'invoicestatus');
			$received = $adb->query_result($result,0,'received');
			$s_h_percent = $adb->query_result($result,0,'s_h_percent');
			$region_id = $adb->query_result($result,0,'region_id');
			$template = $adb->query_result($result,0,'template');
			$payment_type = $adb->query_result($result,0,'payment_type');
			$lang = $adb->query_result($result,0,'lang');			

			$inventory_id = $this->getEntityId();
			$prev_inv_no = $this->setModuleSeqNumber(); 
	
			$adb->pquery($create_invoice_query,array($inventory_id, $prev_inv_no, $salesorderid, $invoicedate, $duedate, $subtotal ,$total, $s_h_amount,$accountid, $invoicestatus, $prev_inv_no, $received, $s_h_percent, $region_id)); 

			$adb->pquery($create_invoice_query2,array($inventory_id,'Debetinė',$template, $payment_type, $lang, 1));  

			$adb->pquery($create_invoice_entity,array($inventory_id, $current_user->id, $current_user->id, 'Invoice', $date,$date, 'CRM', 0, $prev_inv_no)); 		
			

			foreach ($result2 AS $val2) {
				$adb->pquery($create_invoice_rows_query,array($inventory_id,$val2['productid'],$val2['sequence_no'],$val2['quantity'],$val2['listprice'],$val2['discount_percent'],$val2['discount_amount'],$val2['comment'],$val2['description'],$val2['incrementondel'],$val2['cargo_measure'],$val2['cargo_wgt'],$val2['order_id'],$val2['note'],$val2['bill_ads'],$val2['ship_ads'],$val2['service'],$val2['m3'],$val2['costcenter'],$val2['employee'],$val2['object'])); 
				if($val2['order_id']){
					$adb->pquery($insert_to_invoice_salesorder,array($inventory_id,$val2['order_id']));
				}
			}

			$adb->pquery("INSERT INTO vtiger_invoice_converted_from_preinvoice (from_invoiceid, to_invoiceid) VALUES (?,?)", array($recordId,$inventory_id));

			header("Location:/index.php?module=Invoice&view=Detail&record=$inventory_id");	
		}
	}

	public function setModuleSeqNumber(){
		global $adb;
		$today = date('Y-m-d');
	
		$check_cur = $adb->pquery("SELECT invoice_no FROM vtiger_invoice 
																									LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid 
																									LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
																									WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = ? AND vtiger_invoicecf.cf_1277 LIKE 'Debetinė' AND vtiger_crmentity.deleted = 0 
																									ORDER BY vtiger_invoice.invoice_no  
																									DESC LIMIT 1",array($today));	
	
	
		$curid = $adb->query_result($check_cur,0,'invoice_no');
		if(!empty($curid)){
			$curid = substr($curid, -3);
		}else{
			$req_no = '001';
		}
	
		$prefix = 'PT';
		$todayDate = date('y/m/d');
		if(!empty($curid)){
			$strip = strlen($curid) - strlen($curid + 1);
			if ($strip < 0) $strip = 0;
			$temp = str_repeat("0", $strip);
			$req_no.= $temp . ($curid + 1);
		}
	
		$prev_inv_no = $prefix.$todayDate."/".$req_no;
		return $prev_inv_no;
	}

	public function getEntityId(){
		global $adb;

    $sql = "SELECT id FROM `vtiger_crmentity_seq`";
    $sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
    $lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
    $lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
    $lock3 = 'UNLOCK TABLES';

    $adb->pquery($lock, array()); 
      $get_id = $adb->pquery($sql, array()); 
      $seq = $adb->query_result($get_id,0,'id');    
    $adb->pquery($lock3, array());    

    $new_seq = $seq + 1;   

    $adb->pquery($lock2, array()); 
     $adb->pquery($sql2, array($new_seq)); 
    $adb->pquery($lock3, array()); 

    return $new_seq;
  }


	public function sendLabelsToClient($post,$request){
		require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/Exception.php';
		require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/PHPMailer.php';
		require 'vtlib/Vtiger/vendor/phpmailer/phpmailer/src/SMTP.php';
		require_once 'vtlib/Vtiger/Email/vendor/autoload.php';

		global $adb;
		global $current_user;
		global $mailHost, $mailUsername,$mailPassword;
  
		$this->mailHost = $mailHost;
		$this->mailUsername = $mailUsername;
		$this->mailPassword = $mailPassword;    

		$recordId = $request->get('record');

		$sql_shipment_code = "SELECT shipment_code
								FROM vtiger_salesorder so
								WHERE salesorderid = ?";

		$result_sc = $adb->pquery($sql_shipment_code,array($recordId));   
		$shipment_code_full = $adb->query_result($result_sc,0,'shipment_code');
		$shipment_code = substr($shipment_code_full, 0, strrpos($shipment_code_full, '-'));

		$subject = "Užsakymo lipdukai nr. ". $shipment_code;  
		$to = explode(",",$post['emailAddress']);  
		$hash = md5( rand(0,1000) ); 
		$message = $post['message'];
		$file_path = $post['file_path'];

		$sql6 = "SELECT CONCAT(first_name,' ',last_name) AS employee, rolename,phone_mobile
							FROM crm.vtiger_users u
							LEFT JOIN vtiger_user2role ur ON ur.userid=u.id
							LEFT JOIN vtiger_role r ON r.roleid=ur.roleid
							WHERE id = ?";

		$result3 = $adb->pquery($sql6,array($current_user->id));   
		$employee = $adb->query_result($result3,0,'employee');
		$rolename = $adb->query_result($result3,0,'rolename');
		$phone_mobile = $adb->query_result($result3,0,'phone_mobile');

		$bodyText = "<html>
						<body>
							$message
   		 					<br><br>
      						<img src='https://crm.parnasas.lt/resources/signatare.jpg'>
							<br>
							Pagarbiai,<br>
							$employee<br>
							$rolename <br>   
							$phone_mobile
						</body>
					</html>";

		$mail = new PHPMailer\PHPMailer\PHPMailer(true);
		$SMTP = new PHPMailer\PHPMailer\SMTP;
		$PHPMailer = new PHPMailer\PHPMailer\PHPMailer;
	
		try {
			//Server settings
			// $mail->SMTPDebug = $SMTP::DEBUG_SERVER;                      
			// $mail->isSMTP();                                            
			$mail->Host       = $this->mailHost;                    
			$mail->SMTPAuth   = true;                                   
			$mail->Username   = $this->mailUsername;                    
			$mail->Password   = $this->mailPassword;                               
			$mail->SMTPSecure = $PHPMailer::ENCRYPTION_STARTTLS;        
			$mail->Port       = 587; 
			// $mail->SMTPOptions = array(
			// 	'ssl' => array(
			// 		'verify_peer' => false,
			// 		'verify_peer_name' => false,
			// 		'allow_self_signed' => true
			// 	)
			// );
			$mail->setLanguage('lt', '../vtlib/Vtiger/vendor/phpmailer/phpmailer/language/');
			$mail->CharSet = 'UTF-8';
			// $mail->SMTPDebug = false;  

			//Recipients
			$mail->setFrom($this->mailUsername, 'Parnasas');
			for($i=0; $i < count($to); $i++){
				$mail->addAddress($to[$i]);  
			}

			if($cc){
			 $mail->addCC($cc);
			}
			
			if(!empty($file_path)){
				$mail->addAttachment($file_path);  
			}

			// Content
			$mail->isHTML(true); 
			$mail->Subject = $subject;
			// $mail->MsgHTML($bodyText);
			$mail->Body    = $bodyText;
			$mail->AltBody = $bodyText;
			$mail->send();
			
			$referer = $_SERVER['PHP_SELF']."?module=SalesOrder&view=Detail&record=$recordId";
			return header("Location: $referer");
		} catch (Exception $mail) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	}

	public function lt_chars($text) {   
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

    return $text; 
  }


	public function postProcess(Vtiger_Request $request) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();
		if($moduleName=="Calendar"){
			$recordModel = Vtiger_Record_Model::getInstanceById($recordId);
			$activityType = $recordModel->getType();
			if($activityType=="Events"){
				$moduleName="Events";
			}
		}
		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$detailViewLinkParams = array('MODULE'=>$moduleName,'RECORD'=>$recordId);
		$detailViewLinks = $this->record->getDetailViewLinks($detailViewLinkParams);

		$selectedTabLabel = $request->get('tab_label');
		$relationId = $request->get('relationId');

		if(empty($selectedTabLabel)) {
			if($currentUserModel->get('default_record_view') === 'Detail') {
				$selectedTabLabel = vtranslate('SINGLE_'.$moduleName, $moduleName).' '. vtranslate('LBL_DETAILS', $moduleName);
			} else{
				if($moduleModel->isSummaryViewSupported()) {
					$selectedTabLabel = vtranslate('SINGLE_'.$moduleName, $moduleName).' '. vtranslate('LBL_SUMMARY', $moduleName);
				} else {
					$selectedTabLabel = vtranslate('SINGLE_'.$moduleName, $moduleName).' '. vtranslate('LBL_DETAILS', $moduleName);
				}
			}
		}

		$viewer = $this->getViewer($request);

		$viewer->assign('SELECTED_TAB_LABEL', $selectedTabLabel);
		$viewer->assign('SELECTED_RELATION_ID',$relationId);
		$viewer->assign('MODULE_MODEL', $this->record->getModule());
		$viewer->assign('DETAILVIEW_LINKS', $detailViewLinks);

		$viewer->view('DetailViewPostProcess.tpl', $moduleName);

		parent::postProcess($request);
	}


	public function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);
		$moduleName = $request->getModule();

		$jsFileNames = array(
			'modules.Vtiger.resources.Detail',
			"modules.$moduleName.resources.Detail",
			'modules.Vtiger.resources.RelatedList',
			"modules.$moduleName.resources.RelatedList",
			'libraries.jquery.jquery_windowmsg',
			"libraries.jquery.ckeditor.ckeditor",
			"libraries.jquery.ckeditor.adapters.jquery",
			"modules.Emails.resources.MassEdit",
			"modules.Vtiger.resources.CkEditor",
			"~/libraries/jquery/twitter-text-js/twitter-text.js",
			"libraries.jquery.multiplefileupload.jquery_MultiFile",
			'~/libraries/jquery/bootstrapswitch/js/bootstrap-switch.min.js',
			'~/libraries/jquery.bxslider/jquery.bxslider.min.js',
			"~layouts/v7/lib/jquery/Lightweight-jQuery-In-page-Filtering-Plugin-instaFilta/instafilta.js",
			'modules.Vtiger.resources.Tag',
			'modules.Google.resources.Map',
			// "modules.Notification.resources.NotificationsJS" // Notifications itoma			
		);

		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}

	function showDetailViewByMode(Vtiger_Request $request) {
		$requestMode = $request->get('requestMode');
		if($requestMode == 'full') {
			return $this->showModuleDetailView($request);
		}
		return $this->showModuleBasicView($request);
	}

	/**
	 * Function shows the entire detail for the record
	 * @param Vtiger_Request $request
	 * @return <type>
	 */
	function showModuleDetailView(Vtiger_Request $request) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();

		if(!$this->record){
		$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();
		$recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_DETAIL);
		$structuredValues = $recordStrucure->getStructure();

		$moduleModel = $recordModel->getModule();

		$viewer = $this->getViewer($request);
		$viewer->assign('RECORD', $recordModel);
		$viewer->assign('RECORD_STRUCTURE', $structuredValues);
		$viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
		$viewer->assign('MODULE', $moduleName);
		if($moduleName == 'Debts'){
			$db = PearDatabase::getInstance();
			$get_invoice_no = $db->pquery("SELECT cf_2667 FROM vtiger_debtscf WHERE debtsid = ?",array($recordId));
			$invoice_no = $db->query_result($get_invoice_no,0,'cf_2667');

			$get_invoice_type = $db->pquery("SELECT cf_1277 
																			 FROM vtiger_crmentityrel
																			 JOIN vtiger_invoicecf ON invoiceid=crmid
																			 WHERE relcrmid = ?",array($recordId));
			$invoice_type = $db->query_result($get_invoice_type,0,'cf_1277');

			$viewer->assign('KREDITINES', $invoice_no);
			$viewer->assign('INVOICE_TYPE', $invoice_type);
		}		
		$picklistDependencyDatasource = Vtiger_DependencyPicklist::getPicklistDependencyDatasource($moduleName);
		$viewer->assign('PICKIST_DEPENDENCY_DATASOURCE', Vtiger_Functions::jsonEncode($picklistDependencyDatasource));

		if ($request->get('displayMode') == 'overlay') {
			$viewer->assign('MODULE_MODEL', $moduleModel);
			$this->setModuleInfo($request, $moduleModel);
			$viewer->assign('SCRIPTS',$this->getOverlayHeaderScripts($request));

			$detailViewLinkParams = array('MODULE'=>$moduleName, 'RECORD'=>$recordId);
			$detailViewLinks = $this->record->getDetailViewLinks($detailViewLinkParams);
			$viewer->assign('DETAILVIEW_LINKS', $detailViewLinks);
			return $viewer->view('OverlayDetailView.tpl', $moduleName);
		} else {
			return $viewer->view('DetailViewFullContents.tpl', $moduleName, true);
		}
	}
	public function getOverlayHeaderScripts(Vtiger_Request $request){
		$moduleName = $request->getModule();
		$jsFileNames = array(
			"modules.$moduleName.resources.Detail",
		);
		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		return $jsScriptInstances;	
	}

	function showModuleSummaryView($request) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();

		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();
		$recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_SUMMARY);

		$moduleModel = $recordModel->getModule();
		$viewer = $this->getViewer($request);
		$viewer->assign('RECORD', $recordModel);
		$viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());

		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
		$viewer->assign('SUMMARY_RECORD_STRUCTURE', $recordStrucure->getStructure());
		$viewer->assign('RELATED_ACTIVITIES', $this->getActivities($request));

		$viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$pagingModel = new Vtiger_Paging_Model();
		$viewer->assign('PAGING_MODEL', $pagingModel);

		$picklistDependencyDatasource = Vtiger_DependencyPicklist::getPicklistDependencyDatasource($moduleName);
		$viewer->assign('PICKIST_DEPENDENCY_DATASOURCE', Vtiger_Functions::jsonEncode($picklistDependencyDatasource));

		return $viewer->view('ModuleSummaryView.tpl', $moduleName, true);
	}

	/**
	 * Function shows basic detail for the record
	 * @param <type> $request
	 */
	function showModuleBasicView($request) {

		$recordId = $request->get('record');
		$moduleName = $request->getModule();

		if(!$this->record){
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();

		$detailViewLinkParams = array('MODULE'=>$moduleName,'RECORD'=>$recordId);
		$detailViewLinks = $this->record->getDetailViewLinks($detailViewLinkParams);

		$viewer = $this->getViewer($request);
		$viewer->assign('RECORD', $recordModel);
		$viewer->assign('MODULE_SUMMARY', $this->showModuleSummaryView($request));

		$viewer->assign('DETAILVIEW_LINKS', $detailViewLinks);
		$viewer->assign('USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$viewer->assign('IS_AJAX_ENABLED', $this->isAjaxEnabled($recordModel));
		$viewer->assign('MODULE_NAME', $moduleName);

		$recordStrucure = Vtiger_RecordStructure_Model::getInstanceFromRecordModel($recordModel, Vtiger_RecordStructure_Model::RECORD_STRUCTURE_MODE_DETAIL);
		$structuredValues = $recordStrucure->getStructure();

		$moduleModel = $recordModel->getModule();
		$viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
		$viewer->assign('RECORD_STRUCTURE', $structuredValues);
		$viewer->assign('BLOCK_LIST', $moduleModel->getBlocks());
		echo $viewer->view('DetailViewSummaryContents.tpl', $moduleName, true);
	}

	/**
	 * Added to support Engagements view in Vtiger7
	 * @param Vtiger_Request $request
	 */
	function _showRecentActivities(Vtiger_Request $request){
		$parentRecordId = $request->get('record');
		$pageNumber = $request->get('page');
		$limit = $request->get('limit');
		$moduleName = $request->getModule();

		if(empty($pageNumber)) {
			$pageNumber = 1;
		}

		$pagingModel = new Vtiger_Paging_Model();
		$pagingModel->set('page', $pageNumber);
		if(!empty($limit)) {
			$pagingModel->set('limit', $limit);
		}

		$recentActivities = ModTracker_Record_Model::getUpdates($parentRecordId, $pagingModel,$moduleName);
		$pagingModel->calculatePageRange($recentActivities);

		if($pagingModel->getCurrentPage() == ModTracker_Record_Model::getTotalRecordCount($parentRecordId)/$pagingModel->getPageLimit()) {
			$pagingModel->set('nextPageExists', false);
		}
		$recordModel = Vtiger_Record_Model::getInstanceById($parentRecordId);
		$viewer = $this->getViewer($request);
		$viewer->assign('SOURCE',$recordModel->get('source'));
		$viewer->assign('RECENT_ACTIVITIES', $recentActivities);
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('PAGING_MODEL', $pagingModel);
		$viewer->assign('RECORD_ID',$parentRecordId);
	}

	/**
	 * Function returns recent changes made on the record
	 * @param Vtiger_Request $request
	 */
	function showRecentActivities (Vtiger_Request $request){
		$moduleName = $request->getModule();
		$this->_showRecentActivities($request);

		$viewer = $this->getViewer($request);
		echo $viewer->view('RecentActivities.tpl', $moduleName, true);
	}

	/**
	 * Function returns latest comments
	 * @param Vtiger_Request $request
	 * @return <type>
	 */
	function showRecentComments(Vtiger_Request $request) {
		$parentId = $request->get('record');
		$pageNumber = $request->get('page');
		$limit = $request->get('limit');
		$moduleName = $request->getModule();
		$currentUserModel = Users_Record_Model::getCurrentUserModel();

		if(empty($pageNumber)) {
			$pageNumber = 1;
		}

		$pagingModel = new Vtiger_Paging_Model();
		$pagingModel->set('page', $pageNumber);
		if(!empty($limit)) {
			$pagingModel->set('limit', $limit);
		}

		if($request->get('rollup-toggle')) {
			$rollupsettings = ModComments_Module_Model::storeRollupSettingsForUser($currentUserModel, $request);
		} else {
			$rollupsettings = ModComments_Module_Model::getRollupSettingsForUser($currentUserModel, $moduleName);
		}

		if($rollupsettings['rollup_status']) {
			$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentId, $moduleName);
			$recentComments = $parentRecordModel->getRollupCommentsForModule(0, 6);
		}else {
			$recentComments = ModComments_Record_Model::getRecentComments($parentId, $pagingModel);
		}

		$pagingModel->calculatePageRange($recentComments);
		if ($pagingModel->get('limit') < count($recentComments)) {
			array_pop($recentComments);
		}

		$modCommentsModel = Vtiger_Module_Model::getInstance('ModComments');
		$fileNameFieldModel = Vtiger_Field::getInstance("filename", $modCommentsModel);
		$fileFieldModel = Vtiger_Field_Model::getInstanceFromFieldObject($fileNameFieldModel);

		$viewer = $this->getViewer($request);
		$viewer->assign('COMMENTS', $recentComments);
		$viewer->assign('CURRENTUSER', $currentUserModel);
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('PAGING_MODEL', $pagingModel);
		$viewer->assign('FIELD_MODEL', $fileFieldModel);
		$viewer->assign('MAX_UPLOAD_LIMIT_MB', Vtiger_Util_Helper::getMaxUploadSize());
		$viewer->assign('MAX_UPLOAD_LIMIT_BYTES', Vtiger_Util_Helper::getMaxUploadSizeInBytes());
		$viewer->assign('COMMENTS_MODULE_MODEL', $modCommentsModel);
		$viewer->assign('ROLLUP_STATUS', $rollupsettings['rollup_status']);
		$viewer->assign('ROLLUPID', $rollupsettings['rollupid']);
		$viewer->assign('PARENT_RECORD', $parentId);

		return $viewer->view('RecentComments.tpl', $moduleName, 'true');
	}

	/**
	 * Function returns related records
	 * @param Vtiger_Request $request
	 * @return <type>
	 */
	function showRelatedList(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$relatedModuleName = $request->get('relatedModule');
		$targetControllerClass = null;

		if($relatedModuleName == 'ModComments') {
			$currentUserModel = Users_Record_Model::getCurrentUserModel();
			$rollupSettings = ModComments_Module_Model::getRollupSettingsForUser($currentUserModel, $moduleName);
			$request->set('rollup_settings', $rollupSettings);
		}

		// Added to support related list view from the related module, rather than the base module.
		try {
			$targetControllerClass = Vtiger_Loader::getComponentClassName('View', 'In'.$moduleName.'Relation', $relatedModuleName);
		}catch(AppException $e) {
			try {
				// If any module wants to have same view for all the relation, then invoke this.
				$targetControllerClass = Vtiger_Loader::getComponentClassName('View', 'InRelation', $relatedModuleName);
			}catch(AppException $e) {
				// Default related list
				$targetControllerClass = Vtiger_Loader::getComponentClassName('View', 'RelatedList', $moduleName);
			}
		}
		if($targetControllerClass) {
			$targetController = new $targetControllerClass();
			return $targetController->process($request);
		}
	}

	/**
	 * Function sends the child comments for a comment
	 * @param Vtiger_Request $request
	 * @return <type>
	 */
	function showChildComments(Vtiger_Request $request) {
		$parentCommentId = $request->get('commentid');
		$parentCommentModel = ModComments_Record_Model::getInstanceById($parentCommentId);
		$childComments = $parentCommentModel->getChildComments();
		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$modCommentsModel = Vtiger_Module_Model::getInstance('ModComments');

		$viewer = $this->getViewer($request);
		$viewer->assign('PARENT_COMMENTS', $childComments);
		$viewer->assign('CURRENTUSER', $currentUserModel);
		$viewer->assign('COMMENTS_MODULE_MODEL', $modCommentsModel);

		return $viewer->view('CommentsList.tpl', $moduleName, 'true');
	}

	/**
	 * Function to get Ajax is enabled or not
	 * @param Vtiger_Record_Model record model
	 * @return <boolean> true/false
	 */
	function isAjaxEnabled($recordModel) {
		if(is_null($this->isAjaxEnabled)){
			$this->isAjaxEnabled = $recordModel->isEditable();
		}
		return $this->isAjaxEnabled;
	}

	/**
	 * Function to get activities
	 * @param Vtiger_Request $request
	 * @return <List of activity models>
	 */
	public function getActivities(Vtiger_Request $request) {
		return '';
	}

	public function showLinesHistory(Vtiger_Request $request) {
		$parentId = $request->get('record');
		$db = PearDatabase::getInstance();

		$invoice = $db->pquery("SELECT DATE_FORMAT(createdtime, '%Y-%m-%d') AS created,  
																   DATE_FORMAT(modifiedtime, '%Y-%m-%d') AS modified 
												           FROM vtiger_crmentity WHERE crmid = ?", [$parentId]);

		$created = $db->query_result($invoice, 0, 'created');
		$modified = $db->query_result($invoice, 0, 'modified');

		$history = [];

		if(filesize("logs/create_invoice/$created/$parentId.log") > 0){
			$create_log = fopen("logs/create_invoice/$created/$parentId.log", "r");
			$create_log = fread($create_log,filesize("logs/create_invoice/$created/$parentId.log"));
			fclose($create_log);
			$history['new'] = $this->parseLog($create_log, 'New');
		}

		
		if(filesize("logs/edit_invoice/$modified/$parentId.log") > 0){
			$edit_log = fopen("logs/edit_invoice/$modified/$parentId.log", "r");
			$edit_log = fread($edit_log,filesize("logs/edit_invoice/$modified/$parentId.log"));
			fclose($edit_log);
			$history['edit'] = $this->parseLog($edit_log, 'Edit');
		}
	
		$taged_history = [];
		$taged_edit_history = [];

		$history_new = array_merge(...$history['new']);

		$first = 0;		

		if(count($history['edit']) > 0){		
			for($h = 0; $h < count($history['edit'][$first]['data']); $h++){		
				if(trim($history_new['data'][$h]['bill_address']) !=  trim($history['edit'][$first]['data'][$h]['bill_address'])){
					$taged_history[$history['edit'][$first]['time']][$h]['bill_address'] = true;
				}
				if(trim($history_new['data'][$h]['ship_address']) !=  trim($history['edit'][$first]['data'][$h]['ship_address'])){
					$taged_history[$history['edit'][$first]['time']][$h]['ship_address'] = true;
				}
				if($history_new['data'][$h]['type'] == 36641){
					if(trim($history_new['data'][$h]['service']) !=  trim($history['edit'][$first]['data'][$h]['service'])){
						$taged_history[$history['edit'][$first]['time']][$h]['service'] = true;
					}
				}
				if(trim($history_new['data'][$h]['note']) !=  trim($history['edit'][$first]['data'][$h]['note'])){
					$taged_history[$history['edit'][$first]['time']][$h]['note'] = true;
				}
				if(trim($history_new['data'][$h]['costcenter']) !=  trim($history['edit'][$first]['data'][$h]['costcenter'])){
					$taged_history[$history['edit'][$first]['time']][$h]['costcenter'] = true;
				}
				if(trim($history_new['data'][$h]['cargo_measure']) !=  trim($history['edit'][$first]['data'][$h]['cargo_measure'])){
					$taged_history[$history['edit'][$first]['time']][$h]['cargo_measure'] = true;
				}
				if(trim($history_new['data'][$h]['cargo_wgt']) !=  trim($history['edit'][$first]['data'][$h]['cargo_wgt'])){
					$taged_history[$history['edit'][$first]['time']][$h]['cargo_wgt'] = true;
				}
				if(trim($history_new['data'][$h]['cargo_volume']) !=  trim($history['edit'][$first]['data'][$h]['cargo_volume'])){
					$taged_history[$history['edit'][$first]['time']][$h]['cargo_volume'] = true;
				}
				if(trim($history_new['data'][$h]['price']) !=  trim($history['edit'][$first]['data'][$h]['price'])){
					$taged_history[$history['edit'][$first]['time']][$h]['price'] = true;
				}
			}
		}

		if(count($history['edit']) > 1){		
			$total_edit = count($history['edit']) -1;
			for($h = 0; $h < count($history['edit']); $h++){	

				for($e = 0; $e < count($history['edit'][$h]['data']); $e++){	
					$newest = ($h+1 <= $total_edit ? $h+1 : $h);	
									
					if(trim($history['edit'][$h]['data'][$e]['bill_address']) !=  trim($history['edit'][$newest]['data'][$e]['bill_address'])){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['bill_address'] = true;
					}
					if(trim($history['edit'][$h]['data'][$e]['ship_address']) !=  trim($history['edit'][$newest]['data'][$e]['ship_address'])){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['ship_address'] = true;
					}
					if($history['edit'][$h]['data'][$e]['type'] == '36641'){					
						if($history['edit'][$h]['data'][$e]['service'] != $history['edit'][$newest]['data'][$e]['service']){
							$taged_edit_history[$history['edit'][$newest]['time']][$e]['service'] = true;
						}
					}

					if($history['edit'][$newest]['data'][$e]['type'] == '36641'){	
						$plusOne = ($e == count($history['edit'][$h]['data'])-1 ? $e+1 : $e);	

						if(empty($history['edit'][$h]['data'][$plusOne]['service']) && !empty($history['edit'][$newest]['data'][$plusOne]['service'])){
							$taged_edit_history[$history['edit'][$newest]['time']][$plusOne]['service'] = true;
						}
					}

					if(trim($history['edit'][$h]['data'][$e]['note']) !=  trim($history['edit'][$newest]['data'][$e]['note'])){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['note'] = true;
					}
					if(trim($history['edit'][$h]['data'][$e]['costcenter']) !=  trim($history['edit'][$newest]['data'][$e]['costcenter'])){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['costcenter'] = true;
					}
					if(trim($history['edit'][$h]['data'][$e]['cargo_measure']) !=  trim($history['edit'][$newest]['data'][$e]['cargo_measure'])){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['cargo_measure'] = true;
					}
					if(trim($history['edit'][$h]['data'][$e]['cargo_wgt']) !=  trim($history['edit'][$newest]['data'][$e]['cargo_wgt'])){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['cargo_wgt'] = true;
					}
					if(trim($history['edit'][$h]['data'][$e]['cargo_volume']) !=  trim($history['edit'][$newest]['data'][$e]['cargo_volume'])){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['cargo_volume'] = true;
					}
					if($history['edit'][$h]['data'][$e]['price'] !=  $history['edit'][$newest]['data'][$e]['price']){
						$taged_edit_history[$history['edit'][$newest]['time']][$e]['price'] = true;
					}
				}
			}
		}


		rsort($history['edit']);

		$viewer = $this->getViewer($request);
		$viewer->assign('HISTORY', $history);
		$viewer->assign('HISTORY_TAGS', $taged_history);
		$viewer->assign('HISTORY_EDIT_TAGS', $taged_edit_history);

		return $viewer->view('showLinesHistory.tpl', 'Invoice',true);
	}



	function parseLog($log, $type){
		$db = PearDatabase::getInstance();
		$history = [];	
		$splited = explode('--------------------------------', $log);
		$pattern = '/\d{4}\-\d{2}\-\d{2}\ \d{2}\:\d{2}\:\d{2}/';

		$get_services = $db->query("SELECT * FROM app_services_type");
		$services = [];

		foreach($get_services AS $service){
			$services[$service['id']] = $service['name']; 
		}

		$get_cost_centers = $db->query("SELECT * FROM vtiger_costcenter INNER JOIN vtiger_crmentity ON crmid=costcenterid WHERE deleted = 0");
		$cost_centers = [];

		foreach($get_cost_centers AS $cost_center){
			$cost_centers[$cost_center['costcenterid']] = $cost_center['costcenter_tks_cost']; 
		}


		$count = 0;
		foreach ($splited as $key => $value) {
			if(!empty($value)){						
				if(preg_match($pattern, $value)){
					$history[$count]['time'] = $value;					
				}else{
					$lines = json_decode($value, true);		
	
					for ($i = 1; $i <= $lines['totalProductCount']; $i++) {		
						if(isset($lines["hdnProductId{$i}"])){
							$history[$count]['data'][] = ['type' => $lines["hdnProductId{$i}"],
																				'cargo' => $lines["productName{$i}"],
																				'service' => (isset($services[$lines["service_select{$i}"]]) ? $services[$lines["service_select{$i}"]] : ''),												'bill_address' => $lines["bill_address{$i}"],
																				'ship_address' => $lines["ship_address{$i}"],
																				'note' => $lines["note_field{$i}"],
																				'costcenter' => (isset($cost_centers[$lines["costcenter{$i}"]]) ? $cost_centers[$lines["costcenter{$i}"]] : ''),
																				'object' => $lines["object{$i}"],
																				'employee' => $lines["employee{$i}"],
																				'cargo_measure' => ($lines["cargo_measure_invoice{$i}"] > 0 ? $lines["cargo_measure_invoice{$i}"]: ''),
																				'cargo_wgt' => $lines["cargo_wgt{$i}"],
																				'cargo_volume' => $lines["cargo_volume{$i}"],
																				'price' => $lines["fix-price{$i}"]																
																			];
																				
						}else if(isset($lines["order_id_check{$i}"])){
							if($type == 'Edit'){
								$history[$count]['data'][] = ['type' => 36641,
																							'service' => 'Ištrinta eilutė',
																							'price' => 0
																						];
							}
						}	
						
						if($lines['services_count'.$i] > 0){
							for ($e = 1; $e <= $lines['services_count'.$i]; $e++) {		
								$history[$count]['data'][] = ['type' => $lines["hdnProductId{$i}-{$e}"],
																				'cargo' => $lines["productName{$i}-{$e}"],
																				'service' => (isset($services[$lines["service_select{$i}-{$e}"]]) ? $services[$lines["service_select{$i}-{$e}"]] : ''),'bill_address' => '',
																				'ship_address' => '',
																				'note' => $lines["note_field{$i}-{$e}"],
																				'costcenter' => (isset($cost_centers[$lines["costcenter{$i}-{$e}"]]) ? $cost_centers[$lines["costcenter{$i}-{$e}"]] : ''),
																				'object' => $lines["object{$i}-{$e}"],
																				'employee' => $lines["employee{$i}-{$e}"],
																				'cargo_measure' => '',
																				'cargo_wgt' => '',
																				'cargo_volume' => '',
																				'price' => $lines["fix-price{$i}-{$e}"]																
																			];
							}
						}
					}
				}

				if ($key % 2 == 0) $count ++;

			}				
		}
		return $history;
	}



	/**
	 * Function returns related records based on related moduleName
	 * @param Vtiger_Request $request
	 * @return <type>
	 */
	function showRelatedRecords(Vtiger_Request $request) {
		$parentId = $request->get('record');
		$pageNumber = $request->get('page');
		$limit = $request->get('limit');
		$relatedModuleName = $request->get('relatedModule');
		$moduleName = $request->getModule();

		if(empty($pageNumber)) {
			$pageNumber = 1;
		}

		$pagingModel = new Vtiger_Paging_Model();
		$pagingModel->set('page', $pageNumber);
		if(!empty($limit)) {
			$pagingModel->set('limit', $limit);
		}

		$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentId, $moduleName);
		$relationListView = Vtiger_RelationListView_Model::getInstance($parentRecordModel, $relatedModuleName);
		$models = $relationListView->getEntries($pagingModel);
		$header = $relationListView->getHeaders();

		$viewer = $this->getViewer($request);
		$viewer->assign('MODULE' , $moduleName);
		$viewer->assign('RELATED_RECORDS' , $models);
		$viewer->assign('RELATED_HEADERS', $header);
		$viewer->assign('RELATED_MODULE' , $relatedModuleName);
		$viewer->assign('PAGING_MODEL', $pagingModel);

		return $viewer->view('SummaryWidgets.tpl', $moduleName, 'true');
	}

	public function getHeaderCss(Vtiger_Request $request) {
		$headerCssInstances = parent::getHeaderCss($request);
		$cssFileNames = array(
			'~/libraries/jquery/bootstrapswitch/css/bootstrap2/bootstrap-switch.min.css',
		);
		$cssInstances = $this->checkAndConvertCssStyles($cssFileNames);
		$headerCssInstances = array_merge($headerCssInstances, $cssInstances);
		return $headerCssInstances;
	}

	// NOTE dublicate functions

 function changeCrmentityId($module,$crmid,$new_crmid,$adb){
		$adb->pquery("UPDATE vtiger_crmentity SET crmid = ?, label = '' WHERE crmid = ? AND setype = ?",array($new_crmid,$crmid,$module));  
 }
 
 function changeCrmentityRelId($relmodule,$crmid,$new_crmid,$adb){
		$adb->pquery("UPDATE vtiger_crmentityrel SET crmid = ? WHERE crmid = ? AND module = ?",array($new_crmid,$crmid,$relmodule));
 }
 
 function changeDebtId($paymentid,$new_paymentid,$adb){
		$adb->pquery("UPDATE vtiger_payment SET paymentid = ? WHERE paymentid = ?",array($new_paymentid,$paymentid));  
 
		$adb->pquery("UPDATE vtiger_paymentcf SET paymentid = ? WHERE paymentid = ?",array($new_paymentid,$paymentid));
 }
 
 function changeRoutePoint($routepointsid,$new_routepointsid,$adb){
		$adb->pquery("UPDATE vtiger_routepoints SET routepointsid = ? WHERE routepointsid = ?",array($new_routepointsid,$routepointsid));

		$adb->pquery("UPDATE vtiger_routepointscf SET routepointsid = ? WHERE routepointsid = ?",array($new_routepointsid,$routepointsid));
 }

 function changeHelpDesk($ticketid,$new_ticketid,$dbh){
		$update = $dbh->prepare("UPDATE vtiger_troubletickets SET ticketid = ? WHERE ticketid = ?");
		$update->execute(array($new_ticketid,$ticketid));   

		$update_paymentcf = $dbh->prepare("UPDATE vtiger_ticketcf SET ticketid = ? WHERE ticketid = ?");
		$update_paymentcf->execute(array($ticketid,$ticketid)); 
 }
 
 function changeSalesOrderId($salesorderid,$new_salesorderid,$adb){
	  $adb->pquery("UPDATE vtiger_salesorder SET salesorderid = ? WHERE salesorderid = ?",array($new_salesorderid,$salesorderid));	
 
	  $adb->pquery("UPDATE vtiger_salesordercf SET salesorderid = ? WHERE salesorderid = ?",array($new_salesorderid,$salesorderid));  
 
	  $adb->pquery("UPDATE vtiger_sobillads SET sobilladdressid = ? WHERE sobilladdressid = ?",array($new_salesorderid,$salesorderid));
 
	  $adb->pquery("UPDATE vtiger_soshipads SET soshipaddressid = ? WHERE soshipaddressid = ?",array($new_salesorderid,$salesorderid));      
 }
 
 function changeInventoryProductRelId($id,$new_id,$adb){
	 $adb->pquery("UPDATE vtiger_inventoryproductrel SET id = ? WHERE id = ?",array($new_id,$id));  
 }
 
 function changeAccountId($accountid,$new_accountid,$adb){
		$adb->pquery("UPDATE vtiger_account SET accountid = ? WHERE accountid = ?",array($new_accountid,$accountid));
 
		$adb->pquery("UPDATE vtiger_accountscf SET accountid = ? WHERE accountid = ?",array($new_accountid,$accountid));  
 }
 
 function changeVendorId($vendorid,$new_vendorid,$adb){
		$adb->pquery("UPDATE vtiger_vendor SET vendorid = ? WHERE vendorid = ?",array($new_vendorid,$vendorid));
 
		$adb->pquery("UPDATE vtiger_vendorcf SET vendorid = ? WHERE vendorid = ?",array($new_vendorid,$vendorid));    
 }
 
 
 function changeDocumentsId($notesid,$new_notesid,$adb){
		$adb->pquery("UPDATE vtiger_notes  SET notesid = ? WHERE notesid = ?",array($notesid, $new_notesid));
 
		$adb->pquery("UPDATE vtiger_notescf SET notesid = ? WHERE notesid = ?",array($notesid, $new_notesid));
 
		$adb->pquery("UPDATE vtiger_senotesrel SET notesid = ? WHERE notesid = ?",array($notesid, $new_notesid));
	  
		$adb->pquery("UPDATE vtiger_seattachmentsrel SET crmid = ? WHERE crmid = ?", array($notesid, $new_notesid));

 }
 
 function changeDocumentsAttachmentId($attachmentsid,$new_attachmentsid,$adb){
		$adb->pquery("UPDATE vtiger_attachments  SET attachmentsid = ? WHERE attachmentsid = ?",array($attachmentsid, $new_attachmentsid));
 
		$adb->pquery("UPDATE vtiger_seattachmentsrel SET attachmentsid = ? WHERE attachmentsid = ?",array($attachmentsid, $new_attachmentsid));  
 }
 
 function changePurchaseOrderDraftId($purchaseorderdraftid,$new_purchaseorderdraftid,$adb){
		$adb->pquery("UPDATE vtiger_purchaseorderdraft  SET purchaseorderdraftid = ? WHERE purchaseorderdraftid = ?",array($purchaseorderdraftid, $new_purchaseorderdraftid));
 
		$adb->pquery("UPDATE vtiger_purchaseorderdraftcf SET attachmentsid = ? WHERE attachmentsid = ?",array($purchaseorderdraftid, $new_purchaseorderdraftid));
 }
}
