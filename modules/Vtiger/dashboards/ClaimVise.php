<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_ClaimVise_Dashboard extends Vtiger_IndexAjax_View {

	public function process(Vtiger_Request $request) {	
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();

		$linkId = $request->get('linkid');
		$currentUser = Users_Record_Model::getCurrentUserModel();

		if(empty($page)) {
			$page = 1;
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
	
		$widget = Vtiger_Widget_Model::getInstance($linkId, $currentUser->getId());
		$getClaims = $moduleModel->getClaimVise();

		$viewer->assign('WIDGET', $widget);
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('CLAIMS_VISE', $getClaims);

		$content = $request->get('content');
		if(!empty($content)) {
			$viewer->view('dashboards/ClaimViseContent.tpl', $moduleName);
		} else {
			$viewer->view('dashboards/ClaimVise.tpl', $moduleName);
		}
	
	}
}
