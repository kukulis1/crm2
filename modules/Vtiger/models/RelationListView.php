<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_RelationListView_Model extends Vtiger_Base_Model {

	protected $relationModel = false;
	protected $parentRecordModel = false;
	protected $relatedModuleModel = false;

	public function setRelationModel($relation){
		$this->relationModel = $relation;
		return $this;
	}

	public function getRelationModel() {
		return $this->relationModel;
	}

	public function setParentRecordModel($parentRecord){
		$this->parentRecordModel = $parentRecord;
		return $this;
	}

	public function getParentRecordModel(){
		return $this->parentRecordModel;
	}

	public function setRelatedModuleModel($relatedModuleModel){
		$this->relatedModuleModel = $relatedModuleModel;
		return $this;
	}

	public function getRelatedModuleModel(){
		return $this->relatedModuleModel;
	}

	public function getCreateViewUrl(){
		$relationModel = $this->getRelationModel();
		$relatedModel = $relationModel->getRelationModuleModel();
		$parentRecordModule = $this->getParentRecordModel();
		$parentModule = $parentRecordModule->getModule();

		$createViewUrl = $relatedModel->getCreateRecordUrl().'&returnmode=showRelatedList&returntab_label='.$this->tab_label.
							'&returnrecord='.$parentRecordModule->getId().'&returnmodule='.$parentModule->getName().
							'&returnview=Detail&returnrelatedModuleName='.$this->getRelatedModuleModel()->getName().
							'&returnrelationId='.$relationModel->getId();

		if(in_array($relatedModel->getName(), getInventoryModules())){
			$createViewUrl.='&relationOperation=true';
		}
		//To keep the reference fieldname and record value in the url if it is direct relation
		if($relationModel->isDirectRelation()) {
			$relationField = $relationModel->getRelationField();
			$createViewUrl .='&'.$relationField->getName().'='.$parentRecordModule->getId();
		}

		//if parent module has auto fill data it should be automatically filled
		$autoFillData = $parentModule->getAutoFillModuleAndField($parentModule->getName());
		$relatedAutoFillData = $relatedModel->getAutoFillModuleAndField($parentModule->getName());

		if($autoFillData) {
			//There can be more than one auto-filled field.
			foreach ($autoFillData as $autoFilledField){
				$parentAutoFillField  = $autoFilledField['fieldname'];
				$parentAutoFillModule = $autoFilledField['module'];
				if($parentRecordModule->get($parentAutoFillField)) {
					if($relatedAutoFillData){
						foreach ($relatedAutoFillData as $relatedAutoFilledField){
							$relatedAutoFillFieldName = $relatedAutoFilledField['fieldname'];
							$relatedAutoFillModuleName = $relatedAutoFilledField['module'];
							if($parentAutoFillModule === $relatedAutoFillModuleName) {
								$createViewUrl .= '&'.$relatedAutoFillFieldName.'='.$parentRecordModule->get($parentAutoFillField);
							}
						}
					}
				}
			}
		}

		return $createViewUrl;
	}

	public function getCreateEventRecordUrl(){
		$relationModel = $this->getRelationModel();
		$relatedModel = $relationModel->getRelationModuleModel();
		$parentRecordModule = $this->getParentRecordModel();
		$parentModule = $parentRecordModule->getModule();

		$createViewUrl = $relatedModel->getCreateEventRecordUrl().'&returnmode=showRelatedList&returntab_label='.$relationModel->get('label').
							'&returnrecord='.$parentRecordModule->getId().'&returnmodule='.$parentModule->get('name').
							'&returnview=Detail&returnrelatedModuleName=Calendar'.
							'&returnrelationId='.$relationModel->getId();
		//To keep the reference fieldname and record value in the url if it is direct relation
		if($relationModel->isDirectRelation()) {
			$relationField = $relationModel->getRelationField();
			$createViewUrl .='&'.$relationField->getName().'='.$parentRecordModule->getId();
		}
		return $createViewUrl;
	}

	public function getCreateTaskRecordUrl(){
		$relationModel = $this->getRelationModel();
		$relatedModel = $relationModel->getRelationModuleModel();
		$parentRecordModule = $this->getParentRecordModel();
		$parentModule = $parentRecordModule->getModule();

		$createViewUrl = $relatedModel->getCreateTaskRecordUrl().'&returnmode=showRelatedList&returntab_label='.$relationModel->get('label').
							'&returnrecord='.$parentRecordModule->getId().'&returnmodule='.$parentModule->get('name').
							'&returnview=Detail&returnrelatedModuleName=Calendar'.
							'&returnrelationId='.$relationModel->getId();

		//To keep the reference fieldname and record value in the url if it is direct relation
		if($relationModel->isDirectRelation()) {
			$relationField = $relationModel->getRelationField();
			$createViewUrl .='&'.$relationField->getName().'='.$parentRecordModule->getId();
		}
		return $createViewUrl;
	}

	public function getLinks(){
		$relationModel = $this->getRelationModel();
		$actions = $relationModel->getActions();

		$selectLinks = $this->getSelectRelationLinks();
		foreach($selectLinks as $selectLinkModel) {
			$selectLinkModel->set('_selectRelation',true)->set('_module',$relationModel->getRelationModuleModel());
		}
		$addLinks = $this->getAddRelationLinks();

		$links = array_merge($selectLinks, $addLinks);
		$relatedLink = array();
		$relatedLink['LISTVIEWBASIC'] = $links;
		return $relatedLink;
	}

	public function getSelectRelationLinks() {
		$relationModel = $this->getRelationModel();
		$selectLinkModel = array();

		if(!$relationModel->isSelectActionSupported()) {
			return $selectLinkModel;
		}

		$relatedModel = $relationModel->getRelationModuleModel();

		$selectLinkList = array(
			array(
				'linktype' => 'LISTVIEWBASIC',
				'linklabel' => vtranslate('LBL_SELECT')." ".vtranslate('SINGLE_'.$relatedModel->getName(), $relatedModel->getName()),
				'linkurl' => '',
				'linkicon' => '',
				'linkmodule' => $relatedModel->getName(),
			)
		);


		foreach($selectLinkList as $selectLink) {
			$selectLinkModel[] = Vtiger_Link_Model::getInstanceFromValues($selectLink);
		}
		return $selectLinkModel;
	}

	public function getAddRelationLinks() {
		$relationModel = $this->getRelationModel();
		$addLinkModel = array();


		// itoma begin
		$db = PearDatabase::getInstance();
		$moduleName = $_REQUEST['module'];
		$recordId = $_REQUEST['record'];
		$relatedModule = $_REQUEST['relatedModule'];
		$num_row = 1;

		if($moduleName == 'PurchaseOrder' && $relatedModule == 'Purchasevise'){
			$query = "SELECT * FROM vtiger_crmentityrel WHERE crmid = ? AND relmodule = ? ";
			$checkVise = $db->pquery($query, array($recordId,$relatedModule));
			$num_row = $db->num_rows($checkVise);	
		}

		if($moduleName == 'Claims' && $relatedModule == 'PurchaseOrder'){
			$query = "SELECT * FROM vtiger_claims_vise WHERE claim_id = ? AND ceo = 1";
			$checkVise = $db->pquery($query, array($recordId));
			$num_row = $db->num_rows($checkVise);	
		}

		if($relatedModule == 'Autoinvoicingrules'){
			$query3 = "SELECT autoinvoicingrulesid FROM crm.vtiger_autoinvoicingrules r
																							JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
																							JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
																							WHERE deleted = 0 AND rel.crmid = ?
																							GROUP BY relcrmid";			

			$checkRule = $db->pquery($query3, array($recordId));
			$num_row2 = $db->num_rows($checkRule);			
		}




		// itoma end
		
		
		if(!$relationModel->isAddActionSupported()) {
			return $addLinkModel;
		}
		$relatedModel = $relationModel->getRelationModuleModel();

		if($relatedModel->get('label') == 'Calendar'){
			if($relatedModel->isPermitted('CreateView')) {
				$addLinkList[] = array(
					'linktype' => 'LISTVIEWBASIC',
					'linklabel' => vtranslate('LBL_ADD_EVENT'),
					'linkurl' => $this->getCreateEventRecordUrl(),
					'linkicon' => '',
						'_linklabel' => '_add_event'// used in relatedlist.tpl to identify module to open quickcreate popup
				);
				$addLinkList[] = array(
					'linktype' => 'LISTVIEWBASIC',
					'linklabel' => vtranslate('LBL_ADD_TASK'),
					'linkurl' => $this->getCreateTaskRecordUrl(),
					'linkicon' => '',
					'_linklabel' => '_add_task'
				);
			}
		} else if ($relatedModel->get('label') == 'Documents') {
			$parentRecordModule = $this->getParentRecordModel();
			$parentModule = $parentRecordModule->getModule();
			$relationParameters = '&sourceModule='.$parentModule->get('name').'&sourceRecord='.$parentRecordModule->getId().'&relationOperation=true';

			if($relationModel->isDirectRelation()) {
				$relationField = $relationModel->getRelationField();
				$relationParameters .='&'.$relationField->getName().'='.$parentRecordModule->getId();
			}
			$vtigerDocumentTypes = array(
				array(
					'type' => 'I',
					'label' => 'LBL_INTERNAL_DOCUMENT_TYPE',
					'url' => 'index.php?module=Documents&view=EditAjax&type=I'.$relationParameters
				),
				array(
					'type' => 'E',
					'label' => 'LBL_EXTERNAL_DOCUMENT_TYPE',
					'url' => 'index.php?module=Documents&view=EditAjax&type=E'.$relationParameters
				),
				array(
					'type' => 'W',
					'label' => 'LBL_WEBDOCUMENT_TYPE',
					'url' => 'index.php?module=Documents&view=EditAjax&type=W'.$relationParameters
				)
			);
			$addLinkList[] = array(
				'linktype' => 'LISTVIEWBASIC',
				'linklabel' => 'Vtiger',
				'linkurl' => $this->getCreateViewUrl(),
				'linkicon' => 'Vtiger.png',
				'linkdropdowns' => $vtigerDocumentTypes,
				'linkclass' => 'addDocumentToVtiger',
			);

		}else{
	
				if (Users_Privileges_Model::isPermitted($relatedModel->getName(), 'CreateView')) {

					if(($moduleName != 'PurchaseOrder' AND $relatedModule != 'Purchasevise') AND ($moduleName != 'Claims' AND $relatedModule != 'PurchaseOrder') AND ($moduleName != 'Accounts' AND $relatedModule != 'Autoinvoicingrules')){
						$addLinkList = array(
							array(
								'linktype' => 'LISTVIEWBASIC',
								// NOTE: $relatedModel->get('label') assuming it to be a module name - we need singular label for Add action.
								'linklabel' => vtranslate('LBL_ADD')." ".vtranslate('SINGLE_'.$relatedModel->getName(), $relatedModel->getName()),
								'linkurl' => $this->getCreateViewUrl(),
								'linkicon' => '',
							)
						);						
					}else if($moduleName == 'PurchaseOrder' AND $relatedModule == 'Purchasevise' AND !$num_row){
						$addLinkList = array(
							array(
								'linktype' => 'LISTVIEWBASIC',
								// NOTE: $relatedModel->get('label') assuming it to be a module name - we need singular label for Add action.
								'linklabel' => vtranslate('LBL_ADD')." ".vtranslate('SINGLE_'.$relatedModel->getName(), $relatedModel->getName()),
								'linkurl' => $this->getCreateViewUrl(),
								'linkicon' => '',
							)
						);
					}else if($moduleName == 'Claims' AND $relatedModule == 'Purchasevise'  AND !$num_row){
					}else if($moduleName == 'Accounts' AND $relatedModule == 'Autoinvoicingrules'  AND $num_row2){							
						$addLinkList = array(
							array(
								'linktype' => 'LISTVIEWBASIC',
								// NOTE: $relatedModel->get('label') assuming it to be a module name - we need singular label for Add action.
								'linklabel' => 'Pridėti pasikrovimo adresą',
								'linkurl' => $this->getCreateViewUrl(),
								'linkicon' => '',
								'linkclass' => 'customModal',
								'modalId' => '#load_address',
							),
							array(
								'linktype' => 'LISTVIEWBASIC',
								// NOTE: $relatedModel->get('label') assuming it to be a module name - we need singular label for Add action.
								'linklabel' => 'Pridėti išsikrovimo adresą',
								'linkurl' => $this->getCreateViewUrl(),
								'linkicon' => '',
								'linkclass' => 'customModal',
								'modalId' => '#unload_address',
							),
							array(
								'linktype' => 'LISTVIEWBASIC',
								// NOTE: $relatedModel->get('label') assuming it to be a module name - we need singular label for Add action.
								'linklabel' => 'Pridėti darbuotoją',
								'linkurl' => $this->getCreateViewUrl(),
								'linkicon' => '',
								'linkclass' => 'customModal',
								'modalId' => '#employee',
							)
						);				
					}else if($num_row){
						$addLinkList = array(
							array(
								'linktype' => 'LISTVIEWBASIC',
								// NOTE: $relatedModel->get('label') assuming it to be a module name - we need singular label for Add action.
								'linklabel' => vtranslate('LBL_ADD')." ".vtranslate('SINGLE_'.$relatedModel->getName(), $relatedModel->getName()),
								'linkurl' => $this->getCreateViewUrl(),
								'linkicon' => '',
							)
						);
					}
				}
			
		}



		foreach($addLinkList as $addLink) {
			$addLinkModel[] = Vtiger_Link_Model::getInstanceFromValues($addLink);
		}
		return $addLinkModel;
	}


	public function getAutoInvoicingRules($accountid){
		$db = PearDatabase::getInstance();

		$stmt = $db->pquery("SELECT * FROM app_rules_load_address WHERE accountid = ?", array($accountid));
		$stmt2 = $db->pquery("SELECT * FROM app_rules_unload_address WHERE accountid = ?", array($accountid));
		$stmt3 = $db->pquery("SELECT * FROM app_rules_employee WHERE accountid = ?", array($accountid));
		
		$rules_array = array();

		foreach($stmt as $row){
			$rules_array['load'][] = $row;
		}

		foreach($stmt2 as $row){
			$rules_array['unload'][] = $row;
		}

		foreach($stmt3 as $row){
			$rules_array['employee'][] = $row;
		}

		return $rules_array;
	}

	public function getAutoInvoicingHistoryAddress($accountid){
		$db = PearDatabase::getInstance();

		$stmt = $db->pquery("SELECT DISTINCT REPLACE(bill_street,',','') AS bill_street, bill_city, bill_code, CONCAT(REPLACE(bill_street,',',''),'|##|',bill_city) AS address
																	FROM vtiger_salesorder
																	LEFT JOIN vtiger_sobillads ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid                     
																	WHERE vtiger_salesorder.accountid = ? AND bill_street IS NOT NULL AND bill_code != '' AND length(bill_street) > 4
																--	GROUP BY bill_street,bill_city
																	ORDER BY bill_street", array($accountid));

														
		$stmt2 = $db->pquery("SELECT DISTINCT REPLACE(ship_street,',','') AS ship_street, ship_city, ship_code, CONCAT(REPLACE(ship_street,',',''),'|##|',ship_city) AS address
																	FROM vtiger_salesorder
																	LEFT JOIN vtiger_soshipads ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid                     
																	WHERE vtiger_salesorder.accountid = ? AND ship_street IS NOT NULL AND ship_code != '' AND length(ship_street) > 4
																--	GROUP BY ship_street,ship_city
																	ORDER BY ship_street", array($accountid));


		$history_array = array();

		foreach($stmt as $row){
			$history_array['load'][] = array('street' => $row['bill_street'], 'city' => $row['bill_city'], 'code' => $row['bill_code'], 'address' => $row['address']);
		}	
		
		foreach($stmt2 as $row){
			$history_array['unload'][] = array('street' => $row['ship_street'], 'city' => $row['ship_city'], 'code' => $row['ship_code'], 'address' => $row['address']);
		}
		
		return $history_array;
	}

	public function getAutoInvoicingEmployees($accountid){
		$db = PearDatabase::getInstance();

		$stmt = $db->pquery("SELECT created_person AS employee
													FROM vtiger_salesorder s	
													INNER JOIN vtiger_crmentity e ON e.crmid=s.salesorderid	
													WHERE e.deleted = 0 AND s.accountid = ?	AND (created_person IS NOT NULL AND created_person != '')	
													GROUP BY created_person
													ORDER BY created_person", array($accountid));

		$employee_array = array();

		foreach($stmt as $row){
			$employee_array[] = $row['employee'];
		}	

		return $employee_array;
	}

	public function checkAreAutoInvoicingRuleIsCreate($accountid){
		$db = PearDatabase::getInstance();

		$stmt = $db->pquery("SELECT autoinvoicingrulesid FROM vtiger_autoinvoicingrules r
												JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
												JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
												WHERE deleted = 0 AND rel.crmid = ?
												GROUP BY relcrmid",array($accountid));
		$num_row = $db->num_rows($stmt);
		
		return $num_row;
	}

	// itoma
	public function getDeliveryPlan(){
		$db = PearDatabase::getInstance();
		$parentRecordId = $this->getParentRecordModel()->getId();
		$order_id = $db->pquery("SELECT external_order_id FROM vtiger_salesorder WHERE salesorderid = ?",array($parentRecordId));
		$external_order_id = $db->query_result($order_id,0,'external_order_id');
    $ch = curl_init();
		$headers = array(
			'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
			);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/shipment_plan");              
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('shipment_id' => $external_order_id));		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);
		return $data;
	}

	public function getDeliveryPlanForClaim(){
		$db = PearDatabase::getInstance();
		$parentRecordId = $this->getParentRecordModel()->getId();
		$order_id = $db->pquery("SELECT external_order_id FROM vtiger_salesorder
														 LEFT JOIN vtiger_claims ON vtiger_claims.claims_tks_order_number=vtiger_salesorder.salesorderid
														 WHERE claimsid = ?",array($parentRecordId));
		$external_order_id = $db->query_result($order_id,0,'external_order_id');
    $ch = curl_init();
		$headers = array(
			'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
			);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/shipment_plan");              
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('shipment_id' => $external_order_id));		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);
		return $data;
	}

	public function getDamagesFromTerminal(){
		$db = PearDatabase::getInstance();
		$parentRecordId = $this->getParentRecordModel()->getId();
		$order_id = $db->pquery("SELECT external_order_id FROM vtiger_salesorder
															LEFT JOIN vtiger_claims ON vtiger_claims.claims_tks_order_number=vtiger_salesorder.salesorderid
															WHERE claimsid = ?",array($parentRecordId));
		$external_order_id = $db->query_result($order_id,0,'external_order_id');
    $ch = curl_init();
		$headers = array(
			'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => '*', 'table' => 'SHIPMENTS_PACKAGES_DAMAGES_View', 'where' => "SHIPMENT_ID = $external_order_id"));		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);	
		return $data;
	}

	public function getAllDriversFromShipment(){
		$db = PearDatabase::getInstance();
		$parentRecordId = $this->getParentRecordModel()->getId();
		$order_id = $db->pquery("SELECT external_order_id FROM vtiger_salesorder
															LEFT JOIN vtiger_claims ON vtiger_claims.claims_tks_order_number=vtiger_salesorder.salesorderid
															WHERE claimsid = ?",array($parentRecordId));
		$external_order_id = $db->query_result($order_id,0,'external_order_id');
    $ch = curl_init();
		$headers = array(
			'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => '*', 'table' => 'ROUTE_SHIPMENTS_DRIVERS_View', 'where' => "SHIPMENT_ID = $external_order_id"));		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);	
		return $data;
	}


	public function getEntries($pagingModel) {
		$db = PearDatabase::getInstance();
		$parentModule = $this->getParentRecordModel()->getModule();
		$parentRecordId = $this->getParentRecordModel()->getId();
		$relationModule = $this->getRelationModel()->getRelationModuleModel();
		$relationModuleName = $relationModule->get('name');
		$relatedColumnFields = $relationModule->getConfigureRelatedListFields();
		if(count($relatedColumnFields) <= 0){
			$relatedColumnFields = $relationModule->getRelatedListFields();
		}

		if($relationModuleName == 'Calendar') {
			//Adding visibility in the related list, showing records based on the visibility
			$relatedColumnFields['visibility'] = 'visibility';
		}

		if($relationModuleName == 'PriceBooks') {
			//Adding fields in the related list
			$relatedColumnFields['unit_price'] = 'unit_price';
			$relatedColumnFields['listprice'] = 'listprice';
			$relatedColumnFields['currency_id'] = 'currency_id';
		}

		$query = $this->getRelationQuery();

		if ($this->get('whereCondition') && is_array($this->get('whereCondition'))) {
			$currentUser = Users_Record_Model::getCurrentUserModel();
			$queryGenerator = new QueryGenerator($relationModuleName, $currentUser);
			$queryGenerator->setFields(array_values($relatedColumnFields));
			$whereCondition = $this->get('whereCondition');
			foreach ($whereCondition as $fieldName => $fieldValue) {
				if (is_array($fieldValue)) {
					$comparator = $fieldValue[1];
					$searchValue = $fieldValue[2];
					$type = $fieldValue[3];
					if ($type == 'time') {
						$searchValue = Vtiger_Time_UIType::getTimeValueWithSeconds($searchValue);
					}
					$queryGenerator->addCondition($fieldName, $searchValue, $comparator, "AND");
				}
			}
			$whereQuerySplit = split("WHERE", $queryGenerator->getWhereClause());
			$query.=" AND " . $whereQuerySplit[1];
		}

		$startIndex = $pagingModel->getStartIndex();
		$pageLimit = $pagingModel->getPageLimit();

		$orderBy = $this->getForSql('orderby');
		$sortOrder = $this->getForSql('sortorder');


		// Itoma begin
		if($relationModuleName == 'Invoice' && $parentModule->get('name') == 'SalesOrder'){
			$substr = 'FROM vtiger_invoice';
			$substr2 = 'inner join vtiger_salesorder on vtiger_salesorder.salesorderid=vtiger_invoice.salesorderid';

			$attachment = ' LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.invoiceid=vtiger_invoice.invoiceid 
											inner join vtiger_salesorder on vtiger_salesorder.salesorderid = vtiger_invoice_salesorders_list.salesorderid ';
			$query = str_replace($substr2, '', $query);
			$query = str_replace($substr, $substr.$attachment, $query);
		}

		if($parentModule->get('name') == 'Stevedoring' AND $relationModuleName = 'SalesOrder'){
			$str = 'INNER JOIN vtiger_stevedoring AS vtiger_stevedoringStevedoring ON vtiger_stevedoringStevedoring.stevedoringid = vtiger_stevedoring.stevedoringno';

			$attachment = ' INNER JOIN vtiger_stevedoring AS vtiger_stevedoringStevedoring ON vtiger_stevedoringStevedoring.stevedoringno = vtiger_salesorder.salesorderid ';
			$query = str_replace($str, $attachment, $query);
		}

		if($relationModuleName == 'Accounts' && $parentModule->get('name') == 'Claims'){
			$substr = 'vtiger_claimsClaims.claimsid = vtiger_claims.cf_20023';
			$attachment = 'vtiger_claimsClaims.cf_20023 = vtiger_account.accountid';
			$query = str_replace($substr, $attachment, $query);
		}

		if($relationModuleName == 'Kokybe' && $parentModule->get('name') == 'Claims'){
			$substr = " vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claimsid = vtiger_claims.claims_tks_order_number";
			$attachment = ' vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claims_tks_order_number=vtiger_kokybe.kokybeno';
			$query = str_replace($substr, $attachment, $query);
		}

		if($relationModuleName == 'Documents' && $parentModule->get('name') == 'Claims'){
			$substr = "INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claimsid = vtiger_claims.claims_tks_order_number";
			$attachment = ' LEFT JOIN vtiger_senotesrel ON vtiger_senotesrel.notesid=vtiger_notes.notesid
			INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claims_tks_order_number = vtiger_senotesrel.crmid';
			$query = str_replace($substr, $attachment, $query);
		}	

		if($relationModuleName == 'Invoice' && $parentModule->get('name') == 'Claims'){
			$substr = "INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claimsid = vtiger_claims.claims_tks_order_number";
			$attachment = ' LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.invoiceid=vtiger_invoice.invoiceid
			INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claims_tks_order_number=vtiger_invoice_salesorders_list.salesorderid';
			$query = str_replace($substr, $attachment, $query);
		}			
		


		 if($relationModuleName == 'Invoice' && $parentModule->get('name') == 'Accounts' ){
				$substr = 'SELECT DISTINCT';
				$substr2 = 'vtiger_invoicecf.cf_1708,';
				$substr3 = 'INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_invoice.invoiceid';
				$attachment = ' 
			
			CASE 			
					WHEN  vtiger_invoice.duedate > CURDATE() THEN DATEDIFF(vtiger_invoice.duedate, CURDATE())
					ELSE 0
			END  as cf_1710,
            
             CASE WHEN debts_tks_data  THEN
				CASE 
					WHEN  vtiger_invoice.duedate < max(debts_tks_data) THEN CONCAT("+",DATEDIFF( max(debts_tks_data), vtiger_invoice.duedate))
					WHEN  vtiger_invoice.duedate > max(debts_tks_data) THEN CONCAT("-",DATEDIFF(vtiger_invoice.duedate,  max(debts_tks_data)))
					ELSE 0
                 END  
             ELSE "---"	
             END AS cf_1708,	';

				$substr4 = 'vtiger_invoicecf.cf_1710,';

				$attachment2 = '  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
				LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid ';

				$attachment3 = '';

				$query = str_replace($substr, $substr.$attachment, $query);										
				$query = str_replace($substr2, '', $query);										
				$query = str_replace($substr3, $substr3.$attachment2, $query);	
				$query = str_replace($substr4, '', $query);	
				$query .= " GROUP BY vtiger_invoice.invoiceid ORDER BY vtiger_invoice.invoiceid DESC";		
										
		 }

		 if($relationModuleName == 'SalesOrder' && $parentModule->get('name') == 'Accounts' ){
				$substr = 'SELECT DISTINCT';
				$substr2 = 'WHERE';

				$attachment = " CONCAT( ROUND(inv.quantity),' ',app_measures.code,',',inv.cargo_wgt,' kg, ',inv.m3,' m3' ) AS cargo, IF(debts_tks_suma = vtiger_invoice.total, true,false) AS paid,";

				$attachment2 = "LEFT JOIN vtiger_inventoryproductrel ON vtiger_inventoryproductrel.order_id=vtiger_salesorder.salesorderid
												LEFT JOIN vtiger_invoice ON vtiger_invoice.invoiceid = vtiger_inventoryproductrel.id 
												LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_inventoryproductrel.id 
												LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
												LEFT JOIN vtiger_inventoryproductrel inv ON inv.id=vtiger_salesorder.salesorderid
												LEFT JOIN app_measures ON CASE WHEN  inv.cargo_measure REGEXP '^[0-9]+$' THEN app_measures.id=inv.cargo_measure ELSE app_measures.code =inv.cargo_measure END
											  WHERE ";

				$query = str_replace($substr, $substr.$attachment, $query);				
				$query = str_replace($substr2, $attachment2, $query);	
		 }

		 if($relationModuleName == 'Employeeinventory' && $parentModule->get('name') == 'Dismissal'){
				$substr = 'vtiger_dismissalDismissal.dismissalid = vtiger_employeeinventory.cf_20023';
				$attachment = 'vtiger_dismissalDismissal.cf_20023 = vtiger_employeeinventory.cf_20023';
				$query = str_replace($substr, $attachment, $query);
		 }
	 
		// Itoma end	

		if($orderBy) {

			$orderByFieldModuleModel = $relationModule->getFieldByColumn($orderBy);
			if($orderByFieldModuleModel && $orderByFieldModuleModel->isReferenceField()) {
				//If reference field then we need to perform a join with crmentity with the related to field
				$queryComponents = $split = preg_split('/ where /i', $query);
				$selectAndFromClause = $queryComponents[0];
				$whereCondition = $queryComponents[1];
				$qualifiedOrderBy = 'vtiger_crmentity'.$orderByFieldModuleModel->get('column');
				$selectAndFromClause .= ' LEFT JOIN vtiger_crmentity AS '.$qualifiedOrderBy.' ON '.
										$orderByFieldModuleModel->get('table').'.'.$orderByFieldModuleModel->get('column').' = '.
										$qualifiedOrderBy.'.crmid ';
				$query = $selectAndFromClause.' WHERE '.$whereCondition;
				$query .= ' ORDER BY '.$qualifiedOrderBy.'.label '.$sortOrder;
			} elseif($orderByFieldModuleModel && $orderByFieldModuleModel->isOwnerField()) {
				 $query .= ' ORDER BY COALESCE(CONCAT(vtiger_users.first_name,vtiger_users.last_name),vtiger_groups.groupname) '.$sortOrder;
			} else{
				// Qualify the the column name with table to remove ambugity
				$qualifiedOrderBy = $orderBy;
				$orderByField = $relationModule->getFieldByColumn($orderBy);
				if ($orderByField) {
					$qualifiedOrderBy = $relationModule->getOrderBySql($qualifiedOrderBy);
				}
				if($qualifiedOrderBy == 'vtiger_activity.date_start' && ($relationModuleName == 'Calendar' || $relationModuleName == 'Emails')) {
					$qualifiedOrderBy = "str_to_date(concat(vtiger_activity.date_start,vtiger_activity.time_start),'%Y-%m-%d %H:%i:%s')";
				}
				$query = "$query ORDER BY $qualifiedOrderBy $sortOrder";
			}
		} else if($relationModuleName == 'HelpDesk' && empty($orderBy) && empty($sortOrder) && $moduleName != "Users") {
			$query .= ' ORDER BY vtiger_crmentity.modifiedtime DESC';
		}else if($relationModuleName == 'SalesOrder' && $parentModule->get('name') == 'Accounts' ){
			$query .= ' ORDER BY vtiger_crmentity.createdtime DESC';
		}

		// if($relationModuleName == 'DamagesFromTerminal' && $parentModule->get('name') == 'SalesOrder' ){			
			 
		// 	$query = "SELECT delivery_id AS crmid, delivery_date AS deliveryplan_tks_date, 
		// 	CONCAT(load_location_name,', ',load_street_address,', ',load_post_code,', ',load_municipality,', ',DATE_FORMAT(loading_start_time,'%Y-%m-%d %H:%i'),' - ',DATE_FORMAT(loading_end_time,'%H:%i')) AS deliveryplan_tks_load, 
		// 	CONCAT(unload_location_name,', ',unload_street_address,', ',unload_post_code,', ',unload_municipality,', ',DATE_FORMAT(unloading_start_time,'%Y-%m-%d %H:%i'),' - ',DATE_FORMAT(unloading_end_time,'%H:%i')) AS deliveryplan_tks_unload, route_code AS deliveryplan_tks_route,	CONCAT('---') AS deliveryplan_tks_load_date,	CONCAT('---') AS deliveryplan_tks_unload_date 								
		// 					 FROM vtiger_salersorder_delivery_plan p
		// 					 JOIN vtiger_salesorder s ON s.external_order_id=p.shipment_id
		// 					 WHERE s.salesorderid = $parentRecordId
		// 					 GROUP BY delivery_id
		// 					 ORDER BY delivery_id";
		// }


		$limitQuery = $query .' LIMIT '.$startIndex.','.$pageLimit;
//		throw new Exception('kame kampas?');
		$result = $db->pquery($limitQuery, array());
		$relatedRecordList = array();
		$currentUser = Users_Record_Model::getCurrentUserModel();
		$groupsIds = Vtiger_Util_Helper::getGroupsIdsForUsers($currentUser->getId());
		$recordsToUnset = array();
		for($i=0; $i< $db->num_rows($result); $i++ ) {
			$row = $db->fetch_row($result,$i);
			$newRow = array();
			foreach($row as $col=>$val){
				if(array_key_exists($col,$relatedColumnFields)){
					$newRow[$relatedColumnFields[$col]] = $val;
				}
			}
			//To show the value of "Assigned to"
			$ownerId = $row['smownerid'];
			$newRow['assigned_user_id'] = $row['smownerid'];
			if($relationModuleName == 'Calendar') {
				$visibleFields = array('activitytype','date_start','time_start','due_date','time_end','assigned_user_id','visibility','smownerid','parent_id');
				$visibility = true;
				if(in_array($ownerId, $groupsIds)) {
					$visibility = false;
				} else if($ownerId == $currentUser->getId()){
					$visibility = false;
				}
				if(!$currentUser->isAdminUser() && $newRow['activitytype'] != 'Task' && $newRow['visibility'] == 'Private' && $ownerId && $visibility) {
					foreach($newRow as $data => $value) {
						if(in_array($data, $visibleFields) != -1) {
							unset($newRow[$data]);
						}
					}
					$newRow['subject'] = vtranslate('Busy','Events').'*';
				}
				if($newRow['activitytype'] == 'Task') {
					unset($newRow['visibility']);
				}

			}

			$record = Vtiger_Record_Model::getCleanInstance($relationModule->get('name'));
			$record->setData($newRow)->setModuleFromInstance($relationModule)->setRawData($row);
			$record->setId($row['crmid']);
			$relatedRecordList[$row['crmid']] = $record;
			if($relationModuleName == 'Calendar' && !$currentUser->isAdminUser() && $newRow['activitytype'] == 'Task' && isToDoPermittedBySharing($row['crmid']) == 'no') { 
				$recordsToUnset[] = $row['crmid'];
			}
		}
		$pagingModel->calculatePageRange($relatedRecordList);

		$nextLimitQuery = $query. ' LIMIT '.($startIndex+$pageLimit).' , 1';
		$nextPageLimitResult = $db->pquery($nextLimitQuery, array());
		if($db->num_rows($nextPageLimitResult) > 0){
			$pagingModel->set('nextPageExists', true);
		}else{
			$pagingModel->set('nextPageExists', false);
		}
		//setting related list view count before unsetting permission denied records - to make sure paging should not fail
		$pagingModel->set('_relatedlistcount', count($relatedRecordList));
		foreach($recordsToUnset as $record) {
			unset($relatedRecordList[$record]);
		}
		
	//  echo "<pre>";
	// 	 print_R($limitQuery);
	//  echo "</pre>";

		return $relatedRecordList;
	}

	public function getMedian($pagingModel) {
		$db = PearDatabase::getInstance();	
		$relationModule = $this->getRelationModel()->getRelationModuleModel();
		$relationModuleName = $relationModule->get('name');
		$relatedColumnFields = $relationModule->getConfigureRelatedListFields();
		if(count($relatedColumnFields) <= 0){
			$relatedColumnFields = $relationModule->getRelatedListFields();
		}

		if($relationModuleName == 'Calendar') {
			//Adding visibility in the related list, showing records based on the visibility
			$relatedColumnFields['visibility'] = 'visibility';
		}

		if($relationModuleName == 'PriceBooks') {
			//Adding fields in the related list
			$relatedColumnFields['unit_price'] = 'unit_price';
			$relatedColumnFields['listprice'] = 'listprice';
			$relatedColumnFields['currency_id'] = 'currency_id';
		}

		$query = $this->getRelationQuery();

		if ($this->get('whereCondition') && is_array($this->get('whereCondition'))) {
			$currentUser = Users_Record_Model::getCurrentUserModel();
			$queryGenerator = new QueryGenerator($relationModuleName, $currentUser);
			$queryGenerator->setFields(array_values($relatedColumnFields));
			$whereCondition = $this->get('whereCondition');
			foreach ($whereCondition as $fieldName => $fieldValue) {
				if (is_array($fieldValue)) {
					$comparator = $fieldValue[1];
					$searchValue = $fieldValue[2];
					$type = $fieldValue[3];
					if ($type == 'time') {
						$searchValue = Vtiger_Time_UIType::getTimeValueWithSeconds($searchValue);
					}
					$queryGenerator->addCondition($fieldName, $searchValue, $comparator, "AND");
				}
			}
			$whereQuerySplit = split("WHERE", $queryGenerator->getWhereClause());
			$query.=" AND " . $whereQuerySplit[1];
		}

		$startIndex = $pagingModel->getStartIndex();
		$pageLimit = $pagingModel->getPageLimit();

				$substr = 'SELECT DISTINCT';
				$substr2 = 'vtiger_invoicecf.cf_1708,';
				$substr3 = 'INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_invoice.invoiceid';
				$attachment = ' 
			
			CASE 			
					WHEN  vtiger_invoice.duedate > CURDATE() 
					THEN DATEDIFF(vtiger_invoice.duedate, CURDATE())
					ELSE 0
			END  as cf_1710,
            
      CASE WHEN debts_tks_data  THEN
				CASE 
					WHEN  vtiger_invoice.duedate < max(debts_tks_data) THEN CONCAT("+",DATEDIFF( max(debts_tks_data), vtiger_invoice.duedate))
					WHEN  vtiger_invoice.duedate > max(debts_tks_data) THEN CONCAT("-",DATEDIFF(vtiger_invoice.duedate,  max(debts_tks_data)))
					ELSE 0
         END  
			ELSE "---"	
			END AS cf_1708,	';

				$substr4 = 'vtiger_invoicecf.cf_1710,';

				$attachment2 = '  LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = vtiger_invoice.invoiceid 
				LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid ';

				$attachment3 = '';

				$query = str_replace($substr, $substr.$attachment, $query);										
				$query = str_replace($substr2, '', $query);										
				$query = str_replace($substr3, $substr3.$attachment2, $query);	
				$query = str_replace($substr4, '', $query);	
				$query .= " GROUP BY vtiger_invoice.invoiceid ORDER BY vtiger_invoice.invoiceid DESC";	

				
				$result = $db->pquery($query, array());
				$days_arr = array();		

				foreach($result as $row){
					if($row['cf_1708'] != "---")
						$days_arr[] = $row['cf_1708'];
				}

				
				sort($days_arr);
				$count = sizeof($days_arr);   
				$index = floor($count/2);  
				if (!$count) {
					$median = "---";
				} elseif ($count & 1) {    
					$median = $days_arr[$index];
				} else {                  
					$median = ($days_arr[$index-1] + $days_arr[$index]) / 2;
				}
			return $median;		
	}

	public function getHeaders() {
		$relationModel = $this->getRelationModel();
		$relatedModuleModel = $relationModel->getRelationModuleModel();

		$summaryFieldsList = $relatedModuleModel->getHeaderAndSummaryViewFieldsList();

		$headerFields = array();
		if(count($summaryFieldsList) > 0) {
			foreach($summaryFieldsList as $fieldName => $fieldModel) {
				$headerFields[$fieldName] = $fieldModel;
			}
		} else {
			$headerFieldNames = $relatedModuleModel->getRelatedListFields();
			foreach($headerFieldNames as $fieldName) {
				$headerFields[$fieldName] = $relatedModuleModel->getField($fieldName);
			}
		}

		$nameFields = $relatedModuleModel->getNameFields();
		foreach($nameFields as $fieldName){
			if(!$headerFields[$fieldName]) {
				$headerFields[$fieldName] = $relatedModuleModel->getField($fieldName);
			}
		}

		return $headerFields;
	}

	/**
	 * Function to get Relation query
	 * @return <String>
	 */
	public function getRelationQuery() {
		$relationModel = $this->getRelationModel();

		if(!empty($relationModel) && $relationModel->get('name') != NULL){
			$recordModel = $this->getParentRecordModel();
			$query = $relationModel->getQuery($recordModel);
			return $query;
		}
		$relatedModuleModel = $this->getRelatedModuleModel();
		$relatedModuleName = $relatedModuleModel->getName();

		$relatedModuleBaseTable = $relatedModuleModel->basetable;
		$relatedModuleEntityIdField = $relatedModuleModel->basetableid;

		$parentModuleModel = $relationModel->getParentModuleModel();
		$parentModuleBaseTable = $parentModuleModel->basetable;
		$parentModuleEntityIdField = $parentModuleModel->basetableid;
		$parentRecordId = $this->getParentRecordModel()->getId();
		$parentModuleDirectRelatedField = $parentModuleModel->get('directRelatedFieldName');

		$relatedModuleFields = array_keys($this->getHeaders());
		$currentUserModel = Users_Record_Model::getCurrentUserModel();
		$queryGenerator = new QueryGenerator($relatedModuleName, $currentUserModel);
		$queryGenerator->setFields($relatedModuleFields);

		$query = $queryGenerator->getQuery();

		$queryComponents = preg_split('/ FROM /i', $query);
		$query = $queryComponents[0].' ,vtiger_crmentity.crmid FROM '.$queryComponents[1];

		$whereSplitQueryComponents = preg_split('/ WHERE /i', $query);
		$joinQuery = ' INNER JOIN '.$parentModuleBaseTable.' ON '.$parentModuleBaseTable.'.'.$parentModuleDirectRelatedField." = ".$relatedModuleBaseTable.'.'.$relatedModuleEntityIdField;

		$query = "$whereSplitQueryComponents[0] $joinQuery WHERE $parentModuleBaseTable.$parentModuleEntityIdField = $parentRecordId AND $whereSplitQueryComponents[1]";

		return $query;
	}

	public static function getInstance($parentRecordModel, $relationModuleName, $label=false) {
		$parentModuleName = $parentRecordModel->getModule()->get('name');
		$className = Vtiger_Loader::getComponentClassName('Model', 'RelationListView', $parentModuleName);
		$instance = new $className();

		$parentModuleModel = $parentRecordModel->getModule();
		$relatedModuleModel = Vtiger_Module_Model::getInstance($relationModuleName);
		$instance->setRelatedModuleModel($relatedModuleModel);

		$relationModel = Vtiger_Relation_Model::getInstance($parentModuleModel, $relatedModuleModel, $label);
		$instance->setParentRecordModel($parentRecordModel);

		if(!$relationModel){
			$relatedModuleName = $relatedModuleModel->getName();
			$parentModuleModel = $instance->getParentRecordModel()->getModule();
			$referenceFieldOfParentModule = $parentModuleModel->getFieldsByType('reference');
			foreach ($referenceFieldOfParentModule as $fieldName=>$fieldModel) {
				$refredModulesOfReferenceField = $fieldModel->getReferenceList();
				if(in_array($relatedModuleName, $refredModulesOfReferenceField)){
					$relationModelClassName = Vtiger_Loader::getComponentClassName('Model', 'Relation', $parentModuleModel->getName());
					$relationModel = new $relationModelClassName();
					$relationModel->setParentModuleModel($parentModuleModel)->setRelationModuleModel($relatedModuleModel);
					$parentModuleModel->set('directRelatedFieldName',$fieldModel->get('column'));
				}
			}
		}
		if(!$relationModel){
			$relationModel = false;
		}
		$instance->setRelationModel($relationModel);
		return $instance;
	}

	/**
	 * Function to get Total number of record in this relation
	 * @return <Integer>
	 */
	public function getRelatedEntriesCount() {
		$db = PearDatabase::getInstance();
		$currentUser = Users_Record_Model::getCurrentUserModel();
		$realtedModuleModel = $this->getRelatedModuleModel();
		$relatedModuleName = $realtedModuleModel->getName();
		$relationQuery = $this->getRelationQuery();
		$relationQuery = preg_replace("/[ \t\n\r]+/", " ", $relationQuery);
		$position = stripos($relationQuery,' from ');
		if ($position) {
			$split = preg_split('/ FROM /i', $relationQuery);
			$splitCount = count($split);
			if($relatedModuleName == 'Calendar') {
				$relationQuery = 'SELECT DISTINCT vtiger_crmentity.crmid, vtiger_activity.activitytype ';
			} else {
				$relationQuery = 'SELECT COUNT(DISTINCT vtiger_crmentity.crmid) AS count';
			}
			for ($i=1; $i<$splitCount; $i++) {
				$relationQuery = $relationQuery. ' FROM ' .$split[$i];
			}
		}
		if(strpos($relationQuery,' GROUP BY ') !== false){
			$parts = explode(' GROUP BY ',$relationQuery);
			$relationQuery = $parts[0];
		}


						// Itoma begin
		if($_REQUEST['module'] == 'SalesOrder' AND $relatedModuleName == 'Invoice'){
			$substr = 'FROM vtiger_invoice';
			$substr2 = 'inner join vtiger_salesorder on vtiger_salesorder.salesorderid=vtiger_invoice.salesorderid';

			$attachment = ' LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.invoiceid=vtiger_invoice.invoiceid 
											inner join vtiger_salesorder on vtiger_salesorder.salesorderid = vtiger_invoice_salesorders_list.salesorderid ';
			$relationQuery = str_replace($substr2, '', $relationQuery);
			$relationQuery = str_replace($substr, $substr.$attachment, $relationQuery);
		}

		if($_REQUEST['module'] == 'Stevedoring' AND $relatedModuleName = 'SalesOrder'){
			$str = 'INNER JOIN vtiger_stevedoring AS vtiger_stevedoringStevedoring ON vtiger_stevedoringStevedoring.stevedoringid = vtiger_stevedoring.stevedoringno';
			$attachment = ' INNER JOIN vtiger_stevedoring AS vtiger_stevedoringStevedoring ON vtiger_stevedoringStevedoring.stevedoringno = vtiger_salesorder.salesorderid ';
			$relationQuery = str_replace($str, $attachment, $relationQuery);
		}

		if($relatedModuleName == 'Accounts' && $_REQUEST['module'] == 'Claims'){
			$substr = 'vtiger_claimsClaims.claimsid = vtiger_claims.cf_20023';
			$attachment = 'vtiger_claimsClaims.cf_20023 = vtiger_account.accountid';
			$relationQuery = str_replace($substr, $attachment, $relationQuery);
		}

		if($relatedModuleName == 'Kokybe' && $_REQUEST['module'] == 'Claims'){
			$substr = " vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claimsid = vtiger_claims.claims_tks_order_number";
			$attachment = ' vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claims_tks_order_number=vtiger_kokybe.kokybeno';
			$relationQuery = str_replace($substr, $attachment, $relationQuery);
		}

		if($relatedModuleName == 'Documents' && $_REQUEST['module'] == 'Claims'){
			$substr = "INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claimsid = vtiger_claims.claims_tks_order_number";
			$attachment = ' LEFT JOIN vtiger_senotesrel ON vtiger_senotesrel.notesid=vtiger_notes.notesid
			INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claims_tks_order_number = vtiger_senotesrel.crmid';
			$relationQuery = str_replace($substr, $attachment, $relationQuery);
		}	

		if($relatedModuleName == 'Invoice' && $_REQUEST['module'] == 'Claims'){
			$substr = "INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claimsid = vtiger_claims.claims_tks_order_number";
			$attachment = ' LEFT JOIN vtiger_invoice_salesorders_list ON vtiger_invoice_salesorders_list.invoiceid=vtiger_invoice.invoiceid
			INNER JOIN vtiger_claims AS vtiger_claimsClaims ON vtiger_claimsClaims.claims_tks_order_number=vtiger_invoice_salesorders_list.salesorderid';
			$relationQuery = str_replace($substr, $attachment, $relationQuery);
		}	

		// Itoma end
		

		$result = $db->pquery($relationQuery, array());
		if ($result) {
			if($relatedModuleName == 'Calendar') {
				$count = 0;
				for($i=0;$i<$db->num_rows($result);$i++) {
					$id = $db->query_result($result, $i, 'crmid');
					$activityType = $db->query_result($result, $i, 'activitytype');
					if(!$currentUser->isAdminUser() && $activityType == 'Task' && isToDoPermittedBySharing($id) == 'no') {
						continue;
					} else {
						$count++;
					}
				}
				return $count;
			} else {
				return $db->query_result($result, 0, 'count');
			}
		} else {
			return 0;
		}
	}

	/**
	 * Function to update relation query
	 * @param <String> $relationQuery
	 * @return <String> $updatedQuery
	 */
	public function updateQueryWithWhereCondition($relationQuery) {
		$condition = '';

		$whereCondition = $this->get("whereCondition");
		$count = count($whereCondition);
		if ($count > 1) {
			$appendAndCondition = true;
		}

		$i = 1;
		foreach ($whereCondition as $fieldName => $fieldValue) {
			if(is_array($fieldValue)){
				$fieldColumn = $fieldValue[0];
				$comparator = $fieldValue[1];
				$value = $fieldValue[2];
				if($comparator == "c"){
					$condition .= "$fieldColumn like '%$value%' ";
				}else{
					$condition .= "$fieldColumn = '$value' ";
				}
			}else {
				$condition .= " $fieldName = '$fieldValue' ";
			}
			if ($appendAndCondition && ($i++ != $count)) {
				$condition .= " AND ";
			}
		}

		$pos = stripos($relationQuery, 'where');
		if ($pos) {
			$split = preg_split('/where/i', $relationQuery);
			$updatedQuery = $split[0].' WHERE '.$split[1].' AND '.$condition;
		} else {
			$updatedQuery = $relationQuery.' WHERE '.$condition;
		}
		return $updatedQuery;
	}

	public function getCurrencySymbol($recordId, $fieldModel) {
		$db = PearDatabase::getInstance();
		$moduleName = $fieldModel->getModuleName();
		$fieldName = $fieldModel->get('name');
		$tableName = $fieldModel->get('table');
		$columnName = $fieldModel->get('column');

		if(($fieldName == 'unit_price') && ($moduleName == 'Products' || $moduleName == 'Services')) {
			$query = "SELECT currency_symbol FROM vtiger_currency_info WHERE id = (";
			if($moduleName == 'Products') 
				$query .= "SELECT currency_id FROM vtiger_products WHERE productid = ?)";
			else if($moduleName == 'Services')
				$query .= "SELECT currency_id FROM vtiger_service WHERE serviceid = ?)";

			$result = $db->pquery($query, array($recordId));
			return $db->query_result($result, 0, 'currency_symbol');
		} else if(($tableName == 'vtiger_invoice' || $tableName == 'vtiger_quotes' || $tableName == 'vtiger_purchaseorder' || $tableName == 'vtiger_salesorder') &&
			($columnName == 'total' || $columnName == 'subtotal' || $columnName == 'discount_amount' || $columnName == 's_h_amount' || $columnName == 'paid' ||
			$columnName == 'balance' || $columnName == 'received' || $columnName == 'listprice' || $columnName == 'adjustment' || $columnName == 'pre_tax_total')) {
			$focus = CRMEntity::getInstance($moduleName);
			$query = "SELECT currency_symbol FROM vtiger_currency_info WHERE id = ( SELECT currency_id FROM ".$tableName." WHERE ".$focus->table_index." = ? )";
			$result = $db->pquery($query, array($recordId));
			return $db->query_result($result, 0, 'currency_symbol');
		} else {
			$fieldInfo = $fieldModel->getFieldInfo();
			return $fieldInfo['currency_symbol'];
		}
	}

}
