<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Settings_Vtiger_NotificationCenterPermisionsEdit_View extends Settings_Vtiger_Index_View {


	public function process(Vtiger_Request $request) {
		global $adb;
		$qualifiedModuleName = $request->getModule(false);
		$id = $_GET['record'];


		if($_POST){		
			$adb->pquery("UPDATE vtiger_notification_permisions SET responsibles = ?,  managers = ?, person = ? WHERE id = ?",array(implode(",",$_POST['responsibles']),implode(",",$_POST['managers']),implode(",",$_POST['person']),$id));		
			header("Location:/index.php?parent=Settings&module=Vtiger&view=NotificationCenterPermisions");
		}

		$sql = "SELECT * FROM vtiger_notification_permisions WHERE id = ?";
		$records = $adb->pquery($sql,array($id));	

		$sql2 = "SELECT id, CONCAT(first_name,' ',last_name) AS person FROM vtiger_users WHERE status = 'Active' ORDER BY first_name, last_name";		
		$records2 = $adb->query($sql2);

		$result = $adb->fetch_array($records);
		$viewer = $this->getViewer($request);
		$viewer->assign('records', $result);
		$viewer->assign('USERS', $records2);
		$viewer->view('NotificationCenterPermisionsEdit.tpl', $qualifiedModuleName);
	}
	
	
	function getPageTitle(Vtiger_Request $request) {
		$qualifiedModuleName = $request->getModule(false);
		return vtranslate('LBL_NOTIFICATIONS_CENTER',$qualifiedModuleName);
	}
	
   
}