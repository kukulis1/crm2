<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Settings_Vtiger_SelfServiceMessagesEdit_View extends Settings_Vtiger_Index_View {


	public function process(Vtiger_Request $request) {
		global $adb;
		$qualifiedModuleName = $request->getModule(false);
		$id = $_GET['record'];


		if($_POST){		
			$date = date("Y-m-d H:i:s");				
			if($id == 1){
				$adb->pquery("UPDATE vtiger_self_service_messages SET message = ?, period = ?, date = ? WHERE id = ?",array($_POST['message'],$_POST['period'],$date,$id));
			}else{
				$adb->pquery("UPDATE vtiger_self_service_messages SET customer_id = ?,  message = ?, period = ?, date = ? WHERE id = ?",array($_POST['customer_id'],$_POST['message'],$_POST['period'],$date,$id));
			}
			header("Location:/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessages");
		}

		$sql = "SELECT * FROM vtiger_self_service_messages WHERE id = ?";
		$records = $adb->pquery($sql,array($id));	

		
		$sql2 = "SELECT DISTINCT accountname, customer_id 
									FROM vtiger_account a
									LEFT JOIN vtiger_crmentity e ON e.crmid=a.accountid
									WHERE deleted = 0
									ORDER BY accountname ";

		$records2 = $adb->query($sql2);

		$result = $adb->fetch_array($records);
		$viewer = $this->getViewer($request);
		$viewer->assign('records', $result);
		$viewer->assign('accounts', $records2);
		$viewer->view('SelfServiceMessagesEdit.tpl', $qualifiedModuleName);
	}
	
	
	function getPageTitle(Vtiger_Request $request) {
		$qualifiedModuleName = $request->getModule(false);
		return vtranslate('LBL_SELF_SERVICE_MESSAGE',$qualifiedModuleName);
	}
	
   
}