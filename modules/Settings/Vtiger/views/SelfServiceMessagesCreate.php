<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Settings_Vtiger_SelfServiceMessagesCreate_View extends Settings_Vtiger_Index_View {


	public function process(Vtiger_Request $request) {
		global $adb;
		$qualifiedModuleName = $request->getModule(false);

		if($_POST){		
			$date = date("Y-m-d H:i:s");					
			$sql = "INSERT INTO vtiger_self_service_messages (customer_id, message, period, date) VALUES (?,?,?,?)";	
			$adb->pquery($sql, array($_POST['customer_id'],$_POST['message'],$_POST['period'],$date));
			header("Location:/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessages");
		}

		$sql = "SELECT DISTINCT accountname, customer_id
									FROM vtiger_account a
									LEFT JOIN vtiger_crmentity e ON e.crmid=a.accountid
									WHERE deleted = 0 AND customer_id != ''
									ORDER BY accountname ";

		$records = $adb->query($sql);			
		$viewer = $this->getViewer($request);
		$viewer->assign('records', $records);
		$viewer->view('SelfServiceMessagesCreate.tpl', $qualifiedModuleName);
	}
	
	
	function getPageTitle(Vtiger_Request $request) {
		$qualifiedModuleName = $request->getModule(false);
		return vtranslate('LBL_SELF_SERVICE_MESSAGE',$qualifiedModuleName);
	}
	
   
}