<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Settings_Vtiger_SelfServiceMessagesDelete_View extends Settings_Vtiger_Index_View {


	public function process(Vtiger_Request $request) {
		global $adb;
		$id = $_GET['record'];	
		$records = $adb->pquery("DELETE FROM vtiger_self_service_messages WHERE id = ?",array($id));	
		header("Location:/index.php?parent=Settings&module=Vtiger&view=SelfServiceMessages");
	}
   
}