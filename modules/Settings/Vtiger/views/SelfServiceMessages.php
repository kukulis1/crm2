<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Settings_Vtiger_SelfServiceMessages_View extends Settings_Vtiger_Index_View {


	public function process(Vtiger_Request $request) {
		global $adb;
		$qualifiedModuleName = $request->getModule(false);

		$sql = "SELECT s.*, IF(s.customer_id = '---', 'Bendra',a.accountname) AS accountname FROM vtiger_self_service_messages s LEFT JOIN vtiger_account a ON a.customer_id=s.customer_id";

		$records = $adb->query($sql);			
		$viewer = $this->getViewer($request);
		$viewer->assign('records', $records);
		$viewer->view('SelfServiceMessages.tpl', $qualifiedModuleName);
	}
	
	
	function getPageTitle(Vtiger_Request $request) {
		$qualifiedModuleName = $request->getModule(false);
		return vtranslate('LBL_SELF_SERVICE_MESSAGE',$qualifiedModuleName);
	}
	
   
}