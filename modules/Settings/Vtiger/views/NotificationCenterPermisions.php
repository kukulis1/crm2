<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Settings_Vtiger_NotificationCenterPermisions_View extends Settings_Vtiger_Index_View {


	public function process(Vtiger_Request $request) {
		global $adb;
		$qualifiedModuleName = $request->getModule(false);

		$sql = "SELECT * FROM vtiger_notification_permisions";

		$records = $adb->query($sql);			
		$viewer = $this->getViewer($request);
		$viewer->assign('records', $records);
		$moduleModel = Settings_Vtiger_CompanyDetails_Model::getInstance();
		$viewer->assign('MODULE_MODEL', $moduleModel);

		$viewer->view('NotificationCenterPermisions.tpl', $qualifiedModuleName);
	}	
	
	function getPageTitle(Vtiger_Request $request) {
		$qualifiedModuleName = $request->getModule(false);
		return vtranslate('LBL_NOTIFICATIONS_CENTER',$qualifiedModuleName);
	}
	
   
}