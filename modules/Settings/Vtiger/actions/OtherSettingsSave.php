<?php

/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ********************************************************************************** */

class Settings_Vtiger_OtherSettingsSave_Action extends Settings_Vtiger_Basic_Action {

	public function process(Vtiger_Request $request) {
		$reloadUrl = 'index.php?parent=Settings&module=Vtiger&view=OtherSettings';
		try{
			$this->Save($request);
		} catch(Exception $e) {
		
		}
		header('Location: ' . $reloadUrl);
	}

	public function Save(Vtiger_Request $request) {	
		global $adb;
		for($i = 1; $i < $request->get('count');$i++){
			$value = $request->get('value'.$i);
			$id = $request->get('record'.$i);
			$adb->query("UPDATE app_other_settings SET `value` = '$value' WHERE id = $id");
		}
		return;
	}


}