<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Leads_Record_Model extends Vtiger_Record_Model {

	/**
	 * Function returns the url for converting lead
	 */
	function getConvertLeadUrl() {
		return 'index.php?module='.$this->getModuleName().'&view=ConvertLead&record='.$this->getId();
	}

	/**
	 * Static Function to get the list of records matching the search key
	 * @param <String> $searchKey
	 * @return <Array> - List of Vtiger_Record_Model or Module Specific Record Model instances
	 */
	public static function getSearchResult($searchKey, $module=false) {
		$db = PearDatabase::getInstance();

		$deletedCondition = $this->getModule()->getDeletedRecordCondition();
		$query = 'SELECT * FROM vtiger_crmentity
                    INNER JOIN vtiger_leaddetails ON vtiger_leaddetails.leadid = vtiger_crmentity.crmid
                    WHERE label LIKE ? AND '.$deletedCondition;
		$params = array("%$searchKey%");
		$result = $db->pquery($query, $params);
		$noOfRows = $db->num_rows($result);

		$moduleModels = array();
		$matchingRecords = array();
		for($i=0; $i<$noOfRows; ++$i) {
			$row = $db->query_result_rowdata($result, $i);
			$row['id'] = $row['crmid'];
			$moduleName = $row['setype'];
			if(!array_key_exists($moduleName, $moduleModels)) {
				$moduleModels[$moduleName] = Vtiger_Module_Model::getInstance($moduleName);
			}
			$moduleModel = $moduleModels[$moduleName];
			$modelClassName = Vtiger_Loader::getComponentClassName('Model', 'Record', $moduleName);
			$recordInstance = new $modelClassName();
			$matchingRecords[$moduleName][$row['id']] = $recordInstance->setData($row)->setModuleFromInstance($moduleModel);
		}
		return $matchingRecords;
	}

	/**
	 * Function returns Account fields for Lead Convert
	 * @return Array
	 */
	function getAccountFieldsForLeadConvert() {
		$accountsFields = array();
		$privilegeModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleName = 'Accounts';

		if(!Users_Privileges_Model::isPermitted($moduleName, 'CreateView')) {
			return;
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if ($moduleModel->isActive()) {
			$fieldModels = $moduleModel->getFields();
            //Fields that need to be shown
            $complusoryFields = array('industry');
			foreach ($fieldModels as $fieldName => $fieldModel) {
				if($fieldModel->isMandatory() && $fieldName != 'assigned_user_id') {
                    $keyIndex = array_search($fieldName,$complusoryFields);
                    if($keyIndex !== false) {
                        unset($complusoryFields[$keyIndex]);
                    }
					$leadMappedField = $this->getConvertLeadMappedField($fieldName, $moduleName);
                    if($this->get($leadMappedField)) {
                        $fieldModel->set('fieldvalue', $this->get($leadMappedField));
                    } else {
                        $fieldModel->set('fieldvalue', $fieldModel->getDefaultFieldValue());
                    } 
					$accountsFields[] = $fieldModel;
				}
			}
            foreach($complusoryFields as $complusoryField) {
                $fieldModel = Vtiger_Field_Model::getInstance($complusoryField, $moduleModel);
				if($fieldModel->getPermissions('readwrite') && $fieldModel->isEditable()) {
                    $industryFieldModel = $moduleModel->getField($complusoryField);
                    $industryLeadMappedField = $this->getConvertLeadMappedField($complusoryField, $moduleName);
                    if($this->get($industryLeadMappedField)) {
                        $industryFieldModel->set('fieldvalue', $this->get($industryLeadMappedField));
                    } else {
                        $industryFieldModel->set('fieldvalue', $industryFieldModel->getDefaultFieldValue());
                    }
                    $accountsFields[] = $industryFieldModel;
                }
            }
		}
		return $accountsFields;
	}

	/**
	 * Function returns Contact fields for Lead Convert
	 * @return Array
	 */
	function getContactFieldsForLeadConvert() {
		$contactsFields = array();
		$privilegeModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleName = 'Contacts';

		if(!Users_Privileges_Model::isPermitted($moduleName, 'CreateView')) {
			return;
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if ($moduleModel->isActive()) {
			$fieldModels = $moduleModel->getFields();
            $complusoryFields = array('firstname', 'email');
            foreach($fieldModels as $fieldName => $fieldModel) {
                if($fieldModel->isMandatory() &&  $fieldName != 'assigned_user_id' && $fieldName != 'account_id') {
                    $keyIndex = array_search($fieldName,$complusoryFields);
                    if($keyIndex !== false) {
                        unset($complusoryFields[$keyIndex]);
                    }

                    $leadMappedField = $this->getConvertLeadMappedField($fieldName, $moduleName);
                    $fieldValue = $this->get($leadMappedField);
                    if ($fieldName === 'account_id') {
                        $fieldValue = $this->get('company');
                    }
                    if($fieldValue) {
                        $fieldModel->set('fieldvalue', $fieldValue);
                    } else {
                        $fieldModel->set('fieldvalue', $fieldModel->getDefaultFieldValue());
                    }
                    $contactsFields[] = $fieldModel;
                }
            }

			foreach($complusoryFields as $complusoryField) {
                $fieldModel = Vtiger_Field_Model::getInstance($complusoryField, $moduleModel);
				if($fieldModel->getPermissions('readwrite') && $fieldModel->isEditable()) {
					$leadMappedField = $this->getConvertLeadMappedField($complusoryField, $moduleName);
					$fieldModel = $moduleModel->getField($complusoryField);
                    if($this->get($leadMappedField)) {
                        $fieldModel->set('fieldvalue', $this->get($leadMappedField));
                    } else {
                        $fieldModel->set('fieldvalue', $fieldModel->getDefaultFieldValue());
                    }
					$contactsFields[] = $fieldModel;
				}
			}
		}
		return $contactsFields;
	}

	/**
	 * Function returns Potential fields for Lead Convert
	 * @return Array
	 */
	function getPotentialsFieldsForLeadConvert() {
		$potentialFields = array();
		$privilegeModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleName = 'Potentials';

		if(!Users_Privileges_Model::isPermitted($moduleName, 'CreateView')) {
			return;
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if ($moduleModel->isActive()) {
			$fieldModels = $moduleModel->getFields();

            $complusoryFields = array('amount');
			foreach($fieldModels as $fieldName => $fieldModel) {
				if($fieldModel->isMandatory() &&  $fieldName != 'assigned_user_id' && $fieldName != 'related_to'
						&& $fieldName != 'contact_id') {
                    $keyIndex = array_search($fieldName,$complusoryFields);
                    if($keyIndex !== false) {
                        unset($complusoryFields[$keyIndex]);
                    }
					$leadMappedField = $this->getConvertLeadMappedField($fieldName, $moduleName);
                    if($this->get($leadMappedField)) { 
                        $fieldModel->set('fieldvalue', $this->get($leadMappedField));
                    } else {
                        $fieldModel->set('fieldvalue', $fieldModel->getDefaultFieldValue());
                    }
					$potentialFields[] = $fieldModel;
				}
			}
            foreach($complusoryFields as $complusoryField) {
                $fieldModel = Vtiger_Field_Model::getInstance($complusoryField, $moduleModel);
                if($fieldModel->getPermissions('readwrite') && $fieldModel->isEditable()) {
                    $fieldModel = $moduleModel->getField($complusoryField);
                    $amountLeadMappedField = $this->getConvertLeadMappedField($complusoryField, $moduleName);
                    if($this->get($amountLeadMappedField)) {
                        $fieldModel->set('fieldvalue', $this->get($amountLeadMappedField));
                    } else {
                        $fieldModel->set('fieldvalue', $fieldModel->getDefaultFieldValue());
                    }
                    $potentialFields[] = $fieldModel;
                }
            }
		}
		return $potentialFields;
	}

	/**
	 * Function returns field mapped to Leads field, used in Lead Convert for settings the field values
	 * @param <String> $fieldName
	 * @return <String>
	 */
	function getConvertLeadMappedField($fieldName, $moduleName) {
		$mappingFields = $this->get('mappingFields');

		if (!$mappingFields) {
			$db = PearDatabase::getInstance();
			$mappingFields = array();

			$result = $db->pquery('SELECT * FROM vtiger_convertleadmapping', array());
			$numOfRows = $db->num_rows($result);

			$accountInstance = Vtiger_Module_Model::getInstance('Accounts');
			$accountFieldInstances = $accountInstance->getFieldsById();

			$contactInstance = Vtiger_Module_Model::getInstance('Contacts');
			$contactFieldInstances = $contactInstance->getFieldsById();

			$potentialInstance = Vtiger_Module_Model::getInstance('Potentials');
			$potentialFieldInstances = $potentialInstance->getFieldsById();

			$leadInstance = Vtiger_Module_Model::getInstance('Leads');
			$leadFieldInstances = $leadInstance->getFieldsById();

			for($i=0; $i<$numOfRows; $i++) {
				$row = $db->query_result_rowdata($result,$i);
				if(empty($row['leadfid'])) continue;

				$leadFieldInstance = $leadFieldInstances[$row['leadfid']];
				if(!$leadFieldInstance) continue;

				$leadFieldName = $leadFieldInstance->getName();
				$accountFieldInstance = $accountFieldInstances[$row['accountfid']];
				if ($row['accountfid'] && $accountFieldInstance) {
					$mappingFields['Accounts'][$accountFieldInstance->getName()] = $leadFieldName;
				}
				$contactFieldInstance = $contactFieldInstances[$row['contactfid']];
				if ($row['contactfid'] && $contactFieldInstance) {
					$mappingFields['Contacts'][$contactFieldInstance->getName()] = $leadFieldName;
				}
				$potentialFieldInstance = $potentialFieldInstances[$row['potentialfid']];
				if ($row['potentialfid'] && $potentialFieldInstance) {
					$mappingFields['Potentials'][$potentialFieldInstance->getName()] = $leadFieldName;
				}
			}
			$this->set('mappingFields', $mappingFields);
		}
		return $mappingFields[$moduleName][$fieldName];
	}

	/**
	 * Function returns the fields required for Lead Convert
	 * @return <Array of Vtiger_Field_Model>
	 */
	function getConvertLeadFields() {
		$convertFields = array();
		$accountFields = $this->getAccountFieldsForLeadConvert();
		if(!empty($accountFields)) {
			$convertFields['Accounts'] = $accountFields;
		}

		$contactFields = $this->getContactFieldsForLeadConvert();
		if(!empty($contactFields)) {
			$convertFields['Contacts'] = $contactFields;
		}

		$potentialsFields = $this->getPotentialsFieldsForLeadConvert();
		if(!empty($potentialsFields)) {
			$convertFields['Potentials'] = $potentialsFields;
		}
		return $convertFields;
	}

	/**
	 * Function returns the url for create event
	 * @return <String>
	 */
	function getCreateEventUrl() {
		$calendarModuleModel = Vtiger_Module_Model::getInstance('Calendar');
		return $calendarModuleModel->getCreateEventRecordUrl().'&parent_id='.$this->getId();
	}

	/**
	 * Function returns the url for create todo
	 * @return <String>
	 */
	function getCreateTaskUrl() {
		$calendarModuleModel = Vtiger_Module_Model::getInstance('Calendar');
		return $calendarModuleModel->getCreateTaskRecordUrl().'&parent_id='.$this->getId();
	}
    
    /**
	 * Function to check whether the lead is converted or not
	 * @return True if the Lead is Converted false otherwise.
	 */
    function isLeadConverted() {
        $db = PearDatabase::getInstance();
        $id = $this->getId();
        $sql = "select converted from vtiger_leaddetails where converted = 1 and leadid=?";
        $result = $db->pquery($sql,array($id));
        $rowCount = $db->num_rows($result);
        if($rowCount > 0){
            return true;
        }
        return false;
		}

		function createNewClient(){

			$db = PearDatabase::getInstance();
			$id = $this->getId();		
			$date = date("Y-m-d H:i:s");
			$query = "SELECT * FROM vtiger_leaddetails WHERE leadid = ?";
			$query2 = "SELECT * FROM vtiger_leadaddress WHERE leadaddressid = ?";
			$query3 = "SELECT * FROM vtiger_leadscf WHERE leadid = ?";

			$query4 = "INSERT INTO vtiger_account (accountid,accountname,account_type,phone,otherphone,email1,isconvertedfromlead,legal_entity_code,legal_vat_code,billing_address) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";	
			$query5 = "INSERT INTO vtiger_accountbillads (accountaddressid,bill_country,bill_street,bill_city) VALUES (?,?,?,?)";
			$query6 = "INSERT INTO vtiger_accountshipads (accountaddressid,ship_country) VALUES (?,?)";

			$query7 = "UPDATE vtiger_leaddetails SET converted = 1 WHERE leadid = ?";
		
			$result = $db->pquery($query,array($id));
			$result2 = $db->pquery($query2,array($id));
			$result3 = $db->pquery($query3,array($id));

			$firstname = $db->query_result($result,0,'firstname');
			$lastname = $db->query_result($result,0,'lastname');
			$email = $db->query_result($result,0,'email');
			$company = $db->query_result($result,0,'company');
			$type = trim($db->query_result($result,0,'rating'));
			$phone = $db->query_result($result2,0,'mobile');
			$phone2 = $db->query_result($result2,0,'phone');		

			$legal_entity_code = $db->query_result($result3,0,'cf_1797');
			$legal_vat_code = $db->query_result($result3,0,'cf_1799');
			$street = $db->query_result($result3,0,'cf_1801');
			$type2 = 'Juridinis';

			if(empty($company)) $company = $firstname." ".$lastname;

			$last_entity_record = $this->last_entity_id($db);
			$this->insert_entity($db, 'Accounts', $last_entity_record, 26, NULL,  $company, $date);   
			$db->pquery($query4, array($last_entity_record,$company,$type2, $phone,$phone2, $email,1 ,$legal_entity_code,$legal_vat_code, $street));
			$db->pquery($query5, array($last_entity_record,'LTU',$street,'---'));
			$db->pquery($query6, array($last_entity_record,'LTU'));

			$db->pquery($query7,array($id));			
			
			$this->exportAccountToMetrika($last_entity_record);

	

			if($type == 'Transporto užsakymas' || $type == 'Perkraustymo užsakymas'){		
				$this->createNewSalesOrder($last_entity_record,$id);
			}else{
				$this->createProject($last_entity_record,$id);
			}
			
			$referer = $_SERVER['PHP_SELF']."?module=Accounts&view=Detail&record=$last_entity_record";
			return header("Location: $referer");
		}
		
		public function exportAccountToMetrika($account_id){
			$adb = PearDatabase::getInstance();

			$sql = "SELECT accountname,phone,contact,account_type,legal_entity_code,legal_vat_code,bill_street,bill_city,bill_code ,bill_country
									FROM vtiger_account
									LEFT JOIN vtiger_accountbillads ON vtiger_accountbillads.accountaddressid=accountid
									WHERE accountid = ?";  
			$sql2 = "UPDATE vtiger_account SET account_no = ?, customer_id = ?, billing_address = ?, municipality = ?, post_code = ? WHERE accountid = ?";
			$sql3 = "UPDATE vtiger_accountscf SET cf_1576 = ? WHERE accountid = ?";
	
	

			$result = $adb->pquery($sql, array($account_id));
	
			$account = array();
	
			while($row = $adb->fetch_array($result)) {
				$account["customer_id"] = "";
				$account["company"] = $row["accountname"];
				$account["address"] = ($row["bill_street"] ?: '---');
				$account["settlement"] = "";
				$account["municipality"] = ($row["bill_city"] ?: '---');
				$account["region"] = "";
				$account["zipcode"] = ($row["bill_code"] ?: '---');
				$account["country"] = ($row["bill_country"] ?: '---');
				$account["person"] = ($row["contact"] ?: '---');
				$account["phone"] = ($row["phone"] ?: '---');
				$account["company_code"] = ($row["legal_entity_code"] ?: '---');
	
		 }             
	
				$res = json_encode($account);  	

				$url = "https://uzsakymai.parnasas.lt/import/crm/customers.php?insert_new_customer=1";
	
				if($_SERVER['REMOTE_ADDR'] != '127.0.0.1'){
					$url = "https://demo-uzsakymai.parnasas.lt/import/crm/customers.php?insert_new_customer=1";
				}

	
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);           
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'customers' => $res));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
				$result = curl_exec($ch);
				curl_close($ch);	
	
				$data = json_decode($result, true);
				
				$state = $data[0];
									
				if($state != 'ERROR' && $state != 'ALREADY EXIST') { 
					$data = json_decode($result, true);
					$customer_id = $data[0]['customer']['customer_id'];
					$customer_code = $data[0]['customer']['customer_code'];
					$address = $data[0]['customer']['address'];
					$municipality = $data[0]['customer']['municipality'];
					$zipcode = $data[0]['customer']['zipcode'];
					$adb->pquery($sql2, array($customer_code,$customer_id,$address,$municipality,$zipcode,$account_id));
					$adb->pquery($sql3, array($customer_id,$account_id));
				}	
	
		}

		function createProject($accountid,$leadid){
			$db = PearDatabase::getInstance();
			$date = date("Y-m-d H:i:s");

			$query = "SELECT l.total,quantity,comment,cargo_wgt,cargo_length,cargo_width,cargo_height,measure, cf_1793 as bill_address, cf_1801 as ship_address,cf_1785 as bill_code,cf_1787 as  ship_code,rating as type, cf_1858 as load_company, cf_1860 as unload_company
													FROM vtiger_leaddetails l
													LEFT JOIN vtiger_leadaddress ON vtiger_leadaddress.leadaddressid=l.leadid
													LEFT JOIN vtiger_leadscf ON vtiger_leadscf.leadid=l.leadid
													LEFT JOIN vtiger_leads_cargo ON vtiger_leads_cargo.id=l.leadid
													WHERE l.leadid = ?";

			$query2 = "INSERT INTO vtiger_projects (projectsid,projectsno,projects_tks_ordertype,cf_20023) VALUES (?,?,?,?)";
			$query3 = "INSERT INTO vtiger_projects_cargo (id, quantity, comment, cargo_wgt, cargo_length, cargo_width, cargo_height, measure) VALUES(?,?,?,?,?,?,?,?)";
			
			$last_entity_record = $this->last_entity_id($db);
			$insert_entity = $this->insert_entity($db, 'Projects', $last_entity_record, 26, NULL, '', $date); 
			
			$result = $db->pquery($query, array($leadid));
			$proid = $this->getSeqNum('Projects');
			while($row = $db->fetch_array($result)) {
				$db->pquery($query2, array($last_entity_record,$proid,$row['type'],$accountid));
				$db->pquery($query3, array($last_entity_record,$row['quantity'],$row['comment'],$row['cargo_wgt'],$row['cargo_length'],$row['cargo_width'],$row['cargo_height'],$row['measure']));
			}

		}

		function getSeqNum($module){
			$adb = PearDatabase::getInstance();
			$check = $adb->pquery("SELECT prefix FROM vtiger_modentity_num WHERE semodule=? AND active = 1", array($module));
			$prefix = $adb->query_result($check, 0, 'prefix');

			$check_cur = $adb->pquery("SELECT projectsno FROM vtiger_projects 
																		LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_projects.projectsid  																		
																		WHERE  vtiger_crmentity.deleted = 0 
																		ORDER BY vtiger_projects.projectsid  
																		DESC LIMIT 1", array());

			$curid = $adb->query_result($check_cur, 0, 'projectsno');
			if(!empty($curid)){
				$curid = substr($curid, -3);
			}else{
				$req_no = '001';
			}

			if(!empty($curid)){
				$strip = strlen($curid) - strlen($curid + 1);
				if ($strip < 0)
					$strip = 0;
				$temp = str_repeat("0", $strip);
				$req_no.= $temp . ($curid + 1);
			}

			$prev_inv_no = $prefix.$req_no;

			return decode_html($prev_inv_no);
		}

		function createNewSalesOrder($accountid,$leadid){

			$db = PearDatabase::getInstance();
			$date = date("Y-m-d H:i:s");

			$query = "SELECT l.total,cf_1793 as bill_address, cf_1801 as ship_address,cf_1785 as bill_code,cf_1787 as  ship_code,rating as type, cf_1858 as load_company, cf_1860 as unload_company,cf_1789 AS bill_city, cf_1791 AS ship_city
											 FROM vtiger_leaddetails l	
											 LEFT JOIN vtiger_leadaddress ON vtiger_leadaddress.leadaddressid=l.leadid
											 LEFT JOIN vtiger_leadscf ON vtiger_leadscf.leadid=l.leadid
											 WHERE l.leadid = ?";		

			$query7 = "SELECT quantity,cargo_wgt,cargo_length,cargo_width,cargo_height,measure FROM vtiger_leads_cargo WHERE id = ?";

										

			$query2 = "INSERT INTO vtiger_salesorder (salesorderid,subject,total,subtotal,accountid,sostatus,load_date_from,load_time_from,load_date_to,load_time_to,unload_date_from,unload_time_from,unload_date_to,unload_time_to,source,import_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";					
			
			$query3 = "INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,margin,cargo_measure,cargo_wgt,cargo_length,cargo_width,cargo_height,source,ordered_weight,ordered_length,ordered_width,ordered_height,revised_weight,revised_length,revised_width,revised_height,m3,service) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			$query4 = "INSERT INTO vtiger_sobillads  (sobilladdressid,bill_code,bill_street,load_company,bill_country,bill_city) VALUES (?,?,?,?,?,?)";
			$query5 = "INSERT INTO vtiger_soshipads (soshipaddressid,ship_code,ship_street,unload_company,ship_country,ship_city) VALUES (?,?,?,?,?,?)";
			$query6 = "INSERT INTO vtiger_salesordercf (salesorderid,cf_855) VALUES (?,?)";

			$last_entity_record = $this->last_entity_id($db);
			$this->insert_entity($db, 'SalesOrder', $last_entity_record, 26, NULL,  '', $date);   

			$load_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 1 days'));
			$load_time_from = '08:00:00';
			$load_time_to = '17:00:00';
			$unload_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 2 days'));
			$unload_time_from = '08:00:00';
			$unload_time_to = '17:00:00';

			$result = $db->pquery($query, array($leadid));
			$result2 = $db->pquery($query7, array($leadid));

			$total = $db->query_result($result,0,'total');
			$bill_city = $db->query_result($result,0,'bill_city');
			$bill_code = $db->query_result($result,0,'bill_code');
			$ship_city = $db->query_result($result,0,'ship_city');
			$ship_code = $db->query_result($result,0,'ship_code');
			$type = $db->query_result($result,0,'type');
			
			$load_company = str_replace('&Scaron;', 'Š', $db->query_result($result,0,'load_company'));
			$load_company = str_replace('&scaron;', 'š', $db->query_result($result,0,'load_company'));
			$bill_address = str_replace('&Scaron;', 'Š', $db->query_result($result,0,'bill_address'));
			$bill_address = str_replace('&scaron;', 'š', $db->query_result($result,0,'bill_address'));
			$bill_address = ($bill_address ?: '---');

			
			$unload_company = str_replace('&Scaron;', 'Š', $db->query_result($result,0,'unload_company'));
			$unload_company = str_replace('&scaron;', 'š', $db->query_result($result,0,'unload_company'));
			$ship_address = str_replace('&Scaron;', 'Š', $db->query_result($result,0,'ship_address'));
			$ship_address = str_replace('&scaron;', 'š', $db->query_result($result,0,'ship_address'));
			$ship_address = ($ship_address ?: '---');


			$db->pquery($query2, array($last_entity_record,'Užsakymas', $total,$total,$accountid, 'Sent',$load_date,$load_time_from,$load_date,$load_time_to,$unload_date,$unload_time_from,$unload_date,$unload_time_to,'CRM',$date));
			$db->pquery($query4, array($last_entity_record,$bill_code,$bill_address,$load_company,'LTU',$bill_city));
			$db->pquery($query5, array($last_entity_record,$ship_code,$ship_address,$unload_company,'LTU',$ship_city));
			$db->pquery($query6, array($last_entity_record,$type));

			$sequence_no = 1;
			
			while($row = $db->fetch_array($result2)) {	

				$m3 = round(($row['cargo_length'] * $row['cargo_width'] * $row['cargo_height']) * $row['quantity'],2);

				$db->pquery($query3, array($last_entity_record,
																		14244,
																		$sequence_no,
																		$row['quantity'],
																		$total,$row['measure'],
																		$row['cargo_wgt'],
																		$row['cargo_length'],
																		$row['cargo_width'],
																		$row['cargo_height'],
																		'CRM',
																		$row['cargo_wgt'],
																		$row['cargo_length'],
																		$row['cargo_width'],
																		$row['cargo_height'],
																		$row['cargo_wgt'],
																		$row['cargo_length'],
																		$row['cargo_width'],
																		$row['cargo_height'],
																		$m3,
																		2));

				$sequence_no++;
			}
			$this->exportSalesOrderToMetrika($last_entity_record);

		}

		function exportSalesOrderToMetrika($salesorderid){
			$adb = PearDatabase::getInstance();
			$order_sent = 'Sent';  
			$order_not_sent = 'Not Sent'; 
		
			$result_data = array('order' => array()); 
			
			$sql = "SELECT
													s.salesorderid as order_id,
													s.total as finish_price,
													sc.cf_855 as order_type,
													sc.cf_1374 as price,
													sc.cf_1376 as agreed_price,
													sc.cf_1564 as load_phone,
													sc.cf_1566 as unload_phone,                                
													s.sostatus AS order_status,   
													s.status,                     
													e.createdtime AS order_date,
													a.accountname AS customer_name,                
													a.account_no AS customer_code,
													a.customer_id,
													s.external_order_id,             
													s.ivaz_no,
													s.source,                                
													e.description AS notes,
													CONCAT(s.load_date_from, ' ', s.load_time_from) AS load_date_from,
													CONCAT(s.load_date_to, ' ', s.load_time_to) AS load_date_to, 
													CONCAT(s.unload_date_from, ' ', s.unload_time_from) AS unload_date_from,
													CONCAT(s.unload_date_to, ' ', s.unload_time_to) AS unload_date_to,                                
													l.load_company,
													l.bill_street AS load_address,
													l.bill_city AS load_municipality,
													l.bill_state AS load_region,
													l.bill_code AS load_zipcode,
													l.bill_country AS load_country,
													l.load_contact,
													h.unload_company,
													h.ship_street AS unload_address,
													h.ship_city AS unload_municipality,
													h.ship_state AS unload_region,
													h.ship_code AS unload_zipcode,
													h.ship_country AS unload_country,
													h.unload_contact,
													a.legal_vat_code AS unload_vat_code,
													i.productid,  
													i.external_load_id,                              
													i.quantity AS cargo_qty,
													i.cargo_measure,
													i.cargo_wgt,
													i.cargo_length,
													i.cargo_width,
													i.cargo_height,
													i.comment AS cargo_notes,
													r.serviceid,
													i.service,
													i.margin
															FROM vtiger_salesorder s
															JOIN vtiger_salesordercf sc ON sc.salesorderid = s.salesorderid
															JOIN vtiger_crmentity e ON e.crmid = s.salesorderid
															JOIN vtiger_sobillads l ON l.sobilladdressid = s.salesorderid
															JOIN vtiger_soshipads h ON h.soshipaddressid = s.salesorderid                            
															JOIN vtiger_users u ON u.id = e.smownerid 
															JOIN vtiger_account a ON a.accountid = s.accountid 
															JOIN vtiger_inventoryproductrel i ON i.id = s.salesorderid
															LEFT JOIN vtiger_products p ON p.productid = i.productid
															LEFT JOIN vtiger_service r ON r.serviceid = i.productid                                    
															WHERE e.deleted = 0 AND s.salesorderid = ?";

			$result = $adb->pquery($sql, array($salesorderid));

			$orders = array();
			
	while($row = $adb->fetch_array($result)) {
		$date = date("Y-m-d H:i:s");
		$salesorderid = intval($row['order_id']);

		$orders[$salesorderid]['customer_id'] = intval($row['customer_id']);

		if(empty($row['external_order_id'])) {
				$orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
				// $orders[$salesorderid]['order_id'] = ''; 
		} else {
				$orders[$salesorderid]['crm_id'] = intval($row['order_id']);  
				$orders[$salesorderid]['order_id'] = $row['external_order_id']; 
		} 
		
		if($row['order_type'] == 'Transporto užsakymas'){
				$shipment_type = 'parcel';
		}else{
				$shipment_type = 'movement';
		}     
		

		$orders[$salesorderid]['ivaz_no'] = $row['ivaz_no'];  
		$orders[$salesorderid]['shipment_type'] = $shipment_type;
		$orders[$salesorderid]['notes'] = iconv(mb_detect_encoding($row['notes']), "UTF-8", $row['notes']);
		$orders[$salesorderid]['price'] = (float)$row['price'];
		$orders[$salesorderid]['price_agreed'] = (float)$row['agreed_price'];
		$orders[$salesorderid]['finish_price'] = (float)$row['finish_price'];
		
		$orders[$salesorderid]['load_date_from'] = date("Y-m-d H:i:s", strtotime($row['load_date_from'])); 

		if(!empty($row['load_date_to'])){       
				$orders[$salesorderid]['load_date_to'] = date("Y-m-d H:i:s", strtotime($row['load_date_to']));   
		} else {
				$orders[$salesorderid]['load_date_to'] = '';      
		}  

		$load_company = preg_replace('/[^A-Za-z0-9 ]/', '', $row['load_company']);
		$load_company = str_replace("quot", "", $load_company);   


		if(!empty($row['customer_id'])) {
				if(empty($row['load_company'])){       
						$orders[$salesorderid]['load_company'] = $row['customer_name'];          
				} else {
						$orders[$salesorderid]['load_company'] = $load_company;
				}        
		} else {
						$orders[$salesorderid]['load_company'] = $row['customer_name']; 
		} 
		
		$load_address = str_replace('&Scaron;', 'Š', $row['load_address']);
		$load_address = str_replace('&scaron;', 'š', $row['load_address']);


		$orders[$salesorderid]['load_address'] = $load_address;
		$orders[$salesorderid]['load_settlement'] = '';
		$orders[$salesorderid]['load_municipality'] = 'Vilnius';   
		// $orders[$salesorderid]['load_municipality'] = $row['load_municipality'];   
		$orders[$salesorderid]['load_region'] = $row['load_region'];                      
		$orders[$salesorderid]['load_zipcode'] = $row['load_zipcode'];
		
				//if($row['load_zipcode'] == 'LTU') {
				//$orders[$salesorderid]['load_zipcode'] = ''; 
				//}
		
		$orders[$salesorderid]['load_country'] = 'LTU';  
		// $orders[$salesorderid]['load_country'] = $row['load_country'];  
		$orders[$salesorderid]['load_person'] = $row['load_contact'];  


		$orders[$salesorderid]['load_phone'] = $row['load_phone'];


		if(empty($row['load_company'])){       
				$orders[$salesorderid]['load_company_code'] = '';            
		} else {
				$orders[$salesorderid]['load_company_code'] = '';
		}    

		$orders[$salesorderid]['unload_date_from'] = date("Y-m-d H:i:s", strtotime($row['unload_date_from']));

		if(!empty($row['unload_date_to'])){       
				$orders[$salesorderid]['unload_date_to'] = date("Y-m-d H:i:s", strtotime($row['unload_date_to']));  
		} else {
				$orders[$salesorderid]['unload_date_to'] = '';      
		} 


		 $unload_company = preg_replace('/[^A-Za-z0-9 ]/', '', $row['unload_company']); 

		 $unload_company = str_replace("quot", "", $unload_company);         

		if(empty($row['unload_company'])){       
				$orders[$salesorderid]['unload_company'] = $row['customer_name'];          
		} else {
				$orders[$salesorderid]['unload_company'] = $unload_company;
		}

		$unload_address = str_replace('&Scaron;', 'Š', $row['unload_address']);
		$unload_address = str_replace('&scaron;', 'š', $row['unload_address']);

		$orders[$salesorderid]['unload_address'] = $unload_address; 
		$orders[$salesorderid]['unload_settlement'] = '';        
		$orders[$salesorderid]['unload_municipality'] = 'Kaunas';         
		// $orders[$salesorderid]['unload_municipality'] = $row['unload_municipality'];         
		$orders[$salesorderid]['unload_region'] = $row['unload_region'];                      
		$orders[$salesorderid]['unload_zipcode'] = $row['unload_zipcode']; 
		
				//if($row['unload_zipcode'] == 'LTU') {
				//$orders[$salesorderid]['unload_zipcode'] = ''; 
				//}            
		
		// $orders[$salesorderid]['unload_country'] = $row['unload_country']; 
		$orders[$salesorderid]['unload_country'] = 'LTU'; 
		$orders[$salesorderid]['unload_person'] = $row['unload_contact'];  ;  
		$orders[$salesorderid]['unload_phone'] = $row['unload_phone'];

		if(empty($row['unload_company'])){       
				$orders[$salesorderid]['unload_company_code'] = '';            
		} else {
				$orders[$salesorderid]['unload_company_code'] = '';
		}

		if(empty($row['unload_company'])){       
				$orders[$salesorderid]['unload_vat_code'] = '';            
		} else {
				$orders[$salesorderid]['unload_vat_code'] = '';
		}

		$orders_items = array(); 
		$orders_services = array(); 
		$delete_service = array(); 
	

		if($row['productid'] == 36641){
				$orders_services['service_type_id'] = $row['service'];
				$orders_services['quantity_agreed'] = $row['cargo_qty'];
				$orders_services['price_agreed'] = $row['margin'];

				$orders_services['remarks'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

		}

		
 
				$orders_items['cargo']['load_id'] = $row['external_load_id'];
				$orders_items['cargo']['update_type'] = 2;

		if($row['productid'] != 36641){
				$orders_items['cargo']['cargo_qty'] = ($row['cargo_qty'] < 1 ) ? intval(1.000) : intval($row['cargo_qty']);
				if($row['cargo_measure'] == 0){
					$orders_items['cargo']['cargo_measure'] = 1;
				}else{
					$orders_items['cargo']['cargo_measure'] = $row['cargo_measure'];
			}


				if($row['cargo_measure'] == null){
						$orders_items['cargo']['cargo_measure'] = 1;
				}

				if(($row['cargo_measure'] == 1 OR $row['cargo_measure'] == 2 OR $row['cargo_measure'] == 3) OR ($row['cargo_measure'] == 'pll' OR $row['cargo_measure'] == 'RUS-pll' OR $row['cargo_measure'] == 'FIN-pll')){
						$orders_items['cargo']['volume_unit2'] = intval($row['cargo_qty']);
				}else{
						$orders_items['cargo']['volume_unit2'] = 0;
				}
			
				// itoma
				$orders_items['cargo']['cargo_wgt'] = (float)str_replace(",",".", $row['cargo_wgt']);
				$orders_items['cargo']['cargo_length'] = (float)str_replace(",",".", $row['cargo_length']);      
				$orders_items['cargo']['cargo_width'] = (float)str_replace(",",".", $row['cargo_width']);      
				$orders_items['cargo']['cargo_height'] = (float)str_replace(",",".", $row['cargo_height']);   

				$orders_items['cargo']['cargo_ldm'] = sprintf('%0.2f', $row['cargo_ldm']);
				$orders_items['cargo']['cargo_facilities'] = '';        
				$orders_items['cargo']['cargo_termo'] = 0;  
				$orders_items['cargo']['cargo_return_documents'] = intval($row['cargo_return_documents']);
			
				$orders_items['cargo']['cargo_notes'] = iconv(mb_detect_encoding($row['cargo_notes']), "UTF-8", $row['cargo_notes']);

				if($row['source'] == 'CRM') {
						if($row['status'] == 'ERROR' OR $row['status'] == ''){ 
								$orders_items['cargo']['update_type'] = 1;
								$insertType = 1; 
						}else{				
								$orders_items['cargo']['update_type'] = 1;  
								$insertType = 1;  								
						}
				}  
		}    

		
			if($row['serviceid'] == NULL) {  
					if($row['productid'] != 36641){    
							$orders[$salesorderid]['cargos'][] = $orders_items;        
					}

					if($row['productid'] == 36641){  
							$orders[$salesorderid]['additional_services'][] = $orders_services;  
					}          
			}    
														
				$orders_indexed = array_values($orders);

				$result_data['order'] = $orders_indexed;  
	}
						
		$res = json_encode($result_data, JSON_UNESCAPED_UNICODE);

		$url = "https://uzsakymai.parnasas.lt/import/crm/customers.php?insert_new_customer=1";

		if($_SERVER['REMOTE_ADDR'] != '127.0.0.1'){
			$url = "https://demo-uzsakymai.parnasas.lt/import/crm/orders.php";
		}


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);       
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas', 'orders' => $res));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);         

		$data = json_decode($result, true);

	  $state = $data[0];
                
			if($state == 'ERROR') {    
					$order_id = $data[0]['crm_id'];
					$errors = $data[1]['errors'][0]['error_message'];      
				
					//Atnaujiname uzsakymo busena           
					$sql = 'UPDATE vtiger_salesorder SET sostatus = ?, status = ?, errors = ? WHERE salesorderid = ?';
					$result = $adb->pquery($sql, array($order_not_sent, $state, $errors, $salesorderid));

					//Atnaujiname uzsakymo redagavimo data    
					$sql = 'UPDATE vtiger_crmentity SET modifiedtime = ? WHERE crmid = ?';
					$result = $adb->pquery($sql, array($date, $salesorderid));                     
					
			} else {    					
				$order_id = $data[0]['order']['crm_id'];
				$external_order_id = $data[0]['order']['order_id'];   
				$shipment_code = $data[0]['order']['shipment_code'];
				$ivaz_no = $data[0]['order']['ivaz_no'];
				$barcode = $data[0]['order']['barcode'];
				$direction = $data[0]['order']['direction']; 
				$status =  $data[0]['status']; 
				$errors = '';


									//Atnaujiname uzsakymo busena           
					if($insertType == 1){
							$sql = 'UPDATE vtiger_salesorder SET sostatus = ?, ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?';
							$result = $adb->pquery($sql, array($order_sent, $ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
					}else{
						$sql = 'UPDATE vtiger_salesorder SET ivaz_no = ?, barcode = ?, direction = ?, status = ?, errors = ?, external_order_id = ?, shipment_code = ? WHERE salesorderid = ?';
												$result = $adb->pquery($sql, array($ivaz_no, $barcode, $direction, $status, $errors, $external_order_id, $shipment_code, $order_id));
					}
						$sql2 = 'UPDATE vtiger_inventoryproductrel SET external_order_id = ?, external_load_id = ? WHERE id = ? AND sequence_no = ?';
												
						$seq = 1;
						foreach($data[0]['order']['loads'] as $load_id){                  
								$result = $adb->pquery($sql2, array($external_order_id, $load_id, $order_id, $seq ));
								$seq++;
					}								


					//Atnaujiname uzsakymo redagavimo data    
					$sql = 'UPDATE vtiger_crmentity SET label = ?, modifiedtime = ? WHERE crmid = ?';
					$result = $adb->pquery($sql, array($shipment_code, $date, $order_id));


			}

	} 
	
			
		function last_entity_id($adb){
			$sql = "SELECT * FROM vtiger_crmentity_seq";   
			$result = $adb->pquery($sql, array());
			$crmid = $adb->query_result($result,0,'id');
			$crmid = $crmid+1;		

			// Update sequence values
			$sql2 = "UPDATE vtiger_crmentity_seq SET id = ?";
			$result = $adb->pquery($sql2,array($crmid));

			return $crmid;
		}

		function insert_entity($adb, $setype, $entity_id, $user_id, $description, $label, $date) {
			$date = date("Y-m-d H:i:s");  
			// Insert into vtiger_crmentity 
			$sql = "INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, createdtime, modifiedtime,source) 
			VALUES (?,?,?,?,?,?,?,?,?)";
			$result = $adb->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date, 'CRM'));	
		}


		function getFinalPrices() {
			$db = PearDatabase::getInstance();
			$id = $this->getId();

			$query = "SELECT pricebook_price,agreed_price,total FROM vtiger_leaddetails WHERE leadid = ?";
			$result = $db->pquery($query,array($id));

			return $db->fetch_array($result);
		}
		

		function getProducts() {
			$db = PearDatabase::getInstance();
			$id = $this->getId();
			
			
			$query = "SELECT *,app_measures.code FROM vtiger_leads_cargo 
												 LEFT JOIN app_measures ON app_measures.id=vtiger_leads_cargo.measure
								WHERE vtiger_leads_cargo.id = ?";		
			$result = $db->pquery($query,array($id));

			$rowCount = $db->num_rows($result);
			$relatedProducts = array();
		
			for($i = 1; $i <= $rowCount;$i++){
				$relatedProducts[$i]['cargo_wgt'.$i] = $db->query_result($result,$i-1,'cargo_wgt');
				$relatedProducts[$i]['cargo_length'.$i] = $db->query_result($result,$i-1,'cargo_length');
				$relatedProducts[$i]['cargo_width'.$i] = $db->query_result($result,$i-1,'cargo_width');
				$relatedProducts[$i]['cargo_height'.$i] = $db->query_result($result,$i-1,'cargo_height');
				$relatedProducts[$i]['measure'.$i] = $db->query_result($result,$i-1,'measure');
				$relatedProducts[$i]['measure_code'.$i] = $db->query_result($result,$i-1,'code');
				$relatedProducts[$i]['qty'.$i] = $db->query_result($result,$i-1,'quantity');
				$relatedProducts[$i]['comment'.$i] = $db->query_result($result,$i-1,'comment');
				$relatedProducts[$i]['productName'.$i] = $db->query_result($result,$i-1,'cargo_wgt')." ".$db->query_result($result,$i-1,'cargo_length')."x".$db->query_result($result,$i-1,'cargo_width')."x".$db->query_result($result,$i-1,'cargo_height') ;
			}


	
			return $relatedProducts;
		}

		function getMeasure() {
			$db = PearDatabase::getInstance();					
			$query = "SELECT * FROM app_measures";
			$result = $db->query($query);
			$measures = array();
			foreach($result AS $row){
				$measures[] = $row;
			}	
			return $measures;
		}

}
