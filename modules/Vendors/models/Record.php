<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vendors_Record_Model extends Vtiger_Record_Model {

	function getCreatePurchaseOrderUrl() {
		$purchaseOrderModuleModel = Vtiger_Module_Model::getInstance('PurchaseOrder');

		return "index.php?module=".$purchaseOrderModuleModel->getName()."&view=".$purchaseOrderModuleModel->getEditViewName()."&vendor_id=".$this->getId()."&sourceModule=".$this->getModuleName()."&sourceRecord=".$this->getId();
	}

	/**
	 * Function to get List of Fields which are related from Vendors to Inventyory Record
	 * @return <array>
	 */
	public function getInventoryMappingFields() {
		return array(
				//Billing Address Fields
				array('parentField'=>'city', 'inventoryField'=>'bill_city', 'defaultValue'=>''),
				array('parentField'=>'street', 'inventoryField'=>'bill_street', 'defaultValue'=>''),
				array('parentField'=>'state', 'inventoryField'=>'bill_state', 'defaultValue'=>''),
				array('parentField'=>'postalcode', 'inventoryField'=>'bill_code', 'defaultValue'=>''),
				array('parentField'=>'country', 'inventoryField'=>'bill_country', 'defaultValue'=>''),
				array('parentField'=>'pobox', 'inventoryField'=>'bill_pobox', 'defaultValue'=>''),

				//Shipping Address Fields
				array('parentField'=>'street', 'inventoryField'=>'ship_street', 'defaultValue'=>''),
				array('parentField'=>'city', 'inventoryField'=>'ship_city', 'defaultValue'=>''),
				array('parentField'=>'state', 'inventoryField'=>'ship_state', 'defaultValue'=>''),
				array('parentField'=>'postalcode', 'inventoryField'=>'ship_code', 'defaultValue'=>''),
				array('parentField'=>'country', 'inventoryField'=>'ship_country', 'defaultValue'=>''),
				array('parentField'=>'pobox', 'inventoryField'=>'ship_pobox', 'defaultValue'=>'')
		);
	}

  public function	getCostCenters(){
		$db = PearDatabase::getInstance();					
		$query = "SELECT costcenterid AS costid,costcenter_tks_cost AS costname 
							FROM vtiger_costcenter c
							JOIN vtiger_crmentity e ON e.crmid=c.costcenterid
							WHERE deleted = 0
							ORDER BY costcenterid";
		$result = $db->query($query);
		$costcenters = array();
		foreach($result AS $row){
			$costcenters[] = $row;
		}	
		return $costcenters;
	}

	public function getTaxRegions(){
		$db = PearDatabase::getInstance();		
		$query = "SELECT * FROM vtiger_taxregions ORDER BY regionid";
	
		$result = $db->query($query);
		$tax = array();
		foreach($result AS $row){
			$tax[] = $row;
		}	
		return $tax;
	}

	function standingOrdersStatus($record){
		$db = PearDatabase::getInstance();	
		$query = "SELECT cf_2064 FROM vtiger_vendorcf WHERE vendorid = ?";
		$result = $db->pquery($query,array($record));
	  $status =	$db->query_result($result,0,'cf_2064');

		return $status;
	}

	function saveTemplate($req){
		$recordid = $req->get('record');
		$db = PearDatabase::getInstance();
		global $current_user; 
		
		$query = "INSERT INTO app_standing_purchaseorders (accountid, title, invoice_type, invoice_date, note, total, total_with_vat, created_person, createdtime) VALUES (?,?,?,?,?,?,?,?,?)";

		$query2 = "INSERT INTO app_standing_purchaseorder_rows (id, sequence_no, quantity, productid, comment, listprice, vat, service, costcenter, object, employee) VALUES (?,?,?,?,?,?,?,?,?,?,?)";		

		$execute = array($recordid,$req->get('template_name'),$req->get('type'),$req->get('invoice_date'),$req->get('description'),$req->get('netTotal'),$req->get('grandTotal'),$current_user->id,date("Y-m-d H:i:s"));

		$db->pquery($query,$execute);

		$totalProductCount = $req->get('totalProductCount');

	 	$last_id = $db->getLastInsertID();

		for($i = 1; $i <= $totalProductCount;$i++){
			$execute2 = array($last_id,$i,$req->get('qty'.$i),$req->get('hdnProductId'.$i),$req->get('comment'.$i),$req->get('fix-price'.$i),$req->get('region'.$i),$req->get('productName'.$i),$req->get('costcenter'.$i),$req->get('object'.$i),$req->get('employee'.$i));
			$db->pquery($query2,$execute2);
		}

		$referer = $_SERVER['PHP_SELF']."?module=Vendors&view=Detail&record=$recordid";
    return header("Location: $referer");

	}


	function editTemplate($req){
		$recordid = $req->get('record');
		$last_id = $req->get('template_id');
		$db = PearDatabase::getInstance();	

		$query = "UPDATE app_standing_purchaseorders SET title = ?, invoice_type = ?, invoice_date = ?, note = ?, total = ?, total_with_vat = ? WHERE accountid = ? AND id = ?";

		$query2 = "INSERT INTO app_standing_purchaseorder_rows (id, sequence_no, quantity, productid, comment, listprice, vat, service, costcenter, object, employee) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

		$query3 = "DELETE FROM app_standing_purchaseorder_rows WHERE id = ?";

		$execute = array($req->get('template_name'),$req->get('type'),$req->get('invoice_date'),$req->get('description'),$req->get('netTotal'),$req->get('grandTotal'),$recordid,$last_id);

		$db->pquery($query,$execute);

		$db->pquery($query3,array($last_id));

		$totalProductCount = $req->get('totalProductCount');

	 
		for($i = 1; $i <= $totalProductCount;$i++){
			$execute2 = array($last_id,$i,$req->get('qty'.$i),$req->get('hdnProductId'.$i),$req->get('comment'.$i),$req->get('fix-price'.$i),$req->get('region'.$i),$req->get('productName'.$i),$req->get('costcenter'.$i),$req->get('object'.$i),$req->get('employee'.$i));
			$db->pquery($query2,$execute2);
		}

		$referer = $_SERVER['PHP_SELF']."?module=Vendors&view=Detail&record=$recordid";
    return header("Location: $referer");

	}


	function getTemplate(){
		$db = PearDatabase::getInstance();
		$record = $this->getId();

		$query = "SELECT s.*, CONCAT(u.first_name,' ',u.last_name) AS creator 
											 FROM app_standing_purchaseorders s										
											 LEFT JOIN vtiger_users u ON u.id=s.created_person
											 WHERE accountid = ? ORDER BY s.id DESC";		

		$query2 = "SELECT * FROM app_standing_purchaseorder_rows WHERE id = ? ORDER BY sequence_no DESC";

		$result = $db->pquery($query,array($record));
		$templates = array();		

		foreach($result AS $row){
			$result2 = $db->pquery($query2,array($row['id']));
			$loads = array();
			$n = 1;
			foreach($result2 AS $load){
				$loads[$n] = array('productName'.$n => $load['service'],
													 'comment'.$n => $load['comment'],
												   'productid'.$n => $load['productid'],											  	
											 		 'costcenter'.$n => $load['costcenter'],													
													 'object'.$n => $load['object'],
													 'employee'.$n => $load['employee'],
													 'qty'.$n => $load['quantity'],	
													 'listprice'.$n => $load['listprice'],			
													 'region'.$n => $load['vat']
												);
				$n++;
			}

			$templates[] = array('record' => $row['id'],
												   'enabled' => $row['enabled'],
													 'template_name' => $row['title'],
													 'invoice_type' => $row['invoice_type'],
													 'invoice_date' => date("Y-m-").($row['invoice_date'] < 10 ? '0':'').$row['invoice_date'],
													 'day' => $row['invoice_date'],
													 'description' => $row['note'],						 
													 'total' => $row['total'],
													 'total_with_vat' => $row['total_with_vat'],
													 'creator' => $row['creator'],
													 'createdtime' => $row['createdtime'],
													 'loads' => $loads);
		
		}
		return $templates;
	}
}
