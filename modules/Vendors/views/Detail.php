<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

class Vendors_Detail_View extends Vtiger_Detail_View {

	/**
	 * Function to get activities
	 * @param Vtiger_Request $request
	 * @return <List of activity models>
	 */


	public function showModuleDetailView(Vtiger_Request $request) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();

		// Getting model to reuse it in parent 
		if (!$this->record) {
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();

		if($_POST){
			if($_POST['edit'] == 1){
				$recordModel->editTemplate($request);				
			}else{
				$recordModel->saveTemplate($request);
			}
		}	

		$standingOrdersStatus = $recordModel->standingOrdersStatus($recordId);
		$get_templates = 	$recordModel->getTemplate();

		$viewer = $this->getViewer($request);
		$costcenters = $recordModel->getCostCenters();
		$getTaxRegions = $recordModel->getTaxRegions();
		$viewer->assign('STANDING_ORDER_STATUS', $standingOrdersStatus);
		$viewer->assign('COSTCENTERS', $costcenters);
		$viewer->assign('TAX_REGIONS', $getTaxRegions);
		$viewer->assign('GET_TEMPLATES', $get_templates);

		return parent::showModuleDetailView($request);
	}

	function showModuleSummaryView($request) {
		$recordId = $request->get('record');
		$moduleName = $request->getModule();

		// Getting model to reuse it in parent 
		if (!$this->record) {
			$this->record = Vtiger_DetailView_Model::getInstance($moduleName, $recordId);
		}
		$recordModel = $this->record->getRecord();

		if($_POST){
			if($_POST['edit'] == 1){
				$recordModel->editTemplate($request);				
			}else{
				$recordModel->saveTemplate($request);
			}
		}	

		$standingOrdersStatus = $recordModel->standingOrdersStatus($recordId);
		$get_templates = 	$recordModel->getTemplate();

		$viewer = $this->getViewer($request);
		$costcenters = $recordModel->getCostCenters();
		$getTaxRegions = $recordModel->getTaxRegions();
		$viewer->assign('STANDING_ORDER_STATUS', $standingOrdersStatus);
		$viewer->assign('COSTCENTERS', $costcenters);
		$viewer->assign('TAX_REGIONS', $getTaxRegions);
		$viewer->assign('GET_TEMPLATES', $get_templates);

		return $viewer->view('ModuleSummaryView.tpl', $moduleName, true);
	}

}
