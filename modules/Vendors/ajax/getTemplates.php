<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
  
    $vendorid = $_POST['vendorid'];

    $check = mysqli_fetch_assoc($conn->query("SELECT cf_2064 AS enabled FROM vtiger_vendorcf WHERE vendorid = $vendorid"));

    if(!$check['enabled']){
      echo json_encode(array('status'=> false));
      die;
    }

    $query = "SELECT s.*, CONCAT(u.first_name,' ',u.last_name) AS creator 
                        FROM app_standing_purchaseorders s										
                        LEFT JOIN vtiger_users u ON u.id=s.created_person
                        WHERE accountid = $vendorid AND enabled = 1 
                        ORDER BY s.id DESC";		

		$result = $conn->query($query);
		$templates = array();

		foreach($result AS $row){
			$result2 = $conn->query("SELECT r.*,c.costcenter_tks_cost AS costname, IF(r.vat = 0, 'Standartinis 21%',t.name) as name 
                                        FROM app_standing_purchaseorder_rows r
                                        LEFT JOIN vtiger_taxregions t ON t.regionid=r.vat
                                        LEFT JOIN vtiger_costcenter c ON c.costcenterid=r.costcenter
                                        WHERE r.id = '".$row['id']."' 
                                        ORDER BY sequence_no DESC");
			$loads = array();
			$n = 0;
			foreach($result2 AS $load){
				$loads[$n] = array('productName' => $load['service'],
													 'comment' => $load['comment'],
												   'productid' => $load['productid'],											  	 
											 		 'costcenter' => $load['costcenter'],													
											 		 'costname' => $load['costname'],													
													 'object' => $load['object'],
													 'employee' => $load['employee'],
													 'qty' => $load['quantity'],	
													 'listprice' => $load['listprice'],			
													 'region' => $load['vat'],
                           'region_name' => $load['name']
												);
				$n++;
			}

			$templates[] = array('record' => $row['id'],
													 'vendorid' => $vendorid,
												   'enabled' => $row['enabled'],
													 'template_name' => $row['title'],
													 'invoice_type' => $row['invoice_type'],
													 'invoice_date' => date("Y-m-").($row['invoice_date'] < 10 ? '0':'').$row['invoice_date'],
													 'day' => $row['invoice_date'],
													 'description' => $row['note'],						 
													 'total' => $row['total'],
													 'total_with_vat' => $row['total_with_vat'],
													 'creator' => $row['creator'],
													 'createdtime' => $row['createdtime'],
													 'loads' => $loads);
		
		}


		echo json_encode(array('status' => true, 'templates' => $templates));


}else{
  http_response_code(404);
}
  

