<?php

class Purchaseorderdraft_Module_Model extends Vtiger_Module_Model {

  public function isSummaryViewSupported() {
      return false;
  } 
  
  
  public static function getRoutesPrices(array $route_codes){
    $imploded_route_codes = "'" .implode("', '",$route_codes). "'";      
    $select = 'ROUTE_CODE, M_COST AS pajamos, COST_KM_DIFF AS pajamos_km, PRICE AS savikaina_eur, COST_AGREED AS vez_kaina, PROFIT AS pelnas';
    $table = 'ROUTES_ANALYTICS_V2_FOR_CRM_View'; 
    $where = "ROUTE_CODE IN ($imploded_route_codes)";

    $headers = array(
      'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => $select, 'table' => $table, 'where' => $where));				
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);

    $routes = [];

    foreach ($data as $row) {
      $routes['total'] += $row['pajamos'];
      $routes[$row['ROUTE_CODE']] = $row;
    }

    return $routes;
  }

  public function getSpecificRoutePriceByRecordId($recordid){  
    $db = PearDatabase::getInstance();

    $get_route_code = $db->pquery('SELECT purchaseorderdraft_tks_route_code FROM vtiger_purchaseorderdraft WHERE purchaseorderdraftid = ?', [$recordid]);
    $route_code = $db->query_result($get_route_code, 0, 'purchaseorderdraft_tks_route_code');
    
    $select = 'M_COST AS pajamos, COST_KM_DIFF AS pajamos_km, PRICE AS savikaina_eur, COST_AGREED AS vez_kaina, PROFIT AS pelnas';
    $table = 'ROUTES_ANALYTICS_V2_FOR_CRM_View'; 
    $where = "ROUTE_CODE = '$route_code'";

    $headers = array(
      'Authorization : Basic a30cae9045be6ddd53ec5e3c3a842452'	
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://sandelis.parnasas.lt:8080/ParnasasApi/parnasas/select");              
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('select' => $select, 'table' => $table, 'where' => $where));				
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);
    
    return $data[0];
  }

}