<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Projects_Record_Model extends Vtiger_Record_Model {

  function getProducts() {
    $db = PearDatabase::getInstance();
    $id = $this->getId();

    $query = "SELECT *,app_measures.code FROM vtiger_projects_cargo 
                       LEFT JOIN app_measures ON app_measures.id=vtiger_projects_cargo.measure
              WHERE vtiger_projects_cargo.id = ?";		
    $result = $db->pquery($query,array($id));

    $rowCount = $db->num_rows($result);
    $relatedProducts = array();
  
    for($i = 1; $i <= $rowCount;$i++){
      $relatedProducts[$i]['cargo_wgt'.$i] = $db->query_result($result,$i-1,'cargo_wgt');
      $relatedProducts[$i]['cargo_length'.$i] = $db->query_result($result,$i-1,'cargo_length');
      $relatedProducts[$i]['cargo_width'.$i] = $db->query_result($result,$i-1,'cargo_width');
      $relatedProducts[$i]['cargo_height'.$i] = $db->query_result($result,$i-1,'cargo_height');
      $relatedProducts[$i]['measure'.$i] = $db->query_result($result,$i-1,'measure');
      $relatedProducts[$i]['measure_code'.$i] = $db->query_result($result,$i-1,'code');
      $relatedProducts[$i]['qty'.$i] = $db->query_result($result,$i-1,'quantity');
      $relatedProducts[$i]['comment'.$i] = $db->query_result($result,$i-1,'comment');
      $relatedProducts[$i]['productName'.$i] = $db->query_result($result,$i-1,'cargo_wgt')." ".$db->query_result($result,$i-1,'cargo_length')."x".$db->query_result($result,$i-1,'cargo_width')."x".$db->query_result($result,$i-1,'cargo_height') ;
    }

    return $relatedProducts;
  }

  function getMeasure() {
    $db = PearDatabase::getInstance();					
    $query = "SELECT * FROM app_measures";
    $result = $db->query($query);
    $measures = array();
    foreach($result AS $row){
      $measures[] = $row;
    }	
    return $measures;
  }

  function getFinalPrices() {
    $db = PearDatabase::getInstance();
    $id = $this->getId();

    $query = "SELECT REPLACE(agreed_price,',','') AS agreed_price, REPLACE(total,',','') AS total FROM vtiger_projects WHERE projectsid = ?";
    $result = $db->pquery($query,array($id));

    return $db->fetch_array($result);
  }
}