<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

// class Leads_Detail_View extends Accounts_Detail_View {
  class Projects_Detail_View extends Vtiger_Detail_View {
	/**
	 * Function returns Inventory details
	 * @param Vtiger_Request $request
	 */

   
	function showModuleDetailView(Vtiger_Request $request) {
		$this->showLineItemDetails($request);
		return parent::showModuleDetailView($request);
	}

  function showLineItemDetails(Vtiger_Request $request) {
		$record = $request->get('record');
		$moduleName = $request->getModule();
    $recordModel = Inventory_Record_Model::getInstanceById($record);
		$db = PearDatabase::getInstance();
    $relatedProducts = $recordModel->getProducts();
    $prices = $recordModel->getFinalPrices();
    $viewer = $this->getViewer($request);
    $viewer->assign('MODULE_NAME',$moduleName);
    $viewer->assign('PRICES',$prices);
    $viewer->assign('RELATED_PRODUCTS', $relatedProducts);
    $viewer->assign('RECORD', $recordModel);    
  }


}
