<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class CandidatesHandler extends VTEventHandler {

    function handleEvent($eventName, $entityData)
    {
        global $adb, $current_user;

        if($eventName == 'vtiger.entity.aftersave') {
            $moduleName = $entityData->getModuleName();

            if($moduleName != 'Candidates'){
                return;
            }

            if($moduleName == 'Candidates' && $_REQUEST['module']=='Candidates'){
                $recordId = $entityData->getId();
                $email = $entityData->get('email');
                $firstname = $entityData->get('firstname');

                $tmpl_str = explode(' ',$firstname);
                $firname_string = '';
                foreach ($tmpl_str as $tmp){
                    $firname_string .= $tmp;
                }
                $lastname = $entityData->get('lastname');
                $candidateModule = Vtiger_Module_Model::getCleanInstance('Candidates');
                $user_str_1 = $candidateModule->convertVNtoLatinString($firname_string);
                $user_str_1 = strtolower($user_str_1);
                $user_str_2 = $candidateModule->convertVNtoLatinString($lastname);
                $user_str_2 = strtolower($user_str_2);
                $arr_user_str = explode(" ",$user_str_2);
                $username_str = $user_str_1.".".$arr_user_str[0][0]."".$arr_user_str[1][0];

                $dateofbirth = $entityData->get('date_of_birth');
                $position = $entityData->get('position');
                $mobilePhone = $entityData->get('mobile');
                $city = $entityData->get('cf_2410');

                $start_work_date = $entityData->get('mip_swork');
                $official_date = $entityData->get('mip_swork');
                $desired_salary = $entityData->get('mip_de_salary');
                $official_salary = $entityData->get('official_salary');
                $probality_period = $entityData->get('mip_probation');

                $candidate_action = $entityData->get('candidate_action');

                $adb->pquery("UPDATE vtiger_crmentity SET label = '$firstname $lastname' WHERE crmid = ?",array($recordId));
				
				if($candidate_action != 'Hire'){
                    $candidate_status = 'Potential';
                }else{
                    $candidate_status = 'Active';
                    $rs_user = $adb->pquery("SELECT * FROM vtiger_users WHERE candidate = ?",array($recordId));
                    $count_user = $adb->num_rows($rs_user);

                    $get_position = $adb->pquery("SELECT name FROM vtiger_hrm_position WHERE id = ?", array($position));
                    $position_name = $adb->query_result($get_position,0,'name');

                    $rs_roleid = $adb->pquery("SELECT roleid FROM vtiger_role WHERE rolename = '".trim($position_name)."'",array());
                    $roleid = $adb->query_result($rs_roleid,0,'roleid');
                   
                    $userModuleName = "Users";
                    $userRecordModel = Vtiger_Record_Model::getCleanInstance($userModuleName);
                    $userRecordModel->set('mode', '');
                    $userRecordModel->set('user_name',$username_str);
                    $userRecordModel->set('first_name',$firstname);
                    $userRecordModel->set('last_name',$lastname);
                    $userRecordModel->set('email1',$email);
                    $userRecordModel->set('candidate',$recordId);
                    $userRecordModel->set('user_password','admin');
                    $userRecordModel->set('confirm_password','admin');
                    $userRecordModel->set('roleid',$roleid);                        
                    $userRecordModel->set('is_admin','off');
                    $userRecordModel->set('status',$candidate_status);
                    $userRecordModel->save();
                    $employ_user_id = $userRecordModel->getId();
                    $adb->pquery("UPDATE vtiger_users SET candidate = ? WHERE id = ?",array($recordId,$employ_user_id));
                    

                    $employNumber = 'EMPL'.$employ_user_id;                  
                    $recordModel = Vtiger_Record_Model::getCleanInstance("Employee");
                    $recordModel->set('mode', '');
                    $recordModel->set('firstname',$firstname);
                    $recordModel->set('lastname',$lastname);
                    $recordModel->set('email',$email);
                    $recordModel->set('private_email',$email);
                    $recordModel->set('date_of_birth',$dateofbirth);
                    $recordModel->set('mobile',$mobilePhone);
                    $recordModel->set('position',$position);
                    $recordModel->set('employ_number',$employNumber);
                    $recordModel->set('candidate_id',$recordId);
                    $recordModel->set('user_id',$employ_user_id);
                    $recordModel->set('city',$city);
                    $recordModel->set('cf_2370','Dirba');
                    $recordModel->set('assigned_user_id',$employ_user_id); 
                    $recordModel->set('start_work_date',$start_work_date);
                    $recordModel->set('official_date',$official_date);
                    $recordModel->set('desired_salary',$desired_salary);
                    $recordModel->set('official_salary',$official_salary);
                    $recordModel->set('probality_period',$probality_period);
                    $recordModel->save();
                    $employid = $recordModel->getId();
                    $adb->pquery("UPDATE vtiger_hrm_candidate SET employid = ? WHERE id = ?",array($employid,$recordId));
                    
                }
            }
        }
    }
}

?>
