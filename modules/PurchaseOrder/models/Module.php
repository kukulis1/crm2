<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class PurchaseOrder_Module_Model extends Inventory_Module_Model{

  public function getPurchaseOrders($dateFilter=''){
    $db = PearDatabase::getInstance();
    $today = date("Y-m-d");
    $last_week = date('Y-m-d', strtotime('-15 days'));
    global $current_user;
    $user = $current_user->id;

      $sql = "SELECT p.purchaseorderid,e.crmid, e.createdtime, cf_1355 AS invoice_num, vendorname, FORMAT(total,2) AS total, CONCAT(u.first_name,' ', u.last_name) AS user, CONCAT(cu.first_name,' ', cu.last_name) AS creator, cf_1345 as invoicedate,duedate
                    FROM vtiger_purchaseorder p
                    LEFT JOIN vtiger_purchaseordercf pcf ON pcf.purchaseorderid=p.purchaseorderid
                    LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid
                    LEFT JOIN vtiger_crmentity pe ON pe.crmid=p.purchaseorderid
                    LEFT JOIN vtiger_crmentityrel rel ON rel.crmid=p.purchaseorderid
                    LEFT JOIN vtiger_purchasevise vi ON vi.purchaseviseid=rel.relcrmid
                    LEFT JOIN vtiger_crmentity e ON e.crmid=vi.purchaseviseid
                    LEFT JOIN vtiger_users u ON u.id=e.smownerid
                    LEFT JOIN app_vise_purchaseorder vise ON vise.purchaseid=p.purchaseorderid
                    LEFT JOIN vtiger_purchasevisecf cf ON cf.purchaseviseid=vi.purchaseviseid
                    LEFT JOIN vtiger_users cu ON cu.id=cf.cf_1653
                    WHERE pe.deleted = 0 AND vi.purchaseviseid IS NOT NULL AND vise.purchaseid IS NULL AND rel.relmodule = 'Purchasevise' ";

    $params = array();
    	//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$sql .= " AND DATE_FORMAT(pe.createdtime,'%Y-%m-%d') BETWEEN ? AND ? ";
			$params[] = $dateFilter['start'];
      $params[] = date('Y-m-d',  strtotime("+15 day", strtotime($dateFilter['start'])));
      $today = date('Y-m-d',  strtotime("+15 day", strtotime($dateFilter['start'])));
      $last_week = $dateFilter['start'];
		}else{
			$sql .= " AND DATE_FORMAT(pe.createdtime,'%Y-%m-%d') BETWEEN ? AND ? ";
			$params[] = $last_week;
      $params[] = $today;      
		}
		

		$show_filter = "Nuo: ".$last_week." Iki: ".$today; 	

		$sql .= "	GROUP BY rel.crmid
              ORDER BY e.createdtime DESC";

      
    $result = $db->pquery($sql,$params);   		
		$noOfRows = $db->num_rows($result);

		$purchaseOrders = array();
		for($i=0; $i<$noOfRows; $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$purchaseOrders['content'][] = $row;			
    }   


    $purchaseOrders['filter'] = $show_filter;
    $purchaseOrders['user'] = $user;

		return $purchaseOrders;
  }

  public function CancelPurchase($dateFilter=''){
    $db = PearDatabase::getInstance();
    $today = date("Y-m-d");
    $last_week = date('Y-m-d', strtotime('-15 days'));


    global $current_user;
    $role = $current_user->roleid;

    $sql = "SELECT p.purchaseorderid as id,e.createdtime, cf_1355 AS numb, vendorname as name, FORMAT(total,2) AS total,CONCAT(u.first_name,' ', u.last_name) AS user,CONCAT(cu.first_name,' ', cu.last_name) AS creator,cf_1345 as invoicedate,duedate,setype, IF(sh.id IS NOT NULL, 1,0) AS ok
            FROM vtiger_purchaseorder p
            LEFT JOIN vtiger_purchaseordercf cf ON cf.purchaseorderid=p.purchaseorderid
            LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid
            LEFT JOIN vtiger_crmentity e ON e.crmid=p.purchaseorderid   
            LEFT JOIN vtiger_users u ON u.id=e.smownerid   
            LEFT JOIN vtiger_users cu ON cu.id=cf.cf_1643
            LEFT JOIN vtiger_show_calcel_invoices sh ON sh.id=p.purchaseorderid
            WHERE e.deleted = 0  AND cf_1926 = 1";

    $sql2 = "SELECT s.salesorderid as id,e.createdtime, shipment_code AS numb, accountname as name, FORMAT(total,2) AS total,CONCAT(u.first_name,' ', u.last_name) AS creator, setype,IF(sh.id IS NOT NULL, 1,0) AS ok
              FROM vtiger_salesorder s 
              LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
              LEFT JOIN vtiger_salesordercf c ON c.salesorderid=s.salesorderid 
              LEFT JOIN vtiger_account a ON a.accountid=s.accountid 
              LEFT JOIN vtiger_users u ON u.id=c.cf_1661   
              LEFT JOIN vtiger_show_calcel_invoices sh ON sh.id=s.salesorderid 
              WHERE e.deleted = 0 AND cf_1614 = 1 ";        

    $params = array();
    	//handling date filter for history widget in home page
		if(!empty($dateFilter)) {
			$where .= " AND DATE_FORMAT(e.createdtime,'%Y-%m-%d') BETWEEN ? AND ? ";
      $sql .= $where;
      $sql2 .= $where;
			$params[] = $dateFilter['start'];
      $params[] = $dateFilter['end'];
      $today = $dateFilter['end'];
      $last_week = $dateFilter['start'];
		}else{
			$where .= " AND DATE_FORMAT(e.createdtime,'%Y-%m-%d') BETWEEN ? AND ? ";
      $sql .= $where;
      $sql2 .= $where;
			$params[] = $last_week;
      $params[] = $today;      
		}
		

		$show_filter = "Nuo: ".$last_week." Iki: ".$today; 	

		$sql .= " GROUP BY p.purchaseorderid
    	ORDER BY e.createdtime DESC";
    $sql2 .= "GROUP BY s.salesorderid
    	ORDER BY e.createdtime DESC";
     
    $result = $db->pquery($sql,$params);   		
    $result2 = $db->pquery($sql2,$params);   		
		$noOfRows = $db->num_rows($result);

		$array = array();

    foreach($result as $row){
      $array['content'][] = $row;
    }

    foreach($result2 as $row2){
      $array['content'][] = $row2;
    }


    $array['filter'] = $show_filter;
    $array['role'] = $role;

		return $array;
  }
}
?>
