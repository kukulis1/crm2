<?php

error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $purchaseorderid = $_POST['purchaseorderid'];

  $invoice = $conn->query("SELECT DISTINCT p.purchaseorder_no, p.purchaseorderid, FORMAT(p.total,2) AS total, v.vendorname,cf_1345 AS invoicedate, e.createdtime 
                              FROM vtiger_purchaseorder p    
                              JOIN vtiger_purchaseordercf pcf ON pcf.purchaseorderid=p.purchaseorderid
                              LEFT JOIN vtiger_crmentity e ON e.crmid=p.purchaseorderid
                              LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid
                              WHERE e.deleted = 0 AND e.setype = 'PurchaseOrder' AND p.purchaseorderid IN ($purchaseorderid) ");

  $invoice_array = array();

  foreach($invoice as $row){
    $invoice_array[] = $row;
  }

  echo json_encode($invoice_array);

    
}else{
  http_response_code(404);
}
