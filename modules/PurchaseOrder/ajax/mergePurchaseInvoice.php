<?php
error_reporting(E_ALL);
// require_once "../../../config.inc.php";

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   
include $_SERVER['DOCUMENT_ROOT'].'/v1/external_data/utils.php';

if($_POST){



  try {
        $dbh->beginTransaction();  
        $puchaseorderid = $_POST['purchaseorderid']; 
        
        $date = date("Y-m-d H:i:s");
        $sth = $dbh->prepare("SELECT subject,vcf.vendorid,purchaseorder_no,subtotal,total,postatus,balance,cf_1462 AS iban, SUM(listprice) AS totalsum
                              FROM vtiger_purchaseorder                                                        
                              JOIN vtiger_vendorcf vcf ON vcf.vendorid=vtiger_purchaseorder.vendorid 
                              JOIN vtiger_inventoryproductrel i ON i.id=vtiger_purchaseorder.purchaseorderid
                              WHERE purchaseorderid IN ($puchaseorderid)");

        $sth2 = $dbh->prepare("SELECT id,productid,sequence_no,quantity,listprice,comment,tax2,cargo_measure,cargo_wgt,cargo_length,cargo_width,purchase_date 
                               FROM vtiger_inventoryproductrel 
                               WHERE id IN ($puchaseorderid)
                               GROUP BY id");

        $sth3 = $dbh->prepare("UPDATE vtiger_crmentity SET deleted = 1 WHERE crmid = ?");

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());

        $sth2->setFetchMode(PDO::FETCH_ASSOC);
        $sth2->execute(array());         
        
        $po_nr = $dbh->prepare("SELECT cur_id FROM vtiger_modentity_num WHERE semodule = 'PurchaseOrder' AND active = 1");
        $po_nr->setFetchMode(PDO::FETCH_ASSOC);
        $po_nr->execute(array()); 
        $cur_id = $po_nr->fetch()['cur_id'];
        $new_cur_id = $cur_id + 1;

        $update_cur_id = $dbh->prepare("UPDATE vtiger_modentity_num SET cur_id = ? WHERE semodule = 'PurchaseOrder' AND active = 1");
        $update_cur_id->execute(array($new_cur_id)); 

        $insert_purchase = $dbh->prepare("INSERT INTO vtiger_purchaseorder (purchaseorderid,subject,vendorid,purchaseorder_no,subtotal,total,postatus,balance) VALUES (?,?,?,?,?,?,?,?)");
        $insert_purchasecf = $dbh->prepare("INSERT INTO vtiger_purchaseordercf (purchaseorderid,cf_1343,cf_1610) VALUES (?,?,?)");      

        $insert_detail_line = $dbh->prepare("INSERT INTO vtiger_inventoryproductrel (id,productid,sequence_no,quantity,listprice,comment,tax2,cargo_measure,cargo_wgt,cargo_length,cargo_width,purchase_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
       

          $purchaseorder_no = $cur_id;

          $purchaseorder_info = $sth->fetch();

          $purchaseorderid = getEntityId($dbh);
          $insert_purchase->execute(array($purchaseorderid,$purchaseorder_info['subject'],$purchaseorder_info['vendorid'],'PO'.$cur_id,$purchaseorder_info['totalsum'],$purchaseorder_info['totalsum'],'Created',$purchaseorder_info['totalsum']));
          $insert_purchasecf->execute(array($purchaseorderid,'Debetinė',$purchaseorder_info['iban']));


          $seq = 1;
          foreach($sth2 AS $row){                    
            $insert_entity = insert_entity3($dbh, 'PurchaseOrder', $purchaseorderid, 26, NULL,'Metrika',  'PO'.$purchaseorder_no, $date);           
            $insert_detail_line->execute(array($purchaseorderid,36641,$seq,1,$row['listprice'],$row['comment'],1,$row['cargo_measure'],'TRANSPORTO Paslaugos',$row['cargo_length'],103914,$row['purchase_date']));
            $sth3->execute(array($row['id']));
            $seq++;
          }

        echo json_encode(array('status' => 'success'));

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'fail'));
  }
}else{
  http_response_code(404);
} 