<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();  
        
        $vendor_id = $_POST['vendor_id'];

        $sth = $dbh->prepare("SELECT cf_1462 AS iban, cf_1347 AS delay
                              FROM vtiger_vendor v 
                              LEFT JOIN vtiger_vendorcf vcf ON vcf.vendorid=v.vendorid
                              LEFT JOIN vtiger_accountscf acf ON acf.accountid=v.accountid
                              WHERE v.vendorid = ?");

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($vendor_id));

        $result = $sth->fetch();
       
        echo json_encode(array('status' => 'success','data' => $result));

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'fail'));
  }
}else{
  http_response_code(404);
} 