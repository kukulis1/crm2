<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class PriceBooks_Save_Action extends Vtiger_Save_Action {

	/**
	 * Function to save record
	 * @param <Vtiger_Request> $request - values of the record
	 * @return <RecordModel> - record Model of saved record
	 */
	public function saveRecord($request) {
		$recordModel = $this->getRecordModelFromRequest($request);                               
		$recordModel->save();
                
                //Logtime
                $new_pricebook_id = $recordModel->getId(); 
                $pricebook_name = $recordModel->get('bookname');  
                $save_pricebook = $this->SavePricebook($new_pricebook_id,$pricebook_name);
                
		if($request->get('relationOperation')) {
			$parentModuleName = $request->get('sourceModule');
			$parentModuleModel = Vtiger_Module_Model::getInstance($parentModuleName);
			$parentRecordId = $request->get('sourceRecord');
			$relatedModule = $recordModel->getModule();
			$relatedRecordId = $recordModel->getId();

			$relationModel = Vtiger_Relation_Model::getInstance($parentModuleModel, $relatedModule);
			$relationModel->addRelation($parentRecordId, $relatedRecordId);

			//To store the relationship between Products/Services and PriceBooks
			if ($parentRecordId && ($parentModuleName === 'Products' || $parentModuleName === 'Services')) {
				$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentRecordId, $parentModuleName);
				$recordModel->updateListPrice($parentRecordId, $parentRecordModel->get('unit_price'));
			}
		}
		return $recordModel;
	}        
        
//Logtime
public function SavePricebook($new_pricebook_id,$pricebook_name){    
    
global $adb,$log;
           
$default_pricebook = 'BAZINISLT';
$source = 'Generated';

$log->debug("Entering into function CreatePricebook(".$new_pricebook_id.").");

$adb->startTransaction();
    
    $sql = "SELECT pp.* FROM vtiger_pricebookproductrel pp
                                JOIN vtiger_pricebook p ON p.pricebookid = pp.pricebookid
                                JOIN vtiger_crmentity e ON e.crmid = p.pricebookid
            WHERE p.bookname = ? LIMIT 1";

    $result = $adb->pquery($sql, array($default_pricebook));

    while($res = $adb->fetch_array($result))
    {

        $productid = $res["productid"];
        $listprice = $res["listprice"];
        
        $sql_new = "SELECT p.*, e.* FROM vtiger_products p
                          JOIN vtiger_crmentity e ON e.crmid = p.productid
                          WHERE e.deleted = 0 AND p.productid = ?";

        $result_new = $adb->pquery($sql_new, array($productid));

        while($res_new = $adb->fetch_array($result_new))
        {

            $productname = html_entity_decode($pricebook_name . '_' . $res_new["productname"], ENT_NOQUOTES, 'UTF-8');   
            $productcategory = html_entity_decode($res_new["productcategory"], ENT_NOQUOTES, 'UTF-8');
            $discontinued = $res_new["discontinued"];
            $qtyinstock = $res_new["qtyinstock"];
            $min_weight_kg = $res_new["min_weight_kg"];    
            $max_weight_kg = $res_new["max_weight_kg"];
            $min_volume_m3 = $res_new["min_volume_m3"];
            $max_volume_m3 = $res_new["max_volume_m3"];    
            $consignee = html_entity_decode($res_new["consignee"], ENT_NOQUOTES, 'UTF-8');  
            $min_square_m2 = $res_new["min_square_m2"];  
            $max_square_m2 = $res_new["max_square_m2"];
            $pll_pcs = $res_new["pll_pcs"];    
            $description = html_entity_decode($res_new["description"], ENT_NOQUOTES, 'UTF-8');
            $smcreatorid = $res_new["smcreatorid"];
            $smownerid = $res_new["smownerid"];        

            $created_modified_date = date("Y-m-d H:i:s");

            $sql2 = $adb->pquery("SELECT * FROM vtiger_crmentity_seq", array());
            $id = $adb->query_result($sql2, 0, 'id');
            $new_product_id = $id + 1;

            $sql3 = "INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $result3 = $adb->pquery($sql3, array($new_product_id, $smcreatorid, $smownerid, 'Products', $description, $created_modified_date, $created_modified_date)); 

            $sql4 = "UPDATE vtiger_crmentity_seq SET id = ?";
            $result4 = $adb->pquery($sql4, array($new_product_id));

            $sql6 = $adb->pquery("SELECT cur_id FROM vtiger_modentity_num WHERE semodule = 'Products' AND prefix = 'PRO'", array());
            $cur_id = $adb->query_result($sql6, 0, 'cur_id');        

            $product_no = 'PRO' . $cur_id;

            // print_r($new_product_id);
            // die();
            
            $sql5 = "INSERT INTO vtiger_products(productid, product_no, productname, productcategory, discontinued, qtyinstock, min_weight_kg, max_weight_kg, min_volume_m3, max_volume_m3, consignee, min_square_m2, max_square_m2, pll_pcs, productsheet, pricebookid, source) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $result5 = $adb->pquery($sql5, array($new_product_id, $product_no, $productname, $productcategory, $discontinued, $qtyinstock, $min_weight_kg, $max_weight_kg, $min_volume_m3, $max_volume_m3, $consignee, $min_square_m2, $max_square_m2, $pll_pcs, $pricebook_name, $new_pricebook_id, $source));

            $sql9 = "INSERT INTO vtiger_productcf(productid) VALUES (?)";
            $result7 = $adb->pquery($sql9, array($new_product_id));                
            
            $sql7 = $adb->pquery("UPDATE vtiger_modentity_num SET cur_id = ? WHERE semodule = 'Products' AND prefix = 'PRO'", array($cur_id + 1)); 
            
            $sql8 = "INSERT INTO vtiger_pricebookproductrel(pricebookid, productid, listprice, old_listprice, import_date) VALUES (?, ?, ?, ?, ?)";
            $result6 = $adb->pquery($sql8, array($new_pricebook_id, $new_product_id, $listprice, $listprice, $created_modified_date));            
            
        }

    }
    
$adb->completeTransaction();

$log->debug("Exiting from function CreatePricebook(".$new_pricebook_id.").");
            
        }        

}