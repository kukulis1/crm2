<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();  
        $date = date("Y-m-d H:i:s");
        $sth = $dbh->prepare("INSERT INTO vtiger_hrm_emplyee_documentscheck (employeid, documentid, smcreatorid, createddate) VALUES (?,?,?,?)");       
        $sth->execute(array($_POST['employeid'],$_POST['recordid'],$_POST['userid'],$date));

        if($sth){
          $getName = $dbh->prepare("SELECT CONCAT(first_name,' ',last_name) AS name FROM vtiger_users WHERE id = ?");
          $getName->setFetchMode(PDO::FETCH_ASSOC);
          $getName->execute(array($_POST['userid']));
          $name = $getName->fetch()['name'];    
          echo json_encode(array('status' => 'success','name' => $name,'date' => $date));
        }else{
          echo json_encode(array('status' => 'Fail'));
        }

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'Fail'));
  }
}else{
  http_response_code(404);
} 