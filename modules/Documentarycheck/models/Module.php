<?php

class Documentarycheck_Module_Model extends Vtiger_Module_Model {

  static function getDocumentsList($employee_position){
    $db = PearDatabase::getInstance();

    $sql = "SELECT documentarycheck_tks_text AS document_name, documentarycheck_position AS position,documentarycheckid
            FROM vtiger_documentarycheck 
            INNER JOIN vtiger_crmentity ON crmid=documentarycheckid 
            WHERE deleted = 0 AND IF(documentarycheck_position LIKE '%Visi%', true, documentarycheck_position LIKE '%$employee_position%')";

    $result = $db->pquery($sql,array());

    $list = array();

    foreach ($result as $value) {
      $list[] = $value;
    }


    return $list;
  }
  
  static function getCheckedDocumentsList($recordID){
    $db = PearDatabase::getInstance();

    $sql = "SELECT documentid,smcreatorid, createddate, CONCAT(first_name,' ',last_name) AS owner
            FROM vtiger_hrm_emplyee_documentscheck 
            LEFT JOIN vtiger_users u ON u.id=smcreatorid
            WHERE employeid = ?";
    $result = $db->pquery($sql,array($recordID));

    $list = array();

    foreach ($result as $val) {
      $list[$val['documentid']] = $val;
    }
    return $list;
  }

}