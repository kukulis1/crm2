<?php

include "modules/Storagepricebook/models/listModel.php";

use modules\Storagepricebook\models\listModel as listModel;

class Storagepricebook_AddNew_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

       public function process(Vtiger_Request $request) {  
           if($_POST['addNew']){            
              $this->listModel->addNewRecord($_POST);
           }   

        $clasificator = $this->listModel->getClasificator();     
        $measure = $this->listModel->getMeasure();     
        $viewer = $this->getViewer($request);                  
        $viewer->assign('clasificator', $clasificator);
        $viewer->assign('measure', $measure);
        $viewer->view('AddNew.tpl', $request->getModule()); 
      }

}       