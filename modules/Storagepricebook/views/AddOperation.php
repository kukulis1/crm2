<?php

include "modules/Storagepricebook/models/listModel.php";

use modules\Storagepricebook\models\listModel as listModel;


class Storagepricebook_AddOperation_View extends Vtiger_Index_View {

    function __construct(){
            $this->listModel = new listModel;  
    }

    public function process(Vtiger_Request $request) {  
        if($_POST['addNewOperation']){            
            $this->listModel->addNewOperation($_POST);
        }      
      
        $viewer = $this->getViewer($request); 
        $viewer->view('AddNewOperation.tpl', $request->getModule()); 
    }
}