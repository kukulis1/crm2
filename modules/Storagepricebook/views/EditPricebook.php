<?php

include "modules/Storagepricebook/models/listModel.php";

use modules\Storagepricebook\models\listModel as listModel;

class Storagepricebook_EditPricebook_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

       public function process(Vtiger_Request $request) {   
        $pricebookid = $_GET['record'];
        global $BOSES;
        global $current_user;

        if($_POST['edit']){            
          $this->listModel->editRecord($pricebookid,$_POST);
       }   

        $pricebook = $this->listModel->getPriceBook($pricebookid);                
        $priceBookInfo = $this->listModel->getPriceBookInfo($pricebookid);  
        $clasificator = $this->listModel->getClasificator();     
        $measure = $this->listModel->getMeasure();                       
        $num_rows = $this->listModel->num_rows($pricebookid);                       
        $viewer = $this->getViewer($request);                  
        $viewer->assign('type', 'edit');
        $viewer->assign('managers', $BOSES);
        $viewer->assign('role', $current_user->roleid);
        $viewer->assign('pricebook', $pricebook);
        $viewer->assign('priceBookInfo', $priceBookInfo);                 
        $viewer->assign('clasificator', $clasificator);
        $viewer->assign('measure', $measure);
        $viewer->assign('num_rows', $num_rows);
        $viewer->view('EditPricebook.tpl', $request->getModule()); 
      }

}       