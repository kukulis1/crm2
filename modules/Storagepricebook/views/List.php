<?php

include "modules/Storagepricebook/models/listModel.php";

use modules\Storagepricebook\models\listModel as listModel;

class Storagepricebook_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

       public function process(Vtiger_Request $request) {   
        $pricebooks = $this->listModel->getPriceBookList();                
        $viewer = $this->getViewer($request);                  
        $viewer->assign('pricebooks', $pricebooks);
        $viewer->assign('operation_created', ($_GET['operation_created'] ? 1 : 0));
        $viewer->view('List.tpl', $request->getModule()); 
      }

}       