<?php

include "modules/Storagepricebook/models/listModel.php";

use modules\Storagepricebook\models\listModel as listModel;

class Storagepricebook_PriceBook_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

       public function process(Vtiger_Request $request) {   
        $pricebookid = $_GET['record'];
        $pricebook = $this->listModel->getPriceBook($pricebookid);                
        $priceBookInfo = $this->listModel->getPriceBookInfo($pricebookid);                
        $viewer = $this->getViewer($request);                  
        $viewer->assign('pricebook', $pricebook);
        $viewer->assign('priceBookInfo', $priceBookInfo);
        $viewer->view('PriceBook.tpl', $request->getModule()); 
      }

}       