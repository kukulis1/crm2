<?php

include "modules/Storagepricebook/models/listModel.php";

use modules\Storagepricebook\models\listModel as listModel;

class Storagepricebook_Dublicate_View extends Vtiger_Index_View {

       function __construct(){
              $this->listModel = new listModel;  
       }

       public function process(Vtiger_Request $request) {   
        $pricebookid = $_GET['record'];

        if($_POST['edit']){            
          $this->listModel->addNewRecord($_POST);
        } 

        $pricebook = $this->listModel->getPriceBook($pricebookid);                
        $priceBookInfo = $this->listModel->getPriceBookInfo($pricebookid);  
        $clasificator = $this->listModel->getClasificator();     
        $measure = $this->listModel->getMeasure();                       
        $num_rows = $this->listModel->num_rows($pricebookid);                       
        $viewer = $this->getViewer($request);  
        $viewer->assign('type', 'dublicate');                
        $viewer->assign('pricebook', $pricebook);
        $viewer->assign('priceBookInfo', $priceBookInfo);                 
        $viewer->assign('clasificator', $clasificator);
        $viewer->assign('measure', $measure);
        $viewer->assign('num_rows', $num_rows);
        $viewer->view('EditPricebook.tpl', $request->getModule()); 
      }

}       