<?php

namespace modules\Storagepricebook\models;


class listModel {

  public function __construct()
  {
    global $adb;
    $this->db = $adb;   
  }

  public function getPriceBookList()
  {
    $sql = "SELECT storagepricebookid, storagepricebook_tks_storageop AS pricebookname,CONCAT(first_name,' ',last_name) AS creator ,createdtime,modifiedtime 
            FROM vtiger_storagepricebook p          
            JOIN vtiger_crmentity e ON e.crmid=p.storagepricebookid
            JOIN vtiger_users u ON u.id=e.smcreatorid
            WHERE e.deleted = 0
            ORDER BY p.storagepricebookid DESC";

      $records = $this->db->pquery($sql, array());
      return $records ;  
  }

  public function addNewOperation($post){
    $this->db->pquery("INSERT INTO vtiger_storagepricebook_services (title) VALUES ('{$post['operation_name']}')");

    $referer = $_SERVER['PHP_SELF']."?module=Storagepricebook&view=List&operation_created=1";
    return header("Location: $referer");
  }

  public function getPriceBook($pricebookid)
  {
    $sql = "SELECT s.title AS operation, operation_id, m.code AS measure, unit, fee 
                  FROM vtiger_storagepricebook_lines l
                  JOIN vtiger_storagepricebook_services s ON s.id=l.operation_id
                  JOIN app_measures m ON m.id=l.unit
                  WHERE related = ? 
                  ORDER BY l.id ASC";
    $records = $this->db->pquery($sql, array($pricebookid));

    return $records; 
  }

  public function num_rows($pricebookid)
  {
    $sql = "SELECT *FROM vtiger_storagepricebook_lines WHERE related = ?";
    $records = $this->db->pquery($sql, array($pricebookid));
    return  $this->db->num_rows($records);
  }

  public function getPriceBookInfo($pricebookid)
  {
    $sql = "SELECT storagepricebook_tks_storageop,p.accountid,a.accountname 
            FROM vtiger_storagepricebook p
            LEFT JOIN vtiger_account a ON a.accountid=p.accountid
            WHERE storagepricebookid = ?";
    $result = $this->db->pquery($sql, array($pricebookid));
    $pricebookname = $this->db->query_result($result,0,'storagepricebook_tks_storageop');
    $accountname = $this->db->query_result($result,0,'accountname');
    $accountid = $this->db->query_result($result,0,'accountid');
    return array('pricebookname' => $pricebookname,'accountname' => $accountname,'accountid' => $accountid);
  }

  public function getClasificator()
  {
    $sql = "SELECT * FROM vtiger_storagepricebook_services ORDER BY id ASC";  
    $records = $this->db->pquery($sql, array());
    return $records ;  
  }

  public function getMeasure()
  {
    $sql = "SELECT * FROM app_measures ORDER BY id ASC";  
    $records = $this->db->pquery($sql, array());
    return $records;  
  }

  public function addNewRecord($post)
  {
    global $current_user;
    $sql = "INSERT INTO vtiger_storagepricebook (storagepricebookid,storagepricebook_tks_storageop,accountid) VALUES (?,?,?)"; 
    $sql2 = "INSERT INTO vtiger_storagepricebook_lines (related, operation_id, unit, code, fee) VALUES (?,?,?,?,?)";  

    $last_id = $this->last_entity_record();
    $this->insert_entity('Storagepricebook', $last_id, $current_user->id, NULL,'CRM', $post['pricebook_name'], date("Y-m-d H:i:s"));
    $this->db->pquery($sql, array($last_id,$post['pricebook_name'],$post['account_id']));  
    
    $num_rows = $post['num_rows'];

    for ($i=1; $i <= $num_rows; $i++) { 
      $this->db->pquery($sql2, array($last_id,$post['operation'.$i],$post['unit'.$i],'',$post['fee'.$i]));  
    }

    $referer = $_SERVER['PHP_SELF']."?module=Storagepricebook&view=PriceBook&record=$last_id";
    return header("Location: $referer");
  }

  public function editRecord($pricebookid,$post)
  {
    global $current_user;
    $sql = "UPDATE vtiger_storagepricebook SET storagepricebook_tks_storageop = ?, accountid = ? WHERE storagepricebookid = ?";
    $sql2 = "INSERT INTO vtiger_storagepricebook_lines (related, operation_id, unit, code, fee) VALUES (?,?,?,?,?)";  
    $sql3 = "DELETE FROM vtiger_storagepricebook_lines WHERE related = ?";
    $sql4 = "UPDATE vtiger_crmentity SET modifiedby = ?, modifiedtime = ? WHERE crmid = ?";

    $this->db->pquery($sql, array($post['pricebook_name'],$post['account_id'],$pricebookid));  
    $this->db->pquery($sql3, array($pricebookid));  
    $this->db->pquery($sql4, array($current_user->id,date("Y-m-d H:i:s"),$pricebookid));  

    $num_rows = $post['num_rows'];

    for ($i=1; $i <= $num_rows; $i++) { 
      $this->db->pquery($sql2, array($pricebookid,$post['operation'.$i],$post['unit'.$i],'',$post['fee'.$i]));   
    }

    
    $referer = $_SERVER['PHP_SELF']."?module=Storagepricebook&view=PriceBook&record=$pricebookid";
    return header("Location: $referer");
  }

  public function last_entity_record()
  {

    $result = $this->db->pquery('SELECT id FROM vtiger_crmentity_seq');
    $productid1 = $this->db->query_result($result,0,'id');
    $crmid = $productid1 + 1;    
    
    return $crmid;
  }

  public function insert_entity($setype, $entity_id, $user_id, $description, $label, $source, $date)
  {
  
    $sth = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description,source, label, createdtime, modifiedtime) 
                              VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)';
    $sql_params = array($entity_id, $user_id, $user_id, $setype, $description, $label,$source, $date, $date);
    $this->db->pquery($sth, $sql_params);

    $sth2 = 'UPDATE vtiger_crmentity_seq SET id = ?';
    $this->db->pquery($sth2, array($entity_id));
  }

}