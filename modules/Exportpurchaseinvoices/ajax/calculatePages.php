<?php

require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");  

  $params = $_POST['search'];
  $search_params = str_replace('"','',substr($params,3,-3));
	$search_params = explode("],[", $search_params);

  $query = "SELECT p.purchaseorderid  FROM vtiger_purchaseorder p			
              LEFT JOIN vtiger_vendor v ON v.vendorid=p.vendorid						
              INNER JOIN vtiger_purchaseordercf ON p.purchaseorderid = vtiger_purchaseordercf.purchaseorderid 		
              INNER JOIN vtiger_crmentity ON p.purchaseorderid = vtiger_crmentity.crmid 
              WHERE vtiger_crmentity.deleted=0 ";

  $where = '';
  if(!empty($search_params[0])){
    for($i = 0; $i < count($search_params); $i++){
      $param = explode(",", $search_params[$i]);
      if($param[0] == 'exportpurchaseinvoices_tks_vendor_id'){
        $column = 'vendorname';
      }elseif($param[0] == 'exportpurchaseinvoices_tks_invoice_date'){
        $column = 'cf_1345';
      }elseif($param[0] == 'exportpurchaseinvoices_tks_purchase_no'){
        $column = 'cf_1355';
      }elseif($param[0] == 'exportpurchaseinvoices_tks_total'){
        $column = 'total';
      }elseif($param[0] == 'exportpurchaseinvoices_tks_duedate'){
        $column = 'duedate';
      }elseif($param[0] == 'createdtime'){
        $column = 'createdtime';
      }elseif($param[0] == 'exportpurchaseinvoices_tks_show_exported'){
        $column = 'exported_agnum';
      }

      if($param[0] == 'exportpurchaseinvoices_tks_invoice_date' OR $param[0] == 'exportpurchaseinvoices_tks_duedate' OR $param[0] == 'createdtime'){
        $where .= " $column BETWEEN '$param[2]' AND '$param[3]' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";
      }else if($param[0] == 'exportpurchaseinvoices_tks_show_exported'){
      if($param[2] == 0){
        $where .= " $column = $param[2] ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";
      }else{
        $where .= " 1+1 ";
      }         
      }else{
        $where .= " $column LIKE '%$param[2]%' ".($i+1 < COUNT($search_params) ? 'AND ' :'')."";
      }			
    }
  }

  if(!empty($where)) $query .= ' AND ';


  $query .= $where;

  $query .= " GROUP BY p.purchaseorderid";

  $result = $conn->query($query);

  $num_rows = mysqli_num_rows($result);

  echo  ceil($num_rows/$list_max_entries_per_page);
}else{
  http_response_code(404);
} 