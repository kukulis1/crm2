<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Shipmentduration_Record_Model extends Vtiger_Record_Model {

	function getLoads($recordId){				
		$select = 'revised_weight_kg, revised_quantity ,tare_type_name ,revised_length_m ,revised_width_m ,revised_height_m';
		$table = 'load JOIN tare_type ON tare_type.tare_type_id=revised_tare_type_id';

		$where .= "shipment_id = $recordId";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://uzsakymai.parnasas.lt/export/crm/get_table.php");              
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('user' => '123', 'password' => 'raktas','select' => $select, 'table' => $table, 'where' => $where));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);

		$shipment_info = $data['response']['result'];

		return $shipment_info;
	}


}
