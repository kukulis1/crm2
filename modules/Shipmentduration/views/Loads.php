<?php

class Shipmentduration_Loads_View extends Vtiger_Index_View {

	function __construct() {
		parent::__construct();
	}

  function process (Vtiger_Request $request) {	
    $recordId = $request->get('record');
		$moduleName = $request->getModule();
    $SHIPMENT_INFO = Shipmentduration_Record_Model::getLoads($recordId);
    $viewer = $this->getViewer ($request);
    $viewer->assign('SHIPMENT_INFO', $SHIPMENT_INFO);
    $viewName = $request->get('viewname');
    $viewer->view('Loads.tpl', $moduleName);
  }
}