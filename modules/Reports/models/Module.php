<?php
/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

class Reports_Module_Model extends Vtiger_Module_Model {

	/**
	 * Function deletes report
	 * @param Reports_Record_Model $reportModel
	 */
	function deleteRecord(Reports_Record_Model $reportModel) {
		$currentUser = Users_Record_Model::getCurrentUserModel();
		$subOrdinateUsers = $currentUser->getSubordinateUsers();

		$subOrdinates = array();
		foreach($subOrdinateUsers as $id=>$name) {
			$subOrdinates[] = $id;
		}

		$owner = $reportModel->get('owner');

		if($currentUser->isAdminUser() || in_array($owner, $subOrdinates) || $owner == $currentUser->getId()) {
			$reportId = $reportModel->getId();
			$db = PearDatabase::getInstance();

			$db->pquery('DELETE FROM vtiger_selectquery WHERE queryid = ?', array($reportId));

			$db->pquery('DELETE FROM vtiger_report WHERE reportid = ?', array($reportId));

			$db->pquery('DELETE FROM vtiger_schedulereports WHERE reportid = ?', array($reportId));

                        $db->pquery('DELETE FROM vtiger_reporttype WHERE reportid = ?', array($reportId));

			$result = $db->pquery('SELECT * FROM vtiger_homereportchart WHERE reportid = ?',array($reportId));
			$numOfRows = $db->num_rows($result);
			for ($i = 0; $i < $numOfRows; $i++) {
				$homePageChartIdsList[] = $adb->query_result($result, $i, 'stuffid');
			}
			if ($homePageChartIdsList) {
				$deleteQuery = 'DELETE FROM vtiger_homestuff WHERE stuffid IN (' . implode(",", $homePageChartIdsList) . ')';
				$db->pquery($deleteQuery, array());
			}
                        
                        if($reportModel->get('reporttype') == 'chart'){
                            Vtiger_Widget_Model::deleteChartReportWidgets($reportId);
                        }
			return true;
		}
		return false;
	}

	/**
	 * Function returns quick links for the module
	 * @return <Array of Vtiger_Link_Model>
	 */
	function getSideBarLinks() {
		$quickLinks = array(
			array(
				'linktype' => 'SIDEBARLINK',
				'linklabel' => 'LBL_REPORTS',
				'linkurl' => $this->getListViewUrl(),
				'linkicon' => '',
			),
		);
		foreach($quickLinks as $quickLink) {
			$links['SIDEBARLINK'][] = Vtiger_Link_Model::getInstanceFromValues($quickLink);
		}

		$quickWidgets = array(
			array(
				'linktype' => 'SIDEBARWIDGET',
				'linklabel' => 'LBL_RECENTLY_MODIFIED',
				'linkurl' => 'module='.$this->get('name').'&view=IndexAjax&mode=showActiveRecords',
				'linkicon' => ''
			),
		);
		foreach($quickWidgets as $quickWidget) {
			$links['SIDEBARWIDGET'][] = Vtiger_Link_Model::getInstanceFromValues($quickWidget);
		}

		return $links;
	}

	/**
	 * Function returns the recent created reports
	 * @param <Number> $limit
	 * @return <Array of Reports_Record_Model>
	 */
	function getRecentRecords($limit = 10) {
		$db = PearDatabase::getInstance();

		$result = $db->pquery('SELECT * FROM vtiger_report 
						INNER JOIN vtiger_reportmodules ON vtiger_reportmodules.reportmodulesid = vtiger_report.reportid
						INNER JOIN vtiger_tab ON vtiger_tab.name = vtiger_reportmodules.primarymodule AND presence = 0
						ORDER BY reportid DESC LIMIT ?', array($limit));
		$rows = $db->num_rows($result);

		$recentRecords = array();
		for($i=0; $i<$rows; ++$i) {
			$row = $db->query_result_rowdata($result, $i);
			$recentRecords[$row['reportid']] = $this->getRecordFromArray($row);
		}
		return $recentRecords;
	}

	/**
	 * Function returns the report folders
	 * @return <Array of Reports_Folder_Model>
	 */
	function getFolders() {
		return Reports_Folder_Model::getAll();
	}

	/**
	 * Function to get the url for add folder from list view of the module
	 * @return <string> - url
	 */
	function getAddFolderUrl() {
		return 'index.php?module='.$this->get('name').'&view=EditFolder';
	}
    
    /**
     * Function to check if the extension module is permitted for utility action
     * @return <boolean> true
     */
    public function isUtilityActionEnabled() {
        return true;
    }

	/**
	 * Function is a callback from Vtiger_Link model to check permission for the links
	 * @param type $linkData
	 */
	public function checkLinkAccess($linkData) {
		$privileges = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$reportModuleModel = Vtiger_Module_Model::getInstance('Reports');
		return $privileges->hasModulePermission($reportModuleModel->getId());
	}
    
    /*
     * Function to get supported utility actions for a module
     */
    function getUtilityActionsNames() {
        return array('Export');
		}
		
		public function getRemovedOrders($dateFilter=''){
			$db = PearDatabase::getInstance();
			$today = date("Y-m-d");
			$last_week = date('Y-m-d', strtotime('-15 days'));


			global $current_user;
			$user = $current_user->id;
	
				$sql = "SELECT CONCAT(first_name,' ',last_name) as employee, reason, record_id, setype,created_time,
					CASE WHEN setype = 'SalesOrder' THEN s.shipment_code
						WHEN setype = 'Invoice' THEN e.label
								WHEN setype = 'PurchaseOrder' THEN p.purchaseorder_no
								ELSE e.label
					END AS code
						FROM deletereasons d
						LEFT JOIN vtiger_crmentity e ON e.crmid=d.record_id
						LEFT JOIN vtiger_users u ON u.id=e.modifiedby
						LEFT JOIN vtiger_salesorder s ON s.salesorderid=d.record_id  
						LEFT JOIN vtiger_purchaseorder p ON p.purchaseorderid=d.record_id    
						WHERE d.deleted = 0 AND u.id != 1 ";
	
		 $params = array();

			//handling date filter for history widget in home page
			if(!empty($dateFilter)) {
				$sql .= " AND DATE_FORMAT(d.created_time,'%Y-%m-%d') BETWEEN ? AND ? ";
				$params[] = $dateFilter['start'];
				$params[] = date('Y-m-d',  strtotime("+15 day", strtotime($dateFilter['start'])));
				$today = date('Y-m-d',  strtotime("+15 day", strtotime($dateFilter['start'])));
				$last_week = $dateFilter['start'];
			}else{
				$sql .= " AND DATE_FORMAT(d.created_time,'%Y-%m-%d') BETWEEN ? AND ? ";
				$params[] = $last_week;
				$params[] = $today;      
			}
			

			$show_filter = "Nuo: ".$last_week." Iki: ".$today; 	

			$sql .= "	ORDER BY d.id DESC";


			$result = $db->pquery($sql,$params);   		
			// $noOfRows = $db->num_rows($result);
	
			$removedRecords = array();

			foreach ($result as $row) {
				$removedRecords['content'][] = $row;
			}

			// for($i=0; $i<$noOfRows; $i++) {
			// 	$row = $db->query_result_rowdata($result, $i);
			// 	$removedRecords['content'][] = $row;			
			// }   
			$removedRecords['filter'] = $show_filter;
			$removedRecords['user'] = $user;
	
			return $removedRecords;
		}

		public function getSalesOrdersReport($dateFilter=''){
			$db = PearDatabase::getInstance();
	
			global $current_user;
			$role = $current_user->roleid;	

			// START Viso užsakymu,pll, EUR, transporto,perkraustymo
		  $sql = "SELECT DATE_FORMAT(e.createdtime, '%m-%d') AS order_date, DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS order_full_date, COUNT(*) AS total, ROUND(SUM(total),2) AS sum, GROUP_CONCAT(s.salesorderid) AS id, IF(cf_855 = 'Perkraustymo užsakymas', 'movement', 'parcel') as type
                   FROM vtiger_salesorder s 
                   LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid 
                   LEFT JOIN vtiger_salesordercf cf ON cf.salesorderid=s.salesorderid
                   WHERE e.deleted = 0 AND e.setype = 'SalesOrder' ";

			$sql2 = "SELECT COUNT(s.salesorderid) AS viso, DATE_FORMAT(e.createdtime, '%m-%d') AS created, DATE_FORMAT(`timestamp`, '%Y-%m-%d') AS done, DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS order_full_date,
			5 * ((DATEDIFF(timestamp, e.createdtime) ) DIV 7) + MID('0123455501234445012333450122234501101234000123450', 7 * WEEKDAY(e.createdtime) + WEEKDAY(timestamp) + 1, 1) as days,GROUP_CONCAT(s.salesorderid) AS salesorderids
									FROM vtiger_salesorder s 
									LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid    									WHERE e.deleted = 0 AND e.setype = 'SalesOrder'	AND s.sostatus = 'delivered' ";
			// END


			// START visi uzsakymai kurie nera done
			$sql3 = "SELECT COUNT(s.salesorderid) AS viso, DATE_FORMAT(e.createdtime, '%m-%d') AS created, GROUP_CONCAT(s.salesorderid) AS salesorderids, DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS order_full_date
									FROM vtiger_salesorder s 
									LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid                                         
									WHERE e.deleted = 0 AND e.setype = 'SalesOrder' AND s.sostatus != 'delivered'	"; 
			// END

			// START užsakymai be sąskaitos ir keliems uzsakymams yra israsytos saskaitos
			$sales_invoice_status = "SELECT s.salesorderid, DATE_FORMAT(e.createdtime,'%m-%d') AS created, IF(li.salesorderid IS NULL, 0,1) AS has
									 FROM vtiger_salesorder s 
									 LEFT JOIN vtiger_crmentity e ON e.crmid=s.salesorderid  	
									 LEFT JOIN vtiger_invoice_salesorders_list li ON li.salesorderid=s.salesorderid                                   
									 WHERE e.deleted = 0 AND e.setype = 'SalesOrder' AND s.sostatus = 'delivered'";
    	// END


			$params = array();
				//handling date filter for history widget in home page

			$month_locale = array('Jan' => 'Sausis', 'Feb' => 'Vasaris', 'Mar' => 'Kovas', 'Apr' => 'Balandis', 'May' => 'Gegužė', 'Jun' => 'Birželis', 'Jul' => 'Liepa', 'Aug' => 'Rugpjūtis', 'Sep' => 'Rugsėjis', 'Oct' => 'Spalis', 'Nov' => 'Lapkritis', 'Dec' => 'Gruodis');
		
			if(!empty($dateFilter)) {
				$where = " AND DATE_FORMAT(e.createdtime,'%Y-%m') = ? ";
				$sql .= $where;
				$sql2 .= $where;
				$sql3 .= $where;
				$sales_invoice_status .= $where;

				$params[] = date("Y-m",strtotime($dateFilter['start']));
				$month = date("M",strtotime($dateFilter['start']));	
				$month = $month_locale[$month];
				$firstDay = date('Y-m-01', strtotime($dateFilter['start']));
				$lastDay = date('Y-m-t', strtotime($dateFilter['start']));
			}else{
     			return 	array('role' => $role, 'orders' => 0, 'filter' => 0);
			}

		
			$period = $this->getDatesFromRange($firstDay,$lastDay);		
			$show_filter = "Pasirinktas menuo: ".$month; 	

			$sql .= " GROUP BY DATE_FORMAT(e.createdtime, '%Y-%m-%d'),cf_855";
			$sql2 .= " GROUP BY DATE_FORMAT(e.createdtime, '%Y-%m-%d'),DATE_FORMAT(`timestamp`, '%Y-%m-%d')	ORDER BY days"; 
			$sql3 .= " GROUP BY DATE_FORMAT(e.createdtime, '%Y-%m-%d')	ORDER BY e.createdtime";
			$sales_invoice_status .= " GROUP BY s.salesorderid";	
			$db->pquery("SET SESSION group_concat_max_len = ?",array(1000000));  
			$result = $db->pquery($sql,$params);  

			$check = $db->num_rows($result);
			if(!$check) 	return 	array('role' => $role, 'orders' => 0, 'no-records' => 1, 'filter' => $show_filter);

			$result2 = $db->pquery($sql2,$params);  
			$result3 = $db->pquery($sql3,$params);  
			$result4 = $db->pquery($sales_invoice_status,$params);  

			$array = array();
			$orders = array();
			$total_orders_arr = array();
			$total_sum_arr = array();
			$total_parcel_movement_orders_arr = array();
			$total_pll_arr = array();

			$array['period'] = $period;


			// START Viso užsakymu,pll, EUR, transporto,perkraustymo
			foreach($result as $row){
				$query = "SELECT SUM(pll) AS pll FROM vtiger_inventoryproductrel WHERE id IN (".$row['id'].")";
				$query = $db->pquery($query,array()); 
				$pll = $db->query_result($query,0,'pll');
				$orders[$row['order_date']][$row['type']] = array('total' => ($row['total'] ?: '0'), 'sum' => $row['sum'], 'id' => $row['id'], 'pll' => $pll);
				$total_pll_arr[] = $pll;
				$total_orders_arr[] = $row['total'];
				$total_sum_arr[] = $row['sum'];
				$total_parcel_movement_orders_arr[$row['type']][] = $row['total'];				
			}

			$array['total_orders'] = array_sum($total_orders_arr);
			$array['orders'] = $orders;
			$array['total_pll'] = array_sum($total_pll_arr);
			$array['total_pll_arr'] = $total_pll_arr;
			$array['total_orders_arr'] = $total_orders_arr;
			$array['total_sum_arr'] = $total_sum_arr;
			$array['total_sum'] = str_replace(".",",",array_sum($total_sum_arr));

			$array['total_parcel_movement_orders_arr'] = $total_parcel_movement_orders_arr;
			$array['total_parcel_orders']	= array_sum($total_parcel_movement_orders_arr['parcel']);
			$array['total_movement_orders']	= array_sum($total_parcel_movement_orders_arr['movement']);
			$array['total_parcel_procent'] = ROUND((array_sum($total_parcel_movement_orders_arr['parcel']) / array_sum($total_orders_arr)) * 100,2);
			$array['total_movement_procent'] = ROUND((array_sum($total_parcel_movement_orders_arr['movement']) / array_sum($total_orders_arr)) * 100,2);
	
			$done_in_days = array();
			$done_in_days_total = array();
			$salesordersids = array();
			$total_sales_arr = array();
			

			foreach ($result2 as $stats) {	
				// $week_day = date('l',strtotime($stats['order_full_date']));
					// if($week_day != 'Saturday' AND $week_day != 'Sunday'){
						$done_in_days[$stats['days']][$stats['created']] = $stats;
						$done_in_days_total[$stats['days']][$stats['created']] = $stats['viso'];        
					// } 
					$salesordersids[] = $stats['salesorderids']; // visu užsakymu kurie done id array
					$total_sales_arr[$stats['created']][] = $stats['salesorderids'];
			}

			$array['done_in_days'] = $done_in_days;
			$array['total_sales_arr'] = $total_sales_arr;
			$array['done_in_days_total'] = $done_in_days_total;

			// END


			// START visi uzsakymai kurie nera done

			$in_progress = array();
			$in_progress_all = array();

			foreach ($result3 as $in) {	
				// $week_day = date('l',strtotime($in['order_full_date']));
				// if($week_day != 'Saturday' AND $week_day != 'Sunday'){
				$in_progress[$in['created']] = $in;
				$in_progress_all[] = $in['viso'];
				// }
			}

			$total_in_progress = array_sum($in_progress_all);
			$in_progress_procent = ROUND(($total_in_progress /  array_sum($total_orders_arr)) * 100,2);

			$array['total_in_progress'] = $total_in_progress;
			$array['in_progress_procent'] = $in_progress_procent;
			$array['in_progress'] = $in_progress;

			// END



			$salesordersids = $this->multiDimToSingle($salesordersids); // vienos dimensijos visu done uzsakymu array
			$salesorderid = implode(",", $salesordersids); // visu užsakymu kurie done id string
		
			$invoices_arr = array();
			$invoices_arr_total = array();
			$total_invoices_from_sales = array();
			$dont_have_invoice_arr = array();
			$dont_have_invoice_arr_full = array();
			$total_sales_has_invoice = array();  
			$total_sales_has_invoice_date = array();
			$listprice = array();
			$total_sales_sum = array();

		
			$invoices_from_sales = $db->pquery("SELECT DISTINCT invoiceid, DATE_FORMAT(se.createdtime,'%m-%d') AS order_created, DATE_FORMAT(e.createdtime,'%m-%d') AS created, IF(productid = 121391, listprice*quantity,listprice) AS listprice, 
				GROUP_CONCAT(s.total) as total, s.salesorderid, shipment_code
																						FROM vtiger_invoice i
																						LEFT JOIN vtiger_inventoryproductrel inv ON inv.id=i.invoiceid
																						LEFT JOIN vtiger_crmentity e ON e.crmid=i.invoiceid
																						LEFT JOIN vtiger_salesorder s ON s.salesorderid=order_id
																						LEFT JOIN vtiger_crmentity se ON se.crmid=s.salesorderid                                                                       
																						WHERE e.deleted = 0 AND order_id IN ($salesorderid) 
																						GROUP BY s.salesorderid", array());

			foreach ($invoices_from_sales as $item) {	
				$invoices_arr['total'][$item['order_created']][] = $item['invoiceid'];
				$invoices_arr['sum'][$item['order_created']][] = explode(",",$item['listprice']); 
				$invoices_arr_total['sum'][] = explode(",",$item['listprice']);
				$invoices_arr['sales'][$item['order_created']][] = explode(",",$item['total']);		
				$total_invoices_from_sales[] = $item['invoiceid'];			 
			} 
		
				$total_day = array_map(function($a) {  return array_pop($a); }, $invoices_arr_total['sum']);  
				$total_day = str_replace(".",",",ROUND(array_sum($total_day),2));


				foreach ($period as $value) {
					$date = $value->format('m-d');	
					$listprice[$date] = array_map(function($a) {  return array_pop($a); }, $invoices_arr['sum'][$date]);	
					$total_sales_sum[$date] = array_map(function($a) {  return array_pop($a); }, $invoices_arr['sales'][$date]);				
				}



				$array['total_invoices_from_sales'] = $total_invoices_from_sales;
				$array['total_day'] = $total_day;			
				$array['invoices_arr'] = $invoices_arr;
				$array['listprice'] = $listprice;
				$array['total_sales_sum'] = $total_sales_sum;
		
			// START užsakymai be sąskaitos ir keliems uzsakymams yra israsytos saskaitos			

					foreach ($result4 as $row) {
						if(in_array($row['salesorderid'],$salesordersids)){
						// atrenkam uzsakymus kurie neturi ir kurie turi saskaita
							if($row['has']){
								$total_sales_has_invoice[] = $row['salesorderid'];
								$total_sales_has_invoice_date[$row['created']][] = $row['salesorderid'];
							}else{
								$dont_have_invoice_arr[] = $row['salesorderid'];
								$dont_have_invoice_arr_full[$row['created']][] = $row['salesorderid'];
							}
						}
					}

					$orders_with_invoice_id = $total_sales_has_invoice;

					$total_sales_has_invoice = COUNT(array_unique($total_sales_has_invoice)); // Kiek uzsakymu  turi  saskaitas
					$total_invoices_from_sales = COUNT(array_unique($total_invoices_from_sales)); // kiek israsyta saskaitu is atrinktu uzsakymu

					$dont_have_invoice = count(array_unique($dont_have_invoice_arr));
					$total_salesorders_with_done = count($salesordersids);

					$array['dont_have_invoice'] = $dont_have_invoice;
					$array['dont_have_invoice_arr_full'] = $dont_have_invoice_arr_full;
					$array['total_sales_has_invoice_date'] = $total_sales_has_invoice_date;
	
			// END

				$total_salesorders_with_done = count($salesordersids);
				$writed_invoice_procent = str_replace(".",",",ROUND(($total_sales_has_invoice / $total_salesorders_with_done) * 100,2)); // procentaliai kiek israsyta saskaitu is atrinktu uzsakymu

				$array['total_invoices_from_sales'] = $total_invoices_from_sales;
				$array['writed_invoice_procent'] = $writed_invoice_procent;


			// START tarpas tarp done ir sukurtos saskaitos
    
				$sql5 = $db->pquery("SELECT DISTINCT 5 * ((DATEDIFF(e.createdtime, timestamp) ) DIV 7) + MID('0123455501234445012333450122234501101234000123450', 7 * WEEKDAY(timestamp) + WEEKDAY(e.createdtime) + 1, 1) as days, l.salesorderid, DATE_FORMAT(timestamp,'%Y-%m-%d') AS done, DATE_FORMAT(e.createdtime,'%m-%d') AS created_invoice
						FROM app_status_log l
						LEFT JOIN vtiger_inventoryproductrel i ON i.order_id=l.salesorderid
						LEFT JOIN vtiger_crmentity e ON e.crmid=i.id 
						WHERE code = 4 AND e.createdtime IS NOT NULL AND l.salesorderid IN ($salesorderid)
						GROUP BY l.salesorderid
						ORDER BY created_invoice");

					$time_from_done_to_invoice = array();   

					foreach ($sql5 as $stats) {	
						$time_from_done_to_invoice[$stats['created_invoice']][] = $stats['days'];      
					}

					$array['time_from_done_to_invoice'] = $time_from_done_to_invoice;
			// END

		  // START gaunam paskutinės sąskaitos išrašymo laiką
			if(!$dont_have_invoice && !empty($salesorderid)){
				$sql6 = $db->pquery("SELECT DATE_FORMAT(e.createdtime, '%Y-%m-%d') AS close_date 
									 FROM vtiger_invoice_salesorders_list li 
									 LEFT JOIN vtiger_crmentity e ON e.crmid=li.invoiceid
									 WHERE li.salesorderid IN ($salesorderid)
									 ORDER BY li.invoiceid DESC LIMIT 1");
		
				$close_date = $db->query_result($sql6,0,'close_date');
				$array['close_date'] = $close_date;
			}

			$untill_done_procent = str_replace(".",",",ROUND(($total_salesorders_with_done - $total_sales_has_invoice) / $total_salesorders_with_done * 100,2)); // kiek procentu liko israsyti
			$array['untill_done_procent'] = $untill_done_procent;
		
			// END


			// START apmoketi uzsakymai,matuojam per kiek laiko nuo uzsakymo sukurimo yra apmokama
			$orders_with_invoice_id = implode(",",$orders_with_invoice_id);

			$total_sum_payed_invoices = array();
			$total_sum_payed_invoices_by_date = array();
			$diff_between_created_and_payed = array();
			$dont_payed_yet = array();
			$dont_payed_yet_dates = array();


			$sql7 = $db->pquery("SELECT DATE_FORMAT(se.createdtime, '%m-%d') as date,s.salesorderid, invoiceid, 5 * ((DATEDIFF(debts_tks_data,se.createdtime) ) DIV 7) + MID('0123455501234445012333450122234501101234000123450', 7 * WEEKDAY(se.createdtime) + WEEKDAY(debts_tks_data) + 1, 1) as days,IF(debts_tks_data IS NOT NULL,1,0) AS payed
													FROM vtiger_salesorder s
													LEFT JOIN vtiger_crmentity se ON se.crmid=s.salesorderid
													LEFT JOIN vtiger_invoice_salesorders_list li ON li.salesorderid=s.salesorderid
													LEFT JOIN vtiger_crmentity e ON e.crmid=li.invoiceid
													LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.crmid = li.invoiceid 
													LEFT JOIN vtiger_debts ON vtiger_debts.debtsid = vtiger_crmentityrel.relcrmid 
													LEFT JOIN vtiger_crmentity de ON de.crmid=vtiger_debts.debtsid AND de.deleted = 0
													WHERE se.deleted = 0 AND e.deleted = 0 AND s.salesorderid IS NOT NULL AND s.salesorderid IN ($orders_with_invoice_id)
													GROUP BY s.salesorderid");

			foreach($sql7 AS $row){
				if($row['payed']){
					$diff_between_created_and_payed[$row['date']][] = $row['days'];
					$total_sum_payed_invoices[] = $row['salesorderid'];
					$total_sum_payed_invoices_by_date[$row['date']][] = $row['salesorderid'];
				}else{
					$dont_payed_yet[] = $row['salesorderid'];
					$dont_payed_yet_dates[$row['date']][] = $row['salesorderid'];
				}
			}  

			$total_sum_payed_invoices = count($total_sum_payed_invoices);
			$total_sales_has_payment_procent = ROUND(($total_sum_payed_invoices/$total_sales_has_invoice) * 100,2);

			// END

			$array['total_sum_payed_invoices'] = $total_sum_payed_invoices;
			$array['filttotal_sum_payed_invoiceser'] = $total_sum_payed_invoices;
			$array['total_sales_has_payment_procent'] = $total_sales_has_payment_procent;
			$array['total_sum_payed_invoices_by_date'] = $total_sum_payed_invoices_by_date;
			$array['diff_between_created_and_payed'] = $diff_between_created_and_payed;

			$array['dont_payed_yet'] = $dont_payed_yet;
			$array['dont_payed_yet_dates'] = $dont_payed_yet_dates;
				
 
	
			$array['filter'] = $show_filter;
			$array['role'] = $role;
		
	
			return $array;
		}


		public function multiDimToSingle($multiArray){
			if(!count($multiArray)) return $multiArray;
			$temp = implode(",",$multiArray); 
			$temp2 = explode(",",$temp);
			$temp3 =  array_unique($temp2);
			return $temp3;
		}
		
	  public function getDatesFromRange($start, $end) { 
			$array = array();  
			$interval = new DateInterval('P1D');  
			$realEnd = new DateTime($end); 
			$realEnd->add($interval);   
			$period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
			foreach($period as $date) {                  
					$array[] = $date;  
			}  
			return $period; 
		} 

		public function getAutoCostReport($dateFilter=''){
			$db = PearDatabase::getInstance();
			global $current_user;

			$sql = "SELECT i.id, 
						ROUND(listprice,2) AS price, 
						ROUND((listprice*quantity) + ((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100),2) AS total,	
						ROUND(((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100),2) AS vat, 						
						cargo_measure AS car,cf_1345 AS invoice_date, itemservice_tks_name AS service,itemserviceid														
						FROM vtiger_inventoryproductrel i
						JOIN vtiger_purchaseorder po ON po.purchaseorderid=i.id
						JOIN vtiger_purchaseordercf cf ON cf.purchaseorderid=i.id
						LEFT JOIN vtiger_itemservice s ON s.itemservice_tks_name=i.cargo_wgt
						LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=po.region_id)       
						INNER JOIN vtiger_crmentity e ON e.crmid=i.id
						WHERE cf_1345 BETWEEN ? AND ? AND e.deleted = 0 AND itemserviceid IN (103983,103984,103985,104021,108888,108889,110429,114453) AND cargo_measure REGEXP '[A-Za-z]{3}[[:digit:]]{3}'
						ORDER BY cf_1345, i.id,sequence_no";

			$params = array();


			//handling date filter for history widget in home page
			if(!empty($dateFilter)) {
				$params[] = $dateFilter['start'];
				$params[] = $dateFilter['end'];				
			}else{
				$today = date("Y-m-d");
				$last_week = date('Y-m-d', strtotime('-15 days'));
				$params[] = $last_week;
				$params[] = $today;    
			}

			$result = $db->pquery($sql,$params);   		
			$noOfRows = $db->num_rows($result);

			$data = array();			
			$data2 = array();			
			$total_sum = array();			
			$detalization = array();			
			$detalization_price = array();	
			$invoice_date = array();		


			foreach ($result as $row) {
				$invoice_date[$row['invoice_date']] = $row['invoice_date'];
				$data[$row['car']][$row['invoice_date']] += $row['total'];	
				$total_sum[$row['car']] += $row['total'];	
				$detalization[$row['car']][$row['service']]['services'] = $row['service'];
				$detalization_price[$row['invoice_date']][$row['car']][$row['service']]['total'] += $row['total'];	
				$detalization_price[$row['invoice_date']][$row['car']][$row['service']]['id'][] = $row['id'];		
				$detalization_price[$row['invoice_date']][$row['car']][$row['service']]['car_no'] = $row['car'];	
				$detalization_price[$row['invoice_date']][$row['car']][$row['service']]['serviceid'] = $row['itemserviceid'];	
			}
			
			foreach ($result as $row) {
				$data2[array_sum($data[$row['car']])][$row['car']] = $data[$row['car']];
			}

			krsort($data2);	
			sort($invoice_date);

			$processed = array();
			foreach($data2 as $subarr) {
				 foreach($subarr as $id => $value) {
						if(!isset($processed[$id])) {
							 $processed[$id] = array();
						}			
						$processed[$id] = $value;
				 }
			}

			$show_filter = "Nuo: ".$params[0]." Iki: ".$params[1]; 	

			return array('user' => $current_user->id,'filter' => $show_filter,'data' => $processed,'total_sum' => $total_sum,'detalization' => $detalization, 'detalization_price' => $detalization_price,'invoice_date' => $invoice_date);
		}

}