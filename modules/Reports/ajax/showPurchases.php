<?php
require_once "../../../config.inc.php";
error_reporting(0);

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $purchaseorderid = $_POST['id'];
  $car = $_POST['car'];
  $serviceid = $_POST['serviceid'];

  $purchaseorder = $conn->query("SELECT s.purchaseorderid,purchaseorder_no, vendorname,
  	ROUND((listprice*quantity) + ((listprice*quantity) *  IF(vtiger_taxregions.value IS NULL,21, vtiger_taxregions.value) / 100),2) AS listprice,  
    cargo_wgt AS services,createdtime
                                      FROM vtiger_purchaseorder s
                                      JOIN vtiger_inventoryproductrel i ON i.id=s.purchaseorderid
                                      JOIN vtiger_purchaseordercf cf ON cf.purchaseorderid=s.purchaseorderid
                                      INNER JOIN vtiger_crmentity e ON e.crmid=s.purchaseorderid
                                      LEFT JOIN vtiger_itemservice it ON it.itemservice_tks_name=i.cargo_wgt
                                      LEFT JOIN vtiger_vendor a ON s.vendorid=a.vendorid
                                      LEFT JOIN vtiger_taxregions ON IF(i.tax2 > 0, vtiger_taxregions.regionid=i.tax2 ,vtiger_taxregions.regionid=s.region_id)    
                                      WHERE e.deleted = 0 AND e.setype = 'PurchaseOrder' AND s.purchaseorderid IN ($purchaseorderid) AND cargo_measure = '$car' AND itemserviceid = $serviceid 
                                      ORDER BY sequence_no");

  $purchaseorders_array = array();

  foreach($purchaseorder as $row){
    $purchaseorders_array[] = $row;
  }

  echo json_encode($purchaseorders_array);


}else{
  http_response_code(404);
}
