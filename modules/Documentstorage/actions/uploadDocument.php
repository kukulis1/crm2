<?php
require_once $_SERVER['DOCUMENT_ROOT']."/config.inc.php";

if($_POST){

  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8"); 
  $current_user = $_POST['userid'];
  $folderid = $_POST['folderid'];
  $folder = ($_POST['folder'] == 'parent' ? 1:2);
  $file_type = $_POST['file_type'];
  $date = date('Y-m-d H:i:s');
  $root = $_SERVER['DOCUMENT_ROOT'];

  $file_name = $_FILES['file']['name'];
  $file_size =$_FILES['file']['size'];
  $file_tmp =$_FILES['file']['tmp_name'];
  $ext=strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
  $file_name_without_ext = pathinfo($file_name, PATHINFO_FILENAME);

  $time = time();
  $newName = lt_chars(str_replace(' ', '',pathinfo($file_name, PATHINFO_FILENAME))); 
  $newName = $newName."_".$time;

  $year = date("Y");
  $month = date("F");
  $dayofweek = "week".weekOfMonth(strtotime(date("Y-m-d")));  

  if (!file_exists("$root/documents_storage/$year/$month/$dayofweek")) {
    mkdir("$root/documents_storage/$year/$month/$dayofweek", 0777, true);
  }

  $target_dir = "$root/documents_storage/$year/$month/$dayofweek/"; 
  $uploadfile = $target_dir . $newName.".".$ext;  

  if (move_uploaded_file($file_tmp, $uploadfile)) {      
    $conn->query("INSERT INTO vtiger_documentstorage_temp_files (folder_id,folder_type, filename,path, temp_filename, size, extension, flupload, createdtime) VALUES ($folderid,$folder,'$file_name_without_ext','$year/$month/$dayofweek','$newName','$file_size','$ext',$current_user,'$date')");
    echo $uploadfile;
  } else {
    echo "Fail";
  }   
}
     
function lt_chars($text) {
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

    return $text; 
}

function weekOfMonth($date){  
  $firstOfMonth = strtotime(date("Y-m-01", $date));
  return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}