<?php
require_once $_SERVER['DOCUMENT_ROOT']."/config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $recordid = $_POST['recordid'];
  $folder = $_POST['folder'];
  $perPage = $_POST['perPage'];
  $startAt = $_POST['startAt'];
  

    $query = "SELECT d.id, d.filename,d.extension,d.size,e.createdtime, CONCAT(`first_name`,' ',`last_name`) AS owner 
              FROM vtiger_documentstorage d
              INNER JOIN vtiger_crmentity e ON e.crmid=d.documentstorageid
              LEFT JOIN vtiger_users u ON u.id=e.smcreatorid
              WHERE e.deleted = 0 AND folder_type = $folder AND folder_id = $recordid 
              ORDER BY d.id DESC LIMIT $startAt, $perPage";
              
      $files = $conn->query($query);
      if(!$files){
        echo json_encode('empty');
      }

      $result = array();
      while($row = $files->fetch_assoc()) {
        $result[] = $row;      
      }   

      if(!empty($result)){
        echo json_encode($result);
      }else{
        echo json_encode('no_results');
      }
  
 

}else{
  http_response_code(404);
}


