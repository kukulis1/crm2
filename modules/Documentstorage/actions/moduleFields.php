<?php
require_once $_SERVER['DOCUMENT_ROOT']."/config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");
  $moduleid = $_POST['module'];

    $fields = array();
    $query = mysqli_fetch_assoc($conn->query("SELECT tablename FROM vtiger_entityname WHERE tabid = $moduleid"));
    $table = $query['tablename'];

    if($table == 'vtiger_hrm_employee'){
      $subTable = 'vtiger_hrm_employcf';
    }else if($table == 'vtiger_account'){
      $subTable = 'vtiger_accountscf';
    }else if($table == 'vtiger_vendor'){
      $subTable = 'vtiger_vendorcf';
    }

    $query2 = $conn->query("SELECT columnname,fieldlabel FROM vtiger_field WHERE tabid = $moduleid AND (tablename = '$table' OR tablename = '$subTable') AND displaytype != 6 AND uitype != 10");
    
    $fields[] = array('columnname' => 'today_date', 'fieldlabel' => 'Šiandienos data');
    $fields[] = array('columnname' => 'date_string', 'fieldlabel' => 'Šiandienos data, mėnesis žodžiu');
    $fields[] = array('columnname' => 'employment_contract_no', 'fieldlabel' => 'Darbo sutarties nr.');
    $fields[] = array('columnname' => 'contract_no', 'fieldlabel' => 'Sutarties nr. iš datos');
    
    foreach ($query2 as $row) {
      $fields[] = $row;
    }

    echo json_encode(array('status' => 'success', 'data' => $fields));
}