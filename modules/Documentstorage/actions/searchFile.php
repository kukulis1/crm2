<?php
require_once $_SERVER['DOCUMENT_ROOT']."/config.inc.php";

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $recordid = $_POST['recordid'];
  $folder = $_POST['folder'];
  $title = $_POST['title'];
  $owner = $_POST['owner'];
  $extension = $_POST['extension'];
  $size = $_POST['size'];
  $createdtime = $_POST['createdtime'];

  if(!empty($title) || !empty($owner) || !empty($extension) || !empty($size) || !empty($createdtime)){
    $query = "SELECT d.documentstorageid, d.filename,d.extension,d.size,e.createdtime, CONCAT(`first_name`,' ',`last_name`) AS owner 
              FROM vtiger_documentstorage d
              INNER JOIN vtiger_crmentity e ON e.crmid=d.documentstorageid
              LEFT JOIN vtiger_users u ON u.id=e.smcreatorid
              WHERE e.deleted = 0 AND folder_type = $folder AND folder_id = $recordid";

    if(!empty($title)){
      $query .= " AND filename LIKE '%$title%' ";
    }   

    if(!empty($owner)){
      $query .= " AND CONCAT(`first_name`,' ',`last_name`) LIKE '%$owner%' ";
    } 

    if(!empty($extension)){
      $query .= " AND extension LIKE '%$extension%' ";
    }  

    if(!empty($size)){
      $query .= " AND size LIKE '%$size%' ";
    }   
     
    if(!empty($createdtime)){
      $period = explode(",",$createdtime);   
      $query .= " AND  DATE_FORMAT(createdtime, '%Y-%m-%d') BETWEEN  '$period[0]' AND '$period[1]' ";
    }  

    $query .= " ORDER BY id DESC";
      $files = $conn->query($query);
      if(!$files){
        echo json_encode('empty');
      }

      $result = array();
      while($row = $files->fetch_assoc()) {
        $result[] = $row;      
      }   

      if(!empty($result)){
        echo json_encode($result);
      }else{
        echo json_encode('no_results');
      }
  
  }else{
    echo json_encode('empty');
  } 

}else{
  http_response_code(404);
}


