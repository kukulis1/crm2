<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Documentstorage_Save_Action extends Vtiger_Action_Controller {	


	public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$record = $request->get('record');

		$actionName = ($record) ? 'EditView' : 'CreateView';
		if(!Users_Privileges_Model::isPermitted($moduleName, $actionName, $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if(!Users_Privileges_Model::isPermitted($moduleName, 'Save', $record)) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}

		if ($record) {
			$recordEntityName = getSalesEntityType($record);
			if ($recordEntityName !== $moduleName) {
				throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
			}
		}
	}

	public function process(Vtiger_Request $request) {
		$db = PearDatabase::getInstance();
		$date = date("Y-m-d H:i:s");
		global $current_user;

		// NOTE kuriam nauja aplanka
		if($request->get('controller') == 'addNewFolder'){	
			$roles_sql = "SELECT roleid FROM vtiger_role WHERE roleid != 'H1'";
			$sql2 = "INSERT INTO vtiger_documentstorage_folder_permisions (folder_id, folder_type, roles) VALUES (?,?,?)";	
			if($request->get('type') == 2){
				$roles =	$db->pquery($roles_sql,array());
				$role_array = array();

				foreach($roles AS $r){
					if($request->get($r['roleid'])){
						$role_array[] = $r['roleid'];
					}
				}
				$roleList = implode(',',$role_array);
			}

			// NOTE tikrinam ar kuriamas naujas aplankas ar sub aplankas
			if($request->get('recordid')){ 
				$recordid = $request->get('recordid');
				$sql = "INSERT INTO vtiger_documentstorage_subfolders (parent, title, type, smcreatorid, modifiedby,deleted, createdtime, modifiedtime) VALUES (?,?,?,?,?,?,?,?)";
				$db->pquery($sql,array($recordid,$request->get('title'),$request->get('type'),$current_user->id,$current_user->id,0,$date,$date));
				$subrecordid = $db->getLastInsertID();		
				if($request->get('type') == 2){
					$db->pquery($sql2,array($subrecordid,2,$roleList));
				}
			}else{
				$sql = "INSERT INTO vtiger_documentstorage_folders (title,type, smcreatorid, modifiedby,deleted, createdtime, modifiedtime) VALUES (?,?,?,?,?,?,?)";
				$db->pquery($sql,array($request->get('title'),$request->get('type'),$current_user->id,$current_user->id,0,$date,$date));
				$recordid = $db->getLastInsertID();	
				if($request->get('type') == 2){			
					$db->pquery($sql2,array($recordid,1,$roleList));
				}
			}

			header("Location:index.php?module=Documentstorage&view=Catalog&record=$recordid");

		// NOTE aplanko redagavimas
		}elseif($request->get('controller') == 'editFolder'){	
			$roles_sql = "SELECT roleid FROM vtiger_role WHERE roleid != 'H1'";
			$sql2 = "UPDATE vtiger_documentstorage_folder_permisions SET roles = ? WHERE folder_id = ? AND folder_type = ?";	
			$sql3 = "DELETE FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = ?";	
			$sql4 = "INSERT INTO vtiger_documentstorage_folder_permisions (folder_id, folder_type, roles) VALUES (?,?,?)";	
			$sql5 = "SELECT folder_id FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = ?";
			if($request->get('type') == 2){
				$roles =	$db->pquery($roles_sql,array());
				$role_array = array();

				foreach($roles AS $r){
					if($request->get($r['roleid'])){
						$role_array[] = $r['roleid'];
					}
				}
				$roleList = implode(',',$role_array);
			}

			if($request->get('folder') == 'parent'){	
				$sql = "UPDATE vtiger_documentstorage_folders SET title = ?,type = ?,modifiedby = ?, modifiedtime = ? WHERE id = ?";
			}else{
				$sql = "UPDATE vtiger_documentstorage_subfolders SET title = ?,type = ?,modifiedby = ?, modifiedtime = ? WHERE id = ?";
			}	
			$recordid = $request->get('recordid');			
			$db->pquery($sql,array($request->get('title'),$request->get('type'),$current_user->id,$date,$recordid));

			if($request->get('type') == 2){		
				if($request->get('folder') == 'parent'){
					$check = $db->pquery($sql5,array($recordid,1));
					$check_num_rows = $db->num_rows($check);
					if($check_num_rows){
						if(empty($role_array)){
							$db->pquery($sql3,array($recordid,1));
						}else{
							$db->pquery($sql2,array($roleList,$recordid,1));
						}
					}else{
						$db->pquery($sql4,array($recordid,1,$roleList));
					}
				}else{
					$check = $db->pquery($sql5,array($recordid,2));
					$check_num_rows = $db->num_rows($check);

					if($check_num_rows){
						if(empty($role_array)){
							$db->pquery($sql3,array($recordid,2));
						}else{
							$db->pquery($sql2,array($roleList,$recordid,2));
						}
					}else{
						$db->pquery($sql4,array($recordid,2,$roleList));
					}
				}
			}else{
				if($request->get('folder') == 'parent'){
					$db->pquery($sql3,array($recordid,1));
				}else{
					$db->pquery($sql3,array($recordid,2));
				}
			}

			$catalog = ($request->get('folder') == 'sub' ? 'SubCatalog' : 'Catalog');		
			header("Location:index.php?module=Documentstorage&view={$catalog}&record=$recordid");

		// NOTE pridedamas naujas failas
		}elseif($request->get('controller') == 'addNewFile'){
			$recordid = $request->get('recordid');
			$file_type = $request->get('template');
			if($request->get('type') == 0){
				$file_type = 0;
			}
			$sql = "SELECT * FROM vtiger_documentstorage_temp_files WHERE flupload = ?";
			$sql2 = "INSERT INTO vtiger_documentstorage (documentstorageid,folder_id,folder_type,file_type, filename, path, temp_filename, size, extension) VALUES (?,?,?,?,?,?,?,?,?)";

			$sql3 = "DELETE FROM vtiger_documentstorage_temp_files WHERE flupload = ?";
		  $result =	$db->pquery($sql,array($current_user->id));

			foreach($result AS $item){
				$entity_id = $this->getEntityId();
				$this->insertEntity('Documentstorage', $entity_id, $item['flupload'], NULL, $item['filename'], $item['createdtime']);
				$db->pquery($sql2,array($entity_id,$item['folder_id'],$item['folder_type'],$file_type,$item['filename'],$item['path'],$item['temp_filename'],$item['size'],$item['extension']));
			}
			$db->pquery($sql3,array($current_user->id));
			$catalog = ($request->get('folder') == 'sub' ? 'SubCatalog' : 'Catalog');

			header("Location:index.php?module=Documentstorage&view={$catalog}&record=$recordid");

		// NOTE Redaguojamas failas
		}else if($request->get('controller') == 'editFile'){
			$root = $_SERVER['DOCUMENT_ROOT'];	
			$recordid = $request->get('recordid');
			$user_id = $request->get('user_id');
			$file_id = $request->get('file_id');
			$folder = $request->get('folder');
			$file_type = $request->get('template');
			$old_file = $request->get('old_file');
			$folder_type = ($request->get('folder') == 'sub' ? 2 : 1);

			if($request->get('type') == 0){
				$file_type = 0;
			}

			if($old_file){
				$sql = "SELECT * FROM vtiger_documentstorage_temp_files WHERE folder_id = ? AND folder_type = ? AND flupload = ?";
				$result =	$db->pquery($sql,array($recordid,$folder_type,$user_id));
				$file = $db->fetch_array($result);
		
				$sql2 = "UPDATE vtiger_documentstorage SET file_type = ?, filename = ?, path = ?, temp_filename = ?, size = ?, extension = ? WHERE documentstorageid = ?";
				$db->pquery($sql2,array($file_type,$file['filename'],$file['path'],$file['temp_filename'],$file['size'],$file['extension'],$file_id));

				unlink("$root/documents_storage/$old_file");

				$sql3 = "DELETE FROM vtiger_documentstorage_temp_files WHERE folder_id = ? AND folder_type = ? AND flupload = ?";
				$db->pquery($sql3,array($recordid,$folder_type,$user_id));

			}else{
				$sql4 = "UPDATE vtiger_documentstorage SET file_type = ? WHERE documentstorageid = ?";
				$db->pquery($sql4,array($file_type,$file_id));
			}	

			$catalog = ($request->get('folder') == 'sub' ? 'SubCatalog' : 'Catalog');
			header("Location:index.php?module=Documentstorage&view={$catalog}&record=$recordid");
	

		// NOTE redaguojami nustatymai	
		}else if($request->get('controller') == 'SettingsEdit'){
			$sql = "SELECT roleid FROM vtiger_role WHERE roleid != 'H1'";
			$sql2 = "UPDATE vtiger_documentstorage_permisions SET roles = ?";

			$result =	$db->pquery($sql,array());
			$role_array = array();

			foreach($result AS $r){
				if($request->get($r['roleid'])){
					$role_array[] = $r['roleid'];
				}
			}
			$roleList = implode(',',$role_array);
			$db->pquery($sql2,array($roleList));
			header("Location: index.php?module=Documentstorage&view=Settings");

		}else if($request->get('controller') == 'SettingsEditRowNo'){
			$sql = "UPDATE vtiger_documentstorage_employment_contract_no SET number = ?";
			$contract_id = $request->get('employment_contract_no');
			$db->pquery($sql,array($contract_id));
			header("Location: index.php?module=Documentstorage&view=Settings");

		// NOTE Naujo šablono kūrimas
		}else if($request->get('controller') == 'AddTemplate'){	
			$num_rows = $request->get('count_inputs');
			$sql = "INSERT INTO vtiger_documentstorage_templates (title,position, module, classificator,created_person,created_at) VALUES (?,?,?,?,?,?)";
			$classificator = array();

			$from = (empty($request->get("fields1")) ? 2 : 1);

			for($i = 1; $i <= $num_rows; $i++){
				$classificator[$request->get("variable$i")] = $request->get("fields$i");
			}
			
			$output = implode(':', array_map(
				function ($v, $k) { return sprintf("%s=%s", $k, $v); },
				$classificator,
				array_keys($classificator)
			));

			$position = implode(',',$request->get("position"));
			if(empty($position)) $position = 0;	

			$db->pquery($sql,array($request->get("template_name"),$position,$request->get("select_module"),$output,$current_user->id,$date));
			header("Location: index.php?module=Documentstorage&view=Templates");
			
		// NOTE Šablono redagavimas
		}else if($request->get('controller') == 'EditTemplate'){	
			$recordid = $request->get('recordid');
			$num_rows = $request->get('count_inputs');

			$sql = "UPDATE vtiger_documentstorage_templates SET title = ?, position = ?, module = ?, classificator = ? WHERE id = ?";
			$classificator = array();

			$from = (empty($request->get("fields1")) ? 2 : 1);

			for($i = 1; $i <= $num_rows; $i++){
				$classificator[$request->get("variable$i")] = $request->get("fields$i");
			}
			
			$output = implode(':', array_map(
				function ($v, $k) { return sprintf("%s=%s", $k, $v); },
				$classificator,
				array_keys($classificator)
			));	

			$position = implode(',',$request->get("position"));	
			if(empty($position)) $position = 0;	

			$db->pquery($sql,array($request->get("template_name"),$position,$request->get("select_module"),$output,$recordid));
			header("Location: index.php?module=Documentstorage&view=Templates");

		}elseif($request->get('controller') == 'AddDocType'){
			$sql = "INSERT INTO vtiger_documentstorage_file_types (file_type, create_person, type, created_at) VALUES (?,?,?,?)";
			$db->pquery($sql,array($request->get("title"),$request->get("current_user"),$request->get("type"),$date));
			header("Location: index.php?module=Documentstorage&view=DocTypes");

		}elseif($request->get('controller') == 'EditDocType'){
			$sql = "UPDATE vtiger_documentstorage_file_types SET file_type = ?, type = ? WHERE id = ?";	
			$db->pquery($sql,array($request->get("title"),$request->get("type"),$request->get("recordid")));
			header("Location: index.php?module=Documentstorage&view=DocTypes");

		// NOTE paliktas vtiger numatytas saugojimas
		}else{
			try {
				$recordModel = $this->saveRecord($request);
				if ($request->get('returntab_label')){
					$loadUrl = 'index.php?'.$request->getReturnURL();
				} else if($request->get('relationOperation')) {
					$parentModuleName = $request->get('sourceModule');
					$parentRecordId = $request->get('sourceRecord');
					$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentRecordId, $parentModuleName);
					//TODO : Url should load the related list instead of detail view of record
					$loadUrl = $parentRecordModel->getDetailViewUrl();
				} else if ($request->get('returnToList')) {
					$loadUrl = $recordModel->getModule()->getListViewUrl();
				} else if ($request->get('returnmodule') && $request->get('returnview')) {
					$loadUrl = 'index.php?'.$request->getReturnURL();
				} else {
					$loadUrl = $recordModel->getDetailViewUrl();
				}
				//append App name to callback url
				//Special handling for vtiger7.
				$appName = $request->get('appName');
				if(strlen($appName) > 0){
					$loadUrl = $loadUrl.$appName;
				}
				header("Location: $loadUrl");
			} catch (DuplicateException $e) {
				$requestData = $request->getAll();
				$moduleName = $request->getModule();
				unset($requestData['action']);
				unset($requestData['__vtrftk']);
	
				if ($request->isAjax()) {
					$response = new Vtiger_Response();
					$response->setError($e->getMessage(), $e->getDuplicationMessage(), $e->getMessage());
					$response->emit();
				} else {
					$requestData['view'] = 'Edit';
					$requestData['duplicateRecords'] = $e->getDuplicateRecordIds();
					$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
	
					global $vtiger_current_version;
					$viewer = new Vtiger_Viewer();
	
					$viewer->assign('REQUEST_DATA', $requestData);
					$viewer->assign('REQUEST_URL', $moduleModel->getCreateRecordUrl().'&record='.$request->get('record'));
					$viewer->view('RedirectToEditView.tpl', 'Vtiger');
				}
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		}
		
	}



	public function getEntityId(){
		$db = PearDatabase::getInstance();
		$sql = "SELECT id FROM `vtiger_crmentity_seq`";
		$sql2 = "UPDATE `vtiger_crmentity_seq` SET id = ?";
		$lock = 'LOCK TABLES vtiger_crmentity_seq READ'; 
		$lock2 = 'LOCK TABLES vtiger_crmentity_seq WRITE';
		$lock3 = 'UNLOCK TABLES';

		$db->pquery($lock, array()); 
			$get_id = $db->pquery($sql, array()); 
			$seq = $db->query_result($get_id,0,'id');    
		$db->pquery($lock3, array());    

		$new_seq = $seq + 1;   

		$db->pquery($lock2, array()); 
		$db->pquery($sql2, array($new_seq)); 
		$db->pquery($lock3, array()); 

		return $new_seq;
	}

	public function insertEntity($setype, $entity_id, $user_id, $description, $label, $date){
		$db = PearDatabase::getInstance();
		$sql = 'INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, createdtime, modifiedtime) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
		$db->pquery($sql, array($entity_id, $user_id, $user_id, $setype, $description, $label, $date, $date)); 
	}

	public function saveRecord($request) {
		$recordModel = $this->getRecordModelFromRequest($request);
		if($request->get('imgDeleted')) {
			$imageIds = $request->get('imageid');
			foreach($imageIds as $imageId) {
				$status = $recordModel->deleteImage($imageId);
			}
		}
		$recordModel->save();
		if($request->get('relationOperation')) {
			$parentModuleName = $request->get('sourceModule');
			$parentModuleModel = Vtiger_Module_Model::getInstance($parentModuleName);
			$parentRecordId = $request->get('sourceRecord');
			$relatedModule = $recordModel->getModule();
			$relatedRecordId = $recordModel->getId();
			if($relatedModule->getName() == 'Events'){
				$relatedModule = Vtiger_Module_Model::getInstance('Calendar');
			}

			$relationModel = Vtiger_Relation_Model::getInstance($parentModuleModel, $relatedModule);
			$relationModel->addRelation($parentRecordId, $relatedRecordId);
		}
		$this->savedRecordId = $recordModel->getId();
		return $recordModel;
	}

	/**
	 * Function to get the record model based on the request parameters
	 * @param Vtiger_Request $request
	 * @return Vtiger_Record_Model or Module specific Record Model instance
	 */
	protected function getRecordModelFromRequest(Vtiger_Request $request) {

		$moduleName = $request->getModule();
		$recordId = $request->get('record');

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		if(!empty($recordId)) {
			$recordModel = Vtiger_Record_Model::getInstanceById($recordId, $moduleName);
			$recordModel->set('id', $recordId);
			$recordModel->set('mode', 'edit');
		} else {
			$recordModel = Vtiger_Record_Model::getCleanInstance($moduleName);
			$recordModel->set('mode', '');
		}

		$fieldModelList = $moduleModel->getFields();
		foreach ($fieldModelList as $fieldName => $fieldModel) {
			$fieldValue = $request->get($fieldName, null);
			$fieldDataType = $fieldModel->getFieldDataType();
			if($fieldDataType == 'time' && $fieldValue !== null){
				$fieldValue = Vtiger_Time_UIType::getTimeValueWithSeconds($fieldValue);
			}
            $ckeditorFields = array('commentcontent', 'notecontent');
            if((in_array($fieldName, $ckeditorFields)) && $fieldValue !== null){
                $purifiedContent = vtlib_purify(decode_html($fieldValue));
                // Purify malicious html event attributes
                $fieldValue = purifyHtmlEventAttributes(decode_html($purifiedContent),true);
			}
			if($fieldValue !== null) {
				if(!is_array($fieldValue) && $fieldDataType != 'currency') {
					$fieldValue = trim($fieldValue);
				}
				$recordModel->set($fieldName, $fieldValue);
			}
		}
		return $recordModel;
	}

}
