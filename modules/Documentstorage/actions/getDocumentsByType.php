<?php

include($_SERVER['DOCUMENT_ROOT'].'/v1/external_data/mysql_connection.php');   

if($_POST){

  try {
        $dbh->beginTransaction();  
        


        $sth = $dbh->prepare("SELECT id,filename,documentstorageid
                              FROM vtiger_documentstorage_templates t
                              JOIN vtiger_documentstorage d ON d.file_type=t.id
                              INNER JOIN vtiger_crmentity e ON e.crmid=d.documentstorageid
                              WHERE deleted = 0 AND t.id = ?
                              GROUP BY documentstorageid
                              ORDER BY documentstorageid DESC");

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($_POST['template']));

        $documents = array();

        foreach($sth AS $row){ 
          $documents[] = $row;
        }
       
        echo json_encode(array('status' => 'success','data' => $documents));

      $dbh->commit();
  } catch (PDOException $e) {
    $dbh->rollBack();    
    echo json_encode(array('status' => 'fail'));
  }
}else{
  http_response_code(404);
} 