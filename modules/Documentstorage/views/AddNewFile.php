<?php

use Documentstorage_Module_Model AS Module;
use Documentstorage_Templates_Model AS Template;

class Documentstorage_AddNewFile_View extends Vtiger_Index_View {

  public function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$record = $request->get('record');
    $folder = $_GET['folder'];
    global $current_user;	
    $query_folder = ($folder == 'parent' ? 'folders' : 'subfolders');
      if(!Module::checkFolderEditPermision($record,$current_user,$query_folder)){
        throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
      }
  }
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $recordid = $request->get('record');
    $folder = $request->get('folder');
    $query_folder = ($folder == 'parent' ? 'folders' : 'subfolders');
    $folder_info = Module::getFolderInfo($recordid,$query_folder);
    $template_list = Template::getTemplates();
    $viewer->assign('FOLDER_INFO', $folder_info);
    $viewer->assign('TEMPLATE_LIST', $template_list);
    $viewer->assign('FOLDER', $folder);
    $viewer->assign('USER_ID', $current_user->id);
    $viewer->assign('FORM_FILE', 'AddNewFile');
    $viewer->view('FormView.tpl', $moduleName);
  }

}