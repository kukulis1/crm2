<?php

use Documentstorage_DocTypes_Model AS DocTypes;
class Documentstorage_DocTypes_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) { 
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();
    $doc_types = DocTypes::getDocTypes();
   
    $viewer->assign('DOC_TYPES', $doc_types);
    $viewer->view('DocTypes.tpl', $moduleName);
  }

}