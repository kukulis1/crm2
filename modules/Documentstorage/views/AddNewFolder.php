<?php

class Documentstorage_AddNewFolder_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    $recordid = $request->get('record');
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $roleList = Documentstorage_Settings_Model::getRoleList();
    $viewer->assign('ROLE_LIST', $roleList);
    $viewer->assign('RECORD', $recordid);
    $viewer->assign('FORM_FILE', 'AddNewFolder');
    $viewer->view('FormView.tpl', $moduleName);
  }

}