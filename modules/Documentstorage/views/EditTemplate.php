<?php
use Documentstorage_Templates_Model AS Template;

class Documentstorage_EditTemplate_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();
    $position_list = Template::getPositions();
    $template_modules_list = Template::getTemplateModules();
    $record = $_GET['record']; 
    $template = Template::getSpecificTemplate($record);
    $module_fields = Template::getModuleFields($record);
    $managers = Template::getManagers();
    $viewer->assign('RECORD', $record);
    $viewer->assign('TEMPLATE', $template);
    $viewer->assign('MODULE_FIELDS', $module_fields);
    $viewer->assign('MANAGERS', $managers);
    $viewer->assign('POSITION_LIST', $position_list);
    $viewer->assign('TEMPLATE_MODULES_LIST', $template_modules_list);    
    $viewer->assign('FORM_FILE', 'EditTemplate');    
    $viewer->view('FormView.tpl', $moduleName);
  }

}