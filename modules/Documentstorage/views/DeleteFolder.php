<?php
use Documentstorage_DeleteController_Model AS DeleteFolder;
class Documentstorage_DeleteFolder_View extends Vtiger_Index_View {

  public function process(Vtiger_Request $request) {
    $folder = $request->get('folder'); 
    $recordid = $request->get('record');    
    if($folder == 'parent'){
      DeleteFolder::processDeleteParentFolder($recordid);
      header("Location:index.php?module=Documentstorage&view=List");
    }else if($folder == 'sub'){
      $record = DeleteFolder::processDeleteSubFolder($recordid);
      header("Location:index.php?module=Documentstorage&view=Catalog&record=$record");
    }  
	}
}