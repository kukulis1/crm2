<?php

class Documentstorage_SettingsEditRowNo_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $number = Documentstorage_Record_Model::getEmploymentContractNo();
    $viewer->assign('RECORD', true);
    $viewer->assign('NUMBER', $number);
    $viewer->assign('FORM_FILE', 'SettingsEditRowNo');
    $viewer->view('FormView.tpl', $moduleName);
  }

}