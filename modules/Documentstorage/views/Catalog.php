<?php

use Documentstorage_Catalog_Model AS Catalog;
use Documentstorage_Module_Model AS Module;

class Documentstorage_Catalog_View extends Vtiger_Index_View {

  // NOTE patikrinam ar turi teises perziureti aplanka
  public function checkPermission(Vtiger_Request $request) {
    global $current_user;	
		$moduleName = $request->getModule();
		$record = $request->get('record');   
    $folder = $_GET['folder'];   	
    $folder_type = ($folder == 'parent' ? 1 : 2);
    if(!Module::checkFolderPermision($record,$current_user,'folders',$folder_type)){
		 throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
    }
  }
  
	public function process(Vtiger_Request $request) {
    global $current_user;	
    $recordid = $request->get('record');
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $folder_info = Catalog::getFolderInfo($recordid);
    $getSubFolderInfo = Catalog::getSubFolderInfo($recordid);   

    $getFolderFiles = Catalog::getFolderFiles($recordid);
    $folderFiles = $getFolderFiles['result'];
    $folderFilesNumRows = $getFolderFiles['num_rows'];
    $folderFilesTotal = $getFolderFiles['total_records'];
    $perPage = $getFolderFiles['per_page'];
    $totalPages = ceil($folderFilesTotal/$perPage);

    $query_folder = ($_GET['view'] == 'Catalog' ? 'folders' : 'subfolders');
    $editPermision = Module::checkFolderEditPermision($recordid,$current_user,$query_folder);

    $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		$viewer->assign('MODULE_MODEL', $moduleModel);
    $viewer->assign('FOLDER_INFO', $folder_info);
    $viewer->assign('SUBFOLDER_INFO', $getSubFolderInfo);
    $viewer->assign('FOLDER_FILES', $folderFiles);
    $viewer->assign('FOLDER_FILES_NUM_ROWS', $folderFilesNumRows);   
    $viewer->assign('TOTAL_FILES', $folderFilesTotal);  
    $viewer->assign('PER_PAGE', $perPage);  
    $viewer->assign('TOTAL_PAGES', $totalPages);  
    $viewer->assign('EDIT_PERMISION', $editPermision);  
    $viewer->assign('CURRENT_USER', $current_user);  
    $viewer->view('Catalog.tpl', $moduleName);
  }  
}
