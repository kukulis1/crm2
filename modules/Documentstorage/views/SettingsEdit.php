<?php

class Documentstorage_SettingsEdit_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $roleList = Documentstorage_Settings_Model::getRoleList();
    $viewer->assign('RECORD', true);
    $viewer->assign('ROLE_LIST', $roleList);
    $viewer->assign('FORM_FILE', 'SettingsEdit');
    $viewer->view('FormView.tpl', $moduleName);
  }

}