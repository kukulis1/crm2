<?php

use Documentstorage_Templates_Model AS Template;
class Documentstorage_Templates_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $templates = Template::getTemplates();
    $viewer->assign('TEMPLATES', $templates);
    $viewer->view('Templates.tpl', $moduleName);
  }

}