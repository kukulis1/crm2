<?php

class Documentstorage_Settings_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $roleList = Documentstorage_Settings_Model::getRoleList();
    $number = Documentstorage_Record_Model::getEmploymentContractNo();
    $viewer->assign('NUMBER', $number);
    $viewer->assign('ROLE_LIST', $roleList);
    $viewer->view('Settings.tpl', $moduleName);
  }

}