<?php
use Documentstorage_GenerateDoc_Model AS GenerateDoc;

class Documentstorage_GenerateDoc_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) { 
    GenerateDoc::generateEmploymentContract($request->get('templateid'),$request->get('docid'),$request->get('record'));    
  }
}