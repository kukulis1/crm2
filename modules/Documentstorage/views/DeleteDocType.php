<?php
use Documentstorage_DeleteController_Model AS DeleteController;

class Documentstorage_DeleteDocType_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {   
    DeleteController::deleteDocType($request->get('record'));
  }
}