<?php
// use Documentstorage_Templates_Model AS Template;

class Documentstorage_AddDocType_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();
    $viewer->assign('CURRENT_USER', $current_user);    
    $viewer->assign('FORM_FILE', 'AddDocType');
    $viewer->view('FormView.tpl', $moduleName);
  }
}