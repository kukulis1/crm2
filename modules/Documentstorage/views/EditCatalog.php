<?php

use Documentstorage_Catalog_Model AS Catalog;
use Documentstorage_SubCatalog_Model AS SubCatalog;
use Documentstorage_Module_Model AS Module;

class Documentstorage_EditCatalog_View extends Vtiger_Index_View {

  public function checkPermission(Vtiger_Request $request) {
    global $current_user;
		$moduleName = $request->getModule();
		$record = $request->get('record');
    $folder = $_GET['folder'];   	
    $query_folder = ($folder == 'parent' ? 'folders' : 'subfolders');
      if(!Module::checkFolderEditPermision($record,$current_user,$query_folder)){
        throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
      }
  }
  
	public function process(Vtiger_Request $request) {
    $recordid = $request->get('record');
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
    $folder = $_GET['folder'];
    if($folder == 'parent'){
      $folder_info = Catalog::getFolderInfo($recordid);
    }else{
      $folder_info = SubCatalog::getFolderInfo($recordid);
    }
    $folder = $_GET['folder'];   	
    $folder_type = ($folder == 'parent' ? 1 : 2);
    $roleList = Catalog::getFolderRoleList($recordid,$folder_type);
    $viewer->assign('RECORD', $recordid);
    $viewer->assign('ROLE_LIST', $roleList);    
    $viewer->assign('FORM_FILE', 'AddNewFolder');
    $viewer->assign('FOLDER', $folder);
    $viewer->assign('FOLDER_INFO', $folder_info);
    $viewer->view('FormView.tpl', $moduleName);
  }  
}