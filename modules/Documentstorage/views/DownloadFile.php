<?php

class Documentstorage_DownloadFile_View extends Vtiger_Index_View {

  public function process(Vtiger_Request $request) {
    $file_id = $request->get('file_id'); 
    $db = PearDatabase::getInstance();   
    $root = $_SERVER['DOCUMENT_ROOT'];
    $sql = "SELECT * FROM vtiger_documentstorage WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($file_id));
    $file_info = $db->fetch_array($result);

		if (!empty($file_info)){   
      $filepath = $root."/documents_storage/".$file_info['path']."/".$file_info['temp_filename'].".".$file_info['extension'];
      $fileurl = "http://".$_SERVER["HTTP_HOST"]."/documents_storage/".$file_info['path']."/".$file_info['temp_filename'].".".$file_info['extension'];

      ob_start();
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.$file_info['filename'].".".$file_info['extension'].'"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($filepath));
      flush(); // Flush system output buffer
      while (ob_get_level()) {
        ob_end_clean();
      }
      readfile($filepath); 
      exit();
		}

	}
}