<?php
use Documentstorage_Module_Model AS Module;
use Documentstorage_Templates_Model AS Template;

class Documentstorage_EditFile_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {  
    global $current_user;
    $recordid = $request->get('record');
    $file_id = $request->get('file_id');
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();
    $template_list = Template::getTemplates();
    $folder = $request->get('folder');
    $file_info = Module::getFileInfo($file_id);    
    $viewer->assign('FILE_INFO', $file_info);
    $viewer->assign('FOLDER', $folder);
    $viewer->assign('USER_ID', $current_user->id);
    $viewer->assign('RECORD', $recordid);
    $viewer->assign('TEMPLATE_LIST', $template_list);
    $viewer->assign('FORM_FILE', 'EditFile');
    $viewer->view('FormView.tpl', $moduleName);
  }

}