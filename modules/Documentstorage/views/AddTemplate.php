<?php
use Documentstorage_Templates_Model AS Template;
class Documentstorage_AddTemplate_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
    global $current_user;
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();
    $position_list = Template::getPositions();
    $template_modules_list = Template::getTemplateModules();   
    $viewer->assign('POSITION_LIST', $position_list);
    $viewer->assign('TEMPLATE_MODULES_LIST', $template_modules_list);    
    $viewer->assign('FORM_FILE', 'AddTemplate');
    $viewer->view('FormView.tpl', $moduleName);
  }

}