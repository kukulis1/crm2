<?php
use Documentstorage_DocTypes_Model AS DocTypes;

class Documentstorage_EditDocType_View extends Vtiger_Index_View {
  
	public function process(Vtiger_Request $request) {
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();
    $record = $request->get('record');
    $doc_type = DocTypes::getDocType($record);
    $viewer->assign('DOC_TYPE', $doc_type);       
    $viewer->assign('FORM_FILE', 'EditDocType');
    $viewer->view('FormView.tpl', $moduleName);
  }
}