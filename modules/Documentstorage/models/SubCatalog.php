<?php

class Documentstorage_SubCatalog_Model extends Vtiger_Module_Model {

  private $db;

  public function getFolderInfo($recordid) { 
    $db = PearDatabase::getInstance();   
    $sql = "SELECT f.*, CONCAT(`first_name`,' ',`last_name`) AS owner  
            FROM vtiger_documentstorage_subfolders f
            LEFT JOIN vtiger_users u ON u.id=f.smcreatorid
            WHERE f.deleted = 0 AND f.id = ? ORDER BY f.id ASC";
    $result = $db->pquery($sql,array($recordid));
    return $db->fetch_array($result);
  }

  public function getFolderFiles($recordid){
    $PER_PAGE = 20;
    $db = PearDatabase::getInstance(); 
    $sql = "SELECT d.*, CONCAT(`first_name`,' ',`last_name`) AS owner ,e.createdtime,e.modifiedtime
            FROM vtiger_documentstorage d
            INNER JOIN vtiger_crmentity e ON e.crmid=d.documentstorageid
            LEFT JOIN vtiger_users u ON u.id=e.smcreatorid
            WHERE e.deleted = 0 AND folder_id = ? AND folder_type = 2
            ORDER BY documentstorageid DESC ";
    $sql2 .= " LIMIT $PER_PAGE";

    $result = $db->pquery($sql.$sql2,array($recordid));
    $result2 = $db->pquery($sql,array($recordid));
    $num_rows = $db->num_rows($result);
    $num_rows_all = $db->num_rows($result2);
    return array('result' => $result, 'num_rows' => $num_rows,'total_records' => $num_rows_all,'per_page' => $PER_PAGE);
  }
}