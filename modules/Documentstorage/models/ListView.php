<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

/**
 * Vtiger ListView Model Class
 */
class Documentstorage_ListView_Model extends Vtiger_ListView_Model {

	// NOTE Gaunam visa aplanku sarasa ir ji prafiltruojam pagal teises
	public function getFolders(){
		$db = PearDatabase::getInstance();
		global $current_user;

		$roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));
		$check = "SELECT roles FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = ?";
		$sql = "SELECT * FROM vtiger_documentstorage_folders WHERE deleted = 0 AND `hidden` = 0";
		$result = $db->pquery($sql, array());

		$folders = array();

		foreach($result AS $row){
			$result2 = $db->pquery($check,array($row['id'],$row['type']));
			$role = $db->query_result($result2,0,'roles');
			$roles = explode(',',$role);

			if($row['type'] == 1){
				$folders[] = $row;
			}else if($row['smcreatorid'] == $current_user->id){
				$folders[] = $row;
			}else if(in_array($current_user->roleid,$admins)){
				$folders[] = $row;
			}else if(in_array($current_user->roleid,$roles)){
				$folders[] = $row;
			}
		}	

		return $folders;
	}

	public function getListViewEntries($pagingModel) {
		$db = PearDatabase::getInstance();
		global $current_user;
		$moduleName = $this->getModule()->get('name');
		$moduleFocus = CRMEntity::getInstance($moduleName);
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$queryGenerator = $this->get('query_generator');
		$listViewContoller = $this->get('listview_controller');

		 $searchParams = $this->get('search_params');
		if(empty($searchParams)) {
			$searchParams = array();
		}
		$glue = "";
		if(count($queryGenerator->getWhereFields()) > 0 && (count($searchParams)) > 0) {
			$glue = QueryGenerator::$AND;
		}
		$queryGenerator->parseAdvFilterList($searchParams, $glue);

		$searchKey = $this->get('search_key');
		$searchValue = $this->get('search_value');
		$operator = $this->get('operator');
		if(!empty($searchKey)) {
			$queryGenerator->addUserSearchConditions(array('search_field' => $searchKey, 'search_text' => $searchValue, 'operator' => $operator));
		}

		$orderBy = $this->getForSql('orderby');
		$sortOrder = $this->getForSql('sortorder');

		if(!empty($orderBy)){
			$queryGenerator = $this->get('query_generator');
			$fieldModels = $queryGenerator->getModuleFields();
			$orderByFieldModel = $fieldModels[$orderBy];
			if($orderByFieldModel && ($orderByFieldModel->getFieldDataType() == Vtiger_Field_Model::REFERENCE_TYPE ||
					$orderByFieldModel->getFieldDataType() == Vtiger_Field_Model::OWNER_TYPE)){
				$queryGenerator->addWhereField($orderBy);
			}
		}
		$listQuery = $this->getQuery();

		$SELECT = "SELECT";
		$ADD = ' folder_id,folder_type, ';
		$listQuery = str_replace($SELECT, $SELECT.$ADD, $listQuery);

		$sourceModule = $this->get('src_module');
		if(!empty($sourceModule)) {
			if(method_exists($moduleModel, 'getQueryByModuleField')) {
				$overrideQuery = $moduleModel->getQueryByModuleField($sourceModule, $this->get('src_field'), $this->get('src_record'), $listQuery,$this->get('relationId'));
				if(!empty($overrideQuery)) {
					$listQuery = $overrideQuery;
				}
			}
		}

		$startIndex = $pagingModel->getStartIndex();
		$pageLimit = $pagingModel->getPageLimit();
		$paramArray = array();

		if(!empty($orderBy) && $orderByFieldModel) {
			if($orderBy == 'roleid' && $moduleName == 'Users'){
				$listQuery .= ' ORDER BY vtiger_role.rolename '.' '. $sortOrder; 
			} else {
				$listQuery .= ' ORDER BY '.$queryGenerator->getOrderByColumn($orderBy).' '.$sortOrder;
			}

			if ($orderBy == 'first_name' && $moduleName == 'Users') {
				$listQuery .= ' , last_name '.' '. $sortOrder .' ,  email1 '. ' '. $sortOrder;
			} 
		} else if(empty($orderBy) && empty($sortOrder) && $moduleName != "Users"){
			//List view will be displayed on recently created/modified records
			$listQuery .= ' ORDER BY vtiger_crmentity.modifiedtime DESC';
		}

		$viewid = ListViewSession::getCurrentView($moduleName);
		if(empty($viewid)) {
			$viewid = $pagingModel->get('viewid');
		}
		$_SESSION['lvs'][$moduleName][$viewid]['start'] = $pagingModel->get('page');

		ListViewSession::setSessionQuery($moduleName, $listQuery, $viewid);

		$listQuery .= " LIMIT ?, ?";
		array_push($paramArray, $startIndex);
		array_push($paramArray, ($pageLimit+1));
		
		$listResult = $db->pquery($listQuery, $paramArray);

		$listViewRecordModels = array();
		$listViewEntries =  $listViewContoller->getListViewRecords($moduleFocus,$moduleName, $listResult);

		$pagingModel->calculatePageRange($listViewEntries);

		if($db->num_rows($listResult) > $pageLimit){
			array_pop($listViewEntries);
			$pagingModel->set('nextPageExists', true);
		}else{
			$pagingModel->set('nextPageExists', false);
		}


		// NOTE Gaunam nustatymuose nustatytu adminu sarasa kurie gales matyti visus aplankus
		$roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));    

		// NOTE Gaunam sarasa roliu kas gali matyti aplanka
		$check = "SELECT roles FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = ?"; 
		
		$index = 0;
		foreach($listViewEntries as $recordId => $record) {		
			$rawData = $db->query_result_rowdata($listResult, $index++);
			$record['id'] = $recordId;	

			$result2 = $db->pquery($check,array($record['folder_id'],$record['folder_type']));
			$role = $db->query_result($result2,0,'roles');
			$roles = explode(',',$role);

			if($record['folder_type'] == 1){
				$sql = "SELECT `type`,smcreatorid FROM vtiger_documentstorage_folders WHERE id = ?";
			}else{
				$sql = "SELECT f.type,f.smcreatorid FROM vtiger_documentstorage_subfolders s
																					  JOIN vtiger_documentstorage_folders f ON f.id=s.parent 
				 																		WHERE s.id = ?";
			}


			$getType = $db->pquery($sql,array($record['folder_id']));
			$type = $db->query_result($getType,0,'type');
			$smcreatorid = $db->query_result($getType,0,'smcreatorid');

			if($type == 1){
				$listViewRecordModels[$recordId] = $moduleModel->getRecordFromArray($record, $rawData);
			}else if($record['smcreatorid'] == $current_user->id){
				$listViewRecordModels[$recordId] = $moduleModel->getRecordFromArray($record, $rawData);
			}else if(in_array($current_user->roleid,$admins)){
				$listViewRecordModels[$recordId] = $moduleModel->getRecordFromArray($record, $rawData);
			}else if(in_array($current_user->roleid,$roles)){
				$listViewRecordModels[$recordId] = $moduleModel->getRecordFromArray($record, $rawData);
			}
		}

		return $listViewRecordModels;
	}

}
