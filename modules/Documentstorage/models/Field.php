<?php

class Documentstorage_Field_Model extends Vtiger_Field_Model {
	
	public function getFolderName($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT title,folder_id FROM vtiger_documentstorage d JOIN vtiger_documentstorage_folders f ON d.folder_id=f.id WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id)); 
    $title = $db->query_result($result,0,'title');   
    $id = $db->query_result($result,0,'folder_id');      
    return array('title' => $title, 'id' => $id);
  }

  public function getSubFolderName($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT title,folder_id FROM vtiger_documentstorage d JOIN vtiger_documentstorage_subfolders f ON d.folder_id=f.id WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id)); 
    $title = $db->query_result($result,0,'title');   
    $id = $db->query_result($result,0,'folder_id');   
    return array('title' => $title, 'id' => $id);
  }

  public function getFolderType($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT folder_type FROM vtiger_documentstorage WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id)); 
    return  $db->query_result($result,0,'folder_type');
  }

  public function getFolderNameFromSubFolder($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT f.title,f.id FROM vtiger_documentstorage d
            JOIN vtiger_documentstorage_subfolders s ON s.id=d.folder_id
            JOIN vtiger_documentstorage_folders f ON f.id=s.parent
            WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id)); 
    $title = $db->query_result($result,0,'title');   
    $id = $db->query_result($result,0,'id');   
    return array('title' => $title, 'id' => $id);
  }

  public function getFileType($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT file_type FROM vtiger_documentstorage_file_types WHERE id = ?";
    $result = $db->pquery($sql,array($id));    
    return $db->query_result($result,0,'file_type');
  }
}
