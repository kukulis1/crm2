<?php

class Documentstorage_Module_Model extends Vtiger_Module_Model {

  public function isSummaryViewSupported() {
      return false;
  }  

  public function formatBytes($bytes, $precision = 2) { 
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 
    $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow]; 
  } 

  public function getFileType($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT file_type FROM vtiger_documentstorage_file_types WHERE id = ?";
    $result = $db->pquery($sql,array($id));    
    return $db->query_result($result,0,'file_type');
  }

  public function getFileInfo($recordid){
    $db = PearDatabase::getInstance();
    $sql = "SELECT * FROM vtiger_documentstorage WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($recordid));
    return $db->fetch_array($result);
  }

  public function getFolderInfo($recordid,$folder) { 
    $db = PearDatabase::getInstance();   
    $sql = "SELECT f.*
            FROM vtiger_documentstorage_$folder f           
            WHERE f.deleted = 0 AND f.id = ? ORDER BY f.id ASC";
    $result = $db->pquery($sql,array($recordid));
    return $db->fetch_array($result);
  }

  public function getSubFolders($id) { 
    $db = PearDatabase::getInstance();
    global $current_user;

    // NOTE Gaunam nustatymuose nustatytu adminu sarasa kurie gales matyti visus aplankus
    $roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));     

    // NOTE Gaunam visus sub aplankus
    $sql = "SELECT * FROM vtiger_documentstorage_subfolders WHERE deleted = 0 AND parent = ? ";
    $result = $db->pquery($sql,array($id));

    // NOTE Gaunam sarasa roliu kas gali matyti aplanka
    $check = "SELECT roles FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = 2";    

    $subFolders = array();      

    foreach($result AS $row){
      $result2 = $db->pquery($check,array($row['id']));
      $role = $db->query_result($result2,0,'roles');
      $roles = explode(',',$role);

			if($row['type'] == 1){ // Jeigu aplanko tipas 1 - viesas, tada mato visi.
				$subFolders[] = $row;
			}else if($row['smcreatorid'] == $current_user->id){ // Jei aplankas privatus (2) tada ji mato ji sukures asmuo
				$subFolders[] = $row;
			}else if(in_array($current_user->roleid,$admins)){ // Privatu aplanka mato administratoriai priskirti nustatymuose
				$subFolders[] = $row;
			}else if(in_array($current_user->roleid,$roles)){ // Privatu aplanka mato priskirtos roles
				$subFolders[] = $row;      
			}
		}

    return $subFolders;
  }

  public function checkFolderPermision($recordid,$current_user,$folder,$folder_type){
    $db = PearDatabase::getInstance();  

    // NOTE Gaunam nustatymuose nustatytu adminu sarasa kurie gales matyti visus aplankus
    $roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));    

    // NOTE Gaunam sarasa roliu kas gali matyti aplanka
    $check = "SELECT roles FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = ?"; 
    $result2 = $db->pquery($check,array($recordid,$folder_type));
    $role = $db->query_result($result2,0,'roles');
    $roles = explode(',',$role);

    // NOTE Gaunam visus sub aplankus
    $sql = "SELECT * FROM vtiger_documentstorage_$folder WHERE id = ?";
    $result = $db->pquery($sql,array($recordid));
    $type = $db->query_result($result,0,'type');
    $smcreatorid = $db->query_result($result,0,'smcreatorid');

    if($type == 1){
      return true;
    }else if(in_array($current_user->roleid,$admins)){
      return true;
    }else if($smcreatorid == $current_user->id){
      return true;
    }else if(in_array($current_user->roleid,$roles)){
      return true;
    }else{
      return false;
    }
  }

  public function checkFolderEditPermision($recordid,$current_user,$folder){
    $db = PearDatabase::getInstance();  

    // NOTE Gaunam nustatymuose nustatytu adminu sarasa kurie gales matyti visus aplankus
    $roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));   

    // NOTE Gaunam visus aplankus
    $sql = "SELECT * FROM vtiger_documentstorage_$folder WHERE smcreatorid = ? AND id = ?";
    $result = $db->pquery($sql,array($current_user->id,$recordid));

    if(in_array($current_user->roleid,$admins)){
      return true;
    }else if($db->num_rows($result)){
      return true;    
    }else{
      return false;
    }
  }
  public function checkFileEditPermision($recordid,$current_user){
    $db = PearDatabase::getInstance();  

    // NOTE Gaunam nustatymuose nustatytu adminu sarasa kurie gales matyti visus aplankus
    $roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));   

    // NOTE Gaunam visus aplankus
    $sql = "SELECT * FROM vtiger_documentstorage d
            INNER JOIN vtiger_crmentity e ON e.crmid=d.documentstorageid 
            WHERE smcreatorid = ? AND documentstorageid = ?";
    $result = $db->pquery($sql,array($current_user->id,$recordid));

    if(in_array($current_user->roleid,$admins)){
      return true;
    }else if($db->num_rows($result)){
      return true;    
    }else{
      return false;
    }
  }
  

}