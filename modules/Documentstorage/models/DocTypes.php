<?php

class Documentstorage_DocTypes_Model extends Vtiger_Module_Model {

  private $db;

  public function getDocTypes(){
    $db = PearDatabase::getInstance();   
    $sql = "SELECT t.id, t.file_type, t.type, CONCAT(first_name,' ',last_name) AS owner, t.created_at
              FROM vtiger_documentstorage_file_types t             
              JOIN vtiger_users u ON u.id=t.create_person";
    $result = $db->pquery($sql,array());
    if($db->num_rows($result)){   
      return $result;
    }else{
      return false;
    }
  }

  public function getDocType($id){
    $db = PearDatabase::getInstance();   
    $sql = "SELECT * FROM vtiger_documentstorage_file_types  WHERE id = ?";
    $result = $db->pquery($sql,array($id));
    return $db->fetch_array($result);
  }

}