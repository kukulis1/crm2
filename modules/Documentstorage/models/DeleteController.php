<?php

class Documentstorage_DeleteController_Model extends Vtiger_Module_Model {

  private $db;

  // NOTE Trinam aplanka, sub-aplanka ir visus failus juose
  public function processDeleteParentFolder($recordid) { 
    $db = PearDatabase::getInstance(); 
    $root = $_SERVER['DOCUMENT_ROOT'];  
    $sql = "UPDATE vtiger_documentstorage_folders SET deleted = 1 WHERE id = ?";
    $sql2 = "UPDATE vtiger_documentstorage_subfolders SET deleted = 1 WHERE parent = ? AND deleted = 0";
    $sql3 = "SELECT id FROM vtiger_documentstorage_subfolders WHERE parent = ?";

    $db->pquery($sql,array($recordid));

    $subfolder = $db->pquery($sql3,array($recordid));
    $subfolderid = $db->query_result($subfolder,0,'id');

    $sql4 = "SELECT f.documentstorageid, f.path, f.temp_filename, f.extension 
              FROM vtiger_documentstorage f
              INNER JOIN vtiger_crmentity e ON e.crmid=f.documentstorageid 
              WHERE deleted = 0 AND folder_id = ?";   


    $sql5 = "SELECT f.documentstorageid, f.path, f.temp_filename, f.extension 
              FROM vtiger_documentstorage f
              INNER JOIN vtiger_crmentity e ON e.crmid=f.documentstorageid 
              WHERE deleted = 0 AND folder_id = ?";             
    

    $sql6 = "UPDATE vtiger_crmentity SET deleted = 1 WHERE crmid = ?";

    $get_files = $db->pquery($sql4,array($recordid));

    foreach ($get_files as $row) {
      $path = $row['path'];
      $filename = $row['temp_filename'];
      $extension = $row['extension'];
      $db->pquery($sql6,array($row['documentstorageid']));
      unlink("$root/documents_storage/$path/$filename.$extension");
    }

    $get_files2 = $db->pquery($sql5,array($subfolderid));

    foreach ($get_files2 as $subrow) {
      $path = $subrow['path'];
      $filename = $subrow['temp_filename'];
      $extension = $subrow['extension'];
      $db->pquery($sql6,array($subrow['documentstorageid']));
      unlink("$root/documents_storage/$path/$filename.$extension");
    }

    $db->pquery($sql2,array($recordid));
  }
  
  // NOTE Trinam sub-aplanka ir visus failus jame
  public function processDeleteSubFolder($recordid) {
    $db = PearDatabase::getInstance(); 
    $root = $_SERVER['DOCUMENT_ROOT'];      
    $sql = "UPDATE vtiger_documentstorage_subfolders SET deleted = 1 WHERE id = ? AND deleted = 0";    

    $sql2 = "SELECT f.documentstorageid, f.path, f.temp_filename, f.extension 
              FROM vtiger_documentstorage f
              INNER JOIN vtiger_crmentity e ON e.crmid=f.documentstorageid 
              WHERE deleted = 0 AND folder_id = ?";  

    $sql3 = "UPDATE vtiger_crmentity SET deleted = 1 WHERE crmid = ?";          
              
    $get_files = $db->pquery($sql2,array($recordid));

    foreach ($get_files as $row) {
      $path = $row['path'];
      $filename = $row['temp_filename'];
      $extension = $row['extension'];
      $db->pquery($sql3,array($row['documentstorageid']));
      unlink("$root/documents_storage/$path/$filename.$extension");
    }
          
    $db->pquery($sql,array($recordid));

    $sql4 = "SELECT parent FROM vtiger_documentstorage_subfolders WHERE id = ?";
    $parent = $db->pquery($sql4,array($recordid));
    $parentid = $db->query_result($parent,0,'parent');
    return $parentid;
  }

    // NOTE Trinam faila
    public function processDeleteFile($recordid) {
      $db = PearDatabase::getInstance(); 
      $sql = "UPDATE vtiger_crmentity SET deleted = 1 WHERE crmid = ?";   
      $sql2 = "SELECT f.documentstorageid, f.path, f.temp_filename, f.extension 
                FROM vtiger_documentstorage f
                INNER JOIN vtiger_crmentity e ON e.crmid=f.documentstorageid 
                WHERE deleted = 0 AND f.documentstorageid = ?";

      $get_files = $db->pquery($sql2,array($recordid));

      foreach ($get_files as $row) {
            $path = $row['path'];
            $filename = $row['temp_filename'];
            $extension = $row['extension'];            
            unlink("$root/documents_storage/$path/$filename.$extension");
      }

      $db->pquery($sql,array($recordid));
    }

    public function deleteTemplate($recordid){
      $db = PearDatabase::getInstance(); 
      $sql = "DELETE FROM vtiger_documentstorage_templates WHERE id = ?";  
      $db->pquery($sql,array($recordid));
      header("Location:index.php?module=Documentstorage&view=Templates");
    }

    public function deleteDocType($recordid){
      $db = PearDatabase::getInstance(); 
      $sql = "DELETE FROM vtiger_documentstorage_file_types WHERE id = ?";  
      $db->pquery($sql,array($recordid));
      header("Location:index.php?module=Documentstorage&view=DocTypes");
    }

}