<?php
// ini_set('display_errors',1);
// error_reporting(E_ALL);
header('Content-type: text/html; charset=utf-8');
require_once 'vtlib/Vtiger/vendor/autoload.php';
use PhpOffice\PhpWord\TemplateProcessor AS TemplateProcessor;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Settings AS Settings;


class Documentstorage_GenerateDoc_Model extends Vtiger_Module_Model {

  private $db;

  public static function generateEmploymentContract($templateid,$documentid,$record){    
    Settings::setOutputEscapingEnabled(true);
    $db = PearDatabase::getInstance(); 
    $date_now = date('Y-m-d H:i:s');
    global $current_user;

    $sql = "SELECT filename,extension, CONCAT('documents_storage/',path,'/',temp_filename,'.',extension) AS file_location 
            FROM vtiger_documentstorage 
            WHERE documentstorageid = ?";

    $sql2 = "SELECT * FROM vtiger_documentstorage_templates WHERE id = ?";         

    $sql3 = "SELECT tablename FROM vtiger_field WHERE tabid = ? AND displaytype != 6 AND uitype != 10 ORDER BY tabid DESC LIMIT 1";

    $result = $db->pquery($sql, array($documentid));
    $result2 = $db->pquery($sql2, array($templateid));    
    
    $original_file_path = $db->query_result($result,0,'file_location');  
    $filename = $db->query_result($result,0,'filename');    
    $extension = $db->query_result($result,0,'extension'); 

    $classificator = $db->query_result($result2,0,'classificator');     
    $module = $db->query_result($result2,0,'module');     
    $fields = explode(':',$classificator);


    $result3 = $db->pquery($sql3, array($module));
    $tablename = $db->query_result($result3,0,'tablename'); 

    $tablename = 'vtiger_hrm_employee';
    
    if($tablename == 'vtiger_hrm_employee'){
      $subTable = 'vtiger_hrm_employcf';
      $relid = 'employid';
      $name = "CONCAT(firstname,'-',lastname) AS filebeginname";
    }else if($tablename == 'vtiger_account'){
      $subTable = 'vtiger_accountscf';
      $relid = 'accountid';
      $name = "accountname AS filebeginname";
    }else if($tablename == 'vtiger_vendor'){
      $subTable = 'vtiger_vendorcf';
      $relid = 'vendorid';
      $name = "vendorname AS filebeginname";
    }

    $sql4 = "SELECT *,$name
              FROM $tablename f
              LEFT JOIN $subTable s ON s.$relid = f.$relid
              WHERE f.$relid = ?";

    $result4 = $db->pquery($sql4, array($record));

    $moduleInfo = array();
    foreach ($result4 as $row) {
      $moduleInfo = $row;
    }

    $year = date('Y');
    $day = date('d');
    $month = date('m');
    $month_string = Documentstorage_GenerateDoc_Model::translateMonth(date('F'));
    $get_row_no = $db->pquery("SELECT row_no FROM vtiger_documentstorage_row_no WHERE date = ? ORDER BY id DESC LIMIT 1",array(date('Y-m-d')));
    $row_no = $db->query_result($get_row_no,0,'row_no')+1;
    $row_no = ($row_no < 10 ? '0'.$row_no : $row_no);

    $moduleInfo['date_string'] = "$year m. $month_string mėn. $day d.";
    $moduleInfo['contract_no_from_date'] = "$year/$month/$day-$row_no";
    $moduleInfo['today_date'] = date('Y-m-d');
    $moduleInfo['contract_no'] = date('Ymd');
    
    $fileBeginName = $moduleInfo['filebeginname']; 

    $ext = pathinfo($original_file_path, PATHINFO_EXTENSION);
    
    if($ext == 'docx'){
      $year = date("Y");
      $month = date("F");
      $dayofweek = "week".Documentstorage_GenerateDoc_Model::weekOfMonth(strtotime(date("Y-m-d"))); 

      $storage = "documents_storage/$year/$month/$dayofweek"; 
      $insert_path = "$year/$month/$dayofweek"; 
      $tempName = 'tempfile_'.time().'.docx';
      $final_file_name = "$fileBeginName-$filename.$extension";
      $short_filename = "$fileBeginName-$filename";

      $path = $original_file_path;  

    
      $values = array();
      foreach ($fields as $field) {
        $expl = explode('=',$field);
        if($expl[1] == 'contract_no_from_date'){
          $db->pquery("INSERT INTO vtiger_documentstorage_row_no (row_no,date) VALUES (?,?)", array($row_no,date('Y-m-d')));
        }
        // if($expl[1] == 'employment_contract_no'){
        //   $moduleInfo['employment_contract_no'] = Documentstorage_GenerateDoc_Model::getContractNo($db);
        // }  
        if($expl[0] == 'vadovas'){
          $moduleInfo[$expl[1]] = Documentstorage_GenerateDoc_Model::getManagerName($expl[1],$db);         
        } 

        $values[$expl[0]] = html_entity_decode($moduleInfo[$expl[1]], ENT_NOQUOTES, 'UTF-8');        
      }  

      if($templateid == 16){          
        
        $get_inventory = $db->pquery("SELECT date_of_issue, date_of_return, qty, employeeinventory_tks_item, cf_2682 AS unit
        FROM vtiger_employeeinventory_assign
        LEFT JOIN vtiger_employeeinventory ON vtiger_employeeinventory.employeeinventoryid=vtiger_employeeinventory_assign.assigned_item
        LEFT JOIN vtiger_employeeinventorycf ON vtiger_employeeinventorycf.employeeinventoryid=vtiger_employeeinventory.employeeinventoryid
        INNER JOIN vtiger_crmentity ON crmid=vtiger_employeeinventory.employeeinventoryid
        WHERE user_id = ? AND date_of_issue > 0 AND deleted = 0 ", array($record));


        $get_individual_inventory = $db->pquery("SELECT date_of_issue, date_of_return, qty, employeeinventory_tks_item, unit 
        FROM vtiger_employeeinventory_assign
        LEFT JOIN vtiger_employee_individual_inventory ON vtiger_employee_individual_inventory.employeeinventoryid=vtiger_employeeinventory_assign.assigned_item
        WHERE vtiger_employee_individual_inventory.user_id = ? AND date_of_issue > 0", array($record));

        $get_employee_inventory = [];

        foreach ($get_inventory as $value) {
          $get_employee_inventory[] = $value;
        }

        foreach ($get_individual_inventory as $value) {
          $get_employee_inventory[] = $value;
        }
        

        if(count($get_employee_inventory) > 0){

          $table = new Table(['borderSize' => 2, 'borderColor' => '000000', 'cellMargin' => 90,'cellSpacing' => 10, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER]);

          $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
          $cellRowContinue = array('vMerge' => 'continue');
          $cellColSpan = array('gridSpan' => 4, 'valign' => 'center');

          $cellHCentered = array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER);
          $cellVCentered = array('valign' => 'center'); 
          
          $table->addRow();
        
          $table->addCell(700, $cellRowSpan)->addText('Eil. Nr.', ['size' => 16, 'bold' => true], $cellHCentered);
          $table->addCell(3500, $cellRowSpan)->addText('Asmeninės apsaugos priemonės pavadinimas, tipas, markė.', ['size' => 12, 'bold' => true], $cellHCentered);
          $table->addCell(1000, $cellRowSpan)->addText('Mato vnt.', ['size' => 12, 'bold' => true], $cellHCentered);
        
        
          $cell1 = $table->addCell(5000, $cellColSpan);
          $textrun1 = $cell1->addTextRun(['valign' => 'center', 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
          $textrun1->addText('Išduota', $cellHCentered);        
        
        
          $cell2 = $table->addCell(5000, $cellColSpan);
          $textrun2 = $cell2->addTextRun(['valign' => 'center', 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
          $textrun2->addText('Grąžinta', $cellHCentered);   
        
          $table->addCell(1500, $cellRowSpan)->addText('Atsakingo asmens parašas', null, $cellHCentered);
          $table->addRow();
          $table->addCell(700, $cellRowContinue);
          $table->addCell(3500, $cellRowContinue);
          $table->addCell(1000, $cellRowContinue);
          $table->addCell(1500, $cellVCentered)->addText('Data', null, $cellHCentered);
          $table->addCell(900, $cellVCentered)->addText('Kiekis', null, $cellHCentered);
          $table->addCell(1500, $cellVCentered)->addText('Tinkamumo naudoti terminas', null, $cellHCentered);
          $table->addCell(1100, $cellVCentered)->addText('Gavėjo parašas', null, $cellHCentered);
        
        
          $table->addCell(1500, $cellVCentered)->addText('Data', null, $cellHCentered);
          $table->addCell(900, $cellVCentered)->addText('Kiekis', null, $cellHCentered);
          $table->addCell(1500, $cellVCentered)->addText('Tinkamumas naudoti', null, $cellHCentered);
          $table->addCell(1100, $cellVCentered)->addText('Grąžinusio parašas', null, $cellHCentered);
          $table->addCell(1500, $cellRowContinue);

          $no= 1;
          foreach ($get_employee_inventory as $row) {            
            $table->addRow();
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80])->addText("$no.", null, $cellHCentered);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80])->addText($row['employeeinventory_tks_item'], null, null);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80])->addText($row['unit'], null, $cellHCentered);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80])->addText($row['date_of_issue'], null, $cellHCentered);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80])->addText($row['qty'], null, $cellHCentered);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80]);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80]);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80]);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80]);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80]);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80]);
            $table->addCell(null, ['valign' => 'center', 'cellMargin' => 80]);   
            $no++;
          }
        }else{
          echo "<script>alert('Darbuotojas neturi priskirto inventoriaus');
                 window.location.href='/index.php?module=Employee&relatedModule=Documentstorage&view=Detail&record=$record&mode=showRelatedList&relationId=227&tab_label=Dokumentų%20saugykla&app=HRM';
                </script>";
        }
      
      }
      

      $templateProcessor = new TemplateProcessor($path);
      if($templateid == 16){    
        $templateProcessor->setComplexBlock('{table}', $table);
      }

      $templateProcessor->setValues($values);

  
      if (!file_exists($storage)) {
        mkdir($storage, 0777, true);
      }  

      $templateProcessor->saveAs("$storage/$final_file_name");

      $size = filesize("$storage/$final_file_name");

      $sql4 = "INSERT INTO vtiger_documentstorage (documentstorageid, folder_id, folder_type, file_type, filename, path, temp_filename, size, extension) 
      VALUES (?,?,?,?,?,?,?,?,?)";

      $sql5 = "INSERT INTO vtiger_documentstorage_employee_rel (documentstorageid, employid) VALUES (?,?)";

    
      // Documentstorage_GenerateDoc_Model::insertDocument($db,$record,$current_user->id,$storage.'/',$short_filename,$final_file_name);
      $entity_id = Documentstorage_GenerateDoc_Model::get_last_entity($db);
      Documentstorage_GenerateDoc_Model::insert_entity($db,'Documentstorage', $entity_id, $current_user->id, NULL, '', 'CRM', $date_now);

      $db->pquery($sql4, array($entity_id,1,1,0,$short_filename,$insert_path,$short_filename,$size,$extension));
      $db->pquery($sql5, array($entity_id,$record));

      // if($ext == 'doc'){
      //   unlink($path);
      // }


      header("Location:index.php?module=Employee&relatedModule=Documentstorage&view=Detail&record=$record&mode=showRelatedList&relationId=227&tab_label=Dokumentų%20saugykla");

    }else{
      throw new Exception('Netinkamas dokumento formatas, leidžiami tik docx failai', 1);
    }

  }

  static function getManagerName($id,$db){    
    $sql = "SELECT CONCAT(firstname,' ',lastname) AS manager FROM vtiger_hrm_employee WHERE employid = ?";
    $result = $db->pquery($sql,array($id));
    return  $db->query_result($result,0,'manager');
  }

  public function getContractNo($db){
    $sql = "SELECT number FROM vtiger_documentstorage_employment_contract_no";
    $sql2 = "UPDATE vtiger_documentstorage_employment_contract_no SET number = ?";

    $result = $db->pquery($sql,array());
    $number = $db->query_result($result,0,'number');
    $db->pquery($sql2,array($number+1));

    return $number;
  }

  static function translateMonth($month){
    $month_locale = array('January' => 'sausio', 'February' => 'vasario', 'arch' => 'kovo', 'April' => 'balandžio', 'May' => 'gegužės', 'June' => 'birželio', 'July' => 'liepos', 'August' => 'rugpjūčio', 'September' => 'rugsėjo', 'October' => 'spalio', 'November' => 'lapkričio', 'December' => 'gruodžio');

    return str_replace($month, $month_locale[$month],$month);
  }

  public static function get_last_entity($db){  
    $db->pquery('LOCK TABLES vtiger_crmentity_seq READ');
    $row = $db->pquery('SELECT * FROM vtiger_crmentity_seq',array());
    $db->pquery('UNLOCK TABLES');

    $crmoldid = $db->query_result($row,0,'id');
    $crmid = $db->query_result($row,0,'id') + 1;
    
    $db->pquery('LOCK TABLES vtiger_crmentity_seq WRITE');
    $db->pquery("UPDATE vtiger_crmentity_seq SET id = ?",array($crmid));

    $crmnewid = $db->pquery('SELECT id FROM vtiger_crmentity_seq');
    $id = $db->query_result($crmnewid,0,'id');
    $db->pquery('UNLOCK TABLES');
    
    if($crmoldid < $id){
      return $crmid;
    }
  }

  public static function insert_entity($db, $setype, $entity_id, $user_id, $description, $label,$source, $date){
    $db->pquery("INSERT INTO vtiger_crmentity (crmid, smcreatorid, smownerid, setype, description, label, source, createdtime, modifiedtime) VALUES (?,?,?,?,?,?,?,?,?)",array($entity_id, $user_id, $user_id, $setype, $description, $label,$source,$date, $date));
  }


  public static function weekOfMonth($date){  
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
  }

  // public static function insertDocument($db,$recordid,$userid,$patch,$filename,$filefullname){
  //     $date_now = date('Y-m-d H:i:s');
  //     $entity_id = Documentstorage_GenerateDoc_Model::get_last_entity($db);
  //     Documentstorage_GenerateDoc_Model::insert_entity($db,'Documents', $entity_id, $userid, NULL, '', 'CRM', $date_now);
  //     Documentstorage_GenerateDoc_Model::insert_senotesrel($db, $entity_id, $recordid);
  //     Documentstorage_GenerateDoc_Model::insertNotes($db,$entity_id,$filename,$filefullname,1,'I',1);    
  //     Documentstorage_GenerateDoc_Model::insert_NoteCf($db, $entity_id, $date_now);
      
  //     $entity_id2 = Documentstorage_GenerateDoc_Model::get_last_entity($db);  
  //     if(Documentstorage_GenerateDoc_Model::renameFile($patch,$filefullname,$entity_id2)){
  //       Documentstorage_GenerateDoc_Model::insert_entity($db,'Documents Attachment', $entity_id2, $userid, NULL, '', 'CRM', $date_now);
  //       Documentstorage_GenerateDoc_Model::insertAttachments($db, $entity_id2,$filefullname,$patch,$recordid);
  //       Documentstorage_GenerateDoc_Model::insertSeAttachmentsrel($db, $entity_id,$entity_id2);	
  //     }  
  // }

  // public static function insert_senotesrel($db, $entity_id, $moduleId){
  //   $db->pquery("INSERT INTO vtiger_senotesrel (crmid, notesid) VALUES (?,?)", array($moduleId,$entity_id));
  // }

  // public static function insertNotes($db, $entity_id,$title,$filename,$folderid,$locationType,$filestatus){
  //   $db->pquery("INSERT INTO vtiger_notes (notesid, title, filename, folderid, filelocationtype, filestatus) VALUES (?,?,?,?,?,?)",array($entity_id, $title, $filename, $folderid,$locationType,$filestatus));
  // }

  // public static function insert_NoteCf($db, $entity_id,$file_created){
  //   $db->pquery("INSERT INTO vtiger_notescf (notesid,cf_1852) VALUES (?,?)",array($entity_id,$file_created));
  // }


  // public static function renameFile($location,$filename,$entity_id){ 
  //   $oldName = $location.$filename;
  //   $newName = $location.$entity_id.'_'.$filename;
  //   if(rename("$oldName","$newName")){
  //     return true;
  //   }
  // }

  // public static function insertAttachments($db, $entity_id,$title,$filename,$recordid){
  //   $db->pquery("INSERT INTO vtiger_attachments (attachmentsid, name, path,related_id)  VALUES (?,?,?,?)",array($entity_id, $title, $filename,$recordid));
  // }

  // public static function insertSeAttachmentsrel($db, $entity_id,$moduleId){
  //   $db->pquery("INSERT INTO vtiger_seattachmentsrel (crmid, attachmentsid)  VALUES (?,?)",array($entity_id, $moduleId));
  // }

}