<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Documentstorage_Record_Model extends Vtiger_Record_Model {

  function getFolderName($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT title,folder_id FROM vtiger_documentstorage d JOIN vtiger_documentstorage_folders f ON d.folder_id=f.id WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id)); 
    $title = $db->query_result($result,0,'title');   
    $id = $db->query_result($result,0,'folder_id');      
    return array('title' => $title, 'id' => $id);
  }

  function getSubFolderName($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT title,folder_id FROM vtiger_documentstorage d JOIN vtiger_documentstorage_subfolders f ON d.folder_id=f.id WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id)); 
    $title = $db->query_result($result,0,'title');   
    $id = $db->query_result($result,0,'folder_id');   
    return array('title' => $title, 'id' => $id);
  }

  public function getFolderType($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT folder_type FROM vtiger_documentstorage WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id));    
    return $db->query_result($result,0,'folder_type');
  }

  public function getFolderNameFromSubFolder($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT f.title,f.id FROM vtiger_documentstorage d
            JOIN vtiger_documentstorage_subfolders s ON s.id=d.folder_id
            JOIN vtiger_documentstorage_folders f ON f.id=s.parent
            WHERE documentstorageid = ?";
    $result = $db->pquery($sql,array($id)); 
    $title = $db->query_result($result,0,'title');   
    $id = $db->query_result($result,0,'id');   
    return array('title' => $title, 'id' => $id);
  }

  public function getFileType($id){
    $db = PearDatabase::getInstance();
    $sql = "SELECT file_type FROM vtiger_documentstorage_file_types WHERE id = ?";
    $result = $db->pquery($sql,array($id));    
    return $db->query_result($result,0,'file_type');
  }

  public function getEmployeFiles($id){
    $db = PearDatabase::getInstance(); 
    global $current_user;

    // NOTE Gaunam nustatymuose nustatytu adminu sarasa kurie gales matyti visus aplankus
    $roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));

    // NOTE Gaunam sarasa roliu kas gali matyti failus
		$check = "SELECT roles FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = ?"; 

    $sql = "SELECT d.*, CONCAT(`first_name`,' ',`last_name`) AS owner ,e.createdtime,e.smcreatorid ,f.type,folder_id,folder_type
                     FROM vtiger_documentstorage d
                     JOIN vtiger_documentstorage_folders f ON f.id=d.folder_id
                     JOIN vtiger_documentstorage_employee_rel r ON r.documentstorageid=d.documentstorageid
                     INNER JOIN vtiger_crmentity e ON e.crmid=d.documentstorageid
                     LEFT JOIN vtiger_users u ON u.id=e.smcreatorid
                     WHERE e.deleted = 0 AND employid = ?";
                     
    $result = $result = $db->pquery($sql,array($id)); 

    $file_list = array();

    foreach ($result as $value) { 

      $result2 = $db->pquery($check,array($value['folder_id'],$value['folder_type']));
			$role = $db->query_result($result2,0,'roles');
			$roles = explode(',',$role);

      if($value['type'] == 1){
				$file_list[] = $value;
			}else if($value['smcreatorid'] == $current_user->id){
				$file_list[] = $value;
			}else if(in_array($current_user->roleid,$admins)){
				$file_list[] = $value;
			}else if(in_array($current_user->roleid,$roles)){
				$file_list[] = $value;
			}
    }

    return $file_list; 
  }

  public function getTemplates(){
    $db = PearDatabase::getInstance(); 
    $sql = "SELECT * FROM vtiger_documentstorage_templates";
    return $db->pquery($sql,array());
  }

  public function getEmploymentContractNo(){
    $db = PearDatabase::getInstance(); 
    $sql = "SELECT number FROM vtiger_documentstorage_employment_contract_no";
    $result = $db->pquery($sql,array());
    return $db->query_result($result,0,'number');
  }

}
