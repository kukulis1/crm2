<?php

class Documentstorage_Catalog_Model extends Vtiger_Module_Model {

  private $db;

  // NOTE Gaunam aplanko informacija
  public function getFolderInfo($recordid) { 
    $db = PearDatabase::getInstance();   
    $sql = "SELECT f.*, CONCAT(`first_name`,' ',`last_name`) AS owner 
            FROM vtiger_documentstorage_folders f
            LEFT JOIN vtiger_users u ON u.id=f.smcreatorid
            WHERE f.deleted = 0 AND f.id = ? ORDER BY f.id ASC";
    $result = $db->pquery($sql,array($recordid));
    return $db->fetch_array($result);
  }

  // NOTE Gaunam sub folderiu informacija
  public function getSubFolderInfo($recordid) { 
    $db = PearDatabase::getInstance();  
    global $current_user;
    
    // NOTE Gaunam nustatymuose nustatytu adminu sarasa kurie gales matyti visus aplankus
    $roles_sql = "SELECT roles FROM vtiger_documentstorage_permisions";
		$role_result = $db->pquery($roles_sql, array());
		$admins = explode(',',$db->query_result($role_result,0,'roles'));  

    // NOTE Gaunam visus sub aplankus
    $sql = "SELECT * FROM vtiger_documentstorage_subfolders WHERE parent = ? AND deleted = 0";
    $result = $db->pquery($sql,array($recordid));

    // NOTE Gaunam sarasa roliu kas gali matyti aplanka
    $check = "SELECT roles FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = 2";


    $subFolders = array();   

    foreach($result AS $row){
      $result2 = $db->pquery($check,array($row['id']));
      $role = $db->query_result($result2,0,'roles');
      $roles = explode(',',$role);

			if($row['type'] == 1){
				$subFolders[] = $row;
			}else if($row['smcreatorid'] == $current_user->id){
				$subFolders[] = $row;
			}else if(in_array($current_user->roleid,$admins)){
				$subFolders[] = $row;
			}else if(in_array($current_user->roleid,$roles)){
				$subFolders[] = $row;
			}
		} 

    return $subFolders;
  }

  // NOTE gaunam aplanko failus
  public function getFolderFiles($recordid){
    $PER_PAGE = 20;
    $db = PearDatabase::getInstance(); 
    $sql = "SELECT d.*, CONCAT(`first_name`,' ',`last_name`) AS owner ,e.createdtime,e.modifiedtime
            FROM vtiger_documentstorage d
            INNER JOIN vtiger_crmentity e ON e.crmid=d.documentstorageid
            LEFT JOIN vtiger_users u ON u.id=e.smcreatorid
            WHERE e.deleted = 0 AND folder_id = ? AND folder_type = 1
            ORDER BY documentstorageid DESC ";
    $sql2 .= " LIMIT $PER_PAGE";
    $result = $db->pquery($sql.$sql2,array($recordid));
    $result2 = $db->pquery($sql,array($recordid));
    $num_rows = $db->num_rows($result);
    $num_rows_all = $db->num_rows($result2);
    return array('result' => $result, 'num_rows' => $num_rows,'total_records' => $num_rows_all,'per_page' => $PER_PAGE);
  }

  // NOTE Gaunam aplankui priskirtu roliu sarasa ir visas sukurtas roles
  public function getFolderRoleList($recordid,$folder){
    $db = PearDatabase::getInstance();  
    $sql = "SELECT * FROM vtiger_documentstorage_folder_permisions WHERE folder_id = ? AND folder_type = ?";
    $sql2 = "SELECT roleid,rolename FROM vtiger_role WHERE roleid NOT IN ('H1','H2')";
    $result = $db->pquery($sql,array($recordid,$folder));
    $result2 = $db->pquery($sql2,array());
    $roles = $db->query_result($result,0,'roles');
    return array('role' => $roles, 'list' => $result2);
  }

}