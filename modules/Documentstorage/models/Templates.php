<?php

class Documentstorage_Templates_Model extends Vtiger_Module_Model {

  private $db;

  public function getTemplates(){
    $db = PearDatabase::getInstance();   
    $sql = "SELECT t.id, m.module,  t.title, CONCAT(first_name,' ',last_name) AS owner, t.created_at
              FROM vtiger_documentstorage_templates t
              JOIN vtiger_documentstorage_template_modules m ON m.tabid=t.module 
              JOIN vtiger_users u ON u.id=t.created_person";
    $result = $db->pquery($sql,array());
    if(!$db->num_rows($result)){
      return false;
    }else{
      return $result;
    }
  }

  static function getSpecificTemplate($id){
    $db = PearDatabase::getInstance();   
    $sql = "SELECT t.id,t.title,t.position,t.module,t.classificator, m.module AS module_name 
                                             FROM vtiger_documentstorage_templates t
                                             JOIN vtiger_documentstorage_template_modules m ON m.tabid=t.module 
                                             WHERE t.id = ?";
    $result = $db->pquery($sql,array($id));   
    return $db->fetch_array($result);
  }

  static function getModuleFields($id){
    $db = PearDatabase::getInstance();   
    $fields = array();
    $query = "SELECT n.tablename,n.tabid FROM vtiger_entityname n
                                              JOIN vtiger_documentstorage_templates t ON t.module=n.tabid
                                              WHERE t.id = ?";
    $exe_query = $db->pquery($query,array($id));                                              
    $table = $db->query_result($exe_query,0,'tablename');
    $moduleid = $db->query_result($exe_query,0,'tabid');

    if($table == 'vtiger_hrm_employee'){
      $subTable = 'vtiger_hrm_employcf';
    }else if($table == 'vtiger_account'){
      $subTable = 'vtiger_accountscf';
    }else if($table == 'vtiger_vendor'){
      $subTable = 'vtiger_vendorcf';
    }

    $query2 = "SELECT columnname,fieldlabel FROM vtiger_field WHERE tabid = ? AND (tablename = ? OR tablename = ?) AND displaytype != 6 AND uitype != 10";

    $exe_query2 = $db->pquery($query2,array($moduleid,$table,$subTable));

    $fields[] = array('columnname' => 'today_date', 'fieldlabel' => 'Šiandienos data');
    $fields[] = array('columnname' => 'date_string', 'fieldlabel' => 'Šiandienos data, mėnesis žodžiu');
    $fields[] = array('columnname' => 'employment_contract_no', 'fieldlabel' => 'Darbo sutarties nr.');
    $fields[] = array('columnname' => 'contract_no', 'fieldlabel' => 'Sutarties nr. iš datos');
    $fields[] = array('columnname' => 'contract_no_from_date', 'fieldlabel' => 'Sutarties nr. iš datos per slash');
    
    foreach ($exe_query2 as $row) {
      $fields[] = $row;
    }

    return $fields;
  }

  static function getManagers(){
    $db = PearDatabase::getInstance();
    $sql = "SELECT e.employid,firstname,lastname
            FROM vtiger_hrm_employee e
            JOIN vtiger_hrm_employcf c ON c.employid=e.employid
            JOIN vtiger_hrm_position p ON p.id=e.position
            INNER JOIN vtiger_crmentity en ON en.crmid=e.employid
            WHERE deleted = 0 AND cf_2370 = 'Dirba' AND name LIKE '%vadovas%'";

    $result = $db->pquery($sql,array());

    $managers = array();

    foreach ($result as $row) {
      $managers[] = $row;
    }

    return $managers;
  }
  
  static function getPositions() { 
    $db = PearDatabase::getInstance();   
    $sql = "SELECT * FROM vtiger_hrm_position INNER JOIN vtiger_crmentity e ON e.crmid=id WHERE deleted = 0 ORDER BY name";
    $result = $db->pquery($sql,array());
    return $result;
  }

  static function getTemplateModules(){
    $db = PearDatabase::getInstance();
    $sql = "SELECT * FROM vtiger_documentstorage_template_modules";
    $result = $db->pquery($sql,array());
    return $result;
  }

}