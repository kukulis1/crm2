<?php

class Documentstorage_Settings_Model extends Vtiger_Module_Model {

  private $db;

  public function getRoleList() { 
    $db = PearDatabase::getInstance();   
    $sql = "SELECT * FROM vtiger_documentstorage_permisions";
    $sql2 = "SELECT roleid,rolename FROM vtiger_role WHERE roleid != 'H1'";
    $result = $db->pquery($sql,array());
    $result2 = $db->pquery($sql2,array());
    $roles = $db->query_result($result,0,'roles');
    return array('role' => $roles, 'list' => $result2);
  }

}