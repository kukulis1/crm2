<?php

include "modules/Saskaitos/models/ordersModel.php";

use modules\Saskaitos\models\ordersModel as ordersModel;

class Saskaitos_List_View extends Vtiger_Index_View {

	function __construct(){
		$this->ordersModel = new ordersModel;  
	}

	public function process(Vtiger_Request $request) {
		global $current_user; 
		global $site_URL;
		global $BOSES;     
		
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		
		$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1; 
		$perPage = 50;   
		$startAt = $perPage * ($page - 1);
		$orders = $this->ordersModel->getOrders($perPage,$startAt);
		$total = $this->ordersModel->getAllOrders();  
		$today_writed_invoices = $this->ordersModel->todayWritedInvoices();
		// $invoices_plan = $this->ordersModel->invoicesPlan();
		
		$url = $site_URL."/index.php?module=Saskaitos&view=List&app=SALES";
		$pagination = $this->get_pagination_links($page, ceil($total/$perPage), $url); 
		$viewer = $this->getViewer($request); 
		$viewer->assign('MODULE_MODEL', $moduleModel);                   
		$viewer->assign('orders', $orders);
		$viewer->assign('BOSE', $BOSES);
		$viewer->assign('ROLE', $current_user->roleid);
		$viewer->assign('all_records', $total);
		$viewer->assign('today_writed_invoices', $today_writed_invoices);
		// $viewer->assign('invoices_plan', $invoices_plan);
		$viewer->assign('pagination', $pagination);
		$viewer->view('List.tpl', $request->getModule()); 
    }
       
	public function get_pagination_links($current_page, $total_pages, $url){
		$links = "";
		$links .= '<div class="pagination">';    
			if ($total_pages >= 1 && $current_page <= $total_pages) {
				$links .= "<a ".($current_page == 1 ? 'class=active' : '')." href=\"{$url}&page=1\">1</a>";
				$i = max(2, $current_page - 5);
				if ($i > 2){
					$links .= "<div>...</div>";
				}
				for (; $i < min($current_page + 6, $total_pages); $i++) {
					$links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$i}\">{$i}</a>";
				}
				if ($i != $total_pages){
					$links .= "<div>...</div>";
					$links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$total_pages}\">{$total_pages}</a>";
				}
			}
		$links .= '</div>';
		return $links;
	}


}
