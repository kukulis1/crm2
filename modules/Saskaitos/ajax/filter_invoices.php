<?php

require_once "../../../config.inc.php";
require_once(__DIR__.'/../../../Helpers/Helper.php');


if($_POST){
    $db = new PDO("mysql:host={$dbconfig['db_server']};dbname={$dbconfig['db_name']}", $dbconfig['db_username'], $dbconfig['db_password'], [
		PDO::ATTR_PERSISTENT => true,
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ]);

    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    $db->exec("SET NAMES UTF8"); 


    $from = trim($_POST['from']);
    $to = trim($_POST['to']);
    $filterBy = trim($_POST['filterBy']);

    $orderDate = trim($_POST['orderDate']);
    $orderNumber = trim($_POST['orderNumber']);
    $customer = trim($_POST['customer']);
    $pricebook = trim($_POST['pricebook']);
    $type = trim($_POST['type']);
    $loadDate = trim($_POST['loadDate']);
    $loadAddress = trim($_POST['loadAddress']);
    $unloadDate = trim($_POST['unloadDate']);
    $unloadAddress = trim($_POST['unloadAddress']);
    $status = trim($_POST['status']);
    $manager = trim($_POST['manager']);

  	if(!empty($from) || !empty($to) || !empty($orderDate) || !empty($orderNumber) ||!empty($customer) ||!empty($pricebook) ||!empty($loadDate) ||!empty($loadAddress) ||!empty($unloadDate) ||!empty($unloadAddress) ||!empty($status) ||!empty($manager) ||!empty($type)){

		$query = "SELECT vtiger_salesorder.salesorder_no, vtiger_salesorder.salesorderid, vtiger_salesorder.load_date_from, vtiger_salesorder.unload_date_from, vtiger_salesorder.sostatus, vtiger_salesorder.shipment_code, vtiger_salesorder.accountid, vtiger_soshipads.ship_city, vtiger_soshipads.ship_code, vtiger_soshipads.ship_street, vtiger_sobillads.bill_city, vtiger_sobillads.bill_code, vtiger_sobillads.bill_street, vtiger_inventoryproductrel.margin, vtiger_accountscf.cf_1279, vtiger_accountscf.cf_1281,

		IF(cf_1376 > 0,cf_1376, REPLACE(ROUND(vtiger_salesorder.total,2),'.',',')) as totalprice, 

		DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') AS createdtime,

		SUM(FORMAT(vtiger_inventoryproductrel.quantity,0)) as quantity,	SUM(FORMAT(vtiger_inventoryproductrel.revised_quantity,0)) as revised_quantity,

		SUM(vtiger_inventoryproductrel.ordered_weight) as ordered_weight, SUM(vtiger_inventoryproductrel.revised_weight) as revised_weight, 
		
		ROUND(SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity),2)  as ordered_volume,

		ROUND(SUM(m3),2)  as revised_volume,    

		ROUND(
			CASE WHEN app_taxable_dimensions.type = 'revised' 
				 THEN SUM(revised_quantity) 
				 ELSE SUM(quantity) 
			END,2) AS taxable_quantity,
			
		ROUND(
			CASE WHEN app_taxable_dimensions.type = 'revised' 
			  	 THEN SUM(revised_weight) 
				 ELSE SUM(ordered_weight)
			END,2) AS taxable_weight,

		ROUND(
			CASE WHEN app_taxable_dimensions.type = 'revised' 
				 THEN SUM(m3) 
				 ELSE SUM((ordered_length * ordered_width * ordered_height) * quantity) 
			END,2)  AS taxable_volume,

		CASE WHEN app_taxable_dimensions.type = 'fail' OR app_taxable_dimensions.type IS NULL 
			 THEN 0 
			 ELSE 1 
		END AS show_taxable,

		CASE WHEN vtiger_salesordercf.cf_855 = 'Transporto užsakymas' 
			 THEN 'T' 
			 ELSE 'P'
		END AS type,

		SUM(DISTINCT vtiger_inventoryproductrel.cargo_wgt) as cargo_wgt, SUM(DISTINCT vtiger_inventoryproductrel.cargo_length) as cargo_length,SUM(DISTINCT vtiger_inventoryproductrel.cargo_width) as cargo_width, 

		SUM(DISTINCT vtiger_inventoryproductrel.cargo_height) as cargo_height, vtiger_account.accountname,vtiger_users.user_name,vtiger_users.first_name,vtiger_users.last_name, (vtiger_inventoryproductrel.cargo_length * vtiger_inventoryproductrel.cargo_height * vtiger_inventoryproductrel.cargo_width) as cargo_volume, 

		(SELECT CONCAT(`city`,'/ ',IF(`city` != `zone_customer`, CONCAT(`zone_customer`,'/'),''),' ', `state`, '/ ', `zone_base`) FROM crm_post_codes WHERE post_code = vtiger_sobillads.bill_code LIMIT 1) AS `location_from`,

		(SELECT CONCAT(`city`,'/ ',IF(`city` != `zone_customer`, CONCAT(`zone_customer`,'/'),''),' ', `state`, '/ ', `zone_base`) FROM crm_post_codes WHERE post_code = vtiger_soshipads.ship_code LIMIT 1) AS `location_to`,

		SUM(DISTINCT (vtiger_inventoryproductrel.cargo_length * vtiger_inventoryproductrel.cargo_width * vtiger_inventoryproductrel.cargo_height) * vtiger_inventoryproductrel.quantity ) AS volume, cf_2718 AS pricebook_details,

		vtiger_salesordercf.cf_1297 as pricebook, vtiger_salesordercf.cf_1376 as agreed_price, SUM(vtiger_inventoryproductrel.meters) AS meters

		FROM vtiger_salesorder
		LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
		LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid
		LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
		LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
		LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid                                    
		LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid                                    
		LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid
		LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid     
		LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid  
		LEFT JOIN `app_taxable_dimensions` ON app_taxable_dimensions.salesorderid=vtiger_salesorder.salesorderid                
		WHERE vtiger_salesordercf.cf_1468 = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_crmentity.deleted = 0  AND  vtiger_inventoryproductrel.productid != 36641 AND vtiger_salesordercf.cf_1456 = 1 AND cf_1614 = 0 AND vtiger_invoice_salesorders_list.salesorderid IS NULL ";
					
		$query .= getWhere($filterBy, $from, $to, $orderDate, $orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$type);			
		$query .= " GROUP BY vtiger_salesorder.salesorderid";                                       
		$query .= " ORDER BY vtiger_salesorder.external_order_id ASC LIMIT 500";  
         

		$order_dimensions = $db->query($query, PDO::FETCH_ASSOC)->fetchAll();
		if(!$order_dimensions){
			echo json_encode(['rows' => 'no_results']);
			return;
		}

		$orders = [];
		$salesorderids = [];
		$dates = [];

		foreach($order_dimensions ?? [] AS $row){
			$orders[] = $row; 
			$dates[$row['createdtime']] = $row['createdtime']; 	
			$salesorderids[$row['salesorderid']] = $row['salesorderid'];		
		}

		// $plan = \Helpers\InvoicePlanHelper::getInvoicesPlan($db, $dates, $salesorderids);	
		
	
		if(count($orders) > 0){
			echo json_encode(['rows' => $orders, 'salesorderids' => array_values($salesorderids), 'dates' => array_values($dates)]);
		}else{
			echo json_encode(['rows' => 'no_results']);
		}
  
	}else{
		echo json_encode(['rows' => 'no_results']);
	}

}else{
  http_response_code(404);
}

function getWhere($filterBy, $from, $to, $orderDate, $orderNumber,$customer,$pricebook,$loadDate,$loadAddress,$unloadDate,$unloadAddress,$status,$manager,$type){
	$query = '';
	if(!empty($status)){
		$query .= "AND  vtiger_salesorder.sostatus LIKE '$status%' ";
	}			

	if(!empty($from) && !empty($to)){
		if($filterBy == 'order_date'){
			$query .= " AND DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$from' AND '$to' ";
		}elseif($filterBy == 'load_date'){
			$query .= " AND vtiger_salesorder.load_date_from BETWEEN '$from' AND '$to' ";
		}elseif($filterBy == 'unload_date'){
			$query .= " AND vtiger_salesorder.unload_date_from BETWEEN '$from' AND '$to' ";
		}
	}
	if(!empty($orderDate)){
		$query .= " AND  DATE_FORMAT(vtiger_salesordercf.cf_1504, '%Y-%m-%d') =  '$orderDate' ";
	}                                         
	if(!empty($orderNumber)){
		$query .= " AND  vtiger_salesorder.shipment_code LIKE '%$orderNumber%' ";
	}
	if(!empty($customer)){
		$query .= " AND  vtiger_account.accountname LIKE '$customer%' ";
	}
	if(!empty($pricebook)){
		$query .= " AND  vtiger_salesordercf.cf_1297 LIKE '$pricebook%' ";
	}
	if(!empty($type)){
		if($type == 'T') $type = 'Transporto užsakymas'; elseif($type == 'P') $type = 'Perkraustymo užsakymas';
		$query .= " AND  vtiger_salesordercf.cf_855 LIKE '$type%' ";
	}
	if(!empty($loadDate)){
		$query .= " AND  vtiger_salesorder.load_date_from = '$loadDate' ";
	}
	if(!empty($loadAddress)){
		$query .= " AND CONCAT(vtiger_sobillads.bill_street,' ', vtiger_sobillads.bill_city,' ',vtiger_sobillads.bill_code) LIKE '%$loadAddress%' ";
	}
	if(!empty($unloadDate)){
		$query .= " AND  vtiger_salesorder.unload_date_from = '$unloadDate' ";
	}
	if(!empty($unloadAddress)){  
		$query .= " AND CONCAT(vtiger_soshipads.ship_street,' ', vtiger_soshipads.ship_city,' ',vtiger_soshipads.ship_code) LIKE '%$unloadAddress%' ";
	}
	if(!empty($manager)){
		$query .= " AND  vtiger_users.user_name LIKE '$manager%' OR vtiger_users.first_name LIKE'$manager%' OR vtiger_users.last_name  LIKE '$manager%' OR CONCAT(vtiger_users.first_name,vtiger_users.last_name) LIKE '%$manager%'";
	}   
	
	return $query;
}