<?php
namespace modules\Saskaitos\ajax;

require_once "Fill_Fields.php";

if(isset( $_POST['orderId'] )) {
     $orderid = $_POST['orderId'];
     $filler = new Fill_Fields;
     $result = $filler->getValues();
     echo $result;
}