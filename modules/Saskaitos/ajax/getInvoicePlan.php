<?php

require_once "../../../config.inc.php";
require_once(__DIR__.'/../../../Helpers/Helper.php');

if($_POST){

	$db = new PDO("mysql:host={$dbconfig['db_server']};dbname={$dbconfig['db_name']}", $dbconfig['db_username'], $dbconfig['db_password'], [
	PDO::ATTR_PERSISTENT => true,
	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	]);


	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	$db->exec("SET NAMES UTF8"); 

	$dates = $_POST['dates'];
	$salesorderids = $_POST['salesorderids'];

	echo \Helpers\InvoicePlanHelper::getInvoicesPlan($db, $dates, $salesorderids);

}



