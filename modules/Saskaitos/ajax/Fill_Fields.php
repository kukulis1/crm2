<?php 

namespace modules\Saskaitos\ajax;

error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once  "../models/ordersModel.php";

use modules\Saskaitos\models\ordersModel as ordersModel;

class Fill_Fields {

     public function __construct(){
        $this->ordersModel = new ordersModel;
     }

      public  function getValues(){
        $orders = $this->ordersModel->getOrdersById(); 
        return $orders;


      }
}