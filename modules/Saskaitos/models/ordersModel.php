<?php
namespace modules\Saskaitos\models;

class ordersModel {

	public function __construct(){
		global $adb;
		$this->db = $adb;
	}


	public function getOrders($perPage,$startAt){
	
		$sql = "SELECT vtiger_salesorder.salesorder_no,vtiger_salesorder.preinvoice,IF(cf_1376 > 0,cf_1376, REPLACE(ROUND(vtiger_salesorder.total,2),'.',',')) as totalprice, vtiger_salesorder.salesorderid,vtiger_salesorder.load_date_from ,vtiger_salesorder.unload_date_to, vtiger_salesorder.sostatus, vtiger_salesorder.shipment_code,vtiger_salesorder.accountid,
		vtiger_crmentity.createdtime,vtiger_soshipads.ship_city ,vtiger_soshipads.ship_code ,vtiger_soshipads.ship_street, vtiger_sobillads.bill_city ,vtiger_sobillads.bill_code ,vtiger_sobillads.bill_street, vtiger_inventoryproductrel.margin , vtiger_accountscf.cf_1279, vtiger_accountscf.cf_1281, SUM(FORMAT(vtiger_inventoryproductrel.quantity,0)) as quantity,

		SUM(vtiger_inventoryproductrel.ordered_weight) as ordered_weight, 
		SUM(vtiger_inventoryproductrel.revised_weight) as revised_weight, 
		SUM((vtiger_inventoryproductrel.ordered_length * vtiger_inventoryproductrel.ordered_width * vtiger_inventoryproductrel.ordered_height) * quantity)  as ordered_volume,
		SUM((vtiger_inventoryproductrel.revised_length * vtiger_inventoryproductrel.revised_width * vtiger_inventoryproductrel.revised_height) * quantity)  as revised_volume,    
		
		ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(revised_weight) ELSE SUM(ordered_weight) END,2) AS taxable_weight,
		ROUND(CASE WHEN app_taxable_dimensions.type = 'revised' THEN SUM(m3) ELSE SUM((ordered_length * ordered_width * ordered_height) * quantity) END,2)  AS taxable_volume,
		CASE WHEN app_taxable_dimensions.type = 'fail' OR app_taxable_dimensions.type IS NULL THEN 0 ELSE 1 END AS show_taxable,
		CASE WHEN vtiger_salesordercf.cf_855 = 'Transporto užsakymas' THEN 'T' ELSE 'P' END AS type,

	

			SUM(DISTINCT vtiger_inventoryproductrel.cargo_wgt * vtiger_inventoryproductrel.quantity) as cargo_wgt, 
			SUM(DISTINCT vtiger_inventoryproductrel.cargo_length) as cargo_length, 
			SUM(DISTINCT vtiger_inventoryproductrel.cargo_width) as cargo_width , 
			SUM(DISTINCT vtiger_inventoryproductrel.cargo_height) as cargo_height,
			SUM(DISTINCT vtiger_inventoryproductrel.cargo_length * vtiger_inventoryproductrel.cargo_width * vtiger_inventoryproductrel.cargo_height) AS volume,
		vtiger_account.accountname,vtiger_users.user_name,vtiger_users.first_name,vtiger_users.last_name,vtiger_salesordercf.cf_1297 as pricebook, vtiger_salesordercf.cf_1376 as agreed_price, cf_2718 AS pricebook_details
											FROM vtiger_salesorder
											LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
											LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid
											LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
											LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
											LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid
											LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid                                        
											LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid
											LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid                
											LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid     
											LEFT JOIN `app_taxable_dimensions` ON app_taxable_dimensions.salesorderid=vtiger_salesorder.salesorderid                        
											WHERE vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_crmentity.deleted = 0 AND vtiger_salesordercf.cf_1468 = 0 AND  
											vtiger_salesordercf.cf_1456  = 1 ";
											//  $sql .=  " CASE WHEN vtiger_salesordercf.cf_1456 THEN
											//     CASE 
											//       WHEN  vtiger_accountscf.cf_1281 = 'Kas savaitę' THEN YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW()) AND WEEK(vtiger_crmentity.createdtime, 1) < WEEK(NOW(), 1) 
											//       WHEN  vtiger_accountscf.cf_1281 = 'Du kartus per mėn.'                                             
											//         THEN (MONTH(vtiger_crmentity.createdtime) = MONTH(NOW()) 
											//         AND (DAYOFMONTH(NOW()) >= 15) AND DAYOFMONTH(vtiger_crmentity.createdtime) < 15) 
											//         OR  (DAYOFMONTH(NOW()) = date('t') AND DAYOFMONTH(vtiger_crmentity.createdtime) > 15)
											//         OR MONTH(NOW()) > MONTH(vtiger_crmentity.createdtime)
											//       WHEN vtiger_accountscf.cf_1281 = 'Už visą mėn.' THEN MONTH(vtiger_crmentity.createdtime) < MONTH(NOW()) OR YEAR(NOW()) > YEAR(vtiger_crmentity.createdtime)                                          
											//       ELSE YEAR(vtiger_crmentity.createdtime) <= YEAR(NOW())                                      
											//     END
											//    ELSE '' END  ";     
											$sql .= " GROUP BY vtiger_salesorder.salesorderid 
											ORDER BY vtiger_salesorder.shipment_code ASC 
											LIMIT $startAt, $perPage";
											
											
		$records = $this->db->query($sql);

		return $records;
	}


	public function getAllOrders(){
		$sql = "SELECT vtiger_salesorder.salesorderid 
					FROM vtiger_salesorder 
					LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid
					LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid  
					LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid
					WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456 = 1 AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_salesordercf.cf_1468 = 0";
	
		$records = $this->db->query($sql);
		$allRecords = $records->rowCount();    
	
		return $allRecords;
	}

	public function invoicesPlan(){
		$plan = $this->db->pquery("SELECT total_invoices FROM vtiger_saskaitos_plan ORDER BY id DESC LIMIT 1",[]);
		return $this->db->query_result($plan, 0, 'total_invoices');
	}

	public function todayWritedInvoices(){
			$today_writed_invoices = $this->db->pquery("SELECT crmid , (SELECT productid FROM vtiger_inventoryproductrel WHERE id=crmid LIMIT 1) AS productid
													FROM vtiger_crmentity 
													WHERE DATE_FORMAT(createdtime,'%Y-%m-%d') = ? AND setype = 'Invoice' AND deleted = 0
													HAVING productid != 121391", [date('Y-m-d')]);
		return $today_writed_invoices->rowCount();
	}

}
