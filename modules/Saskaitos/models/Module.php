<?php

class Saskaitos_Module_Model extends Vtiger_Module_Model {


	public function getPostCodeInfo($post_code){
		$db = PearDatabase::getInstance();
		$result = $db->pquery("SELECT CONCAT(`city`,'/ ',IF(`city` != `zone_customer`, CONCAT(`zone_customer`,'/'),''),' ', `state`, '/ ', `zone_base`) AS `location` FROM crm_post_codes WHERE post_code = ?", array($post_code));
		return $db->query_result($result,0,'location');
	}

}