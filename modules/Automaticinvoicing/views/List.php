<?php

include "modules/Automaticinvoicing/models/invoicesModel.php";

use modules\Automaticinvoicing\models\invoicesModel as invoicesModel;

class Automaticinvoicing_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->invoicesModel = new invoicesModel;  
       }

	public function process(Vtiger_Request $request) {

              if($_POST['emailAddress']) {
			$this->invoicesModel->sendEmailToClient($_POST);
		}

                $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1; 
                $perPage = 50;   
                $startAt = $perPage * ($page - 1);
                $invoices = $this->invoicesModel->getInvoices($perPage,$startAt);
                $total = $this->invoicesModel->getAllInvoices();  
                $clients = $this->invoicesModel->getClients();
                $url = $site_URL."/index.php?module=Automaticinvoicing&view=List&app=SALES";
                $pagination = $this->get_pagination_links($page, ceil($total/$perPage), $url); 
                $viewer = $this->getViewer($request);                  
                $viewer->assign('INVOICES', $invoices);
                $viewer->assign('all_records', $total);
                $viewer->assign('pagination', $pagination);
                $viewer->assign('CLIENTS', $clients);
                $viewer->view('List.tpl', $request->getModule()); 
       }
       
       public function get_pagination_links($current_page, $total_pages, $url){
        $links = "";
        $links .= '<div class="pagination">';    
              if ($total_pages >= 1 && $current_page <= $total_pages) {
                     $links .= "<a ".($current_page == 1 ? 'class=active' : '')." href=\"{$url}&page=1\">1</a>";
                     $i = max(2, $current_page - 5);
                     if ($i > 2)
                     $links .= "<div>...</div>";
                     for (; $i < min($current_page + 6, $total_pages); $i++) {
                     $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$i}\">{$i}</a>";
                     }
                     if ($i != $total_pages)
                     $links .= "<div>...</div>";
                     $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$total_pages}\">{$total_pages}</a>";
              }
       $links .= '</div>';
       return $links;
       }
      


}
