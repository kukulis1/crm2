<?php
error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");  

  $accountid = $_POST['accountid'];
  $date_range = $_POST['dateRange']; 
  $byLoadCompany = $_POST['byLoadCompany'];  
  $filterBy = $_POST['filterBy'];


  // $filterBy = 'unload_date';
  // $date_range = '2021-01-01,2021-03-15';
  // $accountid = 2762;
  // $byLoadCompany = true;
 

  $date = explode(",",$date_range);

  $check_rule = $conn->query("SELECT autoinvoicingrules_tks_groupby	AS rule, accountname						
                                FROM vtiger_autoinvoicingrules r
                                JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
                                JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
                                JOIN vtiger_account a ON a.accountid=rel.crmid
                                WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND accountid = $accountid AND autoinvoicingrules_tks_autoinv = 1
                                GROUP BY relcrmid");

  $num_rows = mysqli_num_rows($check_rule);

  if($num_rows){

      $result = mysqli_fetch_assoc($check_rule);
      $table = $result['rule'];     

      $street = ($table == 'load_address' ? 'bill_street' : 'ship_street');
      $city = ($table == 'load_address' ? 'bill_city' : 'ship_city');
      $rule = ($table == 'load_address' ? 'Pasikrovimo adresą' : ($table == 'unload_address' ? 'Išsikrovimo adresą' : 'Užsakymą sukūrusi darbuotoja'));

      $query = "SELECT vtiger_salesorder.salesorderid,load_company, ROUND(SUM(total),2) as total, ".($table == 'employee' ? "IF(vtiger_salesorder.source = 'Metrika', IF(created_person IS NOT NULL, created_person, CONCAT(first_name,' ',last_name)) ,CONCAT(first_name,' ',last_name)) AS employee" : "REPLACE(REPLACE($street,',',' '),'.',' ') AS street,$city as city")."
                    FROM `vtiger_salesorder` 
                    LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid                                  
                    JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
                    JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid           
                    JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                    JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                    ".($table == 'employee' ? "JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smcreatorid" : "")."
                    LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid 
                    WHERE vtiger_crmentity.deleted = 0 AND vtiger_account.accountid = $accountid AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456  = 1 AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_salesordercf.cf_1468 = 0 ";
             

                  if($filterBy == 'order_date'){
                    $query .= " AND  DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') BETWEEN '$date[0]' AND '$date[1]' ";
                  }elseif($filterBy == 'load_date'){
                    $query .= " AND  vtiger_salesorder.load_date_from BETWEEN '$date[0]' AND '$date[1]' ";
                  }elseif($filterBy == 'unload_date'){
                    $query .= " AND  vtiger_salesorder.unload_date_to BETWEEN '$date[0]' AND '$date[1]' ";
                  }         

                  $query .=" GROUP BY vtiger_salesorder.salesorderid 
                             ORDER BY vtiger_salesorder.shipment_code DESC";   

                 
      $conn->query("SET SESSION group_concat_max_len = 1000000");
      $execute = $conn->query($query);

      $orders_array = array();
      foreach ($execute as $row){
        $orders_array[] = $row;
      }



     $accountname = strtolower(trim(clean($result['accountname'])));

     $grouped_addreses = array();
     if($table == 'employee'){
      for($i =0; $i < count($orders_array); $i++){    
        if(!empty($orders_array[$i]['salesorderid'])){
          if($byLoadCompany && strtolower(trim(clean($orders_array[$i]['load_company']))) == $accountname){
            $grouped_addreses[$orders_array[$i]['employee']."|##|".$orders_array[$i]['load_company']][] = $orders_array[$i]['salesorderid'];   
          }else{
            $grouped_addreses[$orders_array[$i]['employee']][] = $orders_array[$i]['salesorderid']; 
          }
        }
      }
     }else{
      $splited = splitAddresses($conn, $orders_array);
      for($i =0; $i < count($orders_array); $i++){    
        if(!empty($splited[$i]['salesorderid'])){
          if($byLoadCompany && strtolower($splited[$i]['load_company']) == $accountname){
            $grouped_addreses[$splited[$i]['street']."|##|".$splited[$i]['load_company']][$splited[$i]['city']."|##|".$splited[$i]['street'].$splited[$i]['nr']][] = $splited[$i]['salesorderid'];        
          }else{
            $grouped_addreses[$splited[$i]['street']][$splited[$i]['city']."|##|".$splited[$i]['street'].$splited[$i]['nr']][] = $splited[$i]['salesorderid'];      
          }   
        }
      } 
    }
 
    if($table == 'employee'){
      $employee = array();
      $sql =  $conn->query("SELECT employee FROM app_rules_employee WHERE accountid =  $accountid");  
      foreach($sql AS $row){
        $employee[] = $row['employee'];
      }          
    }else{
      $sql =  mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(address) AS addreses ,GROUP_CONCAT(city) AS cities FROM app_rules_$table WHERE accountid =  $accountid"));      
      $address = $sql['addreses'];
      $cities = $sql['cities']; 
      $cities =  explode(',',$cities);
      $address = cleanAddress($address,$cities); 

    }


    $final_array = array();
    if($table != 'employee'){

      foreach($grouped_addreses as $first_dimension){
        foreach ($first_dimension AS $key => $second_dimension) {

          $salesorderid =  implode(",",$second_dimension);   
          $explode = explode("|##|",$key);  
          $solid_address = $explode[1]."/".$explode[0];        
    
          $query = "SELECT COUNT(DISTINCT vtiger_salesorder.salesorderid) AS orders, GROUP_CONCAT(vtiger_salesorder.salesorderid) AS salesorderids, ROUND(SUM(total),2) as total,accountname, vtiger_account.accountid,$street AS street, $city as city, IF(COUNT(DISTINCT load_company) > 1, 'Keli klientai', load_company) AS load_company,GROUP_CONCAT(load_company) AS grouped_load_company
                  FROM `vtiger_salesorder` 
                  LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid                                  
                  JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
                  JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid           
                  JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                  JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                  LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid 
                  WHERE vtiger_crmentity.deleted = 0 AND vtiger_salesorder.salesorderid IN ($salesorderid) AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456  = 1 AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_salesordercf.cf_1468 = 0 
                  ORDER BY vtiger_salesorder.shipment_code DESC";          

          $query = $conn->query($query);

          foreach ($query AS $value) {
            $gruped = 0;
            if(in_array($solid_address,$address)) $gruped = 1;
            $final_array[] = array('salesorderids' => $value['salesorderids'],
                                  'street' => $value['street'], 
                                  'city' => $value['city'], 
                                  'orders' => $value['orders'], 
                                  'total' => $value['total'], 
                                  'load_company' => $value['load_company'], 
                                  'grouped_load_company' => $value['grouped_load_company'], 
                                  'gruped' => $gruped,
                                  'accountname' => $value['accountname']);
          }
        }   
      }
    }else{          


      foreach($grouped_addreses AS $key => $first_dimension){
        $salesorderid =  implode(",",$first_dimension);      
        $query = "SELECT COUNT(DISTINCT vtiger_salesorder.salesorderid) AS orders, GROUP_CONCAT(vtiger_salesorder.salesorderid) AS salesorderids, ROUND(SUM(total),2) as total,accountname, vtiger_account.accountid, IF(vtiger_salesorder.source = 'Metrika', IF(created_person IS NOT NULL, created_person, CONCAT(first_name,' ',last_name)) ,CONCAT(first_name,' ',last_name)) AS employee,$street AS street, $city as city, IF(COUNT(DISTINCT load_company) > 1, 'Keli klientai', load_company) AS load_company,GROUP_CONCAT(load_company) AS grouped_load_company
                FROM `vtiger_salesorder` 
                LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid                                  
                JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
                JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid           
                JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid 
                JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smcreatorid
                WHERE vtiger_crmentity.deleted = 0 AND vtiger_salesorder.salesorderid IN ($salesorderid) AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456  = 1 AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_salesordercf.cf_1468 = 0 
                ORDER BY vtiger_salesorder.shipment_code DESC";              

        $query = $conn->query($query);

        foreach ($query AS $value) {
          $gruped = 0;
          $explode = explode("|##|",$key);  
          $solid_employee = $explode[0]; 

          if(in_array($solid_employee,$employee)) $gruped = 1;
          $final_array[] = array('salesorderids' => $value['salesorderids'],
                                'employee' => $value['employee'],  
                                'street' => $value['street'], 
                                'city' => $value['city'],                               
                                'orders' => $value['orders'], 
                                'total' => $value['total'], 
                                'load_company' => $value['load_company'], 
                                'grouped_load_company' => $value['grouped_load_company'], 
                                'gruped' => $gruped,
                                'accountname' => $value['accountname']);
        }
      }
    }


      if(count($final_array) > 0){
        echo json_encode(array('status' => 'success' ,'res' => $final_array,'rule' => $rule, 'table' => ($table == 'load_address' ? 'load' : 'unload'))); 

      }else{
        echo json_encode(array('status' => 'empty' ,'res' => 'Užsakymų nerasta'));
      }



  }else{
    echo json_encode(array('status' => 'fail', 'res' => 'Nėra taisyklės'));
  }

}else{
  http_response_code(404);
}

function cleanAddress($street,$cities){
	$lietuviskos = array('Ą','Č','Ę','Ė','Į','Š','Ų','Ū','Ž','ą','č','ę','ė','į','š','ų','ū','ž');
	$lotyniskos = array('Ą','C','E','E','I','S','U','U','Z','a','c','e','e','i','s','u','u','z');
  $array = explode(',',$street);
  $address_array = array();    
  for($i = 0; $i < count($array); $i++){
    $arr = explode(" ",$array[$i]);
    $address_array[] = $arr;
  }

  $clean_adresses = array();
  $numbers = array();
  foreach ($address_array as $first_dimension) {
    for($f =0; $f < count($first_dimension); $f++){
      if(strlen($first_dimension[$f]) > 3){
        $clean_adresses[] = $first_dimension[$f];
        break;
      } 
    }
  } 

  foreach ($address_array as $first_dimension) {
    for($f =0; $f < count($first_dimension); $f++){ 
      if(is_numeric($first_dimension[$f])){
        $numbers[] = $first_dimension[$f];
        break;
      }else{
        $temp = substr($first_dimension[$f], 1);       
        if(is_numeric($temp)){
          $numbers[] = $temp;
          break;
        }else{
          $temp = substr($first_dimension[$f], 0,-1);
          if(is_numeric($temp)){
            $numbers[] = $temp;
            break;
          }
        }
      }
    }
  }  

  $merget_adresses = array();

  for($i = 0; $i < count($clean_adresses); $i++){
  	 $string = str_replace($lietuviskos,$lotyniskos,$clean_adresses[$i]);
  	 $string2 = str_replace($lietuviskos,$lotyniskos,$cities[$i]);
    // $string = @iconv('UTF-8', 'ASCII//TRANSLIT',$clean_adresses[$i]);
    $merget_adresses[] = $string.$numbers[$i]."/".$string2;
  }

  return $merget_adresses;
}


function splitAddresses($conn, $addresses){
	$lietuviskos = array('Ą','Č','Ę','Ė','Į','Š','Ų','Ū','Ž','ą','č','ę','ė','į','š','ų','ū','ž');
	$lotyniskos = array('Ą','C','E','E','I','S','U','U','Z','a','c','e','e','i','s','u','u','z');

  $cities = $conn->query("SELECT DISTINCT cities_tks_city AS city FROM `vtiger_cities` LEFT JOIN vtiger_crmentity ON crmid=citiesid WHERE deleted = 0 GROUP BY crmid");   
  
  $cities_array = array();

  foreach($cities as $row){
    if(!empty($row['city']))  $cities_array[] = $row['city'];
  }

  $cities_array[] = 'gatve';
  $cities_array[] = 'gatvė';

  $array = array();
  $address_array = array();

  $count = 0;
  foreach ($addresses as $key => $row) {
    $array = explode(" ",$row['street']);

    $load_company = trim(clean($row['load_company']));


    for($i = 0; $i < count($array); $i++){
      $string = trim(str_replace($lietuviskos,$lotyniskos,$array[$i]));

	    if($string != '-'){

	      if(!is_numeric($string)){
	        $cut = substr($string, 0, -1);
	        if(is_numeric($cut)){          
	          $address_array[$count]['nr'] = $cut; 
	          $address_array[$count]['city'] = $row['city'];        
	          $address_array[$count]['salesorderid'] = $row['salesorderid'];      
	          $address_array[$count]['load_company'] = $load_company;            
	        }else{
	          $cut2 = substr($string, 1);
	          if(is_numeric($cut2)){            
	            $address_array[$count]['nr'] = $cut2;
	            $address_array[$count]['city'] = $row['city'];	            
	            $address_array[$count]['salesorderid'] = $row['salesorderid']; 
	            $address_array[$count]['load_company'] = $load_company; 
     
	          }
	        }
	        $is_city = in_array($string,$cities_array);
	        if(strlen($string) > 3 && !$is_city){
	           $address_array[$count]['street'] = $string;   
	           $address_array[$count]['city'] = $row['city'];
	           $address_array[$count]['salesorderid'] = $row['salesorderid'];   
	           $address_array[$count]['load_company'] = $load_company;  
     
	        }else{
	        	if(is_numeric($string)){        
	        		$address_array[$count]['nr'] = $string;
	        	}
	        }

	      }elseif(is_numeric($string)){        
	        $address_array[$count]['nr'] = $string;
	        $address_array[$count]['city'] = $row['city'];           
	        $address_array[$count]['salesorderid'] = $row['salesorderid']; 
	        $address_array[$count]['load_company'] = $load_company;     
	      }

	  	}else{
	  		$address_array[$count]['nr'] = '';
	        $address_array[$count]['city'] = $row['city'];
	        $address_array[$count]['street'] = '123';              
	        $address_array[$count]['salesorderid'] = $row['salesorderid']; 
	        $address_array[$count]['load_company'] = $load_company;    
	  	}

    }  

    $count++;
  }

  return $address_array;
}


function clean($string) {
  $string = str_replace('UAB', '', $string); 
  $string = explode(' ',trim($string));       
  $string = preg_replace('/[^\da-z ]/i', '', $string[0]); // Removes special chars.     
  return $string; 
}

