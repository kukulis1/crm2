<?php
require_once "../../../config.inc.php";
// ini_set('display_errors',1);
error_reporting(0);

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");  

  $conn->query("SET SESSION group_concat_max_len = 1000000");

  $accountid = $_POST['accountid'];
  $orders_cheked = $_POST['orders_cheked'];
  $orders_uncheked = $_POST['orders_uncheked'];
  $userId = $_POST['userId'];

 // Patikrinam kokia taisykle del kuro priemokos nustatyta pas klienta
  $get_user_fuel_surcharge_rule = mysqli_fetch_assoc($conn->query("SELECT cf_1568 AS rule FROM vtiger_accountscf WHERE accountid = $accountid"));

  $user_fuel_surcharge_rule = $get_user_fuel_surcharge_rule['rule'];

  $check_rule = $conn->query("SELECT autoinvoicingrules_tks_groupby	AS rule						
                              FROM vtiger_autoinvoicingrules r
                              JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
                              JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
                              JOIN vtiger_account a ON a.accountid=rel.crmid
                              WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND accountid = $accountid AND autoinvoicingrules_tks_autoinv = 1
                              GROUP BY relcrmid");

  $result = mysqli_fetch_assoc($check_rule);

  if(count($orders_cheked) > 0){
    $e = 0;
    foreach ($orders_cheked as $orders) {
      checkAddress($orders,$accountid,$result['rule'],$conn);
      if($user_fuel_surcharge_rule == 'Taikoma užsakymui'){
        checkFuelSurchargeForOrder($conn,$accountid,$orders);	
      }
      $execute = $conn->query("SELECT IF(cf_1376 > 0, SUM(cf_1376), SUM(vtiger_salesorder.total)) AS total, vtiger_salesorder.accountid,CURDATE() + INTERVAL cf_1279 DAY as duedate,GROUP_CONCAT(vtiger_salesorder.salesorderid) AS id
                FROM `vtiger_salesorder`      
                JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid
                JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
                JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                WHERE vtiger_salesorder.salesorderid IN ($orders)                                                   
                ORDER BY vtiger_salesorder.salesorderid ASC");

 
      generateInvoice($execute,$conn,$userId,$accountid,$user_fuel_surcharge_rule);
      $e++;
    }
  }

  if(count($orders_uncheked) > 0){
    $orders2 = implode(',',$orders_uncheked);  
    if($user_fuel_surcharge_rule == 'Taikoma užsakymui'){
      foreach($orders_uncheked AS $salesorderid){
        checkFuelSurchargeForOrder($conn,$accountid,$salesorderid);	
      }
    }

    $execute = $conn->query("SELECT IF(cf_1376 > 0, SUM(cf_1376), SUM(vtiger_salesorder.total)) AS total, vtiger_salesorder.accountid,CURDATE() + INTERVAL cf_1279 DAY as duedate,GROUP_CONCAT(vtiger_salesorder.salesorderid) AS id
                FROM `vtiger_salesorder`  
                JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid      
                JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
                JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                WHERE vtiger_salesorder.salesorderid IN ($orders2)                      
                ORDER BY vtiger_salesorder.salesorderid ASC");

    generateInvoice($execute,$conn,$userId,$accountid,$user_fuel_surcharge_rule);
  }


  echo json_encode('success');

}else{
  http_response_code(404);
}

function checkAddress($order,$accountid,$table,$conn){
  $orders = explode(',',$order);

  $sql =  mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(address,city) AS addreses,GROUP_CONCAT(city) AS cities FROM app_rules_$table WHERE accountid =  $accountid"));      
  $cities = $sql['cities'];
  $cities = explode(',',$cities);
  $address = $sql['addreses'];
  $address = cleanAddress($address,$cities);

  $street = ($table == 'load_address' ? 'bill_street' : 'ship_street');
  $city = ($table == 'load_address' ? 'bill_city' : 'ship_city');

  foreach ($orders as $salesorderid) {
    $query = mysqli_fetch_assoc($conn->query("SELECT $street AS street, $city as city
                              FROM `vtiger_salesorder`                                  
                              JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid  
                              JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                              JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid   
                              WHERE vtiger_crmentity.deleted = 0 AND vtiger_salesorder.salesorderid = ($salesorderid) AND vtiger_crmentity.setype = 'SalesOrder'"));
                              
    $order_street = cleanOneAddress($query['street']);
    $order_street2 = $query['street'];  
    $order_city = $query['city'];
    $solid_address = $order_street."/".$order_city;     
    
    if(!in_array($solid_address,$address)){
      $table = 'app_rules_'.$table;
      $conn->query("INSERT INTO $table (accountid,address,city) VALUES ('$accountid','$order_street2','$order_city')");
    }
  } 

}


function generateInvoice($execute,$conn,$userId,$accountid,$user_fuel_surcharge_rule){ 
  foreach($execute as $single_order){    
    $entityid = last_entity_record_id($conn);
    $prev_inv_no = setModuleSeqNumber($conn);   
   
    $date = date('Y-m-d');
    $order_ids = $single_order['id'];  
    $dateTime = date("Y-m-d H:i:s");        
    
    $conn->query("INSERT INTO `vtiger_invoicecf` (`invoiceid`,`cf_1277`,`cf_1486`, `cf_2114`, `cf_1516`) VALUES ('$entityid','Debetinė','Transportas', 'LT', 'Pavedimu')");
    
    $conn->query("INSERT INTO `vtiger_crmentity` (`crmid`,`smcreatorid`,`smownerid`,`modifiedby`,`setype`,`createdtime`,`modifiedtime`,`source`,`label`) VALUES ('$entityid','$userId','$userId','$userId','Invoice', '$dateTime','$dateTime','CRM','".$prev_inv_no."') ");  
    
    $conn->query("INSERT INTO vtiger_invoice_auto_invoicing (invoiceid) VALUES ('$entityid')"); 
    
    $execute2 = $conn->query("SELECT vtiger_salesorder.salesorder_no, vtiger_salesorder.salesorderid ,vtiger_salesorder.total, vtiger_salesorder.shipment_code,  vtiger_inventoryproductrel.productid,vtiger_salesorder.accountid,
                  CONCAT(vtiger_salesorder.load_date_from,', ', vtiger_sobillads.load_company,', ', vtiger_sobillads.bill_street,', ', vtiger_sobillads.bill_city,', ', vtiger_sobillads.bill_code) AS bill_ads,
                  CONCAT(vtiger_salesorder.unload_date_to,', ', vtiger_soshipads.unload_company,', ', vtiger_soshipads.ship_street,', ', vtiger_soshipads.ship_city,', ', vtiger_soshipads.ship_code) AS ship_ads,
                  CASE WHEN  vtiger_accountscf.cf_1570
                  THEN vtiger_inventoryproductrel.note
                  ELSE '' 
                  END AS note,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN 'Products' ELSE 'Services' END as entitytype,
                  CASE WHEN vtiger_inventoryproductrel.productid = 36641 THEN vtiger_inventoryproductrel.service ELSE 0 END as service_id,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 
                  THEN 
                  IF(SUM(vtiger_inventoryproductrel.margin) = vtiger_salesorder.subtotal, IF(vtiger_inventoryproductrel.source = 'Metrika', FORMAT(SUM(vtiger_inventoryproductrel.margin)/count( vtiger_inventoryproductrel.id),2), 
                  SUM(vtiger_inventoryproductrel.margin)), vtiger_salesorder.subtotal)  
                        WHEN vtiger_inventoryproductrel.productid = 36641 AND inventory_type != 'FuelSurcharge' THEN vtiger_inventoryproductrel.margin 
                        WHEN vtiger_inventoryproductrel.productid = 36641 AND inventory_type = 'FuelSurcharge' THEN vtiger_inventoryproductrel.margin 
                END as margin,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN SUM(vtiger_inventoryproductrel.cargo_wgt) ELSE vtiger_inventoryproductrel.cargo_wgt END as cargo_wgt,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN GROUP_CONCAT(DISTINCT FORMAT(vtiger_inventoryproductrel.quantity,0),' ', app_measures.code)  ELSE vtiger_inventoryproductrel.cargo_wgt END as cargo,	
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_length) ELSE vtiger_inventoryproductrel.cargo_length END as cargo_length,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_width)  ELSE vtiger_inventoryproductrel.cargo_width END AS cargo_width,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  SUM(vtiger_inventoryproductrel.cargo_height) ELSE vtiger_inventoryproductrel.cargo_height END as cargo_height,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN  FORMAT(SUM(vtiger_inventoryproductrel.quantity),0)  ELSE  FORMAT(vtiger_inventoryproductrel.quantity,0) END AS quantity,
                  CASE WHEN vtiger_inventoryproductrel.productid != 36641 THEN CASE WHEN SUM(m3) > SUM(vtiger_inventoryproductrel.cargo_length*vtiger_inventoryproductrel.cargo_width*vtiger_inventoryproductrel.cargo_height) THEN ROUND(SUM(m3),3) ELSE ROUND(SUM(vtiger_inventoryproductrel.cargo_length*vtiger_inventoryproductrel.cargo_width*vtiger_inventoryproductrel.cargo_height),3) END  ELSE 0 END AS volume,vtiger_inventoryproductrel.service
                                FROM vtiger_salesorder                                           
                                LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
                                LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid
                                LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_salesorder.accountid
                                LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid
                                LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_salesorder.salesorderid                    
                                LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                                LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid 
                                LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid 
                                LEFT JOIN `app_measures` ON CASE WHEN  vtiger_inventoryproductrel.cargo_measure REGEXP '^[0-9]+$' THEN app_measures.id=vtiger_inventoryproductrel.cargo_measure ELSE app_measures.code =vtiger_inventoryproductrel.cargo_measure END
                                WHERE vtiger_crmentity.deleted = 0 AND vtiger_salesorder.salesorderid IN ($order_ids)      
                                GROUP BY vtiger_salesorder.salesorderid, vtiger_inventoryproductrel.productid
                                ORDER BY vtiger_salesorder.shipment_code ASC,sequence_no");



    $i = 1;
    $salesorderids = [];
    foreach($execute2 as $inv){ 
      $ship_ads = ($inv['productid'] == 14244 || $inv['productid'] == 0 ? clean($inv['ship_ads']) : '');
      $bill_ads = clean($inv['bill_ads']); 

      if(!empty($inv['salesorderid'])){
        $salesorderids[] = $inv['salesorderid'];
      }
      
      $conn->query("INSERT INTO vtiger_inventoryproductrel(id,productid,sequence_no,quantity,listprice,comment,cargo_measure,cargo_wgt,order_id,note,bill_ads,ship_ads,service,m3,costcenter,employee,object)	
      VALUES('$entityid','".$inv['productid']."','$i','".$inv['quantity']."','".$inv['margin']."','".$inv['note']."','".$inv['cargo']."','".$inv['cargo_wgt']."','".$inv['salesorderid']."','".$inv['note']."','$bill_ads','$ship_ads','".$inv['service']."','".$inv['volume']."','103914','','')");
      
      $num_rows = mysqli_num_rows($conn->query("SELECT salesorderid FROM vtiger_invoice_salesorders_list WHERE salesorderid = '".$inv['salesorderid']."'"));
      if(!$num_rows){
        $conn->query("INSERT INTO vtiger_invoice_salesorders_list (invoiceid, salesorderid) VALUES ('$entityid','".$inv['salesorderid']."')"); 
      }
      $i++;
    }


    $total = $single_order['total'];      
    // Paskaiciuojama kuro priemoka	
    if($user_fuel_surcharge_rule == 'Taikoma bendra'){		
      $sequence_no = $i;
      $fuel_surcharge =	checkFuelSurcharge($conn, $accountid, implode(',',$salesorderids));	
      if($fuel_surcharge > 0){        
        $total =  $total + $fuel_surcharge;   
        $insert_inventory = "INSERT INTO vtiger_inventoryproductrel (id, sequence_no, quantity, productid, listprice, note, service, costcenter) VALUES (:id, :sequence_no, :quantity, :productid, :listprice, :note, :service, :costcenter)";

        $conn->query(str_replace(array(':id', ':sequence_no', ':quantity', ':productid', ':listprice', ':note', ':service', ':costcenter'), array($entityid, $sequence_no,1 , 36641, $fuel_surcharge, "'Kuro priemoka'", 8, 103914), $insert_inventory));
      }
    }else if($user_fuel_surcharge_rule == 'Taikoma užsakymui'){
      $ids = implode(',',$salesorderids);
      $get_total_surcharge = mysqli_fetch_assoc($conn->query("SELECT SUM(margin) AS surcharge FROM vtiger_inventoryproductrel WHERE id IN ($ids) AND service = 8 AND inventory_type = 'FuelSurcharge'"));
      $total =  $total + $get_total_surcharge['surcharge']; 
    }     


    $s_h_amount = $total * 0.21;
    $total_with_vat = $total + $s_h_amount;
 
    $conn->query("INSERT INTO `vtiger_invoice` (`invoiceid`,`subject`,`salesorderid`,`invoicedate`,`duedate`,`subtotal`,`total`,`s_h_amount`,`accountid`,`invoicestatus`,`invoice_no`) VALUES ($entityid,'$prev_inv_no','$order_ids','$date','".$single_order['duedate']."','".$total."',$total_with_vat,$s_h_amount,$accountid,'Created','$prev_inv_no') ");

  }  
  
  return true;
}


function checkFuelSurchargeForOrder($db,$user_id,$order_ids){
  // Gaunam uzsakymo kainas ir uzsakymu datas
  $salesorder_string = "SELECT ROUND(total,2) AS total, DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') AS order_date, vtiger_salesorder.salesorderid, vtiger_salesorder.external_order_id ";
  $salesorder_string .= getInvoiceFromQueryForSurcharge($order_ids);
  $salesorder_string .= " GROUP BY vtiger_salesorder.salesorderid 	ORDER BY vtiger_salesorder.salesorderid ASC LIMIT 500";

  $get_history = $db->query("SELECT fuelpricelist_tks_galiojimopra AS price_start, fuelpricelist_tks_galiojimopab AS price_end, fuelpricelist_tks_kurokaina AS price
                              FROM vtiger_fuelpricelist
                              INNER JOIN vtiger_crmentity ON crmid=fuelpricelistid
                              WHERE setype = 'Fuelpricelist' AND deleted = 0");															

  // Gaunam kuro priemokos procenta pagal tai koks kuro priemokos procentas buvo taikomas uzsakymo diena
  $get_client_ranges = $db->query("SELECT clientfuelrange_tks_minkurokai AS min_price, clientfuelrange_tks_maxkurokai AS max_price, clientfuelrange_tks_priemoka AS surcharge
                                    FROM vtiger_clientfuelrange  
                                    LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_clientfuelrange.clientfuelrangeid   
                                    LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid                 
                                    WHERE fuel_entity.deleted=0 AND vtiger_crmentityrel.crmid = $user_id 
                                    ORDER BY clientfuelrangeid DESC");

  $orders = $db->query($salesorder_string);	
  
  $insert_inventory = "INSERT INTO vtiger_inventoryproductrel (id, sequence_no,external_order_id,productid,margin,note,service,inventory_type, costcenter) VALUES (:id,:sequence_no,:external_order_id,:productid,:margin,:note,:service,:inventory_type,:costcenter)";

  // Suskaiciuojam sumine kuro priemoka nuo uzsakymo sumos
  $check_are_exist_services = $db->query("SELECT id FROM vtiger_inventoryproductrel WHERE id IN ($order_ids) AND service = 8 AND inventory_type = 'FuelSurcharge'");


  $check_exist = [];
  foreach ($check_are_exist_services as $value) {
    $check_exist[$value['id']] = $value;
  }

  foreach($orders AS $row) {
    $salesorderid = $row['salesorderid'];
    if(empty($check_exist[$salesorderid])){      
      $get_sequence_no = $db->query("SELECT id FROM vtiger_inventoryproductrel WHERE id IN ($salesorderid)");
      $sequence_no = mysqli_num_rows($get_sequence_no)+1;

      $surcharge = [];		
      $calc_surcharge = 0;	
      foreach ($get_history as $his){
        if( $row['order_date'] >= $his['price_start'] &&  $row['order_date'] <= $his['price_end'] ){
          foreach ($get_client_ranges as $ran) {
            if($ran['min_price'] <= $his['price'] &&  $his['price'] <= $ran['max_price']){
              $surcharge[] = $ran['surcharge'];
            }			
          }
          $sur = max($surcharge);
        }
      }    

      $calc_surcharge = round(($row['total']/100) * $sur, 2);	
      if($calc_surcharge > 0){
        $db->query(str_replace(array(':id',':sequence_no',':external_order_id',':productid',':margin',':note',':service',':inventory_type',':costcenter'),array($salesorderid, $sequence_no, $row['external_order_id'], 36641, $calc_surcharge, "'Kuro priemoka'", 8, "'FuelSurcharge'", 103914),$insert_inventory));
      }		
    }		
  }

}

function checkFuelSurcharge($db, $user_id, $order_ids){
  // Gaunam uzsakymo kainas ir uzsakymu datas
  $salesorder_string = "SELECT ROUND(total,2) AS total, DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') AS order_date ";
  $salesorder_string .= getInvoiceFromQueryForSurcharge($order_ids);
  $salesorder_string .= " GROUP BY vtiger_salesorder.salesorderid	ORDER BY vtiger_salesorder.salesorderid ASC LIMIT 500";

  $get_history = $db->query("SELECT fuelpricelist_tks_galiojimopra AS price_start, fuelpricelist_tks_galiojimopab AS price_end, fuelpricelist_tks_kurokaina AS price
                              FROM vtiger_fuelpricelist
                              INNER JOIN vtiger_crmentity ON crmid=fuelpricelistid
                              WHERE setype = 'Fuelpricelist' AND deleted = 0");															

  // Gaunam kuro priemokos procenta pagal tai koks kuro priemokos procentas buvo taikomas uzsakymo diena
  $get_client_ranges = $db->query("SELECT clientfuelrange_tks_minkurokai AS min_price, clientfuelrange_tks_maxkurokai AS max_price, clientfuelrange_tks_priemoka AS surcharge
                                    FROM vtiger_clientfuelrange  
                                    LEFT JOIN vtiger_crmentityrel ON vtiger_crmentityrel.relcrmid = vtiger_clientfuelrange.clientfuelrangeid   
                                    LEFT JOIN vtiger_crmentity AS fuel_entity ON fuel_entity.crmid=vtiger_crmentityrel.relcrmid 
                                    WHERE fuel_entity.deleted=0 AND vtiger_crmentityrel.crmid = $user_id 
                                    ORDER BY clientfuelrangeid DESC");                               
    
  $orders = $db->query($salesorder_string);

  // Suskaiciuojam sumine kuro priemoka nuo uzsakymo sumos
  $total_surcharge = 0;
  $total = 0;
  foreach($orders AS $row) {

    $surcharge = [];			
    foreach ($get_history as $his){

      if( $row['order_date'] >= $his['price_start'] &&  $row['order_date'] <= $his['price_end'] ){		
        foreach ($get_client_ranges as $ran) {
          if($ran['min_price'] <= $his['price'] &&  $his['price'] <= $ran['max_price']){
            $surcharge[] = $ran['surcharge'];
          }			
        }		
        $sur = max($surcharge);
      }    
    
    }


    $total_surcharge += ($row['total']/100) * $sur;	
    $total += $row['total'];		
  }

  if($total_surcharge > 0) 	$total_surcharge = round($total_surcharge,2);
  return $total_surcharge;	
}


function getInvoiceFromQueryForSurcharge($order_ids){
  $salesorder_string ="	FROM vtiger_salesorder                               
  LEFT JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
  LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid                 
  LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid 	
  LEFT JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
  LEFT JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid 	
  LEFT JOIN `vtiger_users` ON vtiger_users.id=vtiger_crmentity.smownerid		
  WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'SalesOrder'  AND vtiger_salesordercf.cf_1468 = 0 AND vtiger_salesordercf.cf_1456 = 1  AND  vtiger_salesorder.salesorderid IN ($order_ids) ";    								

  return $salesorder_string;								 
}



function last_entity_record_id($conn)
{
  $result = $conn->query("SELECT id FROM vtiger_crmentity_seq");
  $id = $result->fetch_assoc();
  $crmid = $id['id'] + 1;  
  $conn->query("UPDATE vtiger_crmentity_seq SET id = $crmid");  
  return $crmid;
}


function setModuleSeqNumber($conn){

  $today = date('Y-m-d');

  $check_cur = mysqli_fetch_assoc($conn->query("SELECT invoice_no FROM vtiger_invoice 
                                                LEFT JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid 
                                                LEFT JOIN vtiger_invoicecf ON vtiger_invoicecf.invoiceid=vtiger_invoice.invoiceid
                                                WHERE DATE_FORMAT(vtiger_crmentity.createdtime, '%Y-%m-%d') = '$today' AND vtiger_invoicecf.cf_1277 LIKE 'Debetinė' AND vtiger_crmentity.deleted = 0 
                                                ORDER BY vtiger_invoice.invoice_no  
                                                DESC LIMIT 1"));

  $curid = $check_cur['invoice_no'];
  if(!empty($curid)){
    $curid = substr($curid, -3);
  }else{
    $req_no = '001';
  }

  $prefix = 'PT';
  $todayDate = date('y/m/d');
  if(!empty($curid)){
    $strip = strlen($curid) - strlen($curid + 1);
    if ($strip < 0) $strip = 0;
    $temp = str_repeat("0", $strip);
    $req_no.= $temp . ($curid + 1);
  }

  $prev_inv_no = $prefix.$todayDate."/".$req_no;
  return $prev_inv_no;
}

function cleanAddress($street,$cities){
  $array = explode(',',$street);

  $address_array = array();    
  for($i = 0; $i < count($array); $i++){
    $arr = explode(" ",$array[$i]);
    $address_array[] = $arr;
  }

  $clean_adresses = array();
  foreach ($address_array as $first_dimension) {
    for($f =0; $f < count($first_dimension); $f++){
      if(strlen($first_dimension[$f]) > 3){
        $clean_adresses[] = $first_dimension[$f];
        break;
      }
    }
  } 

  $merget_adresses = array();

  for($i = 0; $i < count($clean_adresses); $i++){
    $merget_adresses[] = $clean_adresses[$i]."/".$cities[$i];
  }
  return $merget_adresses;
}

function cleanOneAddress($string){
  $array = explode(" ",$string);   
  $address = '';
  for($f =0; $f < count($array); $f++){
    if(strlen($array[$f]) > 3){
      $address = $array[$f];
      break;
    }
  }
  
  return $address;
}


function clean($str) {  
  $res = str_replace(array('"', '„','“',"'"), array('','','',''), $str);      
  return $res; 
} 
