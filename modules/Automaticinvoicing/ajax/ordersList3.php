<?php
error_reporting(0);
require_once "../../../config.inc.php";


if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");  


  $accountid = $_POST['accountid'];
  $date_range = $_POST['dateRange'];
  
  $date = explode(",",$date_range);

  $check_rule = $conn->query("SELECT autoinvoicingrules_tks_groupby	AS rule						
                                FROM vtiger_autoinvoicingrules r
                                JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
                                JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
                                JOIN vtiger_account a ON a.accountid=rel.crmid
                                WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND accountid = $accountid AND autoinvoicingrules_tks_autoinv = 1
                                GROUP BY relcrmid");

  $num_rows = mysqli_num_rows($check_rule);

  if($num_rows){

      $result = mysqli_fetch_assoc($check_rule);
      $table = $result['rule']; 
   
      $sql =  mysqli_fetch_assoc($conn->query("SELECT GROUP_CONCAT(\"'\",address,\"'\") AS addreses FROM app_rules_$table WHERE accountid =  $accountid"));      
      $address = $sql['addreses'];    

      $groupBy = ($table == 'load_address' ? 'bill_street' : 'ship_street');
      $rule = ($table == 'load_address' ? 'Pasikrovimo adresą' : 'Išsikrovimo adresą');

      $query = "SELECT COUNT(DISTINCT vtiger_salesorder.salesorderid) AS orders, GROUP_CONCAT(vtiger_salesorder.salesorderid) AS salesorderids, ROUND(SUM(total),2) as total,accountname, vtiger_account.accountid, GROUP_CONCAT(DISTINCT $groupBy) AS street, IF($groupBy IN ($address),true,false) AS gruped
                    FROM `vtiger_salesorder` 
                    LEFT JOIN `vtiger_salesordercf` ON vtiger_salesordercf.salesorderid=vtiger_salesorder.salesorderid                                  
                    JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_salesorder.salesorderid               
                    JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_salesorder.accountid           
                    JOIN `vtiger_soshipads` ON vtiger_soshipads.soshipaddressid=vtiger_salesorder.salesorderid
                    JOIN `vtiger_sobillads` ON vtiger_sobillads.sobilladdressid=vtiger_salesorder.salesorderid
                    LEFT JOIN `vtiger_invoice_salesorders_list` ON vtiger_invoice_salesorders_list.salesorderid=vtiger_salesorder.salesorderid 
                    WHERE vtiger_crmentity.deleted = 0 AND vtiger_account.accountid = $accountid AND vtiger_crmentity.setype = 'SalesOrder' AND vtiger_salesordercf.cf_1456  = 1 AND vtiger_invoice_salesorders_list.salesorderid IS NULL AND vtiger_salesordercf.cf_1468 = 0 AND DATE_FORMAT(createdtime,'%Y-%m-%d') BETWEEN '$date[0]' AND '$date[1]'
                    GROUP BY $groupBy
                    ORDER BY vtiger_salesorder.shipment_code DESC";

      $conn->query("SET SESSION group_concat_max_len = 1000000");
      $execute = $conn->query($query);

      $orders_array = array();
      foreach ($execute as $row){
        $orders_array[] = $row;
      }

      if(count($orders_array) > 0){
        echo json_encode(array('status' => 'success' ,'res' => $orders_array,'rule' => $rule, 'table' => ($table == 'load_address' ? 'load' : 'unload')));
      }else{
        echo json_encode(array('status' => 'empty' ,'res' => 'Užsakymų nerasta'));
      }



  }else{
    echo json_encode(array('status' => 'fail', 'res' => 'Nėra taisyklės'));
  }



}else{
  http_response_code(404);
}