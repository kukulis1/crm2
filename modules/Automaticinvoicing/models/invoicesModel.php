<?php
namespace modules\Automaticinvoicing\models;

class invoicesModel {
  public function __construct(){
    global $adb;
    $this->db = $adb;
  }


 public function getInvoices($perPage,$startAt){
  
    $sql = "SELECT vtiger_invoice.invoiceid, accountname, invoicedate,invoice_no,ROUND(total,2) AS total , duedate, CONCAT(u.first_name,' ',u.last_name) AS created_invoice, CONCAT(responsible_for_debts.first_name,' ',responsible_for_debts.last_name) AS resp_debts, CONCAT(responsible_for_orders.first_name,' ',responsible_for_orders.last_name) AS resp_orders, createdtime,vtiger_invoice.accountid
                                               FROM `vtiger_invoice`                                           
                                               JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid                                        
                                               LEFT JOIN `vtiger_account` ON vtiger_account.accountid=vtiger_invoice.accountid
                                               LEFT JOIN `vtiger_accountscf` ON vtiger_accountscf.accountid=vtiger_invoice.accountid
                                               LEFT JOIN `vtiger_users` u ON u.id=vtiger_crmentity.smownerid     
                                               LEFT JOIN `vtiger_users` responsible_for_debts ON responsible_for_debts.id=vtiger_accountscf.cf_1657   
                                               LEFT JOIN `vtiger_users` responsible_for_orders ON responsible_for_orders.id=vtiger_accountscf.cf_1659                                                 
                                               LEFT JOIN `vtiger_inventoryproductrel` ON vtiger_inventoryproductrel.id=vtiger_invoice.invoiceid       
                                               JOIN `vtiger_invoice_auto_invoicing` ON vtiger_invoice_auto_invoicing.invoiceid=vtiger_invoice.invoiceid 
                                               WHERE vtiger_crmentity.setype = 'Invoice' AND vtiger_crmentity.deleted = 0  
                                               GROUP BY vtiger_invoice.invoiceid      
                                               ORDER BY vtiger_crmentity.createdtime DESC, invoice_no DESC 
                                               LIMIT $startAt, $perPage";
                                          
                                           
    $records = $this->db->query($sql);

    return $records;
 }

 public function getClients(){
  $sql = "SELECT accountid, accountname, autoinvoicingrules_tks_groupby as rule 								
                  FROM vtiger_autoinvoicingrules r
                  JOIN vtiger_crmentity e ON e.crmid=r.autoinvoicingrulesid
                  JOIN vtiger_crmentityrel rel ON rel.relcrmid=r.autoinvoicingrulesid AND relmodule = 'Autoinvoicingrules'
                  JOIN vtiger_account a ON a.accountid=rel.crmid
                  WHERE deleted = 0 AND setype = 'Autoinvoicingrules' AND autoinvoicingrules_tks_autoinv = 1
                  GROUP BY relcrmid";

  $records = $this->db->query($sql);

  return $records;
 }


 public function getAllInvoices(){
   $sql = "SELECT vtiger_invoice.invoiceid  
             FROM vtiger_invoice 
             JOIN `vtiger_crmentity` ON vtiger_crmentity.crmid=vtiger_invoice.invoiceid             
             JOIN `vtiger_invoice_auto_invoicing` ON vtiger_invoice_auto_invoicing.invoiceid=vtiger_invoice.invoiceid  
             WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.setype = 'Invoice'";
 
     $records = $this->db->query($sql);
     $allRecords = $records->rowCount();    
 
     return $allRecords;
 }

 public function sendEmailToClient($post){
	include "vtlib/Vtiger/ShortLink/generateShortLink.php";
		global $adb;
		global $current_user;
    $recordId = $post['invoiceid'];

		$sql = 'SELECT subject FROM vtiger_invoice WHERE invoiceid = ?';
		$sql5 = 'SELECT * FROM app_other_settings WHERE title = ?';

		$result = $adb->pquery($sql, array($recordId));
		$subject = $adb->query_result($result, 0, 'subject');

		$subject_pure = $adb->query_result($result, 0, 'subject');
		$subject = str_replace('/','',$subject);
		$date_with_20_days = date('Y-m-d H:i:s', strtotime("+20 days"));

		$filename = $subject.".pdf";
		$path = 'storage/invoices';    
		$file = $path . "/" . $filename;


		$subject = "Parnasas saskaita nr. $subject_pure";  
		$to = $post['emailAddress'];  
		$hash = md5( rand(0,1000) ); 
		$message = $post['message'];

		$result2 = $adb->pquery($sql5, array('LBL_SEND_INVOICE_TEXT'));
		$text = $adb->query_result($result2, 0, 'value');

		

		$confirmation_message = $url;

		// $getUrl = createShortLink("http://".$_SERVER["HTTP_HOST"]."/downloadInvoice/download.php?invoice=$recordId&secret=$hash");
        // $url = $getUrl['responce']['link'];
		// $id = $getUrl['responce']['id'];

        $url = "https://uzsakymai.parnasas.lt/download-invoice/index.php?";
        $url .= http_build_query([
            'invoice' => $recordId,
            'secret' => $hash
        ]);
        
		$confirmation_message = $text." ".$url;
		
    // $content = file_get_contents($file);
    // $content = chunk_split(base64_encode($content));
    
    // a random hash will be necessary to send mixed content
    $separator = md5(time());
    
    // carriage return type (RFC)
    $eol = "\r\n";
    
    // main header (multipart mandatory)
    $headers = "From: Parnasas saskaita nr. $subject_pure <saskaita@parnasas.lt>" . $eol;
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
    $headers .= "This is a MIME encoded message." . $eol;
    
    // message
    $body = "--" . $separator . $eol;
    $body .= "Content-Type: text/plain; charset=\"utf-8\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol;
    $body .= $eol . $message . $eol . $eol;
    $body .= str_replace(array('<br>','<br/>','<br />',"&lt;br /&gt;"),array("\n","\n","\n","\n"),$confirmation_message.$eol);
    
    // attachment
    // $body .= "--" . $separator . $eol;
    // $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
    // $body .= "Content-Transfer-Encoding: base64" . $eol;
    // $body .= "Content-Disposition: attachment" . $eol;
    // $body .= $eol . $content . $eol . $eol;
    $body .= "--" . $separator . "--";


    mail($to, $subject, $body, $headers);
		// unlink($file);

	
		$sql2 = 'SELECT invoiceid FROM vtiger_send_invoice_for_client WHERE invoiceid = ?';
		$sql3 = 'INSERT INTO vtiger_send_invoice_for_client (invoiceid, `count`, `hash`, `status`,`invoice_no`,`employee`,`url`,`expired`) VALUES (?,?,?,?,?,?,?,?)';
		$sql4 = 'UPDATE vtiger_send_invoice_for_client SET `count` = `count`+?, `hash` = ?, `status` = ?, employee = ?, `url` = ?, `expired` = ?  WHERE invoiceid = ?';

		$result2 = $adb->pquery($sql2, array($recordId));	
		$num_rows = $adb->num_rows($result2);

		if(!$num_rows){
			$adb->pquery($sql3, array($recordId,1,$hash,'success',$filename,$current_user->id,$url,$date_with_20_days));
		}else{
		 $check =	$adb->pquery($sql4, array(1,$hash,'success',$current_user->id,$url,$date_with_20_days,$recordId));
		}
		
    $referer = $_SERVER['PHP_SELF']."?module=Automaticinvoicing&view=List";
    return header("Location: $referer");
}

}
