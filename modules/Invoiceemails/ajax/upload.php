<?php

require_once "../../../config.inc.php";
$root = $_SERVER['DOCUMENT_ROOT'];

if($_POST){
  $conn = new mysqli($dbconfig['db_server'], $dbconfig['db_username'], $dbconfig['db_password'], $dbconfig['db_name']);
  $conn ->set_charset("utf8");

  $date = date('Y-m-d H:i:s');
  $mail_id = $_POST['mail_id'];
  $file_name = $_FILES['file']['name'];
  $file_size =$_FILES['file']['size'];
  $file_tmp =$_FILES['file']['tmp_name'];
  $ext=strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
  $file_name_without_ext = pathinfo($file_name, PATHINFO_FILENAME);

  $time = time();
  $newName = lt_chars(str_replace(' ', '',pathinfo($file_name, PATHINFO_FILENAME))); 
  $newName = $newName."_".$time;

  $year = date("Y");
  $month = date("F");
  $dayofweek = "week".weekOfMonth(strtotime(date("Y-m-d")));  

  if (!file_exists("$root/storage/$year/$month/$dayofweek")) {
    mkdir("$root/storage/$year/$month/$dayofweek", 0777, true);
  }

  $target_dir = "$root/storage/$year/$month/$dayofweek/"; 
  $uploadfile = $target_dir . $newName.".".$ext; 
  $filename_with_ext =  $newName.".".$ext;
  $file_path = "storage/$year/$month/$dayofweek/"; 

  if (move_uploaded_file($file_tmp, $uploadfile)) {      
    $conn->query("INSERT INTO vtiger_invoiceemails_files (email_id, filename, path, createtime) VALUES ('$mail_id','$filename_with_ext','$file_path','$date')");
    echo json_encode(array('status' => 'success', 'mail_id' => $mail_id,'date' => $date, 'filepath' => $file_path.$filename_with_ext));
  }else{
    echo json_encode(array('status' =>'fail'));
  }
    
}else{
  http_response_code(404);
}
  
function lt_chars($text) {
  $char = array(
  "ą" => "a",
  "Ą" => "A",
  "č" => "c",
  "Č" => "C",
  "ę" => "e",
  "Ę" => "E",
  "ė" => "e",
  "Ė" => "E",   
  "į" => "i",
  "Į" => "I",
  "š" => "s",
  "Š" => "S",
  "ų" => "u",
  "Ų" => "U",
  "ū" => "u",
  "Ū" => "U",
  "ž" => "z",
  "Ž" => "Z");
  
  foreach ($char as $lt => $nlt) { 
    $text = str_replace($lt, $nlt, $text); 
  } 

  return $text; 
}

function weekOfMonth($date){  
  $firstOfMonth = strtotime(date("Y-m-01", $date));
  return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}
