<?php
namespace modules\Invoiceemails\models;

require_once 'vtlib/Vtiger/Email/vendor/autoload.php';

use PhpImap\Exceptions\ConnectionException;
use PhpImap\Mailbox;

class mailModel {

  public function __construct()
  {
    global $adb;
    $this->db = $adb;
  }


 public function getAndSaveMails()
 {
    global $mailbox_server,$mailbox_username,$mailbox_pass;
  
    $mailbox = new Mailbox($mailbox_server,$mailbox_username,$mailbox_pass);  

    $sql = "INSERT INTO app_invoice_mails (title, name, body, from_address, filename, original_file_name, ext,message_id, message_no) VALUES (?,?,?,?,?,?,?,?, ?)";
    $sql2 = "SELECT id FROM app_invoice_mails WHERE message_id = ?";
   
    $checktime = "SELECT createdate FROM app_invoice_mails ORDER BY id DESC LIMIT 1";   
    $result = $this->db->pquery($checktime,array());
    $last_check = $this->db->query_result($result,0,'createdate'); 
    $last_check = date("Ymd",strtotime($last_check)); 
    $year = date("Y");
    $month = date("F");
    $dayofweek = "week".$this->weekOfMonth(strtotime(date("Y-m-d"))); 
    
    try {
        $mail_ids = $mailbox->searchMailbox('SINCE "'.$last_check.'"');
    } catch (ConnectionException $ex) {
        die('IMAP connection failed: '.$ex->getMessage());
    } catch (Exception $ex) {
        die('An error occured: '.$ex->getMessage());
    } 

    foreach ($mail_ids AS $mail_id) {   

        $email = $mailbox->getMail(
            $mail_id, 
            true 
        );

        if ($email->hasAttachments()) {
          $subject = $email->subject;
          $sender_name = $email->fromName;
          $from = $email->fromAddress;
          $body = $email->textPlain;              
          $attachments = $email->getAttachments();
          

          foreach ($attachments AS $attachment) {         
            
            if (!file_exists("storage/$year/$month/$dayofweek")) {
              mkdir("storage/$year/$month/$dayofweek", 0777, true);
            } 

              $ext = pathinfo($attachment->name, PATHINFO_EXTENSION); 
              $oldName = $this->lt_chars(str_replace(' ', '',pathinfo($attachment->name, PATHINFO_FILENAME)));                             
                if(empty($ext)) $ext = $oldName;
                $newFileName = $oldName.'_'.time().'.'.$ext;          
                $attachment->setFilePath("storage/$year/$month/$dayofweek/$newFileName");
                $filename = "storage/$year/$month/$dayofweek/$newFileName";
                $original_file_name = $attachment->name;                  
                $result2 = $this->db->pquery($sql2, array($attachment->id));               
                // if(!$this->db->num_rows($result2) && $ext != 'jpeg' && $ext != 'jpg' && $ext != 'JPEG' && $ext != 'JPG'){
                if(!$this->db->num_rows($result2) && $oldName != 'jpeg'){ 
                  if ($attachment->saveToDisk()) {              
                     $this->db->pquery($sql, array($subject,$sender_name,$body,$from,$filename,$original_file_name,$ext,$attachment->id, $mail_id));                                  
                  } else {
                    echo "ERROR, could not save!<br>";
                  }  
                }                
          }            
        }
    }

    $mailbox->disconnect();   

    header("Location: index.php?module=Invoiceemails&view=List");
  
 }

 public function weekOfMonth($date) 
 {  
  $firstOfMonth = strtotime(date("Y-m-01", $date));
  return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
 }

 public function getMails($perPage,$startAt)
 {
  $sql = "SELECT title, name, body, from_address, GROUP_CONCAT(m.filename SEPARATOR '|##|') as filename, GROUP_CONCAT(m.filename SEPARATOR '|del|') as filenameDelete, GROUP_CONCAT(original_file_name SEPARATOR '|##|') AS original_file_name, seen, createdate,
  CASE WHEN message_no THEN message_no ELSE id END AS email_id, f.filename AS new_file, f.path AS new_path,f.createtime,comments
            FROM app_invoice_mails m
            LEFT JOIN vtiger_invoiceemails_files f ON CASE WHEN message_no THEN email_id=message_no ELSE id=f.email_id END 
            LEFT JOIN vtiger_invoiceemails_comments c ON c.email_id=m.message_no 
            WHERE status IS NULL AND deleted = 0 
            GROUP BY CASE WHEN message_no IS NULL THEN id ELSE message_no END
            ORDER BY id DESC LIMIT $startAt, $perPage";
  return $this->db->pquery($sql, array());
 }

 public function deleteDocument($id,$document, $page)
 {
   $documents = explode('|del|', $document);
   foreach($documents as $doc){
    unlink($doc);
   }
  //  $query = $this->db->pquery("SELECT id FROM app_invoice_mails WHERE message_no = $id");	
  //  $check =  $this->db->num_rows($query);
  //  $where = ($check ? 'message_no' : 'id');
  //  $sql = "UPDATE app_invoice_mails SET deleted = 1 WHERE  $where = ?";
    $sql = "DELETE FROM vtiger_invoiceemails_files WHERE email_id = ?";
   $this->db->pquery($sql, array($id));
   if(empty($page)) $page = 1;
   header("Location: index.php?module=Invoiceemails&view=List&page=$page");
 }

 public function getTotalNum()
 {
  $sql = "SELECT COUNT(id) AS total FROM app_invoice_mails WHERE status IS NULL AND deleted = 0";
  $result = $this->db->pquery($sql);
  $total = $this->db->query_result($result,0,'total');
  return $total;
 }

 public function lt_chars($text) {   
    $char = array(
    "ą" => "a",
    "Ą" => "A",
    "č" => "c",
    "Č" => "C",
    "ę" => "e",
    "Ę" => "E",
    "ė" => "e",
    "Ė" => "E",   
    "į" => "i",
    "Į" => "I",
    "š" => "s",
    "Š" => "S",
    "ų" => "u",
    "Ų" => "U",
    "ū" => "u",
    "Ū" => "U",
    "ž" => "z",
    "Ž" => "Z");
    
    foreach ($char as $lt => $nlt) { 
      $text = str_replace($lt, $nlt, $text); 
    } 

  return $text; 
}

}
