<?php

include "modules/Invoiceemails/models/mailModel.php";
use modules\Invoiceemails\models\mailModel as mailModel;

class Invoiceemails_List_View extends Vtiger_Index_View {

       function __construct(){
              $this->mailModel = new mailModel;  
       }

	public function process(Vtiger_Request $request) {

              
                $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1; 
                $perPage = 20;   
                $page = $_GET['page'];
                if(empty($page)) $page = 1;
                $startAt = $perPage * ($page - 1);                
                $total = $this->mailModel->getTotalNum();
                $url = $site_URL."/index.php?module=Invoiceemails&view=List";
                $pagination = $this->get_pagination_links($page, ceil($total/$perPage), $url);   
                $mails = $this->mailModel->getMails($perPage,$startAt);          
                $viewer = $this->getViewer($request);                  
                $viewer->assign('MAILS', $mails);
                $viewer->assign('page', $page);
                $viewer->assign('pagination', $pagination);               
                $viewer->view('List.tpl', $request->getModule()); 
       }

       public function get_pagination_links($current_page, $total_pages, $url){
              $links = "";
              $links .= '<div class="pagination">';    
                    if ($total_pages >= 1 && $current_page <= $total_pages) {
                           $links .= "<a ".($current_page == 1 ? 'class=active' : '')." href=\"{$url}&page=1\">1</a>";
                           $i = max(2, $current_page - 5);
                           if ($i > 2)
                           $links .= "<div>...</div>";
                           for (; $i < min($current_page + 6, $total_pages); $i++) {
                           $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$i}\">{$i}</a>";
                           }
                           if ($i != $total_pages)
                           $links .= "<div>...</div>";
                           $links .= "<a ".($current_page == $i ? 'class=active' : '')." href=\"{$url}&page={$total_pages}\">{$total_pages}</a>";
                    }
             $links .= '</div>';
             return $links;
       }
       
}
