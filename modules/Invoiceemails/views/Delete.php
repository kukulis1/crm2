<?php

include "modules/Invoiceemails/models/mailModel.php";
use modules\Invoiceemails\models\mailModel as mailModel;

class Invoiceemails_Delete_View extends Vtiger_Index_View {

  function __construct(){
        $this->mailModel = new mailModel;  
  }

	public function process(Vtiger_Request $request) {
      $id = $_GET['delete'];
      $page = $_GET['page'];
      $document = $_GET['document'];
      $mails = $this->mailModel->deleteDocument($id,$document, $page);             
               
  }
       
}
