<?php

include "modules/Invoiceemails/models/mailModel.php";
use modules\Invoiceemails\models\mailModel as mailModel;

class Invoiceemails_updateEmails_View extends Vtiger_Index_View {

  function __construct(){
        $this->mailModel = new mailModel;  
  }

	public function process(Vtiger_Request $request) {
        $this->mailModel->getAndSaveMails(); 
  }
       
}
